package blockchain;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$Ed25519Public;
import com.mobilecoin.api.MobileCoinAPI$Ed25519Signature;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes.dex */
public final class Blockchain$BlockSignature extends GeneratedMessageLite<Blockchain$BlockSignature, Builder> implements MessageLiteOrBuilder {
    private static final Blockchain$BlockSignature DEFAULT_INSTANCE;
    private static volatile Parser<Blockchain$BlockSignature> PARSER;
    public static final int SIGNATURE_FIELD_NUMBER;
    public static final int SIGNED_AT_FIELD_NUMBER;
    public static final int SIGNER_FIELD_NUMBER;
    private MobileCoinAPI$Ed25519Signature signature_;
    private long signedAt_;
    private MobileCoinAPI$Ed25519Public signer_;

    private Blockchain$BlockSignature() {
    }

    public boolean hasSignature() {
        return this.signature_ != null;
    }

    public MobileCoinAPI$Ed25519Signature getSignature() {
        MobileCoinAPI$Ed25519Signature mobileCoinAPI$Ed25519Signature = this.signature_;
        return mobileCoinAPI$Ed25519Signature == null ? MobileCoinAPI$Ed25519Signature.getDefaultInstance() : mobileCoinAPI$Ed25519Signature;
    }

    public void setSignature(MobileCoinAPI$Ed25519Signature mobileCoinAPI$Ed25519Signature) {
        mobileCoinAPI$Ed25519Signature.getClass();
        this.signature_ = mobileCoinAPI$Ed25519Signature;
    }

    public void setSignature(MobileCoinAPI$Ed25519Signature.Builder builder) {
        this.signature_ = builder.build();
    }

    public void mergeSignature(MobileCoinAPI$Ed25519Signature mobileCoinAPI$Ed25519Signature) {
        mobileCoinAPI$Ed25519Signature.getClass();
        MobileCoinAPI$Ed25519Signature mobileCoinAPI$Ed25519Signature2 = this.signature_;
        if (mobileCoinAPI$Ed25519Signature2 == null || mobileCoinAPI$Ed25519Signature2 == MobileCoinAPI$Ed25519Signature.getDefaultInstance()) {
            this.signature_ = mobileCoinAPI$Ed25519Signature;
        } else {
            this.signature_ = MobileCoinAPI$Ed25519Signature.newBuilder(this.signature_).mergeFrom((MobileCoinAPI$Ed25519Signature.Builder) mobileCoinAPI$Ed25519Signature).buildPartial();
        }
    }

    public void clearSignature() {
        this.signature_ = null;
    }

    public boolean hasSigner() {
        return this.signer_ != null;
    }

    public MobileCoinAPI$Ed25519Public getSigner() {
        MobileCoinAPI$Ed25519Public mobileCoinAPI$Ed25519Public = this.signer_;
        return mobileCoinAPI$Ed25519Public == null ? MobileCoinAPI$Ed25519Public.getDefaultInstance() : mobileCoinAPI$Ed25519Public;
    }

    public void setSigner(MobileCoinAPI$Ed25519Public mobileCoinAPI$Ed25519Public) {
        mobileCoinAPI$Ed25519Public.getClass();
        this.signer_ = mobileCoinAPI$Ed25519Public;
    }

    public void setSigner(MobileCoinAPI$Ed25519Public.Builder builder) {
        this.signer_ = builder.build();
    }

    public void mergeSigner(MobileCoinAPI$Ed25519Public mobileCoinAPI$Ed25519Public) {
        mobileCoinAPI$Ed25519Public.getClass();
        MobileCoinAPI$Ed25519Public mobileCoinAPI$Ed25519Public2 = this.signer_;
        if (mobileCoinAPI$Ed25519Public2 == null || mobileCoinAPI$Ed25519Public2 == MobileCoinAPI$Ed25519Public.getDefaultInstance()) {
            this.signer_ = mobileCoinAPI$Ed25519Public;
        } else {
            this.signer_ = MobileCoinAPI$Ed25519Public.newBuilder(this.signer_).mergeFrom((MobileCoinAPI$Ed25519Public.Builder) mobileCoinAPI$Ed25519Public).buildPartial();
        }
    }

    public void clearSigner() {
        this.signer_ = null;
    }

    public long getSignedAt() {
        return this.signedAt_;
    }

    public void setSignedAt(long j) {
        this.signedAt_ = j;
    }

    public void clearSignedAt() {
        this.signedAt_ = 0;
    }

    public static Blockchain$BlockSignature parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Blockchain$BlockSignature) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Blockchain$BlockSignature parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Blockchain$BlockSignature) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Blockchain$BlockSignature parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Blockchain$BlockSignature) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Blockchain$BlockSignature parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Blockchain$BlockSignature) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Blockchain$BlockSignature parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Blockchain$BlockSignature) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Blockchain$BlockSignature parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Blockchain$BlockSignature) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Blockchain$BlockSignature parseFrom(InputStream inputStream) throws IOException {
        return (Blockchain$BlockSignature) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Blockchain$BlockSignature parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Blockchain$BlockSignature) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Blockchain$BlockSignature parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Blockchain$BlockSignature) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Blockchain$BlockSignature parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Blockchain$BlockSignature) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Blockchain$BlockSignature parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Blockchain$BlockSignature) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Blockchain$BlockSignature parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Blockchain$BlockSignature) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Blockchain$BlockSignature blockchain$BlockSignature) {
        return DEFAULT_INSTANCE.createBuilder(blockchain$BlockSignature);
    }

    /* loaded from: classes.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Blockchain$BlockSignature, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(Blockchain$1 blockchain$1) {
            this();
        }

        private Builder() {
            super(Blockchain$BlockSignature.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Blockchain$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Blockchain$BlockSignature();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\t\u0002\t\u0003\u0003", new Object[]{"signature_", "signer_", "signedAt_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Blockchain$BlockSignature> parser = PARSER;
                if (parser == null) {
                    synchronized (Blockchain$BlockSignature.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Blockchain$BlockSignature blockchain$BlockSignature = new Blockchain$BlockSignature();
        DEFAULT_INSTANCE = blockchain$BlockSignature;
        GeneratedMessageLite.registerDefaultInstance(Blockchain$BlockSignature.class, blockchain$BlockSignature);
    }

    public static Blockchain$BlockSignature getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Blockchain$BlockSignature> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
