package blockchain;

import blockchain.Blockchain$Block;
import blockchain.Blockchain$BlockContents;
import blockchain.Blockchain$BlockSignature;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes.dex */
public final class Blockchain$ArchiveBlockV1 extends GeneratedMessageLite<Blockchain$ArchiveBlockV1, Builder> implements MessageLiteOrBuilder {
    public static final int BLOCK_CONTENTS_FIELD_NUMBER;
    public static final int BLOCK_FIELD_NUMBER;
    private static final Blockchain$ArchiveBlockV1 DEFAULT_INSTANCE;
    private static volatile Parser<Blockchain$ArchiveBlockV1> PARSER;
    public static final int SIGNATURE_FIELD_NUMBER;
    private Blockchain$BlockContents blockContents_;
    private Blockchain$Block block_;
    private Blockchain$BlockSignature signature_;

    private Blockchain$ArchiveBlockV1() {
    }

    public boolean hasBlock() {
        return this.block_ != null;
    }

    public Blockchain$Block getBlock() {
        Blockchain$Block blockchain$Block = this.block_;
        return blockchain$Block == null ? Blockchain$Block.getDefaultInstance() : blockchain$Block;
    }

    public void setBlock(Blockchain$Block blockchain$Block) {
        blockchain$Block.getClass();
        this.block_ = blockchain$Block;
    }

    public void setBlock(Blockchain$Block.Builder builder) {
        this.block_ = builder.build();
    }

    public void mergeBlock(Blockchain$Block blockchain$Block) {
        blockchain$Block.getClass();
        Blockchain$Block blockchain$Block2 = this.block_;
        if (blockchain$Block2 == null || blockchain$Block2 == Blockchain$Block.getDefaultInstance()) {
            this.block_ = blockchain$Block;
        } else {
            this.block_ = Blockchain$Block.newBuilder(this.block_).mergeFrom((Blockchain$Block.Builder) blockchain$Block).buildPartial();
        }
    }

    public void clearBlock() {
        this.block_ = null;
    }

    public boolean hasBlockContents() {
        return this.blockContents_ != null;
    }

    public Blockchain$BlockContents getBlockContents() {
        Blockchain$BlockContents blockchain$BlockContents = this.blockContents_;
        return blockchain$BlockContents == null ? Blockchain$BlockContents.getDefaultInstance() : blockchain$BlockContents;
    }

    public void setBlockContents(Blockchain$BlockContents blockchain$BlockContents) {
        blockchain$BlockContents.getClass();
        this.blockContents_ = blockchain$BlockContents;
    }

    public void setBlockContents(Blockchain$BlockContents.Builder builder) {
        this.blockContents_ = builder.build();
    }

    public void mergeBlockContents(Blockchain$BlockContents blockchain$BlockContents) {
        blockchain$BlockContents.getClass();
        Blockchain$BlockContents blockchain$BlockContents2 = this.blockContents_;
        if (blockchain$BlockContents2 == null || blockchain$BlockContents2 == Blockchain$BlockContents.getDefaultInstance()) {
            this.blockContents_ = blockchain$BlockContents;
        } else {
            this.blockContents_ = Blockchain$BlockContents.newBuilder(this.blockContents_).mergeFrom((Blockchain$BlockContents.Builder) blockchain$BlockContents).buildPartial();
        }
    }

    public void clearBlockContents() {
        this.blockContents_ = null;
    }

    public boolean hasSignature() {
        return this.signature_ != null;
    }

    public Blockchain$BlockSignature getSignature() {
        Blockchain$BlockSignature blockchain$BlockSignature = this.signature_;
        return blockchain$BlockSignature == null ? Blockchain$BlockSignature.getDefaultInstance() : blockchain$BlockSignature;
    }

    public void setSignature(Blockchain$BlockSignature blockchain$BlockSignature) {
        blockchain$BlockSignature.getClass();
        this.signature_ = blockchain$BlockSignature;
    }

    public void setSignature(Blockchain$BlockSignature.Builder builder) {
        this.signature_ = builder.build();
    }

    public void mergeSignature(Blockchain$BlockSignature blockchain$BlockSignature) {
        blockchain$BlockSignature.getClass();
        Blockchain$BlockSignature blockchain$BlockSignature2 = this.signature_;
        if (blockchain$BlockSignature2 == null || blockchain$BlockSignature2 == Blockchain$BlockSignature.getDefaultInstance()) {
            this.signature_ = blockchain$BlockSignature;
        } else {
            this.signature_ = Blockchain$BlockSignature.newBuilder(this.signature_).mergeFrom((Blockchain$BlockSignature.Builder) blockchain$BlockSignature).buildPartial();
        }
    }

    public void clearSignature() {
        this.signature_ = null;
    }

    public static Blockchain$ArchiveBlockV1 parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Blockchain$ArchiveBlockV1) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Blockchain$ArchiveBlockV1 parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Blockchain$ArchiveBlockV1) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Blockchain$ArchiveBlockV1 parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Blockchain$ArchiveBlockV1) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Blockchain$ArchiveBlockV1 parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Blockchain$ArchiveBlockV1) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Blockchain$ArchiveBlockV1 parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Blockchain$ArchiveBlockV1) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Blockchain$ArchiveBlockV1 parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Blockchain$ArchiveBlockV1) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Blockchain$ArchiveBlockV1 parseFrom(InputStream inputStream) throws IOException {
        return (Blockchain$ArchiveBlockV1) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Blockchain$ArchiveBlockV1 parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Blockchain$ArchiveBlockV1) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Blockchain$ArchiveBlockV1 parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Blockchain$ArchiveBlockV1) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Blockchain$ArchiveBlockV1 parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Blockchain$ArchiveBlockV1) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Blockchain$ArchiveBlockV1 parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Blockchain$ArchiveBlockV1) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Blockchain$ArchiveBlockV1 parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Blockchain$ArchiveBlockV1) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Blockchain$ArchiveBlockV1 blockchain$ArchiveBlockV1) {
        return DEFAULT_INSTANCE.createBuilder(blockchain$ArchiveBlockV1);
    }

    /* loaded from: classes.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Blockchain$ArchiveBlockV1, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(Blockchain$1 blockchain$1) {
            this();
        }

        private Builder() {
            super(Blockchain$ArchiveBlockV1.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Blockchain$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Blockchain$ArchiveBlockV1();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\t\u0002\t\u0003\t", new Object[]{"block_", "blockContents_", "signature_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Blockchain$ArchiveBlockV1> parser = PARSER;
                if (parser == null) {
                    synchronized (Blockchain$ArchiveBlockV1.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Blockchain$ArchiveBlockV1 blockchain$ArchiveBlockV1 = new Blockchain$ArchiveBlockV1();
        DEFAULT_INSTANCE = blockchain$ArchiveBlockV1;
        GeneratedMessageLite.registerDefaultInstance(Blockchain$ArchiveBlockV1.class, blockchain$ArchiveBlockV1);
    }

    public static Blockchain$ArchiveBlockV1 getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Blockchain$ArchiveBlockV1> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
