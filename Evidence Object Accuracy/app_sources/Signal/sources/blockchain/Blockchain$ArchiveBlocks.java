package blockchain;

import blockchain.Blockchain$ArchiveBlock;
import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes.dex */
public final class Blockchain$ArchiveBlocks extends GeneratedMessageLite<Blockchain$ArchiveBlocks, Builder> implements MessageLiteOrBuilder {
    public static final int BLOCKS_FIELD_NUMBER;
    private static final Blockchain$ArchiveBlocks DEFAULT_INSTANCE;
    private static volatile Parser<Blockchain$ArchiveBlocks> PARSER;
    private Internal.ProtobufList<Blockchain$ArchiveBlock> blocks_ = GeneratedMessageLite.emptyProtobufList();

    private Blockchain$ArchiveBlocks() {
    }

    public List<Blockchain$ArchiveBlock> getBlocksList() {
        return this.blocks_;
    }

    public List<? extends Blockchain$ArchiveBlockOrBuilder> getBlocksOrBuilderList() {
        return this.blocks_;
    }

    public int getBlocksCount() {
        return this.blocks_.size();
    }

    public Blockchain$ArchiveBlock getBlocks(int i) {
        return this.blocks_.get(i);
    }

    public Blockchain$ArchiveBlockOrBuilder getBlocksOrBuilder(int i) {
        return this.blocks_.get(i);
    }

    private void ensureBlocksIsMutable() {
        if (!this.blocks_.isModifiable()) {
            this.blocks_ = GeneratedMessageLite.mutableCopy(this.blocks_);
        }
    }

    public void setBlocks(int i, Blockchain$ArchiveBlock blockchain$ArchiveBlock) {
        blockchain$ArchiveBlock.getClass();
        ensureBlocksIsMutable();
        this.blocks_.set(i, blockchain$ArchiveBlock);
    }

    public void setBlocks(int i, Blockchain$ArchiveBlock.Builder builder) {
        ensureBlocksIsMutable();
        this.blocks_.set(i, builder.build());
    }

    public void addBlocks(Blockchain$ArchiveBlock blockchain$ArchiveBlock) {
        blockchain$ArchiveBlock.getClass();
        ensureBlocksIsMutable();
        this.blocks_.add(blockchain$ArchiveBlock);
    }

    public void addBlocks(int i, Blockchain$ArchiveBlock blockchain$ArchiveBlock) {
        blockchain$ArchiveBlock.getClass();
        ensureBlocksIsMutable();
        this.blocks_.add(i, blockchain$ArchiveBlock);
    }

    public void addBlocks(Blockchain$ArchiveBlock.Builder builder) {
        ensureBlocksIsMutable();
        this.blocks_.add(builder.build());
    }

    public void addBlocks(int i, Blockchain$ArchiveBlock.Builder builder) {
        ensureBlocksIsMutable();
        this.blocks_.add(i, builder.build());
    }

    public void addAllBlocks(Iterable<? extends Blockchain$ArchiveBlock> iterable) {
        ensureBlocksIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.blocks_);
    }

    public void clearBlocks() {
        this.blocks_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeBlocks(int i) {
        ensureBlocksIsMutable();
        this.blocks_.remove(i);
    }

    public static Blockchain$ArchiveBlocks parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Blockchain$ArchiveBlocks) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Blockchain$ArchiveBlocks parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Blockchain$ArchiveBlocks) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Blockchain$ArchiveBlocks parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Blockchain$ArchiveBlocks) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Blockchain$ArchiveBlocks parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Blockchain$ArchiveBlocks) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Blockchain$ArchiveBlocks parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Blockchain$ArchiveBlocks) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Blockchain$ArchiveBlocks parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Blockchain$ArchiveBlocks) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Blockchain$ArchiveBlocks parseFrom(InputStream inputStream) throws IOException {
        return (Blockchain$ArchiveBlocks) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Blockchain$ArchiveBlocks parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Blockchain$ArchiveBlocks) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Blockchain$ArchiveBlocks parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Blockchain$ArchiveBlocks) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Blockchain$ArchiveBlocks parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Blockchain$ArchiveBlocks) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Blockchain$ArchiveBlocks parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Blockchain$ArchiveBlocks) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Blockchain$ArchiveBlocks parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Blockchain$ArchiveBlocks) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Blockchain$ArchiveBlocks blockchain$ArchiveBlocks) {
        return DEFAULT_INSTANCE.createBuilder(blockchain$ArchiveBlocks);
    }

    /* loaded from: classes.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Blockchain$ArchiveBlocks, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(Blockchain$1 blockchain$1) {
            this();
        }

        private Builder() {
            super(Blockchain$ArchiveBlocks.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Blockchain$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Blockchain$ArchiveBlocks();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"blocks_", Blockchain$ArchiveBlock.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Blockchain$ArchiveBlocks> parser = PARSER;
                if (parser == null) {
                    synchronized (Blockchain$ArchiveBlocks.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Blockchain$ArchiveBlocks blockchain$ArchiveBlocks = new Blockchain$ArchiveBlocks();
        DEFAULT_INSTANCE = blockchain$ArchiveBlocks;
        GeneratedMessageLite.registerDefaultInstance(Blockchain$ArchiveBlocks.class, blockchain$ArchiveBlocks);
    }

    public static Blockchain$ArchiveBlocks getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Blockchain$ArchiveBlocks> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
