package mobi.upod.timedurationpicker;

import org.thoughtcrime.securesms.util.MessageRecordUtil;

/* loaded from: classes3.dex */
public class TimeDurationUtil {
    public static long durationOf(int i, int i2, int i3) {
        return (long) ((i * 3600000) + (i2 * 60000) + (i3 * MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH));
    }

    public static int hoursOf(long j) {
        return ((int) j) / 3600000;
    }

    public static int minutesOf(long j) {
        return ((int) j) / 60000;
    }

    public static int minutesInHourOf(long j) {
        return ((int) (j - ((long) (hoursOf(j) * 3600000)))) / 60000;
    }

    public static int secondsInMinuteOf(long j) {
        return ((int) ((j - ((long) (hoursOf(j) * 3600000))) - ((long) (minutesInHourOf(j) * 60000)))) / MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH;
    }
}
