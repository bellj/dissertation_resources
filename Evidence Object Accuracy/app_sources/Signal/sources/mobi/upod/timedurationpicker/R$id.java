package mobi.upod.timedurationpicker;

/* loaded from: classes3.dex */
public final class R$id {
    public static final int backspace;
    public static final int clear;
    public static final int displayRow;
    public static final int duration;
    public static final int hours;
    public static final int hoursLabel;
    public static final int minutes;
    public static final int minutesLabel;
    public static final int numPad;
    public static final int numPad0;
    public static final int numPad00;
    public static final int numPad1;
    public static final int numPad2;
    public static final int numPad3;
    public static final int numPad4;
    public static final int numPad5;
    public static final int numPad6;
    public static final int numPad7;
    public static final int numPad8;
    public static final int numPad9;
    public static final int numPadMeasure;
    public static final int seconds;
    public static final int secondsLabel;
    public static final int separator;
}
