package mobi.upod.timedurationpicker;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.app.AlertDialog;

/* loaded from: classes3.dex */
public class TimeDurationPickerDialog extends AlertDialog implements DialogInterface.OnClickListener {
    private final TimeDurationPicker durationInputView;
    private final OnDurationSetListener durationSetListener;

    /* loaded from: classes3.dex */
    public interface OnDurationSetListener {
        void onDurationSet(TimeDurationPicker timeDurationPicker, long j);
    }

    public TimeDurationPickerDialog(Context context, OnDurationSetListener onDurationSetListener, long j) {
        super(context);
        this.durationSetListener = onDurationSetListener;
        View inflate = LayoutInflater.from(context).inflate(R$layout.time_duration_picker_dialog, (ViewGroup) null);
        setView(inflate);
        setButton(-1, context.getString(17039370), this);
        setButton(-2, context.getString(17039360), this);
        TimeDurationPicker timeDurationPicker = (TimeDurationPicker) inflate;
        this.durationInputView = timeDurationPicker;
        timeDurationPicker.setDuration(j);
    }

    public TimeDurationPickerDialog(Context context, OnDurationSetListener onDurationSetListener, long j, int i) {
        this(context, onDurationSetListener, j);
        this.durationInputView.setTimeUnits(i);
    }

    @Override // android.content.DialogInterface.OnClickListener
    public void onClick(DialogInterface dialogInterface, int i) {
        OnDurationSetListener onDurationSetListener;
        if (i == -2) {
            cancel();
        } else if (i == -1 && (onDurationSetListener = this.durationSetListener) != null) {
            TimeDurationPicker timeDurationPicker = this.durationInputView;
            onDurationSetListener.onDurationSet(timeDurationPicker, timeDurationPicker.getDuration());
        }
    }

    @Override // android.app.Dialog
    public Bundle onSaveInstanceState() {
        Bundle onSaveInstanceState = super.onSaveInstanceState();
        onSaveInstanceState.putLong("duration", this.durationInputView.getDuration());
        return onSaveInstanceState;
    }

    @Override // android.app.Dialog
    public void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.durationInputView.setDuration(bundle.getLong("duration"));
    }
}
