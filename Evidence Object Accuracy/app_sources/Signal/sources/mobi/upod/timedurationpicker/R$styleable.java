package mobi.upod.timedurationpicker;

import org.thoughtcrime.securesms.R;

/* loaded from: classes3.dex */
public final class R$styleable {
    public static final int[] ActionBar = {R.attr.background, R.attr.backgroundSplit, R.attr.backgroundStacked, R.attr.contentInsetEnd, R.attr.contentInsetEndWithActions, R.attr.contentInsetLeft, R.attr.contentInsetRight, R.attr.contentInsetStart, R.attr.contentInsetStartWithNavigation, R.attr.customNavigationLayout, R.attr.displayOptions, R.attr.divider, R.attr.elevation, R.attr.height, R.attr.hideOnContentScroll, R.attr.homeAsUpIndicator, R.attr.homeLayout, R.attr.icon, R.attr.indeterminateProgressStyle, R.attr.itemPadding, R.attr.logo, R.attr.navigationMode, R.attr.popupTheme, R.attr.progressBarPadding, R.attr.progressBarStyle, R.attr.subtitle, R.attr.subtitleTextStyle, R.attr.title, R.attr.titleTextStyle};
    public static final int[] ActionBarLayout = {16842931};
    public static final int[] ActionMenuItemView = {16843071};
    public static final int[] ActionMenuView = new int[0];
    public static final int[] ActionMode = {R.attr.background, R.attr.backgroundSplit, R.attr.closeItemLayout, R.attr.height, R.attr.subtitleTextStyle, R.attr.titleTextStyle};
    public static final int[] ActivityChooserView = {R.attr.expandActivityOverflowButtonDrawable, R.attr.initialActivityCount};
    public static final int[] AlertDialog = {16842994, R.attr.buttonIconDimen, R.attr.buttonPanelSideLayout, R.attr.listItemLayout, R.attr.listLayout, R.attr.multiChoiceItemLayout, R.attr.showTitle, R.attr.singleChoiceItemLayout};
    public static final int[] AppCompatTextView = {16842804, R.attr.autoSizeMaxTextSize, R.attr.autoSizeMinTextSize, R.attr.autoSizePresetSizes, R.attr.autoSizeStepGranularity, R.attr.autoSizeTextType, R.attr.drawableBottomCompat, R.attr.drawableEndCompat, R.attr.drawableLeftCompat, R.attr.drawableRightCompat, R.attr.drawableStartCompat, R.attr.drawableTint, R.attr.drawableTintMode, R.attr.drawableTopCompat, R.attr.firstBaselineToTopHeight, R.attr.fontFamily, R.attr.fontVariationSettings, R.attr.lastBaselineToBottomHeight, R.attr.lineHeight, R.attr.textAllCaps, R.attr.textLocale};
    public static final int[] ButtonBarLayout = {R.attr.allowStacking};
    public static final int[] CompoundButton = {16843015, R.attr.buttonCompat, R.attr.buttonTint, R.attr.buttonTintMode};
    public static final int[] DrawerArrowToggle = {R.attr.arrowHeadLength, R.attr.arrowShaftLength, R.attr.barLength, R.attr.color, R.attr.drawableSize, R.attr.gapBetweenBars, R.attr.spinBars, R.attr.thickness};
    public static final int[] LinearLayoutCompat = {16842927, 16842948, 16843046, 16843047, 16843048, R.attr.divider, R.attr.dividerPadding, R.attr.measureWithLargestChild, R.attr.showDividers};
    public static final int[] LinearLayoutCompat_Layout = {16842931, 16842996, 16842997, 16843137};
    public static final int[] ListPopupWindow = {16843436, 16843437};
    public static final int[] MenuGroup = {16842766, 16842960, 16843156, 16843230, 16843231, 16843232};
    public static final int[] MenuItem = {16842754, 16842766, 16842960, 16843014, 16843156, 16843230, 16843231, 16843233, 16843234, 16843235, 16843236, 16843237, 16843375, R.attr.actionLayout, R.attr.actionProviderClass, R.attr.actionViewClass, R.attr.alphabeticModifiers, R.attr.contentDescription, R.attr.iconTint, R.attr.iconTintMode, R.attr.numericModifiers, R.attr.showAsAction, R.attr.tooltipText};
    public static final int[] MenuView = {16842926, 16843052, 16843053, 16843054, 16843055, 16843056, 16843057, R.attr.preserveIconSpacing, R.attr.subMenuArrow};
    public static final int[] PopupWindow = {16843126, 16843465, R.attr.overlapAnchor};
    public static final int[] PopupWindowBackgroundState = {R.attr.state_above_anchor};
    public static final int[] SearchView = {16842970, 16843039, 16843296, 16843364, R.attr.closeIcon, R.attr.commitIcon, R.attr.defaultQueryHint, R.attr.goIcon, R.attr.iconifiedByDefault, R.attr.layout, R.attr.queryBackground, R.attr.queryHint, R.attr.searchHintIcon, R.attr.searchIcon, R.attr.submitBackground, R.attr.suggestionRowLayout, R.attr.voiceIcon};
    public static final int[] Spinner = {16842930, 16843126, 16843131, 16843362, R.attr.popupTheme};
    public static final int[] SwitchCompat = {16843044, 16843045, 16843074, R.attr.showText, R.attr.splitTrack, R.attr.switchMinWidth, R.attr.switchPadding, R.attr.switchTextAppearance, R.attr.thumbTextPadding, R.attr.thumbTint, R.attr.thumbTintMode, R.attr.track, R.attr.trackTint, R.attr.trackTintMode};
    public static final int[] TextAppearance = {16842901, 16842902, 16842903, 16842904, 16842906, 16842907, 16843105, 16843106, 16843107, 16843108, 16843692, 16844165, R.attr.fontFamily, R.attr.fontVariationSettings, R.attr.textAllCaps, R.attr.textLocale};
    public static final int[] TimeDurationPicker = {R.attr.backspaceIcon, R.attr.clearIcon, R.attr.durationDisplayBackground, R.attr.numPadButtonPadding, R.attr.separatorColor, R.attr.textAppearanceButton, R.attr.textAppearanceDisplay, R.attr.textAppearanceUnit, R.attr.timeUnits};
    public static final int[] TimeDurationPickerStyle = {R.attr.timeDurationPickerStyle};
    public static final int TimeDurationPicker_backspaceIcon;
    public static final int TimeDurationPicker_clearIcon;
    public static final int TimeDurationPicker_durationDisplayBackground;
    public static final int TimeDurationPicker_numPadButtonPadding;
    public static final int TimeDurationPicker_separatorColor;
    public static final int TimeDurationPicker_textAppearanceButton;
    public static final int TimeDurationPicker_textAppearanceDisplay;
    public static final int TimeDurationPicker_textAppearanceUnit;
    public static final int TimeDurationPicker_timeUnits;
    public static final int[] Toolbar = {16842927, 16843072, R.attr.buttonGravity, R.attr.collapseContentDescription, R.attr.collapseIcon, R.attr.contentInsetEnd, R.attr.contentInsetEndWithActions, R.attr.contentInsetLeft, R.attr.contentInsetRight, R.attr.contentInsetStart, R.attr.contentInsetStartWithNavigation, R.attr.logo, R.attr.logoDescription, R.attr.maxButtonHeight, R.attr.menu, R.attr.navigationContentDescription, R.attr.navigationIcon, R.attr.popupTheme, R.attr.subtitle, R.attr.subtitleTextAppearance, R.attr.subtitleTextColor, R.attr.title, R.attr.titleMargin, R.attr.titleMarginBottom, R.attr.titleMarginEnd, R.attr.titleMarginStart, R.attr.titleMarginTop, R.attr.titleMargins, R.attr.titleTextAppearance, R.attr.titleTextColor};
    public static final int[] View = {16842752, 16842970, R.attr.paddingEnd, R.attr.paddingStart, R.attr.theme};
    public static final int[] ViewBackgroundHelper = {16842964, R.attr.backgroundTint, R.attr.backgroundTintMode};
    public static final int[] ViewStubCompat = {16842960, 16842994, 16842995};
}
