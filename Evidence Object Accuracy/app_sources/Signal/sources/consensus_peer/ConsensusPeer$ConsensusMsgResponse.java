package consensus_peer;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class ConsensusPeer$ConsensusMsgResponse extends GeneratedMessageLite<ConsensusPeer$ConsensusMsgResponse, Builder> implements MessageLiteOrBuilder {
    private static final ConsensusPeer$ConsensusMsgResponse DEFAULT_INSTANCE;
    private static volatile Parser<ConsensusPeer$ConsensusMsgResponse> PARSER;
    public static final int RESULT_FIELD_NUMBER;
    private int result_;

    private ConsensusPeer$ConsensusMsgResponse() {
    }

    public int getResultValue() {
        return this.result_;
    }

    public ConsensusPeer$ConsensusMsgResult getResult() {
        ConsensusPeer$ConsensusMsgResult forNumber = ConsensusPeer$ConsensusMsgResult.forNumber(this.result_);
        return forNumber == null ? ConsensusPeer$ConsensusMsgResult.UNRECOGNIZED : forNumber;
    }

    public void setResultValue(int i) {
        this.result_ = i;
    }

    public void setResult(ConsensusPeer$ConsensusMsgResult consensusPeer$ConsensusMsgResult) {
        consensusPeer$ConsensusMsgResult.getClass();
        this.result_ = consensusPeer$ConsensusMsgResult.getNumber();
    }

    public void clearResult() {
        this.result_ = 0;
    }

    public static ConsensusPeer$ConsensusMsgResponse parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ConsensusPeer$ConsensusMsgResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ConsensusPeer$ConsensusMsgResponse parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusPeer$ConsensusMsgResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ConsensusPeer$ConsensusMsgResponse parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ConsensusPeer$ConsensusMsgResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ConsensusPeer$ConsensusMsgResponse parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusPeer$ConsensusMsgResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ConsensusPeer$ConsensusMsgResponse parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ConsensusPeer$ConsensusMsgResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ConsensusPeer$ConsensusMsgResponse parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusPeer$ConsensusMsgResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ConsensusPeer$ConsensusMsgResponse parseFrom(InputStream inputStream) throws IOException {
        return (ConsensusPeer$ConsensusMsgResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ConsensusPeer$ConsensusMsgResponse parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusPeer$ConsensusMsgResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ConsensusPeer$ConsensusMsgResponse parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ConsensusPeer$ConsensusMsgResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ConsensusPeer$ConsensusMsgResponse parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusPeer$ConsensusMsgResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ConsensusPeer$ConsensusMsgResponse parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ConsensusPeer$ConsensusMsgResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ConsensusPeer$ConsensusMsgResponse parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusPeer$ConsensusMsgResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ConsensusPeer$ConsensusMsgResponse consensusPeer$ConsensusMsgResponse) {
        return DEFAULT_INSTANCE.createBuilder(consensusPeer$ConsensusMsgResponse);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ConsensusPeer$ConsensusMsgResponse, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(ConsensusPeer$1 consensusPeer$1) {
            this();
        }

        private Builder() {
            super(ConsensusPeer$ConsensusMsgResponse.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (ConsensusPeer$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ConsensusPeer$ConsensusMsgResponse();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\f", new Object[]{"result_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ConsensusPeer$ConsensusMsgResponse> parser = PARSER;
                if (parser == null) {
                    synchronized (ConsensusPeer$ConsensusMsgResponse.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ConsensusPeer$ConsensusMsgResponse consensusPeer$ConsensusMsgResponse = new ConsensusPeer$ConsensusMsgResponse();
        DEFAULT_INSTANCE = consensusPeer$ConsensusMsgResponse;
        GeneratedMessageLite.registerDefaultInstance(ConsensusPeer$ConsensusMsgResponse.class, consensusPeer$ConsensusMsgResponse);
    }

    public static ConsensusPeer$ConsensusMsgResponse getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ConsensusPeer$ConsensusMsgResponse> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
