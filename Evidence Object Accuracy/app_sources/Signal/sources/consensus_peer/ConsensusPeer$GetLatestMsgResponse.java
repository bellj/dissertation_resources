package consensus_peer;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class ConsensusPeer$GetLatestMsgResponse extends GeneratedMessageLite<ConsensusPeer$GetLatestMsgResponse, Builder> implements MessageLiteOrBuilder {
    private static final ConsensusPeer$GetLatestMsgResponse DEFAULT_INSTANCE;
    private static volatile Parser<ConsensusPeer$GetLatestMsgResponse> PARSER;
    public static final int PAYLOAD_FIELD_NUMBER;
    private ByteString payload_ = ByteString.EMPTY;

    private ConsensusPeer$GetLatestMsgResponse() {
    }

    public ByteString getPayload() {
        return this.payload_;
    }

    public void setPayload(ByteString byteString) {
        byteString.getClass();
        this.payload_ = byteString;
    }

    public void clearPayload() {
        this.payload_ = getDefaultInstance().getPayload();
    }

    public static ConsensusPeer$GetLatestMsgResponse parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ConsensusPeer$GetLatestMsgResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ConsensusPeer$GetLatestMsgResponse parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusPeer$GetLatestMsgResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ConsensusPeer$GetLatestMsgResponse parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ConsensusPeer$GetLatestMsgResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ConsensusPeer$GetLatestMsgResponse parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusPeer$GetLatestMsgResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ConsensusPeer$GetLatestMsgResponse parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ConsensusPeer$GetLatestMsgResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ConsensusPeer$GetLatestMsgResponse parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusPeer$GetLatestMsgResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ConsensusPeer$GetLatestMsgResponse parseFrom(InputStream inputStream) throws IOException {
        return (ConsensusPeer$GetLatestMsgResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ConsensusPeer$GetLatestMsgResponse parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusPeer$GetLatestMsgResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ConsensusPeer$GetLatestMsgResponse parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ConsensusPeer$GetLatestMsgResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ConsensusPeer$GetLatestMsgResponse parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusPeer$GetLatestMsgResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ConsensusPeer$GetLatestMsgResponse parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ConsensusPeer$GetLatestMsgResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ConsensusPeer$GetLatestMsgResponse parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusPeer$GetLatestMsgResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ConsensusPeer$GetLatestMsgResponse consensusPeer$GetLatestMsgResponse) {
        return DEFAULT_INSTANCE.createBuilder(consensusPeer$GetLatestMsgResponse);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ConsensusPeer$GetLatestMsgResponse, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(ConsensusPeer$1 consensusPeer$1) {
            this();
        }

        private Builder() {
            super(ConsensusPeer$GetLatestMsgResponse.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (ConsensusPeer$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ConsensusPeer$GetLatestMsgResponse();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\n", new Object[]{"payload_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ConsensusPeer$GetLatestMsgResponse> parser = PARSER;
                if (parser == null) {
                    synchronized (ConsensusPeer$GetLatestMsgResponse.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ConsensusPeer$GetLatestMsgResponse consensusPeer$GetLatestMsgResponse = new ConsensusPeer$GetLatestMsgResponse();
        DEFAULT_INSTANCE = consensusPeer$GetLatestMsgResponse;
        GeneratedMessageLite.registerDefaultInstance(ConsensusPeer$GetLatestMsgResponse.class, consensusPeer$GetLatestMsgResponse);
    }

    public static ConsensusPeer$GetLatestMsgResponse getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ConsensusPeer$GetLatestMsgResponse> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
