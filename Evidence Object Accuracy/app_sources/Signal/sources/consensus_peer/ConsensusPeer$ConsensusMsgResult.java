package consensus_peer;

import com.google.protobuf.Internal;

/* loaded from: classes3.dex */
public enum ConsensusPeer$ConsensusMsgResult implements Internal.EnumLite {
    Ok(0),
    UnknownPeer(10),
    UNRECOGNIZED(-1);
    
    private static final Internal.EnumLiteMap<ConsensusPeer$ConsensusMsgResult> internalValueMap = new Internal.EnumLiteMap<ConsensusPeer$ConsensusMsgResult>() { // from class: consensus_peer.ConsensusPeer$ConsensusMsgResult.1
        @Override // com.google.protobuf.Internal.EnumLiteMap
        public ConsensusPeer$ConsensusMsgResult findValueByNumber(int i) {
            return ConsensusPeer$ConsensusMsgResult.forNumber(i);
        }
    };
    private final int value;

    @Override // com.google.protobuf.Internal.EnumLite
    public final int getNumber() {
        if (this != UNRECOGNIZED) {
            return this.value;
        }
        throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
    }

    public static ConsensusPeer$ConsensusMsgResult forNumber(int i) {
        if (i == 0) {
            return Ok;
        }
        if (i != 10) {
            return null;
        }
        return UnknownPeer;
    }

    ConsensusPeer$ConsensusMsgResult(int i) {
        this.value = i;
    }
}
