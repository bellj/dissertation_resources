package consensus_peer;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class ConsensusPeer$ConsensusMsg extends GeneratedMessageLite<ConsensusPeer$ConsensusMsg, Builder> implements MessageLiteOrBuilder {
    private static final ConsensusPeer$ConsensusMsg DEFAULT_INSTANCE;
    public static final int FROM_RESPONDER_ID_FIELD_NUMBER;
    private static volatile Parser<ConsensusPeer$ConsensusMsg> PARSER;
    public static final int PAYLOAD_FIELD_NUMBER;
    private String fromResponderId_ = "";
    private ByteString payload_ = ByteString.EMPTY;

    private ConsensusPeer$ConsensusMsg() {
    }

    public String getFromResponderId() {
        return this.fromResponderId_;
    }

    public ByteString getFromResponderIdBytes() {
        return ByteString.copyFromUtf8(this.fromResponderId_);
    }

    public void setFromResponderId(String str) {
        str.getClass();
        this.fromResponderId_ = str;
    }

    public void clearFromResponderId() {
        this.fromResponderId_ = getDefaultInstance().getFromResponderId();
    }

    public void setFromResponderIdBytes(ByteString byteString) {
        byteString.getClass();
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.fromResponderId_ = byteString.toStringUtf8();
    }

    public ByteString getPayload() {
        return this.payload_;
    }

    public void setPayload(ByteString byteString) {
        byteString.getClass();
        this.payload_ = byteString;
    }

    public void clearPayload() {
        this.payload_ = getDefaultInstance().getPayload();
    }

    public static ConsensusPeer$ConsensusMsg parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ConsensusPeer$ConsensusMsg) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ConsensusPeer$ConsensusMsg parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusPeer$ConsensusMsg) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ConsensusPeer$ConsensusMsg parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ConsensusPeer$ConsensusMsg) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ConsensusPeer$ConsensusMsg parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusPeer$ConsensusMsg) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ConsensusPeer$ConsensusMsg parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ConsensusPeer$ConsensusMsg) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ConsensusPeer$ConsensusMsg parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusPeer$ConsensusMsg) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ConsensusPeer$ConsensusMsg parseFrom(InputStream inputStream) throws IOException {
        return (ConsensusPeer$ConsensusMsg) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ConsensusPeer$ConsensusMsg parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusPeer$ConsensusMsg) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ConsensusPeer$ConsensusMsg parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ConsensusPeer$ConsensusMsg) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ConsensusPeer$ConsensusMsg parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusPeer$ConsensusMsg) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ConsensusPeer$ConsensusMsg parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ConsensusPeer$ConsensusMsg) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ConsensusPeer$ConsensusMsg parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusPeer$ConsensusMsg) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ConsensusPeer$ConsensusMsg consensusPeer$ConsensusMsg) {
        return DEFAULT_INSTANCE.createBuilder(consensusPeer$ConsensusMsg);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ConsensusPeer$ConsensusMsg, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(ConsensusPeer$1 consensusPeer$1) {
            this();
        }

        private Builder() {
            super(ConsensusPeer$ConsensusMsg.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (ConsensusPeer$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ConsensusPeer$ConsensusMsg();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001Ȉ\u0002\n", new Object[]{"fromResponderId_", "payload_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ConsensusPeer$ConsensusMsg> parser = PARSER;
                if (parser == null) {
                    synchronized (ConsensusPeer$ConsensusMsg.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ConsensusPeer$ConsensusMsg consensusPeer$ConsensusMsg = new ConsensusPeer$ConsensusMsg();
        DEFAULT_INSTANCE = consensusPeer$ConsensusMsg;
        GeneratedMessageLite.registerDefaultInstance(ConsensusPeer$ConsensusMsg.class, consensusPeer$ConsensusMsg);
    }

    public static ConsensusPeer$ConsensusMsg getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ConsensusPeer$ConsensusMsg> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
