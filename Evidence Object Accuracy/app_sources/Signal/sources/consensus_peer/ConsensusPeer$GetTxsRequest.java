package consensus_peer;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class ConsensusPeer$GetTxsRequest extends GeneratedMessageLite<ConsensusPeer$GetTxsRequest, Builder> implements MessageLiteOrBuilder {
    public static final int CHANNEL_ID_FIELD_NUMBER;
    private static final ConsensusPeer$GetTxsRequest DEFAULT_INSTANCE;
    private static volatile Parser<ConsensusPeer$GetTxsRequest> PARSER;
    public static final int TX_HASHES_FIELD_NUMBER;
    private ByteString channelId_ = ByteString.EMPTY;
    private Internal.ProtobufList<ByteString> txHashes_ = GeneratedMessageLite.emptyProtobufList();

    private ConsensusPeer$GetTxsRequest() {
    }

    public ByteString getChannelId() {
        return this.channelId_;
    }

    public void setChannelId(ByteString byteString) {
        byteString.getClass();
        this.channelId_ = byteString;
    }

    public void clearChannelId() {
        this.channelId_ = getDefaultInstance().getChannelId();
    }

    public List<ByteString> getTxHashesList() {
        return this.txHashes_;
    }

    public int getTxHashesCount() {
        return this.txHashes_.size();
    }

    public ByteString getTxHashes(int i) {
        return this.txHashes_.get(i);
    }

    private void ensureTxHashesIsMutable() {
        if (!this.txHashes_.isModifiable()) {
            this.txHashes_ = GeneratedMessageLite.mutableCopy(this.txHashes_);
        }
    }

    public void setTxHashes(int i, ByteString byteString) {
        byteString.getClass();
        ensureTxHashesIsMutable();
        this.txHashes_.set(i, byteString);
    }

    public void addTxHashes(ByteString byteString) {
        byteString.getClass();
        ensureTxHashesIsMutable();
        this.txHashes_.add(byteString);
    }

    public void addAllTxHashes(Iterable<? extends ByteString> iterable) {
        ensureTxHashesIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.txHashes_);
    }

    public void clearTxHashes() {
        this.txHashes_ = GeneratedMessageLite.emptyProtobufList();
    }

    public static ConsensusPeer$GetTxsRequest parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ConsensusPeer$GetTxsRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ConsensusPeer$GetTxsRequest parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusPeer$GetTxsRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ConsensusPeer$GetTxsRequest parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ConsensusPeer$GetTxsRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ConsensusPeer$GetTxsRequest parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusPeer$GetTxsRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ConsensusPeer$GetTxsRequest parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ConsensusPeer$GetTxsRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ConsensusPeer$GetTxsRequest parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusPeer$GetTxsRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ConsensusPeer$GetTxsRequest parseFrom(InputStream inputStream) throws IOException {
        return (ConsensusPeer$GetTxsRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ConsensusPeer$GetTxsRequest parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusPeer$GetTxsRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ConsensusPeer$GetTxsRequest parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ConsensusPeer$GetTxsRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ConsensusPeer$GetTxsRequest parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusPeer$GetTxsRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ConsensusPeer$GetTxsRequest parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ConsensusPeer$GetTxsRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ConsensusPeer$GetTxsRequest parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusPeer$GetTxsRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ConsensusPeer$GetTxsRequest consensusPeer$GetTxsRequest) {
        return DEFAULT_INSTANCE.createBuilder(consensusPeer$GetTxsRequest);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ConsensusPeer$GetTxsRequest, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(ConsensusPeer$1 consensusPeer$1) {
            this();
        }

        private Builder() {
            super(ConsensusPeer$GetTxsRequest.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (ConsensusPeer$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ConsensusPeer$GetTxsRequest();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0001\u0000\u0001\n\u0002\u001c", new Object[]{"channelId_", "txHashes_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ConsensusPeer$GetTxsRequest> parser = PARSER;
                if (parser == null) {
                    synchronized (ConsensusPeer$GetTxsRequest.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ConsensusPeer$GetTxsRequest consensusPeer$GetTxsRequest = new ConsensusPeer$GetTxsRequest();
        DEFAULT_INSTANCE = consensusPeer$GetTxsRequest;
        GeneratedMessageLite.registerDefaultInstance(ConsensusPeer$GetTxsRequest.class, consensusPeer$GetTxsRequest);
    }

    public static ConsensusPeer$GetTxsRequest getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ConsensusPeer$GetTxsRequest> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
