package consensus_peer;

import attest.Attest$Message;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import consensus_peer.ConsensusPeer$TxHashesNotInCache;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class ConsensusPeer$GetTxsResponse extends GeneratedMessageLite<ConsensusPeer$GetTxsResponse, Builder> implements MessageLiteOrBuilder {
    private static final ConsensusPeer$GetTxsResponse DEFAULT_INSTANCE;
    private static volatile Parser<ConsensusPeer$GetTxsResponse> PARSER;
    public static final int SUCCESS_FIELD_NUMBER;
    public static final int TX_HASHES_NOT_IN_CACHE_FIELD_NUMBER;
    private int payloadCase_ = 0;
    private Object payload_;

    private ConsensusPeer$GetTxsResponse() {
    }

    /* loaded from: classes3.dex */
    public enum PayloadCase {
        SUCCESS(1),
        TX_HASHES_NOT_IN_CACHE(2),
        PAYLOAD_NOT_SET(0);
        
        private final int value;

        PayloadCase(int i) {
            this.value = i;
        }

        public static PayloadCase forNumber(int i) {
            if (i == 0) {
                return PAYLOAD_NOT_SET;
            }
            if (i == 1) {
                return SUCCESS;
            }
            if (i != 2) {
                return null;
            }
            return TX_HASHES_NOT_IN_CACHE;
        }
    }

    public PayloadCase getPayloadCase() {
        return PayloadCase.forNumber(this.payloadCase_);
    }

    public void clearPayload() {
        this.payloadCase_ = 0;
        this.payload_ = null;
    }

    public boolean hasSuccess() {
        return this.payloadCase_ == 1;
    }

    public Attest$Message getSuccess() {
        if (this.payloadCase_ == 1) {
            return (Attest$Message) this.payload_;
        }
        return Attest$Message.getDefaultInstance();
    }

    public void setSuccess(Attest$Message attest$Message) {
        attest$Message.getClass();
        this.payload_ = attest$Message;
        this.payloadCase_ = 1;
    }

    public void setSuccess(Attest$Message.Builder builder) {
        this.payload_ = builder.build();
        this.payloadCase_ = 1;
    }

    public void mergeSuccess(Attest$Message attest$Message) {
        attest$Message.getClass();
        if (this.payloadCase_ != 1 || this.payload_ == Attest$Message.getDefaultInstance()) {
            this.payload_ = attest$Message;
        } else {
            this.payload_ = Attest$Message.newBuilder((Attest$Message) this.payload_).mergeFrom((Attest$Message.Builder) attest$Message).buildPartial();
        }
        this.payloadCase_ = 1;
    }

    public void clearSuccess() {
        if (this.payloadCase_ == 1) {
            this.payloadCase_ = 0;
            this.payload_ = null;
        }
    }

    public boolean hasTxHashesNotInCache() {
        return this.payloadCase_ == 2;
    }

    public ConsensusPeer$TxHashesNotInCache getTxHashesNotInCache() {
        if (this.payloadCase_ == 2) {
            return (ConsensusPeer$TxHashesNotInCache) this.payload_;
        }
        return ConsensusPeer$TxHashesNotInCache.getDefaultInstance();
    }

    public void setTxHashesNotInCache(ConsensusPeer$TxHashesNotInCache consensusPeer$TxHashesNotInCache) {
        consensusPeer$TxHashesNotInCache.getClass();
        this.payload_ = consensusPeer$TxHashesNotInCache;
        this.payloadCase_ = 2;
    }

    public void setTxHashesNotInCache(ConsensusPeer$TxHashesNotInCache.Builder builder) {
        this.payload_ = builder.build();
        this.payloadCase_ = 2;
    }

    public void mergeTxHashesNotInCache(ConsensusPeer$TxHashesNotInCache consensusPeer$TxHashesNotInCache) {
        consensusPeer$TxHashesNotInCache.getClass();
        if (this.payloadCase_ != 2 || this.payload_ == ConsensusPeer$TxHashesNotInCache.getDefaultInstance()) {
            this.payload_ = consensusPeer$TxHashesNotInCache;
        } else {
            this.payload_ = ConsensusPeer$TxHashesNotInCache.newBuilder((ConsensusPeer$TxHashesNotInCache) this.payload_).mergeFrom((ConsensusPeer$TxHashesNotInCache.Builder) consensusPeer$TxHashesNotInCache).buildPartial();
        }
        this.payloadCase_ = 2;
    }

    public void clearTxHashesNotInCache() {
        if (this.payloadCase_ == 2) {
            this.payloadCase_ = 0;
            this.payload_ = null;
        }
    }

    public static ConsensusPeer$GetTxsResponse parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ConsensusPeer$GetTxsResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ConsensusPeer$GetTxsResponse parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusPeer$GetTxsResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ConsensusPeer$GetTxsResponse parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ConsensusPeer$GetTxsResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ConsensusPeer$GetTxsResponse parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusPeer$GetTxsResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ConsensusPeer$GetTxsResponse parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ConsensusPeer$GetTxsResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ConsensusPeer$GetTxsResponse parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ConsensusPeer$GetTxsResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ConsensusPeer$GetTxsResponse parseFrom(InputStream inputStream) throws IOException {
        return (ConsensusPeer$GetTxsResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ConsensusPeer$GetTxsResponse parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusPeer$GetTxsResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ConsensusPeer$GetTxsResponse parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ConsensusPeer$GetTxsResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ConsensusPeer$GetTxsResponse parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusPeer$GetTxsResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ConsensusPeer$GetTxsResponse parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ConsensusPeer$GetTxsResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ConsensusPeer$GetTxsResponse parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ConsensusPeer$GetTxsResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ConsensusPeer$GetTxsResponse consensusPeer$GetTxsResponse) {
        return DEFAULT_INSTANCE.createBuilder(consensusPeer$GetTxsResponse);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ConsensusPeer$GetTxsResponse, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(ConsensusPeer$1 consensusPeer$1) {
            this();
        }

        private Builder() {
            super(ConsensusPeer$GetTxsResponse.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (ConsensusPeer$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ConsensusPeer$GetTxsResponse();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0001\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001<\u0000\u0002<\u0000", new Object[]{"payload_", "payloadCase_", Attest$Message.class, ConsensusPeer$TxHashesNotInCache.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ConsensusPeer$GetTxsResponse> parser = PARSER;
                if (parser == null) {
                    synchronized (ConsensusPeer$GetTxsResponse.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ConsensusPeer$GetTxsResponse consensusPeer$GetTxsResponse = new ConsensusPeer$GetTxsResponse();
        DEFAULT_INSTANCE = consensusPeer$GetTxsResponse;
        GeneratedMessageLite.registerDefaultInstance(ConsensusPeer$GetTxsResponse.class, consensusPeer$GetTxsResponse);
    }

    public static ConsensusPeer$GetTxsResponse getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ConsensusPeer$GetTxsResponse> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
