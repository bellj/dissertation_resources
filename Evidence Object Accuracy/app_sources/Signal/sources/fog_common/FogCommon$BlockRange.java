package fog_common;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class FogCommon$BlockRange extends GeneratedMessageLite<FogCommon$BlockRange, Builder> implements FogCommon$BlockRangeOrBuilder {
    private static final FogCommon$BlockRange DEFAULT_INSTANCE;
    public static final int END_BLOCK_FIELD_NUMBER;
    private static volatile Parser<FogCommon$BlockRange> PARSER;
    public static final int START_BLOCK_FIELD_NUMBER;
    private long endBlock_;
    private long startBlock_;

    private FogCommon$BlockRange() {
    }

    public long getStartBlock() {
        return this.startBlock_;
    }

    public void setStartBlock(long j) {
        this.startBlock_ = j;
    }

    public void clearStartBlock() {
        this.startBlock_ = 0;
    }

    public long getEndBlock() {
        return this.endBlock_;
    }

    public void setEndBlock(long j) {
        this.endBlock_ = j;
    }

    public void clearEndBlock() {
        this.endBlock_ = 0;
    }

    public static FogCommon$BlockRange parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (FogCommon$BlockRange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static FogCommon$BlockRange parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (FogCommon$BlockRange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static FogCommon$BlockRange parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (FogCommon$BlockRange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static FogCommon$BlockRange parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (FogCommon$BlockRange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static FogCommon$BlockRange parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (FogCommon$BlockRange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static FogCommon$BlockRange parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (FogCommon$BlockRange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static FogCommon$BlockRange parseFrom(InputStream inputStream) throws IOException {
        return (FogCommon$BlockRange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static FogCommon$BlockRange parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (FogCommon$BlockRange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static FogCommon$BlockRange parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (FogCommon$BlockRange) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static FogCommon$BlockRange parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (FogCommon$BlockRange) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static FogCommon$BlockRange parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (FogCommon$BlockRange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static FogCommon$BlockRange parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (FogCommon$BlockRange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(FogCommon$BlockRange fogCommon$BlockRange) {
        return DEFAULT_INSTANCE.createBuilder(fogCommon$BlockRange);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<FogCommon$BlockRange, Builder> implements FogCommon$BlockRangeOrBuilder {
        /* synthetic */ Builder(FogCommon$1 fogCommon$1) {
            this();
        }

        private Builder() {
            super(FogCommon$BlockRange.DEFAULT_INSTANCE);
        }

        public Builder setStartBlock(long j) {
            copyOnWrite();
            ((FogCommon$BlockRange) this.instance).setStartBlock(j);
            return this;
        }

        public Builder setEndBlock(long j) {
            copyOnWrite();
            ((FogCommon$BlockRange) this.instance).setEndBlock(j);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (FogCommon$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new FogCommon$BlockRange();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0003\u0002\u0003", new Object[]{"startBlock_", "endBlock_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<FogCommon$BlockRange> parser = PARSER;
                if (parser == null) {
                    synchronized (FogCommon$BlockRange.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        FogCommon$BlockRange fogCommon$BlockRange = new FogCommon$BlockRange();
        DEFAULT_INSTANCE = fogCommon$BlockRange;
        GeneratedMessageLite.registerDefaultInstance(FogCommon$BlockRange.class, fogCommon$BlockRange);
    }

    public static FogCommon$BlockRange getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<FogCommon$BlockRange> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
