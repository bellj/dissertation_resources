package fog_view;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class View$TxOutRecord extends GeneratedMessageLite<View$TxOutRecord, Builder> implements MessageLiteOrBuilder {
    public static final int BLOCK_INDEX_FIELD_NUMBER;
    private static final View$TxOutRecord DEFAULT_INSTANCE;
    private static volatile Parser<View$TxOutRecord> PARSER;
    public static final int TIMESTAMP_FIELD_NUMBER;
    public static final int TX_OUT_AMOUNT_COMMITMENT_DATA_FIELD_NUMBER;
    public static final int TX_OUT_AMOUNT_MASKED_VALUE_FIELD_NUMBER;
    public static final int TX_OUT_GLOBAL_INDEX_FIELD_NUMBER;
    public static final int TX_OUT_PUBLIC_KEY_DATA_FIELD_NUMBER;
    public static final int TX_OUT_TARGET_KEY_DATA_FIELD_NUMBER;
    private long blockIndex_;
    private long timestamp_;
    private ByteString txOutAmountCommitmentData_;
    private long txOutAmountMaskedValue_;
    private long txOutGlobalIndex_;
    private ByteString txOutPublicKeyData_;
    private ByteString txOutTargetKeyData_;

    private View$TxOutRecord() {
        ByteString byteString = ByteString.EMPTY;
        this.txOutAmountCommitmentData_ = byteString;
        this.txOutTargetKeyData_ = byteString;
        this.txOutPublicKeyData_ = byteString;
    }

    public ByteString getTxOutAmountCommitmentData() {
        return this.txOutAmountCommitmentData_;
    }

    public void setTxOutAmountCommitmentData(ByteString byteString) {
        byteString.getClass();
        this.txOutAmountCommitmentData_ = byteString;
    }

    public void clearTxOutAmountCommitmentData() {
        this.txOutAmountCommitmentData_ = getDefaultInstance().getTxOutAmountCommitmentData();
    }

    public long getTxOutAmountMaskedValue() {
        return this.txOutAmountMaskedValue_;
    }

    public void setTxOutAmountMaskedValue(long j) {
        this.txOutAmountMaskedValue_ = j;
    }

    public void clearTxOutAmountMaskedValue() {
        this.txOutAmountMaskedValue_ = 0;
    }

    public ByteString getTxOutTargetKeyData() {
        return this.txOutTargetKeyData_;
    }

    public void setTxOutTargetKeyData(ByteString byteString) {
        byteString.getClass();
        this.txOutTargetKeyData_ = byteString;
    }

    public void clearTxOutTargetKeyData() {
        this.txOutTargetKeyData_ = getDefaultInstance().getTxOutTargetKeyData();
    }

    public ByteString getTxOutPublicKeyData() {
        return this.txOutPublicKeyData_;
    }

    public void setTxOutPublicKeyData(ByteString byteString) {
        byteString.getClass();
        this.txOutPublicKeyData_ = byteString;
    }

    public void clearTxOutPublicKeyData() {
        this.txOutPublicKeyData_ = getDefaultInstance().getTxOutPublicKeyData();
    }

    public long getTxOutGlobalIndex() {
        return this.txOutGlobalIndex_;
    }

    public void setTxOutGlobalIndex(long j) {
        this.txOutGlobalIndex_ = j;
    }

    public void clearTxOutGlobalIndex() {
        this.txOutGlobalIndex_ = 0;
    }

    public long getBlockIndex() {
        return this.blockIndex_;
    }

    public void setBlockIndex(long j) {
        this.blockIndex_ = j;
    }

    public void clearBlockIndex() {
        this.blockIndex_ = 0;
    }

    public long getTimestamp() {
        return this.timestamp_;
    }

    public void setTimestamp(long j) {
        this.timestamp_ = j;
    }

    public void clearTimestamp() {
        this.timestamp_ = 0;
    }

    public static View$TxOutRecord parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (View$TxOutRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static View$TxOutRecord parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$TxOutRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static View$TxOutRecord parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (View$TxOutRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static View$TxOutRecord parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$TxOutRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static View$TxOutRecord parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (View$TxOutRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static View$TxOutRecord parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$TxOutRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static View$TxOutRecord parseFrom(InputStream inputStream) throws IOException {
        return (View$TxOutRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static View$TxOutRecord parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$TxOutRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static View$TxOutRecord parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (View$TxOutRecord) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static View$TxOutRecord parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$TxOutRecord) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static View$TxOutRecord parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (View$TxOutRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static View$TxOutRecord parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$TxOutRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(View$TxOutRecord view$TxOutRecord) {
        return DEFAULT_INSTANCE.createBuilder(view$TxOutRecord);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<View$TxOutRecord, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(View$1 view$1) {
            this();
        }

        private Builder() {
            super(View$TxOutRecord.DEFAULT_INSTANCE);
        }

        public Builder setTxOutAmountCommitmentData(ByteString byteString) {
            copyOnWrite();
            ((View$TxOutRecord) this.instance).setTxOutAmountCommitmentData(byteString);
            return this;
        }

        public Builder setTxOutAmountMaskedValue(long j) {
            copyOnWrite();
            ((View$TxOutRecord) this.instance).setTxOutAmountMaskedValue(j);
            return this;
        }

        public Builder setTxOutTargetKeyData(ByteString byteString) {
            copyOnWrite();
            ((View$TxOutRecord) this.instance).setTxOutTargetKeyData(byteString);
            return this;
        }

        public Builder setTxOutPublicKeyData(ByteString byteString) {
            copyOnWrite();
            ((View$TxOutRecord) this.instance).setTxOutPublicKeyData(byteString);
            return this;
        }

        public Builder setTxOutGlobalIndex(long j) {
            copyOnWrite();
            ((View$TxOutRecord) this.instance).setTxOutGlobalIndex(j);
            return this;
        }

        public Builder setBlockIndex(long j) {
            copyOnWrite();
            ((View$TxOutRecord) this.instance).setBlockIndex(j);
            return this;
        }

        public Builder setTimestamp(long j) {
            copyOnWrite();
            ((View$TxOutRecord) this.instance).setTimestamp(j);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (View$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new View$TxOutRecord();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0007\u0000\u0000\u0001\u0007\u0007\u0000\u0000\u0000\u0001\n\u0002\u0005\u0003\n\u0004\n\u0005\u0005\u0006\u0005\u0007\u0005", new Object[]{"txOutAmountCommitmentData_", "txOutAmountMaskedValue_", "txOutTargetKeyData_", "txOutPublicKeyData_", "txOutGlobalIndex_", "blockIndex_", "timestamp_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<View$TxOutRecord> parser = PARSER;
                if (parser == null) {
                    synchronized (View$TxOutRecord.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        View$TxOutRecord view$TxOutRecord = new View$TxOutRecord();
        DEFAULT_INSTANCE = view$TxOutRecord;
        GeneratedMessageLite.registerDefaultInstance(View$TxOutRecord.class, view$TxOutRecord);
    }

    public static View$TxOutRecord getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<View$TxOutRecord> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
