package fog_view;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import kex_rng.KexRng$KexRngPubkey;

/* loaded from: classes3.dex */
public final class View$RngRecord extends GeneratedMessageLite<View$RngRecord, Builder> implements View$RngRecordOrBuilder {
    private static final View$RngRecord DEFAULT_INSTANCE;
    public static final int INGEST_INVOCATION_ID_FIELD_NUMBER;
    private static volatile Parser<View$RngRecord> PARSER;
    public static final int PUBKEY_FIELD_NUMBER;
    public static final int START_BLOCK_FIELD_NUMBER;
    private long ingestInvocationId_;
    private KexRng$KexRngPubkey pubkey_;
    private long startBlock_;

    private View$RngRecord() {
    }

    public long getIngestInvocationId() {
        return this.ingestInvocationId_;
    }

    public void setIngestInvocationId(long j) {
        this.ingestInvocationId_ = j;
    }

    public void clearIngestInvocationId() {
        this.ingestInvocationId_ = 0;
    }

    public boolean hasPubkey() {
        return this.pubkey_ != null;
    }

    public KexRng$KexRngPubkey getPubkey() {
        KexRng$KexRngPubkey kexRng$KexRngPubkey = this.pubkey_;
        return kexRng$KexRngPubkey == null ? KexRng$KexRngPubkey.getDefaultInstance() : kexRng$KexRngPubkey;
    }

    public void setPubkey(KexRng$KexRngPubkey kexRng$KexRngPubkey) {
        kexRng$KexRngPubkey.getClass();
        this.pubkey_ = kexRng$KexRngPubkey;
    }

    public void setPubkey(KexRng$KexRngPubkey.Builder builder) {
        this.pubkey_ = builder.build();
    }

    public void mergePubkey(KexRng$KexRngPubkey kexRng$KexRngPubkey) {
        kexRng$KexRngPubkey.getClass();
        KexRng$KexRngPubkey kexRng$KexRngPubkey2 = this.pubkey_;
        if (kexRng$KexRngPubkey2 == null || kexRng$KexRngPubkey2 == KexRng$KexRngPubkey.getDefaultInstance()) {
            this.pubkey_ = kexRng$KexRngPubkey;
        } else {
            this.pubkey_ = KexRng$KexRngPubkey.newBuilder(this.pubkey_).mergeFrom((KexRng$KexRngPubkey.Builder) kexRng$KexRngPubkey).buildPartial();
        }
    }

    public void clearPubkey() {
        this.pubkey_ = null;
    }

    public long getStartBlock() {
        return this.startBlock_;
    }

    public void setStartBlock(long j) {
        this.startBlock_ = j;
    }

    public void clearStartBlock() {
        this.startBlock_ = 0;
    }

    public static View$RngRecord parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (View$RngRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static View$RngRecord parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$RngRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static View$RngRecord parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (View$RngRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static View$RngRecord parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$RngRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static View$RngRecord parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (View$RngRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static View$RngRecord parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$RngRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static View$RngRecord parseFrom(InputStream inputStream) throws IOException {
        return (View$RngRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static View$RngRecord parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$RngRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static View$RngRecord parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (View$RngRecord) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static View$RngRecord parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$RngRecord) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static View$RngRecord parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (View$RngRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static View$RngRecord parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$RngRecord) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(View$RngRecord view$RngRecord) {
        return DEFAULT_INSTANCE.createBuilder(view$RngRecord);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<View$RngRecord, Builder> implements View$RngRecordOrBuilder {
        /* synthetic */ Builder(View$1 view$1) {
            this();
        }

        private Builder() {
            super(View$RngRecord.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (View$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new View$RngRecord();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\u0002\u0002\t\u0003\u0003", new Object[]{"ingestInvocationId_", "pubkey_", "startBlock_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<View$RngRecord> parser = PARSER;
                if (parser == null) {
                    synchronized (View$RngRecord.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        View$RngRecord view$RngRecord = new View$RngRecord();
        DEFAULT_INSTANCE = view$RngRecord;
        GeneratedMessageLite.registerDefaultInstance(View$RngRecord.class, view$RngRecord);
    }

    public static View$RngRecord getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<View$RngRecord> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
