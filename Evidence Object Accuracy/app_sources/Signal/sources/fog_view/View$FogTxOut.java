package fog_view;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$Amount;
import com.mobilecoin.api.MobileCoinAPI$CompressedRistretto;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class View$FogTxOut extends GeneratedMessageLite<View$FogTxOut, Builder> implements MessageLiteOrBuilder {
    public static final int AMOUNT_FIELD_NUMBER;
    private static final View$FogTxOut DEFAULT_INSTANCE;
    private static volatile Parser<View$FogTxOut> PARSER;
    public static final int PUBLIC_KEY_FIELD_NUMBER;
    public static final int TARGET_KEY_FIELD_NUMBER;
    private MobileCoinAPI$Amount amount_;
    private MobileCoinAPI$CompressedRistretto publicKey_;
    private MobileCoinAPI$CompressedRistretto targetKey_;

    private View$FogTxOut() {
    }

    public boolean hasAmount() {
        return this.amount_ != null;
    }

    public MobileCoinAPI$Amount getAmount() {
        MobileCoinAPI$Amount mobileCoinAPI$Amount = this.amount_;
        return mobileCoinAPI$Amount == null ? MobileCoinAPI$Amount.getDefaultInstance() : mobileCoinAPI$Amount;
    }

    public void setAmount(MobileCoinAPI$Amount mobileCoinAPI$Amount) {
        mobileCoinAPI$Amount.getClass();
        this.amount_ = mobileCoinAPI$Amount;
    }

    public void setAmount(MobileCoinAPI$Amount.Builder builder) {
        this.amount_ = builder.build();
    }

    public void mergeAmount(MobileCoinAPI$Amount mobileCoinAPI$Amount) {
        mobileCoinAPI$Amount.getClass();
        MobileCoinAPI$Amount mobileCoinAPI$Amount2 = this.amount_;
        if (mobileCoinAPI$Amount2 == null || mobileCoinAPI$Amount2 == MobileCoinAPI$Amount.getDefaultInstance()) {
            this.amount_ = mobileCoinAPI$Amount;
        } else {
            this.amount_ = MobileCoinAPI$Amount.newBuilder(this.amount_).mergeFrom((MobileCoinAPI$Amount.Builder) mobileCoinAPI$Amount).buildPartial();
        }
    }

    public void clearAmount() {
        this.amount_ = null;
    }

    public boolean hasTargetKey() {
        return this.targetKey_ != null;
    }

    public MobileCoinAPI$CompressedRistretto getTargetKey() {
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto = this.targetKey_;
        return mobileCoinAPI$CompressedRistretto == null ? MobileCoinAPI$CompressedRistretto.getDefaultInstance() : mobileCoinAPI$CompressedRistretto;
    }

    public void setTargetKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        this.targetKey_ = mobileCoinAPI$CompressedRistretto;
    }

    public void setTargetKey(MobileCoinAPI$CompressedRistretto.Builder builder) {
        this.targetKey_ = builder.build();
    }

    public void mergeTargetKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto2 = this.targetKey_;
        if (mobileCoinAPI$CompressedRistretto2 == null || mobileCoinAPI$CompressedRistretto2 == MobileCoinAPI$CompressedRistretto.getDefaultInstance()) {
            this.targetKey_ = mobileCoinAPI$CompressedRistretto;
        } else {
            this.targetKey_ = MobileCoinAPI$CompressedRistretto.newBuilder(this.targetKey_).mergeFrom((MobileCoinAPI$CompressedRistretto.Builder) mobileCoinAPI$CompressedRistretto).buildPartial();
        }
    }

    public void clearTargetKey() {
        this.targetKey_ = null;
    }

    public boolean hasPublicKey() {
        return this.publicKey_ != null;
    }

    public MobileCoinAPI$CompressedRistretto getPublicKey() {
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto = this.publicKey_;
        return mobileCoinAPI$CompressedRistretto == null ? MobileCoinAPI$CompressedRistretto.getDefaultInstance() : mobileCoinAPI$CompressedRistretto;
    }

    public void setPublicKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        this.publicKey_ = mobileCoinAPI$CompressedRistretto;
    }

    public void setPublicKey(MobileCoinAPI$CompressedRistretto.Builder builder) {
        this.publicKey_ = builder.build();
    }

    public void mergePublicKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto2 = this.publicKey_;
        if (mobileCoinAPI$CompressedRistretto2 == null || mobileCoinAPI$CompressedRistretto2 == MobileCoinAPI$CompressedRistretto.getDefaultInstance()) {
            this.publicKey_ = mobileCoinAPI$CompressedRistretto;
        } else {
            this.publicKey_ = MobileCoinAPI$CompressedRistretto.newBuilder(this.publicKey_).mergeFrom((MobileCoinAPI$CompressedRistretto.Builder) mobileCoinAPI$CompressedRistretto).buildPartial();
        }
    }

    public void clearPublicKey() {
        this.publicKey_ = null;
    }

    public static View$FogTxOut parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (View$FogTxOut) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static View$FogTxOut parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$FogTxOut) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static View$FogTxOut parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (View$FogTxOut) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static View$FogTxOut parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$FogTxOut) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static View$FogTxOut parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (View$FogTxOut) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static View$FogTxOut parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$FogTxOut) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static View$FogTxOut parseFrom(InputStream inputStream) throws IOException {
        return (View$FogTxOut) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static View$FogTxOut parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$FogTxOut) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static View$FogTxOut parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (View$FogTxOut) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static View$FogTxOut parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$FogTxOut) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static View$FogTxOut parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (View$FogTxOut) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static View$FogTxOut parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$FogTxOut) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(View$FogTxOut view$FogTxOut) {
        return DEFAULT_INSTANCE.createBuilder(view$FogTxOut);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<View$FogTxOut, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(View$1 view$1) {
            this();
        }

        private Builder() {
            super(View$FogTxOut.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (View$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new View$FogTxOut();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\t\u0002\t\u0003\t", new Object[]{"amount_", "targetKey_", "publicKey_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<View$FogTxOut> parser = PARSER;
                if (parser == null) {
                    synchronized (View$FogTxOut.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        View$FogTxOut view$FogTxOut = new View$FogTxOut();
        DEFAULT_INSTANCE = view$FogTxOut;
        GeneratedMessageLite.registerDefaultInstance(View$FogTxOut.class, view$FogTxOut);
    }

    public static View$FogTxOut getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<View$FogTxOut> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
