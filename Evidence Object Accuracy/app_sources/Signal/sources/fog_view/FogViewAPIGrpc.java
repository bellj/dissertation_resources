package fog_view;

import attest.Attest$AuthMessage;
import attest.Attest$Message;
import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.MethodDescriptor;
import io.grpc.protobuf.lite.ProtoLiteUtils;
import io.grpc.stub.AbstractBlockingStub;
import io.grpc.stub.AbstractStub;
import io.grpc.stub.ClientCalls;

/* loaded from: classes3.dex */
public final class FogViewAPIGrpc {
    private static volatile MethodDescriptor<Attest$AuthMessage, Attest$AuthMessage> getAuthMethod;
    private static volatile MethodDescriptor<Attest$Message, Attest$Message> getQueryMethod;

    /* renamed from: fog_view.FogViewAPIGrpc$1 */
    /* loaded from: classes3.dex */
    public class AnonymousClass1 implements AbstractStub.StubFactory<Object> {
    }

    private FogViewAPIGrpc() {
    }

    public static MethodDescriptor<Attest$AuthMessage, Attest$AuthMessage> getAuthMethod() {
        MethodDescriptor<Attest$AuthMessage, Attest$AuthMessage> methodDescriptor = getAuthMethod;
        if (methodDescriptor == null) {
            synchronized (FogViewAPIGrpc.class) {
                methodDescriptor = getAuthMethod;
                if (methodDescriptor == null) {
                    methodDescriptor = MethodDescriptor.newBuilder().setType(MethodDescriptor.MethodType.UNARY).setFullMethodName(MethodDescriptor.generateFullMethodName("fog_view.FogViewAPI", "Auth")).setSampledToLocalTracing(true).setRequestMarshaller(ProtoLiteUtils.marshaller(Attest$AuthMessage.getDefaultInstance())).setResponseMarshaller(ProtoLiteUtils.marshaller(Attest$AuthMessage.getDefaultInstance())).build();
                    getAuthMethod = methodDescriptor;
                }
            }
        }
        return methodDescriptor;
    }

    public static MethodDescriptor<Attest$Message, Attest$Message> getQueryMethod() {
        MethodDescriptor<Attest$Message, Attest$Message> methodDescriptor = getQueryMethod;
        if (methodDescriptor == null) {
            synchronized (FogViewAPIGrpc.class) {
                methodDescriptor = getQueryMethod;
                if (methodDescriptor == null) {
                    methodDescriptor = MethodDescriptor.newBuilder().setType(MethodDescriptor.MethodType.UNARY).setFullMethodName(MethodDescriptor.generateFullMethodName("fog_view.FogViewAPI", "Query")).setSampledToLocalTracing(true).setRequestMarshaller(ProtoLiteUtils.marshaller(Attest$Message.getDefaultInstance())).setResponseMarshaller(ProtoLiteUtils.marshaller(Attest$Message.getDefaultInstance())).build();
                    getQueryMethod = methodDescriptor;
                }
            }
        }
        return methodDescriptor;
    }

    public static FogViewAPIBlockingStub newBlockingStub(Channel channel) {
        return (FogViewAPIBlockingStub) AbstractBlockingStub.newStub(new AbstractStub.StubFactory<FogViewAPIBlockingStub>() { // from class: fog_view.FogViewAPIGrpc.2
            @Override // io.grpc.stub.AbstractStub.StubFactory
            public FogViewAPIBlockingStub newStub(Channel channel2, CallOptions callOptions) {
                return new FogViewAPIBlockingStub(channel2, callOptions, null);
            }
        }, channel);
    }

    /* loaded from: classes3.dex */
    public static final class FogViewAPIBlockingStub extends AbstractBlockingStub<FogViewAPIBlockingStub> {
        /* synthetic */ FogViewAPIBlockingStub(Channel channel, CallOptions callOptions, AnonymousClass1 r3) {
            this(channel, callOptions);
        }

        private FogViewAPIBlockingStub(Channel channel, CallOptions callOptions) {
            super(channel, callOptions);
        }

        @Override // io.grpc.stub.AbstractStub
        public FogViewAPIBlockingStub build(Channel channel, CallOptions callOptions) {
            return new FogViewAPIBlockingStub(channel, callOptions);
        }

        public Attest$AuthMessage auth(Attest$AuthMessage attest$AuthMessage) {
            return (Attest$AuthMessage) ClientCalls.blockingUnaryCall(getChannel(), FogViewAPIGrpc.getAuthMethod(), getCallOptions(), attest$AuthMessage);
        }

        public Attest$Message query(Attest$Message attest$Message) {
            return (Attest$Message) ClientCalls.blockingUnaryCall(getChannel(), FogViewAPIGrpc.getQueryMethod(), getCallOptions(), attest$Message);
        }
    }
}
