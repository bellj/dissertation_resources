package fog_view;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class View$QueryRequestAAD extends GeneratedMessageLite<View$QueryRequestAAD, Builder> implements MessageLiteOrBuilder {
    private static final View$QueryRequestAAD DEFAULT_INSTANCE;
    private static volatile Parser<View$QueryRequestAAD> PARSER;
    public static final int START_FROM_BLOCK_INDEX_FIELD_NUMBER;
    public static final int START_FROM_USER_EVENT_ID_FIELD_NUMBER;
    private long startFromBlockIndex_;
    private long startFromUserEventId_;

    private View$QueryRequestAAD() {
    }

    public long getStartFromUserEventId() {
        return this.startFromUserEventId_;
    }

    public void setStartFromUserEventId(long j) {
        this.startFromUserEventId_ = j;
    }

    public void clearStartFromUserEventId() {
        this.startFromUserEventId_ = 0;
    }

    public long getStartFromBlockIndex() {
        return this.startFromBlockIndex_;
    }

    public void setStartFromBlockIndex(long j) {
        this.startFromBlockIndex_ = j;
    }

    public void clearStartFromBlockIndex() {
        this.startFromBlockIndex_ = 0;
    }

    public static View$QueryRequestAAD parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (View$QueryRequestAAD) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static View$QueryRequestAAD parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$QueryRequestAAD) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static View$QueryRequestAAD parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (View$QueryRequestAAD) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static View$QueryRequestAAD parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$QueryRequestAAD) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static View$QueryRequestAAD parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (View$QueryRequestAAD) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static View$QueryRequestAAD parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$QueryRequestAAD) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static View$QueryRequestAAD parseFrom(InputStream inputStream) throws IOException {
        return (View$QueryRequestAAD) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static View$QueryRequestAAD parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$QueryRequestAAD) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static View$QueryRequestAAD parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (View$QueryRequestAAD) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static View$QueryRequestAAD parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$QueryRequestAAD) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static View$QueryRequestAAD parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (View$QueryRequestAAD) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static View$QueryRequestAAD parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$QueryRequestAAD) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(View$QueryRequestAAD view$QueryRequestAAD) {
        return DEFAULT_INSTANCE.createBuilder(view$QueryRequestAAD);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<View$QueryRequestAAD, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(View$1 view$1) {
            this();
        }

        private Builder() {
            super(View$QueryRequestAAD.DEFAULT_INSTANCE);
        }

        public Builder setStartFromUserEventId(long j) {
            copyOnWrite();
            ((View$QueryRequestAAD) this.instance).setStartFromUserEventId(j);
            return this;
        }

        public Builder setStartFromBlockIndex(long j) {
            copyOnWrite();
            ((View$QueryRequestAAD) this.instance).setStartFromBlockIndex(j);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (View$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new View$QueryRequestAAD();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0002\u0002\u0003", new Object[]{"startFromUserEventId_", "startFromBlockIndex_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<View$QueryRequestAAD> parser = PARSER;
                if (parser == null) {
                    synchronized (View$QueryRequestAAD.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        View$QueryRequestAAD view$QueryRequestAAD = new View$QueryRequestAAD();
        DEFAULT_INSTANCE = view$QueryRequestAAD;
        GeneratedMessageLite.registerDefaultInstance(View$QueryRequestAAD.class, view$QueryRequestAAD);
    }

    public static View$QueryRequestAAD getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<View$QueryRequestAAD> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
