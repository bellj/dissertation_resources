package fog_view;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class View$TxOutSearchResult extends GeneratedMessageLite<View$TxOutSearchResult, Builder> implements View$TxOutSearchResultOrBuilder {
    public static final int CIPHERTEXT_FIELD_NUMBER;
    private static final View$TxOutSearchResult DEFAULT_INSTANCE;
    private static volatile Parser<View$TxOutSearchResult> PARSER;
    public static final int RESULT_CODE_FIELD_NUMBER;
    public static final int SEARCH_KEY_FIELD_NUMBER;
    private ByteString ciphertext_;
    private int resultCode_;
    private ByteString searchKey_;

    private View$TxOutSearchResult() {
        ByteString byteString = ByteString.EMPTY;
        this.searchKey_ = byteString;
        this.ciphertext_ = byteString;
    }

    public ByteString getSearchKey() {
        return this.searchKey_;
    }

    public void setSearchKey(ByteString byteString) {
        byteString.getClass();
        this.searchKey_ = byteString;
    }

    public void clearSearchKey() {
        this.searchKey_ = getDefaultInstance().getSearchKey();
    }

    public int getResultCode() {
        return this.resultCode_;
    }

    public void setResultCode(int i) {
        this.resultCode_ = i;
    }

    public void clearResultCode() {
        this.resultCode_ = 0;
    }

    public ByteString getCiphertext() {
        return this.ciphertext_;
    }

    public void setCiphertext(ByteString byteString) {
        byteString.getClass();
        this.ciphertext_ = byteString;
    }

    public void clearCiphertext() {
        this.ciphertext_ = getDefaultInstance().getCiphertext();
    }

    public static View$TxOutSearchResult parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (View$TxOutSearchResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static View$TxOutSearchResult parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$TxOutSearchResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static View$TxOutSearchResult parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (View$TxOutSearchResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static View$TxOutSearchResult parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$TxOutSearchResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static View$TxOutSearchResult parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (View$TxOutSearchResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static View$TxOutSearchResult parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$TxOutSearchResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static View$TxOutSearchResult parseFrom(InputStream inputStream) throws IOException {
        return (View$TxOutSearchResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static View$TxOutSearchResult parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$TxOutSearchResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static View$TxOutSearchResult parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (View$TxOutSearchResult) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static View$TxOutSearchResult parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$TxOutSearchResult) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static View$TxOutSearchResult parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (View$TxOutSearchResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static View$TxOutSearchResult parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$TxOutSearchResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(View$TxOutSearchResult view$TxOutSearchResult) {
        return DEFAULT_INSTANCE.createBuilder(view$TxOutSearchResult);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<View$TxOutSearchResult, Builder> implements View$TxOutSearchResultOrBuilder {
        /* synthetic */ Builder(View$1 view$1) {
            this();
        }

        private Builder() {
            super(View$TxOutSearchResult.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (View$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new View$TxOutSearchResult();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\n\u0002\u0006\u0003\n", new Object[]{"searchKey_", "resultCode_", "ciphertext_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<View$TxOutSearchResult> parser = PARSER;
                if (parser == null) {
                    synchronized (View$TxOutSearchResult.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        View$TxOutSearchResult view$TxOutSearchResult = new View$TxOutSearchResult();
        DEFAULT_INSTANCE = view$TxOutSearchResult;
        GeneratedMessageLite.registerDefaultInstance(View$TxOutSearchResult.class, view$TxOutSearchResult);
    }

    public static View$TxOutSearchResult getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<View$TxOutSearchResult> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
