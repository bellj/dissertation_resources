package fog_view;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class View$DecommissionedIngestInvocation extends GeneratedMessageLite<View$DecommissionedIngestInvocation, Builder> implements View$DecommissionedIngestInvocationOrBuilder {
    private static final View$DecommissionedIngestInvocation DEFAULT_INSTANCE;
    public static final int INGEST_INVOCATION_ID_FIELD_NUMBER;
    public static final int LAST_INGESTED_BLOCK_FIELD_NUMBER;
    private static volatile Parser<View$DecommissionedIngestInvocation> PARSER;
    private long ingestInvocationId_;
    private long lastIngestedBlock_;

    private View$DecommissionedIngestInvocation() {
    }

    public long getIngestInvocationId() {
        return this.ingestInvocationId_;
    }

    public void setIngestInvocationId(long j) {
        this.ingestInvocationId_ = j;
    }

    public void clearIngestInvocationId() {
        this.ingestInvocationId_ = 0;
    }

    public long getLastIngestedBlock() {
        return this.lastIngestedBlock_;
    }

    public void setLastIngestedBlock(long j) {
        this.lastIngestedBlock_ = j;
    }

    public void clearLastIngestedBlock() {
        this.lastIngestedBlock_ = 0;
    }

    public static View$DecommissionedIngestInvocation parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (View$DecommissionedIngestInvocation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static View$DecommissionedIngestInvocation parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$DecommissionedIngestInvocation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static View$DecommissionedIngestInvocation parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (View$DecommissionedIngestInvocation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static View$DecommissionedIngestInvocation parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$DecommissionedIngestInvocation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static View$DecommissionedIngestInvocation parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (View$DecommissionedIngestInvocation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static View$DecommissionedIngestInvocation parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$DecommissionedIngestInvocation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static View$DecommissionedIngestInvocation parseFrom(InputStream inputStream) throws IOException {
        return (View$DecommissionedIngestInvocation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static View$DecommissionedIngestInvocation parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$DecommissionedIngestInvocation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static View$DecommissionedIngestInvocation parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (View$DecommissionedIngestInvocation) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static View$DecommissionedIngestInvocation parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$DecommissionedIngestInvocation) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static View$DecommissionedIngestInvocation parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (View$DecommissionedIngestInvocation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static View$DecommissionedIngestInvocation parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$DecommissionedIngestInvocation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(View$DecommissionedIngestInvocation view$DecommissionedIngestInvocation) {
        return DEFAULT_INSTANCE.createBuilder(view$DecommissionedIngestInvocation);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<View$DecommissionedIngestInvocation, Builder> implements View$DecommissionedIngestInvocationOrBuilder {
        /* synthetic */ Builder(View$1 view$1) {
            this();
        }

        private Builder() {
            super(View$DecommissionedIngestInvocation.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (View$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new View$DecommissionedIngestInvocation();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0002\u0002\u0003", new Object[]{"ingestInvocationId_", "lastIngestedBlock_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<View$DecommissionedIngestInvocation> parser = PARSER;
                if (parser == null) {
                    synchronized (View$DecommissionedIngestInvocation.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        View$DecommissionedIngestInvocation view$DecommissionedIngestInvocation = new View$DecommissionedIngestInvocation();
        DEFAULT_INSTANCE = view$DecommissionedIngestInvocation;
        GeneratedMessageLite.registerDefaultInstance(View$DecommissionedIngestInvocation.class, view$DecommissionedIngestInvocation);
    }

    public static View$DecommissionedIngestInvocation getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<View$DecommissionedIngestInvocation> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
