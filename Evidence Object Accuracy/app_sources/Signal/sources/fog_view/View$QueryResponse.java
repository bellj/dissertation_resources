package fog_view;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import fog_common.FogCommon$BlockRange;
import fog_common.FogCommon$BlockRangeOrBuilder;
import fog_view.View$DecommissionedIngestInvocation;
import fog_view.View$RngRecord;
import fog_view.View$TxOutSearchResult;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class View$QueryResponse extends GeneratedMessageLite<View$QueryResponse, Builder> implements MessageLiteOrBuilder {
    public static final int DECOMMISSIONED_INGEST_INVOCATIONS_FIELD_NUMBER;
    private static final View$QueryResponse DEFAULT_INSTANCE;
    public static final int HIGHEST_PROCESSED_BLOCK_COUNT_FIELD_NUMBER;
    public static final int HIGHEST_PROCESSED_BLOCK_SIGNATURE_TIMESTAMP_FIELD_NUMBER;
    public static final int LAST_KNOWN_BLOCK_COUNT_FIELD_NUMBER;
    public static final int LAST_KNOWN_BLOCK_CUMULATIVE_TXO_COUNT_FIELD_NUMBER;
    public static final int MISSED_BLOCK_RANGES_FIELD_NUMBER;
    public static final int NEXT_START_FROM_USER_EVENT_ID_FIELD_NUMBER;
    private static volatile Parser<View$QueryResponse> PARSER;
    public static final int RNGS_FIELD_NUMBER;
    public static final int TX_OUT_SEARCH_RESULTS_FIELD_NUMBER;
    private Internal.ProtobufList<View$DecommissionedIngestInvocation> decommissionedIngestInvocations_ = GeneratedMessageLite.emptyProtobufList();
    private long highestProcessedBlockCount_;
    private long highestProcessedBlockSignatureTimestamp_;
    private long lastKnownBlockCount_;
    private long lastKnownBlockCumulativeTxoCount_;
    private Internal.ProtobufList<FogCommon$BlockRange> missedBlockRanges_ = GeneratedMessageLite.emptyProtobufList();
    private long nextStartFromUserEventId_;
    private Internal.ProtobufList<View$RngRecord> rngs_ = GeneratedMessageLite.emptyProtobufList();
    private Internal.ProtobufList<View$TxOutSearchResult> txOutSearchResults_ = GeneratedMessageLite.emptyProtobufList();

    private View$QueryResponse() {
    }

    public long getHighestProcessedBlockCount() {
        return this.highestProcessedBlockCount_;
    }

    public void setHighestProcessedBlockCount(long j) {
        this.highestProcessedBlockCount_ = j;
    }

    public void clearHighestProcessedBlockCount() {
        this.highestProcessedBlockCount_ = 0;
    }

    public long getHighestProcessedBlockSignatureTimestamp() {
        return this.highestProcessedBlockSignatureTimestamp_;
    }

    public void setHighestProcessedBlockSignatureTimestamp(long j) {
        this.highestProcessedBlockSignatureTimestamp_ = j;
    }

    public void clearHighestProcessedBlockSignatureTimestamp() {
        this.highestProcessedBlockSignatureTimestamp_ = 0;
    }

    public long getNextStartFromUserEventId() {
        return this.nextStartFromUserEventId_;
    }

    public void setNextStartFromUserEventId(long j) {
        this.nextStartFromUserEventId_ = j;
    }

    public void clearNextStartFromUserEventId() {
        this.nextStartFromUserEventId_ = 0;
    }

    public List<FogCommon$BlockRange> getMissedBlockRangesList() {
        return this.missedBlockRanges_;
    }

    public List<? extends FogCommon$BlockRangeOrBuilder> getMissedBlockRangesOrBuilderList() {
        return this.missedBlockRanges_;
    }

    public int getMissedBlockRangesCount() {
        return this.missedBlockRanges_.size();
    }

    public FogCommon$BlockRange getMissedBlockRanges(int i) {
        return this.missedBlockRanges_.get(i);
    }

    public FogCommon$BlockRangeOrBuilder getMissedBlockRangesOrBuilder(int i) {
        return this.missedBlockRanges_.get(i);
    }

    private void ensureMissedBlockRangesIsMutable() {
        if (!this.missedBlockRanges_.isModifiable()) {
            this.missedBlockRanges_ = GeneratedMessageLite.mutableCopy(this.missedBlockRanges_);
        }
    }

    public void setMissedBlockRanges(int i, FogCommon$BlockRange fogCommon$BlockRange) {
        fogCommon$BlockRange.getClass();
        ensureMissedBlockRangesIsMutable();
        this.missedBlockRanges_.set(i, fogCommon$BlockRange);
    }

    public void setMissedBlockRanges(int i, FogCommon$BlockRange.Builder builder) {
        ensureMissedBlockRangesIsMutable();
        this.missedBlockRanges_.set(i, builder.build());
    }

    public void addMissedBlockRanges(FogCommon$BlockRange fogCommon$BlockRange) {
        fogCommon$BlockRange.getClass();
        ensureMissedBlockRangesIsMutable();
        this.missedBlockRanges_.add(fogCommon$BlockRange);
    }

    public void addMissedBlockRanges(int i, FogCommon$BlockRange fogCommon$BlockRange) {
        fogCommon$BlockRange.getClass();
        ensureMissedBlockRangesIsMutable();
        this.missedBlockRanges_.add(i, fogCommon$BlockRange);
    }

    public void addMissedBlockRanges(FogCommon$BlockRange.Builder builder) {
        ensureMissedBlockRangesIsMutable();
        this.missedBlockRanges_.add(builder.build());
    }

    public void addMissedBlockRanges(int i, FogCommon$BlockRange.Builder builder) {
        ensureMissedBlockRangesIsMutable();
        this.missedBlockRanges_.add(i, builder.build());
    }

    public void addAllMissedBlockRanges(Iterable<? extends FogCommon$BlockRange> iterable) {
        ensureMissedBlockRangesIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.missedBlockRanges_);
    }

    public void clearMissedBlockRanges() {
        this.missedBlockRanges_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeMissedBlockRanges(int i) {
        ensureMissedBlockRangesIsMutable();
        this.missedBlockRanges_.remove(i);
    }

    public List<View$RngRecord> getRngsList() {
        return this.rngs_;
    }

    public List<? extends View$RngRecordOrBuilder> getRngsOrBuilderList() {
        return this.rngs_;
    }

    public int getRngsCount() {
        return this.rngs_.size();
    }

    public View$RngRecord getRngs(int i) {
        return this.rngs_.get(i);
    }

    public View$RngRecordOrBuilder getRngsOrBuilder(int i) {
        return this.rngs_.get(i);
    }

    private void ensureRngsIsMutable() {
        if (!this.rngs_.isModifiable()) {
            this.rngs_ = GeneratedMessageLite.mutableCopy(this.rngs_);
        }
    }

    public void setRngs(int i, View$RngRecord view$RngRecord) {
        view$RngRecord.getClass();
        ensureRngsIsMutable();
        this.rngs_.set(i, view$RngRecord);
    }

    public void setRngs(int i, View$RngRecord.Builder builder) {
        ensureRngsIsMutable();
        this.rngs_.set(i, builder.build());
    }

    public void addRngs(View$RngRecord view$RngRecord) {
        view$RngRecord.getClass();
        ensureRngsIsMutable();
        this.rngs_.add(view$RngRecord);
    }

    public void addRngs(int i, View$RngRecord view$RngRecord) {
        view$RngRecord.getClass();
        ensureRngsIsMutable();
        this.rngs_.add(i, view$RngRecord);
    }

    public void addRngs(View$RngRecord.Builder builder) {
        ensureRngsIsMutable();
        this.rngs_.add(builder.build());
    }

    public void addRngs(int i, View$RngRecord.Builder builder) {
        ensureRngsIsMutable();
        this.rngs_.add(i, builder.build());
    }

    public void addAllRngs(Iterable<? extends View$RngRecord> iterable) {
        ensureRngsIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.rngs_);
    }

    public void clearRngs() {
        this.rngs_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeRngs(int i) {
        ensureRngsIsMutable();
        this.rngs_.remove(i);
    }

    public List<View$DecommissionedIngestInvocation> getDecommissionedIngestInvocationsList() {
        return this.decommissionedIngestInvocations_;
    }

    public List<? extends View$DecommissionedIngestInvocationOrBuilder> getDecommissionedIngestInvocationsOrBuilderList() {
        return this.decommissionedIngestInvocations_;
    }

    public int getDecommissionedIngestInvocationsCount() {
        return this.decommissionedIngestInvocations_.size();
    }

    public View$DecommissionedIngestInvocation getDecommissionedIngestInvocations(int i) {
        return this.decommissionedIngestInvocations_.get(i);
    }

    public View$DecommissionedIngestInvocationOrBuilder getDecommissionedIngestInvocationsOrBuilder(int i) {
        return this.decommissionedIngestInvocations_.get(i);
    }

    private void ensureDecommissionedIngestInvocationsIsMutable() {
        if (!this.decommissionedIngestInvocations_.isModifiable()) {
            this.decommissionedIngestInvocations_ = GeneratedMessageLite.mutableCopy(this.decommissionedIngestInvocations_);
        }
    }

    public void setDecommissionedIngestInvocations(int i, View$DecommissionedIngestInvocation view$DecommissionedIngestInvocation) {
        view$DecommissionedIngestInvocation.getClass();
        ensureDecommissionedIngestInvocationsIsMutable();
        this.decommissionedIngestInvocations_.set(i, view$DecommissionedIngestInvocation);
    }

    public void setDecommissionedIngestInvocations(int i, View$DecommissionedIngestInvocation.Builder builder) {
        ensureDecommissionedIngestInvocationsIsMutable();
        this.decommissionedIngestInvocations_.set(i, builder.build());
    }

    public void addDecommissionedIngestInvocations(View$DecommissionedIngestInvocation view$DecommissionedIngestInvocation) {
        view$DecommissionedIngestInvocation.getClass();
        ensureDecommissionedIngestInvocationsIsMutable();
        this.decommissionedIngestInvocations_.add(view$DecommissionedIngestInvocation);
    }

    public void addDecommissionedIngestInvocations(int i, View$DecommissionedIngestInvocation view$DecommissionedIngestInvocation) {
        view$DecommissionedIngestInvocation.getClass();
        ensureDecommissionedIngestInvocationsIsMutable();
        this.decommissionedIngestInvocations_.add(i, view$DecommissionedIngestInvocation);
    }

    public void addDecommissionedIngestInvocations(View$DecommissionedIngestInvocation.Builder builder) {
        ensureDecommissionedIngestInvocationsIsMutable();
        this.decommissionedIngestInvocations_.add(builder.build());
    }

    public void addDecommissionedIngestInvocations(int i, View$DecommissionedIngestInvocation.Builder builder) {
        ensureDecommissionedIngestInvocationsIsMutable();
        this.decommissionedIngestInvocations_.add(i, builder.build());
    }

    public void addAllDecommissionedIngestInvocations(Iterable<? extends View$DecommissionedIngestInvocation> iterable) {
        ensureDecommissionedIngestInvocationsIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.decommissionedIngestInvocations_);
    }

    public void clearDecommissionedIngestInvocations() {
        this.decommissionedIngestInvocations_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeDecommissionedIngestInvocations(int i) {
        ensureDecommissionedIngestInvocationsIsMutable();
        this.decommissionedIngestInvocations_.remove(i);
    }

    public List<View$TxOutSearchResult> getTxOutSearchResultsList() {
        return this.txOutSearchResults_;
    }

    public List<? extends View$TxOutSearchResultOrBuilder> getTxOutSearchResultsOrBuilderList() {
        return this.txOutSearchResults_;
    }

    public int getTxOutSearchResultsCount() {
        return this.txOutSearchResults_.size();
    }

    public View$TxOutSearchResult getTxOutSearchResults(int i) {
        return this.txOutSearchResults_.get(i);
    }

    public View$TxOutSearchResultOrBuilder getTxOutSearchResultsOrBuilder(int i) {
        return this.txOutSearchResults_.get(i);
    }

    private void ensureTxOutSearchResultsIsMutable() {
        if (!this.txOutSearchResults_.isModifiable()) {
            this.txOutSearchResults_ = GeneratedMessageLite.mutableCopy(this.txOutSearchResults_);
        }
    }

    public void setTxOutSearchResults(int i, View$TxOutSearchResult view$TxOutSearchResult) {
        view$TxOutSearchResult.getClass();
        ensureTxOutSearchResultsIsMutable();
        this.txOutSearchResults_.set(i, view$TxOutSearchResult);
    }

    public void setTxOutSearchResults(int i, View$TxOutSearchResult.Builder builder) {
        ensureTxOutSearchResultsIsMutable();
        this.txOutSearchResults_.set(i, builder.build());
    }

    public void addTxOutSearchResults(View$TxOutSearchResult view$TxOutSearchResult) {
        view$TxOutSearchResult.getClass();
        ensureTxOutSearchResultsIsMutable();
        this.txOutSearchResults_.add(view$TxOutSearchResult);
    }

    public void addTxOutSearchResults(int i, View$TxOutSearchResult view$TxOutSearchResult) {
        view$TxOutSearchResult.getClass();
        ensureTxOutSearchResultsIsMutable();
        this.txOutSearchResults_.add(i, view$TxOutSearchResult);
    }

    public void addTxOutSearchResults(View$TxOutSearchResult.Builder builder) {
        ensureTxOutSearchResultsIsMutable();
        this.txOutSearchResults_.add(builder.build());
    }

    public void addTxOutSearchResults(int i, View$TxOutSearchResult.Builder builder) {
        ensureTxOutSearchResultsIsMutable();
        this.txOutSearchResults_.add(i, builder.build());
    }

    public void addAllTxOutSearchResults(Iterable<? extends View$TxOutSearchResult> iterable) {
        ensureTxOutSearchResultsIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.txOutSearchResults_);
    }

    public void clearTxOutSearchResults() {
        this.txOutSearchResults_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeTxOutSearchResults(int i) {
        ensureTxOutSearchResultsIsMutable();
        this.txOutSearchResults_.remove(i);
    }

    public long getLastKnownBlockCount() {
        return this.lastKnownBlockCount_;
    }

    public void setLastKnownBlockCount(long j) {
        this.lastKnownBlockCount_ = j;
    }

    public void clearLastKnownBlockCount() {
        this.lastKnownBlockCount_ = 0;
    }

    public long getLastKnownBlockCumulativeTxoCount() {
        return this.lastKnownBlockCumulativeTxoCount_;
    }

    public void setLastKnownBlockCumulativeTxoCount(long j) {
        this.lastKnownBlockCumulativeTxoCount_ = j;
    }

    public void clearLastKnownBlockCumulativeTxoCount() {
        this.lastKnownBlockCumulativeTxoCount_ = 0;
    }

    public static View$QueryResponse parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (View$QueryResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static View$QueryResponse parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$QueryResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static View$QueryResponse parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (View$QueryResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static View$QueryResponse parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$QueryResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static View$QueryResponse parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (View$QueryResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static View$QueryResponse parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$QueryResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static View$QueryResponse parseFrom(InputStream inputStream) throws IOException {
        return (View$QueryResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static View$QueryResponse parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$QueryResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static View$QueryResponse parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (View$QueryResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static View$QueryResponse parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$QueryResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static View$QueryResponse parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (View$QueryResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static View$QueryResponse parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$QueryResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(View$QueryResponse view$QueryResponse) {
        return DEFAULT_INSTANCE.createBuilder(view$QueryResponse);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<View$QueryResponse, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(View$1 view$1) {
            this();
        }

        private Builder() {
            super(View$QueryResponse.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (View$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new View$QueryResponse();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\t\u0000\u0000\u0001\t\t\u0000\u0004\u0000\u0001\u0003\u0002\u0003\u0003\u0002\u0004\u001b\u0005\u001b\u0006\u001b\u0007\u001b\b\u0003\t\u0003", new Object[]{"highestProcessedBlockCount_", "highestProcessedBlockSignatureTimestamp_", "nextStartFromUserEventId_", "missedBlockRanges_", FogCommon$BlockRange.class, "rngs_", View$RngRecord.class, "decommissionedIngestInvocations_", View$DecommissionedIngestInvocation.class, "txOutSearchResults_", View$TxOutSearchResult.class, "lastKnownBlockCount_", "lastKnownBlockCumulativeTxoCount_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<View$QueryResponse> parser = PARSER;
                if (parser == null) {
                    synchronized (View$QueryResponse.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        View$QueryResponse view$QueryResponse = new View$QueryResponse();
        DEFAULT_INSTANCE = view$QueryResponse;
        GeneratedMessageLite.registerDefaultInstance(View$QueryResponse.class, view$QueryResponse);
    }

    public static View$QueryResponse getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<View$QueryResponse> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
