package fog_view;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class View$QueryRequest extends GeneratedMessageLite<View$QueryRequest, Builder> implements MessageLiteOrBuilder {
    private static final View$QueryRequest DEFAULT_INSTANCE;
    public static final int GET_TXOS_FIELD_NUMBER;
    private static volatile Parser<View$QueryRequest> PARSER;
    private Internal.ProtobufList<ByteString> getTxos_ = GeneratedMessageLite.emptyProtobufList();

    private View$QueryRequest() {
    }

    public List<ByteString> getGetTxosList() {
        return this.getTxos_;
    }

    public int getGetTxosCount() {
        return this.getTxos_.size();
    }

    public ByteString getGetTxos(int i) {
        return this.getTxos_.get(i);
    }

    private void ensureGetTxosIsMutable() {
        if (!this.getTxos_.isModifiable()) {
            this.getTxos_ = GeneratedMessageLite.mutableCopy(this.getTxos_);
        }
    }

    public void setGetTxos(int i, ByteString byteString) {
        byteString.getClass();
        ensureGetTxosIsMutable();
        this.getTxos_.set(i, byteString);
    }

    public void addGetTxos(ByteString byteString) {
        byteString.getClass();
        ensureGetTxosIsMutable();
        this.getTxos_.add(byteString);
    }

    public void addAllGetTxos(Iterable<? extends ByteString> iterable) {
        ensureGetTxosIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.getTxos_);
    }

    public void clearGetTxos() {
        this.getTxos_ = GeneratedMessageLite.emptyProtobufList();
    }

    public static View$QueryRequest parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (View$QueryRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static View$QueryRequest parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$QueryRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static View$QueryRequest parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (View$QueryRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static View$QueryRequest parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$QueryRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static View$QueryRequest parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (View$QueryRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static View$QueryRequest parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (View$QueryRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static View$QueryRequest parseFrom(InputStream inputStream) throws IOException {
        return (View$QueryRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static View$QueryRequest parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$QueryRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static View$QueryRequest parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (View$QueryRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static View$QueryRequest parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$QueryRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static View$QueryRequest parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (View$QueryRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static View$QueryRequest parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (View$QueryRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(View$QueryRequest view$QueryRequest) {
        return DEFAULT_INSTANCE.createBuilder(view$QueryRequest);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<View$QueryRequest, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(View$1 view$1) {
            this();
        }

        private Builder() {
            super(View$QueryRequest.DEFAULT_INSTANCE);
        }

        public Builder addGetTxos(ByteString byteString) {
            copyOnWrite();
            ((View$QueryRequest) this.instance).addGetTxos(byteString);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (View$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new View$QueryRequest();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001c", new Object[]{"getTxos_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<View$QueryRequest> parser = PARSER;
                if (parser == null) {
                    synchronized (View$QueryRequest.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        View$QueryRequest view$QueryRequest = new View$QueryRequest();
        DEFAULT_INSTANCE = view$QueryRequest;
        GeneratedMessageLite.registerDefaultInstance(View$QueryRequest.class, view$QueryRequest);
    }

    public static View$QueryRequest getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<View$QueryRequest> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
