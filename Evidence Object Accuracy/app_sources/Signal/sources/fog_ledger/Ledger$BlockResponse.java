package fog_ledger;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import fog_ledger.Ledger$Block;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class Ledger$BlockResponse extends GeneratedMessageLite<Ledger$BlockResponse, Builder> implements MessageLiteOrBuilder {
    public static final int BLOCKS_FIELD_NUMBER;
    private static final Ledger$BlockResponse DEFAULT_INSTANCE;
    public static final int GLOBAL_TXO_COUNT_FIELD_NUMBER;
    public static final int NUM_BLOCKS_FIELD_NUMBER;
    private static volatile Parser<Ledger$BlockResponse> PARSER;
    private Internal.ProtobufList<Ledger$Block> blocks_ = GeneratedMessageLite.emptyProtobufList();
    private long globalTxoCount_;
    private long numBlocks_;

    private Ledger$BlockResponse() {
    }

    public List<Ledger$Block> getBlocksList() {
        return this.blocks_;
    }

    public List<? extends Ledger$BlockOrBuilder> getBlocksOrBuilderList() {
        return this.blocks_;
    }

    public int getBlocksCount() {
        return this.blocks_.size();
    }

    public Ledger$Block getBlocks(int i) {
        return this.blocks_.get(i);
    }

    public Ledger$BlockOrBuilder getBlocksOrBuilder(int i) {
        return this.blocks_.get(i);
    }

    private void ensureBlocksIsMutable() {
        if (!this.blocks_.isModifiable()) {
            this.blocks_ = GeneratedMessageLite.mutableCopy(this.blocks_);
        }
    }

    public void setBlocks(int i, Ledger$Block ledger$Block) {
        ledger$Block.getClass();
        ensureBlocksIsMutable();
        this.blocks_.set(i, ledger$Block);
    }

    public void setBlocks(int i, Ledger$Block.Builder builder) {
        ensureBlocksIsMutable();
        this.blocks_.set(i, builder.build());
    }

    public void addBlocks(Ledger$Block ledger$Block) {
        ledger$Block.getClass();
        ensureBlocksIsMutable();
        this.blocks_.add(ledger$Block);
    }

    public void addBlocks(int i, Ledger$Block ledger$Block) {
        ledger$Block.getClass();
        ensureBlocksIsMutable();
        this.blocks_.add(i, ledger$Block);
    }

    public void addBlocks(Ledger$Block.Builder builder) {
        ensureBlocksIsMutable();
        this.blocks_.add(builder.build());
    }

    public void addBlocks(int i, Ledger$Block.Builder builder) {
        ensureBlocksIsMutable();
        this.blocks_.add(i, builder.build());
    }

    public void addAllBlocks(Iterable<? extends Ledger$Block> iterable) {
        ensureBlocksIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.blocks_);
    }

    public void clearBlocks() {
        this.blocks_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeBlocks(int i) {
        ensureBlocksIsMutable();
        this.blocks_.remove(i);
    }

    public long getNumBlocks() {
        return this.numBlocks_;
    }

    public void setNumBlocks(long j) {
        this.numBlocks_ = j;
    }

    public void clearNumBlocks() {
        this.numBlocks_ = 0;
    }

    public long getGlobalTxoCount() {
        return this.globalTxoCount_;
    }

    public void setGlobalTxoCount(long j) {
        this.globalTxoCount_ = j;
    }

    public void clearGlobalTxoCount() {
        this.globalTxoCount_ = 0;
    }

    public static Ledger$BlockResponse parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Ledger$BlockResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Ledger$BlockResponse parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$BlockResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Ledger$BlockResponse parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Ledger$BlockResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Ledger$BlockResponse parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$BlockResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Ledger$BlockResponse parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Ledger$BlockResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Ledger$BlockResponse parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$BlockResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Ledger$BlockResponse parseFrom(InputStream inputStream) throws IOException {
        return (Ledger$BlockResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ledger$BlockResponse parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$BlockResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ledger$BlockResponse parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Ledger$BlockResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ledger$BlockResponse parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$BlockResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ledger$BlockResponse parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Ledger$BlockResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Ledger$BlockResponse parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$BlockResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Ledger$BlockResponse ledger$BlockResponse) {
        return DEFAULT_INSTANCE.createBuilder(ledger$BlockResponse);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Ledger$BlockResponse, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(Ledger$1 ledger$1) {
            this();
        }

        private Builder() {
            super(Ledger$BlockResponse.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Ledger$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Ledger$BlockResponse();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0001\u0000\u0001\u001b\u0002\u0003\u0003\u0003", new Object[]{"blocks_", Ledger$Block.class, "numBlocks_", "globalTxoCount_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Ledger$BlockResponse> parser = PARSER;
                if (parser == null) {
                    synchronized (Ledger$BlockResponse.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Ledger$BlockResponse ledger$BlockResponse = new Ledger$BlockResponse();
        DEFAULT_INSTANCE = ledger$BlockResponse;
        GeneratedMessageLite.registerDefaultInstance(Ledger$BlockResponse.class, ledger$BlockResponse);
    }

    public static Ledger$BlockResponse getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Ledger$BlockResponse> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
