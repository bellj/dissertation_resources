package fog_ledger;

import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.MethodDescriptor;
import io.grpc.protobuf.lite.ProtoLiteUtils;
import io.grpc.stub.AbstractBlockingStub;
import io.grpc.stub.AbstractStub;
import io.grpc.stub.ClientCalls;

/* loaded from: classes3.dex */
public final class FogBlockAPIGrpc {
    private static volatile MethodDescriptor<Ledger$BlockRequest, Ledger$BlockResponse> getGetBlocksMethod;

    /* renamed from: fog_ledger.FogBlockAPIGrpc$1 */
    /* loaded from: classes3.dex */
    public class AnonymousClass1 implements AbstractStub.StubFactory<Object> {
    }

    private FogBlockAPIGrpc() {
    }

    public static MethodDescriptor<Ledger$BlockRequest, Ledger$BlockResponse> getGetBlocksMethod() {
        MethodDescriptor<Ledger$BlockRequest, Ledger$BlockResponse> methodDescriptor = getGetBlocksMethod;
        if (methodDescriptor == null) {
            synchronized (FogBlockAPIGrpc.class) {
                methodDescriptor = getGetBlocksMethod;
                if (methodDescriptor == null) {
                    methodDescriptor = MethodDescriptor.newBuilder().setType(MethodDescriptor.MethodType.UNARY).setFullMethodName(MethodDescriptor.generateFullMethodName("fog_ledger.FogBlockAPI", "GetBlocks")).setSampledToLocalTracing(true).setRequestMarshaller(ProtoLiteUtils.marshaller(Ledger$BlockRequest.getDefaultInstance())).setResponseMarshaller(ProtoLiteUtils.marshaller(Ledger$BlockResponse.getDefaultInstance())).build();
                    getGetBlocksMethod = methodDescriptor;
                }
            }
        }
        return methodDescriptor;
    }

    public static FogBlockAPIBlockingStub newBlockingStub(Channel channel) {
        return (FogBlockAPIBlockingStub) AbstractBlockingStub.newStub(new AbstractStub.StubFactory<FogBlockAPIBlockingStub>() { // from class: fog_ledger.FogBlockAPIGrpc.2
            @Override // io.grpc.stub.AbstractStub.StubFactory
            public FogBlockAPIBlockingStub newStub(Channel channel2, CallOptions callOptions) {
                return new FogBlockAPIBlockingStub(channel2, callOptions, null);
            }
        }, channel);
    }

    /* loaded from: classes3.dex */
    public static final class FogBlockAPIBlockingStub extends AbstractBlockingStub<FogBlockAPIBlockingStub> {
        /* synthetic */ FogBlockAPIBlockingStub(Channel channel, CallOptions callOptions, AnonymousClass1 r3) {
            this(channel, callOptions);
        }

        private FogBlockAPIBlockingStub(Channel channel, CallOptions callOptions) {
            super(channel, callOptions);
        }

        @Override // io.grpc.stub.AbstractStub
        public FogBlockAPIBlockingStub build(Channel channel, CallOptions callOptions) {
            return new FogBlockAPIBlockingStub(channel, callOptions);
        }

        public Ledger$BlockResponse getBlocks(Ledger$BlockRequest ledger$BlockRequest) {
            return (Ledger$BlockResponse) ClientCalls.blockingUnaryCall(getChannel(), FogBlockAPIGrpc.getGetBlocksMethod(), getCallOptions(), ledger$BlockRequest);
        }
    }
}
