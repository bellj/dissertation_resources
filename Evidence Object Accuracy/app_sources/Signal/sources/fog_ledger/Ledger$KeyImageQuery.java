package fog_ledger;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$KeyImage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class Ledger$KeyImageQuery extends GeneratedMessageLite<Ledger$KeyImageQuery, Builder> implements Ledger$KeyImageQueryOrBuilder {
    private static final Ledger$KeyImageQuery DEFAULT_INSTANCE;
    public static final int KEY_IMAGE_FIELD_NUMBER;
    private static volatile Parser<Ledger$KeyImageQuery> PARSER;
    public static final int START_BLOCK_FIELD_NUMBER;
    private MobileCoinAPI$KeyImage keyImage_;
    private long startBlock_;

    private Ledger$KeyImageQuery() {
    }

    public boolean hasKeyImage() {
        return this.keyImage_ != null;
    }

    public MobileCoinAPI$KeyImage getKeyImage() {
        MobileCoinAPI$KeyImage mobileCoinAPI$KeyImage = this.keyImage_;
        return mobileCoinAPI$KeyImage == null ? MobileCoinAPI$KeyImage.getDefaultInstance() : mobileCoinAPI$KeyImage;
    }

    public void setKeyImage(MobileCoinAPI$KeyImage mobileCoinAPI$KeyImage) {
        mobileCoinAPI$KeyImage.getClass();
        this.keyImage_ = mobileCoinAPI$KeyImage;
    }

    public void setKeyImage(MobileCoinAPI$KeyImage.Builder builder) {
        this.keyImage_ = builder.build();
    }

    public void mergeKeyImage(MobileCoinAPI$KeyImage mobileCoinAPI$KeyImage) {
        mobileCoinAPI$KeyImage.getClass();
        MobileCoinAPI$KeyImage mobileCoinAPI$KeyImage2 = this.keyImage_;
        if (mobileCoinAPI$KeyImage2 == null || mobileCoinAPI$KeyImage2 == MobileCoinAPI$KeyImage.getDefaultInstance()) {
            this.keyImage_ = mobileCoinAPI$KeyImage;
        } else {
            this.keyImage_ = MobileCoinAPI$KeyImage.newBuilder(this.keyImage_).mergeFrom((MobileCoinAPI$KeyImage.Builder) mobileCoinAPI$KeyImage).buildPartial();
        }
    }

    public void clearKeyImage() {
        this.keyImage_ = null;
    }

    public long getStartBlock() {
        return this.startBlock_;
    }

    public void setStartBlock(long j) {
        this.startBlock_ = j;
    }

    public void clearStartBlock() {
        this.startBlock_ = 0;
    }

    public static Ledger$KeyImageQuery parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Ledger$KeyImageQuery) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Ledger$KeyImageQuery parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$KeyImageQuery) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Ledger$KeyImageQuery parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Ledger$KeyImageQuery) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Ledger$KeyImageQuery parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$KeyImageQuery) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Ledger$KeyImageQuery parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Ledger$KeyImageQuery) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Ledger$KeyImageQuery parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$KeyImageQuery) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Ledger$KeyImageQuery parseFrom(InputStream inputStream) throws IOException {
        return (Ledger$KeyImageQuery) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ledger$KeyImageQuery parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$KeyImageQuery) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ledger$KeyImageQuery parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Ledger$KeyImageQuery) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ledger$KeyImageQuery parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$KeyImageQuery) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ledger$KeyImageQuery parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Ledger$KeyImageQuery) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Ledger$KeyImageQuery parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$KeyImageQuery) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Ledger$KeyImageQuery ledger$KeyImageQuery) {
        return DEFAULT_INSTANCE.createBuilder(ledger$KeyImageQuery);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Ledger$KeyImageQuery, Builder> implements Ledger$KeyImageQueryOrBuilder {
        /* synthetic */ Builder(Ledger$1 ledger$1) {
            this();
        }

        private Builder() {
            super(Ledger$KeyImageQuery.DEFAULT_INSTANCE);
        }

        public Builder setKeyImage(MobileCoinAPI$KeyImage mobileCoinAPI$KeyImage) {
            copyOnWrite();
            ((Ledger$KeyImageQuery) this.instance).setKeyImage(mobileCoinAPI$KeyImage);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Ledger$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Ledger$KeyImageQuery();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\t\u0002\u0005", new Object[]{"keyImage_", "startBlock_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Ledger$KeyImageQuery> parser = PARSER;
                if (parser == null) {
                    synchronized (Ledger$KeyImageQuery.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Ledger$KeyImageQuery ledger$KeyImageQuery = new Ledger$KeyImageQuery();
        DEFAULT_INSTANCE = ledger$KeyImageQuery;
        GeneratedMessageLite.registerDefaultInstance(Ledger$KeyImageQuery.class, ledger$KeyImageQuery);
    }

    public static Ledger$KeyImageQuery getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Ledger$KeyImageQuery> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
