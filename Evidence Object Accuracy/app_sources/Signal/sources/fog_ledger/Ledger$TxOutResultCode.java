package fog_ledger;

import com.google.protobuf.Internal;

/* loaded from: classes3.dex */
public enum Ledger$TxOutResultCode implements Internal.EnumLite {
    NotFound(0),
    Found(1),
    MalformedRequest(2),
    DatabaseError(3),
    UNRECOGNIZED(-1);
    
    private static final Internal.EnumLiteMap<Ledger$TxOutResultCode> internalValueMap = new Internal.EnumLiteMap<Ledger$TxOutResultCode>() { // from class: fog_ledger.Ledger$TxOutResultCode.1
        @Override // com.google.protobuf.Internal.EnumLiteMap
        public Ledger$TxOutResultCode findValueByNumber(int i) {
            return Ledger$TxOutResultCode.forNumber(i);
        }
    };
    private final int value;

    @Override // com.google.protobuf.Internal.EnumLite
    public final int getNumber() {
        if (this != UNRECOGNIZED) {
            return this.value;
        }
        throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
    }

    public static Ledger$TxOutResultCode forNumber(int i) {
        if (i == 0) {
            return NotFound;
        }
        if (i == 1) {
            return Found;
        }
        if (i == 2) {
            return MalformedRequest;
        }
        if (i != 3) {
            return null;
        }
        return DatabaseError;
    }

    Ledger$TxOutResultCode(int i) {
        this.value = i;
    }
}
