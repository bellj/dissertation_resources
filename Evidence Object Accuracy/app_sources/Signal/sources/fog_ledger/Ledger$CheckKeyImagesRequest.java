package fog_ledger;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import fog_ledger.Ledger$KeyImageQuery;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class Ledger$CheckKeyImagesRequest extends GeneratedMessageLite<Ledger$CheckKeyImagesRequest, Builder> implements MessageLiteOrBuilder {
    private static final Ledger$CheckKeyImagesRequest DEFAULT_INSTANCE;
    private static volatile Parser<Ledger$CheckKeyImagesRequest> PARSER;
    public static final int QUERIES_FIELD_NUMBER;
    private Internal.ProtobufList<Ledger$KeyImageQuery> queries_ = GeneratedMessageLite.emptyProtobufList();

    private Ledger$CheckKeyImagesRequest() {
    }

    public List<Ledger$KeyImageQuery> getQueriesList() {
        return this.queries_;
    }

    public List<? extends Ledger$KeyImageQueryOrBuilder> getQueriesOrBuilderList() {
        return this.queries_;
    }

    public int getQueriesCount() {
        return this.queries_.size();
    }

    public Ledger$KeyImageQuery getQueries(int i) {
        return this.queries_.get(i);
    }

    public Ledger$KeyImageQueryOrBuilder getQueriesOrBuilder(int i) {
        return this.queries_.get(i);
    }

    private void ensureQueriesIsMutable() {
        if (!this.queries_.isModifiable()) {
            this.queries_ = GeneratedMessageLite.mutableCopy(this.queries_);
        }
    }

    public void setQueries(int i, Ledger$KeyImageQuery ledger$KeyImageQuery) {
        ledger$KeyImageQuery.getClass();
        ensureQueriesIsMutable();
        this.queries_.set(i, ledger$KeyImageQuery);
    }

    public void setQueries(int i, Ledger$KeyImageQuery.Builder builder) {
        ensureQueriesIsMutable();
        this.queries_.set(i, builder.build());
    }

    public void addQueries(Ledger$KeyImageQuery ledger$KeyImageQuery) {
        ledger$KeyImageQuery.getClass();
        ensureQueriesIsMutable();
        this.queries_.add(ledger$KeyImageQuery);
    }

    public void addQueries(int i, Ledger$KeyImageQuery ledger$KeyImageQuery) {
        ledger$KeyImageQuery.getClass();
        ensureQueriesIsMutable();
        this.queries_.add(i, ledger$KeyImageQuery);
    }

    public void addQueries(Ledger$KeyImageQuery.Builder builder) {
        ensureQueriesIsMutable();
        this.queries_.add(builder.build());
    }

    public void addQueries(int i, Ledger$KeyImageQuery.Builder builder) {
        ensureQueriesIsMutable();
        this.queries_.add(i, builder.build());
    }

    public void addAllQueries(Iterable<? extends Ledger$KeyImageQuery> iterable) {
        ensureQueriesIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.queries_);
    }

    public void clearQueries() {
        this.queries_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeQueries(int i) {
        ensureQueriesIsMutable();
        this.queries_.remove(i);
    }

    public static Ledger$CheckKeyImagesRequest parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Ledger$CheckKeyImagesRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Ledger$CheckKeyImagesRequest parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$CheckKeyImagesRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Ledger$CheckKeyImagesRequest parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Ledger$CheckKeyImagesRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Ledger$CheckKeyImagesRequest parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$CheckKeyImagesRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Ledger$CheckKeyImagesRequest parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Ledger$CheckKeyImagesRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Ledger$CheckKeyImagesRequest parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$CheckKeyImagesRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Ledger$CheckKeyImagesRequest parseFrom(InputStream inputStream) throws IOException {
        return (Ledger$CheckKeyImagesRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ledger$CheckKeyImagesRequest parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$CheckKeyImagesRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ledger$CheckKeyImagesRequest parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Ledger$CheckKeyImagesRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ledger$CheckKeyImagesRequest parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$CheckKeyImagesRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ledger$CheckKeyImagesRequest parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Ledger$CheckKeyImagesRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Ledger$CheckKeyImagesRequest parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$CheckKeyImagesRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Ledger$CheckKeyImagesRequest ledger$CheckKeyImagesRequest) {
        return DEFAULT_INSTANCE.createBuilder(ledger$CheckKeyImagesRequest);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Ledger$CheckKeyImagesRequest, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(Ledger$1 ledger$1) {
            this();
        }

        private Builder() {
            super(Ledger$CheckKeyImagesRequest.DEFAULT_INSTANCE);
        }

        public Builder addAllQueries(Iterable<? extends Ledger$KeyImageQuery> iterable) {
            copyOnWrite();
            ((Ledger$CheckKeyImagesRequest) this.instance).addAllQueries(iterable);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Ledger$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Ledger$CheckKeyImagesRequest();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"queries_", Ledger$KeyImageQuery.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Ledger$CheckKeyImagesRequest> parser = PARSER;
                if (parser == null) {
                    synchronized (Ledger$CheckKeyImagesRequest.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Ledger$CheckKeyImagesRequest ledger$CheckKeyImagesRequest = new Ledger$CheckKeyImagesRequest();
        DEFAULT_INSTANCE = ledger$CheckKeyImagesRequest;
        GeneratedMessageLite.registerDefaultInstance(Ledger$CheckKeyImagesRequest.class, ledger$CheckKeyImagesRequest);
    }

    public static Ledger$CheckKeyImagesRequest getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Ledger$CheckKeyImagesRequest> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
