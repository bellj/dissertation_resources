package fog_ledger;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$TxOut;
import com.mobilecoin.api.MobileCoinAPI$TxOutOrBuilder;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class Ledger$Block extends GeneratedMessageLite<Ledger$Block, Builder> implements Ledger$BlockOrBuilder {
    private static final Ledger$Block DEFAULT_INSTANCE;
    public static final int GLOBAL_TXO_COUNT_FIELD_NUMBER;
    public static final int INDEX_FIELD_NUMBER;
    public static final int OUTPUTS_FIELD_NUMBER;
    private static volatile Parser<Ledger$Block> PARSER;
    public static final int TIMESTAMP_FIELD_NUMBER;
    public static final int TIMESTAMP_RESULT_CODE_FIELD_NUMBER;
    private long globalTxoCount_;
    private long index_;
    private Internal.ProtobufList<MobileCoinAPI$TxOut> outputs_ = GeneratedMessageLite.emptyProtobufList();
    private int timestampResultCode_;
    private long timestamp_;

    private Ledger$Block() {
    }

    public long getIndex() {
        return this.index_;
    }

    public void setIndex(long j) {
        this.index_ = j;
    }

    public void clearIndex() {
        this.index_ = 0;
    }

    public long getGlobalTxoCount() {
        return this.globalTxoCount_;
    }

    public void setGlobalTxoCount(long j) {
        this.globalTxoCount_ = j;
    }

    public void clearGlobalTxoCount() {
        this.globalTxoCount_ = 0;
    }

    public List<MobileCoinAPI$TxOut> getOutputsList() {
        return this.outputs_;
    }

    public List<? extends MobileCoinAPI$TxOutOrBuilder> getOutputsOrBuilderList() {
        return this.outputs_;
    }

    public int getOutputsCount() {
        return this.outputs_.size();
    }

    public MobileCoinAPI$TxOut getOutputs(int i) {
        return this.outputs_.get(i);
    }

    public MobileCoinAPI$TxOutOrBuilder getOutputsOrBuilder(int i) {
        return this.outputs_.get(i);
    }

    private void ensureOutputsIsMutable() {
        if (!this.outputs_.isModifiable()) {
            this.outputs_ = GeneratedMessageLite.mutableCopy(this.outputs_);
        }
    }

    public void setOutputs(int i, MobileCoinAPI$TxOut mobileCoinAPI$TxOut) {
        mobileCoinAPI$TxOut.getClass();
        ensureOutputsIsMutable();
        this.outputs_.set(i, mobileCoinAPI$TxOut);
    }

    public void setOutputs(int i, MobileCoinAPI$TxOut.Builder builder) {
        ensureOutputsIsMutable();
        this.outputs_.set(i, builder.build());
    }

    public void addOutputs(MobileCoinAPI$TxOut mobileCoinAPI$TxOut) {
        mobileCoinAPI$TxOut.getClass();
        ensureOutputsIsMutable();
        this.outputs_.add(mobileCoinAPI$TxOut);
    }

    public void addOutputs(int i, MobileCoinAPI$TxOut mobileCoinAPI$TxOut) {
        mobileCoinAPI$TxOut.getClass();
        ensureOutputsIsMutable();
        this.outputs_.add(i, mobileCoinAPI$TxOut);
    }

    public void addOutputs(MobileCoinAPI$TxOut.Builder builder) {
        ensureOutputsIsMutable();
        this.outputs_.add(builder.build());
    }

    public void addOutputs(int i, MobileCoinAPI$TxOut.Builder builder) {
        ensureOutputsIsMutable();
        this.outputs_.add(i, builder.build());
    }

    public void addAllOutputs(Iterable<? extends MobileCoinAPI$TxOut> iterable) {
        ensureOutputsIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.outputs_);
    }

    public void clearOutputs() {
        this.outputs_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeOutputs(int i) {
        ensureOutputsIsMutable();
        this.outputs_.remove(i);
    }

    public long getTimestamp() {
        return this.timestamp_;
    }

    public void setTimestamp(long j) {
        this.timestamp_ = j;
    }

    public void clearTimestamp() {
        this.timestamp_ = 0;
    }

    public int getTimestampResultCode() {
        return this.timestampResultCode_;
    }

    public void setTimestampResultCode(int i) {
        this.timestampResultCode_ = i;
    }

    public void clearTimestampResultCode() {
        this.timestampResultCode_ = 0;
    }

    public static Ledger$Block parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Ledger$Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Ledger$Block parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Ledger$Block parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Ledger$Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Ledger$Block parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Ledger$Block parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Ledger$Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Ledger$Block parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Ledger$Block parseFrom(InputStream inputStream) throws IOException {
        return (Ledger$Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ledger$Block parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ledger$Block parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Ledger$Block) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ledger$Block parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$Block) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ledger$Block parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Ledger$Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Ledger$Block parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$Block) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Ledger$Block ledger$Block) {
        return DEFAULT_INSTANCE.createBuilder(ledger$Block);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Ledger$Block, Builder> implements Ledger$BlockOrBuilder {
        /* synthetic */ Builder(Ledger$1 ledger$1) {
            this();
        }

        private Builder() {
            super(Ledger$Block.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Ledger$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Ledger$Block();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0005\u0000\u0000\u0001\u0005\u0005\u0000\u0001\u0000\u0001\u0003\u0002\u0003\u0003\u001b\u0004\u0003\u0005\u000b", new Object[]{"index_", "globalTxoCount_", "outputs_", MobileCoinAPI$TxOut.class, "timestamp_", "timestampResultCode_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Ledger$Block> parser = PARSER;
                if (parser == null) {
                    synchronized (Ledger$Block.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Ledger$Block ledger$Block = new Ledger$Block();
        DEFAULT_INSTANCE = ledger$Block;
        GeneratedMessageLite.registerDefaultInstance(Ledger$Block.class, ledger$Block);
    }

    public static Ledger$Block getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Ledger$Block> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
