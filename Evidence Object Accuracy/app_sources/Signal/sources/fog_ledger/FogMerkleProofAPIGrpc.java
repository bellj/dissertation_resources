package fog_ledger;

import attest.Attest$Message;
import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.MethodDescriptor;
import io.grpc.protobuf.lite.ProtoLiteUtils;
import io.grpc.stub.AbstractBlockingStub;
import io.grpc.stub.AbstractStub;
import io.grpc.stub.ClientCalls;

/* loaded from: classes3.dex */
public final class FogMerkleProofAPIGrpc {
    private static volatile MethodDescriptor<Attest$Message, Attest$Message> getGetOutputsMethod;

    /* renamed from: fog_ledger.FogMerkleProofAPIGrpc$1 */
    /* loaded from: classes3.dex */
    public class AnonymousClass1 implements AbstractStub.StubFactory<Object> {
    }

    private FogMerkleProofAPIGrpc() {
    }

    public static MethodDescriptor<Attest$Message, Attest$Message> getGetOutputsMethod() {
        MethodDescriptor<Attest$Message, Attest$Message> methodDescriptor = getGetOutputsMethod;
        if (methodDescriptor == null) {
            synchronized (FogMerkleProofAPIGrpc.class) {
                methodDescriptor = getGetOutputsMethod;
                if (methodDescriptor == null) {
                    methodDescriptor = MethodDescriptor.newBuilder().setType(MethodDescriptor.MethodType.UNARY).setFullMethodName(MethodDescriptor.generateFullMethodName("fog_ledger.FogMerkleProofAPI", "GetOutputs")).setSampledToLocalTracing(true).setRequestMarshaller(ProtoLiteUtils.marshaller(Attest$Message.getDefaultInstance())).setResponseMarshaller(ProtoLiteUtils.marshaller(Attest$Message.getDefaultInstance())).build();
                    getGetOutputsMethod = methodDescriptor;
                }
            }
        }
        return methodDescriptor;
    }

    public static FogMerkleProofAPIBlockingStub newBlockingStub(Channel channel) {
        return (FogMerkleProofAPIBlockingStub) AbstractBlockingStub.newStub(new AbstractStub.StubFactory<FogMerkleProofAPIBlockingStub>() { // from class: fog_ledger.FogMerkleProofAPIGrpc.2
            @Override // io.grpc.stub.AbstractStub.StubFactory
            public FogMerkleProofAPIBlockingStub newStub(Channel channel2, CallOptions callOptions) {
                return new FogMerkleProofAPIBlockingStub(channel2, callOptions, null);
            }
        }, channel);
    }

    /* loaded from: classes3.dex */
    public static final class FogMerkleProofAPIBlockingStub extends AbstractBlockingStub<FogMerkleProofAPIBlockingStub> {
        /* synthetic */ FogMerkleProofAPIBlockingStub(Channel channel, CallOptions callOptions, AnonymousClass1 r3) {
            this(channel, callOptions);
        }

        private FogMerkleProofAPIBlockingStub(Channel channel, CallOptions callOptions) {
            super(channel, callOptions);
        }

        @Override // io.grpc.stub.AbstractStub
        public FogMerkleProofAPIBlockingStub build(Channel channel, CallOptions callOptions) {
            return new FogMerkleProofAPIBlockingStub(channel, callOptions);
        }

        public Attest$Message getOutputs(Attest$Message attest$Message) {
            return (Attest$Message) ClientCalls.blockingUnaryCall(getChannel(), FogMerkleProofAPIGrpc.getGetOutputsMethod(), getCallOptions(), attest$Message);
        }
    }
}
