package fog_ledger;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import fog_ledger.Ledger$OutputResult;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class Ledger$GetOutputsResponse extends GeneratedMessageLite<Ledger$GetOutputsResponse, Builder> implements MessageLiteOrBuilder {
    private static final Ledger$GetOutputsResponse DEFAULT_INSTANCE;
    public static final int GLOBAL_TXO_COUNT_FIELD_NUMBER;
    public static final int NUM_BLOCKS_FIELD_NUMBER;
    private static volatile Parser<Ledger$GetOutputsResponse> PARSER;
    public static final int RESULTS_FIELD_NUMBER;
    private long globalTxoCount_;
    private long numBlocks_;
    private Internal.ProtobufList<Ledger$OutputResult> results_ = GeneratedMessageLite.emptyProtobufList();

    private Ledger$GetOutputsResponse() {
    }

    public List<Ledger$OutputResult> getResultsList() {
        return this.results_;
    }

    public List<? extends Ledger$OutputResultOrBuilder> getResultsOrBuilderList() {
        return this.results_;
    }

    public int getResultsCount() {
        return this.results_.size();
    }

    public Ledger$OutputResult getResults(int i) {
        return this.results_.get(i);
    }

    public Ledger$OutputResultOrBuilder getResultsOrBuilder(int i) {
        return this.results_.get(i);
    }

    private void ensureResultsIsMutable() {
        if (!this.results_.isModifiable()) {
            this.results_ = GeneratedMessageLite.mutableCopy(this.results_);
        }
    }

    public void setResults(int i, Ledger$OutputResult ledger$OutputResult) {
        ledger$OutputResult.getClass();
        ensureResultsIsMutable();
        this.results_.set(i, ledger$OutputResult);
    }

    public void setResults(int i, Ledger$OutputResult.Builder builder) {
        ensureResultsIsMutable();
        this.results_.set(i, builder.build());
    }

    public void addResults(Ledger$OutputResult ledger$OutputResult) {
        ledger$OutputResult.getClass();
        ensureResultsIsMutable();
        this.results_.add(ledger$OutputResult);
    }

    public void addResults(int i, Ledger$OutputResult ledger$OutputResult) {
        ledger$OutputResult.getClass();
        ensureResultsIsMutable();
        this.results_.add(i, ledger$OutputResult);
    }

    public void addResults(Ledger$OutputResult.Builder builder) {
        ensureResultsIsMutable();
        this.results_.add(builder.build());
    }

    public void addResults(int i, Ledger$OutputResult.Builder builder) {
        ensureResultsIsMutable();
        this.results_.add(i, builder.build());
    }

    public void addAllResults(Iterable<? extends Ledger$OutputResult> iterable) {
        ensureResultsIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.results_);
    }

    public void clearResults() {
        this.results_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeResults(int i) {
        ensureResultsIsMutable();
        this.results_.remove(i);
    }

    public long getNumBlocks() {
        return this.numBlocks_;
    }

    public void setNumBlocks(long j) {
        this.numBlocks_ = j;
    }

    public void clearNumBlocks() {
        this.numBlocks_ = 0;
    }

    public long getGlobalTxoCount() {
        return this.globalTxoCount_;
    }

    public void setGlobalTxoCount(long j) {
        this.globalTxoCount_ = j;
    }

    public void clearGlobalTxoCount() {
        this.globalTxoCount_ = 0;
    }

    public static Ledger$GetOutputsResponse parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Ledger$GetOutputsResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Ledger$GetOutputsResponse parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$GetOutputsResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Ledger$GetOutputsResponse parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Ledger$GetOutputsResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Ledger$GetOutputsResponse parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$GetOutputsResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Ledger$GetOutputsResponse parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Ledger$GetOutputsResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Ledger$GetOutputsResponse parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$GetOutputsResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Ledger$GetOutputsResponse parseFrom(InputStream inputStream) throws IOException {
        return (Ledger$GetOutputsResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ledger$GetOutputsResponse parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$GetOutputsResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ledger$GetOutputsResponse parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Ledger$GetOutputsResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ledger$GetOutputsResponse parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$GetOutputsResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ledger$GetOutputsResponse parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Ledger$GetOutputsResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Ledger$GetOutputsResponse parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$GetOutputsResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Ledger$GetOutputsResponse ledger$GetOutputsResponse) {
        return DEFAULT_INSTANCE.createBuilder(ledger$GetOutputsResponse);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Ledger$GetOutputsResponse, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(Ledger$1 ledger$1) {
            this();
        }

        private Builder() {
            super(Ledger$GetOutputsResponse.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Ledger$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Ledger$GetOutputsResponse();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0001\u0000\u0001\u001b\u0002\u0003\u0003\u0003", new Object[]{"results_", Ledger$OutputResult.class, "numBlocks_", "globalTxoCount_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Ledger$GetOutputsResponse> parser = PARSER;
                if (parser == null) {
                    synchronized (Ledger$GetOutputsResponse.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Ledger$GetOutputsResponse ledger$GetOutputsResponse = new Ledger$GetOutputsResponse();
        DEFAULT_INSTANCE = ledger$GetOutputsResponse;
        GeneratedMessageLite.registerDefaultInstance(Ledger$GetOutputsResponse.class, ledger$GetOutputsResponse);
    }

    public static Ledger$GetOutputsResponse getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Ledger$GetOutputsResponse> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
