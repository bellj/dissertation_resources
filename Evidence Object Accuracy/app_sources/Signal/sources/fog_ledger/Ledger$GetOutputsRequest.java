package fog_ledger;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class Ledger$GetOutputsRequest extends GeneratedMessageLite<Ledger$GetOutputsRequest, Builder> implements MessageLiteOrBuilder {
    private static final Ledger$GetOutputsRequest DEFAULT_INSTANCE;
    public static final int INDICES_FIELD_NUMBER;
    public static final int MERKLE_ROOT_BLOCK_FIELD_NUMBER;
    private static volatile Parser<Ledger$GetOutputsRequest> PARSER;
    private int indicesMemoizedSerializedSize = -1;
    private Internal.LongList indices_ = GeneratedMessageLite.emptyLongList();
    private long merkleRootBlock_;

    private Ledger$GetOutputsRequest() {
    }

    public List<Long> getIndicesList() {
        return this.indices_;
    }

    public int getIndicesCount() {
        return this.indices_.size();
    }

    public long getIndices(int i) {
        return this.indices_.getLong(i);
    }

    private void ensureIndicesIsMutable() {
        if (!this.indices_.isModifiable()) {
            this.indices_ = GeneratedMessageLite.mutableCopy(this.indices_);
        }
    }

    public void setIndices(int i, long j) {
        ensureIndicesIsMutable();
        this.indices_.setLong(i, j);
    }

    public void addIndices(long j) {
        ensureIndicesIsMutable();
        this.indices_.addLong(j);
    }

    public void addAllIndices(Iterable<? extends Long> iterable) {
        ensureIndicesIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.indices_);
    }

    public void clearIndices() {
        this.indices_ = GeneratedMessageLite.emptyLongList();
    }

    public long getMerkleRootBlock() {
        return this.merkleRootBlock_;
    }

    public void setMerkleRootBlock(long j) {
        this.merkleRootBlock_ = j;
    }

    public void clearMerkleRootBlock() {
        this.merkleRootBlock_ = 0;
    }

    public static Ledger$GetOutputsRequest parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Ledger$GetOutputsRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Ledger$GetOutputsRequest parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$GetOutputsRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Ledger$GetOutputsRequest parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Ledger$GetOutputsRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Ledger$GetOutputsRequest parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$GetOutputsRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Ledger$GetOutputsRequest parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Ledger$GetOutputsRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Ledger$GetOutputsRequest parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$GetOutputsRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Ledger$GetOutputsRequest parseFrom(InputStream inputStream) throws IOException {
        return (Ledger$GetOutputsRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ledger$GetOutputsRequest parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$GetOutputsRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ledger$GetOutputsRequest parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Ledger$GetOutputsRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ledger$GetOutputsRequest parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$GetOutputsRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ledger$GetOutputsRequest parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Ledger$GetOutputsRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Ledger$GetOutputsRequest parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$GetOutputsRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Ledger$GetOutputsRequest ledger$GetOutputsRequest) {
        return DEFAULT_INSTANCE.createBuilder(ledger$GetOutputsRequest);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Ledger$GetOutputsRequest, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(Ledger$1 ledger$1) {
            this();
        }

        private Builder() {
            super(Ledger$GetOutputsRequest.DEFAULT_INSTANCE);
        }

        public Builder addAllIndices(Iterable<? extends Long> iterable) {
            copyOnWrite();
            ((Ledger$GetOutputsRequest) this.instance).addAllIndices(iterable);
            return this;
        }

        public Builder setMerkleRootBlock(long j) {
            copyOnWrite();
            ((Ledger$GetOutputsRequest) this.instance).setMerkleRootBlock(j);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Ledger$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Ledger$GetOutputsRequest();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0001\u0000\u0001(\u0002\u0005", new Object[]{"indices_", "merkleRootBlock_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Ledger$GetOutputsRequest> parser = PARSER;
                if (parser == null) {
                    synchronized (Ledger$GetOutputsRequest.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Ledger$GetOutputsRequest ledger$GetOutputsRequest = new Ledger$GetOutputsRequest();
        DEFAULT_INSTANCE = ledger$GetOutputsRequest;
        GeneratedMessageLite.registerDefaultInstance(Ledger$GetOutputsRequest.class, ledger$GetOutputsRequest);
    }

    public static Ledger$GetOutputsRequest getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Ledger$GetOutputsRequest> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
