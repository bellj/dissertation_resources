package fog_ledger;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$CompressedRistretto;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class Ledger$TxOutResult extends GeneratedMessageLite<Ledger$TxOutResult, Builder> implements Ledger$TxOutResultOrBuilder {
    public static final int BLOCK_INDEX_FIELD_NUMBER;
    private static final Ledger$TxOutResult DEFAULT_INSTANCE;
    private static volatile Parser<Ledger$TxOutResult> PARSER;
    public static final int RESULT_CODE_FIELD_NUMBER;
    public static final int TIMESTAMP_FIELD_NUMBER;
    public static final int TIMESTAMP_RESULT_CODE_FIELD_NUMBER;
    public static final int TX_OUT_GLOBAL_INDEX_FIELD_NUMBER;
    public static final int TX_OUT_PUBKEY_FIELD_NUMBER;
    private long blockIndex_;
    private int resultCode_;
    private int timestampResultCode_;
    private long timestamp_;
    private long txOutGlobalIndex_;
    private MobileCoinAPI$CompressedRistretto txOutPubkey_;

    private Ledger$TxOutResult() {
    }

    public boolean hasTxOutPubkey() {
        return this.txOutPubkey_ != null;
    }

    public MobileCoinAPI$CompressedRistretto getTxOutPubkey() {
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto = this.txOutPubkey_;
        return mobileCoinAPI$CompressedRistretto == null ? MobileCoinAPI$CompressedRistretto.getDefaultInstance() : mobileCoinAPI$CompressedRistretto;
    }

    public void setTxOutPubkey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        this.txOutPubkey_ = mobileCoinAPI$CompressedRistretto;
    }

    public void setTxOutPubkey(MobileCoinAPI$CompressedRistretto.Builder builder) {
        this.txOutPubkey_ = builder.build();
    }

    public void mergeTxOutPubkey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto2 = this.txOutPubkey_;
        if (mobileCoinAPI$CompressedRistretto2 == null || mobileCoinAPI$CompressedRistretto2 == MobileCoinAPI$CompressedRistretto.getDefaultInstance()) {
            this.txOutPubkey_ = mobileCoinAPI$CompressedRistretto;
        } else {
            this.txOutPubkey_ = MobileCoinAPI$CompressedRistretto.newBuilder(this.txOutPubkey_).mergeFrom((MobileCoinAPI$CompressedRistretto.Builder) mobileCoinAPI$CompressedRistretto).buildPartial();
        }
    }

    public void clearTxOutPubkey() {
        this.txOutPubkey_ = null;
    }

    public int getResultCodeValue() {
        return this.resultCode_;
    }

    public Ledger$TxOutResultCode getResultCode() {
        Ledger$TxOutResultCode forNumber = Ledger$TxOutResultCode.forNumber(this.resultCode_);
        return forNumber == null ? Ledger$TxOutResultCode.UNRECOGNIZED : forNumber;
    }

    public void setResultCodeValue(int i) {
        this.resultCode_ = i;
    }

    public void setResultCode(Ledger$TxOutResultCode ledger$TxOutResultCode) {
        ledger$TxOutResultCode.getClass();
        this.resultCode_ = ledger$TxOutResultCode.getNumber();
    }

    public void clearResultCode() {
        this.resultCode_ = 0;
    }

    public long getTxOutGlobalIndex() {
        return this.txOutGlobalIndex_;
    }

    public void setTxOutGlobalIndex(long j) {
        this.txOutGlobalIndex_ = j;
    }

    public void clearTxOutGlobalIndex() {
        this.txOutGlobalIndex_ = 0;
    }

    public long getBlockIndex() {
        return this.blockIndex_;
    }

    public void setBlockIndex(long j) {
        this.blockIndex_ = j;
    }

    public void clearBlockIndex() {
        this.blockIndex_ = 0;
    }

    public long getTimestamp() {
        return this.timestamp_;
    }

    public void setTimestamp(long j) {
        this.timestamp_ = j;
    }

    public void clearTimestamp() {
        this.timestamp_ = 0;
    }

    public int getTimestampResultCode() {
        return this.timestampResultCode_;
    }

    public void setTimestampResultCode(int i) {
        this.timestampResultCode_ = i;
    }

    public void clearTimestampResultCode() {
        this.timestampResultCode_ = 0;
    }

    public static Ledger$TxOutResult parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Ledger$TxOutResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Ledger$TxOutResult parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$TxOutResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Ledger$TxOutResult parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Ledger$TxOutResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Ledger$TxOutResult parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$TxOutResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Ledger$TxOutResult parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Ledger$TxOutResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Ledger$TxOutResult parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$TxOutResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Ledger$TxOutResult parseFrom(InputStream inputStream) throws IOException {
        return (Ledger$TxOutResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ledger$TxOutResult parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$TxOutResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ledger$TxOutResult parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Ledger$TxOutResult) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ledger$TxOutResult parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$TxOutResult) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ledger$TxOutResult parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Ledger$TxOutResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Ledger$TxOutResult parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$TxOutResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Ledger$TxOutResult ledger$TxOutResult) {
        return DEFAULT_INSTANCE.createBuilder(ledger$TxOutResult);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Ledger$TxOutResult, Builder> implements Ledger$TxOutResultOrBuilder {
        /* synthetic */ Builder(Ledger$1 ledger$1) {
            this();
        }

        private Builder() {
            super(Ledger$TxOutResult.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Ledger$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Ledger$TxOutResult();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0006\u0000\u0000\u0001\u0006\u0006\u0000\u0000\u0000\u0001\t\u0002\f\u0003\u0003\u0004\u0003\u0005\u0003\u0006\u000b", new Object[]{"txOutPubkey_", "resultCode_", "txOutGlobalIndex_", "blockIndex_", "timestamp_", "timestampResultCode_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Ledger$TxOutResult> parser = PARSER;
                if (parser == null) {
                    synchronized (Ledger$TxOutResult.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Ledger$TxOutResult ledger$TxOutResult = new Ledger$TxOutResult();
        DEFAULT_INSTANCE = ledger$TxOutResult;
        GeneratedMessageLite.registerDefaultInstance(Ledger$TxOutResult.class, ledger$TxOutResult);
    }

    public static Ledger$TxOutResult getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Ledger$TxOutResult> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
