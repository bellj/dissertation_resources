package fog_ledger;

import attest.Attest$AuthMessage;
import attest.Attest$Message;
import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.MethodDescriptor;
import io.grpc.protobuf.lite.ProtoLiteUtils;
import io.grpc.stub.AbstractBlockingStub;
import io.grpc.stub.AbstractStub;
import io.grpc.stub.ClientCalls;

/* loaded from: classes3.dex */
public final class FogKeyImageAPIGrpc {
    private static volatile MethodDescriptor<Attest$AuthMessage, Attest$AuthMessage> getAuthMethod;
    private static volatile MethodDescriptor<Attest$Message, Attest$Message> getCheckKeyImagesMethod;

    /* renamed from: fog_ledger.FogKeyImageAPIGrpc$1 */
    /* loaded from: classes3.dex */
    public class AnonymousClass1 implements AbstractStub.StubFactory<Object> {
    }

    private FogKeyImageAPIGrpc() {
    }

    public static MethodDescriptor<Attest$AuthMessage, Attest$AuthMessage> getAuthMethod() {
        MethodDescriptor<Attest$AuthMessage, Attest$AuthMessage> methodDescriptor = getAuthMethod;
        if (methodDescriptor == null) {
            synchronized (FogKeyImageAPIGrpc.class) {
                methodDescriptor = getAuthMethod;
                if (methodDescriptor == null) {
                    methodDescriptor = MethodDescriptor.newBuilder().setType(MethodDescriptor.MethodType.UNARY).setFullMethodName(MethodDescriptor.generateFullMethodName("fog_ledger.FogKeyImageAPI", "Auth")).setSampledToLocalTracing(true).setRequestMarshaller(ProtoLiteUtils.marshaller(Attest$AuthMessage.getDefaultInstance())).setResponseMarshaller(ProtoLiteUtils.marshaller(Attest$AuthMessage.getDefaultInstance())).build();
                    getAuthMethod = methodDescriptor;
                }
            }
        }
        return methodDescriptor;
    }

    public static MethodDescriptor<Attest$Message, Attest$Message> getCheckKeyImagesMethod() {
        MethodDescriptor<Attest$Message, Attest$Message> methodDescriptor = getCheckKeyImagesMethod;
        if (methodDescriptor == null) {
            synchronized (FogKeyImageAPIGrpc.class) {
                methodDescriptor = getCheckKeyImagesMethod;
                if (methodDescriptor == null) {
                    methodDescriptor = MethodDescriptor.newBuilder().setType(MethodDescriptor.MethodType.UNARY).setFullMethodName(MethodDescriptor.generateFullMethodName("fog_ledger.FogKeyImageAPI", "CheckKeyImages")).setSampledToLocalTracing(true).setRequestMarshaller(ProtoLiteUtils.marshaller(Attest$Message.getDefaultInstance())).setResponseMarshaller(ProtoLiteUtils.marshaller(Attest$Message.getDefaultInstance())).build();
                    getCheckKeyImagesMethod = methodDescriptor;
                }
            }
        }
        return methodDescriptor;
    }

    public static FogKeyImageAPIBlockingStub newBlockingStub(Channel channel) {
        return (FogKeyImageAPIBlockingStub) AbstractBlockingStub.newStub(new AbstractStub.StubFactory<FogKeyImageAPIBlockingStub>() { // from class: fog_ledger.FogKeyImageAPIGrpc.2
            @Override // io.grpc.stub.AbstractStub.StubFactory
            public FogKeyImageAPIBlockingStub newStub(Channel channel2, CallOptions callOptions) {
                return new FogKeyImageAPIBlockingStub(channel2, callOptions, null);
            }
        }, channel);
    }

    /* loaded from: classes3.dex */
    public static final class FogKeyImageAPIBlockingStub extends AbstractBlockingStub<FogKeyImageAPIBlockingStub> {
        /* synthetic */ FogKeyImageAPIBlockingStub(Channel channel, CallOptions callOptions, AnonymousClass1 r3) {
            this(channel, callOptions);
        }

        private FogKeyImageAPIBlockingStub(Channel channel, CallOptions callOptions) {
            super(channel, callOptions);
        }

        @Override // io.grpc.stub.AbstractStub
        public FogKeyImageAPIBlockingStub build(Channel channel, CallOptions callOptions) {
            return new FogKeyImageAPIBlockingStub(channel, callOptions);
        }

        public Attest$AuthMessage auth(Attest$AuthMessage attest$AuthMessage) {
            return (Attest$AuthMessage) ClientCalls.blockingUnaryCall(getChannel(), FogKeyImageAPIGrpc.getAuthMethod(), getCallOptions(), attest$AuthMessage);
        }

        public Attest$Message checkKeyImages(Attest$Message attest$Message) {
            return (Attest$Message) ClientCalls.blockingUnaryCall(getChannel(), FogKeyImageAPIGrpc.getCheckKeyImagesMethod(), getCallOptions(), attest$Message);
        }
    }
}
