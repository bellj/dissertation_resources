package fog_ledger;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$TxOut;
import com.mobilecoin.api.MobileCoinAPI$TxOutMembershipProof;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class Ledger$OutputResult extends GeneratedMessageLite<Ledger$OutputResult, Builder> implements Ledger$OutputResultOrBuilder {
    private static final Ledger$OutputResult DEFAULT_INSTANCE;
    public static final int INDEX_FIELD_NUMBER;
    public static final int OUTPUT_FIELD_NUMBER;
    private static volatile Parser<Ledger$OutputResult> PARSER;
    public static final int PROOF_FIELD_NUMBER;
    public static final int RESULT_CODE_FIELD_NUMBER;
    private long index_;
    private MobileCoinAPI$TxOut output_;
    private MobileCoinAPI$TxOutMembershipProof proof_;
    private int resultCode_;

    private Ledger$OutputResult() {
    }

    public long getIndex() {
        return this.index_;
    }

    public void setIndex(long j) {
        this.index_ = j;
    }

    public void clearIndex() {
        this.index_ = 0;
    }

    public int getResultCode() {
        return this.resultCode_;
    }

    public void setResultCode(int i) {
        this.resultCode_ = i;
    }

    public void clearResultCode() {
        this.resultCode_ = 0;
    }

    public boolean hasOutput() {
        return this.output_ != null;
    }

    public MobileCoinAPI$TxOut getOutput() {
        MobileCoinAPI$TxOut mobileCoinAPI$TxOut = this.output_;
        return mobileCoinAPI$TxOut == null ? MobileCoinAPI$TxOut.getDefaultInstance() : mobileCoinAPI$TxOut;
    }

    public void setOutput(MobileCoinAPI$TxOut mobileCoinAPI$TxOut) {
        mobileCoinAPI$TxOut.getClass();
        this.output_ = mobileCoinAPI$TxOut;
    }

    public void setOutput(MobileCoinAPI$TxOut.Builder builder) {
        this.output_ = builder.build();
    }

    public void mergeOutput(MobileCoinAPI$TxOut mobileCoinAPI$TxOut) {
        mobileCoinAPI$TxOut.getClass();
        MobileCoinAPI$TxOut mobileCoinAPI$TxOut2 = this.output_;
        if (mobileCoinAPI$TxOut2 == null || mobileCoinAPI$TxOut2 == MobileCoinAPI$TxOut.getDefaultInstance()) {
            this.output_ = mobileCoinAPI$TxOut;
        } else {
            this.output_ = MobileCoinAPI$TxOut.newBuilder(this.output_).mergeFrom((MobileCoinAPI$TxOut.Builder) mobileCoinAPI$TxOut).buildPartial();
        }
    }

    public void clearOutput() {
        this.output_ = null;
    }

    public boolean hasProof() {
        return this.proof_ != null;
    }

    public MobileCoinAPI$TxOutMembershipProof getProof() {
        MobileCoinAPI$TxOutMembershipProof mobileCoinAPI$TxOutMembershipProof = this.proof_;
        return mobileCoinAPI$TxOutMembershipProof == null ? MobileCoinAPI$TxOutMembershipProof.getDefaultInstance() : mobileCoinAPI$TxOutMembershipProof;
    }

    public void setProof(MobileCoinAPI$TxOutMembershipProof mobileCoinAPI$TxOutMembershipProof) {
        mobileCoinAPI$TxOutMembershipProof.getClass();
        this.proof_ = mobileCoinAPI$TxOutMembershipProof;
    }

    public void setProof(MobileCoinAPI$TxOutMembershipProof.Builder builder) {
        this.proof_ = builder.build();
    }

    public void mergeProof(MobileCoinAPI$TxOutMembershipProof mobileCoinAPI$TxOutMembershipProof) {
        mobileCoinAPI$TxOutMembershipProof.getClass();
        MobileCoinAPI$TxOutMembershipProof mobileCoinAPI$TxOutMembershipProof2 = this.proof_;
        if (mobileCoinAPI$TxOutMembershipProof2 == null || mobileCoinAPI$TxOutMembershipProof2 == MobileCoinAPI$TxOutMembershipProof.getDefaultInstance()) {
            this.proof_ = mobileCoinAPI$TxOutMembershipProof;
        } else {
            this.proof_ = MobileCoinAPI$TxOutMembershipProof.newBuilder(this.proof_).mergeFrom((MobileCoinAPI$TxOutMembershipProof.Builder) mobileCoinAPI$TxOutMembershipProof).buildPartial();
        }
    }

    public void clearProof() {
        this.proof_ = null;
    }

    public static Ledger$OutputResult parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Ledger$OutputResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Ledger$OutputResult parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$OutputResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Ledger$OutputResult parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Ledger$OutputResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Ledger$OutputResult parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$OutputResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Ledger$OutputResult parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Ledger$OutputResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Ledger$OutputResult parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$OutputResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Ledger$OutputResult parseFrom(InputStream inputStream) throws IOException {
        return (Ledger$OutputResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ledger$OutputResult parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$OutputResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ledger$OutputResult parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Ledger$OutputResult) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ledger$OutputResult parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$OutputResult) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ledger$OutputResult parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Ledger$OutputResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Ledger$OutputResult parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$OutputResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Ledger$OutputResult ledger$OutputResult) {
        return DEFAULT_INSTANCE.createBuilder(ledger$OutputResult);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Ledger$OutputResult, Builder> implements Ledger$OutputResultOrBuilder {
        /* synthetic */ Builder(Ledger$1 ledger$1) {
            this();
        }

        private Builder() {
            super(Ledger$OutputResult.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Ledger$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Ledger$OutputResult();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0000\u0000\u0001\u0005\u0002\u0006\u0003\t\u0004\t", new Object[]{"index_", "resultCode_", "output_", "proof_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Ledger$OutputResult> parser = PARSER;
                if (parser == null) {
                    synchronized (Ledger$OutputResult.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Ledger$OutputResult ledger$OutputResult = new Ledger$OutputResult();
        DEFAULT_INSTANCE = ledger$OutputResult;
        GeneratedMessageLite.registerDefaultInstance(Ledger$OutputResult.class, ledger$OutputResult);
    }

    public static Ledger$OutputResult getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Ledger$OutputResult> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
