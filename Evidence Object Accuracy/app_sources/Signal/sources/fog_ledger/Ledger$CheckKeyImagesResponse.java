package fog_ledger;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import fog_ledger.Ledger$KeyImageResult;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class Ledger$CheckKeyImagesResponse extends GeneratedMessageLite<Ledger$CheckKeyImagesResponse, Builder> implements MessageLiteOrBuilder {
    private static final Ledger$CheckKeyImagesResponse DEFAULT_INSTANCE;
    public static final int GLOBAL_TXO_COUNT_FIELD_NUMBER;
    public static final int NUM_BLOCKS_FIELD_NUMBER;
    private static volatile Parser<Ledger$CheckKeyImagesResponse> PARSER;
    public static final int RESULTS_FIELD_NUMBER;
    private long globalTxoCount_;
    private long numBlocks_;
    private Internal.ProtobufList<Ledger$KeyImageResult> results_ = GeneratedMessageLite.emptyProtobufList();

    private Ledger$CheckKeyImagesResponse() {
    }

    public long getNumBlocks() {
        return this.numBlocks_;
    }

    public void setNumBlocks(long j) {
        this.numBlocks_ = j;
    }

    public void clearNumBlocks() {
        this.numBlocks_ = 0;
    }

    public long getGlobalTxoCount() {
        return this.globalTxoCount_;
    }

    public void setGlobalTxoCount(long j) {
        this.globalTxoCount_ = j;
    }

    public void clearGlobalTxoCount() {
        this.globalTxoCount_ = 0;
    }

    public List<Ledger$KeyImageResult> getResultsList() {
        return this.results_;
    }

    public List<? extends Ledger$KeyImageResultOrBuilder> getResultsOrBuilderList() {
        return this.results_;
    }

    public int getResultsCount() {
        return this.results_.size();
    }

    public Ledger$KeyImageResult getResults(int i) {
        return this.results_.get(i);
    }

    public Ledger$KeyImageResultOrBuilder getResultsOrBuilder(int i) {
        return this.results_.get(i);
    }

    private void ensureResultsIsMutable() {
        if (!this.results_.isModifiable()) {
            this.results_ = GeneratedMessageLite.mutableCopy(this.results_);
        }
    }

    public void setResults(int i, Ledger$KeyImageResult ledger$KeyImageResult) {
        ledger$KeyImageResult.getClass();
        ensureResultsIsMutable();
        this.results_.set(i, ledger$KeyImageResult);
    }

    public void setResults(int i, Ledger$KeyImageResult.Builder builder) {
        ensureResultsIsMutable();
        this.results_.set(i, builder.build());
    }

    public void addResults(Ledger$KeyImageResult ledger$KeyImageResult) {
        ledger$KeyImageResult.getClass();
        ensureResultsIsMutable();
        this.results_.add(ledger$KeyImageResult);
    }

    public void addResults(int i, Ledger$KeyImageResult ledger$KeyImageResult) {
        ledger$KeyImageResult.getClass();
        ensureResultsIsMutable();
        this.results_.add(i, ledger$KeyImageResult);
    }

    public void addResults(Ledger$KeyImageResult.Builder builder) {
        ensureResultsIsMutable();
        this.results_.add(builder.build());
    }

    public void addResults(int i, Ledger$KeyImageResult.Builder builder) {
        ensureResultsIsMutable();
        this.results_.add(i, builder.build());
    }

    public void addAllResults(Iterable<? extends Ledger$KeyImageResult> iterable) {
        ensureResultsIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.results_);
    }

    public void clearResults() {
        this.results_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeResults(int i) {
        ensureResultsIsMutable();
        this.results_.remove(i);
    }

    public static Ledger$CheckKeyImagesResponse parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Ledger$CheckKeyImagesResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Ledger$CheckKeyImagesResponse parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$CheckKeyImagesResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Ledger$CheckKeyImagesResponse parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Ledger$CheckKeyImagesResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Ledger$CheckKeyImagesResponse parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$CheckKeyImagesResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Ledger$CheckKeyImagesResponse parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Ledger$CheckKeyImagesResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Ledger$CheckKeyImagesResponse parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$CheckKeyImagesResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Ledger$CheckKeyImagesResponse parseFrom(InputStream inputStream) throws IOException {
        return (Ledger$CheckKeyImagesResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ledger$CheckKeyImagesResponse parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$CheckKeyImagesResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ledger$CheckKeyImagesResponse parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Ledger$CheckKeyImagesResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ledger$CheckKeyImagesResponse parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$CheckKeyImagesResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ledger$CheckKeyImagesResponse parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Ledger$CheckKeyImagesResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Ledger$CheckKeyImagesResponse parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$CheckKeyImagesResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Ledger$CheckKeyImagesResponse ledger$CheckKeyImagesResponse) {
        return DEFAULT_INSTANCE.createBuilder(ledger$CheckKeyImagesResponse);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Ledger$CheckKeyImagesResponse, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(Ledger$1 ledger$1) {
            this();
        }

        private Builder() {
            super(Ledger$CheckKeyImagesResponse.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Ledger$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Ledger$CheckKeyImagesResponse();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0001\u0000\u0001\u0003\u0002\u0003\u0003\u001b", new Object[]{"numBlocks_", "globalTxoCount_", "results_", Ledger$KeyImageResult.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Ledger$CheckKeyImagesResponse> parser = PARSER;
                if (parser == null) {
                    synchronized (Ledger$CheckKeyImagesResponse.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Ledger$CheckKeyImagesResponse ledger$CheckKeyImagesResponse = new Ledger$CheckKeyImagesResponse();
        DEFAULT_INSTANCE = ledger$CheckKeyImagesResponse;
        GeneratedMessageLite.registerDefaultInstance(Ledger$CheckKeyImagesResponse.class, ledger$CheckKeyImagesResponse);
    }

    public static Ledger$CheckKeyImagesResponse getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Ledger$CheckKeyImagesResponse> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
