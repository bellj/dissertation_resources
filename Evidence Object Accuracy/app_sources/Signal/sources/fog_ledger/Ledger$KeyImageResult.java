package fog_ledger;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$KeyImage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class Ledger$KeyImageResult extends GeneratedMessageLite<Ledger$KeyImageResult, Builder> implements Ledger$KeyImageResultOrBuilder {
    private static final Ledger$KeyImageResult DEFAULT_INSTANCE;
    public static final int KEY_IMAGE_FIELD_NUMBER;
    public static final int KEY_IMAGE_RESULT_CODE_FIELD_NUMBER;
    private static volatile Parser<Ledger$KeyImageResult> PARSER;
    public static final int SPENT_AT_FIELD_NUMBER;
    public static final int TIMESTAMP_FIELD_NUMBER;
    public static final int TIMESTAMP_RESULT_CODE_FIELD_NUMBER;
    private int keyImageResultCode_;
    private MobileCoinAPI$KeyImage keyImage_;
    private long spentAt_;
    private int timestampResultCode_;
    private long timestamp_;

    private Ledger$KeyImageResult() {
    }

    public boolean hasKeyImage() {
        return this.keyImage_ != null;
    }

    public MobileCoinAPI$KeyImage getKeyImage() {
        MobileCoinAPI$KeyImage mobileCoinAPI$KeyImage = this.keyImage_;
        return mobileCoinAPI$KeyImage == null ? MobileCoinAPI$KeyImage.getDefaultInstance() : mobileCoinAPI$KeyImage;
    }

    public void setKeyImage(MobileCoinAPI$KeyImage mobileCoinAPI$KeyImage) {
        mobileCoinAPI$KeyImage.getClass();
        this.keyImage_ = mobileCoinAPI$KeyImage;
    }

    public void setKeyImage(MobileCoinAPI$KeyImage.Builder builder) {
        this.keyImage_ = builder.build();
    }

    public void mergeKeyImage(MobileCoinAPI$KeyImage mobileCoinAPI$KeyImage) {
        mobileCoinAPI$KeyImage.getClass();
        MobileCoinAPI$KeyImage mobileCoinAPI$KeyImage2 = this.keyImage_;
        if (mobileCoinAPI$KeyImage2 == null || mobileCoinAPI$KeyImage2 == MobileCoinAPI$KeyImage.getDefaultInstance()) {
            this.keyImage_ = mobileCoinAPI$KeyImage;
        } else {
            this.keyImage_ = MobileCoinAPI$KeyImage.newBuilder(this.keyImage_).mergeFrom((MobileCoinAPI$KeyImage.Builder) mobileCoinAPI$KeyImage).buildPartial();
        }
    }

    public void clearKeyImage() {
        this.keyImage_ = null;
    }

    public long getSpentAt() {
        return this.spentAt_;
    }

    public void setSpentAt(long j) {
        this.spentAt_ = j;
    }

    public void clearSpentAt() {
        this.spentAt_ = 0;
    }

    public long getTimestamp() {
        return this.timestamp_;
    }

    public void setTimestamp(long j) {
        this.timestamp_ = j;
    }

    public void clearTimestamp() {
        this.timestamp_ = 0;
    }

    public int getTimestampResultCode() {
        return this.timestampResultCode_;
    }

    public void setTimestampResultCode(int i) {
        this.timestampResultCode_ = i;
    }

    public void clearTimestampResultCode() {
        this.timestampResultCode_ = 0;
    }

    public int getKeyImageResultCode() {
        return this.keyImageResultCode_;
    }

    public void setKeyImageResultCode(int i) {
        this.keyImageResultCode_ = i;
    }

    public void clearKeyImageResultCode() {
        this.keyImageResultCode_ = 0;
    }

    public static Ledger$KeyImageResult parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Ledger$KeyImageResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Ledger$KeyImageResult parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$KeyImageResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Ledger$KeyImageResult parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Ledger$KeyImageResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Ledger$KeyImageResult parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$KeyImageResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Ledger$KeyImageResult parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Ledger$KeyImageResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Ledger$KeyImageResult parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$KeyImageResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Ledger$KeyImageResult parseFrom(InputStream inputStream) throws IOException {
        return (Ledger$KeyImageResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ledger$KeyImageResult parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$KeyImageResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ledger$KeyImageResult parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Ledger$KeyImageResult) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ledger$KeyImageResult parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$KeyImageResult) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ledger$KeyImageResult parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Ledger$KeyImageResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Ledger$KeyImageResult parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$KeyImageResult) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Ledger$KeyImageResult ledger$KeyImageResult) {
        return DEFAULT_INSTANCE.createBuilder(ledger$KeyImageResult);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Ledger$KeyImageResult, Builder> implements Ledger$KeyImageResultOrBuilder {
        /* synthetic */ Builder(Ledger$1 ledger$1) {
            this();
        }

        private Builder() {
            super(Ledger$KeyImageResult.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Ledger$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Ledger$KeyImageResult();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0005\u0000\u0000\u0001\u0005\u0005\u0000\u0000\u0000\u0001\t\u0002\u0005\u0003\u0005\u0004\u0006\u0005\u0006", new Object[]{"keyImage_", "spentAt_", "timestamp_", "timestampResultCode_", "keyImageResultCode_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Ledger$KeyImageResult> parser = PARSER;
                if (parser == null) {
                    synchronized (Ledger$KeyImageResult.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Ledger$KeyImageResult ledger$KeyImageResult = new Ledger$KeyImageResult();
        DEFAULT_INSTANCE = ledger$KeyImageResult;
        GeneratedMessageLite.registerDefaultInstance(Ledger$KeyImageResult.class, ledger$KeyImageResult);
    }

    public static Ledger$KeyImageResult getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Ledger$KeyImageResult> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
