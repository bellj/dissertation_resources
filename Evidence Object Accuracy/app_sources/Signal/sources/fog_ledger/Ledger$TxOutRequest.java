package fog_ledger;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$CompressedRistretto;
import com.mobilecoin.api.MobileCoinAPI$CompressedRistrettoOrBuilder;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class Ledger$TxOutRequest extends GeneratedMessageLite<Ledger$TxOutRequest, Builder> implements MessageLiteOrBuilder {
    private static final Ledger$TxOutRequest DEFAULT_INSTANCE;
    private static volatile Parser<Ledger$TxOutRequest> PARSER;
    public static final int TX_OUT_PUBKEYS_FIELD_NUMBER;
    private Internal.ProtobufList<MobileCoinAPI$CompressedRistretto> txOutPubkeys_ = GeneratedMessageLite.emptyProtobufList();

    private Ledger$TxOutRequest() {
    }

    public List<MobileCoinAPI$CompressedRistretto> getTxOutPubkeysList() {
        return this.txOutPubkeys_;
    }

    public List<? extends MobileCoinAPI$CompressedRistrettoOrBuilder> getTxOutPubkeysOrBuilderList() {
        return this.txOutPubkeys_;
    }

    public int getTxOutPubkeysCount() {
        return this.txOutPubkeys_.size();
    }

    public MobileCoinAPI$CompressedRistretto getTxOutPubkeys(int i) {
        return this.txOutPubkeys_.get(i);
    }

    public MobileCoinAPI$CompressedRistrettoOrBuilder getTxOutPubkeysOrBuilder(int i) {
        return this.txOutPubkeys_.get(i);
    }

    private void ensureTxOutPubkeysIsMutable() {
        if (!this.txOutPubkeys_.isModifiable()) {
            this.txOutPubkeys_ = GeneratedMessageLite.mutableCopy(this.txOutPubkeys_);
        }
    }

    public void setTxOutPubkeys(int i, MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        ensureTxOutPubkeysIsMutable();
        this.txOutPubkeys_.set(i, mobileCoinAPI$CompressedRistretto);
    }

    public void setTxOutPubkeys(int i, MobileCoinAPI$CompressedRistretto.Builder builder) {
        ensureTxOutPubkeysIsMutable();
        this.txOutPubkeys_.set(i, builder.build());
    }

    public void addTxOutPubkeys(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        ensureTxOutPubkeysIsMutable();
        this.txOutPubkeys_.add(mobileCoinAPI$CompressedRistretto);
    }

    public void addTxOutPubkeys(int i, MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        ensureTxOutPubkeysIsMutable();
        this.txOutPubkeys_.add(i, mobileCoinAPI$CompressedRistretto);
    }

    public void addTxOutPubkeys(MobileCoinAPI$CompressedRistretto.Builder builder) {
        ensureTxOutPubkeysIsMutable();
        this.txOutPubkeys_.add(builder.build());
    }

    public void addTxOutPubkeys(int i, MobileCoinAPI$CompressedRistretto.Builder builder) {
        ensureTxOutPubkeysIsMutable();
        this.txOutPubkeys_.add(i, builder.build());
    }

    public void addAllTxOutPubkeys(Iterable<? extends MobileCoinAPI$CompressedRistretto> iterable) {
        ensureTxOutPubkeysIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.txOutPubkeys_);
    }

    public void clearTxOutPubkeys() {
        this.txOutPubkeys_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeTxOutPubkeys(int i) {
        ensureTxOutPubkeysIsMutable();
        this.txOutPubkeys_.remove(i);
    }

    public static Ledger$TxOutRequest parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Ledger$TxOutRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Ledger$TxOutRequest parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$TxOutRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Ledger$TxOutRequest parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Ledger$TxOutRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Ledger$TxOutRequest parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$TxOutRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Ledger$TxOutRequest parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Ledger$TxOutRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Ledger$TxOutRequest parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Ledger$TxOutRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Ledger$TxOutRequest parseFrom(InputStream inputStream) throws IOException {
        return (Ledger$TxOutRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ledger$TxOutRequest parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$TxOutRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ledger$TxOutRequest parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Ledger$TxOutRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Ledger$TxOutRequest parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$TxOutRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Ledger$TxOutRequest parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Ledger$TxOutRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Ledger$TxOutRequest parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Ledger$TxOutRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Ledger$TxOutRequest ledger$TxOutRequest) {
        return DEFAULT_INSTANCE.createBuilder(ledger$TxOutRequest);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Ledger$TxOutRequest, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(Ledger$1 ledger$1) {
            this();
        }

        private Builder() {
            super(Ledger$TxOutRequest.DEFAULT_INSTANCE);
        }

        public Builder addTxOutPubkeys(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
            copyOnWrite();
            ((Ledger$TxOutRequest) this.instance).addTxOutPubkeys(mobileCoinAPI$CompressedRistretto);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Ledger$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Ledger$TxOutRequest();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"txOutPubkeys_", MobileCoinAPI$CompressedRistretto.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Ledger$TxOutRequest> parser = PARSER;
                if (parser == null) {
                    synchronized (Ledger$TxOutRequest.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Ledger$TxOutRequest ledger$TxOutRequest = new Ledger$TxOutRequest();
        DEFAULT_INSTANCE = ledger$TxOutRequest;
        GeneratedMessageLite.registerDefaultInstance(Ledger$TxOutRequest.class, ledger$TxOutRequest);
    }

    public static Ledger$TxOutRequest getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Ledger$TxOutRequest> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
