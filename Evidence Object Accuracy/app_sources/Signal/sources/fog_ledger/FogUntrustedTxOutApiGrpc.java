package fog_ledger;

import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.MethodDescriptor;
import io.grpc.protobuf.lite.ProtoLiteUtils;
import io.grpc.stub.AbstractBlockingStub;
import io.grpc.stub.AbstractStub;
import io.grpc.stub.ClientCalls;

/* loaded from: classes3.dex */
public final class FogUntrustedTxOutApiGrpc {
    private static volatile MethodDescriptor<Ledger$TxOutRequest, Ledger$TxOutResponse> getGetTxOutsMethod;

    /* renamed from: fog_ledger.FogUntrustedTxOutApiGrpc$1 */
    /* loaded from: classes3.dex */
    public class AnonymousClass1 implements AbstractStub.StubFactory<Object> {
    }

    private FogUntrustedTxOutApiGrpc() {
    }

    public static MethodDescriptor<Ledger$TxOutRequest, Ledger$TxOutResponse> getGetTxOutsMethod() {
        MethodDescriptor<Ledger$TxOutRequest, Ledger$TxOutResponse> methodDescriptor = getGetTxOutsMethod;
        if (methodDescriptor == null) {
            synchronized (FogUntrustedTxOutApiGrpc.class) {
                methodDescriptor = getGetTxOutsMethod;
                if (methodDescriptor == null) {
                    methodDescriptor = MethodDescriptor.newBuilder().setType(MethodDescriptor.MethodType.UNARY).setFullMethodName(MethodDescriptor.generateFullMethodName("fog_ledger.FogUntrustedTxOutApi", "GetTxOuts")).setSampledToLocalTracing(true).setRequestMarshaller(ProtoLiteUtils.marshaller(Ledger$TxOutRequest.getDefaultInstance())).setResponseMarshaller(ProtoLiteUtils.marshaller(Ledger$TxOutResponse.getDefaultInstance())).build();
                    getGetTxOutsMethod = methodDescriptor;
                }
            }
        }
        return methodDescriptor;
    }

    public static FogUntrustedTxOutApiBlockingStub newBlockingStub(Channel channel) {
        return (FogUntrustedTxOutApiBlockingStub) AbstractBlockingStub.newStub(new AbstractStub.StubFactory<FogUntrustedTxOutApiBlockingStub>() { // from class: fog_ledger.FogUntrustedTxOutApiGrpc.2
            @Override // io.grpc.stub.AbstractStub.StubFactory
            public FogUntrustedTxOutApiBlockingStub newStub(Channel channel2, CallOptions callOptions) {
                return new FogUntrustedTxOutApiBlockingStub(channel2, callOptions, null);
            }
        }, channel);
    }

    /* loaded from: classes3.dex */
    public static final class FogUntrustedTxOutApiBlockingStub extends AbstractBlockingStub<FogUntrustedTxOutApiBlockingStub> {
        /* synthetic */ FogUntrustedTxOutApiBlockingStub(Channel channel, CallOptions callOptions, AnonymousClass1 r3) {
            this(channel, callOptions);
        }

        private FogUntrustedTxOutApiBlockingStub(Channel channel, CallOptions callOptions) {
            super(channel, callOptions);
        }

        @Override // io.grpc.stub.AbstractStub
        public FogUntrustedTxOutApiBlockingStub build(Channel channel, CallOptions callOptions) {
            return new FogUntrustedTxOutApiBlockingStub(channel, callOptions);
        }

        public Ledger$TxOutResponse getTxOuts(Ledger$TxOutRequest ledger$TxOutRequest) {
            return (Ledger$TxOutResponse) ClientCalls.blockingUnaryCall(getChannel(), FogUntrustedTxOutApiGrpc.getGetTxOutsMethod(), getCallOptions(), ledger$TxOutRequest);
        }
    }
}
