package attest;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes.dex */
public final class Attest$AuthMessage extends GeneratedMessageLite<Attest$AuthMessage, Builder> implements MessageLiteOrBuilder {
    public static final int DATA_FIELD_NUMBER;
    private static final Attest$AuthMessage DEFAULT_INSTANCE;
    private static volatile Parser<Attest$AuthMessage> PARSER;
    private ByteString data_ = ByteString.EMPTY;

    private Attest$AuthMessage() {
    }

    public ByteString getData() {
        return this.data_;
    }

    public void setData(ByteString byteString) {
        byteString.getClass();
        this.data_ = byteString;
    }

    public void clearData() {
        this.data_ = getDefaultInstance().getData();
    }

    public static Attest$AuthMessage parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Attest$AuthMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Attest$AuthMessage parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Attest$AuthMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Attest$AuthMessage parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Attest$AuthMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Attest$AuthMessage parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Attest$AuthMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Attest$AuthMessage parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Attest$AuthMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Attest$AuthMessage parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Attest$AuthMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Attest$AuthMessage parseFrom(InputStream inputStream) throws IOException {
        return (Attest$AuthMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Attest$AuthMessage parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Attest$AuthMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Attest$AuthMessage parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Attest$AuthMessage) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Attest$AuthMessage parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Attest$AuthMessage) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Attest$AuthMessage parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Attest$AuthMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Attest$AuthMessage parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Attest$AuthMessage) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Attest$AuthMessage attest$AuthMessage) {
        return DEFAULT_INSTANCE.createBuilder(attest$AuthMessage);
    }

    /* loaded from: classes.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Attest$AuthMessage, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(Attest$1 attest$1) {
            this();
        }

        private Builder() {
            super(Attest$AuthMessage.DEFAULT_INSTANCE);
        }

        public Builder setData(ByteString byteString) {
            copyOnWrite();
            ((Attest$AuthMessage) this.instance).setData(byteString);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Attest$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Attest$AuthMessage();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\n", new Object[]{"data_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Attest$AuthMessage> parser = PARSER;
                if (parser == null) {
                    synchronized (Attest$AuthMessage.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Attest$AuthMessage attest$AuthMessage = new Attest$AuthMessage();
        DEFAULT_INSTANCE = attest$AuthMessage;
        GeneratedMessageLite.registerDefaultInstance(Attest$AuthMessage.class, attest$AuthMessage);
    }

    public static Attest$AuthMessage getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Attest$AuthMessage> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
