package attest;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes.dex */
public final class Attest$Message extends GeneratedMessageLite<Attest$Message, Builder> implements MessageLiteOrBuilder {
    public static final int AAD_FIELD_NUMBER;
    public static final int CHANNEL_ID_FIELD_NUMBER;
    public static final int DATA_FIELD_NUMBER;
    private static final Attest$Message DEFAULT_INSTANCE;
    private static volatile Parser<Attest$Message> PARSER;
    private ByteString aad_;
    private ByteString channelId_;
    private ByteString data_;

    private Attest$Message() {
        ByteString byteString = ByteString.EMPTY;
        this.aad_ = byteString;
        this.channelId_ = byteString;
        this.data_ = byteString;
    }

    public ByteString getAad() {
        return this.aad_;
    }

    public void setAad(ByteString byteString) {
        byteString.getClass();
        this.aad_ = byteString;
    }

    public void clearAad() {
        this.aad_ = getDefaultInstance().getAad();
    }

    public ByteString getChannelId() {
        return this.channelId_;
    }

    public void setChannelId(ByteString byteString) {
        byteString.getClass();
        this.channelId_ = byteString;
    }

    public void clearChannelId() {
        this.channelId_ = getDefaultInstance().getChannelId();
    }

    public ByteString getData() {
        return this.data_;
    }

    public void setData(ByteString byteString) {
        byteString.getClass();
        this.data_ = byteString;
    }

    public void clearData() {
        this.data_ = getDefaultInstance().getData();
    }

    public static Attest$Message parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Attest$Message) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Attest$Message parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Attest$Message) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Attest$Message parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Attest$Message) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Attest$Message parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Attest$Message) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Attest$Message parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Attest$Message) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Attest$Message parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Attest$Message) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Attest$Message parseFrom(InputStream inputStream) throws IOException {
        return (Attest$Message) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Attest$Message parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Attest$Message) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Attest$Message parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Attest$Message) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Attest$Message parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Attest$Message) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Attest$Message parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Attest$Message) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Attest$Message parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Attest$Message) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Attest$Message attest$Message) {
        return DEFAULT_INSTANCE.createBuilder(attest$Message);
    }

    /* loaded from: classes.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Attest$Message, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(Attest$1 attest$1) {
            this();
        }

        private Builder() {
            super(Attest$Message.DEFAULT_INSTANCE);
        }

        public Builder setAad(ByteString byteString) {
            copyOnWrite();
            ((Attest$Message) this.instance).setAad(byteString);
            return this;
        }

        public Builder setChannelId(ByteString byteString) {
            copyOnWrite();
            ((Attest$Message) this.instance).setChannelId(byteString);
            return this;
        }

        public Builder setData(ByteString byteString) {
            copyOnWrite();
            ((Attest$Message) this.instance).setData(byteString);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Attest$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Attest$Message();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\n\u0002\n\u0003\n", new Object[]{"aad_", "channelId_", "data_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Attest$Message> parser = PARSER;
                if (parser == null) {
                    synchronized (Attest$Message.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Attest$Message attest$Message = new Attest$Message();
        DEFAULT_INSTANCE = attest$Message;
        GeneratedMessageLite.registerDefaultInstance(Attest$Message.class, attest$Message);
    }

    public static Attest$Message getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Attest$Message> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
