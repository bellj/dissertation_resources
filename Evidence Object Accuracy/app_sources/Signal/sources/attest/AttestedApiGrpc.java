package attest;

import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.MethodDescriptor;
import io.grpc.protobuf.lite.ProtoLiteUtils;
import io.grpc.stub.AbstractBlockingStub;
import io.grpc.stub.AbstractStub;
import io.grpc.stub.ClientCalls;

/* loaded from: classes.dex */
public final class AttestedApiGrpc {
    private static volatile MethodDescriptor<Attest$AuthMessage, Attest$AuthMessage> getAuthMethod;

    /* renamed from: attest.AttestedApiGrpc$1 */
    /* loaded from: classes.dex */
    public class AnonymousClass1 implements AbstractStub.StubFactory<Object> {
    }

    private AttestedApiGrpc() {
    }

    public static MethodDescriptor<Attest$AuthMessage, Attest$AuthMessage> getAuthMethod() {
        MethodDescriptor<Attest$AuthMessage, Attest$AuthMessage> methodDescriptor = getAuthMethod;
        if (methodDescriptor == null) {
            synchronized (AttestedApiGrpc.class) {
                methodDescriptor = getAuthMethod;
                if (methodDescriptor == null) {
                    methodDescriptor = MethodDescriptor.newBuilder().setType(MethodDescriptor.MethodType.UNARY).setFullMethodName(MethodDescriptor.generateFullMethodName("attest.AttestedApi", "Auth")).setSampledToLocalTracing(true).setRequestMarshaller(ProtoLiteUtils.marshaller(Attest$AuthMessage.getDefaultInstance())).setResponseMarshaller(ProtoLiteUtils.marshaller(Attest$AuthMessage.getDefaultInstance())).build();
                    getAuthMethod = methodDescriptor;
                }
            }
        }
        return methodDescriptor;
    }

    public static AttestedApiBlockingStub newBlockingStub(Channel channel) {
        return (AttestedApiBlockingStub) AbstractBlockingStub.newStub(new AbstractStub.StubFactory<AttestedApiBlockingStub>() { // from class: attest.AttestedApiGrpc.2
            @Override // io.grpc.stub.AbstractStub.StubFactory
            public AttestedApiBlockingStub newStub(Channel channel2, CallOptions callOptions) {
                return new AttestedApiBlockingStub(channel2, callOptions, null);
            }
        }, channel);
    }

    /* loaded from: classes.dex */
    public static final class AttestedApiBlockingStub extends AbstractBlockingStub<AttestedApiBlockingStub> {
        /* synthetic */ AttestedApiBlockingStub(Channel channel, CallOptions callOptions, AnonymousClass1 r3) {
            this(channel, callOptions);
        }

        private AttestedApiBlockingStub(Channel channel, CallOptions callOptions) {
            super(channel, callOptions);
        }

        @Override // io.grpc.stub.AbstractStub
        public AttestedApiBlockingStub build(Channel channel, CallOptions callOptions) {
            return new AttestedApiBlockingStub(channel, callOptions);
        }

        public Attest$AuthMessage auth(Attest$AuthMessage attest$AuthMessage) {
            return (Attest$AuthMessage) ClientCalls.blockingUnaryCall(getChannel(), AttestedApiGrpc.getAuthMethod(), getCallOptions(), attest$AuthMessage);
        }
    }
}
