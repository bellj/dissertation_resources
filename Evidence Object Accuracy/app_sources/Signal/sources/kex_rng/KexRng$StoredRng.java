package kex_rng;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class KexRng$StoredRng extends GeneratedMessageLite<KexRng$StoredRng, Builder> implements MessageLiteOrBuilder {
    public static final int BUFFER_FIELD_NUMBER;
    public static final int COUNTER_FIELD_NUMBER;
    private static final KexRng$StoredRng DEFAULT_INSTANCE;
    private static volatile Parser<KexRng$StoredRng> PARSER;
    public static final int SECRET_FIELD_NUMBER;
    public static final int VERSION_FIELD_NUMBER;
    private ByteString buffer_;
    private long counter_;
    private ByteString secret_;
    private int version_;

    private KexRng$StoredRng() {
        ByteString byteString = ByteString.EMPTY;
        this.secret_ = byteString;
        this.buffer_ = byteString;
    }

    public ByteString getSecret() {
        return this.secret_;
    }

    public void setSecret(ByteString byteString) {
        byteString.getClass();
        this.secret_ = byteString;
    }

    public void clearSecret() {
        this.secret_ = getDefaultInstance().getSecret();
    }

    public ByteString getBuffer() {
        return this.buffer_;
    }

    public void setBuffer(ByteString byteString) {
        byteString.getClass();
        this.buffer_ = byteString;
    }

    public void clearBuffer() {
        this.buffer_ = getDefaultInstance().getBuffer();
    }

    public long getCounter() {
        return this.counter_;
    }

    public void setCounter(long j) {
        this.counter_ = j;
    }

    public void clearCounter() {
        this.counter_ = 0;
    }

    public int getVersion() {
        return this.version_;
    }

    public void setVersion(int i) {
        this.version_ = i;
    }

    public void clearVersion() {
        this.version_ = 0;
    }

    public static KexRng$StoredRng parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (KexRng$StoredRng) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static KexRng$StoredRng parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (KexRng$StoredRng) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static KexRng$StoredRng parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (KexRng$StoredRng) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static KexRng$StoredRng parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (KexRng$StoredRng) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static KexRng$StoredRng parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (KexRng$StoredRng) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static KexRng$StoredRng parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (KexRng$StoredRng) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static KexRng$StoredRng parseFrom(InputStream inputStream) throws IOException {
        return (KexRng$StoredRng) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static KexRng$StoredRng parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (KexRng$StoredRng) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static KexRng$StoredRng parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (KexRng$StoredRng) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static KexRng$StoredRng parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (KexRng$StoredRng) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static KexRng$StoredRng parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (KexRng$StoredRng) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static KexRng$StoredRng parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (KexRng$StoredRng) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(KexRng$StoredRng kexRng$StoredRng) {
        return DEFAULT_INSTANCE.createBuilder(kexRng$StoredRng);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<KexRng$StoredRng, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(KexRng$1 kexRng$1) {
            this();
        }

        private Builder() {
            super(KexRng$StoredRng.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (KexRng$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new KexRng$StoredRng();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0000\u0000\u0001\n\u0002\n\u0003\u0003\u0004\u000b", new Object[]{"secret_", "buffer_", "counter_", "version_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<KexRng$StoredRng> parser = PARSER;
                if (parser == null) {
                    synchronized (KexRng$StoredRng.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        KexRng$StoredRng kexRng$StoredRng = new KexRng$StoredRng();
        DEFAULT_INSTANCE = kexRng$StoredRng;
        GeneratedMessageLite.registerDefaultInstance(KexRng$StoredRng.class, kexRng$StoredRng);
    }

    public static KexRng$StoredRng getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<KexRng$StoredRng> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
