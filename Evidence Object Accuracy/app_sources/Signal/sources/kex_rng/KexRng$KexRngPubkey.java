package kex_rng;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class KexRng$KexRngPubkey extends GeneratedMessageLite<KexRng$KexRngPubkey, Builder> implements MessageLiteOrBuilder {
    private static final KexRng$KexRngPubkey DEFAULT_INSTANCE;
    private static volatile Parser<KexRng$KexRngPubkey> PARSER;
    public static final int PUBKEY_FIELD_NUMBER;
    public static final int VERSION_FIELD_NUMBER;
    private ByteString pubkey_ = ByteString.EMPTY;
    private int version_;

    private KexRng$KexRngPubkey() {
    }

    public ByteString getPubkey() {
        return this.pubkey_;
    }

    public void setPubkey(ByteString byteString) {
        byteString.getClass();
        this.pubkey_ = byteString;
    }

    public void clearPubkey() {
        this.pubkey_ = getDefaultInstance().getPubkey();
    }

    public int getVersion() {
        return this.version_;
    }

    public void setVersion(int i) {
        this.version_ = i;
    }

    public void clearVersion() {
        this.version_ = 0;
    }

    public static KexRng$KexRngPubkey parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (KexRng$KexRngPubkey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static KexRng$KexRngPubkey parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (KexRng$KexRngPubkey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static KexRng$KexRngPubkey parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (KexRng$KexRngPubkey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static KexRng$KexRngPubkey parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (KexRng$KexRngPubkey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static KexRng$KexRngPubkey parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (KexRng$KexRngPubkey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static KexRng$KexRngPubkey parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (KexRng$KexRngPubkey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static KexRng$KexRngPubkey parseFrom(InputStream inputStream) throws IOException {
        return (KexRng$KexRngPubkey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static KexRng$KexRngPubkey parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (KexRng$KexRngPubkey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static KexRng$KexRngPubkey parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (KexRng$KexRngPubkey) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static KexRng$KexRngPubkey parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (KexRng$KexRngPubkey) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static KexRng$KexRngPubkey parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (KexRng$KexRngPubkey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static KexRng$KexRngPubkey parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (KexRng$KexRngPubkey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(KexRng$KexRngPubkey kexRng$KexRngPubkey) {
        return DEFAULT_INSTANCE.createBuilder(kexRng$KexRngPubkey);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<KexRng$KexRngPubkey, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(KexRng$1 kexRng$1) {
            this();
        }

        private Builder() {
            super(KexRng$KexRngPubkey.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (KexRng$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new KexRng$KexRngPubkey();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\n\u0002\u000b", new Object[]{"pubkey_", "version_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<KexRng$KexRngPubkey> parser = PARSER;
                if (parser == null) {
                    synchronized (KexRng$KexRngPubkey.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        KexRng$KexRngPubkey kexRng$KexRngPubkey = new KexRng$KexRngPubkey();
        DEFAULT_INSTANCE = kexRng$KexRngPubkey;
        GeneratedMessageLite.registerDefaultInstance(KexRng$KexRngPubkey.class, kexRng$KexRngPubkey);
    }

    public static KexRng$KexRngPubkey getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<KexRng$KexRngPubkey> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
