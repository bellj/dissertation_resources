package com.google.zxing.qrcode.detector;

import com.google.zxing.DecodeHintType;
import com.google.zxing.NotFoundException;
import com.google.zxing.ResultPoint;
import com.google.zxing.ResultPointCallback;
import com.google.zxing.common.BitMatrix;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/* loaded from: classes3.dex */
public class FinderPatternFinder {
    private static final EstimatedModuleComparator moduleComparator = new EstimatedModuleComparator();
    private final int[] crossCheckStateCount = new int[5];
    private boolean hasSkipped;
    private final BitMatrix image;
    private final List<FinderPattern> possibleCenters = new ArrayList();
    private final ResultPointCallback resultPointCallback;

    public FinderPatternFinder(BitMatrix bitMatrix, ResultPointCallback resultPointCallback) {
        this.image = bitMatrix;
        this.resultPointCallback = resultPointCallback;
    }

    public final FinderPatternInfo find(Map<DecodeHintType, ?> map) throws NotFoundException {
        boolean z = map != null && map.containsKey(DecodeHintType.TRY_HARDER);
        int height = this.image.getHeight();
        int width = this.image.getWidth();
        int i = (height * 3) / 388;
        if (i < 3 || z) {
            i = 3;
        }
        int[] iArr = new int[5];
        int i2 = i - 1;
        boolean z2 = false;
        while (i2 < height && !z2) {
            doClearCounts(iArr);
            int i3 = 0;
            int i4 = 0;
            while (i3 < width) {
                if (this.image.get(i3, i2)) {
                    if ((i4 & 1) == 1) {
                        i4++;
                    }
                    iArr[i4] = iArr[i4] + 1;
                } else if ((i4 & 1) != 0) {
                    iArr[i4] = iArr[i4] + 1;
                } else if (i4 == 4) {
                    if (!foundPatternCross(iArr)) {
                        doShiftCounts2(iArr);
                    } else if (handlePossibleCenter(iArr, i2, i3)) {
                        if (this.hasSkipped) {
                            z2 = haveMultiplyConfirmedCenters();
                        } else {
                            int findRowSkip = findRowSkip();
                            int i5 = iArr[2];
                            if (findRowSkip > i5) {
                                i2 += (findRowSkip - i5) - 2;
                                i3 = width - 1;
                            }
                        }
                        doClearCounts(iArr);
                        i = 2;
                        i4 = 0;
                    } else {
                        doShiftCounts2(iArr);
                    }
                    i4 = 3;
                } else {
                    i4++;
                    iArr[i4] = iArr[i4] + 1;
                }
                i3++;
            }
            if (foundPatternCross(iArr) && handlePossibleCenter(iArr, i2, width)) {
                i = iArr[0];
                if (this.hasSkipped) {
                    z2 = haveMultiplyConfirmedCenters();
                }
            }
            i2 += i;
        }
        FinderPattern[] selectBestPatterns = selectBestPatterns();
        ResultPoint.orderBestPatterns(selectBestPatterns);
        return new FinderPatternInfo(selectBestPatterns);
    }

    private static float centerFromEnd(int[] iArr, int i) {
        return ((float) ((i - iArr[4]) - iArr[3])) - (((float) iArr[2]) / 2.0f);
    }

    protected static boolean foundPatternCross(int[] iArr) {
        int i = 0;
        for (int i2 = 0; i2 < 5; i2++) {
            int i3 = iArr[i2];
            if (i3 == 0) {
                return false;
            }
            i += i3;
        }
        if (i < 7) {
            return false;
        }
        float f = ((float) i) / 7.0f;
        float f2 = f / 2.0f;
        if (Math.abs(f - ((float) iArr[0])) >= f2 || Math.abs(f - ((float) iArr[1])) >= f2 || Math.abs((f * 3.0f) - ((float) iArr[2])) >= 3.0f * f2 || Math.abs(f - ((float) iArr[3])) >= f2 || Math.abs(f - ((float) iArr[4])) >= f2) {
            return false;
        }
        return true;
    }

    protected static boolean foundPatternDiagonal(int[] iArr) {
        int i = 0;
        for (int i2 = 0; i2 < 5; i2++) {
            int i3 = iArr[i2];
            if (i3 == 0) {
                return false;
            }
            i += i3;
        }
        if (i < 7) {
            return false;
        }
        float f = ((float) i) / 7.0f;
        float f2 = f / 1.333f;
        if (Math.abs(f - ((float) iArr[0])) >= f2 || Math.abs(f - ((float) iArr[1])) >= f2 || Math.abs((f * 3.0f) - ((float) iArr[2])) >= 3.0f * f2 || Math.abs(f - ((float) iArr[3])) >= f2 || Math.abs(f - ((float) iArr[4])) >= f2) {
            return false;
        }
        return true;
    }

    private int[] getCrossCheckStateCount() {
        doClearCounts(this.crossCheckStateCount);
        return this.crossCheckStateCount;
    }

    protected static void doClearCounts(int[] iArr) {
        Arrays.fill(iArr, 0);
    }

    protected static void doShiftCounts2(int[] iArr) {
        iArr[0] = iArr[2];
        iArr[1] = iArr[3];
        iArr[2] = iArr[4];
        iArr[3] = 1;
        iArr[4] = 0;
    }

    private boolean crossCheckDiagonal(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int[] crossCheckStateCount = getCrossCheckStateCount();
        int i6 = 0;
        while (i >= i6 && i2 >= i6 && this.image.get(i2 - i6, i - i6)) {
            crossCheckStateCount[2] = crossCheckStateCount[2] + 1;
            i6++;
        }
        if (crossCheckStateCount[2] == 0) {
            return false;
        }
        while (i >= i6 && i2 >= i6 && !this.image.get(i2 - i6, i - i6)) {
            crossCheckStateCount[1] = crossCheckStateCount[1] + 1;
            i6++;
        }
        if (crossCheckStateCount[1] == 0) {
            return false;
        }
        while (i >= i6 && i2 >= i6 && this.image.get(i2 - i6, i - i6)) {
            crossCheckStateCount[0] = crossCheckStateCount[0] + 1;
            i6++;
        }
        if (crossCheckStateCount[0] == 0) {
            return false;
        }
        int height = this.image.getHeight();
        int width = this.image.getWidth();
        int i7 = 1;
        while (true) {
            int i8 = i + i7;
            if (i8 >= height || (i5 = i2 + i7) >= width || !this.image.get(i5, i8)) {
                break;
            }
            crossCheckStateCount[2] = crossCheckStateCount[2] + 1;
            i7++;
        }
        while (true) {
            int i9 = i + i7;
            if (i9 >= height || (i4 = i2 + i7) >= width || this.image.get(i4, i9)) {
                break;
            }
            crossCheckStateCount[3] = crossCheckStateCount[3] + 1;
            i7++;
        }
        if (crossCheckStateCount[3] == 0) {
            return false;
        }
        while (true) {
            int i10 = i + i7;
            if (i10 >= height || (i3 = i2 + i7) >= width || !this.image.get(i3, i10)) {
                break;
            }
            crossCheckStateCount[4] = crossCheckStateCount[4] + 1;
            i7++;
        }
        if (crossCheckStateCount[4] == 0) {
            return false;
        }
        return foundPatternDiagonal(crossCheckStateCount);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0082, code lost:
        if (r2[3] < r13) goto L_0x0085;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0086, code lost:
        if (r11 >= r1) goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x008c, code lost:
        if (r0.get(r12, r11) == false) goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x008e, code lost:
        r9 = r2[4];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0090, code lost:
        if (r9 >= r13) goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0092, code lost:
        r2[4] = r9 + 1;
        r11 = r11 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0099, code lost:
        r12 = r2[4];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x009b, code lost:
        if (r12 < r13) goto L_0x009e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x009d, code lost:
        return Float.NaN;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00b3, code lost:
        if ((java.lang.Math.abs(((((r2[0] + r2[1]) + r2[2]) + r2[3]) + r12) - r14) * 5) < (r14 * 2)) goto L_0x00b6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00b5, code lost:
        return Float.NaN;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00ba, code lost:
        if (foundPatternCross(r2) == false) goto L_0x00c1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00c0, code lost:
        return centerFromEnd(r2, r11);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private float crossCheckVertical(int r11, int r12, int r13, int r14) {
        /*
            r10 = this;
            com.google.zxing.common.BitMatrix r0 = r10.image
            int r1 = r0.getHeight()
            int[] r2 = r10.getCrossCheckStateCount()
            r3 = r11
        L_0x000b:
            r4 = 2
            r5 = 1
            if (r3 < 0) goto L_0x001d
            boolean r6 = r0.get(r12, r3)
            if (r6 == 0) goto L_0x001d
            r6 = r2[r4]
            int r6 = r6 + r5
            r2[r4] = r6
            int r3 = r3 + -1
            goto L_0x000b
        L_0x001d:
            r6 = 2143289344(0x7fc00000, float:NaN)
            if (r3 >= 0) goto L_0x0022
            return r6
        L_0x0022:
            if (r3 < 0) goto L_0x0035
            boolean r7 = r0.get(r12, r3)
            if (r7 != 0) goto L_0x0035
            r7 = r2[r5]
            if (r7 > r13) goto L_0x0035
            int r7 = r7 + 1
            r2[r5] = r7
            int r3 = r3 + -1
            goto L_0x0022
        L_0x0035:
            if (r3 < 0) goto L_0x00c1
            r7 = r2[r5]
            if (r7 <= r13) goto L_0x003d
            goto L_0x00c1
        L_0x003d:
            r7 = 0
            if (r3 < 0) goto L_0x0051
            boolean r8 = r0.get(r12, r3)
            if (r8 == 0) goto L_0x0051
            r8 = r2[r7]
            if (r8 > r13) goto L_0x0051
            int r8 = r8 + 1
            r2[r7] = r8
            int r3 = r3 + -1
            goto L_0x003d
        L_0x0051:
            r3 = r2[r7]
            if (r3 <= r13) goto L_0x0056
            return r6
        L_0x0056:
            int r11 = r11 + r5
        L_0x0057:
            if (r11 >= r1) goto L_0x0067
            boolean r3 = r0.get(r12, r11)
            if (r3 == 0) goto L_0x0067
            r3 = r2[r4]
            int r3 = r3 + r5
            r2[r4] = r3
            int r11 = r11 + 1
            goto L_0x0057
        L_0x0067:
            if (r11 != r1) goto L_0x006a
            return r6
        L_0x006a:
            r3 = 3
            if (r11 >= r1) goto L_0x007e
            boolean r8 = r0.get(r12, r11)
            if (r8 != 0) goto L_0x007e
            r8 = r2[r3]
            if (r8 >= r13) goto L_0x007e
            int r8 = r8 + 1
            r2[r3] = r8
            int r11 = r11 + 1
            goto L_0x006a
        L_0x007e:
            if (r11 == r1) goto L_0x00c1
            r8 = r2[r3]
            if (r8 < r13) goto L_0x0085
            goto L_0x00c1
        L_0x0085:
            r8 = 4
            if (r11 >= r1) goto L_0x0099
            boolean r9 = r0.get(r12, r11)
            if (r9 == 0) goto L_0x0099
            r9 = r2[r8]
            if (r9 >= r13) goto L_0x0099
            int r9 = r9 + 1
            r2[r8] = r9
            int r11 = r11 + 1
            goto L_0x0085
        L_0x0099:
            r12 = r2[r8]
            if (r12 < r13) goto L_0x009e
            return r6
        L_0x009e:
            r13 = r2[r7]
            r0 = r2[r5]
            int r13 = r13 + r0
            r0 = r2[r4]
            int r13 = r13 + r0
            r0 = r2[r3]
            int r13 = r13 + r0
            int r13 = r13 + r12
            int r13 = r13 - r14
            int r12 = java.lang.Math.abs(r13)
            int r12 = r12 * 5
            int r14 = r14 * 2
            if (r12 < r14) goto L_0x00b6
            return r6
        L_0x00b6:
            boolean r12 = foundPatternCross(r2)
            if (r12 == 0) goto L_0x00c1
            float r11 = centerFromEnd(r2, r11)
            return r11
        L_0x00c1:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.qrcode.detector.FinderPatternFinder.crossCheckVertical(int, int, int, int):float");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0082, code lost:
        if (r2[3] < r13) goto L_0x0085;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0086, code lost:
        if (r11 >= r1) goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x008c, code lost:
        if (r0.get(r11, r12) == false) goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x008e, code lost:
        r9 = r2[4];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0090, code lost:
        if (r9 >= r13) goto L_0x0099;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0092, code lost:
        r2[4] = r9 + 1;
        r11 = r11 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0099, code lost:
        r12 = r2[4];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x009b, code lost:
        if (r12 < r13) goto L_0x009e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x009d, code lost:
        return Float.NaN;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00b1, code lost:
        if ((java.lang.Math.abs(((((r2[0] + r2[1]) + r2[2]) + r2[3]) + r12) - r14) * 5) < r14) goto L_0x00b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00b3, code lost:
        return Float.NaN;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00b8, code lost:
        if (foundPatternCross(r2) == false) goto L_0x00bf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00be, code lost:
        return centerFromEnd(r2, r11);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private float crossCheckHorizontal(int r11, int r12, int r13, int r14) {
        /*
            r10 = this;
            com.google.zxing.common.BitMatrix r0 = r10.image
            int r1 = r0.getWidth()
            int[] r2 = r10.getCrossCheckStateCount()
            r3 = r11
        L_0x000b:
            r4 = 2
            r5 = 1
            if (r3 < 0) goto L_0x001d
            boolean r6 = r0.get(r3, r12)
            if (r6 == 0) goto L_0x001d
            r6 = r2[r4]
            int r6 = r6 + r5
            r2[r4] = r6
            int r3 = r3 + -1
            goto L_0x000b
        L_0x001d:
            r6 = 2143289344(0x7fc00000, float:NaN)
            if (r3 >= 0) goto L_0x0022
            return r6
        L_0x0022:
            if (r3 < 0) goto L_0x0035
            boolean r7 = r0.get(r3, r12)
            if (r7 != 0) goto L_0x0035
            r7 = r2[r5]
            if (r7 > r13) goto L_0x0035
            int r7 = r7 + 1
            r2[r5] = r7
            int r3 = r3 + -1
            goto L_0x0022
        L_0x0035:
            if (r3 < 0) goto L_0x00bf
            r7 = r2[r5]
            if (r7 <= r13) goto L_0x003d
            goto L_0x00bf
        L_0x003d:
            r7 = 0
            if (r3 < 0) goto L_0x0051
            boolean r8 = r0.get(r3, r12)
            if (r8 == 0) goto L_0x0051
            r8 = r2[r7]
            if (r8 > r13) goto L_0x0051
            int r8 = r8 + 1
            r2[r7] = r8
            int r3 = r3 + -1
            goto L_0x003d
        L_0x0051:
            r3 = r2[r7]
            if (r3 <= r13) goto L_0x0056
            return r6
        L_0x0056:
            int r11 = r11 + r5
        L_0x0057:
            if (r11 >= r1) goto L_0x0067
            boolean r3 = r0.get(r11, r12)
            if (r3 == 0) goto L_0x0067
            r3 = r2[r4]
            int r3 = r3 + r5
            r2[r4] = r3
            int r11 = r11 + 1
            goto L_0x0057
        L_0x0067:
            if (r11 != r1) goto L_0x006a
            return r6
        L_0x006a:
            r3 = 3
            if (r11 >= r1) goto L_0x007e
            boolean r8 = r0.get(r11, r12)
            if (r8 != 0) goto L_0x007e
            r8 = r2[r3]
            if (r8 >= r13) goto L_0x007e
            int r8 = r8 + 1
            r2[r3] = r8
            int r11 = r11 + 1
            goto L_0x006a
        L_0x007e:
            if (r11 == r1) goto L_0x00bf
            r8 = r2[r3]
            if (r8 < r13) goto L_0x0085
            goto L_0x00bf
        L_0x0085:
            r8 = 4
            if (r11 >= r1) goto L_0x0099
            boolean r9 = r0.get(r11, r12)
            if (r9 == 0) goto L_0x0099
            r9 = r2[r8]
            if (r9 >= r13) goto L_0x0099
            int r9 = r9 + 1
            r2[r8] = r9
            int r11 = r11 + 1
            goto L_0x0085
        L_0x0099:
            r12 = r2[r8]
            if (r12 < r13) goto L_0x009e
            return r6
        L_0x009e:
            r13 = r2[r7]
            r0 = r2[r5]
            int r13 = r13 + r0
            r0 = r2[r4]
            int r13 = r13 + r0
            r0 = r2[r3]
            int r13 = r13 + r0
            int r13 = r13 + r12
            int r13 = r13 - r14
            int r12 = java.lang.Math.abs(r13)
            int r12 = r12 * 5
            if (r12 < r14) goto L_0x00b4
            return r6
        L_0x00b4:
            boolean r12 = foundPatternCross(r2)
            if (r12 == 0) goto L_0x00bf
            float r11 = centerFromEnd(r2, r11)
            return r11
        L_0x00bf:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.qrcode.detector.FinderPatternFinder.crossCheckHorizontal(int, int, int, int):float");
    }

    protected final boolean handlePossibleCenter(int[] iArr, int i, int i2) {
        boolean z = false;
        int i3 = iArr[0] + iArr[1] + iArr[2] + iArr[3] + iArr[4];
        int centerFromEnd = (int) centerFromEnd(iArr, i2);
        float crossCheckVertical = crossCheckVertical(i, centerFromEnd, iArr[2], i3);
        if (!Float.isNaN(crossCheckVertical)) {
            int i4 = (int) crossCheckVertical;
            float crossCheckHorizontal = crossCheckHorizontal(centerFromEnd, i4, iArr[2], i3);
            if (!Float.isNaN(crossCheckHorizontal) && crossCheckDiagonal(i4, (int) crossCheckHorizontal)) {
                float f = ((float) i3) / 7.0f;
                int i5 = 0;
                while (true) {
                    if (i5 >= this.possibleCenters.size()) {
                        break;
                    }
                    FinderPattern finderPattern = this.possibleCenters.get(i5);
                    if (finderPattern.aboutEquals(f, crossCheckVertical, crossCheckHorizontal)) {
                        this.possibleCenters.set(i5, finderPattern.combineEstimate(crossCheckVertical, crossCheckHorizontal, f));
                        z = true;
                        break;
                    }
                    i5++;
                }
                if (!z) {
                    FinderPattern finderPattern2 = new FinderPattern(crossCheckHorizontal, crossCheckVertical, f);
                    this.possibleCenters.add(finderPattern2);
                    ResultPointCallback resultPointCallback = this.resultPointCallback;
                    if (resultPointCallback != null) {
                        resultPointCallback.foundPossibleResultPoint(finderPattern2);
                    }
                }
                return true;
            }
        }
        return false;
    }

    private int findRowSkip() {
        if (this.possibleCenters.size() <= 1) {
            return 0;
        }
        FinderPattern finderPattern = null;
        for (FinderPattern finderPattern2 : this.possibleCenters) {
            if (finderPattern2.getCount() >= 2) {
                if (finderPattern == null) {
                    finderPattern = finderPattern2;
                } else {
                    this.hasSkipped = true;
                    return ((int) (Math.abs(finderPattern.getX() - finderPattern2.getX()) - Math.abs(finderPattern.getY() - finderPattern2.getY()))) / 2;
                }
            }
        }
        return 0;
    }

    private boolean haveMultiplyConfirmedCenters() {
        int size = this.possibleCenters.size();
        float f = 0.0f;
        int i = 0;
        float f2 = 0.0f;
        for (FinderPattern finderPattern : this.possibleCenters) {
            if (finderPattern.getCount() >= 2) {
                i++;
                f2 += finderPattern.getEstimatedModuleSize();
            }
        }
        if (i < 3) {
            return false;
        }
        float f3 = f2 / ((float) size);
        for (FinderPattern finderPattern2 : this.possibleCenters) {
            f += Math.abs(finderPattern2.getEstimatedModuleSize() - f3);
        }
        if (f <= f2 * 0.05f) {
            return true;
        }
        return false;
    }

    private static double squaredDistance(FinderPattern finderPattern, FinderPattern finderPattern2) {
        double x = (double) (finderPattern.getX() - finderPattern2.getX());
        double y = (double) (finderPattern.getY() - finderPattern2.getY());
        Double.isNaN(x);
        Double.isNaN(x);
        Double.isNaN(y);
        Double.isNaN(y);
        return (x * x) + (y * y);
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00c5 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.google.zxing.qrcode.detector.FinderPattern[] selectBestPatterns() throws com.google.zxing.NotFoundException {
        /*
        // Method dump skipped, instructions count: 222
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.zxing.qrcode.detector.FinderPatternFinder.selectBestPatterns():com.google.zxing.qrcode.detector.FinderPattern[]");
    }

    /* loaded from: classes3.dex */
    public static final class EstimatedModuleComparator implements Serializable, Comparator<FinderPattern> {
        private EstimatedModuleComparator() {
        }

        public int compare(FinderPattern finderPattern, FinderPattern finderPattern2) {
            return Float.compare(finderPattern.getEstimatedModuleSize(), finderPattern2.getEstimatedModuleSize());
        }
    }
}
