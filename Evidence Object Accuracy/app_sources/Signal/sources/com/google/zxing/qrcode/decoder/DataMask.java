package com.google.zxing.qrcode.decoder;

import com.google.zxing.common.BitMatrix;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* loaded from: classes3.dex */
public abstract class DataMask extends Enum<DataMask> {
    abstract boolean isMasked(int i, int i2);

    public final void unmaskBitMatrix(BitMatrix bitMatrix, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            for (int i3 = 0; i3 < i; i3++) {
                if (isMasked(i2, i3)) {
                    bitMatrix.flip(i3, i2);
                }
            }
        }
    }
}
