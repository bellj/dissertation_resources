package com.google.common.primitives;

/* loaded from: classes3.dex */
public final class Floats extends FloatsMethodsForWeb {
    public static int hashCode(float f) {
        return Float.valueOf(f).hashCode();
    }
}
