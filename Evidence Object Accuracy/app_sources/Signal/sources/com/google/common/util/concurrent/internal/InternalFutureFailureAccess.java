package com.google.common.util.concurrent.internal;

/* loaded from: classes3.dex */
public abstract class InternalFutureFailureAccess {
    /* access modifiers changed from: protected */
    public abstract Throwable tryInternalFastPathGetFailure();
}
