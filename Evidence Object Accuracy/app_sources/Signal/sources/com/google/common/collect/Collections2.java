package com.google.common.collect;

import com.google.common.base.Preconditions;
import java.util.Collection;
import org.thoughtcrime.securesms.MediaPreviewActivity;

/* loaded from: classes3.dex */
public final class Collections2 {
    public static boolean safeContains(Collection<?> collection, Object obj) {
        Preconditions.checkNotNull(collection);
        try {
            return collection.contains(obj);
        } catch (ClassCastException | NullPointerException unused) {
            return false;
        }
    }

    public static StringBuilder newStringBuilderForCollection(int i) {
        CollectPreconditions.checkNonnegative(i, MediaPreviewActivity.SIZE_EXTRA);
        return new StringBuilder((int) Math.min(((long) i) * 8, 1073741824L));
    }
}
