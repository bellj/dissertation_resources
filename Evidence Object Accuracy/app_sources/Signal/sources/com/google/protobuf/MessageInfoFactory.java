package com.google.protobuf;

/* access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public interface MessageInfoFactory {
    boolean isSupported(Class<?> cls);

    MessageInfo messageInfoFor(Class<?> cls);
}
