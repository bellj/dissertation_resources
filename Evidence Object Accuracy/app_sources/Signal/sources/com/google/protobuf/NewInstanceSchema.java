package com.google.protobuf;

/* loaded from: classes3.dex */
public interface NewInstanceSchema {
    Object newInstance(Object obj);
}
