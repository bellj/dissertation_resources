package com.google.protobuf;

/* access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public interface MessageInfo {
    MessageLite getDefaultInstance();

    ProtoSyntax getSyntax();

    boolean isMessageSetWireFormat();
}
