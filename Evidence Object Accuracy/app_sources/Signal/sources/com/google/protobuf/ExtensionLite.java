package com.google.protobuf;

import com.google.protobuf.MessageLite;

/* loaded from: classes3.dex */
public abstract class ExtensionLite<ContainingType extends MessageLite, Type> {
    public boolean isLite() {
        return true;
    }
}
