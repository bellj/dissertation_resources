package com.google.android.exoplayer2;

/* loaded from: classes.dex */
public interface RendererCapabilities {
    String getName();

    int getTrackType();

    int supportsFormat(Format format) throws ExoPlaybackException;

    int supportsMixedMimeTypeAdaptation() throws ExoPlaybackException;

    /* renamed from: com.google.android.exoplayer2.RendererCapabilities$-CC */
    /* loaded from: classes.dex */
    public final /* synthetic */ class CC {
        public static int create(int i, int i2, int i3) {
            return i | i2 | i3;
        }

        public static int getAdaptiveSupport(int i) {
            return i & 24;
        }

        public static int getFormatSupport(int i) {
            return i & 7;
        }

        public static int getTunnelingSupport(int i) {
            return i & 32;
        }

        public static int create(int i) {
            return create(i, 0, 0);
        }
    }
}
