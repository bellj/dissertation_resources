package com.google.android.exoplayer2.ui;

import org.thoughtcrime.securesms.R;

/* loaded from: classes.dex */
public final class R$styleable {
    public static final int[] AspectRatioFrameLayout = {R.attr.resize_mode};
    public static final int AspectRatioFrameLayout_resize_mode;
    public static final int[] ColorStateListItem = {16843173, 16843551, R.attr.alpha};
    public static final int[] DefaultTimeBar = {R.attr.ad_marker_color, R.attr.ad_marker_width, R.attr.bar_gravity, R.attr.bar_height, R.attr.buffered_color, R.attr.played_ad_marker_color, R.attr.played_color, R.attr.scrubber_color, R.attr.scrubber_disabled_size, R.attr.scrubber_dragged_size, R.attr.scrubber_drawable, R.attr.scrubber_enabled_size, R.attr.touch_target_height, R.attr.unplayed_color};
    public static final int DefaultTimeBar_ad_marker_color;
    public static final int DefaultTimeBar_ad_marker_width;
    public static final int DefaultTimeBar_bar_gravity;
    public static final int DefaultTimeBar_bar_height;
    public static final int DefaultTimeBar_buffered_color;
    public static final int DefaultTimeBar_played_ad_marker_color;
    public static final int DefaultTimeBar_played_color;
    public static final int DefaultTimeBar_scrubber_color;
    public static final int DefaultTimeBar_scrubber_disabled_size;
    public static final int DefaultTimeBar_scrubber_dragged_size;
    public static final int DefaultTimeBar_scrubber_drawable;
    public static final int DefaultTimeBar_scrubber_enabled_size;
    public static final int DefaultTimeBar_touch_target_height;
    public static final int DefaultTimeBar_unplayed_color;
    public static final int[] FontFamily = {R.attr.fontProviderAuthority, R.attr.fontProviderCerts, R.attr.fontProviderFetchStrategy, R.attr.fontProviderFetchTimeout, R.attr.fontProviderPackage, R.attr.fontProviderQuery, R.attr.fontProviderSystemFontFamily};
    public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, R.attr.font, R.attr.fontStyle, R.attr.fontVariationSettings, R.attr.fontWeight, R.attr.ttcIndex};
    public static final int[] GradientColor = {16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051};
    public static final int[] GradientColorItem = {16843173, 16844052};
    public static final int[] PlayerControlView = {R.attr.ad_marker_color, R.attr.ad_marker_width, R.attr.bar_gravity, R.attr.bar_height, R.attr.buffered_color, R.attr.controller_layout_id, R.attr.played_ad_marker_color, R.attr.played_color, R.attr.repeat_toggle_modes, R.attr.scrubber_color, R.attr.scrubber_disabled_size, R.attr.scrubber_dragged_size, R.attr.scrubber_drawable, R.attr.scrubber_enabled_size, R.attr.show_fastforward_button, R.attr.show_next_button, R.attr.show_previous_button, R.attr.show_rewind_button, R.attr.show_shuffle_button, R.attr.show_timeout, R.attr.time_bar_min_update_interval, R.attr.touch_target_height, R.attr.unplayed_color};
    public static final int PlayerControlView_controller_layout_id;
    public static final int PlayerControlView_repeat_toggle_modes;
    public static final int PlayerControlView_show_fastforward_button;
    public static final int PlayerControlView_show_next_button;
    public static final int PlayerControlView_show_previous_button;
    public static final int PlayerControlView_show_rewind_button;
    public static final int PlayerControlView_show_shuffle_button;
    public static final int PlayerControlView_show_timeout;
    public static final int PlayerControlView_time_bar_min_update_interval;
    public static final int[] PlayerView = {R.attr.ad_marker_color, R.attr.ad_marker_width, R.attr.auto_show, R.attr.bar_height, R.attr.buffered_color, R.attr.controller_layout_id, R.attr.default_artwork, R.attr.hide_during_ads, R.attr.hide_on_touch, R.attr.keep_content_on_player_reset, R.attr.played_ad_marker_color, R.attr.played_color, R.attr.player_layout_id, R.attr.repeat_toggle_modes, R.attr.resize_mode, R.attr.scrubber_color, R.attr.scrubber_disabled_size, R.attr.scrubber_dragged_size, R.attr.scrubber_drawable, R.attr.scrubber_enabled_size, R.attr.show_buffering, R.attr.show_shuffle_button, R.attr.show_timeout, R.attr.shutter_background_color, R.attr.surface_type, R.attr.time_bar_min_update_interval, R.attr.touch_target_height, R.attr.unplayed_color, R.attr.use_artwork, R.attr.use_controller};
    public static final int PlayerView_auto_show;
    public static final int PlayerView_default_artwork;
    public static final int PlayerView_hide_during_ads;
    public static final int PlayerView_hide_on_touch;
    public static final int PlayerView_keep_content_on_player_reset;
    public static final int PlayerView_player_layout_id;
    public static final int PlayerView_resize_mode;
    public static final int PlayerView_show_buffering;
    public static final int PlayerView_show_timeout;
    public static final int PlayerView_shutter_background_color;
    public static final int PlayerView_surface_type;
    public static final int PlayerView_use_artwork;
    public static final int PlayerView_use_controller;
    public static final int[] RecyclerView = {16842948, 16842987, 16842993, R.attr.fastScrollEnabled, R.attr.fastScrollHorizontalThumbDrawable, R.attr.fastScrollHorizontalTrackDrawable, R.attr.fastScrollVerticalThumbDrawable, R.attr.fastScrollVerticalTrackDrawable, R.attr.layoutManager, R.attr.reverseLayout, R.attr.spanCount, R.attr.stackFromEnd};
    public static final int[] StyledPlayerControlView = {R.attr.ad_marker_color, R.attr.ad_marker_width, R.attr.animation_enabled, R.attr.bar_gravity, R.attr.bar_height, R.attr.buffered_color, R.attr.controller_layout_id, R.attr.played_ad_marker_color, R.attr.played_color, R.attr.repeat_toggle_modes, R.attr.scrubber_color, R.attr.scrubber_disabled_size, R.attr.scrubber_dragged_size, R.attr.scrubber_drawable, R.attr.scrubber_enabled_size, R.attr.show_fastforward_button, R.attr.show_next_button, R.attr.show_previous_button, R.attr.show_rewind_button, R.attr.show_shuffle_button, R.attr.show_subtitle_button, R.attr.show_timeout, R.attr.show_vr_button, R.attr.time_bar_min_update_interval, R.attr.touch_target_height, R.attr.unplayed_color};
    public static final int[] StyledPlayerView = {R.attr.ad_marker_color, R.attr.ad_marker_width, R.attr.animation_enabled, R.attr.auto_show, R.attr.bar_gravity, R.attr.bar_height, R.attr.buffered_color, R.attr.controller_layout_id, R.attr.default_artwork, R.attr.hide_during_ads, R.attr.hide_on_touch, R.attr.keep_content_on_player_reset, R.attr.played_ad_marker_color, R.attr.played_color, R.attr.player_layout_id, R.attr.repeat_toggle_modes, R.attr.resize_mode, R.attr.scrubber_color, R.attr.scrubber_disabled_size, R.attr.scrubber_dragged_size, R.attr.scrubber_drawable, R.attr.scrubber_enabled_size, R.attr.show_buffering, R.attr.show_shuffle_button, R.attr.show_subtitle_button, R.attr.show_timeout, R.attr.show_vr_button, R.attr.shutter_background_color, R.attr.surface_type, R.attr.time_bar_min_update_interval, R.attr.touch_target_height, R.attr.unplayed_color, R.attr.use_artwork, R.attr.use_controller};
}
