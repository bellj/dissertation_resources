package com.google.android.exoplayer2;

@Deprecated
/* loaded from: classes.dex */
public class DefaultControlDispatcher implements ControlDispatcher {
    private final long fastForwardIncrementMs = -9223372036854775807L;
    private final boolean rewindAndFastForwardIncrementsSet = false;
    private final long rewindIncrementMs = -9223372036854775807L;

    @Override // com.google.android.exoplayer2.ControlDispatcher
    public boolean dispatchPrepare(Player player) {
        player.prepare();
        return true;
    }

    @Override // com.google.android.exoplayer2.ControlDispatcher
    public boolean dispatchSetPlayWhenReady(Player player, boolean z) {
        player.setPlayWhenReady(z);
        return true;
    }

    @Override // com.google.android.exoplayer2.ControlDispatcher
    public boolean dispatchSeekTo(Player player, int i, long j) {
        player.seekTo(i, j);
        return true;
    }

    @Override // com.google.android.exoplayer2.ControlDispatcher
    public boolean dispatchPrevious(Player player) {
        player.seekToPrevious();
        return true;
    }

    @Override // com.google.android.exoplayer2.ControlDispatcher
    public boolean dispatchNext(Player player) {
        player.seekToNext();
        return true;
    }

    @Override // com.google.android.exoplayer2.ControlDispatcher
    public boolean dispatchRewind(Player player) {
        if (!this.rewindAndFastForwardIncrementsSet) {
            player.seekBack();
            return true;
        } else if (!isRewindEnabled() || !player.isCurrentWindowSeekable()) {
            return true;
        } else {
            seekToOffset(player, -this.rewindIncrementMs);
            return true;
        }
    }

    @Override // com.google.android.exoplayer2.ControlDispatcher
    public boolean dispatchFastForward(Player player) {
        if (!this.rewindAndFastForwardIncrementsSet) {
            player.seekForward();
            return true;
        } else if (!isFastForwardEnabled() || !player.isCurrentWindowSeekable()) {
            return true;
        } else {
            seekToOffset(player, this.fastForwardIncrementMs);
            return true;
        }
    }

    @Override // com.google.android.exoplayer2.ControlDispatcher
    public boolean dispatchSetRepeatMode(Player player, int i) {
        player.setRepeatMode(i);
        return true;
    }

    @Override // com.google.android.exoplayer2.ControlDispatcher
    public boolean dispatchSetShuffleModeEnabled(Player player, boolean z) {
        player.setShuffleModeEnabled(z);
        return true;
    }

    @Override // com.google.android.exoplayer2.ControlDispatcher
    public boolean dispatchStop(Player player, boolean z) {
        player.stop(z);
        return true;
    }

    @Override // com.google.android.exoplayer2.ControlDispatcher
    public boolean dispatchSetPlaybackParameters(Player player, PlaybackParameters playbackParameters) {
        player.setPlaybackParameters(playbackParameters);
        return true;
    }

    @Override // com.google.android.exoplayer2.ControlDispatcher
    public boolean isRewindEnabled() {
        return !this.rewindAndFastForwardIncrementsSet || this.rewindIncrementMs > 0;
    }

    @Override // com.google.android.exoplayer2.ControlDispatcher
    public boolean isFastForwardEnabled() {
        return !this.rewindAndFastForwardIncrementsSet || this.fastForwardIncrementMs > 0;
    }

    private static void seekToOffset(Player player, long j) {
        long currentPosition = player.getCurrentPosition() + j;
        long duration = player.getDuration();
        if (duration != -9223372036854775807L) {
            currentPosition = Math.min(currentPosition, duration);
        }
        player.seekTo(Math.max(currentPosition, 0L));
    }
}
