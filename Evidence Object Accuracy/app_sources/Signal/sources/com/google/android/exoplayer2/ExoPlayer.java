package com.google.android.exoplayer2;

/* loaded from: classes.dex */
public interface ExoPlayer extends Player {

    /* loaded from: classes.dex */
    public interface AudioComponent {
        void setVolume(float f);
    }

    /* loaded from: classes.dex */
    public interface AudioOffloadListener {

        /* renamed from: com.google.android.exoplayer2.ExoPlayer$AudioOffloadListener$-CC */
        /* loaded from: classes.dex */
        public final /* synthetic */ class CC {
            public static void $default$onExperimentalOffloadSchedulingEnabledChanged(AudioOffloadListener audioOffloadListener, boolean z) {
            }
        }

        void onExperimentalOffloadSchedulingEnabledChanged(boolean z);

        void onExperimentalSleepingForOffloadChanged(boolean z);
    }
}
