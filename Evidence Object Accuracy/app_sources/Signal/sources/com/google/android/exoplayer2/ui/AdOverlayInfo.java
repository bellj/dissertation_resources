package com.google.android.exoplayer2.ui;

import android.view.View;

/* loaded from: classes.dex */
public final class AdOverlayInfo {
    public final int purpose;
    public final String reasonDetail;
    public final View view;

    public AdOverlayInfo(View view, int i) {
        this(view, i, null);
    }

    public AdOverlayInfo(View view, int i, String str) {
        this.view = view;
        this.purpose = i;
        this.reasonDetail = str;
    }
}
