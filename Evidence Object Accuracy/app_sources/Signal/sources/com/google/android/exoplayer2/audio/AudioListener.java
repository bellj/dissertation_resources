package com.google.android.exoplayer2.audio;

@Deprecated
/* loaded from: classes.dex */
public interface AudioListener {
    void onAudioAttributesChanged(AudioAttributes audioAttributes);

    void onSkipSilenceEnabledChanged(boolean z);

    void onVolumeChanged(float f);
}
