package com.google.android.exoplayer2.video;

import com.google.android.exoplayer2.decoder.OutputBuffer;
import java.nio.ByteBuffer;

/* loaded from: classes.dex */
public class VideoDecoderOutputBuffer extends OutputBuffer {
    public int colorspace;
    public int height;
    private final OutputBuffer.Owner<VideoDecoderOutputBuffer> owner;
    public int width;
    public ByteBuffer[] yuvPlanes;
    public int[] yuvStrides;

    @Override // com.google.android.exoplayer2.decoder.OutputBuffer
    public void release() {
        this.owner.releaseOutputBuffer(this);
    }
}
