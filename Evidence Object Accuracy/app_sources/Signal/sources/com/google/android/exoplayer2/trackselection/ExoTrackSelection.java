package com.google.android.exoplayer2.trackselection;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroup;
import com.google.android.exoplayer2.upstream.BandwidthMeter;

/* loaded from: classes.dex */
public interface ExoTrackSelection extends TrackSelection {

    /* renamed from: com.google.android.exoplayer2.trackselection.ExoTrackSelection$-CC */
    /* loaded from: classes.dex */
    public final /* synthetic */ class CC {
        public static void $default$onDiscontinuity(ExoTrackSelection exoTrackSelection) {
        }

        public static void $default$onPlayWhenReadyChanged(ExoTrackSelection exoTrackSelection, boolean z) {
        }

        public static void $default$onRebuffer(ExoTrackSelection exoTrackSelection) {
        }
    }

    /* loaded from: classes.dex */
    public interface Factory {
        ExoTrackSelection[] createTrackSelections(Definition[] definitionArr, BandwidthMeter bandwidthMeter, MediaSource.MediaPeriodId mediaPeriodId, Timeline timeline);
    }

    void disable();

    void enable();

    Format getSelectedFormat();

    int getSelectedIndex();

    void onDiscontinuity();

    void onPlayWhenReadyChanged(boolean z);

    void onPlaybackSpeed(float f);

    void onRebuffer();

    /* loaded from: classes.dex */
    public static final class Definition {
        public final TrackGroup group;
        public final int[] tracks;
        public final int type;

        public Definition(TrackGroup trackGroup, int... iArr) {
            this(trackGroup, iArr, 0);
        }

        public Definition(TrackGroup trackGroup, int[] iArr, int i) {
            this.group = trackGroup;
            this.tracks = iArr;
            this.type = i;
        }
    }
}
