package com.google.android.exoplayer2.ext.mediasession;

import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.session.MediaSessionCompat;
import com.google.android.exoplayer2.ControlDispatcher;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector;
import com.google.android.exoplayer2.util.Assertions;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;

/* loaded from: classes.dex */
public abstract class TimelineQueueNavigator implements MediaSessionConnector.QueueNavigator {
    public static final int DEFAULT_MAX_QUEUE_SIZE;
    private long activeQueueItemId;
    private final int maxQueueSize;
    private final MediaSessionCompat mediaSession;
    private final Timeline.Window window;

    public abstract MediaDescriptionCompat getMediaDescription(Player player, int i);

    @Override // com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector.CommandReceiver
    public boolean onCommand(Player player, @Deprecated ControlDispatcher controlDispatcher, String str, Bundle bundle, ResultReceiver resultReceiver) {
        return false;
    }

    public TimelineQueueNavigator(MediaSessionCompat mediaSessionCompat) {
        this(mediaSessionCompat, 10);
    }

    public TimelineQueueNavigator(MediaSessionCompat mediaSessionCompat, int i) {
        Assertions.checkState(i > 0);
        this.mediaSession = mediaSessionCompat;
        this.maxQueueSize = i;
        this.activeQueueItemId = -1;
        this.window = new Timeline.Window();
    }

    @Override // com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector.QueueNavigator
    public long getSupportedQueueNavigatorActions(Player player) {
        boolean z;
        boolean z2;
        Timeline currentTimeline = player.getCurrentTimeline();
        boolean z3 = false;
        if (currentTimeline.isEmpty() || player.isPlayingAd()) {
            z = false;
            z2 = false;
        } else {
            currentTimeline.getWindow(player.getCurrentWindowIndex(), this.window);
            boolean z4 = currentTimeline.getWindowCount() > 1;
            z2 = player.isCommandAvailable(4) || !this.window.isLive() || player.isCommandAvailable(5);
            if ((this.window.isLive() && this.window.isDynamic) || player.isCommandAvailable(7)) {
                z3 = true;
            }
            z = z3;
            z3 = z4;
        }
        long j = 0;
        if (z3) {
            j = 4096;
        }
        if (z2) {
            j |= 16;
        }
        return z ? j | 32 : j;
    }

    @Override // com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector.QueueNavigator
    public final void onTimelineChanged(Player player) {
        publishFloatingQueueWindow(player);
    }

    @Override // com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector.QueueNavigator
    public final void onCurrentWindowIndexChanged(Player player) {
        if (this.activeQueueItemId == -1 || player.getCurrentTimeline().getWindowCount() > this.maxQueueSize) {
            publishFloatingQueueWindow(player);
        } else if (!player.getCurrentTimeline().isEmpty()) {
            this.activeQueueItemId = (long) player.getCurrentWindowIndex();
        }
    }

    @Override // com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector.QueueNavigator
    public final long getActiveQueueItemId(Player player) {
        return this.activeQueueItemId;
    }

    @Override // com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector.QueueNavigator
    public void onSkipToPrevious(Player player, @Deprecated ControlDispatcher controlDispatcher) {
        controlDispatcher.dispatchPrevious(player);
    }

    @Override // com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector.QueueNavigator
    public void onSkipToQueueItem(Player player, @Deprecated ControlDispatcher controlDispatcher, long j) {
        int i;
        Timeline currentTimeline = player.getCurrentTimeline();
        if (!currentTimeline.isEmpty() && !player.isPlayingAd() && (i = (int) j) >= 0 && i < currentTimeline.getWindowCount()) {
            controlDispatcher.dispatchSeekTo(player, i, -9223372036854775807L);
        }
    }

    @Override // com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector.QueueNavigator
    public void onSkipToNext(Player player, @Deprecated ControlDispatcher controlDispatcher) {
        controlDispatcher.dispatchNext(player);
    }

    private void publishFloatingQueueWindow(Player player) {
        Timeline currentTimeline = player.getCurrentTimeline();
        if (currentTimeline.isEmpty()) {
            this.mediaSession.setQueue(Collections.emptyList());
            this.activeQueueItemId = -1;
            return;
        }
        ArrayDeque arrayDeque = new ArrayDeque();
        int min = Math.min(this.maxQueueSize, currentTimeline.getWindowCount());
        int currentWindowIndex = player.getCurrentWindowIndex();
        long j = (long) currentWindowIndex;
        arrayDeque.add(new MediaSessionCompat.QueueItem(getMediaDescription(player, currentWindowIndex), j));
        boolean shuffleModeEnabled = player.getShuffleModeEnabled();
        int i = currentWindowIndex;
        while (true) {
            if (!(currentWindowIndex == -1 && i == -1) && arrayDeque.size() < min) {
                if (!(i == -1 || (i = currentTimeline.getNextWindowIndex(i, 0, shuffleModeEnabled)) == -1)) {
                    arrayDeque.add(new MediaSessionCompat.QueueItem(getMediaDescription(player, i), (long) i));
                }
                if (!(currentWindowIndex == -1 || arrayDeque.size() >= min || (currentWindowIndex = currentTimeline.getPreviousWindowIndex(currentWindowIndex, 0, shuffleModeEnabled)) == -1)) {
                    arrayDeque.addFirst(new MediaSessionCompat.QueueItem(getMediaDescription(player, currentWindowIndex), (long) currentWindowIndex));
                }
            }
        }
        this.mediaSession.setQueue(new ArrayList(arrayDeque));
        this.activeQueueItemId = j;
    }
}
