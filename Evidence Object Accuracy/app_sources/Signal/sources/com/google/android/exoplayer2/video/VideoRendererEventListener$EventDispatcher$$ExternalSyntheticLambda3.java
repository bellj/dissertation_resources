package com.google.android.exoplayer2.video;

import com.google.android.exoplayer2.video.VideoRendererEventListener;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class VideoRendererEventListener$EventDispatcher$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ VideoRendererEventListener.EventDispatcher f$0;
    public final /* synthetic */ VideoSize f$1;

    public /* synthetic */ VideoRendererEventListener$EventDispatcher$$ExternalSyntheticLambda3(VideoRendererEventListener.EventDispatcher eventDispatcher, VideoSize videoSize) {
        this.f$0 = eventDispatcher;
        this.f$1 = videoSize;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$videoSizeChanged$5(this.f$1);
    }
}
