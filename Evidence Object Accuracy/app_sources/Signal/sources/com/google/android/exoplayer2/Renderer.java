package com.google.android.exoplayer2;

import com.google.android.exoplayer2.PlayerMessage;
import com.google.android.exoplayer2.source.SampleStream;
import com.google.android.exoplayer2.util.MediaClock;
import java.io.IOException;

/* loaded from: classes.dex */
public interface Renderer extends PlayerMessage.Target {

    /* renamed from: com.google.android.exoplayer2.Renderer$-CC */
    /* loaded from: classes.dex */
    public final /* synthetic */ class CC {
        public static void $default$setPlaybackSpeed(Renderer renderer, float f, float f2) throws ExoPlaybackException {
        }
    }

    /* loaded from: classes.dex */
    public interface WakeupListener {
        void onSleep(long j);

        void onWakeup();
    }

    void disable();

    void enable(RendererConfiguration rendererConfiguration, Format[] formatArr, SampleStream sampleStream, long j, boolean z, boolean z2, long j2, long j3) throws ExoPlaybackException;

    RendererCapabilities getCapabilities();

    MediaClock getMediaClock();

    @Override // com.google.android.exoplayer2.RendererCapabilities
    String getName();

    long getReadingPositionUs();

    int getState();

    SampleStream getStream();

    @Override // com.google.android.exoplayer2.RendererCapabilities
    int getTrackType();

    boolean hasReadStreamToEnd();

    boolean isCurrentStreamFinal();

    boolean isEnded();

    boolean isReady();

    void maybeThrowStreamError() throws IOException;

    void render(long j, long j2) throws ExoPlaybackException;

    void replaceStream(Format[] formatArr, SampleStream sampleStream, long j, long j2) throws ExoPlaybackException;

    void reset();

    void resetPosition(long j) throws ExoPlaybackException;

    void setCurrentStreamFinal();

    void setIndex(int i);

    void setPlaybackSpeed(float f, float f2) throws ExoPlaybackException;

    void start() throws ExoPlaybackException;

    void stop();
}
