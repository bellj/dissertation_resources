package com.google.android.exoplayer2;

/* loaded from: classes.dex */
public class PlaybackException extends Exception {
    public static final Bundleable$Creator<PlaybackException> CREATOR = new ExoPlaybackException$$ExternalSyntheticLambda0();
    public final int errorCode;
    public final long timestampMs;

    public PlaybackException(String str, Throwable th, int i, long j) {
        super(str, th);
        this.errorCode = i;
        this.timestampMs = j;
    }
}
