package com.google.android.exoplayer2.device;

import com.google.android.exoplayer2.Bundleable$Creator;
import com.google.android.exoplayer2.ExoPlaybackException$$ExternalSyntheticLambda0;

/* loaded from: classes.dex */
public final class DeviceInfo {
    public static final Bundleable$Creator<DeviceInfo> CREATOR = new ExoPlaybackException$$ExternalSyntheticLambda0();
    public static final DeviceInfo UNKNOWN = new DeviceInfo(0, 0, 0);
    public final int maxVolume;
    public final int minVolume;
    public final int playbackType;

    public DeviceInfo(int i, int i2, int i3) {
        this.playbackType = i;
        this.minVolume = i2;
        this.maxVolume = i3;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DeviceInfo)) {
            return false;
        }
        DeviceInfo deviceInfo = (DeviceInfo) obj;
        if (this.playbackType == deviceInfo.playbackType && this.minVolume == deviceInfo.minVolume && this.maxVolume == deviceInfo.maxVolume) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return ((((527 + this.playbackType) * 31) + this.minVolume) * 31) + this.maxVolume;
    }
}
