package com.google.android.exoplayer2;

import android.os.Looper;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.audio.AudioListener;
import com.google.android.exoplayer2.device.DeviceInfo;
import com.google.android.exoplayer2.device.DeviceListener;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.metadata.MetadataOutput;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.text.Cue;
import com.google.android.exoplayer2.text.TextOutput;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.util.FlagSet;
import com.google.android.exoplayer2.video.VideoListener;
import com.google.android.exoplayer2.video.VideoSize;
import com.google.common.base.Objects;
import java.util.List;

/* loaded from: classes.dex */
public interface Player {

    @Deprecated
    /* loaded from: classes.dex */
    public interface EventListener {

        /* renamed from: com.google.android.exoplayer2.Player$EventListener$-CC */
        /* loaded from: classes.dex */
        public final /* synthetic */ class CC {
            public static void $default$onAvailableCommandsChanged(EventListener eventListener, Commands commands) {
            }

            public static void $default$onEvents(EventListener eventListener, Player player, Events events) {
            }

            public static void $default$onIsPlayingChanged(EventListener eventListener, boolean z) {
            }

            @Deprecated
            public static void $default$onLoadingChanged(EventListener eventListener, boolean z) {
            }

            public static void $default$onMaxSeekToPreviousPositionChanged(EventListener eventListener, int i) {
            }

            public static void $default$onMediaItemTransition(EventListener eventListener, MediaItem mediaItem, int i) {
            }

            public static void $default$onMediaMetadataChanged(EventListener eventListener, MediaMetadata mediaMetadata) {
            }

            public static void $default$onPlaybackParametersChanged(EventListener eventListener, PlaybackParameters playbackParameters) {
            }

            public static void $default$onPlaybackSuppressionReasonChanged(EventListener eventListener, int i) {
            }

            public static void $default$onPlayerError(EventListener eventListener, PlaybackException playbackException) {
            }

            public static void $default$onPlayerErrorChanged(EventListener eventListener, PlaybackException playbackException) {
            }

            @Deprecated
            public static void $default$onPlayerStateChanged(EventListener eventListener, boolean z, int i) {
            }

            public static void $default$onPlaylistMetadataChanged(EventListener eventListener, MediaMetadata mediaMetadata) {
            }

            @Deprecated
            public static void $default$onPositionDiscontinuity(EventListener eventListener, int i) {
            }

            public static void $default$onPositionDiscontinuity(EventListener eventListener, PositionInfo positionInfo, PositionInfo positionInfo2, int i) {
            }

            public static void $default$onRepeatModeChanged(EventListener eventListener, int i) {
            }

            @Deprecated
            public static void $default$onSeekProcessed(EventListener eventListener) {
            }

            public static void $default$onShuffleModeEnabledChanged(EventListener eventListener, boolean z) {
            }

            @Deprecated
            public static void $default$onStaticMetadataChanged(EventListener eventListener, List list) {
            }

            public static void $default$onTimelineChanged(EventListener eventListener, Timeline timeline, int i) {
            }

            public static void $default$onTracksChanged(EventListener eventListener, TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
            }
        }

        void onAvailableCommandsChanged(Commands commands);

        void onEvents(Player player, Events events);

        void onIsLoadingChanged(boolean z);

        void onIsPlayingChanged(boolean z);

        @Deprecated
        void onLoadingChanged(boolean z);

        void onMediaItemTransition(MediaItem mediaItem, int i);

        void onMediaMetadataChanged(MediaMetadata mediaMetadata);

        void onPlayWhenReadyChanged(boolean z, int i);

        void onPlaybackParametersChanged(PlaybackParameters playbackParameters);

        void onPlaybackStateChanged(int i);

        void onPlaybackSuppressionReasonChanged(int i);

        void onPlayerError(PlaybackException playbackException);

        void onPlayerErrorChanged(PlaybackException playbackException);

        @Deprecated
        void onPlayerStateChanged(boolean z, int i);

        void onPlaylistMetadataChanged(MediaMetadata mediaMetadata);

        @Deprecated
        void onPositionDiscontinuity(int i);

        void onPositionDiscontinuity(PositionInfo positionInfo, PositionInfo positionInfo2, int i);

        void onRepeatModeChanged(int i);

        @Deprecated
        void onSeekProcessed();

        void onShuffleModeEnabledChanged(boolean z);

        @Deprecated
        void onStaticMetadataChanged(List<Metadata> list);

        void onTimelineChanged(Timeline timeline, int i);

        void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray);
    }

    /* loaded from: classes.dex */
    public interface Listener extends VideoListener, AudioListener, TextOutput, MetadataOutput, DeviceListener, EventListener {

        /* renamed from: com.google.android.exoplayer2.Player$Listener$-CC */
        /* loaded from: classes.dex */
        public final /* synthetic */ class CC {
            public static void $default$onAudioAttributesChanged(Listener listener, AudioAttributes audioAttributes) {
            }

            public static void $default$onAudioSessionIdChanged(Listener listener, int i) {
            }

            public static void $default$onAvailableCommandsChanged(Listener listener, Commands commands) {
            }

            public static void $default$onCues(Listener listener, List list) {
            }

            public static void $default$onDeviceInfoChanged(Listener listener, DeviceInfo deviceInfo) {
            }

            public static void $default$onDeviceVolumeChanged(Listener listener, int i, boolean z) {
            }

            public static void $default$onEvents(Listener listener, Player player, Events events) {
            }

            public static void $default$onIsLoadingChanged(Listener listener, boolean z) {
            }

            public static void $default$onIsPlayingChanged(Listener listener, boolean z) {
            }

            public static void $default$onMediaItemTransition(Listener listener, MediaItem mediaItem, int i) {
            }

            public static void $default$onMediaMetadataChanged(Listener listener, MediaMetadata mediaMetadata) {
            }

            public static void $default$onMetadata(Listener listener, Metadata metadata) {
            }

            public static void $default$onPlayWhenReadyChanged(Listener listener, boolean z, int i) {
            }

            public static void $default$onPlaybackParametersChanged(Listener listener, PlaybackParameters playbackParameters) {
            }

            public static void $default$onPlaybackStateChanged(Listener listener, int i) {
            }

            public static void $default$onPlaybackSuppressionReasonChanged(Listener listener, int i) {
            }

            public static void $default$onPlayerError(Listener listener, PlaybackException playbackException) {
            }

            public static void $default$onPlayerErrorChanged(Listener listener, PlaybackException playbackException) {
            }

            public static void $default$onPlaylistMetadataChanged(Listener listener, MediaMetadata mediaMetadata) {
            }

            public static void $default$onPositionDiscontinuity(Listener listener, PositionInfo positionInfo, PositionInfo positionInfo2, int i) {
            }

            public static void $default$onRenderedFirstFrame(Listener listener) {
            }

            public static void $default$onRepeatModeChanged(Listener listener, int i) {
            }

            public static void $default$onSeekBackIncrementChanged(Listener listener, long j) {
            }

            public static void $default$onSeekForwardIncrementChanged(Listener listener, long j) {
            }

            public static void $default$onShuffleModeEnabledChanged(Listener listener, boolean z) {
            }

            public static void $default$onSkipSilenceEnabledChanged(Listener listener, boolean z) {
            }

            public static void $default$onSurfaceSizeChanged(Listener listener, int i, int i2) {
            }

            public static void $default$onTimelineChanged(Listener listener, Timeline timeline, int i) {
            }

            public static void $default$onTracksChanged(Listener listener, TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
            }

            public static void $default$onVideoSizeChanged(Listener listener, VideoSize videoSize) {
            }

            public static void $default$onVolumeChanged(Listener listener, float f) {
            }
        }

        @Override // com.google.android.exoplayer2.audio.AudioListener
        void onAudioAttributesChanged(AudioAttributes audioAttributes);

        @Override // com.google.android.exoplayer2.text.TextOutput
        void onCues(List<Cue> list);

        @Override // com.google.android.exoplayer2.device.DeviceListener
        void onDeviceInfoChanged(DeviceInfo deviceInfo);

        @Override // com.google.android.exoplayer2.device.DeviceListener
        void onDeviceVolumeChanged(int i, boolean z);

        @Override // com.google.android.exoplayer2.metadata.MetadataOutput
        void onMetadata(Metadata metadata);

        @Override // com.google.android.exoplayer2.video.VideoListener
        void onRenderedFirstFrame();

        @Override // com.google.android.exoplayer2.audio.AudioListener
        void onSkipSilenceEnabledChanged(boolean z);

        @Override // com.google.android.exoplayer2.video.VideoListener
        void onSurfaceSizeChanged(int i, int i2);

        @Override // com.google.android.exoplayer2.video.VideoListener
        void onVideoSizeChanged(VideoSize videoSize);

        @Override // com.google.android.exoplayer2.audio.AudioListener
        void onVolumeChanged(float f);
    }

    @Deprecated
    void addListener(EventListener eventListener);

    void addListener(Listener listener);

    void addMediaItem(int i, MediaItem mediaItem);

    void addMediaItem(MediaItem mediaItem);

    void addMediaItems(int i, List<MediaItem> list);

    void addMediaItems(List<MediaItem> list);

    void clearMediaItems();

    void clearVideoSurface();

    void clearVideoSurface(Surface surface);

    void clearVideoSurfaceHolder(SurfaceHolder surfaceHolder);

    void clearVideoSurfaceView(SurfaceView surfaceView);

    void clearVideoTextureView(TextureView textureView);

    void decreaseDeviceVolume();

    Looper getApplicationLooper();

    AudioAttributes getAudioAttributes();

    Commands getAvailableCommands();

    int getBufferedPercentage();

    long getBufferedPosition();

    long getContentBufferedPosition();

    long getContentDuration();

    long getContentPosition();

    int getCurrentAdGroupIndex();

    int getCurrentAdIndexInAdGroup();

    List<Cue> getCurrentCues();

    long getCurrentLiveOffset();

    Object getCurrentManifest();

    MediaItem getCurrentMediaItem();

    int getCurrentPeriodIndex();

    long getCurrentPosition();

    @Deprecated
    List<Metadata> getCurrentStaticMetadata();

    Timeline getCurrentTimeline();

    TrackGroupArray getCurrentTrackGroups();

    TrackSelectionArray getCurrentTrackSelections();

    int getCurrentWindowIndex();

    DeviceInfo getDeviceInfo();

    int getDeviceVolume();

    long getDuration();

    int getMaxSeekToPreviousPosition();

    MediaItem getMediaItemAt(int i);

    int getMediaItemCount();

    MediaMetadata getMediaMetadata();

    int getNextWindowIndex();

    boolean getPlayWhenReady();

    PlaybackParameters getPlaybackParameters();

    int getPlaybackState();

    int getPlaybackSuppressionReason();

    PlaybackException getPlayerError();

    MediaMetadata getPlaylistMetadata();

    int getPreviousWindowIndex();

    int getRepeatMode();

    long getSeekBackIncrement();

    long getSeekForwardIncrement();

    boolean getShuffleModeEnabled();

    long getTotalBufferedDuration();

    VideoSize getVideoSize();

    float getVolume();

    @Deprecated
    boolean hasNext();

    boolean hasNextWindow();

    @Deprecated
    boolean hasPrevious();

    boolean hasPreviousWindow();

    void increaseDeviceVolume();

    boolean isCommandAvailable(int i);

    boolean isCurrentWindowDynamic();

    boolean isCurrentWindowLive();

    boolean isCurrentWindowSeekable();

    boolean isDeviceMuted();

    boolean isLoading();

    boolean isPlaying();

    boolean isPlayingAd();

    void moveMediaItem(int i, int i2);

    void moveMediaItems(int i, int i2, int i3);

    @Deprecated
    void next();

    void pause();

    void play();

    void prepare();

    @Deprecated
    void previous();

    void release();

    @Deprecated
    void removeListener(EventListener eventListener);

    void removeListener(Listener listener);

    void removeMediaItem(int i);

    void removeMediaItems(int i, int i2);

    void seekBack();

    void seekForward();

    void seekTo(int i, long j);

    void seekTo(long j);

    void seekToDefaultPosition();

    void seekToDefaultPosition(int i);

    void seekToNext();

    void seekToNextWindow();

    void seekToPrevious();

    void seekToPreviousWindow();

    void setDeviceMuted(boolean z);

    void setDeviceVolume(int i);

    void setMediaItem(MediaItem mediaItem);

    void setMediaItem(MediaItem mediaItem, long j);

    void setMediaItem(MediaItem mediaItem, boolean z);

    void setMediaItems(List<MediaItem> list);

    void setMediaItems(List<MediaItem> list, int i, long j);

    void setMediaItems(List<MediaItem> list, boolean z);

    void setPlayWhenReady(boolean z);

    void setPlaybackParameters(PlaybackParameters playbackParameters);

    void setPlaybackSpeed(float f);

    void setPlaylistMetadata(MediaMetadata mediaMetadata);

    void setRepeatMode(int i);

    void setShuffleModeEnabled(boolean z);

    void setVideoSurface(Surface surface);

    void setVideoSurfaceHolder(SurfaceHolder surfaceHolder);

    void setVideoSurfaceView(SurfaceView surfaceView);

    void setVideoTextureView(TextureView textureView);

    void setVolume(float f);

    void stop();

    @Deprecated
    void stop(boolean z);

    /* loaded from: classes.dex */
    public static final class Events {
        private final FlagSet flags;

        public Events(FlagSet flagSet) {
            this.flags = flagSet;
        }

        public boolean contains(int i) {
            return this.flags.contains(i);
        }

        public boolean containsAny(int... iArr) {
            return this.flags.containsAny(iArr);
        }

        public int hashCode() {
            return this.flags.hashCode();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Events)) {
                return false;
            }
            return this.flags.equals(((Events) obj).flags);
        }
    }

    /* loaded from: classes.dex */
    public static final class PositionInfo {
        public static final Bundleable$Creator<PositionInfo> CREATOR = new ExoPlaybackException$$ExternalSyntheticLambda0();
        public final int adGroupIndex;
        public final int adIndexInAdGroup;
        public final long contentPositionMs;
        public final int periodIndex;
        public final Object periodUid;
        public final long positionMs;
        public final int windowIndex;
        public final Object windowUid;

        public PositionInfo(Object obj, int i, Object obj2, int i2, long j, long j2, int i3, int i4) {
            this.windowUid = obj;
            this.windowIndex = i;
            this.periodUid = obj2;
            this.periodIndex = i2;
            this.positionMs = j;
            this.contentPositionMs = j2;
            this.adGroupIndex = i3;
            this.adIndexInAdGroup = i4;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || PositionInfo.class != obj.getClass()) {
                return false;
            }
            PositionInfo positionInfo = (PositionInfo) obj;
            if (this.windowIndex == positionInfo.windowIndex && this.periodIndex == positionInfo.periodIndex && this.positionMs == positionInfo.positionMs && this.contentPositionMs == positionInfo.contentPositionMs && this.adGroupIndex == positionInfo.adGroupIndex && this.adIndexInAdGroup == positionInfo.adIndexInAdGroup && Objects.equal(this.windowUid, positionInfo.windowUid) && Objects.equal(this.periodUid, positionInfo.periodUid)) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return Objects.hashCode(this.windowUid, Integer.valueOf(this.windowIndex), this.periodUid, Integer.valueOf(this.periodIndex), Integer.valueOf(this.windowIndex), Long.valueOf(this.positionMs), Long.valueOf(this.contentPositionMs), Integer.valueOf(this.adGroupIndex), Integer.valueOf(this.adIndexInAdGroup));
        }
    }

    /* loaded from: classes.dex */
    public static final class Commands {
        public static final Bundleable$Creator<Commands> CREATOR = new ExoPlaybackException$$ExternalSyntheticLambda0();
        public static final Commands EMPTY = new Builder().build();
        private final FlagSet flags;

        /* loaded from: classes.dex */
        public static final class Builder {
            private static final int[] SUPPORTED_COMMANDS = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27};
            private final FlagSet.Builder flagsBuilder = new FlagSet.Builder();

            public Builder add(int i) {
                this.flagsBuilder.add(i);
                return this;
            }

            public Builder addIf(int i, boolean z) {
                this.flagsBuilder.addIf(i, z);
                return this;
            }

            public Builder addAll(int... iArr) {
                this.flagsBuilder.addAll(iArr);
                return this;
            }

            public Builder addAll(Commands commands) {
                this.flagsBuilder.addAll(commands.flags);
                return this;
            }

            public Commands build() {
                return new Commands(this.flagsBuilder.build());
            }
        }

        private Commands(FlagSet flagSet) {
            this.flags = flagSet;
        }

        public boolean contains(int i) {
            return this.flags.contains(i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Commands)) {
                return false;
            }
            return this.flags.equals(((Commands) obj).flags);
        }

        public int hashCode() {
            return this.flags.hashCode();
        }
    }
}
