package com.google.android.exoplayer2.drm;

import com.google.android.exoplayer2.drm.DefaultDrmSessionManager;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class DefaultDrmSessionManager$PreacquiredSessionReference$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ DefaultDrmSessionManager.PreacquiredSessionReference f$0;

    public /* synthetic */ DefaultDrmSessionManager$PreacquiredSessionReference$$ExternalSyntheticLambda0(DefaultDrmSessionManager.PreacquiredSessionReference preacquiredSessionReference) {
        this.f$0 = preacquiredSessionReference;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$release$1();
    }
}
