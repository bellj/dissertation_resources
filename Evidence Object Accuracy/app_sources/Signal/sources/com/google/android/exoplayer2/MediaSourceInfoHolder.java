package com.google.android.exoplayer2;

/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public interface MediaSourceInfoHolder {
    Timeline getTimeline();

    Object getUid();
}
