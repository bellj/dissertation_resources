package com.google.android.exoplayer2.drm;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class DefaultDrmSessionManager$ReferenceCountListenerImpl$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ DefaultDrmSession f$0;

    public /* synthetic */ DefaultDrmSessionManager$ReferenceCountListenerImpl$$ExternalSyntheticLambda0(DefaultDrmSession defaultDrmSession) {
        this.f$0 = defaultDrmSession;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.release(null);
    }
}
