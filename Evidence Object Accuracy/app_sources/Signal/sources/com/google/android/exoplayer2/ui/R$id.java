package com.google.android.exoplayer2.ui;

/* loaded from: classes.dex */
public final class R$id {
    public static final int exo_ad_overlay;
    public static final int exo_artwork;
    public static final int exo_buffering;
    public static final int exo_content_frame;
    public static final int exo_controller;
    public static final int exo_controller_placeholder;
    public static final int exo_duration;
    public static final int exo_error_message;
    public static final int exo_ffwd;
    public static final int exo_next;
    public static final int exo_overlay;
    public static final int exo_pause;
    public static final int exo_play;
    public static final int exo_position;
    public static final int exo_prev;
    public static final int exo_progress;
    public static final int exo_progress_placeholder;
    public static final int exo_repeat_toggle;
    public static final int exo_rew;
    public static final int exo_shuffle;
    public static final int exo_shutter;
    public static final int exo_subtitles;
    public static final int exo_vr;
}
