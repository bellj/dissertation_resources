package com.google.android.exoplayer2.ui;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.media.session.MediaSession;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.media.session.MediaSessionCompat;
import android.widget.RemoteViews;
import androidx.core.app.NotificationBuilderWithBuilderAccessor;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.media.R$id;
import androidx.media.R$integer;
import androidx.media.R$layout;
import androidx.media.app.NotificationCompat$MediaStyle;
import com.google.android.exoplayer2.ControlDispatcher;
import com.google.android.exoplayer2.DefaultControlDispatcher;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.MediaMetadata;
import com.google.android.exoplayer2.PlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.device.DeviceInfo;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.NotificationUtil;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoListener;
import com.google.android.exoplayer2.video.VideoSize;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* loaded from: classes.dex */
public class PlayerNotificationManager {
    private static int instanceIdCounter;
    private int badgeIconType;
    private NotificationCompat.Builder builder;
    private List<NotificationCompat.Action> builderActions;
    private final String channelId;
    private int color;
    private boolean colorized;
    private final Context context;
    private ControlDispatcher controlDispatcher;
    private int currentNotificationTag;
    private final CustomActionReceiver customActionReceiver;
    private final Map<String, NotificationCompat.Action> customActions;
    private int defaults;
    private final PendingIntent dismissPendingIntent;
    private String groupKey;
    private final int instanceId;
    private final IntentFilter intentFilter;
    private boolean isNotificationStarted;
    private final Handler mainHandler;
    private final MediaDescriptionAdapter mediaDescriptionAdapter;
    private MediaSessionCompat.Token mediaSessionToken;
    private final NotificationBroadcastReceiver notificationBroadcastReceiver;
    private final int notificationId;
    private final NotificationListener notificationListener;
    private final NotificationManagerCompat notificationManager;
    private final Map<String, NotificationCompat.Action> playbackActions;
    private Player player;
    private final Player.Listener playerListener;
    private int priority;
    private int smallIconResourceId;
    private boolean useChronometer;
    private boolean useFastForwardAction;
    private boolean useFastForwardActionInCompactView;
    private boolean useNextAction;
    private boolean useNextActionInCompactView;
    private boolean usePlayPauseActions;
    private boolean usePreviousAction;
    private boolean usePreviousActionInCompactView;
    private boolean useRewindAction;
    private boolean useRewindActionInCompactView;
    private boolean useStopAction;
    private int visibility;

    /* loaded from: classes.dex */
    public interface CustomActionReceiver {
        Map<String, NotificationCompat.Action> createCustomActions(Context context, int i);

        List<String> getCustomActions(Player player);

        void onCustomAction(Player player, String str, Intent intent);
    }

    /* loaded from: classes.dex */
    public interface MediaDescriptionAdapter {

        /* renamed from: com.google.android.exoplayer2.ui.PlayerNotificationManager$MediaDescriptionAdapter$-CC */
        /* loaded from: classes.dex */
        public final /* synthetic */ class CC {
            public static CharSequence $default$getCurrentSubText(MediaDescriptionAdapter mediaDescriptionAdapter, Player player) {
                return null;
            }
        }

        PendingIntent createCurrentContentIntent(Player player);

        CharSequence getCurrentContentText(Player player);

        CharSequence getCurrentContentTitle(Player player);

        Bitmap getCurrentLargeIcon(Player player, BitmapCallback bitmapCallback);

        CharSequence getCurrentSubText(Player player);
    }

    /* loaded from: classes.dex */
    public interface NotificationListener {
        void onNotificationCancelled(int i, boolean z);

        void onNotificationPosted(int i, Notification notification, boolean z);
    }

    /* loaded from: classes.dex */
    public static class Builder {
        private int channelDescriptionResourceId;
        private final String channelId;
        private int channelImportance;
        private int channelNameResourceId;
        private final Context context;
        private CustomActionReceiver customActionReceiver;
        private int fastForwardActionIconResourceId;
        private String groupKey;
        private MediaDescriptionAdapter mediaDescriptionAdapter;
        private int nextActionIconResourceId;
        private final int notificationId;
        private NotificationListener notificationListener;
        private int pauseActionIconResourceId;
        private int playActionIconResourceId;
        private int previousActionIconResourceId;
        private int rewindActionIconResourceId;
        private int smallIconResourceId;
        private int stopActionIconResourceId;

        public Builder(Context context, int i, String str) {
            Assertions.checkArgument(i > 0);
            this.context = context;
            this.notificationId = i;
            this.channelId = str;
            this.channelImportance = 2;
            this.mediaDescriptionAdapter = new DefaultMediaDescriptionAdapter(null);
            this.smallIconResourceId = R$drawable.exo_notification_small_icon;
            this.playActionIconResourceId = R$drawable.exo_notification_play;
            this.pauseActionIconResourceId = R$drawable.exo_notification_pause;
            this.stopActionIconResourceId = R$drawable.exo_notification_stop;
            this.rewindActionIconResourceId = R$drawable.exo_notification_rewind;
            this.fastForwardActionIconResourceId = R$drawable.exo_notification_fastforward;
            this.previousActionIconResourceId = R$drawable.exo_notification_previous;
            this.nextActionIconResourceId = R$drawable.exo_notification_next;
        }

        public Builder setChannelNameResourceId(int i) {
            this.channelNameResourceId = i;
            return this;
        }

        public Builder setNotificationListener(NotificationListener notificationListener) {
            this.notificationListener = notificationListener;
            return this;
        }

        public Builder setMediaDescriptionAdapter(MediaDescriptionAdapter mediaDescriptionAdapter) {
            this.mediaDescriptionAdapter = mediaDescriptionAdapter;
            return this;
        }

        public PlayerNotificationManager build() {
            int i = this.channelNameResourceId;
            if (i != 0) {
                NotificationUtil.createNotificationChannel(this.context, this.channelId, i, this.channelDescriptionResourceId, this.channelImportance);
            }
            return new PlayerNotificationManager(this.context, this.channelId, this.notificationId, this.mediaDescriptionAdapter, this.notificationListener, this.customActionReceiver, this.smallIconResourceId, this.playActionIconResourceId, this.pauseActionIconResourceId, this.stopActionIconResourceId, this.rewindActionIconResourceId, this.fastForwardActionIconResourceId, this.previousActionIconResourceId, this.nextActionIconResourceId, this.groupKey);
        }
    }

    /* loaded from: classes.dex */
    public final class BitmapCallback {
        private final int notificationTag;

        private BitmapCallback(int i) {
            PlayerNotificationManager.this = r1;
            this.notificationTag = i;
        }

        public void onBitmap(Bitmap bitmap) {
            if (bitmap != null) {
                PlayerNotificationManager.this.postUpdateNotificationBitmap(bitmap, this.notificationTag);
            }
        }
    }

    private PlayerNotificationManager(Context context, String str, int i, MediaDescriptionAdapter mediaDescriptionAdapter, NotificationListener notificationListener, CustomActionReceiver customActionReceiver, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, String str2) {
        Map<String, NotificationCompat.Action> map;
        Context applicationContext = context.getApplicationContext();
        this.context = applicationContext;
        this.channelId = str;
        this.notificationId = i;
        this.mediaDescriptionAdapter = mediaDescriptionAdapter;
        this.notificationListener = notificationListener;
        this.customActionReceiver = customActionReceiver;
        this.smallIconResourceId = i2;
        this.groupKey = str2;
        this.controlDispatcher = new DefaultControlDispatcher();
        int i10 = instanceIdCounter;
        instanceIdCounter = i10 + 1;
        this.instanceId = i10;
        this.mainHandler = Util.createHandler(Looper.getMainLooper(), new Handler.Callback() { // from class: com.google.android.exoplayer2.ui.PlayerNotificationManager$$ExternalSyntheticLambda0
            @Override // android.os.Handler.Callback
            public final boolean handleMessage(Message message) {
                return PlayerNotificationManager.this.handleMessage(message);
            }
        });
        this.notificationManager = NotificationManagerCompat.from(applicationContext);
        this.playerListener = new PlayerListener();
        this.notificationBroadcastReceiver = new NotificationBroadcastReceiver();
        this.intentFilter = new IntentFilter();
        this.usePreviousAction = true;
        this.useNextAction = true;
        this.usePlayPauseActions = true;
        this.useRewindAction = true;
        this.useFastForwardAction = true;
        this.colorized = true;
        this.useChronometer = true;
        this.color = 0;
        this.defaults = 0;
        this.priority = -1;
        this.badgeIconType = 1;
        this.visibility = 1;
        Map<String, NotificationCompat.Action> createPlaybackActions = createPlaybackActions(applicationContext, i10, i3, i4, i5, i6, i7, i8, i9);
        this.playbackActions = createPlaybackActions;
        for (String str3 : createPlaybackActions.keySet()) {
            this.intentFilter.addAction(str3);
        }
        if (customActionReceiver != null) {
            map = customActionReceiver.createCustomActions(applicationContext, this.instanceId);
        } else {
            map = Collections.emptyMap();
        }
        this.customActions = map;
        for (String str4 : map.keySet()) {
            this.intentFilter.addAction(str4);
        }
        this.dismissPendingIntent = createBroadcastIntent("com.google.android.exoplayer.dismiss", applicationContext, this.instanceId);
        this.intentFilter.addAction("com.google.android.exoplayer.dismiss");
    }

    public final void setPlayer(Player player) {
        boolean z = true;
        Assertions.checkState(Looper.myLooper() == Looper.getMainLooper());
        if (!(player == null || player.getApplicationLooper() == Looper.getMainLooper())) {
            z = false;
        }
        Assertions.checkArgument(z);
        Player player2 = this.player;
        if (player2 != player) {
            if (player2 != null) {
                player2.removeListener(this.playerListener);
                if (player == null) {
                    stopNotification(false);
                }
            }
            this.player = player;
            if (player != null) {
                player.addListener(this.playerListener);
                postStartOrUpdateNotification();
            }
        }
    }

    public void setUseFastForwardAction(boolean z) {
        if (this.useFastForwardAction != z) {
            this.useFastForwardAction = z;
            invalidate();
        }
    }

    public void setUseRewindAction(boolean z) {
        if (this.useRewindAction != z) {
            this.useRewindAction = z;
            invalidate();
        }
    }

    public final void setUseStopAction(boolean z) {
        if (this.useStopAction != z) {
            this.useStopAction = z;
            invalidate();
        }
    }

    public final void setMediaSessionToken(MediaSessionCompat.Token token) {
        if (!Util.areEqual(this.mediaSessionToken, token)) {
            this.mediaSessionToken = token;
            invalidate();
        }
    }

    public final void setColorized(boolean z) {
        if (this.colorized != z) {
            this.colorized = z;
            invalidate();
        }
    }

    public final void setColor(int i) {
        if (this.color != i) {
            this.color = i;
            invalidate();
        }
    }

    public final void setSmallIcon(int i) {
        if (this.smallIconResourceId != i) {
            this.smallIconResourceId = i;
            invalidate();
        }
    }

    public void invalidate() {
        if (this.isNotificationStarted) {
            postStartOrUpdateNotification();
        }
    }

    private void startOrUpdateNotification(Player player, Bitmap bitmap) {
        boolean ongoing = getOngoing(player);
        NotificationCompat.Builder createNotification = createNotification(player, this.builder, ongoing, bitmap);
        this.builder = createNotification;
        boolean z = false;
        if (createNotification == null) {
            stopNotification(false);
            return;
        }
        Notification build = createNotification.build();
        this.notificationManager.notify(this.notificationId, build);
        if (!this.isNotificationStarted) {
            this.context.registerReceiver(this.notificationBroadcastReceiver, this.intentFilter);
        }
        NotificationListener notificationListener = this.notificationListener;
        if (notificationListener != null) {
            int i = this.notificationId;
            if (ongoing || !this.isNotificationStarted) {
                z = true;
            }
            notificationListener.onNotificationPosted(i, build, z);
        }
        this.isNotificationStarted = true;
    }

    public void stopNotification(boolean z) {
        if (this.isNotificationStarted) {
            this.isNotificationStarted = false;
            this.mainHandler.removeMessages(0);
            this.notificationManager.cancel(this.notificationId);
            this.context.unregisterReceiver(this.notificationBroadcastReceiver);
            NotificationListener notificationListener = this.notificationListener;
            if (notificationListener != null) {
                notificationListener.onNotificationCancelled(this.notificationId, z);
            }
        }
    }

    protected NotificationCompat.Builder createNotification(Player player, NotificationCompat.Builder builder, boolean z, Bitmap bitmap) {
        NotificationCompat.Action action;
        if (player.getPlaybackState() != 1 || !player.getCurrentTimeline().isEmpty()) {
            List<String> actions = getActions(player);
            ArrayList arrayList = new ArrayList(actions.size());
            for (int i = 0; i < actions.size(); i++) {
                String str = actions.get(i);
                if (this.playbackActions.containsKey(str)) {
                    action = this.playbackActions.get(str);
                } else {
                    action = this.customActions.get(str);
                }
                if (action != null) {
                    arrayList.add(action);
                }
            }
            if (builder == null || !arrayList.equals(this.builderActions)) {
                builder = new NotificationCompat.Builder(this.context, this.channelId);
                this.builderActions = arrayList;
                for (int i2 = 0; i2 < arrayList.size(); i2++) {
                    builder.addAction((NotificationCompat.Action) arrayList.get(i2));
                }
            }
            NotificationCompat$MediaStyle notificationCompat$MediaStyle = new NotificationCompat.Style() { // from class: androidx.media.app.NotificationCompat$MediaStyle
                int[] mActionsToShowInCompact = null;
                PendingIntent mCancelButtonIntent;
                boolean mShowCancelButton;
                MediaSessionCompat.Token mToken;

                public NotificationCompat$MediaStyle setShowActionsInCompactView(int... iArr) {
                    this.mActionsToShowInCompact = iArr;
                    return this;
                }

                public NotificationCompat$MediaStyle setMediaSession(MediaSessionCompat.Token token) {
                    this.mToken = token;
                    return this;
                }

                public NotificationCompat$MediaStyle setShowCancelButton(boolean z2) {
                    if (Build.VERSION.SDK_INT < 21) {
                        this.mShowCancelButton = z2;
                    }
                    return this;
                }

                public NotificationCompat$MediaStyle setCancelButtonIntent(PendingIntent pendingIntent) {
                    this.mCancelButtonIntent = pendingIntent;
                    return this;
                }

                @Override // androidx.core.app.NotificationCompat.Style
                public void apply(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
                    if (Build.VERSION.SDK_INT >= 21) {
                        notificationBuilderWithBuilderAccessor.getBuilder().setStyle(fillInMediaStyle(new Notification.MediaStyle()));
                    } else if (this.mShowCancelButton) {
                        notificationBuilderWithBuilderAccessor.getBuilder().setOngoing(true);
                    }
                }

                Notification.MediaStyle fillInMediaStyle(Notification.MediaStyle mediaStyle) {
                    int[] iArr = this.mActionsToShowInCompact;
                    if (iArr != null) {
                        mediaStyle.setShowActionsInCompactView(iArr);
                    }
                    MediaSessionCompat.Token token = this.mToken;
                    if (token != null) {
                        mediaStyle.setMediaSession((MediaSession.Token) token.getToken());
                    }
                    return mediaStyle;
                }

                @Override // androidx.core.app.NotificationCompat.Style
                public RemoteViews makeContentView(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
                    if (Build.VERSION.SDK_INT >= 21) {
                        return null;
                    }
                    return generateContentView();
                }

                RemoteViews generateContentView() {
                    int i3;
                    RemoteViews applyStandardTemplate = applyStandardTemplate(false, getContentViewLayoutResource(), true);
                    int size = this.mBuilder.mActions.size();
                    int[] iArr = this.mActionsToShowInCompact;
                    if (iArr == null) {
                        i3 = 0;
                    } else {
                        i3 = Math.min(iArr.length, 3);
                    }
                    applyStandardTemplate.removeAllViews(R$id.media_actions);
                    if (i3 > 0) {
                        for (int i4 = 0; i4 < i3; i4++) {
                            if (i4 < size) {
                                applyStandardTemplate.addView(R$id.media_actions, generateMediaActionButton(this.mBuilder.mActions.get(this.mActionsToShowInCompact[i4])));
                            } else {
                                throw new IllegalArgumentException(String.format("setShowActionsInCompactView: action %d out of bounds (max %d)", Integer.valueOf(i4), Integer.valueOf(size - 1)));
                            }
                        }
                    }
                    if (this.mShowCancelButton) {
                        applyStandardTemplate.setViewVisibility(R$id.end_padder, 8);
                        int i5 = R$id.cancel_action;
                        applyStandardTemplate.setViewVisibility(i5, 0);
                        applyStandardTemplate.setOnClickPendingIntent(i5, this.mCancelButtonIntent);
                        applyStandardTemplate.setInt(i5, "setAlpha", this.mBuilder.mContext.getResources().getInteger(R$integer.cancel_button_image_alpha));
                    } else {
                        applyStandardTemplate.setViewVisibility(R$id.end_padder, 0);
                        applyStandardTemplate.setViewVisibility(R$id.cancel_action, 8);
                    }
                    return applyStandardTemplate;
                }

                private RemoteViews generateMediaActionButton(NotificationCompat.Action action2) {
                    boolean z2 = action2.getActionIntent() == null;
                    RemoteViews remoteViews = new RemoteViews(this.mBuilder.mContext.getPackageName(), R$layout.notification_media_action);
                    int i3 = R$id.action0;
                    remoteViews.setImageViewResource(i3, action2.getIcon());
                    if (!z2) {
                        remoteViews.setOnClickPendingIntent(i3, action2.getActionIntent());
                    }
                    remoteViews.setContentDescription(i3, action2.getTitle());
                    return remoteViews;
                }

                int getContentViewLayoutResource() {
                    return R$layout.notification_template_media;
                }

                @Override // androidx.core.app.NotificationCompat.Style
                public RemoteViews makeBigContentView(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor) {
                    if (Build.VERSION.SDK_INT >= 21) {
                        return null;
                    }
                    return generateBigContentView();
                }

                RemoteViews generateBigContentView() {
                    int min = Math.min(this.mBuilder.mActions.size(), 5);
                    RemoteViews applyStandardTemplate = applyStandardTemplate(false, getBigContentViewLayoutResource(min), false);
                    applyStandardTemplate.removeAllViews(R$id.media_actions);
                    if (min > 0) {
                        for (int i3 = 0; i3 < min; i3++) {
                            applyStandardTemplate.addView(R$id.media_actions, generateMediaActionButton(this.mBuilder.mActions.get(i3)));
                        }
                    }
                    if (this.mShowCancelButton) {
                        int i4 = R$id.cancel_action;
                        applyStandardTemplate.setViewVisibility(i4, 0);
                        applyStandardTemplate.setInt(i4, "setAlpha", this.mBuilder.mContext.getResources().getInteger(R$integer.cancel_button_image_alpha));
                        applyStandardTemplate.setOnClickPendingIntent(i4, this.mCancelButtonIntent);
                    } else {
                        applyStandardTemplate.setViewVisibility(R$id.cancel_action, 8);
                    }
                    return applyStandardTemplate;
                }

                int getBigContentViewLayoutResource(int i3) {
                    if (i3 <= 3) {
                        return R$layout.notification_template_big_media_narrow;
                    }
                    return R$layout.notification_template_big_media;
                }
            };
            MediaSessionCompat.Token token = this.mediaSessionToken;
            if (token != null) {
                notificationCompat$MediaStyle.setMediaSession(token);
            }
            notificationCompat$MediaStyle.setShowActionsInCompactView(getActionIndicesForCompactView(actions, player));
            notificationCompat$MediaStyle.setShowCancelButton(!z);
            notificationCompat$MediaStyle.setCancelButtonIntent(this.dismissPendingIntent);
            builder.setStyle(notificationCompat$MediaStyle);
            builder.setDeleteIntent(this.dismissPendingIntent);
            builder.setBadgeIconType(this.badgeIconType).setOngoing(z).setColor(this.color).setColorized(this.colorized).setSmallIcon(this.smallIconResourceId).setVisibility(this.visibility).setPriority(this.priority).setDefaults(this.defaults);
            if (Util.SDK_INT < 21 || !this.useChronometer || !player.isPlaying() || player.isPlayingAd() || player.isCurrentWindowDynamic() || player.getPlaybackParameters().speed != 1.0f) {
                builder.setShowWhen(false).setUsesChronometer(false);
            } else {
                builder.setWhen(System.currentTimeMillis() - player.getContentPosition()).setShowWhen(true).setUsesChronometer(true);
            }
            builder.setContentTitle(this.mediaDescriptionAdapter.getCurrentContentTitle(player));
            builder.setContentText(this.mediaDescriptionAdapter.getCurrentContentText(player));
            builder.setSubText(this.mediaDescriptionAdapter.getCurrentSubText(player));
            if (bitmap == null) {
                MediaDescriptionAdapter mediaDescriptionAdapter = this.mediaDescriptionAdapter;
                int i3 = this.currentNotificationTag + 1;
                this.currentNotificationTag = i3;
                bitmap = mediaDescriptionAdapter.getCurrentLargeIcon(player, new BitmapCallback(i3));
            }
            setLargeIcon(builder, bitmap);
            builder.setContentIntent(this.mediaDescriptionAdapter.createCurrentContentIntent(player));
            String str2 = this.groupKey;
            if (str2 != null) {
                builder.setGroup(str2);
            }
            builder.setOnlyAlertOnce(true);
            return builder;
        }
        this.builderActions = null;
        return null;
    }

    protected List<String> getActions(Player player) {
        boolean isCommandAvailable = player.isCommandAvailable(6);
        boolean z = true;
        boolean z2 = player.isCommandAvailable(10) && this.controlDispatcher.isRewindEnabled();
        if (!player.isCommandAvailable(11) || !this.controlDispatcher.isFastForwardEnabled()) {
            z = false;
        }
        boolean isCommandAvailable2 = player.isCommandAvailable(8);
        ArrayList arrayList = new ArrayList();
        if (this.usePreviousAction && isCommandAvailable) {
            arrayList.add("com.google.android.exoplayer.prev");
        }
        if (this.useRewindAction && z2) {
            arrayList.add("com.google.android.exoplayer.rewind");
        }
        if (this.usePlayPauseActions) {
            if (shouldShowPauseButton(player)) {
                arrayList.add("com.google.android.exoplayer.pause");
            } else {
                arrayList.add("com.google.android.exoplayer.play");
            }
        }
        if (this.useFastForwardAction && z) {
            arrayList.add("com.google.android.exoplayer.ffwd");
        }
        if (this.useNextAction && isCommandAvailable2) {
            arrayList.add("com.google.android.exoplayer.next");
        }
        CustomActionReceiver customActionReceiver = this.customActionReceiver;
        if (customActionReceiver != null) {
            arrayList.addAll(customActionReceiver.getCustomActions(player));
        }
        if (this.useStopAction) {
            arrayList.add("com.google.android.exoplayer.stop");
        }
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x005d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected int[] getActionIndicesForCompactView(java.util.List<java.lang.String> r7, com.google.android.exoplayer2.Player r8) {
        /*
            r6 = this;
            java.lang.String r0 = "com.google.android.exoplayer.pause"
            int r0 = r7.indexOf(r0)
            java.lang.String r1 = "com.google.android.exoplayer.play"
            int r1 = r7.indexOf(r1)
            boolean r2 = r6.usePreviousActionInCompactView
            r3 = -1
            if (r2 == 0) goto L_0x0018
            java.lang.String r2 = "com.google.android.exoplayer.prev"
            int r2 = r7.indexOf(r2)
            goto L_0x0024
        L_0x0018:
            boolean r2 = r6.useRewindActionInCompactView
            if (r2 == 0) goto L_0x0023
            java.lang.String r2 = "com.google.android.exoplayer.rewind"
            int r2 = r7.indexOf(r2)
            goto L_0x0024
        L_0x0023:
            r2 = -1
        L_0x0024:
            boolean r4 = r6.useNextActionInCompactView
            if (r4 == 0) goto L_0x002f
            java.lang.String r4 = "com.google.android.exoplayer.next"
            int r7 = r7.indexOf(r4)
            goto L_0x003b
        L_0x002f:
            boolean r4 = r6.useFastForwardActionInCompactView
            if (r4 == 0) goto L_0x003a
            java.lang.String r4 = "com.google.android.exoplayer.ffwd"
            int r7 = r7.indexOf(r4)
            goto L_0x003b
        L_0x003a:
            r7 = -1
        L_0x003b:
            r4 = 3
            int[] r4 = new int[r4]
            r5 = 0
            if (r2 == r3) goto L_0x0044
            r4[r5] = r2
            r5 = 1
        L_0x0044:
            boolean r8 = r6.shouldShowPauseButton(r8)
            if (r0 == r3) goto L_0x0052
            if (r8 == 0) goto L_0x0052
            int r8 = r5 + 1
            r4[r5] = r0
        L_0x0050:
            r5 = r8
            goto L_0x005b
        L_0x0052:
            if (r1 == r3) goto L_0x005b
            if (r8 != 0) goto L_0x005b
            int r8 = r5 + 1
            r4[r5] = r1
            goto L_0x0050
        L_0x005b:
            if (r7 == r3) goto L_0x0062
            int r8 = r5 + 1
            r4[r5] = r7
            r5 = r8
        L_0x0062:
            int[] r7 = java.util.Arrays.copyOf(r4, r5)
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.ui.PlayerNotificationManager.getActionIndicesForCompactView(java.util.List, com.google.android.exoplayer2.Player):int[]");
    }

    protected boolean getOngoing(Player player) {
        int playbackState = player.getPlaybackState();
        return (playbackState == 2 || playbackState == 3) && player.getPlayWhenReady();
    }

    private boolean shouldShowPauseButton(Player player) {
        if (player.getPlaybackState() == 4 || player.getPlaybackState() == 1 || !player.getPlayWhenReady()) {
            return false;
        }
        return true;
    }

    public void postStartOrUpdateNotification() {
        if (!this.mainHandler.hasMessages(0)) {
            this.mainHandler.sendEmptyMessage(0);
        }
    }

    public void postUpdateNotificationBitmap(Bitmap bitmap, int i) {
        this.mainHandler.obtainMessage(1, i, -1, bitmap).sendToTarget();
    }

    public boolean handleMessage(Message message) {
        int i = message.what;
        if (i == 0) {
            Player player = this.player;
            if (player != null) {
                startOrUpdateNotification(player, null);
            }
        } else if (i != 1) {
            return false;
        } else {
            Player player2 = this.player;
            if (player2 != null && this.isNotificationStarted && this.currentNotificationTag == message.arg1) {
                startOrUpdateNotification(player2, (Bitmap) message.obj);
            }
        }
        return true;
    }

    private static Map<String, NotificationCompat.Action> createPlaybackActions(Context context, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        HashMap hashMap = new HashMap();
        hashMap.put("com.google.android.exoplayer.play", new NotificationCompat.Action(i2, context.getString(R$string.exo_controls_play_description), createBroadcastIntent("com.google.android.exoplayer.play", context, i)));
        hashMap.put("com.google.android.exoplayer.pause", new NotificationCompat.Action(i3, context.getString(R$string.exo_controls_pause_description), createBroadcastIntent("com.google.android.exoplayer.pause", context, i)));
        hashMap.put("com.google.android.exoplayer.stop", new NotificationCompat.Action(i4, context.getString(R$string.exo_controls_stop_description), createBroadcastIntent("com.google.android.exoplayer.stop", context, i)));
        hashMap.put("com.google.android.exoplayer.rewind", new NotificationCompat.Action(i5, context.getString(R$string.exo_controls_rewind_description), createBroadcastIntent("com.google.android.exoplayer.rewind", context, i)));
        hashMap.put("com.google.android.exoplayer.ffwd", new NotificationCompat.Action(i6, context.getString(R$string.exo_controls_fastforward_description), createBroadcastIntent("com.google.android.exoplayer.ffwd", context, i)));
        hashMap.put("com.google.android.exoplayer.prev", new NotificationCompat.Action(i7, context.getString(R$string.exo_controls_previous_description), createBroadcastIntent("com.google.android.exoplayer.prev", context, i)));
        hashMap.put("com.google.android.exoplayer.next", new NotificationCompat.Action(i8, context.getString(R$string.exo_controls_next_description), createBroadcastIntent("com.google.android.exoplayer.next", context, i)));
        return hashMap;
    }

    private static PendingIntent createBroadcastIntent(String str, Context context, int i) {
        Intent intent = new Intent(str).setPackage(context.getPackageName());
        intent.putExtra("INSTANCE_ID", i);
        return PendingIntent.getBroadcast(context, i, intent, Util.SDK_INT >= 23 ? 201326592 : 134217728);
    }

    private static void setLargeIcon(NotificationCompat.Builder builder, Bitmap bitmap) {
        builder.setLargeIcon(bitmap);
    }

    /* access modifiers changed from: private */
    /* loaded from: classes.dex */
    public class PlayerListener implements Player.Listener {
        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.audio.AudioListener
        public /* synthetic */ void onAudioAttributesChanged(AudioAttributes audioAttributes) {
            Player.Listener.CC.$default$onAudioAttributesChanged(this, audioAttributes);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* synthetic */ void onAvailableCommandsChanged(Player.Commands commands) {
            Player.Listener.CC.$default$onAvailableCommandsChanged(this, commands);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.text.TextOutput
        public /* synthetic */ void onCues(List list) {
            Player.Listener.CC.$default$onCues(this, list);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.device.DeviceListener
        public /* synthetic */ void onDeviceInfoChanged(DeviceInfo deviceInfo) {
            Player.Listener.CC.$default$onDeviceInfoChanged(this, deviceInfo);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.device.DeviceListener
        public /* synthetic */ void onDeviceVolumeChanged(int i, boolean z) {
            Player.Listener.CC.$default$onDeviceVolumeChanged(this, i, z);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* synthetic */ void onIsLoadingChanged(boolean z) {
            Player.Listener.CC.$default$onIsLoadingChanged(this, z);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* synthetic */ void onIsPlayingChanged(boolean z) {
            Player.Listener.CC.$default$onIsPlayingChanged(this, z);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* synthetic */ void onLoadingChanged(boolean z) {
            Player.EventListener.CC.$default$onLoadingChanged(this, z);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* synthetic */ void onMediaItemTransition(MediaItem mediaItem, int i) {
            Player.Listener.CC.$default$onMediaItemTransition(this, mediaItem, i);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* synthetic */ void onMediaMetadataChanged(MediaMetadata mediaMetadata) {
            Player.Listener.CC.$default$onMediaMetadataChanged(this, mediaMetadata);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.metadata.MetadataOutput
        public /* synthetic */ void onMetadata(Metadata metadata) {
            Player.Listener.CC.$default$onMetadata(this, metadata);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* synthetic */ void onPlayWhenReadyChanged(boolean z, int i) {
            Player.Listener.CC.$default$onPlayWhenReadyChanged(this, z, i);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* synthetic */ void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
            Player.Listener.CC.$default$onPlaybackParametersChanged(this, playbackParameters);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* synthetic */ void onPlaybackStateChanged(int i) {
            Player.Listener.CC.$default$onPlaybackStateChanged(this, i);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* synthetic */ void onPlaybackSuppressionReasonChanged(int i) {
            Player.Listener.CC.$default$onPlaybackSuppressionReasonChanged(this, i);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* synthetic */ void onPlayerError(PlaybackException playbackException) {
            Player.Listener.CC.$default$onPlayerError(this, playbackException);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* synthetic */ void onPlayerErrorChanged(PlaybackException playbackException) {
            Player.Listener.CC.$default$onPlayerErrorChanged(this, playbackException);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* synthetic */ void onPlayerStateChanged(boolean z, int i) {
            Player.EventListener.CC.$default$onPlayerStateChanged(this, z, i);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* synthetic */ void onPlaylistMetadataChanged(MediaMetadata mediaMetadata) {
            Player.Listener.CC.$default$onPlaylistMetadataChanged(this, mediaMetadata);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* synthetic */ void onPositionDiscontinuity(int i) {
            Player.EventListener.CC.$default$onPositionDiscontinuity(this, i);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* synthetic */ void onPositionDiscontinuity(Player.PositionInfo positionInfo, Player.PositionInfo positionInfo2, int i) {
            Player.Listener.CC.$default$onPositionDiscontinuity(this, positionInfo, positionInfo2, i);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.video.VideoListener
        public /* synthetic */ void onRenderedFirstFrame() {
            Player.Listener.CC.$default$onRenderedFirstFrame(this);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* synthetic */ void onRepeatModeChanged(int i) {
            Player.Listener.CC.$default$onRepeatModeChanged(this, i);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* synthetic */ void onSeekProcessed() {
            Player.EventListener.CC.$default$onSeekProcessed(this);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* synthetic */ void onShuffleModeEnabledChanged(boolean z) {
            Player.Listener.CC.$default$onShuffleModeEnabledChanged(this, z);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.audio.AudioListener
        public /* synthetic */ void onSkipSilenceEnabledChanged(boolean z) {
            Player.Listener.CC.$default$onSkipSilenceEnabledChanged(this, z);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* synthetic */ void onStaticMetadataChanged(List list) {
            Player.EventListener.CC.$default$onStaticMetadataChanged(this, list);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.video.VideoListener
        public /* synthetic */ void onSurfaceSizeChanged(int i, int i2) {
            Player.Listener.CC.$default$onSurfaceSizeChanged(this, i, i2);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* synthetic */ void onTimelineChanged(Timeline timeline, int i) {
            Player.Listener.CC.$default$onTimelineChanged(this, timeline, i);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* synthetic */ void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
            Player.Listener.CC.$default$onTracksChanged(this, trackGroupArray, trackSelectionArray);
        }

        @Override // com.google.android.exoplayer2.video.VideoListener
        public /* synthetic */ void onVideoSizeChanged(int i, int i2, int i3, float f) {
            VideoListener.CC.$default$onVideoSizeChanged(this, i, i2, i3, f);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.video.VideoListener
        public /* synthetic */ void onVideoSizeChanged(VideoSize videoSize) {
            Player.Listener.CC.$default$onVideoSizeChanged(this, videoSize);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.audio.AudioListener
        public /* synthetic */ void onVolumeChanged(float f) {
            Player.Listener.CC.$default$onVolumeChanged(this, f);
        }

        private PlayerListener() {
            PlayerNotificationManager.this = r1;
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public void onEvents(Player player, Player.Events events) {
            if (events.containsAny(5, 6, 8, 0, 13, 12, 9, 10, 15)) {
                PlayerNotificationManager.this.postStartOrUpdateNotification();
            }
        }
    }

    /* loaded from: classes.dex */
    public class NotificationBroadcastReceiver extends BroadcastReceiver {
        private NotificationBroadcastReceiver() {
            PlayerNotificationManager.this = r1;
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            Player player = PlayerNotificationManager.this.player;
            if (player != null && PlayerNotificationManager.this.isNotificationStarted && intent.getIntExtra("INSTANCE_ID", PlayerNotificationManager.this.instanceId) == PlayerNotificationManager.this.instanceId) {
                String action = intent.getAction();
                if ("com.google.android.exoplayer.play".equals(action)) {
                    if (player.getPlaybackState() == 1) {
                        PlayerNotificationManager.this.controlDispatcher.dispatchPrepare(player);
                    } else if (player.getPlaybackState() == 4) {
                        PlayerNotificationManager.this.controlDispatcher.dispatchSeekTo(player, player.getCurrentWindowIndex(), -9223372036854775807L);
                    }
                    PlayerNotificationManager.this.controlDispatcher.dispatchSetPlayWhenReady(player, true);
                } else if ("com.google.android.exoplayer.pause".equals(action)) {
                    PlayerNotificationManager.this.controlDispatcher.dispatchSetPlayWhenReady(player, false);
                } else if ("com.google.android.exoplayer.prev".equals(action)) {
                    PlayerNotificationManager.this.controlDispatcher.dispatchPrevious(player);
                } else if ("com.google.android.exoplayer.rewind".equals(action)) {
                    PlayerNotificationManager.this.controlDispatcher.dispatchRewind(player);
                } else if ("com.google.android.exoplayer.ffwd".equals(action)) {
                    PlayerNotificationManager.this.controlDispatcher.dispatchFastForward(player);
                } else if ("com.google.android.exoplayer.next".equals(action)) {
                    PlayerNotificationManager.this.controlDispatcher.dispatchNext(player);
                } else if ("com.google.android.exoplayer.stop".equals(action)) {
                    PlayerNotificationManager.this.controlDispatcher.dispatchStop(player, true);
                } else if ("com.google.android.exoplayer.dismiss".equals(action)) {
                    PlayerNotificationManager.this.stopNotification(true);
                } else if (action != null && PlayerNotificationManager.this.customActionReceiver != null && PlayerNotificationManager.this.customActions.containsKey(action)) {
                    PlayerNotificationManager.this.customActionReceiver.onCustomAction(player, action, intent);
                }
            }
        }
    }
}
