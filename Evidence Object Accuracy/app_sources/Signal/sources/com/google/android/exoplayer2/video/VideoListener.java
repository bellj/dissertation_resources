package com.google.android.exoplayer2.video;

@Deprecated
/* loaded from: classes.dex */
public interface VideoListener {

    /* renamed from: com.google.android.exoplayer2.video.VideoListener$-CC */
    /* loaded from: classes.dex */
    public final /* synthetic */ class CC {
        @Deprecated
        public static void $default$onVideoSizeChanged(VideoListener videoListener, int i, int i2, int i3, float f) {
        }
    }

    void onRenderedFirstFrame();

    void onSurfaceSizeChanged(int i, int i2);

    @Deprecated
    void onVideoSizeChanged(int i, int i2, int i3, float f);

    void onVideoSizeChanged(VideoSize videoSize);
}
