package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.MediaItem;
import java.util.List;

/* loaded from: classes.dex */
public interface MediaSourceFactory {

    /* renamed from: com.google.android.exoplayer2.source.MediaSourceFactory$-CC */
    /* loaded from: classes.dex */
    public final /* synthetic */ class CC {
        @Deprecated
        public static MediaSourceFactory $default$setStreamKeys(MediaSourceFactory mediaSourceFactory, List list) {
            return mediaSourceFactory;
        }
    }

    MediaSource createMediaSource(MediaItem mediaItem);

    int[] getSupportedTypes();
}
