package com.google.android.exoplayer2.ui;

/* loaded from: classes.dex */
public final class R$string {
    public static final int exo_controls_fastforward_description;
    public static final int exo_controls_hide;
    public static final int exo_controls_next_description;
    public static final int exo_controls_pause_description;
    public static final int exo_controls_play_description;
    public static final int exo_controls_previous_description;
    public static final int exo_controls_repeat_all_description;
    public static final int exo_controls_repeat_off_description;
    public static final int exo_controls_repeat_one_description;
    public static final int exo_controls_rewind_description;
    public static final int exo_controls_show;
    public static final int exo_controls_shuffle_off_description;
    public static final int exo_controls_shuffle_on_description;
    public static final int exo_controls_stop_description;
    public static final int exo_item_list;
    public static final int exo_track_bitrate;
    public static final int exo_track_mono;
    public static final int exo_track_resolution;
    public static final int exo_track_role_alternate;
    public static final int exo_track_role_closed_captions;
    public static final int exo_track_role_commentary;
    public static final int exo_track_role_supplementary;
    public static final int exo_track_selection_auto;
    public static final int exo_track_selection_none;
    public static final int exo_track_stereo;
    public static final int exo_track_surround;
    public static final int exo_track_surround_5_point_1;
    public static final int exo_track_surround_7_point_1;
    public static final int exo_track_unknown;
}
