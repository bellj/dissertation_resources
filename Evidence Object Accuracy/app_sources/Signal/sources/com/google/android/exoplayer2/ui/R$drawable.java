package com.google.android.exoplayer2.ui;

/* loaded from: classes.dex */
public final class R$drawable {
    public static final int exo_controls_repeat_all;
    public static final int exo_controls_repeat_off;
    public static final int exo_controls_repeat_one;
    public static final int exo_controls_shuffle_off;
    public static final int exo_controls_shuffle_on;
    public static final int exo_edit_mode_logo;
    public static final int exo_notification_fastforward;
    public static final int exo_notification_next;
    public static final int exo_notification_pause;
    public static final int exo_notification_play;
    public static final int exo_notification_previous;
    public static final int exo_notification_rewind;
    public static final int exo_notification_small_icon;
    public static final int exo_notification_stop;
}
