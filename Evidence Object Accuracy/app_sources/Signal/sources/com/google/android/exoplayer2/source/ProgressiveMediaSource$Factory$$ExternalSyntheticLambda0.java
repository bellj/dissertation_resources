package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ProgressiveMediaExtractor;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class ProgressiveMediaSource$Factory$$ExternalSyntheticLambda0 implements ProgressiveMediaExtractor.Factory {
    public final /* synthetic */ ExtractorsFactory f$0;

    public /* synthetic */ ProgressiveMediaSource$Factory$$ExternalSyntheticLambda0(ExtractorsFactory extractorsFactory) {
        this.f$0 = extractorsFactory;
    }

    @Override // com.google.android.exoplayer2.source.ProgressiveMediaExtractor.Factory
    public final ProgressiveMediaExtractor createProgressiveMediaExtractor() {
        return ProgressiveMediaSource.Factory.lambda$new$0(this.f$0);
    }
}
