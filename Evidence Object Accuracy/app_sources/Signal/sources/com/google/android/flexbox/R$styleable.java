package com.google.android.flexbox;

import org.thoughtcrime.securesms.R;

/* loaded from: classes.dex */
public final class R$styleable {
    public static final int[] FlexboxLayout = {R.attr.alignContent, R.attr.alignItems, R.attr.dividerDrawable, R.attr.dividerDrawableHorizontal, R.attr.dividerDrawableVertical, R.attr.flexDirection, R.attr.flexWrap, R.attr.justifyContent, R.attr.showDivider, R.attr.showDividerHorizontal, R.attr.showDividerVertical};
    public static final int[] FlexboxLayout_Layout = {R.attr.layout_alignSelf, R.attr.layout_flexBasisPercent, R.attr.layout_flexGrow, R.attr.layout_flexShrink, R.attr.layout_maxHeight, R.attr.layout_maxWidth, R.attr.layout_minHeight, R.attr.layout_minWidth, R.attr.layout_order, R.attr.layout_wrapBefore};
    public static final int FlexboxLayout_Layout_layout_alignSelf;
    public static final int FlexboxLayout_Layout_layout_flexBasisPercent;
    public static final int FlexboxLayout_Layout_layout_flexGrow;
    public static final int FlexboxLayout_Layout_layout_flexShrink;
    public static final int FlexboxLayout_Layout_layout_maxHeight;
    public static final int FlexboxLayout_Layout_layout_maxWidth;
    public static final int FlexboxLayout_Layout_layout_minHeight;
    public static final int FlexboxLayout_Layout_layout_minWidth;
    public static final int FlexboxLayout_Layout_layout_order;
    public static final int FlexboxLayout_Layout_layout_wrapBefore;
    public static final int FlexboxLayout_alignContent;
    public static final int FlexboxLayout_alignItems;
    public static final int FlexboxLayout_dividerDrawable;
    public static final int FlexboxLayout_dividerDrawableHorizontal;
    public static final int FlexboxLayout_dividerDrawableVertical;
    public static final int FlexboxLayout_flexDirection;
    public static final int FlexboxLayout_flexWrap;
    public static final int FlexboxLayout_justifyContent;
    public static final int FlexboxLayout_showDivider;
    public static final int FlexboxLayout_showDividerHorizontal;
    public static final int FlexboxLayout_showDividerVertical;
    public static final int[] RecyclerView = {16842948, 16842987, 16842993, R.attr.fastScrollEnabled, R.attr.fastScrollHorizontalThumbDrawable, R.attr.fastScrollHorizontalTrackDrawable, R.attr.fastScrollVerticalThumbDrawable, R.attr.fastScrollVerticalTrackDrawable, R.attr.layoutManager, R.attr.reverseLayout, R.attr.spanCount, R.attr.stackFromEnd};
}
