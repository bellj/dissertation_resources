package com.google.android.gms.base;

/* loaded from: classes.dex */
public final class R$string {
    public static final int common_google_play_services_enable_button;
    public static final int common_google_play_services_enable_text;
    public static final int common_google_play_services_enable_title;
    public static final int common_google_play_services_install_button;
    public static final int common_google_play_services_install_text;
    public static final int common_google_play_services_install_title;
    public static final int common_google_play_services_notification_channel_name;
    public static final int common_google_play_services_notification_ticker;
    public static final int common_google_play_services_unsupported_text;
    public static final int common_google_play_services_update_button;
    public static final int common_google_play_services_update_text;
    public static final int common_google_play_services_update_title;
    public static final int common_google_play_services_updating_text;
    public static final int common_google_play_services_wear_update_text;
    public static final int common_open_on_phone;
}
