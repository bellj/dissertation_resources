package com.google.android.gms.internal.maps;

import android.os.IBinder;

/* loaded from: classes.dex */
public final class zzg extends zza implements zze {
    public zzg(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
    }
}
