package com.google.android.gms.maps.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.maps.model.LatLng;

/* loaded from: classes.dex */
public interface ICameraUpdateFactoryDelegate extends IInterface {
    IObjectWrapper newLatLngZoom(LatLng latLng, float f) throws RemoteException;
}
