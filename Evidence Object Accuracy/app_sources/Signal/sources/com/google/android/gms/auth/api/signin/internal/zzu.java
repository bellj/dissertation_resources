package com.google.android.gms.auth.api.signin.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

/* loaded from: classes.dex */
public interface zzu extends IInterface {
    void zze(zzs zzs, GoogleSignInOptions googleSignInOptions) throws RemoteException;
}
