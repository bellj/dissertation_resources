package com.google.android.gms.common.internal.service;

import androidx.annotation.RecentlyNonNull;
import com.google.android.gms.common.api.Api;

/* compiled from: com.google.android.gms:play-services-base@@17.5.0 */
/* loaded from: classes.dex */
public final class Common {
    @RecentlyNonNull
    public static final Api<Api.ApiOptions.NoOptions> API;
    @RecentlyNonNull
    public static final Api.ClientKey<zah> CLIENT_KEY;
    public static final zab zaa = new zae();
    private static final Api.AbstractClientBuilder<zah, Api.ApiOptions.NoOptions> zab;

    static {
        Api.ClientKey<zah> clientKey = new Api.ClientKey<>();
        CLIENT_KEY = clientKey;
        zac zac = new zac();
        zab = zac;
        API = new Api<>("Common.API", zac, clientKey);
        zaa = new zae();
    }
}
