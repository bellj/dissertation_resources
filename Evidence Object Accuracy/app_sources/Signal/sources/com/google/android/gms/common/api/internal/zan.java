package com.google.android.gms.common.api.internal;

import android.app.Dialog;
import android.app.PendingIntent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiActivity;
import com.google.android.gms.common.internal.Preconditions;

/* compiled from: com.google.android.gms:play-services-base@@17.5.0 */
/* loaded from: classes.dex */
public final class zan implements Runnable {
    final /* synthetic */ zal zaa;
    private final zak zab;

    public zan(zal zal, zak zak) {
        this.zaa = zal;
        this.zab = zak;
    }

    @Override // java.lang.Runnable
    public final void run() {
        if (this.zaa.zaa) {
            ConnectionResult zab = this.zab.zab();
            if (zab.hasResolution()) {
                zal zal = this.zaa;
                zal.mLifecycleFragment.startActivityForResult(GoogleApiActivity.zaa(zal.getActivity(), (PendingIntent) Preconditions.checkNotNull(zab.getResolution()), this.zab.zaa(), false), 1);
                return;
            }
            zal zal2 = this.zaa;
            if (zal2.zac.getErrorResolutionIntent(zal2.getActivity(), zab.getErrorCode(), null) != null) {
                zal zal3 = this.zaa;
                zal3.zac.zaa(zal3.getActivity(), this.zaa.mLifecycleFragment, zab.getErrorCode(), 2, this.zaa);
            } else if (zab.getErrorCode() == 18) {
                Dialog zaa = GoogleApiAvailability.zaa(this.zaa.getActivity(), this.zaa);
                zal zal4 = this.zaa;
                zal4.zac.zaa(zal4.getActivity().getApplicationContext(), new zam(this, zaa));
            } else {
                this.zaa.zaa(zab, this.zab.zaa());
            }
        }
    }
}
