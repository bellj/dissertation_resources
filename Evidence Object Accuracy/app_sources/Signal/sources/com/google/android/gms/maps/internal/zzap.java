package com.google.android.gms.maps.internal;

import android.os.IInterface;
import android.os.RemoteException;

/* loaded from: classes.dex */
public interface zzap extends IInterface {
    void zza(IGoogleMapDelegate iGoogleMapDelegate) throws RemoteException;
}
