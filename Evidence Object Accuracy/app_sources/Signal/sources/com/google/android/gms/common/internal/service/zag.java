package com.google.android.gms.common.internal.service;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BaseImplementation$ResultHolder;

/* compiled from: com.google.android.gms:play-services-base@@17.5.0 */
/* loaded from: classes.dex */
final class zag extends zaa {
    private final BaseImplementation$ResultHolder<Status> zaa;

    public zag(BaseImplementation$ResultHolder<Status> baseImplementation$ResultHolder) {
        this.zaa = baseImplementation$ResultHolder;
    }

    @Override // com.google.android.gms.common.internal.service.zam
    public final void zaa(int i) throws RemoteException {
        this.zaa.setResult(new Status(i));
    }
}
