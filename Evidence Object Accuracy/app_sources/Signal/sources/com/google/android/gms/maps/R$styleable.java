package com.google.android.gms.maps;

import org.thoughtcrime.securesms.R;

/* loaded from: classes.dex */
public final class R$styleable {
    public static final int[] MapAttrs = {R.attr.ambientEnabled, R.attr.cameraBearing, R.attr.cameraMaxZoomPreference, R.attr.cameraMinZoomPreference, R.attr.cameraTargetLat, R.attr.cameraTargetLng, R.attr.cameraTilt, R.attr.cameraZoom, R.attr.latLngBoundsNorthEastLatitude, R.attr.latLngBoundsNorthEastLongitude, R.attr.latLngBoundsSouthWestLatitude, R.attr.latLngBoundsSouthWestLongitude, R.attr.liteMode, R.attr.mapType, R.attr.uiCompass, R.attr.uiMapToolbar, R.attr.uiRotateGestures, R.attr.uiScrollGestures, R.attr.uiScrollGesturesDuringRotateOrZoom, R.attr.uiTiltGestures, R.attr.uiZoomControls, R.attr.uiZoomGestures, R.attr.useViewLifecycle, R.attr.zOrderOnTop};
    public static final int MapAttrs_ambientEnabled;
    public static final int MapAttrs_cameraBearing;
    public static final int MapAttrs_cameraMaxZoomPreference;
    public static final int MapAttrs_cameraMinZoomPreference;
    public static final int MapAttrs_cameraTargetLat;
    public static final int MapAttrs_cameraTargetLng;
    public static final int MapAttrs_cameraTilt;
    public static final int MapAttrs_cameraZoom;
    public static final int MapAttrs_latLngBoundsNorthEastLatitude;
    public static final int MapAttrs_latLngBoundsNorthEastLongitude;
    public static final int MapAttrs_latLngBoundsSouthWestLatitude;
    public static final int MapAttrs_latLngBoundsSouthWestLongitude;
    public static final int MapAttrs_liteMode;
    public static final int MapAttrs_mapType;
    public static final int MapAttrs_uiCompass;
    public static final int MapAttrs_uiMapToolbar;
    public static final int MapAttrs_uiRotateGestures;
    public static final int MapAttrs_uiScrollGestures;
    public static final int MapAttrs_uiScrollGesturesDuringRotateOrZoom;
    public static final int MapAttrs_uiTiltGestures;
    public static final int MapAttrs_uiZoomControls;
    public static final int MapAttrs_uiZoomGestures;
    public static final int MapAttrs_useViewLifecycle;
    public static final int MapAttrs_zOrderOnTop;
}
