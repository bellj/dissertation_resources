package com.google.android.gms.common.util;

import org.thoughtcrime.securesms.util.MessageRecordUtil;

/* compiled from: com.google.android.gms:play-services-basement@@17.5.0 */
/* loaded from: classes.dex */
public final class zza {
    public static int zza(int i) {
        if (i == -1) {
            return -1;
        }
        return i / MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH;
    }
}
