package com.google.android.gms.internal.p001authapi;

import android.os.IBinder;

/* renamed from: com.google.android.gms.internal.auth-api.zzx */
/* loaded from: classes.dex */
public final class zzx extends zzc implements zzw {
    public zzx(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.auth.api.credentials.internal.ICredentialsService");
    }
}
