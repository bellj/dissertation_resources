package com.google.android.gms.maps.internal;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.dynamic.IObjectWrapper;
import com.google.android.gms.internal.maps.zzt;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.MarkerOptions;

/* loaded from: classes.dex */
public interface IGoogleMapDelegate extends IInterface {
    zzt addMarker(MarkerOptions markerOptions) throws RemoteException;

    CameraPosition getCameraPosition() throws RemoteException;

    IUiSettingsDelegate getUiSettings() throws RemoteException;

    void moveCamera(IObjectWrapper iObjectWrapper) throws RemoteException;

    void setBuildingsEnabled(boolean z) throws RemoteException;

    void setMapType(int i) throws RemoteException;

    void setMyLocationEnabled(boolean z) throws RemoteException;

    void setOnCameraIdleListener(zzn zzn) throws RemoteException;

    void setOnCameraMoveStartedListener(zzt zzt) throws RemoteException;

    void setOnMapLoadedCallback(zzal zzal) throws RemoteException;

    void snapshot(zzbs zzbs, IObjectWrapper iObjectWrapper) throws RemoteException;
}
