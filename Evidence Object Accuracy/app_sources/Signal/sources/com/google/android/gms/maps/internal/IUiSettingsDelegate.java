package com.google.android.gms.maps.internal;

import android.os.IInterface;
import android.os.RemoteException;

/* loaded from: classes.dex */
public interface IUiSettingsDelegate extends IInterface {
    void setAllGesturesEnabled(boolean z) throws RemoteException;
}
