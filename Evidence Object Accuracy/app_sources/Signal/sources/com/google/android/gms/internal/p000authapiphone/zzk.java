package com.google.android.gms.internal.p000authapiphone;

import android.os.RemoteException;

/* access modifiers changed from: package-private */
/* renamed from: com.google.android.gms.internal.auth-api-phone.zzk */
/* loaded from: classes.dex */
public final class zzk extends zzm {
    public zzk(zzj zzj) {
        super(null);
    }

    @Override // com.google.android.gms.internal.p000authapiphone.zzm
    protected final void zza(zze zze) throws RemoteException {
        zze.zza(new zzl(this));
    }
}
