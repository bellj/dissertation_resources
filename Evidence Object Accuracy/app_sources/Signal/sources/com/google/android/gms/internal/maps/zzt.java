package com.google.android.gms.internal.maps;

import android.os.IInterface;
import android.os.RemoteException;

/* loaded from: classes.dex */
public interface zzt extends IInterface {
    int zzj() throws RemoteException;

    boolean zzj(zzt zzt) throws RemoteException;
}
