package com.google.android.mms.pdu_alt;

import com.google.android.mms.InvalidHeaderValueException;

/* loaded from: classes3.dex */
public class MultimediaMessagePdu extends GenericPdu {
    private PduBody mMessageBody;

    public MultimediaMessagePdu() {
    }

    public MultimediaMessagePdu(PduHeaders pduHeaders, PduBody pduBody) {
        super(pduHeaders);
        this.mMessageBody = pduBody;
    }

    public PduBody getBody() {
        return this.mMessageBody;
    }

    public void setBody(PduBody pduBody) {
        this.mMessageBody = pduBody;
    }

    public EncodedStringValue[] getTo() {
        return this.mPduHeaders.getEncodedStringValues(151);
    }

    public void addTo(EncodedStringValue encodedStringValue) {
        this.mPduHeaders.appendEncodedStringValue(encodedStringValue, 151);
    }

    public void setPriority(int i) throws InvalidHeaderValueException {
        this.mPduHeaders.setOctet(i, 143);
    }

    public long getDate() {
        return this.mPduHeaders.getLongInteger(133);
    }

    public void setDate(long j) {
        this.mPduHeaders.setLongInteger(j, 133);
    }
}
