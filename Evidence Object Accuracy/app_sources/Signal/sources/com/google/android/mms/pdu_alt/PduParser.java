package com.google.android.mms.pdu_alt;

import com.android.mms.util.ExternalLogger;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.google.android.mms.InvalidHeaderValueException;
import com.klinker.android.logger.Log;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import org.thoughtcrime.securesms.database.helpers.SignalDatabaseMigrations;
import org.thoughtcrime.securesms.profiles.manage.EditAboutFragment;

/* loaded from: classes3.dex */
public class PduParser {
    private static byte[] mStartParam;
    private static byte[] mTypeParam;
    private PduBody mBody;
    private PduHeaders mHeaders;
    private final boolean mParseContentDisposition;
    private ByteArrayInputStream mPduDataStream;

    protected static boolean isText(int i) {
        return (i >= 32 && i <= 126) || (i >= 128 && i <= 255) || i == 9 || i == 10 || i == 13;
    }

    protected static boolean isTokenCharacter(int i) {
        if (!(i < 33 || i > 126 || i == 34 || i == 44 || i == 47 || i == 123 || i == 125 || i == 40 || i == 41)) {
            switch (i) {
                case 58:
                case 59:
                case 60:
                case 61:
                case 62:
                case 63:
                case 64:
                    break;
                default:
                    switch (i) {
                        case 91:
                        case 92:
                        case 93:
                            break;
                        default:
                            return true;
                    }
            }
        }
        return false;
    }

    private static void log(String str) {
    }

    public PduParser(byte[] bArr, boolean z) {
        this.mPduDataStream = null;
        this.mHeaders = null;
        this.mBody = null;
        this.mPduDataStream = new ByteArrayInputStream(bArr);
        this.mParseContentDisposition = z;
    }

    public PduParser(byte[] bArr) {
        this(bArr, true);
    }

    public GenericPdu parse() {
        ByteArrayInputStream byteArrayInputStream = this.mPduDataStream;
        if (byteArrayInputStream == null) {
            return null;
        }
        PduHeaders parseHeaders = parseHeaders(byteArrayInputStream);
        this.mHeaders = parseHeaders;
        if (parseHeaders == null) {
            return null;
        }
        int octet = parseHeaders.getOctet(EditAboutFragment.ABOUT_MAX_GLYPHS);
        if (!checkMandatoryHeader(this.mHeaders)) {
            log("check mandatory headers failed!");
            return null;
        }
        if (128 == octet || 132 == octet) {
            PduBody parseParts = parseParts(this.mPduDataStream);
            this.mBody = parseParts;
            if (parseParts == null) {
                return null;
            }
        }
        switch (octet) {
            case 128:
                return new SendReq(this.mHeaders, this.mBody);
            case 129:
                return new SendConf(this.mHeaders);
            case 130:
                return new NotificationInd(this.mHeaders);
            case 131:
                return new NotifyRespInd(this.mHeaders);
            case 132:
                RetrieveConf retrieveConf = new RetrieveConf(this.mHeaders, this.mBody);
                byte[] contentType = retrieveConf.getContentType();
                if (contentType == null) {
                    return null;
                }
                String str = new String(contentType);
                if (str.equals("application/vnd.wap.multipart.mixed") || str.equals("application/vnd.wap.multipart.related") || str.equals("application/vnd.wap.multipart.alternative")) {
                    return retrieveConf;
                }
                if (str.equals("application/vnd.wap.multipart.alternative")) {
                    PduPart part = this.mBody.getPart(0);
                    this.mBody.removeAll();
                    this.mBody.addPart(0, part);
                    return retrieveConf;
                } else if (str.equals("multipart/signed")) {
                    return retrieveConf;
                } else {
                    ExternalLogger.logMessage("PduParser", "Unsupported ContentType: " + str);
                    return null;
                }
            case 133:
                return new AcknowledgeInd(this.mHeaders);
            case 134:
                return new DeliveryInd(this.mHeaders);
            case 135:
                return new ReadRecInd(this.mHeaders);
            case 136:
                return new ReadOrigInd(this.mHeaders);
            default:
                log("Parser doesn't support this message type in this version!");
                return null;
        }
    }

    protected PduHeaders parseHeaders(ByteArrayInputStream byteArrayInputStream) {
        EncodedStringValue encodedStringValue;
        byte[] textString;
        if (byteArrayInputStream == null) {
            return null;
        }
        PduHeaders pduHeaders = new PduHeaders();
        boolean z = true;
        while (z && byteArrayInputStream.available() > 0) {
            byteArrayInputStream.mark(1);
            int extractByteValue = extractByteValue(byteArrayInputStream);
            if (extractByteValue < 32 || extractByteValue > 127) {
                switch (extractByteValue) {
                    case 129:
                    case 130:
                    case 151:
                        EncodedStringValue parseEncodedStringValue = parseEncodedStringValue(byteArrayInputStream);
                        if (parseEncodedStringValue != null) {
                            byte[] textString2 = parseEncodedStringValue.getTextString();
                            if (textString2 != null) {
                                String str = new String(textString2);
                                int indexOf = str.indexOf("/");
                                if (indexOf > 0) {
                                    str = str.substring(0, indexOf);
                                }
                                try {
                                    parseEncodedStringValue.setTextString(str.getBytes());
                                } catch (NullPointerException unused) {
                                    log("null pointer error!");
                                    return null;
                                }
                            }
                            try {
                                pduHeaders.appendEncodedStringValue(parseEncodedStringValue, extractByteValue);
                                break;
                            } catch (NullPointerException unused2) {
                                log("null pointer error!");
                                break;
                            } catch (RuntimeException unused3) {
                                log(extractByteValue + "is not Encoded-String-Value header field!");
                                return null;
                            }
                        } else {
                            continue;
                        }
                    case 131:
                    case 139:
                    case 152:
                    case 158:
                    case 183:
                    case 184:
                    case 185:
                    case 189:
                    case 190:
                        byte[] parseWapString = parseWapString(byteArrayInputStream, 0);
                        if (parseWapString != null) {
                            try {
                                pduHeaders.setTextString(parseWapString, extractByteValue);
                                break;
                            } catch (NullPointerException unused4) {
                                log("null pointer error!");
                                break;
                            } catch (RuntimeException unused5) {
                                log(extractByteValue + "is not Text-String header field!");
                                return null;
                            }
                        } else {
                            continue;
                        }
                    case 132:
                        HashMap hashMap = new HashMap();
                        byte[] parseContentType = parseContentType(byteArrayInputStream, hashMap);
                        if (parseContentType != null) {
                            try {
                                pduHeaders.setTextString(parseContentType, 132);
                            } catch (NullPointerException unused6) {
                                log("null pointer error!");
                            } catch (RuntimeException unused7) {
                                log(extractByteValue + "is not Text-String header field!");
                                return null;
                            }
                        }
                        mStartParam = (byte[]) hashMap.get(153);
                        mTypeParam = (byte[]) hashMap.get(131);
                        z = false;
                        continue;
                    case 133:
                    case 142:
                    case 159:
                        try {
                            pduHeaders.setLongInteger(parseLongInteger(byteArrayInputStream), extractByteValue);
                            continue;
                        } catch (RuntimeException unused8) {
                            log(extractByteValue + "is not Long-Integer header field!");
                            return null;
                        }
                    case 134:
                    case 143:
                    case 144:
                    case 145:
                    case 146:
                    case 148:
                    case SignalDatabaseMigrations.DATABASE_VERSION:
                    case 153:
                    case 155:
                    case 156:
                    case 162:
                    case 163:
                    case 165:
                    case 167:
                    case 169:
                    case 171:
                    case 177:
                    case SubsamplingScaleImageView.ORIENTATION_180:
                    case 186:
                    case 187:
                    case 188:
                    case 191:
                        int extractByteValue2 = extractByteValue(byteArrayInputStream);
                        try {
                            pduHeaders.setOctet(extractByteValue2, extractByteValue);
                            continue;
                        } catch (InvalidHeaderValueException unused9) {
                            log("Set invalid Octet value: " + extractByteValue2 + " into the header filed: " + extractByteValue);
                            return null;
                        } catch (RuntimeException unused10) {
                            log(extractByteValue + "is not Octet header field!");
                            return null;
                        }
                    case 135:
                    case 136:
                    case 157:
                        parseValueLength(byteArrayInputStream);
                        int extractByteValue3 = extractByteValue(byteArrayInputStream);
                        try {
                            long parseLongInteger = parseLongInteger(byteArrayInputStream);
                            if (129 == extractByteValue3) {
                                parseLongInteger += System.currentTimeMillis() / 1000;
                            }
                            try {
                                pduHeaders.setLongInteger(parseLongInteger, extractByteValue);
                                continue;
                            } catch (RuntimeException unused11) {
                                log(extractByteValue + "is not Long-Integer header field!");
                                return null;
                            }
                        } catch (RuntimeException unused12) {
                            log(extractByteValue + "is not Long-Integer header field!");
                            return null;
                        }
                    case 137:
                        parseValueLength(byteArrayInputStream);
                        if (128 == extractByteValue(byteArrayInputStream)) {
                            encodedStringValue = parseEncodedStringValue(byteArrayInputStream);
                            if (!(encodedStringValue == null || (textString = encodedStringValue.getTextString()) == null)) {
                                String str2 = new String(textString);
                                int indexOf2 = str2.indexOf("/");
                                if (indexOf2 > 0) {
                                    str2 = str2.substring(0, indexOf2);
                                }
                                try {
                                    encodedStringValue.setTextString(str2.getBytes());
                                } catch (NullPointerException unused13) {
                                    log("null pointer error!");
                                    return null;
                                }
                            }
                        } else {
                            try {
                                encodedStringValue = new EncodedStringValue("insert-address-token".getBytes());
                            } catch (NullPointerException unused14) {
                                log(extractByteValue + "is not Encoded-String-Value header field!");
                                return null;
                            }
                        }
                        try {
                            pduHeaders.setEncodedStringValue(encodedStringValue, 137);
                            continue;
                        } catch (NullPointerException unused15) {
                            log("null pointer error!");
                            break;
                        } catch (RuntimeException unused16) {
                            log(extractByteValue + "is not Encoded-String-Value header field!");
                            return null;
                        }
                    case 138:
                        byteArrayInputStream.mark(1);
                        int extractByteValue4 = extractByteValue(byteArrayInputStream);
                        if (extractByteValue4 >= 128) {
                            if (128 != extractByteValue4) {
                                if (129 != extractByteValue4) {
                                    if (130 != extractByteValue4) {
                                        if (131 != extractByteValue4) {
                                            break;
                                        } else {
                                            pduHeaders.setTextString("auto".getBytes(), 138);
                                            break;
                                        }
                                    } else {
                                        pduHeaders.setTextString("informational".getBytes(), 138);
                                        break;
                                    }
                                } else {
                                    pduHeaders.setTextString("advertisement".getBytes(), 138);
                                    break;
                                }
                            } else {
                                try {
                                    pduHeaders.setTextString("personal".getBytes(), 138);
                                    continue;
                                } catch (NullPointerException unused17) {
                                    log("null pointer error!");
                                    break;
                                } catch (RuntimeException unused18) {
                                    log(extractByteValue + "is not Text-String header field!");
                                    return null;
                                }
                            }
                        } else {
                            byteArrayInputStream.reset();
                            byte[] parseWapString2 = parseWapString(byteArrayInputStream, 0);
                            if (parseWapString2 == null) {
                                break;
                            } else {
                                try {
                                    pduHeaders.setTextString(parseWapString2, 138);
                                    break;
                                } catch (NullPointerException unused19) {
                                    log("null pointer error!");
                                    break;
                                } catch (RuntimeException unused20) {
                                    log(extractByteValue + "is not Text-String header field!");
                                    return null;
                                }
                            }
                        }
                    case EditAboutFragment.ABOUT_MAX_GLYPHS:
                        int extractByteValue5 = extractByteValue(byteArrayInputStream);
                        switch (extractByteValue5) {
                            case 137:
                            case 138:
                            case 139:
                            case EditAboutFragment.ABOUT_MAX_GLYPHS:
                            case 141:
                            case 142:
                            case 143:
                            case 144:
                            case 145:
                            case 146:
                            case 147:
                            case 148:
                            case SignalDatabaseMigrations.DATABASE_VERSION:
                            case 150:
                            case 151:
                                return null;
                            default:
                                try {
                                    pduHeaders.setOctet(extractByteValue5, extractByteValue);
                                    continue;
                                } catch (InvalidHeaderValueException unused21) {
                                    log("Set invalid Octet value: " + extractByteValue5 + " into the header filed: " + extractByteValue);
                                    return null;
                                } catch (RuntimeException unused22) {
                                    log(extractByteValue + "is not Octet header field!");
                                    return null;
                                }
                        }
                    case 141:
                        int parseShortInteger = parseShortInteger(byteArrayInputStream);
                        try {
                            pduHeaders.setOctet(parseShortInteger, 141);
                            continue;
                        } catch (InvalidHeaderValueException unused23) {
                            log("Set invalid Octet value: " + parseShortInteger + " into the header filed: " + extractByteValue);
                            return null;
                        } catch (RuntimeException unused24) {
                            log(extractByteValue + "is not Octet header field!");
                            return null;
                        }
                    case 147:
                    case 150:
                    case 154:
                    case 166:
                    case 181:
                    case 182:
                        EncodedStringValue parseEncodedStringValue2 = parseEncodedStringValue(byteArrayInputStream);
                        if (parseEncodedStringValue2 != null) {
                            try {
                                pduHeaders.setEncodedStringValue(parseEncodedStringValue2, extractByteValue);
                                break;
                            } catch (NullPointerException unused25) {
                                log("null pointer error!");
                                break;
                            } catch (RuntimeException unused26) {
                                log(extractByteValue + "is not Encoded-String-Value header field!");
                                return null;
                            }
                        } else {
                            continue;
                        }
                    case 160:
                        parseValueLength(byteArrayInputStream);
                        try {
                            parseIntegerValue(byteArrayInputStream);
                            EncodedStringValue parseEncodedStringValue3 = parseEncodedStringValue(byteArrayInputStream);
                            if (parseEncodedStringValue3 != null) {
                                try {
                                    pduHeaders.setEncodedStringValue(parseEncodedStringValue3, 160);
                                    break;
                                } catch (NullPointerException unused27) {
                                    log("null pointer error!");
                                    break;
                                } catch (RuntimeException unused28) {
                                    log(extractByteValue + "is not Encoded-String-Value header field!");
                                    return null;
                                }
                            } else {
                                continue;
                            }
                        } catch (RuntimeException unused29) {
                            log(extractByteValue + " is not Integer-Value");
                            return null;
                        }
                    case 161:
                        parseValueLength(byteArrayInputStream);
                        try {
                            parseIntegerValue(byteArrayInputStream);
                            try {
                                pduHeaders.setLongInteger(parseLongInteger(byteArrayInputStream), 161);
                                continue;
                            } catch (RuntimeException unused30) {
                                log(extractByteValue + "is not Long-Integer header field!");
                                return null;
                            }
                        } catch (RuntimeException unused31) {
                            log(extractByteValue + " is not Integer-Value");
                            return null;
                        }
                    case 164:
                        parseValueLength(byteArrayInputStream);
                        extractByteValue(byteArrayInputStream);
                        parseEncodedStringValue(byteArrayInputStream);
                        continue;
                    case 168:
                    case 174:
                    case 176:
                    default:
                        log("Unknown header");
                        continue;
                    case 170:
                    case 172:
                        parseValueLength(byteArrayInputStream);
                        extractByteValue(byteArrayInputStream);
                        try {
                            parseIntegerValue(byteArrayInputStream);
                            continue;
                        } catch (RuntimeException unused32) {
                            log(extractByteValue + " is not Integer-Value");
                            return null;
                        }
                    case 173:
                    case 175:
                    case 179:
                        try {
                            pduHeaders.setLongInteger(parseIntegerValue(byteArrayInputStream), extractByteValue);
                            continue;
                        } catch (RuntimeException unused33) {
                            log(extractByteValue + "is not Long-Integer header field!");
                            return null;
                        }
                    case 178:
                        parseContentType(byteArrayInputStream, null);
                        continue;
                }
            } else {
                byteArrayInputStream.reset();
                parseWapString(byteArrayInputStream, 0);
            }
        }
        return pduHeaders;
    }

    protected PduBody parseParts(ByteArrayInputStream byteArrayInputStream) {
        if (byteArrayInputStream == null) {
            return null;
        }
        int parseUnsignedInt = parseUnsignedInt(byteArrayInputStream);
        PduBody pduBody = new PduBody();
        for (int i = 0; i < parseUnsignedInt; i++) {
            int parseUnsignedInt2 = parseUnsignedInt(byteArrayInputStream);
            int parseUnsignedInt3 = parseUnsignedInt(byteArrayInputStream);
            PduPart pduPart = new PduPart();
            int available = byteArrayInputStream.available();
            if (available <= 0) {
                return null;
            }
            HashMap hashMap = new HashMap();
            byte[] parseContentType = parseContentType(byteArrayInputStream, hashMap);
            if (parseContentType != null) {
                pduPart.setContentType(parseContentType);
            } else {
                pduPart.setContentType(PduContentTypes.contentTypes[0].getBytes());
            }
            byte[] bArr = (byte[]) hashMap.get(151);
            if (bArr != null) {
                pduPart.setName(bArr);
            }
            Integer num = (Integer) hashMap.get(129);
            if (num != null) {
                pduPart.setCharset(num.intValue());
            }
            int available2 = parseUnsignedInt2 - (available - byteArrayInputStream.available());
            if (available2 > 0) {
                if (!parsePartHeaders(byteArrayInputStream, pduPart, available2)) {
                    return null;
                }
            } else if (available2 < 0) {
                return null;
            }
            if (pduPart.getContentLocation() == null && pduPart.getName() == null && pduPart.getFilename() == null && pduPart.getContentId() == null) {
                pduPart.setContentLocation(Long.toOctalString(System.currentTimeMillis()).getBytes());
            }
            if (parseUnsignedInt3 > 0) {
                byte[] bArr2 = new byte[parseUnsignedInt3];
                String str = new String(pduPart.getContentType());
                byteArrayInputStream.read(bArr2, 0, parseUnsignedInt3);
                if (str.equalsIgnoreCase("application/vnd.wap.multipart.alternative")) {
                    pduPart = parseParts(new ByteArrayInputStream(bArr2)).getPart(0);
                } else {
                    byte[] contentTransferEncoding = pduPart.getContentTransferEncoding();
                    if (contentTransferEncoding != null) {
                        String str2 = new String(contentTransferEncoding);
                        if (str2.equalsIgnoreCase("base64")) {
                            bArr2 = Base64.decodeBase64(bArr2);
                        } else if (str2.equalsIgnoreCase("quoted-printable")) {
                            bArr2 = QuotedPrintable.decodeQuotedPrintable(bArr2);
                        }
                    }
                    if (bArr2 == null) {
                        log("Decode part data error!");
                        return null;
                    }
                    pduPart.setData(bArr2);
                }
            }
            if (checkPartPosition(pduPart) == 0) {
                pduBody.addPart(0, pduPart);
            } else {
                pduBody.addPart(pduPart);
            }
        }
        return pduBody;
    }

    protected static int parseUnsignedInt(ByteArrayInputStream byteArrayInputStream) {
        int i = 0;
        int read = byteArrayInputStream.read();
        if (read == -1) {
            return read;
        }
        while ((read & 128) != 0) {
            i = (i << 7) | (read & 127);
            read = byteArrayInputStream.read();
            if (read == -1) {
                return read;
            }
        }
        return (i << 7) | (read & 127);
    }

    protected static int parseValueLength(ByteArrayInputStream byteArrayInputStream) {
        int read = byteArrayInputStream.read() & 255;
        if (read <= 30) {
            return read;
        }
        if (read == 31) {
            return parseUnsignedInt(byteArrayInputStream);
        }
        throw new RuntimeException("Value length > LENGTH_QUOTE!");
    }

    protected static EncodedStringValue parseEncodedStringValue(ByteArrayInputStream byteArrayInputStream) {
        int i;
        byteArrayInputStream.mark(1);
        int read = byteArrayInputStream.read() & 255;
        if (read == 0) {
            return new EncodedStringValue("");
        }
        byteArrayInputStream.reset();
        if (read < 32) {
            parseValueLength(byteArrayInputStream);
            i = parseShortInteger(byteArrayInputStream);
        } else {
            i = 0;
        }
        byte[] parseWapString = parseWapString(byteArrayInputStream, 0);
        try {
            if (i != 0) {
                return new EncodedStringValue(i, parseWapString);
            }
            return new EncodedStringValue(parseWapString);
        } catch (Exception unused) {
            return null;
        }
    }

    protected static byte[] parseWapString(ByteArrayInputStream byteArrayInputStream, int i) {
        byteArrayInputStream.mark(1);
        int read = byteArrayInputStream.read();
        if (1 == i && 34 == read) {
            byteArrayInputStream.mark(1);
        } else if (i == 0 && 127 == read) {
            byteArrayInputStream.mark(1);
        } else {
            byteArrayInputStream.reset();
        }
        return getWapString(byteArrayInputStream, i);
    }

    protected static byte[] getWapString(ByteArrayInputStream byteArrayInputStream, int i) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int read = byteArrayInputStream.read();
        while (-1 != read && read != 0) {
            if (i == 2) {
                if (isTokenCharacter(read)) {
                    byteArrayOutputStream.write(read);
                }
            } else if (isText(read)) {
                byteArrayOutputStream.write(read);
            }
            read = byteArrayInputStream.read();
        }
        if (byteArrayOutputStream.size() > 0) {
            return byteArrayOutputStream.toByteArray();
        }
        return null;
    }

    protected static int extractByteValue(ByteArrayInputStream byteArrayInputStream) {
        return byteArrayInputStream.read() & 255;
    }

    protected static int parseShortInteger(ByteArrayInputStream byteArrayInputStream) {
        return byteArrayInputStream.read() & 127;
    }

    protected static long parseLongInteger(ByteArrayInputStream byteArrayInputStream) {
        int read = byteArrayInputStream.read() & 255;
        if (read <= 8) {
            long j = 0;
            for (int i = 0; i < read; i++) {
                j = (j << 8) + ((long) (byteArrayInputStream.read() & 255));
            }
            return j;
        }
        throw new RuntimeException("Octet count greater than 8 and I can't represent that!");
    }

    protected static long parseIntegerValue(ByteArrayInputStream byteArrayInputStream) {
        byteArrayInputStream.mark(1);
        int read = byteArrayInputStream.read();
        byteArrayInputStream.reset();
        if (read > 127) {
            return (long) parseShortInteger(byteArrayInputStream);
        }
        return parseLongInteger(byteArrayInputStream);
    }

    protected static int skipWapValue(ByteArrayInputStream byteArrayInputStream, int i) {
        int read = byteArrayInputStream.read(new byte[i], 0, i);
        if (read < i) {
            return -1;
        }
        return read;
    }

    protected static void parseContentTypeParams(ByteArrayInputStream byteArrayInputStream, HashMap<Integer, Object> hashMap, Integer num) {
        int available;
        int intValue;
        int available2 = byteArrayInputStream.available();
        int intValue2 = num.intValue();
        while (intValue2 > 0) {
            int read = byteArrayInputStream.read();
            intValue2--;
            if (read != 129) {
                if (read != 131) {
                    if (read == 133 || read == 151) {
                        byte[] parseWapString = parseWapString(byteArrayInputStream, 0);
                        if (!(parseWapString == null || hashMap == null)) {
                            hashMap.put(151, parseWapString);
                        }
                        available = byteArrayInputStream.available();
                        intValue = num.intValue();
                    } else {
                        if (read != 153) {
                            if (read != 137) {
                                if (read != 138) {
                                    if (-1 == skipWapValue(byteArrayInputStream, intValue2)) {
                                        Log.e("PduParser", "Corrupt Content-Type");
                                    } else {
                                        intValue2 = 0;
                                    }
                                }
                            }
                        }
                        byte[] parseWapString2 = parseWapString(byteArrayInputStream, 0);
                        if (!(parseWapString2 == null || hashMap == null)) {
                            hashMap.put(153, parseWapString2);
                        }
                        available = byteArrayInputStream.available();
                        intValue = num.intValue();
                    }
                }
                byteArrayInputStream.mark(1);
                int extractByteValue = extractByteValue(byteArrayInputStream);
                byteArrayInputStream.reset();
                if (extractByteValue > 127) {
                    int parseShortInteger = parseShortInteger(byteArrayInputStream);
                    String[] strArr = PduContentTypes.contentTypes;
                    if (parseShortInteger < strArr.length) {
                        hashMap.put(131, strArr[parseShortInteger].getBytes());
                    }
                } else {
                    byte[] parseWapString3 = parseWapString(byteArrayInputStream, 0);
                    if (!(parseWapString3 == null || hashMap == null)) {
                        hashMap.put(131, parseWapString3);
                    }
                }
                available = byteArrayInputStream.available();
                intValue = num.intValue();
            } else {
                byteArrayInputStream.mark(1);
                int extractByteValue2 = extractByteValue(byteArrayInputStream);
                byteArrayInputStream.reset();
                if ((extractByteValue2 <= 32 || extractByteValue2 >= 127) && extractByteValue2 != 0) {
                    int parseIntegerValue = (int) parseIntegerValue(byteArrayInputStream);
                    if (hashMap != null) {
                        hashMap.put(129, Integer.valueOf(parseIntegerValue));
                    }
                } else {
                    byte[] parseWapString4 = parseWapString(byteArrayInputStream, 0);
                    try {
                        hashMap.put(129, Integer.valueOf(CharacterSets.getMibEnumValue(new String(parseWapString4))));
                    } catch (UnsupportedEncodingException e) {
                        Log.e("PduParser", Arrays.toString(parseWapString4), e);
                        hashMap.put(129, 0);
                    }
                }
                available = byteArrayInputStream.available();
                intValue = num.intValue();
            }
            intValue2 = intValue - (available2 - available);
        }
        if (intValue2 != 0) {
            Log.e("PduParser", "Corrupt Content-Type");
        }
    }

    protected static byte[] parseContentType(ByteArrayInputStream byteArrayInputStream, HashMap<Integer, Object> hashMap) {
        byte[] bArr;
        byteArrayInputStream.mark(1);
        int read = byteArrayInputStream.read();
        byteArrayInputStream.reset();
        int i = read & 255;
        if (i < 32) {
            int parseValueLength = parseValueLength(byteArrayInputStream);
            int available = byteArrayInputStream.available();
            byteArrayInputStream.mark(1);
            int read2 = byteArrayInputStream.read();
            byteArrayInputStream.reset();
            int i2 = read2 & 255;
            if (i2 >= 32 && i2 <= 127) {
                bArr = parseWapString(byteArrayInputStream, 0);
            } else if (i2 > 127) {
                int parseShortInteger = parseShortInteger(byteArrayInputStream);
                String[] strArr = PduContentTypes.contentTypes;
                if (parseShortInteger < strArr.length) {
                    bArr = strArr[parseShortInteger].getBytes();
                } else {
                    byteArrayInputStream.reset();
                    bArr = parseWapString(byteArrayInputStream, 0);
                }
            } else {
                Log.e("PduParser", "Corrupt content-type");
                return PduContentTypes.contentTypes[0].getBytes();
            }
            int available2 = parseValueLength - (available - byteArrayInputStream.available());
            if (available2 > 0) {
                parseContentTypeParams(byteArrayInputStream, hashMap, Integer.valueOf(available2));
            }
            if (available2 >= 0) {
                return bArr;
            }
            Log.e("PduParser", "Corrupt MMS message");
            return PduContentTypes.contentTypes[0].getBytes();
        } else if (i <= 127) {
            return parseWapString(byteArrayInputStream, 0);
        } else {
            return PduContentTypes.contentTypes[parseShortInteger(byteArrayInputStream)].getBytes();
        }
    }

    protected boolean parsePartHeaders(ByteArrayInputStream byteArrayInputStream, PduPart pduPart, int i) {
        int i2;
        int available = byteArrayInputStream.available();
        int i3 = i;
        while (true) {
            int i4 = 0;
            if (i3 > 0) {
                int read = byteArrayInputStream.read();
                int i5 = i3 - 1;
                if (read > 127) {
                    if (read != 142) {
                        if (read != 174) {
                            if (read == 192) {
                                byte[] parseWapString = parseWapString(byteArrayInputStream, 1);
                                if (parseWapString != null) {
                                    pduPart.setContentId(parseWapString);
                                }
                                i2 = byteArrayInputStream.available();
                            } else if (read != 197) {
                                if (-1 == skipWapValue(byteArrayInputStream, i5)) {
                                    Log.e("PduParser", "Corrupt Part headers");
                                    return false;
                                }
                                i3 = i4;
                            }
                        }
                        if (this.mParseContentDisposition) {
                            int parseValueLength = parseValueLength(byteArrayInputStream);
                            byteArrayInputStream.mark(1);
                            int available2 = byteArrayInputStream.available();
                            int read2 = byteArrayInputStream.read();
                            if (read2 == 128) {
                                pduPart.setContentDisposition(PduPart.DISPOSITION_FROM_DATA);
                            } else if (read2 == 129) {
                                pduPart.setContentDisposition(PduPart.DISPOSITION_ATTACHMENT);
                            } else if (read2 == 130) {
                                pduPart.setContentDisposition(PduPart.DISPOSITION_INLINE);
                            } else {
                                byteArrayInputStream.reset();
                                pduPart.setContentDisposition(parseWapString(byteArrayInputStream, 0));
                            }
                            if (available2 - byteArrayInputStream.available() < parseValueLength) {
                                if (byteArrayInputStream.read() == 152) {
                                    pduPart.setFilename(parseWapString(byteArrayInputStream, 0));
                                }
                                int available3 = available2 - byteArrayInputStream.available();
                                if (available3 < parseValueLength) {
                                    int i6 = parseValueLength - available3;
                                    byteArrayInputStream.read(new byte[i6], 0, i6);
                                }
                            }
                            i2 = byteArrayInputStream.available();
                        } else {
                            i4 = i5;
                            i3 = i4;
                        }
                    } else {
                        byte[] parseWapString2 = parseWapString(byteArrayInputStream, 0);
                        if (parseWapString2 != null) {
                            pduPart.setContentLocation(parseWapString2);
                        }
                        i2 = byteArrayInputStream.available();
                    }
                    i4 = i - (available - i2);
                    i3 = i4;
                } else if (read >= 32 && read <= 127) {
                    byte[] parseWapString3 = parseWapString(byteArrayInputStream, 0);
                    byte[] parseWapString4 = parseWapString(byteArrayInputStream, 0);
                    if (true == "Content-Transfer-Encoding".equalsIgnoreCase(new String(parseWapString3))) {
                        pduPart.setContentTransferEncoding(parseWapString4);
                    }
                    i3 = i - (available - byteArrayInputStream.available());
                } else if (-1 == skipWapValue(byteArrayInputStream, i5)) {
                    Log.e("PduParser", "Corrupt Part headers");
                    return false;
                } else {
                    i3 = 0;
                }
            } else if (i3 == 0) {
                return true;
            } else {
                Log.e("PduParser", "Corrupt Part headers");
                return false;
            }
        }
    }

    private static int checkPartPosition(PduPart pduPart) {
        byte[] contentType;
        byte[] contentId;
        if (mTypeParam == null && mStartParam == null) {
            return 1;
        }
        if (mStartParam != null && (contentId = pduPart.getContentId()) != null && true == Arrays.equals(mStartParam, contentId)) {
            return 0;
        }
        if (mTypeParam == null || (contentType = pduPart.getContentType()) == null || true != Arrays.equals(mTypeParam, contentType)) {
            return 1;
        }
        return 0;
    }

    protected static boolean checkMandatoryHeader(PduHeaders pduHeaders) {
        if (pduHeaders == null) {
            return false;
        }
        int octet = pduHeaders.getOctet(EditAboutFragment.ABOUT_MAX_GLYPHS);
        if (pduHeaders.getOctet(141) == 0) {
            return false;
        }
        switch (octet) {
            case 128:
                if (pduHeaders.getTextString(132) == null || pduHeaders.getEncodedStringValue(137) == null || pduHeaders.getTextString(152) == null) {
                    return false;
                }
                return true;
            case 129:
                if (pduHeaders.getOctet(146) == 0 || pduHeaders.getTextString(152) == null) {
                    return false;
                }
                return true;
            case 130:
                if (pduHeaders.getTextString(131) == null || -1 == pduHeaders.getLongInteger(136) || pduHeaders.getTextString(138) == null || -1 == pduHeaders.getLongInteger(142) || pduHeaders.getTextString(152) == null) {
                    return false;
                }
                return true;
            case 131:
                if (pduHeaders.getOctet(SignalDatabaseMigrations.DATABASE_VERSION) == 0 || pduHeaders.getTextString(152) == null) {
                    return false;
                }
                return true;
            case 132:
                if (pduHeaders.getTextString(132) == null || -1 == pduHeaders.getLongInteger(133)) {
                    return false;
                }
                return true;
            case 133:
                if (pduHeaders.getTextString(152) == null) {
                    return false;
                }
                return true;
            case 134:
                if (-1 == pduHeaders.getLongInteger(133) || pduHeaders.getTextString(139) == null || pduHeaders.getOctet(SignalDatabaseMigrations.DATABASE_VERSION) == 0 || pduHeaders.getEncodedStringValues(151) == null) {
                    return false;
                }
                return true;
            case 135:
                if (pduHeaders.getEncodedStringValue(137) == null || pduHeaders.getTextString(139) == null || pduHeaders.getOctet(155) == 0 || pduHeaders.getEncodedStringValues(151) == null) {
                    return false;
                }
                return true;
            case 136:
                if (-1 == pduHeaders.getLongInteger(133) || pduHeaders.getEncodedStringValue(137) == null || pduHeaders.getTextString(139) == null || pduHeaders.getOctet(155) == 0 || pduHeaders.getEncodedStringValues(151) == null) {
                    return false;
                }
                return true;
            default:
                return false;
        }
    }
}
