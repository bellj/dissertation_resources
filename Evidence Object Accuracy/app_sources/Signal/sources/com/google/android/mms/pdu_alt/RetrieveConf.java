package com.google.android.mms.pdu_alt;

import com.google.android.mms.InvalidHeaderValueException;

/* loaded from: classes3.dex */
public class RetrieveConf extends MultimediaMessagePdu {
    public RetrieveConf() throws InvalidHeaderValueException {
        setMessageType(132);
    }

    public RetrieveConf(PduHeaders pduHeaders, PduBody pduBody) {
        super(pduHeaders, pduBody);
    }

    public EncodedStringValue[] getCc() {
        return this.mPduHeaders.getEncodedStringValues(130);
    }

    public byte[] getContentType() {
        return this.mPduHeaders.getTextString(132);
    }

    @Override // com.google.android.mms.pdu_alt.GenericPdu
    public EncodedStringValue getFrom() {
        return this.mPduHeaders.getEncodedStringValue(137);
    }
}
