package com.google.android.mms.pdu_alt;

import android.content.ContentResolver;
import android.content.Context;
import android.text.TextUtils;
import com.klinker.android.logger.Log;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import org.thoughtcrime.securesms.database.helpers.SignalDatabaseMigrations;
import org.thoughtcrime.securesms.profiles.manage.EditAboutFragment;

/* loaded from: classes3.dex */
public class PduComposer {
    private static HashMap<String, Integer> mContentTypeMap = new HashMap<>();
    protected ByteArrayOutputStream mMessage = null;
    private GenericPdu mPdu;
    private PduHeaders mPduHeader = null;
    protected int mPosition = 0;
    private final ContentResolver mResolver;
    private BufferStack mStack = null;

    static {
        mContentTypeMap = new HashMap<>();
        int i = 0;
        while (true) {
            String[] strArr = PduContentTypes.contentTypes;
            if (i < strArr.length) {
                mContentTypeMap.put(strArr[i], Integer.valueOf(i));
                i++;
            } else {
                return;
            }
        }
    }

    public PduComposer(Context context, GenericPdu genericPdu) {
        this.mPdu = genericPdu;
        this.mResolver = context.getContentResolver();
        this.mPduHeader = genericPdu.getPduHeaders();
        this.mStack = new BufferStack();
        this.mMessage = new ByteArrayOutputStream();
        this.mPosition = 0;
    }

    public byte[] make() {
        int messageType = this.mPdu.getMessageType();
        if (messageType != 128) {
            if (messageType != 131) {
                if (messageType != 133) {
                    if (!(messageType == 135 && makeReadRecInd() == 0)) {
                        return null;
                    }
                } else if (makeAckInd() != 0) {
                    return null;
                }
            } else if (makeNotifyResp() != 0) {
                return null;
            }
        } else if (makeSendReqPdu() != 0) {
            return null;
        }
        return this.mMessage.toByteArray();
    }

    protected void arraycopy(byte[] bArr, int i, int i2) {
        this.mMessage.write(bArr, i, i2);
        this.mPosition += i2;
    }

    protected void append(int i) {
        this.mMessage.write(i);
        this.mPosition++;
    }

    protected void appendShortInteger(int i) {
        append((i | 128) & 255);
    }

    protected void appendOctet(int i) {
        append(i);
    }

    protected void appendShortLength(int i) {
        append(i);
    }

    protected void appendLongInteger(long j) {
        long j2 = j;
        int i = 0;
        while (j2 != 0 && i < 8) {
            j2 >>>= 8;
            i++;
        }
        appendShortLength(i);
        int i2 = (i - 1) * 8;
        for (int i3 = 0; i3 < i; i3++) {
            append((int) ((j >>> i2) & 255));
            i2 -= 8;
        }
    }

    protected void appendTextString(byte[] bArr) {
        if ((bArr[0] & 255) > 127) {
            append(127);
        }
        arraycopy(bArr, 0, bArr.length);
        append(0);
    }

    protected void appendTextString(String str) {
        appendTextString(str.getBytes());
    }

    protected void appendEncodedString(EncodedStringValue encodedStringValue) {
        int characterSet = encodedStringValue.getCharacterSet();
        byte[] textString = encodedStringValue.getTextString();
        if (textString != null) {
            this.mStack.newbuf();
            PositionMarker mark = this.mStack.mark();
            appendShortInteger(characterSet);
            appendTextString(textString);
            int length = mark.getLength();
            this.mStack.pop();
            appendValueLength((long) length);
            this.mStack.copy();
        }
    }

    protected void appendUintvarInteger(long j) {
        int i = 0;
        long j2 = 127;
        while (i < 5 && j >= j2) {
            j2 = (j2 << 7) | 127;
            i++;
        }
        while (i > 0) {
            append((int) ((((j >>> (i * 7)) & 127) | 128) & 255));
            i--;
        }
        append((int) (j & 127));
    }

    protected void appendDateValue(long j) {
        appendLongInteger(j);
    }

    protected void appendValueLength(long j) {
        if (j < 31) {
            appendShortLength((int) j);
            return;
        }
        append(31);
        appendUintvarInteger(j);
    }

    protected void appendQuotedString(byte[] bArr) {
        append(34);
        arraycopy(bArr, 0, bArr.length);
        append(0);
    }

    protected void appendQuotedString(String str) {
        appendQuotedString(str.getBytes());
    }

    private EncodedStringValue appendAddressType(EncodedStringValue encodedStringValue) {
        try {
            int checkAddressType = checkAddressType(encodedStringValue.getString());
            EncodedStringValue copy = EncodedStringValue.copy(encodedStringValue);
            if (1 == checkAddressType) {
                copy.appendTextString("/TYPE=PLMN".getBytes());
            } else if (3 == checkAddressType) {
                copy.appendTextString("/TYPE=IPV4".getBytes());
            } else if (4 == checkAddressType) {
                copy.appendTextString("/TYPE=IPV6".getBytes());
            }
            return copy;
        } catch (NullPointerException unused) {
            return null;
        }
    }

    private int appendHeader(int i) {
        switch (i) {
            case 129:
            case 130:
            case 151:
                EncodedStringValue[] encodedStringValues = this.mPduHeader.getEncodedStringValues(i);
                if (encodedStringValues != null) {
                    for (EncodedStringValue encodedStringValue : encodedStringValues) {
                        EncodedStringValue appendAddressType = appendAddressType(encodedStringValue);
                        if (appendAddressType == null) {
                            return 1;
                        }
                        appendOctet(i);
                        appendEncodedString(appendAddressType);
                    }
                    break;
                } else {
                    return 2;
                }
            case 131:
            case 132:
            case 135:
            case EditAboutFragment.ABOUT_MAX_GLYPHS:
            case 142:
            case 146:
            case 147:
            case 148:
            case 153:
            case 154:
            default:
                return 3;
            case 133:
                long longInteger = this.mPduHeader.getLongInteger(i);
                if (-1 != longInteger) {
                    appendOctet(i);
                    appendDateValue(longInteger);
                    break;
                } else {
                    return 2;
                }
            case 134:
            case 143:
            case 144:
            case 145:
            case SignalDatabaseMigrations.DATABASE_VERSION:
            case 155:
                int octet = this.mPduHeader.getOctet(i);
                if (octet != 0) {
                    appendOctet(i);
                    appendOctet(octet);
                    break;
                } else {
                    return 2;
                }
            case 136:
                long longInteger2 = this.mPduHeader.getLongInteger(i);
                if (-1 != longInteger2) {
                    appendOctet(i);
                    this.mStack.newbuf();
                    PositionMarker mark = this.mStack.mark();
                    append(129);
                    appendLongInteger(longInteger2);
                    int length = mark.getLength();
                    this.mStack.pop();
                    appendValueLength((long) length);
                    this.mStack.copy();
                    break;
                } else {
                    return 2;
                }
            case 137:
                appendOctet(i);
                EncodedStringValue encodedStringValue2 = this.mPduHeader.getEncodedStringValue(i);
                if (encodedStringValue2 == null || TextUtils.isEmpty(encodedStringValue2.getString()) || new String(encodedStringValue2.getTextString()).equals("insert-address-token")) {
                    append(1);
                    append(129);
                    break;
                } else {
                    this.mStack.newbuf();
                    PositionMarker mark2 = this.mStack.mark();
                    append(128);
                    EncodedStringValue appendAddressType2 = appendAddressType(encodedStringValue2);
                    if (appendAddressType2 != null) {
                        appendEncodedString(appendAddressType2);
                        int length2 = mark2.getLength();
                        this.mStack.pop();
                        appendValueLength((long) length2);
                        this.mStack.copy();
                        break;
                    } else {
                        return 1;
                    }
                }
                break;
            case 138:
                byte[] textString = this.mPduHeader.getTextString(i);
                if (textString != null) {
                    appendOctet(i);
                    if (!Arrays.equals(textString, "advertisement".getBytes())) {
                        if (!Arrays.equals(textString, "auto".getBytes())) {
                            if (!Arrays.equals(textString, "personal".getBytes())) {
                                if (Arrays.equals(textString, "informational".getBytes())) {
                                    appendOctet(130);
                                    break;
                                } else {
                                    appendTextString(textString);
                                    break;
                                }
                            } else {
                                appendOctet(128);
                                break;
                            }
                        } else {
                            appendOctet(131);
                            break;
                        }
                    } else {
                        appendOctet(129);
                        break;
                    }
                } else {
                    return 2;
                }
            case 139:
            case 152:
                byte[] textString2 = this.mPduHeader.getTextString(i);
                if (textString2 != null) {
                    appendOctet(i);
                    appendTextString(textString2);
                    break;
                } else {
                    return 2;
                }
            case 141:
                appendOctet(i);
                int octet2 = this.mPduHeader.getOctet(i);
                if (octet2 == 0) {
                    appendShortInteger(18);
                    break;
                } else {
                    appendShortInteger(octet2);
                    break;
                }
            case 150:
                EncodedStringValue encodedStringValue3 = this.mPduHeader.getEncodedStringValue(i);
                if (encodedStringValue3 != null) {
                    appendOctet(i);
                    appendEncodedString(encodedStringValue3);
                    break;
                } else {
                    return 2;
                }
        }
        return 0;
    }

    private int makeReadRecInd() {
        if (this.mMessage == null) {
            this.mMessage = new ByteArrayOutputStream();
            this.mPosition = 0;
        }
        appendOctet(EditAboutFragment.ABOUT_MAX_GLYPHS);
        appendOctet(135);
        if (appendHeader(141) != 0 || appendHeader(139) != 0 || appendHeader(151) != 0 || appendHeader(137) != 0) {
            return 1;
        }
        appendHeader(133);
        if (appendHeader(155) != 0) {
            return 1;
        }
        return 0;
    }

    private int makeNotifyResp() {
        if (this.mMessage == null) {
            this.mMessage = new ByteArrayOutputStream();
            this.mPosition = 0;
        }
        appendOctet(EditAboutFragment.ABOUT_MAX_GLYPHS);
        appendOctet(131);
        if (appendHeader(152) == 0 && appendHeader(141) == 0 && appendHeader(SignalDatabaseMigrations.DATABASE_VERSION) == 0) {
            return 0;
        }
        return 1;
    }

    private int makeAckInd() {
        if (this.mMessage == null) {
            this.mMessage = new ByteArrayOutputStream();
            this.mPosition = 0;
        }
        appendOctet(EditAboutFragment.ABOUT_MAX_GLYPHS);
        appendOctet(133);
        if (appendHeader(152) != 0 || appendHeader(141) != 0) {
            return 1;
        }
        appendHeader(145);
        return 0;
    }

    private int makeSendReqPdu() {
        boolean z = false;
        if (this.mMessage == null) {
            this.mMessage = new ByteArrayOutputStream();
            this.mPosition = 0;
        }
        appendOctet(EditAboutFragment.ABOUT_MAX_GLYPHS);
        appendOctet(128);
        appendOctet(152);
        byte[] textString = this.mPduHeader.getTextString(152);
        if (textString != null) {
            appendTextString(textString);
            if (appendHeader(141) != 0) {
                return 1;
            }
            appendHeader(133);
            if (appendHeader(137) != 0) {
                return 1;
            }
            if (appendHeader(151) != 1) {
                z = true;
            }
            if (appendHeader(130) != 1) {
                z = true;
            }
            if (appendHeader(129) != 1) {
                z = true;
            }
            if (!z) {
                return 1;
            }
            appendHeader(150);
            appendHeader(138);
            appendHeader(136);
            appendHeader(143);
            appendHeader(134);
            appendHeader(144);
            appendOctet(132);
            return makeMessageBody();
        }
        throw new IllegalArgumentException("Transaction-ID is null.");
    }

    private int makeMessageBody() {
        int i;
        this.mStack.newbuf();
        PositionMarker mark = this.mStack.mark();
        Integer num = mContentTypeMap.get(new String(this.mPduHeader.getTextString(132)));
        if (num == null) {
            return 1;
        }
        appendShortInteger(num.intValue());
        PduBody body = ((SendReq) this.mPdu).getBody();
        if (body == null || body.getPartsNum() == 0) {
            appendUintvarInteger(0);
            this.mStack.pop();
            this.mStack.copy();
            return 0;
        }
        byte b = 62;
        try {
            PduPart part = body.getPart(0);
            byte[] contentId = part.getContentId();
            if (contentId != null) {
                appendOctet(138);
                if (60 == contentId[0] && 62 == contentId[contentId.length - 1]) {
                    appendTextString(contentId);
                } else {
                    appendTextString("<" + new String(contentId) + ">");
                }
            }
            appendOctet(137);
            appendTextString(part.getContentType());
        } catch (ArrayIndexOutOfBoundsException e) {
            Log.e("PduComposer", "logging error", e);
            e.printStackTrace();
        }
        int length = mark.getLength();
        this.mStack.pop();
        appendValueLength((long) length);
        this.mStack.copy();
        int partsNum = body.getPartsNum();
        appendUintvarInteger((long) partsNum);
        int i2 = 0;
        while (i2 < partsNum) {
            PduPart part2 = body.getPart(i2);
            this.mStack.newbuf();
            PositionMarker mark2 = this.mStack.mark();
            this.mStack.newbuf();
            PositionMarker mark3 = this.mStack.mark();
            byte[] contentType = part2.getContentType();
            if (contentType == null) {
                return 1;
            }
            Integer num2 = mContentTypeMap.get(new String(contentType));
            if (num2 == null) {
                appendTextString(contentType);
            } else {
                appendShortInteger(num2.intValue());
            }
            byte[] name = part2.getName();
            if (name == null && (name = part2.getFilename()) == null && (name = part2.getContentLocation()) == null) {
                return 1;
            }
            appendOctet(133);
            appendTextString(name);
            int charset = part2.getCharset();
            if (charset != 0) {
                appendOctet(129);
                appendShortInteger(charset);
            }
            int length2 = mark3.getLength();
            this.mStack.pop();
            appendValueLength((long) length2);
            this.mStack.copy();
            byte[] contentId2 = part2.getContentId();
            if (contentId2 != null) {
                appendOctet(192);
                if (60 == contentId2[0] && b == contentId2[contentId2.length - 1]) {
                    appendQuotedString(contentId2);
                } else {
                    appendQuotedString("<" + new String(contentId2) + ">");
                }
            }
            byte[] contentLocation = part2.getContentLocation();
            if (contentLocation != null) {
                appendOctet(142);
                appendTextString(contentLocation);
            }
            int length3 = mark2.getLength();
            byte[] data = part2.getData();
            if (data != null) {
                arraycopy(data, 0, data.length);
                i = data.length;
            } else {
                InputStream inputStream = null;
                try {
                    byte[] bArr = new byte[1024];
                    inputStream = this.mResolver.openInputStream(part2.getDataUri());
                    i = 0;
                    while (true) {
                        int read = inputStream.read(bArr);
                        if (read != -1) {
                            this.mMessage.write(bArr, 0, read);
                            this.mPosition += read;
                            i += read;
                        } else {
                            try {
                                break;
                            } catch (IOException unused) {
                            }
                        }
                    }
                    inputStream.close();
                } catch (FileNotFoundException unused2) {
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException unused3) {
                        }
                    }
                    return 1;
                } catch (IOException unused4) {
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException unused5) {
                        }
                    }
                    return 1;
                } catch (RuntimeException unused6) {
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException unused7) {
                        }
                    }
                    return 1;
                } catch (Throwable th) {
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException unused8) {
                        }
                    }
                    throw th;
                }
            }
            if (i == mark2.getLength() - length3) {
                this.mStack.pop();
                appendUintvarInteger((long) length3);
                appendUintvarInteger((long) i);
                this.mStack.copy();
                i2++;
                b = 62;
            } else {
                throw new RuntimeException("BUG: Length sanity check failed");
            }
        }
        return 0;
    }

    /* loaded from: classes3.dex */
    public static class LengthRecordNode {
        ByteArrayOutputStream currentMessage;
        public int currentPosition;
        public LengthRecordNode next;

        private LengthRecordNode() {
            this.currentMessage = null;
            this.currentPosition = 0;
            this.next = null;
        }
    }

    /* loaded from: classes3.dex */
    public class PositionMarker {
        private int c_pos;
        private int currentStackSize;

        private PositionMarker() {
            PduComposer.this = r1;
        }

        int getLength() {
            if (this.currentStackSize == PduComposer.this.mStack.stackSize) {
                return PduComposer.this.mPosition - this.c_pos;
            }
            throw new RuntimeException("BUG: Invalid call to getLength()");
        }
    }

    /* loaded from: classes3.dex */
    public class BufferStack {
        private LengthRecordNode stack;
        int stackSize;
        private LengthRecordNode toCopy;

        private BufferStack() {
            PduComposer.this = r1;
            this.stack = null;
            this.toCopy = null;
            this.stackSize = 0;
        }

        void newbuf() {
            if (this.toCopy == null) {
                LengthRecordNode lengthRecordNode = new LengthRecordNode();
                PduComposer pduComposer = PduComposer.this;
                lengthRecordNode.currentMessage = pduComposer.mMessage;
                lengthRecordNode.currentPosition = pduComposer.mPosition;
                lengthRecordNode.next = this.stack;
                this.stack = lengthRecordNode;
                this.stackSize++;
                pduComposer.mMessage = new ByteArrayOutputStream();
                PduComposer.this.mPosition = 0;
                return;
            }
            throw new RuntimeException("BUG: Invalid newbuf() before copy()");
        }

        void pop() {
            PduComposer pduComposer = PduComposer.this;
            ByteArrayOutputStream byteArrayOutputStream = pduComposer.mMessage;
            int i = pduComposer.mPosition;
            LengthRecordNode lengthRecordNode = this.stack;
            pduComposer.mMessage = lengthRecordNode.currentMessage;
            pduComposer.mPosition = lengthRecordNode.currentPosition;
            this.toCopy = lengthRecordNode;
            this.stack = lengthRecordNode.next;
            this.stackSize--;
            lengthRecordNode.currentMessage = byteArrayOutputStream;
            lengthRecordNode.currentPosition = i;
        }

        void copy() {
            PduComposer.this.arraycopy(this.toCopy.currentMessage.toByteArray(), 0, this.toCopy.currentPosition);
            this.toCopy = null;
        }

        PositionMarker mark() {
            PositionMarker positionMarker = new PositionMarker();
            positionMarker.c_pos = PduComposer.this.mPosition;
            positionMarker.currentStackSize = this.stackSize;
            return positionMarker;
        }
    }

    protected static int checkAddressType(String str) {
        if (str == null) {
            return 5;
        }
        if (str.matches("[0-9]{1,3}\\.{1}[0-9]{1,3}\\.{1}[0-9]{1,3}\\.{1}[0-9]{1,3}")) {
            return 3;
        }
        if (str.matches("\\+?[0-9|\\.|\\-]+")) {
            return 1;
        }
        if (str.matches("[a-zA-Z| ]*\\<{0,1}[a-zA-Z| ]+@{1}[a-zA-Z| ]+\\.{1}[a-zA-Z| ]+\\>{0,1}")) {
            return 2;
        }
        if (str.matches("[a-fA-F]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}\\:{1}[a-fA-F0-9]{4}")) {
            return 4;
        }
        return 5;
    }
}
