package com.google.android.mms.pdu_alt;

import com.google.android.mms.InvalidHeaderValueException;
import org.thoughtcrime.securesms.database.helpers.SignalDatabaseMigrations;

/* loaded from: classes3.dex */
public class NotifyRespInd extends GenericPdu {
    public NotifyRespInd(int i, byte[] bArr, int i2) throws InvalidHeaderValueException {
        setMessageType(131);
        setMmsVersion(i);
        setTransactionId(bArr);
        setStatus(i2);
    }

    public NotifyRespInd(PduHeaders pduHeaders) {
        super(pduHeaders);
    }

    public void setStatus(int i) throws InvalidHeaderValueException {
        this.mPduHeaders.setOctet(i, SignalDatabaseMigrations.DATABASE_VERSION);
    }

    public void setTransactionId(byte[] bArr) {
        this.mPduHeaders.setTextString(bArr, 152);
    }
}
