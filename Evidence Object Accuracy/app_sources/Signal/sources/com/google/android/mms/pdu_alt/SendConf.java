package com.google.android.mms.pdu_alt;

import com.google.android.mms.InvalidHeaderValueException;

/* loaded from: classes3.dex */
public class SendConf extends GenericPdu {
    public SendConf() throws InvalidHeaderValueException {
        setMessageType(129);
    }

    public SendConf(PduHeaders pduHeaders) {
        super(pduHeaders);
    }

    public byte[] getMessageId() {
        return this.mPduHeaders.getTextString(139);
    }

    public int getResponseStatus() {
        return this.mPduHeaders.getOctet(146);
    }

    public byte[] getTransactionId() {
        return this.mPduHeaders.getTextString(152);
    }
}
