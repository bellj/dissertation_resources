package com.google.android.mms;

/* loaded from: classes3.dex */
public class MmsException extends Exception {
    public MmsException(String str) {
        super(str);
    }
}
