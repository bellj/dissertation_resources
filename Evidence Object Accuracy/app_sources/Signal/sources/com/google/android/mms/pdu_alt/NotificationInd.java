package com.google.android.mms.pdu_alt;

import com.google.android.mms.InvalidHeaderValueException;

/* loaded from: classes3.dex */
public class NotificationInd extends GenericPdu {
    public NotificationInd() throws InvalidHeaderValueException {
        setMessageType(130);
    }

    public NotificationInd(PduHeaders pduHeaders) {
        super(pduHeaders);
    }

    public byte[] getContentLocation() {
        return this.mPduHeaders.getTextString(131);
    }

    public long getExpiry() {
        return this.mPduHeaders.getLongInteger(136);
    }

    @Override // com.google.android.mms.pdu_alt.GenericPdu
    public EncodedStringValue getFrom() {
        return this.mPduHeaders.getEncodedStringValue(137);
    }

    public long getMessageSize() {
        return this.mPduHeaders.getLongInteger(142);
    }

    public byte[] getTransactionId() {
        return this.mPduHeaders.getTextString(152);
    }
}
