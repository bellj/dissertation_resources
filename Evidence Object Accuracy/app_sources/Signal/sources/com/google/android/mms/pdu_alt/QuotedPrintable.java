package com.google.android.mms.pdu_alt;

import java.io.ByteArrayOutputStream;

/* loaded from: classes3.dex */
public class QuotedPrintable {
    private static byte ESCAPE_CHAR;

    public static final byte[] decodeQuotedPrintable(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i = 0;
        while (i < bArr.length) {
            byte b = bArr[i];
            if (b == ESCAPE_CHAR) {
                int i2 = i + 1;
                try {
                    byte b2 = bArr[i2];
                    if ('\r' == ((char) b2)) {
                        i += 2;
                        if ('\n' == ((char) bArr[i])) {
                        }
                    }
                    int digit = Character.digit((char) b2, 16);
                    int i3 = i2 + 1;
                    int digit2 = Character.digit((char) bArr[i3], 16);
                    if (!(digit == -1 || digit2 == -1)) {
                        byteArrayOutputStream.write((char) ((digit << 4) + digit2));
                        i = i3;
                    }
                } catch (ArrayIndexOutOfBoundsException unused) {
                }
                return null;
            }
            byteArrayOutputStream.write(b);
            i++;
        }
        return byteArrayOutputStream.toByteArray();
    }
}
