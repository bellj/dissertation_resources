package com.google.android.mms.pdu_alt;

import com.klinker.android.logger.Log;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/* loaded from: classes3.dex */
public class EncodedStringValue implements Cloneable {
    private int mCharacterSet;
    private byte[] mData;

    public EncodedStringValue(int i, byte[] bArr) {
        if (bArr != null) {
            this.mCharacterSet = i;
            byte[] bArr2 = new byte[bArr.length];
            this.mData = bArr2;
            System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
            return;
        }
        throw new NullPointerException("EncodedStringValue: Text-string is null.");
    }

    public EncodedStringValue(byte[] bArr) {
        this(106, bArr);
    }

    public EncodedStringValue(String str) {
        try {
            this.mData = str.getBytes("utf-8");
            this.mCharacterSet = 106;
        } catch (UnsupportedEncodingException e) {
            Log.e("EncodedStringValue", "Default encoding must be supported.", e);
        }
    }

    public int getCharacterSet() {
        return this.mCharacterSet;
    }

    public byte[] getTextString() {
        byte[] bArr = this.mData;
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        return bArr2;
    }

    public void setTextString(byte[] bArr) {
        if (bArr != null) {
            byte[] bArr2 = new byte[bArr.length];
            this.mData = bArr2;
            System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
            return;
        }
        throw new NullPointerException("EncodedStringValue: Text-string is null.");
    }

    public String getString() {
        int i = this.mCharacterSet;
        if (i == 0) {
            return new String(this.mData);
        }
        try {
            try {
                return new String(this.mData, CharacterSets.getMimeName(i));
            } catch (UnsupportedEncodingException unused) {
                return new String(this.mData);
            }
        } catch (UnsupportedEncodingException unused2) {
            return new String(this.mData, "iso-8859-1");
        }
    }

    public void appendTextString(byte[] bArr) {
        if (bArr == null) {
            throw new NullPointerException("Text-string is null.");
        } else if (this.mData == null) {
            byte[] bArr2 = new byte[bArr.length];
            this.mData = bArr2;
            System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        } else {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                byteArrayOutputStream.write(this.mData);
                byteArrayOutputStream.write(bArr);
                this.mData = byteArrayOutputStream.toByteArray();
            } catch (IOException e) {
                Log.e("EncodedStringValue", "logging error", e);
                e.printStackTrace();
                throw new NullPointerException("appendTextString: failed when write a new Text-string");
            }
        }
    }

    @Override // java.lang.Object
    public Object clone() throws CloneNotSupportedException {
        super.clone();
        byte[] bArr = this.mData;
        int length = bArr.length;
        byte[] bArr2 = new byte[length];
        System.arraycopy(bArr, 0, bArr2, 0, length);
        try {
            return new EncodedStringValue(this.mCharacterSet, bArr2);
        } catch (Exception e) {
            Log.e("EncodedStringValue", "logging error", e);
            e.printStackTrace();
            throw new CloneNotSupportedException(e.getMessage());
        }
    }

    public static EncodedStringValue copy(EncodedStringValue encodedStringValue) {
        if (encodedStringValue == null) {
            return null;
        }
        return new EncodedStringValue(encodedStringValue.mCharacterSet, encodedStringValue.mData);
    }
}
