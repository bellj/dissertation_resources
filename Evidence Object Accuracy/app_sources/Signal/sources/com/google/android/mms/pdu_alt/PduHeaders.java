package com.google.android.mms.pdu_alt;

import java.util.ArrayList;
import java.util.HashMap;

/* loaded from: classes3.dex */
public class PduHeaders {
    private HashMap<Integer, Object> mHeaderMap;

    public PduHeaders() {
        this.mHeaderMap = null;
        this.mHeaderMap = new HashMap<>();
    }

    public int getOctet(int i) {
        Integer num = (Integer) this.mHeaderMap.get(Integer.valueOf(i));
        if (num == null) {
            return 0;
        }
        return num.intValue();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0105, code lost:
        if (r9 <= 255) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x010c, code lost:
        if (r9 < 192) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x010e, code lost:
        if (r9 > 255) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x011e, code lost:
        if (r9 <= 255) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x0126, code lost:
        if (r9 < 192) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0128, code lost:
        if (r9 > 255) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x007a, code lost:
        if (r9 <= 255) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0082, code lost:
        if (r9 < 192) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0084, code lost:
        if (r9 > 255) goto L_0x0086;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setOctet(int r9, int r10) throws com.google.android.mms.InvalidHeaderValueException {
        /*
        // Method dump skipped, instructions count: 348
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.mms.pdu_alt.PduHeaders.setOctet(int, int):void");
    }

    public byte[] getTextString(int i) {
        return (byte[]) this.mHeaderMap.get(Integer.valueOf(i));
    }

    public void setTextString(byte[] bArr, int i) {
        bArr.getClass();
        if (!(i == 131 || i == 132 || i == 138 || i == 139 || i == 152 || i == 158 || i == 189 || i == 190)) {
            switch (i) {
                case 183:
                case 184:
                case 185:
                    break;
                default:
                    throw new RuntimeException("Invalid header field!");
            }
        }
        this.mHeaderMap.put(Integer.valueOf(i), bArr);
    }

    public EncodedStringValue getEncodedStringValue(int i) {
        return (EncodedStringValue) this.mHeaderMap.get(Integer.valueOf(i));
    }

    public EncodedStringValue[] getEncodedStringValues(int i) {
        ArrayList arrayList = (ArrayList) this.mHeaderMap.get(Integer.valueOf(i));
        if (arrayList == null) {
            return null;
        }
        return (EncodedStringValue[]) arrayList.toArray(new EncodedStringValue[arrayList.size()]);
    }

    public void setEncodedStringValue(EncodedStringValue encodedStringValue, int i) {
        encodedStringValue.getClass();
        if (i == 137 || i == 147 || i == 150 || i == 154 || i == 160 || i == 164 || i == 166 || i == 181 || i == 182) {
            this.mHeaderMap.put(Integer.valueOf(i), encodedStringValue);
            return;
        }
        throw new RuntimeException("Invalid header field!");
    }

    public void appendEncodedStringValue(EncodedStringValue encodedStringValue, int i) {
        encodedStringValue.getClass();
        if (i == 129 || i == 130 || i == 151) {
            ArrayList arrayList = (ArrayList) this.mHeaderMap.get(Integer.valueOf(i));
            if (arrayList == null) {
                arrayList = new ArrayList();
            }
            arrayList.add(encodedStringValue);
            this.mHeaderMap.put(Integer.valueOf(i), arrayList);
            return;
        }
        throw new RuntimeException("Invalid header field!");
    }

    public long getLongInteger(int i) {
        Long l = (Long) this.mHeaderMap.get(Integer.valueOf(i));
        if (l == null) {
            return -1;
        }
        return l.longValue();
    }

    public void setLongInteger(long j, int i) {
        if (i == 133 || i == 142 || i == 157 || i == 159 || i == 161 || i == 173 || i == 175 || i == 179 || i == 135 || i == 136) {
            this.mHeaderMap.put(Integer.valueOf(i), Long.valueOf(j));
            return;
        }
        throw new RuntimeException("Invalid header field!");
    }
}
