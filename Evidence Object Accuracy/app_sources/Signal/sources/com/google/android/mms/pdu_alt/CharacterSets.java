package com.google.android.mms.pdu_alt;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import org.thoughtcrime.securesms.util.MessageRecordUtil;

/* loaded from: classes3.dex */
public class CharacterSets {
    private static final int[] MIBENUM_NUMBERS;
    private static final HashMap<Integer, String> MIBENUM_TO_NAME_MAP = new HashMap<>();
    private static final String[] MIME_NAMES = {"*", "us-ascii", "iso-8859-1", "iso-8859-2", "iso-8859-3", "iso-8859-4", "iso-8859-5", "iso-8859-6", "iso-8859-7", "iso-8859-8", "iso-8859-9", "shift_JIS", "utf-8", "big5", "iso-10646-ucs-2", "utf-16"};
    private static final HashMap<String, Integer> NAME_TO_MIBENUM_MAP = new HashMap<>();

    static {
        int[] iArr = {0, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 17, 106, 2026, MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH, 1015};
        MIBENUM_NUMBERS = iArr;
        MIME_NAMES = new String[]{"*", "us-ascii", "iso-8859-1", "iso-8859-2", "iso-8859-3", "iso-8859-4", "iso-8859-5", "iso-8859-6", "iso-8859-7", "iso-8859-8", "iso-8859-9", "shift_JIS", "utf-8", "big5", "iso-10646-ucs-2", "utf-16"};
        MIBENUM_TO_NAME_MAP = new HashMap<>();
        NAME_TO_MIBENUM_MAP = new HashMap<>();
        int length = iArr.length - 1;
        for (int i = 0; i <= length; i++) {
            HashMap<Integer, String> hashMap = MIBENUM_TO_NAME_MAP;
            int[] iArr2 = MIBENUM_NUMBERS;
            Integer valueOf = Integer.valueOf(iArr2[i]);
            String[] strArr = MIME_NAMES;
            hashMap.put(valueOf, strArr[i]);
            NAME_TO_MIBENUM_MAP.put(strArr[i], Integer.valueOf(iArr2[i]));
        }
    }

    public static String getMimeName(int i) throws UnsupportedEncodingException {
        String str = MIBENUM_TO_NAME_MAP.get(Integer.valueOf(i));
        if (str != null) {
            return str;
        }
        throw new UnsupportedEncodingException();
    }

    public static int getMibEnumValue(String str) throws UnsupportedEncodingException {
        if (str == null) {
            return -1;
        }
        Integer num = NAME_TO_MIBENUM_MAP.get(str);
        if (num != null) {
            return num.intValue();
        }
        throw new UnsupportedEncodingException();
    }
}
