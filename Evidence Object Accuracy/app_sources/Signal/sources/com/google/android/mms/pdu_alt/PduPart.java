package com.google.android.mms.pdu_alt;

import android.net.Uri;
import java.util.HashMap;
import java.util.Map;

/* loaded from: classes3.dex */
public class PduPart {
    static final byte[] DISPOSITION_ATTACHMENT = "attachment".getBytes();
    static final byte[] DISPOSITION_FROM_DATA = "from-data".getBytes();
    static final byte[] DISPOSITION_INLINE = "inline".getBytes();
    private byte[] mPartData;
    private Map<Integer, Object> mPartHeader;
    private Uri mUri;

    public PduPart() {
        this.mPartHeader = null;
        this.mUri = null;
        this.mPartData = null;
        this.mPartHeader = new HashMap();
    }

    public void setData(byte[] bArr) {
        if (bArr != null) {
            byte[] bArr2 = new byte[bArr.length];
            this.mPartData = bArr2;
            System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        }
    }

    public byte[] getData() {
        byte[] bArr = this.mPartData;
        if (bArr == null) {
            return null;
        }
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        return bArr2;
    }

    public Uri getDataUri() {
        return this.mUri;
    }

    public void setContentId(byte[] bArr) {
        if (bArr == null || bArr.length == 0) {
            throw new IllegalArgumentException("Content-Id may not be null or empty.");
        } else if (bArr.length > 1 && ((char) bArr[0]) == '<' && ((char) bArr[bArr.length - 1]) == '>') {
            this.mPartHeader.put(192, bArr);
        } else {
            int length = bArr.length + 2;
            byte[] bArr2 = new byte[length];
            bArr2[0] = 60;
            bArr2[length - 1] = 62;
            System.arraycopy(bArr, 0, bArr2, 1, bArr.length);
            this.mPartHeader.put(192, bArr2);
        }
    }

    public byte[] getContentId() {
        return (byte[]) this.mPartHeader.get(192);
    }

    public void setCharset(int i) {
        this.mPartHeader.put(129, Integer.valueOf(i));
    }

    public int getCharset() {
        Integer num = (Integer) this.mPartHeader.get(129);
        if (num == null) {
            return 0;
        }
        return num.intValue();
    }

    public void setContentLocation(byte[] bArr) {
        if (bArr != null) {
            this.mPartHeader.put(142, bArr);
            return;
        }
        throw new NullPointerException("null content-location");
    }

    public byte[] getContentLocation() {
        return (byte[]) this.mPartHeader.get(142);
    }

    public void setContentDisposition(byte[] bArr) {
        if (bArr != null) {
            this.mPartHeader.put(197, bArr);
            return;
        }
        throw new NullPointerException("null content-disposition");
    }

    public void setContentType(byte[] bArr) {
        if (bArr != null) {
            this.mPartHeader.put(145, bArr);
            return;
        }
        throw new NullPointerException("null content-type");
    }

    public byte[] getContentType() {
        return (byte[]) this.mPartHeader.get(145);
    }

    public void setContentTransferEncoding(byte[] bArr) {
        if (bArr != null) {
            this.mPartHeader.put(200, bArr);
            return;
        }
        throw new NullPointerException("null content-transfer-encoding");
    }

    public byte[] getContentTransferEncoding() {
        return (byte[]) this.mPartHeader.get(200);
    }

    public void setName(byte[] bArr) {
        if (bArr != null) {
            this.mPartHeader.put(151, bArr);
            return;
        }
        throw new NullPointerException("null content-id");
    }

    public byte[] getName() {
        return (byte[]) this.mPartHeader.get(151);
    }

    public void setFilename(byte[] bArr) {
        if (bArr != null) {
            this.mPartHeader.put(152, bArr);
            return;
        }
        throw new NullPointerException("null content-id");
    }

    public byte[] getFilename() {
        return (byte[]) this.mPartHeader.get(152);
    }

    public String generateLocation() {
        byte[] bArr = (byte[]) this.mPartHeader.get(151);
        if (bArr == null && (bArr = (byte[]) this.mPartHeader.get(152)) == null) {
            bArr = (byte[]) this.mPartHeader.get(142);
        }
        if (bArr != null) {
            return new String(bArr);
        }
        return "cid:" + new String((byte[]) this.mPartHeader.get(192));
    }
}
