package com.google.android.mms;

/* loaded from: classes3.dex */
public class InvalidHeaderValueException extends MmsException {
    public InvalidHeaderValueException(String str) {
        super(str);
    }
}
