package com.google.android.mms;

import java.util.ArrayList;
import org.thoughtcrime.securesms.util.MediaUtil;

/* loaded from: classes3.dex */
public class ContentType {
    private static final ArrayList<String> sSupportedAudioTypes;
    private static final ArrayList<String> sSupportedContentTypes;
    private static final ArrayList<String> sSupportedImageTypes;
    private static final ArrayList<String> sSupportedVideoTypes;

    static {
        ArrayList<String> arrayList = new ArrayList<>();
        sSupportedContentTypes = arrayList;
        ArrayList<String> arrayList2 = new ArrayList<>();
        sSupportedImageTypes = arrayList2;
        ArrayList<String> arrayList3 = new ArrayList<>();
        sSupportedAudioTypes = arrayList3;
        ArrayList<String> arrayList4 = new ArrayList<>();
        sSupportedVideoTypes = arrayList4;
        arrayList.add("text/plain");
        arrayList.add("text/html");
        arrayList.add("text/x-vCalendar");
        arrayList.add("text/x-vCard");
        arrayList.add(MediaUtil.IMAGE_JPEG);
        arrayList.add(MediaUtil.IMAGE_GIF);
        arrayList.add("image/vnd.wap.wbmp");
        arrayList.add(MediaUtil.IMAGE_PNG);
        arrayList.add("image/jpg");
        arrayList.add("image/x-ms-bmp");
        arrayList.add(MediaUtil.AUDIO_AAC);
        arrayList.add("audio/aac_mp4");
        arrayList.add("audio/qcelp");
        arrayList.add("audio/evrc");
        arrayList.add("audio/amr");
        arrayList.add("audio/imelody");
        arrayList.add("audio/mid");
        arrayList.add("audio/midi");
        arrayList.add("audio/mp3");
        arrayList.add("audio/mp4");
        arrayList.add("audio/mpeg3");
        arrayList.add("audio/mpeg");
        arrayList.add("audio/mpg");
        arrayList.add("audio/x-mid");
        arrayList.add("audio/x-midi");
        arrayList.add("audio/x-mp3");
        arrayList.add("audio/x-mpeg3");
        arrayList.add("audio/x-mpeg");
        arrayList.add("audio/x-mpg");
        arrayList.add("audio/x-wav");
        arrayList.add("audio/3gpp");
        arrayList.add("application/ogg");
        arrayList.add("video/3gpp");
        arrayList.add("video/3gpp2");
        arrayList.add("video/h263");
        arrayList.add("video/mp4");
        arrayList.add("application/smil");
        arrayList.add("application/vnd.wap.xhtml+xml");
        arrayList.add("application/xhtml+xml");
        arrayList.add("application/vnd.oma.drm.content");
        arrayList.add("application/vnd.oma.drm.message");
        arrayList2.add(MediaUtil.IMAGE_JPEG);
        arrayList2.add(MediaUtil.IMAGE_GIF);
        arrayList2.add("image/vnd.wap.wbmp");
        arrayList2.add(MediaUtil.IMAGE_PNG);
        arrayList2.add("image/jpg");
        arrayList2.add("image/x-ms-bmp");
        arrayList3.add(MediaUtil.AUDIO_AAC);
        arrayList3.add("audio/aac_mp4");
        arrayList3.add("audio/qcelp");
        arrayList3.add("audio/evrc");
        arrayList3.add("audio/amr");
        arrayList3.add("audio/imelody");
        arrayList3.add("audio/mid");
        arrayList3.add("audio/midi");
        arrayList3.add("audio/mp3");
        arrayList3.add("audio/mpeg3");
        arrayList3.add("audio/mpeg");
        arrayList3.add("audio/mpg");
        arrayList3.add("audio/mp4");
        arrayList3.add("audio/x-mid");
        arrayList3.add("audio/x-midi");
        arrayList3.add("audio/x-mp3");
        arrayList3.add("audio/x-mpeg3");
        arrayList3.add("audio/x-mpeg");
        arrayList3.add("audio/x-mpg");
        arrayList3.add("audio/x-wav");
        arrayList3.add("audio/3gpp");
        arrayList3.add("application/ogg");
        arrayList4.add("video/3gpp");
        arrayList4.add("video/3gpp2");
        arrayList4.add("video/h263");
        arrayList4.add("video/mp4");
    }

    public static boolean isTextType(String str) {
        return str != null && str.startsWith("text/");
    }

    public static boolean isImageType(String str) {
        return str != null && str.startsWith("image/");
    }

    public static boolean isAudioType(String str) {
        return str != null && str.startsWith("audio/");
    }

    public static boolean isVideoType(String str) {
        return str != null && str.startsWith("video/");
    }
}
