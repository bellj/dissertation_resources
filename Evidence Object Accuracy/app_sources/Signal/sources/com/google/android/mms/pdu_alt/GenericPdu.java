package com.google.android.mms.pdu_alt;

import com.google.android.mms.InvalidHeaderValueException;
import org.thoughtcrime.securesms.profiles.manage.EditAboutFragment;

/* loaded from: classes3.dex */
public class GenericPdu {
    PduHeaders mPduHeaders;

    public GenericPdu() {
        this.mPduHeaders = null;
        this.mPduHeaders = new PduHeaders();
    }

    public GenericPdu(PduHeaders pduHeaders) {
        this.mPduHeaders = pduHeaders;
    }

    public PduHeaders getPduHeaders() {
        return this.mPduHeaders;
    }

    public int getMessageType() {
        return this.mPduHeaders.getOctet(EditAboutFragment.ABOUT_MAX_GLYPHS);
    }

    public void setMessageType(int i) throws InvalidHeaderValueException {
        this.mPduHeaders.setOctet(i, EditAboutFragment.ABOUT_MAX_GLYPHS);
    }

    public int getMmsVersion() {
        return this.mPduHeaders.getOctet(141);
    }

    public void setMmsVersion(int i) throws InvalidHeaderValueException {
        this.mPduHeaders.setOctet(i, 141);
    }

    public EncodedStringValue getFrom() {
        return this.mPduHeaders.getEncodedStringValue(137);
    }

    public void setFrom(EncodedStringValue encodedStringValue) {
        this.mPduHeaders.setEncodedStringValue(encodedStringValue, 137);
    }
}
