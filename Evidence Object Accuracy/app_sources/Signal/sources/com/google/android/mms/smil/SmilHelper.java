package com.google.android.mms.smil;

import com.android.mms.dom.smil.SmilDocumentImpl;
import com.google.android.mms.ContentType;
import com.google.android.mms.pdu_alt.PduBody;
import com.google.android.mms.pdu_alt.PduPart;
import com.klinker.android.logger.Log;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.w3c.dom.smil.SMILDocument;
import org.w3c.dom.smil.SMILElement;
import org.w3c.dom.smil.SMILLayoutElement;
import org.w3c.dom.smil.SMILMediaElement;
import org.w3c.dom.smil.SMILParElement;

/* loaded from: classes3.dex */
public class SmilHelper {
    public static SMILDocument createSmilDocument(PduBody pduBody) {
        SmilDocumentImpl smilDocumentImpl = new SmilDocumentImpl();
        SMILElement sMILElement = (SMILElement) smilDocumentImpl.createElement("smil");
        sMILElement.setAttribute("xmlns", "http://www.w3.org/2001/SMIL20/Language");
        smilDocumentImpl.appendChild(sMILElement);
        SMILElement sMILElement2 = (SMILElement) smilDocumentImpl.createElement("head");
        sMILElement.appendChild(sMILElement2);
        sMILElement2.appendChild((SMILLayoutElement) smilDocumentImpl.createElement("layout"));
        sMILElement.appendChild((SMILElement) smilDocumentImpl.createElement("body"));
        SMILParElement addPar = addPar(smilDocumentImpl);
        int partsNum = pduBody.getPartsNum();
        if (partsNum == 0) {
            return smilDocumentImpl;
        }
        boolean z = false;
        boolean z2 = false;
        for (int i = 0; i < partsNum; i++) {
            if (addPar == null || (z && z2)) {
                addPar = addPar(smilDocumentImpl);
                z = false;
                z2 = false;
            }
            PduPart part = pduBody.getPart(i);
            String str = new String(part.getContentType());
            if (str.equals("text/plain") || str.equalsIgnoreCase("application/vnd.wap.xhtml+xml") || str.equals("text/html")) {
                addPar.appendChild(createMediaElement(DraftDatabase.Draft.TEXT, smilDocumentImpl, part.generateLocation()));
                z2 = true;
            } else {
                if (ContentType.isImageType(str)) {
                    addPar.appendChild(createMediaElement("img", smilDocumentImpl, part.generateLocation()));
                } else if (ContentType.isVideoType(str)) {
                    addPar.appendChild(createMediaElement("video", smilDocumentImpl, part.generateLocation()));
                } else if (ContentType.isAudioType(str)) {
                    addPar.appendChild(createMediaElement("audio", smilDocumentImpl, part.generateLocation()));
                } else if (str.equals("text/x-vCard")) {
                    addPar.appendChild(createMediaElement("vcard", smilDocumentImpl, part.generateLocation()));
                } else {
                    Log.e("creating_smil_document", "unknown mimetype");
                }
                z = true;
            }
        }
        return smilDocumentImpl;
    }

    public static SMILParElement addPar(SMILDocument sMILDocument) {
        SMILParElement sMILParElement = (SMILParElement) sMILDocument.createElement("par");
        sMILParElement.setDur(8.0f);
        sMILDocument.getBody().appendChild(sMILParElement);
        return sMILParElement;
    }

    public static SMILMediaElement createMediaElement(String str, SMILDocument sMILDocument, String str2) {
        SMILMediaElement sMILMediaElement = (SMILMediaElement) sMILDocument.createElement(str);
        sMILMediaElement.setSrc(escapeXML(str2));
        return sMILMediaElement;
    }

    public static String escapeXML(String str) {
        return str.replaceAll("&", "&amp;").replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\"", "&quot;").replaceAll("'", "&apos;");
    }
}
