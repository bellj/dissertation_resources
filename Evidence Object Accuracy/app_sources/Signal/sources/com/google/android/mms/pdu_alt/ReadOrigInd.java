package com.google.android.mms.pdu_alt;

/* loaded from: classes3.dex */
public class ReadOrigInd extends GenericPdu {
    public ReadOrigInd(PduHeaders pduHeaders) {
        super(pduHeaders);
    }

    @Override // com.google.android.mms.pdu_alt.GenericPdu
    public EncodedStringValue getFrom() {
        return this.mPduHeaders.getEncodedStringValue(137);
    }
}
