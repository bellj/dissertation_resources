package com.google.android.material;

/* loaded from: classes3.dex */
public final class R$color {
    public static final int design_error;
    public static final int design_fab_stroke_end_inner_color;
    public static final int design_fab_stroke_end_outer_color;
    public static final int design_fab_stroke_top_inner_color;
    public static final int design_fab_stroke_top_outer_color;
    public static final int material_timepicker_clockface;
    public static final int mtrl_filled_background_color;
    public static final int mtrl_textinput_default_box_stroke_color;
    public static final int mtrl_textinput_disabled_color;
    public static final int mtrl_textinput_hovered_box_stroke_color;
}
