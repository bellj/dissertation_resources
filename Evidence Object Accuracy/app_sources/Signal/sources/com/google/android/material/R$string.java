package com.google.android.material;

/* loaded from: classes3.dex */
public final class R$string {
    public static final int bottomsheet_action_expand_halfway;
    public static final int character_counter_content_description;
    public static final int character_counter_overflowed_content_description;
    public static final int character_counter_pattern;
    public static final int clear_text_end_icon_content_description;
    public static final int error_icon_content_description;
    public static final int exposed_dropdown_menu_content_description;
    public static final int item_view_role_description;
    public static final int material_hour_selection;
    public static final int material_hour_suffix;
    public static final int material_minute_selection;
    public static final int material_minute_suffix;
    public static final int material_timepicker_clock_mode_description;
    public static final int material_timepicker_hour;
    public static final int material_timepicker_minute;
    public static final int material_timepicker_text_input_mode_description;
    public static final int mtrl_badge_numberless_content_description;
    public static final int mtrl_chip_close_icon_content_description;
    public static final int mtrl_exceed_max_badge_number_content_description;
    public static final int mtrl_exceed_max_badge_number_suffix;
    public static final int mtrl_picker_announce_current_selection;
    public static final int mtrl_picker_day_of_week_column_header;
    public static final int mtrl_picker_navigate_to_year_description;
    public static final int mtrl_picker_toggle_to_calendar_input_mode;
    public static final int mtrl_picker_toggle_to_day_selection;
    public static final int mtrl_picker_toggle_to_text_input_mode;
    public static final int mtrl_picker_toggle_to_year_selection;
    public static final int password_toggle_content_description;
}
