package com.google.android.material;

/* loaded from: classes3.dex */
public final class R$style {
    public static final int MaterialAlertDialog_MaterialComponents;
    public static final int TextAppearance_AppCompat_Caption;
    public static final int TextAppearance_Design_CollapsingToolbar_Expanded;
    public static final int TextAppearance_Design_Tab;
    public static final int TextAppearance_MaterialComponents_Badge;
    public static final int Theme_Design_Light_BottomSheetDialog;
    public static final int Widget_AppCompat_AutoCompleteTextView;
    public static final int Widget_Design_AppBarLayout;
    public static final int Widget_Design_BottomSheet_Modal;
    public static final int Widget_Design_CollapsingToolbar;
    public static final int Widget_Design_FloatingActionButton;
    public static final int Widget_Design_TabLayout;
    public static final int Widget_Design_TextInputEditText;
    public static final int Widget_Design_TextInputLayout;
    public static final int Widget_MaterialComponents_Badge;
    public static final int Widget_MaterialComponents_BottomAppBar;
    public static final int Widget_MaterialComponents_Button;
    public static final int Widget_MaterialComponents_CardView;
    public static final int Widget_MaterialComponents_ChipGroup;
    public static final int Widget_MaterialComponents_Chip_Action;
    public static final int Widget_MaterialComponents_CircularProgressIndicator;
    public static final int Widget_MaterialComponents_CompoundButton_CheckBox;
    public static final int Widget_MaterialComponents_CompoundButton_RadioButton;
    public static final int Widget_MaterialComponents_CompoundButton_Switch;
    public static final int Widget_MaterialComponents_ExtendedFloatingActionButton_Icon;
    public static final int Widget_MaterialComponents_MaterialButtonToggleGroup;
    public static final int Widget_MaterialComponents_MaterialCalendar;
    public static final int Widget_MaterialComponents_ProgressIndicator;
    public static final int Widget_MaterialComponents_ShapeableImageView;
    public static final int Widget_MaterialComponents_TimePicker;
    public static final int Widget_MaterialComponents_TimePicker_Clock;
    public static final int Widget_MaterialComponents_Toolbar;
}
