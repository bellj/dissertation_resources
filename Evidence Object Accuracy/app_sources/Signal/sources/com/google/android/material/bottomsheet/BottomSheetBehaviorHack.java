package com.google.android.material.bottomsheet;

import android.view.View;
import android.widget.FrameLayout;
import java.lang.ref.WeakReference;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: BottomSheetBehaviorHack.kt */
@Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\u00020\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\b\u001a\u00020\t¨\u0006\n"}, d2 = {"Lcom/google/android/material/bottomsheet/BottomSheetBehaviorHack;", "", "()V", "setNestedScrollingChild", "", "behavior", "Lcom/google/android/material/bottomsheet/BottomSheetBehavior;", "Landroid/widget/FrameLayout;", "view", "Landroid/view/View;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class BottomSheetBehaviorHack {
    public static final BottomSheetBehaviorHack INSTANCE = new BottomSheetBehaviorHack();

    private BottomSheetBehaviorHack() {
    }

    public final void setNestedScrollingChild(BottomSheetBehavior<FrameLayout> bottomSheetBehavior, View view) {
        Intrinsics.checkNotNullParameter(bottomSheetBehavior, "behavior");
        Intrinsics.checkNotNullParameter(view, "view");
        bottomSheetBehavior.nestedScrollingChildRef = new WeakReference<>(view);
    }
}
