package com.google.android.material.timepicker;

/* access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public interface TimePickerPresenter {
    void hide();

    void invalidate();

    void show();
}
