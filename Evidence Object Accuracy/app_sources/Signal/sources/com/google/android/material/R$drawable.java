package com.google.android.material;

/* loaded from: classes3.dex */
public final class R$drawable {
    public static final int design_password_eye;
    public static final int material_ic_calendar_black_24dp;
    public static final int material_ic_edit_black_24dp;
    public static final int mtrl_dropdown_arrow;
    public static final int mtrl_ic_arrow_drop_down;
    public static final int mtrl_ic_cancel;
    public static final int navigation_empty_icon;
}
