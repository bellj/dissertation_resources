package com.google.android.material;

/* loaded from: classes3.dex */
public final class R$layout {
    public static final int design_bottom_sheet_dialog;
    public static final int design_layout_snackbar;
    public static final int design_layout_snackbar_include;
    public static final int design_layout_tab_icon;
    public static final int design_layout_tab_text;
    public static final int design_navigation_menu_item;
    public static final int design_text_input_end_icon;
    public static final int design_text_input_start_icon;
    public static final int material_clockface_textview;
    public static final int material_clockface_view;
    public static final int material_radial_view_group;
    public static final int material_time_chip;
    public static final int material_time_input;
    public static final int material_timepicker;
    public static final int material_timepicker_dialog;
    public static final int mtrl_calendar_day;
    public static final int mtrl_calendar_day_of_week;
    public static final int mtrl_calendar_horizontal;
    public static final int mtrl_calendar_month_labeled;
    public static final int mtrl_calendar_vertical;
    public static final int mtrl_calendar_year;
    public static final int mtrl_layout_snackbar;
    public static final int mtrl_layout_snackbar_include;
    public static final int mtrl_picker_dialog;
    public static final int mtrl_picker_fullscreen;
}
