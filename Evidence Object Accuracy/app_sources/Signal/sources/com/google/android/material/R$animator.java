package com.google.android.material;

/* loaded from: classes3.dex */
public final class R$animator {
    public static final int mtrl_fab_hide_motion_spec;
    public static final int mtrl_fab_show_motion_spec;
    public static final int mtrl_fab_transformation_sheet_collapse_spec;
    public static final int mtrl_fab_transformation_sheet_expand_spec;
}
