package com.google.android.datatransport.runtime.scheduling.persistence;

import com.google.android.datatransport.runtime.scheduling.persistence.AutoValue_EventStoreConfig;

/* loaded from: classes.dex */
public abstract class EventStoreConfig {
    static final EventStoreConfig DEFAULT = builder().setMaxStorageSizeInBytes(10485760).setLoadBatchSize(200).setCriticalSectionEnterTimeoutMs(10000).setEventCleanUpAge(604800000).setMaxBlobByteSizePerRow(81920).build();

    /* access modifiers changed from: package-private */
    /* loaded from: classes.dex */
    public static abstract class Builder {
        abstract EventStoreConfig build();

        abstract Builder setCriticalSectionEnterTimeoutMs(int i);

        abstract Builder setEventCleanUpAge(long j);

        abstract Builder setLoadBatchSize(int i);

        abstract Builder setMaxBlobByteSizePerRow(int i);

        abstract Builder setMaxStorageSizeInBytes(long j);
    }

    /* access modifiers changed from: package-private */
    public abstract int getCriticalSectionEnterTimeoutMs();

    /* access modifiers changed from: package-private */
    public abstract long getEventCleanUpAge();

    /* access modifiers changed from: package-private */
    public abstract int getLoadBatchSize();

    /* access modifiers changed from: package-private */
    public abstract int getMaxBlobByteSizePerRow();

    /* access modifiers changed from: package-private */
    public abstract long getMaxStorageSizeInBytes();

    static Builder builder() {
        return new AutoValue_EventStoreConfig.Builder();
    }
}
