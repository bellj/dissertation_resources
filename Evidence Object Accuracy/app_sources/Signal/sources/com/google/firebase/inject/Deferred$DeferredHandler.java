package com.google.firebase.inject;

/* loaded from: classes3.dex */
public interface Deferred$DeferredHandler<T> {
    void handle(Provider<T> provider);
}
