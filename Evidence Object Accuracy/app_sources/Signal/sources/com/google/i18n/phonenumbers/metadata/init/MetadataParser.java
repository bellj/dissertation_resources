package com.google.i18n.phonenumbers.metadata.init;

import com.google.i18n.phonenumbers.Phonemetadata$PhoneMetadata;
import com.google.i18n.phonenumbers.Phonemetadata$PhoneMetadataCollection;
import java.io.Externalizable;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/* loaded from: classes3.dex */
public final class MetadataParser {
    private static final Logger logger = Logger.getLogger(MetadataParser.class.getName());
    private final boolean strictMode;

    public static MetadataParser newLenientParser() {
        return new MetadataParser(false);
    }

    private MetadataParser(boolean z) {
        this.strictMode = z;
    }

    public Collection<Phonemetadata$PhoneMetadata> parse(InputStream inputStream) {
        Throwable th;
        IOException e;
        if (inputStream == null) {
            return handleNullSource();
        }
        try {
        } catch (Throwable th2) {
            th = th2;
        }
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
            try {
                Phonemetadata$PhoneMetadataCollection phonemetadata$PhoneMetadataCollection = new Externalizable() { // from class: com.google.i18n.phonenumbers.Phonemetadata$PhoneMetadataCollection
                    private List<Phonemetadata$PhoneMetadata> metadata_ = new ArrayList();

                    public List<Phonemetadata$PhoneMetadata> getMetadataList() {
                        return this.metadata_;
                    }

                    public int getMetadataCount() {
                        return this.metadata_.size();
                    }

                    @Override // java.io.Externalizable
                    public void writeExternal(ObjectOutput objectOutput) throws IOException {
                        int metadataCount = getMetadataCount();
                        objectOutput.writeInt(metadataCount);
                        for (int i = 0; i < metadataCount; i++) {
                            this.metadata_.get(i).writeExternal(objectOutput);
                        }
                    }

                    @Override // java.io.Externalizable
                    public void readExternal(ObjectInput objectInput) throws IOException {
                        int readInt = objectInput.readInt();
                        for (int i = 0; i < readInt; i++) {
                            Phonemetadata$PhoneMetadata phonemetadata$PhoneMetadata = new Phonemetadata$PhoneMetadata();
                            phonemetadata$PhoneMetadata.readExternal(objectInput);
                            this.metadata_.add(phonemetadata$PhoneMetadata);
                        }
                    }
                };
                phonemetadata$PhoneMetadataCollection.readExternal(objectInputStream);
                if (!phonemetadata$PhoneMetadataCollection.getMetadataList().isEmpty()) {
                    List<Phonemetadata$PhoneMetadata> metadataList = phonemetadata$PhoneMetadataCollection.getMetadataList();
                    close(objectInputStream);
                    return metadataList;
                }
                throw new IllegalStateException("Empty metadata");
            } catch (IOException e2) {
                e = e2;
                throw new IllegalStateException("Unable to parse metadata file", e);
            }
        } catch (IOException e3) {
            e = e3;
        } catch (Throwable th3) {
            th = th3;
            if (0 != 0) {
                close(null);
            } else {
                close(inputStream);
            }
            throw th;
        }
    }

    private List<Phonemetadata$PhoneMetadata> handleNullSource() {
        if (!this.strictMode) {
            return Collections.emptyList();
        }
        throw new IllegalArgumentException("Source cannot be null");
    }

    private void close(InputStream inputStream) {
        try {
            inputStream.close();
        } catch (IOException e) {
            logger.log(Level.WARNING, "Error closing input stream (ignored)", (Throwable) e);
        }
    }
}
