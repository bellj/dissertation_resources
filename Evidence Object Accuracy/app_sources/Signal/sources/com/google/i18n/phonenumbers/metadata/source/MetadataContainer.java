package com.google.i18n.phonenumbers.metadata.source;

import com.google.i18n.phonenumbers.Phonemetadata$PhoneMetadata;

/* loaded from: classes3.dex */
public interface MetadataContainer {
    void accept(Phonemetadata$PhoneMetadata phonemetadata$PhoneMetadata);
}
