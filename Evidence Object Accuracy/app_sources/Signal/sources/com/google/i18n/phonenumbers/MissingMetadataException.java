package com.google.i18n.phonenumbers;

/* loaded from: classes3.dex */
public final class MissingMetadataException extends IllegalStateException {
    public MissingMetadataException(String str) {
        super(str);
    }
}
