package com.google.i18n.phonenumbers.metadata.source;

/* loaded from: classes3.dex */
public interface MetadataSource extends RegionMetadataSource, NonGeographicalEntityMetadataSource {
}
