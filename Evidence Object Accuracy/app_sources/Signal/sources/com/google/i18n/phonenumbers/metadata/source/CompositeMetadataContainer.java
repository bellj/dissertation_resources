package com.google.i18n.phonenumbers.metadata.source;

import com.google.i18n.phonenumbers.Phonemetadata$PhoneMetadata;
import com.google.i18n.phonenumbers.internal.GeoEntityUtility;

/* loaded from: classes3.dex */
public final class CompositeMetadataContainer implements MetadataContainer {
    private final MapBackedMetadataContainer<Integer> metadataByCountryCode = MapBackedMetadataContainer.byCountryCallingCode();
    private final MapBackedMetadataContainer<String> metadataByRegionCode = MapBackedMetadataContainer.byRegionCode();

    public Phonemetadata$PhoneMetadata getMetadataBy(String str) {
        return this.metadataByRegionCode.getMetadataBy(str);
    }

    public Phonemetadata$PhoneMetadata getMetadataBy(int i) {
        return this.metadataByCountryCode.getMetadataBy(Integer.valueOf(i));
    }

    @Override // com.google.i18n.phonenumbers.metadata.source.MetadataContainer
    public void accept(Phonemetadata$PhoneMetadata phonemetadata$PhoneMetadata) {
        if (GeoEntityUtility.isGeoEntity(this.metadataByRegionCode.getKeyProvider().getKeyOf(phonemetadata$PhoneMetadata))) {
            this.metadataByRegionCode.accept(phonemetadata$PhoneMetadata);
        } else {
            this.metadataByCountryCode.accept(phonemetadata$PhoneMetadata);
        }
    }
}
