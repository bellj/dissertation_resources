package com.google.i18n.phonenumbers.internal;

import com.google.i18n.phonenumbers.CountryCodeToRegionCodeMap;
import java.util.List;

/* loaded from: classes3.dex */
public final class GeoEntityUtility {
    public static boolean isGeoEntity(String str) {
        return !str.equals("001");
    }

    public static boolean isGeoEntity(int i) {
        List<String> list = CountryCodeToRegionCodeMap.getCountryCodeToRegionCodeMap().get(Integer.valueOf(i));
        return list != null && !list.contains("001");
    }
}
