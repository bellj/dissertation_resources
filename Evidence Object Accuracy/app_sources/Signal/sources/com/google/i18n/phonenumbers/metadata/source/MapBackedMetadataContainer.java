package com.google.i18n.phonenumbers.metadata.source;

import com.google.i18n.phonenumbers.Phonemetadata$PhoneMetadata;
import j$.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* loaded from: classes3.dex */
public final class MapBackedMetadataContainer<T> implements MetadataContainer {
    private final KeyProvider<T> keyProvider;
    private final ConcurrentMap<T, Phonemetadata$PhoneMetadata> metadataMap = new ConcurrentHashMap();

    /* loaded from: classes3.dex */
    public interface KeyProvider<T> {
        T getKeyOf(Phonemetadata$PhoneMetadata phonemetadata$PhoneMetadata);
    }

    public static MapBackedMetadataContainer<String> byRegionCode() {
        return new MapBackedMetadataContainer<>(new KeyProvider<String>() { // from class: com.google.i18n.phonenumbers.metadata.source.MapBackedMetadataContainer.1
            @Override // com.google.i18n.phonenumbers.metadata.source.MapBackedMetadataContainer.KeyProvider
            public String getKeyOf(Phonemetadata$PhoneMetadata phonemetadata$PhoneMetadata) {
                return phonemetadata$PhoneMetadata.getId();
            }
        });
    }

    public static MapBackedMetadataContainer<Integer> byCountryCallingCode() {
        return new MapBackedMetadataContainer<>(new KeyProvider<Integer>() { // from class: com.google.i18n.phonenumbers.metadata.source.MapBackedMetadataContainer.2
            @Override // com.google.i18n.phonenumbers.metadata.source.MapBackedMetadataContainer.KeyProvider
            public Integer getKeyOf(Phonemetadata$PhoneMetadata phonemetadata$PhoneMetadata) {
                return Integer.valueOf(phonemetadata$PhoneMetadata.getCountryCode());
            }
        });
    }

    private MapBackedMetadataContainer(KeyProvider<T> keyProvider) {
        this.keyProvider = keyProvider;
    }

    public Phonemetadata$PhoneMetadata getMetadataBy(T t) {
        if (t != null) {
            return this.metadataMap.get(t);
        }
        return null;
    }

    public KeyProvider<T> getKeyProvider() {
        return this.keyProvider;
    }

    @Override // com.google.i18n.phonenumbers.metadata.source.MetadataContainer
    public void accept(Phonemetadata$PhoneMetadata phonemetadata$PhoneMetadata) {
        this.metadataMap.put(this.keyProvider.getKeyOf(phonemetadata$PhoneMetadata), phonemetadata$PhoneMetadata);
    }
}
