package com.android.mms.dom;

import java.util.ArrayList;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/* loaded from: classes.dex */
public class NodeListImpl implements NodeList {
    private boolean mDeepSearch;
    private Node mRootNode;
    private ArrayList<Node> mSearchNodes;
    private ArrayList<Node> mStaticNodes;
    private String mTagName;

    public NodeListImpl(Node node, String str, boolean z) {
        this.mRootNode = node;
        this.mTagName = str;
        this.mDeepSearch = z;
    }

    @Override // org.w3c.dom.NodeList
    public int getLength() {
        ArrayList<Node> arrayList = this.mStaticNodes;
        if (arrayList != null) {
            return arrayList.size();
        }
        fillList(this.mRootNode);
        return this.mSearchNodes.size();
    }

    @Override // org.w3c.dom.NodeList
    public Node item(int i) {
        ArrayList<Node> arrayList = this.mStaticNodes;
        try {
            if (arrayList != null) {
                return arrayList.get(i);
            }
            fillList(this.mRootNode);
            return this.mSearchNodes.get(i);
        } catch (IndexOutOfBoundsException unused) {
            while (true) {
                return null;
            }
        }
    }

    private void fillList(Node node) {
        if (node == this.mRootNode) {
            this.mSearchNodes = new ArrayList<>();
        } else if (this.mTagName == null || node.getNodeName().equals(this.mTagName)) {
            this.mSearchNodes.add(node);
        }
        for (Node firstChild = node.getFirstChild(); firstChild != null; firstChild = firstChild.getNextSibling()) {
            if (this.mDeepSearch) {
                fillList(firstChild);
            } else if (this.mTagName == null || firstChild.getNodeName().equals(this.mTagName)) {
                this.mSearchNodes.add(firstChild);
            }
        }
    }
}
