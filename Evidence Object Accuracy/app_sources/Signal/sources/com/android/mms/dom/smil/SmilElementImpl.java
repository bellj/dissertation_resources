package com.android.mms.dom.smil;

import com.android.mms.dom.ElementImpl;
import org.w3c.dom.smil.SMILElement;

/* loaded from: classes.dex */
public class SmilElementImpl extends ElementImpl implements SMILElement {
    public SmilElementImpl(SmilDocumentImpl smilDocumentImpl, String str) {
        super(smilDocumentImpl, str.toLowerCase());
    }
}
