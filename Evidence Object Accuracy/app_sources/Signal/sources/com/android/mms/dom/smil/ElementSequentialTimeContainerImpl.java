package com.android.mms.dom.smil;

import org.w3c.dom.smil.ElementSequentialTimeContainer;
import org.w3c.dom.smil.SMILElement;

/* loaded from: classes.dex */
public abstract class ElementSequentialTimeContainerImpl extends ElementTimeContainerImpl implements ElementSequentialTimeContainer {
    public ElementSequentialTimeContainerImpl(SMILElement sMILElement) {
        super(sMILElement);
    }
}
