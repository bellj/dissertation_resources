package com.android.mms.dom;

import com.android.mms.dom.events.EventTargetImpl;
import java.util.NoSuchElementException;
import java.util.Vector;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.UserDataHandler;
import org.w3c.dom.events.EventTarget;

/* loaded from: classes.dex */
public abstract class NodeImpl implements Node, EventTarget {
    private final Vector<Node> mChildNodes = new Vector<>();
    private final EventTarget mEventTarget = new EventTargetImpl(this);
    DocumentImpl mOwnerDocument;
    private Node mParentNode;

    @Override // org.w3c.dom.Node
    public Node cloneNode(boolean z) {
        return null;
    }

    @Override // org.w3c.dom.Node
    public NamedNodeMap getAttributes() {
        return null;
    }

    @Override // org.w3c.dom.Node
    public String getBaseURI() {
        return null;
    }

    @Override // org.w3c.dom.Node
    public Object getFeature(String str, String str2) {
        return null;
    }

    @Override // org.w3c.dom.Node
    public String getLocalName() {
        return null;
    }

    @Override // org.w3c.dom.Node
    public String getNamespaceURI() {
        return null;
    }

    @Override // org.w3c.dom.Node
    public String getNodeValue() throws DOMException {
        return null;
    }

    @Override // org.w3c.dom.Node
    public String getPrefix() {
        return null;
    }

    @Override // org.w3c.dom.Node
    public Object getUserData(String str) {
        return null;
    }

    @Override // org.w3c.dom.Node
    public boolean hasAttributes() {
        return false;
    }

    @Override // org.w3c.dom.Node
    public Node insertBefore(Node node, Node node2) throws DOMException {
        return null;
    }

    @Override // org.w3c.dom.Node
    public boolean isSupported(String str, String str2) {
        return false;
    }

    @Override // org.w3c.dom.Node
    public String lookupNamespaceURI(String str) {
        return null;
    }

    @Override // org.w3c.dom.Node
    public String lookupPrefix(String str) {
        return null;
    }

    @Override // org.w3c.dom.Node
    public void normalize() {
    }

    @Override // org.w3c.dom.Node
    public void setNodeValue(String str) throws DOMException {
    }

    @Override // org.w3c.dom.Node
    public void setPrefix(String str) throws DOMException {
    }

    public NodeImpl(DocumentImpl documentImpl) {
        this.mOwnerDocument = documentImpl;
    }

    @Override // org.w3c.dom.Node
    public Node appendChild(Node node) throws DOMException {
        ((NodeImpl) node).setParentNode(this);
        this.mChildNodes.remove(node);
        this.mChildNodes.add(node);
        return node;
    }

    @Override // org.w3c.dom.Node
    public NodeList getChildNodes() {
        return new NodeListImpl(this, null, false);
    }

    @Override // org.w3c.dom.Node
    public Node getFirstChild() {
        try {
            return this.mChildNodes.firstElement();
        } catch (NoSuchElementException unused) {
            return null;
        }
    }

    @Override // org.w3c.dom.Node
    public Node getLastChild() {
        try {
            return this.mChildNodes.lastElement();
        } catch (NoSuchElementException unused) {
            return null;
        }
    }

    @Override // org.w3c.dom.Node
    public Node getNextSibling() {
        Node node = this.mParentNode;
        if (node == null || this == node.getLastChild()) {
            return null;
        }
        Vector<Node> vector = ((NodeImpl) this.mParentNode).mChildNodes;
        return vector.elementAt(vector.indexOf(this) + 1);
    }

    @Override // org.w3c.dom.Node
    public Document getOwnerDocument() {
        return this.mOwnerDocument;
    }

    @Override // org.w3c.dom.Node
    public Node getParentNode() {
        return this.mParentNode;
    }

    @Override // org.w3c.dom.Node
    public Node getPreviousSibling() {
        Node node = this.mParentNode;
        if (node == null || this == node.getFirstChild()) {
            return null;
        }
        Vector<Node> vector = ((NodeImpl) this.mParentNode).mChildNodes;
        return vector.elementAt(vector.indexOf(this) - 1);
    }

    @Override // org.w3c.dom.Node
    public boolean hasChildNodes() {
        return !this.mChildNodes.isEmpty();
    }

    @Override // org.w3c.dom.Node
    public Node removeChild(Node node) throws DOMException {
        if (this.mChildNodes.contains(node)) {
            this.mChildNodes.remove(node);
            ((NodeImpl) node).setParentNode(null);
            return null;
        }
        throw new DOMException(8, "Child does not exist");
    }

    @Override // org.w3c.dom.Node
    public Node replaceChild(Node node, Node node2) throws DOMException {
        if (this.mChildNodes.contains(node2)) {
            try {
                this.mChildNodes.remove(node);
            } catch (DOMException unused) {
            }
            Vector<Node> vector = this.mChildNodes;
            vector.setElementAt(node, vector.indexOf(node2));
            ((NodeImpl) node).setParentNode(this);
            ((NodeImpl) node2).setParentNode(null);
            return node2;
        }
        throw new DOMException(8, "Old child does not exist");
    }

    private void setParentNode(Node node) {
        this.mParentNode = node;
    }

    @Override // org.w3c.dom.Node
    public short compareDocumentPosition(Node node) throws DOMException {
        throw new DOMException(9, null);
    }

    @Override // org.w3c.dom.Node
    public String getTextContent() throws DOMException {
        throw new DOMException(9, null);
    }

    @Override // org.w3c.dom.Node
    public void setTextContent(String str) throws DOMException {
        throw new DOMException(9, null);
    }

    @Override // org.w3c.dom.Node
    public boolean isSameNode(Node node) {
        throw new DOMException(9, null);
    }

    @Override // org.w3c.dom.Node
    public boolean isDefaultNamespace(String str) {
        throw new DOMException(9, null);
    }

    @Override // org.w3c.dom.Node
    public boolean isEqualNode(Node node) {
        throw new DOMException(9, null);
    }

    @Override // org.w3c.dom.Node
    public Object setUserData(String str, Object obj, UserDataHandler userDataHandler) {
        throw new DOMException(9, null);
    }
}
