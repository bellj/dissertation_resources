package com.android.mms.dom.events;

import org.w3c.dom.events.EventTarget;

/* loaded from: classes.dex */
public class EventTargetImpl implements EventTarget {
    private EventTarget mNodeTarget;

    public EventTargetImpl(EventTarget eventTarget) {
        this.mNodeTarget = eventTarget;
    }
}
