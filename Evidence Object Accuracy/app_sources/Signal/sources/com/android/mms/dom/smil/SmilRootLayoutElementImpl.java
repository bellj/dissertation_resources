package com.android.mms.dom.smil;

import org.w3c.dom.smil.SMILRootLayoutElement;

/* loaded from: classes.dex */
public class SmilRootLayoutElementImpl extends SmilElementImpl implements SMILRootLayoutElement {
    public SmilRootLayoutElementImpl(SmilDocumentImpl smilDocumentImpl, String str) {
        super(smilDocumentImpl, str);
    }

    @Override // org.w3c.dom.smil.ElementLayout
    public int getHeight() {
        return parseAbsoluteLength(getAttribute("height"));
    }

    @Override // org.w3c.dom.smil.ElementLayout
    public int getWidth() {
        return parseAbsoluteLength(getAttribute("width"));
    }

    private int parseAbsoluteLength(String str) {
        if (str.endsWith("px")) {
            str = str.substring(0, str.indexOf("px"));
        }
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException unused) {
            return 0;
        }
    }
}
