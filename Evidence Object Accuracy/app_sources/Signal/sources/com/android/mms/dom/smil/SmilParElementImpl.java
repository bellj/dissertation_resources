package com.android.mms.dom.smil;

import org.w3c.dom.DOMException;
import org.w3c.dom.smil.ElementParallelTimeContainer;
import org.w3c.dom.smil.SMILParElement;

/* loaded from: classes.dex */
public class SmilParElementImpl extends SmilElementImpl implements SMILParElement {
    ElementParallelTimeContainer mParTimeContainer = new ElementParallelTimeContainerImpl(this) { // from class: com.android.mms.dom.smil.SmilParElementImpl.1
    };

    public SmilParElementImpl(SmilDocumentImpl smilDocumentImpl, String str) {
        super(smilDocumentImpl, str.toUpperCase());
    }

    @Override // org.w3c.dom.smil.ElementTime
    public void setDur(float f) throws DOMException {
        this.mParTimeContainer.setDur(f);
    }
}
