package com.android.mms.dom.smil;

import org.w3c.dom.DOMException;
import org.w3c.dom.smil.ElementTime;
import org.w3c.dom.smil.SMILMediaElement;

/* loaded from: classes.dex */
public class SmilMediaElementImpl extends SmilElementImpl implements SMILMediaElement {
    ElementTime mElementTime = new ElementTimeImpl(this) { // from class: com.android.mms.dom.smil.SmilMediaElementImpl.1
    };

    public SmilMediaElementImpl(SmilDocumentImpl smilDocumentImpl, String str) {
        super(smilDocumentImpl, str);
    }

    @Override // org.w3c.dom.smil.SMILMediaElement
    public void setSrc(String str) throws DOMException {
        setAttribute("src", str);
    }
}
