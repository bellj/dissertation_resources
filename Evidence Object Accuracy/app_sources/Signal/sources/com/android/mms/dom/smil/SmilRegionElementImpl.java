package com.android.mms.dom.smil;

import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.w3c.dom.smil.ElementLayout;
import org.w3c.dom.smil.SMILDocument;

/* loaded from: classes.dex */
public class SmilRegionElementImpl extends SmilElementImpl implements ElementLayout {
    public SmilRegionElementImpl(SmilDocumentImpl smilDocumentImpl, String str) {
        super(smilDocumentImpl, str);
    }

    public int getLeft() {
        try {
            try {
                return parseRegionLength(getAttribute("left"), true);
            } catch (NumberFormatException unused) {
                return 0;
            }
        } catch (NumberFormatException unused2) {
            return (((SMILDocument) getOwnerDocument()).getLayout().getRootLayout().getWidth() - parseRegionLength(getAttribute("right"), true)) - parseRegionLength(getAttribute("width"), true);
        }
    }

    public int getTop() {
        try {
            try {
                return parseRegionLength(getAttribute("top"), false);
            } catch (NumberFormatException unused) {
                return 0;
            }
        } catch (NumberFormatException unused2) {
            return (((SMILDocument) getOwnerDocument()).getLayout().getRootLayout().getHeight() - parseRegionLength(getAttribute("bottom"), false)) - parseRegionLength(getAttribute("height"), false);
        }
    }

    @Override // org.w3c.dom.smil.ElementLayout
    public int getHeight() {
        try {
            int parseRegionLength = parseRegionLength(getAttribute("height"), false);
            return parseRegionLength == 0 ? ((SMILDocument) getOwnerDocument()).getLayout().getRootLayout().getHeight() : parseRegionLength;
        } catch (NumberFormatException unused) {
            int height = ((SMILDocument) getOwnerDocument()).getLayout().getRootLayout().getHeight();
            try {
                height -= parseRegionLength(getAttribute("top"), false);
            } catch (NumberFormatException unused2) {
            }
            try {
                return height - parseRegionLength(getAttribute("bottom"), false);
            } catch (NumberFormatException unused3) {
                return height;
            }
        }
    }

    @Override // org.w3c.dom.smil.ElementLayout
    public int getWidth() {
        try {
            int parseRegionLength = parseRegionLength(getAttribute("width"), true);
            return parseRegionLength == 0 ? ((SMILDocument) getOwnerDocument()).getLayout().getRootLayout().getWidth() : parseRegionLength;
        } catch (NumberFormatException unused) {
            int width = ((SMILDocument) getOwnerDocument()).getLayout().getRootLayout().getWidth();
            try {
                width -= parseRegionLength(getAttribute("left"), true);
            } catch (NumberFormatException unused2) {
            }
            try {
                return width - parseRegionLength(getAttribute("right"), true);
            } catch (NumberFormatException unused3) {
                return width;
            }
        }
    }

    public String getId() {
        return getAttribute(ContactRepository.ID_COLUMN);
    }

    private int parseRegionLength(String str, boolean z) {
        double d;
        if (str.endsWith("px")) {
            return Integer.parseInt(str.substring(0, str.indexOf("px")));
        }
        if (!str.endsWith("%")) {
            return Integer.parseInt(str);
        }
        double parseInt = (double) Integer.parseInt(str.substring(0, str.length() - 1));
        Double.isNaN(parseInt);
        double d2 = parseInt * 0.01d;
        if (z) {
            d = (double) ((SMILDocument) getOwnerDocument()).getLayout().getRootLayout().getWidth();
            Double.isNaN(d);
        } else {
            d = (double) ((SMILDocument) getOwnerDocument()).getLayout().getRootLayout().getHeight();
            Double.isNaN(d);
        }
        return (int) Math.round(d2 * d);
    }

    @Override // java.lang.Object
    public String toString() {
        return super.toString() + ": id=" + getId() + ", width=" + getWidth() + ", height=" + getHeight() + ", left=" + getLeft() + ", top=" + getTop();
    }
}
