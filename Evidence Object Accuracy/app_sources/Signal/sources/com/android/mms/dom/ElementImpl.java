package com.android.mms.dom;

import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.w3c.dom.TypeInfo;

/* loaded from: classes.dex */
public class ElementImpl extends NodeImpl implements Element {
    private NamedNodeMap mAttributes = new NamedNodeMapImpl();
    private String mTagName;

    @Override // org.w3c.dom.Element
    public String getAttributeNS(String str, String str2) {
        return null;
    }

    @Override // org.w3c.dom.Element
    public Attr getAttributeNodeNS(String str, String str2) {
        return null;
    }

    @Override // org.w3c.dom.Element
    public NodeList getElementsByTagNameNS(String str, String str2) {
        return null;
    }

    @Override // org.w3c.dom.Node
    public short getNodeType() {
        return 1;
    }

    @Override // org.w3c.dom.Element
    public TypeInfo getSchemaTypeInfo() {
        return null;
    }

    @Override // org.w3c.dom.Element
    public boolean hasAttributeNS(String str, String str2) {
        return false;
    }

    @Override // org.w3c.dom.Element
    public void removeAttribute(String str) throws DOMException {
    }

    @Override // org.w3c.dom.Element
    public void removeAttributeNS(String str, String str2) throws DOMException {
    }

    @Override // org.w3c.dom.Element
    public Attr removeAttributeNode(Attr attr) throws DOMException {
        return null;
    }

    @Override // org.w3c.dom.Element
    public void setAttributeNS(String str, String str2, String str3) throws DOMException {
    }

    @Override // org.w3c.dom.Element
    public Attr setAttributeNode(Attr attr) throws DOMException {
        return null;
    }

    @Override // org.w3c.dom.Element
    public Attr setAttributeNodeNS(Attr attr) throws DOMException {
        return null;
    }

    public ElementImpl(DocumentImpl documentImpl, String str) {
        super(documentImpl);
        this.mTagName = str;
    }

    @Override // org.w3c.dom.Element
    public String getAttribute(String str) {
        Attr attributeNode = getAttributeNode(str);
        return attributeNode != null ? attributeNode.getValue() : "";
    }

    @Override // org.w3c.dom.Element
    public Attr getAttributeNode(String str) {
        return (Attr) this.mAttributes.getNamedItem(str);
    }

    @Override // org.w3c.dom.Element
    public NodeList getElementsByTagName(String str) {
        return new NodeListImpl(this, str, true);
    }

    @Override // org.w3c.dom.Element
    public String getTagName() {
        return this.mTagName;
    }

    @Override // org.w3c.dom.Element
    public boolean hasAttribute(String str) {
        return getAttributeNode(str) != null;
    }

    @Override // org.w3c.dom.Element
    public void setAttribute(String str, String str2) throws DOMException {
        Attr attributeNode = getAttributeNode(str);
        if (attributeNode == null) {
            attributeNode = this.mOwnerDocument.createAttribute(str);
        }
        attributeNode.setNodeValue(str2);
        this.mAttributes.setNamedItem(attributeNode);
    }

    @Override // org.w3c.dom.Node
    public String getNodeName() {
        return this.mTagName;
    }

    @Override // com.android.mms.dom.NodeImpl, org.w3c.dom.Node
    public NamedNodeMap getAttributes() {
        return this.mAttributes;
    }

    @Override // com.android.mms.dom.NodeImpl, org.w3c.dom.Node
    public boolean hasAttributes() {
        return this.mAttributes.getLength() > 0;
    }

    @Override // org.w3c.dom.Element
    public void setIdAttribute(String str, boolean z) throws DOMException {
        throw new DOMException(9, null);
    }

    @Override // org.w3c.dom.Element
    public void setIdAttributeNS(String str, String str2, boolean z) throws DOMException {
        throw new DOMException(9, null);
    }

    @Override // org.w3c.dom.Element
    public void setIdAttributeNode(Attr attr, boolean z) throws DOMException {
        throw new DOMException(9, null);
    }
}
