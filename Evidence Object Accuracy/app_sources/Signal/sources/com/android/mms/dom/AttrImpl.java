package com.android.mms.dom;

import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.TypeInfo;

/* loaded from: classes.dex */
public class AttrImpl extends NodeImpl implements Attr {
    private String mName;
    private String mValue;

    @Override // com.android.mms.dom.NodeImpl, org.w3c.dom.Node
    public Node getNextSibling() {
        return null;
    }

    @Override // org.w3c.dom.Node
    public short getNodeType() {
        return 2;
    }

    @Override // org.w3c.dom.Attr
    public Element getOwnerElement() {
        return null;
    }

    @Override // com.android.mms.dom.NodeImpl, org.w3c.dom.Node
    public Node getParentNode() {
        return null;
    }

    @Override // com.android.mms.dom.NodeImpl, org.w3c.dom.Node
    public Node getPreviousSibling() {
        return null;
    }

    @Override // org.w3c.dom.Attr
    public TypeInfo getSchemaTypeInfo() {
        return null;
    }

    @Override // org.w3c.dom.Attr
    public boolean isId() {
        return false;
    }

    public AttrImpl(DocumentImpl documentImpl, String str) {
        super(documentImpl);
        this.mName = str;
    }

    @Override // org.w3c.dom.Attr
    public String getName() {
        return this.mName;
    }

    @Override // org.w3c.dom.Attr
    public boolean getSpecified() {
        return this.mValue != null;
    }

    @Override // org.w3c.dom.Attr
    public String getValue() {
        return this.mValue;
    }

    @Override // org.w3c.dom.Attr
    public void setValue(String str) throws DOMException {
        this.mValue = str;
    }

    @Override // org.w3c.dom.Node
    public String getNodeName() {
        return this.mName;
    }

    @Override // com.android.mms.dom.NodeImpl, org.w3c.dom.Node
    public void setNodeValue(String str) throws DOMException {
        setValue(str);
    }
}
