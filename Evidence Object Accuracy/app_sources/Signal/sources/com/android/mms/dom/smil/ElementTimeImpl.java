package com.android.mms.dom.smil;

import org.w3c.dom.DOMException;
import org.w3c.dom.smil.ElementTime;
import org.w3c.dom.smil.SMILElement;

/* loaded from: classes.dex */
public abstract class ElementTimeImpl implements ElementTime {
    final SMILElement mSmilElement;

    public ElementTimeImpl(SMILElement sMILElement) {
        this.mSmilElement = sMILElement;
    }

    @Override // org.w3c.dom.smil.ElementTime
    public void setDur(float f) throws DOMException {
        SMILElement sMILElement = this.mSmilElement;
        sMILElement.setAttribute("dur", Integer.toString((int) (f * 1000.0f)) + "ms");
    }
}
