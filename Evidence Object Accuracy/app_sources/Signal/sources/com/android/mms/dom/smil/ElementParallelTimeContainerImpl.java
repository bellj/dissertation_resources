package com.android.mms.dom.smil;

import org.w3c.dom.smil.ElementParallelTimeContainer;
import org.w3c.dom.smil.SMILElement;

/* loaded from: classes.dex */
public abstract class ElementParallelTimeContainerImpl extends ElementTimeContainerImpl implements ElementParallelTimeContainer {
    public ElementParallelTimeContainerImpl(SMILElement sMILElement) {
        super(sMILElement);
    }
}
