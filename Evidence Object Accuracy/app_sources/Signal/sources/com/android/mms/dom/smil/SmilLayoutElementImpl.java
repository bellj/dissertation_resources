package com.android.mms.dom.smil;

import com.android.mms.layout.LayoutManager;
import org.w3c.dom.NodeList;
import org.w3c.dom.smil.SMILLayoutElement;
import org.w3c.dom.smil.SMILRootLayoutElement;

/* loaded from: classes.dex */
public class SmilLayoutElementImpl extends SmilElementImpl implements SMILLayoutElement {
    public SmilLayoutElementImpl(SmilDocumentImpl smilDocumentImpl, String str) {
        super(smilDocumentImpl, str);
    }

    @Override // org.w3c.dom.smil.SMILLayoutElement
    public SMILRootLayoutElement getRootLayout() {
        NodeList childNodes = getChildNodes();
        int length = childNodes.getLength();
        SMILRootLayoutElement sMILRootLayoutElement = null;
        for (int i = 0; i < length; i++) {
            if (childNodes.item(i).getNodeName().equals("root-layout")) {
                sMILRootLayoutElement = (SMILRootLayoutElement) childNodes.item(i);
            }
        }
        if (sMILRootLayoutElement != null) {
            return sMILRootLayoutElement;
        }
        SMILRootLayoutElement sMILRootLayoutElement2 = (SMILRootLayoutElement) getOwnerDocument().createElement("root-layout");
        LayoutManager.getInstance();
        throw null;
    }
}
