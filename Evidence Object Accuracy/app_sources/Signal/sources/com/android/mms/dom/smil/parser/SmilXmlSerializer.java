package com.android.mms.dom.smil.parser;

import androidx.recyclerview.widget.RecyclerView;
import com.klinker.android.logger.Log;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.smil.SMILDocument;
import org.w3c.dom.smil.SMILElement;

/* loaded from: classes.dex */
public class SmilXmlSerializer {
    public static void serialize(SMILDocument sMILDocument, OutputStream outputStream) {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"), RecyclerView.ItemAnimator.FLAG_MOVED);
            writeElement(bufferedWriter, sMILDocument.getDocumentElement());
            bufferedWriter.flush();
        } catch (UnsupportedEncodingException e) {
            Log.e("SmilXmlSerializer", "exception thrown", e);
        } catch (IOException e2) {
            Log.e("SmilXmlSerializer", "exception thrown", e2);
        }
    }

    private static void writeElement(Writer writer, Element element) throws IOException {
        writer.write(60);
        writer.write(element.getTagName());
        if (element.hasAttributes()) {
            NamedNodeMap attributes = element.getAttributes();
            for (int i = 0; i < attributes.getLength(); i++) {
                Attr attr = (Attr) attributes.item(i);
                writer.write(" " + attr.getName());
                writer.write("=\"" + attr.getValue() + "\"");
            }
        }
        SMILElement sMILElement = (SMILElement) element.getFirstChild();
        if (sMILElement != null) {
            writer.write(62);
            do {
                writeElement(writer, sMILElement);
                sMILElement = (SMILElement) sMILElement.getNextSibling();
            } while (sMILElement != null);
            writer.write("</");
            writer.write(element.getTagName());
            writer.write(62);
            return;
        }
        writer.write("/>");
    }
}
