package com.android.mms.dom;

import java.util.Vector;
import org.w3c.dom.DOMException;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/* loaded from: classes.dex */
public class NamedNodeMapImpl implements NamedNodeMap {
    private Vector<Node> mNodes = new Vector<>();

    @Override // org.w3c.dom.NamedNodeMap
    public Node getNamedItemNS(String str, String str2) {
        return null;
    }

    @Override // org.w3c.dom.NamedNodeMap
    public Node removeNamedItemNS(String str, String str2) throws DOMException {
        return null;
    }

    @Override // org.w3c.dom.NamedNodeMap
    public Node setNamedItemNS(Node node) throws DOMException {
        return null;
    }

    @Override // org.w3c.dom.NamedNodeMap
    public int getLength() {
        return this.mNodes.size();
    }

    @Override // org.w3c.dom.NamedNodeMap
    public Node getNamedItem(String str) {
        for (int i = 0; i < this.mNodes.size(); i++) {
            if (str.equals(this.mNodes.elementAt(i).getNodeName())) {
                return this.mNodes.elementAt(i);
            }
        }
        return null;
    }

    @Override // org.w3c.dom.NamedNodeMap
    public Node item(int i) {
        if (i < this.mNodes.size()) {
            return this.mNodes.elementAt(i);
        }
        return null;
    }

    @Override // org.w3c.dom.NamedNodeMap
    public Node removeNamedItem(String str) throws DOMException {
        Node namedItem = getNamedItem(str);
        if (namedItem != null) {
            this.mNodes.remove(namedItem);
            return namedItem;
        }
        throw new DOMException(8, "Not found");
    }

    @Override // org.w3c.dom.NamedNodeMap
    public Node setNamedItem(Node node) throws DOMException {
        Node namedItem = getNamedItem(node.getNodeName());
        if (namedItem != null) {
            this.mNodes.remove(namedItem);
        }
        this.mNodes.add(node);
        return namedItem;
    }
}
