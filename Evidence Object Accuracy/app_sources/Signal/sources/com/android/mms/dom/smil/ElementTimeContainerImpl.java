package com.android.mms.dom.smil;

import org.w3c.dom.smil.SMILElement;

/* loaded from: classes.dex */
public abstract class ElementTimeContainerImpl extends ElementTimeImpl {
    public ElementTimeContainerImpl(SMILElement sMILElement) {
        super(sMILElement);
    }
}
