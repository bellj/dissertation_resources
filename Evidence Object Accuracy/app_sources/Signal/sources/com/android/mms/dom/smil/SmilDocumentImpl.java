package com.android.mms.dom.smil;

import com.android.mms.dom.DocumentImpl;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.smil.ElementSequentialTimeContainer;
import org.w3c.dom.smil.SMILDocument;
import org.w3c.dom.smil.SMILElement;
import org.w3c.dom.smil.SMILLayoutElement;

/* loaded from: classes.dex */
public class SmilDocumentImpl extends DocumentImpl implements SMILDocument {
    ElementSequentialTimeContainer mSeqTimeContainer;

    @Override // org.w3c.dom.Document
    public Element createElement(String str) throws DOMException {
        String lowerCase = str.toLowerCase();
        if (lowerCase.equals(DraftDatabase.Draft.TEXT) || lowerCase.equals("img") || lowerCase.equals("video")) {
            return new SmilRegionMediaElementImpl(this, lowerCase);
        }
        if (lowerCase.equals("audio")) {
            return new SmilMediaElementImpl(this, lowerCase);
        }
        if (lowerCase.equals("layout")) {
            return new SmilLayoutElementImpl(this, lowerCase);
        }
        if (lowerCase.equals("root-layout")) {
            return new SmilRootLayoutElementImpl(this, lowerCase);
        }
        if (lowerCase.equals("region")) {
            return new SmilRegionElementImpl(this, lowerCase);
        }
        if (lowerCase.equals("ref")) {
            return new SmilRefElementImpl(this, lowerCase);
        }
        if (lowerCase.equals("par")) {
            return new SmilParElementImpl(this, lowerCase);
        }
        if (lowerCase.equals("vcard")) {
            return new SmilRegionMediaElementImpl(this, lowerCase);
        }
        return new SmilElementImpl(this, lowerCase);
    }

    @Override // org.w3c.dom.Document
    public SMILElement getDocumentElement() {
        Node firstChild = getFirstChild();
        if (firstChild == null || !(firstChild instanceof SMILElement)) {
            firstChild = createElement("smil");
            appendChild(firstChild);
        }
        return (SMILElement) firstChild;
    }

    public SMILElement getHead() {
        SMILElement documentElement = getDocumentElement();
        Node firstChild = documentElement.getFirstChild();
        if (firstChild == null || !(firstChild instanceof SMILElement)) {
            firstChild = createElement("head");
            documentElement.appendChild(firstChild);
        }
        return (SMILElement) firstChild;
    }

    @Override // org.w3c.dom.smil.SMILDocument
    public SMILElement getBody() {
        SMILElement documentElement = getDocumentElement();
        Node nextSibling = getHead().getNextSibling();
        if (nextSibling == null || !(nextSibling instanceof SMILElement)) {
            nextSibling = createElement("body");
            documentElement.appendChild(nextSibling);
        }
        SMILElement sMILElement = (SMILElement) nextSibling;
        this.mSeqTimeContainer = new ElementSequentialTimeContainerImpl(sMILElement) { // from class: com.android.mms.dom.smil.SmilDocumentImpl.1
        };
        return sMILElement;
    }

    @Override // org.w3c.dom.smil.SMILDocument
    public SMILLayoutElement getLayout() {
        SMILElement head = getHead();
        Node firstChild = head.getFirstChild();
        while (firstChild != null && !(firstChild instanceof SMILLayoutElement)) {
            firstChild = firstChild.getNextSibling();
        }
        if (firstChild == null) {
            firstChild = new SmilLayoutElementImpl(this, "layout");
            head.appendChild(firstChild);
        }
        return (SMILLayoutElement) firstChild;
    }
}
