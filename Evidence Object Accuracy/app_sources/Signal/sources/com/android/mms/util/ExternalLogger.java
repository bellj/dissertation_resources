package com.android.mms.util;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/* loaded from: classes.dex */
public class ExternalLogger {
    private static final CopyOnWriteArrayList<LoggingListener> sListener = new CopyOnWriteArrayList<>();

    /* loaded from: classes.dex */
    public interface LoggingListener {
        void onLogMessage(String str, String str2);
    }

    public static void logMessage(String str, String str2) {
        Iterator<LoggingListener> it = sListener.iterator();
        while (it.hasNext()) {
            it.next().onLogMessage(str, str2);
        }
    }
}
