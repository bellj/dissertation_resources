package com.android.mms.service_alt;

import android.content.ContentValues;
import com.klinker.android.logger.Log;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

/* loaded from: classes.dex */
public class MmsConfigXmlProcessor {
    private final XmlPullParser mInputParser;
    private final StringBuilder mLogStringBuilder = new StringBuilder();
    private MmsConfigHandler mMmsConfigHandler;

    /* loaded from: classes.dex */
    public interface MmsConfigHandler {
        void process(String str, String str2, String str3);
    }

    private MmsConfigXmlProcessor(XmlPullParser xmlPullParser) {
        this.mInputParser = xmlPullParser;
        this.mMmsConfigHandler = null;
    }

    public static MmsConfigXmlProcessor get(XmlPullParser xmlPullParser) {
        return new MmsConfigXmlProcessor(xmlPullParser);
    }

    public MmsConfigXmlProcessor setMmsConfigHandler(MmsConfigHandler mmsConfigHandler) {
        this.mMmsConfigHandler = mmsConfigHandler;
        return this;
    }

    private int advanceToNextEvent(int i) throws XmlPullParserException, IOException {
        int next;
        do {
            next = this.mInputParser.next();
            if (next == i) {
                break;
            }
        } while (next != 1);
        return next;
    }

    public void process() {
        try {
            if (advanceToNextEvent(2) == 2) {
                new ContentValues();
                if ("mms_config".equals(this.mInputParser.getName())) {
                    processMmsConfig();
                    return;
                }
                return;
            }
            throw new XmlPullParserException("MmsConfigXmlProcessor: expecting start tag @" + xmlParserDebugContext());
        } catch (IOException e) {
            Log.e("MmsConfigXmlProcessor", "MmsConfigXmlProcessor: I/O failure " + e, e);
        } catch (XmlPullParserException e2) {
            Log.e("MmsConfigXmlProcessor", "MmsConfigXmlProcessor: parsing failure " + e2, e2);
        }
    }

    private static String xmlParserEventString(int i) {
        if (i == 0) {
            return "START_DOCUMENT";
        }
        if (i == 1) {
            return "END_DOCUMENT";
        }
        if (i == 2) {
            return "START_TAG";
        }
        if (i != 3) {
            return i != 4 ? Integer.toString(i) : "TEXT";
        }
        return "END_TAG";
    }

    private String xmlParserDebugContext() {
        this.mLogStringBuilder.setLength(0);
        XmlPullParser xmlPullParser = this.mInputParser;
        if (xmlPullParser == null) {
            return "Unknown";
        }
        try {
            int eventType = xmlPullParser.getEventType();
            this.mLogStringBuilder.append(xmlParserEventString(eventType));
            if (eventType == 2 || eventType == 3 || eventType == 4) {
                StringBuilder sb = this.mLogStringBuilder;
                sb.append('<');
                sb.append(this.mInputParser.getName());
                for (int i = 0; i < this.mInputParser.getAttributeCount(); i++) {
                    StringBuilder sb2 = this.mLogStringBuilder;
                    sb2.append(' ');
                    sb2.append(this.mInputParser.getAttributeName(i));
                    sb2.append('=');
                    sb2.append(this.mInputParser.getAttributeValue(i));
                }
                this.mLogStringBuilder.append("/>");
            }
            return this.mLogStringBuilder.toString();
        } catch (XmlPullParserException e) {
            Log.e("MmsConfigXmlProcessor", "xmlParserDebugContext: " + e, e);
            return "Unknown";
        }
    }

    private void processMmsConfig() throws IOException, XmlPullParserException {
        int next;
        while (true) {
            next = this.mInputParser.next();
            if (next != 4) {
                if (next != 2) {
                    break;
                }
                processMmsConfigKeyValue();
            }
        }
        if (next != 3) {
            throw new XmlPullParserException("MmsConfig: expecting start or end tag @" + xmlParserDebugContext());
        }
    }

    private void processMmsConfigKeyValue() throws IOException, XmlPullParserException {
        String str = null;
        String attributeValue = this.mInputParser.getAttributeValue(null, "name");
        String name = this.mInputParser.getName();
        int next = this.mInputParser.next();
        if (next == 4) {
            str = this.mInputParser.getText();
            next = this.mInputParser.next();
        }
        if (next != 3) {
            throw new XmlPullParserException("MmsConfigXmlProcessor: expecting end tag @" + xmlParserDebugContext());
        } else if (MmsConfig.isValidKey(attributeValue, name)) {
            MmsConfigHandler mmsConfigHandler = this.mMmsConfigHandler;
            if (mmsConfigHandler != null) {
                mmsConfigHandler.process(attributeValue, str, name);
            }
        } else {
            Log.w("MmsConfigXmlProcessor", "MmsConfig: invalid key=" + attributeValue + " or type=" + name);
        }
    }
}
