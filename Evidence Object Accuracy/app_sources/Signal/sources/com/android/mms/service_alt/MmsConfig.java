package com.android.mms.service_alt;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.android.mms.service_alt.MmsConfigXmlProcessor;
import com.klinker.android.logger.Log;
import com.klinker.android.send_message.R$xml;
import j$.util.concurrent.ConcurrentHashMap;
import java.util.Map;
import org.thoughtcrime.securesms.database.RecipientDatabase;

/* loaded from: classes.dex */
public class MmsConfig {
    private static final Map<String, Object> DEFAULTS;
    private final Map<String, Object> mKeyValues;
    private final int mSubId;
    private String mUaProfUrl = null;
    private String mUserAgent = null;

    static {
        ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap();
        DEFAULTS = concurrentHashMap;
        Boolean bool = Boolean.TRUE;
        concurrentHashMap.put("enabledMMS", bool);
        Boolean bool2 = Boolean.FALSE;
        concurrentHashMap.put("enabledTransID", bool2);
        concurrentHashMap.put("enabledNotifyWapMMSC", bool2);
        concurrentHashMap.put("aliasEnabled", bool2);
        concurrentHashMap.put("allowAttachAudio", bool);
        concurrentHashMap.put("enableMultipartSMS", bool);
        concurrentHashMap.put("enableSMSDeliveryReports", bool);
        concurrentHashMap.put("enableGroupMms", bool);
        concurrentHashMap.put("supportMmsContentDisposition", bool);
        concurrentHashMap.put("config_cellBroadcastAppLinks", bool);
        concurrentHashMap.put("sendMultipartSmsAsSeparateMessages", bool2);
        concurrentHashMap.put("enableMMSReadReports", bool2);
        concurrentHashMap.put("enableMMSDeliveryReports", bool2);
        concurrentHashMap.put("supportHttpCharsetHeader", bool2);
        concurrentHashMap.put("maxMessageSize", 307200);
        concurrentHashMap.put("maxImageHeight", 480);
        concurrentHashMap.put("maxImageWidth", 640);
        concurrentHashMap.put("recipientLimit", Integer.MAX_VALUE);
        concurrentHashMap.put("httpSocketTimeout", 60000);
        concurrentHashMap.put("aliasMinChars", 2);
        concurrentHashMap.put("aliasMaxChars", 48);
        concurrentHashMap.put("smsToMmsTextThreshold", -1);
        concurrentHashMap.put("smsToMmsTextLengthThreshold", -1);
        concurrentHashMap.put("maxMessageTextSize", -1);
        concurrentHashMap.put("maxSubjectLength", 40);
        concurrentHashMap.put("uaProfTagName", "x-wap-profile");
        concurrentHashMap.put("userAgent", "");
        concurrentHashMap.put("uaProfUrl", "");
        concurrentHashMap.put("httpParams", "");
        concurrentHashMap.put("emailGatewayNumber", "");
        concurrentHashMap.put("naiSuffix", "");
    }

    public MmsConfig(Context context, int i) {
        ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap();
        this.mKeyValues = concurrentHashMap;
        this.mSubId = i;
        concurrentHashMap.clear();
        concurrentHashMap.putAll(DEFAULTS);
        loadDeviceUaSettings(context);
        Log.v("MmsConfig", "MmsConfig: mUserAgent=" + this.mUserAgent + ", mUaProfUrl=" + this.mUaProfUrl);
        loadFromResources(context);
        StringBuilder sb = new StringBuilder();
        sb.append("MmsConfig: all settings -- ");
        sb.append(concurrentHashMap);
        Log.v("MmsConfig", sb.toString());
    }

    public static boolean isValidKey(String str, String str2) {
        Class<?> cls;
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        Map<String, Object> map = DEFAULTS;
        if (!map.containsKey(str)) {
            return false;
        }
        Object obj = map.get(str);
        if (obj != null) {
            cls = obj.getClass();
        } else {
            cls = String.class;
        }
        if ("int".equals(str2)) {
            if (cls == Integer.class) {
                return true;
            }
            return false;
        } else if ("bool".equals(str2)) {
            if (cls == Boolean.class) {
                return true;
            }
            return false;
        } else if (!"string".equals(str2) || cls != String.class) {
            return false;
        } else {
            return true;
        }
    }

    public String getNullableStringValue(String str) {
        Object obj = this.mKeyValues.get(str);
        if (obj != null) {
            return (String) obj;
        }
        return null;
    }

    public void update(String str, String str2, String str3) {
        try {
            if ("int".equals(str3)) {
                this.mKeyValues.put(str, Integer.valueOf(Integer.parseInt(str2)));
            } else if ("bool".equals(str3)) {
                this.mKeyValues.put(str, Boolean.valueOf(Boolean.parseBoolean(str2)));
            } else if ("string".equals(str3)) {
                this.mKeyValues.put(str, str2);
            }
        } catch (NumberFormatException unused) {
            while (true) {
                str = "MmsConfig.update: invalid " + str + "," + str2 + "," + str3;
                str2 = "MmsConfig";
                Log.e(str2, str);
                return;
            }
        }
    }

    private void loadDeviceUaSettings(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(RecipientDatabase.PHONE);
        this.mUserAgent = telephonyManager.getMmsUserAgent();
        this.mUaProfUrl = telephonyManager.getMmsUAProfUrl();
    }

    private void loadFromResources(Context context) {
        Log.d("MmsConfig", "MmsConfig.loadFromResources");
        XmlResourceParser xml = context.getResources().getXml(R$xml.mms_config);
        MmsConfigXmlProcessor mmsConfigXmlProcessor = MmsConfigXmlProcessor.get(xml);
        mmsConfigXmlProcessor.setMmsConfigHandler(new MmsConfigXmlProcessor.MmsConfigHandler() { // from class: com.android.mms.service_alt.MmsConfig.1
            @Override // com.android.mms.service_alt.MmsConfigXmlProcessor.MmsConfigHandler
            public void process(String str, String str2, String str3) {
                MmsConfig.this.update(str, str2, str3);
            }
        });
        try {
            mmsConfigXmlProcessor.process();
        } finally {
            xml.close();
        }
    }

    /* loaded from: classes.dex */
    public static class Overridden {
        private final MmsConfig mBase;
        private final Bundle mOverrides;

        public Overridden(MmsConfig mmsConfig, Bundle bundle) {
            this.mBase = mmsConfig;
            this.mOverrides = bundle;
        }

        private int getInt(String str) {
            Integer num = (Integer) this.mBase.mKeyValues.get(str);
            Bundle bundle = this.mOverrides;
            return bundle != null ? bundle.getInt(str, num.intValue()) : num.intValue();
        }

        private String getString(String str) {
            Bundle bundle = this.mOverrides;
            if (bundle == null || !bundle.containsKey(str)) {
                return this.mBase.getNullableStringValue(str);
            }
            return this.mOverrides.getString(str);
        }

        public int getMaxMessageSize() {
            return getInt("maxMessageSize");
        }

        public String getHttpParams() {
            return getString("httpParams");
        }

        public int getMaxImageHeight() {
            return getInt("maxImageHeight");
        }

        public int getMaxImageWidth() {
            return getInt("maxImageWidth");
        }
    }
}
