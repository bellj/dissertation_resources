package com.codewaves.stickyheadergrid;

import android.content.Context;
import android.graphics.PointF;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/* loaded from: classes.dex */
public class StickyHeaderGridLayoutManager extends RecyclerView.LayoutManager implements RecyclerView.SmoothScroller.ScrollVectorProvider {
    private StickyHeaderGridAdapter mAdapter;
    private AnchorPosition mAnchor = new AnchorPosition();
    private int mAverageHeaderHeight;
    private final FillResult mFillResult = new FillResult();
    private View[] mFillViewSet;
    private int mFloatingHeaderPosition;
    private View mFloatingHeaderView;
    private int mHeaderOverlapMargin;
    private HeaderStateChangeListener mHeaderStateListener;
    private int mHeadersStartPosition;
    private ArrayList<LayoutRow> mLayoutRows = new ArrayList<>(16);
    private SavedState mPendingSavedState;
    private int mPendingScrollPosition = -1;
    private int mPendingScrollPositionOffset;
    private int mSpanCount;
    private SpanSizeLookup mSpanSizeLookup = new DefaultSpanSizeLookup();
    private int mStickOffset;
    private HeaderState mStickyHeadeState;
    private int mStickyHeaderSection = -1;
    private View mStickyHeaderView;

    /* loaded from: classes.dex */
    public enum HeaderState {
        NORMAL,
        STICKY,
        PUSHED
    }

    /* loaded from: classes.dex */
    public interface HeaderStateChangeListener {
        void onHeaderStateChanged(int i, View view, HeaderState headerState, int i2);
    }

    /* loaded from: classes.dex */
    public static abstract class SpanSizeLookup {
        public abstract int getSpanIndex(int i, int i2, int i3);

        public abstract int getSpanSize(int i, int i2);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public boolean canScrollVertically() {
        return true;
    }

    public StickyHeaderGridLayoutManager(int i) {
        this.mSpanCount = i;
        this.mFillViewSet = new View[i];
        this.mHeaderOverlapMargin = 0;
        if (i < 1) {
            throw new IllegalArgumentException("Span count should be at least 1. Provided " + i);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void onAdapterChanged(RecyclerView.Adapter adapter, RecyclerView.Adapter adapter2) {
        super.onAdapterChanged(adapter, adapter2);
        try {
            this.mAdapter = (StickyHeaderGridAdapter) adapter2;
            removeAllViews();
            clearState();
        } catch (ClassCastException unused) {
            throw new ClassCastException("Adapter used with StickyHeaderGridLayoutManager must be kind of StickyHeaderGridAdapter");
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void onAttachedToWindow(RecyclerView recyclerView) {
        super.onAttachedToWindow(recyclerView);
        try {
            this.mAdapter = (StickyHeaderGridAdapter) recyclerView.getAdapter();
        } catch (ClassCastException unused) {
            throw new ClassCastException("Adapter used with StickyHeaderGridLayoutManager must be kind of StickyHeaderGridAdapter");
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-1, -2);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public RecyclerView.LayoutParams generateLayoutParams(Context context, AttributeSet attributeSet) {
        return new LayoutParams(context, attributeSet);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public RecyclerView.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new LayoutParams(layoutParams);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public Parcelable onSaveInstanceState() {
        if (this.mPendingSavedState != null) {
            return new SavedState(this.mPendingSavedState);
        }
        SavedState savedState = new SavedState();
        if (getChildCount() > 0) {
            savedState.mAnchorSection = this.mAnchor.section;
            savedState.mAnchorItem = this.mAnchor.item;
            savedState.mAnchorOffset = this.mAnchor.offset;
        } else {
            savedState.invalidateAnchor();
        }
        return savedState;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof SavedState) {
            this.mPendingSavedState = (SavedState) parcelable;
            requestLayout();
            return;
        }
        Log.d("StickyLayoutManager", "invalid saved state class");
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public boolean checkLayoutParams(RecyclerView.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void scrollToPosition(int i) {
        if (i < 0 || i > getItemCount()) {
            throw new IndexOutOfBoundsException("adapter position out of range");
        }
        this.mPendingScrollPosition = i;
        this.mPendingScrollPositionOffset = 0;
        SavedState savedState = this.mPendingSavedState;
        if (savedState != null) {
            savedState.invalidateAnchor();
        }
        requestLayout();
    }

    private int getExtraLayoutSpace(RecyclerView.State state) {
        if (state.hasTargetScrollPosition()) {
            return getHeight();
        }
        return 0;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int i) {
        AnonymousClass1 r2 = new LinearSmoothScroller(recyclerView.getContext()) { // from class: com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager.1
            @Override // androidx.recyclerview.widget.LinearSmoothScroller
            public int calculateDyToMakeVisible(View view, int i2) {
                RecyclerView.LayoutManager layoutManager = getLayoutManager();
                if (layoutManager == null || !layoutManager.canScrollVertically()) {
                    return 0;
                }
                return calculateDtToFit(layoutManager.getDecoratedTop(view), layoutManager.getDecoratedBottom(view), layoutManager.getPaddingTop() + StickyHeaderGridLayoutManager.this.getPositionSectionHeaderHeight(StickyHeaderGridLayoutManager.this.getPosition(view)), layoutManager.getHeight() - layoutManager.getPaddingBottom(), i2);
            }
        };
        r2.setTargetPosition(i);
        startSmoothScroll(r2);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.SmoothScroller.ScrollVectorProvider
    public PointF computeScrollVectorForPosition(int i) {
        LayoutRow firstVisibleRow;
        if (getChildCount() == 0 || (firstVisibleRow = getFirstVisibleRow()) == null) {
            return null;
        }
        return new PointF(0.0f, (float) (i - firstVisibleRow.adapterPosition));
    }

    private int getAdapterPositionFromAnchor(AnchorPosition anchorPosition) {
        if (anchorPosition.section < 0 || anchorPosition.section >= this.mAdapter.getSectionCount()) {
            anchorPosition.reset();
            return -1;
        } else if (anchorPosition.item >= 0 && anchorPosition.item < this.mAdapter.getSectionItemCount(anchorPosition.section)) {
            return this.mAdapter.getSectionItemPosition(anchorPosition.section, anchorPosition.item);
        } else {
            anchorPosition.offset = 0;
            return this.mAdapter.getSectionHeaderPosition(anchorPosition.section);
        }
    }

    private int getAdapterPositionChecked(int i, int i2) {
        if (i < 0 || i >= this.mAdapter.getSectionCount()) {
            return -1;
        }
        if (i2 < 0 || i2 >= this.mAdapter.getSectionItemCount(i)) {
            return this.mAdapter.getSectionHeaderPosition(i);
        }
        return this.mAdapter.getSectionItemPosition(i, i2);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        int i;
        if (this.mAdapter == null || state.getItemCount() == 0) {
            removeAndRecycleAllViews(recycler);
            clearState();
            return;
        }
        int i2 = this.mPendingScrollPosition;
        if (i2 >= 0) {
            i = this.mPendingScrollPositionOffset;
        } else {
            SavedState savedState = this.mPendingSavedState;
            if (savedState == null || !savedState.hasValidAnchor()) {
                i2 = getAdapterPositionFromAnchor(this.mAnchor);
                i = this.mAnchor.offset;
            } else {
                i2 = getAdapterPositionChecked(this.mPendingSavedState.mAnchorSection, this.mPendingSavedState.mAnchorItem);
                i = this.mPendingSavedState.mAnchorOffset;
                this.mPendingSavedState = null;
            }
        }
        if (i2 < 0 || i2 >= state.getItemCount()) {
            this.mPendingScrollPosition = -1;
            i2 = 0;
            i = 0;
        }
        if (i > 0) {
            i = 0;
        }
        detachAndScrapAttachedViews(recycler);
        clearState();
        int findFirstRowItem = findFirstRowItem(i2);
        int paddingLeft = getPaddingLeft();
        int width = getWidth() - getPaddingRight();
        int height = getHeight() - getPaddingBottom();
        int paddingTop = getPaddingTop() + i;
        int i3 = findFirstRowItem;
        while (i3 < state.getItemCount()) {
            if (this.mAdapter.getItemViewInternalType(i3) == 0) {
                View viewForPosition = recycler.getViewForPosition(i3);
                addView(viewForPosition);
                measureChildWithMargins(viewForPosition, 0, 0);
                int decoratedMeasuredHeight = getDecoratedMeasuredHeight(viewForPosition);
                int i4 = this.mHeaderOverlapMargin;
                int i5 = decoratedMeasuredHeight >= i4 ? i4 : decoratedMeasuredHeight;
                int i6 = paddingTop + decoratedMeasuredHeight;
                layoutDecorated(viewForPosition, paddingLeft, paddingTop, width, i6);
                int i7 = i6 - i5;
                this.mLayoutRows.add(new LayoutRow(viewForPosition, i3, 1, paddingTop, i7));
                i3++;
                this.mAverageHeaderHeight = decoratedMeasuredHeight - i5;
                paddingTop = i7;
            } else {
                FillResult fillBottomRow = fillBottomRow(recycler, state, i3, paddingTop);
                paddingTop += fillBottomRow.height;
                this.mLayoutRows.add(new LayoutRow(fillBottomRow.adapterPosition, fillBottomRow.length, paddingTop, paddingTop));
                i3 += fillBottomRow.length;
            }
            if (paddingTop >= getExtraLayoutSpace(state) + height) {
                break;
            }
        }
        if (getBottomRow().bottom < height) {
            scrollVerticallyBy(getBottomRow().bottom - height, recycler, state);
        } else {
            clearViewsAndStickHeaders(recycler, state, false);
        }
        if (this.mPendingScrollPosition >= 0) {
            this.mPendingScrollPosition = -1;
            int positionSectionHeaderHeight = getPositionSectionHeaderHeight(findFirstRowItem);
            if (positionSectionHeaderHeight != 0) {
                scrollVerticallyBy(-positionSectionHeaderHeight, recycler, state);
            }
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public void onLayoutCompleted(RecyclerView.State state) {
        super.onLayoutCompleted(state);
        this.mPendingSavedState = null;
    }

    public int getPositionSectionHeaderHeight(int i) {
        int adapterPositionSection = this.mAdapter.getAdapterPositionSection(i);
        if (adapterPositionSection < 0 || !this.mAdapter.isSectionHeaderSticky(adapterPositionSection) || this.mAdapter.getItemSectionOffset(adapterPositionSection, i) < 0) {
            return 0;
        }
        int sectionHeaderPosition = this.mAdapter.getSectionHeaderPosition(adapterPositionSection);
        View view = this.mFloatingHeaderView;
        if (view != null && sectionHeaderPosition == this.mFloatingHeaderPosition) {
            return Math.max(0, getDecoratedMeasuredHeight(view) - this.mHeaderOverlapMargin);
        }
        LayoutRow headerRow = getHeaderRow(sectionHeaderPosition);
        if (headerRow != null) {
            return headerRow.getHeight();
        }
        return this.mAverageHeaderHeight;
    }

    private int findFirstRowItem(int i) {
        int adapterPositionSection = this.mAdapter.getAdapterPositionSection(i);
        int itemSectionOffset = this.mAdapter.getItemSectionOffset(adapterPositionSection, i);
        while (itemSectionOffset > 0 && this.mSpanSizeLookup.getSpanIndex(adapterPositionSection, itemSectionOffset, this.mSpanCount) != 0) {
            itemSectionOffset--;
            i--;
        }
        return i;
    }

    private int getSpanWidth(int i, int i2, int i3) {
        int i4 = this.mSpanCount;
        int i5 = i / i4;
        return (i5 * i3) + Math.min(Math.max(0, (i - (i4 * i5)) - i2), i3);
    }

    private FillResult fillBottomRow(RecyclerView.Recycler recycler, RecyclerView.State state, int i, int i2) {
        int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
        int adapterPositionSection = this.mAdapter.getAdapterPositionSection(i);
        int itemSectionOffset = this.mAdapter.getItemSectionOffset(adapterPositionSection, i);
        int spanSize = this.mSpanSizeLookup.getSpanSize(adapterPositionSection, itemSectionOffset);
        int spanIndex = this.mSpanSizeLookup.getSpanIndex(adapterPositionSection, itemSectionOffset, this.mSpanCount);
        Arrays.fill(this.mFillViewSet, (Object) null);
        int i3 = i;
        int i4 = 0;
        int i5 = 0;
        while (spanIndex < this.mSpanCount) {
            int spanWidth = getSpanWidth(width, spanIndex, spanSize);
            View viewForPosition = recycler.getViewForPosition(i3);
            LayoutParams layoutParams = (LayoutParams) viewForPosition.getLayoutParams();
            layoutParams.mSpanIndex = spanIndex;
            layoutParams.mSpanSize = spanSize;
            addView(viewForPosition, this.mHeadersStartPosition);
            this.mHeadersStartPosition++;
            measureChildWithMargins(viewForPosition, width - spanWidth, 0);
            this.mFillViewSet[i4] = viewForPosition;
            i4++;
            int decoratedMeasuredHeight = getDecoratedMeasuredHeight(viewForPosition);
            if (i5 < decoratedMeasuredHeight) {
                i5 = decoratedMeasuredHeight;
            }
            i3++;
            itemSectionOffset++;
            if (itemSectionOffset >= this.mAdapter.getSectionItemCount(adapterPositionSection)) {
                break;
            }
            spanIndex += spanSize;
            spanSize = this.mSpanSizeLookup.getSpanSize(adapterPositionSection, itemSectionOffset);
        }
        int paddingLeft = getPaddingLeft();
        int i6 = 0;
        while (i6 < i4) {
            View view = this.mFillViewSet[i6];
            int decoratedMeasuredHeight2 = getDecoratedMeasuredHeight(view);
            int decoratedMeasuredWidth = paddingLeft + getDecoratedMeasuredWidth(view);
            layoutDecorated(view, paddingLeft, i2, decoratedMeasuredWidth, i2 + decoratedMeasuredHeight2);
            i6++;
            paddingLeft = decoratedMeasuredWidth;
        }
        this.mFillResult.edgeView = this.mFillViewSet[i4 - 1];
        this.mFillResult.adapterPosition = i;
        this.mFillResult.length = i4;
        this.mFillResult.height = i5;
        return this.mFillResult;
    }

    private FillResult fillTopRow(RecyclerView.Recycler recycler, RecyclerView.State state, int i, int i2) {
        int i3 = i;
        int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
        int adapterPositionSection = this.mAdapter.getAdapterPositionSection(i3);
        int itemSectionOffset = this.mAdapter.getItemSectionOffset(adapterPositionSection, i3);
        int spanSize = this.mSpanSizeLookup.getSpanSize(adapterPositionSection, itemSectionOffset);
        int spanIndex = this.mSpanSizeLookup.getSpanIndex(adapterPositionSection, itemSectionOffset, this.mSpanCount);
        Arrays.fill(this.mFillViewSet, (Object) null);
        int i4 = 0;
        int i5 = 0;
        while (spanIndex >= 0) {
            int spanWidth = getSpanWidth(width, spanIndex, spanSize);
            View viewForPosition = recycler.getViewForPosition(i3);
            LayoutParams layoutParams = (LayoutParams) viewForPosition.getLayoutParams();
            layoutParams.mSpanIndex = spanIndex;
            layoutParams.mSpanSize = spanSize;
            addView(viewForPosition, 0);
            this.mHeadersStartPosition++;
            measureChildWithMargins(viewForPosition, width - spanWidth, 0);
            this.mFillViewSet[i4] = viewForPosition;
            i4++;
            int decoratedMeasuredHeight = getDecoratedMeasuredHeight(viewForPosition);
            if (i5 < decoratedMeasuredHeight) {
                i5 = decoratedMeasuredHeight;
            }
            i3--;
            itemSectionOffset--;
            if (itemSectionOffset < 0) {
                break;
            }
            spanSize = this.mSpanSizeLookup.getSpanSize(adapterPositionSection, itemSectionOffset);
            spanIndex -= spanSize;
        }
        int paddingLeft = getPaddingLeft();
        int i6 = i4 - 1;
        int i7 = i6;
        while (i7 >= 0) {
            View view = this.mFillViewSet[i7];
            int decoratedMeasuredHeight2 = getDecoratedMeasuredHeight(view);
            int decoratedMeasuredWidth = paddingLeft + getDecoratedMeasuredWidth(view);
            layoutDecorated(view, paddingLeft, i2 - i5, decoratedMeasuredWidth, i2 - (i5 - decoratedMeasuredHeight2));
            i7--;
            paddingLeft = decoratedMeasuredWidth;
        }
        this.mFillResult.edgeView = this.mFillViewSet[i6];
        this.mFillResult.adapterPosition = i3 + 1;
        this.mFillResult.length = i4;
        this.mFillResult.height = i5;
        return this.mFillResult;
    }

    private void clearHiddenRows(RecyclerView.Recycler recycler, RecyclerView.State state, boolean z) {
        if (this.mLayoutRows.size() > 0) {
            int paddingTop = getPaddingTop();
            int height = getHeight() - getPaddingBottom();
            if (z) {
                LayoutRow topRow = getTopRow();
                while (true) {
                    if (topRow.bottom < paddingTop - getExtraLayoutSpace(state) || topRow.top > height) {
                        if (topRow.header) {
                            removeAndRecycleViewAt(this.mHeadersStartPosition + (this.mFloatingHeaderView != null ? 1 : 0), recycler);
                        } else {
                            for (int i = 0; i < topRow.length; i++) {
                                removeAndRecycleViewAt(0, recycler);
                                this.mHeadersStartPosition--;
                            }
                        }
                        this.mLayoutRows.remove(0);
                        topRow = getTopRow();
                    } else {
                        return;
                    }
                }
            } else {
                LayoutRow bottomRow = getBottomRow();
                while (true) {
                    if (bottomRow.bottom < paddingTop || bottomRow.top > getExtraLayoutSpace(state) + height) {
                        if (bottomRow.header) {
                            removeAndRecycleViewAt(getChildCount() - 1, recycler);
                        } else {
                            for (int i2 = 0; i2 < bottomRow.length; i2++) {
                                removeAndRecycleViewAt(this.mHeadersStartPosition - 1, recycler);
                                this.mHeadersStartPosition--;
                            }
                        }
                        ArrayList<LayoutRow> arrayList = this.mLayoutRows;
                        arrayList.remove(arrayList.size() - 1);
                        bottomRow = getBottomRow();
                    } else {
                        return;
                    }
                }
            }
        }
    }

    private void clearViewsAndStickHeaders(RecyclerView.Recycler recycler, RecyclerView.State state, boolean z) {
        clearHiddenRows(recycler, state, z);
        if (getChildCount() > 0) {
            stickTopHeader(recycler);
        }
        updateTopPosition();
    }

    private LayoutRow getBottomRow() {
        ArrayList<LayoutRow> arrayList = this.mLayoutRows;
        return arrayList.get(arrayList.size() - 1);
    }

    private LayoutRow getTopRow() {
        return this.mLayoutRows.get(0);
    }

    private void offsetRowsVertical(int i) {
        Iterator<LayoutRow> it = this.mLayoutRows.iterator();
        while (it.hasNext()) {
            LayoutRow next = it.next();
            next.top += i;
            next.bottom += i;
        }
        offsetChildrenVertical(i);
    }

    private void addRow(RecyclerView.Recycler recycler, RecyclerView.State state, boolean z, int i, int i2) {
        int paddingLeft = getPaddingLeft();
        int width = getWidth() - getPaddingRight();
        if (z && this.mFloatingHeaderView != null && i == this.mFloatingHeaderPosition) {
            removeFloatingHeader(recycler);
        }
        if (this.mAdapter.getItemViewInternalType(i) == 0) {
            View viewForPosition = recycler.getViewForPosition(i);
            if (z) {
                addView(viewForPosition, this.mHeadersStartPosition);
            } else {
                addView(viewForPosition);
            }
            measureChildWithMargins(viewForPosition, 0, 0);
            int decoratedMeasuredHeight = getDecoratedMeasuredHeight(viewForPosition);
            int i3 = this.mHeaderOverlapMargin;
            int i4 = decoratedMeasuredHeight >= i3 ? i3 : decoratedMeasuredHeight;
            if (z) {
                int i5 = (i2 - decoratedMeasuredHeight) + i4;
                layoutDecorated(viewForPosition, paddingLeft, i5, width, i2 + i4);
                this.mLayoutRows.add(0, new LayoutRow(viewForPosition, i, 1, i5, i2));
            } else {
                int i6 = i2 + decoratedMeasuredHeight;
                layoutDecorated(viewForPosition, paddingLeft, i2, width, i6);
                this.mLayoutRows.add(new LayoutRow(viewForPosition, i, 1, i2, i6 - i4));
            }
            this.mAverageHeaderHeight = decoratedMeasuredHeight - i4;
        } else if (z) {
            FillResult fillTopRow = fillTopRow(recycler, state, i, i2);
            this.mLayoutRows.add(0, new LayoutRow(fillTopRow.adapterPosition, fillTopRow.length, i2 - fillTopRow.height, i2));
        } else {
            FillResult fillBottomRow = fillBottomRow(recycler, state, i, i2);
            this.mLayoutRows.add(new LayoutRow(fillBottomRow.adapterPosition, fillBottomRow.length, i2, fillBottomRow.height + i2));
        }
    }

    private void addOffScreenRows(RecyclerView.Recycler recycler, RecyclerView.State state, int i, int i2, boolean z) {
        if (z) {
            while (true) {
                LayoutRow bottomRow = getBottomRow();
                int i3 = bottomRow.adapterPosition + bottomRow.length;
                if (bottomRow.bottom < getExtraLayoutSpace(state) + i2 && i3 < state.getItemCount()) {
                    addRow(recycler, state, false, i3, bottomRow.bottom);
                } else {
                    return;
                }
            }
        } else {
            while (true) {
                LayoutRow topRow = getTopRow();
                int i4 = topRow.adapterPosition - 1;
                if (topRow.top >= i - getExtraLayoutSpace(state) && i4 >= 0) {
                    addRow(recycler, state, true, i4, topRow.top);
                } else {
                    return;
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00cb  */
    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int scrollVerticallyBy(int r14, androidx.recyclerview.widget.RecyclerView.Recycler r15, androidx.recyclerview.widget.RecyclerView.State r16) {
        /*
            r13 = this;
            r6 = r13
            r7 = r14
            int r0 = r13.getChildCount()
            r8 = 0
            if (r0 != 0) goto L_0x000a
            return r8
        L_0x000a:
            r13.getPaddingLeft()
            r13.getWidth()
            r13.getPaddingRight()
            int r9 = r13.getPaddingTop()
            int r0 = r13.getHeight()
            int r1 = r13.getPaddingBottom()
            int r10 = r0 - r1
            int r0 = r13.getFirstVisibleSectionHeader()
            r1 = -1
            if (r0 == r1) goto L_0x003a
            java.util.ArrayList<com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager$LayoutRow> r1 = r6.mLayoutRows
            java.lang.Object r0 = r1.get(r0)
            com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager$LayoutRow r0 = (com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager.LayoutRow) r0
            android.view.View r0 = com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager.LayoutRow.access$1900(r0)
            int r1 = r6.mStickOffset
            int r1 = -r1
            r0.offsetTopAndBottom(r1)
        L_0x003a:
            r11 = 1
            r0 = 0
            if (r7 < 0) goto L_0x007c
        L_0x003e:
            if (r0 >= r7) goto L_0x007a
            com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager$LayoutRow r1 = r13.getBottomRow()
            int r2 = com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager.LayoutRow.access$1200(r1)
            int r2 = r2 - r10
            int r2 = java.lang.Math.max(r2, r8)
            int r3 = r7 - r0
            int r2 = java.lang.Math.min(r2, r3)
            int r2 = -r2
            r13.offsetRowsVertical(r2)
            int r12 = r0 - r2
            int r0 = com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager.LayoutRow.access$800(r1)
            int r2 = com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager.LayoutRow.access$1800(r1)
            int r4 = r0 + r2
            if (r12 >= r7) goto L_0x00b6
            int r0 = r16.getItemCount()
            if (r4 < r0) goto L_0x006c
            goto L_0x00b6
        L_0x006c:
            r3 = 0
            int r5 = com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager.LayoutRow.access$1200(r1)
            r0 = r13
            r1 = r15
            r2 = r16
            r0.addRow(r1, r2, r3, r4, r5)
            r0 = r12
            goto L_0x003e
        L_0x007a:
            r12 = r0
            goto L_0x00b6
        L_0x007c:
            if (r0 <= r7) goto L_0x007a
            com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager$LayoutRow r1 = r13.getTopRow()
            int r2 = com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager.LayoutRow.access$1600(r1)
            int r2 = -r2
            int r2 = r2 + r9
            int r2 = java.lang.Math.max(r2, r8)
            int r3 = r0 - r7
            int r2 = java.lang.Math.min(r2, r3)
            r13.offsetRowsVertical(r2)
            int r12 = r0 - r2
            int r0 = com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager.LayoutRow.access$800(r1)
            int r4 = r0 + -1
            if (r12 <= r7) goto L_0x00b6
            int r0 = r16.getItemCount()
            if (r4 >= r0) goto L_0x00b6
            if (r4 >= 0) goto L_0x00a8
            goto L_0x00b6
        L_0x00a8:
            r3 = 1
            int r5 = com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager.LayoutRow.access$1600(r1)
            r0 = r13
            r1 = r15
            r2 = r16
            r0.addRow(r1, r2, r3, r4, r5)
            r0 = r12
            goto L_0x007c
        L_0x00b6:
            if (r12 != r7) goto L_0x00c6
            if (r7 < 0) goto L_0x00bc
            r5 = 1
            goto L_0x00bd
        L_0x00bc:
            r5 = 0
        L_0x00bd:
            r0 = r13
            r1 = r15
            r2 = r16
            r3 = r9
            r4 = r10
            r0.addOffScreenRows(r1, r2, r3, r4, r5)
        L_0x00c6:
            r0 = r15
            r1 = r16
            if (r7 < 0) goto L_0x00cc
            r8 = 1
        L_0x00cc:
            r13.clearViewsAndStickHeaders(r15, r1, r8)
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager.scrollVerticallyBy(int, androidx.recyclerview.widget.RecyclerView$Recycler, androidx.recyclerview.widget.RecyclerView$State):int");
    }

    private LayoutRow getFirstVisibleRow() {
        int paddingTop = getPaddingTop();
        Iterator<LayoutRow> it = this.mLayoutRows.iterator();
        while (it.hasNext()) {
            LayoutRow next = it.next();
            if (next.bottom > paddingTop) {
                return next;
            }
        }
        return null;
    }

    private int getFirstVisibleSectionHeader() {
        int paddingTop = getPaddingTop();
        int size = this.mLayoutRows.size();
        int i = -1;
        for (int i2 = 0; i2 < size; i2++) {
            LayoutRow layoutRow = this.mLayoutRows.get(i2);
            if (layoutRow.header) {
                i = i2;
            }
            if (layoutRow.bottom > paddingTop) {
                return i;
            }
        }
        return -1;
    }

    private LayoutRow getNextVisibleSectionHeader(int i) {
        int size = this.mLayoutRows.size();
        for (int i2 = i + 1; i2 < size; i2++) {
            LayoutRow layoutRow = this.mLayoutRows.get(i2);
            if (layoutRow.header) {
                return layoutRow;
            }
        }
        return null;
    }

    private LayoutRow getHeaderRow(int i) {
        int size = this.mLayoutRows.size();
        for (int i2 = 0; i2 < size; i2++) {
            LayoutRow layoutRow = this.mLayoutRows.get(i2);
            if (layoutRow.header && layoutRow.adapterPosition == i) {
                return layoutRow;
            }
        }
        return null;
    }

    private void removeFloatingHeader(RecyclerView.Recycler recycler) {
        View view = this.mFloatingHeaderView;
        if (view != null) {
            this.mFloatingHeaderView = null;
            this.mFloatingHeaderPosition = -1;
            removeAndRecycleView(view, recycler);
        }
    }

    private void onHeaderChanged(int i, View view, HeaderState headerState, int i2) {
        HeaderStateChangeListener headerStateChangeListener;
        int i3 = this.mStickyHeaderSection;
        if (!(i3 == -1 || i == i3)) {
            onHeaderUnstick();
        }
        boolean z = this.mStickyHeaderSection != i || !this.mStickyHeadeState.equals(headerState) || headerState.equals(HeaderState.PUSHED);
        this.mStickyHeaderSection = i;
        this.mStickyHeaderView = view;
        this.mStickyHeadeState = headerState;
        if (z && (headerStateChangeListener = this.mHeaderStateListener) != null) {
            headerStateChangeListener.onHeaderStateChanged(i, view, headerState, i2);
        }
    }

    private void onHeaderUnstick() {
        int i = this.mStickyHeaderSection;
        if (i != -1) {
            HeaderStateChangeListener headerStateChangeListener = this.mHeaderStateListener;
            if (headerStateChangeListener != null) {
                headerStateChangeListener.onHeaderStateChanged(i, this.mStickyHeaderView, HeaderState.NORMAL, 0);
            }
            this.mStickyHeaderSection = -1;
            this.mStickyHeaderView = null;
            this.mStickyHeadeState = HeaderState.NORMAL;
        }
    }

    private void stickTopHeader(RecyclerView.Recycler recycler) {
        int i;
        int firstVisibleSectionHeader = getFirstVisibleSectionHeader();
        int paddingTop = getPaddingTop();
        int paddingLeft = getPaddingLeft();
        int width = getWidth() - getPaddingRight();
        HeaderState headerState = HeaderState.NORMAL;
        int i2 = 0;
        if (firstVisibleSectionHeader != -1) {
            removeFloatingHeader(recycler);
            LayoutRow layoutRow = this.mLayoutRows.get(firstVisibleSectionHeader);
            int adapterPositionSection = this.mAdapter.getAdapterPositionSection(layoutRow.adapterPosition);
            if (this.mAdapter.isSectionHeaderSticky(adapterPositionSection)) {
                LayoutRow nextVisibleSectionHeader = getNextVisibleSectionHeader(firstVisibleSectionHeader);
                if (nextVisibleSectionHeader != null) {
                    int height = layoutRow.getHeight();
                    i2 = Math.min(Math.max(paddingTop - nextVisibleSectionHeader.top, -height) + height, height);
                }
                this.mStickOffset = (paddingTop - layoutRow.top) - i2;
                layoutRow.headerView.offsetTopAndBottom(this.mStickOffset);
                onHeaderChanged(adapterPositionSection, layoutRow.headerView, i2 == 0 ? HeaderState.STICKY : HeaderState.PUSHED, i2);
                return;
            }
            onHeaderUnstick();
            this.mStickOffset = 0;
            return;
        }
        LayoutRow firstVisibleRow = getFirstVisibleRow();
        if (firstVisibleRow != null) {
            int adapterPositionSection2 = this.mAdapter.getAdapterPositionSection(firstVisibleRow.adapterPosition);
            if (this.mAdapter.isSectionHeaderSticky(adapterPositionSection2)) {
                int sectionHeaderPosition = this.mAdapter.getSectionHeaderPosition(adapterPositionSection2);
                if (this.mFloatingHeaderView == null || this.mFloatingHeaderPosition != sectionHeaderPosition) {
                    removeFloatingHeader(recycler);
                    View viewForPosition = recycler.getViewForPosition(sectionHeaderPosition);
                    addView(viewForPosition, this.mHeadersStartPosition);
                    measureChildWithMargins(viewForPosition, 0, 0);
                    this.mFloatingHeaderView = viewForPosition;
                    this.mFloatingHeaderPosition = sectionHeaderPosition;
                }
                int decoratedMeasuredHeight = getDecoratedMeasuredHeight(this.mFloatingHeaderView);
                int childCount = getChildCount();
                int i3 = this.mHeadersStartPosition;
                if (childCount - i3 > 1) {
                    View childAt = getChildAt(i3 + 1);
                    int max = Math.max(0, decoratedMeasuredHeight - this.mHeaderOverlapMargin);
                    i = max + Math.max(paddingTop - getDecoratedTop(childAt), -max);
                } else {
                    i = 0;
                }
                layoutDecorated(this.mFloatingHeaderView, paddingLeft, paddingTop - i, width, (paddingTop + decoratedMeasuredHeight) - i);
                onHeaderChanged(adapterPositionSection2, this.mFloatingHeaderView, i == 0 ? HeaderState.STICKY : HeaderState.PUSHED, i);
                return;
            }
            onHeaderUnstick();
            return;
        }
        onHeaderUnstick();
    }

    private void updateTopPosition() {
        if (getChildCount() == 0) {
            this.mAnchor.reset();
        }
        LayoutRow firstVisibleRow = getFirstVisibleRow();
        if (firstVisibleRow != null) {
            this.mAnchor.section = this.mAdapter.getAdapterPositionSection(firstVisibleRow.adapterPosition);
            AnchorPosition anchorPosition = this.mAnchor;
            anchorPosition.item = this.mAdapter.getItemSectionOffset(anchorPosition.section, firstVisibleRow.adapterPosition);
            this.mAnchor.offset = Math.min(firstVisibleRow.top - getPaddingTop(), 0);
        }
    }

    private void clearState() {
        this.mHeadersStartPosition = 0;
        this.mStickOffset = 0;
        this.mFloatingHeaderView = null;
        this.mFloatingHeaderPosition = -1;
        this.mAverageHeaderHeight = 0;
        this.mLayoutRows.clear();
        int i = this.mStickyHeaderSection;
        if (i != -1) {
            HeaderStateChangeListener headerStateChangeListener = this.mHeaderStateListener;
            if (headerStateChangeListener != null) {
                headerStateChangeListener.onHeaderStateChanged(i, this.mStickyHeaderView, HeaderState.NORMAL, 0);
            }
            this.mStickyHeaderSection = -1;
            this.mStickyHeaderView = null;
            this.mStickyHeadeState = HeaderState.NORMAL;
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int computeVerticalScrollExtent(RecyclerView.State state) {
        if (!(this.mHeadersStartPosition == 0 || state.getItemCount() == 0)) {
            View childAt = getChildAt(0);
            View childAt2 = getChildAt(this.mHeadersStartPosition - 1);
            if (!(childAt == null || childAt2 == null)) {
                return Math.abs(getPosition(childAt) - getPosition(childAt2)) + 1;
            }
        }
        return 0;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int computeVerticalScrollOffset(RecyclerView.State state) {
        if (!(this.mHeadersStartPosition == 0 || state.getItemCount() == 0)) {
            View childAt = getChildAt(0);
            View childAt2 = getChildAt(this.mHeadersStartPosition - 1);
            if (!(childAt == null || childAt2 == null)) {
                if (Math.max((-getTopRow().top) + getPaddingTop(), 0) == 0) {
                    return 0;
                }
                int min = Math.min(getPosition(childAt), getPosition(childAt2));
                Math.max(getPosition(childAt), getPosition(childAt2));
                return Math.max(0, min);
            }
        }
        return 0;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.LayoutManager
    public int computeVerticalScrollRange(RecyclerView.State state) {
        if (!(this.mHeadersStartPosition == 0 || state.getItemCount() == 0)) {
            View childAt = getChildAt(0);
            View childAt2 = getChildAt(this.mHeadersStartPosition - 1);
            if (!(childAt == null || childAt2 == null)) {
                return state.getItemCount();
            }
        }
        return 0;
    }

    /* loaded from: classes.dex */
    public static class LayoutParams extends RecyclerView.LayoutParams {
        private int mSpanIndex = -1;
        private int mSpanSize = 0;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }
    }

    /* loaded from: classes.dex */
    public static final class DefaultSpanSizeLookup extends SpanSizeLookup {
        @Override // com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager.SpanSizeLookup
        public int getSpanSize(int i, int i2) {
            return 1;
        }

        @Override // com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager.SpanSizeLookup
        public int getSpanIndex(int i, int i2, int i3) {
            return i2 % i3;
        }
    }

    /* loaded from: classes.dex */
    public static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() { // from class: com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager.SavedState.1
            @Override // android.os.Parcelable.Creator
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            @Override // android.os.Parcelable.Creator
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };
        private int mAnchorItem;
        private int mAnchorOffset;
        private int mAnchorSection;

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        public SavedState() {
        }

        SavedState(Parcel parcel) {
            this.mAnchorSection = parcel.readInt();
            this.mAnchorItem = parcel.readInt();
            this.mAnchorOffset = parcel.readInt();
        }

        public SavedState(SavedState savedState) {
            this.mAnchorSection = savedState.mAnchorSection;
            this.mAnchorItem = savedState.mAnchorItem;
            this.mAnchorOffset = savedState.mAnchorOffset;
        }

        boolean hasValidAnchor() {
            return this.mAnchorSection >= 0;
        }

        void invalidateAnchor() {
            this.mAnchorSection = -1;
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.mAnchorSection);
            parcel.writeInt(this.mAnchorItem);
            parcel.writeInt(this.mAnchorOffset);
        }
    }

    /* loaded from: classes.dex */
    public static class LayoutRow {
        private int adapterPosition;
        private int bottom;
        private boolean header;
        private View headerView;
        private int length;
        private int top;

        public LayoutRow(int i, int i2, int i3, int i4) {
            this.header = false;
            this.headerView = null;
            this.adapterPosition = i;
            this.length = i2;
            this.top = i3;
            this.bottom = i4;
        }

        public LayoutRow(View view, int i, int i2, int i3, int i4) {
            this.header = true;
            this.headerView = view;
            this.adapterPosition = i;
            this.length = i2;
            this.top = i3;
            this.bottom = i4;
        }

        int getHeight() {
            return this.bottom - this.top;
        }
    }

    /* loaded from: classes.dex */
    public static class FillResult {
        private int adapterPosition;
        private View edgeView;
        private int height;
        private int length;

        private FillResult() {
        }
    }

    /* loaded from: classes.dex */
    public static class AnchorPosition {
        private int item;
        private int offset;
        private int section;

        public AnchorPosition() {
            reset();
        }

        public void reset() {
            this.section = -1;
            this.item = 0;
            this.offset = 0;
        }
    }
}
