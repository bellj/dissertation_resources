package com.codewaves.stickyheadergrid;

import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.security.InvalidParameterException;
import java.util.ArrayList;

/* loaded from: classes.dex */
public abstract class StickyHeaderGridAdapter extends RecyclerView.Adapter<ViewHolder> {
    public static final String TAG;
    public static final int TYPE_HEADER;
    public static final int TYPE_ITEM;
    private int[] mSectionIndices;
    private ArrayList<Section> mSections;
    private int mTotalItemNumber;

    public static int externalViewType(int i) {
        return i >> 8;
    }

    private int getItemViewInternalType(int i, int i2) {
        return i2 == 0 ? 0 : 1;
    }

    private static int internalViewType(int i) {
        return i & 255;
    }

    public int getSectionCount() {
        return 0;
    }

    public int getSectionHeaderViewType(int i) {
        return 0;
    }

    public int getSectionItemCount(int i) {
        return 0;
    }

    public int getSectionItemViewType(int i, int i2) {
        return 0;
    }

    public boolean isSectionHeaderSticky(int i) {
        return true;
    }

    public abstract void onBindHeaderViewHolder(HeaderViewHolder headerViewHolder, int i);

    public abstract void onBindItemViewHolder(ItemViewHolder itemViewHolder, int i, int i2);

    public abstract HeaderViewHolder onCreateHeaderViewHolder(ViewGroup viewGroup, int i);

    public abstract ItemViewHolder onCreateItemViewHolder(ViewGroup viewGroup, int i);

    /* loaded from: classes.dex */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public boolean isHeader() {
            return false;
        }

        public ViewHolder(View view) {
            super(view);
        }

        public int getSectionItemViewType() {
            return StickyHeaderGridAdapter.externalViewType(getItemViewType());
        }
    }

    /* loaded from: classes.dex */
    public static class ItemViewHolder extends ViewHolder {
        public ItemViewHolder(View view) {
            super(view);
        }
    }

    /* loaded from: classes.dex */
    public static class HeaderViewHolder extends ViewHolder {
        public boolean isHeader() {
            return true;
        }

        public HeaderViewHolder(View view) {
            super(view);
        }
    }

    /* loaded from: classes.dex */
    public static class Section {
        private int itemNumber;
        private int length;
        private int position;

        private Section() {
        }
    }

    private void calculateSections() {
        this.mSections = new ArrayList<>();
        int sectionCount = getSectionCount();
        int i = 0;
        for (int i2 = 0; i2 < sectionCount; i2++) {
            Section section = new Section();
            section.position = i;
            section.itemNumber = getSectionItemCount(i2);
            section.length = section.itemNumber + 1;
            this.mSections.add(section);
            i += section.length;
        }
        this.mTotalItemNumber = i;
        this.mSectionIndices = new int[i];
        int sectionCount2 = getSectionCount();
        int i3 = 0;
        for (int i4 = 0; i4 < sectionCount2; i4++) {
            Section section2 = this.mSections.get(i4);
            for (int i5 = 0; i5 < section2.length; i5++) {
                this.mSectionIndices[i3 + i5] = i4;
            }
            i3 += section2.length;
        }
    }

    public int getItemViewInternalType(int i) {
        int adapterPositionSection = getAdapterPositionSection(i);
        return getItemViewInternalType(adapterPositionSection, i - this.mSections.get(adapterPositionSection).position);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public final int getItemCount() {
        if (this.mSections == null) {
            calculateSections();
        }
        return this.mTotalItemNumber;
    }

    public final ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        int internalViewType = internalViewType(i);
        int externalViewType = externalViewType(i);
        if (internalViewType == 0) {
            return onCreateHeaderViewHolder(viewGroup, externalViewType);
        }
        if (internalViewType == 1) {
            return onCreateItemViewHolder(viewGroup, externalViewType);
        }
        throw new InvalidParameterException("Invalid viewType: " + i);
    }

    public final void onBindViewHolder(ViewHolder viewHolder, int i) {
        if (this.mSections == null) {
            calculateSections();
        }
        int i2 = this.mSectionIndices[i];
        int internalViewType = internalViewType(viewHolder.getItemViewType());
        externalViewType(viewHolder.getItemViewType());
        if (internalViewType == 0) {
            onBindHeaderViewHolder((HeaderViewHolder) viewHolder, i2);
        } else if (internalViewType == 1) {
            onBindItemViewHolder((ItemViewHolder) viewHolder, i2, getItemSectionOffset(i2, i));
        } else {
            throw new InvalidParameterException("invalid viewType: " + internalViewType);
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public final int getItemViewType(int i) {
        int i2;
        int adapterPositionSection = getAdapterPositionSection(i);
        int i3 = i - this.mSections.get(adapterPositionSection).position;
        int itemViewInternalType = getItemViewInternalType(adapterPositionSection, i3);
        if (itemViewInternalType == 0) {
            i2 = getSectionHeaderViewType(adapterPositionSection);
        } else if (itemViewInternalType != 1) {
            i2 = 0;
        } else {
            i2 = getSectionItemViewType(adapterPositionSection, i3 - 1);
        }
        return ((i2 & 255) << 8) | (itemViewInternalType & 255);
    }

    private int getItemSectionHeaderPosition(int i) {
        return getSectionHeaderPosition(getAdapterPositionSection(i));
    }

    private int getAdapterPosition(int i, int i2) {
        if (this.mSections == null) {
            calculateSections();
        }
        if (i < 0) {
            throw new IndexOutOfBoundsException("section " + i + " < 0");
        } else if (i < this.mSections.size()) {
            return this.mSections.get(i).position + i2;
        } else {
            throw new IndexOutOfBoundsException("section " + i + " >=" + this.mSections.size());
        }
    }

    public int getItemSectionOffset(int i, int i2) {
        if (this.mSections == null) {
            calculateSections();
        }
        if (i < 0) {
            throw new IndexOutOfBoundsException("section " + i + " < 0");
        } else if (i < this.mSections.size()) {
            Section section = this.mSections.get(i);
            int i3 = i2 - section.position;
            if (i3 < section.length) {
                return i3 - 1;
            }
            throw new IndexOutOfBoundsException("localPosition: " + i3 + " >=" + section.length);
        } else {
            throw new IndexOutOfBoundsException("section " + i + " >=" + this.mSections.size());
        }
    }

    public int getAdapterPositionSection(int i) {
        if (this.mSections == null) {
            calculateSections();
        }
        if (getItemCount() == 0) {
            return -1;
        }
        if (i < 0) {
            throw new IndexOutOfBoundsException("position " + i + " < 0");
        } else if (i < getItemCount()) {
            return this.mSectionIndices[i];
        } else {
            throw new IndexOutOfBoundsException("position " + i + " >=" + getItemCount());
        }
    }

    public int getSectionHeaderPosition(int i) {
        return getAdapterPosition(i, 0);
    }

    public int getSectionItemPosition(int i, int i2) {
        return getAdapterPosition(i, i2 + 1);
    }

    public void notifyAllSectionsDataSetChanged() {
        calculateSections();
        notifyDataSetChanged();
    }

    public void notifySectionDataSetChanged(int i) {
        calculateSections();
        ArrayList<Section> arrayList = this.mSections;
        if (arrayList == null) {
            notifyAllSectionsDataSetChanged();
            return;
        }
        Section section = arrayList.get(i);
        notifyItemRangeChanged(section.position, section.length);
    }

    public void notifySectionHeaderChanged(int i) {
        calculateSections();
        ArrayList<Section> arrayList = this.mSections;
        if (arrayList == null) {
            notifyAllSectionsDataSetChanged();
        } else {
            notifyItemRangeChanged(arrayList.get(i).position, 1);
        }
    }

    public void notifySectionItemChanged(int i, int i2) {
        calculateSections();
        ArrayList<Section> arrayList = this.mSections;
        if (arrayList == null) {
            notifyAllSectionsDataSetChanged();
            return;
        }
        Section section = arrayList.get(i);
        if (i2 < section.itemNumber) {
            notifyItemChanged(section.position + i2 + 1);
            return;
        }
        throw new IndexOutOfBoundsException("Invalid index " + i2 + ", size is " + section.itemNumber);
    }

    public void notifySectionInserted(int i) {
        calculateSections();
        ArrayList<Section> arrayList = this.mSections;
        if (arrayList == null) {
            notifyAllSectionsDataSetChanged();
            return;
        }
        Section section = arrayList.get(i);
        notifyItemRangeInserted(section.position, section.length);
    }

    public void notifySectionItemInserted(int i, int i2) {
        calculateSections();
        ArrayList<Section> arrayList = this.mSections;
        if (arrayList == null) {
            notifyAllSectionsDataSetChanged();
            return;
        }
        Section section = arrayList.get(i);
        if (i2 < 0 || i2 >= section.itemNumber) {
            throw new IndexOutOfBoundsException("Invalid index " + i2 + ", size is " + section.itemNumber);
        }
        notifyItemInserted(section.position + i2 + 1);
    }

    public void notifySectionItemRangeInserted(int i, int i2, int i3) {
        calculateSections();
        ArrayList<Section> arrayList = this.mSections;
        if (arrayList == null) {
            notifyAllSectionsDataSetChanged();
            return;
        }
        Section section = arrayList.get(i);
        if (i2 < 0 || i2 >= section.itemNumber) {
            throw new IndexOutOfBoundsException("Invalid index " + i2 + ", size is " + section.itemNumber);
        }
        int i4 = i2 + i3;
        if (i4 <= section.itemNumber) {
            notifyItemRangeInserted(section.position + i2 + 1, i3);
            return;
        }
        throw new IndexOutOfBoundsException("Invalid index " + i4 + ", size is " + section.itemNumber);
    }

    public void notifySectionRemoved(int i) {
        ArrayList<Section> arrayList = this.mSections;
        if (arrayList == null) {
            calculateSections();
            notifyAllSectionsDataSetChanged();
            return;
        }
        Section section = arrayList.get(i);
        calculateSections();
        notifyItemRangeRemoved(section.position, section.length);
    }

    public void notifySectionItemRemoved(int i, int i2) {
        ArrayList<Section> arrayList = this.mSections;
        if (arrayList == null) {
            calculateSections();
            notifyAllSectionsDataSetChanged();
            return;
        }
        Section section = arrayList.get(i);
        if (i2 < 0 || i2 >= section.itemNumber) {
            throw new IndexOutOfBoundsException("Invalid index " + i2 + ", size is " + section.itemNumber);
        }
        calculateSections();
        notifyItemRemoved(section.position + i2 + 1);
    }

    private void notifySectionItemRangeRemoved(int i, int i2, int i3) {
        ArrayList<Section> arrayList = this.mSections;
        if (arrayList == null) {
            calculateSections();
            notifyAllSectionsDataSetChanged();
            return;
        }
        Section section = arrayList.get(i);
        if (i2 < 0 || i2 >= section.itemNumber) {
            throw new IndexOutOfBoundsException("Invalid index " + i2 + ", size is " + section.itemNumber);
        }
        int i4 = i2 + i3;
        if (i4 <= section.itemNumber) {
            calculateSections();
            notifyItemRangeRemoved(section.position + i2 + 1, i3);
            return;
        }
        throw new IndexOutOfBoundsException("Invalid index " + i4 + ", size is " + section.itemNumber);
    }
}
