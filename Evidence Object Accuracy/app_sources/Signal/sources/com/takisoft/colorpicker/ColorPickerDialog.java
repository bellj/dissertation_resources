package com.takisoft.colorpicker;

import android.content.Context;
import android.content.res.Resources;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import androidx.appcompat.app.AlertDialog;
import java.util.Arrays;

/* loaded from: classes3.dex */
public class ColorPickerDialog extends AlertDialog implements OnColorSelectedListener {
    private OnColorSelectedListener listener;
    private final ColorPickerPaletteFlex mPalette;
    private final ProgressBar mProgress;
    private Params params;

    public ColorPickerDialog(Context context, OnColorSelectedListener onColorSelectedListener, Params params) {
        this(context, 0, onColorSelectedListener, params);
    }

    public ColorPickerDialog(Context context, int i, OnColorSelectedListener onColorSelectedListener, Params params) {
        super(context, resolveDialogTheme(context, i));
        Context context2 = getContext();
        this.listener = onColorSelectedListener;
        this.params = params;
        View inflate = LayoutInflater.from(context2).inflate(R$layout.color_picker_dialog, getListView());
        setView(inflate);
        this.mProgress = (ProgressBar) inflate.findViewById(16908301);
        ColorPickerPaletteFlex colorPickerPaletteFlex = (ColorPickerPaletteFlex) inflate.findViewById(R$id.color_picker);
        this.mPalette = colorPickerPaletteFlex;
        colorPickerPaletteFlex.setOnColorSelectedListener(this);
        if (params.mColumns > 0) {
            colorPickerPaletteFlex.getLayoutParams().width = params.mColumns * (params.mSwatchLength + (params.mMarginSize * 2));
        }
        if (params.mColors != null) {
            showPaletteView();
        }
    }

    public void showPaletteView() {
        ProgressBar progressBar = this.mProgress;
        if (progressBar != null && this.mPalette != null) {
            progressBar.setVisibility(8);
            refreshPalette();
            this.mPalette.setVisibility(0);
        }
    }

    private void refreshPalette() {
        ColorPickerPaletteFlex colorPickerPaletteFlex = this.mPalette;
        if (colorPickerPaletteFlex != null) {
            Params params = this.params;
            if (params.mColors != null) {
                colorPickerPaletteFlex.setup(params);
            }
        }
    }

    @Override // com.takisoft.colorpicker.OnColorSelectedListener
    public void onColorSelected(int i) {
        OnColorSelectedListener onColorSelectedListener = this.listener;
        if (onColorSelectedListener != null) {
            onColorSelectedListener.onColorSelected(i);
        }
        Params params = this.params;
        if (i != params.mSelectedColor) {
            params.mSelectedColor = i;
            this.mPalette.setup(params);
        }
        dismiss();
    }

    private static int resolveDialogTheme(Context context, int i) {
        return i == 0 ? R$style.ThemeOverlay_Material_Dialog_ColorPicker : i;
    }

    /* loaded from: classes3.dex */
    public static class Params implements Parcelable {
        public static final Parcelable.Creator<Params> CREATOR = new Parcelable.Creator<Params>() { // from class: com.takisoft.colorpicker.ColorPickerDialog.Params.1
            @Override // android.os.Parcelable.Creator
            public Params createFromParcel(Parcel parcel) {
                return new Params(parcel);
            }

            @Override // android.os.Parcelable.Creator
            public Params[] newArray(int i) {
                return new Params[i];
            }
        };
        CharSequence[] mColorContentDescriptions;
        int[] mColors;
        int mColumns;
        int mMarginSize;
        int mSelectedColor;
        int mSelectedColorIndex;
        int mSize;
        int mSwatchLength;

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        private Params() {
            this.mSelectedColorIndex = -1;
        }

        /* loaded from: classes3.dex */
        public static class Builder {
            private CharSequence[] colorContentDescriptions;
            private int[] colors;
            private int columns;
            private Context context;
            private int selectedColor;
            private int size = 2;
            private boolean sortColors = false;

            public Builder(Context context) {
                this.context = context;
            }

            public Builder setColors(int[] iArr) {
                this.colors = iArr;
                return this;
            }

            public Builder setColorContentDescriptions(CharSequence[] charSequenceArr) {
                this.colorContentDescriptions = charSequenceArr;
                return this;
            }

            public Builder setSelectedColor(int i) {
                this.selectedColor = i;
                return this;
            }

            public Builder setColumns(int i) {
                this.columns = i;
                return this;
            }

            public Builder setSortColors(boolean z) {
                this.sortColors = z;
                return this;
            }

            public Builder setSize(int i) {
                this.size = i;
                return this;
            }

            public Params build() {
                Resources resources = this.context.getResources();
                if (this.colors == null) {
                    this.colors = resources.getIntArray(R$array.color_picker_default_colors);
                }
                Params params = new Params();
                if (this.sortColors) {
                    int length = this.colors.length;
                    Integer[] numArr = new Integer[length];
                    for (int i = 0; i < length; i++) {
                        numArr[i] = Integer.valueOf(this.colors[i]);
                    }
                    Arrays.sort(numArr, new HsvColorComparator());
                    int[] iArr = new int[length];
                    for (int i2 = 0; i2 < length; i2++) {
                        iArr[i2] = numArr[i2].intValue();
                    }
                    params.mColors = iArr;
                } else {
                    params.mColors = this.colors;
                }
                params.mColorContentDescriptions = this.colorContentDescriptions;
                params.mSelectedColor = this.selectedColor;
                params.mColumns = this.columns;
                int i3 = this.size;
                params.mSize = i3;
                if (i3 == 1) {
                    params.mSwatchLength = resources.getDimensionPixelSize(R$dimen.color_swatch_large);
                    params.mMarginSize = resources.getDimensionPixelSize(R$dimen.color_swatch_margins_large);
                } else {
                    params.mSwatchLength = resources.getDimensionPixelSize(R$dimen.color_swatch_small);
                    params.mMarginSize = resources.getDimensionPixelSize(R$dimen.color_swatch_margins_small);
                }
                return params;
            }
        }

        protected Params(Parcel parcel) {
            this.mSelectedColorIndex = -1;
            this.mColors = parcel.createIntArray();
            this.mSelectedColor = parcel.readInt();
            this.mColumns = parcel.readInt();
            this.mSize = parcel.readInt();
            this.mSwatchLength = parcel.readInt();
            this.mMarginSize = parcel.readInt();
            this.mSelectedColorIndex = parcel.readInt();
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeIntArray(this.mColors);
            parcel.writeInt(this.mSelectedColor);
            parcel.writeInt(this.mColumns);
            parcel.writeInt(this.mSize);
            parcel.writeInt(this.mSwatchLength);
            parcel.writeInt(this.mMarginSize);
            parcel.writeInt(this.mSelectedColorIndex);
        }
    }
}
