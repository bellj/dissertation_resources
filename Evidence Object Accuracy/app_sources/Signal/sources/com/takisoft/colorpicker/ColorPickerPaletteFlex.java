package com.takisoft.colorpicker;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.takisoft.colorpicker.ColorPickerDialog;

/* loaded from: classes3.dex */
public class ColorPickerPaletteFlex extends RecyclerView implements OnColorSelectedListener {
    private String mDescription;
    private String mDescriptionSelected;
    public OnColorSelectedListener mOnColorSelectedListener;

    public ColorPickerPaletteFlex(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ColorPickerPaletteFlex(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setLayoutManager(new FlexboxLayoutManager(context, 0, 1));
        Resources resources = getResources();
        this.mDescription = resources.getString(R$string.color_swatch_description);
        this.mDescriptionSelected = resources.getString(R$string.color_swatch_description_selected);
    }

    public void setup(ColorPickerDialog.Params params) {
        if (params.mColors != null) {
            setAdapter(new ColorAdapter(params, this, this.mDescription, this.mDescriptionSelected));
            int length = params.mColors.length;
            for (int i = 0; i < length; i++) {
                if (params.mColors[i] == params.mSelectedColor) {
                    scrollToPosition(i);
                    return;
                }
            }
            return;
        }
        throw new IllegalArgumentException("The supplied params (" + params + ") does not contain colors.");
    }

    public void setOnColorSelectedListener(OnColorSelectedListener onColorSelectedListener) {
        this.mOnColorSelectedListener = onColorSelectedListener;
    }

    @Override // com.takisoft.colorpicker.OnColorSelectedListener
    public void onColorSelected(int i) {
        OnColorSelectedListener onColorSelectedListener = this.mOnColorSelectedListener;
        if (onColorSelectedListener != null) {
            onColorSelectedListener.onColorSelected(i);
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes3.dex */
    public static class ColorAdapter extends RecyclerView.Adapter<ColorHolder> {
        private OnColorSelectedListener colorSelectedListener;
        private final String description;
        private final String descriptionSelected;
        private ColorPickerDialog.Params params;

        private ColorAdapter(ColorPickerDialog.Params params, OnColorSelectedListener onColorSelectedListener, String str, String str2) {
            this.params = params;
            this.colorSelectedListener = onColorSelectedListener;
            this.description = str;
            this.descriptionSelected = str2;
        }

        public ColorHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            ColorPickerSwatch colorPickerSwatch = new ColorPickerSwatch(viewGroup.getContext());
            colorPickerSwatch.setOnColorSelectedListener(this.colorSelectedListener);
            int i2 = this.params.mSwatchLength;
            FlexboxLayoutManager.LayoutParams layoutParams = new FlexboxLayoutManager.LayoutParams(i2, i2);
            int i3 = this.params.mMarginSize;
            layoutParams.setMargins(i3, i3, i3, i3);
            layoutParams.setFlexGrow(0.0f);
            layoutParams.setFlexShrink(0.0f);
            colorPickerSwatch.setLayoutParams(layoutParams);
            return new ColorHolder(colorPickerSwatch);
        }

        public void onBindViewHolder(ColorHolder colorHolder, int i) {
            ColorPickerSwatch colorPickerSwatch = (ColorPickerSwatch) colorHolder.itemView;
            ColorPickerDialog.Params params = this.params;
            int i2 = params.mSelectedColor;
            int i3 = params.mColors[i];
            boolean z = i2 == i3;
            colorPickerSwatch.setColor(i3);
            colorPickerSwatch.setChecked(z);
            FlexboxLayoutManager.LayoutParams layoutParams = (FlexboxLayoutManager.LayoutParams) colorPickerSwatch.getLayoutParams();
            int i4 = this.params.mColumns;
            if (i4 <= 0 || i % i4 != 0) {
                layoutParams.setWrapBefore(false);
            } else {
                layoutParams.setWrapBefore(true);
            }
            setSwatchDescription(i, z, colorPickerSwatch, this.params.mColorContentDescriptions);
        }

        private void setSwatchDescription(int i, boolean z, View view, CharSequence[] charSequenceArr) {
            CharSequence charSequence;
            if (charSequenceArr == null || charSequenceArr.length <= i) {
                int i2 = i + 1;
                if (z) {
                    charSequence = String.format(this.descriptionSelected, Integer.valueOf(i2));
                } else {
                    charSequence = String.format(this.description, Integer.valueOf(i2));
                }
            } else {
                charSequence = charSequenceArr[i];
            }
            view.setContentDescription(charSequence);
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public int getItemCount() {
            return this.params.mColors.length;
        }
    }

    /* loaded from: classes3.dex */
    public static class ColorHolder extends RecyclerView.ViewHolder {
        ColorHolder(View view) {
            super(view);
        }
    }
}
