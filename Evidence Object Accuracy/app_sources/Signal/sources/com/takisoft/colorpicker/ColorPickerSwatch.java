package com.takisoft.colorpicker;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import androidx.appcompat.content.res.AppCompatResources;

/* loaded from: classes3.dex */
public class ColorPickerSwatch extends FrameLayout implements View.OnClickListener {
    private ImageView mCheckmarkImage;
    private int mColor;
    private OnColorSelectedListener mOnColorSelectedListener;
    private ImageView mSwatchImage = ((ImageView) findViewById(R$id.color_picker_swatch));

    public ColorPickerSwatch(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R$layout.color_picker_swatch, this);
        ImageView imageView = (ImageView) findViewById(R$id.color_picker_checkmark);
        this.mCheckmarkImage = imageView;
        imageView.setImageDrawable(getCheckmark(context));
        setOnClickListener(this);
    }

    private Drawable getCheckmark(Context context) {
        if (Build.VERSION.SDK_INT >= 23) {
            return AppCompatResources.getDrawable(context, R$drawable.color_picker_checkmark);
        }
        Drawable drawable = AppCompatResources.getDrawable(context, R$drawable.color_picker_check_tick);
        Drawable drawable2 = AppCompatResources.getDrawable(context, R$drawable.color_picker_check_base);
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(R$dimen.checkmark_base_padding);
        int dimensionPixelSize2 = context.getResources().getDimensionPixelSize(R$dimen.checkmark_tick_padding);
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{drawable2, drawable});
        layerDrawable.setLayerInset(0, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize, dimensionPixelSize);
        layerDrawable.setLayerInset(1, dimensionPixelSize2, dimensionPixelSize2, dimensionPixelSize2, dimensionPixelSize2);
        return layerDrawable;
    }

    public void setColor(int i) {
        this.mSwatchImage.setImageDrawable(new ColorStateDrawable(new Drawable[]{getContext().getResources().getDrawable(R$drawable.color_picker_swatch)}, i));
        this.mColor = i;
    }

    public void setOnColorSelectedListener(OnColorSelectedListener onColorSelectedListener) {
        this.mOnColorSelectedListener = onColorSelectedListener;
    }

    public void setChecked(boolean z) {
        if (z) {
            this.mCheckmarkImage.setVisibility(0);
        } else {
            this.mCheckmarkImage.setVisibility(8);
        }
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        OnColorSelectedListener onColorSelectedListener = this.mOnColorSelectedListener;
        if (onColorSelectedListener != null) {
            onColorSelectedListener.onColorSelected(this.mColor);
        }
    }
}
