package com.takisoft.colorpicker;

/* loaded from: classes3.dex */
public interface OnColorSelectedListener {
    void onColorSelected(int i);
}
