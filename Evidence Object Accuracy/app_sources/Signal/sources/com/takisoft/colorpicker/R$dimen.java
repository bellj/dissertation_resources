package com.takisoft.colorpicker;

/* loaded from: classes3.dex */
public final class R$dimen {
    public static final int checkmark_base_padding;
    public static final int checkmark_tick_padding;
    public static final int color_swatch_large;
    public static final int color_swatch_margins_large;
    public static final int color_swatch_margins_small;
    public static final int color_swatch_small;
}
