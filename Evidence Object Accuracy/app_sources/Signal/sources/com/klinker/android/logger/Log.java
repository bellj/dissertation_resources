package com.klinker.android.logger;

import android.os.Environment;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Locale;

/* loaded from: classes3.dex */
public class Log {
    private static boolean DEBUG_ENABLED;
    private static String PATH;
    private static OnLogListener logListener;

    public static void e(String str, String str2) {
        if (DEBUG_ENABLED && android.util.Log.e(str, str2) > 0) {
            logToFile(str, str2);
        }
    }

    public static void e(String str, String str2, Throwable th) {
        if (DEBUG_ENABLED && android.util.Log.e(str, str2, th) > 0) {
            logToFile(str, str2 + "\r\n" + android.util.Log.getStackTraceString(th));
        }
    }

    public static void v(String str, String str2) {
        if (DEBUG_ENABLED && android.util.Log.v(str, str2) > 0) {
            logToFile(str, str2);
        }
    }

    public static void d(String str, String str2) {
        if (DEBUG_ENABLED && android.util.Log.d(str, str2) > 0) {
            logToFile(str, str2);
        }
    }

    public static void w(String str, String str2) {
        if (DEBUG_ENABLED && android.util.Log.w(str, str2) > 0) {
            logToFile(str, str2);
        }
    }

    private static String getDateTimeStamp() {
        return DateFormat.getDateTimeInstance(3, 3, Locale.US).format(Calendar.getInstance().getTime());
    }

    private static void logToFile(String str, String str2) {
        try {
            File file = new File(Environment.getExternalStorageDirectory(), PATH);
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            if (file.length() > 2097152) {
                file.delete();
                file.createNewFile();
            }
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, true));
            bufferedWriter.write(String.format("%1s [%2s]:%3s\r\n", getDateTimeStamp(), str, str2));
            bufferedWriter.close();
            OnLogListener onLogListener = logListener;
            if (onLogListener != null) {
                onLogListener.onLogged(str, str2);
            }
        } catch (IOException e) {
            android.util.Log.e("Log", "Unable to log exception to file.", e);
        }
    }
}
