package com.klinker.android.send_message;

import android.content.Context;
import android.telephony.TelephonyManager;
import java.util.regex.Pattern;
import org.thoughtcrime.securesms.database.RecipientDatabase;

/* loaded from: classes3.dex */
public class Utils {
    private static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-]{1,256}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+");
    private static final Pattern NAME_ADDR_EMAIL_PATTERN = Pattern.compile("\\s*(\"[^\"]*\"|[^<>\"]+)\\s*<([^<>]+)>\\s*");

    public static String getMyPhoneNumber(Context context) {
        return ((TelephonyManager) context.getSystemService(RecipientDatabase.PHONE)).getLine1Number();
    }
}
