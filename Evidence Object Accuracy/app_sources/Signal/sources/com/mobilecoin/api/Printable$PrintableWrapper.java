package com.mobilecoin.api;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$PublicAddress;
import com.mobilecoin.api.Printable$PaymentRequest;
import com.mobilecoin.api.Printable$TransferPayload;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class Printable$PrintableWrapper extends GeneratedMessageLite<Printable$PrintableWrapper, Builder> implements MessageLiteOrBuilder {
    private static final Printable$PrintableWrapper DEFAULT_INSTANCE;
    private static volatile Parser<Printable$PrintableWrapper> PARSER;
    public static final int PAYMENT_REQUEST_FIELD_NUMBER;
    public static final int PUBLIC_ADDRESS_FIELD_NUMBER;
    public static final int TRANSFER_PAYLOAD_FIELD_NUMBER;
    private int wrapperCase_ = 0;
    private Object wrapper_;

    private Printable$PrintableWrapper() {
    }

    /* loaded from: classes3.dex */
    public enum WrapperCase {
        PUBLIC_ADDRESS(1),
        PAYMENT_REQUEST(2),
        TRANSFER_PAYLOAD(3),
        WRAPPER_NOT_SET(0);
        
        private final int value;

        WrapperCase(int i) {
            this.value = i;
        }

        public static WrapperCase forNumber(int i) {
            if (i == 0) {
                return WRAPPER_NOT_SET;
            }
            if (i == 1) {
                return PUBLIC_ADDRESS;
            }
            if (i == 2) {
                return PAYMENT_REQUEST;
            }
            if (i != 3) {
                return null;
            }
            return TRANSFER_PAYLOAD;
        }
    }

    public WrapperCase getWrapperCase() {
        return WrapperCase.forNumber(this.wrapperCase_);
    }

    public void clearWrapper() {
        this.wrapperCase_ = 0;
        this.wrapper_ = null;
    }

    public boolean hasPublicAddress() {
        return this.wrapperCase_ == 1;
    }

    public MobileCoinAPI$PublicAddress getPublicAddress() {
        if (this.wrapperCase_ == 1) {
            return (MobileCoinAPI$PublicAddress) this.wrapper_;
        }
        return MobileCoinAPI$PublicAddress.getDefaultInstance();
    }

    public void setPublicAddress(MobileCoinAPI$PublicAddress mobileCoinAPI$PublicAddress) {
        mobileCoinAPI$PublicAddress.getClass();
        this.wrapper_ = mobileCoinAPI$PublicAddress;
        this.wrapperCase_ = 1;
    }

    public void setPublicAddress(MobileCoinAPI$PublicAddress.Builder builder) {
        this.wrapper_ = builder.build();
        this.wrapperCase_ = 1;
    }

    public void mergePublicAddress(MobileCoinAPI$PublicAddress mobileCoinAPI$PublicAddress) {
        mobileCoinAPI$PublicAddress.getClass();
        if (this.wrapperCase_ != 1 || this.wrapper_ == MobileCoinAPI$PublicAddress.getDefaultInstance()) {
            this.wrapper_ = mobileCoinAPI$PublicAddress;
        } else {
            this.wrapper_ = MobileCoinAPI$PublicAddress.newBuilder((MobileCoinAPI$PublicAddress) this.wrapper_).mergeFrom((MobileCoinAPI$PublicAddress.Builder) mobileCoinAPI$PublicAddress).buildPartial();
        }
        this.wrapperCase_ = 1;
    }

    public void clearPublicAddress() {
        if (this.wrapperCase_ == 1) {
            this.wrapperCase_ = 0;
            this.wrapper_ = null;
        }
    }

    public boolean hasPaymentRequest() {
        return this.wrapperCase_ == 2;
    }

    public Printable$PaymentRequest getPaymentRequest() {
        if (this.wrapperCase_ == 2) {
            return (Printable$PaymentRequest) this.wrapper_;
        }
        return Printable$PaymentRequest.getDefaultInstance();
    }

    public void setPaymentRequest(Printable$PaymentRequest printable$PaymentRequest) {
        printable$PaymentRequest.getClass();
        this.wrapper_ = printable$PaymentRequest;
        this.wrapperCase_ = 2;
    }

    public void setPaymentRequest(Printable$PaymentRequest.Builder builder) {
        this.wrapper_ = builder.build();
        this.wrapperCase_ = 2;
    }

    public void mergePaymentRequest(Printable$PaymentRequest printable$PaymentRequest) {
        printable$PaymentRequest.getClass();
        if (this.wrapperCase_ != 2 || this.wrapper_ == Printable$PaymentRequest.getDefaultInstance()) {
            this.wrapper_ = printable$PaymentRequest;
        } else {
            this.wrapper_ = Printable$PaymentRequest.newBuilder((Printable$PaymentRequest) this.wrapper_).mergeFrom((Printable$PaymentRequest.Builder) printable$PaymentRequest).buildPartial();
        }
        this.wrapperCase_ = 2;
    }

    public void clearPaymentRequest() {
        if (this.wrapperCase_ == 2) {
            this.wrapperCase_ = 0;
            this.wrapper_ = null;
        }
    }

    public boolean hasTransferPayload() {
        return this.wrapperCase_ == 3;
    }

    public Printable$TransferPayload getTransferPayload() {
        if (this.wrapperCase_ == 3) {
            return (Printable$TransferPayload) this.wrapper_;
        }
        return Printable$TransferPayload.getDefaultInstance();
    }

    public void setTransferPayload(Printable$TransferPayload printable$TransferPayload) {
        printable$TransferPayload.getClass();
        this.wrapper_ = printable$TransferPayload;
        this.wrapperCase_ = 3;
    }

    public void setTransferPayload(Printable$TransferPayload.Builder builder) {
        this.wrapper_ = builder.build();
        this.wrapperCase_ = 3;
    }

    public void mergeTransferPayload(Printable$TransferPayload printable$TransferPayload) {
        printable$TransferPayload.getClass();
        if (this.wrapperCase_ != 3 || this.wrapper_ == Printable$TransferPayload.getDefaultInstance()) {
            this.wrapper_ = printable$TransferPayload;
        } else {
            this.wrapper_ = Printable$TransferPayload.newBuilder((Printable$TransferPayload) this.wrapper_).mergeFrom((Printable$TransferPayload.Builder) printable$TransferPayload).buildPartial();
        }
        this.wrapperCase_ = 3;
    }

    public void clearTransferPayload() {
        if (this.wrapperCase_ == 3) {
            this.wrapperCase_ = 0;
            this.wrapper_ = null;
        }
    }

    public static Printable$PrintableWrapper parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Printable$PrintableWrapper) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Printable$PrintableWrapper parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Printable$PrintableWrapper) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Printable$PrintableWrapper parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Printable$PrintableWrapper) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Printable$PrintableWrapper parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Printable$PrintableWrapper) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Printable$PrintableWrapper parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Printable$PrintableWrapper) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Printable$PrintableWrapper parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Printable$PrintableWrapper) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Printable$PrintableWrapper parseFrom(InputStream inputStream) throws IOException {
        return (Printable$PrintableWrapper) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Printable$PrintableWrapper parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Printable$PrintableWrapper) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Printable$PrintableWrapper parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Printable$PrintableWrapper) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Printable$PrintableWrapper parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Printable$PrintableWrapper) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Printable$PrintableWrapper parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Printable$PrintableWrapper) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Printable$PrintableWrapper parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Printable$PrintableWrapper) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Printable$PrintableWrapper printable$PrintableWrapper) {
        return DEFAULT_INSTANCE.createBuilder(printable$PrintableWrapper);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Printable$PrintableWrapper, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(Printable$1 printable$1) {
            this();
        }

        private Builder() {
            super(Printable$PrintableWrapper.DEFAULT_INSTANCE);
        }

        public Builder setPublicAddress(MobileCoinAPI$PublicAddress mobileCoinAPI$PublicAddress) {
            copyOnWrite();
            ((Printable$PrintableWrapper) this.instance).setPublicAddress(mobileCoinAPI$PublicAddress);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Printable$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Printable$PrintableWrapper();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0001\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001<\u0000\u0002<\u0000\u0003<\u0000", new Object[]{"wrapper_", "wrapperCase_", MobileCoinAPI$PublicAddress.class, Printable$PaymentRequest.class, Printable$TransferPayload.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Printable$PrintableWrapper> parser = PARSER;
                if (parser == null) {
                    synchronized (Printable$PrintableWrapper.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Printable$PrintableWrapper printable$PrintableWrapper = new Printable$PrintableWrapper();
        DEFAULT_INSTANCE = printable$PrintableWrapper;
        GeneratedMessageLite.registerDefaultInstance(Printable$PrintableWrapper.class, printable$PrintableWrapper);
    }

    public static Printable$PrintableWrapper getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Printable$PrintableWrapper> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
