package com.mobilecoin.api;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$RootEntropy;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class MobileCoinAPI$RootIdentity extends GeneratedMessageLite<MobileCoinAPI$RootIdentity, Builder> implements MessageLiteOrBuilder {
    private static final MobileCoinAPI$RootIdentity DEFAULT_INSTANCE;
    public static final int FOG_AUTHORITY_SPKI_FIELD_NUMBER;
    public static final int FOG_REPORT_ID_FIELD_NUMBER;
    public static final int FOG_REPORT_URL_FIELD_NUMBER;
    private static volatile Parser<MobileCoinAPI$RootIdentity> PARSER;
    public static final int ROOT_ENTROPY_FIELD_NUMBER;
    private ByteString fogAuthoritySpki_ = ByteString.EMPTY;
    private String fogReportId_ = "";
    private String fogReportUrl_ = "";
    private MobileCoinAPI$RootEntropy rootEntropy_;

    private MobileCoinAPI$RootIdentity() {
    }

    public boolean hasRootEntropy() {
        return this.rootEntropy_ != null;
    }

    public MobileCoinAPI$RootEntropy getRootEntropy() {
        MobileCoinAPI$RootEntropy mobileCoinAPI$RootEntropy = this.rootEntropy_;
        return mobileCoinAPI$RootEntropy == null ? MobileCoinAPI$RootEntropy.getDefaultInstance() : mobileCoinAPI$RootEntropy;
    }

    public void setRootEntropy(MobileCoinAPI$RootEntropy mobileCoinAPI$RootEntropy) {
        mobileCoinAPI$RootEntropy.getClass();
        this.rootEntropy_ = mobileCoinAPI$RootEntropy;
    }

    public void setRootEntropy(MobileCoinAPI$RootEntropy.Builder builder) {
        this.rootEntropy_ = builder.build();
    }

    public void mergeRootEntropy(MobileCoinAPI$RootEntropy mobileCoinAPI$RootEntropy) {
        mobileCoinAPI$RootEntropy.getClass();
        MobileCoinAPI$RootEntropy mobileCoinAPI$RootEntropy2 = this.rootEntropy_;
        if (mobileCoinAPI$RootEntropy2 == null || mobileCoinAPI$RootEntropy2 == MobileCoinAPI$RootEntropy.getDefaultInstance()) {
            this.rootEntropy_ = mobileCoinAPI$RootEntropy;
        } else {
            this.rootEntropy_ = MobileCoinAPI$RootEntropy.newBuilder(this.rootEntropy_).mergeFrom((MobileCoinAPI$RootEntropy.Builder) mobileCoinAPI$RootEntropy).buildPartial();
        }
    }

    public void clearRootEntropy() {
        this.rootEntropy_ = null;
    }

    public String getFogReportUrl() {
        return this.fogReportUrl_;
    }

    public ByteString getFogReportUrlBytes() {
        return ByteString.copyFromUtf8(this.fogReportUrl_);
    }

    public void setFogReportUrl(String str) {
        str.getClass();
        this.fogReportUrl_ = str;
    }

    public void clearFogReportUrl() {
        this.fogReportUrl_ = getDefaultInstance().getFogReportUrl();
    }

    public void setFogReportUrlBytes(ByteString byteString) {
        byteString.getClass();
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.fogReportUrl_ = byteString.toStringUtf8();
    }

    public String getFogReportId() {
        return this.fogReportId_;
    }

    public ByteString getFogReportIdBytes() {
        return ByteString.copyFromUtf8(this.fogReportId_);
    }

    public void setFogReportId(String str) {
        str.getClass();
        this.fogReportId_ = str;
    }

    public void clearFogReportId() {
        this.fogReportId_ = getDefaultInstance().getFogReportId();
    }

    public void setFogReportIdBytes(ByteString byteString) {
        byteString.getClass();
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.fogReportId_ = byteString.toStringUtf8();
    }

    public ByteString getFogAuthoritySpki() {
        return this.fogAuthoritySpki_;
    }

    public void setFogAuthoritySpki(ByteString byteString) {
        byteString.getClass();
        this.fogAuthoritySpki_ = byteString;
    }

    public void clearFogAuthoritySpki() {
        this.fogAuthoritySpki_ = getDefaultInstance().getFogAuthoritySpki();
    }

    public static MobileCoinAPI$RootIdentity parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$RootIdentity) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static MobileCoinAPI$RootIdentity parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$RootIdentity) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static MobileCoinAPI$RootIdentity parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$RootIdentity) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static MobileCoinAPI$RootIdentity parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$RootIdentity) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static MobileCoinAPI$RootIdentity parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$RootIdentity) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static MobileCoinAPI$RootIdentity parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$RootIdentity) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static MobileCoinAPI$RootIdentity parseFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$RootIdentity) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$RootIdentity parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$RootIdentity) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$RootIdentity parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$RootIdentity) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$RootIdentity parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$RootIdentity) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$RootIdentity parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (MobileCoinAPI$RootIdentity) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static MobileCoinAPI$RootIdentity parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$RootIdentity) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(MobileCoinAPI$RootIdentity mobileCoinAPI$RootIdentity) {
        return DEFAULT_INSTANCE.createBuilder(mobileCoinAPI$RootIdentity);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<MobileCoinAPI$RootIdentity, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(MobileCoinAPI$1 mobileCoinAPI$1) {
            this();
        }

        private Builder() {
            super(MobileCoinAPI$RootIdentity.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (MobileCoinAPI$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new MobileCoinAPI$RootIdentity();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0004\u0000\u0000\u0001\u0005\u0004\u0000\u0000\u0000\u0001\t\u0002Ȉ\u0003Ȉ\u0005\n", new Object[]{"rootEntropy_", "fogReportUrl_", "fogReportId_", "fogAuthoritySpki_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<MobileCoinAPI$RootIdentity> parser = PARSER;
                if (parser == null) {
                    synchronized (MobileCoinAPI$RootIdentity.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        MobileCoinAPI$RootIdentity mobileCoinAPI$RootIdentity = new MobileCoinAPI$RootIdentity();
        DEFAULT_INSTANCE = mobileCoinAPI$RootIdentity;
        GeneratedMessageLite.registerDefaultInstance(MobileCoinAPI$RootIdentity.class, mobileCoinAPI$RootIdentity);
    }

    public static MobileCoinAPI$RootIdentity getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<MobileCoinAPI$RootIdentity> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
