package com.mobilecoin.api;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$TxOutMembershipElement;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class MobileCoinAPI$TxOutMembershipProof extends GeneratedMessageLite<MobileCoinAPI$TxOutMembershipProof, Builder> implements MobileCoinAPI$TxOutMembershipProofOrBuilder {
    private static final MobileCoinAPI$TxOutMembershipProof DEFAULT_INSTANCE;
    public static final int ELEMENTS_FIELD_NUMBER;
    public static final int HIGHEST_INDEX_FIELD_NUMBER;
    public static final int INDEX_FIELD_NUMBER;
    private static volatile Parser<MobileCoinAPI$TxOutMembershipProof> PARSER;
    private Internal.ProtobufList<MobileCoinAPI$TxOutMembershipElement> elements_ = GeneratedMessageLite.emptyProtobufList();
    private long highestIndex_;
    private long index_;

    private MobileCoinAPI$TxOutMembershipProof() {
    }

    public long getIndex() {
        return this.index_;
    }

    public void setIndex(long j) {
        this.index_ = j;
    }

    public void clearIndex() {
        this.index_ = 0;
    }

    public long getHighestIndex() {
        return this.highestIndex_;
    }

    public void setHighestIndex(long j) {
        this.highestIndex_ = j;
    }

    public void clearHighestIndex() {
        this.highestIndex_ = 0;
    }

    public List<MobileCoinAPI$TxOutMembershipElement> getElementsList() {
        return this.elements_;
    }

    public List<? extends MobileCoinAPI$TxOutMembershipElementOrBuilder> getElementsOrBuilderList() {
        return this.elements_;
    }

    public int getElementsCount() {
        return this.elements_.size();
    }

    public MobileCoinAPI$TxOutMembershipElement getElements(int i) {
        return this.elements_.get(i);
    }

    public MobileCoinAPI$TxOutMembershipElementOrBuilder getElementsOrBuilder(int i) {
        return this.elements_.get(i);
    }

    private void ensureElementsIsMutable() {
        if (!this.elements_.isModifiable()) {
            this.elements_ = GeneratedMessageLite.mutableCopy(this.elements_);
        }
    }

    public void setElements(int i, MobileCoinAPI$TxOutMembershipElement mobileCoinAPI$TxOutMembershipElement) {
        mobileCoinAPI$TxOutMembershipElement.getClass();
        ensureElementsIsMutable();
        this.elements_.set(i, mobileCoinAPI$TxOutMembershipElement);
    }

    public void setElements(int i, MobileCoinAPI$TxOutMembershipElement.Builder builder) {
        ensureElementsIsMutable();
        this.elements_.set(i, builder.build());
    }

    public void addElements(MobileCoinAPI$TxOutMembershipElement mobileCoinAPI$TxOutMembershipElement) {
        mobileCoinAPI$TxOutMembershipElement.getClass();
        ensureElementsIsMutable();
        this.elements_.add(mobileCoinAPI$TxOutMembershipElement);
    }

    public void addElements(int i, MobileCoinAPI$TxOutMembershipElement mobileCoinAPI$TxOutMembershipElement) {
        mobileCoinAPI$TxOutMembershipElement.getClass();
        ensureElementsIsMutable();
        this.elements_.add(i, mobileCoinAPI$TxOutMembershipElement);
    }

    public void addElements(MobileCoinAPI$TxOutMembershipElement.Builder builder) {
        ensureElementsIsMutable();
        this.elements_.add(builder.build());
    }

    public void addElements(int i, MobileCoinAPI$TxOutMembershipElement.Builder builder) {
        ensureElementsIsMutable();
        this.elements_.add(i, builder.build());
    }

    public void addAllElements(Iterable<? extends MobileCoinAPI$TxOutMembershipElement> iterable) {
        ensureElementsIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.elements_);
    }

    public void clearElements() {
        this.elements_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeElements(int i) {
        ensureElementsIsMutable();
        this.elements_.remove(i);
    }

    public static MobileCoinAPI$TxOutMembershipProof parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutMembershipProof) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static MobileCoinAPI$TxOutMembershipProof parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutMembershipProof) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOutMembershipProof parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutMembershipProof) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static MobileCoinAPI$TxOutMembershipProof parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutMembershipProof) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOutMembershipProof parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutMembershipProof) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static MobileCoinAPI$TxOutMembershipProof parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutMembershipProof) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOutMembershipProof parseFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$TxOutMembershipProof) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$TxOutMembershipProof parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$TxOutMembershipProof) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOutMembershipProof parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$TxOutMembershipProof) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$TxOutMembershipProof parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$TxOutMembershipProof) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOutMembershipProof parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (MobileCoinAPI$TxOutMembershipProof) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static MobileCoinAPI$TxOutMembershipProof parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$TxOutMembershipProof) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(MobileCoinAPI$TxOutMembershipProof mobileCoinAPI$TxOutMembershipProof) {
        return DEFAULT_INSTANCE.createBuilder(mobileCoinAPI$TxOutMembershipProof);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<MobileCoinAPI$TxOutMembershipProof, Builder> implements MobileCoinAPI$TxOutMembershipProofOrBuilder {
        /* synthetic */ Builder(MobileCoinAPI$1 mobileCoinAPI$1) {
            this();
        }

        private Builder() {
            super(MobileCoinAPI$TxOutMembershipProof.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (MobileCoinAPI$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new MobileCoinAPI$TxOutMembershipProof();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0001\u0000\u0001\u0003\u0002\u0003\u0003\u001b", new Object[]{"index_", "highestIndex_", "elements_", MobileCoinAPI$TxOutMembershipElement.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<MobileCoinAPI$TxOutMembershipProof> parser = PARSER;
                if (parser == null) {
                    synchronized (MobileCoinAPI$TxOutMembershipProof.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        MobileCoinAPI$TxOutMembershipProof mobileCoinAPI$TxOutMembershipProof = new MobileCoinAPI$TxOutMembershipProof();
        DEFAULT_INSTANCE = mobileCoinAPI$TxOutMembershipProof;
        GeneratedMessageLite.registerDefaultInstance(MobileCoinAPI$TxOutMembershipProof.class, mobileCoinAPI$TxOutMembershipProof);
    }

    public static MobileCoinAPI$TxOutMembershipProof getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<MobileCoinAPI$TxOutMembershipProof> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
