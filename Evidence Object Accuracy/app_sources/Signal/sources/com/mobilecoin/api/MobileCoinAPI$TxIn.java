package com.mobilecoin.api;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$TxOut;
import com.mobilecoin.api.MobileCoinAPI$TxOutMembershipProof;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class MobileCoinAPI$TxIn extends GeneratedMessageLite<MobileCoinAPI$TxIn, Builder> implements MobileCoinAPI$TxInOrBuilder {
    private static final MobileCoinAPI$TxIn DEFAULT_INSTANCE;
    private static volatile Parser<MobileCoinAPI$TxIn> PARSER;
    public static final int PROOFS_FIELD_NUMBER;
    public static final int RING_FIELD_NUMBER;
    private Internal.ProtobufList<MobileCoinAPI$TxOutMembershipProof> proofs_ = GeneratedMessageLite.emptyProtobufList();
    private Internal.ProtobufList<MobileCoinAPI$TxOut> ring_ = GeneratedMessageLite.emptyProtobufList();

    private MobileCoinAPI$TxIn() {
    }

    public List<MobileCoinAPI$TxOut> getRingList() {
        return this.ring_;
    }

    public List<? extends MobileCoinAPI$TxOutOrBuilder> getRingOrBuilderList() {
        return this.ring_;
    }

    public int getRingCount() {
        return this.ring_.size();
    }

    public MobileCoinAPI$TxOut getRing(int i) {
        return this.ring_.get(i);
    }

    public MobileCoinAPI$TxOutOrBuilder getRingOrBuilder(int i) {
        return this.ring_.get(i);
    }

    private void ensureRingIsMutable() {
        if (!this.ring_.isModifiable()) {
            this.ring_ = GeneratedMessageLite.mutableCopy(this.ring_);
        }
    }

    public void setRing(int i, MobileCoinAPI$TxOut mobileCoinAPI$TxOut) {
        mobileCoinAPI$TxOut.getClass();
        ensureRingIsMutable();
        this.ring_.set(i, mobileCoinAPI$TxOut);
    }

    public void setRing(int i, MobileCoinAPI$TxOut.Builder builder) {
        ensureRingIsMutable();
        this.ring_.set(i, builder.build());
    }

    public void addRing(MobileCoinAPI$TxOut mobileCoinAPI$TxOut) {
        mobileCoinAPI$TxOut.getClass();
        ensureRingIsMutable();
        this.ring_.add(mobileCoinAPI$TxOut);
    }

    public void addRing(int i, MobileCoinAPI$TxOut mobileCoinAPI$TxOut) {
        mobileCoinAPI$TxOut.getClass();
        ensureRingIsMutable();
        this.ring_.add(i, mobileCoinAPI$TxOut);
    }

    public void addRing(MobileCoinAPI$TxOut.Builder builder) {
        ensureRingIsMutable();
        this.ring_.add(builder.build());
    }

    public void addRing(int i, MobileCoinAPI$TxOut.Builder builder) {
        ensureRingIsMutable();
        this.ring_.add(i, builder.build());
    }

    public void addAllRing(Iterable<? extends MobileCoinAPI$TxOut> iterable) {
        ensureRingIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.ring_);
    }

    public void clearRing() {
        this.ring_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeRing(int i) {
        ensureRingIsMutable();
        this.ring_.remove(i);
    }

    public List<MobileCoinAPI$TxOutMembershipProof> getProofsList() {
        return this.proofs_;
    }

    public List<? extends MobileCoinAPI$TxOutMembershipProofOrBuilder> getProofsOrBuilderList() {
        return this.proofs_;
    }

    public int getProofsCount() {
        return this.proofs_.size();
    }

    public MobileCoinAPI$TxOutMembershipProof getProofs(int i) {
        return this.proofs_.get(i);
    }

    public MobileCoinAPI$TxOutMembershipProofOrBuilder getProofsOrBuilder(int i) {
        return this.proofs_.get(i);
    }

    private void ensureProofsIsMutable() {
        if (!this.proofs_.isModifiable()) {
            this.proofs_ = GeneratedMessageLite.mutableCopy(this.proofs_);
        }
    }

    public void setProofs(int i, MobileCoinAPI$TxOutMembershipProof mobileCoinAPI$TxOutMembershipProof) {
        mobileCoinAPI$TxOutMembershipProof.getClass();
        ensureProofsIsMutable();
        this.proofs_.set(i, mobileCoinAPI$TxOutMembershipProof);
    }

    public void setProofs(int i, MobileCoinAPI$TxOutMembershipProof.Builder builder) {
        ensureProofsIsMutable();
        this.proofs_.set(i, builder.build());
    }

    public void addProofs(MobileCoinAPI$TxOutMembershipProof mobileCoinAPI$TxOutMembershipProof) {
        mobileCoinAPI$TxOutMembershipProof.getClass();
        ensureProofsIsMutable();
        this.proofs_.add(mobileCoinAPI$TxOutMembershipProof);
    }

    public void addProofs(int i, MobileCoinAPI$TxOutMembershipProof mobileCoinAPI$TxOutMembershipProof) {
        mobileCoinAPI$TxOutMembershipProof.getClass();
        ensureProofsIsMutable();
        this.proofs_.add(i, mobileCoinAPI$TxOutMembershipProof);
    }

    public void addProofs(MobileCoinAPI$TxOutMembershipProof.Builder builder) {
        ensureProofsIsMutable();
        this.proofs_.add(builder.build());
    }

    public void addProofs(int i, MobileCoinAPI$TxOutMembershipProof.Builder builder) {
        ensureProofsIsMutable();
        this.proofs_.add(i, builder.build());
    }

    public void addAllProofs(Iterable<? extends MobileCoinAPI$TxOutMembershipProof> iterable) {
        ensureProofsIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.proofs_);
    }

    public void clearProofs() {
        this.proofs_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeProofs(int i) {
        ensureProofsIsMutable();
        this.proofs_.remove(i);
    }

    public static MobileCoinAPI$TxIn parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxIn) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static MobileCoinAPI$TxIn parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxIn) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxIn parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxIn) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static MobileCoinAPI$TxIn parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxIn) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxIn parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxIn) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static MobileCoinAPI$TxIn parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxIn) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxIn parseFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$TxIn) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$TxIn parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$TxIn) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxIn parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$TxIn) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$TxIn parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$TxIn) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxIn parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (MobileCoinAPI$TxIn) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static MobileCoinAPI$TxIn parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$TxIn) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(MobileCoinAPI$TxIn mobileCoinAPI$TxIn) {
        return DEFAULT_INSTANCE.createBuilder(mobileCoinAPI$TxIn);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<MobileCoinAPI$TxIn, Builder> implements MobileCoinAPI$TxInOrBuilder {
        /* synthetic */ Builder(MobileCoinAPI$1 mobileCoinAPI$1) {
            this();
        }

        private Builder() {
            super(MobileCoinAPI$TxIn.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (MobileCoinAPI$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new MobileCoinAPI$TxIn();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0002\u0000\u0001\u001b\u0002\u001b", new Object[]{"ring_", MobileCoinAPI$TxOut.class, "proofs_", MobileCoinAPI$TxOutMembershipProof.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<MobileCoinAPI$TxIn> parser = PARSER;
                if (parser == null) {
                    synchronized (MobileCoinAPI$TxIn.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        MobileCoinAPI$TxIn mobileCoinAPI$TxIn = new MobileCoinAPI$TxIn();
        DEFAULT_INSTANCE = mobileCoinAPI$TxIn;
        GeneratedMessageLite.registerDefaultInstance(MobileCoinAPI$TxIn.class, mobileCoinAPI$TxIn);
    }

    public static MobileCoinAPI$TxIn getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<MobileCoinAPI$TxIn> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
