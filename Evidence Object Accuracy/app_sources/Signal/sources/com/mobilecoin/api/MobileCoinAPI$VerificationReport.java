package com.mobilecoin.api;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$VerificationSignature;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class MobileCoinAPI$VerificationReport extends GeneratedMessageLite<MobileCoinAPI$VerificationReport, Builder> implements MessageLiteOrBuilder {
    public static final int CHAIN_FIELD_NUMBER;
    private static final MobileCoinAPI$VerificationReport DEFAULT_INSTANCE;
    public static final int HTTP_BODY_FIELD_NUMBER;
    private static volatile Parser<MobileCoinAPI$VerificationReport> PARSER;
    public static final int SIG_FIELD_NUMBER;
    private Internal.ProtobufList<ByteString> chain_ = GeneratedMessageLite.emptyProtobufList();
    private String httpBody_ = "";
    private MobileCoinAPI$VerificationSignature sig_;

    private MobileCoinAPI$VerificationReport() {
    }

    public boolean hasSig() {
        return this.sig_ != null;
    }

    public MobileCoinAPI$VerificationSignature getSig() {
        MobileCoinAPI$VerificationSignature mobileCoinAPI$VerificationSignature = this.sig_;
        return mobileCoinAPI$VerificationSignature == null ? MobileCoinAPI$VerificationSignature.getDefaultInstance() : mobileCoinAPI$VerificationSignature;
    }

    public void setSig(MobileCoinAPI$VerificationSignature mobileCoinAPI$VerificationSignature) {
        mobileCoinAPI$VerificationSignature.getClass();
        this.sig_ = mobileCoinAPI$VerificationSignature;
    }

    public void setSig(MobileCoinAPI$VerificationSignature.Builder builder) {
        this.sig_ = builder.build();
    }

    public void mergeSig(MobileCoinAPI$VerificationSignature mobileCoinAPI$VerificationSignature) {
        mobileCoinAPI$VerificationSignature.getClass();
        MobileCoinAPI$VerificationSignature mobileCoinAPI$VerificationSignature2 = this.sig_;
        if (mobileCoinAPI$VerificationSignature2 == null || mobileCoinAPI$VerificationSignature2 == MobileCoinAPI$VerificationSignature.getDefaultInstance()) {
            this.sig_ = mobileCoinAPI$VerificationSignature;
        } else {
            this.sig_ = MobileCoinAPI$VerificationSignature.newBuilder(this.sig_).mergeFrom((MobileCoinAPI$VerificationSignature.Builder) mobileCoinAPI$VerificationSignature).buildPartial();
        }
    }

    public void clearSig() {
        this.sig_ = null;
    }

    public List<ByteString> getChainList() {
        return this.chain_;
    }

    public int getChainCount() {
        return this.chain_.size();
    }

    public ByteString getChain(int i) {
        return this.chain_.get(i);
    }

    private void ensureChainIsMutable() {
        if (!this.chain_.isModifiable()) {
            this.chain_ = GeneratedMessageLite.mutableCopy(this.chain_);
        }
    }

    public void setChain(int i, ByteString byteString) {
        byteString.getClass();
        ensureChainIsMutable();
        this.chain_.set(i, byteString);
    }

    public void addChain(ByteString byteString) {
        byteString.getClass();
        ensureChainIsMutable();
        this.chain_.add(byteString);
    }

    public void addAllChain(Iterable<? extends ByteString> iterable) {
        ensureChainIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.chain_);
    }

    public void clearChain() {
        this.chain_ = GeneratedMessageLite.emptyProtobufList();
    }

    public String getHttpBody() {
        return this.httpBody_;
    }

    public ByteString getHttpBodyBytes() {
        return ByteString.copyFromUtf8(this.httpBody_);
    }

    public void setHttpBody(String str) {
        str.getClass();
        this.httpBody_ = str;
    }

    public void clearHttpBody() {
        this.httpBody_ = getDefaultInstance().getHttpBody();
    }

    public void setHttpBodyBytes(ByteString byteString) {
        byteString.getClass();
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.httpBody_ = byteString.toStringUtf8();
    }

    public static MobileCoinAPI$VerificationReport parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$VerificationReport) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static MobileCoinAPI$VerificationReport parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$VerificationReport) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static MobileCoinAPI$VerificationReport parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$VerificationReport) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static MobileCoinAPI$VerificationReport parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$VerificationReport) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static MobileCoinAPI$VerificationReport parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$VerificationReport) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static MobileCoinAPI$VerificationReport parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$VerificationReport) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static MobileCoinAPI$VerificationReport parseFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$VerificationReport) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$VerificationReport parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$VerificationReport) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$VerificationReport parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$VerificationReport) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$VerificationReport parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$VerificationReport) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$VerificationReport parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (MobileCoinAPI$VerificationReport) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static MobileCoinAPI$VerificationReport parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$VerificationReport) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(MobileCoinAPI$VerificationReport mobileCoinAPI$VerificationReport) {
        return DEFAULT_INSTANCE.createBuilder(mobileCoinAPI$VerificationReport);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<MobileCoinAPI$VerificationReport, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(MobileCoinAPI$1 mobileCoinAPI$1) {
            this();
        }

        private Builder() {
            super(MobileCoinAPI$VerificationReport.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (MobileCoinAPI$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new MobileCoinAPI$VerificationReport();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0001\u0000\u0001\t\u0002\u001c\u0003Ȉ", new Object[]{"sig_", "chain_", "httpBody_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<MobileCoinAPI$VerificationReport> parser = PARSER;
                if (parser == null) {
                    synchronized (MobileCoinAPI$VerificationReport.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        MobileCoinAPI$VerificationReport mobileCoinAPI$VerificationReport = new MobileCoinAPI$VerificationReport();
        DEFAULT_INSTANCE = mobileCoinAPI$VerificationReport;
        GeneratedMessageLite.registerDefaultInstance(MobileCoinAPI$VerificationReport.class, mobileCoinAPI$VerificationReport);
    }

    public static MobileCoinAPI$VerificationReport getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<MobileCoinAPI$VerificationReport> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
