package com.mobilecoin.api;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class MobileCoinAPI$TxOutConfirmationNumber extends GeneratedMessageLite<MobileCoinAPI$TxOutConfirmationNumber, Builder> implements MessageLiteOrBuilder {
    private static final MobileCoinAPI$TxOutConfirmationNumber DEFAULT_INSTANCE;
    public static final int HASH_FIELD_NUMBER;
    private static volatile Parser<MobileCoinAPI$TxOutConfirmationNumber> PARSER;
    private ByteString hash_ = ByteString.EMPTY;

    private MobileCoinAPI$TxOutConfirmationNumber() {
    }

    public ByteString getHash() {
        return this.hash_;
    }

    public void setHash(ByteString byteString) {
        byteString.getClass();
        this.hash_ = byteString;
    }

    public void clearHash() {
        this.hash_ = getDefaultInstance().getHash();
    }

    public static MobileCoinAPI$TxOutConfirmationNumber parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutConfirmationNumber) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static MobileCoinAPI$TxOutConfirmationNumber parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutConfirmationNumber) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOutConfirmationNumber parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutConfirmationNumber) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static MobileCoinAPI$TxOutConfirmationNumber parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutConfirmationNumber) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOutConfirmationNumber parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutConfirmationNumber) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static MobileCoinAPI$TxOutConfirmationNumber parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutConfirmationNumber) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOutConfirmationNumber parseFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$TxOutConfirmationNumber) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$TxOutConfirmationNumber parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$TxOutConfirmationNumber) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOutConfirmationNumber parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$TxOutConfirmationNumber) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$TxOutConfirmationNumber parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$TxOutConfirmationNumber) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOutConfirmationNumber parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (MobileCoinAPI$TxOutConfirmationNumber) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static MobileCoinAPI$TxOutConfirmationNumber parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$TxOutConfirmationNumber) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(MobileCoinAPI$TxOutConfirmationNumber mobileCoinAPI$TxOutConfirmationNumber) {
        return DEFAULT_INSTANCE.createBuilder(mobileCoinAPI$TxOutConfirmationNumber);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<MobileCoinAPI$TxOutConfirmationNumber, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(MobileCoinAPI$1 mobileCoinAPI$1) {
            this();
        }

        private Builder() {
            super(MobileCoinAPI$TxOutConfirmationNumber.DEFAULT_INSTANCE);
        }

        public Builder setHash(ByteString byteString) {
            copyOnWrite();
            ((MobileCoinAPI$TxOutConfirmationNumber) this.instance).setHash(byteString);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (MobileCoinAPI$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new MobileCoinAPI$TxOutConfirmationNumber();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\n", new Object[]{"hash_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<MobileCoinAPI$TxOutConfirmationNumber> parser = PARSER;
                if (parser == null) {
                    synchronized (MobileCoinAPI$TxOutConfirmationNumber.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        MobileCoinAPI$TxOutConfirmationNumber mobileCoinAPI$TxOutConfirmationNumber = new MobileCoinAPI$TxOutConfirmationNumber();
        DEFAULT_INSTANCE = mobileCoinAPI$TxOutConfirmationNumber;
        GeneratedMessageLite.registerDefaultInstance(MobileCoinAPI$TxOutConfirmationNumber.class, mobileCoinAPI$TxOutConfirmationNumber);
    }

    public static MobileCoinAPI$TxOutConfirmationNumber getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<MobileCoinAPI$TxOutConfirmationNumber> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
