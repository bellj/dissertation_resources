package com.mobilecoin.api;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$CompressedRistretto;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class MobileCoinAPI$Amount extends GeneratedMessageLite<MobileCoinAPI$Amount, Builder> implements MessageLiteOrBuilder {
    public static final int COMMITMENT_FIELD_NUMBER;
    private static final MobileCoinAPI$Amount DEFAULT_INSTANCE;
    public static final int MASKED_VALUE_FIELD_NUMBER;
    private static volatile Parser<MobileCoinAPI$Amount> PARSER;
    private MobileCoinAPI$CompressedRistretto commitment_;
    private long maskedValue_;

    private MobileCoinAPI$Amount() {
    }

    public boolean hasCommitment() {
        return this.commitment_ != null;
    }

    public MobileCoinAPI$CompressedRistretto getCommitment() {
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto = this.commitment_;
        return mobileCoinAPI$CompressedRistretto == null ? MobileCoinAPI$CompressedRistretto.getDefaultInstance() : mobileCoinAPI$CompressedRistretto;
    }

    public void setCommitment(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        this.commitment_ = mobileCoinAPI$CompressedRistretto;
    }

    public void setCommitment(MobileCoinAPI$CompressedRistretto.Builder builder) {
        this.commitment_ = builder.build();
    }

    public void mergeCommitment(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto2 = this.commitment_;
        if (mobileCoinAPI$CompressedRistretto2 == null || mobileCoinAPI$CompressedRistretto2 == MobileCoinAPI$CompressedRistretto.getDefaultInstance()) {
            this.commitment_ = mobileCoinAPI$CompressedRistretto;
        } else {
            this.commitment_ = MobileCoinAPI$CompressedRistretto.newBuilder(this.commitment_).mergeFrom((MobileCoinAPI$CompressedRistretto.Builder) mobileCoinAPI$CompressedRistretto).buildPartial();
        }
    }

    public void clearCommitment() {
        this.commitment_ = null;
    }

    public long getMaskedValue() {
        return this.maskedValue_;
    }

    public void setMaskedValue(long j) {
        this.maskedValue_ = j;
    }

    public void clearMaskedValue() {
        this.maskedValue_ = 0;
    }

    public static MobileCoinAPI$Amount parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$Amount) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static MobileCoinAPI$Amount parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$Amount) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static MobileCoinAPI$Amount parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$Amount) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static MobileCoinAPI$Amount parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$Amount) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static MobileCoinAPI$Amount parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$Amount) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static MobileCoinAPI$Amount parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$Amount) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static MobileCoinAPI$Amount parseFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$Amount) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$Amount parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$Amount) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$Amount parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$Amount) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$Amount parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$Amount) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$Amount parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (MobileCoinAPI$Amount) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static MobileCoinAPI$Amount parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$Amount) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(MobileCoinAPI$Amount mobileCoinAPI$Amount) {
        return DEFAULT_INSTANCE.createBuilder(mobileCoinAPI$Amount);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<MobileCoinAPI$Amount, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(MobileCoinAPI$1 mobileCoinAPI$1) {
            this();
        }

        private Builder() {
            super(MobileCoinAPI$Amount.DEFAULT_INSTANCE);
        }

        public Builder setCommitment(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
            copyOnWrite();
            ((MobileCoinAPI$Amount) this.instance).setCommitment(mobileCoinAPI$CompressedRistretto);
            return this;
        }

        public Builder setMaskedValue(long j) {
            copyOnWrite();
            ((MobileCoinAPI$Amount) this.instance).setMaskedValue(j);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (MobileCoinAPI$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new MobileCoinAPI$Amount();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\t\u0002\u0005", new Object[]{"commitment_", "maskedValue_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<MobileCoinAPI$Amount> parser = PARSER;
                if (parser == null) {
                    synchronized (MobileCoinAPI$Amount.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        MobileCoinAPI$Amount mobileCoinAPI$Amount = new MobileCoinAPI$Amount();
        DEFAULT_INSTANCE = mobileCoinAPI$Amount;
        GeneratedMessageLite.registerDefaultInstance(MobileCoinAPI$Amount.class, mobileCoinAPI$Amount);
    }

    public static MobileCoinAPI$Amount getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<MobileCoinAPI$Amount> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
