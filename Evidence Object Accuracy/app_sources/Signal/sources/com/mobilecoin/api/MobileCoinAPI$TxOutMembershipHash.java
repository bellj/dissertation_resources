package com.mobilecoin.api;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class MobileCoinAPI$TxOutMembershipHash extends GeneratedMessageLite<MobileCoinAPI$TxOutMembershipHash, Builder> implements MessageLiteOrBuilder {
    public static final int DATA_FIELD_NUMBER;
    private static final MobileCoinAPI$TxOutMembershipHash DEFAULT_INSTANCE;
    private static volatile Parser<MobileCoinAPI$TxOutMembershipHash> PARSER;
    private ByteString data_ = ByteString.EMPTY;

    private MobileCoinAPI$TxOutMembershipHash() {
    }

    public ByteString getData() {
        return this.data_;
    }

    public void setData(ByteString byteString) {
        byteString.getClass();
        this.data_ = byteString;
    }

    public void clearData() {
        this.data_ = getDefaultInstance().getData();
    }

    public static MobileCoinAPI$TxOutMembershipHash parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutMembershipHash) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static MobileCoinAPI$TxOutMembershipHash parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutMembershipHash) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOutMembershipHash parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutMembershipHash) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static MobileCoinAPI$TxOutMembershipHash parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutMembershipHash) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOutMembershipHash parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutMembershipHash) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static MobileCoinAPI$TxOutMembershipHash parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutMembershipHash) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOutMembershipHash parseFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$TxOutMembershipHash) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$TxOutMembershipHash parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$TxOutMembershipHash) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOutMembershipHash parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$TxOutMembershipHash) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$TxOutMembershipHash parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$TxOutMembershipHash) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOutMembershipHash parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (MobileCoinAPI$TxOutMembershipHash) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static MobileCoinAPI$TxOutMembershipHash parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$TxOutMembershipHash) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(MobileCoinAPI$TxOutMembershipHash mobileCoinAPI$TxOutMembershipHash) {
        return DEFAULT_INSTANCE.createBuilder(mobileCoinAPI$TxOutMembershipHash);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<MobileCoinAPI$TxOutMembershipHash, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(MobileCoinAPI$1 mobileCoinAPI$1) {
            this();
        }

        private Builder() {
            super(MobileCoinAPI$TxOutMembershipHash.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (MobileCoinAPI$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new MobileCoinAPI$TxOutMembershipHash();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\n", new Object[]{"data_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<MobileCoinAPI$TxOutMembershipHash> parser = PARSER;
                if (parser == null) {
                    synchronized (MobileCoinAPI$TxOutMembershipHash.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        MobileCoinAPI$TxOutMembershipHash mobileCoinAPI$TxOutMembershipHash = new MobileCoinAPI$TxOutMembershipHash();
        DEFAULT_INSTANCE = mobileCoinAPI$TxOutMembershipHash;
        GeneratedMessageLite.registerDefaultInstance(MobileCoinAPI$TxOutMembershipHash.class, mobileCoinAPI$TxOutMembershipHash);
    }

    public static MobileCoinAPI$TxOutMembershipHash getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<MobileCoinAPI$TxOutMembershipHash> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
