package com.mobilecoin.api;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$Amount;
import com.mobilecoin.api.MobileCoinAPI$CompressedRistretto;
import com.mobilecoin.api.MobileCoinAPI$EncryptedFogHint;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class MobileCoinAPI$TxOut extends GeneratedMessageLite<MobileCoinAPI$TxOut, Builder> implements MobileCoinAPI$TxOutOrBuilder {
    public static final int AMOUNT_FIELD_NUMBER;
    private static final MobileCoinAPI$TxOut DEFAULT_INSTANCE;
    public static final int E_FOG_HINT_FIELD_NUMBER;
    private static volatile Parser<MobileCoinAPI$TxOut> PARSER;
    public static final int PUBLIC_KEY_FIELD_NUMBER;
    public static final int TARGET_KEY_FIELD_NUMBER;
    private MobileCoinAPI$Amount amount_;
    private MobileCoinAPI$EncryptedFogHint eFogHint_;
    private MobileCoinAPI$CompressedRistretto publicKey_;
    private MobileCoinAPI$CompressedRistretto targetKey_;

    private MobileCoinAPI$TxOut() {
    }

    public boolean hasAmount() {
        return this.amount_ != null;
    }

    public MobileCoinAPI$Amount getAmount() {
        MobileCoinAPI$Amount mobileCoinAPI$Amount = this.amount_;
        return mobileCoinAPI$Amount == null ? MobileCoinAPI$Amount.getDefaultInstance() : mobileCoinAPI$Amount;
    }

    public void setAmount(MobileCoinAPI$Amount mobileCoinAPI$Amount) {
        mobileCoinAPI$Amount.getClass();
        this.amount_ = mobileCoinAPI$Amount;
    }

    public void setAmount(MobileCoinAPI$Amount.Builder builder) {
        this.amount_ = builder.build();
    }

    public void mergeAmount(MobileCoinAPI$Amount mobileCoinAPI$Amount) {
        mobileCoinAPI$Amount.getClass();
        MobileCoinAPI$Amount mobileCoinAPI$Amount2 = this.amount_;
        if (mobileCoinAPI$Amount2 == null || mobileCoinAPI$Amount2 == MobileCoinAPI$Amount.getDefaultInstance()) {
            this.amount_ = mobileCoinAPI$Amount;
        } else {
            this.amount_ = MobileCoinAPI$Amount.newBuilder(this.amount_).mergeFrom((MobileCoinAPI$Amount.Builder) mobileCoinAPI$Amount).buildPartial();
        }
    }

    public void clearAmount() {
        this.amount_ = null;
    }

    public boolean hasTargetKey() {
        return this.targetKey_ != null;
    }

    public MobileCoinAPI$CompressedRistretto getTargetKey() {
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto = this.targetKey_;
        return mobileCoinAPI$CompressedRistretto == null ? MobileCoinAPI$CompressedRistretto.getDefaultInstance() : mobileCoinAPI$CompressedRistretto;
    }

    public void setTargetKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        this.targetKey_ = mobileCoinAPI$CompressedRistretto;
    }

    public void setTargetKey(MobileCoinAPI$CompressedRistretto.Builder builder) {
        this.targetKey_ = builder.build();
    }

    public void mergeTargetKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto2 = this.targetKey_;
        if (mobileCoinAPI$CompressedRistretto2 == null || mobileCoinAPI$CompressedRistretto2 == MobileCoinAPI$CompressedRistretto.getDefaultInstance()) {
            this.targetKey_ = mobileCoinAPI$CompressedRistretto;
        } else {
            this.targetKey_ = MobileCoinAPI$CompressedRistretto.newBuilder(this.targetKey_).mergeFrom((MobileCoinAPI$CompressedRistretto.Builder) mobileCoinAPI$CompressedRistretto).buildPartial();
        }
    }

    public void clearTargetKey() {
        this.targetKey_ = null;
    }

    public boolean hasPublicKey() {
        return this.publicKey_ != null;
    }

    public MobileCoinAPI$CompressedRistretto getPublicKey() {
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto = this.publicKey_;
        return mobileCoinAPI$CompressedRistretto == null ? MobileCoinAPI$CompressedRistretto.getDefaultInstance() : mobileCoinAPI$CompressedRistretto;
    }

    public void setPublicKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        this.publicKey_ = mobileCoinAPI$CompressedRistretto;
    }

    public void setPublicKey(MobileCoinAPI$CompressedRistretto.Builder builder) {
        this.publicKey_ = builder.build();
    }

    public void mergePublicKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto2 = this.publicKey_;
        if (mobileCoinAPI$CompressedRistretto2 == null || mobileCoinAPI$CompressedRistretto2 == MobileCoinAPI$CompressedRistretto.getDefaultInstance()) {
            this.publicKey_ = mobileCoinAPI$CompressedRistretto;
        } else {
            this.publicKey_ = MobileCoinAPI$CompressedRistretto.newBuilder(this.publicKey_).mergeFrom((MobileCoinAPI$CompressedRistretto.Builder) mobileCoinAPI$CompressedRistretto).buildPartial();
        }
    }

    public void clearPublicKey() {
        this.publicKey_ = null;
    }

    public boolean hasEFogHint() {
        return this.eFogHint_ != null;
    }

    public MobileCoinAPI$EncryptedFogHint getEFogHint() {
        MobileCoinAPI$EncryptedFogHint mobileCoinAPI$EncryptedFogHint = this.eFogHint_;
        return mobileCoinAPI$EncryptedFogHint == null ? MobileCoinAPI$EncryptedFogHint.getDefaultInstance() : mobileCoinAPI$EncryptedFogHint;
    }

    public void setEFogHint(MobileCoinAPI$EncryptedFogHint mobileCoinAPI$EncryptedFogHint) {
        mobileCoinAPI$EncryptedFogHint.getClass();
        this.eFogHint_ = mobileCoinAPI$EncryptedFogHint;
    }

    public void setEFogHint(MobileCoinAPI$EncryptedFogHint.Builder builder) {
        this.eFogHint_ = builder.build();
    }

    public void mergeEFogHint(MobileCoinAPI$EncryptedFogHint mobileCoinAPI$EncryptedFogHint) {
        mobileCoinAPI$EncryptedFogHint.getClass();
        MobileCoinAPI$EncryptedFogHint mobileCoinAPI$EncryptedFogHint2 = this.eFogHint_;
        if (mobileCoinAPI$EncryptedFogHint2 == null || mobileCoinAPI$EncryptedFogHint2 == MobileCoinAPI$EncryptedFogHint.getDefaultInstance()) {
            this.eFogHint_ = mobileCoinAPI$EncryptedFogHint;
        } else {
            this.eFogHint_ = MobileCoinAPI$EncryptedFogHint.newBuilder(this.eFogHint_).mergeFrom((MobileCoinAPI$EncryptedFogHint.Builder) mobileCoinAPI$EncryptedFogHint).buildPartial();
        }
    }

    public void clearEFogHint() {
        this.eFogHint_ = null;
    }

    public static MobileCoinAPI$TxOut parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOut) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static MobileCoinAPI$TxOut parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOut) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOut parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOut) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static MobileCoinAPI$TxOut parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOut) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOut parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOut) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static MobileCoinAPI$TxOut parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOut) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOut parseFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$TxOut) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$TxOut parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$TxOut) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOut parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$TxOut) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$TxOut parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$TxOut) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOut parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (MobileCoinAPI$TxOut) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static MobileCoinAPI$TxOut parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$TxOut) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(MobileCoinAPI$TxOut mobileCoinAPI$TxOut) {
        return DEFAULT_INSTANCE.createBuilder(mobileCoinAPI$TxOut);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<MobileCoinAPI$TxOut, Builder> implements MobileCoinAPI$TxOutOrBuilder {
        /* synthetic */ Builder(MobileCoinAPI$1 mobileCoinAPI$1) {
            this();
        }

        private Builder() {
            super(MobileCoinAPI$TxOut.DEFAULT_INSTANCE);
        }

        public Builder setAmount(MobileCoinAPI$Amount mobileCoinAPI$Amount) {
            copyOnWrite();
            ((MobileCoinAPI$TxOut) this.instance).setAmount(mobileCoinAPI$Amount);
            return this;
        }

        public Builder setTargetKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
            copyOnWrite();
            ((MobileCoinAPI$TxOut) this.instance).setTargetKey(mobileCoinAPI$CompressedRistretto);
            return this;
        }

        public Builder setPublicKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
            copyOnWrite();
            ((MobileCoinAPI$TxOut) this.instance).setPublicKey(mobileCoinAPI$CompressedRistretto);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (MobileCoinAPI$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new MobileCoinAPI$TxOut();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0000\u0000\u0001\t\u0002\t\u0003\t\u0004\t", new Object[]{"amount_", "targetKey_", "publicKey_", "eFogHint_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<MobileCoinAPI$TxOut> parser = PARSER;
                if (parser == null) {
                    synchronized (MobileCoinAPI$TxOut.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        MobileCoinAPI$TxOut mobileCoinAPI$TxOut = new MobileCoinAPI$TxOut();
        DEFAULT_INSTANCE = mobileCoinAPI$TxOut;
        GeneratedMessageLite.registerDefaultInstance(MobileCoinAPI$TxOut.class, mobileCoinAPI$TxOut);
    }

    public static MobileCoinAPI$TxOut getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<MobileCoinAPI$TxOut> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
