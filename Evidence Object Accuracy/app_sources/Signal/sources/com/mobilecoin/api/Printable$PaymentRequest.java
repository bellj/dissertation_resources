package com.mobilecoin.api;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$PublicAddress;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class Printable$PaymentRequest extends GeneratedMessageLite<Printable$PaymentRequest, Builder> implements MessageLiteOrBuilder {
    private static final Printable$PaymentRequest DEFAULT_INSTANCE;
    public static final int MEMO_FIELD_NUMBER;
    private static volatile Parser<Printable$PaymentRequest> PARSER;
    public static final int PUBLIC_ADDRESS_FIELD_NUMBER;
    public static final int VALUE_FIELD_NUMBER;
    private String memo_ = "";
    private MobileCoinAPI$PublicAddress publicAddress_;
    private long value_;

    private Printable$PaymentRequest() {
    }

    public boolean hasPublicAddress() {
        return this.publicAddress_ != null;
    }

    public MobileCoinAPI$PublicAddress getPublicAddress() {
        MobileCoinAPI$PublicAddress mobileCoinAPI$PublicAddress = this.publicAddress_;
        return mobileCoinAPI$PublicAddress == null ? MobileCoinAPI$PublicAddress.getDefaultInstance() : mobileCoinAPI$PublicAddress;
    }

    public void setPublicAddress(MobileCoinAPI$PublicAddress mobileCoinAPI$PublicAddress) {
        mobileCoinAPI$PublicAddress.getClass();
        this.publicAddress_ = mobileCoinAPI$PublicAddress;
    }

    public void setPublicAddress(MobileCoinAPI$PublicAddress.Builder builder) {
        this.publicAddress_ = builder.build();
    }

    public void mergePublicAddress(MobileCoinAPI$PublicAddress mobileCoinAPI$PublicAddress) {
        mobileCoinAPI$PublicAddress.getClass();
        MobileCoinAPI$PublicAddress mobileCoinAPI$PublicAddress2 = this.publicAddress_;
        if (mobileCoinAPI$PublicAddress2 == null || mobileCoinAPI$PublicAddress2 == MobileCoinAPI$PublicAddress.getDefaultInstance()) {
            this.publicAddress_ = mobileCoinAPI$PublicAddress;
        } else {
            this.publicAddress_ = MobileCoinAPI$PublicAddress.newBuilder(this.publicAddress_).mergeFrom((MobileCoinAPI$PublicAddress.Builder) mobileCoinAPI$PublicAddress).buildPartial();
        }
    }

    public void clearPublicAddress() {
        this.publicAddress_ = null;
    }

    public long getValue() {
        return this.value_;
    }

    public void setValue(long j) {
        this.value_ = j;
    }

    public void clearValue() {
        this.value_ = 0;
    }

    public String getMemo() {
        return this.memo_;
    }

    public ByteString getMemoBytes() {
        return ByteString.copyFromUtf8(this.memo_);
    }

    public void setMemo(String str) {
        str.getClass();
        this.memo_ = str;
    }

    public void clearMemo() {
        this.memo_ = getDefaultInstance().getMemo();
    }

    public void setMemoBytes(ByteString byteString) {
        byteString.getClass();
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.memo_ = byteString.toStringUtf8();
    }

    public static Printable$PaymentRequest parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Printable$PaymentRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Printable$PaymentRequest parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Printable$PaymentRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Printable$PaymentRequest parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Printable$PaymentRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Printable$PaymentRequest parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Printable$PaymentRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Printable$PaymentRequest parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Printable$PaymentRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Printable$PaymentRequest parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Printable$PaymentRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Printable$PaymentRequest parseFrom(InputStream inputStream) throws IOException {
        return (Printable$PaymentRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Printable$PaymentRequest parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Printable$PaymentRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Printable$PaymentRequest parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Printable$PaymentRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Printable$PaymentRequest parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Printable$PaymentRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Printable$PaymentRequest parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Printable$PaymentRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Printable$PaymentRequest parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Printable$PaymentRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Printable$PaymentRequest printable$PaymentRequest) {
        return DEFAULT_INSTANCE.createBuilder(printable$PaymentRequest);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Printable$PaymentRequest, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(Printable$1 printable$1) {
            this();
        }

        private Builder() {
            super(Printable$PaymentRequest.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Printable$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Printable$PaymentRequest();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\t\u0002\u0003\u0003Ȉ", new Object[]{"publicAddress_", "value_", "memo_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Printable$PaymentRequest> parser = PARSER;
                if (parser == null) {
                    synchronized (Printable$PaymentRequest.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Printable$PaymentRequest printable$PaymentRequest = new Printable$PaymentRequest();
        DEFAULT_INSTANCE = printable$PaymentRequest;
        GeneratedMessageLite.registerDefaultInstance(Printable$PaymentRequest.class, printable$PaymentRequest);
    }

    public static Printable$PaymentRequest getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Printable$PaymentRequest> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
