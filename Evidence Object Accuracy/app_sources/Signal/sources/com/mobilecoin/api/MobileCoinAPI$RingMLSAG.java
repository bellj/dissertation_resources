package com.mobilecoin.api;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$CurveScalar;
import com.mobilecoin.api.MobileCoinAPI$KeyImage;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class MobileCoinAPI$RingMLSAG extends GeneratedMessageLite<MobileCoinAPI$RingMLSAG, Builder> implements MobileCoinAPI$RingMLSAGOrBuilder {
    public static final int C_ZERO_FIELD_NUMBER;
    private static final MobileCoinAPI$RingMLSAG DEFAULT_INSTANCE;
    public static final int KEY_IMAGE_FIELD_NUMBER;
    private static volatile Parser<MobileCoinAPI$RingMLSAG> PARSER;
    public static final int RESPONSES_FIELD_NUMBER;
    private MobileCoinAPI$CurveScalar cZero_;
    private MobileCoinAPI$KeyImage keyImage_;
    private Internal.ProtobufList<MobileCoinAPI$CurveScalar> responses_ = GeneratedMessageLite.emptyProtobufList();

    private MobileCoinAPI$RingMLSAG() {
    }

    public boolean hasCZero() {
        return this.cZero_ != null;
    }

    public MobileCoinAPI$CurveScalar getCZero() {
        MobileCoinAPI$CurveScalar mobileCoinAPI$CurveScalar = this.cZero_;
        return mobileCoinAPI$CurveScalar == null ? MobileCoinAPI$CurveScalar.getDefaultInstance() : mobileCoinAPI$CurveScalar;
    }

    public void setCZero(MobileCoinAPI$CurveScalar mobileCoinAPI$CurveScalar) {
        mobileCoinAPI$CurveScalar.getClass();
        this.cZero_ = mobileCoinAPI$CurveScalar;
    }

    public void setCZero(MobileCoinAPI$CurveScalar.Builder builder) {
        this.cZero_ = builder.build();
    }

    public void mergeCZero(MobileCoinAPI$CurveScalar mobileCoinAPI$CurveScalar) {
        mobileCoinAPI$CurveScalar.getClass();
        MobileCoinAPI$CurveScalar mobileCoinAPI$CurveScalar2 = this.cZero_;
        if (mobileCoinAPI$CurveScalar2 == null || mobileCoinAPI$CurveScalar2 == MobileCoinAPI$CurveScalar.getDefaultInstance()) {
            this.cZero_ = mobileCoinAPI$CurveScalar;
        } else {
            this.cZero_ = MobileCoinAPI$CurveScalar.newBuilder(this.cZero_).mergeFrom((MobileCoinAPI$CurveScalar.Builder) mobileCoinAPI$CurveScalar).buildPartial();
        }
    }

    public void clearCZero() {
        this.cZero_ = null;
    }

    public List<MobileCoinAPI$CurveScalar> getResponsesList() {
        return this.responses_;
    }

    public List<? extends MobileCoinAPI$CurveScalarOrBuilder> getResponsesOrBuilderList() {
        return this.responses_;
    }

    public int getResponsesCount() {
        return this.responses_.size();
    }

    public MobileCoinAPI$CurveScalar getResponses(int i) {
        return this.responses_.get(i);
    }

    public MobileCoinAPI$CurveScalarOrBuilder getResponsesOrBuilder(int i) {
        return this.responses_.get(i);
    }

    private void ensureResponsesIsMutable() {
        if (!this.responses_.isModifiable()) {
            this.responses_ = GeneratedMessageLite.mutableCopy(this.responses_);
        }
    }

    public void setResponses(int i, MobileCoinAPI$CurveScalar mobileCoinAPI$CurveScalar) {
        mobileCoinAPI$CurveScalar.getClass();
        ensureResponsesIsMutable();
        this.responses_.set(i, mobileCoinAPI$CurveScalar);
    }

    public void setResponses(int i, MobileCoinAPI$CurveScalar.Builder builder) {
        ensureResponsesIsMutable();
        this.responses_.set(i, builder.build());
    }

    public void addResponses(MobileCoinAPI$CurveScalar mobileCoinAPI$CurveScalar) {
        mobileCoinAPI$CurveScalar.getClass();
        ensureResponsesIsMutable();
        this.responses_.add(mobileCoinAPI$CurveScalar);
    }

    public void addResponses(int i, MobileCoinAPI$CurveScalar mobileCoinAPI$CurveScalar) {
        mobileCoinAPI$CurveScalar.getClass();
        ensureResponsesIsMutable();
        this.responses_.add(i, mobileCoinAPI$CurveScalar);
    }

    public void addResponses(MobileCoinAPI$CurveScalar.Builder builder) {
        ensureResponsesIsMutable();
        this.responses_.add(builder.build());
    }

    public void addResponses(int i, MobileCoinAPI$CurveScalar.Builder builder) {
        ensureResponsesIsMutable();
        this.responses_.add(i, builder.build());
    }

    public void addAllResponses(Iterable<? extends MobileCoinAPI$CurveScalar> iterable) {
        ensureResponsesIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.responses_);
    }

    public void clearResponses() {
        this.responses_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeResponses(int i) {
        ensureResponsesIsMutable();
        this.responses_.remove(i);
    }

    public boolean hasKeyImage() {
        return this.keyImage_ != null;
    }

    public MobileCoinAPI$KeyImage getKeyImage() {
        MobileCoinAPI$KeyImage mobileCoinAPI$KeyImage = this.keyImage_;
        return mobileCoinAPI$KeyImage == null ? MobileCoinAPI$KeyImage.getDefaultInstance() : mobileCoinAPI$KeyImage;
    }

    public void setKeyImage(MobileCoinAPI$KeyImage mobileCoinAPI$KeyImage) {
        mobileCoinAPI$KeyImage.getClass();
        this.keyImage_ = mobileCoinAPI$KeyImage;
    }

    public void setKeyImage(MobileCoinAPI$KeyImage.Builder builder) {
        this.keyImage_ = builder.build();
    }

    public void mergeKeyImage(MobileCoinAPI$KeyImage mobileCoinAPI$KeyImage) {
        mobileCoinAPI$KeyImage.getClass();
        MobileCoinAPI$KeyImage mobileCoinAPI$KeyImage2 = this.keyImage_;
        if (mobileCoinAPI$KeyImage2 == null || mobileCoinAPI$KeyImage2 == MobileCoinAPI$KeyImage.getDefaultInstance()) {
            this.keyImage_ = mobileCoinAPI$KeyImage;
        } else {
            this.keyImage_ = MobileCoinAPI$KeyImage.newBuilder(this.keyImage_).mergeFrom((MobileCoinAPI$KeyImage.Builder) mobileCoinAPI$KeyImage).buildPartial();
        }
    }

    public void clearKeyImage() {
        this.keyImage_ = null;
    }

    public static MobileCoinAPI$RingMLSAG parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$RingMLSAG) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static MobileCoinAPI$RingMLSAG parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$RingMLSAG) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static MobileCoinAPI$RingMLSAG parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$RingMLSAG) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static MobileCoinAPI$RingMLSAG parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$RingMLSAG) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static MobileCoinAPI$RingMLSAG parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$RingMLSAG) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static MobileCoinAPI$RingMLSAG parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$RingMLSAG) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static MobileCoinAPI$RingMLSAG parseFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$RingMLSAG) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$RingMLSAG parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$RingMLSAG) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$RingMLSAG parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$RingMLSAG) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$RingMLSAG parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$RingMLSAG) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$RingMLSAG parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (MobileCoinAPI$RingMLSAG) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static MobileCoinAPI$RingMLSAG parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$RingMLSAG) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(MobileCoinAPI$RingMLSAG mobileCoinAPI$RingMLSAG) {
        return DEFAULT_INSTANCE.createBuilder(mobileCoinAPI$RingMLSAG);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<MobileCoinAPI$RingMLSAG, Builder> implements MobileCoinAPI$RingMLSAGOrBuilder {
        /* synthetic */ Builder(MobileCoinAPI$1 mobileCoinAPI$1) {
            this();
        }

        private Builder() {
            super(MobileCoinAPI$RingMLSAG.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (MobileCoinAPI$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new MobileCoinAPI$RingMLSAG();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0001\u0000\u0001\t\u0002\u001b\u0003\t", new Object[]{"cZero_", "responses_", MobileCoinAPI$CurveScalar.class, "keyImage_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<MobileCoinAPI$RingMLSAG> parser = PARSER;
                if (parser == null) {
                    synchronized (MobileCoinAPI$RingMLSAG.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        MobileCoinAPI$RingMLSAG mobileCoinAPI$RingMLSAG = new MobileCoinAPI$RingMLSAG();
        DEFAULT_INSTANCE = mobileCoinAPI$RingMLSAG;
        GeneratedMessageLite.registerDefaultInstance(MobileCoinAPI$RingMLSAG.class, mobileCoinAPI$RingMLSAG);
    }

    public static MobileCoinAPI$RingMLSAG getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<MobileCoinAPI$RingMLSAG> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
