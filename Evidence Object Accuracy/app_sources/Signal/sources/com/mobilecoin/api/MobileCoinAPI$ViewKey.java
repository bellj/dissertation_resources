package com.mobilecoin.api;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$CompressedRistretto;
import com.mobilecoin.api.MobileCoinAPI$RistrettoPrivate;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class MobileCoinAPI$ViewKey extends GeneratedMessageLite<MobileCoinAPI$ViewKey, Builder> implements MessageLiteOrBuilder {
    private static final MobileCoinAPI$ViewKey DEFAULT_INSTANCE;
    private static volatile Parser<MobileCoinAPI$ViewKey> PARSER;
    public static final int SPEND_PUBLIC_KEY_FIELD_NUMBER;
    public static final int VIEW_PRIVATE_KEY_FIELD_NUMBER;
    private MobileCoinAPI$CompressedRistretto spendPublicKey_;
    private MobileCoinAPI$RistrettoPrivate viewPrivateKey_;

    private MobileCoinAPI$ViewKey() {
    }

    public boolean hasViewPrivateKey() {
        return this.viewPrivateKey_ != null;
    }

    public MobileCoinAPI$RistrettoPrivate getViewPrivateKey() {
        MobileCoinAPI$RistrettoPrivate mobileCoinAPI$RistrettoPrivate = this.viewPrivateKey_;
        return mobileCoinAPI$RistrettoPrivate == null ? MobileCoinAPI$RistrettoPrivate.getDefaultInstance() : mobileCoinAPI$RistrettoPrivate;
    }

    public void setViewPrivateKey(MobileCoinAPI$RistrettoPrivate mobileCoinAPI$RistrettoPrivate) {
        mobileCoinAPI$RistrettoPrivate.getClass();
        this.viewPrivateKey_ = mobileCoinAPI$RistrettoPrivate;
    }

    public void setViewPrivateKey(MobileCoinAPI$RistrettoPrivate.Builder builder) {
        this.viewPrivateKey_ = builder.build();
    }

    public void mergeViewPrivateKey(MobileCoinAPI$RistrettoPrivate mobileCoinAPI$RistrettoPrivate) {
        mobileCoinAPI$RistrettoPrivate.getClass();
        MobileCoinAPI$RistrettoPrivate mobileCoinAPI$RistrettoPrivate2 = this.viewPrivateKey_;
        if (mobileCoinAPI$RistrettoPrivate2 == null || mobileCoinAPI$RistrettoPrivate2 == MobileCoinAPI$RistrettoPrivate.getDefaultInstance()) {
            this.viewPrivateKey_ = mobileCoinAPI$RistrettoPrivate;
        } else {
            this.viewPrivateKey_ = MobileCoinAPI$RistrettoPrivate.newBuilder(this.viewPrivateKey_).mergeFrom((MobileCoinAPI$RistrettoPrivate.Builder) mobileCoinAPI$RistrettoPrivate).buildPartial();
        }
    }

    public void clearViewPrivateKey() {
        this.viewPrivateKey_ = null;
    }

    public boolean hasSpendPublicKey() {
        return this.spendPublicKey_ != null;
    }

    public MobileCoinAPI$CompressedRistretto getSpendPublicKey() {
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto = this.spendPublicKey_;
        return mobileCoinAPI$CompressedRistretto == null ? MobileCoinAPI$CompressedRistretto.getDefaultInstance() : mobileCoinAPI$CompressedRistretto;
    }

    public void setSpendPublicKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        this.spendPublicKey_ = mobileCoinAPI$CompressedRistretto;
    }

    public void setSpendPublicKey(MobileCoinAPI$CompressedRistretto.Builder builder) {
        this.spendPublicKey_ = builder.build();
    }

    public void mergeSpendPublicKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto2 = this.spendPublicKey_;
        if (mobileCoinAPI$CompressedRistretto2 == null || mobileCoinAPI$CompressedRistretto2 == MobileCoinAPI$CompressedRistretto.getDefaultInstance()) {
            this.spendPublicKey_ = mobileCoinAPI$CompressedRistretto;
        } else {
            this.spendPublicKey_ = MobileCoinAPI$CompressedRistretto.newBuilder(this.spendPublicKey_).mergeFrom((MobileCoinAPI$CompressedRistretto.Builder) mobileCoinAPI$CompressedRistretto).buildPartial();
        }
    }

    public void clearSpendPublicKey() {
        this.spendPublicKey_ = null;
    }

    public static MobileCoinAPI$ViewKey parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$ViewKey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static MobileCoinAPI$ViewKey parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$ViewKey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static MobileCoinAPI$ViewKey parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$ViewKey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static MobileCoinAPI$ViewKey parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$ViewKey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static MobileCoinAPI$ViewKey parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$ViewKey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static MobileCoinAPI$ViewKey parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$ViewKey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static MobileCoinAPI$ViewKey parseFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$ViewKey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$ViewKey parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$ViewKey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$ViewKey parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$ViewKey) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$ViewKey parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$ViewKey) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$ViewKey parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (MobileCoinAPI$ViewKey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static MobileCoinAPI$ViewKey parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$ViewKey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(MobileCoinAPI$ViewKey mobileCoinAPI$ViewKey) {
        return DEFAULT_INSTANCE.createBuilder(mobileCoinAPI$ViewKey);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<MobileCoinAPI$ViewKey, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(MobileCoinAPI$1 mobileCoinAPI$1) {
            this();
        }

        private Builder() {
            super(MobileCoinAPI$ViewKey.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (MobileCoinAPI$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new MobileCoinAPI$ViewKey();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\t\u0002\t", new Object[]{"viewPrivateKey_", "spendPublicKey_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<MobileCoinAPI$ViewKey> parser = PARSER;
                if (parser == null) {
                    synchronized (MobileCoinAPI$ViewKey.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        MobileCoinAPI$ViewKey mobileCoinAPI$ViewKey = new MobileCoinAPI$ViewKey();
        DEFAULT_INSTANCE = mobileCoinAPI$ViewKey;
        GeneratedMessageLite.registerDefaultInstance(MobileCoinAPI$ViewKey.class, mobileCoinAPI$ViewKey);
    }

    public static MobileCoinAPI$ViewKey getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<MobileCoinAPI$ViewKey> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
