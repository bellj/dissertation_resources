package com.mobilecoin.api;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$TxIn;
import com.mobilecoin.api.MobileCoinAPI$TxOut;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class MobileCoinAPI$TxPrefix extends GeneratedMessageLite<MobileCoinAPI$TxPrefix, Builder> implements MessageLiteOrBuilder {
    private static final MobileCoinAPI$TxPrefix DEFAULT_INSTANCE;
    public static final int FEE_FIELD_NUMBER;
    public static final int INPUTS_FIELD_NUMBER;
    public static final int OUTPUTS_FIELD_NUMBER;
    private static volatile Parser<MobileCoinAPI$TxPrefix> PARSER;
    public static final int TOMBSTONE_BLOCK_FIELD_NUMBER;
    private long fee_;
    private Internal.ProtobufList<MobileCoinAPI$TxIn> inputs_ = GeneratedMessageLite.emptyProtobufList();
    private Internal.ProtobufList<MobileCoinAPI$TxOut> outputs_ = GeneratedMessageLite.emptyProtobufList();
    private long tombstoneBlock_;

    private MobileCoinAPI$TxPrefix() {
    }

    public List<MobileCoinAPI$TxIn> getInputsList() {
        return this.inputs_;
    }

    public List<? extends MobileCoinAPI$TxInOrBuilder> getInputsOrBuilderList() {
        return this.inputs_;
    }

    public int getInputsCount() {
        return this.inputs_.size();
    }

    public MobileCoinAPI$TxIn getInputs(int i) {
        return this.inputs_.get(i);
    }

    public MobileCoinAPI$TxInOrBuilder getInputsOrBuilder(int i) {
        return this.inputs_.get(i);
    }

    private void ensureInputsIsMutable() {
        if (!this.inputs_.isModifiable()) {
            this.inputs_ = GeneratedMessageLite.mutableCopy(this.inputs_);
        }
    }

    public void setInputs(int i, MobileCoinAPI$TxIn mobileCoinAPI$TxIn) {
        mobileCoinAPI$TxIn.getClass();
        ensureInputsIsMutable();
        this.inputs_.set(i, mobileCoinAPI$TxIn);
    }

    public void setInputs(int i, MobileCoinAPI$TxIn.Builder builder) {
        ensureInputsIsMutable();
        this.inputs_.set(i, builder.build());
    }

    public void addInputs(MobileCoinAPI$TxIn mobileCoinAPI$TxIn) {
        mobileCoinAPI$TxIn.getClass();
        ensureInputsIsMutable();
        this.inputs_.add(mobileCoinAPI$TxIn);
    }

    public void addInputs(int i, MobileCoinAPI$TxIn mobileCoinAPI$TxIn) {
        mobileCoinAPI$TxIn.getClass();
        ensureInputsIsMutable();
        this.inputs_.add(i, mobileCoinAPI$TxIn);
    }

    public void addInputs(MobileCoinAPI$TxIn.Builder builder) {
        ensureInputsIsMutable();
        this.inputs_.add(builder.build());
    }

    public void addInputs(int i, MobileCoinAPI$TxIn.Builder builder) {
        ensureInputsIsMutable();
        this.inputs_.add(i, builder.build());
    }

    public void addAllInputs(Iterable<? extends MobileCoinAPI$TxIn> iterable) {
        ensureInputsIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.inputs_);
    }

    public void clearInputs() {
        this.inputs_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeInputs(int i) {
        ensureInputsIsMutable();
        this.inputs_.remove(i);
    }

    public List<MobileCoinAPI$TxOut> getOutputsList() {
        return this.outputs_;
    }

    public List<? extends MobileCoinAPI$TxOutOrBuilder> getOutputsOrBuilderList() {
        return this.outputs_;
    }

    public int getOutputsCount() {
        return this.outputs_.size();
    }

    public MobileCoinAPI$TxOut getOutputs(int i) {
        return this.outputs_.get(i);
    }

    public MobileCoinAPI$TxOutOrBuilder getOutputsOrBuilder(int i) {
        return this.outputs_.get(i);
    }

    private void ensureOutputsIsMutable() {
        if (!this.outputs_.isModifiable()) {
            this.outputs_ = GeneratedMessageLite.mutableCopy(this.outputs_);
        }
    }

    public void setOutputs(int i, MobileCoinAPI$TxOut mobileCoinAPI$TxOut) {
        mobileCoinAPI$TxOut.getClass();
        ensureOutputsIsMutable();
        this.outputs_.set(i, mobileCoinAPI$TxOut);
    }

    public void setOutputs(int i, MobileCoinAPI$TxOut.Builder builder) {
        ensureOutputsIsMutable();
        this.outputs_.set(i, builder.build());
    }

    public void addOutputs(MobileCoinAPI$TxOut mobileCoinAPI$TxOut) {
        mobileCoinAPI$TxOut.getClass();
        ensureOutputsIsMutable();
        this.outputs_.add(mobileCoinAPI$TxOut);
    }

    public void addOutputs(int i, MobileCoinAPI$TxOut mobileCoinAPI$TxOut) {
        mobileCoinAPI$TxOut.getClass();
        ensureOutputsIsMutable();
        this.outputs_.add(i, mobileCoinAPI$TxOut);
    }

    public void addOutputs(MobileCoinAPI$TxOut.Builder builder) {
        ensureOutputsIsMutable();
        this.outputs_.add(builder.build());
    }

    public void addOutputs(int i, MobileCoinAPI$TxOut.Builder builder) {
        ensureOutputsIsMutable();
        this.outputs_.add(i, builder.build());
    }

    public void addAllOutputs(Iterable<? extends MobileCoinAPI$TxOut> iterable) {
        ensureOutputsIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.outputs_);
    }

    public void clearOutputs() {
        this.outputs_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeOutputs(int i) {
        ensureOutputsIsMutable();
        this.outputs_.remove(i);
    }

    public long getFee() {
        return this.fee_;
    }

    public void setFee(long j) {
        this.fee_ = j;
    }

    public void clearFee() {
        this.fee_ = 0;
    }

    public long getTombstoneBlock() {
        return this.tombstoneBlock_;
    }

    public void setTombstoneBlock(long j) {
        this.tombstoneBlock_ = j;
    }

    public void clearTombstoneBlock() {
        this.tombstoneBlock_ = 0;
    }

    public static MobileCoinAPI$TxPrefix parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxPrefix) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static MobileCoinAPI$TxPrefix parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxPrefix) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxPrefix parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxPrefix) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static MobileCoinAPI$TxPrefix parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxPrefix) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxPrefix parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxPrefix) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static MobileCoinAPI$TxPrefix parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxPrefix) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxPrefix parseFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$TxPrefix) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$TxPrefix parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$TxPrefix) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxPrefix parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$TxPrefix) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$TxPrefix parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$TxPrefix) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxPrefix parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (MobileCoinAPI$TxPrefix) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static MobileCoinAPI$TxPrefix parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$TxPrefix) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(MobileCoinAPI$TxPrefix mobileCoinAPI$TxPrefix) {
        return DEFAULT_INSTANCE.createBuilder(mobileCoinAPI$TxPrefix);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<MobileCoinAPI$TxPrefix, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(MobileCoinAPI$1 mobileCoinAPI$1) {
            this();
        }

        private Builder() {
            super(MobileCoinAPI$TxPrefix.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (MobileCoinAPI$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new MobileCoinAPI$TxPrefix();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0002\u0000\u0001\u001b\u0002\u001b\u0003\u0003\u0004\u0003", new Object[]{"inputs_", MobileCoinAPI$TxIn.class, "outputs_", MobileCoinAPI$TxOut.class, "fee_", "tombstoneBlock_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<MobileCoinAPI$TxPrefix> parser = PARSER;
                if (parser == null) {
                    synchronized (MobileCoinAPI$TxPrefix.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        MobileCoinAPI$TxPrefix mobileCoinAPI$TxPrefix = new MobileCoinAPI$TxPrefix();
        DEFAULT_INSTANCE = mobileCoinAPI$TxPrefix;
        GeneratedMessageLite.registerDefaultInstance(MobileCoinAPI$TxPrefix.class, mobileCoinAPI$TxPrefix);
    }

    public static MobileCoinAPI$TxPrefix getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<MobileCoinAPI$TxPrefix> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
