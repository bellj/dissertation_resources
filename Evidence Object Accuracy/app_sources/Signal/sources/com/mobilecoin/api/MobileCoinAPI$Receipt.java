package com.mobilecoin.api;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$Amount;
import com.mobilecoin.api.MobileCoinAPI$CompressedRistretto;
import com.mobilecoin.api.MobileCoinAPI$TxOutConfirmationNumber;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class MobileCoinAPI$Receipt extends GeneratedMessageLite<MobileCoinAPI$Receipt, Builder> implements MessageLiteOrBuilder {
    public static final int AMOUNT_FIELD_NUMBER;
    public static final int CONFIRMATION_FIELD_NUMBER;
    private static final MobileCoinAPI$Receipt DEFAULT_INSTANCE;
    private static volatile Parser<MobileCoinAPI$Receipt> PARSER;
    public static final int PUBLIC_KEY_FIELD_NUMBER;
    public static final int TOMBSTONE_BLOCK_FIELD_NUMBER;
    private MobileCoinAPI$Amount amount_;
    private MobileCoinAPI$TxOutConfirmationNumber confirmation_;
    private MobileCoinAPI$CompressedRistretto publicKey_;
    private long tombstoneBlock_;

    private MobileCoinAPI$Receipt() {
    }

    public boolean hasPublicKey() {
        return this.publicKey_ != null;
    }

    public MobileCoinAPI$CompressedRistretto getPublicKey() {
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto = this.publicKey_;
        return mobileCoinAPI$CompressedRistretto == null ? MobileCoinAPI$CompressedRistretto.getDefaultInstance() : mobileCoinAPI$CompressedRistretto;
    }

    public void setPublicKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        this.publicKey_ = mobileCoinAPI$CompressedRistretto;
    }

    public void setPublicKey(MobileCoinAPI$CompressedRistretto.Builder builder) {
        this.publicKey_ = builder.build();
    }

    public void mergePublicKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto2 = this.publicKey_;
        if (mobileCoinAPI$CompressedRistretto2 == null || mobileCoinAPI$CompressedRistretto2 == MobileCoinAPI$CompressedRistretto.getDefaultInstance()) {
            this.publicKey_ = mobileCoinAPI$CompressedRistretto;
        } else {
            this.publicKey_ = MobileCoinAPI$CompressedRistretto.newBuilder(this.publicKey_).mergeFrom((MobileCoinAPI$CompressedRistretto.Builder) mobileCoinAPI$CompressedRistretto).buildPartial();
        }
    }

    public void clearPublicKey() {
        this.publicKey_ = null;
    }

    public boolean hasConfirmation() {
        return this.confirmation_ != null;
    }

    public MobileCoinAPI$TxOutConfirmationNumber getConfirmation() {
        MobileCoinAPI$TxOutConfirmationNumber mobileCoinAPI$TxOutConfirmationNumber = this.confirmation_;
        return mobileCoinAPI$TxOutConfirmationNumber == null ? MobileCoinAPI$TxOutConfirmationNumber.getDefaultInstance() : mobileCoinAPI$TxOutConfirmationNumber;
    }

    public void setConfirmation(MobileCoinAPI$TxOutConfirmationNumber mobileCoinAPI$TxOutConfirmationNumber) {
        mobileCoinAPI$TxOutConfirmationNumber.getClass();
        this.confirmation_ = mobileCoinAPI$TxOutConfirmationNumber;
    }

    public void setConfirmation(MobileCoinAPI$TxOutConfirmationNumber.Builder builder) {
        this.confirmation_ = builder.build();
    }

    public void mergeConfirmation(MobileCoinAPI$TxOutConfirmationNumber mobileCoinAPI$TxOutConfirmationNumber) {
        mobileCoinAPI$TxOutConfirmationNumber.getClass();
        MobileCoinAPI$TxOutConfirmationNumber mobileCoinAPI$TxOutConfirmationNumber2 = this.confirmation_;
        if (mobileCoinAPI$TxOutConfirmationNumber2 == null || mobileCoinAPI$TxOutConfirmationNumber2 == MobileCoinAPI$TxOutConfirmationNumber.getDefaultInstance()) {
            this.confirmation_ = mobileCoinAPI$TxOutConfirmationNumber;
        } else {
            this.confirmation_ = MobileCoinAPI$TxOutConfirmationNumber.newBuilder(this.confirmation_).mergeFrom((MobileCoinAPI$TxOutConfirmationNumber.Builder) mobileCoinAPI$TxOutConfirmationNumber).buildPartial();
        }
    }

    public void clearConfirmation() {
        this.confirmation_ = null;
    }

    public long getTombstoneBlock() {
        return this.tombstoneBlock_;
    }

    public void setTombstoneBlock(long j) {
        this.tombstoneBlock_ = j;
    }

    public void clearTombstoneBlock() {
        this.tombstoneBlock_ = 0;
    }

    public boolean hasAmount() {
        return this.amount_ != null;
    }

    public MobileCoinAPI$Amount getAmount() {
        MobileCoinAPI$Amount mobileCoinAPI$Amount = this.amount_;
        return mobileCoinAPI$Amount == null ? MobileCoinAPI$Amount.getDefaultInstance() : mobileCoinAPI$Amount;
    }

    public void setAmount(MobileCoinAPI$Amount mobileCoinAPI$Amount) {
        mobileCoinAPI$Amount.getClass();
        this.amount_ = mobileCoinAPI$Amount;
    }

    public void setAmount(MobileCoinAPI$Amount.Builder builder) {
        this.amount_ = builder.build();
    }

    public void mergeAmount(MobileCoinAPI$Amount mobileCoinAPI$Amount) {
        mobileCoinAPI$Amount.getClass();
        MobileCoinAPI$Amount mobileCoinAPI$Amount2 = this.amount_;
        if (mobileCoinAPI$Amount2 == null || mobileCoinAPI$Amount2 == MobileCoinAPI$Amount.getDefaultInstance()) {
            this.amount_ = mobileCoinAPI$Amount;
        } else {
            this.amount_ = MobileCoinAPI$Amount.newBuilder(this.amount_).mergeFrom((MobileCoinAPI$Amount.Builder) mobileCoinAPI$Amount).buildPartial();
        }
    }

    public void clearAmount() {
        this.amount_ = null;
    }

    public static MobileCoinAPI$Receipt parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$Receipt) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static MobileCoinAPI$Receipt parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$Receipt) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static MobileCoinAPI$Receipt parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$Receipt) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static MobileCoinAPI$Receipt parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$Receipt) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static MobileCoinAPI$Receipt parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$Receipt) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static MobileCoinAPI$Receipt parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$Receipt) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static MobileCoinAPI$Receipt parseFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$Receipt) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$Receipt parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$Receipt) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$Receipt parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$Receipt) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$Receipt parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$Receipt) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$Receipt parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (MobileCoinAPI$Receipt) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static MobileCoinAPI$Receipt parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$Receipt) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(MobileCoinAPI$Receipt mobileCoinAPI$Receipt) {
        return DEFAULT_INSTANCE.createBuilder(mobileCoinAPI$Receipt);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<MobileCoinAPI$Receipt, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(MobileCoinAPI$1 mobileCoinAPI$1) {
            this();
        }

        private Builder() {
            super(MobileCoinAPI$Receipt.DEFAULT_INSTANCE);
        }

        public Builder setPublicKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
            copyOnWrite();
            ((MobileCoinAPI$Receipt) this.instance).setPublicKey(mobileCoinAPI$CompressedRistretto);
            return this;
        }

        public Builder setConfirmation(MobileCoinAPI$TxOutConfirmationNumber mobileCoinAPI$TxOutConfirmationNumber) {
            copyOnWrite();
            ((MobileCoinAPI$Receipt) this.instance).setConfirmation(mobileCoinAPI$TxOutConfirmationNumber);
            return this;
        }

        public Builder setTombstoneBlock(long j) {
            copyOnWrite();
            ((MobileCoinAPI$Receipt) this.instance).setTombstoneBlock(j);
            return this;
        }

        public Builder setAmount(MobileCoinAPI$Amount mobileCoinAPI$Amount) {
            copyOnWrite();
            ((MobileCoinAPI$Receipt) this.instance).setAmount(mobileCoinAPI$Amount);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (MobileCoinAPI$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new MobileCoinAPI$Receipt();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0000\u0000\u0001\t\u0002\t\u0003\u0003\u0004\t", new Object[]{"publicKey_", "confirmation_", "tombstoneBlock_", "amount_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<MobileCoinAPI$Receipt> parser = PARSER;
                if (parser == null) {
                    synchronized (MobileCoinAPI$Receipt.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        MobileCoinAPI$Receipt mobileCoinAPI$Receipt = new MobileCoinAPI$Receipt();
        DEFAULT_INSTANCE = mobileCoinAPI$Receipt;
        GeneratedMessageLite.registerDefaultInstance(MobileCoinAPI$Receipt.class, mobileCoinAPI$Receipt);
    }

    public static MobileCoinAPI$Receipt getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<MobileCoinAPI$Receipt> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
