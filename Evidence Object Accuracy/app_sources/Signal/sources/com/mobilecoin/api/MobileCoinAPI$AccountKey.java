package com.mobilecoin.api;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$RistrettoPrivate;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class MobileCoinAPI$AccountKey extends GeneratedMessageLite<MobileCoinAPI$AccountKey, Builder> implements MessageLiteOrBuilder {
    private static final MobileCoinAPI$AccountKey DEFAULT_INSTANCE;
    public static final int FOG_AUTHORITY_SPKI_FIELD_NUMBER;
    public static final int FOG_REPORT_ID_FIELD_NUMBER;
    public static final int FOG_REPORT_URL_FIELD_NUMBER;
    private static volatile Parser<MobileCoinAPI$AccountKey> PARSER;
    public static final int SPEND_PRIVATE_KEY_FIELD_NUMBER;
    public static final int VIEW_PRIVATE_KEY_FIELD_NUMBER;
    private ByteString fogAuthoritySpki_ = ByteString.EMPTY;
    private String fogReportId_ = "";
    private String fogReportUrl_ = "";
    private MobileCoinAPI$RistrettoPrivate spendPrivateKey_;
    private MobileCoinAPI$RistrettoPrivate viewPrivateKey_;

    private MobileCoinAPI$AccountKey() {
    }

    public boolean hasViewPrivateKey() {
        return this.viewPrivateKey_ != null;
    }

    public MobileCoinAPI$RistrettoPrivate getViewPrivateKey() {
        MobileCoinAPI$RistrettoPrivate mobileCoinAPI$RistrettoPrivate = this.viewPrivateKey_;
        return mobileCoinAPI$RistrettoPrivate == null ? MobileCoinAPI$RistrettoPrivate.getDefaultInstance() : mobileCoinAPI$RistrettoPrivate;
    }

    public void setViewPrivateKey(MobileCoinAPI$RistrettoPrivate mobileCoinAPI$RistrettoPrivate) {
        mobileCoinAPI$RistrettoPrivate.getClass();
        this.viewPrivateKey_ = mobileCoinAPI$RistrettoPrivate;
    }

    public void setViewPrivateKey(MobileCoinAPI$RistrettoPrivate.Builder builder) {
        this.viewPrivateKey_ = builder.build();
    }

    public void mergeViewPrivateKey(MobileCoinAPI$RistrettoPrivate mobileCoinAPI$RistrettoPrivate) {
        mobileCoinAPI$RistrettoPrivate.getClass();
        MobileCoinAPI$RistrettoPrivate mobileCoinAPI$RistrettoPrivate2 = this.viewPrivateKey_;
        if (mobileCoinAPI$RistrettoPrivate2 == null || mobileCoinAPI$RistrettoPrivate2 == MobileCoinAPI$RistrettoPrivate.getDefaultInstance()) {
            this.viewPrivateKey_ = mobileCoinAPI$RistrettoPrivate;
        } else {
            this.viewPrivateKey_ = MobileCoinAPI$RistrettoPrivate.newBuilder(this.viewPrivateKey_).mergeFrom((MobileCoinAPI$RistrettoPrivate.Builder) mobileCoinAPI$RistrettoPrivate).buildPartial();
        }
    }

    public void clearViewPrivateKey() {
        this.viewPrivateKey_ = null;
    }

    public boolean hasSpendPrivateKey() {
        return this.spendPrivateKey_ != null;
    }

    public MobileCoinAPI$RistrettoPrivate getSpendPrivateKey() {
        MobileCoinAPI$RistrettoPrivate mobileCoinAPI$RistrettoPrivate = this.spendPrivateKey_;
        return mobileCoinAPI$RistrettoPrivate == null ? MobileCoinAPI$RistrettoPrivate.getDefaultInstance() : mobileCoinAPI$RistrettoPrivate;
    }

    public void setSpendPrivateKey(MobileCoinAPI$RistrettoPrivate mobileCoinAPI$RistrettoPrivate) {
        mobileCoinAPI$RistrettoPrivate.getClass();
        this.spendPrivateKey_ = mobileCoinAPI$RistrettoPrivate;
    }

    public void setSpendPrivateKey(MobileCoinAPI$RistrettoPrivate.Builder builder) {
        this.spendPrivateKey_ = builder.build();
    }

    public void mergeSpendPrivateKey(MobileCoinAPI$RistrettoPrivate mobileCoinAPI$RistrettoPrivate) {
        mobileCoinAPI$RistrettoPrivate.getClass();
        MobileCoinAPI$RistrettoPrivate mobileCoinAPI$RistrettoPrivate2 = this.spendPrivateKey_;
        if (mobileCoinAPI$RistrettoPrivate2 == null || mobileCoinAPI$RistrettoPrivate2 == MobileCoinAPI$RistrettoPrivate.getDefaultInstance()) {
            this.spendPrivateKey_ = mobileCoinAPI$RistrettoPrivate;
        } else {
            this.spendPrivateKey_ = MobileCoinAPI$RistrettoPrivate.newBuilder(this.spendPrivateKey_).mergeFrom((MobileCoinAPI$RistrettoPrivate.Builder) mobileCoinAPI$RistrettoPrivate).buildPartial();
        }
    }

    public void clearSpendPrivateKey() {
        this.spendPrivateKey_ = null;
    }

    public String getFogReportUrl() {
        return this.fogReportUrl_;
    }

    public ByteString getFogReportUrlBytes() {
        return ByteString.copyFromUtf8(this.fogReportUrl_);
    }

    public void setFogReportUrl(String str) {
        str.getClass();
        this.fogReportUrl_ = str;
    }

    public void clearFogReportUrl() {
        this.fogReportUrl_ = getDefaultInstance().getFogReportUrl();
    }

    public void setFogReportUrlBytes(ByteString byteString) {
        byteString.getClass();
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.fogReportUrl_ = byteString.toStringUtf8();
    }

    public String getFogReportId() {
        return this.fogReportId_;
    }

    public ByteString getFogReportIdBytes() {
        return ByteString.copyFromUtf8(this.fogReportId_);
    }

    public void setFogReportId(String str) {
        str.getClass();
        this.fogReportId_ = str;
    }

    public void clearFogReportId() {
        this.fogReportId_ = getDefaultInstance().getFogReportId();
    }

    public void setFogReportIdBytes(ByteString byteString) {
        byteString.getClass();
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.fogReportId_ = byteString.toStringUtf8();
    }

    public ByteString getFogAuthoritySpki() {
        return this.fogAuthoritySpki_;
    }

    public void setFogAuthoritySpki(ByteString byteString) {
        byteString.getClass();
        this.fogAuthoritySpki_ = byteString;
    }

    public void clearFogAuthoritySpki() {
        this.fogAuthoritySpki_ = getDefaultInstance().getFogAuthoritySpki();
    }

    public static MobileCoinAPI$AccountKey parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$AccountKey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static MobileCoinAPI$AccountKey parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$AccountKey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static MobileCoinAPI$AccountKey parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$AccountKey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static MobileCoinAPI$AccountKey parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$AccountKey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static MobileCoinAPI$AccountKey parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$AccountKey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static MobileCoinAPI$AccountKey parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$AccountKey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static MobileCoinAPI$AccountKey parseFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$AccountKey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$AccountKey parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$AccountKey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$AccountKey parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$AccountKey) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$AccountKey parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$AccountKey) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$AccountKey parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (MobileCoinAPI$AccountKey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static MobileCoinAPI$AccountKey parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$AccountKey) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(MobileCoinAPI$AccountKey mobileCoinAPI$AccountKey) {
        return DEFAULT_INSTANCE.createBuilder(mobileCoinAPI$AccountKey);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<MobileCoinAPI$AccountKey, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(MobileCoinAPI$1 mobileCoinAPI$1) {
            this();
        }

        private Builder() {
            super(MobileCoinAPI$AccountKey.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (MobileCoinAPI$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new MobileCoinAPI$AccountKey();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0005\u0000\u0000\u0001\u0005\u0005\u0000\u0000\u0000\u0001\t\u0002\t\u0003Ȉ\u0004Ȉ\u0005\n", new Object[]{"viewPrivateKey_", "spendPrivateKey_", "fogReportUrl_", "fogReportId_", "fogAuthoritySpki_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<MobileCoinAPI$AccountKey> parser = PARSER;
                if (parser == null) {
                    synchronized (MobileCoinAPI$AccountKey.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        MobileCoinAPI$AccountKey mobileCoinAPI$AccountKey = new MobileCoinAPI$AccountKey();
        DEFAULT_INSTANCE = mobileCoinAPI$AccountKey;
        GeneratedMessageLite.registerDefaultInstance(MobileCoinAPI$AccountKey.class, mobileCoinAPI$AccountKey);
    }

    public static MobileCoinAPI$AccountKey getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<MobileCoinAPI$AccountKey> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
