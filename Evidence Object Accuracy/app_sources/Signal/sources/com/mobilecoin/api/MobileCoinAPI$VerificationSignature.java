package com.mobilecoin.api;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class MobileCoinAPI$VerificationSignature extends GeneratedMessageLite<MobileCoinAPI$VerificationSignature, Builder> implements MessageLiteOrBuilder {
    public static final int CONTENTS_FIELD_NUMBER;
    private static final MobileCoinAPI$VerificationSignature DEFAULT_INSTANCE;
    private static volatile Parser<MobileCoinAPI$VerificationSignature> PARSER;
    private ByteString contents_ = ByteString.EMPTY;

    private MobileCoinAPI$VerificationSignature() {
    }

    public ByteString getContents() {
        return this.contents_;
    }

    public void setContents(ByteString byteString) {
        byteString.getClass();
        this.contents_ = byteString;
    }

    public void clearContents() {
        this.contents_ = getDefaultInstance().getContents();
    }

    public static MobileCoinAPI$VerificationSignature parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$VerificationSignature) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static MobileCoinAPI$VerificationSignature parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$VerificationSignature) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static MobileCoinAPI$VerificationSignature parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$VerificationSignature) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static MobileCoinAPI$VerificationSignature parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$VerificationSignature) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static MobileCoinAPI$VerificationSignature parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$VerificationSignature) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static MobileCoinAPI$VerificationSignature parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$VerificationSignature) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static MobileCoinAPI$VerificationSignature parseFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$VerificationSignature) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$VerificationSignature parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$VerificationSignature) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$VerificationSignature parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$VerificationSignature) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$VerificationSignature parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$VerificationSignature) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$VerificationSignature parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (MobileCoinAPI$VerificationSignature) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static MobileCoinAPI$VerificationSignature parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$VerificationSignature) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(MobileCoinAPI$VerificationSignature mobileCoinAPI$VerificationSignature) {
        return DEFAULT_INSTANCE.createBuilder(mobileCoinAPI$VerificationSignature);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<MobileCoinAPI$VerificationSignature, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(MobileCoinAPI$1 mobileCoinAPI$1) {
            this();
        }

        private Builder() {
            super(MobileCoinAPI$VerificationSignature.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (MobileCoinAPI$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new MobileCoinAPI$VerificationSignature();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\n", new Object[]{"contents_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<MobileCoinAPI$VerificationSignature> parser = PARSER;
                if (parser == null) {
                    synchronized (MobileCoinAPI$VerificationSignature.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        MobileCoinAPI$VerificationSignature mobileCoinAPI$VerificationSignature = new MobileCoinAPI$VerificationSignature();
        DEFAULT_INSTANCE = mobileCoinAPI$VerificationSignature;
        GeneratedMessageLite.registerDefaultInstance(MobileCoinAPI$VerificationSignature.class, mobileCoinAPI$VerificationSignature);
    }

    public static MobileCoinAPI$VerificationSignature getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<MobileCoinAPI$VerificationSignature> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
