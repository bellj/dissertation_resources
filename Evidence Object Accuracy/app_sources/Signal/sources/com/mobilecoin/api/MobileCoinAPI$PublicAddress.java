package com.mobilecoin.api;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$CompressedRistretto;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class MobileCoinAPI$PublicAddress extends GeneratedMessageLite<MobileCoinAPI$PublicAddress, Builder> implements MessageLiteOrBuilder {
    private static final MobileCoinAPI$PublicAddress DEFAULT_INSTANCE;
    public static final int FOG_AUTHORITY_SIG_FIELD_NUMBER;
    public static final int FOG_REPORT_ID_FIELD_NUMBER;
    public static final int FOG_REPORT_URL_FIELD_NUMBER;
    private static volatile Parser<MobileCoinAPI$PublicAddress> PARSER;
    public static final int SPEND_PUBLIC_KEY_FIELD_NUMBER;
    public static final int VIEW_PUBLIC_KEY_FIELD_NUMBER;
    private ByteString fogAuthoritySig_ = ByteString.EMPTY;
    private String fogReportId_ = "";
    private String fogReportUrl_ = "";
    private MobileCoinAPI$CompressedRistretto spendPublicKey_;
    private MobileCoinAPI$CompressedRistretto viewPublicKey_;

    private MobileCoinAPI$PublicAddress() {
    }

    public boolean hasViewPublicKey() {
        return this.viewPublicKey_ != null;
    }

    public MobileCoinAPI$CompressedRistretto getViewPublicKey() {
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto = this.viewPublicKey_;
        return mobileCoinAPI$CompressedRistretto == null ? MobileCoinAPI$CompressedRistretto.getDefaultInstance() : mobileCoinAPI$CompressedRistretto;
    }

    public void setViewPublicKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        this.viewPublicKey_ = mobileCoinAPI$CompressedRistretto;
    }

    public void setViewPublicKey(MobileCoinAPI$CompressedRistretto.Builder builder) {
        this.viewPublicKey_ = builder.build();
    }

    public void mergeViewPublicKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto2 = this.viewPublicKey_;
        if (mobileCoinAPI$CompressedRistretto2 == null || mobileCoinAPI$CompressedRistretto2 == MobileCoinAPI$CompressedRistretto.getDefaultInstance()) {
            this.viewPublicKey_ = mobileCoinAPI$CompressedRistretto;
        } else {
            this.viewPublicKey_ = MobileCoinAPI$CompressedRistretto.newBuilder(this.viewPublicKey_).mergeFrom((MobileCoinAPI$CompressedRistretto.Builder) mobileCoinAPI$CompressedRistretto).buildPartial();
        }
    }

    public void clearViewPublicKey() {
        this.viewPublicKey_ = null;
    }

    public boolean hasSpendPublicKey() {
        return this.spendPublicKey_ != null;
    }

    public MobileCoinAPI$CompressedRistretto getSpendPublicKey() {
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto = this.spendPublicKey_;
        return mobileCoinAPI$CompressedRistretto == null ? MobileCoinAPI$CompressedRistretto.getDefaultInstance() : mobileCoinAPI$CompressedRistretto;
    }

    public void setSpendPublicKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        this.spendPublicKey_ = mobileCoinAPI$CompressedRistretto;
    }

    public void setSpendPublicKey(MobileCoinAPI$CompressedRistretto.Builder builder) {
        this.spendPublicKey_ = builder.build();
    }

    public void mergeSpendPublicKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto2 = this.spendPublicKey_;
        if (mobileCoinAPI$CompressedRistretto2 == null || mobileCoinAPI$CompressedRistretto2 == MobileCoinAPI$CompressedRistretto.getDefaultInstance()) {
            this.spendPublicKey_ = mobileCoinAPI$CompressedRistretto;
        } else {
            this.spendPublicKey_ = MobileCoinAPI$CompressedRistretto.newBuilder(this.spendPublicKey_).mergeFrom((MobileCoinAPI$CompressedRistretto.Builder) mobileCoinAPI$CompressedRistretto).buildPartial();
        }
    }

    public void clearSpendPublicKey() {
        this.spendPublicKey_ = null;
    }

    public String getFogReportUrl() {
        return this.fogReportUrl_;
    }

    public ByteString getFogReportUrlBytes() {
        return ByteString.copyFromUtf8(this.fogReportUrl_);
    }

    public void setFogReportUrl(String str) {
        str.getClass();
        this.fogReportUrl_ = str;
    }

    public void clearFogReportUrl() {
        this.fogReportUrl_ = getDefaultInstance().getFogReportUrl();
    }

    public void setFogReportUrlBytes(ByteString byteString) {
        byteString.getClass();
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.fogReportUrl_ = byteString.toStringUtf8();
    }

    public String getFogReportId() {
        return this.fogReportId_;
    }

    public ByteString getFogReportIdBytes() {
        return ByteString.copyFromUtf8(this.fogReportId_);
    }

    public void setFogReportId(String str) {
        str.getClass();
        this.fogReportId_ = str;
    }

    public void clearFogReportId() {
        this.fogReportId_ = getDefaultInstance().getFogReportId();
    }

    public void setFogReportIdBytes(ByteString byteString) {
        byteString.getClass();
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.fogReportId_ = byteString.toStringUtf8();
    }

    public ByteString getFogAuthoritySig() {
        return this.fogAuthoritySig_;
    }

    public void setFogAuthoritySig(ByteString byteString) {
        byteString.getClass();
        this.fogAuthoritySig_ = byteString;
    }

    public void clearFogAuthoritySig() {
        this.fogAuthoritySig_ = getDefaultInstance().getFogAuthoritySig();
    }

    public static MobileCoinAPI$PublicAddress parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$PublicAddress) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static MobileCoinAPI$PublicAddress parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$PublicAddress) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static MobileCoinAPI$PublicAddress parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$PublicAddress) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static MobileCoinAPI$PublicAddress parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$PublicAddress) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static MobileCoinAPI$PublicAddress parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$PublicAddress) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static MobileCoinAPI$PublicAddress parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$PublicAddress) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static MobileCoinAPI$PublicAddress parseFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$PublicAddress) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$PublicAddress parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$PublicAddress) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$PublicAddress parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$PublicAddress) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$PublicAddress parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$PublicAddress) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$PublicAddress parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (MobileCoinAPI$PublicAddress) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static MobileCoinAPI$PublicAddress parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$PublicAddress) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(MobileCoinAPI$PublicAddress mobileCoinAPI$PublicAddress) {
        return DEFAULT_INSTANCE.createBuilder(mobileCoinAPI$PublicAddress);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<MobileCoinAPI$PublicAddress, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(MobileCoinAPI$1 mobileCoinAPI$1) {
            this();
        }

        private Builder() {
            super(MobileCoinAPI$PublicAddress.DEFAULT_INSTANCE);
        }

        public Builder setViewPublicKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
            copyOnWrite();
            ((MobileCoinAPI$PublicAddress) this.instance).setViewPublicKey(mobileCoinAPI$CompressedRistretto);
            return this;
        }

        public Builder setSpendPublicKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
            copyOnWrite();
            ((MobileCoinAPI$PublicAddress) this.instance).setSpendPublicKey(mobileCoinAPI$CompressedRistretto);
            return this;
        }

        public Builder setFogReportUrl(String str) {
            copyOnWrite();
            ((MobileCoinAPI$PublicAddress) this.instance).setFogReportUrl(str);
            return this;
        }

        public Builder setFogReportId(String str) {
            copyOnWrite();
            ((MobileCoinAPI$PublicAddress) this.instance).setFogReportId(str);
            return this;
        }

        public Builder setFogAuthoritySig(ByteString byteString) {
            copyOnWrite();
            ((MobileCoinAPI$PublicAddress) this.instance).setFogAuthoritySig(byteString);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (MobileCoinAPI$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new MobileCoinAPI$PublicAddress();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0005\u0000\u0000\u0001\u0005\u0005\u0000\u0000\u0000\u0001\t\u0002\t\u0003Ȉ\u0004Ȉ\u0005\n", new Object[]{"viewPublicKey_", "spendPublicKey_", "fogReportUrl_", "fogReportId_", "fogAuthoritySig_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<MobileCoinAPI$PublicAddress> parser = PARSER;
                if (parser == null) {
                    synchronized (MobileCoinAPI$PublicAddress.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        MobileCoinAPI$PublicAddress mobileCoinAPI$PublicAddress = new MobileCoinAPI$PublicAddress();
        DEFAULT_INSTANCE = mobileCoinAPI$PublicAddress;
        GeneratedMessageLite.registerDefaultInstance(MobileCoinAPI$PublicAddress.class, mobileCoinAPI$PublicAddress);
    }

    public static MobileCoinAPI$PublicAddress getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<MobileCoinAPI$PublicAddress> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
