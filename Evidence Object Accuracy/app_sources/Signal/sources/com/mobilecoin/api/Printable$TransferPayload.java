package com.mobilecoin.api;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$CompressedRistretto;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class Printable$TransferPayload extends GeneratedMessageLite<Printable$TransferPayload, Builder> implements MessageLiteOrBuilder {
    public static final int BIP39_ENTROPY_FIELD_NUMBER;
    private static final Printable$TransferPayload DEFAULT_INSTANCE;
    public static final int MEMO_FIELD_NUMBER;
    private static volatile Parser<Printable$TransferPayload> PARSER;
    public static final int ROOT_ENTROPY_FIELD_NUMBER;
    public static final int TX_OUT_PUBLIC_KEY_FIELD_NUMBER;
    private ByteString bip39Entropy_;
    private String memo_ = "";
    private ByteString rootEntropy_;
    private MobileCoinAPI$CompressedRistretto txOutPublicKey_;

    private Printable$TransferPayload() {
        ByteString byteString = ByteString.EMPTY;
        this.rootEntropy_ = byteString;
        this.bip39Entropy_ = byteString;
    }

    @Deprecated
    public ByteString getRootEntropy() {
        return this.rootEntropy_;
    }

    public void setRootEntropy(ByteString byteString) {
        byteString.getClass();
        this.rootEntropy_ = byteString;
    }

    public void clearRootEntropy() {
        this.rootEntropy_ = getDefaultInstance().getRootEntropy();
    }

    public boolean hasTxOutPublicKey() {
        return this.txOutPublicKey_ != null;
    }

    public MobileCoinAPI$CompressedRistretto getTxOutPublicKey() {
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto = this.txOutPublicKey_;
        return mobileCoinAPI$CompressedRistretto == null ? MobileCoinAPI$CompressedRistretto.getDefaultInstance() : mobileCoinAPI$CompressedRistretto;
    }

    public void setTxOutPublicKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        this.txOutPublicKey_ = mobileCoinAPI$CompressedRistretto;
    }

    public void setTxOutPublicKey(MobileCoinAPI$CompressedRistretto.Builder builder) {
        this.txOutPublicKey_ = builder.build();
    }

    public void mergeTxOutPublicKey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto2 = this.txOutPublicKey_;
        if (mobileCoinAPI$CompressedRistretto2 == null || mobileCoinAPI$CompressedRistretto2 == MobileCoinAPI$CompressedRistretto.getDefaultInstance()) {
            this.txOutPublicKey_ = mobileCoinAPI$CompressedRistretto;
        } else {
            this.txOutPublicKey_ = MobileCoinAPI$CompressedRistretto.newBuilder(this.txOutPublicKey_).mergeFrom((MobileCoinAPI$CompressedRistretto.Builder) mobileCoinAPI$CompressedRistretto).buildPartial();
        }
    }

    public void clearTxOutPublicKey() {
        this.txOutPublicKey_ = null;
    }

    public String getMemo() {
        return this.memo_;
    }

    public ByteString getMemoBytes() {
        return ByteString.copyFromUtf8(this.memo_);
    }

    public void setMemo(String str) {
        str.getClass();
        this.memo_ = str;
    }

    public void clearMemo() {
        this.memo_ = getDefaultInstance().getMemo();
    }

    public void setMemoBytes(ByteString byteString) {
        byteString.getClass();
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.memo_ = byteString.toStringUtf8();
    }

    public ByteString getBip39Entropy() {
        return this.bip39Entropy_;
    }

    public void setBip39Entropy(ByteString byteString) {
        byteString.getClass();
        this.bip39Entropy_ = byteString;
    }

    public void clearBip39Entropy() {
        this.bip39Entropy_ = getDefaultInstance().getBip39Entropy();
    }

    public static Printable$TransferPayload parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Printable$TransferPayload) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Printable$TransferPayload parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Printable$TransferPayload) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Printable$TransferPayload parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Printable$TransferPayload) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Printable$TransferPayload parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Printable$TransferPayload) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Printable$TransferPayload parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Printable$TransferPayload) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Printable$TransferPayload parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Printable$TransferPayload) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Printable$TransferPayload parseFrom(InputStream inputStream) throws IOException {
        return (Printable$TransferPayload) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Printable$TransferPayload parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Printable$TransferPayload) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Printable$TransferPayload parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Printable$TransferPayload) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Printable$TransferPayload parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Printable$TransferPayload) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Printable$TransferPayload parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Printable$TransferPayload) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Printable$TransferPayload parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Printable$TransferPayload) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Printable$TransferPayload printable$TransferPayload) {
        return DEFAULT_INSTANCE.createBuilder(printable$TransferPayload);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Printable$TransferPayload, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(Printable$1 printable$1) {
            this();
        }

        private Builder() {
            super(Printable$TransferPayload.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (Printable$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Printable$TransferPayload();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0000\u0000\u0001\n\u0002\t\u0003Ȉ\u0004\n", new Object[]{"rootEntropy_", "txOutPublicKey_", "memo_", "bip39Entropy_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Printable$TransferPayload> parser = PARSER;
                if (parser == null) {
                    synchronized (Printable$TransferPayload.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Printable$TransferPayload printable$TransferPayload = new Printable$TransferPayload();
        DEFAULT_INSTANCE = printable$TransferPayload;
        GeneratedMessageLite.registerDefaultInstance(Printable$TransferPayload.class, printable$TransferPayload);
    }

    public static Printable$TransferPayload getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Printable$TransferPayload> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
