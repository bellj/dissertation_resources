package com.mobilecoin.api;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$Range;
import com.mobilecoin.api.MobileCoinAPI$TxOutMembershipHash;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class MobileCoinAPI$TxOutMembershipElement extends GeneratedMessageLite<MobileCoinAPI$TxOutMembershipElement, Builder> implements MobileCoinAPI$TxOutMembershipElementOrBuilder {
    private static final MobileCoinAPI$TxOutMembershipElement DEFAULT_INSTANCE;
    public static final int HASH_FIELD_NUMBER;
    private static volatile Parser<MobileCoinAPI$TxOutMembershipElement> PARSER;
    public static final int RANGE_FIELD_NUMBER;
    private MobileCoinAPI$TxOutMembershipHash hash_;
    private MobileCoinAPI$Range range_;

    private MobileCoinAPI$TxOutMembershipElement() {
    }

    public boolean hasRange() {
        return this.range_ != null;
    }

    public MobileCoinAPI$Range getRange() {
        MobileCoinAPI$Range mobileCoinAPI$Range = this.range_;
        return mobileCoinAPI$Range == null ? MobileCoinAPI$Range.getDefaultInstance() : mobileCoinAPI$Range;
    }

    public void setRange(MobileCoinAPI$Range mobileCoinAPI$Range) {
        mobileCoinAPI$Range.getClass();
        this.range_ = mobileCoinAPI$Range;
    }

    public void setRange(MobileCoinAPI$Range.Builder builder) {
        this.range_ = builder.build();
    }

    public void mergeRange(MobileCoinAPI$Range mobileCoinAPI$Range) {
        mobileCoinAPI$Range.getClass();
        MobileCoinAPI$Range mobileCoinAPI$Range2 = this.range_;
        if (mobileCoinAPI$Range2 == null || mobileCoinAPI$Range2 == MobileCoinAPI$Range.getDefaultInstance()) {
            this.range_ = mobileCoinAPI$Range;
        } else {
            this.range_ = MobileCoinAPI$Range.newBuilder(this.range_).mergeFrom((MobileCoinAPI$Range.Builder) mobileCoinAPI$Range).buildPartial();
        }
    }

    public void clearRange() {
        this.range_ = null;
    }

    public boolean hasHash() {
        return this.hash_ != null;
    }

    public MobileCoinAPI$TxOutMembershipHash getHash() {
        MobileCoinAPI$TxOutMembershipHash mobileCoinAPI$TxOutMembershipHash = this.hash_;
        return mobileCoinAPI$TxOutMembershipHash == null ? MobileCoinAPI$TxOutMembershipHash.getDefaultInstance() : mobileCoinAPI$TxOutMembershipHash;
    }

    public void setHash(MobileCoinAPI$TxOutMembershipHash mobileCoinAPI$TxOutMembershipHash) {
        mobileCoinAPI$TxOutMembershipHash.getClass();
        this.hash_ = mobileCoinAPI$TxOutMembershipHash;
    }

    public void setHash(MobileCoinAPI$TxOutMembershipHash.Builder builder) {
        this.hash_ = builder.build();
    }

    public void mergeHash(MobileCoinAPI$TxOutMembershipHash mobileCoinAPI$TxOutMembershipHash) {
        mobileCoinAPI$TxOutMembershipHash.getClass();
        MobileCoinAPI$TxOutMembershipHash mobileCoinAPI$TxOutMembershipHash2 = this.hash_;
        if (mobileCoinAPI$TxOutMembershipHash2 == null || mobileCoinAPI$TxOutMembershipHash2 == MobileCoinAPI$TxOutMembershipHash.getDefaultInstance()) {
            this.hash_ = mobileCoinAPI$TxOutMembershipHash;
        } else {
            this.hash_ = MobileCoinAPI$TxOutMembershipHash.newBuilder(this.hash_).mergeFrom((MobileCoinAPI$TxOutMembershipHash.Builder) mobileCoinAPI$TxOutMembershipHash).buildPartial();
        }
    }

    public void clearHash() {
        this.hash_ = null;
    }

    public static MobileCoinAPI$TxOutMembershipElement parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutMembershipElement) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static MobileCoinAPI$TxOutMembershipElement parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutMembershipElement) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOutMembershipElement parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutMembershipElement) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static MobileCoinAPI$TxOutMembershipElement parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutMembershipElement) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOutMembershipElement parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutMembershipElement) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static MobileCoinAPI$TxOutMembershipElement parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$TxOutMembershipElement) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOutMembershipElement parseFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$TxOutMembershipElement) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$TxOutMembershipElement parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$TxOutMembershipElement) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOutMembershipElement parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$TxOutMembershipElement) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$TxOutMembershipElement parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$TxOutMembershipElement) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$TxOutMembershipElement parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (MobileCoinAPI$TxOutMembershipElement) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static MobileCoinAPI$TxOutMembershipElement parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$TxOutMembershipElement) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(MobileCoinAPI$TxOutMembershipElement mobileCoinAPI$TxOutMembershipElement) {
        return DEFAULT_INSTANCE.createBuilder(mobileCoinAPI$TxOutMembershipElement);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<MobileCoinAPI$TxOutMembershipElement, Builder> implements MobileCoinAPI$TxOutMembershipElementOrBuilder {
        /* synthetic */ Builder(MobileCoinAPI$1 mobileCoinAPI$1) {
            this();
        }

        private Builder() {
            super(MobileCoinAPI$TxOutMembershipElement.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (MobileCoinAPI$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new MobileCoinAPI$TxOutMembershipElement();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\t\u0002\t", new Object[]{"range_", "hash_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<MobileCoinAPI$TxOutMembershipElement> parser = PARSER;
                if (parser == null) {
                    synchronized (MobileCoinAPI$TxOutMembershipElement.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        MobileCoinAPI$TxOutMembershipElement mobileCoinAPI$TxOutMembershipElement = new MobileCoinAPI$TxOutMembershipElement();
        DEFAULT_INSTANCE = mobileCoinAPI$TxOutMembershipElement;
        GeneratedMessageLite.registerDefaultInstance(MobileCoinAPI$TxOutMembershipElement.class, mobileCoinAPI$TxOutMembershipElement);
    }

    public static MobileCoinAPI$TxOutMembershipElement getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<MobileCoinAPI$TxOutMembershipElement> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
