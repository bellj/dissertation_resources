package com.mobilecoin.api;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$SignatureRctBulletproofs;
import com.mobilecoin.api.MobileCoinAPI$TxPrefix;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class MobileCoinAPI$Tx extends GeneratedMessageLite<MobileCoinAPI$Tx, Builder> implements MessageLiteOrBuilder {
    private static final MobileCoinAPI$Tx DEFAULT_INSTANCE;
    private static volatile Parser<MobileCoinAPI$Tx> PARSER;
    public static final int PREFIX_FIELD_NUMBER;
    public static final int SIGNATURE_FIELD_NUMBER;
    private MobileCoinAPI$TxPrefix prefix_;
    private MobileCoinAPI$SignatureRctBulletproofs signature_;

    private MobileCoinAPI$Tx() {
    }

    public boolean hasPrefix() {
        return this.prefix_ != null;
    }

    public MobileCoinAPI$TxPrefix getPrefix() {
        MobileCoinAPI$TxPrefix mobileCoinAPI$TxPrefix = this.prefix_;
        return mobileCoinAPI$TxPrefix == null ? MobileCoinAPI$TxPrefix.getDefaultInstance() : mobileCoinAPI$TxPrefix;
    }

    public void setPrefix(MobileCoinAPI$TxPrefix mobileCoinAPI$TxPrefix) {
        mobileCoinAPI$TxPrefix.getClass();
        this.prefix_ = mobileCoinAPI$TxPrefix;
    }

    public void setPrefix(MobileCoinAPI$TxPrefix.Builder builder) {
        this.prefix_ = builder.build();
    }

    public void mergePrefix(MobileCoinAPI$TxPrefix mobileCoinAPI$TxPrefix) {
        mobileCoinAPI$TxPrefix.getClass();
        MobileCoinAPI$TxPrefix mobileCoinAPI$TxPrefix2 = this.prefix_;
        if (mobileCoinAPI$TxPrefix2 == null || mobileCoinAPI$TxPrefix2 == MobileCoinAPI$TxPrefix.getDefaultInstance()) {
            this.prefix_ = mobileCoinAPI$TxPrefix;
        } else {
            this.prefix_ = MobileCoinAPI$TxPrefix.newBuilder(this.prefix_).mergeFrom((MobileCoinAPI$TxPrefix.Builder) mobileCoinAPI$TxPrefix).buildPartial();
        }
    }

    public void clearPrefix() {
        this.prefix_ = null;
    }

    public boolean hasSignature() {
        return this.signature_ != null;
    }

    public MobileCoinAPI$SignatureRctBulletproofs getSignature() {
        MobileCoinAPI$SignatureRctBulletproofs mobileCoinAPI$SignatureRctBulletproofs = this.signature_;
        return mobileCoinAPI$SignatureRctBulletproofs == null ? MobileCoinAPI$SignatureRctBulletproofs.getDefaultInstance() : mobileCoinAPI$SignatureRctBulletproofs;
    }

    public void setSignature(MobileCoinAPI$SignatureRctBulletproofs mobileCoinAPI$SignatureRctBulletproofs) {
        mobileCoinAPI$SignatureRctBulletproofs.getClass();
        this.signature_ = mobileCoinAPI$SignatureRctBulletproofs;
    }

    public void setSignature(MobileCoinAPI$SignatureRctBulletproofs.Builder builder) {
        this.signature_ = builder.build();
    }

    public void mergeSignature(MobileCoinAPI$SignatureRctBulletproofs mobileCoinAPI$SignatureRctBulletproofs) {
        mobileCoinAPI$SignatureRctBulletproofs.getClass();
        MobileCoinAPI$SignatureRctBulletproofs mobileCoinAPI$SignatureRctBulletproofs2 = this.signature_;
        if (mobileCoinAPI$SignatureRctBulletproofs2 == null || mobileCoinAPI$SignatureRctBulletproofs2 == MobileCoinAPI$SignatureRctBulletproofs.getDefaultInstance()) {
            this.signature_ = mobileCoinAPI$SignatureRctBulletproofs;
        } else {
            this.signature_ = MobileCoinAPI$SignatureRctBulletproofs.newBuilder(this.signature_).mergeFrom((MobileCoinAPI$SignatureRctBulletproofs.Builder) mobileCoinAPI$SignatureRctBulletproofs).buildPartial();
        }
    }

    public void clearSignature() {
        this.signature_ = null;
    }

    public static MobileCoinAPI$Tx parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$Tx) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static MobileCoinAPI$Tx parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$Tx) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static MobileCoinAPI$Tx parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$Tx) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static MobileCoinAPI$Tx parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$Tx) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static MobileCoinAPI$Tx parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$Tx) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static MobileCoinAPI$Tx parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$Tx) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static MobileCoinAPI$Tx parseFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$Tx) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$Tx parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$Tx) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$Tx parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$Tx) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$Tx parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$Tx) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$Tx parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (MobileCoinAPI$Tx) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static MobileCoinAPI$Tx parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$Tx) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(MobileCoinAPI$Tx mobileCoinAPI$Tx) {
        return DEFAULT_INSTANCE.createBuilder(mobileCoinAPI$Tx);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<MobileCoinAPI$Tx, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(MobileCoinAPI$1 mobileCoinAPI$1) {
            this();
        }

        private Builder() {
            super(MobileCoinAPI$Tx.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (MobileCoinAPI$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new MobileCoinAPI$Tx();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\t\u0002\t", new Object[]{"prefix_", "signature_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<MobileCoinAPI$Tx> parser = PARSER;
                if (parser == null) {
                    synchronized (MobileCoinAPI$Tx.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        MobileCoinAPI$Tx mobileCoinAPI$Tx = new MobileCoinAPI$Tx();
        DEFAULT_INSTANCE = mobileCoinAPI$Tx;
        GeneratedMessageLite.registerDefaultInstance(MobileCoinAPI$Tx.class, mobileCoinAPI$Tx);
    }

    public static MobileCoinAPI$Tx getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<MobileCoinAPI$Tx> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
