package com.mobilecoin.api;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$CompressedRistretto;
import com.mobilecoin.api.MobileCoinAPI$RingMLSAG;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class MobileCoinAPI$SignatureRctBulletproofs extends GeneratedMessageLite<MobileCoinAPI$SignatureRctBulletproofs, Builder> implements MessageLiteOrBuilder {
    private static final MobileCoinAPI$SignatureRctBulletproofs DEFAULT_INSTANCE;
    private static volatile Parser<MobileCoinAPI$SignatureRctBulletproofs> PARSER;
    public static final int PSEUDO_OUTPUT_COMMITMENTS_FIELD_NUMBER;
    public static final int RANGE_PROOFS_FIELD_NUMBER;
    public static final int RING_SIGNATURES_FIELD_NUMBER;
    private Internal.ProtobufList<MobileCoinAPI$CompressedRistretto> pseudoOutputCommitments_ = GeneratedMessageLite.emptyProtobufList();
    private ByteString rangeProofs_ = ByteString.EMPTY;
    private Internal.ProtobufList<MobileCoinAPI$RingMLSAG> ringSignatures_ = GeneratedMessageLite.emptyProtobufList();

    private MobileCoinAPI$SignatureRctBulletproofs() {
    }

    public List<MobileCoinAPI$RingMLSAG> getRingSignaturesList() {
        return this.ringSignatures_;
    }

    public List<? extends MobileCoinAPI$RingMLSAGOrBuilder> getRingSignaturesOrBuilderList() {
        return this.ringSignatures_;
    }

    public int getRingSignaturesCount() {
        return this.ringSignatures_.size();
    }

    public MobileCoinAPI$RingMLSAG getRingSignatures(int i) {
        return this.ringSignatures_.get(i);
    }

    public MobileCoinAPI$RingMLSAGOrBuilder getRingSignaturesOrBuilder(int i) {
        return this.ringSignatures_.get(i);
    }

    private void ensureRingSignaturesIsMutable() {
        if (!this.ringSignatures_.isModifiable()) {
            this.ringSignatures_ = GeneratedMessageLite.mutableCopy(this.ringSignatures_);
        }
    }

    public void setRingSignatures(int i, MobileCoinAPI$RingMLSAG mobileCoinAPI$RingMLSAG) {
        mobileCoinAPI$RingMLSAG.getClass();
        ensureRingSignaturesIsMutable();
        this.ringSignatures_.set(i, mobileCoinAPI$RingMLSAG);
    }

    public void setRingSignatures(int i, MobileCoinAPI$RingMLSAG.Builder builder) {
        ensureRingSignaturesIsMutable();
        this.ringSignatures_.set(i, builder.build());
    }

    public void addRingSignatures(MobileCoinAPI$RingMLSAG mobileCoinAPI$RingMLSAG) {
        mobileCoinAPI$RingMLSAG.getClass();
        ensureRingSignaturesIsMutable();
        this.ringSignatures_.add(mobileCoinAPI$RingMLSAG);
    }

    public void addRingSignatures(int i, MobileCoinAPI$RingMLSAG mobileCoinAPI$RingMLSAG) {
        mobileCoinAPI$RingMLSAG.getClass();
        ensureRingSignaturesIsMutable();
        this.ringSignatures_.add(i, mobileCoinAPI$RingMLSAG);
    }

    public void addRingSignatures(MobileCoinAPI$RingMLSAG.Builder builder) {
        ensureRingSignaturesIsMutable();
        this.ringSignatures_.add(builder.build());
    }

    public void addRingSignatures(int i, MobileCoinAPI$RingMLSAG.Builder builder) {
        ensureRingSignaturesIsMutable();
        this.ringSignatures_.add(i, builder.build());
    }

    public void addAllRingSignatures(Iterable<? extends MobileCoinAPI$RingMLSAG> iterable) {
        ensureRingSignaturesIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.ringSignatures_);
    }

    public void clearRingSignatures() {
        this.ringSignatures_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeRingSignatures(int i) {
        ensureRingSignaturesIsMutable();
        this.ringSignatures_.remove(i);
    }

    public List<MobileCoinAPI$CompressedRistretto> getPseudoOutputCommitmentsList() {
        return this.pseudoOutputCommitments_;
    }

    public List<? extends MobileCoinAPI$CompressedRistrettoOrBuilder> getPseudoOutputCommitmentsOrBuilderList() {
        return this.pseudoOutputCommitments_;
    }

    public int getPseudoOutputCommitmentsCount() {
        return this.pseudoOutputCommitments_.size();
    }

    public MobileCoinAPI$CompressedRistretto getPseudoOutputCommitments(int i) {
        return this.pseudoOutputCommitments_.get(i);
    }

    public MobileCoinAPI$CompressedRistrettoOrBuilder getPseudoOutputCommitmentsOrBuilder(int i) {
        return this.pseudoOutputCommitments_.get(i);
    }

    private void ensurePseudoOutputCommitmentsIsMutable() {
        if (!this.pseudoOutputCommitments_.isModifiable()) {
            this.pseudoOutputCommitments_ = GeneratedMessageLite.mutableCopy(this.pseudoOutputCommitments_);
        }
    }

    public void setPseudoOutputCommitments(int i, MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        ensurePseudoOutputCommitmentsIsMutable();
        this.pseudoOutputCommitments_.set(i, mobileCoinAPI$CompressedRistretto);
    }

    public void setPseudoOutputCommitments(int i, MobileCoinAPI$CompressedRistretto.Builder builder) {
        ensurePseudoOutputCommitmentsIsMutable();
        this.pseudoOutputCommitments_.set(i, builder.build());
    }

    public void addPseudoOutputCommitments(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        ensurePseudoOutputCommitmentsIsMutable();
        this.pseudoOutputCommitments_.add(mobileCoinAPI$CompressedRistretto);
    }

    public void addPseudoOutputCommitments(int i, MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        ensurePseudoOutputCommitmentsIsMutable();
        this.pseudoOutputCommitments_.add(i, mobileCoinAPI$CompressedRistretto);
    }

    public void addPseudoOutputCommitments(MobileCoinAPI$CompressedRistretto.Builder builder) {
        ensurePseudoOutputCommitmentsIsMutable();
        this.pseudoOutputCommitments_.add(builder.build());
    }

    public void addPseudoOutputCommitments(int i, MobileCoinAPI$CompressedRistretto.Builder builder) {
        ensurePseudoOutputCommitmentsIsMutable();
        this.pseudoOutputCommitments_.add(i, builder.build());
    }

    public void addAllPseudoOutputCommitments(Iterable<? extends MobileCoinAPI$CompressedRistretto> iterable) {
        ensurePseudoOutputCommitmentsIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.pseudoOutputCommitments_);
    }

    public void clearPseudoOutputCommitments() {
        this.pseudoOutputCommitments_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removePseudoOutputCommitments(int i) {
        ensurePseudoOutputCommitmentsIsMutable();
        this.pseudoOutputCommitments_.remove(i);
    }

    public ByteString getRangeProofs() {
        return this.rangeProofs_;
    }

    public void setRangeProofs(ByteString byteString) {
        byteString.getClass();
        this.rangeProofs_ = byteString;
    }

    public void clearRangeProofs() {
        this.rangeProofs_ = getDefaultInstance().getRangeProofs();
    }

    public static MobileCoinAPI$SignatureRctBulletproofs parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$SignatureRctBulletproofs) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static MobileCoinAPI$SignatureRctBulletproofs parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$SignatureRctBulletproofs) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static MobileCoinAPI$SignatureRctBulletproofs parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$SignatureRctBulletproofs) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static MobileCoinAPI$SignatureRctBulletproofs parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$SignatureRctBulletproofs) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static MobileCoinAPI$SignatureRctBulletproofs parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$SignatureRctBulletproofs) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static MobileCoinAPI$SignatureRctBulletproofs parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (MobileCoinAPI$SignatureRctBulletproofs) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static MobileCoinAPI$SignatureRctBulletproofs parseFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$SignatureRctBulletproofs) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$SignatureRctBulletproofs parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$SignatureRctBulletproofs) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$SignatureRctBulletproofs parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (MobileCoinAPI$SignatureRctBulletproofs) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static MobileCoinAPI$SignatureRctBulletproofs parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$SignatureRctBulletproofs) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static MobileCoinAPI$SignatureRctBulletproofs parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (MobileCoinAPI$SignatureRctBulletproofs) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static MobileCoinAPI$SignatureRctBulletproofs parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (MobileCoinAPI$SignatureRctBulletproofs) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(MobileCoinAPI$SignatureRctBulletproofs mobileCoinAPI$SignatureRctBulletproofs) {
        return DEFAULT_INSTANCE.createBuilder(mobileCoinAPI$SignatureRctBulletproofs);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<MobileCoinAPI$SignatureRctBulletproofs, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(MobileCoinAPI$1 mobileCoinAPI$1) {
            this();
        }

        private Builder() {
            super(MobileCoinAPI$SignatureRctBulletproofs.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (MobileCoinAPI$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new MobileCoinAPI$SignatureRctBulletproofs();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0002\u0000\u0001\u001b\u0002\u001b\u0003\n", new Object[]{"ringSignatures_", MobileCoinAPI$RingMLSAG.class, "pseudoOutputCommitments_", MobileCoinAPI$CompressedRistretto.class, "rangeProofs_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<MobileCoinAPI$SignatureRctBulletproofs> parser = PARSER;
                if (parser == null) {
                    synchronized (MobileCoinAPI$SignatureRctBulletproofs.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        MobileCoinAPI$SignatureRctBulletproofs mobileCoinAPI$SignatureRctBulletproofs = new MobileCoinAPI$SignatureRctBulletproofs();
        DEFAULT_INSTANCE = mobileCoinAPI$SignatureRctBulletproofs;
        GeneratedMessageLite.registerDefaultInstance(MobileCoinAPI$SignatureRctBulletproofs.class, mobileCoinAPI$SignatureRctBulletproofs);
    }

    public static MobileCoinAPI$SignatureRctBulletproofs getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<MobileCoinAPI$SignatureRctBulletproofs> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
