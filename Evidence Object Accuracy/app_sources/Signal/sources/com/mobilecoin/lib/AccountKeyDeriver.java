package com.mobilecoin.lib;

import com.mobilecoin.lib.exceptions.BadBip39EntropyException;

/* access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public final class AccountKeyDeriver extends Native {
    private static final String TAG;

    private static native long accountKey_from_mnemonic(String str, int i);

    public static AccountKey deriveAccountKeyFromMnemonic(String str, int i) throws BadBip39EntropyException {
        try {
            return AccountKey.fomJNI(accountKey_from_mnemonic(str, i));
        } catch (Exception e) {
            BadBip39EntropyException badBip39EntropyException = new BadBip39EntropyException("Unable to derive ed25519 private key", e);
            Util.logException(TAG, badBip39EntropyException);
            throw badBip39EntropyException;
        }
    }
}
