package com.mobilecoin.lib;

import java.util.Arrays;

/* loaded from: classes3.dex */
public final class RistrettoPrivate extends Native {
    private final byte[] keyBytes = getKeyBytes();

    private native void finalize_jni();

    private native byte[] get_bytes();

    private native long get_public();

    private RistrettoPrivate(long j) {
        this.rustObj = j;
    }

    public static RistrettoPrivate fromJNI(long j) {
        return new RistrettoPrivate(j);
    }

    public byte[] getKeyBytes() {
        try {
            return get_bytes();
        } catch (Exception e) {
            throw new IllegalStateException("BUG: Unable to call FFI getKeyBytes", e);
        }
    }

    public RistrettoPublic getPublicKey() {
        try {
            return RistrettoPublic.fromJNI(get_public());
        } catch (Exception unused) {
            throw new IllegalStateException("BUG: Unable to call FFI getKeyBytes");
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || RistrettoPrivate.class != obj.getClass()) {
            return false;
        }
        return Arrays.equals(this.keyBytes, ((RistrettoPrivate) obj).keyBytes);
    }

    public int hashCode() {
        return Arrays.hashCode(getKeyBytes());
    }

    protected void finalize() throws Throwable {
        if (this.rustObj != 0) {
            finalize_jni();
        }
        super.finalize();
    }
}
