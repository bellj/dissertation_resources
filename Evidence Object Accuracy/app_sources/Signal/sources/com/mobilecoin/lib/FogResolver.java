package com.mobilecoin.lib;

import com.mobilecoin.lib.exceptions.FogReportException;
import com.mobilecoin.lib.log.Logger;

/* loaded from: classes3.dex */
public final class FogResolver extends Native {
    private static final String TAG;

    private native void finalize_jni();

    private native void init_jni(FogReportResponses fogReportResponses, Verifier verifier);

    public FogResolver(FogReportResponses fogReportResponses, Verifier verifier) throws FogReportException {
        try {
            init_jni(fogReportResponses, verifier);
        } catch (Exception e) {
            throw new FogReportException("Unable to create fog report responses container", e);
        }
    }

    protected void finalize() throws Throwable {
        try {
            finalize_jni();
        } catch (Exception e) {
            Logger.e(TAG, "Failed to free fog report data", e, new Object[0]);
        }
        super.finalize();
    }
}
