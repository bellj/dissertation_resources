package com.mobilecoin.lib;

import com.mobilecoin.lib.exceptions.InvalidFogResponse;
import com.mobilecoin.lib.exceptions.TransactionBuilderException;
import com.mobilecoin.lib.log.Logger;

/* loaded from: classes3.dex */
public final class Util extends Native {
    private static final String TAG;

    private static native long recover_onetime_private_key(RistrettoPublic ristrettoPublic, RistrettoPrivate ristrettoPrivate, RistrettoPrivate ristrettoPrivate2);

    private static native byte[] versioned_crypto_box_decrypt(RistrettoPrivate ristrettoPrivate, byte[] bArr);

    public static RistrettoPrivate recoverOnetimePrivateKey(RistrettoPublic ristrettoPublic, RistrettoPrivate ristrettoPrivate, RistrettoPrivate ristrettoPrivate2) throws TransactionBuilderException {
        Logger.i(TAG, "Recovering onetime private key", null, "tx_pub_key:", ristrettoPublic);
        try {
            return RistrettoPrivate.fromJNI(recover_onetime_private_key(ristrettoPublic, ristrettoPrivate, ristrettoPrivate2));
        } catch (Exception e) {
            throw new TransactionBuilderException(e.getLocalizedMessage(), e);
        }
    }

    public static byte[] versionedCryptoBoxDecrypt(RistrettoPrivate ristrettoPrivate, byte[] bArr) throws InvalidFogResponse {
        Logger.i(TAG, "Decrypting with view key", null, "viewKey public:", ristrettoPrivate.getPublicKey());
        try {
            return versioned_crypto_box_decrypt(ristrettoPrivate, bArr);
        } catch (Exception e) {
            throw new InvalidFogResponse(e.getLocalizedMessage(), e);
        }
    }

    public static void logException(String str, Exception exc) {
        String message = exc.getMessage();
        if (message == null) {
            message = "";
        }
        Logger.w(str, message, exc, new Object[0]);
    }
}
