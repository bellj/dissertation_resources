package com.mobilecoin.lib;

import attest.Attest$AuthMessage;
import attest.Attest$Message;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.mobilecoin.lib.ClientConfig;
import com.mobilecoin.lib.exceptions.AttestationException;
import com.mobilecoin.lib.exceptions.InvalidFogResponse;
import com.mobilecoin.lib.exceptions.NetworkException;
import com.mobilecoin.lib.log.Logger;
import com.mobilecoin.lib.uri.FogUri;
import com.mobilecoin.lib.util.NetworkingCall;
import fog_view.FogViewAPIGrpc;
import fog_view.View$QueryRequest;
import fog_view.View$QueryRequestAAD;
import fog_view.View$QueryResponse;
import io.grpc.ManagedChannel;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import java.util.List;
import java.util.concurrent.Callable;

/* loaded from: classes3.dex */
public final class AttestedViewClient extends AttestedClient {
    private static final String TAG;
    private long lastKnownBlockIndex;
    private long lastKnownEventId;

    public AttestedViewClient(FogUri fogUri, ClientConfig.Service service) {
        super(fogUri.getUri(), service);
        Logger.i(TAG, "Created new AttestedViewClient", null, "uri:", fogUri, "verifier:", service);
    }

    @Override // com.mobilecoin.lib.AttestedClient
    protected synchronized void attest(ManagedChannel managedChannel) throws AttestationException, NetworkException {
        try {
            try {
                Logger.i(TAG, "Attest view connection");
                byte[] attestStart = attestStart(getServiceUri());
                attestFinish(getAPIManager().getFogViewAPIStub(managedChannel).auth(Attest$AuthMessage.newBuilder().setData(ByteString.copyFrom(attestStart)).build()).getData().toByteArray(), getServiceConfig().getVerifier());
            } catch (StatusRuntimeException e) {
                attestReset();
                if (e.getStatus().getCode() == Status.Code.INTERNAL) {
                    AttestationException attestationException = new AttestationException(e.getStatus().getDescription(), e);
                    Util.logException(TAG, attestationException);
                    throw attestationException;
                }
                Logger.w(TAG, "Failed to attest the fog view connection", e, new Object[0]);
                throw new NetworkException(e);
            }
        } catch (Exception e2) {
            AttestationException attestationException2 = new AttestationException("Failed to attest the fog view connection", e2);
            Util.logException(TAG, attestationException2);
            throw attestationException2;
        }
    }

    public synchronized View$QueryResponse request(List<byte[]> list) throws InvalidFogResponse, AttestationException, NetworkException {
        FogViewAPIGrpc.FogViewAPIBlockingStub fogViewAPIStub;
        View$QueryRequest.Builder newBuilder;
        View$QueryRequestAAD.Builder newBuilder2;
        Exception e;
        fogViewAPIStub = getAPIManager().getFogViewAPIStub(getManagedChannel());
        newBuilder = View$QueryRequest.newBuilder();
        newBuilder2 = View$QueryRequestAAD.newBuilder();
        if (list != null) {
            Logger.i(TAG, "Requesting outputs from fog view", null, "search keys count:", Integer.valueOf(list.size()));
            for (byte[] bArr : list) {
                newBuilder.addGetTxos(ByteString.copyFrom(bArr));
            }
        }
        newBuilder2.setStartFromUserEventId(this.lastKnownEventId);
        newBuilder2.setStartFromBlockIndex(this.lastKnownBlockIndex);
        try {
        } catch (AttestationException e2) {
            e = e2;
            Util.logException(TAG, e);
            throw e;
        } catch (InvalidFogResponse e3) {
            e = e3;
            Util.logException(TAG, e);
            throw e;
        } catch (NetworkException e4) {
            e = e4;
            Util.logException(TAG, e);
            throw e;
        } catch (RuntimeException e5) {
            e = e5;
            Util.logException(TAG, e);
            throw e;
        } catch (Exception unused) {
            throw new IllegalStateException("BUG: unreachable code");
        }
        return (View$QueryResponse) new NetworkingCall(new Callable(fogViewAPIStub, encryptMessage(newBuilder.build(), newBuilder2.build())) { // from class: com.mobilecoin.lib.AttestedViewClient$$ExternalSyntheticLambda0
            public final /* synthetic */ FogViewAPIGrpc.FogViewAPIBlockingStub f$1;
            public final /* synthetic */ Attest$Message f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return AttestedViewClient.$r8$lambda$dbigo_2mOrZ8ZbtBCn7Fp63K6Ks(AttestedViewClient.this, this.f$1, this.f$2);
            }
        }).run();
    }

    public /* synthetic */ View$QueryResponse lambda$request$0(FogViewAPIGrpc.FogViewAPIBlockingStub fogViewAPIBlockingStub, Attest$Message attest$Message) throws Exception {
        try {
            View$QueryResponse parseFrom = View$QueryResponse.parseFrom(decryptMessage(fogViewAPIBlockingStub.query(attest$Message)).getData());
            this.lastKnownBlockIndex = parseFrom.getHighestProcessedBlockCount();
            this.lastKnownEventId = parseFrom.getNextStartFromUserEventId();
            return parseFrom;
        } catch (InvalidProtocolBufferException e) {
            InvalidFogResponse invalidFogResponse = new InvalidFogResponse("View response contains invalid data", e);
            Util.logException(TAG, invalidFogResponse);
            throw invalidFogResponse;
        } catch (StatusRuntimeException e2) {
            attestReset();
            throw new NetworkException(e2);
        }
    }
}
