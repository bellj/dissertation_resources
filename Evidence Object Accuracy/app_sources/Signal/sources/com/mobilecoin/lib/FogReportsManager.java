package com.mobilecoin.lib;

import com.mobilecoin.lib.ClientConfig;
import com.mobilecoin.lib.exceptions.FogReportException;
import com.mobilecoin.lib.exceptions.MobileCoinException;
import com.mobilecoin.lib.exceptions.NetworkException;
import com.mobilecoin.lib.log.Logger;
import com.mobilecoin.lib.uri.FogUri;
import com.mobilecoin.lib.util.Result;
import com.mobilecoin.lib.util.Task;
import j$.util.Collection$EL;
import j$.util.Optional;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/* loaded from: classes3.dex */
public final class FogReportsManager {
    private static final String TAG;
    private final HashMap<FogUri, ReportResponse> cachedResponses = new HashMap<>();

    synchronized ReportResponse getCachedReportResponse(FogUri fogUri, UnsignedLong unsignedLong) {
        ReportResponse reportResponse;
        String str = TAG;
        Logger.i(str, "Checking for cached report response");
        reportResponse = this.cachedResponses.get(fogUri);
        if (reportResponse != null) {
            Optional min = Collection$EL.stream(reportResponse.getReports()).min(new Comparator() { // from class: com.mobilecoin.lib.FogReportsManager$$ExternalSyntheticLambda0
                @Override // java.util.Comparator
                public final int compare(Object obj, Object obj2) {
                    return FogReportsManager.$r8$lambda$BSUGbUdEEvrKFjHhApHwHVO7asY((Report) obj, (Report) obj2);
                }
            });
            if (min.isPresent() && UnsignedLong.fromLongBits(((Report) min.get()).getPublicKeyExpiry()).compareTo(unsignedLong) < 0) {
                this.cachedResponses.remove(fogUri);
                reportResponse = null;
            }
            Logger.i(str, "Got cached report response", null, "fogUri:", fogUri, "response:", reportResponse);
        } else {
            Logger.i(str, "Found no cached responses");
        }
        return reportResponse;
    }

    public static /* synthetic */ int lambda$getCachedReportResponse$0(Report report2, Report report3) {
        return Long.compare(report2.getPublicKeyExpiry(), report3.getPublicKeyExpiry());
    }

    synchronized void cacheReportResponse(FogUri fogUri, ReportResponse reportResponse) {
        Logger.i(TAG, "Caching report response", null, fogUri, reportResponse);
        this.cachedResponses.put(fogUri, reportResponse);
    }

    public FogReportResponses fetchReports(Set<FogUri> set, UnsignedLong unsignedLong, final ClientConfig.Service service) throws NetworkException, FogReportException {
        Throwable e;
        Throwable e2;
        Logger.i(TAG, "Fetching reports from fogUriList and tombstone block index", null, set, unsignedLong);
        FogReportResponses fogReportResponses = new FogReportResponses();
        ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(Math.min(set.size(), 10));
        HashMap hashMap = new HashMap();
        for (final FogUri fogUri : set) {
            ReportResponse cachedReportResponse = getCachedReportResponse(fogUri, unsignedLong);
            if (cachedReportResponse == null) {
                hashMap.put(fogUri, new Task<ReportResponse, Exception>() { // from class: com.mobilecoin.lib.FogReportsManager.1
                    @Override // com.mobilecoin.lib.util.Task
                    public ReportResponse execute() throws Exception {
                        ReportClient reportClient = new ReportClient(fogUri, service);
                        ReportResponse reports = reportClient.getReports();
                        reportClient.shutdown();
                        return reports;
                    }
                });
            } else {
                fogReportResponses.addResponse(fogUri.getUri(), cachedReportResponse);
            }
        }
        try {
            HashMap hashMap2 = new HashMap();
            for (FogUri fogUri2 : hashMap.keySet()) {
                hashMap2.put(fogUri2, newFixedThreadPool.submit((Callable) hashMap.get(fogUri2)));
            }
            newFixedThreadPool.shutdown();
            newFixedThreadPool.awaitTermination(300, TimeUnit.SECONDS);
            HashMap hashMap3 = new HashMap();
            for (FogUri fogUri3 : hashMap2.keySet()) {
                Future future = (Future) hashMap2.get(fogUri3);
                if (future != null) {
                    hashMap3.put(fogUri3, (Result) future.get());
                }
            }
            for (FogUri fogUri4 : hashMap3.keySet()) {
                Result result = (Result) hashMap3.get(fogUri4);
                if (result == null || !result.isOk()) {
                    throw ((Exception) result.getError());
                }
                fogReportResponses.addResponse(fogUri4.getUri(), (ReportResponse) result.getValue());
                cacheReportResponse(fogUri4, (ReportResponse) result.getValue());
            }
            return fogReportResponses;
        } catch (MobileCoinException e3) {
            e2 = e3;
            FogReportException fogReportException = new FogReportException("Unable to fetch Fog reports", e2);
            Util.logException(TAG, fogReportException);
            throw fogReportException;
        } catch (InterruptedException e4) {
            e = e4;
            NetworkException networkException = new NetworkException(504, "Timeout fetching fog reports", e);
            Util.logException(TAG, networkException);
            throw networkException;
        } catch (RuntimeException e5) {
            e2 = e5;
            FogReportException fogReportException = new FogReportException("Unable to fetch Fog reports", e2);
            Util.logException(TAG, fogReportException);
            throw fogReportException;
        } catch (ExecutionException e6) {
            e = e6;
            NetworkException networkException = new NetworkException(504, "Timeout fetching fog reports", e);
            Util.logException(TAG, networkException);
            throw networkException;
        } catch (Exception e7) {
            Logger.wtf(TAG, "Unexpected exception", e7, new Object[0]);
            throw new IllegalStateException(e7);
        }
    }
}
