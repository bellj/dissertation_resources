package com.mobilecoin.lib.uri;

import android.net.Uri;
import com.mobilecoin.lib.exceptions.InvalidUriException;
import com.mobilecoin.lib.log.Logger;
import java.util.Objects;

/* loaded from: classes3.dex */
public class MobileCoinUri {
    private static final String TAG;
    private final Uri uri;
    private final boolean useTls;

    public MobileCoinUri(Uri uri, MobileCoinScheme mobileCoinScheme) throws InvalidUriException {
        int i;
        Logger.i(TAG, "Getting MobileCoin Uri");
        String scheme = uri.getScheme();
        if (scheme != null) {
            if (scheme.equals(mobileCoinScheme.secureScheme())) {
                this.useTls = true;
            } else if (scheme.equals(mobileCoinScheme.insecureScheme())) {
                this.useTls = false;
            } else {
                throw new InvalidUriException("Unsupported scheme: " + scheme);
            }
            String host = uri.getHost();
            if (host == null || host.length() == 0) {
                throw new InvalidUriException("URI host cannot be empty");
            }
            if (uri.getPort() == -1) {
                Uri.Builder buildUpon = uri.buildUpon();
                StringBuilder sb = new StringBuilder();
                sb.append(host);
                sb.append(":");
                if (this.useTls) {
                    i = mobileCoinScheme.securePort();
                } else {
                    i = mobileCoinScheme.insecurePort();
                }
                sb.append(i);
                uri = buildUpon.encodedAuthority(sb.toString()).build();
            }
            this.uri = uri;
            return;
        }
        throw new InvalidUriException("Invalid URI scheme (null)", new NullPointerException());
    }

    public Uri getUri() {
        return this.uri;
    }

    public String toString() {
        return this.uri.toString();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.uri.equals(((MobileCoinUri) obj).uri);
    }

    public int hashCode() {
        return Objects.hash(getUri());
    }
}
