package com.mobilecoin.lib.uri;

/* loaded from: classes3.dex */
public interface MobileCoinScheme {
    int insecurePort();

    String insecureScheme();

    int securePort();

    String secureScheme();
}
