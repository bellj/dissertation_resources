package com.mobilecoin.lib.uri;

import android.net.Uri;
import com.mobilecoin.lib.exceptions.InvalidUriException;

/* loaded from: classes3.dex */
public final class FogUri extends MobileCoinUri {
    public FogUri(Uri uri) throws InvalidUriException {
        super(uri, new FogUriScheme());
    }
}
