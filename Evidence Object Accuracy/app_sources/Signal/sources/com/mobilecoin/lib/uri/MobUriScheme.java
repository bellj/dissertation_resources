package com.mobilecoin.lib.uri;

/* loaded from: classes3.dex */
public final class MobUriScheme implements MobileCoinScheme {
    @Override // com.mobilecoin.lib.uri.MobileCoinScheme
    public int insecurePort() {
        return -1;
    }

    @Override // com.mobilecoin.lib.uri.MobileCoinScheme
    public String insecureScheme() {
        return "mob";
    }

    public String payloadType() {
        return "b58";
    }

    @Override // com.mobilecoin.lib.uri.MobileCoinScheme
    public int securePort() {
        return -1;
    }

    @Override // com.mobilecoin.lib.uri.MobileCoinScheme
    public String secureScheme() {
        return "mob";
    }
}
