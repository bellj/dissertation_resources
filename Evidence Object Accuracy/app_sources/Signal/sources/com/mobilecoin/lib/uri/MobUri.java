package com.mobilecoin.lib.uri;

import android.net.Uri;
import com.mobilecoin.lib.exceptions.InvalidUriException;
import com.mobilecoin.lib.log.Logger;
import java.util.List;
import java.util.Locale;

/* loaded from: classes3.dex */
public final class MobUri {
    private static final String TAG;
    private static final MobUriScheme mobScheme = new MobUriScheme();
    private final String payload;
    private final Uri uri;

    private MobUri(Uri uri) throws InvalidUriException {
        Logger.i(TAG, "Getting MobUri", null, "mobUri:", uri.toString());
        String scheme = uri.getScheme();
        if (scheme != null) {
            MobUriScheme mobUriScheme = mobScheme;
            if (scheme.equals(mobUriScheme.secureScheme()) || scheme.equals(mobUriScheme.insecureScheme())) {
                List<String> pathSegments = uri.getPathSegments();
                if (pathSegments.size() >= 2) {
                    String str = pathSegments.get(1);
                    if (!pathSegments.get(0).equals(mobUriScheme.payloadType())) {
                        throw new InvalidUriException("Unsupported payload type");
                    } else if (!str.isEmpty()) {
                        this.uri = uri;
                        this.payload = str;
                    } else {
                        throw new InvalidUriException("Empty payload");
                    }
                } else {
                    throw new InvalidUriException("MobUri must have at least two path fragments");
                }
            } else {
                throw new InvalidUriException("Unsupported scheme: " + scheme);
            }
        } else {
            throw new InvalidUriException("Invalid URI scheme (null)", new NullPointerException());
        }
    }

    public MobUri(String str) throws InvalidUriException {
        this(Uri.parse(str));
    }

    public static MobUri fromUri(Uri uri) throws InvalidUriException {
        return new MobUri(uri);
    }

    public static MobUri fromB58(String str) throws InvalidUriException {
        return new MobUri(String.format(Locale.US, "%s:///b58/%s", mobScheme.secureScheme(), str));
    }

    public String getPayload() {
        return this.payload;
    }

    public Uri getUri() {
        return this.uri;
    }

    public String toString() {
        return this.uri.toString();
    }
}
