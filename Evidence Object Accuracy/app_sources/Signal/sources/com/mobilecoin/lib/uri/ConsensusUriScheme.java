package com.mobilecoin.lib.uri;

import org.thoughtcrime.securesms.BuildConfig;

/* loaded from: classes3.dex */
public final class ConsensusUriScheme implements MobileCoinScheme {
    @Override // com.mobilecoin.lib.uri.MobileCoinScheme
    public int insecurePort() {
        return 3223;
    }

    @Override // com.mobilecoin.lib.uri.MobileCoinScheme
    public String insecureScheme() {
        return "insecure-mc";
    }

    @Override // com.mobilecoin.lib.uri.MobileCoinScheme
    public int securePort() {
        return BuildConfig.CONTENT_PROXY_PORT;
    }

    @Override // com.mobilecoin.lib.uri.MobileCoinScheme
    public String secureScheme() {
        return "mc";
    }
}
