package com.mobilecoin.lib.uri;

import org.thoughtcrime.securesms.BuildConfig;

/* loaded from: classes3.dex */
public final class FogUriScheme implements MobileCoinScheme {
    @Override // com.mobilecoin.lib.uri.MobileCoinScheme
    public int insecurePort() {
        return 3225;
    }

    @Override // com.mobilecoin.lib.uri.MobileCoinScheme
    public String insecureScheme() {
        return "insecure-fog";
    }

    @Override // com.mobilecoin.lib.uri.MobileCoinScheme
    public int securePort() {
        return BuildConfig.CONTENT_PROXY_PORT;
    }

    @Override // com.mobilecoin.lib.uri.MobileCoinScheme
    public String secureScheme() {
        return "fog";
    }
}
