package com.mobilecoin.lib;

import com.google.protobuf.InvalidProtocolBufferException;
import com.mobilecoin.api.MobileCoinAPI$RingMLSAG;
import com.mobilecoin.api.MobileCoinAPI$Tx;
import com.mobilecoin.api.MobileCoinAPI$TxOut;
import com.mobilecoin.lib.exceptions.SerializationException;
import com.mobilecoin.lib.log.Logger;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.util.Set;

/* loaded from: classes3.dex */
public final class Transaction extends Native {
    private static final String TAG;
    private final MobileCoinAPI$Tx protoBufTx;
    private byte[] serializedBytes;

    private native byte[] encode();

    private native void finalize_jni();

    private Transaction(long j) throws SerializationException {
        this.rustObj = j;
        try {
            this.protoBufTx = MobileCoinAPI$Tx.parseFrom(toByteArray());
        } catch (InvalidProtocolBufferException e) {
            throw new SerializationException(e.getLocalizedMessage(), e);
        }
    }

    private Transaction(MobileCoinAPI$Tx mobileCoinAPI$Tx) {
        this.protoBufTx = mobileCoinAPI$Tx;
    }

    public static Transaction fromJNI(long j) throws SerializationException {
        return new Transaction(j);
    }

    public static Transaction fromBytes(byte[] bArr) throws SerializationException {
        Logger.i(TAG, "Deserializing transaction from bytes");
        try {
            return fromProtoBufObject(MobileCoinAPI$Tx.parseFrom(bArr));
        } catch (InvalidProtocolBufferException e) {
            throw new SerializationException(e.getLocalizedMessage());
        }
    }

    static Transaction fromProtoBufObject(MobileCoinAPI$Tx mobileCoinAPI$Tx) {
        Logger.i(TAG, "Deserializing transaction from protobuf");
        return new Transaction(mobileCoinAPI$Tx);
    }

    public Set<RistrettoPublic> getOutputPublicKeys() {
        Logger.i(TAG, "Getting output public keys");
        return (Set) Collection$EL.stream(this.protoBufTx.getPrefix().getOutputsList()).map(new Function() { // from class: com.mobilecoin.lib.Transaction$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return Transaction.lambda$getOutputPublicKeys$0((MobileCoinAPI$TxOut) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toSet());
    }

    public static /* synthetic */ RistrettoPublic lambda$getOutputPublicKeys$0(MobileCoinAPI$TxOut mobileCoinAPI$TxOut) {
        try {
            return RistrettoPublic.fromProtoBufObject(mobileCoinAPI$TxOut.getPublicKey());
        } catch (SerializationException e) {
            Logger.e(TAG, "BUG: ");
            throw new IllegalStateException("BUG: type conversion should not trigger serialization exception", e);
        }
    }

    public byte[] toByteArray() throws SerializationException {
        Logger.i(TAG, "Serializing to byte array");
        try {
            if (this.serializedBytes == null) {
                this.serializedBytes = encode();
            }
            return this.serializedBytes;
        } catch (Exception e) {
            throw new SerializationException(e.getLocalizedMessage(), e);
        }
    }

    public synchronized MobileCoinAPI$Tx toProtoBufObject() {
        Logger.i(TAG, "Serializing to protobuf");
        return this.protoBufTx;
    }

    public synchronized Set<KeyImage> getKeyImages() {
        Logger.i(TAG, "Getting key images");
        return (Set) Collection$EL.stream(this.protoBufTx.getSignature().getRingSignaturesList()).map(new Function() { // from class: com.mobilecoin.lib.Transaction$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return Transaction.lambda$getKeyImages$1((MobileCoinAPI$RingMLSAG) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toSet());
    }

    public static /* synthetic */ KeyImage lambda$getKeyImages$1(MobileCoinAPI$RingMLSAG mobileCoinAPI$RingMLSAG) {
        return KeyImage.fromBytes(mobileCoinAPI$RingMLSAG.getKeyImage().getData().toByteArray());
    }

    public long getTombstoneBlockIndex() {
        long tombstoneBlock = toProtoBufObject().getPrefix().getTombstoneBlock();
        Logger.i(TAG, "Getting tombstone block index", null, Long.valueOf(tombstoneBlock));
        return tombstoneBlock;
    }

    protected void finalize() throws Throwable {
        if (this.rustObj != 0) {
            finalize_jni();
        }
        super.finalize();
    }

    /* loaded from: classes3.dex */
    public enum Status {
        UNKNOWN,
        ACCEPTED,
        FAILED;
        
        private UnsignedLong blockIndex;

        public synchronized Status atBlock(UnsignedLong unsignedLong) {
            this.blockIndex = unsignedLong;
            return this;
        }

        public synchronized UnsignedLong getBlockIndex() {
            return this.blockIndex;
        }
    }
}
