package com.mobilecoin.lib;

import com.google.protobuf.ByteString;
import com.mobilecoin.lib.ClientConfig;
import com.mobilecoin.lib.exceptions.AttestationException;
import com.mobilecoin.lib.exceptions.InvalidFogResponse;
import com.mobilecoin.lib.exceptions.NetworkException;
import com.mobilecoin.lib.log.Logger;
import com.mobilecoin.lib.uri.FogUri;
import io.grpc.StatusRuntimeException;
import java.util.ArrayList;
import java.util.List;
import report.ReportOuterClass$Report;
import report.ReportOuterClass$ReportRequest;
import report.ReportOuterClass$ReportResponse;

/* access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public final class ReportClient extends AnyClient {
    private static final String TAG;

    public ReportClient(FogUri fogUri, ClientConfig.Service service) {
        super(fogUri.getUri(), service);
    }

    public synchronized ReportResponse getReports() throws InvalidFogResponse, AttestationException, NetworkException {
        ReportOuterClass$ReportResponse reports;
        ArrayList arrayList;
        byte[][] bArr;
        Logger.i(TAG, "Retrieving the fog public key");
        try {
            reports = getAPIManager().getReportAPIStub(getManagedChannel()).getReports(ReportOuterClass$ReportRequest.newBuilder().build());
            List<ReportOuterClass$Report> reportsList = reports.getReportsList();
            arrayList = new ArrayList();
            for (ReportOuterClass$Report reportOuterClass$Report : reportsList) {
                arrayList.add(Report.fromProtoBuf(reportOuterClass$Report));
            }
            if (!arrayList.isEmpty()) {
                List<ByteString> chainList = reports.getChainList();
                bArr = new byte[chainList.size()];
                for (int i = 0; i < chainList.size(); i++) {
                    bArr[i] = chainList.get(i).toByteArray();
                }
            } else {
                InvalidFogResponse invalidFogResponse = new InvalidFogResponse("No valid reports can be found");
                Util.logException(TAG, invalidFogResponse);
                throw invalidFogResponse;
            }
        } catch (StatusRuntimeException e) {
            Logger.w(TAG, "Error retrieving the fog public key", e, new Object[0]);
            throw new NetworkException(e);
        } catch (Throwable th) {
            Logger.w(TAG, "Error retrieving the fog reports", th, new Object[0]);
            throw new InvalidFogResponse("Unable to retrieve the fog report", th);
        }
        return new ReportResponse(arrayList, bArr, reports.getSignature().toByteArray());
    }
}
