package com.mobilecoin.lib;

import com.google.protobuf.ByteString;
import com.mobilecoin.api.MobileCoinAPI$CompressedRistretto;
import com.mobilecoin.lib.exceptions.SerializationException;
import java.io.Serializable;
import java.util.Arrays;

/* loaded from: classes3.dex */
public final class RistrettoPublic extends Native implements Serializable {
    private MobileCoinAPI$CompressedRistretto compressedRistretto;

    private native void finalize_jni();

    private native byte[] get_bytes();

    private native void init_jni(byte[] bArr);

    private RistrettoPublic(long j) {
        this.rustObj = j;
        try {
            byte[] bArr = get_bytes();
            MobileCoinAPI$CompressedRistretto.Builder newBuilder = MobileCoinAPI$CompressedRistretto.newBuilder();
            newBuilder.setData(ByteString.copyFrom(bArr));
            this.compressedRistretto = newBuilder.build();
        } catch (Exception e) {
            throw new IllegalStateException("BUG: unable to get key bytes from the native code", e);
        }
    }

    private RistrettoPublic(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) throws SerializationException {
        this.compressedRistretto = mobileCoinAPI$CompressedRistretto;
        try {
            init_jni(mobileCoinAPI$CompressedRistretto.getData().toByteArray());
        } catch (Exception e) {
            throw new SerializationException(e.getLocalizedMessage());
        }
    }

    public static RistrettoPublic fromProtoBufObject(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) throws SerializationException {
        return new RistrettoPublic(mobileCoinAPI$CompressedRistretto);
    }

    public static RistrettoPublic fromBytes(byte[] bArr) throws SerializationException {
        return fromProtoBufObject(MobileCoinAPI$CompressedRistretto.newBuilder().setData(ByteString.copyFrom(bArr)).build());
    }

    public static RistrettoPublic fromJNI(long j) {
        return new RistrettoPublic(j);
    }

    public MobileCoinAPI$CompressedRistretto toProtoBufObject() {
        return this.compressedRistretto;
    }

    public byte[] getKeyBytes() {
        return this.compressedRistretto.getData().toByteArray();
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Arrays.hashCode(this.compressedRistretto.getData().toByteArray());
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || RistrettoPublic.class != obj.getClass()) {
            return false;
        }
        return Arrays.equals(this.compressedRistretto.getData().toByteArray(), ((RistrettoPublic) obj).compressedRistretto.getData().toByteArray());
    }

    @Override // java.lang.Object
    protected void finalize() throws Throwable {
        if (this.rustObj != 0) {
            finalize_jni();
        }
        super.finalize();
    }
}
