package com.mobilecoin.lib;

import com.squareup.okhttp.Credentials;
import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.ClientCall;
import io.grpc.ClientInterceptor;
import io.grpc.ForwardingClientCall;
import io.grpc.ForwardingClientCallListener;
import io.grpc.Metadata;
import io.grpc.MethodDescriptor;

/* loaded from: classes3.dex */
public final class AuthInterceptor implements ClientInterceptor {
    static final Metadata.Key<String> AUTHORIZATION_HEADER_KEY = Metadata.Key.of("Authorization", Metadata.ASCII_STRING_MARSHALLER);
    private String authToken;

    public void setAuthorization(String str, String str2) {
        this.authToken = Credentials.basic(str, str2);
    }

    @Override // io.grpc.ClientInterceptor
    public <ReqT, RespT> ClientCall<ReqT, RespT> interceptCall(MethodDescriptor<ReqT, RespT> methodDescriptor, CallOptions callOptions, Channel channel) {
        return new ForwardingClientCall.SimpleForwardingClientCall<ReqT, RespT>(channel.newCall(methodDescriptor, callOptions)) { // from class: com.mobilecoin.lib.AuthInterceptor.1
            @Override // io.grpc.ForwardingClientCall, io.grpc.ClientCall
            public void start(ClientCall.Listener<RespT> listener, Metadata metadata) {
                if (AuthInterceptor.this.authToken != null) {
                    metadata.put(AuthInterceptor.AUTHORIZATION_HEADER_KEY, AuthInterceptor.this.authToken);
                }
                super.start(new ForwardingClientCallListener.SimpleForwardingClientCallListener<RespT>(listener) { // from class: com.mobilecoin.lib.AuthInterceptor.1.1
                    @Override // io.grpc.ForwardingClientCallListener.SimpleForwardingClientCallListener, io.grpc.ForwardingClientCallListener, io.grpc.PartialForwardingClientCallListener, io.grpc.ClientCall.Listener
                    public void onHeaders(Metadata metadata2) {
                        super.onHeaders(metadata2);
                    }
                }, metadata);
            }
        };
    }
}
