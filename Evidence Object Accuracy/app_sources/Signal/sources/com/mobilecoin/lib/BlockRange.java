package com.mobilecoin.lib;

import fog_common.FogCommon$BlockRange;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class BlockRange implements Comparable<BlockRange> {
    private final UnsignedLong end;
    private final UnsignedLong start;

    public BlockRange(UnsignedLong unsignedLong, UnsignedLong unsignedLong2) {
        if (unsignedLong.compareTo(unsignedLong2) <= 0) {
            this.start = unsignedLong;
            this.end = unsignedLong2;
            return;
        }
        throw new IllegalArgumentException("Invalid range");
    }

    BlockRange(long j, long j2) {
        this(UnsignedLong.fromLongBits(j), UnsignedLong.fromLongBits(j2));
    }

    public BlockRange(FogCommon$BlockRange fogCommon$BlockRange) {
        this(fogCommon$BlockRange.getStartBlock(), fogCommon$BlockRange.getEndBlock());
    }

    public UnsignedLong getStart() {
        return this.start;
    }

    public UnsignedLong getEnd() {
        return this.end;
    }

    public FogCommon$BlockRange toProtoBuf() {
        return FogCommon$BlockRange.newBuilder().setStartBlock(this.start.longValue()).setEndBlock(this.end.longValue()).build();
    }

    @Override // java.lang.Object
    public String toString() {
        return "BlockRange{start=" + this.start + ", end=" + this.end + '}';
    }

    public int compareTo(BlockRange blockRange) {
        if (this.start.equals(blockRange.start)) {
            return this.end.compareTo(blockRange.end);
        }
        return this.start.compareTo(blockRange.start);
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Objects.hash(getStart(), getEnd());
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if ((obj instanceof BlockRange) && compareTo((BlockRange) obj) == 0) {
            return true;
        }
        return false;
    }
}
