package com.mobilecoin.lib;

import com.mobilecoin.lib.exceptions.AttestationException;
import com.mobilecoin.lib.log.Logger;

/* loaded from: classes3.dex */
public final class Verifier extends Native {
    private static final String TAG;

    private native void add_mr_enclave(byte[] bArr, String[] strArr, String[] strArr2);

    private native void add_mr_signer(byte[] bArr, short s, short s2, String[] strArr, String[] strArr2);

    private native void finalize_jni();

    private native void init_jni();

    public Verifier() throws AttestationException {
        Logger.i(TAG, "Creating a new Verifier");
        try {
            init_jni();
        } catch (Exception e) {
            Logger.wtf(TAG, "Unable to create a native verifier", e, new Object[0]);
            throw new AttestationException(e.getLocalizedMessage(), e);
        }
    }

    public Verifier withMrSigner(byte[] bArr, short s, short s2, String[] strArr, String[] strArr2) throws AttestationException {
        Logger.i(TAG, "Adding a MrSigner to the Verifier");
        if (strArr == null) {
            try {
                strArr = new String[0];
            } catch (Exception e) {
                Logger.e(TAG, "Unable to add a MrSigner to the verifier", e, new Object[0]);
                throw new AttestationException(e.getLocalizedMessage(), e);
            }
        }
        if (strArr2 == null) {
            strArr2 = new String[0];
        }
        add_mr_signer(bArr, s, s2, strArr, strArr2);
        return this;
    }

    public Verifier withMrEnclave(byte[] bArr, String[] strArr, String[] strArr2) throws AttestationException {
        Logger.i(TAG, "Adding a MrEnclave to the Verifier");
        if (strArr == null) {
            try {
                strArr = new String[0];
            } catch (Exception e) {
                Logger.e(TAG, "Unable to add a MrEnclave to the verifier", e, new Object[0]);
                throw new AttestationException(e.getLocalizedMessage(), e);
            }
        }
        if (strArr2 == null) {
            strArr2 = new String[0];
        }
        add_mr_enclave(bArr, strArr, strArr2);
        return this;
    }

    protected void finalize() throws Throwable {
        if (this.rustObj != 0) {
            finalize_jni();
        }
        super.finalize();
    }
}
