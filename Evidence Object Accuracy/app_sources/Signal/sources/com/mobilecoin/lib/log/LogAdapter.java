package com.mobilecoin.lib.log;

import com.mobilecoin.lib.log.Logger;

/* loaded from: classes3.dex */
public interface LogAdapter {
    boolean isLoggable(Logger.Level level, String str);

    void log(Logger.Level level, String str, String str2, Throwable th, Object... objArr);
}
