package com.mobilecoin.lib.log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/* loaded from: classes3.dex */
public final class Logger {
    private static final ArrayList<LogAdapter> adapters = new ArrayList<>(Collections.singletonList(new DebugLogAdapter()));

    /* loaded from: classes3.dex */
    public enum Level {
        VERBOSE,
        DEBUG,
        INFO,
        WARNING,
        ERROR,
        WTF
    }

    public static synchronized void addAdapter(LogAdapter logAdapter) {
        synchronized (Logger.class) {
            adapters.add(logAdapter);
        }
    }

    public static void i(String str, String str2, Throwable th, Object... objArr) {
        logMessage(Level.INFO, str, str2, th, objArr);
    }

    public static void i(String str, String str2) {
        i(str, str2, null, new Object[0]);
    }

    public static void d(String str, String str2, Throwable th, Object... objArr) {
        logMessage(Level.DEBUG, str, str2, th, objArr);
    }

    public static void d(String str, String str2) {
        d(str, str2, null, new Object[0]);
    }

    public static void w(String str, String str2, Throwable th, Object... objArr) {
        logMessage(Level.WARNING, str, str2, th, objArr);
    }

    public static void w(String str, String str2) {
        w(str, str2, null, new Object[0]);
    }

    public static void e(String str, String str2, Throwable th, Object... objArr) {
        logMessage(Level.ERROR, str, str2, th, objArr);
    }

    public static void e(String str, String str2) {
        e(str, str2, null, new Object[0]);
    }

    public static void wtf(String str, String str2, Throwable th, Object... objArr) {
        logMessage(Level.WTF, str, str2, th, objArr);
    }

    private static synchronized void logMessage(Level level, String str, String str2, Throwable th, Object... objArr) {
        synchronized (Logger.class) {
            Iterator<LogAdapter> it = adapters.iterator();
            while (it.hasNext()) {
                LogAdapter next = it.next();
                if (next.isLoggable(level, str)) {
                    next.log(level, str, str2, th, objArr);
                }
            }
        }
    }
}
