package com.mobilecoin.lib.log;

import android.util.Log;
import com.mobilecoin.lib.log.Logger;

/* loaded from: classes3.dex */
public class DebugLogAdapter implements LogAdapter {
    @Override // com.mobilecoin.lib.log.LogAdapter
    public boolean isLoggable(Logger.Level level, String str) {
        return false;
    }

    /* renamed from: com.mobilecoin.lib.log.DebugLogAdapter$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$mobilecoin$lib$log$Logger$Level;

        static {
            int[] iArr = new int[Logger.Level.values().length];
            $SwitchMap$com$mobilecoin$lib$log$Logger$Level = iArr;
            try {
                iArr[Logger.Level.INFO.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$mobilecoin$lib$log$Logger$Level[Logger.Level.VERBOSE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$mobilecoin$lib$log$Logger$Level[Logger.Level.DEBUG.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$mobilecoin$lib$log$Logger$Level[Logger.Level.WARNING.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$mobilecoin$lib$log$Logger$Level[Logger.Level.ERROR.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$mobilecoin$lib$log$Logger$Level[Logger.Level.WTF.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
        }
    }

    @Override // com.mobilecoin.lib.log.LogAdapter
    public void log(Logger.Level level, String str, String str2, Throwable th, Object... objArr) {
        switch (AnonymousClass1.$SwitchMap$com$mobilecoin$lib$log$Logger$Level[level.ordinal()]) {
            case 1:
                Log.i(str, str2 + printMetadataObjects(objArr), th);
                return;
            case 2:
                Log.v(str, str2 + printMetadataObjects(objArr), th);
                return;
            case 3:
                Log.d(str, str2 + printMetadataObjects(objArr), th);
                return;
            case 4:
                Log.w(str, str2 + printMetadataObjects(objArr), th);
                return;
            case 5:
                Log.e(str, str2 + printMetadataObjects(objArr), th);
                return;
            case 6:
                Log.wtf(str, str2 + printMetadataObjects(objArr), th);
                return;
            default:
                return;
        }
    }

    public String printMetadataObject(Object obj) {
        return obj == null ? "(null)" : obj.toString();
    }

    public String printMetadataObjects(Object... objArr) {
        StringBuilder sb = new StringBuilder();
        for (Object obj : objArr) {
            String printMetadataObject = printMetadataObject(obj);
            sb.append(" ");
            sb.append(printMetadataObject);
        }
        return sb.toString();
    }
}
