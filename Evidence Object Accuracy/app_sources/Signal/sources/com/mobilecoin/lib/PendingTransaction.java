package com.mobilecoin.lib;

import com.mobilecoin.lib.log.Logger;

/* loaded from: classes3.dex */
public final class PendingTransaction {
    private static final String TAG;
    private final Receipt receipt;
    private final Transaction transaction;

    public PendingTransaction(Transaction transaction, Receipt receipt) {
        this.transaction = transaction;
        this.receipt = receipt;
        Logger.i(TAG, "Created PendingTransaction", null, "receipt:", receipt, "transaction:", transaction);
    }

    public Receipt getReceipt() {
        Logger.i(TAG, "Getting receipt", null, this.receipt);
        return this.receipt;
    }

    public Transaction getTransaction() {
        Logger.i(TAG, "Getting transaction", null, this.transaction);
        return this.transaction;
    }
}
