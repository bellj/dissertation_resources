package com.mobilecoin.lib;

import com.mobilecoin.lib.exceptions.AttestationException;
import com.mobilecoin.lib.exceptions.InvalidFogResponse;
import com.mobilecoin.lib.exceptions.KexRngException;
import com.mobilecoin.lib.exceptions.NetworkException;
import com.mobilecoin.lib.log.Logger;
import fog_ledger.Ledger$CheckKeyImagesResponse;
import fog_ledger.Ledger$KeyImageResult;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;

/* loaded from: classes3.dex */
public final class TxOutStore implements Serializable {
    private static final String TAG = TxOutStore.class.getName();
    private AccountKey accountKey;
    private UnsignedLong ledgerBlockIndex;
    private UnsignedLong ledgerTotalTxCount;
    private ConcurrentLinkedQueue<OwnedTxOut> recoveredTxOuts;
    private HashMap<Integer, FogSeed> seeds = new HashMap<>();
    private UnsignedLong viewBlockIndex;

    public TxOutStore(AccountKey accountKey) {
        this.accountKey = accountKey;
        UnsignedLong unsignedLong = UnsignedLong.ZERO;
        this.ledgerBlockIndex = unsignedLong;
        this.viewBlockIndex = unsignedLong;
        this.recoveredTxOuts = new ConcurrentLinkedQueue<>();
    }

    public synchronized Set<OwnedTxOut> getSyncedTxOuts() {
        HashSet hashSet;
        hashSet = new HashSet();
        for (FogSeed fogSeed : this.seeds.values()) {
            for (OwnedTxOut ownedTxOut : fogSeed.getTxOuts()) {
                if (getCurrentBlockIndex().equals(UnsignedLong.ZERO) || ownedTxOut.getReceivedBlockIndex().compareTo(getCurrentBlockIndex()) <= 0) {
                    hashSet.add(ownedTxOut);
                }
            }
        }
        hashSet.addAll((Collection) Collection$EL.stream(this.recoveredTxOuts).filter(new Predicate() { // from class: com.mobilecoin.lib.TxOutStore$$ExternalSyntheticLambda3
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return TxOutStore.this.lambda$getSyncedTxOuts$0((OwnedTxOut) obj);
            }
        }).collect(Collectors.toList()));
        return hashSet;
    }

    public /* synthetic */ boolean lambda$getSyncedTxOuts$0(OwnedTxOut ownedTxOut) {
        return getCurrentBlockIndex().equals(UnsignedLong.ZERO) || ownedTxOut.getReceivedBlockIndex().compareTo(getCurrentBlockIndex()) <= 0;
    }

    public /* synthetic */ boolean lambda$getUnspentTxOuts$1(OwnedTxOut ownedTxOut) {
        return !ownedTxOut.isSpent(getCurrentBlockIndex());
    }

    public Set<OwnedTxOut> getUnspentTxOuts() {
        return (Set) Collection$EL.stream(getSyncedTxOuts()).filter(new Predicate() { // from class: com.mobilecoin.lib.TxOutStore$$ExternalSyntheticLambda2
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return TxOutStore.this.lambda$getUnspentTxOuts$1((OwnedTxOut) obj);
            }
        }).collect(Collectors.toCollection(new AccountSnapshot$$ExternalSyntheticLambda1()));
    }

    public synchronized UnsignedLong getCurrentBlockIndex() {
        UnsignedLong unsignedLong;
        if (this.ledgerBlockIndex.compareTo(this.viewBlockIndex) < 0) {
            unsignedLong = this.ledgerBlockIndex;
        } else {
            unsignedLong = this.viewBlockIndex;
        }
        return unsignedLong;
    }

    public synchronized UnsignedLong getLedgerTotalTxCount() {
        return this.ledgerTotalTxCount;
    }

    public void refresh(AttestedViewClient attestedViewClient, AttestedLedgerClient attestedLedgerClient, FogBlockClient fogBlockClient) throws InvalidFogResponse, NetworkException, AttestationException {
        try {
            Set<BlockRange> updateRNGsAndTxOuts = updateRNGsAndTxOuts(attestedViewClient, new DefaultFogQueryScalingStrategy());
            Optional min = Collection$EL.stream(this.seeds.values()).min(new Comparator() { // from class: com.mobilecoin.lib.TxOutStore$$ExternalSyntheticLambda0
                @Override // java.util.Comparator
                public final int compare(Object obj, Object obj2) {
                    return TxOutStore.lambda$refresh$2((FogSeed) obj, (FogSeed) obj2);
                }
            });
            if (min.isPresent()) {
                this.recoveredTxOuts.addAll(fetchFogMisses((Set) Collection$EL.stream(updateRNGsAndTxOuts).filter(new Predicate() { // from class: com.mobilecoin.lib.TxOutStore$$ExternalSyntheticLambda1
                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate and(Predicate predicate) {
                        return Predicate.CC.$default$and(this, predicate);
                    }

                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate negate() {
                        return Predicate.CC.$default$negate(this);
                    }

                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate or(Predicate predicate) {
                        return Predicate.CC.$default$or(this, predicate);
                    }

                    @Override // j$.util.function.Predicate
                    public final boolean test(Object obj) {
                        return TxOutStore.lambda$refresh$3(UnsignedLong.this, (BlockRange) obj);
                    }
                }).collect(Collectors.toSet()), fogBlockClient));
            }
            updateKeyImages(attestedLedgerClient);
        } catch (KexRngException e) {
            throw new InvalidFogResponse("Invalid KexRng", e);
        }
    }

    public static /* synthetic */ int lambda$refresh$2(FogSeed fogSeed, FogSeed fogSeed2) {
        return fogSeed.getStartBlock().compareTo(fogSeed2.getStartBlock());
    }

    public static /* synthetic */ boolean lambda$refresh$3(UnsignedLong unsignedLong, BlockRange blockRange) {
        return blockRange.getEnd().compareTo(unsignedLong) < 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:75:0x0213, code lost:
        continue;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    synchronized java.util.Set<com.mobilecoin.lib.BlockRange> updateRNGsAndTxOuts(com.mobilecoin.lib.AttestedViewClient r18, com.mobilecoin.lib.FogQueryScalingStrategy r19) throws com.mobilecoin.lib.exceptions.InvalidFogResponse, com.mobilecoin.lib.exceptions.NetworkException, com.mobilecoin.lib.exceptions.AttestationException, com.mobilecoin.lib.exceptions.KexRngException {
        /*
        // Method dump skipped, instructions count: 546
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobilecoin.lib.TxOutStore.updateRNGsAndTxOuts(com.mobilecoin.lib.AttestedViewClient, com.mobilecoin.lib.FogQueryScalingStrategy):java.util.Set");
    }

    void updateTxOutsSpentState(Ledger$CheckKeyImagesResponse ledger$CheckKeyImagesResponse) throws InvalidFogResponse {
        for (Ledger$KeyImageResult ledger$KeyImageResult : ledger$CheckKeyImagesResponse.getResultsList()) {
            if (ledger$KeyImageResult.getKeyImageResultCode() != 2) {
                OwnedTxOut utxoByKeyImage = getUtxoByKeyImage(ledger$KeyImageResult.getKeyImage().getData().toByteArray());
                if (utxoByKeyImage != null) {
                    Date date = null;
                    UnsignedLong fromLongBits = UnsignedLong.fromLongBits(ledger$KeyImageResult.getTimestamp());
                    if (!fromLongBits.equals(UnsignedLong.MAX_VALUE)) {
                        date = new Date(TimeUnit.SECONDS.toMillis(fromLongBits.longValue()));
                    }
                    utxoByKeyImage.setSpent(UnsignedLong.fromLongBits(ledger$KeyImageResult.getSpentAt()), date);
                    String str = TAG;
                    Locale locale = Locale.US;
                    UnsignedLong spentBlockIndex = utxoByKeyImage.getSpentBlockIndex();
                    Objects.requireNonNull(spentBlockIndex);
                    Logger.d(str, String.format(locale, "TxOut has been marked spent in block %s", spentBlockIndex.toString()));
                } else {
                    throw new InvalidFogResponse("checkKeyImages returned invalid key image result");
                }
            }
        }
        synchronized (this) {
            this.ledgerTotalTxCount = UnsignedLong.fromLongBits(ledger$CheckKeyImagesResponse.getGlobalTxoCount());
            this.ledgerBlockIndex = UnsignedLong.fromLongBits(ledger$CheckKeyImagesResponse.getNumBlocks()).sub(UnsignedLong.ONE);
        }
    }

    void updateKeyImages(AttestedLedgerClient attestedLedgerClient) throws InvalidFogResponse, NetworkException, AttestationException {
        Logger.i(TAG, "Checking unspent TXOs key images");
        updateTxOutsSpentState(attestedLedgerClient.checkUtxoKeyImages(getUnspentTxOuts()));
    }

    synchronized Set<OwnedTxOut> fetchFogMisses(Set<BlockRange> set, FogBlockClient fogBlockClient) throws NetworkException {
        HashSet hashSet;
        hashSet = new HashSet();
        for (BlockRange blockRange : set) {
            hashSet.addAll(fogBlockClient.scanForTxOutsInBlockRange(blockRange, this.accountKey));
        }
        return hashSet;
    }

    OwnedTxOut getUtxoByKeyImage(byte[] bArr) {
        int hashCode = Arrays.hashCode(bArr);
        for (OwnedTxOut ownedTxOut : getSyncedTxOuts()) {
            if (ownedTxOut.getKeyImageHashCode() == hashCode) {
                return ownedTxOut;
            }
        }
        return null;
    }
}
