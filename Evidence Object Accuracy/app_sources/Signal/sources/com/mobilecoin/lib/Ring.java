package com.mobilecoin.lib;

import com.mobilecoin.api.MobileCoinAPI$TxOut;
import com.mobilecoin.api.MobileCoinAPI$TxOutMembershipProof;
import com.mobilecoin.lib.exceptions.SerializationException;
import com.mobilecoin.lib.log.Logger;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes3.dex */
public final class Ring {
    private static final String TAG;
    private final List<TxOutMembershipProof> nativeTxOutMembershipProofs;
    private final List<TxOut> nativeTxOuts = new ArrayList();
    private final List<MobileCoinAPI$TxOutMembershipProof> proofs;
    public final short realIndex;
    private final List<MobileCoinAPI$TxOut> txOuts;
    public final OwnedTxOut utxo;

    public Ring(List<MobileCoinAPI$TxOut> list, List<MobileCoinAPI$TxOutMembershipProof> list2, short s, OwnedTxOut ownedTxOut) throws SerializationException {
        Logger.d(TAG, "Initializing ring", null, "txOuts:", list, "proofs:", list2, "realIndex:", Short.valueOf(s), "utxo:", ownedTxOut);
        this.txOuts = list;
        this.proofs = list2;
        this.realIndex = s;
        this.utxo = ownedTxOut;
        for (MobileCoinAPI$TxOut mobileCoinAPI$TxOut : list) {
            this.nativeTxOuts.add(TxOut.fromProtoBufObject(mobileCoinAPI$TxOut));
        }
        this.nativeTxOutMembershipProofs = new ArrayList();
        for (MobileCoinAPI$TxOutMembershipProof mobileCoinAPI$TxOutMembershipProof : this.proofs) {
            this.nativeTxOutMembershipProofs.add(new TxOutMembershipProof(mobileCoinAPI$TxOutMembershipProof.toByteArray()));
        }
    }

    public List<TxOut> getNativeTxOuts() {
        return this.nativeTxOuts;
    }

    public List<TxOutMembershipProof> getNativeTxOutMembershipProofs() {
        return this.nativeTxOutMembershipProofs;
    }
}
