package com.mobilecoin.lib;

import android.net.Uri;
import com.mobilecoin.lib.Receipt;
import com.mobilecoin.lib.exceptions.AttestationException;
import com.mobilecoin.lib.exceptions.FeeRejectedException;
import com.mobilecoin.lib.exceptions.FogReportException;
import com.mobilecoin.lib.exceptions.FragmentedAccountException;
import com.mobilecoin.lib.exceptions.InsufficientFundsException;
import com.mobilecoin.lib.exceptions.InvalidFogResponse;
import com.mobilecoin.lib.exceptions.InvalidReceiptException;
import com.mobilecoin.lib.exceptions.InvalidTransactionException;
import com.mobilecoin.lib.exceptions.InvalidUriException;
import com.mobilecoin.lib.exceptions.NetworkException;
import com.mobilecoin.lib.exceptions.TransactionBuilderException;
import com.mobilecoin.lib.log.LogAdapter;
import com.mobilecoin.lib.log.Logger;
import com.mobilecoin.lib.uri.ConsensusUri;
import com.mobilecoin.lib.uri.FogUri;
import com.mobilecoin.lib.util.Result;
import com.mobilecoin.lib.util.Task;
import consensus_common.ConsensusCommon$ProposeTxResponse;
import fog_ledger.Ledger$OutputResult;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import j$.util.stream.Stream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/* loaded from: classes3.dex */
public final class MobileCoinClient {
    static final BigInteger INPUT_FEE = BigInteger.valueOf(0);
    static final BigInteger OUTPUT_FEE = BigInteger.valueOf(0);
    private static final String TAG = MobileCoinClient.class.toString();
    private final AccountKey accountKey;
    final BlockchainClient blockchainClient;
    private final ClientConfig clientConfig;
    final AttestedConsensusClient consensusClient;
    final FogBlockClient fogBlockClient;
    private final FogReportsManager fogReportsManager = new FogReportsManager();
    final AttestedLedgerClient ledgerClient;
    private final TxOutStore txOutStore;
    final FogUntrustedClient untrustedClient;
    final AttestedViewClient viewClient;

    public MobileCoinClient(AccountKey accountKey, Uri uri, Uri uri2, ClientConfig clientConfig) throws InvalidUriException {
        Logger.i(TAG, "Creating MobileCoinClient");
        this.accountKey = accountKey;
        this.clientConfig = clientConfig;
        clientConfig.getClass();
        FogUri fogUri = new FogUri(uri);
        this.blockchainClient = new BlockchainClient(new ConsensusUri(uri2), clientConfig.consensus, clientConfig.minimumFeeCacheTTL);
        this.viewClient = new AttestedViewClient(fogUri, clientConfig.fogView);
        this.ledgerClient = new AttestedLedgerClient(fogUri, clientConfig.fogLedger);
        this.consensusClient = new AttestedConsensusClient(new ConsensusUri(uri2), clientConfig.consensus);
        this.fogBlockClient = new FogBlockClient(fogUri, clientConfig.fogLedger);
        this.untrustedClient = new FogUntrustedClient(fogUri, clientConfig.fogLedger);
        this.txOutStore = new TxOutStore(accountKey);
        LogAdapter logAdapter = clientConfig.logAdapter;
        if (logAdapter != null) {
            Logger.addAdapter(logAdapter);
        }
    }

    public AccountSnapshot getAccountSnapshot() throws NetworkException, InvalidFogResponse, AttestationException {
        AccountSnapshot accountSnapshot = getAccountSnapshot(UnsignedLong.MAX_VALUE);
        Objects.requireNonNull(accountSnapshot);
        return accountSnapshot;
    }

    public AccountSnapshot getAccountSnapshot(UnsignedLong unsignedLong) throws NetworkException, InvalidFogResponse, AttestationException {
        Logger.i(TAG, "GetAccountSnapshot call");
        TxOutStore txOutStore = getTxOutStore();
        UnsignedLong currentBlockIndex = txOutStore.getCurrentBlockIndex();
        if (currentBlockIndex.compareTo(unsignedLong) < 0) {
            txOutStore.refresh(this.viewClient, this.ledgerClient, this.fogBlockClient);
            currentBlockIndex = txOutStore.getCurrentBlockIndex();
        }
        if (unsignedLong.compareTo(UnsignedLong.MAX_VALUE) < 0 && unsignedLong.compareTo(currentBlockIndex) > 0) {
            return null;
        }
        if (currentBlockIndex.compareTo(unsignedLong) <= 0) {
            unsignedLong = currentBlockIndex;
        }
        return new AccountSnapshot(this, (Set) Collection$EL.stream(txOutStore.getSyncedTxOuts()).filter(new Predicate() { // from class: com.mobilecoin.lib.MobileCoinClient$$ExternalSyntheticLambda0
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return MobileCoinClient.lambda$getAccountSnapshot$0(UnsignedLong.this, (OwnedTxOut) obj);
            }
        }).collect(Collectors.toSet()), unsignedLong);
    }

    public static /* synthetic */ boolean lambda$getAccountSnapshot$0(UnsignedLong unsignedLong, OwnedTxOut ownedTxOut) {
        return ownedTxOut.getReceivedBlockIndex().compareTo(unsignedLong) <= 0;
    }

    public PendingTransaction prepareTransaction(PublicAddress publicAddress, BigInteger bigInteger, BigInteger bigInteger2) throws InsufficientFundsException, FragmentedAccountException, FeeRejectedException, InvalidFogResponse, AttestationException, NetworkException, TransactionBuilderException, FogReportException {
        Logger.i(TAG, "PrepareTransaction call", null, "recipient:", publicAddress, "amount:", bigInteger, "fee:", bigInteger2);
        Set<OwnedTxOut> unspentTxOuts = getUnspentTxOuts();
        BigInteger add = bigInteger.add(bigInteger2);
        Stream map = Collection$EL.stream(unspentTxOuts).map(new Function() { // from class: com.mobilecoin.lib.MobileCoinClient$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((OwnedTxOut) obj).getValue();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        BigInteger bigInteger3 = BigInteger.ZERO;
        if (((BigInteger) map.reduce(bigInteger3, new MobileCoinClient$$ExternalSyntheticLambda2())).compareTo(add) >= 0) {
            return prepareTransaction(publicAddress, bigInteger, UTXOSelector.selectInputsForAmount(unspentTxOuts, add, bigInteger3, bigInteger3, bigInteger3, 0).txOuts, bigInteger2);
        }
        throw new InsufficientFundsException();
    }

    PendingTransaction prepareTransaction(PublicAddress publicAddress, BigInteger bigInteger, final List<OwnedTxOut> list, BigInteger bigInteger2) throws InvalidFogResponse, AttestationException, NetworkException, TransactionBuilderException, FogReportException {
        Exception e;
        Throwable e2;
        String str = TAG;
        Logger.i(str, "PrepareTransaction with TxOuts call", null, "recipient:", publicAddress, "amount:", bigInteger, "fee:", bigInteger2);
        RistrettoPrivate viewKey = this.accountKey.getViewKey();
        final UnsignedLong add = this.txOutStore.getCurrentBlockIndex().add(UnsignedLong.fromLongBits(50));
        final HashSet hashSet = new HashSet();
        try {
            if (publicAddress.hasFogInfo()) {
                hashSet.add(new FogUri(publicAddress.getFogReportUri()));
            }
            hashSet.add(new FogUri(getAccountKey().getFogReportUri()));
            long currentTimeMillis = System.currentTimeMillis();
            AnonymousClass1 r13 = new Task<FogReportResponses, Exception>() { // from class: com.mobilecoin.lib.MobileCoinClient.1
                @Override // com.mobilecoin.lib.util.Task
                public FogReportResponses execute() throws Exception {
                    return MobileCoinClient.this.fogReportsManager.fetchReports(hashSet, add, MobileCoinClient.this.clientConfig.f12report);
                }
            };
            AnonymousClass2 r10 = new Task<List<Ring>, Exception>() { // from class: com.mobilecoin.lib.MobileCoinClient.2
                @Override // com.mobilecoin.lib.util.Task
                public List<Ring> execute() throws Exception {
                    MobileCoinClient mobileCoinClient = MobileCoinClient.this;
                    return mobileCoinClient.getRingsForUTXOs(list, mobileCoinClient.getTxOutStore().getLedgerTotalTxCount());
                }
            };
            ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(2);
            Future submit = newFixedThreadPool.submit(r13);
            Future submit2 = newFixedThreadPool.submit(r10);
            newFixedThreadPool.shutdown();
            try {
                Result result = (Result) submit2.get();
                if (result.isErr()) {
                    Exception exc = (Exception) result.getError();
                    Objects.requireNonNull(exc);
                    Exception exc2 = exc;
                    throw exc;
                } else if (result.isOk()) {
                    List list2 = (List) result.getValue();
                    Objects.requireNonNull(list2);
                    List<Ring> list3 = list2;
                    Result result2 = (Result) submit.get();
                    if (result2.isErr()) {
                        Exception exc3 = (Exception) result2.getError();
                        Objects.requireNonNull(exc3);
                        Exception exc4 = exc3;
                        throw exc3;
                    } else if (result2.isOk()) {
                        FogReportResponses fogReportResponses = (FogReportResponses) result2.getValue();
                        Objects.requireNonNull(fogReportResponses);
                        long currentTimeMillis2 = System.currentTimeMillis();
                        Logger.d(str, "Report + Rings fetch time: " + (currentTimeMillis2 - currentTimeMillis) + "ms");
                        TransactionBuilder transactionBuilder = new TransactionBuilder(new FogResolver(fogReportResponses, this.clientConfig.f12report.getVerifier()));
                        BigInteger valueOf = BigInteger.valueOf(0);
                        for (Ring ring : list3) {
                            OwnedTxOut ownedTxOut = ring.utxo;
                            valueOf = valueOf.add(ownedTxOut.getValue());
                            transactionBuilder.addInput(ring.getNativeTxOuts(), ring.getNativeTxOutMembershipProofs(), ring.realIndex, Util.recoverOnetimePrivateKey(ownedTxOut.getPublicKey(), viewKey, this.accountKey.getSubAddressSpendKey()), viewKey);
                        }
                        byte[] bArr = new byte[32];
                        TxOut addOutput = transactionBuilder.addOutput(bigInteger, publicAddress, bArr);
                        BigInteger add2 = bigInteger.add(bigInteger2);
                        if (valueOf.compareTo(add2) > 0) {
                            transactionBuilder.addOutput(valueOf.subtract(add2), this.accountKey.getPublicAddress(), null);
                        }
                        transactionBuilder.setTombstoneBlockIndex(add);
                        transactionBuilder.setFee(bigInteger2.longValue());
                        return new PendingTransaction(transactionBuilder.build(), new Receipt(addOutput.getPubKey(), bArr, addOutput.getAmount(), add));
                    } else {
                        throw new InvalidFogResponse("Unable to retrieve Fog Reports");
                    }
                } else {
                    throw new InvalidFogResponse("Unable to retrieve Rings");
                }
            } catch (AttestationException e3) {
                e = e3;
                Util.logException(TAG, e);
                throw e;
            } catch (FogReportException e4) {
                e = e4;
                Util.logException(TAG, e);
                throw e;
            } catch (InvalidFogResponse e5) {
                e = e5;
                Util.logException(TAG, e);
                throw e;
            } catch (NetworkException e6) {
                e = e6;
                Util.logException(TAG, e);
                throw e;
            } catch (InterruptedException e7) {
                e2 = e7;
                NetworkException networkException = new NetworkException(504, "Timeout fetching fog reports", e2);
                Util.logException(TAG, networkException);
                throw networkException;
            } catch (ExecutionException e8) {
                e2 = e8;
                NetworkException networkException = new NetworkException(504, "Timeout fetching fog reports", e2);
                Util.logException(TAG, networkException);
                throw networkException;
            } catch (Exception e9) {
                Logger.wtf(TAG, "Bug: Unexpected exception", e9, new Object[0]);
                throw new IllegalStateException(e9);
            }
        } catch (InvalidUriException unused) {
            FogReportException fogReportException = new FogReportException("Invalid Fog Report Uri in the public address");
            Util.logException(TAG, fogReportException);
            throw fogReportException;
        }
    }

    public void submitTransaction(Transaction transaction) throws InvalidTransactionException, NetworkException, AttestationException {
        String str = TAG;
        Logger.i(str, "SubmitTransaction call", null, "transaction:", transaction);
        ConsensusCommon$ProposeTxResponse proposeTx = this.consensusClient.proposeTx(transaction.toProtoBufObject());
        if (proposeTx.getResult().getNumber() != 0) {
            this.blockchainClient.resetCache();
            InvalidTransactionException invalidTransactionException = new InvalidTransactionException(proposeTx.getResult().toString());
            Util.logException(str, invalidTransactionException);
            throw invalidTransactionException;
        }
    }

    public Receipt.Status getReceiptStatus(Receipt receipt) throws InvalidFogResponse, NetworkException, AttestationException, InvalidReceiptException {
        Logger.i(TAG, "GetReceiptStatus call");
        return getAccountSnapshot().getReceiptStatus(receipt);
    }

    public BigInteger estimateTotalFee(BigInteger bigInteger) throws InsufficientFundsException, NetworkException, InvalidFogResponse, AttestationException {
        Logger.i(TAG, "EstimateTotalFee call");
        return UTXOSelector.calculateFee(getUnspentTxOuts(), bigInteger, getOrFetchMinimumTxFee(), INPUT_FEE, OUTPUT_FEE, 2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0085, code lost:
        com.mobilecoin.lib.log.Logger.w(com.mobilecoin.lib.MobileCoinClient.TAG, "Exceeded waiting time for the transaction to post");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0091, code lost:
        throw new java.util.concurrent.TimeoutException();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void defragmentAccount(java.math.BigInteger r9, com.mobilecoin.lib.DefragmentationDelegate r10) throws com.mobilecoin.lib.exceptions.InvalidFogResponse, com.mobilecoin.lib.exceptions.AttestationException, com.mobilecoin.lib.exceptions.NetworkException, com.mobilecoin.lib.exceptions.InsufficientFundsException, com.mobilecoin.lib.exceptions.TransactionBuilderException, com.mobilecoin.lib.exceptions.InvalidTransactionException, com.mobilecoin.lib.exceptions.FogReportException, java.util.concurrent.TimeoutException {
        /*
            r8 = this;
            r10.onStart()
            r0 = 0
        L_0x0004:
            java.util.Set r7 = r8.getUnspentTxOuts()
            java.math.BigInteger r3 = r8.getOrFetchMinimumTxFee()     // Catch: FragmentedAccountException -> 0x0019
            java.math.BigInteger r4 = com.mobilecoin.lib.MobileCoinClient.INPUT_FEE     // Catch: FragmentedAccountException -> 0x0019
            java.math.BigInteger r5 = com.mobilecoin.lib.MobileCoinClient.OUTPUT_FEE     // Catch: FragmentedAccountException -> 0x0019
            r6 = 1
            r1 = r7
            r2 = r9
            com.mobilecoin.lib.UTXOSelector$Selection r0 = com.mobilecoin.lib.UTXOSelector.selectInputsForAmount(r1, r2, r3, r4, r5, r6)     // Catch: FragmentedAccountException -> 0x0019
            goto L_0x0096
        L_0x0019:
            java.math.BigInteger r1 = r8.getOrFetchMinimumTxFee()
            java.math.BigInteger r2 = com.mobilecoin.lib.MobileCoinClient.INPUT_FEE
            java.math.BigInteger r3 = com.mobilecoin.lib.MobileCoinClient.OUTPUT_FEE
            com.mobilecoin.lib.UTXOSelector$Selection r1 = com.mobilecoin.lib.UTXOSelector.selectInputsForMerging(r7, r1, r2, r3)
            java.math.BigInteger r2 = java.math.BigInteger.ZERO
            java.util.List<T> r3 = r1.txOuts
            java.util.Iterator r3 = r3.iterator()
        L_0x002d:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0042
            java.lang.Object r4 = r3.next()
            com.mobilecoin.lib.OwnedTxOut r4 = (com.mobilecoin.lib.OwnedTxOut) r4
            java.math.BigInteger r4 = r4.getValue()
            java.math.BigInteger r2 = r2.add(r4)
            goto L_0x002d
        L_0x0042:
            com.mobilecoin.lib.AccountKey r3 = r8.accountKey
            com.mobilecoin.lib.PublicAddress r3 = r3.getPublicAddress()
            java.math.BigInteger r4 = r1.fee
            java.math.BigInteger r2 = r2.subtract(r4)
            java.util.List<T> r4 = r1.txOuts
            java.math.BigInteger r5 = r1.fee
            com.mobilecoin.lib.PendingTransaction r2 = r8.prepareTransaction(r3, r2, r4, r5)
            java.math.BigInteger r1 = r1.fee
            boolean r1 = r10.onStepReady(r2, r1)
            if (r1 != 0) goto L_0x0062
            r10.onCancel()
            return
        L_0x0062:
            r1 = 0
            r3 = 0
        L_0x0064:
            com.mobilecoin.lib.Receipt r4 = r2.getReceipt()     // Catch: InvalidReceiptException -> 0x00a4
            com.mobilecoin.lib.Receipt$Status r4 = r8.getReceiptStatus(r4)     // Catch: InvalidReceiptException -> 0x00a4
            com.mobilecoin.lib.Receipt$Status r5 = com.mobilecoin.lib.Receipt.Status.UNKNOWN     // Catch: InvalidReceiptException -> 0x00a4
            if (r4 != r5) goto L_0x0092
            int r4 = r3 + 1
            r5 = 300(0x12c, float:4.2E-43)
            if (r3 == r5) goto L_0x0085
            r5 = 1000(0x3e8, double:4.94E-321)
            java.lang.Thread.sleep(r5)     // Catch: InterruptedException -> 0x007c, InvalidReceiptException -> 0x00a4
            goto L_0x0083
        L_0x007c:
            java.lang.String r3 = com.mobilecoin.lib.MobileCoinClient.TAG     // Catch: InvalidReceiptException -> 0x00a4
            java.lang.String r5 = "Sleep interruption during defragmentation"
            com.mobilecoin.lib.log.Logger.w(r3, r5)     // Catch: InvalidReceiptException -> 0x00a4
        L_0x0083:
            r3 = r4
            goto L_0x0064
        L_0x0085:
            java.lang.String r9 = com.mobilecoin.lib.MobileCoinClient.TAG     // Catch: InvalidReceiptException -> 0x00a4
            java.lang.String r10 = "Exceeded waiting time for the transaction to post"
            com.mobilecoin.lib.log.Logger.w(r9, r10)     // Catch: InvalidReceiptException -> 0x00a4
            java.util.concurrent.TimeoutException r9 = new java.util.concurrent.TimeoutException     // Catch: InvalidReceiptException -> 0x00a4
            r9.<init>()     // Catch: InvalidReceiptException -> 0x00a4
            throw r9     // Catch: InvalidReceiptException -> 0x00a4
        L_0x0092:
            com.mobilecoin.lib.Receipt$Status r1 = com.mobilecoin.lib.Receipt.Status.FAILED
            if (r4 == r1) goto L_0x009c
        L_0x0096:
            if (r0 == 0) goto L_0x0004
            r10.onComplete()
            return
        L_0x009c:
            com.mobilecoin.lib.exceptions.InvalidTransactionException r9 = new com.mobilecoin.lib.exceptions.InvalidTransactionException
            java.lang.String r10 = "Defrag step transaction has failed"
            r9.<init>(r10)
            throw r9
        L_0x00a4:
            r9 = move-exception
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            r10.<init>(r9)
            java.lang.String r9 = com.mobilecoin.lib.MobileCoinClient.TAG
            java.lang.Object[] r0 = new java.lang.Object[r1]
            java.lang.String r1 = "BUG: unreachable code"
            com.mobilecoin.lib.log.Logger.e(r9, r1, r10, r0)
            goto L_0x00b5
        L_0x00b4:
            throw r10
        L_0x00b5:
            goto L_0x00b4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobilecoin.lib.MobileCoinClient.defragmentAccount(java.math.BigInteger, com.mobilecoin.lib.DefragmentationDelegate):void");
    }

    TxOutStore getTxOutStore() {
        return this.txOutStore;
    }

    Set<OwnedTxOut> getUnspentTxOuts() throws InvalidFogResponse, NetworkException, AttestationException {
        getTxOutStore().refresh(this.viewClient, this.ledgerClient, this.fogBlockClient);
        return getTxOutStore().getUnspentTxOuts();
    }

    public BigInteger getOrFetchMinimumTxFee() throws NetworkException {
        return this.blockchainClient.getOrFetchMinimumFee().toBigInteger();
    }

    List<Ring> getRingsForUTXOs(List<OwnedTxOut> list, UnsignedLong unsignedLong) throws InvalidFogResponse, NetworkException, AttestationException {
        Object obj;
        HashSet hashSet = new HashSet();
        for (OwnedTxOut ownedTxOut : list) {
            if (!hashSet.add(ownedTxOut.getTxOutGlobalIndex())) {
                throw new IllegalStateException("utxos contains non-unique indices");
            }
        }
        int size = list.size() * 11;
        if (size <= unsignedLong.intValue()) {
            HashSet hashSet2 = new HashSet(hashSet);
            Random random = new Random();
            while (hashSet.size() != size) {
                hashSet.add(UnsignedLong.valueOf(Math.abs(random.nextLong())).remainder(unsignedLong));
            }
            List<Ledger$OutputResult> resultsList = this.ledgerClient.getOutputs(new ArrayList(hashSet), 0).getResultsList();
            if (resultsList.size() == size) {
                HashMap hashMap = new HashMap();
                ArrayList arrayList = new ArrayList();
                for (Ledger$OutputResult ledger$OutputResult : resultsList) {
                    if (hashSet2.contains(UnsignedLong.fromLongBits(ledger$OutputResult.getIndex()))) {
                        hashMap.put(UnsignedLong.fromLongBits(ledger$OutputResult.getIndex()), ledger$OutputResult);
                    } else {
                        arrayList.add(ledger$OutputResult);
                    }
                }
                if (hashSet2.size() == list.size()) {
                    ArrayList arrayList2 = new ArrayList();
                    for (OwnedTxOut ownedTxOut2 : list) {
                        short nextInt = (short) random.nextInt(11);
                        ArrayList arrayList3 = new ArrayList();
                        ArrayList arrayList4 = new ArrayList();
                        for (int i = 0; i < 11; i++) {
                            if (i == nextInt) {
                                obj = hashMap.get(ownedTxOut2.getTxOutGlobalIndex());
                            } else {
                                obj = arrayList.remove(0);
                            }
                            Ledger$OutputResult ledger$OutputResult2 = (Ledger$OutputResult) obj;
                            arrayList3.add(ledger$OutputResult2.getOutput());
                            arrayList4.add(ledger$OutputResult2.getProof());
                        }
                        try {
                            arrayList2.add(new Ring(arrayList3, arrayList4, nextInt, ownedTxOut2));
                        } catch (Exception unused) {
                            throw new InvalidFogResponse("Unable to decode rings");
                        }
                    }
                    if (arrayList.isEmpty()) {
                        return arrayList2;
                    }
                    throw new IllegalStateException("BUG: Not all rings consumed");
                }
                throw new IllegalStateException("BUG");
            }
            throw new InvalidFogResponse("getOutputs returned incorrect number of outputs");
        }
        throw new InvalidFogResponse("Ledger does not contain enough outputs");
    }

    public final AccountKey getAccountKey() {
        return this.accountKey;
    }

    public void setFogBasicAuthorization(String str, String str2) {
        this.viewClient.setAuthorization(str, str2);
        this.ledgerClient.setAuthorization(str, str2);
        this.fogBlockClient.setAuthorization(str, str2);
        this.untrustedClient.setAuthorization(str, str2);
    }

    public synchronized void shutdown() {
        AttestedViewClient attestedViewClient = this.viewClient;
        if (attestedViewClient != null) {
            attestedViewClient.shutdown();
        }
        AttestedLedgerClient attestedLedgerClient = this.ledgerClient;
        if (attestedLedgerClient != null) {
            attestedLedgerClient.shutdown();
        }
        AttestedConsensusClient attestedConsensusClient = this.consensusClient;
        if (attestedConsensusClient != null) {
            attestedConsensusClient.shutdown();
        }
        FogBlockClient fogBlockClient = this.fogBlockClient;
        if (fogBlockClient != null) {
            fogBlockClient.shutdown();
        }
        BlockchainClient blockchainClient = this.blockchainClient;
        if (blockchainClient != null) {
            blockchainClient.shutdown();
        }
    }

    protected void finalize() throws Throwable {
        shutdown();
        super.finalize();
    }
}
