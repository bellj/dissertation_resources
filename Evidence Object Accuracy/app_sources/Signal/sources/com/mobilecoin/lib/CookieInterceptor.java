package com.mobilecoin.lib;

import com.mobilecoin.lib.log.Logger;
import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.ClientCall;
import io.grpc.ClientInterceptor;
import io.grpc.ForwardingClientCall;
import io.grpc.ForwardingClientCallListener;
import io.grpc.Metadata;
import io.grpc.MethodDescriptor;

/* loaded from: classes3.dex */
public final class CookieInterceptor implements ClientInterceptor {
    static final Metadata.Key<String> COOKIE_HEADER_KEY;
    static final Metadata.Key<String> SET_COOKIE_HEADER_KEY;
    private static final String TAG;
    private String serviceCookie;

    static {
        Metadata.AsciiMarshaller<String> asciiMarshaller = Metadata.ASCII_STRING_MARSHALLER;
        SET_COOKIE_HEADER_KEY = Metadata.Key.of("set-cookie", asciiMarshaller);
        COOKIE_HEADER_KEY = Metadata.Key.of("cookie", asciiMarshaller);
    }

    @Override // io.grpc.ClientInterceptor
    public <ReqT, RespT> ClientCall<ReqT, RespT> interceptCall(MethodDescriptor<ReqT, RespT> methodDescriptor, CallOptions callOptions, Channel channel) {
        Logger.i(TAG, "Intercepting client call");
        return new ForwardingClientCall.SimpleForwardingClientCall<ReqT, RespT>(channel.newCall(methodDescriptor, callOptions)) { // from class: com.mobilecoin.lib.CookieInterceptor.1
            @Override // io.grpc.ForwardingClientCall, io.grpc.ClientCall
            public void start(ClientCall.Listener<RespT> listener, Metadata metadata) {
                String serviceCookie = CookieInterceptor.this.getServiceCookie();
                if (serviceCookie != null) {
                    metadata.put(CookieInterceptor.COOKIE_HEADER_KEY, serviceCookie);
                }
                super.start(new ForwardingClientCallListener.SimpleForwardingClientCallListener<RespT>(listener) { // from class: com.mobilecoin.lib.CookieInterceptor.1.1
                    @Override // io.grpc.ForwardingClientCallListener.SimpleForwardingClientCallListener, io.grpc.ForwardingClientCallListener, io.grpc.PartialForwardingClientCallListener, io.grpc.ClientCall.Listener
                    public void onHeaders(Metadata metadata2) {
                        Metadata.Key<String> key = CookieInterceptor.SET_COOKIE_HEADER_KEY;
                        if (metadata2.containsKey(key)) {
                            CookieInterceptor.this.setServiceCookie((String) metadata2.get(key));
                        }
                        super.onHeaders(metadata2);
                    }
                }, metadata);
            }
        };
    }

    synchronized String getServiceCookie() {
        return this.serviceCookie;
    }

    synchronized void setServiceCookie(String str) {
        this.serviceCookie = str;
    }
}
