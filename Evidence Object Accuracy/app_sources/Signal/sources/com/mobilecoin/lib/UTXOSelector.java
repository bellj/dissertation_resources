package com.mobilecoin.lib;

import com.mobilecoin.lib.UTXOSelector;
import com.mobilecoin.lib.exceptions.FragmentedAccountException;
import com.mobilecoin.lib.exceptions.InsufficientFundsException;
import com.mobilecoin.lib.log.Logger;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.function.Supplier;
import j$.util.stream.Collectors;
import j$.util.stream.Stream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

/* loaded from: classes3.dex */
public final class UTXOSelector {
    private static final String TAG;

    public static Selection<OwnedTxOut> selectInputsForMerging(Set<OwnedTxOut> set, BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3) throws InsufficientFundsException {
        Logger.i(TAG, "Selecting inputs for merging");
        TxOutNode selectTxOutNodesForMerging = selectTxOutNodesForMerging((List) Collection$EL.stream(set).map(new Function() { // from class: com.mobilecoin.lib.UTXOSelector$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return UTXOSelector.lambda$selectInputsForMerging$0((OwnedTxOut) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList()), bigInteger, bigInteger2, bigInteger3);
        return new Selection<>((List) Collection$EL.stream(selectTxOutNodesForMerging.children).map(new Function() { // from class: com.mobilecoin.lib.UTXOSelector$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((UTXOSelector.TxOutNode) obj).txOut;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList()), selectTxOutNodesForMerging.getFee(bigInteger, bigInteger2));
    }

    public static /* synthetic */ TxOutNode lambda$selectInputsForMerging$0(OwnedTxOut ownedTxOut) {
        return new TxOutNode(ownedTxOut, null);
    }

    static TxOutNode selectTxOutNodesForMerging(List<TxOutNode> list, BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3) throws InsufficientFundsException {
        Logger.i(TAG, "Selecting TxOutNodes for merging");
        Stream map = Collection$EL.stream(list).map(new UTXOSelector$$ExternalSyntheticLambda4());
        BigInteger bigInteger4 = BigInteger.ZERO;
        BigInteger bigInteger5 = (BigInteger) map.reduce(bigInteger4, new MobileCoinClient$$ExternalSyntheticLambda2());
        ArrayList arrayList = new ArrayList();
        BigInteger add = bigInteger.add(bigInteger3);
        for (TxOutNode txOutNode : (List) Collection$EL.stream(list).sorted(new Comparator() { // from class: com.mobilecoin.lib.UTXOSelector$$ExternalSyntheticLambda12
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return UTXOSelector.lambda$selectTxOutNodesForMerging$2((UTXOSelector.TxOutNode) obj, (UTXOSelector.TxOutNode) obj2);
            }
        }).collect(Collectors.toList())) {
            if (arrayList.size() == 16) {
                break;
            }
            bigInteger4 = bigInteger4.add(txOutNode.getValue());
            BigInteger add2 = add.add(txOutNode.getFee(bigInteger, bigInteger2));
            if (bigInteger4.add(add).compareTo(bigInteger5) > 0) {
                break;
            }
            arrayList.add(txOutNode);
            add = add2;
        }
        if (arrayList.size() >= 2) {
            return new TxOutNode(null, arrayList);
        }
        throw new InsufficientFundsException();
    }

    public static /* synthetic */ int lambda$selectTxOutNodesForMerging$2(TxOutNode txOutNode, TxOutNode txOutNode2) {
        return txOutNode2.getValue().compareTo(txOutNode.getValue());
    }

    public static Selection<OwnedTxOut> selectInputsForAmount(Set<OwnedTxOut> set, BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, BigInteger bigInteger4, int i) throws InsufficientFundsException, FragmentedAccountException {
        Logger.i(TAG, "Selecting inputs for amount", null, "amount:", bigInteger, "txFee:", bigInteger2, "inputFee:", bigInteger3, "outputFee:", bigInteger4);
        Selection<TxOutNode> selectTxOutNodesForAmount = selectTxOutNodesForAmount((List) Collection$EL.stream(set).map(new Function() { // from class: com.mobilecoin.lib.UTXOSelector$$ExternalSyntheticLambda5
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return UTXOSelector.lambda$selectInputsForAmount$3((OwnedTxOut) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList()), bigInteger, bigInteger2, bigInteger3, bigInteger4, i);
        return new Selection<>((List) Collection$EL.stream(selectTxOutNodesForAmount.txOuts).map(new Function() { // from class: com.mobilecoin.lib.UTXOSelector$$ExternalSyntheticLambda6
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((UTXOSelector.TxOutNode) obj).txOut;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList()), selectTxOutNodesForAmount.fee);
    }

    public static /* synthetic */ TxOutNode lambda$selectInputsForAmount$3(OwnedTxOut ownedTxOut) {
        return new TxOutNode(ownedTxOut, null);
    }

    static Selection<TxOutNode> selectTxOutNodesForAmount(List<TxOutNode> list, BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, BigInteger bigInteger4, int i) throws InsufficientFundsException, FragmentedAccountException {
        Logger.i(TAG, "Selecting TxOutNodes for amount", null, "amount:", bigInteger, "txFee:", bigInteger2, "inputFee:", bigInteger3, "outputFee:", bigInteger4);
        BigInteger bigInteger5 = (BigInteger) Collection$EL.stream(list).map(new UTXOSelector$$ExternalSyntheticLambda4()).reduce(BigInteger.ZERO, new MobileCoinClient$$ExternalSyntheticLambda2());
        if (list.size() != 0) {
            Vector vector = (Vector) Collection$EL.stream(list).sorted(new Comparator() { // from class: com.mobilecoin.lib.UTXOSelector$$ExternalSyntheticLambda10
                @Override // java.util.Comparator
                public final int compare(Object obj, Object obj2) {
                    return UTXOSelector.lambda$selectTxOutNodesForAmount$5((UTXOSelector.TxOutNode) obj, (UTXOSelector.TxOutNode) obj2);
                }
            }).collect(Collectors.toCollection(new Supplier() { // from class: com.mobilecoin.lib.UTXOSelector$$ExternalSyntheticLambda11
                @Override // j$.util.function.Supplier
                public final Object get() {
                    return new Vector();
                }
            }));
            BigInteger add = bigInteger2.add(BigInteger.valueOf((long) i).multiply(bigInteger4));
            ArrayList arrayList = new ArrayList();
            TxOutNode txOutNode = (TxOutNode) vector.firstElement();
            BigInteger add2 = add.add(txOutNode.getFee(bigInteger2, bigInteger3));
            arrayList.add(txOutNode);
            vector.removeElement(txOutNode);
            BigInteger value = txOutNode.getValue();
            while (vector.size() > 0 && arrayList.size() < 16 && value.compareTo(bigInteger.add(add2)) < 0) {
                TxOutNode txOutNode2 = (TxOutNode) vector.lastElement();
                arrayList.add(txOutNode2);
                add2 = add2.add(txOutNode2.getFee(bigInteger2, bigInteger3));
                vector.removeElement(txOutNode2);
                value = value.add(txOutNode2.getValue());
            }
            if (bigInteger5.compareTo(bigInteger.add(add2)) < 0) {
                throw new InsufficientFundsException();
            } else if (value.compareTo(bigInteger.add(add2)) >= 0) {
                return new Selection<>(arrayList, add2);
            } else {
                throw new FragmentedAccountException("The account requires defragmentation to send the required amount");
            }
        } else {
            throw new InsufficientFundsException();
        }
    }

    public static /* synthetic */ int lambda$selectTxOutNodesForAmount$5(TxOutNode txOutNode, TxOutNode txOutNode2) {
        return txOutNode.getValue().compareTo(txOutNode2.getValue());
    }

    public static BigInteger calculateFee(Set<OwnedTxOut> set, BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, BigInteger bigInteger4, int i) throws InsufficientFundsException {
        Logger.i(TAG, "Calculating fee", null, "amount:", bigInteger, "txFee:", bigInteger2, "inputFee:", bigInteger3, "outputFee:", bigInteger4);
        List list = (List) Collection$EL.stream(set).map(new Function() { // from class: com.mobilecoin.lib.UTXOSelector$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return UTXOSelector.lambda$calculateFee$6((OwnedTxOut) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toCollection(new UTXOSelector$$ExternalSyntheticLambda3()));
        BigInteger bigInteger5 = (BigInteger) Collection$EL.stream(list).map(new UTXOSelector$$ExternalSyntheticLambda4()).reduce(BigInteger.ZERO, new MobileCoinClient$$ExternalSyntheticLambda2());
        while (true) {
            try {
                return selectTxOutNodesForAmount(list, bigInteger, bigInteger2, bigInteger3, bigInteger4, i).fee;
            } catch (FragmentedAccountException unused) {
                TxOutNode selectTxOutNodesForMerging = selectTxOutNodesForMerging(list, bigInteger2, bigInteger3, bigInteger4);
                if (bigInteger5.compareTo(selectTxOutNodesForMerging.getValue().add(selectTxOutNodesForMerging.getFee(bigInteger2, bigInteger3))) >= 0) {
                    list.removeAll(selectTxOutNodesForMerging.children);
                    list.add(selectTxOutNodesForMerging);
                } else {
                    throw new InsufficientFundsException();
                }
            }
        }
    }

    public static /* synthetic */ TxOutNode lambda$calculateFee$6(OwnedTxOut ownedTxOut) {
        return new TxOutNode(ownedTxOut, null);
    }

    public static BigInteger getTransferableAmount(Set<OwnedTxOut> set, BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3) throws InsufficientFundsException {
        Logger.i(TAG, "Getting transferable amount", null, "unspent:", set, "txFee:", bigInteger, "inputFee:", bigInteger2, "outputFee:", bigInteger3);
        List list = (List) Collection$EL.stream(set).filter(new Predicate(bigInteger2) { // from class: com.mobilecoin.lib.UTXOSelector$$ExternalSyntheticLambda7
            public final /* synthetic */ BigInteger f$0;

            {
                this.f$0 = r1;
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return UTXOSelector.lambda$getTransferableAmount$7(this.f$0, (OwnedTxOut) obj);
            }
        }).map(new Function() { // from class: com.mobilecoin.lib.UTXOSelector$$ExternalSyntheticLambda8
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return UTXOSelector.lambda$getTransferableAmount$8((OwnedTxOut) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toCollection(new UTXOSelector$$ExternalSyntheticLambda3()));
        Stream map = Collection$EL.stream(list).map(new UTXOSelector$$ExternalSyntheticLambda4());
        BigInteger bigInteger4 = BigInteger.ZERO;
        BigInteger bigInteger5 = (BigInteger) map.reduce(bigInteger4, new MobileCoinClient$$ExternalSyntheticLambda2());
        if (list.size() == 0) {
            return bigInteger4;
        }
        while (list.size() > 16) {
            TxOutNode selectTxOutNodesForMerging = selectTxOutNodesForMerging(list, bigInteger, bigInteger2, bigInteger3);
            list.removeAll(selectTxOutNodesForMerging.children);
            list.add(selectTxOutNodesForMerging);
        }
        BigInteger bigInteger6 = (BigInteger) Collection$EL.stream(list).map(new Function(bigInteger, bigInteger2) { // from class: com.mobilecoin.lib.UTXOSelector$$ExternalSyntheticLambda9
            public final /* synthetic */ BigInteger f$0;
            public final /* synthetic */ BigInteger f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((UTXOSelector.TxOutNode) obj).getFee(this.f$0, this.f$1);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).reduce(bigInteger, new MobileCoinClient$$ExternalSyntheticLambda2());
        if (bigInteger5.compareTo(bigInteger6) <= 0) {
            return BigInteger.ZERO;
        }
        return bigInteger5.subtract(bigInteger6);
    }

    public static /* synthetic */ boolean lambda$getTransferableAmount$7(BigInteger bigInteger, OwnedTxOut ownedTxOut) {
        return ownedTxOut.getValue().compareTo(bigInteger) > 0;
    }

    public static /* synthetic */ TxOutNode lambda$getTransferableAmount$8(OwnedTxOut ownedTxOut) {
        return new TxOutNode(ownedTxOut, null);
    }

    /* loaded from: classes3.dex */
    public static class Selection<T> {
        public final BigInteger fee;
        public final List<T> txOuts;

        public Selection(List<T> list, BigInteger bigInteger) {
            this.txOuts = list;
            this.fee = bigInteger;
        }
    }

    /* loaded from: classes3.dex */
    public static class TxOutNode {
        public final List<TxOutNode> children;
        public final OwnedTxOut txOut;

        TxOutNode(OwnedTxOut ownedTxOut, List<TxOutNode> list) {
            this.children = list == null ? new ArrayList<>() : list;
            this.txOut = ownedTxOut;
        }

        public BigInteger getValue() {
            OwnedTxOut ownedTxOut = this.txOut;
            if (ownedTxOut != null) {
                return ownedTxOut.getValue();
            }
            return (BigInteger) Collection$EL.stream(this.children).map(new UTXOSelector$$ExternalSyntheticLambda4()).reduce(BigInteger.ZERO, new MobileCoinClient$$ExternalSyntheticLambda2());
        }

        public BigInteger getFee(BigInteger bigInteger, BigInteger bigInteger2) {
            return this.txOut != null ? bigInteger2 : bigInteger.add((BigInteger) Collection$EL.stream(this.children).map(new UTXOSelector$TxOutNode$$ExternalSyntheticLambda0(bigInteger, bigInteger2)).reduce(BigInteger.ZERO, new MobileCoinClient$$ExternalSyntheticLambda2()));
        }
    }
}
