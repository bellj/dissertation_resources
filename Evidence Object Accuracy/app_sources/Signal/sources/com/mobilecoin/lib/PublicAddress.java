package com.mobilecoin.lib;

import android.net.Uri;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.mobilecoin.api.MobileCoinAPI$PublicAddress;
import com.mobilecoin.lib.exceptions.SerializationException;
import com.mobilecoin.lib.log.Logger;
import com.mobilecoin.lib.uri.FogUri;
import com.mobilecoin.lib.util.Hex;
import java.util.Arrays;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class PublicAddress extends Native {
    private static final String TAG;
    private final byte[] fogAuthoritySig;
    private final String fogReportId;
    private final Uri fogReportUri;
    private final RistrettoPublic spendKey;
    private final RistrettoPublic viewKey;

    private native byte[] get_fog_authority_sig();

    private native String get_fog_uri();

    private native String get_report_id();

    private native long get_spend_key();

    private native long get_view_key();

    private native void init_jni(RistrettoPublic ristrettoPublic, RistrettoPublic ristrettoPublic2);

    private native void init_jni_with_fog(RistrettoPublic ristrettoPublic, RistrettoPublic ristrettoPublic2, String str, byte[] bArr, String str2);

    public PublicAddress(RistrettoPublic ristrettoPublic, RistrettoPublic ristrettoPublic2, Uri uri, byte[] bArr, String str) {
        this.viewKey = ristrettoPublic;
        this.spendKey = ristrettoPublic2;
        this.fogReportUri = uri;
        this.fogAuthoritySig = bArr;
        this.fogReportId = str;
        try {
            new FogUri(uri);
            init_jni_with_fog(ristrettoPublic, ristrettoPublic2, uri.toString(), bArr, str);
            Logger.i(TAG, "Created Public Address from view/spend public keys");
        } catch (NullPointerException e) {
            throw e;
        } catch (Exception e2) {
            Logger.e(TAG, "Unable to create PublicAddress instance", e2, new Object[0]);
            throw new IllegalArgumentException(e2.getLocalizedMessage());
        }
    }

    public PublicAddress(RistrettoPublic ristrettoPublic, RistrettoPublic ristrettoPublic2) {
        String str = TAG;
        Logger.i(str, "Creating Public Address from view/spend public keys");
        this.viewKey = ristrettoPublic;
        this.spendKey = ristrettoPublic2;
        this.fogReportUri = null;
        this.fogReportId = null;
        this.fogAuthoritySig = null;
        try {
            init_jni(ristrettoPublic, ristrettoPublic2);
            Logger.i(str, "Created Public Address from view/spend public keys");
        } catch (NullPointerException e) {
            throw e;
        } catch (Exception e2) {
            Logger.e(TAG, "Unable to create PublicAddress instance", e2, new Object[0]);
            throw new IllegalArgumentException(e2.getLocalizedMessage());
        }
    }

    private PublicAddress(long j) {
        this.rustObj = j;
        this.viewKey = RistrettoPublic.fromJNI(get_view_key());
        this.spendKey = RistrettoPublic.fromJNI(get_spend_key());
        String str = get_fog_uri();
        if (str != null) {
            this.fogReportUri = Uri.parse(str);
        } else {
            this.fogReportUri = null;
        }
        String str2 = get_report_id();
        this.fogReportId = str2 == null ? "" : str2;
        this.fogAuthoritySig = get_fog_authority_sig();
    }

    public static PublicAddress fromBytes(byte[] bArr) throws SerializationException {
        try {
            return fromProtoBufObject(MobileCoinAPI$PublicAddress.parseFrom(bArr));
        } catch (InvalidProtocolBufferException e) {
            throw new SerializationException("Unable to construct an object from the provided data", e);
        }
    }

    public static PublicAddress fromProtoBufObject(MobileCoinAPI$PublicAddress mobileCoinAPI$PublicAddress) throws SerializationException {
        String fogReportUrl = mobileCoinAPI$PublicAddress.getFogReportUrl();
        byte[] bArr = null;
        Uri parse = (fogReportUrl == null || fogReportUrl.isEmpty()) ? null : Uri.parse(fogReportUrl);
        ByteString fogAuthoritySig = mobileCoinAPI$PublicAddress.getFogAuthoritySig();
        if (fogAuthoritySig != null && fogAuthoritySig.size() > 0) {
            bArr = fogAuthoritySig.toByteArray();
        }
        String fogReportId = mobileCoinAPI$PublicAddress.getFogReportId();
        boolean z = (parse == null || bArr == null || fogReportId == null) ? false : true;
        RistrettoPublic fromProtoBufObject = RistrettoPublic.fromProtoBufObject(mobileCoinAPI$PublicAddress.getViewPublicKey());
        RistrettoPublic fromProtoBufObject2 = RistrettoPublic.fromProtoBufObject(mobileCoinAPI$PublicAddress.getSpendPublicKey());
        if (z) {
            return new PublicAddress(fromProtoBufObject, fromProtoBufObject2, parse, bArr, fogReportId);
        }
        return new PublicAddress(fromProtoBufObject, fromProtoBufObject2);
    }

    public static PublicAddress fromJNI(long j) {
        return new PublicAddress(j);
    }

    public byte[] toByteArray() {
        return toProtoBufObject().toByteArray();
    }

    public MobileCoinAPI$PublicAddress toProtoBufObject() {
        MobileCoinAPI$PublicAddress.Builder newBuilder = MobileCoinAPI$PublicAddress.newBuilder();
        if (getFogAuthoritySig() != null) {
            newBuilder.setFogAuthoritySig(ByteString.copyFrom(getFogAuthoritySig()));
        }
        if (getFogReportId() != null) {
            newBuilder.setFogReportId(getFogReportId());
        }
        if (getFogReportUri() != null) {
            newBuilder.setFogReportUrl(getFogReportUri().toString());
        }
        newBuilder.setViewPublicKey(getViewKey().toProtoBufObject());
        newBuilder.setSpendPublicKey(getSpendKey().toProtoBufObject());
        return newBuilder.build();
    }

    public RistrettoPublic getViewKey() {
        return this.viewKey;
    }

    public RistrettoPublic getSpendKey() {
        return this.spendKey;
    }

    public byte[] getFogAuthoritySig() {
        return this.fogAuthoritySig;
    }

    public String getFogReportId() {
        return this.fogReportId;
    }

    public Uri getFogReportUri() {
        return this.fogReportUri;
    }

    public boolean hasFogInfo() {
        return (getFogReportUri() == null || getFogAuthoritySig() == null || getFogReportId() == null) ? false : true;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || PublicAddress.class != obj.getClass()) {
            return false;
        }
        PublicAddress publicAddress = (PublicAddress) obj;
        if (!this.viewKey.equals(publicAddress.viewKey) || !this.spendKey.equals(publicAddress.spendKey) || !Arrays.equals(this.fogAuthoritySig, publicAddress.fogAuthoritySig) || !Objects.equals(this.fogReportUri, publicAddress.fogReportUri) || !Objects.equals(this.fogReportId, publicAddress.fogReportId)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (Objects.hash(this.fogReportId, this.viewKey, this.spendKey, this.fogReportUri) * 31) + Arrays.hashCode(this.fogAuthoritySig);
    }

    public String toString() {
        String str;
        byte[] bArr = this.fogAuthoritySig;
        if (bArr == null) {
            str = "(null)";
        } else {
            str = Hex.toString(bArr);
        }
        return "PublicAddress{viewKey=" + this.viewKey.toString() + ", spendKey=" + this.spendKey.toString() + ", fogReportUri=" + this.fogReportUri + ", fogReportId=" + this.fogReportId + ", fogAuthoritySig=" + str + '}';
    }
}
