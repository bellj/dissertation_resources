package com.mobilecoin.lib;

import com.mobilecoin.lib.exceptions.SerializationException;
import com.mobilecoin.lib.log.Logger;

/* loaded from: classes3.dex */
public final class TxOutMembershipProof extends Native {
    private static final String TAG;

    private native void finalize_jni();

    private native void init_from_protobuf_bytes(byte[] bArr);

    public TxOutMembershipProof(byte[] bArr) throws SerializationException {
        Logger.i(TAG, "Initializing from protobuf");
        try {
            init_from_protobuf_bytes(bArr);
        } catch (Exception e) {
            SerializationException serializationException = new SerializationException(e.getLocalizedMessage(), e);
            Util.logException(TAG, serializationException);
            throw serializationException;
        }
    }

    protected void finalize() throws Throwable {
        if (this.rustObj != 0) {
            finalize_jni();
        }
        super.finalize();
    }
}
