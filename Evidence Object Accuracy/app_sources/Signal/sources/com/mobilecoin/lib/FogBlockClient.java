package com.mobilecoin.lib;

import com.mobilecoin.api.MobileCoinAPI$TxOut;
import com.mobilecoin.lib.ClientConfig;
import com.mobilecoin.lib.exceptions.AttestationException;
import com.mobilecoin.lib.exceptions.NetworkException;
import com.mobilecoin.lib.log.Logger;
import com.mobilecoin.lib.uri.FogUri;
import com.mobilecoin.lib.util.NetworkingCall;
import fog_ledger.FogBlockAPIGrpc;
import fog_ledger.Ledger$Block;
import fog_ledger.Ledger$BlockRequest;
import fog_ledger.Ledger$BlockResponse;
import fog_view.View$TxOutRecord;
import io.grpc.StatusRuntimeException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;

/* loaded from: classes3.dex */
public final class FogBlockClient extends AnyClient {
    private static final String TAG;

    public FogBlockClient(FogUri fogUri, ClientConfig.Service service) {
        super(fogUri.getUri(), service);
        Logger.i(TAG, "Created new FogBlockClient", null, "uri:", fogUri, "verifier:", service);
    }

    public List<OwnedTxOut> scanForTxOutsInBlockRange(BlockRange blockRange, AccountKey accountKey) throws NetworkException {
        String str = TAG;
        Logger.i(str, "Scanning the ledger for TxOuts");
        ArrayList arrayList = new ArrayList();
        List<View$TxOutRecord> fetchTxRecordsInBlockRange = fetchTxRecordsInBlockRange(blockRange);
        Logger.d(str, "Received TxRecords response", null, "count:", Integer.valueOf(fetchTxRecordsInBlockRange.size()), "range:", blockRange);
        for (View$TxOutRecord view$TxOutRecord : fetchTxRecordsInBlockRange) {
            try {
                arrayList.add(new OwnedTxOut(view$TxOutRecord, accountKey));
                Logger.d(TAG, "Found TxOut", null, "block:", Long.valueOf(view$TxOutRecord.getBlockIndex()));
            } catch (Exception unused) {
            }
        }
        Logger.d(TAG, String.format(Locale.US, "Found total %d TxOuts", Integer.valueOf(arrayList.size())));
        return arrayList;
    }

    public List<View$TxOutRecord> fetchTxRecordsInBlockRange(BlockRange blockRange) throws NetworkException {
        Logger.i(TAG, "Fetching TxOuts via Block API", null, "range:", blockRange);
        try {
            try {
                List<Ledger$Block> blocksList = ((Ledger$BlockResponse) new NetworkingCall(new Callable(Ledger$BlockRequest.newBuilder().addRanges(blockRange.toProtoBuf()).build()) { // from class: com.mobilecoin.lib.FogBlockClient$$ExternalSyntheticLambda0
                    public final /* synthetic */ Ledger$BlockRequest f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // java.util.concurrent.Callable
                    public final Object call() {
                        return FogBlockClient.$r8$lambda$LH23rHUPXHRs7JLk_2nNdPOeXDU(FogBlockAPIGrpc.FogBlockAPIBlockingStub.this, this.f$1);
                    }
                }).run()).getBlocksList();
                ArrayList arrayList = new ArrayList();
                for (Ledger$Block ledger$Block : blocksList) {
                    long globalTxoCount = ledger$Block.getGlobalTxoCount() - ((long) ledger$Block.getOutputsCount());
                    for (MobileCoinAPI$TxOut mobileCoinAPI$TxOut : ledger$Block.getOutputsList()) {
                        View$TxOutRecord build = View$TxOutRecord.newBuilder().setBlockIndex(ledger$Block.getIndex()).setTimestamp(ledger$Block.getTimestamp()).setTxOutGlobalIndex(((long) ledger$Block.getOutputsList().indexOf(mobileCoinAPI$TxOut)) + globalTxoCount).setTxOutAmountCommitmentData(mobileCoinAPI$TxOut.getAmount().getCommitment().getData()).setTxOutAmountMaskedValue(mobileCoinAPI$TxOut.getAmount().getMaskedValue()).setTxOutPublicKeyData(mobileCoinAPI$TxOut.getPublicKey().getData()).setTxOutTargetKeyData(mobileCoinAPI$TxOut.getTargetKey().getData()).build();
                        arrayList.add(build);
                        Logger.d(TAG, "Found TxOut", null, "block index:", Long.valueOf(build.getBlockIndex()));
                    }
                }
                return arrayList;
            } catch (NetworkException e) {
                throw e;
            } catch (RuntimeException e2) {
                throw e2;
            } catch (Exception unused) {
                throw new IllegalStateException("BUG: unreachable code");
            }
        } catch (AttestationException e3) {
            throw new IllegalStateException("BUG", e3);
        }
    }

    public static /* synthetic */ Ledger$BlockResponse lambda$fetchTxRecordsInBlockRange$0(FogBlockAPIGrpc.FogBlockAPIBlockingStub fogBlockAPIBlockingStub, Ledger$BlockRequest ledger$BlockRequest) throws Exception {
        try {
            return fogBlockAPIBlockingStub.getBlocks(ledger$BlockRequest);
        } catch (StatusRuntimeException e) {
            Logger.w(TAG, "Unable to post transaction with consensus", e, new Object[0]);
            throw new NetworkException(e);
        }
    }
}
