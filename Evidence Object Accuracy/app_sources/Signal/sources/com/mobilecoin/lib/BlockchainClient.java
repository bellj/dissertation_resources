package com.mobilecoin.lib;

import com.google.protobuf.Empty;
import com.mobilecoin.lib.ClientConfig;
import com.mobilecoin.lib.exceptions.AttestationException;
import com.mobilecoin.lib.exceptions.NetworkException;
import com.mobilecoin.lib.log.Logger;
import com.mobilecoin.lib.uri.ConsensusUri;
import com.mobilecoin.lib.util.NetworkingCall;
import consensus_common.BlockchainAPIGrpc;
import consensus_common.ConsensusCommon$LastBlockInfoResponse;
import io.grpc.StatusRuntimeException;
import j$.time.Duration;
import j$.time.LocalDateTime;
import j$.time.chrono.ChronoLocalDateTime;
import java.math.BigInteger;
import java.util.concurrent.Callable;

/* loaded from: classes3.dex */
public final class BlockchainClient extends AnyClient {
    private static final BigInteger DEFAULT_TX_FEE = BigInteger.valueOf(10000000000L);
    private static final String TAG;
    private ConsensusCommon$LastBlockInfoResponse lastBlockInfo;
    private LocalDateTime lastBlockInfoTimestamp;
    private final Duration minimumFeeCacheTTL;

    public BlockchainClient(ConsensusUri consensusUri, ClientConfig.Service service, Duration duration) {
        super(consensusUri.getUri(), service);
        this.minimumFeeCacheTTL = duration;
    }

    public synchronized UnsignedLong getOrFetchMinimumFee() throws NetworkException {
        UnsignedLong fromLongBits;
        fromLongBits = UnsignedLong.fromLongBits(getOrFetchLastBlockInfo().getMinimumFee());
        if (fromLongBits.equals(UnsignedLong.ZERO)) {
            fromLongBits = UnsignedLong.fromBigInteger(DEFAULT_TX_FEE);
        }
        return fromLongBits;
    }

    public synchronized void resetCache() {
        this.lastBlockInfo = null;
        this.lastBlockInfoTimestamp = null;
    }

    synchronized ConsensusCommon$LastBlockInfoResponse getOrFetchLastBlockInfo() throws NetworkException {
        if (this.lastBlockInfo == null || this.lastBlockInfoTimestamp.plus(this.minimumFeeCacheTTL).compareTo((ChronoLocalDateTime<?>) LocalDateTime.now()) <= 0) {
            this.lastBlockInfo = fetchLastBlockInfo();
            this.lastBlockInfoTimestamp = LocalDateTime.now();
        }
        return this.lastBlockInfo;
    }

    ConsensusCommon$LastBlockInfoResponse fetchLastBlockInfo() throws NetworkException {
        Logger.i(TAG, "Fetching last block info via Blockchain API");
        try {
            try {
                return (ConsensusCommon$LastBlockInfoResponse) new NetworkingCall(new Callable() { // from class: com.mobilecoin.lib.BlockchainClient$$ExternalSyntheticLambda0
                    @Override // java.util.concurrent.Callable
                    public final Object call() {
                        return BlockchainClient.$r8$lambda$lvzyNtOD9AVDBmf1UbYYjl8bXg0(BlockchainAPIGrpc.BlockchainAPIBlockingStub.this);
                    }
                }).run();
            } catch (NetworkException e) {
                throw e;
            } catch (RuntimeException e2) {
                throw e2;
            } catch (Exception unused) {
                throw new IllegalStateException("BUG: unreachable code");
            }
        } catch (AttestationException e3) {
            throw new IllegalStateException("BUG", e3);
        }
    }

    public static /* synthetic */ ConsensusCommon$LastBlockInfoResponse lambda$fetchLastBlockInfo$0(BlockchainAPIGrpc.BlockchainAPIBlockingStub blockchainAPIBlockingStub) throws Exception {
        try {
            return blockchainAPIBlockingStub.getLastBlockInfo(Empty.newBuilder().build());
        } catch (StatusRuntimeException e) {
            Logger.w(TAG, "Unable to post transaction with consensus", e, new Object[0]);
            throw new NetworkException(e);
        }
    }
}
