package com.mobilecoin.lib;

import android.net.Uri;
import com.mobilecoin.lib.exceptions.BadBip39EntropyException;
import com.mobilecoin.lib.exceptions.BadEntropyException;
import com.mobilecoin.lib.exceptions.InvalidUriException;
import com.mobilecoin.lib.log.Logger;
import com.mobilecoin.lib.uri.FogUri;
import com.mobilecoin.lib.util.Hex;
import java.util.Arrays;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class AccountKey extends Native {
    private static final String TAG;
    private final byte[] fogAuthoritySpki;
    private final String fogReportId;
    private final Uri fogReportUri;
    private final PublicAddress publicAddress;
    private final RistrettoPrivate spendKey;
    private final RistrettoPrivate subAddressSpendKey;
    private final RistrettoPrivate subAddressViewKey;
    private final RistrettoPrivate viewKey;

    private native void finalize_jni();

    private native String getFogUriString();

    private native long get_default_subaddress_spend_key();

    private native long get_default_subaddress_view_key();

    private native byte[] get_fog_authority_spki();

    private native long get_public_address();

    private native String get_report_id();

    private native long get_spend_key();

    private native long get_view_key();

    private native void init_jni(RistrettoPrivate ristrettoPrivate, RistrettoPrivate ristrettoPrivate2, String str, byte[] bArr, String str2);

    AccountKey(RistrettoPrivate ristrettoPrivate, RistrettoPrivate ristrettoPrivate2, Uri uri, String str, byte[] bArr) throws InvalidUriException {
        String str2 = TAG;
        Logger.i(str2, "Create a new AccountKey from view/spend keys", null, "fogReportUri:", uri, "fogReportId:", str, "fogAuthoritySpki: ", Hex.toString(bArr));
        new FogUri(uri);
        try {
            init_jni(ristrettoPrivate, ristrettoPrivate2, uri.toString(), bArr, str);
            this.fogReportUri = uri;
            this.fogReportId = str;
            this.fogAuthoritySpki = bArr;
            this.viewKey = ristrettoPrivate;
            this.spendKey = ristrettoPrivate2;
            this.subAddressViewKey = RistrettoPrivate.fromJNI(get_default_subaddress_view_key());
            this.subAddressSpendKey = RistrettoPrivate.fromJNI(get_default_subaddress_spend_key());
            this.publicAddress = PublicAddress.fromJNI(get_public_address());
            Logger.i(str2, "AccountKey created from view/spend keys");
        } catch (Exception e) {
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException("Failed to create an AccountKey", e);
            Util.logException(TAG, illegalArgumentException);
            throw illegalArgumentException;
        }
    }

    AccountKey(long j) {
        this.rustObj = j;
        String fogUriString = getFogUriString();
        this.fogReportUri = (fogUriString == null || fogUriString.isEmpty()) ? null : Uri.parse(fogUriString);
        this.fogReportId = get_report_id();
        this.fogAuthoritySpki = get_fog_authority_spki();
        this.viewKey = RistrettoPrivate.fromJNI(get_view_key());
        this.spendKey = RistrettoPrivate.fromJNI(get_spend_key());
        this.subAddressViewKey = RistrettoPrivate.fromJNI(get_default_subaddress_view_key());
        this.subAddressSpendKey = RistrettoPrivate.fromJNI(get_default_subaddress_spend_key());
        this.publicAddress = PublicAddress.fromJNI(get_public_address());
    }

    public static AccountKey fomJNI(long j) {
        return new AccountKey(j);
    }

    public static AccountKey fromBip39Entropy(byte[] bArr, int i, Uri uri, String str, byte[] bArr2) throws InvalidUriException, BadEntropyException {
        Logger.i(TAG, "Derive AccountKey from Bip39 entropy", null, "account index:", Integer.valueOf(i));
        try {
            AccountKey deriveAccountKeyFromMnemonic = AccountKeyDeriver.deriveAccountKeyFromMnemonic(Mnemonics.bip39EntropyToMnemonic(bArr), i);
            return new AccountKey(deriveAccountKeyFromMnemonic.getViewKey(), deriveAccountKeyFromMnemonic.getSpendKey(), uri, str, bArr2);
        } catch (BadBip39EntropyException | BadEntropyException e) {
            BadEntropyException badEntropyException = new BadEntropyException("Unable to derive AccountKey from the bip39 entropy", e);
            Util.logException(TAG, badEntropyException);
            throw badEntropyException;
        }
    }

    public RistrettoPrivate getViewKey() {
        return this.viewKey;
    }

    RistrettoPrivate getSpendKey() {
        return this.spendKey;
    }

    public Uri getFogReportUri() {
        return this.fogReportUri;
    }

    public PublicAddress getPublicAddress() {
        return this.publicAddress;
    }

    public RistrettoPrivate getSubAddressSpendKey() {
        return this.subAddressSpendKey;
    }

    public RistrettoPrivate getSubAddressViewKey() {
        return this.subAddressViewKey;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AccountKey.class != obj.getClass()) {
            return false;
        }
        AccountKey accountKey = (AccountKey) obj;
        if (!Objects.equals(this.fogReportId, accountKey.fogReportId) || !Objects.equals(this.fogReportUri, accountKey.fogReportUri) || !Arrays.equals(this.fogAuthoritySpki, accountKey.fogAuthoritySpki) || !this.viewKey.equals(accountKey.viewKey) || !this.spendKey.equals(accountKey.spendKey) || !this.subAddressViewKey.equals(accountKey.subAddressViewKey) || !this.subAddressSpendKey.equals(accountKey.subAddressSpendKey)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (Objects.hash(this.fogReportId, this.fogReportUri, this.viewKey, this.spendKey, this.subAddressViewKey, this.subAddressSpendKey) * 31) + Arrays.hashCode(this.fogAuthoritySpki);
    }

    protected void finalize() throws Throwable {
        Logger.i(TAG, "Finalizing the object");
        if (this.rustObj != 0) {
            finalize_jni();
        }
        super.finalize();
    }
}
