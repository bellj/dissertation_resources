package com.mobilecoin.lib;

import j$.util.function.BiFunction;
import j$.util.function.BinaryOperator;
import j$.util.function.Function;
import java.math.BigInteger;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class MobileCoinClient$$ExternalSyntheticLambda2 implements BinaryOperator {
    @Override // j$.util.function.BinaryOperator, j$.util.function.BiFunction
    public /* synthetic */ BiFunction andThen(Function function) {
        return BiFunction.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.BiFunction
    public final Object apply(Object obj, Object obj2) {
        return ((BigInteger) obj).add((BigInteger) obj2);
    }
}
