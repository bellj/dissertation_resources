package com.mobilecoin.lib.exceptions;

/* loaded from: classes3.dex */
public class MobileCoinException extends Exception {
    public MobileCoinException(Exception exc) {
        super(exc);
    }

    public MobileCoinException(String str) {
        super(str);
    }

    public MobileCoinException(String str, Throwable th) {
        super(str, th);
    }
}
