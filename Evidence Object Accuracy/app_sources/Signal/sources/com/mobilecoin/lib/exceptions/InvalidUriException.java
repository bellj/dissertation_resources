package com.mobilecoin.lib.exceptions;

/* loaded from: classes3.dex */
public final class InvalidUriException extends MobileCoinException {
    public InvalidUriException(String str) {
        super(str);
    }

    public InvalidUriException(String str, Throwable th) {
        super(str, th);
    }
}
