package com.mobilecoin.lib.exceptions;

/* loaded from: classes3.dex */
public final class KexRngException extends MobileCoinException {
    public KexRngException(String str, Throwable th) {
        super(str, th);
    }
}
