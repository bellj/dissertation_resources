package com.mobilecoin.lib.exceptions;

/* loaded from: classes3.dex */
public final class AttestationException extends MobileCoinException {
    public AttestationException(String str, Throwable th) {
        super(str, th);
    }
}
