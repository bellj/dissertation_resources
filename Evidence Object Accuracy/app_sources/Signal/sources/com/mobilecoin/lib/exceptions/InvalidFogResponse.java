package com.mobilecoin.lib.exceptions;

/* loaded from: classes3.dex */
public final class InvalidFogResponse extends MobileCoinException {
    public InvalidFogResponse(String str) {
        super(str);
    }

    public InvalidFogResponse(String str, Throwable th) {
        super(str, th);
    }
}
