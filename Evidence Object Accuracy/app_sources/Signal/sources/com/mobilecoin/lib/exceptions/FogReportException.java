package com.mobilecoin.lib.exceptions;

/* loaded from: classes3.dex */
public final class FogReportException extends MobileCoinException {
    public FogReportException(String str) {
        super(str);
    }

    public FogReportException(String str, Throwable th) {
        super(str, th);
    }
}
