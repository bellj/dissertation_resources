package com.mobilecoin.lib.exceptions;

/* loaded from: classes3.dex */
public final class AmountDecoderException extends MobileCoinException {
    public AmountDecoderException(String str) {
        super(str);
    }

    public AmountDecoderException(String str, Throwable th) {
        super(str, th);
    }
}
