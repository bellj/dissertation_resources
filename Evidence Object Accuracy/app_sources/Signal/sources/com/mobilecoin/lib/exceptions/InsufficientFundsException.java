package com.mobilecoin.lib.exceptions;

/* loaded from: classes3.dex */
public final class InsufficientFundsException extends MobileCoinException {
    InsufficientFundsException(String str) {
        super(str);
    }

    public InsufficientFundsException() {
        this("Insufficient funds");
    }
}
