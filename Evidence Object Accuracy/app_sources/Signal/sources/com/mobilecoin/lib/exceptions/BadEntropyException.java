package com.mobilecoin.lib.exceptions;

/* loaded from: classes3.dex */
public final class BadEntropyException extends MobileCoinException {
    public BadEntropyException(String str, Throwable th) {
        super(str, th);
    }
}
