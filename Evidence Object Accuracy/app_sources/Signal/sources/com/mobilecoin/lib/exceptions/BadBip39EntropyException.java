package com.mobilecoin.lib.exceptions;

/* loaded from: classes3.dex */
public final class BadBip39EntropyException extends MobileCoinException {
    public BadBip39EntropyException(String str, Throwable th) {
        super(str, th);
    }
}
