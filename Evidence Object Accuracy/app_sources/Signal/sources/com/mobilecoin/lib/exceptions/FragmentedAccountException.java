package com.mobilecoin.lib.exceptions;

/* loaded from: classes3.dex */
public final class FragmentedAccountException extends MobileCoinException {
    public FragmentedAccountException(String str) {
        super(str);
    }
}
