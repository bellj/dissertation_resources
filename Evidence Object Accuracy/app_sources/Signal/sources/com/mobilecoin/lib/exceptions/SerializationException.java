package com.mobilecoin.lib.exceptions;

/* loaded from: classes3.dex */
public final class SerializationException extends MobileCoinException {
    public SerializationException(String str) {
        super(str);
    }

    public SerializationException(String str, Throwable th) {
        super(str, th);
    }
}
