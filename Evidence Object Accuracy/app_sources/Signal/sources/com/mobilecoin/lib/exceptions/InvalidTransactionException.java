package com.mobilecoin.lib.exceptions;

/* loaded from: classes3.dex */
public final class InvalidTransactionException extends MobileCoinException {
    public InvalidTransactionException(String str) {
        super(str);
    }
}
