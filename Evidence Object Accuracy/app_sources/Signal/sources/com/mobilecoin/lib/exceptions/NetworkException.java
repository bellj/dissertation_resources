package com.mobilecoin.lib.exceptions;

import io.grpc.Status;
import io.grpc.StatusRuntimeException;

/* loaded from: classes3.dex */
public final class NetworkException extends MobileCoinException {
    public final int statusCode;

    public NetworkException(int i, String str, Throwable th) {
        super(str, th);
        this.statusCode = i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: com.mobilecoin.lib.exceptions.NetworkException$1 */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$io$grpc$Status$Code;

        static {
            int[] iArr = new int[Status.Code.values().length];
            $SwitchMap$io$grpc$Status$Code = iArr;
            try {
                iArr[Status.Code.OK.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$io$grpc$Status$Code[Status.Code.INVALID_ARGUMENT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$io$grpc$Status$Code[Status.Code.FAILED_PRECONDITION.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$io$grpc$Status$Code[Status.Code.OUT_OF_RANGE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$io$grpc$Status$Code[Status.Code.UNAUTHENTICATED.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$io$grpc$Status$Code[Status.Code.PERMISSION_DENIED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$io$grpc$Status$Code[Status.Code.NOT_FOUND.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$io$grpc$Status$Code[Status.Code.ABORTED.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$io$grpc$Status$Code[Status.Code.ALREADY_EXISTS.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$io$grpc$Status$Code[Status.Code.RESOURCE_EXHAUSTED.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                $SwitchMap$io$grpc$Status$Code[Status.Code.CANCELLED.ordinal()] = 11;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                $SwitchMap$io$grpc$Status$Code[Status.Code.DATA_LOSS.ordinal()] = 12;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                $SwitchMap$io$grpc$Status$Code[Status.Code.UNKNOWN.ordinal()] = 13;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                $SwitchMap$io$grpc$Status$Code[Status.Code.INTERNAL.ordinal()] = 14;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                $SwitchMap$io$grpc$Status$Code[Status.Code.UNAVAILABLE.ordinal()] = 15;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                $SwitchMap$io$grpc$Status$Code[Status.Code.DEADLINE_EXCEEDED.ordinal()] = 16;
            } catch (NoSuchFieldError unused16) {
            }
        }
    }

    public NetworkException(StatusRuntimeException statusRuntimeException) {
        super(statusRuntimeException);
        switch (AnonymousClass1.$SwitchMap$io$grpc$Status$Code[statusRuntimeException.getStatus().getCode().ordinal()]) {
            case 1:
                this.statusCode = 200;
                return;
            case 2:
            case 3:
            case 4:
                this.statusCode = 400;
                return;
            case 5:
                this.statusCode = 401;
                return;
            case 6:
                this.statusCode = 403;
                return;
            case 7:
                this.statusCode = 404;
                return;
            case 8:
            case 9:
                this.statusCode = 409;
                return;
            case 10:
                this.statusCode = 429;
                return;
            case 11:
                this.statusCode = 499;
                return;
            case 12:
            case 13:
            case 14:
                this.statusCode = 500;
                return;
            case 15:
                this.statusCode = 503;
                return;
            case 16:
                this.statusCode = 504;
                return;
            default:
                this.statusCode = 501;
                return;
        }
    }
}
