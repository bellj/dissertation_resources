package com.mobilecoin.lib.exceptions;

/* loaded from: classes3.dex */
public final class BadMnemonicException extends MobileCoinException {
    public BadMnemonicException(String str, Throwable th) {
        super(str, th);
    }
}
