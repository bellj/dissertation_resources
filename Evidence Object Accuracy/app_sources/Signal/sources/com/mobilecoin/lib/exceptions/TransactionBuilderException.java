package com.mobilecoin.lib.exceptions;

/* loaded from: classes3.dex */
public final class TransactionBuilderException extends MobileCoinException {
    public TransactionBuilderException(String str) {
        super(str);
    }

    public TransactionBuilderException(String str, Throwable th) {
        super(str, th);
    }
}
