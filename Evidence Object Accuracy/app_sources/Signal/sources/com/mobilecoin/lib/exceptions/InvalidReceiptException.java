package com.mobilecoin.lib.exceptions;

/* loaded from: classes3.dex */
public final class InvalidReceiptException extends MobileCoinException {
    public InvalidReceiptException(String str) {
        super(str);
    }

    public InvalidReceiptException(String str, Throwable th) {
        super(str, th);
    }
}
