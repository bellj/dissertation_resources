package com.mobilecoin.lib;

import java.math.BigInteger;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class UnsignedLong extends Number implements Comparable<UnsignedLong> {
    public static final UnsignedLong MAX_VALUE = new UnsignedLong(-1);
    public static final UnsignedLong ONE = new UnsignedLong(1);
    public static final UnsignedLong TEN = new UnsignedLong(10);
    public static final UnsignedLong ZERO = new UnsignedLong(0);
    private final long value;

    private UnsignedLong(long j) {
        this.value = j;
    }

    public static UnsignedLong fromLongBits(long j) {
        return new UnsignedLong(j);
    }

    public static UnsignedLong fromBigInteger(BigInteger bigInteger) {
        return new UnsignedLong(bigInteger.longValue());
    }

    public static UnsignedLong valueOf(long j) {
        if (j >= 0) {
            return fromLongBits(j);
        }
        throw new IllegalArgumentException("The value is outside of the long range");
    }

    public int compareTo(UnsignedLong unsignedLong) {
        return Long.compare(this.value - Long.MIN_VALUE, unsignedLong.value - Long.MIN_VALUE);
    }

    @Override // java.lang.Number
    public int intValue() {
        return (int) this.value;
    }

    @Override // java.lang.Number
    public long longValue() {
        return this.value;
    }

    @Override // java.lang.Number
    public float floatValue() {
        long j = this.value;
        float f = (float) (Long.MAX_VALUE & j);
        return j < 0 ? f + 9.223372E18f : f;
    }

    @Override // java.lang.Number
    public double doubleValue() {
        long j = this.value;
        double d = (double) (Long.MAX_VALUE & j);
        if (j >= 0) {
            return d;
        }
        Double.isNaN(d);
        return d + 9.223372036854776E18d;
    }

    public BigInteger toBigInteger() {
        long j = this.value;
        if (j >= 0) {
            return BigInteger.valueOf(j);
        }
        return BigInteger.valueOf(j & Long.MAX_VALUE).setBit(63);
    }

    @Override // java.lang.Object
    public String toString() {
        long j = this.value;
        if (j > 0) {
            return Long.toString(j);
        }
        return toBigInteger().toString();
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && UnsignedLong.class == obj.getClass() && this.value == ((UnsignedLong) obj).value) {
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Objects.hash(Long.valueOf(this.value));
    }

    public UnsignedLong add(UnsignedLong unsignedLong) {
        return fromLongBits(this.value + unsignedLong.value);
    }

    public UnsignedLong sub(UnsignedLong unsignedLong) {
        return fromLongBits(this.value - unsignedLong.value);
    }

    public long remainder(long j) {
        if (j >= 0) {
            long j2 = this.value;
            if (j2 >= 0) {
                return j2 % j;
            }
            return toBigInteger().remainder(fromLongBits(j).toBigInteger()).longValue();
        } else if (compareTo(fromLongBits(j)) < 0) {
            return this.value;
        } else {
            return this.value - j;
        }
    }

    public UnsignedLong remainder(UnsignedLong unsignedLong) {
        return fromLongBits(remainder(unsignedLong.value));
    }
}
