package com.mobilecoin.lib;

import com.mobilecoin.lib.exceptions.AttestationException;
import com.mobilecoin.lib.exceptions.InvalidTransactionException;
import com.mobilecoin.lib.exceptions.NetworkException;
import java.math.BigInteger;

/* loaded from: classes3.dex */
public interface DefragmentationDelegate {
    void onCancel();

    void onComplete();

    void onStart();

    boolean onStepReady(PendingTransaction pendingTransaction, BigInteger bigInteger) throws NetworkException, InvalidTransactionException, AttestationException;
}
