package com.mobilecoin.lib;

import com.google.protobuf.ByteString;
import com.mobilecoin.lib.exceptions.FogReportException;
import com.mobilecoin.lib.log.Logger;
import java.util.List;
import report.ReportOuterClass$Report;

/* loaded from: classes3.dex */
public final class Report extends Native {
    private static final String TAG;
    private final long publicKeyExpiry;

    private native void finalize_jni();

    private native void init_jni(String str, VerificationReport verificationReport, long j);

    Report(String str, VerificationReport verificationReport, long j) throws FogReportException {
        this.publicKeyExpiry = j;
        try {
            init_jni(str, verificationReport, j);
        } catch (Exception e) {
            throw new FogReportException("Unable to create report from the provided arguments", e);
        }
    }

    public static Report fromProtoBuf(ReportOuterClass$Report reportOuterClass$Report) throws FogReportException {
        Logger.i(TAG, "Deserializing report from protobuf");
        VerificationSignature verificationSignature = new VerificationSignature(reportOuterClass$Report.getReport().getSig().getContents().toByteArray());
        List<ByteString> chainList = reportOuterClass$Report.getReport().getChainList();
        byte[][] bArr = new byte[chainList.size()];
        for (int i = 0; i < chainList.size(); i++) {
            bArr[i] = chainList.get(i).toByteArray();
        }
        return new Report(reportOuterClass$Report.getFogReportId(), new VerificationReport(verificationSignature, bArr, reportOuterClass$Report.getReport().getHttpBody()), reportOuterClass$Report.getPubkeyExpiry());
    }

    public long getPublicKeyExpiry() {
        Logger.i(TAG, "Getting public key expiry", null, Long.valueOf(this.publicKeyExpiry));
        return this.publicKeyExpiry;
    }

    protected void finalize() throws Throwable {
        try {
            finalize_jni();
        } catch (Exception e) {
            Logger.e(TAG, "Failed to free fog report data", e, new Object[0]);
        }
        super.finalize();
    }
}
