package com.mobilecoin.lib;

import com.mobilecoin.lib.exceptions.BadEntropyException;
import com.mobilecoin.lib.exceptions.BadMnemonicException;
import com.mobilecoin.lib.log.Logger;

/* loaded from: classes3.dex */
public final class Mnemonics extends Native {
    private static final String TAG;

    private static native byte[] entropy_from_mnemonic(String str) throws Exception;

    private static native String entropy_to_mnemonic(byte[] bArr) throws Exception;

    private static native String words_by_prefix(String str);

    public static String bip39EntropyToMnemonic(byte[] bArr) throws BadEntropyException {
        Logger.i(TAG, "Converting entropy to mnemonic");
        try {
            return entropy_to_mnemonic(bArr);
        } catch (Exception e) {
            BadEntropyException badEntropyException = new BadEntropyException("Failed to get a mnemonic", e);
            Util.logException(TAG, badEntropyException);
            throw badEntropyException;
        }
    }

    public static byte[] bip39EntropyFromMnemonic(String str) throws BadMnemonicException {
        Logger.i(TAG, "Getting entropy from mnemonic");
        try {
            return entropy_from_mnemonic(str);
        } catch (Exception e) {
            BadMnemonicException badMnemonicException = new BadMnemonicException("Failed to get an entropy from mnemonic", e);
            Util.logException(TAG, badMnemonicException);
            throw badMnemonicException;
        }
    }

    public static String[] wordsByPrefix(String str) throws BadMnemonicException {
        Logger.i(TAG, "Listing words by prefix");
        try {
            return words_by_prefix(str).split(",");
        } catch (Exception e) {
            BadMnemonicException badMnemonicException = new BadMnemonicException("Unable to create words dictionary", e);
            Util.logException(TAG, badMnemonicException);
            throw badMnemonicException;
        }
    }
}
