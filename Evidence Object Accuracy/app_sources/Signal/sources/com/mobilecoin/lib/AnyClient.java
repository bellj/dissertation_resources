package com.mobilecoin.lib;

import android.net.Uri;
import com.mobilecoin.lib.ClientConfig;
import com.mobilecoin.lib.exceptions.AttestationException;
import com.mobilecoin.lib.exceptions.NetworkException;
import com.mobilecoin.lib.log.Logger;
import io.grpc.ManagedChannel;
import io.grpc.okhttp.OkHttpChannelBuilder;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

/* loaded from: classes3.dex */
public class AnyClient extends Native {
    private static final String TAG = AttestedClient.class.getName();
    private final ServiceAPIManager apiManager = new ServiceAPIManager();
    private ManagedChannel managedChannel;
    private final ClientConfig.Service serviceConfig;
    private final Uri serviceUri;

    public AnyClient(Uri uri, ClientConfig.Service service) {
        this.serviceUri = uri;
        this.serviceConfig = service;
    }

    public final ServiceAPIManager getAPIManager() {
        return this.apiManager;
    }

    public final Uri getServiceUri() {
        return this.serviceUri;
    }

    public final ClientConfig.Service getServiceConfig() {
        return this.serviceConfig;
    }

    public synchronized ManagedChannel getManagedChannel() throws AttestationException, NetworkException {
        try {
            if (this.managedChannel == null) {
                Logger.i(TAG, "Managed channel does not exist: creating one");
                OkHttpChannelBuilder useTransportSecurity = OkHttpChannelBuilder.forAddress(this.serviceUri.getHost(), this.serviceUri.getPort()).useTransportSecurity();
                Set<X509Certificate> trustRoots = getServiceConfig().getTrustRoots();
                if (trustRoots != null && trustRoots.size() > 0) {
                    useTransportSecurity.sslSocketFactory(getTrustedSSLSocketFactory(getTrustRootsKeyStore(trustRoots)));
                }
                this.managedChannel = useTransportSecurity.build();
            } else {
                Logger.i(TAG, "Managed channel exists: using existing");
            }
        } catch (Exception e) {
            Util.logException(TAG, new NetworkException(500, "Unable to create managed channel", e));
        }
        return this.managedChannel;
    }

    static SSLSocketFactory getTrustedSSLSocketFactory(KeyStore keyStore) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        TrustManagerFactory instance = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        instance.init(keyStore);
        SSLContext instance2 = SSLContext.getInstance("TLS");
        if (instance2 != null) {
            instance2.init(null, instance.getTrustManagers(), new SecureRandom());
            return instance2.getSocketFactory();
        }
        throw new NoSuchAlgorithmException("TLS is not supported");
    }

    static KeyStore getTrustRootsKeyStore(Set<X509Certificate> set) throws KeyStoreException, NoSuchAlgorithmException, IOException, CertificateException {
        KeyStore instance = KeyStore.getInstance(KeyStore.getDefaultType());
        instance.load(null, null);
        for (X509Certificate x509Certificate : set) {
            instance.setCertificateEntry(x509Certificate.toString(), x509Certificate);
        }
        return instance;
    }

    public void setAuthorization(String str, String str2) {
        Logger.i(TAG, "Set API authorization");
        getAPIManager().setAuthorization(str, str2);
    }

    public void shutdown() {
        String str = TAG;
        Logger.i(str, "Client shutdown");
        this.apiManager.reset();
        ManagedChannel managedChannel = this.managedChannel;
        if (managedChannel != null) {
            try {
                managedChannel.shutdown();
                Logger.i(str, "Shutting down the managed channel, awaiting for termination...");
                this.managedChannel.awaitTermination(1000, TimeUnit.MILLISECONDS);
                Logger.i(str, "The managed channel has been shut down");
            } catch (InterruptedException unused) {
            }
            this.managedChannel = null;
        }
    }
}
