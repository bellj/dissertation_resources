package com.mobilecoin.lib;

import com.mobilecoin.api.Printable$TransferPayload;
import com.mobilecoin.lib.exceptions.SerializationException;
import com.mobilecoin.lib.log.Logger;
import java.util.Arrays;
import java.util.Objects;

/* loaded from: classes3.dex */
public class TransferPayload {
    private static final String TAG;
    private final String memo;
    private final RistrettoPublic publicKey;
    private final byte[] rootEntropy;

    public TransferPayload(byte[] bArr, RistrettoPublic ristrettoPublic, String str) {
        this.rootEntropy = bArr;
        this.publicKey = ristrettoPublic;
        this.memo = str;
    }

    public static TransferPayload fromProtoBufObject(Printable$TransferPayload printable$TransferPayload) throws SerializationException {
        Logger.i(TAG, "Initializing from protobuf");
        return new TransferPayload(printable$TransferPayload.getBip39Entropy().toByteArray(), RistrettoPublic.fromBytes(printable$TransferPayload.getTxOutPublicKey().getData().toByteArray()), printable$TransferPayload.getMemo());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        TransferPayload transferPayload = (TransferPayload) obj;
        if (!Arrays.equals(this.rootEntropy, transferPayload.rootEntropy) || !this.publicKey.equals(transferPayload.publicKey) || !this.memo.equals(transferPayload.memo)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (Objects.hash(this.publicKey, this.memo) * 31) + Arrays.hashCode(this.rootEntropy);
    }
}
