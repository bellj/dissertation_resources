package com.mobilecoin.lib;

import com.mobilecoin.lib.UTXOSelector;
import j$.util.function.Function;
import java.math.BigInteger;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class UTXOSelector$TxOutNode$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ BigInteger f$0;
    public final /* synthetic */ BigInteger f$1;

    public /* synthetic */ UTXOSelector$TxOutNode$$ExternalSyntheticLambda0(BigInteger bigInteger, BigInteger bigInteger2) {
        this.f$0 = bigInteger;
        this.f$1 = bigInteger2;
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return ((UTXOSelector.TxOutNode) obj).getFee(this.f$0, this.f$1);
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
