package com.mobilecoin.lib;

import com.mobilecoin.api.MobileCoinAPI$CompressedRistretto;
import com.mobilecoin.api.MobileCoinAPI$TxOut;
import com.mobilecoin.lib.exceptions.AmountDecoderException;
import com.mobilecoin.lib.exceptions.SerializationException;
import com.mobilecoin.lib.log.Logger;
import fog_view.View$TxOutRecord;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/* loaded from: classes3.dex */
public class OwnedTxOut implements Serializable {
    private static final String TAG = OwnedTxOut.class.getName();
    private final byte[] keyImage;
    private int keyImageHash;
    private final UnsignedLong receivedBlockIndex;
    private final Date receivedBlockTimestamp;
    private UnsignedLong spentBlockIndex;
    private Date spentBlockTimestamp;
    private final UnsignedLong txOutGlobalIndex;
    private final RistrettoPublic txOutPublicKey;
    private final BigInteger value;

    public OwnedTxOut(View$TxOutRecord view$TxOutRecord, AccountKey accountKey) {
        try {
            this.txOutGlobalIndex = UnsignedLong.fromLongBits(view$TxOutRecord.getTxOutGlobalIndex());
            UnsignedLong fromLongBits = UnsignedLong.fromLongBits(view$TxOutRecord.getTimestamp());
            if (!fromLongBits.equals(UnsignedLong.MAX_VALUE)) {
                this.receivedBlockTimestamp = new Date(TimeUnit.SECONDS.toMillis(fromLongBits.longValue()));
            } else {
                this.receivedBlockTimestamp = null;
            }
            this.receivedBlockIndex = UnsignedLong.fromLongBits(view$TxOutRecord.getBlockIndex());
            MobileCoinAPI$CompressedRistretto build = MobileCoinAPI$CompressedRistretto.newBuilder().setData(view$TxOutRecord.getTxOutPublicKeyData()).build();
            RistrettoPublic fromProtoBufObject = RistrettoPublic.fromProtoBufObject(build);
            this.txOutPublicKey = fromProtoBufObject;
            Amount amount = new Amount(view$TxOutRecord.getTxOutAmountCommitmentData().toByteArray(), view$TxOutRecord.getTxOutAmountMaskedValue());
            this.value = amount.unmaskValue(accountKey.getViewKey(), fromProtoBufObject);
            this.keyImage = TxOut.fromProtoBufObject(MobileCoinAPI$TxOut.newBuilder().setAmount(amount.toProtoBufObject()).setPublicKey(build).setTargetKey(MobileCoinAPI$CompressedRistretto.newBuilder().setData(view$TxOutRecord.getTxOutTargetKeyData()).build()).build()).computeKeyImage(accountKey);
        } catch (AmountDecoderException | SerializationException e) {
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException("Unable to decode the TxOutRecord", e);
            Util.logException(TAG, illegalArgumentException);
            throw illegalArgumentException;
        }
    }

    public BigInteger getValue() {
        return this.value;
    }

    public KeyImage getKeyImage() {
        return KeyImage.fromBytes(this.keyImage);
    }

    public synchronized UnsignedLong getReceivedBlockIndex() {
        return this.receivedBlockIndex;
    }

    public Date getReceivedBlockTimestamp() {
        return this.receivedBlockTimestamp;
    }

    public synchronized UnsignedLong getSpentBlockIndex() {
        return this.spentBlockIndex;
    }

    public synchronized Date getSpentBlockTimestamp() {
        return this.spentBlockTimestamp;
    }

    public RistrettoPublic getPublicKey() {
        return this.txOutPublicKey;
    }

    public synchronized boolean isSpent(UnsignedLong unsignedLong) {
        boolean z;
        UnsignedLong unsignedLong2 = this.spentBlockIndex;
        if (unsignedLong2 != null) {
            if (unsignedLong2.compareTo(unsignedLong) <= 0) {
                z = true;
            }
        }
        z = false;
        return z;
    }

    public synchronized void setSpent(UnsignedLong unsignedLong, Date date) {
        Logger.i(TAG, "Setting spent status", null, "spentBlockIndex:", unsignedLong, "spentBlockTimeStamp:", date);
        this.spentBlockIndex = unsignedLong;
        this.spentBlockTimestamp = date;
    }

    public UnsignedLong getTxOutGlobalIndex() {
        return this.txOutGlobalIndex;
    }

    public int getKeyImageHashCode() {
        if (this.keyImageHash == 0) {
            this.keyImageHash = Arrays.hashCode(this.keyImage);
        }
        return this.keyImageHash;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        OwnedTxOut ownedTxOut = (OwnedTxOut) obj;
        if (!this.txOutGlobalIndex.equals(ownedTxOut.txOutGlobalIndex) || !this.receivedBlockIndex.equals(ownedTxOut.receivedBlockIndex) || !this.spentBlockIndex.equals(ownedTxOut.spentBlockIndex) || !this.value.equals(ownedTxOut.value) || !this.txOutPublicKey.equals(ownedTxOut.txOutPublicKey) || !Arrays.equals(this.keyImage, ownedTxOut.keyImage)) {
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return (Objects.hash(this.txOutGlobalIndex, this.receivedBlockIndex, this.spentBlockIndex, this.value, this.txOutPublicKey) * 31) + Arrays.hashCode(this.keyImage);
    }
}
