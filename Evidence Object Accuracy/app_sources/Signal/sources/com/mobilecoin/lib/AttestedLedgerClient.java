package com.mobilecoin.lib;

import attest.Attest$AuthMessage;
import attest.Attest$Message;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.mobilecoin.api.MobileCoinAPI$KeyImage;
import com.mobilecoin.lib.ClientConfig;
import com.mobilecoin.lib.exceptions.AttestationException;
import com.mobilecoin.lib.exceptions.InvalidFogResponse;
import com.mobilecoin.lib.exceptions.NetworkException;
import com.mobilecoin.lib.log.Logger;
import com.mobilecoin.lib.uri.FogUri;
import com.mobilecoin.lib.util.NetworkingCall;
import fog_ledger.FogKeyImageAPIGrpc;
import fog_ledger.FogMerkleProofAPIGrpc;
import fog_ledger.Ledger$CheckKeyImagesRequest;
import fog_ledger.Ledger$CheckKeyImagesResponse;
import fog_ledger.Ledger$GetOutputsRequest;
import fog_ledger.Ledger$GetOutputsResponse;
import fog_ledger.Ledger$KeyImageQuery;
import io.grpc.ManagedChannel;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;

/* loaded from: classes3.dex */
public final class AttestedLedgerClient extends AttestedClient {
    private static final String TAG;

    public AttestedLedgerClient(FogUri fogUri, ClientConfig.Service service) {
        super(fogUri.getUri(), service);
        Logger.i(TAG, "Created new AttestedLedgerClient", null, "uri:", fogUri, "verifier:", service);
    }

    @Override // com.mobilecoin.lib.AttestedClient
    protected synchronized void attest(ManagedChannel managedChannel) throws AttestationException, NetworkException {
        try {
            try {
                Logger.i(TAG, "Attest ledger connection");
                byte[] attestStart = attestStart(getServiceUri());
                attestFinish(getAPIManager().getFogKeyImageAPIStub(managedChannel).auth(Attest$AuthMessage.newBuilder().setData(ByteString.copyFrom(attestStart)).build()).getData().toByteArray(), getServiceConfig().getVerifier());
            } catch (StatusRuntimeException e) {
                attestReset();
                if (e.getStatus().getCode() == Status.Code.INTERNAL) {
                    AttestationException attestationException = new AttestationException(e.getStatus().getDescription(), e);
                    Util.logException(TAG, attestationException);
                    throw attestationException;
                }
                NetworkException networkException = new NetworkException(e);
                Util.logException(TAG, networkException);
                throw networkException;
            }
        } catch (Exception e2) {
            AttestationException attestationException2 = new AttestationException("Failed to attest the ledger connection", e2);
            Util.logException(TAG, attestationException2);
            throw attestationException2;
        }
    }

    public synchronized Ledger$GetOutputsResponse getOutputs(Collection<UnsignedLong> collection, long j) throws InvalidFogResponse, AttestationException, NetworkException {
        Exception e;
        Logger.i(TAG, "Retrieving outputs");
        try {
        } catch (AttestationException e2) {
            e = e2;
            Util.logException(TAG, e);
            throw e;
        } catch (InvalidFogResponse e3) {
            e = e3;
            Util.logException(TAG, e);
            throw e;
        } catch (NetworkException e4) {
            e = e4;
            Util.logException(TAG, e);
            throw e;
        } catch (RuntimeException e5) {
            e = e5;
            Util.logException(TAG, e);
            throw e;
        } catch (Exception unused) {
            throw new IllegalStateException("BUG: unreachable code");
        }
        return (Ledger$GetOutputsResponse) new NetworkingCall(new Callable(getAPIManager().getMerkleProofAPIStub(getManagedChannel()), encryptMessage(Ledger$GetOutputsRequest.newBuilder().addAllIndices((Iterable) Collection$EL.stream(collection).map(new Function() { // from class: com.mobilecoin.lib.AttestedLedgerClient$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return Long.valueOf(((UnsignedLong) obj).longValue());
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList())).setMerkleRootBlock(j).build())) { // from class: com.mobilecoin.lib.AttestedLedgerClient$$ExternalSyntheticLambda2
            public final /* synthetic */ FogMerkleProofAPIGrpc.FogMerkleProofAPIBlockingStub f$1;
            public final /* synthetic */ Attest$Message f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return AttestedLedgerClient.$r8$lambda$o9MXfQOuZ8_LfZbAINLPUzmuc7w(AttestedLedgerClient.this, this.f$1, this.f$2);
            }
        }).run();
    }

    public /* synthetic */ Ledger$GetOutputsResponse lambda$getOutputs$0(FogMerkleProofAPIGrpc.FogMerkleProofAPIBlockingStub fogMerkleProofAPIBlockingStub, Attest$Message attest$Message) throws Exception {
        try {
            return Ledger$GetOutputsResponse.parseFrom(decryptMessage(fogMerkleProofAPIBlockingStub.getOutputs(attest$Message)).getData());
        } catch (InvalidProtocolBufferException e) {
            throw new InvalidFogResponse("GetOutputsResponse contains invalid data", e);
        } catch (StatusRuntimeException e2) {
            attestReset();
            throw new NetworkException(e2);
        }
    }

    public synchronized Ledger$CheckKeyImagesResponse checkKeyImages(Set<KeyImage> set) throws InvalidFogResponse, AttestationException, NetworkException {
        FogKeyImageAPIGrpc.FogKeyImageAPIBlockingStub fogKeyImageAPIStub;
        ArrayList arrayList;
        Exception e;
        Logger.i(TAG, "Checking key images", null, "size:", Integer.valueOf(set.size()));
        fogKeyImageAPIStub = getAPIManager().getFogKeyImageAPIStub(getManagedChannel());
        arrayList = new ArrayList();
        for (KeyImage keyImage : set) {
            arrayList.add(Ledger$KeyImageQuery.newBuilder().setKeyImage(MobileCoinAPI$KeyImage.newBuilder().setData(ByteString.copyFrom(keyImage.getData())).build()).build());
        }
        try {
        } catch (AttestationException e2) {
            e = e2;
            Util.logException(TAG, e);
            throw e;
        } catch (InvalidFogResponse e3) {
            e = e3;
            Util.logException(TAG, e);
            throw e;
        } catch (NetworkException e4) {
            e = e4;
            Util.logException(TAG, e);
            throw e;
        } catch (RuntimeException e5) {
            e = e5;
            Util.logException(TAG, e);
            throw e;
        } catch (Exception unused) {
            throw new IllegalStateException("BUG: unreachable code");
        }
        return (Ledger$CheckKeyImagesResponse) new NetworkingCall(new Callable(fogKeyImageAPIStub, encryptMessage(Ledger$CheckKeyImagesRequest.newBuilder().addAllQueries(arrayList).build())) { // from class: com.mobilecoin.lib.AttestedLedgerClient$$ExternalSyntheticLambda0
            public final /* synthetic */ FogKeyImageAPIGrpc.FogKeyImageAPIBlockingStub f$1;
            public final /* synthetic */ Attest$Message f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return AttestedLedgerClient.$r8$lambda$3PPJyHe3lTIbFp45npUBLK6O64U(AttestedLedgerClient.this, this.f$1, this.f$2);
            }
        }).run();
    }

    public /* synthetic */ Ledger$CheckKeyImagesResponse lambda$checkKeyImages$1(FogKeyImageAPIGrpc.FogKeyImageAPIBlockingStub fogKeyImageAPIBlockingStub, Attest$Message attest$Message) throws Exception {
        try {
            return Ledger$CheckKeyImagesResponse.parseFrom(decryptMessage(fogKeyImageAPIBlockingStub.checkKeyImages(attest$Message)).getData().toByteArray());
        } catch (InvalidProtocolBufferException e) {
            throw new InvalidFogResponse("CheckKeyImagesResponse contains invalid data", e);
        } catch (StatusRuntimeException e2) {
            attestReset();
            throw new NetworkException(e2);
        }
    }

    public synchronized Ledger$CheckKeyImagesResponse checkUtxoKeyImages(Set<OwnedTxOut> set) throws InvalidFogResponse, AttestationException, NetworkException {
        HashSet hashSet;
        Logger.i(TAG, "Checking unspent OwnedTxOut key images");
        hashSet = new HashSet();
        for (OwnedTxOut ownedTxOut : set) {
            hashSet.add(ownedTxOut.getKeyImage());
        }
        return checkKeyImages(hashSet);
    }
}
