package com.mobilecoin.lib;

import com.mobilecoin.lib.exceptions.FogReportException;
import com.mobilecoin.lib.log.Logger;
import java.util.List;

/* loaded from: classes3.dex */
public final class ReportResponse extends Native {
    private static final String TAG;
    private final byte[][] chain;
    private final List<Report> reports;
    private final byte[] signature;

    private native void finalize_jni();

    private native void init_jni(Report[] reportArr, byte[][] bArr, byte[] bArr2);

    public ReportResponse(List<Report> list, byte[][] bArr, byte[] bArr2) throws FogReportException {
        this.reports = list;
        this.chain = bArr;
        this.signature = bArr2;
        try {
            init_jni((Report[]) list.toArray(new Report[0]), bArr, bArr2);
        } catch (Exception e) {
            FogReportException fogReportException = new FogReportException("Unable to create report response", e);
            Util.logException(TAG, fogReportException);
            throw fogReportException;
        }
    }

    public List<Report> getReports() {
        Logger.i(TAG, "Getting reports", null, this.reports);
        return this.reports;
    }

    protected void finalize() throws Throwable {
        try {
            finalize_jni();
        } catch (Exception e) {
            Logger.e(TAG, "Failed to free fog report response data", e, new Object[0]);
        }
        super.finalize();
    }
}
