package com.mobilecoin.lib;

import com.google.protobuf.ByteString;
import com.mobilecoin.api.MobileCoinAPI$Amount;
import com.mobilecoin.api.MobileCoinAPI$CompressedRistretto;
import com.mobilecoin.lib.exceptions.AmountDecoderException;
import com.mobilecoin.lib.log.Logger;
import java.math.BigInteger;

/* loaded from: classes3.dex */
public final class Amount extends Native {
    private static final String TAG;
    private final MobileCoinAPI$Amount protoBufAmount;

    private native void finalize_jni();

    private native void init_jni(byte[] bArr, long j);

    private native BigInteger unmask_value(RistrettoPrivate ristrettoPrivate, RistrettoPublic ristrettoPublic);

    public Amount(byte[] bArr, long j) throws AmountDecoderException {
        this.protoBufAmount = MobileCoinAPI$Amount.newBuilder().setCommitment(MobileCoinAPI$CompressedRistretto.newBuilder().setData(ByteString.copyFrom(bArr)).build()).setMaskedValue(j).build();
        try {
            init_jni(bArr, j);
        } catch (Exception e) {
            AmountDecoderException amountDecoderException = new AmountDecoderException("Unable to initialize amount object", e);
            Util.logException(TAG, amountDecoderException);
            throw amountDecoderException;
        }
    }

    Amount(MobileCoinAPI$Amount mobileCoinAPI$Amount) throws AmountDecoderException {
        this(mobileCoinAPI$Amount.getCommitment().getData().toByteArray(), mobileCoinAPI$Amount.getMaskedValue());
    }

    public static Amount fromProtoBufObject(MobileCoinAPI$Amount mobileCoinAPI$Amount) throws AmountDecoderException {
        Logger.i(TAG, "Deserializing amount from protobuf object");
        return new Amount(mobileCoinAPI$Amount);
    }

    public MobileCoinAPI$Amount toProtoBufObject() {
        return this.protoBufAmount;
    }

    public BigInteger unmaskValue(RistrettoPrivate ristrettoPrivate, RistrettoPublic ristrettoPublic) throws AmountDecoderException {
        Logger.i(TAG, "Unmasking amount");
        try {
            return unmask_value(ristrettoPrivate, ristrettoPublic);
        } catch (Exception e) {
            AmountDecoderException amountDecoderException = new AmountDecoderException("Unable to unmask the amount", e);
            Util.logException(TAG, amountDecoderException);
            throw amountDecoderException;
        }
    }

    protected void finalize() throws Throwable {
        Logger.i(TAG, "Finalizing object");
        if (this.rustObj != 0) {
            finalize_jni();
        }
        super.finalize();
    }
}
