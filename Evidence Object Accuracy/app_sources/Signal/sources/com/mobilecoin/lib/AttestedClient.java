package com.mobilecoin.lib;

import android.net.Uri;
import attest.Attest$Message;
import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.mobilecoin.lib.ClientConfig;
import com.mobilecoin.lib.exceptions.AttestationException;
import com.mobilecoin.lib.exceptions.NetworkException;
import com.mobilecoin.lib.log.Logger;
import io.grpc.ManagedChannel;

/* loaded from: classes3.dex */
public abstract class AttestedClient extends AnyClient {
    private static final String TAG;

    private native void attest_finish(byte[] bArr, Verifier verifier);

    private native byte[] attest_start(ResponderId responderId);

    private native byte[] decrypt_payload(byte[] bArr, byte[] bArr2);

    private native byte[] encrypt_payload(byte[] bArr, byte[] bArr2);

    private native void finalize_jni();

    private native byte[] get_binding();

    protected abstract void attest(ManagedChannel managedChannel) throws AttestationException, NetworkException;

    public AttestedClient(Uri uri, ClientConfig.Service service) {
        super(uri, service);
    }

    @Override // com.mobilecoin.lib.AnyClient
    public synchronized ManagedChannel getManagedChannel() throws AttestationException, NetworkException {
        ManagedChannel managedChannel;
        managedChannel = super.getManagedChannel();
        if (!isAttested()) {
            attest(managedChannel);
        }
        return managedChannel;
    }

    protected synchronized void deattest() {
        Logger.i(TAG, "De-attesting the managed channel");
        attestReset();
    }

    protected synchronized boolean isAttested() {
        boolean z;
        String str = TAG;
        z = true;
        Object[] objArr = new Object[1];
        objArr[0] = this.rustObj != 0 ? "Yes" : "No";
        Logger.i(str, "Is channel attested?", null, objArr);
        if (this.rustObj == 0) {
            z = false;
        }
        return z;
    }

    public synchronized Attest$Message encryptMessage(AbstractMessageLite<?, ?> abstractMessageLite, AbstractMessageLite<?, ?> abstractMessageLite2) throws AttestationException {
        byte[] byteArray;
        Logger.i(TAG, "Encrypting request message for attested channel");
        byteArray = abstractMessageLite2 != null ? abstractMessageLite2.toByteArray() : new byte[0];
        return Attest$Message.newBuilder().setData(ByteString.copyFrom(encryptPayload(abstractMessageLite != null ? abstractMessageLite.toByteArray() : new byte[0], byteArray))).setChannelId(ByteString.copyFrom(getBinding())).setAad(ByteString.copyFrom(byteArray)).build();
    }

    public synchronized Attest$Message encryptMessage(AbstractMessageLite<?, ?> abstractMessageLite) throws AttestationException {
        return encryptMessage(abstractMessageLite, null);
    }

    public synchronized Attest$Message decryptMessage(Attest$Message attest$Message) throws AttestationException {
        Logger.i(TAG, "Decrypt response message");
        try {
        } catch (Exception e) {
            AttestationException attestationException = new AttestationException("Unable to decrypt response message", e);
            Util.logException(TAG, attestationException);
            throw attestationException;
        }
        return Attest$Message.newBuilder(attest$Message).setData(ByteString.copyFrom(decryptPayload(attest$Message.getData().toByteArray(), attest$Message.getAad().toByteArray()))).build();
    }

    public byte[] attestStart(Uri uri) throws AttestationException {
        Logger.i(TAG, "FFI: attest_start call");
        try {
            return attest_start(new ResponderId(uri));
        } catch (Exception e) {
            AttestationException attestationException = new AttestationException("Unable to start attestation", e);
            Util.logException(TAG, attestationException);
            attestReset();
            throw attestationException;
        }
    }

    public void attestFinish(byte[] bArr, Verifier verifier) throws AttestationException {
        Logger.i(TAG, "FFI: attest_finish call");
        try {
            attest_finish(bArr, verifier);
        } catch (Exception e) {
            AttestationException attestationException = new AttestationException("Unable to finish attestation", e);
            Util.logException(TAG, attestationException);
            attestReset();
            throw attestationException;
        }
    }

    public synchronized void attestReset() {
        Logger.i(TAG, "Reset attested state");
        if (this.rustObj != 0) {
            try {
                finalize_jni();
            } catch (Exception e) {
                Logger.e(TAG, "Unable to free attested state", e, new Object[0]);
            }
            this.rustObj = 0;
        }
        shutdown();
    }

    @Override // com.mobilecoin.lib.AnyClient
    public void setAuthorization(String str, String str2) {
        Logger.i(TAG, "Set API authorization");
        getAPIManager().setAuthorization(str, str2);
    }

    protected void finalize() throws Throwable {
        Logger.i(TAG, "Destroying attested client object");
        deattest();
        if (this.rustObj != 0) {
            finalize_jni();
        }
        super.finalize();
    }

    private synchronized byte[] getBinding() throws AttestationException {
        try {
            Logger.i(TAG, "FFI: get_binding call");
        } catch (Exception e) {
            AttestationException attestationException = new AttestationException("Unable to get connection binding", e);
            Util.logException(TAG, attestationException);
            attestReset();
            throw attestationException;
        }
        return get_binding();
    }

    private synchronized byte[] decryptPayload(byte[] bArr, byte[] bArr2) throws AttestationException {
        Logger.i(TAG, "FFI: decrypt_payload call");
        try {
        } catch (Exception e) {
            AttestationException attestationException = new AttestationException("Unable to decrypt payload", e);
            Util.logException(TAG, attestationException);
            attestReset();
            throw attestationException;
        }
        return decrypt_payload(bArr, bArr2);
    }

    private synchronized byte[] encryptPayload(byte[] bArr, byte[] bArr2) throws AttestationException {
        Logger.i(TAG, "FFI: encrypt_payload call");
        try {
        } catch (Exception e) {
            AttestationException attestationException = new AttestationException("Unable to encrypt payload", e);
            Util.logException(TAG, attestationException);
            attestReset();
            throw attestationException;
        }
        return encrypt_payload(bArr, bArr2);
    }
}
