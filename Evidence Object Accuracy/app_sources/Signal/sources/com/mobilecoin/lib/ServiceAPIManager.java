package com.mobilecoin.lib;

import attest.AttestedApiGrpc;
import com.mobilecoin.lib.log.Logger;
import consensus_client.ConsensusClientAPIGrpc;
import consensus_common.BlockchainAPIGrpc;
import fog_ledger.FogBlockAPIGrpc;
import fog_ledger.FogKeyImageAPIGrpc;
import fog_ledger.FogMerkleProofAPIGrpc;
import fog_ledger.FogUntrustedTxOutApiGrpc;
import fog_view.FogViewAPIGrpc;
import io.grpc.ManagedChannel;
import io.grpc.stub.AbstractStub;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import report.ReportAPIGrpc;

/* loaded from: classes3.dex */
public final class ServiceAPIManager {
    private static final String TAG = AttestedClient.class.toString();
    private AttestedApiGrpc.AttestedApiBlockingStub attestedApiBlockingStub;
    private final AuthInterceptor authInterceptor = new AuthInterceptor();
    private BlockchainAPIGrpc.BlockchainAPIBlockingStub blockchainAPIBlockingStub;
    private ConsensusClientAPIGrpc.ConsensusClientAPIBlockingStub consensusClientAPIBlockingStub;
    private final CookieInterceptor cookieInterceptor = new CookieInterceptor();
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();
    private FogBlockAPIGrpc.FogBlockAPIBlockingStub fogBlockAPIBlockingStub;
    private FogKeyImageAPIGrpc.FogKeyImageAPIBlockingStub fogKeyImageAPIBlockingStub;
    private FogMerkleProofAPIGrpc.FogMerkleProofAPIBlockingStub fogMerkleProofAPIBlockingStub;
    private FogUntrustedTxOutApiGrpc.FogUntrustedTxOutApiBlockingStub fogUntrustedTxOutApiBlockingStub;
    private FogViewAPIGrpc.FogViewAPIBlockingStub fogViewAPIBlockingStub;
    private ReportAPIGrpc.ReportAPIBlockingStub reportAPIBlockingStub;

    <T extends AbstractStub<T>> T configureStub(T t) {
        return (T) t.withInterceptors(this.cookieInterceptor, this.authInterceptor).withMaxInboundMessageSize(52428800).withExecutor(this.executorService);
    }

    public synchronized FogViewAPIGrpc.FogViewAPIBlockingStub getFogViewAPIStub(ManagedChannel managedChannel) {
        if (this.fogViewAPIBlockingStub == null) {
            this.fogViewAPIBlockingStub = (FogViewAPIGrpc.FogViewAPIBlockingStub) configureStub(FogViewAPIGrpc.newBlockingStub(managedChannel));
        }
        return this.fogViewAPIBlockingStub;
    }

    public synchronized FogUntrustedTxOutApiGrpc.FogUntrustedTxOutApiBlockingStub getFogUntrustedTxOutApiBlockingStub(ManagedChannel managedChannel) {
        if (this.fogUntrustedTxOutApiBlockingStub == null) {
            this.fogUntrustedTxOutApiBlockingStub = (FogUntrustedTxOutApiGrpc.FogUntrustedTxOutApiBlockingStub) configureStub(FogUntrustedTxOutApiGrpc.newBlockingStub(managedChannel));
        }
        return this.fogUntrustedTxOutApiBlockingStub;
    }

    public synchronized ReportAPIGrpc.ReportAPIBlockingStub getReportAPIStub(ManagedChannel managedChannel) {
        if (this.reportAPIBlockingStub == null) {
            this.reportAPIBlockingStub = (ReportAPIGrpc.ReportAPIBlockingStub) configureStub(ReportAPIGrpc.newBlockingStub(managedChannel));
        }
        return this.reportAPIBlockingStub;
    }

    public synchronized FogKeyImageAPIGrpc.FogKeyImageAPIBlockingStub getFogKeyImageAPIStub(ManagedChannel managedChannel) {
        if (this.fogKeyImageAPIBlockingStub == null) {
            this.fogKeyImageAPIBlockingStub = (FogKeyImageAPIGrpc.FogKeyImageAPIBlockingStub) configureStub(FogKeyImageAPIGrpc.newBlockingStub(managedChannel));
        }
        return this.fogKeyImageAPIBlockingStub;
    }

    public synchronized FogMerkleProofAPIGrpc.FogMerkleProofAPIBlockingStub getMerkleProofAPIStub(ManagedChannel managedChannel) {
        if (this.fogMerkleProofAPIBlockingStub == null) {
            this.fogMerkleProofAPIBlockingStub = (FogMerkleProofAPIGrpc.FogMerkleProofAPIBlockingStub) configureStub(FogMerkleProofAPIGrpc.newBlockingStub(managedChannel));
        }
        return this.fogMerkleProofAPIBlockingStub;
    }

    public synchronized FogBlockAPIGrpc.FogBlockAPIBlockingStub getBlockAPIStub(ManagedChannel managedChannel) {
        if (this.fogBlockAPIBlockingStub == null) {
            this.fogBlockAPIBlockingStub = (FogBlockAPIGrpc.FogBlockAPIBlockingStub) configureStub(FogBlockAPIGrpc.newBlockingStub(managedChannel));
        }
        return this.fogBlockAPIBlockingStub;
    }

    public synchronized ConsensusClientAPIGrpc.ConsensusClientAPIBlockingStub getConsensusAPIStub(ManagedChannel managedChannel) {
        if (this.consensusClientAPIBlockingStub == null) {
            this.consensusClientAPIBlockingStub = (ConsensusClientAPIGrpc.ConsensusClientAPIBlockingStub) configureStub(ConsensusClientAPIGrpc.newBlockingStub(managedChannel));
        }
        return this.consensusClientAPIBlockingStub;
    }

    public synchronized BlockchainAPIGrpc.BlockchainAPIBlockingStub getBlockchainAPIStub(ManagedChannel managedChannel) {
        if (this.blockchainAPIBlockingStub == null) {
            this.blockchainAPIBlockingStub = (BlockchainAPIGrpc.BlockchainAPIBlockingStub) configureStub(BlockchainAPIGrpc.newBlockingStub(managedChannel));
        }
        return this.blockchainAPIBlockingStub;
    }

    public synchronized AttestedApiGrpc.AttestedApiBlockingStub getAttestedAPIStub(ManagedChannel managedChannel) {
        if (this.attestedApiBlockingStub == null) {
            this.attestedApiBlockingStub = (AttestedApiGrpc.AttestedApiBlockingStub) configureStub(AttestedApiGrpc.newBlockingStub(managedChannel));
        }
        return this.attestedApiBlockingStub;
    }

    void retireExecutorService(ExecutorService executorService) {
        if (executorService != null) {
            executorService.shutdown();
            try {
                TimeUnit timeUnit = TimeUnit.MILLISECONDS;
                if (!executorService.awaitTermination(5000, timeUnit)) {
                    executorService.shutdownNow();
                    if (!executorService.awaitTermination(5000, timeUnit)) {
                        Logger.e(TAG, "Unable to terminate ExecutorService");
                    }
                }
            } catch (InterruptedException e) {
                String localizedMessage = e.getLocalizedMessage();
                String str = TAG;
                if (localizedMessage == null) {
                    localizedMessage = e.toString();
                }
                Logger.e(str, localizedMessage);
                executorService.shutdownNow();
            }
        }
    }

    public void setAuthorization(String str, String str2) {
        this.authInterceptor.setAuthorization(str, str2);
    }

    public synchronized void reset() {
        this.fogViewAPIBlockingStub = null;
        this.reportAPIBlockingStub = null;
        this.fogKeyImageAPIBlockingStub = null;
        this.fogMerkleProofAPIBlockingStub = null;
        this.fogBlockAPIBlockingStub = null;
        this.consensusClientAPIBlockingStub = null;
        this.attestedApiBlockingStub = null;
    }

    protected void finalize() throws Throwable {
        retireExecutorService(this.executorService);
        super.finalize();
    }
}
