package com.mobilecoin.lib;

import java.util.Arrays;

/* loaded from: classes3.dex */
public final class KeyImage {
    private final byte[] data;

    private KeyImage(byte[] bArr) {
        this.data = bArr;
    }

    public static KeyImage fromBytes(byte[] bArr) {
        return new KeyImage(bArr);
    }

    public byte[] getData() {
        return this.data;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || KeyImage.class != obj.getClass()) {
            return false;
        }
        return Arrays.equals(getData(), ((KeyImage) obj).getData());
    }

    public int hashCode() {
        return Arrays.hashCode(this.data);
    }
}
