package com.mobilecoin.lib;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.mobilecoin.api.MobileCoinAPI$Amount;
import com.mobilecoin.api.MobileCoinAPI$Receipt;
import com.mobilecoin.api.MobileCoinAPI$TxOutConfirmationNumber;
import com.mobilecoin.lib.exceptions.AmountDecoderException;
import com.mobilecoin.lib.exceptions.SerializationException;
import java.math.BigInteger;

/* loaded from: classes3.dex */
public final class Receipt {
    private final RistrettoPublic publicKey;
    private final MobileCoinAPI$Receipt receiptBuf;

    private Receipt(byte[] bArr) throws SerializationException {
        try {
            MobileCoinAPI$Receipt parseFrom = MobileCoinAPI$Receipt.parseFrom(bArr);
            this.receiptBuf = parseFrom;
            this.publicKey = RistrettoPublic.fromBytes(parseFrom.getPublicKey().getData().toByteArray());
        } catch (InvalidProtocolBufferException e) {
            throw new SerializationException(e.getLocalizedMessage());
        }
    }

    public Receipt(RistrettoPublic ristrettoPublic, byte[] bArr, Amount amount, UnsignedLong unsignedLong) {
        MobileCoinAPI$Receipt.Builder newBuilder = MobileCoinAPI$Receipt.newBuilder();
        newBuilder.setTombstoneBlock(unsignedLong.longValue());
        newBuilder.setAmount(amount.toProtoBufObject());
        newBuilder.setConfirmation(MobileCoinAPI$TxOutConfirmationNumber.newBuilder().setHash(ByteString.copyFrom(bArr)).build());
        newBuilder.setPublicKey(ristrettoPublic.toProtoBufObject());
        this.receiptBuf = newBuilder.build();
        this.publicKey = ristrettoPublic;
    }

    public static Receipt fromBytes(byte[] bArr) throws SerializationException {
        return new Receipt(bArr);
    }

    public BigInteger getAmount(AccountKey accountKey) throws AmountDecoderException {
        if (this.receiptBuf.hasAmount()) {
            MobileCoinAPI$Amount amount = this.receiptBuf.getAmount();
            return new Amount(amount.getCommitment().getData().toByteArray(), amount.getMaskedValue()).unmaskValue(accountKey.getViewKey(), getPublicKey());
        }
        throw new AmountDecoderException("Receipt does not contain an encoded amount");
    }

    public byte[] toByteArray() {
        return this.receiptBuf.toByteArray();
    }

    public RistrettoPublic getPublicKey() {
        return this.publicKey;
    }

    public UnsignedLong getTombstoneBlockIndex() {
        return UnsignedLong.fromLongBits(this.receiptBuf.getTombstoneBlock());
    }

    /* loaded from: classes3.dex */
    public enum Status {
        UNKNOWN,
        RECEIVED,
        FAILED;
        
        private UnsignedLong blockIndex;

        public synchronized Status atBlock(UnsignedLong unsignedLong) {
            this.blockIndex = unsignedLong;
            return this;
        }

        public synchronized UnsignedLong getBlockIndex() {
            return this.blockIndex;
        }
    }
}
