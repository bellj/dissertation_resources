package com.mobilecoin.lib;

import android.net.Uri;
import com.mobilecoin.api.Printable$PrintableWrapper;
import com.mobilecoin.lib.exceptions.InvalidUriException;
import com.mobilecoin.lib.exceptions.SerializationException;
import com.mobilecoin.lib.log.Logger;
import com.mobilecoin.lib.uri.MobUri;

/* loaded from: classes3.dex */
public final class PrintableWrapper extends Native {
    private static final String TAG;
    private final PaymentRequest paymentRequest;
    private final Printable$PrintableWrapper protoBuf;
    private final PublicAddress publicAddress;
    private final TransferPayload transferPayload;

    private static native byte[] b58_decode(String str);

    private static native String b58_encode(byte[] bArr);

    PrintableWrapper(Printable$PrintableWrapper printable$PrintableWrapper) throws SerializationException {
        this.protoBuf = printable$PrintableWrapper;
        if (printable$PrintableWrapper.hasPublicAddress()) {
            this.publicAddress = PublicAddress.fromProtoBufObject(printable$PrintableWrapper.getPublicAddress());
        } else {
            this.publicAddress = null;
        }
        if (printable$PrintableWrapper.hasPaymentRequest()) {
            this.paymentRequest = PaymentRequest.fromProtoBufObject(printable$PrintableWrapper.getPaymentRequest());
        } else {
            this.paymentRequest = null;
        }
        if (printable$PrintableWrapper.hasTransferPayload()) {
            this.transferPayload = TransferPayload.fromProtoBufObject(printable$PrintableWrapper.getTransferPayload());
        } else {
            this.transferPayload = null;
        }
    }

    public static PrintableWrapper fromPublicAddress(PublicAddress publicAddress) throws SerializationException {
        Logger.i(TAG, "Creating printable wrapper from public address", null, publicAddress);
        return new PrintableWrapper(Printable$PrintableWrapper.newBuilder().setPublicAddress(publicAddress.toProtoBufObject()).build());
    }

    public static PrintableWrapper fromB58String(String str) throws SerializationException {
        Logger.i(TAG, "Creating printable wrapper from base58_string", null, str);
        try {
            return new PrintableWrapper(Printable$PrintableWrapper.parseFrom(b58_decode(str)));
        } catch (Exception e) {
            SerializationException serializationException = new SerializationException(e.getLocalizedMessage());
            Util.logException(TAG, serializationException);
            throw serializationException;
        }
    }

    public String toB58String() throws SerializationException {
        Logger.i(TAG, "Converting printable wrapper to base58");
        try {
            return b58_encode(this.protoBuf.toByteString().toByteArray());
        } catch (Exception e) {
            SerializationException serializationException = new SerializationException(e.getLocalizedMessage());
            Util.logException(TAG, serializationException);
            throw serializationException;
        }
    }

    public Uri toUri() throws SerializationException {
        Logger.i(TAG, "Converting printable wrapper to URI");
        try {
            return MobUri.fromB58(toB58String()).getUri();
        } catch (InvalidUriException unused) {
            IllegalStateException illegalStateException = new IllegalStateException("BUG: unreachable code");
            Util.logException(TAG, illegalStateException);
            throw illegalStateException;
        }
    }

    public static PrintableWrapper fromUri(Uri uri) throws InvalidUriException, SerializationException {
        Logger.i(TAG, "Getting printable wrapper from URI", null, uri);
        return fromB58String(MobUri.fromUri(uri).getPayload());
    }

    public PublicAddress getPublicAddress() {
        Logger.i(TAG, "Getting public address", null, this.publicAddress);
        return this.publicAddress;
    }
}
