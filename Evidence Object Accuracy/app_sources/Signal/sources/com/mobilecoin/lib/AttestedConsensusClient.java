package com.mobilecoin.lib;

import attest.Attest$AuthMessage;
import com.google.protobuf.ByteString;
import com.mobilecoin.api.MobileCoinAPI$Tx;
import com.mobilecoin.lib.ClientConfig;
import com.mobilecoin.lib.exceptions.AttestationException;
import com.mobilecoin.lib.exceptions.NetworkException;
import com.mobilecoin.lib.log.Logger;
import com.mobilecoin.lib.uri.ConsensusUri;
import com.mobilecoin.lib.util.NetworkingCall;
import consensus_common.ConsensusCommon$ProposeTxResponse;
import io.grpc.ManagedChannel;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import java.util.concurrent.Callable;

/* loaded from: classes3.dex */
public final class AttestedConsensusClient extends AttestedClient {
    private static final String TAG;

    public AttestedConsensusClient(ConsensusUri consensusUri, ClientConfig.Service service) {
        super(consensusUri.getUri(), service);
        Logger.i(TAG, "Created new AttestedConsensusClient", null, "uri:", consensusUri, "verifier:", service);
    }

    @Override // com.mobilecoin.lib.AttestedClient
    protected synchronized void attest(ManagedChannel managedChannel) throws AttestationException, NetworkException {
        try {
            try {
                Logger.i(TAG, "Attest consensus connection");
                byte[] attestStart = attestStart(getServiceUri());
                attestFinish(getAPIManager().getAttestedAPIStub(managedChannel).auth(Attest$AuthMessage.newBuilder().setData(ByteString.copyFrom(attestStart)).build()).getData().toByteArray(), getServiceConfig().getVerifier());
            } catch (StatusRuntimeException e) {
                attestReset();
                if (e.getStatus().getCode() == Status.Code.INTERNAL) {
                    AttestationException attestationException = new AttestationException(e.getStatus().getDescription(), e);
                    Util.logException(TAG, attestationException);
                    throw attestationException;
                }
                NetworkException networkException = new NetworkException(e);
                Util.logException(TAG, networkException);
                throw networkException;
            }
        } catch (Exception e2) {
            AttestationException attestationException2 = new AttestationException("Failed to attest the consensus connection", e2);
            Util.logException(TAG, attestationException2);
            throw attestationException2;
        }
    }

    public synchronized ConsensusCommon$ProposeTxResponse proposeTx(MobileCoinAPI$Tx mobileCoinAPI$Tx) throws AttestationException, NetworkException {
        Exception e;
        try {
        } catch (AttestationException e2) {
            e = e2;
            Util.logException(TAG, e);
            throw e;
        } catch (NetworkException e3) {
            e = e3;
            Util.logException(TAG, e);
            throw e;
        } catch (RuntimeException e4) {
            e = e4;
            Util.logException(TAG, e);
            throw e;
        } catch (Exception unused) {
            throw new IllegalStateException("BUG: unreachable code");
        }
        return (ConsensusCommon$ProposeTxResponse) new NetworkingCall(new Callable(mobileCoinAPI$Tx) { // from class: com.mobilecoin.lib.AttestedConsensusClient$$ExternalSyntheticLambda0
            public final /* synthetic */ MobileCoinAPI$Tx f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return AttestedConsensusClient.m128$r8$lambda$sY92x4u0f_TihnccaPISbPqMPE(AttestedConsensusClient.this, this.f$1);
            }
        }).run();
    }

    public /* synthetic */ ConsensusCommon$ProposeTxResponse lambda$proposeTx$0(MobileCoinAPI$Tx mobileCoinAPI$Tx) throws Exception {
        Logger.i(TAG, "Propose transaction to consensus");
        try {
            return getAPIManager().getConsensusAPIStub(getManagedChannel()).clientTxPropose(encryptMessage(mobileCoinAPI$Tx));
        } catch (StatusRuntimeException e) {
            attestReset();
            throw new NetworkException(e);
        }
    }
}
