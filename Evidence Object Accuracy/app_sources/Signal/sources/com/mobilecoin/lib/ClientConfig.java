package com.mobilecoin.lib;

import com.mobilecoin.lib.log.LogAdapter;
import j$.time.Duration;
import java.security.cert.X509Certificate;
import java.util.Objects;
import java.util.Set;

/* loaded from: classes3.dex */
public final class ClientConfig {
    public Service consensus;
    public Service fogLedger;
    public Service fogView;
    public LogAdapter logAdapter;
    public Duration minimumFeeCacheTTL = Duration.ofMinutes(30);

    /* renamed from: report */
    public Service f12report;

    /* loaded from: classes3.dex */
    public static final class Service {
        private Set<X509Certificate> trustRoots;
        private Verifier verifier;

        public Service withVerifier(Verifier verifier) {
            this.verifier = verifier;
            return this;
        }

        public Service withTrustRoots(Set<X509Certificate> set) {
            this.trustRoots = set;
            return this;
        }

        public Verifier getVerifier() {
            Verifier verifier = this.verifier;
            Objects.requireNonNull(verifier);
            return verifier;
        }

        public Set<X509Certificate> getTrustRoots() {
            return this.trustRoots;
        }
    }
}
