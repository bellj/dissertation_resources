package com.mobilecoin.lib;

import com.google.protobuf.InvalidProtocolBufferException;
import com.mobilecoin.api.MobileCoinAPI$TxOut;
import com.mobilecoin.lib.exceptions.AmountDecoderException;
import com.mobilecoin.lib.exceptions.SerializationException;
import com.mobilecoin.lib.log.Logger;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class TxOut extends Native {
    private static final String TAG;
    private final MobileCoinAPI$TxOut protoBufTxOut;
    private final RistrettoPublic pubKey;

    private native byte[] compute_key_image(AccountKey accountKey);

    private native byte[] encode();

    private native void finalize_jni();

    private native void init_from_protobuf_bytes(byte[] bArr);

    private TxOut(long j) throws SerializationException {
        this.rustObj = j;
        try {
            MobileCoinAPI$TxOut parseFrom = MobileCoinAPI$TxOut.parseFrom(toByteArray());
            this.protoBufTxOut = parseFrom;
            this.pubKey = RistrettoPublic.fromProtoBufObject(parseFrom.getPublicKey());
        } catch (InvalidProtocolBufferException e) {
            SerializationException serializationException = new SerializationException(e.getLocalizedMessage());
            Util.logException(TAG, serializationException);
            throw serializationException;
        }
    }

    private TxOut(MobileCoinAPI$TxOut mobileCoinAPI$TxOut) throws SerializationException {
        try {
            this.protoBufTxOut = mobileCoinAPI$TxOut;
            this.pubKey = RistrettoPublic.fromProtoBufObject(mobileCoinAPI$TxOut.getPublicKey());
            init_from_protobuf_bytes(mobileCoinAPI$TxOut.toByteString().toByteArray());
        } catch (Exception e) {
            SerializationException serializationException = new SerializationException(e.getLocalizedMessage());
            Util.logException(TAG, serializationException);
            throw serializationException;
        }
    }

    public static TxOut fromJNI(long j) throws SerializationException {
        return new TxOut(j);
    }

    public static TxOut fromProtoBufObject(MobileCoinAPI$TxOut mobileCoinAPI$TxOut) throws SerializationException {
        return new TxOut(mobileCoinAPI$TxOut);
    }

    public Amount getAmount() {
        try {
            return Amount.fromProtoBufObject(this.protoBufTxOut.getAmount());
        } catch (AmountDecoderException e) {
            IllegalStateException illegalStateException = new IllegalStateException(e);
            Logger.wtf(TAG, "BUG: unreachable code", illegalStateException, new Object[0]);
            throw illegalStateException;
        }
    }

    public byte[] toByteArray() throws SerializationException {
        try {
            return encode();
        } catch (Exception e) {
            throw new SerializationException(e.getLocalizedMessage());
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || TxOut.class != obj.getClass()) {
            return false;
        }
        TxOut txOut = (TxOut) obj;
        if (!this.protoBufTxOut.equals(txOut.protoBufTxOut) || !this.pubKey.equals(txOut.pubKey)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(this.protoBufTxOut, this.pubKey);
    }

    protected void finalize() throws Throwable {
        if (this.rustObj != 0) {
            finalize_jni();
        }
        super.finalize();
    }

    public byte[] computeKeyImage(AccountKey accountKey) {
        return compute_key_image(accountKey);
    }

    public RistrettoPublic getPubKey() {
        return this.pubKey;
    }
}
