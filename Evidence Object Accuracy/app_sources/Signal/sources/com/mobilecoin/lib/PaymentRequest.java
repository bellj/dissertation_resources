package com.mobilecoin.lib;

import com.mobilecoin.api.Printable$PaymentRequest;
import com.mobilecoin.lib.exceptions.SerializationException;
import com.mobilecoin.lib.log.Logger;
import java.util.Objects;

/* loaded from: classes3.dex */
public final class PaymentRequest {
    private static final String TAG;
    private final String memo;
    private final PublicAddress publicAddress;
    private final UnsignedLong value;

    public PaymentRequest(PublicAddress publicAddress, UnsignedLong unsignedLong, String str) {
        this.publicAddress = publicAddress;
        this.value = unsignedLong;
        this.memo = str;
    }

    public static PaymentRequest fromProtoBufObject(Printable$PaymentRequest printable$PaymentRequest) throws SerializationException {
        Logger.i(TAG, "Deserializing PaymentRequest from protobof");
        return new PaymentRequest(PublicAddress.fromProtoBufObject(printable$PaymentRequest.getPublicAddress()), UnsignedLong.fromLongBits(printable$PaymentRequest.getValue()), printable$PaymentRequest.getMemo());
    }

    public int hashCode() {
        return Objects.hash(this.publicAddress, this.value, this.memo);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || PaymentRequest.class != obj.getClass()) {
            return false;
        }
        PaymentRequest paymentRequest = (PaymentRequest) obj;
        if (!this.publicAddress.equals(paymentRequest.publicAddress) || !this.value.equals(paymentRequest.value) || !this.memo.equals(paymentRequest.memo)) {
            return false;
        }
        return true;
    }
}
