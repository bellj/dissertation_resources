package com.mobilecoin.lib;

import android.net.Uri;

/* loaded from: classes3.dex */
public final class ResponderId extends Native {
    private native void finalize_jni();

    private native void init_jni(String str);

    public ResponderId(Uri uri) {
        init_jni(String.format("%s:%s", uri.getHost(), Integer.valueOf(uri.getPort())));
    }

    protected void finalize() throws Throwable {
        if (this.rustObj != 0) {
            finalize_jni();
        }
        super.finalize();
    }
}
