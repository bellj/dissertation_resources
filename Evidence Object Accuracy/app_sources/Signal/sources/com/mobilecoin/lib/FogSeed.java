package com.mobilecoin.lib;

import com.mobilecoin.lib.exceptions.KexRngException;
import com.mobilecoin.lib.log.Logger;
import fog_view.View$RngRecord;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* loaded from: classes3.dex */
public final class FogSeed implements Serializable {
    private static final String TAG = FogSeed.class.getName();
    private final ClientKexRng kexRng;
    private byte[] nonce;
    private int rngVersion;
    private UnsignedLong startBlock;
    private ArrayList<OwnedTxOut> utxos = new ArrayList<>();

    public FogSeed(RistrettoPrivate ristrettoPrivate, View$RngRecord view$RngRecord) throws KexRngException {
        String str = TAG;
        Logger.i(str, "Initializing Fog Seed");
        this.nonce = view$RngRecord.getPubkey().getPubkey().toByteArray();
        this.rngVersion = view$RngRecord.getPubkey().getVersion();
        this.startBlock = UnsignedLong.fromLongBits(view$RngRecord.getStartBlock());
        if (this.rngVersion == 0) {
            this.kexRng = new ClientKexRng(ristrettoPrivate, this.nonce, this.rngVersion);
            return;
        }
        UnsupportedOperationException unsupportedOperationException = new UnsupportedOperationException("Unsupported rng version");
        Util.logException(str, unsupportedOperationException);
        throw unsupportedOperationException;
    }

    public void update(View$RngRecord view$RngRecord) {
        if (!Arrays.equals(this.nonce, view$RngRecord.getPubkey().getPubkey().toByteArray())) {
            IllegalStateException illegalStateException = new IllegalStateException("Update cannot change the nonce");
            Util.logException(TAG, illegalStateException);
            throw illegalStateException;
        } else if (this.rngVersion != view$RngRecord.getPubkey().getVersion()) {
            UnsupportedOperationException unsupportedOperationException = new UnsupportedOperationException("Updating rng version is not supported");
            Util.logException(TAG, unsupportedOperationException);
            throw unsupportedOperationException;
        } else if (this.startBlock.compareTo(UnsignedLong.fromLongBits(view$RngRecord.getStartBlock())) != 0) {
            IllegalStateException illegalStateException2 = new IllegalStateException("Start block should never change");
            Util.logException(TAG, illegalStateException2);
            throw illegalStateException2;
        }
    }

    public byte[][] getNextN(long j) throws KexRngException {
        Logger.i(TAG, "Getting the next N search keys", null, Long.valueOf(j));
        return this.kexRng.getNextN(j);
    }

    public byte[] getOutput() throws KexRngException {
        Logger.i(TAG, "Getting output");
        return this.kexRng.getOutput();
    }

    void advance() throws KexRngException {
        Logger.i(TAG, "Advancing seed");
        this.kexRng.advance();
    }

    public void addTXO(OwnedTxOut ownedTxOut) throws KexRngException {
        Logger.i(TAG, "Adding TXO");
        this.utxos.add(ownedTxOut);
        advance();
    }

    public UnsignedLong getStartBlock() {
        return this.startBlock;
    }

    public List<OwnedTxOut> getTxOuts() {
        return this.utxos;
    }
}
