package com.mobilecoin.lib;

/* loaded from: classes3.dex */
public final class DefaultFogQueryScalingStrategy implements FogQueryScalingStrategy {
    private int currentQuerySize = 10;

    @Override // com.mobilecoin.lib.FogQueryScalingStrategy
    public int nextQuerySize() {
        int i = this.currentQuerySize;
        this.currentQuerySize = Math.min(i * 3, 200);
        return i;
    }
}
