package com.mobilecoin.lib;

import com.mobilecoin.lib.exceptions.FogReportException;
import com.mobilecoin.lib.log.Logger;

/* loaded from: classes3.dex */
public final class VerificationSignature extends Native {
    private static final String TAG;

    private native void finalize_jni();

    private native void init_jni(byte[] bArr);

    public VerificationSignature(byte[] bArr) throws FogReportException {
        try {
            init_jni(bArr);
        } catch (Exception e) {
            throw new FogReportException("Unable to create verification signature", e);
        }
    }

    protected void finalize() throws Throwable {
        try {
            finalize_jni();
        } catch (Exception e) {
            Logger.e(TAG, "Failed to free verification signature data", e, new Object[0]);
        }
        super.finalize();
    }
}
