package com.mobilecoin.lib;

import com.mobilecoin.lib.exceptions.FogReportException;
import com.mobilecoin.lib.exceptions.TransactionBuilderException;
import com.mobilecoin.lib.log.Logger;
import java.math.BigInteger;
import java.util.List;
import java.util.Locale;

/* loaded from: classes3.dex */
public final class TransactionBuilder extends Native {
    private static final String TAG;

    private native void add_input(TxOut[] txOutArr, TxOutMembershipProof[] txOutMembershipProofArr, short s, RistrettoPrivate ristrettoPrivate, RistrettoPrivate ristrettoPrivate2);

    private native long add_output(BigInteger bigInteger, PublicAddress publicAddress, byte[] bArr);

    private native long build_tx();

    private native void finalize_jni();

    private native void init_jni(FogResolver fogResolver);

    private native void set_fee(long j);

    private native void set_tombstone_block(long j);

    public TransactionBuilder(FogResolver fogResolver) throws FogReportException {
        try {
            init_jni(fogResolver);
        } catch (Exception e) {
            throw new FogReportException("Unable to create TxBuilder", e);
        }
    }

    public void addInput(List<TxOut> list, List<TxOutMembershipProof> list2, short s, RistrettoPrivate ristrettoPrivate, RistrettoPrivate ristrettoPrivate2) throws TransactionBuilderException {
        Logger.i(TAG, "Adding transaction input");
        try {
            add_input((TxOut[]) list.toArray(new TxOut[0]), (TxOutMembershipProof[]) list2.toArray(new TxOutMembershipProof[0]), s, ristrettoPrivate, ristrettoPrivate2);
        } catch (Exception e) {
            Logger.e(TAG, "Unable to add transaction input", e, new Object[0]);
            throw new TransactionBuilderException("Unable to add transaction input", e);
        }
    }

    public TxOut addOutput(BigInteger bigInteger, PublicAddress publicAddress, byte[] bArr) throws TransactionBuilderException {
        Logger.i(TAG, "Adding transaction output");
        byte[] bArr2 = new byte[32];
        try {
            long add_output = add_output(bigInteger, publicAddress, bArr2);
            if (bArr != null) {
                if (bArr.length >= 32) {
                    System.arraycopy(bArr2, 0, bArr, 0, 32);
                } else {
                    throw new IllegalArgumentException("ConfirmationNumber buffer is too small");
                }
            }
            return TxOut.fromJNI(add_output);
        } catch (Exception e) {
            Logger.e(TAG, "Unable to add transaction output", e, new Object[0]);
            throw new TransactionBuilderException(e.getLocalizedMessage(), e);
        }
    }

    public void setTombstoneBlockIndex(UnsignedLong unsignedLong) throws TransactionBuilderException {
        Logger.i(TAG, String.format(Locale.US, "Set transaction tombstone %s", unsignedLong.toString()));
        try {
            set_tombstone_block(unsignedLong.longValue());
        } catch (Exception e) {
            Logger.e(TAG, "Unable to set tombstone block", e, new Object[0]);
            throw new TransactionBuilderException(e.getLocalizedMessage(), e);
        }
    }

    public void setFee(long j) throws TransactionBuilderException {
        Logger.i(TAG, String.format(Locale.US, "Set transaction fee %d", Long.valueOf(j)));
        try {
            set_fee(j);
        } catch (Exception e) {
            Logger.e(TAG, "Unable to set transaction fee", e, new Object[0]);
            throw new TransactionBuilderException(e.getLocalizedMessage(), e);
        }
    }

    public Transaction build() throws TransactionBuilderException {
        Logger.i(TAG, "Building the native transaction");
        try {
            return Transaction.fromJNI(build_tx());
        } catch (Exception e) {
            Logger.e(TAG, "Unable to set transaction fee", e, new Object[0]);
            throw new TransactionBuilderException("Unable to build transaction from supplied arguments");
        }
    }

    protected void finalize() throws Throwable {
        if (this.rustObj != 0) {
            finalize_jni();
        }
        super.finalize();
    }
}
