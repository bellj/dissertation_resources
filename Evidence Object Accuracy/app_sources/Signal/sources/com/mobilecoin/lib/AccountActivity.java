package com.mobilecoin.lib;

import com.mobilecoin.lib.log.Logger;
import java.util.Set;

/* loaded from: classes3.dex */
public final class AccountActivity {
    private static final String TAG;
    private final UnsignedLong blockCount;
    private final Set<OwnedTxOut> txOuts;

    public AccountActivity(Set<OwnedTxOut> set, UnsignedLong unsignedLong) {
        this.txOuts = set;
        this.blockCount = unsignedLong;
        Logger.i(TAG, "Created AccountActivity", null, "txOuts size:", Integer.valueOf(set.size()), "blockCount:", unsignedLong);
    }

    public Set<OwnedTxOut> getAllTxOuts() {
        Logger.i(TAG, "GetAllTxOuts", null, "txOuts count:", Integer.valueOf(this.txOuts.size()));
        return this.txOuts;
    }
}
