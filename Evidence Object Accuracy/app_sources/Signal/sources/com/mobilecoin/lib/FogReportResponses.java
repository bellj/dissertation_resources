package com.mobilecoin.lib;

import android.net.Uri;
import com.mobilecoin.lib.exceptions.FogReportException;
import com.mobilecoin.lib.log.Logger;

/* loaded from: classes3.dex */
public final class FogReportResponses extends Native {
    private static final String TAG;

    private native void add_response(String str, ReportResponse reportResponse);

    private native void finalize_jni();

    private native void init_jni();

    public FogReportResponses() throws FogReportException {
        try {
            init_jni();
        } catch (Exception e) {
            FogReportException fogReportException = new FogReportException("Unable to create fog report responses container", e);
            Util.logException(TAG, fogReportException);
            throw fogReportException;
        }
    }

    public synchronized void addResponse(Uri uri, ReportResponse reportResponse) throws FogReportException {
        try {
            add_response(uri.toString(), reportResponse);
        } catch (Exception e) {
            FogReportException fogReportException = new FogReportException("Unable to add response", e);
            Util.logException(TAG, fogReportException);
            throw fogReportException;
        }
    }

    protected void finalize() throws Throwable {
        try {
            finalize_jni();
        } catch (Exception e) {
            Logger.e(TAG, "Failed to free fog report data", e, new Object[0]);
        }
        super.finalize();
    }
}
