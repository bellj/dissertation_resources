package com.mobilecoin.lib;

/* loaded from: classes3.dex */
public interface FogQueryScalingStrategy {
    int nextQuerySize();
}
