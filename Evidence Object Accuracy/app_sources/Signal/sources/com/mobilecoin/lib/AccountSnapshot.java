package com.mobilecoin.lib;

import com.mobilecoin.lib.Receipt;
import com.mobilecoin.lib.Transaction;
import com.mobilecoin.lib.exceptions.AmountDecoderException;
import com.mobilecoin.lib.exceptions.InsufficientFundsException;
import com.mobilecoin.lib.exceptions.InvalidReceiptException;
import com.mobilecoin.lib.exceptions.NetworkException;
import com.mobilecoin.lib.log.Logger;
import fog_ledger.Ledger$TxOutResult;
import fog_ledger.Ledger$TxOutResultCode;
import j$.util.Collection$EL;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/* loaded from: classes3.dex */
public final class AccountSnapshot {
    private static final String TAG;
    private final UnsignedLong blockIndex;
    private final MobileCoinClient mobileCoinClient;
    private final Set<OwnedTxOut> txOuts;

    public AccountSnapshot(MobileCoinClient mobileCoinClient, Set<OwnedTxOut> set, UnsignedLong unsignedLong) {
        this.txOuts = set;
        this.blockIndex = unsignedLong;
        this.mobileCoinClient = mobileCoinClient;
    }

    public UnsignedLong getBlockIndex() {
        return this.blockIndex;
    }

    public AccountActivity getAccountActivity() {
        return new AccountActivity(this.txOuts, this.blockIndex.add(UnsignedLong.ONE));
    }

    public Receipt.Status getReceiptStatus(Receipt receipt) throws InvalidReceiptException {
        Logger.i(TAG, "Checking receipt status");
        RistrettoPublic publicKey = receipt.getPublicKey();
        for (OwnedTxOut ownedTxOut : this.txOuts) {
            if (ownedTxOut.getPublicKey().equals(publicKey)) {
                try {
                    if (ownedTxOut.getValue().equals(receipt.getAmount(this.mobileCoinClient.getAccountKey()))) {
                        return Receipt.Status.RECEIVED.atBlock(ownedTxOut.getReceivedBlockIndex());
                    }
                    InvalidReceiptException invalidReceiptException = new InvalidReceiptException("Receipt amount mismatch");
                    Util.logException(TAG, invalidReceiptException);
                    throw invalidReceiptException;
                } catch (AmountDecoderException e) {
                    InvalidReceiptException invalidReceiptException2 = new InvalidReceiptException("Malformed Receipt", e);
                    Util.logException(TAG, invalidReceiptException2);
                    throw invalidReceiptException2;
                }
            }
        }
        return this.blockIndex.compareTo(receipt.getTombstoneBlockIndex()) >= 0 ? Receipt.Status.FAILED.atBlock(this.blockIndex) : Receipt.Status.UNKNOWN.atBlock(this.blockIndex);
    }

    public Transaction.Status getTransactionStatus(Transaction transaction) throws NetworkException {
        Logger.i(TAG, "Checking transaction status");
        HashMap hashMap = new HashMap();
        for (KeyImage keyImage : transaction.getKeyImages()) {
            hashMap.put(Integer.valueOf(keyImage.hashCode()), Boolean.TRUE);
        }
        for (OwnedTxOut ownedTxOut : this.txOuts) {
            if (ownedTxOut.isSpent(this.blockIndex)) {
                hashMap.remove(Integer.valueOf(ownedTxOut.getKeyImage().hashCode()));
            }
        }
        if (hashMap.isEmpty()) {
            List<Ledger$TxOutResult> resultsList = this.mobileCoinClient.untrustedClient.fetchTxOuts(transaction.getOutputPublicKeys()).getResultsList();
            boolean z = true;
            UnsignedLong unsignedLong = UnsignedLong.ZERO;
            Iterator<Ledger$TxOutResult> it = resultsList.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Ledger$TxOutResult next = it.next();
                if (next.getResultCode() != Ledger$TxOutResultCode.Found) {
                    z = false;
                    break;
                }
                UnsignedLong fromLongBits = UnsignedLong.fromLongBits(next.getBlockIndex());
                if (unsignedLong.compareTo(fromLongBits) < 0) {
                    unsignedLong = fromLongBits;
                }
            }
            if (z && unsignedLong.compareTo(this.blockIndex) <= 0) {
                return Transaction.Status.ACCEPTED.atBlock(unsignedLong);
            }
        }
        if (this.blockIndex.compareTo(UnsignedLong.fromLongBits(transaction.getTombstoneBlockIndex())) >= 0) {
            return Transaction.Status.FAILED.atBlock(this.blockIndex);
        }
        return Transaction.Status.UNKNOWN.atBlock(this.blockIndex);
    }

    public BigInteger getTransferableAmount(BigInteger bigInteger) {
        Logger.i(TAG, "Calculating transferable amount");
        try {
            return UTXOSelector.getTransferableAmount((HashSet) Collection$EL.stream(this.txOuts).filter(new Predicate() { // from class: com.mobilecoin.lib.AccountSnapshot$$ExternalSyntheticLambda0
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return AccountSnapshot.$r8$lambda$oIdFaLAgb6uZa5COxkPRwXigi0U(AccountSnapshot.this, (OwnedTxOut) obj);
                }
            }).collect(Collectors.toCollection(new AccountSnapshot$$ExternalSyntheticLambda1())), bigInteger, MobileCoinClient.INPUT_FEE, MobileCoinClient.OUTPUT_FEE);
        } catch (InsufficientFundsException unused) {
            return BigInteger.ZERO;
        }
    }

    public /* synthetic */ boolean lambda$getTransferableAmount$0(OwnedTxOut ownedTxOut) {
        return !ownedTxOut.isSpent(this.blockIndex);
    }
}
