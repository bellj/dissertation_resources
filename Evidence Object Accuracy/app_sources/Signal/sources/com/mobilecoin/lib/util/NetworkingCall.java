package com.mobilecoin.lib.util;

import com.mobilecoin.lib.exceptions.NetworkException;
import j$.util.DesugarArrays;
import j$.util.function.IntPredicate;
import java.util.concurrent.Callable;

/* loaded from: classes3.dex */
public class NetworkingCall<T> {
    private final Callable<T> callable;
    private final RetryPolicy retryPolicy;

    /* loaded from: classes3.dex */
    public static class RetryPolicy {
        int retryCount;
        int[] statusCodes;
    }

    public NetworkingCall(RetryPolicy retryPolicy, Callable<T> callable) {
        this.retryPolicy = retryPolicy;
        this.callable = callable;
    }

    public NetworkingCall(Callable<T> callable) {
        this(new DefaultRetryPolicy(), callable);
    }

    public T run() throws Exception {
        int i = 0;
        do {
            try {
                return this.callable.call();
            } catch (NetworkException e) {
                if (DesugarArrays.stream(this.retryPolicy.statusCodes).anyMatch(new IntPredicate() { // from class: com.mobilecoin.lib.util.NetworkingCall$$ExternalSyntheticLambda0
                    @Override // j$.util.function.IntPredicate
                    public /* synthetic */ IntPredicate and(IntPredicate intPredicate) {
                        return IntPredicate.CC.$default$and(this, intPredicate);
                    }

                    @Override // j$.util.function.IntPredicate
                    public /* synthetic */ IntPredicate negate() {
                        return IntPredicate.CC.$default$negate(this);
                    }

                    @Override // j$.util.function.IntPredicate
                    public /* synthetic */ IntPredicate or(IntPredicate intPredicate) {
                        return IntPredicate.CC.$default$or(this, intPredicate);
                    }

                    @Override // j$.util.function.IntPredicate
                    public final boolean test(int i2) {
                        return NetworkingCall.lambda$run$0(NetworkException.this, i2);
                    }
                })) {
                    i++;
                    if (i == this.retryPolicy.retryCount) {
                        throw e;
                    }
                } else {
                    throw e;
                }
            }
        } while (i == this.retryPolicy.retryCount);
        throw e;
    }

    public static /* synthetic */ boolean lambda$run$0(NetworkException networkException, int i) {
        return i == networkException.statusCode;
    }

    /* loaded from: classes3.dex */
    public static class DefaultRetryPolicy extends RetryPolicy {
        DefaultRetryPolicy() {
            this.statusCodes = new int[]{403, 500};
            this.retryCount = 1;
        }
    }
}
