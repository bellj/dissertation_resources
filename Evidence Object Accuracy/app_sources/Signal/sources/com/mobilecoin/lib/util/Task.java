package com.mobilecoin.lib.util;

import com.mobilecoin.lib.util.Result;
import java.lang.Exception;
import java.util.concurrent.Callable;

/* loaded from: classes3.dex */
public abstract class Task<V, E extends Exception> implements Callable<Result<V, E>> {
    public abstract V execute() throws Exception;

    @Override // java.util.concurrent.Callable
    public Result<V, E> call() {
        try {
            return Result.CC.ok(execute());
        } catch (Exception e) {
            return Result.CC.err(e);
        }
    }
}
