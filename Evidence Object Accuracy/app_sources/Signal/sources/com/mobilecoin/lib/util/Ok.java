package com.mobilecoin.lib.util;

/* loaded from: classes3.dex */
public class Ok<V, E> implements Result<V, E> {
    private final V value;

    @Override // com.mobilecoin.lib.util.Result
    public E getError() {
        return null;
    }

    @Override // com.mobilecoin.lib.util.Result
    public boolean isErr() {
        return false;
    }

    @Override // com.mobilecoin.lib.util.Result
    public boolean isOk() {
        return true;
    }

    public Ok(V v) {
        this.value = v;
    }

    @Override // com.mobilecoin.lib.util.Result
    public V getValue() {
        return this.value;
    }

    public String toString() {
        return String.format("Ok(%s)", this.value);
    }
}
