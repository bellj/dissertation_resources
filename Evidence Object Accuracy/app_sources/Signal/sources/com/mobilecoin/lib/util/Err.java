package com.mobilecoin.lib.util;

/* loaded from: classes3.dex */
public final class Err<V, E> implements Result<V, E> {
    private final E error;

    @Override // com.mobilecoin.lib.util.Result
    public V getValue() {
        return null;
    }

    @Override // com.mobilecoin.lib.util.Result
    public boolean isErr() {
        return true;
    }

    @Override // com.mobilecoin.lib.util.Result
    public boolean isOk() {
        return false;
    }

    public Err(E e) {
        this.error = e;
    }

    @Override // com.mobilecoin.lib.util.Result
    public E getError() {
        return this.error;
    }

    public String toString() {
        return String.format("Err(%s)", this.error);
    }
}
