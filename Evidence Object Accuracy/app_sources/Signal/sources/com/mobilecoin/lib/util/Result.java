package com.mobilecoin.lib.util;

/* loaded from: classes3.dex */
public interface Result<V, E> {
    E getError();

    V getValue();

    boolean isErr();

    boolean isOk();

    /* renamed from: com.mobilecoin.lib.util.Result$-CC */
    /* loaded from: classes3.dex */
    public final /* synthetic */ class CC<V, E> {
        public static <V, E> Result<V, E> ok(V v) {
            return new Ok(v);
        }

        public static <V, E> Result<V, E> err(E e) {
            return new Err(e);
        }
    }
}
