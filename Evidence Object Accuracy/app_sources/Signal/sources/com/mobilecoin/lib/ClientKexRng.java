package com.mobilecoin.lib;

import com.mobilecoin.lib.exceptions.KexRngException;
import com.mobilecoin.lib.log.Logger;

/* access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public final class ClientKexRng extends Native {
    private static final String TAG;

    private native void finalize_jni();

    private native byte[][] get_next_n(long j);

    private native byte[] get_output();

    private native void init_jni(RistrettoPrivate ristrettoPrivate, byte[] bArr, int i);

    private native void rng_advance();

    public ClientKexRng(RistrettoPrivate ristrettoPrivate, byte[] bArr, int i) throws KexRngException {
        Logger.i(TAG, "Initializing ClientKexRng");
        try {
            init_jni(ristrettoPrivate, bArr, i);
        } catch (Exception e) {
            KexRngException kexRngException = new KexRngException("Unable to create a KexRng with the provided arguments", e);
            Util.logException(TAG, kexRngException);
            throw kexRngException;
        }
    }

    public void advance() throws KexRngException {
        Logger.i(TAG, "Advancing ClientKexRng");
        try {
            rng_advance();
        } catch (Exception e) {
            KexRngException kexRngException = new KexRngException("Unable to advance KexRng", e);
            Util.logException(TAG, kexRngException);
            throw kexRngException;
        }
    }

    public byte[] getOutput() throws KexRngException {
        Logger.i(TAG, "Getting output");
        try {
            return get_output();
        } catch (Exception e) {
            KexRngException kexRngException = new KexRngException("Unable to get a KexRng output", e);
            Util.logException(TAG, kexRngException);
            throw kexRngException;
        }
    }

    public byte[][] getNextN(long j) throws KexRngException {
        Logger.i(TAG, "Getting next N KexRngs");
        try {
            return get_next_n(j);
        } catch (Exception e) {
            KexRngException kexRngException = new KexRngException("Unable to get the next N KexRng outputs", e);
            Util.logException(TAG, kexRngException);
            throw kexRngException;
        }
    }

    protected void finalize() throws Throwable {
        if (this.rustObj != 0) {
            finalize_jni();
        }
        super.finalize();
    }
}
