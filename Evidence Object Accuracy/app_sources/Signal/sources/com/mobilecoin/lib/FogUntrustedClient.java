package com.mobilecoin.lib;

import com.mobilecoin.lib.ClientConfig;
import com.mobilecoin.lib.exceptions.AttestationException;
import com.mobilecoin.lib.exceptions.NetworkException;
import com.mobilecoin.lib.log.Logger;
import com.mobilecoin.lib.uri.FogUri;
import com.mobilecoin.lib.util.NetworkingCall;
import fog_ledger.FogUntrustedTxOutApiGrpc;
import fog_ledger.Ledger$TxOutRequest;
import fog_ledger.Ledger$TxOutResponse;
import io.grpc.StatusRuntimeException;
import java.util.Set;
import java.util.concurrent.Callable;

/* access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public final class FogUntrustedClient extends AnyClient {
    private static final String TAG = AttestedLedgerClient.class.getName();

    public FogUntrustedClient(FogUri fogUri, ClientConfig.Service service) {
        super(fogUri.getUri(), service);
        Logger.i(TAG, "Created new FogUntrustedClient", null, "uri:", fogUri, "verifier:", service);
    }

    public Ledger$TxOutResponse fetchTxOuts(Set<RistrettoPublic> set) throws NetworkException {
        Logger.i(TAG, "Fetching TxOuts via untrusted fog API", null, "public keys number:", Integer.valueOf(set.size()));
        try {
            FogUntrustedTxOutApiGrpc.FogUntrustedTxOutApiBlockingStub fogUntrustedTxOutApiBlockingStub = getAPIManager().getFogUntrustedTxOutApiBlockingStub(getManagedChannel());
            Ledger$TxOutRequest.Builder newBuilder = Ledger$TxOutRequest.newBuilder();
            for (RistrettoPublic ristrettoPublic : set) {
                newBuilder.addTxOutPubkeys(ristrettoPublic.toProtoBufObject());
            }
            try {
                return (Ledger$TxOutResponse) new NetworkingCall(new Callable(newBuilder) { // from class: com.mobilecoin.lib.FogUntrustedClient$$ExternalSyntheticLambda0
                    public final /* synthetic */ Ledger$TxOutRequest.Builder f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // java.util.concurrent.Callable
                    public final Object call() {
                        return FogUntrustedClient.lambda$fetchTxOuts$0(FogUntrustedTxOutApiGrpc.FogUntrustedTxOutApiBlockingStub.this, this.f$1);
                    }
                }).run();
            } catch (NetworkException e) {
                throw e;
            } catch (RuntimeException e2) {
                throw e2;
            } catch (Exception unused) {
                throw new IllegalStateException("BUG: unreachable code");
            }
        } catch (AttestationException unused2) {
            throw new IllegalStateException("BUG: Untrusted service cannot throw attestation exception");
        }
    }

    public static /* synthetic */ Ledger$TxOutResponse lambda$fetchTxOuts$0(FogUntrustedTxOutApiGrpc.FogUntrustedTxOutApiBlockingStub fogUntrustedTxOutApiBlockingStub, Ledger$TxOutRequest.Builder builder) throws Exception {
        try {
            return fogUntrustedTxOutApiBlockingStub.getTxOuts(builder.build());
        } catch (StatusRuntimeException e) {
            Logger.w(TAG, "Unable to fetch TxOuts from the untrusted service", e, new Object[0]);
            throw new NetworkException(e);
        }
    }
}
