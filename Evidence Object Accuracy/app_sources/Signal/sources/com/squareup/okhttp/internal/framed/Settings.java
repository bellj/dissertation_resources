package com.squareup.okhttp.internal.framed;

import java.util.Arrays;

/* loaded from: classes3.dex */
public final class Settings {
    static final int CLIENT_CERTIFICATE_VECTOR_SIZE;
    static final int COUNT;
    static final int CURRENT_CWND;
    static final int DEFAULT_INITIAL_WINDOW_SIZE;
    static final int DOWNLOAD_BANDWIDTH;
    static final int DOWNLOAD_RETRANS_RATE;
    static final int ENABLE_PUSH;
    static final int FLAG_CLEAR_PREVIOUSLY_PERSISTED_SETTINGS;
    static final int FLOW_CONTROL_OPTIONS;
    static final int FLOW_CONTROL_OPTIONS_DISABLED;
    static final int HEADER_TABLE_SIZE;
    static final int INITIAL_WINDOW_SIZE;
    static final int MAX_CONCURRENT_STREAMS;
    static final int MAX_FRAME_SIZE;
    static final int MAX_HEADER_LIST_SIZE;
    static final int PERSISTED;
    static final int PERSIST_VALUE;
    static final int ROUND_TRIP_TIME;
    static final int UPLOAD_BANDWIDTH;
    private int persistValue;
    private int persisted;
    private int set;
    private final int[] values = new int[10];

    public void clear() {
        this.persisted = 0;
        this.persistValue = 0;
        this.set = 0;
        Arrays.fill(this.values, 0);
    }

    public Settings set(int i, int i2, int i3) {
        int[] iArr = this.values;
        if (i >= iArr.length) {
            return this;
        }
        int i4 = 1 << i;
        this.set |= i4;
        if ((i2 & 1) != 0) {
            this.persistValue |= i4;
        } else {
            this.persistValue &= i4 ^ -1;
        }
        if ((i2 & 2) != 0) {
            this.persisted |= i4;
        } else {
            this.persisted &= i4 ^ -1;
        }
        iArr[i] = i3;
        return this;
    }

    public boolean isSet(int i) {
        return ((1 << i) & this.set) != 0;
    }

    public int get(int i) {
        return this.values[i];
    }

    public int flags(int i) {
        int i2 = isPersisted(i) ? 2 : 0;
        return persistValue(i) ? i2 | 1 : i2;
    }

    public int size() {
        return Integer.bitCount(this.set);
    }

    int getUploadBandwidth(int i) {
        return (this.set & 2) != 0 ? this.values[1] : i;
    }

    public int getHeaderTableSize() {
        if ((this.set & 2) != 0) {
            return this.values[1];
        }
        return -1;
    }

    int getDownloadBandwidth(int i) {
        return (this.set & 4) != 0 ? this.values[2] : i;
    }

    boolean getEnablePush(boolean z) {
        return ((this.set & 4) != 0 ? this.values[2] : z ? 1 : 0) == 1;
    }

    int getRoundTripTime(int i) {
        return (this.set & 8) != 0 ? this.values[3] : i;
    }

    public int getMaxConcurrentStreams(int i) {
        return (this.set & 16) != 0 ? this.values[4] : i;
    }

    int getCurrentCwnd(int i) {
        return (this.set & 32) != 0 ? this.values[5] : i;
    }

    public int getMaxFrameSize(int i) {
        return (this.set & 32) != 0 ? this.values[5] : i;
    }

    int getDownloadRetransRate(int i) {
        return (this.set & 64) != 0 ? this.values[6] : i;
    }

    int getMaxHeaderListSize(int i) {
        return (this.set & 64) != 0 ? this.values[6] : i;
    }

    public int getInitialWindowSize(int i) {
        return (this.set & 128) != 0 ? this.values[7] : i;
    }

    int getClientCertificateVectorSize(int i) {
        return (this.set & 256) != 0 ? this.values[8] : i;
    }

    boolean isFlowControlDisabled() {
        return (((this.set & 1024) != 0 ? this.values[10] : 0) & 1) != 0;
    }

    boolean persistValue(int i) {
        return ((1 << i) & this.persistValue) != 0;
    }

    boolean isPersisted(int i) {
        return ((1 << i) & this.persisted) != 0;
    }

    public void merge(Settings settings) {
        for (int i = 0; i < 10; i++) {
            if (settings.isSet(i)) {
                set(i, settings.flags(i), settings.get(i));
            }
        }
    }
}
