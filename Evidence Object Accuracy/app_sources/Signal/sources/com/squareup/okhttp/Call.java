package com.squareup.okhttp;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.internal.NamedRunnable;
import com.squareup.okhttp.internal.http.HttpEngine;
import java.io.IOException;

/* loaded from: classes3.dex */
public class Call {
    volatile boolean canceled;
    private final OkHttpClient client;
    HttpEngine engine;
    private boolean executed;
    Request originalRequest;

    public Call(OkHttpClient okHttpClient, Request request) {
        this.client = okHttpClient.copyWithDefaults();
        this.originalRequest = request;
    }

    public Response execute() throws IOException {
        synchronized (this) {
            if (!this.executed) {
                this.executed = true;
            } else {
                throw new IllegalStateException("Already Executed");
            }
        }
        try {
            this.client.getDispatcher().executed(this);
            Response responseWithInterceptorChain = getResponseWithInterceptorChain(false);
            if (responseWithInterceptorChain != null) {
                return responseWithInterceptorChain;
            }
            throw new IOException("Canceled");
        } finally {
            this.client.getDispatcher().finished(this);
        }
    }

    public Object tag() {
        return this.originalRequest.tag();
    }

    public void enqueue(Callback callback) {
        enqueue(callback, false);
    }

    public void enqueue(Callback callback, boolean z) {
        synchronized (this) {
            if (!this.executed) {
                this.executed = true;
            } else {
                throw new IllegalStateException("Already Executed");
            }
        }
        this.client.getDispatcher().enqueue(new AsyncCall(callback, z));
    }

    public void cancel() {
        this.canceled = true;
        HttpEngine httpEngine = this.engine;
        if (httpEngine != null) {
            httpEngine.cancel();
        }
    }

    public synchronized boolean isExecuted() {
        return this.executed;
    }

    public boolean isCanceled() {
        return this.canceled;
    }

    /* loaded from: classes3.dex */
    public final class AsyncCall extends NamedRunnable {
        private final boolean forWebSocket;
        private final Callback responseCallback;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        private AsyncCall(Callback callback, boolean z) {
            super("OkHttp %s", r3.originalRequest.urlString());
            Call.this = r3;
            this.responseCallback = callback;
            this.forWebSocket = z;
        }

        public String host() {
            return Call.this.originalRequest.httpUrl().host();
        }

        Request request() {
            return Call.this.originalRequest;
        }

        public Object tag() {
            return Call.this.originalRequest.tag();
        }

        public void cancel() {
            Call.this.cancel();
        }

        public Call get() {
            return Call.this;
        }

        /* JADX DEBUG: Failed to insert an additional move for type inference into block B:25:? */
        /* JADX DEBUG: Failed to insert an additional move for type inference into block B:8:0x0034 */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v0 */
        /* JADX WARN: Type inference failed for: r0v1 */
        /* JADX WARN: Type inference failed for: r0v8 */
        /* JADX WARN: Type inference failed for: r0v10 */
        /* JADX WARN: Type inference failed for: r0v13, types: [com.squareup.okhttp.Dispatcher] */
        /* JADX WARNING: Removed duplicated region for block: B:13:0x003d A[Catch: all -> 0x0036, TRY_ENTER, TryCatch #1 {all -> 0x0036, blocks: (B:3:0x0002, B:5:0x0010, B:6:0x0021, B:13:0x003d, B:14:0x005c, B:16:0x0062, B:17:0x0065, B:18:0x0069), top: B:22:0x0002 }] */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x005c A[Catch: all -> 0x0036, TryCatch #1 {all -> 0x0036, blocks: (B:3:0x0002, B:5:0x0010, B:6:0x0021, B:13:0x003d, B:14:0x005c, B:16:0x0062, B:17:0x0065, B:18:0x0069), top: B:22:0x0002 }] */
        /* JADX WARNING: Unknown variable types count: 1 */
        @Override // com.squareup.okhttp.internal.NamedRunnable
        /* Code decompiled incorrectly, please refer to instructions dump. */
        protected void execute() {
            /*
                r5 = this;
                r0 = 1
                r1 = 0
                com.squareup.okhttp.Call r2 = com.squareup.okhttp.Call.this     // Catch: IOException -> 0x0038, all -> 0x0036
                boolean r3 = r5.forWebSocket     // Catch: IOException -> 0x0038, all -> 0x0036
                com.squareup.okhttp.Response r2 = com.squareup.okhttp.Call.access$100(r2, r3)     // Catch: IOException -> 0x0038, all -> 0x0036
                com.squareup.okhttp.Call r3 = com.squareup.okhttp.Call.this     // Catch: IOException -> 0x0038, all -> 0x0036
                boolean r1 = r3.canceled     // Catch: IOException -> 0x0038, all -> 0x0036
                if (r1 == 0) goto L_0x0021
                com.squareup.okhttp.Callback r1 = r5.responseCallback     // Catch: IOException -> 0x0034, all -> 0x0036
                com.squareup.okhttp.Call r2 = com.squareup.okhttp.Call.this     // Catch: IOException -> 0x0034, all -> 0x0036
                com.squareup.okhttp.Request r2 = r2.originalRequest     // Catch: IOException -> 0x0034, all -> 0x0036
                java.io.IOException r3 = new java.io.IOException     // Catch: IOException -> 0x0034, all -> 0x0036
                java.lang.String r4 = "Canceled"
                r3.<init>(r4)     // Catch: IOException -> 0x0034, all -> 0x0036
                r1.onFailure(r2, r3)     // Catch: IOException -> 0x0034, all -> 0x0036
                goto L_0x0026
            L_0x0021:
                com.squareup.okhttp.Callback r1 = r5.responseCallback     // Catch: IOException -> 0x0034, all -> 0x0036
                r1.onResponse(r2)     // Catch: IOException -> 0x0034, all -> 0x0036
            L_0x0026:
                com.squareup.okhttp.Call r0 = com.squareup.okhttp.Call.this
                com.squareup.okhttp.OkHttpClient r0 = com.squareup.okhttp.Call.access$300(r0)
                com.squareup.okhttp.Dispatcher r0 = r0.getDispatcher()
                r0.finished(r5)
                goto L_0x006f
            L_0x0034:
                r1 = move-exception
                goto L_0x003b
            L_0x0036:
                r0 = move-exception
                goto L_0x0070
            L_0x0038:
                r0 = move-exception
                r1 = r0
                r0 = 0
            L_0x003b:
                if (r0 == 0) goto L_0x005c
                java.util.logging.Logger r0 = com.squareup.okhttp.internal.Internal.logger     // Catch: all -> 0x0036
                java.util.logging.Level r2 = java.util.logging.Level.INFO     // Catch: all -> 0x0036
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch: all -> 0x0036
                r3.<init>()     // Catch: all -> 0x0036
                java.lang.String r4 = "Callback failure for "
                r3.append(r4)     // Catch: all -> 0x0036
                com.squareup.okhttp.Call r4 = com.squareup.okhttp.Call.this     // Catch: all -> 0x0036
                java.lang.String r4 = com.squareup.okhttp.Call.access$200(r4)     // Catch: all -> 0x0036
                r3.append(r4)     // Catch: all -> 0x0036
                java.lang.String r3 = r3.toString()     // Catch: all -> 0x0036
                r0.log(r2, r3, r1)     // Catch: all -> 0x0036
                goto L_0x0026
            L_0x005c:
                com.squareup.okhttp.Call r0 = com.squareup.okhttp.Call.this     // Catch: all -> 0x0036
                com.squareup.okhttp.internal.http.HttpEngine r2 = r0.engine     // Catch: all -> 0x0036
                if (r2 != 0) goto L_0x0065
                com.squareup.okhttp.Request r0 = r0.originalRequest     // Catch: all -> 0x0036
                goto L_0x0069
            L_0x0065:
                com.squareup.okhttp.Request r0 = r2.getRequest()     // Catch: all -> 0x0036
            L_0x0069:
                com.squareup.okhttp.Callback r2 = r5.responseCallback     // Catch: all -> 0x0036
                r2.onFailure(r0, r1)     // Catch: all -> 0x0036
                goto L_0x0026
            L_0x006f:
                return
            L_0x0070:
                com.squareup.okhttp.Call r1 = com.squareup.okhttp.Call.this
                com.squareup.okhttp.OkHttpClient r1 = com.squareup.okhttp.Call.access$300(r1)
                com.squareup.okhttp.Dispatcher r1 = r1.getDispatcher()
                r1.finished(r5)
                goto L_0x007f
            L_0x007e:
                throw r0
            L_0x007f:
                goto L_0x007e
            */
            throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.Call.AsyncCall.execute():void");
        }
    }

    public String toLoggableString() {
        String str = this.canceled ? "canceled call" : "call";
        HttpUrl resolve = this.originalRequest.httpUrl().resolve("/...");
        return str + " to " + resolve;
    }

    public Response getResponseWithInterceptorChain(boolean z) throws IOException {
        return new ApplicationInterceptorChain(0, this.originalRequest, z).proceed(this.originalRequest);
    }

    /* loaded from: classes3.dex */
    public class ApplicationInterceptorChain implements Interceptor.Chain {
        private final boolean forWebSocket;
        private final int index;
        private final Request request;

        @Override // com.squareup.okhttp.Interceptor.Chain
        public Connection connection() {
            return null;
        }

        ApplicationInterceptorChain(int i, Request request, boolean z) {
            Call.this = r1;
            this.index = i;
            this.request = request;
            this.forWebSocket = z;
        }

        @Override // com.squareup.okhttp.Interceptor.Chain
        public Request request() {
            return this.request;
        }

        @Override // com.squareup.okhttp.Interceptor.Chain
        public Response proceed(Request request) throws IOException {
            if (this.index >= Call.this.client.interceptors().size()) {
                return Call.this.getResponse(request, this.forWebSocket);
            }
            ApplicationInterceptorChain applicationInterceptorChain = new ApplicationInterceptorChain(this.index + 1, request, this.forWebSocket);
            Interceptor interceptor = Call.this.client.interceptors().get(this.index);
            Response intercept = interceptor.intercept(applicationInterceptorChain);
            if (intercept != null) {
                return intercept;
            }
            throw new NullPointerException("application interceptor " + interceptor + " returned null");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:49:0x00eb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    com.squareup.okhttp.Response getResponse(com.squareup.okhttp.Request r13, boolean r14) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 260
        */
        throw new UnsupportedOperationException("Method not decompiled: com.squareup.okhttp.Call.getResponse(com.squareup.okhttp.Request, boolean):com.squareup.okhttp.Response");
    }
}
