package com.bumptech.glide.load.engine.executor;

/* loaded from: classes.dex */
public final class RuntimeCompat {
    public static int availableProcessors() {
        return Runtime.getRuntime().availableProcessors();
    }
}
