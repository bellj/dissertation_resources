package com.bumptech.glide.load.engine;

/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public final class CallbackException extends RuntimeException {
    public CallbackException(Throwable th) {
        super("Unexpected exception thrown by non-Glide code", th);
    }
}
