package com.bumptech.glide.gifdecoder;

/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public class GifFrame {
    int bufferFrameStart;
    int delay;
    int dispose;
    int ih;
    boolean interlace;
    int iw;
    int ix;
    int iy;
    int[] lct;
    int transIndex;
    boolean transparency;
}
