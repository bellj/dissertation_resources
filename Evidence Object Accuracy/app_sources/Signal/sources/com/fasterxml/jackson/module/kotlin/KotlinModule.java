package com.fasterxml.jackson.module.kotlin;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.module.SimpleModule;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.CharRange;
import kotlin.ranges.ClosedRange;
import kotlin.ranges.IntRange;
import kotlin.ranges.LongRange;
import kotlin.reflect.KClass;

/* compiled from: KotlinModule.kt */
@Metadata(bv = {}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 $2\u00020\u0001:\u0002%$BC\u0012\b\b\u0002\u0010\u000b\u001a\u00020\n\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u000f\u0012\b\b\u0002\u0010\u0014\u001a\u00020\u000f\u0012\b\b\u0002\u0010\u0016\u001a\u00020\u000f\u0012\b\b\u0002\u0010\u0019\u001a\u00020\u0018\u0012\b\b\u0002\u0010\u001d\u001a\u00020\u000f¢\u0006\u0004\b\u001f\u0010 B\u0011\b\u0012\u0012\u0006\u0010\"\u001a\u00020!¢\u0006\u0004\b\u001f\u0010#J\u0010\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016R\u001e\u0010\b\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00070\u00068\u0002X\u0004¢\u0006\u0006\n\u0004\b\b\u0010\tR\u0017\u0010\u000b\u001a\u00020\n8\u0006¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000eR\u0017\u0010\u0010\u001a\u00020\u000f8\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u0017\u0010\u0014\u001a\u00020\u000f8\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0011\u001a\u0004\b\u0015\u0010\u0013R\u0017\u0010\u0016\u001a\u00020\u000f8\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0011\u001a\u0004\b\u0017\u0010\u0013R\u0017\u0010\u0019\u001a\u00020\u00188\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001cR\u0017\u0010\u001d\u001a\u00020\u000f8\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u0011\u001a\u0004\b\u001e\u0010\u0013¨\u0006&"}, d2 = {"Lcom/fasterxml/jackson/module/kotlin/KotlinModule;", "Lcom/fasterxml/jackson/databind/module/SimpleModule;", "Lcom/fasterxml/jackson/databind/Module$SetupContext;", "context", "", "setupModule", "", "Lkotlin/reflect/KClass;", "ignoredClassesForImplyingJsonCreator", "Ljava/util/Set;", "", "reflectionCacheSize", "I", "getReflectionCacheSize", "()I", "", "nullToEmptyCollection", "Z", "getNullToEmptyCollection", "()Z", "nullToEmptyMap", "getNullToEmptyMap", "nullIsSameAsDefault", "getNullIsSameAsDefault", "Lcom/fasterxml/jackson/module/kotlin/SingletonSupport;", "singletonSupport", "Lcom/fasterxml/jackson/module/kotlin/SingletonSupport;", "getSingletonSupport", "()Lcom/fasterxml/jackson/module/kotlin/SingletonSupport;", "strictNullChecks", "getStrictNullChecks", "<init>", "(IZZZLcom/fasterxml/jackson/module/kotlin/SingletonSupport;Z)V", "Lcom/fasterxml/jackson/module/kotlin/KotlinModule$Builder;", "builder", "(Lcom/fasterxml/jackson/module/kotlin/KotlinModule$Builder;)V", "Companion", "Builder", "jackson-module-kotlin"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes.dex */
public final class KotlinModule extends SimpleModule {
    public static final Companion Companion = new Companion(null);
    private final Set<KClass<?>> ignoredClassesForImplyingJsonCreator;
    private final boolean nullIsSameAsDefault;
    private final boolean nullToEmptyCollection;
    private final boolean nullToEmptyMap;
    private final int reflectionCacheSize;
    private final SingletonSupport singletonSupport;
    private final boolean strictNullChecks;

    @Metadata(bv = {1, 0, 3}, k = 3, mv = {1, 4, 0})
    /* loaded from: classes.dex */
    public final /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[SingletonSupport.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[SingletonSupport.DISABLED.ordinal()] = 1;
            iArr[SingletonSupport.CANONICALIZE.ordinal()] = 2;
        }
    }

    public KotlinModule() {
        this(0, false, false, false, null, false, 63, null);
    }

    public /* synthetic */ KotlinModule(Builder builder, DefaultConstructorMarker defaultConstructorMarker) {
        this(builder);
    }

    public /* synthetic */ KotlinModule(int i, boolean z, boolean z2, boolean z3, SingletonSupport singletonSupport, boolean z4, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this((i2 & 1) != 0 ? 512 : i, (i2 & 2) != 0 ? false : z, (i2 & 4) != 0 ? false : z2, (i2 & 8) != 0 ? false : z3, (i2 & 16) != 0 ? SingletonSupport.DISABLED : singletonSupport, (i2 & 32) == 0 ? z4 : false);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public KotlinModule(int i, boolean z, boolean z2, boolean z3, SingletonSupport singletonSupport, boolean z4) {
        super(PackageVersion.VERSION);
        Intrinsics.checkNotNullParameter(singletonSupport, "singletonSupport");
        this.reflectionCacheSize = i;
        this.nullToEmptyCollection = z;
        this.nullToEmptyMap = z2;
        this.nullIsSameAsDefault = z3;
        this.singletonSupport = singletonSupport;
        this.strictNullChecks = z4;
        this.ignoredClassesForImplyingJsonCreator = SetsKt__SetsKt.emptySet();
    }

    private KotlinModule(Builder builder) {
        this(builder.getReflectionCacheSize(), builder.getNullToEmptyCollection(), builder.getNullToEmptyMap(), builder.getNullIsSameAsDefault(), builder.getSingletonSupport(), builder.getStrictNullChecks());
    }

    /* compiled from: KotlinModule.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lcom/fasterxml/jackson/module/kotlin/KotlinModule$Companion;", "", "()V", "serialVersionUID", "", "jackson-module-kotlin"}, k = 1, mv = {1, 4, 0})
    /* loaded from: classes.dex */
    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    @Override // com.fasterxml.jackson.databind.module.SimpleModule, com.fasterxml.jackson.databind.Module
    public void setupModule(Module.SetupContext setupContext) {
        Intrinsics.checkNotNullParameter(setupContext, "context");
        super.setupModule(setupContext);
        if (setupContext.isEnabled(MapperFeature.USE_ANNOTATIONS)) {
            ReflectionCache reflectionCache = new ReflectionCache(this.reflectionCacheSize);
            setupContext.addValueInstantiators(new KotlinInstantiators(reflectionCache, this.nullToEmptyCollection, this.nullToEmptyMap, this.nullIsSameAsDefault, this.strictNullChecks));
            if (WhenMappings.$EnumSwitchMapping$0[this.singletonSupport.ordinal()] == 2) {
                setupContext.addBeanDeserializerModifier(KotlinBeanDeserializerModifier.INSTANCE);
            }
            setupContext.insertAnnotationIntrospector(new KotlinAnnotationIntrospector(setupContext, reflectionCache, this.nullToEmptyCollection, this.nullToEmptyMap, this.nullIsSameAsDefault));
            setupContext.appendAnnotationIntrospector(new KotlinNamesAnnotationIntrospector(this, reflectionCache, this.ignoredClassesForImplyingJsonCreator));
            setupContext.addDeserializers(new KotlinDeserializers());
            setupContext.addSerializers(new KotlinSerializers());
            KotlinModule$setupModule$1 kotlinModule$setupModule$1 = new Function2<Class<?>, Class<?>, Unit>(setupContext) { // from class: com.fasterxml.jackson.module.kotlin.KotlinModule$setupModule$1
                final /* synthetic */ Module.SetupContext $context;

                /* access modifiers changed from: package-private */
                {
                    this.$context = r1;
                }

                /* Return type fixed from 'java.lang.Object' to match base method */
                @Override // kotlin.jvm.functions.Function2
                public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
                    invoke((Class) obj, (Class) obj2);
                    return Unit.INSTANCE;
                }

                public final void invoke(Class<?> cls, Class<?> cls2) {
                    Intrinsics.checkNotNullParameter(cls, "clazz");
                    Intrinsics.checkNotNullParameter(cls2, "mixin");
                    this.$context.setMixInAnnotations(cls, cls2);
                }
            };
            kotlinModule$setupModule$1.invoke(IntRange.class, ClosedRangeMixin.class);
            kotlinModule$setupModule$1.invoke(CharRange.class, ClosedRangeMixin.class);
            kotlinModule$setupModule$1.invoke(LongRange.class, ClosedRangeMixin.class);
            kotlinModule$setupModule$1.invoke(ClosedRange.class, ClosedRangeMixin.class);
            return;
        }
        throw new IllegalStateException("The Jackson Kotlin module requires USE_ANNOTATIONS to be true or it cannot function");
    }

    /* compiled from: KotlinModule.kt */
    @Metadata(bv = {}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u001a\u0010\u001bJ\u0006\u0010\u0003\u001a\u00020\u0002R$\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00048\u0006@BX\u000e¢\u0006\f\n\u0004\b\u0006\u0010\u0007\u001a\u0004\b\b\u0010\tR$\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0005\u001a\u00020\n8\u0006@BX\u000e¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000eR$\u0010\u000f\u001a\u00020\n2\u0006\u0010\u0005\u001a\u00020\n8\u0006@BX\u000e¢\u0006\f\n\u0004\b\u000f\u0010\f\u001a\u0004\b\u0010\u0010\u000eR$\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0005\u001a\u00020\n8\u0006@BX\u000e¢\u0006\f\n\u0004\b\u0011\u0010\f\u001a\u0004\b\u0012\u0010\u000eR$\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0005\u001a\u00020\u00138\u0006@BX\u000e¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R$\u0010\u0018\u001a\u00020\n2\u0006\u0010\u0005\u001a\u00020\n8\u0006@BX\u000e¢\u0006\f\n\u0004\b\u0018\u0010\f\u001a\u0004\b\u0019\u0010\u000e¨\u0006\u001c"}, d2 = {"Lcom/fasterxml/jackson/module/kotlin/KotlinModule$Builder;", "", "Lcom/fasterxml/jackson/module/kotlin/KotlinModule;", "build", "", "<set-?>", "reflectionCacheSize", "I", "getReflectionCacheSize", "()I", "", "nullToEmptyCollection", "Z", "getNullToEmptyCollection", "()Z", "nullToEmptyMap", "getNullToEmptyMap", "nullIsSameAsDefault", "getNullIsSameAsDefault", "Lcom/fasterxml/jackson/module/kotlin/SingletonSupport;", "singletonSupport", "Lcom/fasterxml/jackson/module/kotlin/SingletonSupport;", "getSingletonSupport", "()Lcom/fasterxml/jackson/module/kotlin/SingletonSupport;", "strictNullChecks", "getStrictNullChecks", "<init>", "()V", "jackson-module-kotlin"}, k = 1, mv = {1, 4, 0})
    /* loaded from: classes.dex */
    public static final class Builder {
        private boolean nullIsSameAsDefault;
        private boolean nullToEmptyCollection;
        private boolean nullToEmptyMap;
        private int reflectionCacheSize = 512;
        private SingletonSupport singletonSupport = SingletonSupport.DISABLED;
        private boolean strictNullChecks;

        public final int getReflectionCacheSize() {
            return this.reflectionCacheSize;
        }

        public final boolean getNullToEmptyCollection() {
            return this.nullToEmptyCollection;
        }

        public final boolean getNullToEmptyMap() {
            return this.nullToEmptyMap;
        }

        public final boolean getNullIsSameAsDefault() {
            return this.nullIsSameAsDefault;
        }

        public final SingletonSupport getSingletonSupport() {
            return this.singletonSupport;
        }

        public final boolean getStrictNullChecks() {
            return this.strictNullChecks;
        }

        public final KotlinModule build() {
            return new KotlinModule(this, null);
        }
    }
}
