package com.fasterxml.jackson.module.kotlin;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.kotlin.KotlinModule;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: Extensions.kt */
@Metadata(bv = {}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u001a\u001c\u0010\u0005\u001a\u00020\u00042\u0014\b\u0002\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00020\u0000\u001a\n\u0010\u0007\u001a\u00020\u0006*\u00020\u0006\u001a&\u0010\u000e\u001a\n \r*\u0004\u0018\u00010\b0\b*\u00020\b2\b\u0010\n\u001a\u0004\u0018\u00010\t2\u0006\u0010\f\u001a\u00020\u000bH\u0000¨\u0006\u000f"}, d2 = {"Lkotlin/Function1;", "Lcom/fasterxml/jackson/module/kotlin/KotlinModule$Builder;", "", "initializer", "Lcom/fasterxml/jackson/module/kotlin/KotlinModule;", "kotlinModule", "Lcom/fasterxml/jackson/databind/ObjectMapper;", "registerKotlinModule", "Lcom/fasterxml/jackson/databind/JsonMappingException;", "", "refFrom", "", "refFieldName", "kotlin.jvm.PlatformType", "wrapWithPath", "jackson-module-kotlin"}, k = 2, mv = {1, 4, 0})
/* loaded from: classes.dex */
public final class ExtensionsKt {
    public static /* synthetic */ KotlinModule kotlinModule$default(Function1 function1, int i, Object obj) {
        if ((i & 1) != 0) {
            function1 = ExtensionsKt$kotlinModule$1.INSTANCE;
        }
        return kotlinModule(function1);
    }

    public static final KotlinModule kotlinModule(Function1<? super KotlinModule.Builder, Unit> function1) {
        Intrinsics.checkNotNullParameter(function1, "initializer");
        KotlinModule.Builder builder = new KotlinModule.Builder();
        function1.invoke(builder);
        return builder.build();
    }

    public static final ObjectMapper registerKotlinModule(ObjectMapper objectMapper) {
        Intrinsics.checkNotNullParameter(objectMapper, "$this$registerKotlinModule");
        ObjectMapper registerModule = objectMapper.registerModule(kotlinModule$default(null, 1, null));
        Intrinsics.checkNotNullExpressionValue(registerModule, "this.registerModule(kotlinModule())");
        return registerModule;
    }

    public static final JsonMappingException wrapWithPath(JsonMappingException jsonMappingException, Object obj, String str) {
        Intrinsics.checkNotNullParameter(jsonMappingException, "$this$wrapWithPath");
        Intrinsics.checkNotNullParameter(str, "refFieldName");
        return JsonMappingException.wrapWithPath(jsonMappingException, obj, str);
    }
}
