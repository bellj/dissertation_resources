package com.fasterxml.jackson.module.kotlin;

import kotlin.Metadata;

/* compiled from: SingletonSupport.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"}, d2 = {"Lcom/fasterxml/jackson/module/kotlin/SingletonSupport;", "", "(Ljava/lang/String;I)V", "DISABLED", "CANONICALIZE", "jackson-module-kotlin"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes.dex */
public enum SingletonSupport {
    DISABLED,
    CANONICALIZE
}
