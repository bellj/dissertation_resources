package com.fasterxml.jackson.module.kotlin;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.introspect.AnnotatedField;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.introspect.AnnotatedParameter;
import com.fasterxml.jackson.databind.introspect.NopAnnotationIntrospector;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.JvmClassMappingKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import kotlin.reflect.KClass;
import kotlin.reflect.KFunction;
import kotlin.reflect.KMutableProperty;
import kotlin.reflect.KMutableProperty1;
import kotlin.reflect.KParameter;
import kotlin.reflect.KProperty;
import kotlin.reflect.KProperty1;
import kotlin.reflect.KType;
import kotlin.reflect.full.KClasses;
import kotlin.reflect.full.KClassifiers;
import kotlin.reflect.jvm.ReflectJvmMapping;

/* compiled from: KotlinAnnotationIntrospector.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0000\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\u0007¢\u0006\u0002\u0010\nJ\u001e\u0010\u000b\u001a\u0004\u0018\u00010\f2\n\u0010\r\u001a\u0006\u0012\u0002\b\u00030\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0018\u0010\u0011\u001a\n\u0012\u0004\u0012\u00020\u0013\u0018\u00010\u00122\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0017\u0010\u0014\u001a\u0004\u0018\u00010\u00072\u0006\u0010\u0015\u001a\u00020\u0016H\u0016¢\u0006\u0002\u0010\u0017J#\u0010\u0018\u001a\u0004\u0018\u00010\u00072\b\u0010\u0019\u001a\u0004\u0018\u00010\u00072\b\u0010\u001a\u001a\u0004\u0018\u00010\u0007H\u0002¢\u0006\u0002\u0010\u001bJ\u001e\u0010\u001c\u001a\u0014\u0012\u0006\b\u0001\u0012\u00020\u001e\u0012\u0006\u0012\u0004\u0018\u00010\u001e\u0018\u00010\u001d*\u00020\u001fH\u0002J\u001e\u0010 \u001a\u0014\u0012\u0006\b\u0001\u0012\u00020\u001e\u0012\u0006\u0012\u0004\u0018\u00010\u001e\u0018\u00010!*\u00020\u001fH\u0002J\u0013\u0010\u0014\u001a\u0004\u0018\u00010\u0007*\u00020\"H\u0002¢\u0006\u0002\u0010#J\u0013\u0010\u0014\u001a\u0004\u0018\u00010\u0007*\u00020\u001fH\u0002¢\u0006\u0002\u0010$J\u0013\u0010\u0014\u001a\u0004\u0018\u00010\u0007*\u00020%H\u0002¢\u0006\u0002\u0010&J\u0018\u0010'\u001a\u00020\u0007*\u0006\u0012\u0002\b\u00030(2\u0006\u0010)\u001a\u00020*H\u0002J\u0010\u0010+\u001a\u00020\u0007*\u0006\u0012\u0002\b\u00030(H\u0002J\u0018\u0010,\u001a\u00020\u0007*\u0006\u0012\u0002\b\u00030(2\u0006\u0010)\u001a\u00020*H\u0002J\u0018\u0010-\u001a\u00020\u0007*\u0006\u0012\u0002\b\u00030(2\u0006\u0010)\u001a\u00020*H\u0002J\f\u0010.\u001a\u00020\u0007*\u00020/H\u0002J\u0013\u00100\u001a\u0004\u0018\u00010\u0007*\u000201H\u0002¢\u0006\u0002\u00102J\u0013\u00100\u001a\u0004\u0018\u00010\u0007*\u000203H\u0002¢\u0006\u0002\u00104J\u0010\u00105\u001a\u00020\u0007*\u0006\u0012\u0002\b\u00030(H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u00066"}, d2 = {"Lcom/fasterxml/jackson/module/kotlin/KotlinAnnotationIntrospector;", "Lcom/fasterxml/jackson/databind/introspect/NopAnnotationIntrospector;", "context", "Lcom/fasterxml/jackson/databind/Module$SetupContext;", "cache", "Lcom/fasterxml/jackson/module/kotlin/ReflectionCache;", "nullToEmptyCollection", "", "nullToEmptyMap", "nullIsSameAsDefault", "(Lcom/fasterxml/jackson/databind/Module$SetupContext;Lcom/fasterxml/jackson/module/kotlin/ReflectionCache;ZZZ)V", "findCreatorAnnotation", "Lcom/fasterxml/jackson/annotation/JsonCreator$Mode;", "config", "Lcom/fasterxml/jackson/databind/cfg/MapperConfig;", "a", "Lcom/fasterxml/jackson/databind/introspect/Annotated;", "findSubtypes", "", "Lcom/fasterxml/jackson/databind/jsontype/NamedType;", "hasRequiredMarker", "m", "Lcom/fasterxml/jackson/databind/introspect/AnnotatedMember;", "(Lcom/fasterxml/jackson/databind/introspect/AnnotatedMember;)Ljava/lang/Boolean;", "requiredAnnotationOrNullability", "byAnnotation", "byNullability", "(Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;", "getCorrespondingGetter", "Lkotlin/reflect/KProperty1;", "", "Lcom/fasterxml/jackson/databind/introspect/AnnotatedMethod;", "getCorrespondingSetter", "Lkotlin/reflect/KMutableProperty1$Setter;", "Lcom/fasterxml/jackson/databind/introspect/AnnotatedField;", "(Lcom/fasterxml/jackson/databind/introspect/AnnotatedField;)Ljava/lang/Boolean;", "(Lcom/fasterxml/jackson/databind/introspect/AnnotatedMethod;)Ljava/lang/Boolean;", "Lcom/fasterxml/jackson/databind/introspect/AnnotatedParameter;", "(Lcom/fasterxml/jackson/databind/introspect/AnnotatedParameter;)Ljava/lang/Boolean;", "isConstructorParameterRequired", "Lkotlin/reflect/KFunction;", "index", "", "isGetterLike", "isMethodParameterRequired", "isParameterRequired", "isRequired", "Lkotlin/reflect/KType;", "isRequiredByAnnotation", "Ljava/lang/reflect/AccessibleObject;", "(Ljava/lang/reflect/AccessibleObject;)Ljava/lang/Boolean;", "Ljava/lang/reflect/Method;", "(Ljava/lang/reflect/Method;)Ljava/lang/Boolean;", "isSetterLike", "jackson-module-kotlin"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes.dex */
public final class KotlinAnnotationIntrospector extends NopAnnotationIntrospector {
    private final ReflectionCache cache;
    private final Module.SetupContext context;
    private final boolean nullIsSameAsDefault;
    private final boolean nullToEmptyCollection;
    private final boolean nullToEmptyMap;

    public KotlinAnnotationIntrospector(Module.SetupContext setupContext, ReflectionCache reflectionCache, boolean z, boolean z2, boolean z3) {
        Intrinsics.checkNotNullParameter(setupContext, "context");
        Intrinsics.checkNotNullParameter(reflectionCache, "cache");
        this.context = setupContext;
        this.cache = reflectionCache;
        this.nullToEmptyCollection = z;
        this.nullToEmptyMap = z2;
        this.nullIsSameAsDefault = z3;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public Boolean hasRequiredMarker(AnnotatedMember annotatedMember) {
        Intrinsics.checkNotNullParameter(annotatedMember, "m");
        return this.cache.javaMemberIsRequired(annotatedMember, new Function1<AnnotatedMember, Boolean>(this, annotatedMember) { // from class: com.fasterxml.jackson.module.kotlin.KotlinAnnotationIntrospector$hasRequiredMarker$hasRequired$1
            final /* synthetic */ AnnotatedMember $m;
            final /* synthetic */ KotlinAnnotationIntrospector this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
                this.$m = r2;
            }

            public final Boolean invoke(AnnotatedMember annotatedMember2) {
                Intrinsics.checkNotNullParameter(annotatedMember2, "it");
                try {
                    if (KotlinAnnotationIntrospector.access$getNullToEmptyCollection$p(this.this$0)) {
                        JavaType type = this.$m.getType();
                        Intrinsics.checkNotNullExpressionValue(type, "m.type");
                        if (type.isCollectionLikeType()) {
                            return Boolean.FALSE;
                        }
                    }
                    if (KotlinAnnotationIntrospector.access$getNullToEmptyMap$p(this.this$0)) {
                        JavaType type2 = this.$m.getType();
                        Intrinsics.checkNotNullExpressionValue(type2, "m.type");
                        if (type2.isMapLikeType()) {
                            return Boolean.FALSE;
                        }
                    }
                    Member member = this.$m.getMember();
                    Intrinsics.checkNotNullExpressionValue(member, "m.member");
                    Class<?> declaringClass = member.getDeclaringClass();
                    Intrinsics.checkNotNullExpressionValue(declaringClass, "m.member.declaringClass");
                    if (!KotlinModuleKt.isKotlinClass(declaringClass)) {
                        return null;
                    }
                    AnnotatedMember annotatedMember3 = this.$m;
                    if (annotatedMember3 instanceof AnnotatedField) {
                        return KotlinAnnotationIntrospector.access$hasRequiredMarker(this.this$0, (AnnotatedField) annotatedMember3);
                    }
                    if (annotatedMember3 instanceof AnnotatedMethod) {
                        return KotlinAnnotationIntrospector.access$hasRequiredMarker(this.this$0, (AnnotatedMethod) annotatedMember3);
                    }
                    if (annotatedMember3 instanceof AnnotatedParameter) {
                        return KotlinAnnotationIntrospector.access$hasRequiredMarker(this.this$0, (AnnotatedParameter) annotatedMember3);
                    }
                    return null;
                } catch (UnsupportedOperationException unused) {
                    return null;
                }
            }
        });
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public JsonCreator.Mode findCreatorAnnotation(MapperConfig<?> mapperConfig, Annotated annotated) {
        Intrinsics.checkNotNullParameter(mapperConfig, "config");
        Intrinsics.checkNotNullParameter(annotated, "a");
        return super.findCreatorAnnotation(mapperConfig, annotated);
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public List<NamedType> findSubtypes(Annotated annotated) {
        Intrinsics.checkNotNullParameter(annotated, "a");
        Class<?> rawType = annotated.getRawType();
        Intrinsics.checkNotNullExpressionValue(rawType, "rawType");
        if (!KotlinModuleKt.isKotlinClass(rawType)) {
            return null;
        }
        KClass kotlinClass = JvmClassMappingKt.getKotlinClass(rawType);
        if (!kotlinClass.isSealed()) {
            return null;
        }
        List<KClass> sealedSubclasses = kotlinClass.getSealedSubclasses();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(sealedSubclasses, 10));
        for (KClass kClass : sealedSubclasses) {
            arrayList.add(new NamedType(JvmClassMappingKt.getJavaClass(kClass)));
        }
        return CollectionsKt___CollectionsKt.toMutableList((Collection) arrayList);
    }

    public final Boolean hasRequiredMarker(AnnotatedField annotatedField) {
        KType returnType;
        Member member = annotatedField.getMember();
        if (member != null) {
            Boolean isRequiredByAnnotation = isRequiredByAnnotation((Field) member);
            Member member2 = annotatedField.getMember();
            if (member2 != null) {
                KProperty<?> kotlinProperty = ReflectJvmMapping.getKotlinProperty((Field) member2);
                return requiredAnnotationOrNullability(isRequiredByAnnotation, (kotlinProperty == null || (returnType = kotlinProperty.getReturnType()) == null) ? null : Boolean.valueOf(isRequired(returnType)));
            }
            throw new NullPointerException("null cannot be cast to non-null type java.lang.reflect.Field");
        }
        throw new NullPointerException("null cannot be cast to non-null type java.lang.reflect.Field");
    }

    private final Boolean isRequiredByAnnotation(AccessibleObject accessibleObject) {
        JsonProperty jsonProperty;
        Annotation[] annotations = accessibleObject.getAnnotations();
        if (annotations == null) {
            return null;
        }
        int length = annotations.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                jsonProperty = null;
                break;
            }
            jsonProperty = annotations[i];
            if (Intrinsics.areEqual(JvmClassMappingKt.getAnnotationClass(jsonProperty), Reflection.getOrCreateKotlinClass(JsonProperty.class))) {
                break;
            }
            i++;
        }
        if (jsonProperty != null) {
            return Boolean.valueOf(jsonProperty.required());
        }
        return null;
    }

    private final Boolean requiredAnnotationOrNullability(Boolean bool, Boolean bool2) {
        if (bool == null || bool2 == null) {
            return bool2 != null ? bool2 : bool;
        }
        return Boolean.valueOf(bool.booleanValue() || bool2.booleanValue());
    }

    private final Boolean isRequiredByAnnotation(Method method) {
        JsonProperty jsonProperty;
        Annotation[] annotations = method.getAnnotations();
        Intrinsics.checkNotNullExpressionValue(annotations, "this.annotations");
        int length = annotations.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                jsonProperty = null;
                break;
            }
            jsonProperty = annotations[i];
            if (Intrinsics.areEqual(JvmClassMappingKt.getJavaClass(JvmClassMappingKt.getAnnotationClass(jsonProperty)), JsonProperty.class)) {
                break;
            }
            i++;
        }
        if (!(jsonProperty instanceof JsonProperty)) {
            jsonProperty = null;
        }
        JsonProperty jsonProperty2 = jsonProperty;
        if (jsonProperty2 != null) {
            return Boolean.valueOf(jsonProperty2.required());
        }
        return null;
    }

    public final Boolean hasRequiredMarker(AnnotatedMethod annotatedMethod) {
        KProperty1<? extends Object, Object> correspondingGetter = getCorrespondingGetter(annotatedMethod);
        Boolean bool = null;
        if (correspondingGetter != null) {
            Method javaGetter = ReflectJvmMapping.getJavaGetter(correspondingGetter);
            if (javaGetter != null) {
                bool = isRequiredByAnnotation(javaGetter);
            }
            return requiredAnnotationOrNullability(bool, Boolean.valueOf(isRequired(correspondingGetter.getReturnType())));
        }
        KMutableProperty1.Setter<? extends Object, Object> correspondingSetter = getCorrespondingSetter(annotatedMethod);
        if (correspondingSetter != null) {
            Method javaMethod = ReflectJvmMapping.getJavaMethod(correspondingSetter);
            if (javaMethod != null) {
                bool = isRequiredByAnnotation(javaMethod);
            }
            return requiredAnnotationOrNullability(bool, Boolean.valueOf(isMethodParameterRequired(correspondingSetter, 0)));
        }
        Method member = annotatedMethod.getMember();
        Intrinsics.checkNotNullExpressionValue(member, "this.member");
        KFunction<?> kotlinFunction = ReflectJvmMapping.getKotlinFunction(member);
        if (kotlinFunction != null) {
            Method javaMethod2 = ReflectJvmMapping.getJavaMethod(kotlinFunction);
            Boolean isRequiredByAnnotation = javaMethod2 != null ? isRequiredByAnnotation(javaMethod2) : null;
            if (isGetterLike(kotlinFunction)) {
                return requiredAnnotationOrNullability(isRequiredByAnnotation, Boolean.valueOf(isRequired(kotlinFunction.getReturnType())));
            }
            if (isSetterLike(kotlinFunction)) {
                return requiredAnnotationOrNullability(isRequiredByAnnotation, Boolean.valueOf(isMethodParameterRequired(kotlinFunction, 0)));
            }
        }
        return null;
    }

    private final boolean isGetterLike(KFunction<?> kFunction) {
        return kFunction.getParameters().size() == 1;
    }

    private final boolean isSetterLike(KFunction<?> kFunction) {
        return kFunction.getParameters().size() == 2 && Intrinsics.areEqual(kFunction.getReturnType(), KClassifiers.createType$default(Reflection.getOrCreateKotlinClass(Unit.class), null, false, null, 7, null));
    }

    private final KProperty1<? extends Object, Object> getCorrespondingGetter(AnnotatedMethod annotatedMethod) {
        Object obj;
        Method member = annotatedMethod.getMember();
        Intrinsics.checkNotNullExpressionValue(member, "member");
        Class<?> declaringClass = member.getDeclaringClass();
        Intrinsics.checkNotNullExpressionValue(declaringClass, "member.declaringClass");
        Iterator it = KClasses.getDeclaredMemberProperties(JvmClassMappingKt.getKotlinClass(declaringClass)).iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (Intrinsics.areEqual(ReflectJvmMapping.getJavaMethod(((KProperty1) obj).getGetter()), annotatedMethod.getMember())) {
                break;
            }
        }
        return (KProperty1) obj;
    }

    private final KMutableProperty1.Setter<? extends Object, Object> getCorrespondingSetter(AnnotatedMethod annotatedMethod) {
        Object obj;
        boolean z;
        Method member = annotatedMethod.getMember();
        Intrinsics.checkNotNullExpressionValue(member, "member");
        Class<?> declaringClass = member.getDeclaringClass();
        Intrinsics.checkNotNullExpressionValue(declaringClass, "member.declaringClass");
        Iterator it = KClasses.getDeclaredMemberProperties(JvmClassMappingKt.getKotlinClass(declaringClass)).iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            KProperty1 kProperty1 = (KProperty1) obj;
            if (kProperty1 instanceof KMutableProperty1) {
                z = Intrinsics.areEqual(ReflectJvmMapping.getJavaSetter((KMutableProperty) kProperty1), annotatedMethod.getMember());
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        KProperty1 kProperty12 = (KProperty1) obj;
        if (!(kProperty12 instanceof KMutableProperty1)) {
            kProperty12 = null;
        }
        KMutableProperty1 kMutableProperty1 = (KMutableProperty1) kProperty12;
        if (kMutableProperty1 != null) {
            return kMutableProperty1.getSetter();
        }
        return null;
    }

    public final Boolean hasRequiredMarker(AnnotatedParameter annotatedParameter) {
        KFunction<?> kotlinFunction;
        Member member = annotatedParameter.getMember();
        JsonProperty jsonProperty = (JsonProperty) annotatedParameter.getAnnotation(JsonProperty.class);
        Boolean bool = null;
        Boolean valueOf = jsonProperty != null ? Boolean.valueOf(jsonProperty.required()) : null;
        if (member instanceof Constructor) {
            KFunction<?> kotlinFunction2 = ReflectJvmMapping.getKotlinFunction((Constructor) member);
            if (kotlinFunction2 != null) {
                bool = Boolean.valueOf(isConstructorParameterRequired(kotlinFunction2, annotatedParameter.getIndex()));
            }
        } else if ((member instanceof Method) && (kotlinFunction = ReflectJvmMapping.getKotlinFunction((Method) member)) != null) {
            bool = Boolean.valueOf(isMethodParameterRequired(kotlinFunction, annotatedParameter.getIndex()));
        }
        return requiredAnnotationOrNullability(valueOf, bool);
    }

    private final boolean isConstructorParameterRequired(KFunction<?> kFunction, int i) {
        return isParameterRequired(kFunction, i);
    }

    private final boolean isMethodParameterRequired(KFunction<?> kFunction, int i) {
        return isParameterRequired(kFunction, i + 1);
    }

    private final boolean isParameterRequired(KFunction<?> kFunction, int i) {
        KParameter kParameter = kFunction.getParameters().get(i);
        KType type = kParameter.getType();
        Type javaType = ReflectJvmMapping.getJavaType(type);
        boolean isPrimitive = javaType instanceof Class ? ((Class) javaType).isPrimitive() : false;
        if (type.isMarkedNullable() || kParameter.isOptional()) {
            return false;
        }
        if (!isPrimitive || this.context.isEnabled(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES)) {
            return true;
        }
        return false;
    }

    private final boolean isRequired(KType kType) {
        return !kType.isMarkedNullable();
    }
}
