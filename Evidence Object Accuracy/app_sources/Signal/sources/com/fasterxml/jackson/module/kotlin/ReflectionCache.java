package com.fasterxml.jackson.module.kotlin;

import com.fasterxml.jackson.databind.introspect.AnnotatedConstructor;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import com.fasterxml.jackson.databind.util.LRUMap;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.JvmClassMappingKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.reflect.KClass;
import kotlin.reflect.KFunction;
import kotlin.reflect.jvm.ReflectJvmMapping;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* compiled from: ReflectionCache.kt */
@Metadata(bv = {}, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0000\u0018\u00002\u00020\u0001:\u0001\u001eB\u000f\u0012\u0006\u0010\u001b\u001a\u00020\u001a¢\u0006\u0004\b\u001c\u0010\u001dJ\u001a\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00010\u00042\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00010\u0002J\u001c\u0010\u0005\u001a\n\u0012\u0004\u0012\u00020\u0001\u0018\u00010\u00072\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00010\u0006J\u0014\u0010\u0005\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u00072\u0006\u0010\u0003\u001a\u00020\bJ\"\u0010\r\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\t2\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u000b0\nJ-\u0010\u000f\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\u0003\u001a\u00020\u000e2\u0014\u0010\f\u001a\u0010\u0012\u0004\u0012\u00020\u000e\u0012\u0006\u0012\u0004\u0018\u00010\u000b0\n¢\u0006\u0004\b\u000f\u0010\u0010R,\u0010\u0012\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00010\u0002\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00010\u00040\u00118\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0013R,\u0010\u0014\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00010\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00010\u00070\u00118\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0013R$\u0010\u0015\u001a\u0012\u0012\u0004\u0012\u00020\b\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00070\u00118\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0013R \u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u000b0\u00118\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0013R\"\u0010\u000f\u001a\u0010\u0012\u0004\u0012\u00020\u000e\u0012\u0006\u0012\u0004\u0018\u00010\u00170\u00118\u0002X\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u0013R \u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0018\u0012\u0004\u0012\u00020\u000b0\u00118\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u0013¨\u0006\u001f"}, d2 = {"Lcom/fasterxml/jackson/module/kotlin/ReflectionCache;", "", "Ljava/lang/Class;", "key", "Lkotlin/reflect/KClass;", "kotlinFromJava", "Ljava/lang/reflect/Constructor;", "Lkotlin/reflect/KFunction;", "Ljava/lang/reflect/Method;", "Lcom/fasterxml/jackson/databind/introspect/AnnotatedConstructor;", "Lkotlin/Function1;", "", "calc", "checkConstructorIsCreatorAnnotated", "Lcom/fasterxml/jackson/databind/introspect/AnnotatedMember;", "javaMemberIsRequired", "(Lcom/fasterxml/jackson/databind/introspect/AnnotatedMember;Lkotlin/jvm/functions/Function1;)Ljava/lang/Boolean;", "Lcom/fasterxml/jackson/databind/util/LRUMap;", "javaClassToKotlin", "Lcom/fasterxml/jackson/databind/util/LRUMap;", "javaConstructorToKotlin", "javaMethodToKotlin", "javaConstructorIsCreatorAnnotated", "Lcom/fasterxml/jackson/module/kotlin/ReflectionCache$BooleanTriState;", "Lcom/fasterxml/jackson/databind/introspect/AnnotatedMethod;", "kotlinGeneratedMethod", "", "reflectionCacheSize", "<init>", "(I)V", "BooleanTriState", "jackson-module-kotlin"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes.dex */
public final class ReflectionCache {
    private final LRUMap<Class<Object>, KClass<Object>> javaClassToKotlin;
    private final LRUMap<AnnotatedConstructor, Boolean> javaConstructorIsCreatorAnnotated;
    private final LRUMap<Constructor<Object>, KFunction<Object>> javaConstructorToKotlin;
    private final LRUMap<AnnotatedMember, BooleanTriState> javaMemberIsRequired;
    private final LRUMap<Method, KFunction<?>> javaMethodToKotlin;
    private final LRUMap<AnnotatedMethod, Boolean> kotlinGeneratedMethod;

    /* compiled from: ReflectionCache.kt */
    @Metadata(bv = {1, 0, 3}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \b2\u00020\u0001:\u0004\b\t\n\u000bB\u0011\b\u0002\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0004R\u0015\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\n\n\u0002\u0010\u0007\u001a\u0004\b\u0005\u0010\u0006\u0001\u0003\f\r\u000e¨\u0006\u000f"}, d2 = {"Lcom/fasterxml/jackson/module/kotlin/ReflectionCache$BooleanTriState;", "", DraftDatabase.DRAFT_VALUE, "", "(Ljava/lang/Boolean;)V", "getValue", "()Ljava/lang/Boolean;", "Ljava/lang/Boolean;", "Companion", "Empty", "False", "True", "Lcom/fasterxml/jackson/module/kotlin/ReflectionCache$BooleanTriState$True;", "Lcom/fasterxml/jackson/module/kotlin/ReflectionCache$BooleanTriState$False;", "Lcom/fasterxml/jackson/module/kotlin/ReflectionCache$BooleanTriState$Empty;", "jackson-module-kotlin"}, k = 1, mv = {1, 4, 0})
    /* loaded from: classes.dex */
    public static abstract class BooleanTriState {
        public static final Companion Companion = new Companion(null);
        private static final Empty EMPTY = new Empty();
        private static final False FALSE = new False();
        private static final True TRUE = new True();
        private final Boolean value;

        /* compiled from: ReflectionCache.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lcom/fasterxml/jackson/module/kotlin/ReflectionCache$BooleanTriState$True;", "Lcom/fasterxml/jackson/module/kotlin/ReflectionCache$BooleanTriState;", "()V", "jackson-module-kotlin"}, k = 1, mv = {1, 4, 0})
        /* loaded from: classes.dex */
        public static final class True extends BooleanTriState {
            public True() {
                super(Boolean.TRUE, null);
            }
        }

        private BooleanTriState(Boolean bool) {
            this.value = bool;
        }

        public /* synthetic */ BooleanTriState(Boolean bool, DefaultConstructorMarker defaultConstructorMarker) {
            this(bool);
        }

        public final Boolean getValue() {
            return this.value;
        }

        /* compiled from: ReflectionCache.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lcom/fasterxml/jackson/module/kotlin/ReflectionCache$BooleanTriState$False;", "Lcom/fasterxml/jackson/module/kotlin/ReflectionCache$BooleanTriState;", "()V", "jackson-module-kotlin"}, k = 1, mv = {1, 4, 0})
        /* loaded from: classes.dex */
        public static final class False extends BooleanTriState {
            public False() {
                super(Boolean.FALSE, null);
            }
        }

        /* compiled from: ReflectionCache.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lcom/fasterxml/jackson/module/kotlin/ReflectionCache$BooleanTriState$Empty;", "Lcom/fasterxml/jackson/module/kotlin/ReflectionCache$BooleanTriState;", "()V", "jackson-module-kotlin"}, k = 1, mv = {1, 4, 0})
        /* loaded from: classes.dex */
        public static final class Empty extends BooleanTriState {
            public Empty() {
                super(null, null);
            }
        }

        /* compiled from: ReflectionCache.kt */
        @Metadata(bv = {1, 0, 3}, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0015\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\f¢\u0006\u0002\u0010\rR\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lcom/fasterxml/jackson/module/kotlin/ReflectionCache$BooleanTriState$Companion;", "", "()V", "EMPTY", "Lcom/fasterxml/jackson/module/kotlin/ReflectionCache$BooleanTriState$Empty;", "FALSE", "Lcom/fasterxml/jackson/module/kotlin/ReflectionCache$BooleanTriState$False;", "TRUE", "Lcom/fasterxml/jackson/module/kotlin/ReflectionCache$BooleanTriState$True;", "fromBoolean", "Lcom/fasterxml/jackson/module/kotlin/ReflectionCache$BooleanTriState;", DraftDatabase.DRAFT_VALUE, "", "(Ljava/lang/Boolean;)Lcom/fasterxml/jackson/module/kotlin/ReflectionCache$BooleanTriState;", "jackson-module-kotlin"}, k = 1, mv = {1, 4, 0})
        /* loaded from: classes.dex */
        public static final class Companion {
            private Companion() {
            }

            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            public final BooleanTriState fromBoolean(Boolean bool) {
                if (bool == null) {
                    return BooleanTriState.EMPTY;
                }
                if (Intrinsics.areEqual(bool, Boolean.TRUE)) {
                    return BooleanTriState.TRUE;
                }
                if (Intrinsics.areEqual(bool, Boolean.FALSE)) {
                    return BooleanTriState.FALSE;
                }
                throw new NoWhenBranchMatchedException();
            }
        }
    }

    public ReflectionCache(int i) {
        this.javaClassToKotlin = new LRUMap<>(i, i);
        this.javaConstructorToKotlin = new LRUMap<>(i, i);
        this.javaMethodToKotlin = new LRUMap<>(i, i);
        this.javaConstructorIsCreatorAnnotated = new LRUMap<>(i, i);
        this.javaMemberIsRequired = new LRUMap<>(i, i);
        this.kotlinGeneratedMethod = new LRUMap<>(i, i);
    }

    public final KClass<Object> kotlinFromJava(Class<Object> cls) {
        Intrinsics.checkNotNullParameter(cls, "key");
        KClass<Object> kClass = this.javaClassToKotlin.get(cls);
        if (kClass != null) {
            return kClass;
        }
        KClass<Object> kotlinClass = JvmClassMappingKt.getKotlinClass(cls);
        KClass<Object> putIfAbsent = this.javaClassToKotlin.putIfAbsent(cls, kotlinClass);
        return putIfAbsent != null ? putIfAbsent : kotlinClass;
    }

    public final KFunction<Object> kotlinFromJava(Constructor<Object> constructor) {
        Intrinsics.checkNotNullParameter(constructor, "key");
        KFunction<Object> kFunction = this.javaConstructorToKotlin.get(constructor);
        if (kFunction != null) {
            return kFunction;
        }
        KFunction<Object> kotlinFunction = ReflectJvmMapping.getKotlinFunction(constructor);
        if (kotlinFunction == null) {
            return null;
        }
        KFunction<Object> putIfAbsent = this.javaConstructorToKotlin.putIfAbsent(constructor, kotlinFunction);
        return putIfAbsent != null ? putIfAbsent : kotlinFunction;
    }

    public final KFunction<?> kotlinFromJava(Method method) {
        Intrinsics.checkNotNullParameter(method, "key");
        KFunction<?> kFunction = this.javaMethodToKotlin.get(method);
        if (kFunction != null) {
            return kFunction;
        }
        KFunction<?> kotlinFunction = ReflectJvmMapping.getKotlinFunction(method);
        if (kotlinFunction == null) {
            return null;
        }
        KFunction<?> putIfAbsent = this.javaMethodToKotlin.putIfAbsent(method, kotlinFunction);
        return putIfAbsent != null ? putIfAbsent : kotlinFunction;
    }

    public final boolean checkConstructorIsCreatorAnnotated(AnnotatedConstructor annotatedConstructor, Function1<? super AnnotatedConstructor, Boolean> function1) {
        Intrinsics.checkNotNullParameter(annotatedConstructor, "key");
        Intrinsics.checkNotNullParameter(function1, "calc");
        Boolean bool = this.javaConstructorIsCreatorAnnotated.get(annotatedConstructor);
        if (bool != null) {
            return bool.booleanValue();
        }
        boolean booleanValue = function1.invoke(annotatedConstructor).booleanValue();
        Boolean putIfAbsent = this.javaConstructorIsCreatorAnnotated.putIfAbsent(annotatedConstructor, Boolean.valueOf(booleanValue));
        return putIfAbsent != null ? putIfAbsent.booleanValue() : booleanValue;
    }

    public final Boolean javaMemberIsRequired(AnnotatedMember annotatedMember, Function1<? super AnnotatedMember, Boolean> function1) {
        Boolean value;
        Boolean value2;
        Intrinsics.checkNotNullParameter(annotatedMember, "key");
        Intrinsics.checkNotNullParameter(function1, "calc");
        BooleanTriState booleanTriState = this.javaMemberIsRequired.get(annotatedMember);
        if (booleanTriState != null && (value2 = booleanTriState.getValue()) != null) {
            return value2;
        }
        Boolean invoke = function1.invoke(annotatedMember);
        BooleanTriState putIfAbsent = this.javaMemberIsRequired.putIfAbsent(annotatedMember, BooleanTriState.Companion.fromBoolean(invoke));
        return (putIfAbsent == null || (value = putIfAbsent.getValue()) == null) ? invoke : value;
    }
}
