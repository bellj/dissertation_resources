package com.fasterxml.jackson.module.kotlin;

import kotlin.Metadata;
import kotlin.jvm.JvmClassMappingKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.reflect.KType;
import kotlin.reflect.jvm.KTypesJvm;

/* compiled from: Types.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\u001a\u0014\u0010\u0003\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00020\u0001*\u00020\u0000H\u0000¨\u0006\u0004"}, d2 = {"Lkotlin/reflect/KType;", "Ljava/lang/Class;", "", "erasedType", "jackson-module-kotlin"}, k = 2, mv = {1, 4, 0})
/* loaded from: classes.dex */
public final class TypesKt {
    public static final Class<? extends Object> erasedType(KType kType) {
        Intrinsics.checkNotNullParameter(kType, "$this$erasedType");
        return JvmClassMappingKt.getJavaClass(KTypesJvm.getJvmErasure(kType));
    }
}
