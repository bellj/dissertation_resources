package com.fasterxml.jackson.module.kotlin;

import kotlin.Metadata;

/* compiled from: KotlinMixins.kt */
@Metadata(bv = {}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\b \u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002¨\u0006\u0003"}, d2 = {"Lcom/fasterxml/jackson/module/kotlin/ClosedRangeMixin;", "T", "", "jackson-module-kotlin"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes.dex */
public abstract class ClosedRangeMixin<T> {
}
