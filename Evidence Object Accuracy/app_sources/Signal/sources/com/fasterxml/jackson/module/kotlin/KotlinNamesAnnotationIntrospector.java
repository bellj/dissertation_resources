package com.fasterxml.jackson.module.kotlin;

import com.fasterxml.jackson.databind.PropertyName;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.introspect.AnnotatedConstructor;
import com.fasterxml.jackson.databind.introspect.AnnotatedField;
import com.fasterxml.jackson.databind.introspect.AnnotatedMember;
import com.fasterxml.jackson.databind.introspect.AnnotatedParameter;
import com.fasterxml.jackson.databind.introspect.AnnotatedWithParams;
import com.fasterxml.jackson.databind.introspect.NopAnnotationIntrospector;
import com.fasterxml.jackson.databind.util.BeanUtil;
import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.reflect.KClass;
import kotlin.reflect.KFunction;
import kotlin.reflect.KParameter;
import kotlin.reflect.jvm.ReflectJvmMapping;
import kotlin.reflect.jvm.internal.KotlinReflectionInternalError;
import kotlin.text.StringsKt__StringsJVMKt;

/* compiled from: KotlinNamesAnnotationIntrospector.kt */
@Metadata(bv = {}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0000\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u0014\u001a\u00020\u0013\u0012\u0006\u0010\u0019\u001a\u00020\u0018\u0012\u0010\u0010\u001f\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u001e0\u001d¢\u0006\u0004\b#\u0010$J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002J\u0012\u0010\b\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\u0006H\u0016J&\u0010\u000f\u001a\u0004\u0018\u00010\r2\n\u0010\n\u001a\u0006\u0012\u0002\b\u00030\t2\u0006\u0010\f\u001a\u00020\u000b2\u0006\u0010\u000e\u001a\u00020\rH\u0016J\u0010\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0007\u001a\u00020\u0010H\u0016R\u0017\u0010\u0014\u001a\u00020\u00138\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R\u0017\u0010\u0019\u001a\u00020\u00188\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001cR!\u0010\u001f\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u001e0\u001d8\u0006¢\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b!\u0010\"¨\u0006%"}, d2 = {"Lcom/fasterxml/jackson/module/kotlin/KotlinNamesAnnotationIntrospector;", "Lcom/fasterxml/jackson/databind/introspect/NopAnnotationIntrospector;", "Lcom/fasterxml/jackson/databind/introspect/AnnotatedParameter;", "param", "", "findKotlinParameterName", "Lcom/fasterxml/jackson/databind/introspect/AnnotatedMember;", "member", "findImplicitPropertyName", "Lcom/fasterxml/jackson/databind/cfg/MapperConfig;", "config", "Lcom/fasterxml/jackson/databind/introspect/AnnotatedField;", "field", "Lcom/fasterxml/jackson/databind/PropertyName;", "implName", "findRenameByField", "Lcom/fasterxml/jackson/databind/introspect/Annotated;", "", "hasCreatorAnnotation", "Lcom/fasterxml/jackson/module/kotlin/KotlinModule;", "module", "Lcom/fasterxml/jackson/module/kotlin/KotlinModule;", "getModule", "()Lcom/fasterxml/jackson/module/kotlin/KotlinModule;", "Lcom/fasterxml/jackson/module/kotlin/ReflectionCache;", "cache", "Lcom/fasterxml/jackson/module/kotlin/ReflectionCache;", "getCache", "()Lcom/fasterxml/jackson/module/kotlin/ReflectionCache;", "", "Lkotlin/reflect/KClass;", "ignoredClassesForImplyingJsonCreator", "Ljava/util/Set;", "getIgnoredClassesForImplyingJsonCreator", "()Ljava/util/Set;", "<init>", "(Lcom/fasterxml/jackson/module/kotlin/KotlinModule;Lcom/fasterxml/jackson/module/kotlin/ReflectionCache;Ljava/util/Set;)V", "jackson-module-kotlin"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes.dex */
public final class KotlinNamesAnnotationIntrospector extends NopAnnotationIntrospector {
    private final ReflectionCache cache;
    private final Set<KClass<?>> ignoredClassesForImplyingJsonCreator;
    private final KotlinModule module;

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.util.Set<? extends kotlin.reflect.KClass<?>> */
    /* JADX WARN: Multi-variable type inference failed */
    public KotlinNamesAnnotationIntrospector(KotlinModule kotlinModule, ReflectionCache reflectionCache, Set<? extends KClass<?>> set) {
        Intrinsics.checkNotNullParameter(kotlinModule, "module");
        Intrinsics.checkNotNullParameter(reflectionCache, "cache");
        Intrinsics.checkNotNullParameter(set, "ignoredClassesForImplyingJsonCreator");
        this.module = kotlinModule;
        this.cache = reflectionCache;
        this.ignoredClassesForImplyingJsonCreator = set;
    }

    public final ReflectionCache getCache() {
        return this.cache;
    }

    public final Set<KClass<?>> getIgnoredClassesForImplyingJsonCreator() {
        return this.ignoredClassesForImplyingJsonCreator;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public String findImplicitPropertyName(AnnotatedMember annotatedMember) {
        Intrinsics.checkNotNullParameter(annotatedMember, "member");
        if (annotatedMember instanceof AnnotatedParameter) {
            return findKotlinParameterName((AnnotatedParameter) annotatedMember);
        }
        return null;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public PropertyName findRenameByField(MapperConfig<?> mapperConfig, AnnotatedField annotatedField, PropertyName propertyName) {
        String stdManglePropertyName;
        Intrinsics.checkNotNullParameter(mapperConfig, "config");
        Intrinsics.checkNotNullParameter(annotatedField, "field");
        Intrinsics.checkNotNullParameter(propertyName, "implName");
        String simpleName = propertyName.getSimpleName();
        Class<?> declaringClass = annotatedField.getDeclaringClass();
        Intrinsics.checkNotNullExpressionValue(declaringClass, "field.declaringClass");
        if (KotlinModuleKt.isKotlinClass(declaringClass)) {
            Intrinsics.checkNotNullExpressionValue(simpleName, "origSimple");
            if ((StringsKt__StringsJVMKt.startsWith$default(simpleName, "is", false, 2, null)) && (stdManglePropertyName = BeanUtil.stdManglePropertyName(simpleName, 2)) != null && !stdManglePropertyName.equals(simpleName)) {
                return PropertyName.construct(stdManglePropertyName);
            }
        }
        return null;
    }

    @Override // com.fasterxml.jackson.databind.AnnotationIntrospector
    public boolean hasCreatorAnnotation(Annotated annotated) {
        Intrinsics.checkNotNullParameter(annotated, "member");
        if (!(annotated instanceof AnnotatedConstructor)) {
            return false;
        }
        AnnotatedConstructor annotatedConstructor = (AnnotatedConstructor) annotated;
        Class<?> declaringClass = annotatedConstructor.getDeclaringClass();
        Intrinsics.checkNotNullExpressionValue(declaringClass, "member.declaringClass");
        if (declaringClass.isEnum() || annotatedConstructor.getParameterCount() <= 0) {
            return false;
        }
        Class<?> declaringClass2 = annotatedConstructor.getDeclaringClass();
        Intrinsics.checkNotNullExpressionValue(declaringClass2, "member.declaringClass");
        if (KotlinModuleKt.isKotlinClass(declaringClass2)) {
            return this.cache.checkConstructorIsCreatorAnnotated(annotatedConstructor, new Function1<AnnotatedConstructor, Boolean>(this, annotated) { // from class: com.fasterxml.jackson.module.kotlin.KotlinNamesAnnotationIntrospector$hasCreatorAnnotation$1
                final /* synthetic */ Annotated $member;
                final /* synthetic */ KotlinNamesAnnotationIntrospector this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                    this.$member = r2;
                }

                /* Return type fixed from 'java.lang.Object' to match base method */
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Boolean invoke(AnnotatedConstructor annotatedConstructor2) {
                    return Boolean.valueOf(invoke(annotatedConstructor2));
                }

                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final boolean invoke(com.fasterxml.jackson.databind.introspect.AnnotatedConstructor r12) {
                    /*
                    // Method dump skipped, instructions count: 513
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.module.kotlin.KotlinNamesAnnotationIntrospector$hasCreatorAnnotation$1.invoke(com.fasterxml.jackson.databind.introspect.AnnotatedConstructor):boolean");
                }
            });
        }
        return false;
    }

    private final String findKotlinParameterName(AnnotatedParameter annotatedParameter) {
        List<KParameter> parameters;
        KParameter kParameter;
        List<KParameter> parameters2;
        List<KParameter> parameters3;
        KParameter kParameter2;
        KFunction kotlinFunction;
        List<KParameter> parameters4;
        KParameter kParameter3;
        List<KParameter> parameters5;
        Class<?> declaringClass = annotatedParameter.getDeclaringClass();
        Intrinsics.checkNotNullExpressionValue(declaringClass, "param.declaringClass");
        if (!KotlinModuleKt.isKotlinClass(declaringClass)) {
            return null;
        }
        AnnotatedWithParams owner = annotatedParameter.getOwner();
        Intrinsics.checkNotNullExpressionValue(owner, "param.owner");
        Member member = owner.getMember();
        int i = 0;
        if (member instanceof Constructor) {
            Constructor constructor = (Constructor) member;
            int length = constructor.getParameterTypes().length;
            try {
                KFunction kotlinFunction2 = ReflectJvmMapping.getKotlinFunction(constructor);
                if (!(kotlinFunction2 == null || (parameters5 = kotlinFunction2.getParameters()) == null)) {
                    i = parameters5.size();
                }
            } catch (UnsupportedOperationException | KotlinReflectionInternalError unused) {
            }
            if (i <= 0 || i != length || (kotlinFunction = ReflectJvmMapping.getKotlinFunction(constructor)) == null || (parameters4 = kotlinFunction.getParameters()) == null || (kParameter3 = parameters4.get(annotatedParameter.getIndex())) == null) {
                return null;
            }
            return kParameter3.getName();
        } else if (!(member instanceof Method)) {
            return null;
        } else {
            try {
                KFunction<?> kotlinFunction3 = ReflectJvmMapping.getKotlinFunction((Method) member);
                int index = ((kotlinFunction3 == null || (parameters3 = kotlinFunction3.getParameters()) == null || (kParameter2 = (KParameter) CollectionsKt___CollectionsKt.firstOrNull((List) ((List<? extends Object>) parameters3))) == null) ? null : kParameter2.getKind()) != KParameter.Kind.VALUE ? annotatedParameter.getIndex() + 1 : annotatedParameter.getIndex();
                if (!(kotlinFunction3 == null || (parameters2 = kotlinFunction3.getParameters()) == null)) {
                    i = parameters2.size();
                }
                if (i <= index || kotlinFunction3 == null || (parameters = kotlinFunction3.getParameters()) == null || (kParameter = parameters.get(index)) == null) {
                    return null;
                }
                return kParameter.getName();
            } catch (KotlinReflectionInternalError unused2) {
                return null;
            }
        }
    }
}
