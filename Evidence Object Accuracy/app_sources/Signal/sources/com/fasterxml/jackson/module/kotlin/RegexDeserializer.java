package com.fasterxml.jackson.module.kotlin;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.util.Iterator;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.sequences.SequencesKt__SequencesKt;
import kotlin.sequences.SequencesKt___SequencesKt;
import kotlin.text.Regex;

/* compiled from: KotlinDeserializers.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0003J\u0018\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"}, d2 = {"Lcom/fasterxml/jackson/module/kotlin/RegexDeserializer;", "Lcom/fasterxml/jackson/databind/deser/std/StdDeserializer;", "Lkotlin/text/Regex;", "()V", "deserialize", "p", "Lcom/fasterxml/jackson/core/JsonParser;", "ctxt", "Lcom/fasterxml/jackson/databind/DeserializationContext;", "jackson-module-kotlin"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes.dex */
public final class RegexDeserializer extends StdDeserializer<Regex> {
    public static final RegexDeserializer INSTANCE = new RegexDeserializer();

    private RegexDeserializer() {
        super(Regex.class);
    }

    public Regex deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        Set set;
        Intrinsics.checkNotNullParameter(jsonParser, "p");
        Intrinsics.checkNotNullParameter(deserializationContext, "ctxt");
        JsonNode readTree = deserializationContext.readTree(jsonParser);
        Intrinsics.checkNotNullExpressionValue(readTree, "node");
        if (readTree.isTextual()) {
            String asText = readTree.asText();
            Intrinsics.checkNotNullExpressionValue(asText, "node.asText()");
            return new Regex(asText);
        } else if (readTree.isObject()) {
            String asText2 = readTree.get("pattern").asText();
            if (readTree.has("options")) {
                JsonNode jsonNode = readTree.get("options");
                Intrinsics.checkNotNullExpressionValue(jsonNode, "optionsNode");
                if (jsonNode.isArray()) {
                    Iterator<JsonNode> elements = jsonNode.elements();
                    Intrinsics.checkNotNullExpressionValue(elements, "optionsNode.elements()");
                    set = SequencesKt___SequencesKt.toSet(SequencesKt___SequencesKt.map(SequencesKt__SequencesKt.asSequence(elements), RegexDeserializer$deserialize$options$1.INSTANCE));
                } else {
                    throw new IllegalStateException("Expected an array of strings for RegexOptions, but type was " + readTree.getNodeType());
                }
            } else {
                set = SetsKt__SetsKt.emptySet();
            }
            Intrinsics.checkNotNullExpressionValue(asText2, "pattern");
            return new Regex(asText2, set);
        } else {
            throw new IllegalStateException("Expected a string or an object to deserialize a Regex, but type was " + readTree.getNodeType());
        }
    }
}
