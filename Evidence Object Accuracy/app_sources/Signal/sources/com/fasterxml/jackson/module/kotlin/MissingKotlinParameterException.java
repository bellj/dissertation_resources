package com.fasterxml.jackson.module.kotlin;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.reflect.KParameter;

/* compiled from: Exceptions.kt */
@Metadata(bv = {}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0007\u0012\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\u000b\u0010\fR\u0017\u0010\u0003\u001a\u00020\u00028\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006¨\u0006\r"}, d2 = {"Lcom/fasterxml/jackson/module/kotlin/MissingKotlinParameterException;", "Lcom/fasterxml/jackson/databind/exc/MismatchedInputException;", "Lkotlin/reflect/KParameter;", "parameter", "Lkotlin/reflect/KParameter;", "getParameter", "()Lkotlin/reflect/KParameter;", "Lcom/fasterxml/jackson/core/JsonParser;", "processor", "", "msg", "<init>", "(Lkotlin/reflect/KParameter;Lcom/fasterxml/jackson/core/JsonParser;Ljava/lang/String;)V", "jackson-module-kotlin"}, k = 1, mv = {1, 4, 0})
/* loaded from: classes.dex */
public final class MissingKotlinParameterException extends MismatchedInputException {
    private final KParameter parameter;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public MissingKotlinParameterException(KParameter kParameter, JsonParser jsonParser, String str) {
        super(jsonParser, str);
        Intrinsics.checkNotNullParameter(kParameter, "parameter");
        Intrinsics.checkNotNullParameter(str, "msg");
        this.parameter = kParameter;
    }
}
