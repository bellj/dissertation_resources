package com.fasterxml.jackson.module.kotlin;

import com.fasterxml.jackson.module.kotlin.KotlinModule;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

/* compiled from: Extensions.kt */
@Metadata(bv = {1, 0, 3}, d1 = {"\u0000\f\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\n¢\u0006\u0002\b\u0003"}, d2 = {"<anonymous>", "", "Lcom/fasterxml/jackson/module/kotlin/KotlinModule$Builder;", "invoke"}, k = 3, mv = {1, 4, 0})
/* loaded from: classes.dex */
public final class ExtensionsKt$kotlinModule$1 extends Lambda implements Function1<KotlinModule.Builder, Unit> {
    public static final ExtensionsKt$kotlinModule$1 INSTANCE = new ExtensionsKt$kotlinModule$1();

    ExtensionsKt$kotlinModule$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(KotlinModule.Builder builder) {
        invoke(builder);
        return Unit.INSTANCE;
    }

    public final void invoke(KotlinModule.Builder builder) {
        Intrinsics.checkNotNullParameter(builder, "$receiver");
    }
}
