package com.fasterxml.jackson.module.kotlin;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.core.util.VersionUtil;

/* loaded from: classes.dex */
public final class PackageVersion {
    public static final Version VERSION = VersionUtil.parseVersion("2.12.0", "com.fasterxml.jackson.module", "jackson-module-kotlin");
}
