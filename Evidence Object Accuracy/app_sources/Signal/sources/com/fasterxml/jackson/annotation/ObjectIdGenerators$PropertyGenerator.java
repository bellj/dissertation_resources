package com.fasterxml.jackson.annotation;

/* loaded from: classes.dex */
public abstract class ObjectIdGenerators$PropertyGenerator extends ObjectIdGenerators$Base<Object> {
    @Override // com.fasterxml.jackson.annotation.ObjectIdGenerators$Base, com.fasterxml.jackson.annotation.ObjectIdGenerator
    public /* bridge */ /* synthetic */ Class getScope() {
        return super.getScope();
    }

    @Override // com.fasterxml.jackson.annotation.ObjectIdGenerators$Base, com.fasterxml.jackson.annotation.ObjectIdGenerator
    public /* bridge */ /* synthetic */ boolean canUseFor(ObjectIdGenerator objectIdGenerator) {
        return super.canUseFor(objectIdGenerator);
    }

    public ObjectIdGenerators$PropertyGenerator(Class<?> cls) {
        super(cls);
    }
}
