package com.fasterxml.jackson.databind.jsonFormatVisitors;

import com.fasterxml.jackson.annotation.JsonValue;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;

/* loaded from: classes.dex */
public enum JsonValueFormat {
    COLOR(NotificationProfileDatabase.NotificationProfileTable.COLOR),
    DATE("date"),
    DATE_TIME("date-time"),
    EMAIL(RecipientDatabase.EMAIL),
    HOST_NAME("host-name"),
    IP_ADDRESS("ip-address"),
    IPV6("ipv6"),
    PHONE(RecipientDatabase.PHONE),
    REGEX("regex"),
    STYLE("style"),
    TIME("time"),
    URI("uri"),
    UTC_MILLISEC("utc-millisec"),
    UUID(RecipientDatabase.SERVICE_ID);
    
    private final String _desc;

    JsonValueFormat(String str) {
        this._desc = str;
    }

    @Override // java.lang.Enum, java.lang.Object
    @JsonValue
    public String toString() {
        return this._desc;
    }
}
