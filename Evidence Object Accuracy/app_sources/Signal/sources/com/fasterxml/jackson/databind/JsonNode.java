package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.databind.JsonSerializable;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.util.ClassUtil;
import java.util.Iterator;
import java.util.Map;

/* loaded from: classes.dex */
public abstract class JsonNode extends JsonSerializable.Base implements Iterable<JsonNode> {
    public int asInt(int i) {
        return i;
    }

    public abstract String asText();

    public JsonNode get(String str) {
        return null;
    }

    public abstract JsonNodeType getNodeType();

    public boolean isArray() {
        return false;
    }

    public boolean isObject() {
        return false;
    }

    public int size() {
        return 0;
    }

    public String textValue() {
        return null;
    }

    public final boolean isTextual() {
        return getNodeType() == JsonNodeType.STRING;
    }

    public int asInt() {
        return asInt(0);
    }

    public boolean has(String str) {
        return get(str) != null;
    }

    @Override // java.lang.Iterable
    public final Iterator<JsonNode> iterator() {
        return elements();
    }

    public Iterator<JsonNode> elements() {
        return ClassUtil.emptyIterator();
    }

    public Iterator<Map.Entry<String, JsonNode>> fields() {
        return ClassUtil.emptyIterator();
    }
}
