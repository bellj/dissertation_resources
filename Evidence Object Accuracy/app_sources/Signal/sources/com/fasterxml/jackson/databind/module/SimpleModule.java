package com.fasterxml.jackson.databind.module;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.deser.BeanDeserializerModifier;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.databind.ser.BeanSerializerModifier;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;

/* loaded from: classes.dex */
public class SimpleModule extends Module implements Serializable {
    protected BeanDeserializerModifier _deserializerModifier = null;
    protected HashMap<Class<?>, Class<?>> _mixins = null;
    protected final String _name;
    protected PropertyNamingStrategy _namingStrategy = null;
    protected BeanSerializerModifier _serializerModifier = null;
    protected LinkedHashSet<NamedType> _subtypes = null;
    protected final Version _version;

    public SimpleModule() {
        String str;
        if (getClass() == SimpleModule.class) {
            str = "SimpleModule-" + System.identityHashCode(this);
        } else {
            str = getClass().getName();
        }
        this._name = str;
        this._version = Version.unknownVersion();
    }

    public SimpleModule(Version version) {
        this._name = version.getArtifactId();
        this._version = version;
    }

    @Override // com.fasterxml.jackson.databind.Module
    public Object getTypeId() {
        if (getClass() == SimpleModule.class) {
            return null;
        }
        return super.getTypeId();
    }

    @Override // com.fasterxml.jackson.databind.Module
    public String getModuleName() {
        return this._name;
    }

    @Override // com.fasterxml.jackson.databind.Module
    public void setupModule(Module.SetupContext setupContext) {
        BeanDeserializerModifier beanDeserializerModifier = this._deserializerModifier;
        if (beanDeserializerModifier != null) {
            setupContext.addBeanDeserializerModifier(beanDeserializerModifier);
        }
        BeanSerializerModifier beanSerializerModifier = this._serializerModifier;
        if (beanSerializerModifier != null) {
            setupContext.addBeanSerializerModifier(beanSerializerModifier);
        }
        LinkedHashSet<NamedType> linkedHashSet = this._subtypes;
        if (linkedHashSet != null && linkedHashSet.size() > 0) {
            LinkedHashSet<NamedType> linkedHashSet2 = this._subtypes;
            setupContext.registerSubtypes((NamedType[]) linkedHashSet2.toArray(new NamedType[linkedHashSet2.size()]));
        }
        PropertyNamingStrategy propertyNamingStrategy = this._namingStrategy;
        if (propertyNamingStrategy != null) {
            setupContext.setNamingStrategy(propertyNamingStrategy);
        }
        HashMap<Class<?>, Class<?>> hashMap = this._mixins;
        if (hashMap != null) {
            for (Map.Entry<Class<?>, Class<?>> entry : hashMap.entrySet()) {
                setupContext.setMixInAnnotations(entry.getKey(), entry.getValue());
            }
        }
    }

    @Override // com.fasterxml.jackson.databind.Module
    public Version version() {
        return this._version;
    }
}
