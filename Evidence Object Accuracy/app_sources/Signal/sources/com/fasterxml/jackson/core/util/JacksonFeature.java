package com.fasterxml.jackson.core.util;

/* loaded from: classes.dex */
public interface JacksonFeature {
    boolean enabledByDefault();

    int getMask();
}
