package com.airbnb.lottie.animation.content;

import java.util.ListIterator;

/* access modifiers changed from: package-private */
/* loaded from: classes.dex */
public interface GreedyContent {
    void absorbContent(ListIterator<Content> listIterator);
}
