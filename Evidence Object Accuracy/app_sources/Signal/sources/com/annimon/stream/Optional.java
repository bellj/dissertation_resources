package com.annimon.stream;

import com.annimon.stream.function.Function;
import com.annimon.stream.function.Supplier;

/* loaded from: classes.dex */
public class Optional<T> {
    private static final Optional<?> EMPTY = new Optional<>();
    private final T value;

    public static <T> Optional<T> of(T t) {
        return new Optional<>(t);
    }

    public static <T> Optional<T> ofNullable(T t) {
        return t == null ? empty() : of(t);
    }

    public static <T> Optional<T> empty() {
        return (Optional<T>) EMPTY;
    }

    private Optional() {
        this.value = null;
    }

    private Optional(T t) {
        this.value = (T) Objects.requireNonNull(t);
    }

    public boolean isPresent() {
        return this.value != null;
    }

    /* JADX DEBUG: Type inference failed for r0v1. Raw type applied. Possible types: T, ? super T */
    public <U> Optional<U> map(Function<? super T, ? extends U> function) {
        if (!isPresent()) {
            return empty();
        }
        return ofNullable(function.apply((T) this.value));
    }

    public T orElse(T t) {
        T t2 = this.value;
        return t2 != null ? t2 : t;
    }

    public <X extends Throwable> T orElseThrow(Supplier<? extends X> supplier) throws Throwable {
        T t = this.value;
        if (t != null) {
            return t;
        }
        throw ((Throwable) supplier.get());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Optional)) {
            return false;
        }
        return Objects.equals(this.value, ((Optional) obj).value);
    }

    public int hashCode() {
        return Objects.hashCode(this.value);
    }

    public String toString() {
        T t = this.value;
        return t != null ? String.format("Optional[%s]", t) : "Optional.empty";
    }
}
