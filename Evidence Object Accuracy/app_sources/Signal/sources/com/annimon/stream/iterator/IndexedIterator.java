package com.annimon.stream.iterator;

import j$.util.Iterator;
import j$.util.function.Consumer;
import java.util.Iterator;

/* loaded from: classes.dex */
public class IndexedIterator<T> implements Iterator<T>, j$.util.Iterator {
    private int index;
    private final Iterator<? extends T> iterator;
    private final int step;

    @Override // j$.util.Iterator
    public /* synthetic */ void forEachRemaining(Consumer consumer) {
        Iterator.CC.$default$forEachRemaining(this, consumer);
    }

    @Override // java.util.Iterator
    public /* synthetic */ void forEachRemaining(java.util.function.Consumer consumer) {
        forEachRemaining(Consumer.VivifiedWrapper.convert(consumer));
    }

    public IndexedIterator(int i, int i2, java.util.Iterator<? extends T> it) {
        this.iterator = it;
        this.step = i2;
        this.index = i;
    }

    public int getIndex() {
        return this.index;
    }

    @Override // java.util.Iterator, j$.util.Iterator
    public boolean hasNext() {
        return this.iterator.hasNext();
    }

    @Override // java.util.Iterator, j$.util.Iterator
    public T next() {
        T t = (T) this.iterator.next();
        this.index += this.step;
        return t;
    }

    @Override // java.util.Iterator, j$.util.Iterator
    public void remove() {
        this.iterator.remove();
    }
}
