package com.annimon.stream.iterator;

import j$.util.Iterator;
import j$.util.function.Consumer;
import java.util.Iterator;
import java.util.NoSuchElementException;

/* loaded from: classes.dex */
public abstract class LsaExtIterator<T> implements Iterator<T>, j$.util.Iterator {
    protected boolean hasNext;
    protected boolean isInit;
    protected T next;

    @Override // j$.util.Iterator
    public /* synthetic */ void forEachRemaining(Consumer consumer) {
        Iterator.CC.$default$forEachRemaining(this, consumer);
    }

    @Override // java.util.Iterator
    public /* synthetic */ void forEachRemaining(java.util.function.Consumer consumer) {
        forEachRemaining(Consumer.VivifiedWrapper.convert(consumer));
    }

    protected abstract void nextIteration();

    @Override // java.util.Iterator, j$.util.Iterator
    public boolean hasNext() {
        if (!this.isInit) {
            nextIteration();
            this.isInit = true;
        }
        return this.hasNext;
    }

    @Override // java.util.Iterator, j$.util.Iterator
    public T next() {
        if (!this.isInit) {
            hasNext();
        }
        if (this.hasNext) {
            T t = this.next;
            nextIteration();
            if (!this.hasNext) {
                this.next = null;
            }
            return t;
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator, j$.util.Iterator
    public void remove() {
        throw new UnsupportedOperationException("remove not supported");
    }
}
