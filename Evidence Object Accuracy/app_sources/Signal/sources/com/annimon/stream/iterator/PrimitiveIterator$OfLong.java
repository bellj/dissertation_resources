package com.annimon.stream.iterator;

import j$.util.Iterator;
import j$.util.function.Consumer;
import java.util.Iterator;

/* loaded from: classes.dex */
public abstract class PrimitiveIterator$OfLong implements Iterator<Long>, j$.util.Iterator {
    @Override // j$.util.Iterator
    public /* synthetic */ void forEachRemaining(Consumer consumer) {
        Iterator.CC.$default$forEachRemaining(this, consumer);
    }

    @Override // java.util.Iterator
    public /* synthetic */ void forEachRemaining(java.util.function.Consumer<? super Long> consumer) {
        forEachRemaining(Consumer.VivifiedWrapper.convert(consumer));
    }

    public abstract long nextLong();

    @Override // java.util.Iterator, j$.util.Iterator
    public Long next() {
        return Long.valueOf(nextLong());
    }

    @Override // java.util.Iterator, j$.util.Iterator
    public void remove() {
        throw new UnsupportedOperationException("remove");
    }
}
