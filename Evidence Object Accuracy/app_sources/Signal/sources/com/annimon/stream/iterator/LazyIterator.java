package com.annimon.stream.iterator;

import j$.util.Iterator;
import j$.util.function.Consumer;
import java.util.Iterator;

/* loaded from: classes.dex */
public class LazyIterator<T> implements Iterator<T>, j$.util.Iterator {
    private final Iterable<? extends T> iterable;
    private Iterator<? extends T> iterator;

    @Override // j$.util.Iterator
    public /* synthetic */ void forEachRemaining(Consumer consumer) {
        Iterator.CC.$default$forEachRemaining(this, consumer);
    }

    @Override // java.util.Iterator
    public /* synthetic */ void forEachRemaining(java.util.function.Consumer consumer) {
        forEachRemaining(Consumer.VivifiedWrapper.convert(consumer));
    }

    public LazyIterator(Iterable<? extends T> iterable) {
        this.iterable = iterable;
    }

    private void ensureIterator() {
        if (this.iterator == null) {
            this.iterator = this.iterable.iterator();
        }
    }

    @Override // java.util.Iterator, j$.util.Iterator
    public boolean hasNext() {
        ensureIterator();
        return this.iterator.hasNext();
    }

    @Override // java.util.Iterator, j$.util.Iterator
    public T next() {
        ensureIterator();
        return (T) this.iterator.next();
    }

    @Override // java.util.Iterator, j$.util.Iterator
    public void remove() {
        ensureIterator();
        this.iterator.remove();
    }
}
