package com.annimon.stream;

import com.annimon.stream.function.ToIntFunction;
import com.annimon.stream.internal.Operators;
import com.annimon.stream.internal.Params;
import com.annimon.stream.iterator.PrimitiveIterator$OfInt;
import com.annimon.stream.operator.IntArray;
import com.annimon.stream.operator.IntRangeClosed;
import java.io.Closeable;

/* loaded from: classes.dex */
public final class IntStream implements Closeable {
    private static final IntStream EMPTY = new IntStream(new PrimitiveIterator$OfInt() { // from class: com.annimon.stream.IntStream.1
        @Override // java.util.Iterator, j$.util.Iterator
        public boolean hasNext() {
            return false;
        }

        @Override // com.annimon.stream.iterator.PrimitiveIterator$OfInt
        public int nextInt() {
            return 0;
        }
    });
    private static final ToIntFunction<Integer> UNBOX_FUNCTION = new ToIntFunction<Integer>() { // from class: com.annimon.stream.IntStream.5
        public int applyAsInt(Integer num) {
            return num.intValue();
        }
    };
    private final PrimitiveIterator$OfInt iterator;
    private final Params params;

    public static IntStream empty() {
        return EMPTY;
    }

    public static IntStream of(int i) {
        return new IntStream(new IntArray(new int[]{i}));
    }

    public static IntStream rangeClosed(int i, int i2) {
        if (i > i2) {
            return empty();
        }
        if (i == i2) {
            return of(i);
        }
        return new IntStream(new IntRangeClosed(i, i2));
    }

    private IntStream(PrimitiveIterator$OfInt primitiveIterator$OfInt) {
        this(null, primitiveIterator$OfInt);
    }

    public IntStream(Params params, PrimitiveIterator$OfInt primitiveIterator$OfInt) {
        this.params = params;
        this.iterator = primitiveIterator$OfInt;
    }

    public Stream<Integer> boxed() {
        return new Stream<>(this.params, this.iterator);
    }

    public int[] toArray() {
        return Operators.toIntArray(this.iterator);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        Runnable runnable;
        Params params = this.params;
        if (params != null && (runnable = params.closeHandler) != null) {
            runnable.run();
            this.params.closeHandler = null;
        }
    }
}
