package com.annimon.stream;

import j$.util.Comparator;
import j$.util.function.Function;
import j$.util.function.ToDoubleFunction;
import j$.util.function.ToIntFunction;
import j$.util.function.ToLongFunction;
import java.util.Collections;
import java.util.Comparator;

/* loaded from: classes.dex */
public final class ComparatorCompat<T> implements Comparator<T>, j$.util.Comparator {
    private static final ComparatorCompat<Comparable<Object>> NATURAL_ORDER = new ComparatorCompat<>(new Comparator<Comparable<Object>>() { // from class: com.annimon.stream.ComparatorCompat.1
        public int compare(Comparable<Object> comparable, Comparable<Object> comparable2) {
            return comparable.compareTo(comparable2);
        }
    });
    private static final ComparatorCompat<Comparable<Object>> REVERSE_ORDER = new ComparatorCompat<>(Collections.reverseOrder());
    private final Comparator<? super T> comparator;

    @Override // j$.util.Comparator
    public /* synthetic */ Comparator thenComparing(Function function) {
        return Comparator.CC.$default$thenComparing(this, function);
    }

    @Override // j$.util.Comparator
    public /* synthetic */ java.util.Comparator thenComparing(Function function, java.util.Comparator comparator) {
        return Comparator.CC.$default$thenComparing(this, function, comparator);
    }

    public /* synthetic */ java.util.Comparator thenComparing(java.util.function.Function function) {
        return thenComparing(Function.VivifiedWrapper.convert(function));
    }

    public /* synthetic */ java.util.Comparator thenComparing(java.util.function.Function function, java.util.Comparator comparator) {
        return thenComparing(Function.VivifiedWrapper.convert(function), comparator);
    }

    public /* synthetic */ java.util.Comparator thenComparingDouble(ToDoubleFunction toDoubleFunction) {
        return Comparator.CC.$default$thenComparingDouble(this, toDoubleFunction);
    }

    public /* synthetic */ java.util.Comparator thenComparingDouble(java.util.function.ToDoubleFunction toDoubleFunction) {
        return thenComparingDouble(ToDoubleFunction.VivifiedWrapper.convert(toDoubleFunction));
    }

    public /* synthetic */ java.util.Comparator thenComparingInt(ToIntFunction toIntFunction) {
        return Comparator.CC.$default$thenComparingInt(this, toIntFunction);
    }

    public /* synthetic */ java.util.Comparator thenComparingInt(java.util.function.ToIntFunction toIntFunction) {
        return thenComparingInt(ToIntFunction.VivifiedWrapper.convert(toIntFunction));
    }

    public /* synthetic */ java.util.Comparator thenComparingLong(ToLongFunction toLongFunction) {
        return Comparator.CC.$default$thenComparingLong(this, toLongFunction);
    }

    public /* synthetic */ java.util.Comparator thenComparingLong(java.util.function.ToLongFunction toLongFunction) {
        return thenComparingLong(ToLongFunction.VivifiedWrapper.convert(toLongFunction));
    }

    public static <T> java.util.Comparator<T> reversed(java.util.Comparator<T> comparator) {
        return Collections.reverseOrder(comparator);
    }

    public static <T, U extends Comparable<? super U>> ComparatorCompat<T> comparing(final com.annimon.stream.function.Function<? super T, ? extends U> function) {
        Objects.requireNonNull(function);
        return new ComparatorCompat<>(new java.util.Comparator<T>() { // from class: com.annimon.stream.ComparatorCompat.4
            public int compare(T t, T t2) {
                return ((Comparable) function.apply(t)).compareTo((Comparable) function.apply(t2));
            }
        });
    }

    public static <T> ComparatorCompat<T> chain(java.util.Comparator<T> comparator) {
        return new ComparatorCompat<>(comparator);
    }

    public ComparatorCompat(java.util.Comparator<? super T> comparator) {
        this.comparator = comparator;
    }

    @Override // java.util.Comparator, j$.util.Comparator
    public ComparatorCompat<T> reversed() {
        return new ComparatorCompat<>(Collections.reverseOrder(this.comparator));
    }

    @Override // java.util.Comparator, j$.util.Comparator
    public ComparatorCompat<T> thenComparing(final java.util.Comparator<? super T> comparator) {
        Objects.requireNonNull(comparator);
        return new ComparatorCompat<>(new java.util.Comparator<T>() { // from class: com.annimon.stream.ComparatorCompat.9
            public int compare(T t, T t2) {
                int compare = ComparatorCompat.this.comparator.compare(t, t2);
                return compare != 0 ? compare : comparator.compare(t, t2);
            }
        });
    }

    @Override // java.util.Comparator, j$.util.Comparator
    public int compare(T t, T t2) {
        return this.comparator.compare(t, t2);
    }
}
