package com.annimon.stream.operator;

import com.annimon.stream.function.Function;
import com.annimon.stream.iterator.LsaExtIterator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/* loaded from: classes.dex */
public class ObjDistinctBy<T, K> extends LsaExtIterator<T> {
    private final Function<? super T, ? extends K> classifier;
    private final Iterator<? extends T> iterator;
    private final Set<K> set = new HashSet();

    public ObjDistinctBy(Iterator<? extends T> it, Function<? super T, ? extends K> function) {
        this.iterator = it;
        this.classifier = function;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: java.util.Set<K> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.annimon.stream.iterator.LsaExtIterator
    protected void nextIteration() {
        T t;
        do {
            boolean hasNext = this.iterator.hasNext();
            this.hasNext = hasNext;
            if (hasNext) {
                t = (T) this.iterator.next();
                this.next = t;
            } else {
                return;
            }
        } while (!this.set.add(this.classifier.apply(t)));
    }
}
