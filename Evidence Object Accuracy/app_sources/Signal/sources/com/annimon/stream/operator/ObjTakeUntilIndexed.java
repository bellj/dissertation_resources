package com.annimon.stream.operator;

import com.annimon.stream.function.IndexedPredicate;
import com.annimon.stream.iterator.IndexedIterator;
import com.annimon.stream.iterator.LsaExtIterator;

/* loaded from: classes.dex */
public class ObjTakeUntilIndexed<T> extends LsaExtIterator<T> {
    private final IndexedIterator<? extends T> iterator;
    private final IndexedPredicate<? super T> stopPredicate;

    public ObjTakeUntilIndexed(IndexedIterator<? extends T> indexedIterator, IndexedPredicate<? super T> indexedPredicate) {
        this.iterator = indexedIterator;
        this.stopPredicate = indexedPredicate;
    }

    /* JADX DEBUG: Type inference failed for r2v0. Raw type applied. Possible types: T, ? super T */
    @Override // com.annimon.stream.iterator.LsaExtIterator
    protected void nextIteration() {
        boolean z = this.iterator.hasNext() && (!this.isInit || !this.stopPredicate.test(this.iterator.getIndex(), (T) this.next));
        this.hasNext = z;
        if (z) {
            this.next = (T) this.iterator.next();
        }
    }
}
