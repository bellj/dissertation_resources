package com.annimon.stream.operator;

import com.annimon.stream.function.Predicate;
import com.annimon.stream.iterator.LsaExtIterator;
import java.util.Iterator;

/* loaded from: classes.dex */
public class ObjTakeWhile<T> extends LsaExtIterator<T> {
    private final Iterator<? extends T> iterator;
    private final Predicate<? super T> predicate;

    public ObjTakeWhile(Iterator<? extends T> it, Predicate<? super T> predicate) {
        this.iterator = it;
        this.predicate = predicate;
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [T, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // com.annimon.stream.iterator.LsaExtIterator
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected void nextIteration() {
        /*
            r2 = this;
            java.util.Iterator<? extends T> r0 = r2.iterator
            boolean r0 = r0.hasNext()
            if (r0 == 0) goto L_0x001a
            com.annimon.stream.function.Predicate<? super T> r0 = r2.predicate
            java.util.Iterator<? extends T> r1 = r2.iterator
            java.lang.Object r1 = r1.next()
            r2.next = r1
            boolean r0 = r0.test(r1)
            if (r0 == 0) goto L_0x001a
            r0 = 1
            goto L_0x001b
        L_0x001a:
            r0 = 0
        L_0x001b:
            r2.hasNext = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.annimon.stream.operator.ObjTakeWhile.nextIteration():void");
    }
}
