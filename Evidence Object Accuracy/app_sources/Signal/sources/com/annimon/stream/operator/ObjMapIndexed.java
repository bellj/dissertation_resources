package com.annimon.stream.operator;

import com.annimon.stream.function.IndexedFunction;
import com.annimon.stream.iterator.IndexedIterator;
import com.annimon.stream.iterator.LsaIterator;

/* loaded from: classes.dex */
public class ObjMapIndexed<T, R> extends LsaIterator<R> {
    private final IndexedIterator<? extends T> iterator;
    private final IndexedFunction<? super T, ? extends R> mapper;

    public ObjMapIndexed(IndexedIterator<? extends T> indexedIterator, IndexedFunction<? super T, ? extends R> indexedFunction) {
        this.iterator = indexedIterator;
        this.mapper = indexedFunction;
    }

    @Override // java.util.Iterator, j$.util.Iterator
    public boolean hasNext() {
        return this.iterator.hasNext();
    }

    @Override // com.annimon.stream.iterator.LsaIterator
    public R nextIteration() {
        return (R) this.mapper.apply(this.iterator.getIndex(), (Object) this.iterator.next());
    }
}
