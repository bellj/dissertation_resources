package com.annimon.stream.operator;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.iterator.LsaExtIterator;
import java.util.Iterator;

/* loaded from: classes.dex */
public class ObjFlatMap<T, R> extends LsaExtIterator<R> {
    private Iterator<? extends R> inner;
    private Stream<? extends R> innerStream;
    private final Iterator<? extends T> iterator;
    private final Function<? super T, ? extends Stream<? extends R>> mapper;

    public ObjFlatMap(Iterator<? extends T> it, Function<? super T, ? extends Stream<? extends R>> function) {
        this.iterator = it;
        this.mapper = function;
    }

    /* JADX DEBUG: Type inference failed for r2v2. Raw type applied. Possible types: java.util.Iterator<? extends ? extends R>, java.util.Iterator<? extends R> */
    @Override // com.annimon.stream.iterator.LsaExtIterator
    protected void nextIteration() {
        Iterator<? extends R> it = this.inner;
        if (it == null || !it.hasNext()) {
            while (this.iterator.hasNext()) {
                Iterator<? extends R> it2 = this.inner;
                if (it2 == null || !it2.hasNext()) {
                    Stream<? extends R> stream = this.innerStream;
                    if (stream != null) {
                        stream.close();
                        this.innerStream = null;
                    }
                    Stream<? extends R> stream2 = (Stream) this.mapper.apply(this.iterator.next());
                    if (stream2 != null) {
                        this.inner = (Iterator<? extends Object>) stream2.iterator();
                        this.innerStream = stream2;
                    }
                }
                Iterator<? extends R> it3 = this.inner;
                if (it3 != null && it3.hasNext()) {
                    this.next = (T) this.inner.next();
                    this.hasNext = true;
                    return;
                }
            }
            this.hasNext = false;
            Stream<? extends R> stream3 = this.innerStream;
            if (stream3 != null) {
                stream3.close();
                this.innerStream = null;
                return;
            }
            return;
        }
        this.next = (T) this.inner.next();
        this.hasNext = true;
    }
}
