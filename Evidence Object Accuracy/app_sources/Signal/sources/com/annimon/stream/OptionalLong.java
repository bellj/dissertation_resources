package com.annimon.stream;

import com.annimon.stream.function.LongConsumer;
import com.annimon.stream.function.LongFunction;
import com.annimon.stream.function.LongUnaryOperator;
import com.annimon.stream.function.Supplier;
import java.util.NoSuchElementException;

/* loaded from: classes.dex */
public final class OptionalLong {
    private static final OptionalLong EMPTY = new OptionalLong();
    private final boolean isPresent;
    private final long value;

    public static OptionalLong empty() {
        return EMPTY;
    }

    public static OptionalLong of(long j) {
        return new OptionalLong(j);
    }

    private OptionalLong() {
        this.isPresent = false;
        this.value = 0;
    }

    private OptionalLong(long j) {
        this.isPresent = true;
        this.value = j;
    }

    public long getAsLong() {
        if (this.isPresent) {
            return this.value;
        }
        throw new NoSuchElementException("No value present");
    }

    public boolean isPresent() {
        return this.isPresent;
    }

    public void ifPresent(LongConsumer longConsumer) {
        if (this.isPresent) {
            longConsumer.accept(this.value);
        }
    }

    public OptionalLong executeIfPresent(LongConsumer longConsumer) {
        ifPresent(longConsumer);
        return this;
    }

    public OptionalLong map(LongUnaryOperator longUnaryOperator) {
        if (!isPresent()) {
            return empty();
        }
        Objects.requireNonNull(longUnaryOperator);
        return of(longUnaryOperator.applyAsLong(this.value));
    }

    public <U> Optional<U> mapToObj(LongFunction<U> longFunction) {
        if (!isPresent()) {
            return Optional.empty();
        }
        Objects.requireNonNull(longFunction);
        return Optional.ofNullable(longFunction.apply(this.value));
    }

    public OptionalLong or(Supplier<OptionalLong> supplier) {
        if (isPresent()) {
            return this;
        }
        Objects.requireNonNull(supplier);
        return (OptionalLong) Objects.requireNonNull(supplier.get());
    }

    public long orElse(long j) {
        return this.isPresent ? this.value : j;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof OptionalLong)) {
            return false;
        }
        OptionalLong optionalLong = (OptionalLong) obj;
        boolean z = this.isPresent;
        if (!z || !optionalLong.isPresent) {
            if (z == optionalLong.isPresent) {
                return true;
            }
        } else if (this.value == optionalLong.value) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.isPresent) {
            return Objects.hashCode(Long.valueOf(this.value));
        }
        return 0;
    }

    public String toString() {
        return this.isPresent ? String.format("OptionalLong[%s]", Long.valueOf(this.value)) : "OptionalLong.empty";
    }
}
