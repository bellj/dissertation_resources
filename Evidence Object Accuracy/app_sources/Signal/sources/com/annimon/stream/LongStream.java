package com.annimon.stream;

import com.annimon.stream.function.LongBinaryOperator;
import com.annimon.stream.function.ToLongFunction;
import com.annimon.stream.internal.Params;
import com.annimon.stream.iterator.PrimitiveIterator$OfLong;
import java.io.Closeable;

/* loaded from: classes.dex */
public final class LongStream implements Closeable {
    private static final LongStream EMPTY = new LongStream(new PrimitiveIterator$OfLong() { // from class: com.annimon.stream.LongStream.1
        @Override // java.util.Iterator, j$.util.Iterator
        public boolean hasNext() {
            return false;
        }

        @Override // com.annimon.stream.iterator.PrimitiveIterator$OfLong
        public long nextLong() {
            return 0;
        }
    });
    private static final ToLongFunction<Long> UNBOX_FUNCTION = new ToLongFunction<Long>() { // from class: com.annimon.stream.LongStream.5
        public long applyAsLong(Long l) {
            return l.longValue();
        }
    };
    private final PrimitiveIterator$OfLong iterator;
    private final Params params;

    private LongStream(PrimitiveIterator$OfLong primitiveIterator$OfLong) {
        this(null, primitiveIterator$OfLong);
    }

    public LongStream(Params params, PrimitiveIterator$OfLong primitiveIterator$OfLong) {
        this.params = params;
        this.iterator = primitiveIterator$OfLong;
    }

    public OptionalLong reduce(LongBinaryOperator longBinaryOperator) {
        boolean z = false;
        long j = 0;
        while (this.iterator.hasNext()) {
            long nextLong = this.iterator.nextLong();
            if (!z) {
                z = true;
                j = nextLong;
            } else {
                j = longBinaryOperator.applyAsLong(j, nextLong);
            }
        }
        return z ? OptionalLong.of(j) : OptionalLong.empty();
    }

    public long sum() {
        long j = 0;
        while (this.iterator.hasNext()) {
            j += this.iterator.nextLong();
        }
        return j;
    }

    public OptionalLong max() {
        return reduce(new LongBinaryOperator() { // from class: com.annimon.stream.LongStream.3
            @Override // com.annimon.stream.function.LongBinaryOperator
            public long applyAsLong(long j, long j2) {
                return Math.max(j, j2);
            }
        });
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        Runnable runnable;
        Params params = this.params;
        if (params != null && (runnable = params.closeHandler) != null) {
            runnable.run();
            this.params.closeHandler = null;
        }
    }
}
