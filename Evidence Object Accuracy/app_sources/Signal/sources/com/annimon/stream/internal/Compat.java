package com.annimon.stream.internal;

import java.lang.reflect.Array;
import java.util.Arrays;

/* loaded from: classes.dex */
public final class Compat {
    @SafeVarargs
    public static <E> E[] newArray(int i, E... eArr) {
        try {
            return (E[]) Arrays.copyOf(eArr, i);
        } catch (NoSuchMethodError unused) {
            return (E[]) newArrayCompat(eArr, i);
        }
    }

    public static <E> E[] newArrayCompat(E[] eArr, int i) {
        E[] eArr2 = (E[]) ((Object[]) Array.newInstance(eArr.getClass().getComponentType(), i));
        System.arraycopy(eArr, 0, eArr2, 0, Math.min(i, eArr.length));
        return eArr2;
    }

    public static void checkMaxArraySize(long j) {
        if (j >= 2147483639) {
            throw new IllegalArgumentException("Stream size exceeds max array size");
        }
    }
}
