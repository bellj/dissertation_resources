package com.annimon.stream.internal;

import java.util.Arrays;

/* loaded from: classes.dex */
public abstract class SpinedBuffer$OfPrimitive<E, T_ARR, T_CONS> implements Iterable<E> {
    T_ARR curChunk = newArray(1 << 4);
    int elementIndex;
    final int initialChunkPower = 4;
    long[] priorElementCount;
    T_ARR[] spine;
    int spineIndex;

    protected abstract int arrayLength(T_ARR t_arr);

    protected abstract T_ARR newArray(int i);

    protected abstract T_ARR[] newArrayArray(int i);

    public long count() {
        int i = this.spineIndex;
        return i == 0 ? (long) this.elementIndex : this.priorElementCount[i] + ((long) this.elementIndex);
    }

    int chunkSize(int i) {
        int i2;
        if (i == 0 || i == 1) {
            i2 = this.initialChunkPower;
        } else {
            i2 = Math.min((this.initialChunkPower + i) - 1, 30);
        }
        return 1 << i2;
    }

    long capacity() {
        int i = this.spineIndex;
        if (i == 0) {
            return (long) arrayLength(this.curChunk);
        }
        return ((long) arrayLength(this.spine[i])) + this.priorElementCount[i];
    }

    private void inflateSpine() {
        if (this.spine == null) {
            T_ARR[] newArrayArray = newArrayArray(8);
            this.spine = newArrayArray;
            this.priorElementCount = new long[8];
            newArrayArray[0] = this.curChunk;
        }
    }

    final void ensureCapacity(long j) {
        long capacity = capacity();
        if (j > capacity) {
            inflateSpine();
            int i = this.spineIndex;
            while (true) {
                i++;
                if (j > capacity) {
                    T_ARR[] t_arrArr = this.spine;
                    if (i >= t_arrArr.length) {
                        int length = t_arrArr.length * 2;
                        this.spine = (T_ARR[]) Arrays.copyOf(t_arrArr, length);
                        this.priorElementCount = Arrays.copyOf(this.priorElementCount, length);
                    }
                    int chunkSize = chunkSize(i);
                    this.spine[i] = newArray(chunkSize);
                    long[] jArr = this.priorElementCount;
                    int i2 = i - 1;
                    jArr[i] = jArr[i2] + ((long) arrayLength(this.spine[i2]));
                    capacity += (long) chunkSize;
                } else {
                    return;
                }
            }
        }
    }

    void increaseCapacity() {
        ensureCapacity(capacity() + 1);
    }

    public int chunkFor(long j) {
        if (this.spineIndex == 0) {
            if (j < ((long) this.elementIndex)) {
                return 0;
            }
            throw new IndexOutOfBoundsException(Long.toString(j));
        } else if (j < count()) {
            for (int i = 0; i <= this.spineIndex; i++) {
                if (j < this.priorElementCount[i] + ((long) arrayLength(this.spine[i]))) {
                    return i;
                }
            }
            throw new IndexOutOfBoundsException(Long.toString(j));
        } else {
            throw new IndexOutOfBoundsException(Long.toString(j));
        }
    }

    void copyInto(T_ARR t_arr, int i) {
        long j = (long) i;
        long count = count() + j;
        if (count > ((long) arrayLength(t_arr)) || count < j) {
            throw new IndexOutOfBoundsException("does not fit");
        } else if (this.spineIndex == 0) {
            System.arraycopy(this.curChunk, 0, t_arr, i, this.elementIndex);
        } else {
            for (int i2 = 0; i2 < this.spineIndex; i2++) {
                T_ARR t_arr2 = this.spine[i2];
                System.arraycopy(t_arr2, 0, t_arr, i, arrayLength(t_arr2));
                i += arrayLength(this.spine[i2]);
            }
            int i3 = this.elementIndex;
            if (i3 > 0) {
                System.arraycopy(this.curChunk, 0, t_arr, i, i3);
            }
        }
    }

    public T_ARR asPrimitiveArray() {
        long count = count();
        Compat.checkMaxArraySize(count);
        T_ARR newArray = newArray((int) count);
        copyInto(newArray, 0);
        return newArray;
    }

    public void preAccept() {
        if (this.elementIndex == arrayLength(this.curChunk)) {
            inflateSpine();
            int i = this.spineIndex;
            int i2 = i + 1;
            T_ARR[] t_arrArr = this.spine;
            if (i2 >= t_arrArr.length || t_arrArr[i + 1] == null) {
                increaseCapacity();
            }
            this.elementIndex = 0;
            int i3 = this.spineIndex + 1;
            this.spineIndex = i3;
            this.curChunk = this.spine[i3];
        }
    }
}
