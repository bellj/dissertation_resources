package com.annimon.stream.internal;

import com.annimon.stream.function.IntFunction;
import com.annimon.stream.iterator.PrimitiveIterator$OfInt;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes.dex */
public final class Operators {
    public static <T> List<T> toList(Iterator<? extends T> it) {
        ArrayList arrayList = new ArrayList();
        while (it.hasNext()) {
            arrayList.add(it.next());
        }
        return arrayList;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: java.util.List */
    /* JADX WARN: Multi-variable type inference failed */
    public static <T, R> R[] toArray(Iterator<? extends T> it, IntFunction<R[]> intFunction) {
        List list = toList(it);
        int size = list.size();
        Compat.checkMaxArraySize((long) size);
        Object[] array = list.toArray(Compat.newArray(size, new Object[0]));
        R[] apply = intFunction.apply(size);
        System.arraycopy(array, 0, apply, 0, size);
        return apply;
    }

    public static int[] toIntArray(PrimitiveIterator$OfInt primitiveIterator$OfInt) {
        SpinedBuffer$OfInt spinedBuffer$OfInt = new SpinedBuffer$OfInt();
        while (primitiveIterator$OfInt.hasNext()) {
            spinedBuffer$OfInt.accept(primitiveIterator$OfInt.nextInt());
        }
        return spinedBuffer$OfInt.asPrimitiveArray();
    }
}
