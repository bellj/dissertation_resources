package com.annimon.stream.internal;

import com.annimon.stream.iterator.PrimitiveIterator$OfInt;

/* loaded from: classes.dex */
public class SpinedBuffer$OfInt extends SpinedBuffer$OfPrimitive<Integer, int[], Object> {
    @Override // com.annimon.stream.internal.SpinedBuffer$OfPrimitive
    public int[][] newArrayArray(int i) {
        return new int[i];
    }

    @Override // com.annimon.stream.internal.SpinedBuffer$OfPrimitive
    public int[] newArray(int i) {
        return new int[i];
    }

    public int arrayLength(int[] iArr) {
        return iArr.length;
    }

    public void accept(int i) {
        preAccept();
        int i2 = this.elementIndex;
        this.elementIndex = i2 + 1;
        ((int[]) this.curChunk)[i2] = i;
    }

    public int get(long j) {
        int chunkFor = chunkFor(j);
        if (this.spineIndex == 0 && chunkFor == 0) {
            return ((int[]) this.curChunk)[(int) j];
        }
        return ((int[][]) this.spine)[chunkFor][(int) (j - this.priorElementCount[chunkFor])];
    }

    @Override // java.lang.Iterable
    public PrimitiveIterator$OfInt iterator() {
        return new PrimitiveIterator$OfInt() { // from class: com.annimon.stream.internal.SpinedBuffer$OfInt.1
            long index = 0;

            @Override // com.annimon.stream.iterator.PrimitiveIterator$OfInt
            public int nextInt() {
                SpinedBuffer$OfInt spinedBuffer$OfInt = SpinedBuffer$OfInt.this;
                long j = this.index;
                this.index = 1 + j;
                return spinedBuffer$OfInt.get(j);
            }

            @Override // java.util.Iterator, j$.util.Iterator
            public boolean hasNext() {
                return this.index < SpinedBuffer$OfInt.this.count();
            }
        };
    }
}
