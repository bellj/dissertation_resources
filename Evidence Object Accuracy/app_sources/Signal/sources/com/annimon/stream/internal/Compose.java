package com.annimon.stream.internal;

import java.io.Closeable;

/* loaded from: classes.dex */
public final class Compose {
    public static Runnable runnables(final Runnable runnable, final Runnable runnable2) {
        return new Runnable() { // from class: com.annimon.stream.internal.Compose.1
            @Override // java.lang.Runnable
            public void run() {
                try {
                    runnable.run();
                    runnable2.run();
                } catch (Throwable th) {
                    try {
                        runnable2.run();
                    } catch (Throwable unused) {
                    }
                    if (th instanceof RuntimeException) {
                        throw th;
                    }
                    throw ((Error) th);
                }
            }
        };
    }

    public static Runnable closeables(final Closeable closeable, final Closeable closeable2) {
        return new Runnable() { // from class: com.annimon.stream.internal.Compose.2
            @Override // java.lang.Runnable
            public void run() {
                try {
                    closeable.close();
                    try {
                        closeable2.close();
                    } catch (Throwable th) {
                        if (th instanceof RuntimeException) {
                            throw ((RuntimeException) th);
                        } else if (th instanceof Error) {
                            throw th;
                        } else {
                            throw new RuntimeException(th);
                        }
                    }
                } catch (Throwable th2) {
                    try {
                        closeable2.close();
                    } catch (Throwable unused) {
                    }
                    if (th2 instanceof RuntimeException) {
                        throw th2;
                    }
                    throw ((Error) th2);
                }
            }
        };
    }
}
