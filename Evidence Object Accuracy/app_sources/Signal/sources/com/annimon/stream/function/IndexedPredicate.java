package com.annimon.stream.function;

/* loaded from: classes.dex */
public interface IndexedPredicate<T> {
    boolean test(int i, T t);
}
