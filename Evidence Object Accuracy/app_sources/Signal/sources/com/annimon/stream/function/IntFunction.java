package com.annimon.stream.function;

/* loaded from: classes.dex */
public interface IntFunction<R> {
    R apply(int i);
}
