package com.annimon.stream.function;

/* loaded from: classes.dex */
public interface IndexedFunction<T, R> {
    R apply(int i, T t);
}
