package com.annimon.stream.function;

/* loaded from: classes.dex */
public interface LongFunction<R> {
    R apply(long j);
}
