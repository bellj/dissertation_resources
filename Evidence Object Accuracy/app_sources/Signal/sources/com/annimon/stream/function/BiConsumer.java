package com.annimon.stream.function;

/* loaded from: classes.dex */
public interface BiConsumer<T, U> {
    void accept(T t, U u);
}
