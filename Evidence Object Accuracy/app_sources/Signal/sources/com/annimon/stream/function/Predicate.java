package com.annimon.stream.function;

/* loaded from: classes.dex */
public interface Predicate<T> {
    boolean test(T t);

    /* loaded from: classes.dex */
    public static class Util {
        public static <T> Predicate<T> negate(final Predicate<? super T> predicate) {
            return new Predicate<T>() { // from class: com.annimon.stream.function.Predicate.Util.4
                @Override // com.annimon.stream.function.Predicate
                public boolean test(T t) {
                    return !predicate.test(t);
                }
            };
        }

        public static <T> Predicate<T> notNull() {
            return new Predicate<T>() { // from class: com.annimon.stream.function.Predicate.Util.5
                @Override // com.annimon.stream.function.Predicate
                public boolean test(T t) {
                    return t != null;
                }
            };
        }
    }
}
