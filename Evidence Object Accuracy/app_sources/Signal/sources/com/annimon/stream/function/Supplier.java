package com.annimon.stream.function;

/* loaded from: classes.dex */
public interface Supplier<T> {
    T get();
}
