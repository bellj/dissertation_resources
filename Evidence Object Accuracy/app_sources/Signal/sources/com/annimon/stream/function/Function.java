package com.annimon.stream.function;

/* loaded from: classes.dex */
public interface Function<T, R> {
    R apply(T t);
}
