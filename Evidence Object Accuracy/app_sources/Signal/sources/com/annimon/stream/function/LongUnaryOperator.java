package com.annimon.stream.function;

/* loaded from: classes.dex */
public interface LongUnaryOperator {
    long applyAsLong(long j);
}
