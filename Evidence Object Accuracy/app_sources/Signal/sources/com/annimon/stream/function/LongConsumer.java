package com.annimon.stream.function;

/* loaded from: classes.dex */
public interface LongConsumer {
    void accept(long j);
}
