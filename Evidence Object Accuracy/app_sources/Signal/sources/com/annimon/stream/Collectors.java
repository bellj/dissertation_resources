package com.annimon.stream;

import com.annimon.stream.function.BiConsumer;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Supplier;
import com.annimon.stream.function.ToLongFunction;
import com.annimon.stream.function.UnaryOperator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* loaded from: classes.dex */
public final class Collectors {
    private static final Supplier<double[]> DOUBLE_2ELEMENTS_ARRAY_SUPPLIER = new Supplier<double[]>() { // from class: com.annimon.stream.Collectors.2
        @Override // com.annimon.stream.function.Supplier
        public double[] get() {
            return new double[]{0.0d, 0.0d};
        }
    };
    private static final Supplier<long[]> LONG_2ELEMENTS_ARRAY_SUPPLIER = new Supplier<long[]>() { // from class: com.annimon.stream.Collectors.1
        @Override // com.annimon.stream.function.Supplier
        public long[] get() {
            return new long[]{0, 0};
        }
    };

    public static <T> Collector<T, ?, List<T>> toList() {
        return new CollectorsImpl(new Supplier<List<T>>() { // from class: com.annimon.stream.Collectors.4
            @Override // com.annimon.stream.function.Supplier
            public List<T> get() {
                return new ArrayList();
            }
        }, new BiConsumer<List<T>, T>() { // from class: com.annimon.stream.Collectors.5
            @Override // com.annimon.stream.function.BiConsumer
            public /* bridge */ /* synthetic */ void accept(Object obj, Object obj2) {
                accept((List<List>) ((List) obj), (List) obj2);
            }

            public void accept(List<T> list, T t) {
                list.add(t);
            }
        });
    }

    public static <T> Collector<T, ?, Set<T>> toSet() {
        return new CollectorsImpl(new Supplier<Set<T>>() { // from class: com.annimon.stream.Collectors.6
            @Override // com.annimon.stream.function.Supplier
            public Set<T> get() {
                return new HashSet();
            }
        }, new BiConsumer<Set<T>, T>() { // from class: com.annimon.stream.Collectors.7
            @Override // com.annimon.stream.function.BiConsumer
            public /* bridge */ /* synthetic */ void accept(Object obj, Object obj2) {
                accept((Set<Set>) ((Set) obj), (Set) obj2);
            }

            public void accept(Set<T> set, T t) {
                set.add(t);
            }
        });
    }

    public static <T, K> Collector<T, ?, Map<K, T>> toMap(Function<? super T, ? extends K> function) {
        return toMap(function, UnaryOperator.Util.identity());
    }

    public static <T, K, V> Collector<T, ?, Map<K, V>> toMap(Function<? super T, ? extends K> function, Function<? super T, ? extends V> function2) {
        return toMap(function, function2, hashMapSupplier());
    }

    public static <T, K, V, M extends Map<K, V>> Collector<T, ?, M> toMap(final Function<? super T, ? extends K> function, final Function<? super T, ? extends V> function2, Supplier<M> supplier) {
        return new CollectorsImpl(supplier, new BiConsumer<M, T>() { // from class: com.annimon.stream.Collectors.8
            public void accept(Map map, Object obj) {
                Object apply = function.apply(obj);
                Object apply2 = function2.apply(obj);
                Object obj2 = map.get(apply);
                if (obj2 != null) {
                    apply2 = obj2;
                }
                if (apply2 == null) {
                    map.remove(apply);
                } else {
                    map.put(apply, apply2);
                }
            }
        });
    }

    public static Collector<CharSequence, ?, String> joining() {
        return joining("");
    }

    public static Collector<CharSequence, ?, String> joining(CharSequence charSequence) {
        return joining(charSequence, "", "");
    }

    public static Collector<CharSequence, ?, String> joining(CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3) {
        return joining(charSequence, charSequence2, charSequence3, charSequence2.toString() + charSequence3.toString());
    }

    public static Collector<CharSequence, ?, String> joining(final CharSequence charSequence, final CharSequence charSequence2, final CharSequence charSequence3, final String str) {
        return new CollectorsImpl(new Supplier<StringBuilder>() { // from class: com.annimon.stream.Collectors.9
            @Override // com.annimon.stream.function.Supplier
            public StringBuilder get() {
                return new StringBuilder();
            }
        }, new BiConsumer<StringBuilder, CharSequence>() { // from class: com.annimon.stream.Collectors.10
            public void accept(StringBuilder sb, CharSequence charSequence4) {
                if (sb.length() > 0) {
                    sb.append(charSequence);
                } else {
                    sb.append(charSequence2);
                }
                sb.append(charSequence4);
            }
        }, new Function<StringBuilder, String>() { // from class: com.annimon.stream.Collectors.11
            public String apply(StringBuilder sb) {
                if (sb.length() == 0) {
                    return str;
                }
                sb.append(charSequence3);
                return sb.toString();
            }
        });
    }

    public static <T> Collector<T, ?, Long> summingLong(final ToLongFunction<? super T> toLongFunction) {
        return new CollectorsImpl(LONG_2ELEMENTS_ARRAY_SUPPLIER, new BiConsumer<long[], T>() { // from class: com.annimon.stream.Collectors.21
            @Override // com.annimon.stream.function.BiConsumer
            public /* bridge */ /* synthetic */ void accept(Object obj, Object obj2) {
                accept((long[]) obj, (long[]) obj2);
            }

            public void accept(long[] jArr, T t) {
                jArr[0] = jArr[0] + toLongFunction.applyAsLong(t);
            }
        }, new Function<long[], Long>() { // from class: com.annimon.stream.Collectors.22
            public Long apply(long[] jArr) {
                return Long.valueOf(jArr[0]);
            }
        });
    }

    public static <T, K> Collector<T, ?, Map<K, List<T>>> groupingBy(Function<? super T, ? extends K> function) {
        return groupingBy(function, toList());
    }

    public static <T, K, A, D> Collector<T, ?, Map<K, D>> groupingBy(Function<? super T, ? extends K> function, Collector<? super T, A, D> collector) {
        return groupingBy(function, hashMapSupplier(), collector);
    }

    public static <T, K, D, A, M extends Map<K, D>> Collector<T, ?, M> groupingBy(final Function<? super T, ? extends K> function, Supplier<M> supplier, final Collector<? super T, A, D> collector) {
        final Function<A, D> finisher = collector.finisher();
        return new CollectorsImpl(supplier, new BiConsumer<Map<K, A>, T>() { // from class: com.annimon.stream.Collectors.36
            @Override // com.annimon.stream.function.BiConsumer
            public /* bridge */ /* synthetic */ void accept(Object obj, Object obj2) {
                accept((Map) obj, (Map) obj2);
            }

            public void accept(Map<K, A> map, T t) {
                Object requireNonNull = Objects.requireNonNull(function.apply(t), "element cannot be mapped to a null key");
                Object obj = map.get(requireNonNull);
                if (obj == null) {
                    obj = collector.supplier().get();
                    map.put(requireNonNull, obj);
                }
                collector.accumulator().accept(obj, t);
            }
        }, finisher != null ? new Function<Map<K, A>, M>() { // from class: com.annimon.stream.Collectors.35
            public Map apply(Map map) {
                for (Map.Entry entry : map.entrySet()) {
                    entry.setValue(finisher.apply(entry.getValue()));
                }
                return map;
            }
        } : null);
    }

    private static <K, V> Supplier<Map<K, V>> hashMapSupplier() {
        return new Supplier<Map<K, V>>() { // from class: com.annimon.stream.Collectors.37
            @Override // com.annimon.stream.function.Supplier
            public Map<K, V> get() {
                return new HashMap();
            }
        };
    }

    public static <A, R> Function<A, R> castIdentity() {
        return new Function<A, R>() { // from class: com.annimon.stream.Collectors.38
            /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: A */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.annimon.stream.function.Function
            public R apply(A a) {
                return a;
            }
        };
    }

    /* loaded from: classes.dex */
    public static final class CollectorsImpl<T, A, R> implements Collector<T, A, R> {
        private final BiConsumer<A, T> accumulator;
        private final Function<A, R> finisher;
        private final Supplier<A> supplier;

        public CollectorsImpl(Supplier<A> supplier, BiConsumer<A, T> biConsumer) {
            this(supplier, biConsumer, null);
        }

        public CollectorsImpl(Supplier<A> supplier, BiConsumer<A, T> biConsumer, Function<A, R> function) {
            this.supplier = supplier;
            this.accumulator = biConsumer;
            this.finisher = function;
        }

        @Override // com.annimon.stream.Collector
        public Supplier<A> supplier() {
            return this.supplier;
        }

        @Override // com.annimon.stream.Collector
        public BiConsumer<A, T> accumulator() {
            return this.accumulator;
        }

        @Override // com.annimon.stream.Collector
        public Function<A, R> finisher() {
            return this.finisher;
        }
    }
}
