package com.annimon.stream;

import com.annimon.stream.function.BiConsumer;
import com.annimon.stream.function.BiFunction;
import com.annimon.stream.function.BinaryOperator;
import com.annimon.stream.function.Consumer;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.IndexedFunction;
import com.annimon.stream.function.IndexedPredicate;
import com.annimon.stream.function.IntFunction;
import com.annimon.stream.function.Predicate;
import com.annimon.stream.function.Supplier;
import com.annimon.stream.function.ToIntFunction;
import com.annimon.stream.function.ToLongFunction;
import com.annimon.stream.internal.Compose;
import com.annimon.stream.internal.Operators;
import com.annimon.stream.internal.Params;
import com.annimon.stream.iterator.IndexedIterator;
import com.annimon.stream.iterator.LazyIterator;
import com.annimon.stream.operator.ObjArray;
import com.annimon.stream.operator.ObjConcat;
import com.annimon.stream.operator.ObjDistinct;
import com.annimon.stream.operator.ObjDistinctBy;
import com.annimon.stream.operator.ObjFilter;
import com.annimon.stream.operator.ObjFlatMap;
import com.annimon.stream.operator.ObjLimit;
import com.annimon.stream.operator.ObjMap;
import com.annimon.stream.operator.ObjMapIndexed;
import com.annimon.stream.operator.ObjMapToInt;
import com.annimon.stream.operator.ObjMapToLong;
import com.annimon.stream.operator.ObjSkip;
import com.annimon.stream.operator.ObjSorted;
import com.annimon.stream.operator.ObjTakeUntilIndexed;
import com.annimon.stream.operator.ObjTakeWhile;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

/* loaded from: classes.dex */
public class Stream<T> implements Closeable {
    private final Iterator<? extends T> iterator;
    private final Params params;

    public static <T> Stream<T> empty() {
        return of(Collections.emptyList());
    }

    public static <K, V> Stream<Map.Entry<K, V>> of(Map<K, V> map) {
        Objects.requireNonNull(map);
        return new Stream<>(map.entrySet());
    }

    public static <T> Stream<T> of(Iterable<? extends T> iterable) {
        Objects.requireNonNull(iterable);
        return new Stream<>(iterable);
    }

    public static <T> Stream<T> of(T... tArr) {
        Objects.requireNonNull(tArr);
        if (tArr.length == 0) {
            return empty();
        }
        return new Stream<>(new ObjArray(tArr));
    }

    public static Stream<Integer> rangeClosed(int i, int i2) {
        return IntStream.rangeClosed(i, i2).boxed();
    }

    public static <T> Stream<T> concat(Stream<? extends T> stream, Stream<? extends T> stream2) {
        Objects.requireNonNull(stream);
        Objects.requireNonNull(stream2);
        return new Stream(new ObjConcat(((Stream) stream).iterator, ((Stream) stream2).iterator)).onClose(Compose.closeables(stream, stream2));
    }

    private Stream(Iterator<? extends T> it) {
        this((Params) null, it);
    }

    private Stream(Iterable<? extends T> iterable) {
        this((Params) null, new LazyIterator(iterable));
    }

    private Stream(Params params, Iterable<? extends T> iterable) {
        this(params, new LazyIterator(iterable));
    }

    public Stream(Params params, Iterator<? extends T> it) {
        this.params = params;
        this.iterator = it;
    }

    public Iterator<? extends T> iterator() {
        return this.iterator;
    }

    public Stream<T> filter(Predicate<? super T> predicate) {
        return new Stream<>(this.params, new ObjFilter(this.iterator, predicate));
    }

    public Stream<T> filterNot(Predicate<? super T> predicate) {
        return filter(Predicate.Util.negate(predicate));
    }

    /* JADX DEBUG: Type inference failed for r2v1. Raw type applied. Possible types: com.annimon.stream.Stream<T>, com.annimon.stream.Stream<TT> */
    public <TT> Stream<TT> select(final Class<TT> cls) {
        return (Stream<T>) filter(new Predicate<T>() { // from class: com.annimon.stream.Stream.1
            @Override // com.annimon.stream.function.Predicate
            public boolean test(T t) {
                return cls.isInstance(t);
            }
        });
    }

    public Stream<T> withoutNulls() {
        return filter(Predicate.Util.notNull());
    }

    public <R> Stream<R> map(Function<? super T, ? extends R> function) {
        return new Stream<>(this.params, new ObjMap(this.iterator, function));
    }

    public <R> Stream<R> mapIndexed(IndexedFunction<? super T, ? extends R> indexedFunction) {
        return mapIndexed(0, 1, indexedFunction);
    }

    public <R> Stream<R> mapIndexed(int i, int i2, IndexedFunction<? super T, ? extends R> indexedFunction) {
        return new Stream<>(this.params, new ObjMapIndexed(new IndexedIterator(i, i2, this.iterator), indexedFunction));
    }

    public IntStream mapToInt(ToIntFunction<? super T> toIntFunction) {
        return new IntStream(this.params, new ObjMapToInt(this.iterator, toIntFunction));
    }

    public LongStream mapToLong(ToLongFunction<? super T> toLongFunction) {
        return new LongStream(this.params, new ObjMapToLong(this.iterator, toLongFunction));
    }

    public <R> Stream<R> flatMap(Function<? super T, ? extends Stream<? extends R>> function) {
        return new Stream<>(this.params, new ObjFlatMap(this.iterator, function));
    }

    public Stream<IntPair<T>> indexed() {
        return indexed(0, 1);
    }

    public Stream<IntPair<T>> indexed(int i, int i2) {
        return (Stream<IntPair<T>>) mapIndexed(i, i2, new IndexedFunction<T, IntPair<T>>() { // from class: com.annimon.stream.Stream.2
            @Override // com.annimon.stream.function.IndexedFunction
            public IntPair<T> apply(int i3, T t) {
                return new IntPair<>(i3, t);
            }
        });
    }

    public Stream<T> distinct() {
        return new Stream<>(this.params, new ObjDistinct(this.iterator));
    }

    public <K> Stream<T> distinctBy(Function<? super T, ? extends K> function) {
        return new Stream<>(this.params, new ObjDistinctBy(this.iterator, function));
    }

    public Stream<T> sorted() {
        return sorted(new Comparator<T>() { // from class: com.annimon.stream.Stream.3
            @Override // java.util.Comparator
            public int compare(T t, T t2) {
                return ((Comparable) t).compareTo((Comparable) t2);
            }
        });
    }

    public Stream<T> sorted(Comparator<? super T> comparator) {
        return new Stream<>(this.params, new ObjSorted(this.iterator, comparator));
    }

    public <R extends Comparable<? super R>> Stream<T> sortBy(Function<? super T, ? extends R> function) {
        return sorted(ComparatorCompat.comparing(function));
    }

    public <K> Stream<Map.Entry<K, List<T>>> groupBy(Function<? super T, ? extends K> function) {
        return new Stream<>(this.params, ((Map) collect(Collectors.groupingBy(function))).entrySet());
    }

    public Stream<T> takeWhile(Predicate<? super T> predicate) {
        return new Stream<>(this.params, new ObjTakeWhile(this.iterator, predicate));
    }

    public Stream<T> takeUntilIndexed(IndexedPredicate<? super T> indexedPredicate) {
        return takeUntilIndexed(0, 1, indexedPredicate);
    }

    public Stream<T> takeUntilIndexed(int i, int i2, IndexedPredicate<? super T> indexedPredicate) {
        return new Stream<>(this.params, new ObjTakeUntilIndexed(new IndexedIterator(i, i2, this.iterator), indexedPredicate));
    }

    public Stream<T> limit(long j) {
        if (j < 0) {
            throw new IllegalArgumentException("maxSize cannot be negative");
        } else if (j == 0) {
            return empty();
        } else {
            return new Stream<>(this.params, new ObjLimit(this.iterator, j));
        }
    }

    public Stream<T> skip(long j) {
        if (j >= 0) {
            return j == 0 ? this : new Stream<>(this.params, new ObjSkip(this.iterator, j));
        }
        throw new IllegalArgumentException("n cannot be negative");
    }

    public void forEach(Consumer<? super T> consumer) {
        while (this.iterator.hasNext()) {
            consumer.accept((Object) this.iterator.next());
        }
    }

    /* JADX DEBUG: Type inference failed for r2v4. Raw type applied. Possible types: R, ? super R */
    public <R> R reduce(R r, BiFunction<? super R, ? super T, ? extends R> biFunction) {
        while (this.iterator.hasNext()) {
            r = (R) biFunction.apply(r, (Object) this.iterator.next());
        }
        return r;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: com.annimon.stream.function.BiFunction<T, T, T> */
    /* JADX WARN: Multi-variable type inference failed */
    public Optional<T> reduce(BiFunction<T, T, T> biFunction) {
        boolean z = false;
        Object obj = null;
        while (this.iterator.hasNext()) {
            Object next = this.iterator.next();
            if (!z) {
                z = true;
                obj = next;
            } else {
                obj = biFunction.apply(obj, next);
            }
        }
        return z ? Optional.of(obj) : Optional.empty();
    }

    public Object[] toArray() {
        return toArray(new IntFunction<Object[]>() { // from class: com.annimon.stream.Stream.5
            @Override // com.annimon.stream.function.IntFunction
            public Object[] apply(int i) {
                return new Object[i];
            }
        });
    }

    public <R> R[] toArray(IntFunction<R[]> intFunction) {
        return (R[]) Operators.toArray(this.iterator, intFunction);
    }

    public List<T> toList() {
        ArrayList arrayList = new ArrayList();
        while (this.iterator.hasNext()) {
            arrayList.add(this.iterator.next());
        }
        return arrayList;
    }

    public <R> R collect(Supplier<R> supplier, BiConsumer<R, ? super T> biConsumer) {
        R r = supplier.get();
        while (this.iterator.hasNext()) {
            biConsumer.accept(r, (Object) this.iterator.next());
        }
        return r;
    }

    public <R, A> R collect(Collector<? super T, A, R> collector) {
        A a = collector.supplier().get();
        while (this.iterator.hasNext()) {
            collector.accumulator().accept(a, (Object) this.iterator.next());
        }
        if (collector.finisher() != null) {
            return collector.finisher().apply(a);
        }
        return (R) Collectors.castIdentity().apply(a);
    }

    public Optional<T> max(Comparator<? super T> comparator) {
        return reduce(BinaryOperator.Util.maxBy(comparator));
    }

    public long count() {
        long j = 0;
        while (this.iterator.hasNext()) {
            this.iterator.next();
            j++;
        }
        return j;
    }

    public boolean anyMatch(Predicate<? super T> predicate) {
        return match(predicate, 0);
    }

    public boolean allMatch(Predicate<? super T> predicate) {
        return match(predicate, 1);
    }

    public boolean noneMatch(Predicate<? super T> predicate) {
        return match(predicate, 2);
    }

    public Optional<T> findFirst() {
        if (this.iterator.hasNext()) {
            return Optional.of(this.iterator.next());
        }
        return Optional.empty();
    }

    public Optional<T> findLast() {
        return reduce(new BinaryOperator<T>() { // from class: com.annimon.stream.Stream.6
            @Override // com.annimon.stream.function.BiFunction
            public T apply(T t, T t2) {
                return t2;
            }
        });
    }

    public T single() {
        if (this.iterator.hasNext()) {
            T t = (T) this.iterator.next();
            if (!this.iterator.hasNext()) {
                return t;
            }
            throw new IllegalStateException("Stream contains more than one element");
        }
        throw new NoSuchElementException("Stream contains no element");
    }

    public Optional<T> findSingle() {
        if (!this.iterator.hasNext()) {
            return Optional.empty();
        }
        Object next = this.iterator.next();
        if (!this.iterator.hasNext()) {
            return Optional.of(next);
        }
        throw new IllegalStateException("Stream contains more than one element");
    }

    public Stream<T> onClose(Runnable runnable) {
        Objects.requireNonNull(runnable);
        Params params = this.params;
        if (params == null) {
            params = new Params();
            params.closeHandler = runnable;
        } else {
            params.closeHandler = Compose.runnables(params.closeHandler, runnable);
        }
        return new Stream<>(params, this.iterator);
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        Runnable runnable;
        Params params = this.params;
        if (params != null && (runnable = params.closeHandler) != null) {
            runnable.run();
            this.params.closeHandler = null;
        }
    }

    private boolean match(Predicate<? super T> predicate, int i) {
        boolean z = i == 0;
        boolean z2 = i == 1;
        while (this.iterator.hasNext()) {
            boolean test = predicate.test((Object) this.iterator.next());
            if (test ^ z2) {
                if (!z || !test) {
                    return false;
                }
                return true;
            }
        }
        return !z;
    }
}
