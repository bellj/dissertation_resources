package consensus_client;

import attest.Attest$Message;
import consensus_common.ConsensusCommon$ProposeTxResponse;
import io.grpc.CallOptions;
import io.grpc.Channel;
import io.grpc.MethodDescriptor;
import io.grpc.protobuf.lite.ProtoLiteUtils;
import io.grpc.stub.AbstractBlockingStub;
import io.grpc.stub.AbstractStub;
import io.grpc.stub.ClientCalls;

/* loaded from: classes3.dex */
public final class ConsensusClientAPIGrpc {
    private static volatile MethodDescriptor<Attest$Message, ConsensusCommon$ProposeTxResponse> getClientTxProposeMethod;

    /* renamed from: consensus_client.ConsensusClientAPIGrpc$1 */
    /* loaded from: classes3.dex */
    public class AnonymousClass1 implements AbstractStub.StubFactory<Object> {
    }

    private ConsensusClientAPIGrpc() {
    }

    public static MethodDescriptor<Attest$Message, ConsensusCommon$ProposeTxResponse> getClientTxProposeMethod() {
        MethodDescriptor<Attest$Message, ConsensusCommon$ProposeTxResponse> methodDescriptor = getClientTxProposeMethod;
        if (methodDescriptor == null) {
            synchronized (ConsensusClientAPIGrpc.class) {
                methodDescriptor = getClientTxProposeMethod;
                if (methodDescriptor == null) {
                    methodDescriptor = MethodDescriptor.newBuilder().setType(MethodDescriptor.MethodType.UNARY).setFullMethodName(MethodDescriptor.generateFullMethodName("consensus_client.ConsensusClientAPI", "ClientTxPropose")).setSampledToLocalTracing(true).setRequestMarshaller(ProtoLiteUtils.marshaller(Attest$Message.getDefaultInstance())).setResponseMarshaller(ProtoLiteUtils.marshaller(ConsensusCommon$ProposeTxResponse.getDefaultInstance())).build();
                    getClientTxProposeMethod = methodDescriptor;
                }
            }
        }
        return methodDescriptor;
    }

    public static ConsensusClientAPIBlockingStub newBlockingStub(Channel channel) {
        return (ConsensusClientAPIBlockingStub) AbstractBlockingStub.newStub(new AbstractStub.StubFactory<ConsensusClientAPIBlockingStub>() { // from class: consensus_client.ConsensusClientAPIGrpc.2
            @Override // io.grpc.stub.AbstractStub.StubFactory
            public ConsensusClientAPIBlockingStub newStub(Channel channel2, CallOptions callOptions) {
                return new ConsensusClientAPIBlockingStub(channel2, callOptions, null);
            }
        }, channel);
    }

    /* loaded from: classes3.dex */
    public static final class ConsensusClientAPIBlockingStub extends AbstractBlockingStub<ConsensusClientAPIBlockingStub> {
        /* synthetic */ ConsensusClientAPIBlockingStub(Channel channel, CallOptions callOptions, AnonymousClass1 r3) {
            this(channel, callOptions);
        }

        private ConsensusClientAPIBlockingStub(Channel channel, CallOptions callOptions) {
            super(channel, callOptions);
        }

        @Override // io.grpc.stub.AbstractStub
        public ConsensusClientAPIBlockingStub build(Channel channel, CallOptions callOptions) {
            return new ConsensusClientAPIBlockingStub(channel, callOptions);
        }

        public ConsensusCommon$ProposeTxResponse clientTxPropose(Attest$Message attest$Message) {
            return (ConsensusCommon$ProposeTxResponse) ClientCalls.blockingUnaryCall(getChannel(), ConsensusClientAPIGrpc.getClientTxProposeMethod(), getCallOptions(), attest$Message);
        }
    }
}
