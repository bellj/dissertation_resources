package ezvcard;

import ezvcard.io.chain.ChainingTextParser;
import ezvcard.io.chain.ChainingTextStringParser;
import ezvcard.util.IOUtils;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/* loaded from: classes3.dex */
public final class Ezvcard {
    public static final String ARTIFACT_ID;
    public static final String GROUP_ID;
    public static final String URL;
    public static final String VERSION;

    /* JADX INFO: finally extract failed */
    static {
        Properties properties = new Properties();
        InputStream inputStream = null;
        try {
            try {
                inputStream = Ezvcard.class.getResourceAsStream("ez-vcard.properties");
                properties.load(inputStream);
                IOUtils.closeQuietly(inputStream);
                VERSION = properties.getProperty("version");
                GROUP_ID = properties.getProperty("groupId");
                ARTIFACT_ID = properties.getProperty("artifactId");
                URL = properties.getProperty("url");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } catch (Throwable th) {
            IOUtils.closeQuietly(inputStream);
            throw th;
        }
    }

    public static ChainingTextStringParser parse(String str) {
        return new ChainingTextStringParser(str);
    }

    public static ChainingTextParser<ChainingTextParser<?>> parse(InputStream inputStream) {
        return new ChainingTextParser<>(inputStream);
    }

    private Ezvcard() {
    }
}
