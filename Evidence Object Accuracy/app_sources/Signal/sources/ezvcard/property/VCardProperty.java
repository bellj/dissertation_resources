package ezvcard.property;

import ezvcard.Messages;
import ezvcard.parameter.VCardParameters;
import java.util.Collections;
import java.util.Map;

/* loaded from: classes3.dex */
public abstract class VCardProperty implements Comparable<VCardProperty> {
    protected String group;
    protected VCardParameters parameters = new VCardParameters();

    public VCardParameters getParameters() {
        return this.parameters;
    }

    public void setParameters(VCardParameters vCardParameters) {
        if (vCardParameters != null) {
            this.parameters = vCardParameters;
            return;
        }
        throw new NullPointerException(Messages.INSTANCE.getExceptionMessage(42, new Object[0]));
    }

    public void setGroup(String str) {
        this.group = str;
    }

    public int compareTo(VCardProperty vCardProperty) {
        Integer pref = getParameters().getPref();
        Integer pref2 = vCardProperty.getParameters().getPref();
        if (pref == null && pref2 == null) {
            return 0;
        }
        if (pref == null) {
            return 1;
        }
        if (pref2 == null) {
            return -1;
        }
        return pref2.compareTo(pref);
    }

    protected Map<String, Object> toStringValues() {
        return Collections.emptyMap();
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getName());
        sb.append(" [ group=");
        sb.append(this.group);
        sb.append(" | parameters=");
        sb.append(this.parameters);
        for (Map.Entry<String, Object> entry : toStringValues().entrySet()) {
            Object value = entry.getValue();
            sb.append(" | ");
            sb.append(entry.getKey());
            sb.append('=');
            sb.append(value);
        }
        sb.append(" ]");
        return sb.toString();
    }

    @Override // java.lang.Object
    public int hashCode() {
        String str = this.group;
        return (((str == null ? 0 : str.toLowerCase().hashCode()) + 31) * 31) + this.parameters.hashCode();
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        VCardProperty vCardProperty = (VCardProperty) obj;
        String str = this.group;
        if (str == null) {
            if (vCardProperty.group != null) {
                return false;
            }
        } else if (!str.equalsIgnoreCase(vCardProperty.group)) {
            return false;
        }
        return this.parameters.equals(vCardProperty.parameters);
    }
}
