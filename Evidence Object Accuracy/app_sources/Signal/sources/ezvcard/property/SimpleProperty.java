package ezvcard.property;

import java.util.LinkedHashMap;
import java.util.Map;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* loaded from: classes3.dex */
public class SimpleProperty<T> extends VCardProperty {
    protected T value;

    public SimpleProperty(T t) {
        this.value = t;
    }

    public T getValue() {
        return this.value;
    }

    public void setValue(T t) {
        this.value = t;
    }

    @Override // ezvcard.property.VCardProperty
    protected Map<String, Object> toStringValues() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put(DraftDatabase.DRAFT_VALUE, this.value);
        return linkedHashMap;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        T t = this.value;
        return hashCode + (t == null ? 0 : t.hashCode());
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        SimpleProperty simpleProperty = (SimpleProperty) obj;
        T t = this.value;
        if (t == null) {
            if (simpleProperty.value != null) {
                return false;
            }
        } else if (!t.equals(simpleProperty.value)) {
            return false;
        }
        return true;
    }
}
