package ezvcard.property;

import ezvcard.parameter.ImageType;

/* loaded from: classes3.dex */
public class Logo extends ImageProperty {
    public Logo(String str, ImageType imageType) {
        super(str, imageType);
    }

    public Logo(byte[] bArr, ImageType imageType) {
        super(bArr, imageType);
    }
}
