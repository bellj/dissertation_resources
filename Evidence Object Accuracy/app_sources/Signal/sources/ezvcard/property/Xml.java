package ezvcard.property;

import ezvcard.util.XmlUtils;
import java.util.LinkedHashMap;
import java.util.Map;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/* loaded from: classes3.dex */
public class Xml extends VCardProperty {
    private Document value;

    public Xml(String str) throws SAXException {
        this(str == null ? null : XmlUtils.toDocument(str));
    }

    public Xml(Document document) {
        this.value = document;
    }

    @Override // ezvcard.property.VCardProperty
    protected Map<String, Object> toStringValues() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        Document document = this.value;
        linkedHashMap.put(DraftDatabase.DRAFT_VALUE, document == null ? "null" : XmlUtils.toString(document));
        return linkedHashMap;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        Document document = this.value;
        return hashCode + (document == null ? 0 : XmlUtils.toString(document).hashCode());
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        Xml xml = (Xml) obj;
        Document document = this.value;
        if (document == null) {
            if (xml.value != null) {
                return false;
            }
        } else if (xml.value == null || !XmlUtils.toString(document).equals(XmlUtils.toString(xml.value))) {
            return false;
        }
        return true;
    }
}
