package ezvcard.property;

import ezvcard.util.PartialDate;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* loaded from: classes3.dex */
public class DateOrTimeProperty extends VCardProperty {
    private Date date;
    private boolean dateHasTime;
    private PartialDate partialDate;
    private String text;

    public DateOrTimeProperty(Date date, boolean z) {
        setDate(date, z);
    }

    public DateOrTimeProperty(PartialDate partialDate) {
        setPartialDate(partialDate);
    }

    public DateOrTimeProperty(String str) {
        setText(str);
    }

    public void setDate(Date date, boolean z) {
        this.date = date;
        if (date == null) {
            z = false;
        }
        this.dateHasTime = z;
        this.text = null;
        this.partialDate = null;
    }

    public void setPartialDate(PartialDate partialDate) {
        boolean z;
        this.partialDate = partialDate;
        if (partialDate == null) {
            z = false;
        } else {
            z = partialDate.hasTimeComponent();
        }
        this.dateHasTime = z;
        this.text = null;
        this.date = null;
    }

    public void setText(String str) {
        this.text = str;
        this.date = null;
        this.partialDate = null;
        this.dateHasTime = false;
    }

    @Override // ezvcard.property.VCardProperty
    protected Map<String, Object> toStringValues() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put(DraftDatabase.Draft.TEXT, this.text);
        linkedHashMap.put("date", this.date);
        linkedHashMap.put("dateHasTime", Boolean.valueOf(this.dateHasTime));
        linkedHashMap.put("partialDate", this.partialDate);
        return linkedHashMap;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        Date date = this.date;
        int i = 0;
        int hashCode2 = (((hashCode + (date == null ? 0 : date.hashCode())) * 31) + (this.dateHasTime ? 1231 : 1237)) * 31;
        PartialDate partialDate = this.partialDate;
        int hashCode3 = (hashCode2 + (partialDate == null ? 0 : partialDate.hashCode())) * 31;
        String str = this.text;
        if (str != null) {
            i = str.hashCode();
        }
        return hashCode3 + i;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        DateOrTimeProperty dateOrTimeProperty = (DateOrTimeProperty) obj;
        Date date = this.date;
        if (date == null) {
            if (dateOrTimeProperty.date != null) {
                return false;
            }
        } else if (!date.equals(dateOrTimeProperty.date)) {
            return false;
        }
        if (this.dateHasTime != dateOrTimeProperty.dateHasTime) {
            return false;
        }
        PartialDate partialDate = this.partialDate;
        if (partialDate == null) {
            if (dateOrTimeProperty.partialDate != null) {
                return false;
            }
        } else if (!partialDate.equals(dateOrTimeProperty.partialDate)) {
            return false;
        }
        String str = this.text;
        if (str == null) {
            if (dateOrTimeProperty.text != null) {
                return false;
            }
        } else if (!str.equals(dateOrTimeProperty.text)) {
            return false;
        }
        return true;
    }
}
