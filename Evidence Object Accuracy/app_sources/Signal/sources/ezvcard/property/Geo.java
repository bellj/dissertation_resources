package ezvcard.property;

import ezvcard.util.GeoUri;
import java.util.LinkedHashMap;
import java.util.Map;

/* loaded from: classes3.dex */
public class Geo extends VCardProperty {
    private GeoUri uri;

    public Geo(Double d, Double d2) {
        this(new GeoUri.Builder(d, d2).build());
    }

    public Geo(GeoUri geoUri) {
        this.uri = geoUri;
    }

    @Override // ezvcard.property.VCardProperty
    protected Map<String, Object> toStringValues() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("uri", this.uri);
        return linkedHashMap;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        GeoUri geoUri = this.uri;
        return hashCode + (geoUri == null ? 0 : geoUri.hashCode());
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        Geo geo = (Geo) obj;
        GeoUri geoUri = this.uri;
        if (geoUri == null) {
            if (geo.uri != null) {
                return false;
            }
        } else if (!geoUri.equals(geo.uri)) {
            return false;
        }
        return true;
    }
}
