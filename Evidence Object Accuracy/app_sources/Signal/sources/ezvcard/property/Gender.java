package ezvcard.property;

import java.util.LinkedHashMap;
import java.util.Map;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* loaded from: classes3.dex */
public class Gender extends VCardProperty {
    private String gender;
    private String text;

    public Gender(String str) {
        this.gender = str;
    }

    public void setText(String str) {
        this.text = str;
    }

    @Override // ezvcard.property.VCardProperty
    protected Map<String, Object> toStringValues() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("gender", this.gender);
        linkedHashMap.put(DraftDatabase.Draft.TEXT, this.text);
        return linkedHashMap;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        String str = this.gender;
        int i = 0;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.text;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode2 + i;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        Gender gender = (Gender) obj;
        String str = this.gender;
        if (str == null) {
            if (gender.gender != null) {
                return false;
            }
        } else if (!str.equals(gender.gender)) {
            return false;
        }
        String str2 = this.text;
        if (str2 == null) {
            if (gender.text != null) {
                return false;
            }
        } else if (!str2.equals(gender.text)) {
            return false;
        }
        return true;
    }
}
