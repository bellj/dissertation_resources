package ezvcard.property;

import java.util.LinkedHashMap;
import java.util.Map;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* loaded from: classes3.dex */
public class Related extends VCardProperty {
    private String text;
    private String uri;

    public void setUri(String str) {
        this.uri = str;
        this.text = null;
    }

    public void setText(String str) {
        this.text = str;
        this.uri = null;
    }

    @Override // ezvcard.property.VCardProperty
    protected Map<String, Object> toStringValues() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("uri", this.uri);
        linkedHashMap.put(DraftDatabase.Draft.TEXT, this.text);
        return linkedHashMap;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        String str = this.text;
        int i = 0;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.uri;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode2 + i;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        Related related = (Related) obj;
        String str = this.text;
        if (str == null) {
            if (related.text != null) {
                return false;
            }
        } else if (!str.equals(related.text)) {
            return false;
        }
        String str2 = this.uri;
        if (str2 == null) {
            if (related.uri != null) {
                return false;
            }
        } else if (!str2.equals(related.uri)) {
            return false;
        }
        return true;
    }
}
