package ezvcard.property;

import ezvcard.util.PartialDate;
import java.util.Date;

/* loaded from: classes3.dex */
public class Birthday extends DateOrTimeProperty {
    public Birthday(Date date, boolean z) {
        super(date, z);
    }

    public Birthday(PartialDate partialDate) {
        super(partialDate);
    }

    public Birthday(String str) {
        super(str);
    }
}
