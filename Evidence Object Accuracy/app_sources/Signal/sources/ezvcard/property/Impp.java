package ezvcard.property;

import java.net.URI;
import java.util.LinkedHashMap;
import java.util.Map;

/* loaded from: classes3.dex */
public class Impp extends VCardProperty {
    private URI uri;

    public Impp(String str) {
        setUri(str);
    }

    public Impp(URI uri) {
        setUri(uri);
    }

    public void setUri(String str) {
        setUri(str == null ? null : URI.create(str));
    }

    public void setUri(URI uri) {
        this.uri = uri;
    }

    @Override // ezvcard.property.VCardProperty
    protected Map<String, Object> toStringValues() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("uri", this.uri);
        return linkedHashMap;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        URI uri = this.uri;
        return hashCode + (uri == null ? 0 : uri.hashCode());
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        Impp impp = (Impp) obj;
        URI uri = this.uri;
        if (uri == null) {
            if (impp.uri != null) {
                return false;
            }
        } else if (!uri.equals(impp.uri)) {
            return false;
        }
        return true;
    }
}
