package ezvcard.property;

import java.util.Date;

/* loaded from: classes3.dex */
public class Revision extends SimpleProperty<Date> {
    public Revision(Date date) {
        super(date);
    }
}
