package ezvcard.property;

import ezvcard.VCard;
import java.util.LinkedHashMap;
import java.util.Map;

/* loaded from: classes3.dex */
public class Agent extends VCardProperty {
    private String url;
    private VCard vcard;

    public void setUrl(String str) {
        this.url = str;
        this.vcard = null;
    }

    public void setVCard(VCard vCard) {
        this.vcard = vCard;
        this.url = null;
    }

    @Override // ezvcard.property.VCardProperty
    protected Map<String, Object> toStringValues() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("url", this.url);
        linkedHashMap.put("vcard", this.vcard);
        return linkedHashMap;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        String str = this.url;
        int i = 0;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        VCard vCard = this.vcard;
        if (vCard != null) {
            i = vCard.hashCode();
        }
        return hashCode2 + i;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        Agent agent = (Agent) obj;
        String str = this.url;
        if (str == null) {
            if (agent.url != null) {
                return false;
            }
        } else if (!str.equals(agent.url)) {
            return false;
        }
        VCard vCard = this.vcard;
        if (vCard == null) {
            if (agent.vcard != null) {
                return false;
            }
        } else if (!vCard.equals(agent.vcard)) {
            return false;
        }
        return true;
    }
}
