package ezvcard.property;

import java.util.LinkedHashMap;
import java.util.Map;

/* loaded from: classes3.dex */
public class ClientPidMap extends VCardProperty {
    private Integer pid;
    private String uri;

    public ClientPidMap(Integer num, String str) {
        this.pid = num;
        this.uri = str;
    }

    @Override // ezvcard.property.VCardProperty
    protected Map<String, Object> toStringValues() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("pid", this.pid);
        linkedHashMap.put("uri", this.uri);
        return linkedHashMap;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        Integer num = this.pid;
        int i = 0;
        int hashCode2 = (hashCode + (num == null ? 0 : num.hashCode())) * 31;
        String str = this.uri;
        if (str != null) {
            i = str.hashCode();
        }
        return hashCode2 + i;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        ClientPidMap clientPidMap = (ClientPidMap) obj;
        Integer num = this.pid;
        if (num == null) {
            if (clientPidMap.pid != null) {
                return false;
            }
        } else if (!num.equals(clientPidMap.pid)) {
            return false;
        }
        String str = this.uri;
        if (str == null) {
            if (clientPidMap.uri != null) {
                return false;
            }
        } else if (!str.equals(clientPidMap.uri)) {
            return false;
        }
        return true;
    }
}
