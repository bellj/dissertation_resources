package ezvcard.property;

import ezvcard.parameter.EmailType;
import ezvcard.parameter.VCardParameters;
import java.util.List;

/* loaded from: classes3.dex */
public class Email extends TextProperty {
    public Email(String str) {
        super(str);
    }

    public List<EmailType> getTypes() {
        VCardParameters vCardParameters = this.parameters;
        vCardParameters.getClass();
        return new VCardParameters.TypeParameterList<EmailType>(vCardParameters) { // from class: ezvcard.property.Email.1
            {
                r2.getClass();
            }

            @Override // ezvcard.parameter.VCardParameters.VCardParameterList
            public EmailType _asObject(String str) {
                return EmailType.get(str);
            }
        };
    }
}
