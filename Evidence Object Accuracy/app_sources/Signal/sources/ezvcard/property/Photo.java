package ezvcard.property;

import ezvcard.parameter.ImageType;

/* loaded from: classes3.dex */
public class Photo extends ImageProperty {
    public Photo(String str, ImageType imageType) {
        super(str, imageType);
    }

    public Photo(byte[] bArr, ImageType imageType) {
        super(bArr, imageType);
    }
}
