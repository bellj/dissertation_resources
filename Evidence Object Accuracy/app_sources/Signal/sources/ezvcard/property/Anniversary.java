package ezvcard.property;

import ezvcard.util.PartialDate;
import java.util.Date;

/* loaded from: classes3.dex */
public class Anniversary extends DateOrTimeProperty {
    public Anniversary(Date date, boolean z) {
        super(date, z);
    }

    public Anniversary(PartialDate partialDate) {
        super(partialDate);
    }

    public Anniversary(String str) {
        super(str);
    }
}
