package ezvcard.property;

import ezvcard.util.UtcOffset;
import java.util.LinkedHashMap;
import java.util.Map;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* loaded from: classes3.dex */
public class Timezone extends VCardProperty {
    private UtcOffset offset;
    private String text;

    public Timezone(String str) {
        this(null, str);
    }

    public Timezone(UtcOffset utcOffset) {
        this(utcOffset, null);
    }

    public Timezone(UtcOffset utcOffset, String str) {
        setOffset(utcOffset);
        setText(str);
    }

    public void setOffset(UtcOffset utcOffset) {
        this.offset = utcOffset;
    }

    public void setText(String str) {
        this.text = str;
    }

    @Override // ezvcard.property.VCardProperty
    protected Map<String, Object> toStringValues() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("offset", this.offset);
        linkedHashMap.put(DraftDatabase.Draft.TEXT, this.text);
        return linkedHashMap;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        UtcOffset utcOffset = this.offset;
        int i = 0;
        int hashCode2 = (hashCode + (utcOffset == null ? 0 : utcOffset.hashCode())) * 31;
        String str = this.text;
        if (str != null) {
            i = str.hashCode();
        }
        return hashCode2 + i;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        Timezone timezone = (Timezone) obj;
        UtcOffset utcOffset = this.offset;
        if (utcOffset == null) {
            if (timezone.offset != null) {
                return false;
            }
        } else if (!utcOffset.equals(timezone.offset)) {
            return false;
        }
        String str = this.text;
        if (str == null) {
            if (timezone.text != null) {
                return false;
            }
        } else if (!str.equals(timezone.text)) {
            return false;
        }
        return true;
    }
}
