package ezvcard.property;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* loaded from: classes3.dex */
public class StructuredName extends VCardProperty {
    private final List<String> additional = new ArrayList();
    private String family;
    private String given;
    private final List<String> prefixes = new ArrayList();
    private final List<String> suffixes = new ArrayList();

    public String getFamily() {
        return this.family;
    }

    public void setFamily(String str) {
        this.family = str;
    }

    public String getGiven() {
        return this.given;
    }

    public void setGiven(String str) {
        this.given = str;
    }

    public List<String> getAdditionalNames() {
        return this.additional;
    }

    public List<String> getPrefixes() {
        return this.prefixes;
    }

    public List<String> getSuffixes() {
        return this.suffixes;
    }

    @Override // ezvcard.property.VCardProperty
    protected Map<String, Object> toStringValues() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("family", this.family);
        linkedHashMap.put("given", this.given);
        linkedHashMap.put("additional", this.additional);
        linkedHashMap.put("prefixes", this.prefixes);
        linkedHashMap.put("suffixes", this.suffixes);
        return linkedHashMap;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        List<String> list = this.additional;
        int i = 0;
        int hashCode2 = (hashCode + (list == null ? 0 : list.hashCode())) * 31;
        String str = this.family;
        int hashCode3 = (hashCode2 + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.given;
        int hashCode4 = (hashCode3 + (str2 == null ? 0 : str2.hashCode())) * 31;
        List<String> list2 = this.prefixes;
        int hashCode5 = (hashCode4 + (list2 == null ? 0 : list2.hashCode())) * 31;
        List<String> list3 = this.suffixes;
        if (list3 != null) {
            i = list3.hashCode();
        }
        return hashCode5 + i;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        StructuredName structuredName = (StructuredName) obj;
        List<String> list = this.additional;
        if (list == null) {
            if (structuredName.additional != null) {
                return false;
            }
        } else if (!list.equals(structuredName.additional)) {
            return false;
        }
        String str = this.family;
        if (str == null) {
            if (structuredName.family != null) {
                return false;
            }
        } else if (!str.equals(structuredName.family)) {
            return false;
        }
        String str2 = this.given;
        if (str2 == null) {
            if (structuredName.given != null) {
                return false;
            }
        } else if (!str2.equals(structuredName.given)) {
            return false;
        }
        List<String> list2 = this.prefixes;
        if (list2 == null) {
            if (structuredName.prefixes != null) {
                return false;
            }
        } else if (!list2.equals(structuredName.prefixes)) {
            return false;
        }
        List<String> list3 = this.suffixes;
        if (list3 == null) {
            if (structuredName.suffixes != null) {
                return false;
            }
        } else if (!list3.equals(structuredName.suffixes)) {
            return false;
        }
        return true;
    }
}
