package ezvcard.property;

import ezvcard.parameter.KeyType;
import java.util.Map;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* loaded from: classes3.dex */
public class Key extends BinaryProperty<KeyType> {
    private String text;

    public Key() {
    }

    public Key(byte[] bArr, KeyType keyType) {
        super(bArr, keyType);
    }

    public Key(String str, KeyType keyType) {
        super(str, keyType);
    }

    public void setUrl(String str, KeyType keyType) {
        super.setUrl(str, (String) keyType);
        this.text = null;
    }

    public void setData(byte[] bArr, KeyType keyType) {
        super.setData(bArr, (byte[]) keyType);
        this.text = null;
    }

    protected Map<String, Object> toStringValues() {
        Map<String, Object> stringValues = super.toStringValues();
        stringValues.put(DraftDatabase.Draft.TEXT, this.text);
        return stringValues;
    }

    @Override // ezvcard.property.BinaryProperty, ezvcard.property.VCardProperty, java.lang.Object
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        String str = this.text;
        return hashCode + (str == null ? 0 : str.hashCode());
    }

    @Override // ezvcard.property.BinaryProperty, ezvcard.property.VCardProperty, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        Key key = (Key) obj;
        String str = this.text;
        if (str == null) {
            if (key.text != null) {
                return false;
            }
        } else if (!str.equals(key.text)) {
            return false;
        }
        return true;
    }
}
