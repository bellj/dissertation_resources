package ezvcard.property;

import ezvcard.parameter.AddressType;
import ezvcard.parameter.VCardParameters;
import java.util.List;

/* loaded from: classes3.dex */
public class Label extends TextProperty {
    public Label(String str) {
        super(str);
    }

    public List<AddressType> getTypes() {
        VCardParameters vCardParameters = this.parameters;
        vCardParameters.getClass();
        return new VCardParameters.TypeParameterList<AddressType>(vCardParameters) { // from class: ezvcard.property.Label.1
            {
                r2.getClass();
            }

            @Override // ezvcard.parameter.VCardParameters.VCardParameterList
            public AddressType _asObject(String str) {
                return AddressType.get(str);
            }
        };
    }
}
