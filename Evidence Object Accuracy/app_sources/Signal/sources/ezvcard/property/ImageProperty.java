package ezvcard.property;

import ezvcard.parameter.ImageType;

/* loaded from: classes3.dex */
public class ImageProperty extends BinaryProperty<ImageType> {
    public ImageProperty(String str, ImageType imageType) {
        super(str, imageType);
    }

    public ImageProperty(byte[] bArr, ImageType imageType) {
        super(bArr, imageType);
    }
}
