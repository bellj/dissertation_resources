package ezvcard.property;

import ezvcard.parameter.TelephoneType;
import ezvcard.parameter.VCardParameters;
import ezvcard.util.TelUri;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* loaded from: classes3.dex */
public class Telephone extends VCardProperty {
    private String text;
    private TelUri uri;

    public Telephone(String str) {
        setText(str);
    }

    public Telephone(TelUri telUri) {
        setUri(telUri);
    }

    public String getText() {
        return this.text;
    }

    public void setText(String str) {
        this.text = str;
        this.uri = null;
    }

    public TelUri getUri() {
        return this.uri;
    }

    public void setUri(TelUri telUri) {
        this.text = null;
        this.uri = telUri;
    }

    public List<TelephoneType> getTypes() {
        VCardParameters vCardParameters = this.parameters;
        vCardParameters.getClass();
        return new VCardParameters.TypeParameterList<TelephoneType>(vCardParameters) { // from class: ezvcard.property.Telephone.1
            {
                r2.getClass();
            }

            @Override // ezvcard.parameter.VCardParameters.VCardParameterList
            public TelephoneType _asObject(String str) {
                return TelephoneType.get(str);
            }
        };
    }

    @Override // ezvcard.property.VCardProperty
    protected Map<String, Object> toStringValues() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("uri", this.uri);
        linkedHashMap.put(DraftDatabase.Draft.TEXT, this.text);
        return linkedHashMap;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        String str = this.text;
        int i = 0;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        TelUri telUri = this.uri;
        if (telUri != null) {
            i = telUri.hashCode();
        }
        return hashCode2 + i;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        Telephone telephone = (Telephone) obj;
        String str = this.text;
        if (str == null) {
            if (telephone.text != null) {
                return false;
            }
        } else if (!str.equals(telephone.text)) {
            return false;
        }
        TelUri telUri = this.uri;
        if (telUri == null) {
            if (telephone.uri != null) {
                return false;
            }
        } else if (!telUri.equals(telephone.uri)) {
            return false;
        }
        return true;
    }
}
