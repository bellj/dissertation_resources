package ezvcard.property;

import ezvcard.VCardDataType;
import java.util.LinkedHashMap;
import java.util.Map;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* loaded from: classes3.dex */
public class RawProperty extends TextProperty {
    private VCardDataType dataType;
    private String propertyName;

    public RawProperty(String str, String str2) {
        this(str, str2, null);
    }

    public RawProperty(String str, String str2, VCardDataType vCardDataType) {
        super(str2);
        this.propertyName = str;
        this.dataType = vCardDataType;
    }

    public void setDataType(VCardDataType vCardDataType) {
        this.dataType = vCardDataType;
    }

    @Override // ezvcard.property.SimpleProperty, ezvcard.property.VCardProperty
    protected Map<String, Object> toStringValues() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("propertyName", this.propertyName);
        linkedHashMap.put("dataType", this.dataType);
        linkedHashMap.put(DraftDatabase.DRAFT_VALUE, this.value);
        return linkedHashMap;
    }

    @Override // ezvcard.property.SimpleProperty, ezvcard.property.VCardProperty, java.lang.Object
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        VCardDataType vCardDataType = this.dataType;
        int i = 0;
        int hashCode2 = (hashCode + (vCardDataType == null ? 0 : vCardDataType.hashCode())) * 31;
        String str = this.propertyName;
        if (str != null) {
            i = str.toLowerCase().hashCode();
        }
        return hashCode2 + i;
    }

    @Override // ezvcard.property.SimpleProperty, ezvcard.property.VCardProperty, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        RawProperty rawProperty = (RawProperty) obj;
        VCardDataType vCardDataType = this.dataType;
        if (vCardDataType == null) {
            if (rawProperty.dataType != null) {
                return false;
            }
        } else if (!vCardDataType.equals(rawProperty.dataType)) {
            return false;
        }
        String str = this.propertyName;
        if (str == null) {
            if (rawProperty.propertyName != null) {
                return false;
            }
        } else if (!str.equalsIgnoreCase(rawProperty.propertyName)) {
            return false;
        }
        return true;
    }
}
