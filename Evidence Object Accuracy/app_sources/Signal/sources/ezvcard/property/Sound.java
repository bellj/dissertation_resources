package ezvcard.property;

import ezvcard.parameter.SoundType;

/* loaded from: classes3.dex */
public class Sound extends BinaryProperty<SoundType> {
    public Sound(String str, SoundType soundType) {
        super(str, soundType);
    }

    public Sound(byte[] bArr, SoundType soundType) {
        super(bArr, soundType);
    }
}
