package ezvcard.property;

import ezvcard.util.PartialDate;
import java.util.Date;

/* loaded from: classes3.dex */
public class Deathdate extends DateOrTimeProperty {
    public Deathdate(Date date, boolean z) {
        super(date, z);
    }

    public Deathdate(PartialDate partialDate) {
        super(partialDate);
    }

    public Deathdate(String str) {
        super(str);
    }
}
