package ezvcard.property;

import ezvcard.parameter.MediaTypeParameter;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

/* loaded from: classes3.dex */
public abstract class BinaryProperty<T extends MediaTypeParameter> extends VCardProperty {
    protected T contentType;
    protected byte[] data;
    protected String url;

    public BinaryProperty() {
    }

    public BinaryProperty(String str, T t) {
        setUrl(str, t);
    }

    public BinaryProperty(byte[] bArr, T t) {
        setData(bArr, t);
    }

    public void setData(byte[] bArr, T t) {
        this.url = null;
        this.data = bArr;
        setContentType(t);
    }

    public void setUrl(String str, T t) {
        this.url = str;
        this.data = null;
        setContentType(t);
    }

    public void setContentType(T t) {
        this.contentType = t;
    }

    @Override // ezvcard.property.VCardProperty
    public Map<String, Object> toStringValues() {
        String str;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (this.data == null) {
            str = "null";
        } else {
            str = "length: " + this.data.length;
        }
        linkedHashMap.put("data", str);
        linkedHashMap.put("url", this.url);
        linkedHashMap.put("contentType", this.contentType);
        return linkedHashMap;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        T t = this.contentType;
        int i = 0;
        int hashCode2 = (((hashCode + (t == null ? 0 : t.hashCode())) * 31) + Arrays.hashCode(this.data)) * 31;
        String str = this.url;
        if (str != null) {
            i = str.hashCode();
        }
        return hashCode2 + i;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        BinaryProperty binaryProperty = (BinaryProperty) obj;
        T t = this.contentType;
        if (t == null) {
            if (binaryProperty.contentType != null) {
                return false;
            }
        } else if (!t.equals(binaryProperty.contentType)) {
            return false;
        }
        if (!Arrays.equals(this.data, binaryProperty.data)) {
            return false;
        }
        String str = this.url;
        if (str == null) {
            if (binaryProperty.url != null) {
                return false;
            }
        } else if (!str.equals(binaryProperty.url)) {
            return false;
        }
        return true;
    }
}
