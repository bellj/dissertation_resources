package ezvcard.property;

import ezvcard.parameter.AddressType;
import ezvcard.parameter.VCardParameters;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/* loaded from: classes3.dex */
public class Address extends VCardProperty {
    private final List<String> countries = new ArrayList(1);
    private final List<String> extendedAddresses = new ArrayList(1);
    private final List<String> localities = new ArrayList(1);
    private final List<String> poBoxes = new ArrayList(1);
    private final List<String> postalCodes = new ArrayList(1);
    private final List<String> regions = new ArrayList(1);
    private final List<String> streetAddresses = new ArrayList(1);

    public String getPoBox() {
        return first(this.poBoxes);
    }

    public List<String> getPoBoxes() {
        return this.poBoxes;
    }

    public List<String> getExtendedAddresses() {
        return this.extendedAddresses;
    }

    public String getStreetAddress() {
        return first(this.streetAddresses);
    }

    public List<String> getStreetAddresses() {
        return this.streetAddresses;
    }

    public String getLocality() {
        return first(this.localities);
    }

    public List<String> getLocalities() {
        return this.localities;
    }

    public String getRegion() {
        return first(this.regions);
    }

    public List<String> getRegions() {
        return this.regions;
    }

    public String getPostalCode() {
        return first(this.postalCodes);
    }

    public List<String> getPostalCodes() {
        return this.postalCodes;
    }

    public String getCountry() {
        return first(this.countries);
    }

    public List<String> getCountries() {
        return this.countries;
    }

    public List<AddressType> getTypes() {
        VCardParameters vCardParameters = this.parameters;
        vCardParameters.getClass();
        return new VCardParameters.TypeParameterList<AddressType>(vCardParameters) { // from class: ezvcard.property.Address.1
            {
                r2.getClass();
            }

            @Override // ezvcard.parameter.VCardParameters.VCardParameterList
            public AddressType _asObject(String str) {
                return AddressType.get(str);
            }
        };
    }

    public String getLabel() {
        return this.parameters.getLabel();
    }

    public void setLabel(String str) {
        this.parameters.setLabel(str);
    }

    @Override // ezvcard.property.VCardProperty
    protected Map<String, Object> toStringValues() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("poBoxes", this.poBoxes);
        linkedHashMap.put("extendedAddresses", this.extendedAddresses);
        linkedHashMap.put("streetAddresses", this.streetAddresses);
        linkedHashMap.put("localities", this.localities);
        linkedHashMap.put("regions", this.regions);
        linkedHashMap.put("postalCodes", this.postalCodes);
        linkedHashMap.put("countries", this.countries);
        return linkedHashMap;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public int hashCode() {
        return (((((((((((((super.hashCode() * 31) + this.countries.hashCode()) * 31) + this.extendedAddresses.hashCode()) * 31) + this.localities.hashCode()) * 31) + this.poBoxes.hashCode()) * 31) + this.postalCodes.hashCode()) * 31) + this.regions.hashCode()) * 31) + this.streetAddresses.hashCode();
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        Address address = (Address) obj;
        return this.countries.equals(address.countries) && this.extendedAddresses.equals(address.extendedAddresses) && this.localities.equals(address.localities) && this.poBoxes.equals(address.poBoxes) && this.postalCodes.equals(address.postalCodes) && this.regions.equals(address.regions) && this.streetAddresses.equals(address.streetAddresses);
    }

    private static String first(List<String> list) {
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }
}
