package ezvcard.property;

import ezvcard.util.GeoUri;
import java.util.LinkedHashMap;
import java.util.Map;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* loaded from: classes3.dex */
public class PlaceProperty extends VCardProperty {
    protected GeoUri geoUri;
    protected String text;
    protected String uri;

    public void setGeoUri(GeoUri geoUri) {
        this.geoUri = geoUri;
        this.uri = null;
        this.text = null;
    }

    public void setUri(String str) {
        this.uri = str;
        this.geoUri = null;
        this.text = null;
    }

    public void setText(String str) {
        this.text = str;
        this.geoUri = null;
        this.uri = null;
    }

    @Override // ezvcard.property.VCardProperty
    protected Map<String, Object> toStringValues() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("geoUri", this.geoUri);
        linkedHashMap.put("uri", this.uri);
        linkedHashMap.put(DraftDatabase.Draft.TEXT, this.text);
        return linkedHashMap;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        GeoUri geoUri = this.geoUri;
        int i = 0;
        int hashCode2 = (hashCode + (geoUri == null ? 0 : geoUri.hashCode())) * 31;
        String str = this.text;
        int hashCode3 = (hashCode2 + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.uri;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode3 + i;
    }

    @Override // ezvcard.property.VCardProperty, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        PlaceProperty placeProperty = (PlaceProperty) obj;
        GeoUri geoUri = this.geoUri;
        if (geoUri == null) {
            if (placeProperty.geoUri != null) {
                return false;
            }
        } else if (!geoUri.equals(placeProperty.geoUri)) {
            return false;
        }
        String str = this.text;
        if (str == null) {
            if (placeProperty.text != null) {
                return false;
            }
        } else if (!str.equals(placeProperty.text)) {
            return false;
        }
        String str2 = this.uri;
        if (str2 == null) {
            if (placeProperty.uri != null) {
                return false;
            }
        } else if (!str2.equals(placeProperty.uri)) {
            return false;
        }
        return true;
    }
}
