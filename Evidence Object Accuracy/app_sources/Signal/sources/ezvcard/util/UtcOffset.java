package ezvcard.util;

import ezvcard.Messages;

/* loaded from: classes3.dex */
public final class UtcOffset {
    private final long millis;

    private static long hoursToMillis(long j) {
        return j * 60 * 60 * 1000;
    }

    private static long minutesToMillis(long j) {
        return j * 60 * 1000;
    }

    public UtcOffset(boolean z, int i, int i2) {
        this.millis = ((long) (z ? 1 : -1)) * (hoursToMillis((long) Math.abs(i)) + minutesToMillis((long) Math.abs(i2)));
    }

    public static UtcOffset parse(String str) {
        boolean z;
        int i;
        String str2;
        int i2 = 0;
        char charAt = str.charAt(0);
        if (charAt == '-') {
            i = 1;
            z = false;
        } else {
            i = charAt == '+' ? 1 : 0;
            z = true;
        }
        int i3 = i + 4;
        int indexOf = str.indexOf(58, i);
        if (indexOf >= 0) {
            i3++;
        }
        if (str.length() <= i3) {
            String str3 = null;
            if (indexOf < 0) {
                str2 = str.substring(i);
                int length = str2.length() - 2;
                if (length > 0) {
                    str3 = str2.substring(length);
                    str2 = str2.substring(0, length);
                }
            } else {
                str2 = str.substring(i, indexOf);
                if (indexOf < str.length() - 1) {
                    str3 = str.substring(indexOf + 1);
                }
            }
            try {
                int parseInt = Integer.parseInt(str2);
                if (str3 != null) {
                    i2 = Integer.parseInt(str3);
                }
                return new UtcOffset(z, parseInt, i2);
            } catch (NumberFormatException unused) {
                throw Messages.INSTANCE.getIllegalArgumentException(40, str);
            }
        } else {
            throw Messages.INSTANCE.getIllegalArgumentException(40, str);
        }
    }

    public String toString() {
        return toString(false);
    }

    public String toString(boolean z) {
        StringBuilder sb = new StringBuilder();
        long j = this.millis;
        boolean z2 = j >= 0;
        long abs = Math.abs(millisToHours(j));
        long abs2 = Math.abs(millisToMinutes(this.millis));
        sb.append(z2 ? '+' : '-');
        if (abs < 10) {
            sb.append('0');
        }
        sb.append(abs);
        if (z) {
            sb.append(':');
        }
        if (abs2 < 10) {
            sb.append('0');
        }
        sb.append(abs2);
        return sb.toString();
    }

    public int hashCode() {
        long j = this.millis;
        return 31 + ((int) (j ^ (j >>> 32)));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && UtcOffset.class == obj.getClass() && this.millis == ((UtcOffset) obj).millis;
    }

    private static long millisToHours(long j) {
        return ((j / 1000) / 60) / 60;
    }

    private static long millisToMinutes(long j) {
        return ((j / 1000) / 60) % 60;
    }
}
