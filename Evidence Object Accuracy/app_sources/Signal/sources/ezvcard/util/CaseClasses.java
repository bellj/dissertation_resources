package ezvcard.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/* loaded from: classes3.dex */
public abstract class CaseClasses<T, V> {
    protected final Class<T> clazz;
    private volatile Collection<T> preDefined = null;
    private Collection<T> runtimeDefined = null;

    protected abstract T create(V v);

    protected abstract boolean matches(T t, V v);

    public CaseClasses(Class<T> cls) {
        this.clazz = cls;
    }

    public T find(V v) {
        checkInit();
        for (T t : this.preDefined) {
            if (matches(t, v)) {
                return t;
            }
        }
        return null;
    }

    public T get(V v) {
        T find = find(v);
        if (find != null) {
            return find;
        }
        synchronized (this.runtimeDefined) {
            for (T t : this.runtimeDefined) {
                if (matches(t, v)) {
                    return t;
                }
            }
            T create = create(v);
            this.runtimeDefined.add(create);
            return create;
        }
    }

    private void checkInit() {
        if (this.preDefined == null) {
            synchronized (this) {
                if (this.preDefined == null) {
                    init();
                }
            }
        }
    }

    private void init() {
        ArrayList arrayList = new ArrayList();
        Field[] fields = this.clazz.getFields();
        for (Field field : fields) {
            int modifiers = field.getModifiers();
            if (Modifier.isStatic(modifiers) && Modifier.isPublic(modifiers) && field.getDeclaringClass() == this.clazz && field.getType() == this.clazz) {
                try {
                    Object obj = field.get(null);
                    if (obj != null) {
                        arrayList.add(this.clazz.cast(obj));
                    }
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        }
        this.runtimeDefined = new ArrayList(0);
        this.preDefined = Collections.unmodifiableCollection(arrayList);
    }
}
