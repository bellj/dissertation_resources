package ezvcard.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/* loaded from: classes3.dex */
public class VCardFloatFormatter extends DecimalFormat {
    public VCardFloatFormatter(int i) {
        setMaximumFractionDigits(i);
        if (i > 0) {
            setMinimumFractionDigits(1);
        }
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setDecimalSeparator('.');
        decimalFormatSymbols.setMinusSign('-');
        setDecimalFormatSymbols(decimalFormatSymbols);
    }
}
