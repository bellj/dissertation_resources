package ezvcard.util;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/* loaded from: classes3.dex */
public final class XmlUtils {
    public static Document toDocument(String str) throws SAXException {
        try {
            return toDocument(new StringReader(str));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Document toDocument(Reader reader) throws SAXException, IOException {
        return toDocument(new InputSource(reader));
    }

    private static Document toDocument(InputSource inputSource) throws SAXException, IOException {
        DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
        newInstance.setNamespaceAware(true);
        newInstance.setIgnoringComments(true);
        applyXXEProtection(newInstance);
        try {
            return newInstance.newDocumentBuilder().parse(inputSource);
        } catch (ParserConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

    public static void applyXXEProtection(DocumentBuilderFactory documentBuilderFactory) {
        HashMap hashMap = new HashMap();
        hashMap.put("http://apache.org/xml/features/disallow-doctype-decl", Boolean.TRUE);
        Boolean bool = Boolean.FALSE;
        hashMap.put("http://xml.org/sax/features/external-general-entities", bool);
        hashMap.put("http://xml.org/sax/features/external-parameter-entities", bool);
        hashMap.put("http://apache.org/xml/features/nonvalidating/load-external-dtd", bool);
        for (Map.Entry entry : hashMap.entrySet()) {
            try {
                documentBuilderFactory.setFeature((String) entry.getKey(), ((Boolean) entry.getValue()).booleanValue());
            } catch (ParserConfigurationException unused) {
            }
        }
        documentBuilderFactory.setXIncludeAware(false);
        documentBuilderFactory.setExpandEntityReferences(false);
    }

    public static String toString(Node node) {
        return toString(node, new HashMap());
    }

    public static String toString(Node node, Map<String, String> map) {
        try {
            StringWriter stringWriter = new StringWriter();
            toWriter(node, stringWriter, map);
            return stringWriter.toString();
        } catch (TransformerException e) {
            throw new RuntimeException(e);
        }
    }

    public static void toWriter(Node node, Writer writer, Map<String, String> map) throws TransformerException {
        try {
            Transformer newTransformer = TransformerFactory.newInstance().newTransformer();
            assignOutputProperties(newTransformer, map);
            newTransformer.transform(new DOMSource(node), new StreamResult(writer));
        } catch (TransformerConfigurationException e) {
            throw new RuntimeException(e);
        } catch (TransformerFactoryConfigurationError e2) {
            throw new RuntimeException(e2);
        }
    }

    public static void assignOutputProperties(Transformer transformer, Map<String, String> map) {
        for (Map.Entry<String, String> entry : map.entrySet()) {
            try {
                transformer.setOutputProperty(entry.getKey(), entry.getValue());
            } catch (IllegalArgumentException unused) {
            }
        }
    }
}
