package ezvcard.util;

import ezvcard.Messages;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* loaded from: classes3.dex */
public final class PartialDate {
    private static final Format[] dateFormats = {new Format("(\\d{4})", 0), new Format("(\\d{4})-(\\d{2})", 0, 1), new Format("(\\d{4})-?(\\d{2})-?(\\d{2})", 0, 1, 2), new Format("--(\\d{2})-?(\\d{2})", 1, 2), new Format("--(\\d{2})", 1), new Format("---(\\d{2})", 2)};
    private static final Format[] timeFormats = {new Format("(\\d{2})(([-+]\\d{1,2}):?(\\d{2})?)?", 3, null, 6, 7), new Format("(\\d{2}):?(\\d{2})(([-+]\\d{1,2}):?(\\d{2})?)?", 3, 4, null, 6, 7), new Format("(\\d{2}):?(\\d{2}):?(\\d{2})(([-+]\\d{1,2}):?(\\d{2})?)?", 3, 4, 5, null, 6, 7), new Format("-(\\d{2}):?(\\d{2})(([-+]\\d{1,2}):?(\\d{2})?)?", 4, 5, null, 6, 7), new Format("-(\\d{2})(([-+]\\d{1,2}):?(\\d{2})?)?", 4, null, 6, 7), new Format("--(\\d{2})(([-+]\\d{1,2}):?(\\d{2})?)?", 5, null, 6, 7)};
    private final Integer[] components;
    private final UtcOffset offset;

    private PartialDate(Integer[] numArr, UtcOffset utcOffset) {
        this.components = numArr;
        this.offset = utcOffset;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002f, code lost:
        if (parseTime(r4, r0) == false) goto L_0x0032;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x004b, code lost:
        if (parseTime(r1, r0) != false) goto L_0x0034;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static ezvcard.util.PartialDate parse(java.lang.String r6) {
        /*
            r0 = 84
            int r0 = r6.indexOf(r0)
            r1 = 0
            r2 = 0
            r3 = 1
            if (r0 >= 0) goto L_0x000d
            r4 = r6
            goto L_0x001e
        L_0x000d:
            java.lang.String r4 = r6.substring(r2, r0)
            int r5 = r6.length()
            int r5 = r5 - r3
            if (r0 >= r5) goto L_0x001e
            int r0 = r0 + r3
            java.lang.String r0 = r6.substring(r0)
            r1 = r0
        L_0x001e:
            ezvcard.util.PartialDate$Builder r0 = new ezvcard.util.PartialDate$Builder
            r0.<init>()
            if (r1 != 0) goto L_0x0036
            boolean r1 = parseDate(r4, r0)
            if (r1 != 0) goto L_0x0034
            boolean r1 = parseTime(r4, r0)
            if (r1 == 0) goto L_0x0032
            goto L_0x0034
        L_0x0032:
            r1 = 0
            goto L_0x004e
        L_0x0034:
            r1 = 1
            goto L_0x004e
        L_0x0036:
            int r5 = r4.length()
            if (r5 != 0) goto L_0x0041
            boolean r1 = parseTime(r1, r0)
            goto L_0x004e
        L_0x0041:
            boolean r4 = parseDate(r4, r0)
            if (r4 == 0) goto L_0x0032
            boolean r1 = parseTime(r1, r0)
            if (r1 == 0) goto L_0x0032
            goto L_0x0034
        L_0x004e:
            if (r1 == 0) goto L_0x0055
            ezvcard.util.PartialDate r6 = r0.build()
            return r6
        L_0x0055:
            ezvcard.Messages r0 = ezvcard.Messages.INSTANCE
            r1 = 36
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r3[r2] = r6
            java.lang.IllegalArgumentException r6 = r0.getIllegalArgumentException(r1, r3)
            goto L_0x0063
        L_0x0062:
            throw r6
        L_0x0063:
            goto L_0x0062
        */
        throw new UnsupportedOperationException("Method not decompiled: ezvcard.util.PartialDate.parse(java.lang.String):ezvcard.util.PartialDate");
    }

    private static boolean parseDate(String str, Builder builder) {
        return parseFormats(str, builder, dateFormats);
    }

    private static boolean parseTime(String str, Builder builder) {
        return parseFormats(str, builder, timeFormats);
    }

    private static boolean parseFormats(String str, Builder builder, Format[] formatArr) {
        for (Format format : formatArr) {
            if (format.parse(builder, str)) {
                return true;
            }
        }
        return false;
    }

    public Integer getYear() {
        return this.components[0];
    }

    private boolean hasYear() {
        return getYear() != null;
    }

    public Integer getMonth() {
        return this.components[1];
    }

    private boolean hasMonth() {
        return getMonth() != null;
    }

    public Integer getDate() {
        return this.components[2];
    }

    private boolean hasDate() {
        return getDate() != null;
    }

    public Integer getHour() {
        return this.components[3];
    }

    private boolean hasHour() {
        return getHour() != null;
    }

    public Integer getMinute() {
        return this.components[4];
    }

    private boolean hasMinute() {
        return getMinute() != null;
    }

    public Integer getSecond() {
        return this.components[5];
    }

    private boolean hasSecond() {
        return getSecond() != null;
    }

    private boolean hasUtcOffset() {
        return this.offset != null;
    }

    public boolean hasTimeComponent() {
        return hasHour() || hasMinute() || hasSecond();
    }

    public String toISO8601(boolean z) {
        StringBuilder sb = new StringBuilder();
        DecimalFormat decimalFormat = new DecimalFormat("00");
        String str = null;
        String num = hasYear() ? getYear().toString() : null;
        String format = hasMonth() ? decimalFormat.format(getMonth()) : null;
        String format2 = hasDate() ? decimalFormat.format(getDate()) : null;
        String str2 = "";
        String str3 = z ? "-" : str2;
        if (hasYear() && !hasMonth() && !hasDate()) {
            sb.append(num);
        } else if (!hasYear() && hasMonth() && !hasDate()) {
            sb.append("--");
            sb.append(format);
        } else if (!hasYear() && !hasMonth() && hasDate()) {
            sb.append("---");
            sb.append(format2);
        } else if (hasYear() && hasMonth() && !hasDate()) {
            sb.append(num);
            sb.append("-");
            sb.append(format);
        } else if (!hasYear() && hasMonth() && hasDate()) {
            sb.append("--");
            sb.append(format);
            sb.append(str3);
            sb.append(format2);
        } else if (hasYear() && !hasMonth() && hasDate()) {
            throw new IllegalStateException(Messages.INSTANCE.getExceptionMessage(38, new Object[0]));
        } else if (hasYear() && hasMonth() && hasDate()) {
            sb.append(num);
            sb.append(str3);
            sb.append(format);
            sb.append(str3);
            sb.append(format2);
        }
        if (hasTimeComponent()) {
            sb.append('T');
            String format3 = hasHour() ? decimalFormat.format(getHour()) : null;
            String format4 = hasMinute() ? decimalFormat.format(getMinute()) : null;
            if (hasSecond()) {
                str = decimalFormat.format(getSecond());
            }
            if (z) {
                str2 = ":";
            }
            if (hasHour() && !hasMinute() && !hasSecond()) {
                sb.append(format3);
            } else if (!hasHour() && hasMinute() && !hasSecond()) {
                sb.append("-");
                sb.append(format4);
            } else if (!hasHour() && !hasMinute() && hasSecond()) {
                sb.append("--");
                sb.append(str);
            } else if (hasHour() && hasMinute() && !hasSecond()) {
                sb.append(format3);
                sb.append(str2);
                sb.append(format4);
            } else if (!hasHour() && hasMinute() && hasSecond()) {
                sb.append("-");
                sb.append(format4);
                sb.append(str2);
                sb.append(str);
            } else if (hasHour() && !hasMinute() && hasSecond()) {
                throw new IllegalStateException(Messages.INSTANCE.getExceptionMessage(39, new Object[0]));
            } else if (hasHour() && hasMinute() && hasSecond()) {
                sb.append(format3);
                sb.append(str2);
                sb.append(format4);
                sb.append(str2);
                sb.append(str);
            }
            if (hasUtcOffset()) {
                sb.append(this.offset.toString(z));
            }
        }
        return sb.toString();
    }

    public int hashCode() {
        int hashCode = (Arrays.hashCode(this.components) + 31) * 31;
        UtcOffset utcOffset = this.offset;
        return hashCode + (utcOffset == null ? 0 : utcOffset.hashCode());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || PartialDate.class != obj.getClass()) {
            return false;
        }
        PartialDate partialDate = (PartialDate) obj;
        if (!Arrays.equals(this.components, partialDate.components)) {
            return false;
        }
        UtcOffset utcOffset = this.offset;
        if (utcOffset == null) {
            if (partialDate.offset != null) {
                return false;
            }
        } else if (!utcOffset.equals(partialDate.offset)) {
            return false;
        }
        return true;
    }

    public String toString() {
        return toISO8601(true);
    }

    /* loaded from: classes3.dex */
    public static class Format {
        private Integer[] componentIndexes;
        private Pattern regex;

        public Format(String str, Integer... numArr) {
            this.regex = Pattern.compile('^' + str + '$');
            this.componentIndexes = numArr;
        }

        public boolean parse(Builder builder, String str) {
            String group;
            Matcher matcher = this.regex.matcher(str);
            if (!matcher.find()) {
                return false;
            }
            Integer num = null;
            Integer num2 = null;
            int i = 0;
            boolean z = false;
            while (true) {
                Integer[] numArr = this.componentIndexes;
                if (i >= numArr.length) {
                    break;
                }
                Integer num3 = numArr[i];
                if (!(num3 == null || (group = matcher.group(i + 1)) == null)) {
                    boolean startsWith = group.startsWith("+");
                    if (startsWith) {
                        group = group.substring(1);
                    }
                    int parseInt = Integer.parseInt(group);
                    if (num3.intValue() == 6) {
                        num = Integer.valueOf(parseInt);
                        z = startsWith;
                    } else if (num3.intValue() == 7) {
                        num2 = Integer.valueOf(parseInt);
                    } else {
                        builder.components[num3.intValue()] = Integer.valueOf(parseInt);
                    }
                }
                i++;
            }
            if (num != null) {
                if (num2 == null) {
                    num2 = 0;
                }
                builder.offset = new UtcOffset(z, num.intValue(), num2.intValue());
            }
            return true;
        }
    }

    /* loaded from: classes3.dex */
    public static class Builder {
        private final Integer[] components = new Integer[6];
        private UtcOffset offset;

        public PartialDate build() {
            Integer[] numArr = this.components;
            if (numArr[0] != null && numArr[1] == null && numArr[2] != null) {
                throw Messages.INSTANCE.getIllegalArgumentException(38, new Object[0]);
            } else if (numArr[3] == null || numArr[4] != null || numArr[5] == null) {
                return new PartialDate(this.components, this.offset);
            } else {
                throw Messages.INSTANCE.getIllegalArgumentException(39, new Object[0]);
            }
        }
    }
}
