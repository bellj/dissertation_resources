package ezvcard.util;

/* loaded from: classes3.dex */
public class ClearableStringBuilder {
    private final StringBuilder sb = new StringBuilder();

    public ClearableStringBuilder clear() {
        this.sb.setLength(0);
        return this;
    }

    public String get() {
        return this.sb.toString();
    }

    public String getAndClear() {
        String str = get();
        clear();
        return str;
    }

    public ClearableStringBuilder append(char c) {
        this.sb.append(c);
        return this;
    }

    public ClearableStringBuilder append(CharSequence charSequence) {
        this.sb.append(charSequence);
        return this;
    }

    public ClearableStringBuilder chop() {
        StringBuilder sb = this.sb;
        sb.setLength(sb.length() - 1);
        return this;
    }

    public int length() {
        return this.sb.length();
    }
}
