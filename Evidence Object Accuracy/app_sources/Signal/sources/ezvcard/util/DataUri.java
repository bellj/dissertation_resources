package ezvcard.util;

import ezvcard.Messages;
import ezvcard.util.org.apache.commons.codec.binary.Base64;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

/* loaded from: classes3.dex */
public final class DataUri {
    private final String contentType;
    private final byte[] data;
    private final String text;

    private DataUri(String str, byte[] bArr, String str2) {
        String str3;
        if (str == null) {
            str3 = "";
        } else {
            str3 = str.toLowerCase();
        }
        this.contentType = str3;
        this.data = bArr;
        this.text = str2;
    }

    public static DataUri parse(String str) {
        String str2;
        int i = 5;
        if (str.length() < 5 || !str.substring(0, 5).equalsIgnoreCase("data:")) {
            throw Messages.INSTANCE.getIllegalArgumentException(18, "data:");
        }
        byte[] bArr = null;
        String str3 = null;
        String str4 = null;
        int i2 = 5;
        boolean z = false;
        while (true) {
            if (i >= str.length()) {
                str2 = null;
                break;
            }
            char charAt = str.charAt(i);
            if (charAt == ';') {
                String substring = str.substring(i2, i);
                if (str3 == null) {
                    str3 = substring.toLowerCase();
                } else if (substring.toLowerCase().startsWith("charset=")) {
                    str4 = substring.substring(substring.indexOf(61) + 1);
                } else if ("base64".equalsIgnoreCase(substring)) {
                    z = true;
                }
                i2 = i + 1;
            } else if (charAt == ',') {
                String substring2 = str.substring(i2, i);
                if (str3 == null) {
                    str3 = substring2.toLowerCase();
                } else if (substring2.toLowerCase().startsWith("charset=")) {
                    str4 = substring2.substring(substring2.indexOf(61) + 1);
                } else if ("base64".equalsIgnoreCase(substring2)) {
                    z = true;
                }
                str2 = str.substring(i + 1);
            }
            i++;
        }
        if (str2 != null) {
            if (z) {
                byte[] decodeBase64 = Base64.decodeBase64(str2.replaceAll("\\s", ""));
                if (str4 != null) {
                    try {
                        str2 = new String(decodeBase64, str4);
                    } catch (UnsupportedEncodingException e) {
                        throw new IllegalArgumentException(Messages.INSTANCE.getExceptionMessage(43, str4), e);
                    }
                } else {
                    bArr = decodeBase64;
                    str2 = null;
                }
            }
            return new DataUri(str3, bArr, str2);
        }
        throw Messages.INSTANCE.getIllegalArgumentException(20, new Object[0]);
    }

    public byte[] getData() {
        return this.data;
    }

    public String getContentType() {
        return this.contentType;
    }

    public String toString() {
        return toString(null);
    }

    public String toString(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("data:");
        sb.append(this.contentType);
        if (this.data != null) {
            sb.append(";base64,");
            sb.append(Base64.encodeBase64String(this.data));
        } else {
            String str2 = this.text;
            if (str2 == null) {
                sb.append(',');
            } else if (str == null) {
                sb.append(',');
                sb.append(this.text);
            } else {
                try {
                    byte[] bytes = str2.getBytes(str);
                    sb.append(";charset=");
                    sb.append(str);
                    sb.append(";base64,");
                    sb.append(Base64.encodeBase64String(bytes));
                } catch (UnsupportedEncodingException e) {
                    throw new IllegalArgumentException(Messages.INSTANCE.getExceptionMessage(44, str), e);
                }
            }
        }
        return sb.toString();
    }

    public int hashCode() {
        int hashCode = (((this.contentType.hashCode() + 31) * 31) + Arrays.hashCode(this.data)) * 31;
        String str = this.text;
        return hashCode + (str == null ? 0 : str.hashCode());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || DataUri.class != obj.getClass()) {
            return false;
        }
        DataUri dataUri = (DataUri) obj;
        if (!this.contentType.equals(dataUri.contentType) || !Arrays.equals(this.data, dataUri.data)) {
            return false;
        }
        String str = this.text;
        if (str == null) {
            if (dataUri.text != null) {
                return false;
            }
        } else if (!str.equals(dataUri.text)) {
            return false;
        }
        return true;
    }
}
