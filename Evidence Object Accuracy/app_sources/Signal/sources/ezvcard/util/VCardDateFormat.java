package ezvcard.util;

import ezvcard.Messages;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Pattern;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* loaded from: classes3.dex */
public class VCardDateFormat extends Enum<VCardDateFormat> {
    protected final String formatStr;
    private final Pattern pattern;

    private VCardDateFormat(String str, int i, String str2, String str3) {
        this.pattern = Pattern.compile(str2);
        this.formatStr = str3;
    }

    public boolean matches(String str) {
        return this.pattern.matcher(str).matches();
    }

    public DateFormat getDateFormat() {
        return getDateFormat(null);
    }

    public DateFormat getDateFormat(TimeZone timeZone) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(this.formatStr);
        if (timeZone != null) {
            simpleDateFormat.setTimeZone(timeZone);
        }
        return simpleDateFormat;
    }

    public static VCardDateFormat find(String str) {
        VCardDateFormat[] values = values();
        for (VCardDateFormat vCardDateFormat : values) {
            if (vCardDateFormat.matches(str)) {
                return vCardDateFormat;
            }
        }
        return null;
    }

    public static Date parse(String str) {
        VCardDateFormat find = find(str);
        if (find != null) {
            try {
                return find.getDateFormat().parse(str);
            } catch (ParseException unused) {
                throw Messages.INSTANCE.getIllegalArgumentException(41, str);
            }
        } else {
            throw Messages.INSTANCE.getIllegalArgumentException(41, str);
        }
    }
}
