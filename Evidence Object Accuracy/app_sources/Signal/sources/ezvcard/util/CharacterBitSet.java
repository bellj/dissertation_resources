package ezvcard.util;

import java.util.BitSet;

/* loaded from: classes3.dex */
public class CharacterBitSet {
    private final BitSet bitSet = new BitSet(128);
    private final String characters;

    public CharacterBitSet(String str) {
        this.characters = str;
        int i = 0;
        while (i < str.length()) {
            char charAt = str.charAt(i);
            if ((i < str.length() + -2 ? str.charAt(i + 1) : 0) == '-') {
                i += 2;
                char charAt2 = str.charAt(i);
                if (charAt > charAt2) {
                    charAt2 = charAt;
                    charAt = charAt2;
                }
                this.bitSet.set(charAt, charAt2 + 1);
            } else {
                this.bitSet.set(charAt);
            }
            i++;
        }
    }

    public int hashCode() {
        return this.bitSet.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            return this.bitSet.equals(((CharacterBitSet) obj).bitSet);
        }
        return false;
    }

    public String toString() {
        return this.characters;
    }
}
