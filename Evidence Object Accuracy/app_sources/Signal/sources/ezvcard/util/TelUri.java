package ezvcard.util;

import ezvcard.Messages;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* loaded from: classes3.dex */
public final class TelUri {
    private static final Pattern hexPattern = Pattern.compile("(?i)%([0-9a-f]{2})");
    private static final boolean[] validParameterValueCharacters = new boolean[128];
    private final String extension;
    private final String isdnSubaddress;
    private final String number;
    private final Map<String, String> parameters;
    private final String phoneContext;

    static {
        validParameterValueCharacters = new boolean[128];
        for (int i = 48; i <= 57; i++) {
            validParameterValueCharacters[i] = true;
        }
        for (int i2 = 65; i2 <= 90; i2++) {
            validParameterValueCharacters[i2] = true;
        }
        for (int i3 = 97; i3 <= 122; i3++) {
            validParameterValueCharacters[i3] = true;
        }
        for (int i4 = 0; i4 < 16; i4++) {
            validParameterValueCharacters["!$&'()*+-.:[]_~/".charAt(i4)] = true;
        }
        hexPattern = Pattern.compile("(?i)%([0-9a-f]{2})");
    }

    private TelUri(Builder builder) {
        this.number = builder.number;
        this.extension = builder.extension;
        this.isdnSubaddress = builder.isdnSubaddress;
        this.phoneContext = builder.phoneContext;
        this.parameters = Collections.unmodifiableMap(builder.parameters);
    }

    public static TelUri parse(String str) {
        if (str.length() < 4 || !str.substring(0, 4).equalsIgnoreCase("tel:")) {
            throw Messages.INSTANCE.getIllegalArgumentException(18, "tel:");
        }
        Builder builder = new Builder();
        ClearableStringBuilder clearableStringBuilder = new ClearableStringBuilder();
        String str2 = null;
        for (int i = 4; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt == '=' && builder.number != null && str2 == null) {
                str2 = clearableStringBuilder.getAndClear();
            } else if (charAt == ';') {
                handleEndOfParameter(clearableStringBuilder, str2, builder);
                str2 = null;
            } else {
                clearableStringBuilder.append(charAt);
            }
        }
        handleEndOfParameter(clearableStringBuilder, str2, builder);
        return builder.build();
    }

    private static void addParameter(String str, String str2, Builder builder) {
        String decodeParameterValue = decodeParameterValue(str2);
        if ("ext".equalsIgnoreCase(str)) {
            builder.extension = decodeParameterValue;
        } else if ("isub".equalsIgnoreCase(str)) {
            builder.isdnSubaddress = decodeParameterValue;
        } else if ("phone-context".equalsIgnoreCase(str)) {
            builder.phoneContext = decodeParameterValue;
        } else {
            builder.parameters.put(str, decodeParameterValue);
        }
    }

    private static void handleEndOfParameter(ClearableStringBuilder clearableStringBuilder, String str, Builder builder) {
        String andClear = clearableStringBuilder.getAndClear();
        if (builder.number == null) {
            builder.number = andClear;
        } else if (str != null) {
            addParameter(str, andClear, builder);
        } else if (andClear.length() > 0) {
            addParameter(andClear, "", builder);
        }
    }

    public String getNumber() {
        return this.number;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("tel:");
        sb.append(this.number);
        String str = this.extension;
        if (str != null) {
            writeParameter("ext", str, sb);
        }
        String str2 = this.isdnSubaddress;
        if (str2 != null) {
            writeParameter("isub", str2, sb);
        }
        String str3 = this.phoneContext;
        if (str3 != null) {
            writeParameter("phone-context", str3, sb);
        }
        for (Map.Entry<String, String> entry : this.parameters.entrySet()) {
            writeParameter(entry.getKey(), entry.getValue(), sb);
        }
        return sb.toString();
    }

    private static void writeParameter(String str, String str2, StringBuilder sb) {
        sb.append(';');
        sb.append(str);
        sb.append('=');
        sb.append(encodeParameterValue(str2));
    }

    public int hashCode() {
        String str = this.extension;
        int i = 0;
        int hashCode = ((str == null ? 0 : str.toLowerCase().hashCode()) + 31) * 31;
        String str2 = this.isdnSubaddress;
        int hashCode2 = (hashCode + (str2 == null ? 0 : str2.toLowerCase().hashCode())) * 31;
        String str3 = this.number;
        int hashCode3 = (hashCode2 + (str3 == null ? 0 : str3.toLowerCase().hashCode())) * 31;
        Map<String, String> map = this.parameters;
        int hashCode4 = (hashCode3 + (map == null ? 0 : StringUtils.toLowerCase(map).hashCode())) * 31;
        String str4 = this.phoneContext;
        if (str4 != null) {
            i = str4.toLowerCase().hashCode();
        }
        return hashCode4 + i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || TelUri.class != obj.getClass()) {
            return false;
        }
        TelUri telUri = (TelUri) obj;
        String str = this.extension;
        if (str == null) {
            if (telUri.extension != null) {
                return false;
            }
        } else if (!str.equalsIgnoreCase(telUri.extension)) {
            return false;
        }
        String str2 = this.isdnSubaddress;
        if (str2 == null) {
            if (telUri.isdnSubaddress != null) {
                return false;
            }
        } else if (!str2.equalsIgnoreCase(telUri.isdnSubaddress)) {
            return false;
        }
        String str3 = this.number;
        if (str3 == null) {
            if (telUri.number != null) {
                return false;
            }
        } else if (!str3.equalsIgnoreCase(telUri.number)) {
            return false;
        }
        Map<String, String> map = this.parameters;
        if (map == null) {
            if (telUri.parameters != null) {
                return false;
            }
        } else if (telUri.parameters == null || map.size() != telUri.parameters.size() || !StringUtils.toLowerCase(this.parameters).equals(StringUtils.toLowerCase(telUri.parameters))) {
            return false;
        }
        String str4 = this.phoneContext;
        if (str4 == null) {
            if (telUri.phoneContext != null) {
                return false;
            }
        } else if (!str4.equalsIgnoreCase(telUri.phoneContext)) {
            return false;
        }
        return true;
    }

    private static String encodeParameterValue(String str) {
        StringBuilder sb = null;
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            boolean[] zArr = validParameterValueCharacters;
            if (charAt >= zArr.length || !zArr[charAt]) {
                if (sb == null) {
                    sb = new StringBuilder(str.length() * 2);
                    sb.append(str.substring(0, i));
                }
                String num = Integer.toString(charAt, 16);
                sb.append('%');
                sb.append(num);
            } else if (sb != null) {
                sb.append(charAt);
            }
        }
        return sb == null ? str : sb.toString();
    }

    private static String decodeParameterValue(String str) {
        Matcher matcher = hexPattern.matcher(str);
        StringBuffer stringBuffer = null;
        while (matcher.find()) {
            if (stringBuffer == null) {
                stringBuffer = new StringBuffer(str.length());
            }
            matcher.appendReplacement(stringBuffer, Character.toString((char) Integer.parseInt(matcher.group(1), 16)));
        }
        if (stringBuffer == null) {
            return str;
        }
        matcher.appendTail(stringBuffer);
        return stringBuffer.toString();
    }

    /* loaded from: classes3.dex */
    public static class Builder {
        private String extension;
        private String isdnSubaddress;
        private String number;
        private Map<String, String> parameters;
        private String phoneContext;
        private CharacterBitSet validParamNameChars;

        private Builder() {
            this.validParamNameChars = new CharacterBitSet("a-zA-Z0-9-");
            this.parameters = new TreeMap();
        }

        public TelUri build() {
            return new TelUri(this);
        }
    }
}
