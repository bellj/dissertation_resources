package ezvcard.util;

import ezvcard.Messages;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* loaded from: classes3.dex */
public final class GeoUri {
    private static final Pattern hexPattern = Pattern.compile("(?i)%([0-9a-f]{2})");
    private static final boolean[] validParameterValueCharacters = new boolean[128];
    private final Double coordA;
    private final Double coordB;
    private final Double coordC;
    private final String crs;
    private final Map<String, String> parameters;
    private final Double uncertainty;

    static {
        validParameterValueCharacters = new boolean[128];
        for (int i = 48; i <= 57; i++) {
            validParameterValueCharacters[i] = true;
        }
        for (int i2 = 65; i2 <= 90; i2++) {
            validParameterValueCharacters[i2] = true;
        }
        for (int i3 = 97; i3 <= 122; i3++) {
            validParameterValueCharacters[i3] = true;
        }
        for (int i4 = 0; i4 < 15; i4++) {
            validParameterValueCharacters["!$&'()*+-.:[]_~".charAt(i4)] = true;
        }
        hexPattern = Pattern.compile("(?i)%([0-9a-f]{2})");
    }

    private GeoUri(Builder builder) {
        Double d;
        Double d2 = builder.coordA;
        Double valueOf = Double.valueOf(0.0d);
        if (d2 == null) {
            d = valueOf;
        } else {
            d = builder.coordA;
        }
        this.coordA = d;
        this.coordB = builder.coordB != null ? builder.coordB : valueOf;
        this.coordC = builder.coordC;
        this.crs = builder.crs;
        this.uncertainty = builder.uncertainty;
        this.parameters = Collections.unmodifiableMap(builder.parameters);
    }

    public static GeoUri parse(String str) {
        if (str.length() < 4 || !str.substring(0, 4).equalsIgnoreCase("geo:")) {
            throw Messages.INSTANCE.getIllegalArgumentException(18, "geo:");
        }
        Builder builder = new Builder(null, null);
        ClearableStringBuilder clearableStringBuilder = new ClearableStringBuilder();
        String str2 = null;
        boolean z = false;
        for (int i = 4; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt == ',' && !z) {
                handleEndOfCoordinate(clearableStringBuilder, builder);
            } else if (charAt == ';') {
                if (z) {
                    handleEndOfParameter(clearableStringBuilder, str2, builder);
                    str2 = null;
                } else {
                    handleEndOfCoordinate(clearableStringBuilder, builder);
                    if (builder.coordB != null) {
                        z = true;
                    } else {
                        throw Messages.INSTANCE.getIllegalArgumentException(21, new Object[0]);
                    }
                }
            } else if (charAt == '=' && z && str2 == null) {
                str2 = clearableStringBuilder.getAndClear();
            } else {
                clearableStringBuilder.append(charAt);
            }
        }
        if (z) {
            handleEndOfParameter(clearableStringBuilder, str2, builder);
        } else {
            handleEndOfCoordinate(clearableStringBuilder, builder);
            if (builder.coordB == null) {
                throw Messages.INSTANCE.getIllegalArgumentException(21, new Object[0]);
            }
        }
        return builder.build();
    }

    private static void handleEndOfCoordinate(ClearableStringBuilder clearableStringBuilder, Builder builder) {
        String andClear = clearableStringBuilder.getAndClear();
        if (builder.coordA == null) {
            try {
                builder.coordA = Double.valueOf(Double.parseDouble(andClear));
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException(Messages.INSTANCE.getExceptionMessage(22, "A"), e);
            }
        } else if (builder.coordB == null) {
            try {
                builder.coordB = Double.valueOf(Double.parseDouble(andClear));
            } catch (NumberFormatException e2) {
                throw new IllegalArgumentException(Messages.INSTANCE.getExceptionMessage(22, "B"), e2);
            }
        } else if (builder.coordC == null) {
            try {
                builder.coordC = Double.valueOf(Double.parseDouble(andClear));
            } catch (NumberFormatException e3) {
                throw new IllegalArgumentException(Messages.INSTANCE.getExceptionMessage(22, "C"), e3);
            }
        }
    }

    private static void addParameter(String str, String str2, Builder builder) {
        String decodeParameterValue = decodeParameterValue(str2);
        if ("crs".equalsIgnoreCase(str)) {
            builder.crs = decodeParameterValue;
            return;
        }
        if ("u".equalsIgnoreCase(str)) {
            try {
                builder.uncertainty = Double.valueOf(decodeParameterValue);
                return;
            } catch (NumberFormatException unused) {
            }
        }
        builder.parameters.put(str, decodeParameterValue);
    }

    private static void handleEndOfParameter(ClearableStringBuilder clearableStringBuilder, String str, Builder builder) {
        String andClear = clearableStringBuilder.getAndClear();
        if (str != null) {
            addParameter(str, andClear, builder);
        } else if (andClear.length() > 0) {
            addParameter(andClear, "", builder);
        }
    }

    public String toString() {
        return toString(6);
    }

    public String toString(int i) {
        VCardFloatFormatter vCardFloatFormatter = new VCardFloatFormatter(i);
        StringBuilder sb = new StringBuilder("geo:");
        sb.append(vCardFloatFormatter.format(this.coordA));
        sb.append(',');
        sb.append(vCardFloatFormatter.format(this.coordB));
        if (this.coordC != null) {
            sb.append(',');
            sb.append(this.coordC);
        }
        String str = this.crs;
        if (str != null && !str.equalsIgnoreCase("wgs84")) {
            writeParameter("crs", this.crs, sb);
        }
        Double d = this.uncertainty;
        if (d != null) {
            writeParameter("u", vCardFloatFormatter.format(d), sb);
        }
        for (Map.Entry<String, String> entry : this.parameters.entrySet()) {
            writeParameter(entry.getKey(), entry.getValue(), sb);
        }
        return sb.toString();
    }

    private void writeParameter(String str, String str2, StringBuilder sb) {
        sb.append(';');
        sb.append(str);
        sb.append('=');
        sb.append(encodeParameterValue(str2));
    }

    private static String encodeParameterValue(String str) {
        StringBuilder sb = null;
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            boolean[] zArr = validParameterValueCharacters;
            if (charAt >= zArr.length || !zArr[charAt]) {
                if (sb == null) {
                    sb = new StringBuilder(str.length() * 2);
                    sb.append(str.substring(0, i));
                }
                String num = Integer.toString(charAt, 16);
                sb.append('%');
                sb.append(num);
            } else if (sb != null) {
                sb.append(charAt);
            }
        }
        return sb == null ? str : sb.toString();
    }

    private static String decodeParameterValue(String str) {
        Matcher matcher = hexPattern.matcher(str);
        StringBuffer stringBuffer = null;
        while (matcher.find()) {
            if (stringBuffer == null) {
                stringBuffer = new StringBuffer(str.length());
            }
            matcher.appendReplacement(stringBuffer, Character.toString((char) Integer.parseInt(matcher.group(1), 16)));
        }
        if (stringBuffer == null) {
            return str;
        }
        matcher.appendTail(stringBuffer);
        return stringBuffer.toString();
    }

    public int hashCode() {
        Double d = this.coordA;
        int i = 0;
        int hashCode = ((d == null ? 0 : d.hashCode()) + 31) * 31;
        Double d2 = this.coordB;
        int hashCode2 = (hashCode + (d2 == null ? 0 : d2.hashCode())) * 31;
        Double d3 = this.coordC;
        int hashCode3 = (hashCode2 + (d3 == null ? 0 : d3.hashCode())) * 31;
        String str = this.crs;
        int hashCode4 = (hashCode3 + (str == null ? 0 : str.toLowerCase().hashCode())) * 31;
        Map<String, String> map = this.parameters;
        int hashCode5 = (hashCode4 + (map == null ? 0 : StringUtils.toLowerCase(map).hashCode())) * 31;
        Double d4 = this.uncertainty;
        if (d4 != null) {
            i = d4.hashCode();
        }
        return hashCode5 + i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || GeoUri.class != obj.getClass()) {
            return false;
        }
        GeoUri geoUri = (GeoUri) obj;
        Double d = this.coordA;
        if (d == null) {
            if (geoUri.coordA != null) {
                return false;
            }
        } else if (!d.equals(geoUri.coordA)) {
            return false;
        }
        Double d2 = this.coordB;
        if (d2 == null) {
            if (geoUri.coordB != null) {
                return false;
            }
        } else if (!d2.equals(geoUri.coordB)) {
            return false;
        }
        Double d3 = this.coordC;
        if (d3 == null) {
            if (geoUri.coordC != null) {
                return false;
            }
        } else if (!d3.equals(geoUri.coordC)) {
            return false;
        }
        String str = this.crs;
        if (str == null) {
            if (geoUri.crs != null) {
                return false;
            }
        } else if (!str.equalsIgnoreCase(geoUri.crs)) {
            return false;
        }
        Double d4 = this.uncertainty;
        if (d4 == null) {
            if (geoUri.uncertainty != null) {
                return false;
            }
        } else if (!d4.equals(geoUri.uncertainty)) {
            return false;
        }
        Map<String, String> map = this.parameters;
        if (map == null) {
            if (geoUri.parameters != null) {
                return false;
            }
        } else if (geoUri.parameters == null || map.size() != geoUri.parameters.size() || !StringUtils.toLowerCase(this.parameters).equals(StringUtils.toLowerCase(geoUri.parameters))) {
            return false;
        }
        return true;
    }

    /* loaded from: classes3.dex */
    public static class Builder {
        private Double coordA;
        private Double coordB;
        private Double coordC;
        private String crs;
        private Map<String, String> parameters = new LinkedHashMap(0);
        private Double uncertainty;
        private CharacterBitSet validParamChars = new CharacterBitSet("a-zA-Z0-9-");

        public Builder(Double d, Double d2) {
            coordA(d);
            coordB(d2);
        }

        public Builder coordA(Double d) {
            this.coordA = d;
            return this;
        }

        public Builder coordB(Double d) {
            this.coordB = d;
            return this;
        }

        public GeoUri build() {
            return new GeoUri(this);
        }
    }
}
