package ezvcard.util;

import java.util.HashMap;
import java.util.Map;

/* loaded from: classes3.dex */
public final class StringUtils {
    public static final String NEWLINE = System.getProperty("line.separator");

    public static String ltrim(String str) {
        if (str == null) {
            return null;
        }
        int i = 0;
        while (i < str.length() && Character.isWhitespace(str.charAt(i))) {
            i++;
        }
        return i == str.length() ? "" : str.substring(i);
    }

    public static String rtrim(String str) {
        if (str == null) {
            return null;
        }
        int length = str.length() - 1;
        while (length >= 0 && Character.isWhitespace(str.charAt(length))) {
            length--;
        }
        if (length == 0) {
            return "";
        }
        return str.substring(0, length + 1);
    }

    public static Map<String, String> toLowerCase(Map<String, String> map) {
        String str;
        HashMap hashMap = new HashMap(map.size());
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String lowerCase = entry.getKey().toLowerCase();
            String value = entry.getValue();
            if (value == null) {
                str = null;
            } else {
                str = value.toLowerCase();
            }
            hashMap.put(lowerCase, str);
        }
        return hashMap;
    }
}
