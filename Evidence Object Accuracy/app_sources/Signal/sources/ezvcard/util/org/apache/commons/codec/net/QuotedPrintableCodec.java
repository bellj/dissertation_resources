package ezvcard.util.org.apache.commons.codec.net;

import ezvcard.util.org.apache.commons.codec.DecoderException;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.BitSet;

/* loaded from: classes3.dex */
public class QuotedPrintableCodec {
    private static final BitSet PRINTABLE_CHARS = new BitSet(256);
    private final String charset;

    static {
        PRINTABLE_CHARS = new BitSet(256);
        for (int i = 33; i <= 60; i++) {
            PRINTABLE_CHARS.set(i);
        }
        for (int i2 = 62; i2 <= 126; i2++) {
            PRINTABLE_CHARS.set(i2);
        }
        BitSet bitSet = PRINTABLE_CHARS;
        bitSet.set(9);
        bitSet.set(32);
    }

    public QuotedPrintableCodec(String str) {
        this.charset = str;
    }

    public String decode(String str) throws DecoderException {
        try {
            byte[] bytes = str.getBytes("US-ASCII");
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            int i = 0;
            while (i < bytes.length) {
                byte b = bytes[i];
                if (b == 61) {
                    int i2 = i + 1;
                    try {
                        int digit16 = digit16(bytes[i2]);
                        i = i2 + 1;
                        byteArrayOutputStream.write((char) ((digit16 << 4) + digit16(bytes[i])));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        throw new DecoderException("Invalid quoted-printable encoding", e);
                    }
                } else {
                    byteArrayOutputStream.write(b);
                }
                i++;
            }
            try {
                return new String(byteArrayOutputStream.toByteArray(), this.charset);
            } catch (UnsupportedEncodingException e2) {
                throw new DecoderException(e2);
            }
        } catch (UnsupportedEncodingException e3) {
            throw new DecoderException(e3);
        }
    }

    private static int digit16(byte b) throws DecoderException {
        int digit = Character.digit((char) b, 16);
        if (digit != -1) {
            return digit;
        }
        throw new DecoderException("Invalid URL encoding: not a valid digit (radix 16): " + ((int) b));
    }
}
