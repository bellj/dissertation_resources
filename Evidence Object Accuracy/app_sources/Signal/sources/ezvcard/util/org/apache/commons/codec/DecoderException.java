package ezvcard.util.org.apache.commons.codec;

/* loaded from: classes3.dex */
public class DecoderException extends Exception {
    public DecoderException(String str) {
        super(str);
    }

    public DecoderException(String str, Throwable th) {
        super(str, th);
    }

    public DecoderException(Throwable th) {
        super(th);
    }
}
