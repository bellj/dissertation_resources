package ezvcard.util;

import j$.util.Iterator;
import j$.util.function.Consumer;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/* loaded from: classes3.dex */
public class ListMultimap<K, V> implements Iterable<Map.Entry<K, List<V>>> {
    private final Map<K, List<V>> map;

    protected K sanitizeKey(K k) {
        return k;
    }

    public ListMultimap() {
        this(new LinkedHashMap());
    }

    public ListMultimap(Map<K, List<V>> map) {
        this.map = map;
    }

    public void put(K k, V v) {
        K sanitizeKey = sanitizeKey(k);
        List<V> list = this.map.get(sanitizeKey);
        if (list == null) {
            list = new ArrayList<>();
            this.map.put(sanitizeKey, list);
        }
        list.add(v);
    }

    public List<V> get(K k) {
        K sanitizeKey = sanitizeKey(k);
        List<V> list = this.map.get(sanitizeKey);
        if (list == null) {
            list = new ArrayList<>(0);
        }
        return new WrappedList(sanitizeKey, list, null);
    }

    public V first(K k) {
        List<V> list = this.map.get(sanitizeKey(k));
        if (list == null) {
            return null;
        }
        return list.get(0);
    }

    public List<V> removeAll(K k) {
        List<V> remove = this.map.remove(sanitizeKey(k));
        if (remove == null) {
            return Collections.emptyList();
        }
        List<V> unmodifiableList = Collections.unmodifiableList(new ArrayList(remove));
        remove.clear();
        return unmodifiableList;
    }

    public List<V> replace(K k, V v) {
        List<V> removeAll = removeAll(k);
        if (v != null) {
            put(k, v);
        }
        return removeAll;
    }

    public List<V> values() {
        ArrayList arrayList = new ArrayList();
        for (List<V> list : this.map.values()) {
            arrayList.addAll(list);
        }
        return Collections.unmodifiableList(arrayList);
    }

    public int size() {
        int i = 0;
        for (List<V> list : this.map.values()) {
            i += list.size();
        }
        return i;
    }

    @Override // java.lang.Iterable
    public Iterator<Map.Entry<K, List<V>>> iterator() {
        final Iterator<Map.Entry<K, List<V>>> it = this.map.entrySet().iterator();
        return new Object() { // from class: ezvcard.util.ListMultimap.1
            @Override // j$.util.Iterator
            public /* synthetic */ void forEachRemaining(Consumer consumer) {
                Iterator.CC.$default$forEachRemaining(this, consumer);
            }

            @Override // java.util.Iterator
            public /* synthetic */ void forEachRemaining(java.util.function.Consumer consumer) {
                forEachRemaining(Consumer.VivifiedWrapper.convert(consumer));
            }

            @Override // java.util.Iterator, j$.util.Iterator
            public boolean hasNext() {
                return it.hasNext();
            }

            @Override // java.util.Iterator, j$.util.Iterator
            public Map.Entry<K, List<V>> next() {
                final Map.Entry entry = (Map.Entry) it.next();
                return new Map.Entry<K, List<V>>() { // from class: ezvcard.util.ListMultimap.1.1
                    @Override // java.util.Map.Entry
                    public /* bridge */ /* synthetic */ Object setValue(Object obj) {
                        return setValue((List) ((List) obj));
                    }

                    @Override // java.util.Map.Entry
                    public K getKey() {
                        return (K) entry.getKey();
                    }

                    @Override // java.util.Map.Entry
                    public List<V> getValue() {
                        return Collections.unmodifiableList((List) entry.getValue());
                    }

                    public List<V> setValue(List<V> list) {
                        throw new UnsupportedOperationException();
                    }
                };
            }

            @Override // java.util.Iterator, j$.util.Iterator
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    @Override // java.lang.Object
    public String toString() {
        return this.map.toString();
    }

    @Override // java.lang.Object
    public int hashCode() {
        return this.map.hashCode();
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && getClass() == obj.getClass()) {
            return this.map.equals(((ListMultimap) obj).map);
        }
        return false;
    }

    /* loaded from: classes3.dex */
    public class WrappedList extends AbstractCollection<V> implements List<V> {
        final ListMultimap<K, V>.WrappedList ancestor;
        final List<V> ancestorDelegate;
        List<V> delegate;
        final K key;

        WrappedList(K k, List<V> list, ListMultimap<K, V>.WrappedList wrappedList) {
            List<V> list2;
            ListMultimap.this = r1;
            this.key = k;
            this.delegate = list;
            this.ancestor = wrappedList;
            if (wrappedList == null) {
                list2 = null;
            } else {
                list2 = wrappedList.getDelegate();
            }
            this.ancestorDelegate = list2;
        }

        @Override // java.util.List
        public boolean addAll(int i, Collection<? extends V> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean addAll = getDelegate().addAll(i, collection);
            if (addAll && size == 0) {
                addToMap();
            }
            return addAll;
        }

        @Override // java.util.List
        public V get(int i) {
            refreshIfEmpty();
            return getDelegate().get(i);
        }

        @Override // java.util.List
        public V set(int i, V v) {
            refreshIfEmpty();
            return getDelegate().set(i, v);
        }

        @Override // java.util.List
        public void add(int i, V v) {
            refreshIfEmpty();
            boolean isEmpty = getDelegate().isEmpty();
            getDelegate().add(i, v);
            if (isEmpty) {
                addToMap();
            }
        }

        @Override // java.util.List
        public V remove(int i) {
            refreshIfEmpty();
            V remove = getDelegate().remove(i);
            removeIfEmpty();
            return remove;
        }

        @Override // java.util.List
        public int indexOf(Object obj) {
            refreshIfEmpty();
            return getDelegate().indexOf(obj);
        }

        @Override // java.util.List
        public int lastIndexOf(Object obj) {
            refreshIfEmpty();
            return getDelegate().lastIndexOf(obj);
        }

        @Override // java.util.List
        public ListIterator<V> listIterator() {
            refreshIfEmpty();
            return new WrappedListIterator();
        }

        @Override // java.util.List
        public ListIterator<V> listIterator(int i) {
            refreshIfEmpty();
            return new WrappedListIterator(i);
        }

        @Override // java.util.List
        public List<V> subList(int i, int i2) {
            refreshIfEmpty();
            return new WrappedList(getKey(), getDelegate().subList(i, i2), getAncestor() == null ? this : getAncestor());
        }

        void refreshIfEmpty() {
            List<V> list;
            ListMultimap<K, V>.WrappedList wrappedList = this.ancestor;
            if (wrappedList != null) {
                wrappedList.refreshIfEmpty();
                if (this.ancestor.getDelegate() != this.ancestorDelegate) {
                    throw new ConcurrentModificationException();
                }
            } else if (this.delegate.isEmpty() && (list = (List) ListMultimap.this.map.get(this.key)) != null) {
                this.delegate = list;
            }
        }

        void removeIfEmpty() {
            ListMultimap<K, V>.WrappedList wrappedList = this.ancestor;
            if (wrappedList != null) {
                wrappedList.removeIfEmpty();
            } else if (this.delegate.isEmpty()) {
                ListMultimap.this.map.remove(this.key);
            }
        }

        K getKey() {
            return this.key;
        }

        void addToMap() {
            ListMultimap<K, V>.WrappedList wrappedList = this.ancestor;
            if (wrappedList != null) {
                wrappedList.addToMap();
            } else {
                ListMultimap.this.map.put(this.key, this.delegate);
            }
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
        public int size() {
            refreshIfEmpty();
            return this.delegate.size();
        }

        @Override // java.util.Collection, java.lang.Object, java.util.List
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            refreshIfEmpty();
            return this.delegate.equals(obj);
        }

        @Override // java.util.Collection, java.lang.Object, java.util.List
        public int hashCode() {
            refreshIfEmpty();
            return this.delegate.hashCode();
        }

        @Override // java.util.AbstractCollection, java.lang.Object
        public String toString() {
            refreshIfEmpty();
            return this.delegate.toString();
        }

        List<V> getDelegate() {
            return this.delegate;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable, java.util.List
        public java.util.Iterator<V> iterator() {
            refreshIfEmpty();
            return new WrappedListIterator();
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
        public boolean add(V v) {
            refreshIfEmpty();
            boolean isEmpty = this.delegate.isEmpty();
            boolean add = this.delegate.add(v);
            if (add && isEmpty) {
                addToMap();
            }
            return add;
        }

        ListMultimap<K, V>.WrappedList getAncestor() {
            return this.ancestor;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
        public boolean addAll(Collection<? extends V> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean addAll = this.delegate.addAll(collection);
            if (addAll && size == 0) {
                addToMap();
            }
            return addAll;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
        public boolean contains(Object obj) {
            refreshIfEmpty();
            return this.delegate.contains(obj);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
        public boolean containsAll(Collection<?> collection) {
            refreshIfEmpty();
            return this.delegate.containsAll(collection);
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
        public void clear() {
            if (size() != 0) {
                this.delegate.clear();
                removeIfEmpty();
            }
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
        public boolean remove(Object obj) {
            refreshIfEmpty();
            boolean remove = this.delegate.remove(obj);
            if (remove) {
                removeIfEmpty();
            }
            return remove;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
        public boolean removeAll(Collection<?> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            refreshIfEmpty();
            boolean removeAll = this.delegate.removeAll(collection);
            if (removeAll) {
                removeIfEmpty();
            }
            return removeAll;
        }

        @Override // java.util.AbstractCollection, java.util.Collection, java.util.List
        public boolean retainAll(Collection<?> collection) {
            refreshIfEmpty();
            boolean retainAll = this.delegate.retainAll(collection);
            if (retainAll) {
                removeIfEmpty();
            }
            return retainAll;
        }

        /* loaded from: classes3.dex */
        private class WrappedListIterator implements ListIterator<V>, j$.util.Iterator {
            final ListIterator<V> delegateIterator;
            final List<V> originalDelegate;

            @Override // j$.util.Iterator
            public /* synthetic */ void forEachRemaining(Consumer consumer) {
                Iterator.CC.$default$forEachRemaining(this, consumer);
            }

            @Override // java.util.Iterator
            public /* synthetic */ void forEachRemaining(java.util.function.Consumer consumer) {
                forEachRemaining(Consumer.VivifiedWrapper.convert(consumer));
            }

            WrappedListIterator() {
                WrappedList.this = r1;
                List<V> list = r1.delegate;
                this.originalDelegate = list;
                this.delegateIterator = list.listIterator();
            }

            public WrappedListIterator(int i) {
                WrappedList.this = r1;
                List<V> list = r1.delegate;
                this.originalDelegate = list;
                this.delegateIterator = list.listIterator(i);
            }

            @Override // java.util.ListIterator
            public boolean hasPrevious() {
                return getDelegateIterator().hasPrevious();
            }

            @Override // java.util.ListIterator
            public V previous() {
                return getDelegateIterator().previous();
            }

            @Override // java.util.ListIterator
            public int nextIndex() {
                return getDelegateIterator().nextIndex();
            }

            @Override // java.util.ListIterator
            public int previousIndex() {
                return getDelegateIterator().previousIndex();
            }

            @Override // java.util.ListIterator
            public void set(V v) {
                getDelegateIterator().set(v);
            }

            @Override // java.util.ListIterator
            public void add(V v) {
                boolean isEmpty = WrappedList.this.isEmpty();
                getDelegateIterator().add(v);
                if (isEmpty) {
                    WrappedList.this.addToMap();
                }
            }

            void validateIterator() {
                WrappedList.this.refreshIfEmpty();
                if (WrappedList.this.delegate != this.originalDelegate) {
                    throw new ConcurrentModificationException();
                }
            }

            @Override // java.util.ListIterator, java.util.Iterator, j$.util.Iterator
            public boolean hasNext() {
                validateIterator();
                return this.delegateIterator.hasNext();
            }

            @Override // java.util.ListIterator, java.util.Iterator, j$.util.Iterator
            public V next() {
                validateIterator();
                return this.delegateIterator.next();
            }

            @Override // java.util.ListIterator, java.util.Iterator, j$.util.Iterator
            public void remove() {
                this.delegateIterator.remove();
                WrappedList.this.removeIfEmpty();
            }

            ListIterator<V> getDelegateIterator() {
                validateIterator();
                return this.delegateIterator;
            }
        }
    }
}
