package ezvcard.util;

import java.io.Closeable;
import java.io.IOException;
import java.nio.charset.Charset;

/* loaded from: classes3.dex */
public final class IOUtils {
    private static final Charset UTF8 = Charset.forName("UTF-8");

    public static void closeQuietly(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException unused) {
            }
        }
    }
}
