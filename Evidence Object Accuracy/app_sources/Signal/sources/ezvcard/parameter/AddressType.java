package ezvcard.parameter;

/* loaded from: classes3.dex */
public class AddressType extends VCardParameter {
    public static final AddressType DOM = new AddressType("dom");
    public static final AddressType HOME = new AddressType("home");
    public static final AddressType INTL = new AddressType("intl");
    public static final AddressType PARCEL = new AddressType("parcel");
    public static final AddressType POSTAL = new AddressType("postal");
    public static final AddressType PREF = new AddressType("pref");
    public static final AddressType WORK = new AddressType("work");
    private static final VCardParameterCaseClasses<AddressType> enums = new VCardParameterCaseClasses<>(AddressType.class);

    private AddressType(String str) {
        super(str);
    }

    public static AddressType get(String str) {
        return (AddressType) enums.get(str);
    }
}
