package ezvcard.parameter;

/* loaded from: classes3.dex */
public class VCardParameter {
    protected final String value;

    public VCardParameter(String str) {
        this(str, false);
    }

    public VCardParameter(String str, boolean z) {
        if (str != null && !z) {
            str = str.toLowerCase();
        }
        this.value = str;
    }

    public String getValue() {
        return this.value;
    }

    public String toString() {
        return this.value;
    }

    public int hashCode() {
        String str = this.value;
        return 31 + (str == null ? 0 : str.hashCode());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        VCardParameter vCardParameter = (VCardParameter) obj;
        String str = this.value;
        if (str == null) {
            if (vCardParameter.value != null) {
                return false;
            }
        } else if (!str.equals(vCardParameter.value)) {
            return false;
        }
        return true;
    }
}
