package ezvcard.parameter;

import ezvcard.parameter.MediaTypeParameter;
import ezvcard.util.CaseClasses;
import java.lang.reflect.Constructor;

/* loaded from: classes3.dex */
public class MediaTypeCaseClasses<T extends MediaTypeParameter> extends CaseClasses<T, String[]> {
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: ezvcard.parameter.MediaTypeCaseClasses<T extends ezvcard.parameter.MediaTypeParameter> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // ezvcard.util.CaseClasses
    protected /* bridge */ /* synthetic */ boolean matches(Object obj, Object obj2) {
        return matches((MediaTypeCaseClasses<T>) ((MediaTypeParameter) obj), (String[]) obj2);
    }

    public MediaTypeCaseClasses(Class<T> cls) {
        super(cls);
    }

    public T create(String[] strArr) {
        try {
            Constructor declaredConstructor = this.clazz.getDeclaredConstructor(String.class, String.class, String.class);
            declaredConstructor.setAccessible(true);
            return (T) ((MediaTypeParameter) declaredConstructor.newInstance(strArr[0], strArr[1], strArr[2]));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected boolean matches(T t, String[] strArr) {
        String[] strArr2 = {t.getValue(), t.getMediaType(), t.getExtension()};
        for (int i = 0; i < strArr.length; i++) {
            String str = strArr[i];
            if (!(str == null || str.equalsIgnoreCase(strArr2[i]))) {
                return false;
            }
        }
        return true;
    }
}
