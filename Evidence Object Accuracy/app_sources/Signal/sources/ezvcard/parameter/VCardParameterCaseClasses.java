package ezvcard.parameter;

import ezvcard.VCardVersion;
import ezvcard.parameter.VCardParameter;
import ezvcard.util.CaseClasses;
import java.lang.reflect.Constructor;

/* loaded from: classes3.dex */
public class VCardParameterCaseClasses<T extends VCardParameter> extends CaseClasses<T, String> {
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: ezvcard.parameter.VCardParameterCaseClasses<T extends ezvcard.parameter.VCardParameter> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // ezvcard.util.CaseClasses
    protected /* bridge */ /* synthetic */ boolean matches(Object obj, Object obj2) {
        return matches((VCardParameterCaseClasses<T>) ((VCardParameter) obj), (String) obj2);
    }

    public VCardParameterCaseClasses(Class<T> cls) {
        super(cls);
    }

    public T create(String str) {
        try {
            try {
                Constructor declaredConstructor = this.clazz.getDeclaredConstructor(String.class);
                declaredConstructor.setAccessible(true);
                return (T) ((VCardParameter) declaredConstructor.newInstance(str));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } catch (Exception unused) {
            Constructor declaredConstructor2 = this.clazz.getDeclaredConstructor(String.class, VCardVersion[].class);
            declaredConstructor2.setAccessible(true);
            return (T) ((VCardParameter) declaredConstructor2.newInstance(str, new VCardVersion[0]));
        }
    }

    protected boolean matches(T t, String str) {
        return t.getValue().equalsIgnoreCase(str);
    }
}
