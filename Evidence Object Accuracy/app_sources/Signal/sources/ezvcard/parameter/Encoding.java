package ezvcard.parameter;

/* loaded from: classes3.dex */
public class Encoding extends VCardParameter {
    public static final Encoding B = new Encoding("b");
    public static final Encoding BASE64 = new Encoding("BASE64", true);
    public static final Encoding QUOTED_PRINTABLE = new Encoding("QUOTED-PRINTABLE", true);
    public static final Encoding _7BIT = new Encoding("7BIT", true);
    public static final Encoding _8BIT = new Encoding("8BIT", true);
    private static final VCardParameterCaseClasses<Encoding> enums = new VCardParameterCaseClasses<>(Encoding.class);

    private Encoding(String str) {
        super(str);
    }

    private Encoding(String str, boolean z) {
        super(str, z);
    }

    public static Encoding find(String str) {
        return (Encoding) enums.find(str);
    }

    public static Encoding get(String str) {
        return (Encoding) enums.get(str);
    }
}
