package ezvcard.parameter;

import ezvcard.Messages;
import ezvcard.VCardDataType;
import ezvcard.VCardVersion;
import ezvcard.util.ListMultimap;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* loaded from: classes3.dex */
public class VCardParameters extends ListMultimap<String, String> {
    private static final Map<String, Set<VCardVersion>> supportedVersions;

    static {
        HashMap hashMap = new HashMap();
        VCardVersion vCardVersion = VCardVersion.V4_0;
        hashMap.put("ALTID", EnumSet.of(vCardVersion));
        hashMap.put("CALSCALE", EnumSet.of(vCardVersion));
        hashMap.put("CHARSET", EnumSet.of(VCardVersion.V2_1));
        hashMap.put("GEO", EnumSet.of(vCardVersion));
        hashMap.put("INDEX", EnumSet.of(vCardVersion));
        hashMap.put("LEVEL", EnumSet.of(vCardVersion));
        hashMap.put("MEDIATYPE", EnumSet.of(vCardVersion));
        hashMap.put("PID", EnumSet.of(vCardVersion));
        hashMap.put("SORT-AS", EnumSet.of(vCardVersion));
        hashMap.put("TZ", EnumSet.of(vCardVersion));
        supportedVersions = Collections.unmodifiableMap(hashMap);
    }

    public String getCharset() {
        return first("CHARSET");
    }

    public Encoding getEncoding() {
        String first = first("ENCODING");
        if (first == null) {
            return null;
        }
        return Encoding.get(first);
    }

    public void setEncoding(Encoding encoding) {
        replace("ENCODING", encoding == null ? null : encoding.getValue());
    }

    public String getLabel() {
        return first("LABEL");
    }

    public void setLabel(String str) {
        replace("LABEL", str);
    }

    public String getMediaType() {
        return first("MEDIATYPE");
    }

    public Integer getPref() {
        String first = first("PREF");
        if (first == null) {
            return null;
        }
        try {
            return Integer.valueOf(first);
        } catch (NumberFormatException e) {
            throw new IllegalStateException(Messages.INSTANCE.getExceptionMessage(15, "PREF"), e);
        }
    }

    public List<String> getTypes() {
        return get("TYPE");
    }

    public String getType() {
        return first("TYPE");
    }

    public VCardDataType getValue() {
        String first = first("VALUE");
        if (first == null) {
            return null;
        }
        return VCardDataType.get(first);
    }

    public void setValue(VCardDataType vCardDataType) {
        replace("VALUE", vCardDataType == null ? null : vCardDataType.getName());
    }

    public String sanitizeKey(String str) {
        if (str == null) {
            return null;
        }
        return str.toUpperCase();
    }

    @Override // ezvcard.util.ListMultimap, java.lang.Object
    public int hashCode() {
        int i;
        Iterator<Map.Entry<String, List<String>>> it = iterator();
        int i2 = 1;
        while (it.hasNext()) {
            Map.Entry<String, List<String>> next = it.next();
            String key = next.getKey();
            int i3 = 1;
            for (String str : next.getValue()) {
                i3 += str.toLowerCase().hashCode();
            }
            if (key == null) {
                i = 0;
            } else {
                i = key.toLowerCase().hashCode();
            }
            int i4 = 31 + i + 1;
            i2 += i4 + (i4 * 31) + i3;
        }
        return i2;
    }

    @Override // ezvcard.util.ListMultimap, java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        VCardParameters vCardParameters = (VCardParameters) obj;
        if (size() != vCardParameters.size()) {
            return false;
        }
        Iterator<Map.Entry<String, List<String>>> it = iterator();
        while (it.hasNext()) {
            Map.Entry<String, List<String>> next = it.next();
            List<String> value = next.getValue();
            List<String> list = vCardParameters.get(next.getKey());
            if (value.size() != list.size()) {
                return false;
            }
            ArrayList arrayList = new ArrayList(value.size());
            for (String str : value) {
                arrayList.add(str.toLowerCase());
            }
            Collections.sort(arrayList);
            ArrayList arrayList2 = new ArrayList(list.size());
            for (String str2 : list) {
                arrayList2.add(str2.toLowerCase());
            }
            Collections.sort(arrayList2);
            if (!arrayList.equals(arrayList2)) {
                return false;
            }
        }
        return true;
    }

    /* loaded from: classes3.dex */
    public abstract class TypeParameterList<T extends VCardParameter> extends EnumParameterList<T> {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public TypeParameterList() {
            super("TYPE");
            VCardParameters.this = r2;
        }
    }

    /* loaded from: classes3.dex */
    public abstract class EnumParameterList<T extends VCardParameter> extends VCardParameterList<T> {
        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: ezvcard.parameter.VCardParameters$EnumParameterList<T extends ezvcard.parameter.VCardParameter> */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // ezvcard.parameter.VCardParameters.VCardParameterList
        protected /* bridge */ /* synthetic */ String _asString(Object obj) {
            return _asString((EnumParameterList<T>) ((VCardParameter) obj));
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public EnumParameterList(String str) {
            super(str);
            VCardParameters.this = r1;
        }

        protected String _asString(T t) {
            return t.getValue();
        }
    }

    /* loaded from: classes3.dex */
    public abstract class VCardParameterList<T> extends AbstractList<T> {
        protected final String parameterName;
        protected final List<String> parameterValues;

        protected abstract T _asObject(String str) throws Exception;

        protected abstract String _asString(T t);

        public VCardParameterList(String str) {
            VCardParameters.this = r1;
            this.parameterName = str;
            this.parameterValues = r1.get(str);
        }

        @Override // java.util.AbstractList, java.util.List
        public void add(int i, T t) {
            this.parameterValues.add(i, _asString(t));
        }

        @Override // java.util.AbstractList, java.util.List
        public T remove(int i) {
            return asObject(this.parameterValues.remove(i));
        }

        @Override // java.util.AbstractList, java.util.List
        public T get(int i) {
            return asObject(this.parameterValues.get(i));
        }

        @Override // java.util.AbstractList, java.util.List
        public T set(int i, T t) {
            return asObject(this.parameterValues.set(i, _asString(t)));
        }

        @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
        public int size() {
            return this.parameterValues.size();
        }

        private T asObject(String str) {
            try {
                return _asObject(str);
            } catch (Exception e) {
                throw _exception(str, e);
            }
        }

        protected IllegalStateException _exception(String str, Exception exc) {
            return new IllegalStateException(Messages.INSTANCE.getExceptionMessage(26, this.parameterName), exc);
        }
    }
}
