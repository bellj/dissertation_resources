package ezvcard.parameter;

import org.thoughtcrime.securesms.database.DraftDatabase;

/* loaded from: classes3.dex */
public class TelephoneType extends VCardParameter {
    public static final TelephoneType BBS = new TelephoneType("bbs");
    public static final TelephoneType CAR = new TelephoneType("car");
    public static final TelephoneType CELL = new TelephoneType("cell");
    public static final TelephoneType FAX = new TelephoneType("fax");
    public static final TelephoneType HOME = new TelephoneType("home");
    public static final TelephoneType ISDN = new TelephoneType("isdn");
    public static final TelephoneType MODEM = new TelephoneType("modem");
    public static final TelephoneType MSG = new TelephoneType("msg");
    public static final TelephoneType PAGER = new TelephoneType("pager");
    public static final TelephoneType PCS = new TelephoneType("pcs");
    public static final TelephoneType PREF = new TelephoneType("pref");
    public static final TelephoneType TEXT = new TelephoneType(DraftDatabase.Draft.TEXT);
    public static final TelephoneType TEXTPHONE = new TelephoneType("textphone");
    public static final TelephoneType VIDEO = new TelephoneType("video");
    public static final TelephoneType VOICE = new TelephoneType("voice");
    public static final TelephoneType WORK = new TelephoneType("work");
    private static final VCardParameterCaseClasses<TelephoneType> enums = new VCardParameterCaseClasses<>(TelephoneType.class);

    private TelephoneType(String str) {
        super(str);
    }

    public static TelephoneType get(String str) {
        return (TelephoneType) enums.get(str);
    }
}
