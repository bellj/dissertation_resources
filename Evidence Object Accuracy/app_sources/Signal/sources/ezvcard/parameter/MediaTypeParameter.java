package ezvcard.parameter;

/* loaded from: classes3.dex */
public class MediaTypeParameter extends VCardParameter {
    protected final String extension;
    protected final String mediaType;

    public MediaTypeParameter(String str, String str2, String str3) {
        super(str);
        this.mediaType = str2;
        this.extension = str3;
    }

    public String getMediaType() {
        return this.mediaType;
    }

    public String getExtension() {
        return this.extension;
    }

    @Override // ezvcard.parameter.VCardParameter
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        String str = this.extension;
        int i = 0;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.mediaType;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode2 + i;
    }

    @Override // ezvcard.parameter.VCardParameter
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj) || getClass() != obj.getClass()) {
            return false;
        }
        MediaTypeParameter mediaTypeParameter = (MediaTypeParameter) obj;
        String str = this.extension;
        if (str == null) {
            if (mediaTypeParameter.extension != null) {
                return false;
            }
        } else if (!str.equals(mediaTypeParameter.extension)) {
            return false;
        }
        String str2 = this.mediaType;
        if (str2 == null) {
            if (mediaTypeParameter.mediaType != null) {
                return false;
            }
        } else if (!str2.equals(mediaTypeParameter.mediaType)) {
            return false;
        }
        return true;
    }
}
