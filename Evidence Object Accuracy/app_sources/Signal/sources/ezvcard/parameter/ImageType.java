package ezvcard.parameter;

import org.thoughtcrime.securesms.util.MediaUtil;

/* loaded from: classes3.dex */
public class ImageType extends MediaTypeParameter {
    public static final ImageType GIF = new ImageType("GIF", MediaUtil.IMAGE_GIF, "gif");
    public static final ImageType JPEG = new ImageType("JPEG", MediaUtil.IMAGE_JPEG, "jpg");
    public static final ImageType PNG = new ImageType("PNG", MediaUtil.IMAGE_PNG, "png");
    private static final MediaTypeCaseClasses<ImageType> enums = new MediaTypeCaseClasses<>(ImageType.class);

    private ImageType(String str, String str2, String str3) {
        super(str, str2, str3);
    }

    public static ImageType find(String str, String str2, String str3) {
        return (ImageType) enums.find(new String[]{str, str2, str3});
    }

    public static ImageType get(String str, String str2, String str3) {
        return (ImageType) enums.get(new String[]{str, str2, str3});
    }
}
