package ezvcard.parameter;

/* loaded from: classes3.dex */
public class EmailType extends VCardParameter {
    public static final EmailType AOL = new EmailType("aol");
    public static final EmailType APPLELINK = new EmailType("applelink");
    public static final EmailType ATTMAIL = new EmailType("attmail");
    public static final EmailType CIS = new EmailType("cis");
    public static final EmailType EWORLD = new EmailType("eworld");
    public static final EmailType HOME = new EmailType("home");
    public static final EmailType IBMMAIL = new EmailType("ibmmail");
    public static final EmailType INTERNET = new EmailType("internet");
    public static final EmailType MCIMAIL = new EmailType("mcimail");
    public static final EmailType POWERSHARE = new EmailType("powershare");
    public static final EmailType PREF = new EmailType("pref");
    public static final EmailType PRODIGY = new EmailType("prodigy");
    public static final EmailType TLX = new EmailType("tlx");
    public static final EmailType WORK = new EmailType("work");
    public static final EmailType X400 = new EmailType("x400");
    private static final VCardParameterCaseClasses<EmailType> enums = new VCardParameterCaseClasses<>(EmailType.class);

    private EmailType(String str) {
        super(str);
    }

    public static EmailType get(String str) {
        return (EmailType) enums.get(str);
    }
}
