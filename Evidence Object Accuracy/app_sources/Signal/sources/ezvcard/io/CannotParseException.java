package ezvcard.io;

import ezvcard.Messages;

/* loaded from: classes3.dex */
public class CannotParseException extends RuntimeException {
    public CannotParseException(String str) {
        super(str);
    }

    public CannotParseException(int i, Object... objArr) {
        this(Messages.INSTANCE.getParseMessage(i, objArr));
    }
}
