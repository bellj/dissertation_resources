package ezvcard.io.scribe;

import ezvcard.property.Nickname;

/* loaded from: classes3.dex */
public class NicknameScribe extends ListPropertyScribe<Nickname> {
    public NicknameScribe() {
        super(Nickname.class, "NICKNAME");
    }

    @Override // ezvcard.io.scribe.ListPropertyScribe
    public Nickname _newInstance() {
        return new Nickname();
    }
}
