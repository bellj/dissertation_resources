package ezvcard.io.scribe;

import ezvcard.VCardDataType;
import ezvcard.VCardVersion;
import ezvcard.parameter.VCardParameters;
import ezvcard.property.RawProperty;
import java.util.List;

/* loaded from: classes3.dex */
public class RawPropertyScribe extends VCardPropertyScribe<RawProperty> {
    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected VCardDataType _defaultDataType(VCardVersion vCardVersion) {
        return null;
    }

    public RawPropertyScribe(String str) {
        super(RawProperty.class, str);
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected RawProperty _parseText(String str, VCardDataType vCardDataType, VCardVersion vCardVersion, VCardParameters vCardParameters, List<String> list) {
        RawProperty rawProperty = new RawProperty(this.propertyName, str);
        rawProperty.setDataType(vCardDataType);
        return rawProperty;
    }
}
