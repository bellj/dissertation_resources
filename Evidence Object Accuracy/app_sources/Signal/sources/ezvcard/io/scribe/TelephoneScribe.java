package ezvcard.io.scribe;

import ezvcard.Messages;
import ezvcard.VCardDataType;
import ezvcard.VCardVersion;
import ezvcard.parameter.VCardParameters;
import ezvcard.property.Telephone;
import ezvcard.util.TelUri;
import java.util.List;

/* loaded from: classes3.dex */
public class TelephoneScribe extends VCardPropertyScribe<Telephone> {
    public TelephoneScribe() {
        super(Telephone.class, "TEL");
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected VCardDataType _defaultDataType(VCardVersion vCardVersion) {
        return VCardDataType.TEXT;
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected Telephone _parseText(String str, VCardDataType vCardDataType, VCardVersion vCardVersion, VCardParameters vCardParameters, List<String> list) {
        return parse(VCardPropertyScribe.unescape(str), vCardDataType, list);
    }

    private Telephone parse(String str, VCardDataType vCardDataType, List<String> list) {
        try {
            return new Telephone(TelUri.parse(str));
        } catch (IllegalArgumentException unused) {
            if (vCardDataType == VCardDataType.URI) {
                list.add(Messages.INSTANCE.getParseMessage(18, new Object[0]));
            }
            return new Telephone(str);
        }
    }
}
