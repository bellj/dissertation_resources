package ezvcard.io.scribe;

import ezvcard.property.Categories;

/* loaded from: classes3.dex */
public class CategoriesScribe extends ListPropertyScribe<Categories> {
    public CategoriesScribe() {
        super(Categories.class, "CATEGORIES");
    }

    @Override // ezvcard.io.scribe.ListPropertyScribe
    public Categories _newInstance() {
        return new Categories();
    }
}
