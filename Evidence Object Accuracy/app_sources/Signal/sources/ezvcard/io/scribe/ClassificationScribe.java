package ezvcard.io.scribe;

import ezvcard.property.Classification;

/* loaded from: classes3.dex */
public class ClassificationScribe extends StringPropertyScribe<Classification> {
    public ClassificationScribe() {
        super(Classification.class, "CLASS");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public Classification _parseValue(String str) {
        return new Classification(str);
    }
}
