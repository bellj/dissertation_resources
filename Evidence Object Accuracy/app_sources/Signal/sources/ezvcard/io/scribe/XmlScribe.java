package ezvcard.io.scribe;

import ezvcard.VCardDataType;
import ezvcard.VCardVersion;
import ezvcard.io.CannotParseException;
import ezvcard.parameter.VCardParameters;
import ezvcard.property.Xml;
import java.util.List;
import org.xml.sax.SAXException;

/* loaded from: classes3.dex */
public class XmlScribe extends VCardPropertyScribe<Xml> {
    public XmlScribe() {
        super(Xml.class, "XML");
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected VCardDataType _defaultDataType(VCardVersion vCardVersion) {
        return VCardDataType.TEXT;
    }

    protected Xml _parseText(String str, VCardDataType vCardDataType, VCardVersion vCardVersion, VCardParameters vCardParameters, List<String> list) {
        try {
            return new Xml(VCardPropertyScribe.unescape(str));
        } catch (SAXException unused) {
            throw new CannotParseException(21, new Object[0]);
        }
    }
}
