package ezvcard.io.scribe;

import ezvcard.property.Url;

/* loaded from: classes3.dex */
public class UrlScribe extends UriPropertyScribe<Url> {
    public UrlScribe() {
        super(Url.class, "URL");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public Url _parseValue(String str) {
        return new Url(str);
    }
}
