package ezvcard.io.scribe;

import ezvcard.property.Kind;

/* loaded from: classes3.dex */
public class KindScribe extends StringPropertyScribe<Kind> {
    public KindScribe() {
        super(Kind.class, "KIND");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public Kind _parseValue(String str) {
        return new Kind(str);
    }
}
