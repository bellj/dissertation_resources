package ezvcard.io.scribe;

import ezvcard.property.CalendarUri;

/* loaded from: classes3.dex */
public class CalendarUriScribe extends UriPropertyScribe<CalendarUri> {
    public CalendarUriScribe() {
        super(CalendarUri.class, "CALURI");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public CalendarUri _parseValue(String str) {
        return new CalendarUri(str);
    }
}
