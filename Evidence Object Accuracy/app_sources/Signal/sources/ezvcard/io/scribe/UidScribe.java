package ezvcard.io.scribe;

import ezvcard.property.Uid;

/* loaded from: classes3.dex */
public class UidScribe extends UriPropertyScribe<Uid> {
    public UidScribe() {
        super(Uid.class, "UID");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public Uid _parseValue(String str) {
        return new Uid(str);
    }
}
