package ezvcard.io.scribe;

import ezvcard.property.ProductId;

/* loaded from: classes3.dex */
public class ProductIdScribe extends StringPropertyScribe<ProductId> {
    public ProductIdScribe() {
        super(ProductId.class, "PRODID");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public ProductId _parseValue(String str) {
        return new ProductId(str);
    }
}
