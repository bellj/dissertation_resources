package ezvcard.io.scribe;

import ezvcard.VCardDataType;
import ezvcard.VCardVersion;
import ezvcard.io.scribe.VCardPropertyScribe;
import ezvcard.parameter.VCardParameters;
import ezvcard.property.Address;
import java.util.List;

/* loaded from: classes3.dex */
public class AddressScribe extends VCardPropertyScribe<Address> {
    public AddressScribe() {
        super(Address.class, "ADR");
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected VCardDataType _defaultDataType(VCardVersion vCardVersion) {
        return VCardDataType.TEXT;
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected Address _parseText(String str, VCardDataType vCardDataType, VCardVersion vCardVersion, VCardParameters vCardParameters, List<String> list) {
        return parseStructuredValue(VCardPropertyScribe.structured(str));
    }

    private Address parseStructuredValue(VCardPropertyScribe.StructuredIterator structuredIterator) {
        Address address = new Address();
        address.getPoBoxes().addAll(structuredIterator.nextComponent());
        address.getExtendedAddresses().addAll(structuredIterator.nextComponent());
        address.getStreetAddresses().addAll(structuredIterator.nextComponent());
        address.getLocalities().addAll(structuredIterator.nextComponent());
        address.getRegions().addAll(structuredIterator.nextComponent());
        address.getPostalCodes().addAll(structuredIterator.nextComponent());
        address.getCountries().addAll(structuredIterator.nextComponent());
        return address;
    }
}
