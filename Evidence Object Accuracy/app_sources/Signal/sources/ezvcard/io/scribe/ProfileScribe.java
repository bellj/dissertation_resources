package ezvcard.io.scribe;

import ezvcard.property.Profile;

/* loaded from: classes3.dex */
public class ProfileScribe extends StringPropertyScribe<Profile> {
    public ProfileScribe() {
        super(Profile.class, "PROFILE");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public Profile _parseValue(String str) {
        Profile profile = new Profile();
        profile.setValue(str);
        return profile;
    }
}
