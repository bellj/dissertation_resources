package ezvcard.io.scribe;

import ezvcard.parameter.ImageType;
import ezvcard.property.Logo;

/* loaded from: classes3.dex */
public class LogoScribe extends ImagePropertyScribe<Logo> {
    public LogoScribe() {
        super(Logo.class, "LOGO");
    }

    public Logo _newInstance(String str, ImageType imageType) {
        return new Logo(str, imageType);
    }

    public Logo _newInstance(byte[] bArr, ImageType imageType) {
        return new Logo(bArr, imageType);
    }
}
