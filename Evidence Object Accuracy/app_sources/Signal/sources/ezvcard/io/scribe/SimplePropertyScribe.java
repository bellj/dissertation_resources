package ezvcard.io.scribe;

import ezvcard.VCardDataType;
import ezvcard.VCardVersion;
import ezvcard.parameter.VCardParameters;
import ezvcard.property.VCardProperty;
import java.util.List;

/* loaded from: classes3.dex */
public abstract class SimplePropertyScribe<T extends VCardProperty> extends VCardPropertyScribe<T> {
    protected final VCardDataType dataType;

    protected abstract T _parseValue(String str);

    public SimplePropertyScribe(Class<T> cls, String str, VCardDataType vCardDataType) {
        super(cls, str);
        this.dataType = vCardDataType;
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected VCardDataType _defaultDataType(VCardVersion vCardVersion) {
        return this.dataType;
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected T _parseText(String str, VCardDataType vCardDataType, VCardVersion vCardVersion, VCardParameters vCardParameters, List<String> list) {
        return _parseValue(VCardPropertyScribe.unescape(str));
    }
}
