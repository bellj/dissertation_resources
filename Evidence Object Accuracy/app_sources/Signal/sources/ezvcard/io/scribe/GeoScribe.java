package ezvcard.io.scribe;

import ezvcard.VCardDataType;
import ezvcard.VCardVersion;
import ezvcard.io.CannotParseException;
import ezvcard.io.scribe.VCardPropertyScribe;
import ezvcard.parameter.VCardParameters;
import ezvcard.property.Geo;
import ezvcard.util.GeoUri;
import java.util.List;

/* loaded from: classes3.dex */
public class GeoScribe extends VCardPropertyScribe<Geo> {
    public GeoScribe() {
        super(Geo.class, "GEO");
    }

    /* renamed from: ezvcard.io.scribe.GeoScribe$1 */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$ezvcard$VCardVersion;

        static {
            int[] iArr = new int[VCardVersion.values().length];
            $SwitchMap$ezvcard$VCardVersion = iArr;
            try {
                iArr[VCardVersion.V2_1.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$ezvcard$VCardVersion[VCardVersion.V3_0.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$ezvcard$VCardVersion[VCardVersion.V4_0.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected VCardDataType _defaultDataType(VCardVersion vCardVersion) {
        if (AnonymousClass1.$SwitchMap$ezvcard$VCardVersion[vCardVersion.ordinal()] != 3) {
            return null;
        }
        return VCardDataType.URI;
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected Geo _parseText(String str, VCardDataType vCardDataType, VCardVersion vCardVersion, VCardParameters vCardParameters, List<String> list) {
        return parse(VCardPropertyScribe.unescape(str), vCardVersion);
    }

    private Geo parse(String str, VCardVersion vCardVersion) {
        if (str == null || str.length() == 0) {
            return new Geo(null);
        }
        int i = AnonymousClass1.$SwitchMap$ezvcard$VCardVersion[vCardVersion.ordinal()];
        if (i == 1 || i == 2) {
            VCardPropertyScribe.SemiStructuredIterator semistructured = VCardPropertyScribe.semistructured(str);
            String next = semistructured.next();
            String next2 = semistructured.next();
            if (next == null || next2 == null) {
                throw new CannotParseException(11, new Object[0]);
            }
            try {
                try {
                    return new Geo(Double.valueOf(next), Double.valueOf(next2));
                } catch (NumberFormatException unused) {
                    throw new CannotParseException(10, next2);
                }
            } catch (NumberFormatException unused2) {
                throw new CannotParseException(8, next);
            }
        } else if (i != 3) {
            return null;
        } else {
            try {
                return new Geo(GeoUri.parse(str));
            } catch (IllegalArgumentException unused3) {
                throw new CannotParseException(12, new Object[0]);
            }
        }
    }
}
