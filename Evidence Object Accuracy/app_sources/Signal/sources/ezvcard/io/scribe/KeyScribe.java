package ezvcard.io.scribe;

import ezvcard.parameter.KeyType;
import ezvcard.property.Key;

/* loaded from: classes3.dex */
public class KeyScribe extends BinaryPropertyScribe<Key, KeyType> {
    public KeyScribe() {
        super(Key.class, "KEY");
    }

    @Override // ezvcard.io.scribe.BinaryPropertyScribe
    public KeyType _mediaTypeFromTypeParameter(String str) {
        return KeyType.get(str, null, null);
    }

    @Override // ezvcard.io.scribe.BinaryPropertyScribe
    public KeyType _mediaTypeFromMediaTypeParameter(String str) {
        return KeyType.get(null, str, null);
    }

    @Override // ezvcard.io.scribe.BinaryPropertyScribe
    public KeyType _mediaTypeFromFileExtension(String str) {
        return KeyType.find(null, null, str);
    }

    public Key _newInstance(String str, KeyType keyType) {
        return new Key(str, keyType);
    }

    public Key _newInstance(byte[] bArr, KeyType keyType) {
        return new Key(bArr, keyType);
    }
}
