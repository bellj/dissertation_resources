package ezvcard.io.scribe;

import ezvcard.property.Hobby;

/* loaded from: classes3.dex */
public class HobbyScribe extends StringPropertyScribe<Hobby> {
    public HobbyScribe() {
        super(Hobby.class, "HOBBY");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public Hobby _parseValue(String str) {
        return new Hobby(str);
    }
}
