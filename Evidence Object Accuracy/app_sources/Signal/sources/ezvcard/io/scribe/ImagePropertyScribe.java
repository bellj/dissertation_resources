package ezvcard.io.scribe;

import ezvcard.parameter.ImageType;
import ezvcard.property.ImageProperty;

/* loaded from: classes3.dex */
public abstract class ImagePropertyScribe<T extends ImageProperty> extends BinaryPropertyScribe<T, ImageType> {
    public ImagePropertyScribe(Class<T> cls, String str) {
        super(cls, str);
    }

    @Override // ezvcard.io.scribe.BinaryPropertyScribe
    public ImageType _mediaTypeFromTypeParameter(String str) {
        return ImageType.get(str, null, null);
    }

    @Override // ezvcard.io.scribe.BinaryPropertyScribe
    public ImageType _mediaTypeFromMediaTypeParameter(String str) {
        return ImageType.get(null, str, null);
    }

    @Override // ezvcard.io.scribe.BinaryPropertyScribe
    public ImageType _mediaTypeFromFileExtension(String str) {
        return ImageType.find(null, null, str);
    }
}
