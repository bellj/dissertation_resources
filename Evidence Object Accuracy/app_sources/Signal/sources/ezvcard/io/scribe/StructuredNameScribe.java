package ezvcard.io.scribe;

import ezvcard.VCardDataType;
import ezvcard.VCardVersion;
import ezvcard.io.scribe.VCardPropertyScribe;
import ezvcard.parameter.VCardParameters;
import ezvcard.property.StructuredName;
import java.util.List;

/* loaded from: classes3.dex */
public class StructuredNameScribe extends VCardPropertyScribe<StructuredName> {
    public StructuredNameScribe() {
        super(StructuredName.class, "N");
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected VCardDataType _defaultDataType(VCardVersion vCardVersion) {
        return VCardDataType.TEXT;
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected StructuredName _parseText(String str, VCardDataType vCardDataType, VCardVersion vCardVersion, VCardParameters vCardParameters, List<String> list) {
        StructuredName structuredName = new StructuredName();
        VCardPropertyScribe.StructuredIterator structured = VCardPropertyScribe.structured(str);
        structuredName.setFamily(structured.nextString());
        structuredName.setGiven(structured.nextString());
        structuredName.getAdditionalNames().addAll(structured.nextComponent());
        structuredName.getPrefixes().addAll(structured.nextComponent());
        structuredName.getSuffixes().addAll(structured.nextComponent());
        return structuredName;
    }
}
