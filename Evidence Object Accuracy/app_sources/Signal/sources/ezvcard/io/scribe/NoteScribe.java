package ezvcard.io.scribe;

import ezvcard.property.Note;

/* loaded from: classes3.dex */
public class NoteScribe extends StringPropertyScribe<Note> {
    public NoteScribe() {
        super(Note.class, "NOTE");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public Note _parseValue(String str) {
        return new Note(str);
    }
}
