package ezvcard.io.scribe;

import ezvcard.property.Birthday;
import ezvcard.util.PartialDate;
import java.util.Date;

/* loaded from: classes3.dex */
public class BirthdayScribe extends DateOrTimePropertyScribe<Birthday> {
    public BirthdayScribe() {
        super(Birthday.class, "BDAY");
    }

    @Override // ezvcard.io.scribe.DateOrTimePropertyScribe
    public Birthday newInstance(String str) {
        return new Birthday(str);
    }

    @Override // ezvcard.io.scribe.DateOrTimePropertyScribe
    public Birthday newInstance(Date date, boolean z) {
        return new Birthday(date, z);
    }

    @Override // ezvcard.io.scribe.DateOrTimePropertyScribe
    public Birthday newInstance(PartialDate partialDate) {
        return new Birthday(partialDate);
    }
}
