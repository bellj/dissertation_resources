package ezvcard.io.scribe;

import ezvcard.property.Anniversary;
import ezvcard.util.PartialDate;
import java.util.Date;

/* loaded from: classes3.dex */
public class AnniversaryScribe extends DateOrTimePropertyScribe<Anniversary> {
    public AnniversaryScribe() {
        super(Anniversary.class, "ANNIVERSARY");
    }

    @Override // ezvcard.io.scribe.DateOrTimePropertyScribe
    public Anniversary newInstance(String str) {
        return new Anniversary(str);
    }

    @Override // ezvcard.io.scribe.DateOrTimePropertyScribe
    public Anniversary newInstance(Date date, boolean z) {
        return new Anniversary(date, z);
    }

    @Override // ezvcard.io.scribe.DateOrTimePropertyScribe
    public Anniversary newInstance(PartialDate partialDate) {
        return new Anniversary(partialDate);
    }
}
