package ezvcard.io.scribe;

import ezvcard.property.Title;

/* loaded from: classes3.dex */
public class TitleScribe extends StringPropertyScribe<Title> {
    public TitleScribe() {
        super(Title.class, "TITLE");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public Title _parseValue(String str) {
        return new Title(str);
    }
}
