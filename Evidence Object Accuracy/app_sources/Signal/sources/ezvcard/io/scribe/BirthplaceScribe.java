package ezvcard.io.scribe;

import ezvcard.property.Birthplace;

/* loaded from: classes3.dex */
public class BirthplaceScribe extends PlacePropertyScribe<Birthplace> {
    public BirthplaceScribe() {
        super(Birthplace.class, "BIRTHPLACE");
    }

    @Override // ezvcard.io.scribe.PlacePropertyScribe
    public Birthplace newInstance() {
        return new Birthplace();
    }
}
