package ezvcard.io.scribe;

import ezvcard.property.Label;

/* loaded from: classes3.dex */
public class LabelScribe extends StringPropertyScribe<Label> {
    public LabelScribe() {
        super(Label.class, "LABEL");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public Label _parseValue(String str) {
        return new Label(str);
    }
}
