package ezvcard.io.scribe;

import ezvcard.property.OrgDirectory;

/* loaded from: classes3.dex */
public class OrgDirectoryScribe extends StringPropertyScribe<OrgDirectory> {
    public OrgDirectoryScribe() {
        super(OrgDirectory.class, "ORG-DIRECTORY");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public OrgDirectory _parseValue(String str) {
        return new OrgDirectory(str);
    }
}
