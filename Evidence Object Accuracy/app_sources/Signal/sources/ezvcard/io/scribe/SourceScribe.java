package ezvcard.io.scribe;

import ezvcard.property.Source;

/* loaded from: classes3.dex */
public class SourceScribe extends UriPropertyScribe<Source> {
    public SourceScribe() {
        super(Source.class, "SOURCE");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public Source _parseValue(String str) {
        return new Source(str);
    }
}
