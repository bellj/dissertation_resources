package ezvcard.io.scribe;

import ezvcard.property.Role;

/* loaded from: classes3.dex */
public class RoleScribe extends StringPropertyScribe<Role> {
    public RoleScribe() {
        super(Role.class, "ROLE");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public Role _parseValue(String str) {
        return new Role(str);
    }
}
