package ezvcard.io.scribe;

import ezvcard.property.SourceDisplayText;

/* loaded from: classes3.dex */
public class SourceDisplayTextScribe extends StringPropertyScribe<SourceDisplayText> {
    public SourceDisplayTextScribe() {
        super(SourceDisplayText.class, "NAME");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public SourceDisplayText _parseValue(String str) {
        return new SourceDisplayText(str);
    }
}
