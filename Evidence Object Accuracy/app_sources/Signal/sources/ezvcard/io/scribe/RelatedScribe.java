package ezvcard.io.scribe;

import ezvcard.VCardDataType;
import ezvcard.VCardVersion;
import ezvcard.parameter.VCardParameters;
import ezvcard.property.Related;
import java.util.List;

/* loaded from: classes3.dex */
public class RelatedScribe extends VCardPropertyScribe<Related> {
    public RelatedScribe() {
        super(Related.class, "RELATED");
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected VCardDataType _defaultDataType(VCardVersion vCardVersion) {
        return VCardDataType.URI;
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected Related _parseText(String str, VCardDataType vCardDataType, VCardVersion vCardVersion, VCardParameters vCardParameters, List<String> list) {
        String unescape = VCardPropertyScribe.unescape(str);
        Related related = new Related();
        if (vCardDataType == VCardDataType.TEXT) {
            related.setText(unescape);
        } else {
            related.setUri(unescape);
        }
        return related;
    }
}
