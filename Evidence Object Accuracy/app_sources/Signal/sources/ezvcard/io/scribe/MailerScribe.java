package ezvcard.io.scribe;

import ezvcard.property.Mailer;

/* loaded from: classes3.dex */
public class MailerScribe extends StringPropertyScribe<Mailer> {
    public MailerScribe() {
        super(Mailer.class, "MAILER");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public Mailer _parseValue(String str) {
        return new Mailer(str);
    }
}
