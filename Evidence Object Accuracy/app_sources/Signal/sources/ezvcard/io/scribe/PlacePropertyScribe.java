package ezvcard.io.scribe;

import ezvcard.VCardDataType;
import ezvcard.VCardVersion;
import ezvcard.parameter.VCardParameters;
import ezvcard.property.PlaceProperty;
import ezvcard.util.GeoUri;
import java.util.List;

/* loaded from: classes3.dex */
public abstract class PlacePropertyScribe<T extends PlaceProperty> extends VCardPropertyScribe<T> {
    protected abstract T newInstance();

    public PlacePropertyScribe(Class<T> cls, String str) {
        super(cls, str);
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected VCardDataType _defaultDataType(VCardVersion vCardVersion) {
        return VCardDataType.TEXT;
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected T _parseText(String str, VCardDataType vCardDataType, VCardVersion vCardVersion, VCardParameters vCardParameters, List<String> list) {
        T newInstance = newInstance();
        String unescape = VCardPropertyScribe.unescape(str);
        if (vCardDataType == VCardDataType.TEXT) {
            newInstance.setText(unescape);
            return newInstance;
        } else if (vCardDataType == VCardDataType.URI) {
            try {
                newInstance.setGeoUri(GeoUri.parse(unescape));
            } catch (IllegalArgumentException unused) {
                newInstance.setUri(unescape);
            }
            return newInstance;
        } else {
            newInstance.setText(unescape);
            return newInstance;
        }
    }
}
