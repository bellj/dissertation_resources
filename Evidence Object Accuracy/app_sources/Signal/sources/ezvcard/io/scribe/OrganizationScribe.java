package ezvcard.io.scribe;

import ezvcard.VCardDataType;
import ezvcard.VCardVersion;
import ezvcard.io.scribe.VCardPropertyScribe;
import ezvcard.parameter.VCardParameters;
import ezvcard.property.Organization;
import java.util.List;

/* loaded from: classes3.dex */
public class OrganizationScribe extends VCardPropertyScribe<Organization> {
    public OrganizationScribe() {
        super(Organization.class, "ORG");
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected VCardDataType _defaultDataType(VCardVersion vCardVersion) {
        return VCardDataType.TEXT;
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected Organization _parseText(String str, VCardDataType vCardDataType, VCardVersion vCardVersion, VCardParameters vCardParameters, List<String> list) {
        Organization organization = new Organization();
        VCardPropertyScribe.SemiStructuredIterator semistructured = VCardPropertyScribe.semistructured(str);
        while (semistructured.hasNext()) {
            organization.getValues().add(semistructured.next());
        }
        return organization;
    }
}
