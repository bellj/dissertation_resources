package ezvcard.io.scribe;

import ezvcard.parameter.SoundType;
import ezvcard.property.Sound;

/* loaded from: classes3.dex */
public class SoundScribe extends BinaryPropertyScribe<Sound, SoundType> {
    public SoundScribe() {
        super(Sound.class, "SOUND");
    }

    @Override // ezvcard.io.scribe.BinaryPropertyScribe
    public SoundType _mediaTypeFromTypeParameter(String str) {
        return SoundType.get(str, null, null);
    }

    @Override // ezvcard.io.scribe.BinaryPropertyScribe
    public SoundType _mediaTypeFromMediaTypeParameter(String str) {
        return SoundType.get(null, str, null);
    }

    @Override // ezvcard.io.scribe.BinaryPropertyScribe
    public SoundType _mediaTypeFromFileExtension(String str) {
        return SoundType.find(null, null, str);
    }

    public Sound _newInstance(String str, SoundType soundType) {
        return new Sound(str, soundType);
    }

    public Sound _newInstance(byte[] bArr, SoundType soundType) {
        return new Sound(bArr, soundType);
    }
}
