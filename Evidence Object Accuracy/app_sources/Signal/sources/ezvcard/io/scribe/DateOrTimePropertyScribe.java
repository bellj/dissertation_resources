package ezvcard.io.scribe;

import ezvcard.Messages;
import ezvcard.VCardDataType;
import ezvcard.VCardVersion;
import ezvcard.io.CannotParseException;
import ezvcard.parameter.VCardParameters;
import ezvcard.property.DateOrTimeProperty;
import ezvcard.util.PartialDate;
import java.util.Date;
import java.util.List;

/* loaded from: classes3.dex */
public abstract class DateOrTimePropertyScribe<T extends DateOrTimeProperty> extends VCardPropertyScribe<T> {
    protected abstract T newInstance(PartialDate partialDate);

    protected abstract T newInstance(String str);

    protected abstract T newInstance(Date date, boolean z);

    public DateOrTimePropertyScribe(Class<T> cls, String str) {
        super(cls, str);
    }

    /* renamed from: ezvcard.io.scribe.DateOrTimePropertyScribe$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$ezvcard$VCardVersion;

        static {
            int[] iArr = new int[VCardVersion.values().length];
            $SwitchMap$ezvcard$VCardVersion = iArr;
            try {
                iArr[VCardVersion.V2_1.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$ezvcard$VCardVersion[VCardVersion.V3_0.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$ezvcard$VCardVersion[VCardVersion.V4_0.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected VCardDataType _defaultDataType(VCardVersion vCardVersion) {
        if (AnonymousClass1.$SwitchMap$ezvcard$VCardVersion[vCardVersion.ordinal()] != 3) {
            return null;
        }
        return VCardDataType.DATE_AND_OR_TIME;
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected T _parseText(String str, VCardDataType vCardDataType, VCardVersion vCardVersion, VCardParameters vCardParameters, List<String> list) {
        String unescape = VCardPropertyScribe.unescape(str);
        if (vCardVersion == VCardVersion.V4_0 && vCardDataType == VCardDataType.TEXT) {
            return newInstance(unescape);
        }
        return parse(unescape, vCardVersion, list);
    }

    private T parse(String str, VCardVersion vCardVersion, List<String> list) {
        try {
            return newInstance(VCardPropertyScribe.date(str), str.contains("T"));
        } catch (IllegalArgumentException unused) {
            if (vCardVersion == VCardVersion.V2_1 || vCardVersion == VCardVersion.V3_0) {
                throw new CannotParseException(5, new Object[0]);
            }
            try {
                return newInstance(PartialDate.parse(str));
            } catch (IllegalArgumentException unused2) {
                list.add(Messages.INSTANCE.getParseMessage(6, new Object[0]));
                return newInstance(str);
            }
        }
    }
}
