package ezvcard.io.scribe;

import ezvcard.property.Deathplace;

/* loaded from: classes3.dex */
public class DeathplaceScribe extends PlacePropertyScribe<Deathplace> {
    public DeathplaceScribe() {
        super(Deathplace.class, "DEATHPLACE");
    }

    @Override // ezvcard.io.scribe.PlacePropertyScribe
    public Deathplace newInstance() {
        return new Deathplace();
    }
}
