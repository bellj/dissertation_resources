package ezvcard.io.scribe;

import ezvcard.parameter.ImageType;
import ezvcard.property.Photo;

/* loaded from: classes3.dex */
public class PhotoScribe extends ImagePropertyScribe<Photo> {
    public PhotoScribe() {
        super(Photo.class, "PHOTO");
    }

    public Photo _newInstance(String str, ImageType imageType) {
        return new Photo(str, imageType);
    }

    public Photo _newInstance(byte[] bArr, ImageType imageType) {
        return new Photo(bArr, imageType);
    }
}
