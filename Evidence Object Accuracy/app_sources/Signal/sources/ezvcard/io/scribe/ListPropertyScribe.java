package ezvcard.io.scribe;

import ezvcard.VCardDataType;
import ezvcard.VCardVersion;
import ezvcard.parameter.VCardParameters;
import ezvcard.property.TextListProperty;
import java.util.List;

/* loaded from: classes3.dex */
public abstract class ListPropertyScribe<T extends TextListProperty> extends VCardPropertyScribe<T> {
    protected abstract T _newInstance();

    public ListPropertyScribe(Class<T> cls, String str) {
        super(cls, str);
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected VCardDataType _defaultDataType(VCardVersion vCardVersion) {
        return VCardDataType.TEXT;
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected T _parseText(String str, VCardDataType vCardDataType, VCardVersion vCardVersion, VCardParameters vCardParameters, List<String> list) {
        return parse(VCardPropertyScribe.list(str));
    }

    private T parse(List<String> list) {
        T _newInstance = _newInstance();
        _newInstance.getValues().addAll(list);
        return _newInstance;
    }
}
