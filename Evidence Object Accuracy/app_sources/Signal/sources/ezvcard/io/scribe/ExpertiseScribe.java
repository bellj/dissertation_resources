package ezvcard.io.scribe;

import ezvcard.property.Expertise;

/* loaded from: classes3.dex */
public class ExpertiseScribe extends StringPropertyScribe<Expertise> {
    public ExpertiseScribe() {
        super(Expertise.class, "EXPERTISE");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public Expertise _parseValue(String str) {
        return new Expertise(str);
    }
}
