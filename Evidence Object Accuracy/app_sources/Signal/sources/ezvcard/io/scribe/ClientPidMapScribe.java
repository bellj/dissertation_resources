package ezvcard.io.scribe;

import ezvcard.VCardDataType;
import ezvcard.VCardVersion;
import ezvcard.io.CannotParseException;
import ezvcard.io.scribe.VCardPropertyScribe;
import ezvcard.parameter.VCardParameters;
import ezvcard.property.ClientPidMap;
import java.util.List;

/* loaded from: classes3.dex */
public class ClientPidMapScribe extends VCardPropertyScribe<ClientPidMap> {
    public ClientPidMapScribe() {
        super(ClientPidMap.class, "CLIENTPIDMAP");
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected VCardDataType _defaultDataType(VCardVersion vCardVersion) {
        return VCardDataType.TEXT;
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected ClientPidMap _parseText(String str, VCardDataType vCardDataType, VCardVersion vCardVersion, VCardParameters vCardParameters, List<String> list) {
        VCardPropertyScribe.SemiStructuredIterator semistructured = VCardPropertyScribe.semistructured(str, 2);
        String next = semistructured.next();
        String next2 = semistructured.next();
        if (next != null && next2 != null) {
            return parse(next, next2);
        }
        throw new CannotParseException(3, new Object[0]);
    }

    private ClientPidMap parse(String str, String str2) {
        try {
            return new ClientPidMap(Integer.valueOf(Integer.parseInt(str)), str2);
        } catch (NumberFormatException unused) {
            throw new CannotParseException(4, new Object[0]);
        }
    }
}
