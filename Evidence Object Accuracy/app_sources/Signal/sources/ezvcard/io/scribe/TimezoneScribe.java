package ezvcard.io.scribe;

import ezvcard.Messages;
import ezvcard.VCardDataType;
import ezvcard.VCardVersion;
import ezvcard.io.CannotParseException;
import ezvcard.parameter.VCardParameters;
import ezvcard.property.Timezone;
import ezvcard.util.UtcOffset;
import java.util.List;

/* loaded from: classes3.dex */
public class TimezoneScribe extends VCardPropertyScribe<Timezone> {
    public TimezoneScribe() {
        super(Timezone.class, "TZ");
    }

    /* renamed from: ezvcard.io.scribe.TimezoneScribe$1 */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$ezvcard$VCardVersion;

        static {
            int[] iArr = new int[VCardVersion.values().length];
            $SwitchMap$ezvcard$VCardVersion = iArr;
            try {
                iArr[VCardVersion.V2_1.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$ezvcard$VCardVersion[VCardVersion.V3_0.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$ezvcard$VCardVersion[VCardVersion.V4_0.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected VCardDataType _defaultDataType(VCardVersion vCardVersion) {
        int i = AnonymousClass1.$SwitchMap$ezvcard$VCardVersion[vCardVersion.ordinal()];
        if (i == 1 || i == 2) {
            return VCardDataType.UTC_OFFSET;
        }
        if (i != 3) {
            return null;
        }
        return VCardDataType.TEXT;
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected Timezone _parseText(String str, VCardDataType vCardDataType, VCardVersion vCardVersion, VCardParameters vCardParameters, List<String> list) {
        return parse(VCardPropertyScribe.unescape(str), vCardDataType, vCardVersion, list);
    }

    private Timezone parse(String str, VCardDataType vCardDataType, VCardVersion vCardVersion, List<String> list) {
        if (str == null || str.length() == 0) {
            return new Timezone((String) null);
        }
        int i = AnonymousClass1.$SwitchMap$ezvcard$VCardVersion[vCardVersion.ordinal()];
        if (i == 1) {
            try {
                return new Timezone(UtcOffset.parse(str));
            } catch (IllegalArgumentException unused) {
                throw new CannotParseException(19, new Object[0]);
            }
        } else if (i != 2 && i != 3) {
            return new Timezone((String) null);
        } else {
            try {
                return new Timezone(UtcOffset.parse(str));
            } catch (IllegalArgumentException unused2) {
                if (vCardDataType == VCardDataType.UTC_OFFSET) {
                    list.add(Messages.INSTANCE.getParseMessage(20, new Object[0]));
                }
                return new Timezone(str);
            }
        }
    }
}
