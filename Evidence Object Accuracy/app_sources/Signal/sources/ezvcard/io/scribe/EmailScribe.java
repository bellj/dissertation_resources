package ezvcard.io.scribe;

import ezvcard.property.Email;

/* loaded from: classes3.dex */
public class EmailScribe extends StringPropertyScribe<Email> {
    public EmailScribe() {
        super(Email.class, "EMAIL");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public Email _parseValue(String str) {
        return new Email(str);
    }
}
