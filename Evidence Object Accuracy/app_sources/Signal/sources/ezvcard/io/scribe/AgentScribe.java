package ezvcard.io.scribe;

import ezvcard.VCard;
import ezvcard.VCardDataType;
import ezvcard.VCardVersion;
import ezvcard.io.EmbeddedVCardException;
import ezvcard.parameter.VCardParameters;
import ezvcard.property.Agent;
import ezvcard.property.VCardProperty;
import java.util.List;

/* loaded from: classes3.dex */
public class AgentScribe extends VCardPropertyScribe<Agent> {
    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected VCardDataType _defaultDataType(VCardVersion vCardVersion) {
        return null;
    }

    public AgentScribe() {
        super(Agent.class, "AGENT");
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected Agent _parseText(String str, VCardDataType vCardDataType, VCardVersion vCardVersion, VCardParameters vCardParameters, List<String> list) {
        Agent agent = new Agent();
        if (vCardDataType != null) {
            agent.setUrl(VCardPropertyScribe.unescape(str));
            return agent;
        }
        throw new EmbeddedVCardException(new Injector(agent));
    }

    /* loaded from: classes3.dex */
    public static class Injector implements EmbeddedVCardException.InjectionCallback {
        private final Agent property;

        public Injector(Agent agent) {
            this.property = agent;
        }

        public void injectVCard(VCard vCard) {
            this.property.setVCard(vCard);
        }

        public VCardProperty getProperty() {
            return this.property;
        }
    }
}
