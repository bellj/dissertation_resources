package ezvcard.io.scribe;

import ezvcard.property.Interest;

/* loaded from: classes3.dex */
public class InterestScribe extends StringPropertyScribe<Interest> {
    public InterestScribe() {
        super(Interest.class, "INTEREST");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public Interest _parseValue(String str) {
        return new Interest(str);
    }
}
