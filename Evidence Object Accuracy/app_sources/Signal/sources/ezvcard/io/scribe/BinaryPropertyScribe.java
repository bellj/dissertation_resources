package ezvcard.io.scribe;

import ezvcard.VCardDataType;
import ezvcard.VCardVersion;
import ezvcard.parameter.Encoding;
import ezvcard.parameter.MediaTypeParameter;
import ezvcard.parameter.VCardParameters;
import ezvcard.property.BinaryProperty;
import ezvcard.util.DataUri;
import ezvcard.util.org.apache.commons.codec.binary.Base64;
import java.util.List;

/* loaded from: classes3.dex */
public abstract class BinaryPropertyScribe<T extends BinaryProperty<U>, U extends MediaTypeParameter> extends VCardPropertyScribe<T> {
    protected abstract U _mediaTypeFromFileExtension(String str);

    protected abstract U _mediaTypeFromMediaTypeParameter(String str);

    protected abstract U _mediaTypeFromTypeParameter(String str);

    protected abstract T _newInstance(String str, U u);

    protected abstract T _newInstance(byte[] bArr, U u);

    public BinaryPropertyScribe(Class<T> cls, String str) {
        super(cls, str);
    }

    /* renamed from: ezvcard.io.scribe.BinaryPropertyScribe$1 */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$ezvcard$VCardVersion;

        static {
            int[] iArr = new int[VCardVersion.values().length];
            $SwitchMap$ezvcard$VCardVersion = iArr;
            try {
                iArr[VCardVersion.V2_1.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$ezvcard$VCardVersion[VCardVersion.V3_0.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$ezvcard$VCardVersion[VCardVersion.V4_0.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected VCardDataType _defaultDataType(VCardVersion vCardVersion) {
        if (AnonymousClass1.$SwitchMap$ezvcard$VCardVersion[vCardVersion.ordinal()] != 3) {
            return null;
        }
        return VCardDataType.URI;
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected T _parseText(String str, VCardDataType vCardDataType, VCardVersion vCardVersion, VCardParameters vCardParameters, List<String> list) {
        return parse(VCardPropertyScribe.unescape(str), vCardDataType, vCardParameters, vCardVersion, list);
    }

    protected T cannotUnmarshalValue(String str, VCardVersion vCardVersion, List<String> list, U u) {
        int i = AnonymousClass1.$SwitchMap$ezvcard$VCardVersion[vCardVersion.ordinal()];
        if (i == 1 || i == 2) {
            if (str.startsWith("http")) {
                return _newInstance(str, (String) u);
            }
            return _newInstance(Base64.decodeBase64(str), (byte[]) u);
        } else if (i != 3) {
            return null;
        } else {
            return _newInstance(str, (String) u);
        }
    }

    private U parseContentType(String str, VCardParameters vCardParameters, VCardVersion vCardVersion) {
        String mediaType;
        int i = AnonymousClass1.$SwitchMap$ezvcard$VCardVersion[vCardVersion.ordinal()];
        if (i == 1 || i == 2) {
            String type = vCardParameters.getType();
            if (type != null) {
                return _mediaTypeFromTypeParameter(type);
            }
        } else if (i == 3 && (mediaType = vCardParameters.getMediaType()) != null) {
            return _mediaTypeFromMediaTypeParameter(mediaType);
        }
        String fileExtension = getFileExtension(str);
        if (fileExtension == null) {
            return null;
        }
        return _mediaTypeFromFileExtension(fileExtension);
    }

    private T parse(String str, VCardDataType vCardDataType, VCardParameters vCardParameters, VCardVersion vCardVersion, List<String> list) {
        U parseContentType = parseContentType(str, vCardParameters, vCardVersion);
        int i = AnonymousClass1.$SwitchMap$ezvcard$VCardVersion[vCardVersion.ordinal()];
        if (i == 1 || i == 2) {
            if (vCardDataType == VCardDataType.URL || vCardDataType == VCardDataType.URI) {
                return _newInstance(str, (String) parseContentType);
            }
            Encoding encoding = vCardParameters.getEncoding();
            if (encoding == Encoding.BASE64 || encoding == Encoding.B) {
                return _newInstance(Base64.decodeBase64(str), (byte[]) parseContentType);
            }
        } else if (i == 3) {
            try {
                DataUri parse = DataUri.parse(str);
                U _mediaTypeFromMediaTypeParameter = _mediaTypeFromMediaTypeParameter(parse.getContentType());
                try {
                    return _newInstance(parse.getData(), (byte[]) _mediaTypeFromMediaTypeParameter);
                } catch (IllegalArgumentException unused) {
                    parseContentType = _mediaTypeFromMediaTypeParameter;
                }
            } catch (IllegalArgumentException unused2) {
            }
        }
        return cannotUnmarshalValue(str, vCardVersion, list, parseContentType);
    }

    protected static String getFileExtension(String str) {
        int lastIndexOf = str.lastIndexOf(46);
        if (lastIndexOf < 0 || lastIndexOf == str.length() - 1 || str.lastIndexOf(47) > lastIndexOf) {
            return null;
        }
        return str.substring(lastIndexOf + 1);
    }
}
