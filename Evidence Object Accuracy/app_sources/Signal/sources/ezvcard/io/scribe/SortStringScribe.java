package ezvcard.io.scribe;

import ezvcard.property.SortString;

/* loaded from: classes3.dex */
public class SortStringScribe extends StringPropertyScribe<SortString> {
    public SortStringScribe() {
        super(SortString.class, "SORT-STRING");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public SortString _parseValue(String str) {
        return new SortString(str);
    }
}
