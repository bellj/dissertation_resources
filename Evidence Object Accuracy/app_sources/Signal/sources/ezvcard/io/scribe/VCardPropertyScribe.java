package ezvcard.io.scribe;

import ezvcard.VCardDataType;
import ezvcard.VCardVersion;
import ezvcard.parameter.VCardParameters;
import ezvcard.property.VCardProperty;
import ezvcard.util.StringUtils;
import ezvcard.util.VCardDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.xml.namespace.QName;

/* loaded from: classes3.dex */
public abstract class VCardPropertyScribe<T extends VCardProperty> {
    protected final Class<T> clazz;
    protected final String propertyName;
    protected final QName qname;

    protected abstract VCardDataType _defaultDataType(VCardVersion vCardVersion);

    protected abstract T _parseText(String str, VCardDataType vCardDataType, VCardVersion vCardVersion, VCardParameters vCardParameters, List<String> list);

    public VCardPropertyScribe(Class<T> cls, String str) {
        this(cls, str, new QName(VCardVersion.V4_0.getXmlNamespace(), str.toLowerCase()));
    }

    public VCardPropertyScribe(Class<T> cls, String str, QName qName) {
        this.clazz = cls;
        this.propertyName = str;
        this.qname = qName;
    }

    public Class<T> getPropertyClass() {
        return this.clazz;
    }

    public String getPropertyName() {
        return this.propertyName;
    }

    public QName getQName() {
        return this.qname;
    }

    public final VCardDataType defaultDataType(VCardVersion vCardVersion) {
        return _defaultDataType(vCardVersion);
    }

    public final Result<T> parseText(String str, VCardDataType vCardDataType, VCardVersion vCardVersion, VCardParameters vCardParameters) {
        ArrayList arrayList = new ArrayList(0);
        T _parseText = _parseText(str, vCardDataType, vCardVersion, vCardParameters, arrayList);
        _parseText.setParameters(vCardParameters);
        return new Result<>(_parseText, arrayList);
    }

    public static String unescape(String str) {
        StringBuilder sb = null;
        if (str == null) {
            return null;
        }
        boolean z = false;
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (z) {
                if (sb == null) {
                    sb = new StringBuilder(str.length());
                    sb.append(str.substring(0, i - 1));
                }
                if (charAt == 'n' || charAt == 'N') {
                    sb.append(StringUtils.NEWLINE);
                } else {
                    sb.append(charAt);
                }
                z = false;
            } else if (charAt == '\\') {
                z = true;
            } else if (sb != null) {
                sb.append(charAt);
            }
        }
        return sb == null ? str : sb.toString();
    }

    protected static Splitter splitter(char c) {
        return new Splitter(c);
    }

    /* loaded from: classes3.dex */
    public static class Splitter {
        private char delimiter;
        private int limit = -1;
        private boolean nullEmpties = false;
        private boolean unescape = false;

        public Splitter(char c) {
            this.delimiter = c;
        }

        public Splitter unescape(boolean z) {
            this.unescape = z;
            return this;
        }

        public Splitter limit(int i) {
            this.limit = i;
            return this;
        }

        public List<String> split(String str) {
            ArrayList arrayList = new ArrayList();
            boolean z = false;
            int i = 0;
            for (int i2 = 0; i2 < str.length(); i2++) {
                char charAt = str.charAt(i2);
                if (z) {
                    z = false;
                } else if (charAt == this.delimiter) {
                    add(str.substring(i, i2), arrayList);
                    i = i2 + 1;
                    if (this.limit > 0 && arrayList.size() == this.limit - 1) {
                        break;
                    }
                } else if (charAt == '\\') {
                    z = true;
                }
            }
            add(str.substring(i), arrayList);
            return arrayList;
        }

        private void add(String str, List<String> list) {
            String trim = str.trim();
            if (this.nullEmpties && trim.length() == 0) {
                trim = null;
            } else if (this.unescape) {
                trim = VCardPropertyScribe.unescape(trim);
            }
            list.add(trim);
        }
    }

    public static List<String> list(String str) {
        if (str.length() == 0) {
            return new ArrayList(0);
        }
        return splitter(',').unescape(true).split(str);
    }

    public static SemiStructuredIterator semistructured(String str) {
        return semistructured(str, -1);
    }

    public static SemiStructuredIterator semistructured(String str, int i) {
        return new SemiStructuredIterator(splitter(';').unescape(true).limit(i).split(str).iterator());
    }

    public static StructuredIterator structured(String str) {
        List<String> split = splitter(';').split(str);
        ArrayList arrayList = new ArrayList(split.size());
        for (String str2 : split) {
            arrayList.add(list(str2));
        }
        return new StructuredIterator(arrayList.iterator());
    }

    /* access modifiers changed from: protected */
    /* loaded from: classes3.dex */
    public static class StructuredIterator {
        private final Iterator<List<String>> it;

        public StructuredIterator(Iterator<List<String>> it) {
            this.it = it;
        }

        public String nextString() {
            if (!hasNext()) {
                return null;
            }
            List<String> next = this.it.next();
            if (next.isEmpty()) {
                return null;
            }
            String str = next.get(0);
            if (str.length() == 0) {
                return null;
            }
            return str;
        }

        public List<String> nextComponent() {
            if (!hasNext()) {
                return new ArrayList(0);
            }
            List<String> next = this.it.next();
            return (next.size() == 1 && next.get(0).length() == 0) ? new ArrayList(0) : next;
        }

        public boolean hasNext() {
            return this.it.hasNext();
        }
    }

    /* loaded from: classes3.dex */
    public static class SemiStructuredIterator {
        private final Iterator<String> it;

        public SemiStructuredIterator(Iterator<String> it) {
            this.it = it;
        }

        public String next() {
            if (hasNext()) {
                return this.it.next();
            }
            return null;
        }

        public boolean hasNext() {
            return this.it.hasNext();
        }
    }

    public static Date date(String str) {
        return VCardDateFormat.parse(str);
    }

    /* loaded from: classes3.dex */
    public static class Result<T extends VCardProperty> {
        private final T property;
        private final List<String> warnings;

        public Result(T t, List<String> list) {
            this.property = t;
            this.warnings = list;
        }

        public List<String> getWarnings() {
            return this.warnings;
        }

        public T getProperty() {
            return this.property;
        }
    }
}
