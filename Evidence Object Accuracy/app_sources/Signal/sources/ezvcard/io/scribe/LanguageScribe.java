package ezvcard.io.scribe;

import ezvcard.VCardDataType;
import ezvcard.property.Language;

/* loaded from: classes3.dex */
public class LanguageScribe extends StringPropertyScribe<Language> {
    public LanguageScribe() {
        super(Language.class, "LANG", VCardDataType.LANGUAGE_TAG);
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public Language _parseValue(String str) {
        return new Language(str);
    }
}
