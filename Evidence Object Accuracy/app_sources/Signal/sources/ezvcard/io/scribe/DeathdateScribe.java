package ezvcard.io.scribe;

import ezvcard.property.Deathdate;
import ezvcard.util.PartialDate;
import java.util.Date;

/* loaded from: classes3.dex */
public class DeathdateScribe extends DateOrTimePropertyScribe<Deathdate> {
    public DeathdateScribe() {
        super(Deathdate.class, "DEATHDATE");
    }

    public Deathdate newInstance(String str) {
        return new Deathdate(str);
    }

    public Deathdate newInstance(Date date, boolean z) {
        return new Deathdate(date, z);
    }

    public Deathdate newInstance(PartialDate partialDate) {
        return new Deathdate(partialDate);
    }
}
