package ezvcard.io.scribe;

import ezvcard.property.Member;

/* loaded from: classes3.dex */
public class MemberScribe extends StringPropertyScribe<Member> {
    public MemberScribe() {
        super(Member.class, "MEMBER");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public Member _parseValue(String str) {
        return new Member(str);
    }
}
