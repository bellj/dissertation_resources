package ezvcard.io.scribe;

import ezvcard.VCardDataType;
import ezvcard.property.TextProperty;

/* loaded from: classes3.dex */
public abstract class UriPropertyScribe<T extends TextProperty> extends StringPropertyScribe<T> {
    public UriPropertyScribe(Class<T> cls, String str) {
        super(cls, str, VCardDataType.URI);
    }
}
