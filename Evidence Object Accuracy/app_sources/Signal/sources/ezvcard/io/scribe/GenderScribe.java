package ezvcard.io.scribe;

import ezvcard.VCardDataType;
import ezvcard.VCardVersion;
import ezvcard.io.scribe.VCardPropertyScribe;
import ezvcard.parameter.VCardParameters;
import ezvcard.property.Gender;
import java.util.List;

/* loaded from: classes3.dex */
public class GenderScribe extends VCardPropertyScribe<Gender> {
    public GenderScribe() {
        super(Gender.class, "GENDER");
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected VCardDataType _defaultDataType(VCardVersion vCardVersion) {
        return VCardDataType.TEXT;
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected Gender _parseText(String str, VCardDataType vCardDataType, VCardVersion vCardVersion, VCardParameters vCardParameters, List<String> list) {
        VCardPropertyScribe.SemiStructuredIterator semistructured = VCardPropertyScribe.semistructured(str, 2);
        String next = semistructured.next();
        if (next != null) {
            if (next.length() == 0) {
                next = null;
            } else {
                next = next.toUpperCase();
            }
        }
        String next2 = semistructured.next();
        Gender gender = new Gender(next);
        gender.setText(next2);
        return gender;
    }
}
