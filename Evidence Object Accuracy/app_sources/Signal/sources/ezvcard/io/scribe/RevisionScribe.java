package ezvcard.io.scribe;

import ezvcard.VCardDataType;
import ezvcard.VCardVersion;
import ezvcard.io.CannotParseException;
import ezvcard.parameter.VCardParameters;
import ezvcard.property.Revision;
import java.util.List;

/* loaded from: classes3.dex */
public class RevisionScribe extends VCardPropertyScribe<Revision> {
    public RevisionScribe() {
        super(Revision.class, "REV");
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected VCardDataType _defaultDataType(VCardVersion vCardVersion) {
        return VCardDataType.TIMESTAMP;
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected Revision _parseText(String str, VCardDataType vCardDataType, VCardVersion vCardVersion, VCardParameters vCardParameters, List<String> list) {
        return parse(str);
    }

    private Revision parse(String str) {
        if (str == null || str.length() == 0) {
            return new Revision(null);
        }
        try {
            return new Revision(VCardPropertyScribe.date(str));
        } catch (IllegalArgumentException unused) {
            throw new CannotParseException(5, new Object[0]);
        }
    }
}
