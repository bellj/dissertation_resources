package ezvcard.io.scribe;

import ezvcard.VCardDataType;
import ezvcard.VCardVersion;
import ezvcard.io.CannotParseException;
import ezvcard.parameter.VCardParameters;
import ezvcard.property.Impp;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

/* loaded from: classes3.dex */
public class ImppScribe extends VCardPropertyScribe<Impp> {
    private static final List<HtmlLinkFormat> htmlLinkFormats;

    public ImppScribe() {
        super(Impp.class, "IMPP");
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected VCardDataType _defaultDataType(VCardVersion vCardVersion) {
        return VCardDataType.URI;
    }

    @Override // ezvcard.io.scribe.VCardPropertyScribe
    protected Impp _parseText(String str, VCardDataType vCardDataType, VCardVersion vCardVersion, VCardParameters vCardParameters, List<String> list) {
        return parse(VCardPropertyScribe.unescape(str));
    }

    private Impp parse(String str) {
        if (str == null || str.length() == 0) {
            return new Impp((URI) null);
        }
        try {
            return new Impp(str);
        } catch (IllegalArgumentException e) {
            throw new CannotParseException(15, str, e.getMessage());
        }
    }

    static {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new HtmlLinkFormat("aim", "(goim|addbuddy)\\?.*?\\bscreenname=(.*?)(&|$)", 2, "goim?screenname=%s"));
        arrayList.add(new HtmlLinkFormat("ymsgr", "(sendim|addfriend|sendfile|call)\\?(.*)", 2, "sendim?%s"));
        arrayList.add(new HtmlLinkFormat("skype", "(.*?)(\\?|$)", 1, "%s"));
        arrayList.add(new HtmlLinkFormat("msnim", "(chat|add|voice|video)\\?contact=(.*?)(&|$)", 2, "chat?contact=%s"));
        arrayList.add(new HtmlLinkFormat("xmpp", "(.*?)(\\?|$)", 1, "%s?message"));
        arrayList.add(new HtmlLinkFormat("icq", "message\\?uin=(\\d+)", 1, "message?uin=%s"));
        arrayList.add(new HtmlLinkFormat("sip"));
        arrayList.add(new HtmlLinkFormat("irc"));
        htmlLinkFormats = Collections.unmodifiableList(arrayList);
    }

    /* loaded from: classes3.dex */
    private static class HtmlLinkFormat {
        private final int handleGroup;
        private final String linkFormat;
        private final Pattern parseRegex;
        private final String protocol;

        public HtmlLinkFormat(String str) {
            this(str, "(.*)", 1, "%s");
        }

        public HtmlLinkFormat(String str, String str2, int i, String str3) {
            this.parseRegex = Pattern.compile('^' + str + ':' + str2, 2);
            this.protocol = str;
            this.handleGroup = i;
            this.linkFormat = str + ':' + str3;
        }
    }
}
