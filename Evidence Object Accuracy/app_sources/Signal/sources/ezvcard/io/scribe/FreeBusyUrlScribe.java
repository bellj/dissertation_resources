package ezvcard.io.scribe;

import ezvcard.property.FreeBusyUrl;

/* loaded from: classes3.dex */
public class FreeBusyUrlScribe extends UriPropertyScribe<FreeBusyUrl> {
    public FreeBusyUrlScribe() {
        super(FreeBusyUrl.class, "FBURL");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public FreeBusyUrl _parseValue(String str) {
        return new FreeBusyUrl(str);
    }
}
