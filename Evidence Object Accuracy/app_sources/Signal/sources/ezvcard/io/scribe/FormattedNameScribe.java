package ezvcard.io.scribe;

import ezvcard.property.FormattedName;

/* loaded from: classes3.dex */
public class FormattedNameScribe extends StringPropertyScribe<FormattedName> {
    public FormattedNameScribe() {
        super(FormattedName.class, "FN");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public FormattedName _parseValue(String str) {
        return new FormattedName(str);
    }
}
