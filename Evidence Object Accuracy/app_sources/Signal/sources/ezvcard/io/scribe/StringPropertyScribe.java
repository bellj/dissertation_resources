package ezvcard.io.scribe;

import ezvcard.VCardDataType;
import ezvcard.property.TextProperty;

/* loaded from: classes3.dex */
public abstract class StringPropertyScribe<T extends TextProperty> extends SimplePropertyScribe<T> {
    public StringPropertyScribe(Class<T> cls, String str) {
        this(cls, str, VCardDataType.TEXT);
    }

    public StringPropertyScribe(Class<T> cls, String str, VCardDataType vCardDataType) {
        super(cls, str, vCardDataType);
    }
}
