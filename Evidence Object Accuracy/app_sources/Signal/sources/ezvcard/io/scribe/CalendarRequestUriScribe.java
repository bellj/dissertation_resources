package ezvcard.io.scribe;

import ezvcard.property.CalendarRequestUri;

/* loaded from: classes3.dex */
public class CalendarRequestUriScribe extends UriPropertyScribe<CalendarRequestUri> {
    public CalendarRequestUriScribe() {
        super(CalendarRequestUri.class, "CALADRURI");
    }

    @Override // ezvcard.io.scribe.SimplePropertyScribe
    public CalendarRequestUri _parseValue(String str) {
        return new CalendarRequestUri(str);
    }
}
