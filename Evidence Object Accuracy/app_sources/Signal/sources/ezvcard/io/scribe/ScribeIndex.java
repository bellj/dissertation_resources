package ezvcard.io.scribe;

import ezvcard.property.VCardProperty;
import java.util.HashMap;
import java.util.Map;
import javax.xml.namespace.QName;

/* loaded from: classes3.dex */
public class ScribeIndex {
    private static final Map<Class<? extends VCardProperty>, VCardPropertyScribe<? extends VCardProperty>> standardByClass = new HashMap();
    private static final Map<String, VCardPropertyScribe<? extends VCardProperty>> standardByName = new HashMap();
    private static final Map<QName, VCardPropertyScribe<? extends VCardProperty>> standardByQName = new HashMap();
    private final Map<Class<? extends VCardProperty>, VCardPropertyScribe<? extends VCardProperty>> extendedByClass = new HashMap(0);
    private final Map<String, VCardPropertyScribe<? extends VCardProperty>> extendedByName = new HashMap(0);
    private final Map<QName, VCardPropertyScribe<? extends VCardProperty>> extendedByQName = new HashMap(0);

    static {
        standardByName = new HashMap();
        standardByClass = new HashMap();
        standardByQName = new HashMap();
        registerStandard(new AddressScribe());
        registerStandard(new AgentScribe());
        registerStandard(new AnniversaryScribe());
        registerStandard(new BirthdayScribe());
        registerStandard(new CalendarRequestUriScribe());
        registerStandard(new CalendarUriScribe());
        registerStandard(new CategoriesScribe());
        registerStandard(new ClassificationScribe());
        registerStandard(new ClientPidMapScribe());
        registerStandard(new EmailScribe());
        registerStandard(new FreeBusyUrlScribe());
        registerStandard(new FormattedNameScribe());
        registerStandard(new GenderScribe());
        registerStandard(new GeoScribe());
        registerStandard(new ImppScribe());
        registerStandard(new KeyScribe());
        registerStandard(new KindScribe());
        registerStandard(new LabelScribe());
        registerStandard(new LanguageScribe());
        registerStandard(new LogoScribe());
        registerStandard(new MailerScribe());
        registerStandard(new MemberScribe());
        registerStandard(new NicknameScribe());
        registerStandard(new NoteScribe());
        registerStandard(new OrganizationScribe());
        registerStandard(new PhotoScribe());
        registerStandard(new ProductIdScribe());
        registerStandard(new ProfileScribe());
        registerStandard(new RelatedScribe());
        registerStandard(new RevisionScribe());
        registerStandard(new RoleScribe());
        registerStandard(new SortStringScribe());
        registerStandard(new SoundScribe());
        registerStandard(new SourceDisplayTextScribe());
        registerStandard(new SourceScribe());
        registerStandard(new StructuredNameScribe());
        registerStandard(new TelephoneScribe());
        registerStandard(new TimezoneScribe());
        registerStandard(new TitleScribe());
        registerStandard(new UidScribe());
        registerStandard(new UrlScribe());
        registerStandard(new XmlScribe());
        registerStandard(new BirthplaceScribe());
        registerStandard(new DeathdateScribe());
        registerStandard(new DeathplaceScribe());
        registerStandard(new ExpertiseScribe());
        registerStandard(new OrgDirectoryScribe());
        registerStandard(new InterestScribe());
        registerStandard(new HobbyScribe());
    }

    public VCardPropertyScribe<? extends VCardProperty> getPropertyScribe(String str) {
        String upperCase = str.toUpperCase();
        VCardPropertyScribe<? extends VCardProperty> vCardPropertyScribe = this.extendedByName.get(upperCase);
        if (vCardPropertyScribe != null) {
            return vCardPropertyScribe;
        }
        return standardByName.get(upperCase);
    }

    private static void registerStandard(VCardPropertyScribe<? extends VCardProperty> vCardPropertyScribe) {
        standardByName.put(vCardPropertyScribe.getPropertyName().toUpperCase(), vCardPropertyScribe);
        standardByClass.put(vCardPropertyScribe.getPropertyClass(), vCardPropertyScribe);
        standardByQName.put(vCardPropertyScribe.getQName(), vCardPropertyScribe);
    }
}
