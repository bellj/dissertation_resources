package ezvcard.io;

import ezvcard.VCard;
import ezvcard.property.VCardProperty;

/* loaded from: classes3.dex */
public class EmbeddedVCardException extends RuntimeException {
    private final InjectionCallback callback;
    private final VCard vcard = null;

    /* loaded from: classes3.dex */
    public interface InjectionCallback {
        VCardProperty getProperty();

        void injectVCard(VCard vCard);
    }

    public EmbeddedVCardException(InjectionCallback injectionCallback) {
        this.callback = injectionCallback;
    }

    public void injectVCard(VCard vCard) {
        InjectionCallback injectionCallback = this.callback;
        if (injectionCallback != null) {
            injectionCallback.injectVCard(vCard);
        }
    }

    public VCardProperty getProperty() {
        InjectionCallback injectionCallback = this.callback;
        if (injectionCallback == null) {
            return null;
        }
        return injectionCallback.getProperty();
    }
}
