package ezvcard.io.chain;

import ezvcard.VCard;
import ezvcard.io.StreamReader;
import ezvcard.io.chain.ChainingTextParser;
import ezvcard.io.text.VCardReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.List;

/* loaded from: classes3.dex */
public class ChainingTextParser<T extends ChainingTextParser<?>> extends ChainingParser<T> {
    private boolean caretDecoding = true;

    @Override // ezvcard.io.chain.ChainingParser
    public /* bridge */ /* synthetic */ List all() throws IOException {
        return super.all();
    }

    @Override // ezvcard.io.chain.ChainingParser
    public /* bridge */ /* synthetic */ VCard first() throws IOException {
        return super.first();
    }

    public ChainingTextParser(String str) {
        super(str);
    }

    public ChainingTextParser(InputStream inputStream) {
        super(inputStream);
    }

    @Override // ezvcard.io.chain.ChainingParser
    StreamReader constructReader() throws IOException {
        VCardReader newReader = newReader();
        newReader.setCaretDecodingEnabled(this.caretDecoding);
        return newReader;
    }

    private VCardReader newReader() throws IOException {
        String str = this.string;
        if (str != null) {
            return new VCardReader(str);
        }
        InputStream inputStream = this.in;
        if (inputStream != null) {
            return new VCardReader(inputStream);
        }
        Reader reader = this.reader;
        if (reader != null) {
            return new VCardReader(reader);
        }
        return new VCardReader(this.file);
    }
}
