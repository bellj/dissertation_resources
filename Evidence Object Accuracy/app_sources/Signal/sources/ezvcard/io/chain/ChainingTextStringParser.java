package ezvcard.io.chain;

import ezvcard.VCard;
import java.io.IOException;
import java.util.List;

/* loaded from: classes3.dex */
public class ChainingTextStringParser extends ChainingTextParser<ChainingTextStringParser> {
    public ChainingTextStringParser(String str) {
        super(str);
    }

    @Override // ezvcard.io.chain.ChainingTextParser, ezvcard.io.chain.ChainingParser
    public VCard first() {
        try {
            return super.first();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override // ezvcard.io.chain.ChainingTextParser, ezvcard.io.chain.ChainingParser
    public List<VCard> all() {
        try {
            return super.all();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
