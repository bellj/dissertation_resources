package ezvcard.io.chain;

import ezvcard.VCard;
import ezvcard.io.StreamReader;
import ezvcard.io.chain.ChainingParser;
import ezvcard.io.scribe.ScribeIndex;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes3.dex */
public abstract class ChainingParser<T extends ChainingParser<?>> {
    final File file;
    final InputStream in;
    ScribeIndex index;
    final Reader reader;
    final String string;
    final T this_;
    List<List<String>> warnings;

    abstract StreamReader constructReader() throws IOException;

    public ChainingParser(String str) {
        this(str, null, null, null);
    }

    public ChainingParser(InputStream inputStream) {
        this(null, inputStream, null, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: ezvcard.io.chain.ChainingParser<T extends ezvcard.io.chain.ChainingParser<?>> */
    /* JADX WARN: Multi-variable type inference failed */
    private ChainingParser(String str, InputStream inputStream, Reader reader, File file) {
        this.this_ = this;
        this.string = str;
        this.in = inputStream;
        this.reader = reader;
        this.file = file;
    }

    public VCard first() throws IOException {
        StreamReader constructReader = constructReader();
        ScribeIndex scribeIndex = this.index;
        if (scribeIndex != null) {
            constructReader.setScribeIndex(scribeIndex);
        }
        try {
            VCard readNext = constructReader.readNext();
            List<List<String>> list = this.warnings;
            if (list != null) {
                list.add(constructReader.getWarnings());
            }
            return readNext;
        } finally {
            if (closeWhenDone()) {
                constructReader.close();
            }
        }
    }

    public List<VCard> all() throws IOException {
        StreamReader constructReader = constructReader();
        try {
            ArrayList arrayList = new ArrayList();
            while (true) {
                VCard readNext = constructReader.readNext();
                if (readNext == null) {
                    break;
                }
                List<List<String>> list = this.warnings;
                if (list != null) {
                    list.add(constructReader.getWarnings());
                }
                arrayList.add(readNext);
            }
            return arrayList;
        } finally {
            if (closeWhenDone()) {
                constructReader.close();
            }
        }
    }

    private boolean closeWhenDone() {
        return this.in == null && this.reader == null;
    }
}
