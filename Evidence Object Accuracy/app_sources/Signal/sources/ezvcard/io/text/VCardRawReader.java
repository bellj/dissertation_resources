package ezvcard.io.text;

import ezvcard.Messages;
import ezvcard.VCardVersion;
import ezvcard.parameter.Encoding;
import ezvcard.parameter.VCardParameters;
import ezvcard.util.ClearableStringBuilder;
import ezvcard.util.StringUtils;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/* loaded from: classes3.dex */
public class VCardRawReader implements Closeable {
    private final ClearableStringBuilder buffer = new ClearableStringBuilder();
    private boolean caretDecodingEnabled;
    private boolean eos;
    private int lineNum;
    private int prevChar;
    private int propertyLineNum;
    private final Reader reader;
    private final ClearableStringBuilder unfoldedLine = new ClearableStringBuilder();
    private VCardVersion version;
    private final Map<String, VCardVersion> versionAliases = new HashMap();

    private boolean isNewline(char c) {
        return c == '\n' || c == '\r';
    }

    private boolean isWhitespace(char c) {
        return c == ' ' || c == '\t';
    }

    public VCardRawReader(Reader reader) {
        VCardVersion[] values = VCardVersion.values();
        for (VCardVersion vCardVersion : values) {
            this.versionAliases.put(vCardVersion.getVersion(), vCardVersion);
        }
        this.eos = false;
        this.caretDecodingEnabled = true;
        this.version = VCardVersion.V2_1;
        this.prevChar = -1;
        this.propertyLineNum = 1;
        this.lineNum = 1;
        this.reader = reader;
    }

    public int getLineNumber() {
        return this.propertyLineNum;
    }

    public VCardVersion getVersion() {
        return this.version;
    }

    public VCardRawLine readLine() throws IOException {
        VCardRawLine vCardRawLine = null;
        if (this.eos) {
            return null;
        }
        this.propertyLineNum = this.lineNum;
        this.buffer.clear();
        this.unfoldedLine.clear();
        VCardParameters vCardParameters = new VCardParameters();
        boolean z = true;
        String str = null;
        String str2 = null;
        String str3 = null;
        char c = 0;
        boolean z2 = false;
        boolean z3 = false;
        char c2 = 0;
        boolean z4 = false;
        boolean z5 = false;
        while (true) {
            int nextChar = nextChar();
            if (nextChar < 0) {
                this.eos = z;
                break;
            }
            char c3 = (char) nextChar;
            if (c != '\r' || c3 != '\n') {
                if (isNewline(c3)) {
                    z3 = z2 && c == '=' && isQuotedPrintable(vCardParameters);
                    if (z3) {
                        this.buffer.chop();
                        this.unfoldedLine.chop();
                    }
                    int i = this.lineNum;
                    int i2 = z ? 1 : 0;
                    int i3 = z ? 1 : 0;
                    int i4 = z ? 1 : 0;
                    int i5 = z ? 1 : 0;
                    int i6 = z ? 1 : 0;
                    int i7 = z ? 1 : 0;
                    this.lineNum = i + i2;
                    c = c3;
                } else {
                    if (isNewline(c)) {
                        if (!isWhitespace(c3)) {
                            if (!z3) {
                                this.prevChar = c3;
                                break;
                            }
                        } else {
                            c = c3;
                            z4 = true;
                        }
                    }
                    if (z4) {
                        if (!isWhitespace(c3) || this.version != VCardVersion.V2_1) {
                            z4 = false;
                        }
                    }
                    this.unfoldedLine.append(c3);
                    if (z2) {
                        this.buffer.append(c3);
                    } else if (c2 != 0) {
                        if (c2 == '\\') {
                            if (c3 == '\\') {
                                this.buffer.append(c3);
                            } else if (c3 == 'n' || c3 == 'N') {
                                this.buffer.append(StringUtils.NEWLINE);
                            } else if (c3 == '\"' && this.version != VCardVersion.V2_1) {
                                this.buffer.append(c3);
                            } else if (c3 == ';' && this.version == VCardVersion.V2_1) {
                                this.buffer.append(c3);
                            } else {
                                this.buffer.append(c2).append(c3);
                            }
                        } else if (c2 == '^') {
                            if (c3 == '^') {
                                this.buffer.append(c3);
                            } else if (c3 == 'n') {
                                this.buffer.append(StringUtils.NEWLINE);
                            } else if (c3 == '\'') {
                                this.buffer.append('\"');
                            } else {
                                this.buffer.append(c2).append(c3);
                            }
                        }
                        c = c3;
                        vCardRawLine = null;
                        z = true;
                        c2 = 0;
                    } else if (c3 == '\\' || (c3 == '^' && this.version != VCardVersion.V2_1 && this.caretDecodingEnabled)) {
                        c = c3;
                        c2 = c;
                        vCardRawLine = null;
                        z = true;
                    } else if (c3 == '.' && str2 == null && str == null) {
                        str2 = this.buffer.getAndClear();
                    } else if ((c3 == ';' || c3 == ':') && !z5) {
                        if (str == null) {
                            str = this.buffer.getAndClear();
                        } else {
                            String andClear = this.buffer.getAndClear();
                            if (this.version == VCardVersion.V2_1) {
                                andClear = StringUtils.ltrim(andClear);
                            }
                            vCardParameters.put(str3, andClear);
                            str3 = null;
                        }
                        if (c3 == ':') {
                            c = c3;
                            vCardRawLine = null;
                            z = true;
                            z2 = true;
                        }
                    } else if (c3 == ',' && !z5 && this.version != VCardVersion.V2_1) {
                        vCardParameters.put(str3, this.buffer.getAndClear());
                    } else if (c3 == '=' && str3 == null) {
                        String andClear2 = this.buffer.getAndClear();
                        if (this.version == VCardVersion.V2_1) {
                            andClear2 = StringUtils.rtrim(andClear2);
                        }
                        str3 = andClear2;
                    } else if (c3 != '\"' || this.version == VCardVersion.V2_1) {
                        this.buffer.append(c3);
                    } else {
                        z5 = !z5;
                    }
                    c = c3;
                    vCardRawLine = null;
                    z = true;
                }
            }
            c = c3;
            vCardRawLine = null;
        }
        if (this.unfoldedLine.length() == 0) {
            return vCardRawLine;
        }
        if (str != null) {
            String andClear3 = this.buffer.getAndClear();
            if ("VERSION".equalsIgnoreCase(str)) {
                VCardVersion vCardVersion = this.versionAliases.get(andClear3);
                if (vCardVersion != null) {
                    this.version = vCardVersion;
                } else {
                    throw new VCardParseException(this.unfoldedLine.get(), this.propertyLineNum, Messages.INSTANCE.getExceptionMessage(6, new Object[0]));
                }
            }
            return new VCardRawLine(str2, str, vCardParameters, andClear3);
        }
        throw new VCardParseException(this.unfoldedLine.get(), this.propertyLineNum, Messages.INSTANCE.getExceptionMessage(5, new Object[0]));
    }

    public void setCaretDecodingEnabled(boolean z) {
        this.caretDecodingEnabled = z;
    }

    public Charset getEncoding() {
        String encoding;
        Reader reader = this.reader;
        if (!(reader instanceof InputStreamReader) || (encoding = ((InputStreamReader) reader).getEncoding()) == null) {
            return null;
        }
        return Charset.forName(encoding);
    }

    private int nextChar() throws IOException {
        int i = this.prevChar;
        if (i < 0) {
            return this.reader.read();
        }
        this.prevChar = -1;
        return i;
    }

    private boolean isQuotedPrintable(VCardParameters vCardParameters) {
        if (vCardParameters.getEncoding() == Encoding.QUOTED_PRINTABLE) {
            return true;
        }
        for (String str : vCardParameters.get(null)) {
            if ("QUOTED-PRINTABLE".equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.reader.close();
    }
}
