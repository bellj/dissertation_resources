package ezvcard.io.text;

import ezvcard.VCard;
import ezvcard.VCardDataType;
import ezvcard.VCardVersion;
import ezvcard.io.CannotParseException;
import ezvcard.io.EmbeddedVCardException;
import ezvcard.io.StreamReader;
import ezvcard.io.scribe.RawPropertyScribe;
import ezvcard.io.scribe.VCardPropertyScribe;
import ezvcard.parameter.Encoding;
import ezvcard.parameter.VCardParameters;
import ezvcard.property.Label;
import ezvcard.property.RawProperty;
import ezvcard.property.VCardProperty;
import ezvcard.util.IOUtils;
import ezvcard.util.org.apache.commons.codec.DecoderException;
import ezvcard.util.org.apache.commons.codec.net.QuotedPrintableCodec;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes3.dex */
public class VCardReader extends StreamReader {
    private Charset defaultQuotedPrintableCharset;
    private final VCardRawReader reader;

    public VCardReader(String str) {
        this(new StringReader(str));
    }

    public VCardReader(InputStream inputStream) {
        this(new InputStreamReader(inputStream));
    }

    public VCardReader(File file) throws FileNotFoundException {
        this(new BufferedReader(new FileReader(file)));
    }

    public VCardReader(Reader reader) {
        VCardRawReader vCardRawReader = new VCardRawReader(reader);
        this.reader = vCardRawReader;
        Charset encoding = vCardRawReader.getEncoding();
        this.defaultQuotedPrintableCharset = encoding;
        if (encoding == null) {
            this.defaultQuotedPrintableCharset = Charset.defaultCharset();
        }
    }

    public void setCaretDecodingEnabled(boolean z) {
        this.reader.setCaretDecodingEnabled(z);
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:108:0x000a */
    /* JADX DEBUG: Multi-variable search result rejected for r5v11, resolved type: ezvcard.io.EmbeddedVCardException */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // ezvcard.io.StreamReader
    protected VCard _readNext() throws IOException {
        VCard vCard;
        VCardRawLine readLine;
        VCard vCard2 = null;
        VCardStack vCardStack = new VCardStack();
        VCard vCard3 = null;
        EmbeddedVCardException embeddedVCardException = null;
        while (true) {
            try {
                readLine = this.reader.readLine();
            } catch (VCardParseException e) {
                if (!vCardStack.isEmpty()) {
                    vCard = null;
                    this.warnings.add(Integer.valueOf(e.getLineNumber()), null, 27, e.getMessage(), e.getLine());
                } else {
                    vCard = null;
                }
            }
            if (readLine == null) {
                break;
            } else if (!"BEGIN".equalsIgnoreCase(readLine.getName()) || !"VCARD".equalsIgnoreCase(readLine.getValue())) {
                if (!vCardStack.isEmpty()) {
                    if ("VERSION".equalsIgnoreCase(readLine.getName())) {
                        vCardStack.peek().vcard.setVersion(this.reader.getVersion());
                    } else if ("END".equalsIgnoreCase(readLine.getName()) && "VCARD".equalsIgnoreCase(readLine.getValue())) {
                        VCardStack.Item pop = vCardStack.pop();
                        assignLabels(pop.vcard, pop.labels);
                        if (vCardStack.isEmpty()) {
                            break;
                        }
                    } else {
                        String group = readLine.getGroup();
                        VCardParameters parameters = readLine.getParameters();
                        String name = readLine.getName();
                        String value = readLine.getValue();
                        embeddedVCardException = embeddedVCardException;
                        if (embeddedVCardException != null) {
                            embeddedVCardException.injectVCard(vCard2);
                            embeddedVCardException = vCard2;
                        }
                        VCard vCard4 = vCardStack.peek().vcard;
                        VCardVersion version = vCard4.getVersion();
                        processNamelessParameters(parameters);
                        processQuotedMultivaluedTypeParams(parameters);
                        try {
                            value = decodeQuotedPrintableValue(name, parameters, value);
                        } catch (DecoderException e2) {
                            this.warnings.add(Integer.valueOf(this.reader.getLineNumber()), name, 38, e2.getMessage());
                        }
                        VCardPropertyScribe<? extends VCardProperty> propertyScribe = this.index.getPropertyScribe(name);
                        if (propertyScribe == null) {
                            propertyScribe = new RawPropertyScribe(name);
                        }
                        VCardDataType value2 = parameters.getValue();
                        parameters.setValue(null);
                        if (value2 == null) {
                            value2 = propertyScribe.defaultDataType(version);
                        }
                        try {
                            VCardPropertyScribe.Result<? extends VCardProperty> parseText = propertyScribe.parseText(value, value2, version, parameters);
                            for (String str : parseText.getWarnings()) {
                                this.warnings.add(Integer.valueOf(this.reader.getLineNumber()), name, str);
                            }
                            VCardProperty property = parseText.getProperty();
                            property.setGroup(group);
                            if (property instanceof Label) {
                                vCardStack.peek().labels.add((Label) property);
                            } else {
                                vCard4.addProperty(property);
                            }
                        } catch (CannotParseException e3) {
                            this.warnings.add(Integer.valueOf(this.reader.getLineNumber()), name, 25, value, e3.getMessage());
                            RawProperty property2 = new RawPropertyScribe(name).parseText(value, value2, version, parameters).getProperty();
                            property2.setGroup(group);
                            vCard4.addProperty(property2);
                        } catch (EmbeddedVCardException e4) {
                            VCardProperty property3 = e4.getProperty();
                            if (value.length() == 0 || version == VCardVersion.V2_1) {
                                embeddedVCardException = e4;
                            } else {
                                VCardReader vCardReader = new VCardReader(VCardPropertyScribe.unescape(value));
                                vCardReader.setScribeIndex(this.index);
                                try {
                                    VCard readNext = vCardReader.readNext();
                                    if (readNext != null) {
                                        e4.injectVCard(readNext);
                                    }
                                    Iterator<String> it = vCardReader.getWarnings().iterator();
                                    while (it.hasNext()) {
                                        this.warnings.add(Integer.valueOf(this.reader.getLineNumber()), name, 26, it.next());
                                    }
                                } catch (IOException unused) {
                                    Iterator<String> it2 = vCardReader.getWarnings().iterator();
                                    while (it2.hasNext()) {
                                        this.warnings.add(Integer.valueOf(this.reader.getLineNumber()), name, 26, it2.next());
                                    }
                                } catch (Throwable th) {
                                    Iterator<String> it3 = vCardReader.getWarnings().iterator();
                                    while (it3.hasNext()) {
                                        this.warnings.add(Integer.valueOf(this.reader.getLineNumber()), name, 26, it3.next());
                                    }
                                    IOUtils.closeQuietly(vCardReader);
                                    throw th;
                                }
                                IOUtils.closeQuietly(vCardReader);
                            }
                            vCard4.addProperty(property3);
                        }
                        vCard2 = null;
                    }
                }
                vCard = vCard2;
                vCard2 = vCard;
            } else {
                VCard vCard5 = new VCard();
                vCard5.setVersion(this.reader.getVersion());
                vCardStack.push(vCard5);
                if (vCard3 == null) {
                    vCard3 = vCard5;
                }
                if (embeddedVCardException != null) {
                    embeddedVCardException.injectVCard(vCard5);
                    embeddedVCardException = vCard2;
                }
            }
        }
        return vCard3;
    }

    private void processNamelessParameters(VCardParameters vCardParameters) {
        for (String str : vCardParameters.removeAll(null)) {
            vCardParameters.put(guessParameterName(str), str);
        }
    }

    private String guessParameterName(String str) {
        if (VCardDataType.find(str) != null) {
            return "VALUE";
        }
        return Encoding.find(str) != null ? "ENCODING" : "TYPE";
    }

    private void processQuotedMultivaluedTypeParams(VCardParameters vCardParameters) {
        String str;
        List<String> types = vCardParameters.getTypes();
        Iterator<String> it = types.iterator();
        while (true) {
            if (!it.hasNext()) {
                str = null;
                break;
            }
            str = it.next();
            if (str.indexOf(44) >= 0) {
                break;
            }
        }
        if (str != null) {
            types.clear();
            int i = -1;
            while (true) {
                int i2 = i + 1;
                int indexOf = str.indexOf(44, i2);
                if (indexOf >= 0) {
                    types.add(str.substring(i2, indexOf));
                    i = indexOf;
                } else {
                    types.add(str.substring(i2));
                    return;
                }
            }
        }
    }

    private String decodeQuotedPrintableValue(String str, VCardParameters vCardParameters, String str2) throws DecoderException {
        Charset charset;
        if (vCardParameters.getEncoding() != Encoding.QUOTED_PRINTABLE) {
            return str2;
        }
        Charset charset2 = null;
        vCardParameters.setEncoding(null);
        String charset3 = vCardParameters.getCharset();
        if (charset3 == null) {
            charset = this.defaultQuotedPrintableCharset;
        } else {
            try {
                charset2 = Charset.forName(charset3);
            } catch (IllegalCharsetNameException | UnsupportedCharsetException unused) {
            }
            if (charset2 == null) {
                charset2 = this.defaultQuotedPrintableCharset;
                this.warnings.add(Integer.valueOf(this.reader.getLineNumber()), str, 23, charset3, charset2.name());
            }
            charset = charset2;
        }
        return new QuotedPrintableCodec(charset.name()).decode(str2);
    }

    /* loaded from: classes3.dex */
    private static class VCardStack {
        private final List<Item> stack;

        private VCardStack() {
            this.stack = new ArrayList();
        }

        public void push(VCard vCard) {
            this.stack.add(new Item(vCard, new ArrayList()));
        }

        public Item pop() {
            if (isEmpty()) {
                return null;
            }
            List<Item> list = this.stack;
            return list.remove(list.size() - 1);
        }

        public Item peek() {
            if (isEmpty()) {
                return null;
            }
            List<Item> list = this.stack;
            return list.get(list.size() - 1);
        }

        public boolean isEmpty() {
            return this.stack.isEmpty();
        }

        /* loaded from: classes3.dex */
        public static class Item {
            public final List<Label> labels;
            public final VCard vcard;

            public Item(VCard vCard, List<Label> list) {
                this.vcard = vCard;
                this.labels = list;
            }
        }
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.reader.close();
    }
}
