package ezvcard.io.text;

import ezvcard.parameter.VCardParameters;

/* loaded from: classes3.dex */
public class VCardRawLine {
    private final String group;
    private final String name;
    private final VCardParameters parameters;
    private final String value;

    public VCardRawLine(String str, String str2, VCardParameters vCardParameters, String str3) {
        this.group = str;
        this.name = str2;
        this.value = str3;
        this.parameters = vCardParameters;
    }

    public String getGroup() {
        return this.group;
    }

    public String getName() {
        return this.name;
    }

    public String getValue() {
        return this.value;
    }

    public VCardParameters getParameters() {
        return this.parameters;
    }

    public int hashCode() {
        String str = this.group;
        int i = 0;
        int hashCode = ((str == null ? 0 : str.hashCode()) + 31) * 31;
        String str2 = this.name;
        int hashCode2 = (hashCode + (str2 == null ? 0 : str2.hashCode())) * 31;
        VCardParameters vCardParameters = this.parameters;
        int hashCode3 = (hashCode2 + (vCardParameters == null ? 0 : vCardParameters.hashCode())) * 31;
        String str3 = this.value;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode3 + i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        VCardRawLine vCardRawLine = (VCardRawLine) obj;
        String str = this.group;
        if (str == null) {
            if (vCardRawLine.group != null) {
                return false;
            }
        } else if (!str.equals(vCardRawLine.group)) {
            return false;
        }
        String str2 = this.name;
        if (str2 == null) {
            if (vCardRawLine.name != null) {
                return false;
            }
        } else if (!str2.equals(vCardRawLine.name)) {
            return false;
        }
        VCardParameters vCardParameters = this.parameters;
        if (vCardParameters == null) {
            if (vCardRawLine.parameters != null) {
                return false;
            }
        } else if (!vCardParameters.equals(vCardRawLine.parameters)) {
            return false;
        }
        String str3 = this.value;
        if (str3 == null) {
            if (vCardRawLine.value != null) {
                return false;
            }
        } else if (!str3.equals(vCardRawLine.value)) {
            return false;
        }
        return true;
    }
}
