package ezvcard.io.text;

import java.io.IOException;

/* loaded from: classes3.dex */
public class VCardParseException extends IOException {
    private final String line;
    private final int lineNumber;

    public VCardParseException(String str, int i, String str2) {
        super(str2);
        this.line = str;
        this.lineNumber = i;
    }

    public String getLine() {
        return this.line;
    }

    public int getLineNumber() {
        return this.lineNumber;
    }
}
