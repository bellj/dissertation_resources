package ezvcard.io;

import ezvcard.VCard;
import ezvcard.io.scribe.ScribeIndex;
import ezvcard.parameter.AddressType;
import ezvcard.property.Address;
import ezvcard.property.Label;
import java.io.Closeable;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/* loaded from: classes3.dex */
public abstract class StreamReader implements Closeable {
    protected ScribeIndex index = new ScribeIndex();
    protected final ParseWarnings warnings = new ParseWarnings();

    protected abstract VCard _readNext() throws IOException;

    public VCard readNext() throws IOException {
        this.warnings.clear();
        return _readNext();
    }

    public void assignLabels(VCard vCard, List<Label> list) {
        List<Address> addresses = vCard.getAddresses();
        for (Label label : list) {
            boolean z = true;
            List<AddressType> types = label.getTypes();
            Iterator<Address> it = addresses.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Address next = it.next();
                if (next.getLabel() == null && next.getTypes().equals(types)) {
                    next.setLabel(label.getValue());
                    z = false;
                    break;
                }
            }
            if (z) {
                vCard.addOrphanedLabel(label);
            }
        }
    }

    public void setScribeIndex(ScribeIndex scribeIndex) {
        this.index = scribeIndex;
    }

    public List<String> getWarnings() {
        return this.warnings.copy();
    }
}
