package ezvcard;

import ezvcard.util.CaseClasses;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* loaded from: classes3.dex */
public class VCardDataType {
    public static final VCardDataType BINARY = new VCardDataType("binary");
    public static final VCardDataType BOOLEAN = new VCardDataType("boolean");
    public static final VCardDataType CONTENT_ID = new VCardDataType("content-id");
    public static final VCardDataType DATE = new VCardDataType("date");
    public static final VCardDataType DATE_AND_OR_TIME = new VCardDataType("date-and-or-time");
    public static final VCardDataType DATE_TIME = new VCardDataType("date-time");
    public static final VCardDataType FLOAT = new VCardDataType("float");
    public static final VCardDataType INTEGER = new VCardDataType("integer");
    public static final VCardDataType LANGUAGE_TAG = new VCardDataType("language-tag");
    public static final VCardDataType TEXT = new VCardDataType(DraftDatabase.Draft.TEXT);
    public static final VCardDataType TIME = new VCardDataType("time");
    public static final VCardDataType TIMESTAMP = new VCardDataType("timestamp");
    public static final VCardDataType URI = new VCardDataType("uri");
    public static final VCardDataType URL = new VCardDataType("url");
    public static final VCardDataType UTC_OFFSET = new VCardDataType("utc-offset");
    private static final CaseClasses<VCardDataType, String> enums = new CaseClasses<VCardDataType, String>(VCardDataType.class) { // from class: ezvcard.VCardDataType.1
        public VCardDataType create(String str) {
            return new VCardDataType(str);
        }

        public boolean matches(VCardDataType vCardDataType, String str) {
            return vCardDataType.name.equalsIgnoreCase(str);
        }
    };
    private final String name;

    private VCardDataType(String str) {
        this.name = str;
    }

    public String getName() {
        return this.name;
    }

    public String toString() {
        return this.name;
    }

    public static VCardDataType find(String str) {
        return enums.find(str);
    }

    public static VCardDataType get(String str) {
        return enums.get(str);
    }

    public int hashCode() {
        return super.hashCode();
    }

    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
