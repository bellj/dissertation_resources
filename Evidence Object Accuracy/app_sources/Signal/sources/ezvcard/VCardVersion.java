package ezvcard;

/* loaded from: classes3.dex */
public enum VCardVersion {
    V2_1("2.1", null),
    V3_0("3.0", null),
    V4_0("4.0", "urn:ietf:params:xml:ns:vcard-4.0");
    
    private final String version;
    private final String xmlNamespace;

    VCardVersion(String str, String str2) {
        this.version = str;
        this.xmlNamespace = str2;
    }

    public String getVersion() {
        return this.version;
    }

    public String getXmlNamespace() {
        return this.xmlNamespace;
    }

    @Override // java.lang.Enum, java.lang.Object
    public String toString() {
        return this.version;
    }
}
