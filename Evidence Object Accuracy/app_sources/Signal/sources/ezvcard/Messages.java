package ezvcard;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/* loaded from: classes3.dex */
public enum Messages {
    INSTANCE;
    
    private final ResourceBundle messages = ResourceBundle.getBundle("ezvcard/messages");

    Messages() {
    }

    public String getParseMessage(int i, Object... objArr) {
        return getMessage("parse." + i, objArr);
    }

    public String getExceptionMessage(int i, Object... objArr) {
        String message = getMessage("exception." + i, objArr);
        if (message == null) {
            return null;
        }
        return getMessage("exception.0", Integer.valueOf(i), message);
    }

    public IllegalArgumentException getIllegalArgumentException(int i, Object... objArr) {
        String exceptionMessage = getExceptionMessage(i, objArr);
        if (exceptionMessage == null) {
            return null;
        }
        return new IllegalArgumentException(exceptionMessage);
    }

    public String getMessage(String str, Object... objArr) {
        try {
            return MessageFormat.format(this.messages.getString(str), objArr);
        } catch (MissingResourceException unused) {
            return null;
        }
    }
}
