package ezvcard;

import ezvcard.property.Address;
import ezvcard.property.Email;
import ezvcard.property.FormattedName;
import ezvcard.property.Label;
import ezvcard.property.Organization;
import ezvcard.property.StructuredName;
import ezvcard.property.Telephone;
import ezvcard.property.VCardProperty;
import ezvcard.util.ListMultimap;
import ezvcard.util.StringUtils;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* loaded from: classes3.dex */
public class VCard implements Iterable<VCardProperty> {
    private final ListMultimap<Class<? extends VCardProperty>, VCardProperty> properties;
    private VCardVersion version;

    public VCard() {
        this(VCardVersion.V3_0);
    }

    public VCard(VCardVersion vCardVersion) {
        this.properties = new ListMultimap<>();
        this.version = vCardVersion;
    }

    public VCardVersion getVersion() {
        return this.version;
    }

    public void setVersion(VCardVersion vCardVersion) {
        this.version = vCardVersion;
    }

    public FormattedName getFormattedName() {
        return (FormattedName) getProperty(FormattedName.class);
    }

    public StructuredName getStructuredName() {
        return (StructuredName) getProperty(StructuredName.class);
    }

    public List<Address> getAddresses() {
        return getProperties(Address.class);
    }

    public void addOrphanedLabel(Label label) {
        addProperty(label);
    }

    public List<Email> getEmails() {
        return getProperties(Email.class);
    }

    public List<Telephone> getTelephoneNumbers() {
        return getProperties(Telephone.class);
    }

    public Organization getOrganization() {
        return (Organization) getProperty(Organization.class);
    }

    @Override // java.lang.Iterable
    public Iterator<VCardProperty> iterator() {
        return this.properties.values().iterator();
    }

    public <T extends VCardProperty> T getProperty(Class<T> cls) {
        return cls.cast(this.properties.first(cls));
    }

    public <T extends VCardProperty> List<T> getProperties(Class<T> cls) {
        return new VCardPropertyList(cls);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: ezvcard.util.ListMultimap<java.lang.Class<? extends ezvcard.property.VCardProperty>, ezvcard.property.VCardProperty> */
    /* JADX WARN: Multi-variable type inference failed */
    public void addProperty(VCardProperty vCardProperty) {
        this.properties.put(vCardProperty.getClass(), vCardProperty);
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("version=");
        sb.append(this.version);
        for (VCardProperty vCardProperty : this.properties.values()) {
            sb.append(StringUtils.NEWLINE);
            sb.append(vCardProperty);
        }
        return sb.toString();
    }

    @Override // java.lang.Object
    public int hashCode() {
        VCardVersion vCardVersion = this.version;
        int hashCode = (vCardVersion == null ? 0 : vCardVersion.hashCode()) + 31;
        int i = 1;
        for (VCardProperty vCardProperty : this.properties.values()) {
            i += vCardProperty.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        VCard vCard = (VCard) obj;
        if (!(this.version == vCard.version && this.properties.size() == vCard.properties.size())) {
            return false;
        }
        Iterator<Map.Entry<Class<? extends VCardProperty>, List<VCardProperty>>> it = this.properties.iterator();
        while (it.hasNext()) {
            Map.Entry<Class<? extends VCardProperty>, List<VCardProperty>> next = it.next();
            List<VCardProperty> value = next.getValue();
            List<VCardProperty> list = vCard.properties.get(next.getKey());
            if (value.size() != list.size()) {
                return false;
            }
            ArrayList arrayList = new ArrayList(list);
            for (VCardProperty vCardProperty : value) {
                if (!arrayList.remove(vCardProperty)) {
                    return false;
                }
            }
        }
        return true;
    }

    /* loaded from: classes3.dex */
    public class VCardPropertyList<T extends VCardProperty> extends AbstractList<T> {
        protected final List<VCardProperty> properties;
        protected final Class<T> propertyClass;

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: ezvcard.VCard$VCardPropertyList<T extends ezvcard.property.VCardProperty> */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // java.util.AbstractList, java.util.List
        public /* bridge */ /* synthetic */ void add(int i, Object obj) {
            add(i, (int) ((VCardProperty) obj));
        }

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: ezvcard.VCard$VCardPropertyList<T extends ezvcard.property.VCardProperty> */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // java.util.AbstractList, java.util.List
        public /* bridge */ /* synthetic */ Object set(int i, Object obj) {
            return set(i, (int) ((VCardProperty) obj));
        }

        public VCardPropertyList(Class<T> cls) {
            VCard.this = r1;
            this.propertyClass = cls;
            this.properties = r1.properties.get(cls);
        }

        public void add(int i, T t) {
            this.properties.add(i, t);
        }

        @Override // java.util.AbstractList, java.util.List
        public T remove(int i) {
            return cast(this.properties.remove(i));
        }

        @Override // java.util.AbstractList, java.util.List
        public T get(int i) {
            return cast(this.properties.get(i));
        }

        public T set(int i, T t) {
            return cast(this.properties.set(i, t));
        }

        @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
        public int size() {
            return this.properties.size();
        }

        private T cast(VCardProperty vCardProperty) {
            return this.propertyClass.cast(vCardProperty);
        }
    }
}
