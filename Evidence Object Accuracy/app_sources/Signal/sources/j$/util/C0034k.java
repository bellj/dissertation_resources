package j$.util;

import java.util.NoSuchElementException;

/* renamed from: j$.util.k */
/* loaded from: classes2.dex */
public final class C0034k {
    private static final C0034k c = new C0034k();
    private final boolean a;
    private final int b;

    private C0034k() {
        this.a = false;
        this.b = 0;
    }

    private C0034k(int i) {
        this.a = true;
        this.b = i;
    }

    public static C0034k a() {
        return c;
    }

    public static C0034k d(int i) {
        return new C0034k(i);
    }

    public final int b() {
        if (this.a) {
            return this.b;
        }
        throw new NoSuchElementException("No value present");
    }

    public final boolean c() {
        return this.a;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C0034k)) {
            return false;
        }
        C0034k kVar = (C0034k) obj;
        boolean z = this.a;
        if (!z || !kVar.a) {
            if (z == kVar.a) {
                return true;
            }
        } else if (this.b == kVar.b) {
            return true;
        }
        return false;
    }

    public final int hashCode() {
        if (this.a) {
            return this.b;
        }
        return 0;
    }

    public final String toString() {
        return this.a ? String.format("OptionalInt[%s]", Integer.valueOf(this.b)) : "OptionalInt.empty";
    }
}
