package j$.util;

import j$.util.function.B;
import j$.util.function.Consumer;
import java.util.Comparator;

/* loaded from: classes2.dex */
final class N extends AbstractC0037n implements C {
    @Override // j$.util.C, j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.o(this, consumer);
    }

    @Override // j$.util.C
    public final void b(B b) {
        b.getClass();
    }

    @Override // j$.util.C
    public final boolean c(B b) {
        b.getClass();
        return false;
    }

    @Override // j$.util.C, j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.f(this, consumer);
    }

    @Override // j$.util.Spliterator
    public final Comparator getComparator() {
        throw new IllegalStateException();
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ long getExactSizeIfKnown() {
        return AbstractC0037n.h(this);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean hasCharacteristics(int i) {
        return AbstractC0037n.j(this, i);
    }
}
