package j$.util;

import j$.util.stream.AbstractC0115s0;
import j$.util.stream.IntStream;
import j$.util.stream.Stream;

/* loaded from: classes2.dex */
public class DesugarArrays {
    public static IntStream stream(int[] iArr) {
        return AbstractC0115s0.g0(T.k(iArr, 0, iArr.length));
    }

    public static <T> Stream<T> stream(T[] tArr) {
        return AbstractC0115s0.r0(T.m(tArr, 0, tArr.length), false);
    }
}
