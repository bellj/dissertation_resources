package j$.util;

import j$.util.function.AbstractC0024g;
import j$.util.function.Consumer;

/* loaded from: classes2.dex */
public interface y extends E {
    @Override // j$.util.Spliterator
    boolean a(Consumer consumer);

    @Override // j$.util.Spliterator
    void forEachRemaining(Consumer consumer);

    boolean k(AbstractC0024g gVar);

    void m(AbstractC0024g gVar);

    @Override // j$.util.E, j$.util.Spliterator
    y trySplit();
}
