package j$.util.function;

/* loaded from: classes2.dex */
public interface IntPredicate {

    /* renamed from: j$.util.function.IntPredicate$-CC */
    /* loaded from: classes2.dex */
    public final /* synthetic */ class CC {
        public static IntPredicate $default$and(IntPredicate intPredicate, IntPredicate intPredicate2) {
            intPredicate2.getClass();
            return new t(intPredicate, intPredicate2, 1);
        }

        public static IntPredicate $default$negate(IntPredicate intPredicate) {
            return new s(intPredicate);
        }

        public static IntPredicate $default$or(IntPredicate intPredicate, IntPredicate intPredicate2) {
            intPredicate2.getClass();
            return new t(intPredicate, intPredicate2, 0);
        }
    }

    IntPredicate and(IntPredicate intPredicate);

    IntPredicate negate();

    IntPredicate or(IntPredicate intPredicate);

    boolean test(int i);
}
