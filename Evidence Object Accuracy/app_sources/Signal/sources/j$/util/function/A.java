package j$.util.function;

import java.util.function.LongConsumer;

/* loaded from: classes2.dex */
public final /* synthetic */ class A implements B {
    final /* synthetic */ LongConsumer a;

    private /* synthetic */ A(LongConsumer longConsumer) {
        this.a = longConsumer;
    }

    public static /* synthetic */ A a(LongConsumer longConsumer) {
        if (longConsumer == null) {
            return null;
        }
        return new A(longConsumer);
    }

    @Override // j$.util.function.B
    public final /* synthetic */ void accept(long j) {
        this.a.accept(j);
    }
}
