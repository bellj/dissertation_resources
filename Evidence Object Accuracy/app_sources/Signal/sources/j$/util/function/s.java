package j$.util.function;

import j$.util.function.IntPredicate;
import j$.util.function.Predicate;

/* loaded from: classes2.dex */
public final /* synthetic */ class s implements IntPredicate, Predicate {
    public final /* synthetic */ Object a;

    public /* synthetic */ s(Object obj) {
        this.a = obj;
    }

    @Override // j$.util.function.IntPredicate
    public final /* synthetic */ IntPredicate and(IntPredicate intPredicate) {
        return IntPredicate.CC.$default$and(this, intPredicate);
    }

    @Override // j$.util.function.Predicate
    public final /* synthetic */ Predicate and(Predicate predicate) {
        return Predicate.CC.$default$and(this, predicate);
    }

    @Override // j$.util.function.IntPredicate
    public final /* synthetic */ IntPredicate or(IntPredicate intPredicate) {
        return IntPredicate.CC.$default$or(this, intPredicate);
    }

    @Override // j$.util.function.Predicate
    public final /* synthetic */ Predicate or(Predicate predicate) {
        return Predicate.CC.$default$or(this, predicate);
    }

    @Override // j$.util.function.IntPredicate
    public final boolean test(int i) {
        return !((IntPredicate) this.a).test(i);
    }

    @Override // j$.util.function.Predicate
    public final boolean test(Object obj) {
        return !((Predicate) this.a).test(obj);
    }
}
