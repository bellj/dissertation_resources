package j$.util.function;

import java.util.function.IntConsumer;

/* loaded from: classes2.dex */
public final /* synthetic */ class q implements r {
    final /* synthetic */ IntConsumer a;

    private /* synthetic */ q(IntConsumer intConsumer) {
        this.a = intConsumer;
    }

    public static /* synthetic */ q a(IntConsumer intConsumer) {
        if (intConsumer == null) {
            return null;
        }
        return new q(intConsumer);
    }

    @Override // j$.util.function.r
    public final /* synthetic */ void accept(int i) {
        this.a.accept(i);
    }
}
