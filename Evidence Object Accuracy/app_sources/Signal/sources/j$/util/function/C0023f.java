package j$.util.function;

import java.util.function.DoubleConsumer;

/* renamed from: j$.util.function.f */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0023f implements AbstractC0024g {
    final /* synthetic */ DoubleConsumer a;

    private /* synthetic */ C0023f(DoubleConsumer doubleConsumer) {
        this.a = doubleConsumer;
    }

    public static /* synthetic */ C0023f a(DoubleConsumer doubleConsumer) {
        if (doubleConsumer == null) {
            return null;
        }
        return new C0023f(doubleConsumer);
    }

    @Override // j$.util.function.AbstractC0024g
    public final /* synthetic */ void accept(double d) {
        this.a.accept(d);
    }
}
