package j$.util.function;

import j$.util.function.BiConsumer;
import java.util.function.BiConsumer;

/* renamed from: j$.util.function.a */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0018a implements BiConsumer {
    final /* synthetic */ BiConsumer a;

    private /* synthetic */ C0018a(BiConsumer biConsumer) {
        this.a = biConsumer;
    }

    public static /* synthetic */ BiConsumer a(BiConsumer biConsumer) {
        if (biConsumer == null) {
            return null;
        }
        return biConsumer instanceof BiConsumer.VivifiedWrapper ? ((BiConsumer.VivifiedWrapper) biConsumer).a : new C0018a(biConsumer);
    }

    @Override // java.util.function.BiConsumer
    public final /* synthetic */ void accept(Object obj, Object obj2) {
        this.a.accept(obj, obj2);
    }

    @Override // java.util.function.BiConsumer
    public final /* synthetic */ java.util.function.BiConsumer andThen(java.util.function.BiConsumer biConsumer) {
        return a(this.a.andThen(BiConsumer.VivifiedWrapper.convert(biConsumer)));
    }
}
