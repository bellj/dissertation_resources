package j$.util.function;

import j$.util.function.Function;
import java.util.function.Function;

/* loaded from: classes2.dex */
public final /* synthetic */ class o implements Function {
    final /* synthetic */ Function a;

    private /* synthetic */ o(Function function) {
        this.a = function;
    }

    public static /* synthetic */ Function a(Function function) {
        if (function == null) {
            return null;
        }
        return function instanceof Function.VivifiedWrapper ? ((Function.VivifiedWrapper) function).a : new o(function);
    }

    @Override // java.util.function.Function
    public final /* synthetic */ java.util.function.Function andThen(java.util.function.Function function) {
        return a(this.a.andThen(Function.VivifiedWrapper.convert(function)));
    }

    @Override // java.util.function.Function
    public final /* synthetic */ Object apply(Object obj) {
        return this.a.apply(obj);
    }

    @Override // java.util.function.Function
    public final /* synthetic */ java.util.function.Function compose(java.util.function.Function function) {
        return a(this.a.compose(Function.VivifiedWrapper.convert(function)));
    }
}
