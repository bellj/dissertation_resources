package j$.util.function;

/* loaded from: classes2.dex */
public interface BinaryOperator<T> extends BiFunction<T, T, T> {
    @Override // j$.util.function.BiFunction
    /* synthetic */ BiFunction andThen(Function function);
}
