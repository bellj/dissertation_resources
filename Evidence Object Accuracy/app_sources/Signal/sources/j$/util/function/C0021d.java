package j$.util.function;

import j$.util.function.Consumer;
import java.util.function.Consumer;

/* renamed from: j$.util.function.d */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0021d implements Consumer {
    final /* synthetic */ Consumer a;

    private /* synthetic */ C0021d(Consumer consumer) {
        this.a = consumer;
    }

    public static /* synthetic */ Consumer a(Consumer consumer) {
        if (consumer == null) {
            return null;
        }
        return consumer instanceof Consumer.VivifiedWrapper ? ((Consumer.VivifiedWrapper) consumer).a : new C0021d(consumer);
    }

    @Override // java.util.function.Consumer
    public final /* synthetic */ void accept(Object obj) {
        this.a.accept(obj);
    }

    @Override // java.util.function.Consumer
    public final /* synthetic */ java.util.function.Consumer andThen(java.util.function.Consumer consumer) {
        return a(this.a.andThen(Consumer.VivifiedWrapper.convert(consumer)));
    }
}
