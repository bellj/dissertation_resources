package j$.util.function;

import j$.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleFunction;
import java.util.function.DoublePredicate;
import java.util.function.DoubleToIntFunction;
import java.util.function.DoubleToLongFunction;
import java.util.function.DoubleUnaryOperator;
import java.util.function.IntBinaryOperator;
import java.util.function.IntFunction;
import java.util.function.IntToDoubleFunction;
import java.util.function.IntToLongFunction;
import java.util.function.IntUnaryOperator;
import java.util.function.LongBinaryOperator;
import java.util.function.LongFunction;
import java.util.function.LongPredicate;
import java.util.function.LongToDoubleFunction;
import java.util.function.LongToIntFunction;
import java.util.function.LongUnaryOperator;
import java.util.function.ObjDoubleConsumer;
import java.util.function.ObjIntConsumer;
import java.util.function.ObjLongConsumer;
import java.util.function.Supplier;

/* renamed from: j$.util.function.c */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0020c implements BinaryOperator, AbstractC0022e, AbstractC0025h, AbstractC0026i, AbstractC0027j, AbstractC0028k, AbstractC0029l, p, IntFunction, w, x, y, z, C, D, E, F, G, H, I, J, Supplier {
    final /* synthetic */ Object a;

    public /* synthetic */ C0020c(Object obj) {
        this.a = obj;
    }

    @Override // j$.util.function.I
    public final /* synthetic */ void a(int i, Object obj) {
        ((ObjIntConsumer) this.a).accept(obj, i);
    }

    @Override // j$.util.function.H
    public final /* synthetic */ void accept(Object obj, double d) {
        ((ObjDoubleConsumer) this.a).accept(obj, d);
    }

    @Override // j$.util.function.J
    public final /* synthetic */ void accept(Object obj, long j) {
        ((ObjLongConsumer) this.a).accept(obj, j);
    }

    @Override // j$.util.function.BinaryOperator, j$.util.function.BiFunction
    public final /* synthetic */ BiFunction andThen(Function function) {
        return BiFunction.VivifiedWrapper.convert(((BinaryOperator) this.a).andThen(o.a(function)));
    }

    @Override // j$.util.function.AbstractC0025h
    public final /* synthetic */ Object apply(double d) {
        return ((DoubleFunction) this.a).apply(d);
    }

    @Override // j$.util.function.IntFunction
    public final /* synthetic */ Object apply(int i) {
        return ((IntFunction) this.a).apply(i);
    }

    @Override // j$.util.function.C
    public final /* synthetic */ Object apply(long j) {
        return ((LongFunction) this.a).apply(j);
    }

    @Override // j$.util.function.BiFunction
    public final /* synthetic */ Object apply(Object obj, Object obj2) {
        return ((BinaryOperator) this.a).apply(obj, obj2);
    }

    public final /* synthetic */ double applyAsDouble(double d, double d2) {
        return ((DoubleBinaryOperator) this.a).applyAsDouble(d, d2);
    }

    public final /* synthetic */ int applyAsInt(int i, int i2) {
        return ((IntBinaryOperator) this.a).applyAsInt(i, i2);
    }

    public final /* synthetic */ long applyAsLong(double d) {
        return ((DoubleToLongFunction) this.a).applyAsLong(d);
    }

    public final /* synthetic */ long applyAsLong(int i) {
        return ((IntToLongFunction) this.a).applyAsLong(i);
    }

    public final /* synthetic */ long applyAsLong(long j) {
        return ((LongUnaryOperator) this.a).applyAsLong(j);
    }

    public final /* synthetic */ long applyAsLong(long j, long j2) {
        return ((LongBinaryOperator) this.a).applyAsLong(j, j2);
    }

    public final /* synthetic */ double b(double d) {
        return ((DoubleUnaryOperator) this.a).applyAsDouble(d);
    }

    public final /* synthetic */ double c(int i) {
        return ((IntToDoubleFunction) this.a).applyAsDouble(i);
    }

    public final /* synthetic */ double d(long j) {
        return ((LongToDoubleFunction) this.a).applyAsDouble(j);
    }

    public final /* synthetic */ int e(double d) {
        return ((DoubleToIntFunction) this.a).applyAsInt(d);
    }

    public final /* synthetic */ int f(int i) {
        return ((IntUnaryOperator) this.a).applyAsInt(i);
    }

    public final /* synthetic */ int g(long j) {
        return ((LongToIntFunction) this.a).applyAsInt(j);
    }

    public final /* synthetic */ Object get() {
        return ((Supplier) this.a).get();
    }

    public final /* synthetic */ boolean i(double d) {
        return ((DoublePredicate) this.a).test(d);
    }

    public final /* synthetic */ boolean j(long j) {
        return ((LongPredicate) this.a).test(j);
    }
}
