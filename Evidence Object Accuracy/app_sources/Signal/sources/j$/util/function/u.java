package j$.util.function;

import java.util.function.IntPredicate;

/* loaded from: classes2.dex */
public final /* synthetic */ class u implements IntPredicate {
    final /* synthetic */ IntPredicate a;

    private /* synthetic */ u(IntPredicate intPredicate) {
        this.a = intPredicate;
    }

    public static /* synthetic */ IntPredicate a(IntPredicate intPredicate) {
        if (intPredicate == null) {
            return null;
        }
        return intPredicate instanceof v ? ((v) intPredicate).a : new u(intPredicate);
    }

    @Override // j$.util.function.IntPredicate
    public final /* synthetic */ IntPredicate and(IntPredicate intPredicate) {
        return a(this.a.and(v.a(intPredicate)));
    }

    @Override // j$.util.function.IntPredicate
    public final /* synthetic */ IntPredicate negate() {
        return a(this.a.negate());
    }

    @Override // j$.util.function.IntPredicate
    public final /* synthetic */ IntPredicate or(IntPredicate intPredicate) {
        return a(this.a.or(v.a(intPredicate)));
    }

    @Override // j$.util.function.IntPredicate
    public final /* synthetic */ boolean test(int i) {
        return this.a.test(i);
    }
}
