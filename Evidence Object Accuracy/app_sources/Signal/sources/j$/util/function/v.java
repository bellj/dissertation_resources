package j$.util.function;

import java.util.function.IntPredicate;

/* loaded from: classes2.dex */
public final /* synthetic */ class v implements IntPredicate {
    final /* synthetic */ IntPredicate a;

    private /* synthetic */ v(IntPredicate intPredicate) {
        this.a = intPredicate;
    }

    public static /* synthetic */ IntPredicate a(IntPredicate intPredicate) {
        if (intPredicate == null) {
            return null;
        }
        return intPredicate instanceof u ? ((u) intPredicate).a : new v(intPredicate);
    }

    @Override // java.util.function.IntPredicate
    public final /* synthetic */ IntPredicate and(IntPredicate intPredicate) {
        return a(this.a.and(u.a(intPredicate)));
    }

    @Override // java.util.function.IntPredicate
    public final /* synthetic */ IntPredicate negate() {
        return a(this.a.negate());
    }

    @Override // java.util.function.IntPredicate
    public final /* synthetic */ IntPredicate or(IntPredicate intPredicate) {
        return a(this.a.or(u.a(intPredicate)));
    }

    @Override // java.util.function.IntPredicate
    public final /* synthetic */ boolean test(int i) {
        return this.a.test(i);
    }
}
