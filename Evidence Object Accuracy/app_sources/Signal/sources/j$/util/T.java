package j$.util;

import java.util.Iterator;

/* loaded from: classes2.dex */
public abstract class T {
    private static final Spliterator a = new O();
    private static final A b = new M();
    private static final C c = new N();
    private static final y d = new L();

    private static void a(int i, int i2, int i3) {
        if (i2 > i3) {
            throw new ArrayIndexOutOfBoundsException("origin(" + i2 + ") > fence(" + i3 + ")");
        } else if (i2 < 0) {
            throw new ArrayIndexOutOfBoundsException(i2);
        } else if (i3 > i) {
            throw new ArrayIndexOutOfBoundsException(i3);
        }
    }

    public static y b() {
        return d;
    }

    public static A c() {
        return b;
    }

    public static C d() {
        return c;
    }

    public static Spliterator e() {
        return a;
    }

    public static AbstractC0039p f(y yVar) {
        yVar.getClass();
        return new I(yVar);
    }

    public static AbstractC0041s g(A a2) {
        a2.getClass();
        return new G(a2);
    }

    public static AbstractC0148v h(C c2) {
        c2.getClass();
        return new H(c2);
    }

    public static Iterator i(Spliterator spliterator) {
        spliterator.getClass();
        return new F(spliterator);
    }

    public static y j(double[] dArr, int i, int i2) {
        dArr.getClass();
        a(dArr.length, i, i2);
        return new K(dArr, i, i2, 1040);
    }

    public static A k(int[] iArr, int i, int i2) {
        iArr.getClass();
        a(iArr.length, i, i2);
        return new P(iArr, i, i2, 1040);
    }

    public static C l(long[] jArr, int i, int i2) {
        jArr.getClass();
        a(jArr.length, i, i2);
        return new S(jArr, i, i2, 1040);
    }

    public static Spliterator m(Object[] objArr, int i, int i2) {
        objArr.getClass();
        a(objArr.length, i, i2);
        return new J(objArr, i, i2, 1040);
    }

    public static Spliterator n(Iterator it) {
        it.getClass();
        return new Q(it);
    }
}
