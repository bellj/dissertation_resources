package j$.util;

import j$.util.function.AbstractC0024g;
import j$.util.function.Consumer;
import java.util.Comparator;

/* loaded from: classes2.dex */
public final class K implements y {
    private final double[] a;
    private int b;
    private final int c;
    private final int d;

    public K(double[] dArr, int i, int i2, int i3) {
        this.a = dArr;
        this.b = i;
        this.c = i2;
        this.d = i3 | 64 | 16384;
    }

    @Override // j$.util.y, j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.l(this, consumer);
    }

    @Override // j$.util.Spliterator
    public final int characteristics() {
        return this.d;
    }

    @Override // j$.util.Spliterator
    public final long estimateSize() {
        return (long) (this.c - this.b);
    }

    @Override // j$.util.y, j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.d(this, consumer);
    }

    @Override // j$.util.Spliterator
    public final Comparator getComparator() {
        if (AbstractC0037n.j(this, 4)) {
            return null;
        }
        throw new IllegalStateException();
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ long getExactSizeIfKnown() {
        return AbstractC0037n.h(this);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean hasCharacteristics(int i) {
        return AbstractC0037n.j(this, i);
    }

    @Override // j$.util.y
    /* renamed from: k */
    public final boolean tryAdvance(AbstractC0024g gVar) {
        gVar.getClass();
        int i = this.b;
        if (i < 0 || i >= this.c) {
            return false;
        }
        double[] dArr = this.a;
        this.b = i + 1;
        gVar.accept(dArr[i]);
        return true;
    }

    @Override // j$.util.y
    /* renamed from: m */
    public final void forEachRemaining(AbstractC0024g gVar) {
        int i;
        gVar.getClass();
        double[] dArr = this.a;
        int length = dArr.length;
        int i2 = this.c;
        if (length >= i2 && (i = this.b) >= 0) {
            this.b = i2;
            if (i < i2) {
                do {
                    gVar.accept(dArr[i]);
                    i++;
                } while (i < i2);
            }
        }
    }

    @Override // j$.util.y, j$.util.E, j$.util.Spliterator
    public final y trySplit() {
        int i = this.b;
        int i2 = (this.c + i) >>> 1;
        if (i >= i2) {
            return null;
        }
        double[] dArr = this.a;
        this.b = i2;
        return new K(dArr, i, i2, this.d);
    }
}
