package j$.util;

import j$.util.function.Consumer;
import j$.util.function.r;
import java.util.NoSuchElementException;

/* loaded from: classes2.dex */
public final class G implements AbstractC0041s, r, Iterator {
    boolean a = false;
    int b;
    final /* synthetic */ A c;

    public G(A a) {
        this.c = a;
    }

    public final void a(r rVar) {
        rVar.getClass();
        while (hasNext()) {
            rVar.accept(nextInt());
        }
    }

    @Override // j$.util.function.r
    public final void accept(int i) {
        this.a = true;
        this.b = i;
    }

    @Override // j$.util.Iterator
    public final void forEachRemaining(Consumer consumer) {
        if (consumer instanceof r) {
            a((r) consumer);
            return;
        }
        consumer.getClass();
        if (!V.a) {
            a(new C0040q(consumer));
        } else {
            V.a(G.class, "{0} calling PrimitiveIterator.OfInt.forEachRemainingInt(action::accept)");
            throw null;
        }
    }

    @Override // java.util.Iterator, j$.util.Iterator
    public final boolean hasNext() {
        if (!this.a) {
            this.c.g(this);
        }
        return this.a;
    }

    @Override // java.util.Iterator, j$.util.Iterator
    public final Object next() {
        if (!V.a) {
            return Integer.valueOf(nextInt());
        }
        V.a(G.class, "{0} calling PrimitiveIterator.OfInt.nextInt()");
        throw null;
    }

    public final int nextInt() {
        if (this.a || hasNext()) {
            this.a = false;
            return this.b;
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator, j$.util.Iterator
    public final void remove() {
        throw new UnsupportedOperationException("remove");
    }
}
