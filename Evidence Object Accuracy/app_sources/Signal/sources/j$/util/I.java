package j$.util;

import j$.util.function.AbstractC0024g;
import j$.util.function.Consumer;
import java.util.NoSuchElementException;

/* loaded from: classes2.dex */
public final class I implements AbstractC0039p, AbstractC0024g, Iterator {
    boolean a = false;
    double b;
    final /* synthetic */ y c;

    public I(y yVar) {
        this.c = yVar;
    }

    public final void a(AbstractC0024g gVar) {
        gVar.getClass();
        while (hasNext()) {
            gVar.accept(nextDouble());
        }
    }

    @Override // j$.util.function.AbstractC0024g
    public final void accept(double d) {
        this.a = true;
        this.b = d;
    }

    @Override // j$.util.Iterator
    public final void forEachRemaining(Consumer consumer) {
        if (consumer instanceof AbstractC0024g) {
            a((AbstractC0024g) consumer);
            return;
        }
        consumer.getClass();
        if (!V.a) {
            a(new C0036m(consumer));
        } else {
            V.a(I.class, "{0} calling PrimitiveIterator.OfDouble.forEachRemainingDouble(action::accept)");
            throw null;
        }
    }

    @Override // java.util.Iterator, j$.util.Iterator
    public final boolean hasNext() {
        if (!this.a) {
            this.c.k(this);
        }
        return this.a;
    }

    @Override // java.util.Iterator, j$.util.Iterator
    public final Object next() {
        if (!V.a) {
            return Double.valueOf(nextDouble());
        }
        V.a(I.class, "{0} calling PrimitiveIterator.OfDouble.nextLong()");
        throw null;
    }

    public final double nextDouble() {
        if (this.a || hasNext()) {
            this.a = false;
            return this.b;
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator, j$.util.Iterator
    public final void remove() {
        throw new UnsupportedOperationException("remove");
    }
}
