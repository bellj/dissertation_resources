package j$.util;

import java.util.Collection;
import java.util.Comparator;
import java.util.SortedSet;

/* loaded from: classes2.dex */
public final class w extends Q {
    final /* synthetic */ SortedSet f;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public w(SortedSet sortedSet, Collection collection) {
        super(21, collection);
        this.f = sortedSet;
    }

    @Override // j$.util.Q, j$.util.Spliterator
    public final Comparator getComparator() {
        return this.f.comparator();
    }
}
