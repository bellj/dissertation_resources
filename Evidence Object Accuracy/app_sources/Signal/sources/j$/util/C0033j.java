package j$.util;

import java.util.NoSuchElementException;

/* renamed from: j$.util.j */
/* loaded from: classes2.dex */
public final class C0033j {
    private static final C0033j c = new C0033j();
    private final boolean a;
    private final double b;

    private C0033j() {
        this.a = false;
        this.b = Double.NaN;
    }

    private C0033j(double d) {
        this.a = true;
        this.b = d;
    }

    public static C0033j a() {
        return c;
    }

    public static C0033j d(double d) {
        return new C0033j(d);
    }

    public final double b() {
        if (this.a) {
            return this.b;
        }
        throw new NoSuchElementException("No value present");
    }

    public final boolean c() {
        return this.a;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C0033j)) {
            return false;
        }
        C0033j jVar = (C0033j) obj;
        boolean z = this.a;
        if (!z || !jVar.a) {
            if (z == jVar.a) {
                return true;
            }
        } else if (Double.compare(this.b, jVar.b) == 0) {
            return true;
        }
        return false;
    }

    public final int hashCode() {
        if (!this.a) {
            return 0;
        }
        long doubleToLongBits = Double.doubleToLongBits(this.b);
        return (int) (doubleToLongBits ^ (doubleToLongBits >>> 32));
    }

    public final String toString() {
        return this.a ? String.format("OptionalDouble[%s]", Double.valueOf(this.b)) : "OptionalDouble.empty";
    }
}
