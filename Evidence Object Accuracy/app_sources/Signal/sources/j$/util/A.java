package j$.util;

import j$.util.function.Consumer;
import j$.util.function.r;

/* loaded from: classes2.dex */
public interface A extends E {
    @Override // j$.util.Spliterator
    boolean a(Consumer consumer);

    @Override // j$.util.Spliterator
    void forEachRemaining(Consumer consumer);

    boolean g(r rVar);

    void i(r rVar);

    @Override // j$.util.E, j$.util.Spliterator
    A trySplit();
}
