package j$.util;

import j$.util.List;
import j$.util.function.Consumer;
import j$.util.stream.Stream;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

/* renamed from: j$.util.Collection$-EL */
/* loaded from: classes2.dex */
public final /* synthetic */ class Collection$EL {
    public static void a(Collection collection, Consumer consumer) {
        if (collection instanceof AbstractC0010a) {
            ((AbstractC0010a) collection).forEach(consumer);
            return;
        }
        consumer.getClass();
        for (Object obj : collection) {
            consumer.accept(obj);
        }
    }

    public static Spliterator b(Collection collection) {
        if (collection instanceof AbstractC0010a) {
            return ((AbstractC0010a) collection).mo150spliterator();
        }
        if (collection instanceof LinkedHashSet) {
            LinkedHashSet linkedHashSet = (LinkedHashSet) collection;
            linkedHashSet.getClass();
            return new Q(17, linkedHashSet);
        } else if (collection instanceof SortedSet) {
            SortedSet sortedSet = (SortedSet) collection;
            return new w(sortedSet, sortedSet);
        } else if (collection instanceof Set) {
            Set set = (Set) collection;
            set.getClass();
            return new Q(1, set);
        } else if (collection instanceof List) {
            return List.CC.$default$spliterator((java.util.List) collection);
        } else {
            collection.getClass();
            return new Q(0, collection);
        }
    }

    public static /* synthetic */ Stream stream(Collection collection) {
        return collection instanceof AbstractC0010a ? ((AbstractC0010a) collection).stream() : Collection$CC.$default$stream(collection);
    }
}
