package j$.util;

/* loaded from: classes2.dex */
public interface E extends Spliterator {
    void forEachRemaining(Object obj);

    boolean tryAdvance(Object obj);

    @Override // j$.util.Spliterator
    E trySplit();
}
