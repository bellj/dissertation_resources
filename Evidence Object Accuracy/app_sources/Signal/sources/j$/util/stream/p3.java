package j$.util.stream;

import j$.util.AbstractC0037n;
import j$.util.C;
import j$.util.Spliterator;
import j$.util.function.B;
import j$.util.function.Consumer;

/* loaded from: classes2.dex */
public final class p3 extends q3 implements C, B {
    long e;

    public p3(C c, long j, long j2) {
        super(c, j, j2);
    }

    p3(C c, p3 p3Var) {
        super(c, p3Var);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.o(this, consumer);
    }

    @Override // j$.util.function.B
    public final void accept(long j) {
        this.e = j;
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.f(this, consumer);
    }

    @Override // j$.util.stream.s3
    protected final Spliterator o(Spliterator spliterator) {
        return new p3((C) spliterator, this);
    }

    @Override // j$.util.stream.q3
    protected final void q(Object obj) {
        ((B) obj).accept(this.e);
    }

    @Override // j$.util.stream.q3
    protected final U2 r() {
        return new T2();
    }
}
