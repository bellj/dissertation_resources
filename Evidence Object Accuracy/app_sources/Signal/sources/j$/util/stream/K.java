package j$.util.stream;

import j$.util.function.AbstractC0024g;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public final class K extends O implements Z1 {
    final AbstractC0024g b;

    public K(AbstractC0024g gVar, boolean z) {
        super(z);
        this.b = gVar;
    }

    @Override // j$.util.stream.O, j$.util.stream.AbstractC0054c2
    public final void accept(double d) {
        this.b.accept(d);
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        l((Double) obj);
    }

    @Override // j$.util.stream.Z1
    public final /* synthetic */ void l(Double d) {
        AbstractC0115s0.E(this, d);
    }
}
