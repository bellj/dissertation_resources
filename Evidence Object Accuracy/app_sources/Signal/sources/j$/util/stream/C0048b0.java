package j$.util.stream;

import j$.util.function.B;

/* renamed from: j$.util.stream.b0 */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0048b0 implements B {
    public final /* synthetic */ int a;
    public final /* synthetic */ AbstractC0054c2 b;

    public /* synthetic */ C0048b0(int i, AbstractC0054c2 c2Var) {
        this.a = i;
        this.b = c2Var;
    }

    @Override // j$.util.function.B
    public final void accept(long j) {
        switch (this.a) {
            case 0:
                this.b.accept(j);
                return;
            default:
                ((C0056d0) this.b).a.accept(j);
                return;
        }
    }
}
