package j$.util.stream;

/* renamed from: j$.util.stream.h2 */
/* loaded from: classes2.dex */
final class C0074h2 extends X1 {
    long b;
    long c;
    final /* synthetic */ C0078i2 d;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0074h2(C0078i2 i2Var, AbstractC0054c2 c2Var) {
        super(c2Var);
        this.d = i2Var;
        this.b = i2Var.m;
        long j = i2Var.n;
        this.c = j < 0 ? Long.MAX_VALUE : j;
    }

    @Override // j$.util.stream.AbstractC0050b2, j$.util.stream.AbstractC0054c2
    public final void accept(long j) {
        long j2 = this.b;
        if (j2 == 0) {
            long j3 = this.c;
            if (j3 > 0) {
                this.c = j3 - 1;
                this.a.accept(j);
                return;
            }
            return;
        }
        this.b = j2 - 1;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        this.a.d(AbstractC0115s0.Z(j, this.d.m, this.c));
    }

    @Override // j$.util.stream.X1, j$.util.stream.AbstractC0054c2
    public final boolean f() {
        return this.c == 0 || this.a.f();
    }
}
