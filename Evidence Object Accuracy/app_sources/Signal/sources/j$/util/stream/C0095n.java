package j$.util.stream;

import j$.util.Spliterator;
import j$.util.concurrent.ConcurrentHashMap;
import j$.util.function.IntFunction;
import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: j$.util.stream.n */
/* loaded from: classes2.dex */
public final class C0095n extends S1 {
    public C0095n(AbstractC0051c cVar, int i) {
        super(cVar, i);
    }

    static F0 L0(AbstractC0051c cVar, Spliterator spliterator) {
        C0052c0 c0Var = new C0052c0(24);
        return new F0((Collection) new C0116s1(1, new C0052c0(26), new C0052c0(25), c0Var, 3).a(cVar, spliterator));
    }

    @Override // j$.util.stream.AbstractC0051c
    final B0 D0(Spliterator spliterator, IntFunction intFunction, AbstractC0051c cVar) {
        if (P2.DISTINCT.e(cVar.f0())) {
            return cVar.u0(spliterator, false, intFunction);
        }
        if (P2.ORDERED.e(cVar.f0())) {
            return L0(cVar, spliterator);
        }
        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        ConcurrentHashMap concurrentHashMap = new ConcurrentHashMap();
        new N(new C0083k(0, atomicBoolean, concurrentHashMap), false).a(cVar, spliterator);
        Collection keySet = concurrentHashMap.keySet();
        if (atomicBoolean.get()) {
            HashSet hashSet = new HashSet(keySet);
            hashSet.add(null);
            keySet = hashSet;
        }
        return new F0(keySet);
    }

    @Override // j$.util.stream.AbstractC0051c
    final Spliterator E0(AbstractC0051c cVar, Spliterator spliterator) {
        return P2.DISTINCT.e(cVar.f0()) ? cVar.K0(spliterator) : P2.ORDERED.e(cVar.f0()) ? L0(cVar, spliterator).mo148spliterator() : new X2(cVar.K0(spliterator));
    }

    @Override // j$.util.stream.AbstractC0051c
    public final AbstractC0054c2 G0(int i, AbstractC0054c2 c2Var) {
        c2Var.getClass();
        return P2.DISTINCT.e(i) ? c2Var : P2.SORTED.e(i) ? new C0087l(c2Var) : new C0091m(this, c2Var);
    }
}
