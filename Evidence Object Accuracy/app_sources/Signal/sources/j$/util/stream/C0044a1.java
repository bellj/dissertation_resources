package j$.util.stream;

import j$.util.A;
import j$.util.AbstractC0037n;
import j$.util.function.Consumer;

/* renamed from: j$.util.stream.a1 */
/* loaded from: classes2.dex */
final class C0044a1 extends AbstractC0053c1 implements A {
    public C0044a1(AbstractC0139y0 y0Var) {
        super(y0Var);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.n(this, consumer);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.e(this, consumer);
    }
}
