package j$.util.stream;

import java.util.Arrays;

/* loaded from: classes2.dex */
final class B2 extends AbstractC0106p2 {
    private long[] c;
    private int d;

    public B2(AbstractC0054c2 c2Var) {
        super(c2Var);
    }

    @Override // j$.util.stream.AbstractC0050b2, j$.util.stream.AbstractC0054c2
    public final void accept(long j) {
        long[] jArr = this.c;
        int i = this.d;
        this.d = i + 1;
        jArr[i] = j;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        if (j < 2147483639) {
            this.c = new long[(int) j];
            return;
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    @Override // j$.util.stream.X1, j$.util.stream.AbstractC0054c2
    public final void end() {
        int i = 0;
        Arrays.sort(this.c, 0, this.d);
        this.a.d((long) this.d);
        if (!this.b) {
            while (i < this.d) {
                this.a.accept(this.c[i]);
                i++;
            }
        } else {
            while (i < this.d && !this.a.f()) {
                this.a.accept(this.c[i]);
                i++;
            }
        }
        this.a.end();
        this.c = null;
    }
}
