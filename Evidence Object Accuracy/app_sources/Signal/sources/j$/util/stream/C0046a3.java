package j$.util.stream;

import j$.util.function.Consumer;
import j$.util.function.r;

/* renamed from: j$.util.stream.a3 */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0046a3 implements AbstractC0045a2 {
    public final /* synthetic */ int a;
    public final /* synthetic */ r b;

    public /* synthetic */ C0046a3(r rVar, int i) {
        this.a = i;
        this.b = rVar;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(double d) {
        switch (this.a) {
            case 0:
                AbstractC0115s0.D();
                throw null;
            default:
                AbstractC0115s0.D();
                throw null;
        }
    }

    @Override // j$.util.stream.AbstractC0045a2, j$.util.stream.AbstractC0054c2
    public final void accept(int i) {
        switch (this.a) {
            case 0:
                ((H2) this.b).accept(i);
                return;
            default:
                this.b.accept(i);
                return;
        }
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(long j) {
        switch (this.a) {
            case 0:
                AbstractC0115s0.L();
                throw null;
            default:
                AbstractC0115s0.L();
                throw null;
        }
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        switch (this.a) {
            case 0:
                e((Integer) obj);
                return;
            default:
                e((Integer) obj);
                return;
        }
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        switch (this.a) {
            case 0:
                return Consumer.CC.$default$andThen(this, consumer);
            default:
                return Consumer.CC.$default$andThen(this, consumer);
        }
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void d(long j) {
    }

    @Override // j$.util.stream.AbstractC0045a2
    public final /* synthetic */ void e(Integer num) {
        switch (this.a) {
            case 0:
                AbstractC0115s0.G(this, num);
                return;
            default:
                AbstractC0115s0.G(this, num);
                return;
        }
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void end() {
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ boolean f() {
        return false;
    }
}
