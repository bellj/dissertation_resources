package j$.util.stream;

import j$.util.A;
import j$.util.AbstractC0037n;
import j$.util.Spliterator;
import j$.util.function.Consumer;
import j$.util.function.r;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public final class b3 extends Q2 implements A {
    b3(AbstractC0115s0 s0Var, Spliterator spliterator, boolean z) {
        super(s0Var, spliterator, z);
    }

    public b3(AbstractC0115s0 s0Var, C0042a aVar, boolean z) {
        super(s0Var, aVar, z);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.n(this, consumer);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.e(this, consumer);
    }

    @Override // j$.util.A
    /* renamed from: g */
    public final boolean tryAdvance(r rVar) {
        rVar.getClass();
        boolean d = d();
        if (d) {
            H2 h2 = (H2) this.h;
            long j = this.g;
            int q = h2.q(j);
            rVar.accept((h2.c == 0 && q == 0) ? ((int[]) h2.e)[(int) j] : ((int[][]) h2.f)[q][(int) (j - h2.d[q])]);
        }
        return d;
    }

    @Override // j$.util.stream.Q2
    final void h() {
        H2 h2 = new H2();
        this.h = h2;
        this.e = this.b.t0(new C0046a3(h2, 0));
        this.f = new C0042a(5, this);
    }

    @Override // j$.util.A
    /* renamed from: i */
    public final void forEachRemaining(r rVar) {
        if (this.h != null || this.i) {
            do {
            } while (tryAdvance(rVar));
            return;
        }
        rVar.getClass();
        f();
        this.b.s0(this.d, new C0046a3(rVar, 1));
        this.i = true;
    }

    @Override // j$.util.stream.Q2
    final Q2 j(Spliterator spliterator) {
        return new b3(this.b, spliterator, this.a);
    }

    @Override // j$.util.stream.Q2, j$.util.Spliterator
    public final A trySplit() {
        return (A) super.trySplit();
    }
}
