package j$.util.stream;

import j$.util.Spliterator;
import java.util.concurrent.CountedCompleter;

/* loaded from: classes2.dex */
public final class Q extends CountedCompleter {
    private Spliterator a;
    private final AbstractC0054c2 b;
    private final AbstractC0115s0 c;
    private long d;

    Q(Q q, Spliterator spliterator) {
        super(q);
        this.a = spliterator;
        this.b = q.b;
        this.d = q.d;
        this.c = q.c;
    }

    public Q(AbstractC0115s0 s0Var, Spliterator spliterator, AbstractC0054c2 c2Var) {
        super(null);
        this.b = c2Var;
        this.c = s0Var;
        this.a = spliterator;
        this.d = 0;
    }

    @Override // java.util.concurrent.CountedCompleter
    public final void compute() {
        Spliterator trySplit;
        Spliterator spliterator = this.a;
        long estimateSize = spliterator.estimateSize();
        long j = this.d;
        if (j == 0) {
            j = AbstractC0063f.f(estimateSize);
            this.d = j;
        }
        boolean e = P2.SHORT_CIRCUIT.e(this.c.f0());
        boolean z = false;
        AbstractC0054c2 c2Var = this.b;
        Q q = this;
        while (true) {
            if (e && c2Var.f()) {
                break;
            } else if (estimateSize <= j || (trySplit = spliterator.trySplit()) == null) {
                break;
            } else {
                q = new Q(q, trySplit);
                q.addToPendingCount(1);
                if (z) {
                    spliterator = trySplit;
                } else {
                    q = q;
                    q = q;
                }
                z = !z;
                q.fork();
                estimateSize = spliterator.estimateSize();
            }
        }
        q.c.a0(spliterator, c2Var);
        q.a = null;
        q.propagateCompletion();
    }
}
