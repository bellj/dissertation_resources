package j$.util.stream;

import java.util.concurrent.CountedCompleter;

/* renamed from: j$.util.stream.q1 */
/* loaded from: classes2.dex */
public abstract class AbstractC0109q1 extends CountedCompleter {
    protected final B0 a;
    protected final int b;

    public AbstractC0109q1(B0 b0) {
        this.a = b0;
        this.b = 0;
    }

    public AbstractC0109q1(AbstractC0109q1 q1Var, B0 b0, int i) {
        super(q1Var);
        this.a = b0;
        this.b = i;
    }

    abstract void a();

    abstract C0105p1 b(int i, int i2);

    @Override // java.util.concurrent.CountedCompleter
    public final void compute() {
        AbstractC0109q1 q1Var = this;
        while (q1Var.a.k() != 0) {
            q1Var.setPendingCount(q1Var.a.k() - 1);
            int i = 0;
            int i2 = 0;
            while (i < q1Var.a.k() - 1) {
                C0105p1 b = q1Var.b(i, q1Var.b + i2);
                i2 = (int) (((long) i2) + b.a.count());
                b.fork();
                i++;
            }
            q1Var = q1Var.b(i, q1Var.b + i2);
        }
        q1Var.a();
        q1Var.propagateCompletion();
    }
}
