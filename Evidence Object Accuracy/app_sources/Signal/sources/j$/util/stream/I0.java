package j$.util.stream;

import j$.util.E;
import j$.util.Spliterator;
import j$.util.function.Consumer;
import j$.util.function.IntFunction;

/* loaded from: classes2.dex */
public final class I0 extends L0 implements AbstractC0135x0 {
    public I0(AbstractC0135x0 x0Var, AbstractC0135x0 x0Var2) {
        super(x0Var, x0Var2);
    }

    /* renamed from: d */
    public final /* synthetic */ void i(Double[] dArr, int i) {
        AbstractC0115s0.N(this, dArr, i);
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ void forEach(Consumer consumer) {
        AbstractC0115s0.Q(this, consumer);
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ B0 n(long j, long j2, IntFunction intFunction) {
        return AbstractC0115s0.T(this, j, j2);
    }

    @Override // j$.util.stream.A0
    public final Object newArray(int i) {
        return new double[i];
    }

    @Override // j$.util.stream.B0
    /* renamed from: spliterator */
    public final E mo148spliterator() {
        return new Z0(this);
    }

    @Override // j$.util.stream.B0
    /* renamed from: spliterator */
    public final Spliterator mo148spliterator() {
        return new Z0(this);
    }
}
