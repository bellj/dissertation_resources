package j$.util.stream;

import j$.util.E;
import j$.util.Spliterator;
import j$.util.function.Consumer;
import j$.util.function.IntFunction;

/* loaded from: classes2.dex */
public final class K0 extends L0 implements AbstractC0143z0 {
    public K0(AbstractC0143z0 z0Var, AbstractC0143z0 z0Var2) {
        super(z0Var, z0Var2);
    }

    /* renamed from: d */
    public final /* synthetic */ void i(Long[] lArr, int i) {
        AbstractC0115s0.P(this, lArr, i);
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ void forEach(Consumer consumer) {
        AbstractC0115s0.S(this, consumer);
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ B0 n(long j, long j2, IntFunction intFunction) {
        return AbstractC0115s0.V(this, j, j2);
    }

    @Override // j$.util.stream.A0
    public final Object newArray(int i) {
        return new long[i];
    }

    @Override // j$.util.stream.B0
    /* renamed from: spliterator */
    public final E mo148spliterator() {
        return new C0049b1(this);
    }

    @Override // j$.util.stream.B0
    /* renamed from: spliterator */
    public final Spliterator mo148spliterator() {
        return new C0049b1(this);
    }
}
