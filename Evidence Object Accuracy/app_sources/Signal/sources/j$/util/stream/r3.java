package j$.util.stream;

import j$.util.AbstractC0037n;
import j$.util.Spliterator;
import j$.util.function.Consumer;
import java.util.Comparator;

/* loaded from: classes2.dex */
public final class r3 extends s3 implements Spliterator, Consumer {
    Object e;

    public r3(Spliterator spliterator, long j, long j2) {
        super(spliterator, j, j2);
    }

    r3(Spliterator spliterator, r3 r3Var) {
        super(spliterator, r3Var);
    }

    @Override // j$.util.Spliterator
    public final boolean a(Consumer consumer) {
        consumer.getClass();
        while (p() != 1 && this.a.a(this)) {
            if (n(1) == 1) {
                consumer.accept(this.e);
                this.e = null;
                return true;
            }
        }
        return false;
    }

    @Override // j$.util.function.Consumer
    public final void accept(Object obj) {
        this.e = obj;
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // j$.util.Spliterator
    public final void forEachRemaining(Consumer consumer) {
        consumer.getClass();
        V2 v2 = null;
        while (true) {
            int p = p();
            if (p == 1) {
                return;
            }
            if (p == 2) {
                if (v2 == null) {
                    v2 = new V2();
                } else {
                    v2.a = 0;
                }
                long j = 0;
                while (this.a.a(v2)) {
                    j++;
                    if (j >= 128) {
                        break;
                    }
                }
                if (j != 0) {
                    long n = n(j);
                    for (int i = 0; ((long) i) < n; i++) {
                        consumer.accept(v2.b[i]);
                    }
                } else {
                    return;
                }
            } else {
                this.a.forEachRemaining(consumer);
                return;
            }
        }
    }

    @Override // j$.util.Spliterator
    public final Comparator getComparator() {
        throw new IllegalStateException();
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ long getExactSizeIfKnown() {
        return AbstractC0037n.h(this);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean hasCharacteristics(int i) {
        return AbstractC0037n.j(this, i);
    }

    @Override // j$.util.stream.s3
    protected final Spliterator o(Spliterator spliterator) {
        return new r3(spliterator, this);
    }
}
