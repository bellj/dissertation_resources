package j$.util.stream;

import j$.util.Optional;
import j$.util.Spliterator;
import j$.util.T;
import j$.util.function.BiConsumer;
import j$.util.function.BiFunction;
import j$.util.function.BinaryOperator;
import j$.util.function.C0019b;
import j$.util.function.C0020c;
import j$.util.function.Consumer;
import j$.util.function.Function;
import j$.util.function.IntFunction;
import j$.util.function.Predicate;
import j$.util.function.ToDoubleFunction;
import j$.util.function.ToIntFunction;
import j$.util.function.ToLongFunction;
import j$.util.stream.Collector;
import java.util.Comparator;
import java.util.Iterator;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public abstract class U1 extends AbstractC0051c implements Stream {
    public static final /* synthetic */ int l;

    public U1(Spliterator spliterator, int i, boolean z) {
        super(spliterator, i, z);
    }

    public U1(AbstractC0051c cVar, int i) {
        super(cVar, i);
    }

    @Override // j$.util.stream.Stream
    public final AbstractC0080j0 A(ToLongFunction toLongFunction) {
        toLongFunction.getClass();
        return new C0126v(this, P2.p | P2.n, toLongFunction, 7);
    }

    @Override // j$.util.stream.Stream
    public final C C(ToDoubleFunction toDoubleFunction) {
        toDoubleFunction.getClass();
        return new C0114s(this, P2.p | P2.n, toDoubleFunction, 6);
    }

    @Override // j$.util.stream.AbstractC0051c
    final Spliterator J0(AbstractC0115s0 s0Var, C0042a aVar, boolean z) {
        return new u3(s0Var, aVar, z);
    }

    @Override // j$.util.stream.Stream
    public final IntStream a(Function function) {
        function.getClass();
        return new C0122u(this, P2.p | P2.n | P2.t, function, 7);
    }

    @Override // j$.util.stream.Stream
    public final boolean allMatch(Predicate predicate) {
        return ((Boolean) v0(AbstractC0115s0.p0(predicate, EnumC0104p0.ALL))).booleanValue();
    }

    @Override // j$.util.stream.Stream
    public final boolean anyMatch(Predicate predicate) {
        return ((Boolean) v0(AbstractC0115s0.p0(predicate, EnumC0104p0.ANY))).booleanValue();
    }

    @Override // j$.util.stream.Stream
    public final C c(Function function) {
        function.getClass();
        return new C0114s(this, P2.p | P2.n | P2.t, function, 7);
    }

    @Override // j$.util.stream.Stream
    public final Object collect(Collector collector) {
        Object obj;
        if (!isParallel() || !collector.characteristics().contains(Collector.Characteristics.CONCURRENT) || (B0() && !collector.characteristics().contains(Collector.Characteristics.UNORDERED))) {
            collector.getClass();
            obj = v0(new B1(1, collector.combiner(), collector.accumulator(), collector.supplier(), collector));
        } else {
            obj = collector.supplier().get();
            forEach(new C0083k(5, collector.accumulator(), obj));
        }
        return collector.characteristics().contains(Collector.Characteristics.IDENTITY_FINISH) ? obj : collector.finisher().apply(obj);
    }

    @Override // j$.util.stream.Stream
    public final long count() {
        return ((AbstractC0072h0) A(new C0052c0(9))).sum();
    }

    @Override // j$.util.stream.Stream
    public final Stream distinct() {
        return new C0095n(this, P2.m | P2.t);
    }

    @Override // j$.util.stream.Stream
    public final Object e(Object obj, BiFunction biFunction, C0020c cVar) {
        biFunction.getClass();
        cVar.getClass();
        return v0(new C0116s1(1, cVar, biFunction, obj, 2));
    }

    @Override // j$.util.stream.Stream
    public final Stream filter(Predicate predicate) {
        predicate.getClass();
        return new C0118t(this, P2.t, predicate, 4);
    }

    @Override // j$.util.stream.Stream
    public final Optional findAny() {
        return (Optional) v0(new D(false, 1, Optional.empty(), new C0107q(4), new C0047b(16)));
    }

    @Override // j$.util.stream.Stream
    public final Optional findFirst() {
        return (Optional) v0(new D(true, 1, Optional.empty(), new C0107q(4), new C0047b(16)));
    }

    @Override // j$.util.stream.Stream
    public final Stream flatMap(Function function) {
        function.getClass();
        return new Q1(this, P2.p | P2.n | P2.t, function, 1);
    }

    @Override // j$.util.stream.Stream
    public void forEach(Consumer consumer) {
        consumer.getClass();
        v0(new N(consumer, false));
    }

    @Override // j$.util.stream.AbstractC0071h
    public final Iterator iterator() {
        return T.i(spliterator());
    }

    @Override // j$.util.stream.Stream
    public final Stream j(Consumer consumer) {
        consumer.getClass();
        return new C0118t(this, 0, consumer, 3);
    }

    @Override // j$.util.stream.Stream
    public final Object l(C0020c cVar, BiConsumer biConsumer, BiConsumer biConsumer2) {
        cVar.getClass();
        biConsumer.getClass();
        biConsumer2.getClass();
        return v0(new C0116s1(1, biConsumer2, biConsumer, cVar, 3));
    }

    @Override // j$.util.stream.Stream
    public final Stream limit(long j) {
        if (j >= 0) {
            return AbstractC0115s0.q0(this, 0, j);
        }
        throw new IllegalArgumentException(Long.toString(j));
    }

    @Override // j$.util.stream.Stream
    public final Stream map(Function function) {
        function.getClass();
        return new Q1(this, P2.p | P2.n, function, 0);
    }

    @Override // j$.util.stream.Stream
    public final Optional max(Comparator comparator) {
        comparator.getClass();
        return t(new C0019b(comparator, 0));
    }

    @Override // j$.util.stream.Stream
    public final Optional min(Comparator comparator) {
        comparator.getClass();
        return t(new C0019b(comparator, 1));
    }

    @Override // j$.util.stream.Stream
    public final boolean noneMatch(Predicate predicate) {
        return ((Boolean) v0(AbstractC0115s0.p0(predicate, EnumC0104p0.NONE))).booleanValue();
    }

    @Override // j$.util.stream.AbstractC0115s0
    public final AbstractC0131w0 o0(long j, IntFunction intFunction) {
        return AbstractC0112r1.g(j, intFunction);
    }

    @Override // j$.util.stream.Stream
    public void q(Consumer consumer) {
        consumer.getClass();
        v0(new N(consumer, true));
    }

    @Override // j$.util.stream.Stream
    public final Object reduce(Object obj, BinaryOperator binaryOperator) {
        binaryOperator.getClass();
        return v0(new C0116s1(1, binaryOperator, binaryOperator, obj, 2));
    }

    public final IntStream s(ToIntFunction toIntFunction) {
        toIntFunction.getClass();
        return new C0122u(this, P2.p | P2.n, toIntFunction, 6);
    }

    public final Stream skip(long j) {
        if (j >= 0) {
            return j == 0 ? this : AbstractC0115s0.q0(this, j, -1);
        }
        throw new IllegalArgumentException(Long.toString(j));
    }

    public final Stream sorted() {
        return new C0137x2(this);
    }

    public final Stream sorted(Comparator comparator) {
        return new C0137x2(this, comparator);
    }

    public final Optional t(BinaryOperator binaryOperator) {
        binaryOperator.getClass();
        return (Optional) v0(new C0132w1(1, binaryOperator, 1));
    }

    public final Object[] toArray() {
        return toArray(new C0052c0(8));
    }

    public final Object[] toArray(IntFunction intFunction) {
        return AbstractC0112r1.o(w0(intFunction), intFunction).m(intFunction);
    }

    public final AbstractC0071h unordered() {
        return !B0() ? this : new P1(this, P2.r);
    }

    public final AbstractC0080j0 x(Function function) {
        function.getClass();
        return new C0126v(this, P2.p | P2.n | P2.t, function, 6);
    }

    final B0 x0(AbstractC0115s0 s0Var, Spliterator spliterator, boolean z, IntFunction intFunction) {
        return AbstractC0112r1.h(s0Var, spliterator, z, intFunction);
    }

    final void y0(Spliterator spliterator, AbstractC0054c2 c2Var) {
        while (!c2Var.f() && spliterator.a(c2Var)) {
        }
    }

    final int z0() {
        return 1;
    }
}
