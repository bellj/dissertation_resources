package j$.util.stream;

import j$.util.Spliterator;

/* renamed from: j$.util.stream.l1 */
/* loaded from: classes2.dex */
public final class C0089l1 extends AbstractC0093m1 {
    private final Object[] h;

    public C0089l1(Spliterator spliterator, AbstractC0115s0 s0Var, Object[] objArr) {
        super(objArr.length, spliterator, s0Var);
        this.h = objArr;
    }

    C0089l1(C0089l1 l1Var, Spliterator spliterator, long j, long j2) {
        super(l1Var, spliterator, j, j2, l1Var.h.length);
        this.h = l1Var.h;
    }

    @Override // j$.util.stream.AbstractC0093m1
    final AbstractC0093m1 a(Spliterator spliterator, long j, long j2) {
        return new C0089l1(this, spliterator, j, j2);
    }

    @Override // j$.util.function.Consumer
    public final void accept(Object obj) {
        int i = this.f;
        if (i < this.g) {
            Object[] objArr = this.h;
            this.f = i + 1;
            objArr[i] = obj;
            return;
        }
        throw new IndexOutOfBoundsException(Integer.toString(this.f));
    }
}
