package j$.util.stream;

import java.util.Arrays;

/* renamed from: j$.util.stream.z2 */
/* loaded from: classes2.dex */
final class C0145z2 extends AbstractC0098n2 {
    private double[] c;
    private int d;

    public C0145z2(AbstractC0054c2 c2Var) {
        super(c2Var);
    }

    @Override // j$.util.stream.Z1, j$.util.stream.AbstractC0054c2
    public final void accept(double d) {
        double[] dArr = this.c;
        int i = this.d;
        this.d = i + 1;
        dArr[i] = d;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        if (j < 2147483639) {
            this.c = new double[(int) j];
            return;
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    @Override // j$.util.stream.V1, j$.util.stream.AbstractC0054c2
    public final void end() {
        int i = 0;
        Arrays.sort(this.c, 0, this.d);
        this.a.d((long) this.d);
        if (!this.b) {
            while (i < this.d) {
                this.a.accept(this.c[i]);
                i++;
            }
        } else {
            while (i < this.d && !this.a.f()) {
                this.a.accept(this.c[i]);
                i++;
            }
        }
        this.a.end();
        this.c = null;
    }
}
