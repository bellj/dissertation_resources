package j$.util.stream;

import j$.util.AbstractC0037n;
import j$.util.C;
import j$.util.Spliterator;
import j$.util.function.Consumer;

/* loaded from: classes2.dex */
public final class j3 extends k3 implements C {
    public j3(C c, long j, long j2) {
        super(c, j, j2);
    }

    j3(C c, long j, long j2, long j3, long j4) {
        super(c, j, j2, j3, j4);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.o(this, consumer);
    }

    @Override // j$.util.stream.m3
    protected final Spliterator d(Spliterator spliterator, long j, long j2, long j3, long j4) {
        return new j3((C) spliterator, j, j2, j3, j4);
    }

    @Override // j$.util.stream.k3
    protected final Object e() {
        return new i3(0);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.f(this, consumer);
    }
}
