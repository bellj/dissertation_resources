package j$.util.stream;

/* renamed from: j$.util.stream.f2 */
/* loaded from: classes2.dex */
final class C0066f2 extends W1 {
    long b;
    long c;
    final /* synthetic */ C0070g2 d;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0066f2(C0070g2 g2Var, AbstractC0054c2 c2Var) {
        super(c2Var);
        this.d = g2Var;
        this.b = g2Var.m;
        long j = g2Var.n;
        this.c = j < 0 ? Long.MAX_VALUE : j;
    }

    @Override // j$.util.stream.AbstractC0045a2, j$.util.stream.AbstractC0054c2
    public final void accept(int i) {
        long j = this.b;
        if (j == 0) {
            long j2 = this.c;
            if (j2 > 0) {
                this.c = j2 - 1;
                this.a.accept(i);
                return;
            }
            return;
        }
        this.b = j - 1;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        this.a.d(AbstractC0115s0.Z(j, this.d.m, this.c));
    }

    @Override // j$.util.stream.W1, j$.util.stream.AbstractC0054c2
    public final boolean f() {
        return this.c == 0 || this.a.f();
    }
}
