package j$.util.stream;

import j$.util.function.Consumer;

/* loaded from: classes2.dex */
public final class N extends O {
    final Consumer b;

    public N(Consumer consumer, boolean z) {
        super(z);
        this.b = consumer;
    }

    @Override // j$.util.function.Consumer
    public final void accept(Object obj) {
        this.b.accept(obj);
    }
}
