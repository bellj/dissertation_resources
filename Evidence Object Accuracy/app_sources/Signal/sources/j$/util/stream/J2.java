package j$.util.stream;

import j$.util.C;
import j$.util.T;
import j$.util.function.B;
import j$.util.function.Consumer;
import java.util.Arrays;
import java.util.Iterator;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public class J2 extends L2 implements B {
    public J2() {
    }

    public J2(int i) {
        super(i);
    }

    @Override // j$.util.function.B
    public void accept(long j) {
        t();
        int i = this.b;
        this.b = i + 1;
        ((long[]) this.e)[i] = j;
    }

    @Override // j$.lang.Iterable
    public final void forEach(Consumer consumer) {
        if (consumer instanceof B) {
            g((B) consumer);
        } else if (!A3.a) {
            mo150spliterator().forEachRemaining(consumer);
        } else {
            A3.a(getClass(), "{0} calling SpinedBuffer.OfLong.forEach(Consumer)");
            throw null;
        }
    }

    @Override // java.lang.Iterable, j$.lang.Iterable
    public final Iterator iterator() {
        return T.h(mo150spliterator());
    }

    @Override // j$.util.stream.L2
    public final Object newArray(int i) {
        return new long[i];
    }

    @Override // j$.util.stream.L2
    public final void o(Object obj, int i, int i2, Object obj2) {
        long[] jArr = (long[]) obj;
        B b = (B) obj2;
        while (i < i2) {
            b.accept(jArr[i]);
            i++;
        }
    }

    @Override // j$.util.stream.L2
    public final int p(Object obj) {
        return ((long[]) obj).length;
    }

    @Override // j$.util.stream.L2
    protected final Object[] s() {
        return new long[8];
    }

    @Override // java.lang.Object
    public final String toString() {
        long[] jArr = (long[]) b();
        return jArr.length < 200 ? String.format("%s[length=%d, chunks=%d]%s", getClass().getSimpleName(), Integer.valueOf(jArr.length), Integer.valueOf(this.c), Arrays.toString(jArr)) : String.format("%s[length=%d, chunks=%d]%s...", getClass().getSimpleName(), Integer.valueOf(jArr.length), Integer.valueOf(this.c), Arrays.toString(Arrays.copyOf(jArr, 200)));
    }

    /* renamed from: u */
    public C mo150spliterator() {
        return new I2(this, 0, this.c, 0, this.b);
    }
}
