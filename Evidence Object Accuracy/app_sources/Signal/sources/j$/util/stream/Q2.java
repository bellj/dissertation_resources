package j$.util.stream;

import j$.util.AbstractC0037n;
import j$.util.Spliterator;
import j$.util.function.Supplier;
import java.util.Comparator;

/* loaded from: classes2.dex */
public abstract class Q2 implements Spliterator {
    final boolean a;
    final AbstractC0115s0 b;
    private Supplier c;
    Spliterator d;
    AbstractC0054c2 e;
    C0042a f;
    long g;
    AbstractC0059e h;
    boolean i;

    public Q2(AbstractC0115s0 s0Var, Spliterator spliterator, boolean z) {
        this.b = s0Var;
        this.c = null;
        this.d = spliterator;
        this.a = z;
    }

    public Q2(AbstractC0115s0 s0Var, C0042a aVar, boolean z) {
        this.b = s0Var;
        this.c = aVar;
        this.d = null;
        this.a = z;
    }

    private boolean e() {
        boolean z;
        while (this.h.count() == 0) {
            if (!this.e.f()) {
                C0042a aVar = this.f;
                switch (aVar.a) {
                    case 4:
                        Z2 z2 = (Z2) aVar.b;
                        z = z2.d.a(z2.e);
                        break;
                    case 5:
                        b3 b3Var = (b3) aVar.b;
                        z = b3Var.d.a(b3Var.e);
                        break;
                    case 6:
                        d3 d3Var = (d3) aVar.b;
                        z = d3Var.d.a(d3Var.e);
                        break;
                    default:
                        u3 u3Var = (u3) aVar.b;
                        z = u3Var.d.a(u3Var.e);
                        break;
                }
                if (z) {
                    continue;
                }
            }
            if (this.i) {
                return false;
            }
            this.e.end();
            this.i = true;
        }
        return true;
    }

    @Override // j$.util.Spliterator
    public final int characteristics() {
        f();
        int h = P2.h(this.b.f0()) & P2.f;
        return (h & 64) != 0 ? (h & -16449) | (this.d.characteristics() & 16448) : h;
    }

    public final boolean d() {
        AbstractC0059e eVar = this.h;
        boolean z = false;
        if (eVar != null) {
            long j = this.g + 1;
            this.g = j;
            if (j < eVar.count()) {
                z = true;
            }
            if (z) {
                return z;
            }
            this.g = 0;
            this.h.clear();
            return e();
        } else if (this.i) {
            return false;
        } else {
            f();
            h();
            this.g = 0;
            this.e.d(this.d.getExactSizeIfKnown());
            return e();
        }
    }

    @Override // j$.util.Spliterator
    public final long estimateSize() {
        f();
        return this.d.estimateSize();
    }

    public final void f() {
        if (this.d == null) {
            this.d = (Spliterator) this.c.get();
            this.c = null;
        }
    }

    @Override // j$.util.Spliterator
    public final Comparator getComparator() {
        if (AbstractC0037n.j(this, 4)) {
            return null;
        }
        throw new IllegalStateException();
    }

    @Override // j$.util.Spliterator
    public final long getExactSizeIfKnown() {
        f();
        if (P2.SIZED.e(this.b.f0())) {
            return this.d.getExactSizeIfKnown();
        }
        return -1;
    }

    abstract void h();

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean hasCharacteristics(int i) {
        return AbstractC0037n.j(this, i);
    }

    abstract Q2 j(Spliterator spliterator);

    public final String toString() {
        return String.format("%s[%s]", getClass().getName(), this.d);
    }

    @Override // j$.util.Spliterator
    public Spliterator trySplit() {
        if (!this.a || this.i) {
            return null;
        }
        f();
        Spliterator trySplit = this.d.trySplit();
        if (trySplit == null) {
            return null;
        }
        return j(trySplit);
    }
}
