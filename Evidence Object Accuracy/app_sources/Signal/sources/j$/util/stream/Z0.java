package j$.util.stream;

import j$.util.AbstractC0037n;
import j$.util.function.Consumer;
import j$.util.y;

/* loaded from: classes2.dex */
final class Z0 extends AbstractC0053c1 implements y {
    public Z0(AbstractC0135x0 x0Var) {
        super(x0Var);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.l(this, consumer);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.d(this, consumer);
    }
}
