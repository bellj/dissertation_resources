package j$.util.stream;

import j$.util.Spliterator;
import java.util.concurrent.CountedCompleter;

/* loaded from: classes2.dex */
public final class N1 extends AbstractC0063f {
    private final AbstractC0112r1 h;

    N1(N1 n1, Spliterator spliterator) {
        super(n1, spliterator);
        this.h = n1.h;
    }

    public N1(AbstractC0112r1 r1Var, AbstractC0115s0 s0Var, Spliterator spliterator) {
        super(s0Var, spliterator);
        this.h = r1Var;
    }

    @Override // j$.util.stream.AbstractC0063f
    public final Object a() {
        AbstractC0115s0 s0Var = this.a;
        L1 u = this.h.u();
        s0Var.s0(this.b, u);
        return u;
    }

    @Override // j$.util.stream.AbstractC0063f
    public final AbstractC0063f d(Spliterator spliterator) {
        return new N1(this, spliterator);
    }

    @Override // j$.util.stream.AbstractC0063f, java.util.concurrent.CountedCompleter
    public final void onCompletion(CountedCompleter countedCompleter) {
        AbstractC0063f fVar = this.d;
        if (!(fVar == null)) {
            L1 l1 = (L1) ((N1) fVar).b();
            l1.h((L1) ((N1) this.e).b());
            e(l1);
        }
        super.onCompletion(countedCompleter);
    }
}
