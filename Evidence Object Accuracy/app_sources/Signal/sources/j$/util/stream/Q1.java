package j$.util.stream;

import j$.util.function.Function;

/* loaded from: classes2.dex */
final class Q1 extends T1 {
    public final /* synthetic */ int m;
    final /* synthetic */ Function n;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Q1(AbstractC0051c cVar, int i, Function function, int i2) {
        super(cVar, i);
        this.m = i2;
        this.n = function;
    }

    @Override // j$.util.stream.AbstractC0051c
    public final AbstractC0054c2 G0(int i, AbstractC0054c2 c2Var) {
        switch (this.m) {
            case 0:
                return new O1(this, c2Var, 2);
            default:
                return new O1(this, c2Var, 6);
        }
    }
}
