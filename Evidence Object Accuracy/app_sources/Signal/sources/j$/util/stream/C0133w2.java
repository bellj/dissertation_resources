package j$.util.stream;

import j$.util.Spliterator;
import j$.util.function.IntFunction;
import java.util.Arrays;

/* renamed from: j$.util.stream.w2 */
/* loaded from: classes2.dex */
final class C0133w2 extends AbstractC0064f0 {
    public C0133w2(AbstractC0072h0 h0Var) {
        super(h0Var, P2.q | P2.o);
    }

    @Override // j$.util.stream.AbstractC0051c
    public final B0 D0(Spliterator spliterator, IntFunction intFunction, AbstractC0051c cVar) {
        if (P2.SORTED.e(cVar.f0())) {
            return cVar.u0(spliterator, false, intFunction);
        }
        long[] jArr = (long[]) ((AbstractC0143z0) cVar.u0(spliterator, true, intFunction)).b();
        Arrays.sort(jArr);
        return new C0065f1(jArr);
    }

    @Override // j$.util.stream.AbstractC0051c
    public final AbstractC0054c2 G0(int i, AbstractC0054c2 c2Var) {
        c2Var.getClass();
        return P2.SORTED.e(i) ? c2Var : P2.SIZED.e(i) ? new B2(c2Var) : new C0121t2(c2Var);
    }
}
