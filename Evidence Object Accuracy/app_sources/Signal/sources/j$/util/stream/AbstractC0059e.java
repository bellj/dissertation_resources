package j$.util.stream;

/* renamed from: j$.util.stream.e */
/* loaded from: classes2.dex */
public abstract class AbstractC0059e {
    protected final int a;
    protected int b;
    protected int c;
    protected long[] d;

    public AbstractC0059e() {
        this.a = 4;
    }

    public AbstractC0059e(int i) {
        if (i >= 0) {
            this.a = Math.max(4, 32 - Integer.numberOfLeadingZeros(i - 1));
            return;
        }
        throw new IllegalArgumentException("Illegal Capacity: " + i);
    }

    public abstract void clear();

    public final long count() {
        int i = this.c;
        return i == 0 ? (long) this.b : this.d[i] + ((long) this.b);
    }
}
