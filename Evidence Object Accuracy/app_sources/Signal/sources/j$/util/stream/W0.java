package j$.util.stream;

import j$.util.E;
import j$.util.Spliterator;
import j$.util.T;
import j$.util.function.Consumer;
import j$.util.function.IntFunction;
import j$.util.function.r;
import java.util.Arrays;

/* loaded from: classes2.dex */
public class W0 implements AbstractC0139y0 {
    final int[] a;
    int b;

    public W0(long j) {
        if (j < 2147483639) {
            this.a = new int[(int) j];
            this.b = 0;
            return;
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    public W0(int[] iArr) {
        this.a = iArr;
        this.b = iArr.length;
    }

    @Override // j$.util.stream.A0, j$.util.stream.B0
    public final A0 a(int i) {
        throw new IndexOutOfBoundsException();
    }

    @Override // j$.util.stream.A0
    public final Object b() {
        int[] iArr = this.a;
        int length = iArr.length;
        int i = this.b;
        return length == i ? iArr : Arrays.copyOf(iArr, i);
    }

    @Override // j$.util.stream.A0
    public final void c(int i, Object obj) {
        System.arraycopy(this.a, 0, (int[]) obj, i, this.b);
    }

    @Override // j$.util.stream.B0
    public final long count() {
        return (long) this.b;
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ void forEach(Consumer consumer) {
        AbstractC0115s0.R(this, consumer);
    }

    @Override // j$.util.stream.A0
    public final void g(Object obj) {
        r rVar = (r) obj;
        for (int i = 0; i < this.b; i++) {
            rVar.accept(this.a[i]);
        }
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ int k() {
        return 0;
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ Object[] m(IntFunction intFunction) {
        return AbstractC0115s0.M(this, intFunction);
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ B0 n(long j, long j2, IntFunction intFunction) {
        return AbstractC0115s0.U(this, j, j2);
    }

    /* renamed from: o */
    public final /* synthetic */ void i(Integer[] numArr, int i) {
        AbstractC0115s0.O(this, numArr, i);
    }

    @Override // j$.util.stream.A0, j$.util.stream.B0
    /* renamed from: spliterator */
    public final E mo148spliterator() {
        return T.k(this.a, 0, this.b);
    }

    @Override // j$.util.stream.A0, j$.util.stream.B0
    /* renamed from: spliterator */
    public final Spliterator mo148spliterator() {
        return T.k(this.a, 0, this.b);
    }

    public String toString() {
        return String.format("IntArrayNode[%d][%s]", Integer.valueOf(this.a.length - this.b), Arrays.toString(this.a));
    }
}
