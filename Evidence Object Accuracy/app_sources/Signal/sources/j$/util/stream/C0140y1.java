package j$.util.stream;

import j$.util.function.BinaryOperator;
import j$.util.function.Consumer;
import j$.util.function.H;
import j$.util.function.Supplier;

/* renamed from: j$.util.stream.y1 */
/* loaded from: classes2.dex */
final class C0140y1 extends M1 implements L1, Z1 {
    final /* synthetic */ Supplier b;
    final /* synthetic */ H c;
    final /* synthetic */ BinaryOperator d;

    public C0140y1(Supplier supplier, H h, BinaryOperator binaryOperator) {
        this.b = supplier;
        this.c = h;
        this.d = binaryOperator;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void accept(double d) {
        this.c.accept(this.a, d);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(int i) {
        AbstractC0115s0.K();
        throw null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(long j) {
        AbstractC0115s0.L();
        throw null;
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        l((Double) obj);
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        this.a = this.b.get();
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void end() {
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ boolean f() {
        return false;
    }

    @Override // j$.util.stream.L1
    public final void h(L1 l1) {
        this.a = this.d.apply(this.a, ((C0140y1) l1).a);
    }

    @Override // j$.util.stream.Z1
    public final /* synthetic */ void l(Double d) {
        AbstractC0115s0.E(this, d);
    }
}
