package j$.util.stream;

import j$.util.Map;
import j$.util.Spliterator;
import java.util.EnumMap;
import java.util.Map;

/* JADX WARN: Init of enum DISTINCT can be incorrect */
/* JADX WARN: Init of enum SORTED can be incorrect */
/* JADX WARN: Init of enum ORDERED can be incorrect */
/* JADX WARN: Init of enum SIZED can be incorrect */
/* JADX WARN: Init of enum SHORT_CIRCUIT can be incorrect */
/* loaded from: classes2.dex */
public enum P2 {
    DISTINCT(0, r2),
    SORTED(1, r6),
    ORDERED(2, r8),
    SIZED(3, r13),
    SHORT_CIRCUIT(12, r14);
    
    static final int f;
    static final int g;
    static final int h;
    private static final int i;
    private static final int j;
    private static final int k;
    static final int l;
    static final int m;
    static final int n;
    static final int o;
    static final int p;
    static final int q;
    static final int r;
    static final int s;
    static final int t;
    static final int u = SHORT_CIRCUIT.c;
    private final Map a;
    private final int b;
    private final int c;
    private final int d;
    private final int e;

    static {
        O2 o2 = O2.SPLITERATOR;
        N2 g2 = g(o2);
        O2 o22 = O2.STREAM;
        g2.a(o22);
        O2 o23 = O2.OP;
        g2.a.put(o23, 3);
        N2 g3 = g(o2);
        g3.a(o22);
        g3.a.put(o23, 3);
        N2 g4 = g(o2);
        g4.a(o22);
        g4.a.put(o23, 3);
        O2 o24 = O2.TERMINAL_OP;
        g4.a.put(o24, 2);
        O2 o25 = O2.UPSTREAM_TERMINAL_OP;
        g4.a.put(o25, 2);
        N2 g5 = g(o2);
        g5.a(o22);
        g5.a.put(o23, 2);
        g(o23).a(o24);
        f = c(o2);
        g = c(o22);
        h = c(o23);
        c(o24);
        c(o25);
        int i2 = 0;
        for (P2 p2 : values()) {
            i2 |= p2.e;
        }
        i = i2;
        int i3 = g;
        j = i3;
        int i4 = i3 << 1;
        k = i4;
        l = i3 | i4;
        P2 p22 = DISTINCT;
        m = p22.c;
        n = p22.d;
        P2 p23 = SORTED;
        o = p23.c;
        p = p23.d;
        P2 p24 = ORDERED;
        q = p24.c;
        r = p24.d;
        P2 p25 = SIZED;
        s = p25.c;
        t = p25.d;
        u = SHORT_CIRCUIT.c;
    }

    P2(int i2, N2 n2) {
        for (O2 o2 : O2.values()) {
            Map.EL.a(n2.a, o2, 0);
        }
        this.a = n2.a;
        int i3 = i2 * 2;
        this.b = i3;
        this.c = 1 << i3;
        this.d = 2 << i3;
        this.e = 3 << i3;
    }

    public static int a(int i2, int i3) {
        return i2 | (i3 & (i2 == 0 ? i : ((((j & i2) << 1) | i2) | ((k & i2) >> 1)) ^ -1));
    }

    private static int c(O2 o2) {
        P2[] values = values();
        int i2 = 0;
        for (P2 p2 : values) {
            i2 |= ((Integer) p2.a.get(o2)).intValue() << p2.b;
        }
        return i2;
    }

    public static int d(Spliterator spliterator) {
        int characteristics = spliterator.characteristics();
        return ((characteristics & 4) == 0 || spliterator.getComparator() == null) ? f & characteristics : f & characteristics & -5;
    }

    private static N2 g(O2 o2) {
        N2 n2 = new N2(new EnumMap(O2.class));
        n2.a(o2);
        return n2;
    }

    public static int h(int i2) {
        return i2 & ((i2 ^ -1) >> 1) & j;
    }

    public final boolean e(int i2) {
        return (i2 & this.e) == this.c;
    }

    public final boolean f(int i2) {
        int i3 = this.e;
        return (i2 & i3) == i3;
    }
}
