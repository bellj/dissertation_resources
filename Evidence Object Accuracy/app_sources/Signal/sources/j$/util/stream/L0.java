package j$.util.stream;

import j$.util.function.IntFunction;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public abstract class L0 extends D0 implements A0 {
    public L0(A0 a0, A0 a02) {
        super(a0, a02);
    }

    @Override // j$.util.stream.A0
    public final Object b() {
        long count = count();
        if (count < 2147483639) {
            Object newArray = newArray((int) count);
            c(0, newArray);
            return newArray;
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    @Override // j$.util.stream.A0
    public final void c(int i, Object obj) {
        ((A0) this.a).c(i, obj);
        ((A0) this.b).c(i + ((int) ((A0) this.a).count()), obj);
    }

    @Override // j$.util.stream.A0
    public final void g(Object obj) {
        ((A0) this.a).g(obj);
        ((A0) this.b).g(obj);
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ Object[] m(IntFunction intFunction) {
        return AbstractC0115s0.M(this, intFunction);
    }

    public final String toString() {
        return count() < 32 ? String.format("%s[%s.%s]", getClass().getName(), this.a, this.b) : String.format("%s[size=%d]", getClass().getName(), Long.valueOf(count()));
    }
}
