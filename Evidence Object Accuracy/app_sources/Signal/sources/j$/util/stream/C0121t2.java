package j$.util.stream;

import java.util.Arrays;

/* renamed from: j$.util.stream.t2 */
/* loaded from: classes2.dex */
final class C0121t2 extends AbstractC0106p2 {
    private J2 c;

    public C0121t2(AbstractC0054c2 c2Var) {
        super(c2Var);
    }

    @Override // j$.util.stream.AbstractC0050b2, j$.util.stream.AbstractC0054c2
    public final void accept(long j) {
        this.c.accept(j);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        if (j < 2147483639) {
            this.c = j > 0 ? new J2((int) j) : new J2();
            return;
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    @Override // j$.util.stream.X1, j$.util.stream.AbstractC0054c2
    public final void end() {
        long[] jArr = (long[]) this.c.b();
        Arrays.sort(jArr);
        this.a.d((long) jArr.length);
        int i = 0;
        if (!this.b) {
            int length = jArr.length;
            while (i < length) {
                this.a.accept(jArr[i]);
                i++;
            }
        } else {
            int length2 = jArr.length;
            while (i < length2) {
                long j = jArr[i];
                if (this.a.f()) {
                    break;
                }
                this.a.accept(j);
                i++;
            }
        }
        this.a.end();
    }
}
