package j$.util.stream;

import j$.util.AbstractC0032i;
import j$.util.C0017f;
import j$.util.C0033j;
import j$.util.C0038o;
import j$.util.T;
import j$.util.function.BiConsumer;
import j$.util.function.C0020c;
import j$.util.function.C0023f;
import j$.util.stream.Stream;
import java.util.DoubleSummaryStatistics;
import java.util.Iterator;
import java.util.OptionalDouble;
import java.util.PrimitiveIterator;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleConsumer;
import java.util.function.DoubleFunction;
import java.util.function.DoublePredicate;
import java.util.function.DoubleToIntFunction;
import java.util.function.DoubleToLongFunction;
import java.util.function.DoubleUnaryOperator;
import java.util.function.ObjDoubleConsumer;
import java.util.function.Supplier;
import java.util.stream.BaseStream;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

/* loaded from: classes2.dex */
public final /* synthetic */ class B implements DoubleStream {
    final /* synthetic */ C a;

    private /* synthetic */ B(C c) {
        this.a = c;
    }

    public static /* synthetic */ B D(C c) {
        if (c == null) {
            return null;
        }
        return new B(c);
    }

    @Override // java.util.stream.DoubleStream
    public final boolean allMatch(DoublePredicate doublePredicate) {
        C c = this.a;
        C0020c cVar = doublePredicate == null ? null : new C0020c(doublePredicate);
        A a = (A) c;
        a.getClass();
        return ((Boolean) a.v0(AbstractC0115s0.j0(cVar, EnumC0104p0.ALL))).booleanValue();
    }

    @Override // java.util.stream.DoubleStream
    public final boolean anyMatch(DoublePredicate doublePredicate) {
        C c = this.a;
        C0020c cVar = doublePredicate == null ? null : new C0020c(doublePredicate);
        A a = (A) c;
        a.getClass();
        return ((Boolean) a.v0(AbstractC0115s0.j0(cVar, EnumC0104p0.ANY))).booleanValue();
    }

    @Override // java.util.stream.DoubleStream
    public final OptionalDouble average() {
        C0033j jVar;
        A a = (A) this.a;
        a.getClass();
        double[] dArr = (double[]) a.N0(new C0047b(9), new C0047b(10), new C0047b(11));
        if (dArr[2] > 0.0d) {
            Set set = Collectors.a;
            double d = dArr[0] + dArr[1];
            double d2 = dArr[dArr.length - 1];
            if (Double.isNaN(d) && Double.isInfinite(d2)) {
                d = d2;
            }
            jVar = C0033j.d(d / dArr[2]);
        } else {
            jVar = C0033j.a();
        }
        return AbstractC0032i.b(jVar);
    }

    @Override // java.util.stream.DoubleStream
    public final Stream boxed() {
        A a = (A) this.a;
        a.getClass();
        return Stream.Wrapper.convert(new C0118t(a, P2.p | P2.n, new C0052c0(29), 0));
    }

    @Override // java.util.stream.BaseStream, java.lang.AutoCloseable
    public final /* synthetic */ void close() {
        ((AbstractC0051c) this.a).close();
    }

    @Override // java.util.stream.DoubleStream
    public final /* synthetic */ Object collect(Supplier supplier, ObjDoubleConsumer objDoubleConsumer, BiConsumer biConsumer) {
        C c = this.a;
        C0020c cVar = null;
        C0020c cVar2 = supplier == null ? null : new C0020c(supplier);
        if (objDoubleConsumer != null) {
            cVar = new C0020c(objDoubleConsumer);
        }
        return ((A) c).N0(cVar2, cVar, BiConsumer.VivifiedWrapper.convert(biConsumer));
    }

    @Override // java.util.stream.DoubleStream
    public final long count() {
        A a = (A) this.a;
        a.getClass();
        return new C0126v(a, P2.p | P2.n, new C0047b(8), 0).sum();
    }

    @Override // java.util.stream.DoubleStream
    public final DoubleStream distinct() {
        A a = (A) this.a;
        a.getClass();
        return D(((U1) new C0118t(a, P2.p | P2.n, new C0052c0(29), 0).distinct()).C(new C0047b(12)));
    }

    @Override // java.util.stream.DoubleStream
    public final DoubleStream filter(DoublePredicate doublePredicate) {
        C c = this.a;
        C0020c cVar = doublePredicate == null ? null : new C0020c(doublePredicate);
        A a = (A) c;
        a.getClass();
        cVar.getClass();
        return D(new C0114s(a, P2.t, cVar, 2));
    }

    @Override // java.util.stream.DoubleStream
    public final OptionalDouble findAny() {
        return AbstractC0032i.b((C0033j) ((A) this.a).v0(new D(false, 4, C0033j.a(), new C0107q(2), new C0047b(14))));
    }

    @Override // java.util.stream.DoubleStream
    public final OptionalDouble findFirst() {
        return AbstractC0032i.b((C0033j) ((A) this.a).v0(new D(true, 4, C0033j.a(), new C0107q(2), new C0047b(14))));
    }

    @Override // java.util.stream.DoubleStream
    public final DoubleStream flatMap(DoubleFunction doubleFunction) {
        C c = this.a;
        C0020c cVar = doubleFunction == null ? null : new C0020c(doubleFunction);
        A a = (A) c;
        a.getClass();
        return D(new C0114s(a, P2.p | P2.n | P2.t, cVar, 1));
    }

    @Override // java.util.stream.DoubleStream
    public final /* synthetic */ void forEach(DoubleConsumer doubleConsumer) {
        this.a.r(C0023f.a(doubleConsumer));
    }

    @Override // java.util.stream.DoubleStream
    public final /* synthetic */ void forEachOrdered(DoubleConsumer doubleConsumer) {
        this.a.u(C0023f.a(doubleConsumer));
    }

    @Override // java.util.stream.BaseStream
    public final /* synthetic */ boolean isParallel() {
        return ((AbstractC0051c) this.a).isParallel();
    }

    @Override // java.util.stream.DoubleStream, java.util.stream.BaseStream
    public final Iterator iterator() {
        return T.f(((A) this.a).spliterator());
    }

    @Override // java.util.stream.DoubleStream, java.util.stream.BaseStream
    /* renamed from: iterator */
    public final PrimitiveIterator.OfDouble mo135iterator() {
        return C0038o.a(T.f(((A) this.a).spliterator()));
    }

    @Override // java.util.stream.DoubleStream
    public final DoubleStream limit(long j) {
        A a = (A) this.a;
        a.getClass();
        if (j >= 0) {
            return D(AbstractC0115s0.i0(a, 0, j));
        }
        throw new IllegalArgumentException(Long.toString(j));
    }

    @Override // java.util.stream.DoubleStream
    public final DoubleStream map(DoubleUnaryOperator doubleUnaryOperator) {
        C c = this.a;
        C0020c cVar = doubleUnaryOperator == null ? null : new C0020c(doubleUnaryOperator);
        A a = (A) c;
        a.getClass();
        cVar.getClass();
        return D(new C0114s(a, P2.p | P2.n, cVar, 0));
    }

    @Override // java.util.stream.DoubleStream
    public final IntStream mapToInt(DoubleToIntFunction doubleToIntFunction) {
        C c = this.a;
        C0020c cVar = doubleToIntFunction == null ? null : new C0020c(doubleToIntFunction);
        A a = (A) c;
        a.getClass();
        cVar.getClass();
        return C0043a0.D(new C0122u(a, P2.p | P2.n, cVar, 0));
    }

    @Override // java.util.stream.DoubleStream
    public final LongStream mapToLong(DoubleToLongFunction doubleToLongFunction) {
        C c = this.a;
        C0020c cVar = doubleToLongFunction == null ? null : new C0020c(doubleToLongFunction);
        A a = (A) c;
        a.getClass();
        cVar.getClass();
        return C0076i0.D(new C0126v(a, P2.p | P2.n, cVar, 0));
    }

    @Override // java.util.stream.DoubleStream
    public final java.util.stream.Stream mapToObj(DoubleFunction doubleFunction) {
        C c = this.a;
        C0020c cVar = doubleFunction == null ? null : new C0020c(doubleFunction);
        A a = (A) c;
        a.getClass();
        cVar.getClass();
        return Stream.Wrapper.convert(new C0118t(a, P2.p | P2.n, cVar, 0));
    }

    @Override // java.util.stream.DoubleStream
    public final OptionalDouble max() {
        A a = (A) this.a;
        a.getClass();
        return AbstractC0032i.b(a.O0(new C0052c0(28)));
    }

    @Override // java.util.stream.DoubleStream
    public final OptionalDouble min() {
        A a = (A) this.a;
        a.getClass();
        return AbstractC0032i.b(a.O0(new C0052c0(27)));
    }

    @Override // java.util.stream.DoubleStream
    public final boolean noneMatch(DoublePredicate doublePredicate) {
        C c = this.a;
        C0020c cVar = doublePredicate == null ? null : new C0020c(doublePredicate);
        A a = (A) c;
        a.getClass();
        return ((Boolean) a.v0(AbstractC0115s0.j0(cVar, EnumC0104p0.NONE))).booleanValue();
    }

    @Override // java.util.stream.BaseStream
    public final /* synthetic */ BaseStream onClose(Runnable runnable) {
        AbstractC0051c cVar = (AbstractC0051c) this.a;
        cVar.onClose(runnable);
        return C0067g.D(cVar);
    }

    @Override // java.util.stream.DoubleStream
    public final DoubleStream peek(DoubleConsumer doubleConsumer) {
        C c = this.a;
        C0023f a = C0023f.a(doubleConsumer);
        A a2 = (A) c;
        a2.getClass();
        a.getClass();
        return D(new C0114s(a2, 0, a, 3));
    }

    @Override // java.util.stream.DoubleStream
    public final double reduce(double d, DoubleBinaryOperator doubleBinaryOperator) {
        C c = this.a;
        C0020c cVar = doubleBinaryOperator == null ? null : new C0020c(doubleBinaryOperator);
        A a = (A) c;
        a.getClass();
        cVar.getClass();
        return ((Double) a.v0(new C0124u1(4, cVar, d))).doubleValue();
    }

    @Override // java.util.stream.DoubleStream
    public final /* synthetic */ OptionalDouble reduce(DoubleBinaryOperator doubleBinaryOperator) {
        return AbstractC0032i.b(((A) this.a).O0(doubleBinaryOperator == null ? null : new C0020c(doubleBinaryOperator)));
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v4, types: [j$.util.stream.C] */
    @Override // java.util.stream.DoubleStream
    public final DoubleStream skip(long j) {
        A a = (A) this.a;
        a.getClass();
        if (j >= 0) {
            A a2 = a;
            if (j != 0) {
                a2 = AbstractC0115s0.i0(a, j, -1);
            }
            return D(a2);
        }
        throw new IllegalArgumentException(Long.toString(j));
    }

    @Override // java.util.stream.DoubleStream
    public final DoubleStream sorted() {
        A a = (A) this.a;
        a.getClass();
        return D(new C0125u2(a));
    }

    @Override // java.util.stream.DoubleStream
    public final double sum() {
        A a = (A) this.a;
        a.getClass();
        double[] dArr = (double[]) a.N0(new C0047b(13), new C0047b(6), new C0047b(7));
        Set set = Collectors.a;
        double d = dArr[0] + dArr[1];
        double d2 = dArr[dArr.length - 1];
        return (!Double.isNaN(d) || !Double.isInfinite(d2)) ? d : d2;
    }

    @Override // java.util.stream.DoubleStream
    public final DoubleSummaryStatistics summaryStatistics() {
        A a = (A) this.a;
        a.getClass();
        C0017f fVar = (C0017f) a.N0(new C0052c0(17), new C0107q(0), new C0107q(1));
        throw new Error("Java 8+ API desugaring (library desugaring) cannot convert to java.util.DoubleSummaryStatistics");
    }

    @Override // java.util.stream.DoubleStream
    public final double[] toArray() {
        A a = (A) this.a;
        a.getClass();
        return (double[]) AbstractC0112r1.p((AbstractC0135x0) a.w0(new C0047b(5))).b();
    }

    @Override // java.util.stream.BaseStream
    public final /* synthetic */ BaseStream unordered() {
        return C0067g.D(((A) this.a).unordered());
    }
}
