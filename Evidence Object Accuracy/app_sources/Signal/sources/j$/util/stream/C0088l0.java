package j$.util.stream;

import j$.util.function.IntPredicate;

/* renamed from: j$.util.stream.l0 */
/* loaded from: classes2.dex */
final class C0088l0 extends AbstractC0100o0 implements AbstractC0045a2 {
    final /* synthetic */ EnumC0104p0 c;
    final /* synthetic */ IntPredicate d;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0088l0(IntPredicate intPredicate, EnumC0104p0 p0Var) {
        super(p0Var);
        this.c = p0Var;
        this.d = intPredicate;
    }

    @Override // j$.util.stream.AbstractC0100o0, j$.util.stream.AbstractC0054c2
    public final void accept(int i) {
        if (!this.a && this.d.test(i) == this.c.a) {
            this.a = true;
            this.b = this.c.b;
        }
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        e((Integer) obj);
    }

    @Override // j$.util.stream.AbstractC0045a2
    public final /* synthetic */ void e(Integer num) {
        AbstractC0115s0.G(this, num);
    }
}
