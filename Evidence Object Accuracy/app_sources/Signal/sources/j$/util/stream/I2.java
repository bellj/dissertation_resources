package j$.util.stream;

import j$.util.AbstractC0037n;
import j$.util.C;
import j$.util.E;
import j$.util.T;
import j$.util.function.B;
import j$.util.function.Consumer;

/* loaded from: classes2.dex */
public final class I2 extends K2 implements C {
    final /* synthetic */ J2 g;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public I2(J2 j2, int i, int i2, int i3, int i4) {
        super(j2, i, i2, i3, i4);
        this.g = j2;
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.o(this, consumer);
    }

    @Override // j$.util.stream.K2
    final void d(int i, Object obj, Object obj2) {
        ((B) obj2).accept(((long[]) obj)[i]);
    }

    @Override // j$.util.stream.K2
    final E e(Object obj, int i, int i2) {
        return T.l((long[]) obj, i, i2 + i);
    }

    @Override // j$.util.stream.K2
    final E f(int i, int i2, int i3, int i4) {
        return new I2(this.g, i, i2, i3, i4);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.f(this, consumer);
    }
}
