package j$.util.stream;

import j$.util.function.AbstractC0024g;
import j$.util.function.AbstractC0025h;
import j$.util.function.AbstractC0026i;
import j$.util.function.AbstractC0027j;
import j$.util.function.AbstractC0028k;
import j$.util.function.AbstractC0029l;
import j$.util.function.C0020c;

/* loaded from: classes2.dex */
final class r extends V1 {
    public final /* synthetic */ int b;
    final /* synthetic */ AbstractC0051c c;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ r(AbstractC0051c cVar, AbstractC0054c2 c2Var, int i) {
        super(c2Var);
        this.b = i;
        this.c = cVar;
    }

    @Override // j$.util.stream.Z1, j$.util.stream.AbstractC0054c2
    public final void accept(double d) {
        switch (this.b) {
            case 0:
                this.a.accept(((C0020c) ((AbstractC0029l) ((C0114s) this.c).n)).b(d));
                return;
            case 1:
                this.a.accept((AbstractC0054c2) ((AbstractC0025h) ((C0118t) this.c).n).apply(d));
                return;
            case 2:
                this.a.accept(((C0020c) ((AbstractC0027j) ((C0122u) this.c).n)).e(d));
                return;
            case 3:
                this.a.accept(((AbstractC0028k) ((C0126v) this.c).n).applyAsLong(d));
                return;
            case 4:
                C c = (C) ((AbstractC0025h) ((C0114s) this.c).n).apply(d);
                if (c != null) {
                    try {
                        c.sequential().r(new C0103p(1, this));
                    } catch (Throwable th) {
                        try {
                            ((AbstractC0051c) c).close();
                        } catch (Throwable th2) {
                            th.addSuppressed(th2);
                        }
                        throw th;
                    }
                }
                if (c != null) {
                    ((AbstractC0051c) c).close();
                    return;
                }
                return;
            case 5:
                if (((C0020c) ((AbstractC0026i) ((C0114s) this.c).n)).i(d)) {
                    this.a.accept(d);
                    return;
                }
                return;
            default:
                ((AbstractC0024g) ((C0114s) this.c).n).accept(d);
                this.a.accept(d);
                return;
        }
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        switch (this.b) {
            case 4:
                this.a.d(-1);
                return;
            case 5:
                this.a.d(-1);
                return;
            default:
                this.a.d(j);
                return;
        }
    }
}
