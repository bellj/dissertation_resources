package j$.util.stream;

import j$.util.Spliterator;
import j$.util.function.Consumer;

/* loaded from: classes2.dex */
public final class R1 extends U1 {
    public R1(Spliterator spliterator, int i, boolean z) {
        super(spliterator, i, z);
    }

    @Override // j$.util.stream.AbstractC0051c
    final boolean F0() {
        throw new UnsupportedOperationException();
    }

    @Override // j$.util.stream.AbstractC0051c
    public final AbstractC0054c2 G0(int i, AbstractC0054c2 c2Var) {
        throw new UnsupportedOperationException();
    }

    @Override // j$.util.stream.U1, j$.util.stream.Stream
    public final void forEach(Consumer consumer) {
        if (!isParallel()) {
            I0().forEachRemaining(consumer);
        } else {
            super.forEach(consumer);
        }
    }

    @Override // j$.util.stream.U1, j$.util.stream.Stream
    public final void q(Consumer consumer) {
        if (!isParallel()) {
            I0().forEachRemaining(consumer);
        } else {
            super.q(consumer);
        }
    }
}
