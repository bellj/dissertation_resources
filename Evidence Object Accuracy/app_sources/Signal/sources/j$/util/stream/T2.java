package j$.util.stream;

import j$.util.function.B;

/* loaded from: classes2.dex */
final class T2 extends U2 implements B {
    final long[] c = new long[128];

    @Override // j$.util.stream.U2
    public final void a(Object obj, long j) {
        B b = (B) obj;
        for (int i = 0; ((long) i) < j; i++) {
            b.accept(this.c[i]);
        }
    }

    @Override // j$.util.function.B
    public final void accept(long j) {
        long[] jArr = this.c;
        int i = this.b;
        this.b = i + 1;
        jArr[i] = j;
    }
}
