package j$.util.stream;

import j$.util.Collection$EL;
import j$.util.List;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;

/* renamed from: j$.util.stream.y2 */
/* loaded from: classes2.dex */
final class C0141y2 extends AbstractC0110q2 {
    private ArrayList d;

    public C0141y2(AbstractC0054c2 c2Var, Comparator comparator) {
        super(c2Var, comparator);
    }

    @Override // j$.util.function.Consumer
    public final void accept(Object obj) {
        this.d.add(obj);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        if (j < 2147483639) {
            this.d = j >= 0 ? new ArrayList((int) j) : new ArrayList();
            return;
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    @Override // j$.util.stream.Y1, j$.util.stream.AbstractC0054c2
    public final void end() {
        List.EL.sort(this.d, this.b);
        this.a.d((long) this.d.size());
        if (!this.c) {
            ArrayList arrayList = this.d;
            AbstractC0054c2 c2Var = this.a;
            c2Var.getClass();
            Collection$EL.a(arrayList, new C0042a(3, c2Var));
        } else {
            Iterator it = this.d.iterator();
            while (it.hasNext()) {
                Object next = it.next();
                if (this.a.f()) {
                    break;
                }
                this.a.accept((AbstractC0054c2) next);
            }
        }
        this.a.end();
        this.d = null;
    }
}
