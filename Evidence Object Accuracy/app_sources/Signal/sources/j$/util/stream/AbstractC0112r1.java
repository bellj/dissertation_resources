package j$.util.stream;

import j$.time.b;
import j$.util.Spliterator;
import j$.util.function.IntFunction;

/* renamed from: j$.util.stream.r1 */
/* loaded from: classes2.dex */
public abstract class AbstractC0112r1 implements x3 {
    private static final T0 a = new T0();
    private static final AbstractC0139y0 b = new R0();
    private static final AbstractC0143z0 c = new S0();
    private static final AbstractC0135x0 d = new Q0();
    private static final int[] e = new int[0];
    private static final long[] f = new long[0];
    private static final double[] g = new double[0];

    public /* synthetic */ AbstractC0112r1(int i) {
    }

    public static AbstractC0131w0 g(long j, IntFunction intFunction) {
        return (j < 0 || j >= 2147483639) ? new C0097n1() : new V0(j, intFunction);
    }

    public static B0 h(AbstractC0115s0 s0Var, Spliterator spliterator, boolean z, IntFunction intFunction) {
        long d0 = s0Var.d0(spliterator);
        if (d0 < 0 || !spliterator.hasCharacteristics(16384)) {
            B0 b0 = (B0) new G0(spliterator, intFunction, s0Var).invoke();
            return z ? o(b0, intFunction) : b0;
        } else if (d0 < 2147483639) {
            Object[] objArr = (Object[]) intFunction.apply((int) d0);
            new C0089l1(spliterator, s0Var, objArr).invoke();
            return new E0(objArr);
        } else {
            throw new IllegalArgumentException("Stream size exceeds max array size");
        }
    }

    public static AbstractC0135x0 i(AbstractC0115s0 s0Var, Spliterator spliterator, boolean z) {
        long d0 = s0Var.d0(spliterator);
        if (d0 < 0 || !spliterator.hasCharacteristics(16384)) {
            AbstractC0135x0 x0Var = (AbstractC0135x0) new G0(0, spliterator, s0Var).invoke();
            return z ? p(x0Var) : x0Var;
        } else if (d0 < 2147483639) {
            double[] dArr = new double[(int) d0];
            new C0077i1(spliterator, s0Var, dArr).invoke();
            return new N0(dArr);
        } else {
            throw new IllegalArgumentException("Stream size exceeds max array size");
        }
    }

    public static AbstractC0139y0 j(AbstractC0115s0 s0Var, Spliterator spliterator, boolean z) {
        long d0 = s0Var.d0(spliterator);
        if (d0 < 0 || !spliterator.hasCharacteristics(16384)) {
            AbstractC0139y0 y0Var = (AbstractC0139y0) new G0(1, spliterator, s0Var).invoke();
            return z ? q(y0Var) : y0Var;
        } else if (d0 < 2147483639) {
            int[] iArr = new int[(int) d0];
            new C0081j1(spliterator, s0Var, iArr).invoke();
            return new W0(iArr);
        } else {
            throw new IllegalArgumentException("Stream size exceeds max array size");
        }
    }

    public static AbstractC0143z0 k(AbstractC0115s0 s0Var, Spliterator spliterator, boolean z) {
        long d0 = s0Var.d0(spliterator);
        if (d0 < 0 || !spliterator.hasCharacteristics(16384)) {
            AbstractC0143z0 z0Var = (AbstractC0143z0) new G0(2, spliterator, s0Var).invoke();
            return z ? r(z0Var) : z0Var;
        } else if (d0 < 2147483639) {
            long[] jArr = new long[(int) d0];
            new C0085k1(spliterator, s0Var, jArr).invoke();
            return new C0065f1(jArr);
        } else {
            throw new IllegalArgumentException("Stream size exceeds max array size");
        }
    }

    public static D0 l(int i, B0 b0, B0 b02) {
        int[] iArr = C0.a;
        if (i != 0) {
            int i2 = iArr[i - 1];
            if (i2 == 1) {
                return new M0(b0, b02);
            }
            if (i2 == 2) {
                return new J0((AbstractC0139y0) b0, (AbstractC0139y0) b02);
            }
            if (i2 == 3) {
                return new K0((AbstractC0143z0) b0, (AbstractC0143z0) b02);
            }
            if (i2 == 4) {
                return new I0((AbstractC0135x0) b0, (AbstractC0135x0) b02);
            }
            StringBuilder a2 = b.a("Unknown shape ");
            a2.append(b.b(i));
            throw new IllegalStateException(a2.toString());
        }
        throw null;
    }

    public static AbstractC0119t0 m(long j) {
        return (j < 0 || j >= 2147483639) ? new P0() : new O0(j);
    }

    public static U0 n(int i) {
        Object obj;
        int[] iArr = C0.a;
        if (i != 0) {
            int i2 = iArr[i - 1];
            if (i2 == 1) {
                return a;
            }
            if (i2 == 2) {
                obj = b;
            } else if (i2 == 3) {
                obj = c;
            } else if (i2 == 4) {
                obj = d;
            } else {
                StringBuilder a2 = b.a("Unknown shape ");
                a2.append(b.b(i));
                throw new IllegalStateException(a2.toString());
            }
            return (U0) obj;
        }
        throw null;
    }

    public static B0 o(B0 b0, IntFunction intFunction) {
        if (b0.k() <= 0) {
            return b0;
        }
        long count = b0.count();
        if (count < 2147483639) {
            Object[] objArr = (Object[]) intFunction.apply((int) count);
            new C0105p1(b0, objArr).invoke();
            return new E0(objArr);
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    public static AbstractC0135x0 p(AbstractC0135x0 x0Var) {
        if (x0Var.k() <= 0) {
            return x0Var;
        }
        long count = x0Var.count();
        if (count < 2147483639) {
            double[] dArr = new double[(int) count];
            new C0101o1(x0Var, dArr).invoke();
            return new N0(dArr);
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    public static AbstractC0139y0 q(AbstractC0139y0 y0Var) {
        if (y0Var.k() <= 0) {
            return y0Var;
        }
        long count = y0Var.count();
        if (count < 2147483639) {
            int[] iArr = new int[(int) count];
            new C0101o1(y0Var, iArr).invoke();
            return new W0(iArr);
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    public static AbstractC0143z0 r(AbstractC0143z0 z0Var) {
        if (z0Var.k() <= 0) {
            return z0Var;
        }
        long count = z0Var.count();
        if (count < 2147483639) {
            long[] jArr = new long[(int) count];
            new C0101o1(z0Var, jArr).invoke();
            return new C0065f1(jArr);
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    public static AbstractC0123u0 s(long j) {
        return (j < 0 || j >= 2147483639) ? new Y0() : new X0(j);
    }

    public static AbstractC0127v0 t(long j) {
        return (j < 0 || j >= 2147483639) ? new C0073h1() : new C0069g1(j);
    }

    @Override // j$.util.stream.x3
    public Object a(AbstractC0115s0 s0Var, Spliterator spliterator) {
        return ((L1) new N1(this, s0Var, spliterator).invoke()).get();
    }

    @Override // j$.util.stream.x3
    public /* synthetic */ int b() {
        return 0;
    }

    @Override // j$.util.stream.x3
    public Object c(AbstractC0115s0 s0Var, Spliterator spliterator) {
        L1 u = u();
        s0Var.s0(spliterator, u);
        return u.get();
    }

    public abstract L1 u();
}
