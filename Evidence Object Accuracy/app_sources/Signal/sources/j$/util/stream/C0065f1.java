package j$.util.stream;

import j$.util.E;
import j$.util.Spliterator;
import j$.util.T;
import j$.util.function.B;
import j$.util.function.Consumer;
import j$.util.function.IntFunction;
import java.util.Arrays;

/* renamed from: j$.util.stream.f1 */
/* loaded from: classes2.dex */
public class C0065f1 implements AbstractC0143z0 {
    final long[] a;
    int b;

    public C0065f1(long j) {
        if (j < 2147483639) {
            this.a = new long[(int) j];
            this.b = 0;
            return;
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    public C0065f1(long[] jArr) {
        this.a = jArr;
        this.b = jArr.length;
    }

    @Override // j$.util.stream.A0, j$.util.stream.B0
    public final A0 a(int i) {
        throw new IndexOutOfBoundsException();
    }

    @Override // j$.util.stream.A0
    public final Object b() {
        long[] jArr = this.a;
        int length = jArr.length;
        int i = this.b;
        return length == i ? jArr : Arrays.copyOf(jArr, i);
    }

    @Override // j$.util.stream.A0
    public final void c(int i, Object obj) {
        System.arraycopy(this.a, 0, (long[]) obj, i, this.b);
    }

    @Override // j$.util.stream.B0
    public final long count() {
        return (long) this.b;
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ void forEach(Consumer consumer) {
        AbstractC0115s0.S(this, consumer);
    }

    @Override // j$.util.stream.A0
    public final void g(Object obj) {
        B b = (B) obj;
        for (int i = 0; i < this.b; i++) {
            b.accept(this.a[i]);
        }
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ int k() {
        return 0;
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ Object[] m(IntFunction intFunction) {
        return AbstractC0115s0.M(this, intFunction);
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ B0 n(long j, long j2, IntFunction intFunction) {
        return AbstractC0115s0.V(this, j, j2);
    }

    /* renamed from: o */
    public final /* synthetic */ void i(Long[] lArr, int i) {
        AbstractC0115s0.P(this, lArr, i);
    }

    @Override // j$.util.stream.A0, j$.util.stream.B0
    /* renamed from: spliterator */
    public final E mo148spliterator() {
        return T.l(this.a, 0, this.b);
    }

    @Override // j$.util.stream.A0, j$.util.stream.B0
    /* renamed from: spliterator */
    public final Spliterator mo148spliterator() {
        return T.l(this.a, 0, this.b);
    }

    public String toString() {
        return String.format("LongArrayNode[%d][%s]", Integer.valueOf(this.a.length - this.b), Arrays.toString(this.a));
    }
}
