package j$.util.stream;

import j$.util.Spliterator;
import java.util.Iterator;

/* renamed from: j$.util.stream.h */
/* loaded from: classes2.dex */
public interface AbstractC0071h extends AutoCloseable {
    @Override // java.lang.AutoCloseable
    void close();

    boolean isParallel();

    Iterator iterator();

    AbstractC0071h onClose(Runnable runnable);

    AbstractC0071h parallel();

    AbstractC0071h sequential();

    Spliterator spliterator();

    AbstractC0071h unordered();
}
