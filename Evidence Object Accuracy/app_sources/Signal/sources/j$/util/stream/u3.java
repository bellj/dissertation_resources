package j$.util.stream;

import j$.util.Spliterator;
import j$.util.function.Consumer;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public final class u3 extends Q2 {
    u3(AbstractC0115s0 s0Var, Spliterator spliterator, boolean z) {
        super(s0Var, spliterator, z);
    }

    public u3(AbstractC0115s0 s0Var, C0042a aVar, boolean z) {
        super(s0Var, aVar, z);
    }

    @Override // j$.util.Spliterator
    public final boolean a(Consumer consumer) {
        Object obj;
        consumer.getClass();
        boolean d = d();
        if (d) {
            M2 m2 = (M2) this.h;
            long j = this.g;
            if (m2.c == 0) {
                if (j < ((long) m2.b)) {
                    obj = m2.e[(int) j];
                } else {
                    throw new IndexOutOfBoundsException(Long.toString(j));
                }
            } else if (j < m2.count()) {
                for (int i = 0; i <= m2.c; i++) {
                    long j2 = m2.d[i];
                    Object[] objArr = m2.f[i];
                    if (j < ((long) objArr.length) + j2) {
                        obj = objArr[(int) (j - j2)];
                    }
                }
                throw new IndexOutOfBoundsException(Long.toString(j));
            } else {
                throw new IndexOutOfBoundsException(Long.toString(j));
            }
            consumer.accept(obj);
        }
        return d;
    }

    @Override // j$.util.Spliterator
    public final void forEachRemaining(Consumer consumer) {
        if (this.h != null || this.i) {
            do {
            } while (a(consumer));
            return;
        }
        consumer.getClass();
        f();
        this.b.s0(this.d, new t3(consumer, 1));
        this.i = true;
    }

    @Override // j$.util.stream.Q2
    final void h() {
        M2 m2 = new M2();
        this.h = m2;
        this.e = this.b.t0(new t3(m2, 0));
        this.f = new C0042a(7, this);
    }

    @Override // j$.util.stream.Q2
    final Q2 j(Spliterator spliterator) {
        return new u3(this.b, spliterator, this.a);
    }
}
