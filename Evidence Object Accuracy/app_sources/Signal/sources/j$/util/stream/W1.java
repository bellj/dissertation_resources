package j$.util.stream;

import j$.util.function.Consumer;

/* loaded from: classes2.dex */
public abstract class W1 implements AbstractC0045a2 {
    protected final AbstractC0054c2 a;

    public W1(AbstractC0054c2 c2Var) {
        c2Var.getClass();
        this.a = c2Var;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(double d) {
        AbstractC0115s0.D();
        throw null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(long j) {
        AbstractC0115s0.L();
        throw null;
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        e((Integer) obj);
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // j$.util.stream.AbstractC0045a2
    public final /* synthetic */ void e(Integer num) {
        AbstractC0115s0.G(this, num);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public void end() {
        this.a.end();
    }

    @Override // j$.util.stream.AbstractC0054c2
    public boolean f() {
        return this.a.f();
    }
}
