package j$.util.stream;

import j$.util.C0033j;
import j$.util.function.AbstractC0022e;
import j$.util.function.Consumer;

/* renamed from: j$.util.stream.x1 */
/* loaded from: classes2.dex */
final class C0136x1 implements L1, Z1 {
    private boolean a;
    private double b;
    final /* synthetic */ AbstractC0022e c;

    public C0136x1(AbstractC0022e eVar) {
        this.c = eVar;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void accept(double d) {
        if (this.a) {
            this.a = false;
        } else {
            d = this.c.applyAsDouble(this.b, d);
        }
        this.b = d;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(int i) {
        AbstractC0115s0.K();
        throw null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(long j) {
        AbstractC0115s0.L();
        throw null;
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        l((Double) obj);
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        this.a = true;
        this.b = 0.0d;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void end() {
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ boolean f() {
        return false;
    }

    @Override // j$.util.function.Supplier
    public final Object get() {
        return this.a ? C0033j.a() : C0033j.d(this.b);
    }

    @Override // j$.util.stream.L1
    public final void h(L1 l1) {
        C0136x1 x1Var = (C0136x1) l1;
        if (!x1Var.a) {
            accept(x1Var.b);
        }
    }

    @Override // j$.util.stream.Z1
    public final /* synthetic */ void l(Double d) {
        AbstractC0115s0.E(this, d);
    }
}
