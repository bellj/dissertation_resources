package j$.util.stream;

import j$.util.C;
import j$.util.C0035l;
import j$.util.Spliterator;
import j$.util.T;
import j$.util.function.A;
import j$.util.function.B;
import j$.util.function.BiConsumer;
import j$.util.function.IntFunction;
import j$.util.function.J;
import j$.util.function.Supplier;
import j$.util.function.z;
import java.util.Iterator;

/* renamed from: j$.util.stream.h0 */
/* loaded from: classes2.dex */
public abstract class AbstractC0072h0 extends AbstractC0051c implements AbstractC0080j0 {
    public static final /* synthetic */ int l;

    public AbstractC0072h0(Spliterator spliterator, int i) {
        super(spliterator, i, false);
    }

    public AbstractC0072h0(AbstractC0051c cVar, int i) {
        super(cVar, i);
    }

    public static C M0(Spliterator spliterator) {
        if (spliterator instanceof C) {
            return (C) spliterator;
        }
        if (A3.a) {
            A3.a(AbstractC0051c.class, "using LongStream.adapt(Spliterator<Long> s)");
            throw null;
        }
        throw new UnsupportedOperationException("LongStream.adapt(Spliterator<Long> s)");
    }

    @Override // j$.util.stream.AbstractC0051c
    final Spliterator J0(AbstractC0115s0 s0Var, C0042a aVar, boolean z) {
        return new d3(s0Var, aVar, z);
    }

    public final Object N0(Supplier supplier, J j, BiConsumer biConsumer) {
        C0099o oVar = new C0099o(biConsumer, 2);
        supplier.getClass();
        j.getClass();
        return v0(new C0116s1(3, oVar, j, supplier, 0));
    }

    public final C0035l O0(z zVar) {
        zVar.getClass();
        return (C0035l) v0(new C0132w1(3, zVar, 3));
    }

    /* renamed from: P0 */
    public final C spliterator() {
        return M0(super.spliterator());
    }

    @Override // j$.util.stream.AbstractC0071h
    public final Iterator iterator() {
        return T.h(spliterator());
    }

    @Override // j$.util.stream.AbstractC0080j0
    public void o(A a) {
        a.getClass();
        v0(new M(a, true));
    }

    @Override // j$.util.stream.AbstractC0115s0
    public final AbstractC0131w0 o0(long j, IntFunction intFunction) {
        return AbstractC0112r1.t(j);
    }

    @Override // j$.util.stream.AbstractC0080j0
    public void p(B b) {
        b.getClass();
        v0(new M(b, false));
    }

    public final long sum() {
        return ((Long) v0(new I1(3, new C0107q(15), 0))).longValue();
    }

    @Override // j$.util.stream.AbstractC0071h
    public final AbstractC0071h unordered() {
        return !B0() ? this : new U(this, P2.r, 1);
    }

    @Override // j$.util.stream.AbstractC0051c
    final B0 x0(AbstractC0115s0 s0Var, Spliterator spliterator, boolean z, IntFunction intFunction) {
        return AbstractC0112r1.k(s0Var, spliterator, z);
    }

    @Override // j$.util.stream.AbstractC0051c
    final void y0(Spliterator spliterator, AbstractC0054c2 c2Var) {
        B b;
        C M0 = M0(spliterator);
        if (c2Var instanceof B) {
            b = (B) c2Var;
        } else if (!A3.a) {
            c2Var.getClass();
            b = new C0048b0(0, c2Var);
        } else {
            A3.a(AbstractC0051c.class, "using LongStream.adapt(Sink<Long> s)");
            throw null;
        }
        while (!c2Var.f() && M0.c(b)) {
        }
    }

    @Override // j$.util.stream.AbstractC0051c
    public final int z0() {
        return 3;
    }
}
