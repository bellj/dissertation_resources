package j$.util.stream;

import j$.util.function.AbstractC0028k;
import j$.util.function.BiConsumer;
import j$.util.function.BinaryOperator;
import j$.util.function.Function;
import j$.util.function.G;
import j$.util.function.H;
import j$.util.function.I;
import j$.util.function.IntFunction;
import j$.util.function.J;
import j$.util.function.Supplier;
import j$.util.function.ToDoubleFunction;
import j$.util.function.ToIntFunction;
import j$.util.function.ToLongFunction;
import j$.util.function.x;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/* renamed from: j$.util.stream.b */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0047b implements IntFunction, Function, BinaryOperator, H, BiConsumer, AbstractC0028k, Supplier, ToDoubleFunction, ToIntFunction, x, I, G, ToLongFunction, J {
    public final /* synthetic */ int a;

    public /* synthetic */ C0047b(int i) {
        this.a = i;
    }

    @Override // j$.util.function.I
    public final void a(int i, Object obj) {
        long[] jArr = (long[]) obj;
        int i2 = Z.l;
        jArr[0] = jArr[0] + 1;
        jArr[1] = jArr[1] + ((long) i);
    }

    @Override // j$.util.function.H
    public final void accept(Object obj, double d) {
        switch (this.a) {
            case 6:
                double[] dArr = (double[]) obj;
                int i = A.l;
                Collectors.a(dArr, d);
                dArr[2] = dArr[2] + d;
                return;
            default:
                double[] dArr2 = (double[]) obj;
                int i2 = A.l;
                dArr2[2] = dArr2[2] + 1.0d;
                Collectors.a(dArr2, d);
                dArr2[3] = dArr2[3] + d;
                return;
        }
    }

    @Override // j$.util.function.J
    public final void accept(Object obj, long j) {
        long[] jArr = (long[]) obj;
        int i = AbstractC0072h0.l;
        jArr[0] = jArr[0] + 1;
        jArr[1] = jArr[1] + j;
    }

    @Override // j$.util.function.BiConsumer
    public final void accept(Object obj, Object obj2) {
        switch (this.a) {
            case 7:
                double[] dArr = (double[]) obj;
                double[] dArr2 = (double[]) obj2;
                int i = A.l;
                Collectors.a(dArr, dArr2[0]);
                Collectors.a(dArr, dArr2[1]);
                dArr[2] = dArr[2] + dArr2[2];
                return;
            case 11:
                double[] dArr3 = (double[]) obj;
                double[] dArr4 = (double[]) obj2;
                int i2 = A.l;
                Collectors.a(dArr3, dArr4[0]);
                Collectors.a(dArr3, dArr4[1]);
                dArr3[2] = dArr3[2] + dArr4[2];
                dArr3[3] = dArr3[3] + dArr4[3];
                return;
            default:
                long[] jArr = (long[]) obj;
                long[] jArr2 = (long[]) obj2;
                int i3 = Z.l;
                jArr[0] = jArr[0] + jArr2[0];
                jArr[1] = jArr[1] + jArr2[1];
                return;
        }
    }

    public final /* synthetic */ BiConsumer andThen(BiConsumer biConsumer) {
        switch (this.a) {
            case 7:
                return BiConsumer.CC.$default$andThen(this, biConsumer);
            case 11:
                return BiConsumer.CC.$default$andThen(this, biConsumer);
            default:
                return BiConsumer.CC.$default$andThen(this, biConsumer);
        }
    }

    public final Object apply(int i) {
        switch (this.a) {
            case 0:
                return new Object[i];
            case 5:
                int i2 = A.l;
                return new Double[i];
            case 18:
                int i3 = P.h;
                return new Object[i];
            case 24:
                int i4 = Z.l;
                return new Integer[i];
            default:
                int i5 = AbstractC0072h0.l;
                return new Long[i];
        }
    }

    public final Object apply(Object obj) {
        Set set = Collectors.a;
        return obj;
    }

    public final Object apply(Object obj, Object obj2) {
        switch (this.a) {
            case 2:
                Collection collection = (Collection) obj;
                Set set = Collectors.a;
                collection.addAll((Collection) obj2);
                return collection;
            case 3:
                List list = (List) obj;
                Set set2 = Collectors.a;
                list.addAll((List) obj2);
                return list;
            default:
                Set set3 = (Set) obj;
                Set set4 = Collectors.a;
                set3.addAll((Set) obj2);
                return set3;
        }
    }

    public final double applyAsDouble(Object obj) {
        int i = A.l;
        return ((Double) obj).doubleValue();
    }

    public final int applyAsInt(Object obj) {
        int i = Z.l;
        return ((Integer) obj).intValue();
    }

    public final long applyAsLong(double d) {
        int i = A.l;
        return 1;
    }

    public final long applyAsLong(int i) {
        int i2 = Z.l;
        return 1;
    }

    public final long applyAsLong(long j) {
        int i = AbstractC0072h0.l;
        return 1;
    }

    public final long applyAsLong(Object obj) {
        int i = AbstractC0072h0.l;
        return ((Long) obj).longValue();
    }

    public final /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }

    public final Object get() {
        switch (this.a) {
            case 9:
                int i = A.l;
                return new double[4];
            case 10:
            case 11:
            case 12:
            case 18:
            case 19:
            case 20:
            default:
                int i2 = AbstractC0072h0.l;
                return new long[2];
            case 13:
                int i3 = A.l;
                return new double[3];
            case 14:
                return new E();
            case 15:
                return new G();
            case 16:
                return new H();
            case 17:
                return new F();
            case 21:
                int i4 = Z.l;
                return new long[2];
        }
    }
}
