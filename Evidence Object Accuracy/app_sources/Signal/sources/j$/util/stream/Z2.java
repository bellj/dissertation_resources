package j$.util.stream;

import j$.util.AbstractC0037n;
import j$.util.Spliterator;
import j$.util.function.AbstractC0024g;
import j$.util.function.Consumer;
import j$.util.y;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public final class Z2 extends Q2 implements y {
    Z2(AbstractC0115s0 s0Var, Spliterator spliterator, boolean z) {
        super(s0Var, spliterator, z);
    }

    public Z2(AbstractC0115s0 s0Var, C0042a aVar, boolean z) {
        super(s0Var, aVar, z);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.l(this, consumer);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.d(this, consumer);
    }

    @Override // j$.util.stream.Q2
    final void h() {
        F2 f2 = new F2();
        this.h = f2;
        this.e = this.b.t0(new Y2(f2, 0));
        this.f = new C0042a(4, this);
    }

    @Override // j$.util.stream.Q2
    final Q2 j(Spliterator spliterator) {
        return new Z2(this.b, spliterator, this.a);
    }

    @Override // j$.util.y
    /* renamed from: k */
    public final boolean tryAdvance(AbstractC0024g gVar) {
        gVar.getClass();
        boolean d = d();
        if (d) {
            F2 f2 = (F2) this.h;
            long j = this.g;
            int q = f2.q(j);
            gVar.accept((f2.c == 0 && q == 0) ? ((double[]) f2.e)[(int) j] : ((double[][]) f2.f)[q][(int) (j - f2.d[q])]);
        }
        return d;
    }

    @Override // j$.util.y
    /* renamed from: m */
    public final void forEachRemaining(AbstractC0024g gVar) {
        if (this.h != null || this.i) {
            do {
            } while (tryAdvance(gVar));
            return;
        }
        gVar.getClass();
        f();
        this.b.s0(this.d, new Y2(gVar, 1));
        this.i = true;
    }

    @Override // j$.util.stream.Q2, j$.util.Spliterator
    public final y trySplit() {
        return (y) super.trySplit();
    }
}
