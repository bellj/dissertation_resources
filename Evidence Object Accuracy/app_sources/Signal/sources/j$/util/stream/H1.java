package j$.util.stream;

import j$.util.function.BinaryOperator;
import j$.util.function.Consumer;
import j$.util.function.I;
import j$.util.function.Supplier;

/* loaded from: classes2.dex */
final class H1 extends M1 implements L1, AbstractC0045a2 {
    final /* synthetic */ Supplier b;
    final /* synthetic */ I c;
    final /* synthetic */ BinaryOperator d;

    public H1(Supplier supplier, I i, BinaryOperator binaryOperator) {
        this.b = supplier;
        this.c = i;
        this.d = binaryOperator;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(double d) {
        AbstractC0115s0.D();
        throw null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void accept(int i) {
        this.c.a(i, this.a);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(long j) {
        AbstractC0115s0.L();
        throw null;
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        e((Integer) obj);
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        this.a = this.b.get();
    }

    @Override // j$.util.stream.AbstractC0045a2
    public final /* synthetic */ void e(Integer num) {
        AbstractC0115s0.G(this, num);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void end() {
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ boolean f() {
        return false;
    }

    @Override // j$.util.stream.L1
    public final void h(L1 l1) {
        this.a = this.d.apply(this.a, ((H1) l1).a);
    }
}
