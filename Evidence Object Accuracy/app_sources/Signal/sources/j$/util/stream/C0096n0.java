package j$.util.stream;

import j$.util.function.AbstractC0026i;
import j$.util.function.C0020c;

/* renamed from: j$.util.stream.n0 */
/* loaded from: classes2.dex */
final class C0096n0 extends AbstractC0100o0 implements Z1 {
    final /* synthetic */ EnumC0104p0 c;
    final /* synthetic */ AbstractC0026i d;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0096n0(AbstractC0026i iVar, EnumC0104p0 p0Var) {
        super(p0Var);
        this.c = p0Var;
        this.d = iVar;
    }

    @Override // j$.util.stream.AbstractC0100o0, j$.util.stream.AbstractC0054c2
    public final void accept(double d) {
        if (!this.a && ((C0020c) this.d).i(d) == this.c.a) {
            this.a = true;
            this.b = this.c.b;
        }
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        l((Double) obj);
    }

    @Override // j$.util.stream.Z1
    public final /* synthetic */ void l(Double d) {
        AbstractC0115s0.E(this, d);
    }
}
