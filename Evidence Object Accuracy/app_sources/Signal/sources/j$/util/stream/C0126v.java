package j$.util.stream;

/* renamed from: j$.util.stream.v */
/* loaded from: classes2.dex */
public final class C0126v extends AbstractC0068g0 {
    public final /* synthetic */ int m;
    final /* synthetic */ Object n;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ C0126v(AbstractC0051c cVar, int i, Object obj, int i2) {
        super(cVar, i);
        this.m = i2;
        this.n = obj;
    }

    @Override // j$.util.stream.AbstractC0051c
    public final AbstractC0054c2 G0(int i, AbstractC0054c2 c2Var) {
        switch (this.m) {
            case 0:
                return new r(this, c2Var, 3);
            case 1:
                return new T(this, c2Var, 5);
            case 2:
                return new C0056d0(this, c2Var, 1);
            case 3:
                return new C0056d0(this, c2Var, 5);
            case 4:
                return new C0056d0(this, c2Var, 6);
            case 5:
                return new C0056d0(this, c2Var, 7);
            case 6:
                return new C0091m(this, c2Var);
            default:
                return new O1(this, c2Var, 4);
        }
    }
}
