package j$.util.stream;

import j$.util.function.AbstractC0022e;
import j$.util.function.BinaryOperator;
import j$.util.function.p;
import j$.util.function.z;

/* renamed from: j$.util.stream.w1 */
/* loaded from: classes2.dex */
public final class C0132w1 extends AbstractC0112r1 {
    public final /* synthetic */ int h;
    final /* synthetic */ Object i;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ C0132w1(int i, Object obj, int i2) {
        super(i);
        this.h = i2;
        this.i = obj;
    }

    @Override // j$.util.stream.AbstractC0112r1
    public final L1 u() {
        switch (this.h) {
            case 0:
                return new C0136x1((AbstractC0022e) this.i);
            case 1:
                return new A1((BinaryOperator) this.i);
            case 2:
                return new G1((p) this.i);
            default:
                return new K1((z) this.i);
        }
    }
}
