package j$.util.stream;

import j$.util.AbstractC0037n;
import j$.util.E;
import java.util.Comparator;

/* loaded from: classes2.dex */
public abstract class q3 extends s3 implements E {
    public q3(E e, long j, long j2) {
        super(e, j, j2);
    }

    public q3(E e, q3 q3Var) {
        super(e, q3Var);
    }

    @Override // j$.util.E
    /* renamed from: forEachRemaining */
    public final void m(Object obj) {
        obj.getClass();
        U2 u2 = null;
        while (true) {
            int p = p();
            if (p == 1) {
                return;
            }
            if (p == 2) {
                if (u2 == null) {
                    u2 = r();
                } else {
                    u2.b = 0;
                }
                long j = 0;
                while (((E) this.a).tryAdvance(u2)) {
                    j++;
                    if (j >= 128) {
                        break;
                    }
                }
                if (j != 0) {
                    u2.a(obj, n(j));
                } else {
                    return;
                }
            } else {
                ((E) this.a).forEachRemaining(obj);
                return;
            }
        }
    }

    @Override // j$.util.Spliterator
    public final Comparator getComparator() {
        throw new IllegalStateException();
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ long getExactSizeIfKnown() {
        return AbstractC0037n.h(this);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean hasCharacteristics(int i) {
        return AbstractC0037n.j(this, i);
    }

    protected abstract void q(Object obj);

    protected abstract U2 r();

    @Override // j$.util.E
    /* renamed from: tryAdvance */
    public final boolean k(Object obj) {
        obj.getClass();
        while (p() != 1 && ((E) this.a).tryAdvance(this)) {
            if (n(1) == 1) {
                q(obj);
                return true;
            }
        }
        return false;
    }
}
