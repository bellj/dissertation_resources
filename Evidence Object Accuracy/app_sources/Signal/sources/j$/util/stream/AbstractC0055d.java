package j$.util.stream;

import j$.util.Spliterator;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: j$.util.stream.d */
/* loaded from: classes2.dex */
public abstract class AbstractC0055d extends AbstractC0063f {
    protected final AtomicReference h;
    protected volatile boolean i;

    public AbstractC0055d(AbstractC0055d dVar, Spliterator spliterator) {
        super(dVar, spliterator);
        this.h = dVar.h;
    }

    public AbstractC0055d(AbstractC0115s0 s0Var, Spliterator spliterator) {
        super(s0Var, spliterator);
        this.h = new AtomicReference(null);
    }

    @Override // j$.util.stream.AbstractC0063f
    public final Object b() {
        if (!(c() == null)) {
            return super.b();
        }
        Object obj = this.h.get();
        return obj == null ? i() : obj;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x006b, code lost:
        r8 = r7.a();
     */
    @Override // j$.util.stream.AbstractC0063f, java.util.concurrent.CountedCompleter
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void compute() {
        /*
            r10 = this;
            j$.util.Spliterator r0 = r10.b
            long r1 = r0.estimateSize()
            long r3 = r10.c
            r5 = 0
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x000f
            goto L_0x0015
        L_0x000f:
            long r3 = j$.util.stream.AbstractC0063f.f(r1)
            r10.c = r3
        L_0x0015:
            java.util.concurrent.atomic.AtomicReference r5 = r10.h
            r6 = 0
            r7 = r10
        L_0x0019:
            java.lang.Object r8 = r5.get()
            if (r8 != 0) goto L_0x006f
            boolean r8 = r7.i
            if (r8 != 0) goto L_0x0034
            j$.util.stream.f r9 = r7.c()
        L_0x0027:
            j$.util.stream.d r9 = (j$.util.stream.AbstractC0055d) r9
            if (r8 != 0) goto L_0x0034
            if (r9 == 0) goto L_0x0034
            boolean r8 = r9.i
            j$.util.stream.f r9 = r9.c()
            goto L_0x0027
        L_0x0034:
            if (r8 == 0) goto L_0x003b
            java.lang.Object r8 = r7.i()
            goto L_0x006f
        L_0x003b:
            int r8 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r8 <= 0) goto L_0x006b
            j$.util.Spliterator r1 = r0.trySplit()
            if (r1 != 0) goto L_0x0046
            goto L_0x006b
        L_0x0046:
            j$.util.stream.f r2 = r7.d(r1)
            j$.util.stream.d r2 = (j$.util.stream.AbstractC0055d) r2
            r7.d = r2
            j$.util.stream.f r8 = r7.d(r0)
            j$.util.stream.d r8 = (j$.util.stream.AbstractC0055d) r8
            r7.e = r8
            r9 = 1
            r7.setPendingCount(r9)
            if (r6 == 0) goto L_0x0060
            r0 = r1
            r7 = r2
            r2 = r8
            goto L_0x0061
        L_0x0060:
            r7 = r8
        L_0x0061:
            r6 = r6 ^ 1
            r2.fork()
            long r1 = r0.estimateSize()
            goto L_0x0019
        L_0x006b:
            java.lang.Object r8 = r7.a()
        L_0x006f:
            r7.e(r8)
            r7.tryComplete()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j$.util.stream.AbstractC0055d.compute():void");
    }

    @Override // j$.util.stream.AbstractC0063f
    public final void e(Object obj) {
        if (!(c() == null)) {
            super.e(obj);
        } else if (obj != null) {
            AtomicReference atomicReference = this.h;
            while (!atomicReference.compareAndSet(null, obj) && atomicReference.get() == null) {
            }
        }
    }

    protected void g() {
        this.i = true;
    }

    @Override // j$.util.stream.AbstractC0063f, java.util.concurrent.CountedCompleter, java.util.concurrent.ForkJoinTask
    public final Object getRawResult() {
        return b();
    }

    public final void h() {
        AbstractC0055d dVar = (AbstractC0055d) c();
        AbstractC0055d dVar2 = this;
        while (dVar != null) {
            if (dVar.d == dVar2) {
                AbstractC0055d dVar3 = (AbstractC0055d) dVar.e;
                if (!dVar3.i) {
                    dVar3.g();
                }
            }
            dVar = (AbstractC0055d) dVar.c();
            dVar2 = dVar;
        }
    }

    protected abstract Object i();
}
