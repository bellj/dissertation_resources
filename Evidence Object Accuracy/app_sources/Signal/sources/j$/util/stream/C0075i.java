package j$.util.stream;

import j$.util.function.BiConsumer;
import j$.util.function.BinaryOperator;
import j$.util.function.C0020c;
import j$.util.function.Function;
import j$.util.function.Supplier;
import java.util.Set;
import java.util.stream.Collector;

/* renamed from: j$.util.stream.i */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0075i implements Collector {
    final /* synthetic */ Collector a;

    private /* synthetic */ C0075i(Collector collector) {
        this.a = collector;
    }

    public static /* synthetic */ C0075i a(Collector collector) {
        if (collector == null) {
            return null;
        }
        return new C0075i(collector);
    }

    @Override // j$.util.stream.Collector
    public final /* synthetic */ BiConsumer accumulator() {
        return BiConsumer.VivifiedWrapper.convert(this.a.accumulator());
    }

    @Override // j$.util.stream.Collector
    public final /* synthetic */ Set characteristics() {
        return this.a.characteristics();
    }

    @Override // j$.util.stream.Collector
    public final /* synthetic */ BinaryOperator combiner() {
        java.util.function.BinaryOperator combiner = this.a.combiner();
        if (combiner == null) {
            return null;
        }
        return new C0020c(combiner);
    }

    @Override // j$.util.stream.Collector
    public final /* synthetic */ Function finisher() {
        return Function.VivifiedWrapper.convert(this.a.finisher());
    }

    @Override // j$.util.stream.Collector
    public final /* synthetic */ Supplier supplier() {
        java.util.function.Supplier supplier = this.a.supplier();
        if (supplier == null) {
            return null;
        }
        return new C0020c(supplier);
    }
}
