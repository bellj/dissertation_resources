package j$.util.stream;

import j$.util.AbstractC0037n;
import j$.util.E;
import j$.util.T;
import j$.util.function.AbstractC0024g;
import j$.util.function.Consumer;
import j$.util.y;

/* loaded from: classes2.dex */
public final class E2 extends K2 implements y {
    final /* synthetic */ F2 g;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public E2(F2 f2, int i, int i2, int i3, int i4) {
        super(f2, i, i2, i3, i4);
        this.g = f2;
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.l(this, consumer);
    }

    @Override // j$.util.stream.K2
    final void d(int i, Object obj, Object obj2) {
        ((AbstractC0024g) obj2).accept(((double[]) obj)[i]);
    }

    @Override // j$.util.stream.K2
    final E e(Object obj, int i, int i2) {
        return T.j((double[]) obj, i, i2 + i);
    }

    @Override // j$.util.stream.K2
    final E f(int i, int i2, int i3, int i4) {
        return new E2(this.g, i, i2, i3, i4);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.d(this, consumer);
    }
}
