package j$.util.stream;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public abstract class D0 implements B0 {
    protected final B0 a;
    protected final B0 b;
    private final long c;

    public D0(B0 b0, B0 b02) {
        this.a = b0;
        this.b = b02;
        this.c = b0.count() + b02.count();
    }

    @Override // j$.util.stream.B0
    public final B0 a(int i) {
        if (i == 0) {
            return this.a;
        }
        if (i == 1) {
            return this.b;
        }
        throw new IndexOutOfBoundsException();
    }

    @Override // j$.util.stream.B0
    public final long count() {
        return this.c;
    }

    @Override // j$.util.stream.B0
    public final int k() {
        return 2;
    }
}
