package j$.util.stream;

import j$.util.Spliterator;
import j$.util.function.IntFunction;

/* renamed from: j$.util.stream.m2 */
/* loaded from: classes2.dex */
final class C0094m2 extends AbstractC0055d {
    private final AbstractC0051c j;
    private final IntFunction k;
    private final long l;
    private final long m;
    private long n;
    private volatile boolean o;

    public C0094m2(AbstractC0051c cVar, AbstractC0051c cVar2, Spliterator spliterator, IntFunction intFunction, long j, long j2) {
        super(cVar2, spliterator);
        this.j = cVar;
        this.k = intFunction;
        this.l = j;
        this.m = j2;
    }

    C0094m2(C0094m2 m2Var, Spliterator spliterator) {
        super(m2Var, spliterator);
        this.j = m2Var.j;
        this.k = m2Var.k;
        this.l = m2Var.l;
        this.m = m2Var.m;
    }

    private long j(long j) {
        if (this.o) {
            return this.n;
        }
        C0094m2 m2Var = (C0094m2) this.d;
        C0094m2 m2Var2 = (C0094m2) this.e;
        if (m2Var == null || m2Var2 == null) {
            return this.n;
        }
        long j2 = m2Var.j(j);
        return j2 >= j ? j2 : j2 + m2Var2.j(j);
    }

    @Override // j$.util.stream.AbstractC0063f
    public final Object a() {
        long j = -1;
        if (c() == null) {
            if (P2.SIZED.f(this.j.c)) {
                j = this.j.d0(this.b);
            }
            AbstractC0131w0 o0 = this.j.o0(j, this.k);
            AbstractC0054c2 G0 = this.j.G0(this.a.f0(), o0);
            AbstractC0115s0 s0Var = this.a;
            s0Var.b0(this.b, s0Var.t0(G0));
            return o0.mo149build();
        }
        AbstractC0115s0 s0Var2 = this.a;
        AbstractC0131w0 o02 = s0Var2.o0(-1, this.k);
        s0Var2.s0(this.b, o02);
        B0 build = o02.mo149build();
        this.n = build.count();
        this.o = true;
        this.b = null;
        return build;
    }

    @Override // j$.util.stream.AbstractC0063f
    public final AbstractC0063f d(Spliterator spliterator) {
        return new C0094m2(this, spliterator);
    }

    @Override // j$.util.stream.AbstractC0055d
    protected final void g() {
        this.i = true;
        if (this.o) {
            e(i());
        }
    }

    /* renamed from: k */
    public final U0 i() {
        return AbstractC0112r1.n(this.j.z0());
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x006c  */
    @Override // j$.util.stream.AbstractC0063f, java.util.concurrent.CountedCompleter
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onCompletion(java.util.concurrent.CountedCompleter r15) {
        /*
        // Method dump skipped, instructions count: 236
        */
        throw new UnsupportedOperationException("Method not decompiled: j$.util.stream.C0094m2.onCompletion(java.util.concurrent.CountedCompleter):void");
    }
}
