package j$.util.stream;

import j$.util.Spliterator;
import j$.util.function.IntFunction;

/* renamed from: j$.util.stream.e2 */
/* loaded from: classes2.dex */
public final class C0062e2 extends S1 {
    final /* synthetic */ long m;
    final /* synthetic */ long n;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0062e2(AbstractC0051c cVar, int i, long j, long j2) {
        super(cVar, i);
        this.m = j;
        this.n = j2;
    }

    static Spliterator L0(Spliterator spliterator, long j, long j2, long j3) {
        long j4;
        long j5;
        if (j <= j3) {
            long j6 = j3 - j;
            j4 = j2 >= 0 ? Math.min(j2, j6) : j6;
            j5 = 0;
        } else {
            j5 = j;
            j4 = j2;
        }
        return new r3(spliterator, j5, j4);
    }

    @Override // j$.util.stream.AbstractC0051c
    final B0 D0(Spliterator spliterator, IntFunction intFunction, AbstractC0051c cVar) {
        long d0 = cVar.d0(spliterator);
        return (d0 <= 0 || !spliterator.hasCharacteristics(16384)) ? !P2.ORDERED.e(cVar.f0()) ? AbstractC0112r1.h(this, L0(cVar.K0(spliterator), this.m, this.n, d0), true, intFunction) : (B0) new C0094m2(this, cVar, spliterator, intFunction, this.m, this.n).invoke() : AbstractC0112r1.h(cVar, AbstractC0115s0.Y(cVar.A0(), spliterator, this.m, this.n), true, intFunction);
    }

    @Override // j$.util.stream.AbstractC0051c
    final Spliterator E0(AbstractC0051c cVar, Spliterator spliterator) {
        long d0 = cVar.d0(spliterator);
        if (d0 <= 0 || !spliterator.hasCharacteristics(16384)) {
            return !P2.ORDERED.e(cVar.f0()) ? L0(cVar.K0(spliterator), this.m, this.n, d0) : ((B0) new C0094m2(this, cVar, spliterator, new C0052c0(10), this.m, this.n).invoke()).mo148spliterator();
        }
        Spliterator K0 = cVar.K0(spliterator);
        long j = this.m;
        return new l3(K0, j, AbstractC0115s0.X(j, this.n));
    }

    @Override // j$.util.stream.AbstractC0051c
    public final AbstractC0054c2 G0(int i, AbstractC0054c2 c2Var) {
        return new C0058d2(this, c2Var);
    }
}
