package j$.util.stream;

import j$.util.A;
import j$.util.E;
import j$.util.Spliterator;
import j$.util.function.Consumer;
import j$.util.function.IntFunction;
import j$.util.function.r;

/* loaded from: classes2.dex */
public final class Y0 extends H2 implements AbstractC0139y0, AbstractC0123u0 {
    @Override // j$.util.stream.A0, j$.util.stream.B0
    public final A0 a(int i) {
        throw new IndexOutOfBoundsException();
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(double d) {
        AbstractC0115s0.D();
        throw null;
    }

    @Override // j$.util.stream.H2, j$.util.function.r
    public final void accept(int i) {
        super.accept(i);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(long j) {
        AbstractC0115s0.L();
        throw null;
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        e((Integer) obj);
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // j$.util.stream.L2, j$.util.stream.A0
    public final Object b() {
        return (int[]) super.b();
    }

    @Override // j$.util.stream.AbstractC0123u0, j$.util.stream.AbstractC0131w0
    /* renamed from: build */
    public final B0 mo149build() {
        return this;
    }

    @Override // j$.util.stream.AbstractC0123u0, j$.util.stream.AbstractC0131w0
    /* renamed from: build */
    public final AbstractC0139y0 mo149build() {
        return this;
    }

    @Override // j$.util.stream.L2, j$.util.stream.A0
    public final void c(int i, Object obj) {
        super.c(i, (int[]) obj);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        clear();
        r(j);
    }

    @Override // j$.util.stream.AbstractC0045a2
    public final /* synthetic */ void e(Integer num) {
        AbstractC0115s0.G(this, num);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void end() {
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ boolean f() {
        return false;
    }

    @Override // j$.util.stream.L2, j$.util.stream.A0
    public final void g(Object obj) {
        super.g((r) obj);
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ int k() {
        return 0;
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ Object[] m(IntFunction intFunction) {
        return AbstractC0115s0.M(this, intFunction);
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ B0 n(long j, long j2, IntFunction intFunction) {
        return AbstractC0115s0.U(this, j, j2);
    }

    @Override // j$.util.stream.H2, j$.util.stream.L2, java.lang.Iterable, j$.lang.Iterable
    /* renamed from: spliterator */
    public final E mo150spliterator() {
        return super.mo150spliterator();
    }

    @Override // j$.util.stream.H2, j$.util.stream.L2, java.lang.Iterable, j$.lang.Iterable
    /* renamed from: spliterator */
    public final Spliterator mo150spliterator() {
        return super.mo150spliterator();
    }

    @Override // j$.util.stream.H2
    public final A u() {
        return super.mo150spliterator();
    }

    /* renamed from: v */
    public final /* synthetic */ void i(Integer[] numArr, int i) {
        AbstractC0115s0.O(this, numArr, i);
    }
}
