package j$.util.stream;

import j$.util.E;
import j$.util.Spliterator;
import j$.util.T;
import j$.util.function.Consumer;
import j$.util.function.IntFunction;

/* loaded from: classes2.dex */
final class Q0 extends U0 implements AbstractC0135x0 {
    @Override // j$.util.stream.U0, j$.util.stream.B0
    public final A0 a(int i) {
        throw new IndexOutOfBoundsException();
    }

    @Override // j$.util.stream.A0
    public final Object b() {
        return AbstractC0112r1.g;
    }

    /* renamed from: d */
    public final /* synthetic */ void i(Double[] dArr, int i) {
        AbstractC0115s0.N(this, dArr, i);
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ void forEach(Consumer consumer) {
        AbstractC0115s0.Q(this, consumer);
    }

    @Override // j$.util.stream.U0, j$.util.stream.B0
    public final /* synthetic */ B0 n(long j, long j2, IntFunction intFunction) {
        return AbstractC0115s0.T(this, j, j2);
    }

    @Override // j$.util.stream.B0
    /* renamed from: spliterator */
    public final E mo148spliterator() {
        return T.b();
    }

    @Override // j$.util.stream.B0
    /* renamed from: spliterator */
    public final Spliterator mo148spliterator() {
        return T.b();
    }
}
