package j$.util.stream;

import java.util.Arrays;

/* loaded from: classes2.dex */
final class A2 extends AbstractC0102o2 {
    private int[] c;
    private int d;

    public A2(AbstractC0054c2 c2Var) {
        super(c2Var);
    }

    @Override // j$.util.stream.AbstractC0045a2, j$.util.stream.AbstractC0054c2
    public final void accept(int i) {
        int[] iArr = this.c;
        int i2 = this.d;
        this.d = i2 + 1;
        iArr[i2] = i;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        if (j < 2147483639) {
            this.c = new int[(int) j];
            return;
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    @Override // j$.util.stream.W1, j$.util.stream.AbstractC0054c2
    public final void end() {
        int i = 0;
        Arrays.sort(this.c, 0, this.d);
        this.a.d((long) this.d);
        if (!this.b) {
            while (i < this.d) {
                this.a.accept(this.c[i]);
                i++;
            }
        } else {
            while (i < this.d && !this.a.f()) {
                this.a.accept(this.c[i]);
                i++;
            }
        }
        this.a.end();
        this.c = null;
    }
}
