package j$.util.stream;

import j$.util.function.AbstractC0024g;

/* loaded from: classes2.dex */
final class R2 extends U2 implements AbstractC0024g {
    final double[] c = new double[128];

    @Override // j$.util.stream.U2
    public final void a(Object obj, long j) {
        AbstractC0024g gVar = (AbstractC0024g) obj;
        for (int i = 0; ((long) i) < j; i++) {
            gVar.accept(this.c[i]);
        }
    }

    @Override // j$.util.function.AbstractC0024g
    public final void accept(double d) {
        double[] dArr = this.c;
        int i = this.b;
        this.b = i + 1;
        dArr[i] = d;
    }
}
