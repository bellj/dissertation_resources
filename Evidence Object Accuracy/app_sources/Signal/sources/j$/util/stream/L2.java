package j$.util.stream;

import j$.lang.Iterable;
import j$.util.Spliterator;
import java.util.Arrays;

/* loaded from: classes2.dex */
public abstract class L2 extends AbstractC0059e implements Iterable, Iterable {
    Object e = newArray(16);
    Object[] f;

    public L2() {
    }

    public L2(int i) {
        super(i);
    }

    @Override // j$.util.stream.A0
    public Object b() {
        long count = count();
        if (count < 2147483639) {
            Object newArray = newArray((int) count);
            c(0, newArray);
            return newArray;
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    @Override // j$.util.stream.A0
    public void c(int i, Object obj) {
        long j = (long) i;
        long count = count() + j;
        if (count > ((long) p(obj)) || count < j) {
            throw new IndexOutOfBoundsException("does not fit");
        } else if (this.c == 0) {
            System.arraycopy(this.e, 0, obj, i, this.b);
        } else {
            for (int i2 = 0; i2 < this.c; i2++) {
                Object obj2 = this.f[i2];
                System.arraycopy(obj2, 0, obj, i, p(obj2));
                i += p(this.f[i2]);
            }
            int i3 = this.b;
            if (i3 > 0) {
                System.arraycopy(this.e, 0, obj, i, i3);
            }
        }
    }

    @Override // j$.util.stream.AbstractC0059e
    public final void clear() {
        Object[] objArr = this.f;
        if (objArr != null) {
            this.e = objArr[0];
            this.f = null;
            this.d = null;
        }
        this.b = 0;
        this.c = 0;
    }

    @Override // j$.util.stream.A0
    public void g(Object obj) {
        for (int i = 0; i < this.c; i++) {
            Object obj2 = this.f[i];
            o(obj2, 0, p(obj2), obj);
        }
        o(this.e, 0, this.b, obj);
    }

    public abstract Object newArray(int i);

    /* access modifiers changed from: protected */
    public abstract void o(Object obj, int i, int i2, Object obj2);

    /* access modifiers changed from: protected */
    public abstract int p(Object obj);

    public final int q(long j) {
        if (this.c == 0) {
            if (j < ((long) this.b)) {
                return 0;
            }
            throw new IndexOutOfBoundsException(Long.toString(j));
        } else if (j < count()) {
            for (int i = 0; i <= this.c; i++) {
                if (j < this.d[i] + ((long) p(this.f[i]))) {
                    return i;
                }
            }
            throw new IndexOutOfBoundsException(Long.toString(j));
        } else {
            throw new IndexOutOfBoundsException(Long.toString(j));
        }
    }

    public final void r(long j) {
        int i = this.c;
        long p = i == 0 ? (long) p(this.e) : ((long) p(this.f[i])) + this.d[i];
        if (j > p) {
            if (this.f == null) {
                Object[] s = s();
                this.f = s;
                this.d = new long[8];
                s[0] = this.e;
            }
            int i2 = this.c;
            while (true) {
                i2++;
                if (j > p) {
                    Object[] objArr = this.f;
                    if (i2 >= objArr.length) {
                        int length = objArr.length * 2;
                        this.f = Arrays.copyOf(objArr, length);
                        this.d = Arrays.copyOf(this.d, length);
                    }
                    int min = 1 << ((i2 == 0 || i2 == 1) ? this.a : Math.min((this.a + i2) - 1, 30));
                    this.f[i2] = newArray(min);
                    long[] jArr = this.d;
                    int i3 = i2 - 1;
                    jArr[i2] = jArr[i3] + ((long) p(this.f[i3]));
                    p += (long) min;
                } else {
                    return;
                }
            }
        }
    }

    protected abstract Object[] s();

    @Override // java.lang.Iterable, j$.lang.Iterable
    /* renamed from: spliterator */
    public abstract Spliterator mo150spliterator();

    public final void t() {
        long j;
        if (this.b == p(this.e)) {
            if (this.f == null) {
                Object[] s = s();
                this.f = s;
                this.d = new long[8];
                s[0] = this.e;
            }
            int i = this.c;
            int i2 = i + 1;
            Object[] objArr = this.f;
            if (i2 >= objArr.length || objArr[i2] == null) {
                if (i == 0) {
                    j = (long) p(this.e);
                } else {
                    j = ((long) p(objArr[i])) + this.d[i];
                }
                r(j + 1);
            }
            this.b = 0;
            int i3 = this.c + 1;
            this.c = i3;
            this.e = this.f[i3];
        }
    }
}
