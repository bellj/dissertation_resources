package j$.util.stream;

import j$.util.Spliterator;

/* renamed from: j$.util.stream.i1 */
/* loaded from: classes2.dex */
public final class C0077i1 extends AbstractC0093m1 implements Z1 {
    private final double[] h;

    public C0077i1(Spliterator spliterator, AbstractC0115s0 s0Var, double[] dArr) {
        super(dArr.length, spliterator, s0Var);
        this.h = dArr;
    }

    C0077i1(C0077i1 i1Var, Spliterator spliterator, long j, long j2) {
        super(i1Var, spliterator, j, j2, i1Var.h.length);
        this.h = i1Var.h;
    }

    @Override // j$.util.stream.AbstractC0093m1
    final AbstractC0093m1 a(Spliterator spliterator, long j, long j2) {
        return new C0077i1(this, spliterator, j, j2);
    }

    @Override // j$.util.stream.AbstractC0093m1, j$.util.stream.AbstractC0054c2
    public final void accept(double d) {
        int i = this.f;
        if (i < this.g) {
            double[] dArr = this.h;
            this.f = i + 1;
            dArr[i] = d;
            return;
        }
        throw new IndexOutOfBoundsException(Integer.toString(this.f));
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        l((Double) obj);
    }

    @Override // j$.util.stream.Z1
    public final /* synthetic */ void l(Double d) {
        AbstractC0115s0.E(this, d);
    }
}
