package j$.util.stream;

import j$.util.Spliterator;

/* renamed from: j$.util.stream.j1 */
/* loaded from: classes2.dex */
public final class C0081j1 extends AbstractC0093m1 implements AbstractC0045a2 {
    private final int[] h;

    public C0081j1(Spliterator spliterator, AbstractC0115s0 s0Var, int[] iArr) {
        super(iArr.length, spliterator, s0Var);
        this.h = iArr;
    }

    C0081j1(C0081j1 j1Var, Spliterator spliterator, long j, long j2) {
        super(j1Var, spliterator, j, j2, j1Var.h.length);
        this.h = j1Var.h;
    }

    @Override // j$.util.stream.AbstractC0093m1
    final AbstractC0093m1 a(Spliterator spliterator, long j, long j2) {
        return new C0081j1(this, spliterator, j, j2);
    }

    @Override // j$.util.stream.AbstractC0093m1, j$.util.stream.AbstractC0054c2
    public final void accept(int i) {
        int i2 = this.f;
        if (i2 < this.g) {
            int[] iArr = this.h;
            this.f = i2 + 1;
            iArr[i2] = i;
            return;
        }
        throw new IndexOutOfBoundsException(Integer.toString(this.f));
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        e((Integer) obj);
    }

    @Override // j$.util.stream.AbstractC0045a2
    public final /* synthetic */ void e(Integer num) {
        AbstractC0115s0.G(this, num);
    }
}
