package j$.util.stream;

import j$.util.Spliterator;
import j$.util.function.A;
import j$.util.function.B;

/* renamed from: j$.util.stream.e0 */
/* loaded from: classes2.dex */
public final class C0060e0 extends AbstractC0072h0 {
    public C0060e0(Spliterator spliterator, int i) {
        super(spliterator, i);
    }

    @Override // j$.util.stream.AbstractC0051c
    final boolean F0() {
        throw new UnsupportedOperationException();
    }

    @Override // j$.util.stream.AbstractC0051c
    public final AbstractC0054c2 G0(int i, AbstractC0054c2 c2Var) {
        throw new UnsupportedOperationException();
    }

    @Override // j$.util.stream.AbstractC0072h0, j$.util.stream.AbstractC0080j0
    public final void o(A a) {
        if (!isParallel()) {
            AbstractC0072h0.M0(I0()).b(a);
        } else {
            super.o(a);
        }
    }

    @Override // j$.util.stream.AbstractC0072h0, j$.util.stream.AbstractC0080j0
    public final void p(B b) {
        if (!isParallel()) {
            AbstractC0072h0.M0(I0()).b(b);
        } else {
            super.p(b);
        }
    }

    @Override // j$.util.stream.AbstractC0051c, j$.util.stream.AbstractC0071h
    public final /* bridge */ /* synthetic */ AbstractC0080j0 parallel() {
        parallel();
        return this;
    }

    @Override // j$.util.stream.AbstractC0051c, j$.util.stream.AbstractC0071h
    public final /* bridge */ /* synthetic */ AbstractC0080j0 sequential() {
        sequential();
        return this;
    }
}
