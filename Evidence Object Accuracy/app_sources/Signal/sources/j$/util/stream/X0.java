package j$.util.stream;

import j$.util.function.Consumer;
import java.util.Arrays;

/* loaded from: classes2.dex */
public final class X0 extends W0 implements AbstractC0123u0 {
    public X0(long j) {
        super(j);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(double d) {
        AbstractC0115s0.D();
        throw null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void accept(int i) {
        int i2 = this.b;
        int[] iArr = this.a;
        if (i2 < iArr.length) {
            this.b = i2 + 1;
            iArr[i2] = i;
            return;
        }
        throw new IllegalStateException(String.format("Accept exceeded fixed size of %d", Integer.valueOf(this.a.length)));
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(long j) {
        AbstractC0115s0.L();
        throw null;
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        e((Integer) obj);
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // j$.util.stream.AbstractC0123u0, j$.util.stream.AbstractC0131w0
    /* renamed from: build */
    public final AbstractC0139y0 mo149build() {
        if (this.b >= this.a.length) {
            return this;
        }
        throw new IllegalStateException(String.format("Current size %d is less than fixed size %d", Integer.valueOf(this.b), Integer.valueOf(this.a.length)));
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        if (j == ((long) this.a.length)) {
            this.b = 0;
            return;
        }
        throw new IllegalStateException(String.format("Begin size %d is not equal to fixed size %d", Long.valueOf(j), Integer.valueOf(this.a.length)));
    }

    @Override // j$.util.stream.AbstractC0045a2
    public final /* synthetic */ void e(Integer num) {
        AbstractC0115s0.G(this, num);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void end() {
        if (this.b < this.a.length) {
            throw new IllegalStateException(String.format("End size %d is less than fixed size %d", Integer.valueOf(this.b), Integer.valueOf(this.a.length)));
        }
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ boolean f() {
        return false;
    }

    @Override // j$.util.stream.W0
    public final String toString() {
        return String.format("IntFixedNodeBuilder[%d][%s]", Integer.valueOf(this.a.length - this.b), Arrays.toString(this.a));
    }
}
