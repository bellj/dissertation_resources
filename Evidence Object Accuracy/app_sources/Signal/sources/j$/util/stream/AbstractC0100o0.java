package j$.util.stream;

import j$.util.function.Consumer;

/* renamed from: j$.util.stream.o0 */
/* loaded from: classes2.dex */
public abstract class AbstractC0100o0 implements AbstractC0054c2 {
    boolean a;
    boolean b;

    public AbstractC0100o0(EnumC0104p0 p0Var) {
        this.b = !p0Var.b;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public /* synthetic */ void accept(double d) {
        AbstractC0115s0.D();
        throw null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public /* synthetic */ void accept(int i) {
        AbstractC0115s0.K();
        throw null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public /* synthetic */ void accept(long j) {
        AbstractC0115s0.L();
        throw null;
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void d(long j) {
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void end() {
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final boolean f() {
        return this.a;
    }
}
