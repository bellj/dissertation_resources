package j$.util.stream;

import j$.util.AbstractC0037n;
import j$.util.C;
import j$.util.function.Consumer;

/* renamed from: j$.util.stream.b1 */
/* loaded from: classes2.dex */
final class C0049b1 extends AbstractC0053c1 implements C {
    public C0049b1(AbstractC0143z0 z0Var) {
        super(z0Var);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.o(this, consumer);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.f(this, consumer);
    }
}
