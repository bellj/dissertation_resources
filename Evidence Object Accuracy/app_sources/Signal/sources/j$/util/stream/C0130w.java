package j$.util.stream;

/* renamed from: j$.util.stream.w */
/* loaded from: classes2.dex */
public final class C0130w extends AbstractC0142z {
    public final /* synthetic */ int m;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ C0130w(AbstractC0051c cVar, int i, int i2) {
        super(cVar, i);
        this.m = i2;
    }

    @Override // j$.util.stream.AbstractC0051c
    public final AbstractC0054c2 G0(int i, AbstractC0054c2 c2Var) {
        switch (this.m) {
            case 0:
                return c2Var;
            case 1:
                return new T(this, c2Var, 2);
            default:
                return new C0056d0(this, c2Var, 0);
        }
    }
}
