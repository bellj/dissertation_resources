package j$.util.stream;

import j$.util.Spliterator;
import j$.util.function.IntFunction;
import java.util.Arrays;

/* renamed from: j$.util.stream.u2 */
/* loaded from: classes2.dex */
final class C0125u2 extends AbstractC0138y {
    public C0125u2(A a) {
        super(a, P2.q | P2.o);
    }

    @Override // j$.util.stream.AbstractC0051c
    public final B0 D0(Spliterator spliterator, IntFunction intFunction, AbstractC0051c cVar) {
        if (P2.SORTED.e(cVar.f0())) {
            return cVar.u0(spliterator, false, intFunction);
        }
        double[] dArr = (double[]) ((AbstractC0135x0) cVar.u0(spliterator, true, intFunction)).b();
        Arrays.sort(dArr);
        return new N0(dArr);
    }

    @Override // j$.util.stream.AbstractC0051c
    public final AbstractC0054c2 G0(int i, AbstractC0054c2 c2Var) {
        c2Var.getClass();
        return P2.SORTED.e(i) ? c2Var : P2.SIZED.e(i) ? new C0145z2(c2Var) : new C0113r2(c2Var);
    }
}
