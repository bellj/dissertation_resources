package j$.util.stream;

/* renamed from: j$.util.stream.p1 */
/* loaded from: classes2.dex */
public class C0105p1 extends AbstractC0109q1 {
    public final /* synthetic */ int c;
    private final Object d;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ C0105p1(A0 a0, Object obj) {
        this(a0, obj, 0);
        this.c = 0;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ C0105p1(B0 b0, Object obj, int i) {
        super(b0);
        this.c = i;
        this.d = obj;
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ C0105p1(B0 b0, Object[] objArr) {
        this(b0, objArr, 1);
        this.c = 1;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0105p1(C0105p1 p1Var, A0 a0, int i) {
        super(p1Var, a0, i);
        this.c = 0;
        this.d = p1Var.d;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0105p1(C0105p1 p1Var, B0 b0, int i) {
        super(p1Var, b0, i);
        this.c = 1;
        this.d = (Object[]) p1Var.d;
    }

    @Override // j$.util.stream.AbstractC0109q1
    final void a() {
        switch (this.c) {
            case 0:
                ((A0) this.a).c(this.b, this.d);
                return;
            default:
                this.a.i((Object[]) this.d, this.b);
                return;
        }
    }

    @Override // j$.util.stream.AbstractC0109q1
    final C0105p1 b(int i, int i2) {
        switch (this.c) {
            case 0:
                return new C0105p1(this, ((A0) this.a).a(i), i2);
            default:
                return new C0105p1(this, this.a.a(i), i2);
        }
    }
}
