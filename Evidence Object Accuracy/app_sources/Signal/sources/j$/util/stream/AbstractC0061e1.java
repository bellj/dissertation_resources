package j$.util.stream;

import j$.util.AbstractC0037n;
import j$.util.Spliterator;
import java.util.ArrayDeque;
import java.util.Comparator;

/* renamed from: j$.util.stream.e1 */
/* loaded from: classes2.dex */
public abstract class AbstractC0061e1 implements Spliterator {
    B0 a;
    int b;
    Spliterator c;
    Spliterator d;
    ArrayDeque e;

    public AbstractC0061e1(B0 b0) {
        this.a = b0;
    }

    public static B0 d(ArrayDeque arrayDeque) {
        while (true) {
            B0 b0 = (B0) arrayDeque.pollFirst();
            if (b0 == null) {
                return null;
            }
            if (b0.k() != 0) {
                int k = b0.k();
                while (true) {
                    k--;
                    if (k >= 0) {
                        arrayDeque.addFirst(b0.a(k));
                    }
                }
            } else if (b0.count() > 0) {
                return b0;
            }
        }
    }

    @Override // j$.util.Spliterator
    public final int characteristics() {
        return 64;
    }

    public final ArrayDeque e() {
        ArrayDeque arrayDeque = new ArrayDeque(8);
        int k = this.a.k();
        while (true) {
            k--;
            if (k < this.b) {
                return arrayDeque;
            }
            arrayDeque.addFirst(this.a.a(k));
        }
    }

    @Override // j$.util.Spliterator
    public final long estimateSize() {
        long j = 0;
        if (this.a == null) {
            return 0;
        }
        Spliterator spliterator = this.c;
        if (spliterator != null) {
            return spliterator.estimateSize();
        }
        for (int i = this.b; i < this.a.k(); i++) {
            j += this.a.a(i).count();
        }
        return j;
    }

    public final boolean f() {
        if (this.a == null) {
            return false;
        }
        if (this.d != null) {
            return true;
        }
        Spliterator spliterator = this.c;
        if (spliterator == null) {
            ArrayDeque e = e();
            this.e = e;
            B0 d = d(e);
            if (d != null) {
                spliterator = d.mo148spliterator();
            } else {
                this.a = null;
                return false;
            }
        }
        this.d = spliterator;
        return true;
    }

    @Override // j$.util.Spliterator
    public final Comparator getComparator() {
        throw new IllegalStateException();
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ long getExactSizeIfKnown() {
        return AbstractC0037n.h(this);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean hasCharacteristics(int i) {
        return AbstractC0037n.j(this, i);
    }

    @Override // j$.util.Spliterator
    public final Spliterator trySplit() {
        B0 b0 = this.a;
        if (b0 == null || this.d != null) {
            return null;
        }
        Spliterator spliterator = this.c;
        if (spliterator != null) {
            return spliterator.trySplit();
        }
        if (this.b < b0.k() - 1) {
            B0 b02 = this.a;
            int i = this.b;
            this.b = i + 1;
            return b02.a(i).mo148spliterator();
        }
        B0 a = this.a.a(this.b);
        this.a = a;
        if (a.k() == 0) {
            Spliterator spliterator2 = this.a.mo148spliterator();
            this.c = spliterator2;
            return spliterator2.trySplit();
        }
        B0 b03 = this.a;
        this.b = 0 + 1;
        return b03.a(0).mo148spliterator();
    }
}
