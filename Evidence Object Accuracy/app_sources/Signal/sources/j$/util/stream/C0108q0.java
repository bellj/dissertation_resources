package j$.util.stream;

import j$.util.Spliterator;
import j$.util.function.Supplier;

/* renamed from: j$.util.stream.q0 */
/* loaded from: classes2.dex */
public final class C0108q0 implements x3 {
    final EnumC0104p0 a;
    final Supplier b;

    public C0108q0(int i, EnumC0104p0 p0Var, C0083k kVar) {
        this.a = p0Var;
        this.b = kVar;
    }

    @Override // j$.util.stream.x3
    public final Object a(AbstractC0115s0 s0Var, Spliterator spliterator) {
        return (Boolean) new C0111r0(this, s0Var, spliterator).invoke();
    }

    @Override // j$.util.stream.x3
    public final int b() {
        return P2.u | P2.r;
    }

    @Override // j$.util.stream.x3
    public final Object c(AbstractC0115s0 s0Var, Spliterator spliterator) {
        return Boolean.valueOf(((AbstractC0100o0) s0Var.s0(spliterator, (AbstractC0100o0) this.b.get())).b);
    }
}
