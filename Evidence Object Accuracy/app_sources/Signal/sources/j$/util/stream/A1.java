package j$.util.stream;

import j$.util.Optional;
import j$.util.function.BinaryOperator;
import j$.util.function.Consumer;

/* loaded from: classes2.dex */
final class A1 implements L1 {
    private boolean a;
    private Object b;
    final /* synthetic */ BinaryOperator c;

    public A1(BinaryOperator binaryOperator) {
        this.c = binaryOperator;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(double d) {
        AbstractC0115s0.D();
        throw null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(int i) {
        AbstractC0115s0.K();
        throw null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(long j) {
        AbstractC0115s0.L();
        throw null;
    }

    @Override // j$.util.function.Consumer
    public final void accept(Object obj) {
        if (this.a) {
            this.a = false;
        } else {
            obj = this.c.apply(this.b, obj);
        }
        this.b = obj;
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        this.a = true;
        this.b = null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void end() {
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ boolean f() {
        return false;
    }

    @Override // j$.util.function.Supplier
    public final Object get() {
        return this.a ? Optional.empty() : Optional.of(this.b);
    }

    @Override // j$.util.stream.L1
    public final void h(L1 l1) {
        A1 a1 = (A1) l1;
        if (!a1.a) {
            accept(a1.b);
        }
    }
}
