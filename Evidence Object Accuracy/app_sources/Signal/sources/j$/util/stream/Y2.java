package j$.util.stream;

import j$.util.function.AbstractC0024g;
import j$.util.function.Consumer;

/* loaded from: classes2.dex */
public final /* synthetic */ class Y2 implements Z1 {
    public final /* synthetic */ int a;
    public final /* synthetic */ AbstractC0024g b;

    public /* synthetic */ Y2(AbstractC0024g gVar, int i) {
        this.a = i;
        this.b = gVar;
    }

    @Override // j$.util.stream.Z1, j$.util.stream.AbstractC0054c2
    public final void accept(double d) {
        switch (this.a) {
            case 0:
                ((F2) this.b).accept(d);
                return;
            default:
                this.b.accept(d);
                return;
        }
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(int i) {
        switch (this.a) {
            case 0:
                AbstractC0115s0.K();
                throw null;
            default:
                AbstractC0115s0.K();
                throw null;
        }
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(long j) {
        switch (this.a) {
            case 0:
                AbstractC0115s0.L();
                throw null;
            default:
                AbstractC0115s0.L();
                throw null;
        }
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        switch (this.a) {
            case 0:
                l((Double) obj);
                return;
            default:
                l((Double) obj);
                return;
        }
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        switch (this.a) {
            case 0:
                return Consumer.CC.$default$andThen(this, consumer);
            default:
                return Consumer.CC.$default$andThen(this, consumer);
        }
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void d(long j) {
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void end() {
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ boolean f() {
        return false;
    }

    @Override // j$.util.stream.Z1
    public final /* synthetic */ void l(Double d) {
        switch (this.a) {
            case 0:
                AbstractC0115s0.E(this, d);
                return;
            default:
                AbstractC0115s0.E(this, d);
                return;
        }
    }
}
