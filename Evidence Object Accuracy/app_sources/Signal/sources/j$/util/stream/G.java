package j$.util.stream;

import j$.util.C0035l;

/* loaded from: classes2.dex */
final class G extends I implements AbstractC0050b2 {
    @Override // j$.util.stream.I, j$.util.stream.AbstractC0054c2
    public final void accept(long j) {
        l(Long.valueOf(j));
    }

    @Override // j$.util.function.Supplier
    public final Object get() {
        if (this.a) {
            return C0035l.d(((Long) this.b).longValue());
        }
        return null;
    }
}
