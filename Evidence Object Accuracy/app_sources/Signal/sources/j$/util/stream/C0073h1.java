package j$.util.stream;

import j$.util.C;
import j$.util.E;
import j$.util.Spliterator;
import j$.util.function.B;
import j$.util.function.Consumer;
import j$.util.function.IntFunction;

/* renamed from: j$.util.stream.h1 */
/* loaded from: classes2.dex */
public final class C0073h1 extends J2 implements AbstractC0143z0, AbstractC0127v0 {
    @Override // j$.util.stream.A0, j$.util.stream.B0
    public final A0 a(int i) {
        throw new IndexOutOfBoundsException();
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(double d) {
        AbstractC0115s0.D();
        throw null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(int i) {
        AbstractC0115s0.K();
        throw null;
    }

    @Override // j$.util.stream.J2, j$.util.function.B
    public final void accept(long j) {
        super.accept(j);
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        j((Long) obj);
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // j$.util.stream.L2, j$.util.stream.A0
    public final Object b() {
        return (long[]) super.b();
    }

    @Override // j$.util.stream.AbstractC0127v0, j$.util.stream.AbstractC0131w0
    /* renamed from: build */
    public final B0 mo149build() {
        return this;
    }

    @Override // j$.util.stream.AbstractC0127v0, j$.util.stream.AbstractC0131w0
    /* renamed from: build */
    public final AbstractC0143z0 mo149build() {
        return this;
    }

    @Override // j$.util.stream.L2, j$.util.stream.A0
    public final void c(int i, Object obj) {
        super.c(i, (long[]) obj);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        clear();
        r(j);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void end() {
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ boolean f() {
        return false;
    }

    @Override // j$.util.stream.L2, j$.util.stream.A0
    public final void g(Object obj) {
        super.g((B) obj);
    }

    @Override // j$.util.stream.AbstractC0050b2
    public final /* synthetic */ void j(Long l) {
        AbstractC0115s0.I(this, l);
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ int k() {
        return 0;
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ Object[] m(IntFunction intFunction) {
        return AbstractC0115s0.M(this, intFunction);
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ B0 n(long j, long j2, IntFunction intFunction) {
        return AbstractC0115s0.V(this, j, j2);
    }

    @Override // j$.util.stream.J2, j$.util.stream.L2, java.lang.Iterable, j$.lang.Iterable
    /* renamed from: spliterator */
    public final E mo150spliterator() {
        return super.mo150spliterator();
    }

    @Override // j$.util.stream.J2, j$.util.stream.L2, java.lang.Iterable, j$.lang.Iterable
    /* renamed from: spliterator */
    public final Spliterator mo150spliterator() {
        return super.mo150spliterator();
    }

    @Override // j$.util.stream.J2
    public final C u() {
        return super.mo150spliterator();
    }

    /* renamed from: v */
    public final /* synthetic */ void i(Long[] lArr, int i) {
        AbstractC0115s0.P(this, lArr, i);
    }
}
