package j$.util.stream;

import j$.util.Spliterator;
import j$.util.function.Consumer;
import java.util.ArrayDeque;

/* renamed from: j$.util.stream.d1 */
/* loaded from: classes2.dex */
final class C0057d1 extends AbstractC0061e1 {
    public C0057d1(B0 b0) {
        super(b0);
    }

    @Override // j$.util.Spliterator
    public final boolean a(Consumer consumer) {
        B0 d;
        if (!f()) {
            return false;
        }
        boolean a = this.d.a(consumer);
        if (!a) {
            if (this.c != null || (d = AbstractC0061e1.d(this.e)) == null) {
                this.a = null;
            } else {
                Spliterator spliterator = d.mo148spliterator();
                this.d = spliterator;
                return spliterator.a(consumer);
            }
        }
        return a;
    }

    @Override // j$.util.Spliterator
    public final void forEachRemaining(Consumer consumer) {
        if (this.a != null) {
            if (this.d == null) {
                Spliterator spliterator = this.c;
                if (spliterator == null) {
                    ArrayDeque e = e();
                    while (true) {
                        B0 d = AbstractC0061e1.d(e);
                        if (d != null) {
                            d.forEach(consumer);
                        } else {
                            this.a = null;
                            return;
                        }
                    }
                } else {
                    spliterator.forEachRemaining(consumer);
                }
            } else {
                do {
                } while (a(consumer));
            }
        }
    }
}
