package j$.util.stream;

import j$.util.Spliterator;
import java.util.concurrent.atomic.AtomicLong;

/* loaded from: classes2.dex */
public abstract class s3 {
    protected final Spliterator a;
    protected final boolean b;
    private final long c;
    private final AtomicLong d;

    public s3(Spliterator spliterator, long j, long j2) {
        this.a = spliterator;
        this.b = j2 < 0;
        this.c = j2 >= 0 ? j2 : 0;
        this.d = new AtomicLong(j2 >= 0 ? j + j2 : j);
    }

    public s3(Spliterator spliterator, s3 s3Var) {
        this.a = spliterator;
        this.b = s3Var.b;
        this.d = s3Var.d;
        this.c = s3Var.c;
    }

    public final int characteristics() {
        return this.a.characteristics() & -16465;
    }

    public final long estimateSize() {
        return this.a.estimateSize();
    }

    public final long n(long j) {
        long j2;
        long min;
        do {
            j2 = this.d.get();
            if (j2 != 0) {
                min = Math.min(j2, j);
                if (min <= 0) {
                    break;
                }
            } else if (this.b) {
                return j;
            } else {
                return 0;
            }
        } while (!this.d.compareAndSet(j2, j2 - min));
        if (this.b) {
            return Math.max(j - min, 0L);
        }
        long j3 = this.c;
        return j2 > j3 ? Math.max(min - (j2 - j3), 0L) : min;
    }

    protected abstract Spliterator o(Spliterator spliterator);

    public final int p() {
        if (this.d.get() > 0) {
            return 2;
        }
        return this.b ? 3 : 1;
    }

    public final Spliterator trySplit() {
        Spliterator trySplit;
        if (this.d.get() == 0 || (trySplit = this.a.trySplit()) == null) {
            return null;
        }
        return o(trySplit);
    }
}
