package j$.util.stream;

import j$.util.function.Predicate;

/* renamed from: j$.util.stream.k0 */
/* loaded from: classes2.dex */
final class C0084k0 extends AbstractC0100o0 {
    final /* synthetic */ EnumC0104p0 c;
    final /* synthetic */ Predicate d;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0084k0(Predicate predicate, EnumC0104p0 p0Var) {
        super(p0Var);
        this.c = p0Var;
        this.d = predicate;
    }

    @Override // j$.util.function.Consumer
    public final void accept(Object obj) {
        if (!this.a && this.d.test(obj) == this.c.a) {
            this.a = true;
            this.b = this.c.b;
        }
    }
}
