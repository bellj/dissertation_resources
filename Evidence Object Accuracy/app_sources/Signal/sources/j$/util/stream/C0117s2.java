package j$.util.stream;

import java.util.Arrays;

/* renamed from: j$.util.stream.s2 */
/* loaded from: classes2.dex */
final class C0117s2 extends AbstractC0102o2 {
    private H2 c;

    public C0117s2(AbstractC0054c2 c2Var) {
        super(c2Var);
    }

    @Override // j$.util.stream.AbstractC0045a2, j$.util.stream.AbstractC0054c2
    public final void accept(int i) {
        this.c.accept(i);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        if (j < 2147483639) {
            this.c = j > 0 ? new H2((int) j) : new H2();
            return;
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    @Override // j$.util.stream.W1, j$.util.stream.AbstractC0054c2
    public final void end() {
        int[] iArr = (int[]) this.c.b();
        Arrays.sort(iArr);
        this.a.d((long) iArr.length);
        int i = 0;
        if (!this.b) {
            int length = iArr.length;
            while (i < length) {
                this.a.accept(iArr[i]);
                i++;
            }
        } else {
            int length2 = iArr.length;
            while (i < length2) {
                int i2 = iArr[i];
                if (this.a.f()) {
                    break;
                }
                this.a.accept(i2);
                i++;
            }
        }
        this.a.end();
    }
}
