package j$.util.stream;

import j$.util.Spliterator;
import j$.util.function.BinaryOperator;
import j$.util.function.C;
import java.util.concurrent.CountedCompleter;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public class H0 extends AbstractC0063f {
    protected final AbstractC0115s0 h;
    protected final C i;
    protected final BinaryOperator j;

    H0(H0 h0, Spliterator spliterator) {
        super(h0, spliterator);
        this.h = h0.h;
        this.i = h0.i;
        this.j = h0.j;
    }

    public H0(AbstractC0115s0 s0Var, Spliterator spliterator, C c, C0052c0 c0Var) {
        super(s0Var, spliterator);
        this.h = s0Var;
        this.i = c;
        this.j = c0Var;
    }

    /* access modifiers changed from: protected */
    @Override // j$.util.stream.AbstractC0063f
    public final Object a() {
        AbstractC0131w0 w0Var = (AbstractC0131w0) this.i.apply(this.h.d0(this.b));
        this.h.s0(this.b, w0Var);
        return w0Var.mo149build();
    }

    /* access modifiers changed from: protected */
    @Override // j$.util.stream.AbstractC0063f
    public final AbstractC0063f d(Spliterator spliterator) {
        return new H0(this, spliterator);
    }

    @Override // j$.util.stream.AbstractC0063f, java.util.concurrent.CountedCompleter
    public final void onCompletion(CountedCompleter countedCompleter) {
        AbstractC0063f fVar = this.d;
        if (!(fVar == null)) {
            e((B0) this.j.apply((B0) ((H0) fVar).b(), (B0) ((H0) this.e).b()));
        }
        super.onCompletion(countedCompleter);
    }
}
