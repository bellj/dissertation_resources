package j$.util.stream;

import j$.util.Spliterator;
import j$.util.function.Predicate;
import j$.util.function.Supplier;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public final class D implements x3 {
    final boolean a;
    final Object b;
    final Predicate c;
    final Supplier d;

    public D(boolean z, int i, Object obj, C0107q qVar, C0047b bVar) {
        this.a = z;
        this.b = obj;
        this.c = qVar;
        this.d = bVar;
    }

    @Override // j$.util.stream.x3
    public final Object a(AbstractC0115s0 s0Var, Spliterator spliterator) {
        return new J(this, s0Var, spliterator).invoke();
    }

    @Override // j$.util.stream.x3
    public final int b() {
        return P2.u | (this.a ? 0 : P2.r);
    }

    @Override // j$.util.stream.x3
    public final Object c(AbstractC0115s0 s0Var, Spliterator spliterator) {
        y3 y3Var = (y3) this.d.get();
        s0Var.s0(spliterator, y3Var);
        Object obj = y3Var.get();
        return obj != null ? obj : this.b;
    }
}
