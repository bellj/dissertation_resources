package j$.util.stream;

import j$.util.function.A;
import j$.util.function.B;

/* renamed from: j$.util.stream.j0 */
/* loaded from: classes2.dex */
public interface AbstractC0080j0 extends AbstractC0071h {
    void o(A a);

    void p(B b);

    @Override // j$.util.stream.AbstractC0071h
    AbstractC0080j0 parallel();

    @Override // j$.util.stream.AbstractC0071h
    AbstractC0080j0 sequential();
}
