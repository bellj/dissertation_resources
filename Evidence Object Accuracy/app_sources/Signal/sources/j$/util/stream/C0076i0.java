package j$.util.stream;

import j$.util.AbstractC0032i;
import j$.util.C0031h;
import j$.util.C0033j;
import j$.util.C0035l;
import j$.util.C0147u;
import j$.util.T;
import j$.util.function.A;
import j$.util.function.BiConsumer;
import j$.util.function.C0020c;
import j$.util.stream.Stream;
import java.util.Iterator;
import java.util.LongSummaryStatistics;
import java.util.OptionalDouble;
import java.util.OptionalLong;
import java.util.PrimitiveIterator;
import java.util.function.BiConsumer;
import java.util.function.LongBinaryOperator;
import java.util.function.LongConsumer;
import java.util.function.LongFunction;
import java.util.function.LongPredicate;
import java.util.function.LongToDoubleFunction;
import java.util.function.LongToIntFunction;
import java.util.function.LongUnaryOperator;
import java.util.function.ObjLongConsumer;
import java.util.function.Supplier;
import java.util.stream.BaseStream;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

/* renamed from: j$.util.stream.i0 */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0076i0 implements LongStream {
    final /* synthetic */ AbstractC0080j0 a;

    private /* synthetic */ C0076i0(AbstractC0080j0 j0Var) {
        this.a = j0Var;
    }

    public static /* synthetic */ C0076i0 D(AbstractC0080j0 j0Var) {
        if (j0Var == null) {
            return null;
        }
        return new C0076i0(j0Var);
    }

    @Override // java.util.stream.LongStream
    public final boolean allMatch(LongPredicate longPredicate) {
        AbstractC0080j0 j0Var = this.a;
        C0020c cVar = longPredicate == null ? null : new C0020c(longPredicate);
        AbstractC0072h0 h0Var = (AbstractC0072h0) j0Var;
        h0Var.getClass();
        return ((Boolean) h0Var.v0(AbstractC0115s0.n0(cVar, EnumC0104p0.ALL))).booleanValue();
    }

    @Override // java.util.stream.LongStream
    public final boolean anyMatch(LongPredicate longPredicate) {
        AbstractC0080j0 j0Var = this.a;
        C0020c cVar = longPredicate == null ? null : new C0020c(longPredicate);
        AbstractC0072h0 h0Var = (AbstractC0072h0) j0Var;
        h0Var.getClass();
        return ((Boolean) h0Var.v0(AbstractC0115s0.n0(cVar, EnumC0104p0.ANY))).booleanValue();
    }

    @Override // java.util.stream.LongStream
    public final DoubleStream asDoubleStream() {
        AbstractC0072h0 h0Var = (AbstractC0072h0) this.a;
        h0Var.getClass();
        return B.D(new C0130w(h0Var, P2.p | P2.n, 2));
    }

    @Override // java.util.stream.LongStream
    public final OptionalDouble average() {
        C0033j jVar;
        AbstractC0072h0 h0Var = (AbstractC0072h0) this.a;
        h0Var.getClass();
        long[] jArr = (long[]) h0Var.N0(new C0047b(28), new C0047b(29), new C0052c0(0));
        long j = jArr[0];
        if (j > 0) {
            double d = (double) jArr[1];
            double d2 = (double) j;
            Double.isNaN(d);
            Double.isNaN(d2);
            Double.isNaN(d);
            Double.isNaN(d2);
            jVar = C0033j.d(d / d2);
        } else {
            jVar = C0033j.a();
        }
        return AbstractC0032i.b(jVar);
    }

    @Override // java.util.stream.LongStream
    public final Stream boxed() {
        AbstractC0072h0 h0Var = (AbstractC0072h0) this.a;
        h0Var.getClass();
        return Stream.Wrapper.convert(new C0118t(h0Var, P2.p | P2.n, new C0107q(14), 2));
    }

    @Override // java.util.stream.BaseStream, java.lang.AutoCloseable
    public final /* synthetic */ void close() {
        ((AbstractC0051c) this.a).close();
    }

    @Override // java.util.stream.LongStream
    public final /* synthetic */ Object collect(Supplier supplier, ObjLongConsumer objLongConsumer, BiConsumer biConsumer) {
        AbstractC0080j0 j0Var = this.a;
        C0020c cVar = null;
        C0020c cVar2 = supplier == null ? null : new C0020c(supplier);
        if (objLongConsumer != null) {
            cVar = new C0020c(objLongConsumer);
        }
        return ((AbstractC0072h0) j0Var).N0(cVar2, cVar, BiConsumer.VivifiedWrapper.convert(biConsumer));
    }

    @Override // java.util.stream.LongStream
    public final long count() {
        AbstractC0072h0 h0Var = (AbstractC0072h0) this.a;
        h0Var.getClass();
        return new C0126v(h0Var, P2.p | P2.n, new C0047b(25), 2).sum();
    }

    @Override // java.util.stream.LongStream
    public final LongStream distinct() {
        AbstractC0072h0 h0Var = (AbstractC0072h0) this.a;
        h0Var.getClass();
        return D(((U1) new C0118t(h0Var, P2.p | P2.n, new C0107q(14), 2).distinct()).A(new C0047b(26)));
    }

    @Override // java.util.stream.LongStream
    public final LongStream filter(LongPredicate longPredicate) {
        AbstractC0080j0 j0Var = this.a;
        C0020c cVar = longPredicate == null ? null : new C0020c(longPredicate);
        AbstractC0072h0 h0Var = (AbstractC0072h0) j0Var;
        h0Var.getClass();
        cVar.getClass();
        return D(new C0126v(h0Var, P2.t, cVar, 4));
    }

    @Override // java.util.stream.LongStream
    public final OptionalLong findAny() {
        return AbstractC0032i.d((C0035l) ((AbstractC0072h0) this.a).v0(new D(false, 3, C0035l.a(), new C0107q(3), new C0047b(15))));
    }

    @Override // java.util.stream.LongStream
    public final OptionalLong findFirst() {
        return AbstractC0032i.d((C0035l) ((AbstractC0072h0) this.a).v0(new D(true, 3, C0035l.a(), new C0107q(3), new C0047b(15))));
    }

    @Override // java.util.stream.LongStream
    public final LongStream flatMap(LongFunction longFunction) {
        AbstractC0080j0 j0Var = this.a;
        C0020c cVar = longFunction == null ? null : new C0020c(longFunction);
        AbstractC0072h0 h0Var = (AbstractC0072h0) j0Var;
        h0Var.getClass();
        return D(new C0126v(h0Var, P2.p | P2.n | P2.t, cVar, 3));
    }

    @Override // java.util.stream.LongStream
    public final /* synthetic */ void forEach(LongConsumer longConsumer) {
        this.a.p(A.a(longConsumer));
    }

    @Override // java.util.stream.LongStream
    public final /* synthetic */ void forEachOrdered(LongConsumer longConsumer) {
        this.a.o(A.a(longConsumer));
    }

    @Override // java.util.stream.BaseStream
    public final /* synthetic */ boolean isParallel() {
        return ((AbstractC0051c) this.a).isParallel();
    }

    @Override // java.util.stream.LongStream, java.util.stream.BaseStream
    public final Iterator iterator() {
        return T.h(((AbstractC0072h0) this.a).spliterator());
    }

    @Override // java.util.stream.LongStream, java.util.stream.BaseStream
    /* renamed from: iterator */
    public final PrimitiveIterator.OfLong mo151iterator() {
        return C0147u.a(T.h(((AbstractC0072h0) this.a).spliterator()));
    }

    @Override // java.util.stream.LongStream
    public final LongStream limit(long j) {
        AbstractC0072h0 h0Var = (AbstractC0072h0) this.a;
        h0Var.getClass();
        if (j >= 0) {
            return D(AbstractC0115s0.m0(h0Var, 0, j));
        }
        throw new IllegalArgumentException(Long.toString(j));
    }

    @Override // java.util.stream.LongStream
    public final LongStream map(LongUnaryOperator longUnaryOperator) {
        AbstractC0080j0 j0Var = this.a;
        C0020c cVar = longUnaryOperator == null ? null : new C0020c(longUnaryOperator);
        AbstractC0072h0 h0Var = (AbstractC0072h0) j0Var;
        h0Var.getClass();
        cVar.getClass();
        return D(new C0126v(h0Var, P2.p | P2.n, cVar, 2));
    }

    @Override // java.util.stream.LongStream
    public final DoubleStream mapToDouble(LongToDoubleFunction longToDoubleFunction) {
        AbstractC0080j0 j0Var = this.a;
        C0020c cVar = longToDoubleFunction == null ? null : new C0020c(longToDoubleFunction);
        AbstractC0072h0 h0Var = (AbstractC0072h0) j0Var;
        h0Var.getClass();
        cVar.getClass();
        return B.D(new C0114s(h0Var, P2.p | P2.n, cVar, 5));
    }

    @Override // java.util.stream.LongStream
    public final IntStream mapToInt(LongToIntFunction longToIntFunction) {
        AbstractC0080j0 j0Var = this.a;
        C0020c cVar = longToIntFunction == null ? null : new C0020c(longToIntFunction);
        AbstractC0072h0 h0Var = (AbstractC0072h0) j0Var;
        h0Var.getClass();
        cVar.getClass();
        return C0043a0.D(new C0122u(h0Var, P2.p | P2.n, cVar, 5));
    }

    @Override // java.util.stream.LongStream
    public final java.util.stream.Stream mapToObj(LongFunction longFunction) {
        AbstractC0080j0 j0Var = this.a;
        C0020c cVar = longFunction == null ? null : new C0020c(longFunction);
        AbstractC0072h0 h0Var = (AbstractC0072h0) j0Var;
        h0Var.getClass();
        cVar.getClass();
        return Stream.Wrapper.convert(new C0118t(h0Var, P2.p | P2.n, cVar, 2));
    }

    @Override // java.util.stream.LongStream
    public final OptionalLong max() {
        AbstractC0072h0 h0Var = (AbstractC0072h0) this.a;
        h0Var.getClass();
        return AbstractC0032i.d(h0Var.O0(new C0107q(13)));
    }

    @Override // java.util.stream.LongStream
    public final OptionalLong min() {
        AbstractC0072h0 h0Var = (AbstractC0072h0) this.a;
        h0Var.getClass();
        return AbstractC0032i.d(h0Var.O0(new C0107q(12)));
    }

    @Override // java.util.stream.LongStream
    public final boolean noneMatch(LongPredicate longPredicate) {
        AbstractC0080j0 j0Var = this.a;
        C0020c cVar = longPredicate == null ? null : new C0020c(longPredicate);
        AbstractC0072h0 h0Var = (AbstractC0072h0) j0Var;
        h0Var.getClass();
        return ((Boolean) h0Var.v0(AbstractC0115s0.n0(cVar, EnumC0104p0.NONE))).booleanValue();
    }

    @Override // java.util.stream.BaseStream
    public final /* synthetic */ BaseStream onClose(Runnable runnable) {
        AbstractC0051c cVar = (AbstractC0051c) this.a;
        cVar.onClose(runnable);
        return C0067g.D(cVar);
    }

    @Override // java.util.stream.LongStream
    public final LongStream peek(LongConsumer longConsumer) {
        AbstractC0080j0 j0Var = this.a;
        A a = A.a(longConsumer);
        AbstractC0072h0 h0Var = (AbstractC0072h0) j0Var;
        h0Var.getClass();
        a.getClass();
        return D(new C0126v(h0Var, 0, a, 5));
    }

    @Override // java.util.stream.LongStream
    public final long reduce(long j, LongBinaryOperator longBinaryOperator) {
        AbstractC0080j0 j0Var = this.a;
        C0020c cVar = longBinaryOperator == null ? null : new C0020c(longBinaryOperator);
        AbstractC0072h0 h0Var = (AbstractC0072h0) j0Var;
        h0Var.getClass();
        cVar.getClass();
        return ((Long) h0Var.v0(new I1(3, cVar, j))).longValue();
    }

    @Override // java.util.stream.LongStream
    public final /* synthetic */ OptionalLong reduce(LongBinaryOperator longBinaryOperator) {
        return AbstractC0032i.d(((AbstractC0072h0) this.a).O0(longBinaryOperator == null ? null : new C0020c(longBinaryOperator)));
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v4, types: [j$.util.stream.j0] */
    @Override // java.util.stream.LongStream
    public final LongStream skip(long j) {
        AbstractC0072h0 h0Var = (AbstractC0072h0) this.a;
        h0Var.getClass();
        if (j >= 0) {
            AbstractC0072h0 h0Var2 = h0Var;
            if (j != 0) {
                h0Var2 = AbstractC0115s0.m0(h0Var, j, -1);
            }
            return D(h0Var2);
        }
        throw new IllegalArgumentException(Long.toString(j));
    }

    @Override // java.util.stream.LongStream
    public final LongStream sorted() {
        AbstractC0072h0 h0Var = (AbstractC0072h0) this.a;
        h0Var.getClass();
        return D(new C0133w2(h0Var));
    }

    @Override // java.util.stream.LongStream
    public final /* synthetic */ long sum() {
        return ((AbstractC0072h0) this.a).sum();
    }

    @Override // java.util.stream.LongStream
    public final LongSummaryStatistics summaryStatistics() {
        AbstractC0072h0 h0Var = (AbstractC0072h0) this.a;
        h0Var.getClass();
        C0031h hVar = (C0031h) h0Var.N0(new C0052c0(21), new C0107q(16), new C0107q(17));
        throw new Error("Java 8+ API desugaring (library desugaring) cannot convert to java.util.LongSummaryStatistics");
    }

    @Override // java.util.stream.LongStream
    public final long[] toArray() {
        AbstractC0072h0 h0Var = (AbstractC0072h0) this.a;
        h0Var.getClass();
        return (long[]) AbstractC0112r1.r((AbstractC0143z0) h0Var.w0(new C0047b(27))).b();
    }

    @Override // java.util.stream.BaseStream
    public final /* synthetic */ BaseStream unordered() {
        return C0067g.D(((AbstractC0072h0) this.a).unordered());
    }
}
