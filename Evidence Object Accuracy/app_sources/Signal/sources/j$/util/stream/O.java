package j$.util.stream;

import j$.util.Spliterator;
import j$.util.function.Consumer;

/* loaded from: classes2.dex */
public abstract class O implements x3, y3 {
    private final boolean a;

    public O(boolean z) {
        this.a = z;
    }

    @Override // j$.util.stream.x3
    public final Object a(AbstractC0115s0 s0Var, Spliterator spliterator) {
        (this.a ? new P(s0Var, spliterator, this) : new Q(s0Var, spliterator, s0Var.t0(this))).invoke();
        return null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public /* synthetic */ void accept(double d) {
        AbstractC0115s0.D();
        throw null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public /* synthetic */ void accept(int i) {
        AbstractC0115s0.K();
        throw null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public /* synthetic */ void accept(long j) {
        AbstractC0115s0.L();
        throw null;
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // j$.util.stream.x3
    public final int b() {
        if (this.a) {
            return 0;
        }
        return P2.r;
    }

    @Override // j$.util.stream.x3
    public final Object c(AbstractC0115s0 s0Var, Spliterator spliterator) {
        ((O) s0Var.s0(spliterator, this)).getClass();
        return null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void d(long j) {
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void end() {
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ boolean f() {
        return false;
    }

    @Override // j$.util.function.Supplier
    public final /* bridge */ /* synthetic */ Object get() {
        return null;
    }
}
