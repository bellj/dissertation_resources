package j$.util.stream;

import j$.util.function.AbstractC0024g;

/* renamed from: j$.util.stream.p */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0103p implements AbstractC0024g {
    public final /* synthetic */ int a;
    public final /* synthetic */ AbstractC0054c2 b;

    public /* synthetic */ C0103p(int i, AbstractC0054c2 c2Var) {
        this.a = i;
        this.b = c2Var;
    }

    @Override // j$.util.function.AbstractC0024g
    public final void accept(double d) {
        switch (this.a) {
            case 0:
                this.b.accept(d);
                return;
            default:
                ((r) this.b).a.accept(d);
                return;
        }
    }
}
