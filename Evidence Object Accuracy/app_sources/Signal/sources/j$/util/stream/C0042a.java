package j$.util.stream;

import j$.util.Spliterator;
import j$.util.function.C;
import j$.util.function.Consumer;
import j$.util.function.IntFunction;
import j$.util.function.Supplier;
import java.util.List;

/* renamed from: j$.util.stream.a */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0042a implements Supplier, C, Consumer {
    public final /* synthetic */ int a;
    public final /* synthetic */ Object b;

    public /* synthetic */ C0042a(int i, Object obj) {
        this.a = i;
        this.b = obj;
    }

    @Override // j$.util.function.Consumer
    public final void accept(Object obj) {
        switch (this.a) {
            case 3:
                ((AbstractC0054c2) this.b).accept((AbstractC0054c2) obj);
                return;
            default:
                ((List) this.b).add(obj);
                return;
        }
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        switch (this.a) {
            case 3:
                return Consumer.CC.$default$andThen(this, consumer);
            default:
                return Consumer.CC.$default$andThen(this, consumer);
        }
    }

    @Override // j$.util.function.C
    public final Object apply(long j) {
        int i = G0.k;
        return AbstractC0112r1.g(j, (IntFunction) this.b);
    }

    @Override // j$.util.function.Supplier
    public final Object get() {
        switch (this.a) {
            case 0:
                return (Spliterator) this.b;
            default:
                return ((AbstractC0051c) this.b).C0();
        }
    }
}
