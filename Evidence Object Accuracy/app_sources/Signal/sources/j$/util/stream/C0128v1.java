package j$.util.stream;

import j$.util.function.AbstractC0022e;
import j$.util.function.Consumer;

/* renamed from: j$.util.stream.v1 */
/* loaded from: classes2.dex */
final class C0128v1 implements L1, Z1 {
    private double a;
    final /* synthetic */ double b;
    final /* synthetic */ AbstractC0022e c;

    public C0128v1(double d, AbstractC0022e eVar) {
        this.b = d;
        this.c = eVar;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void accept(double d) {
        this.a = this.c.applyAsDouble(this.a, d);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(int i) {
        AbstractC0115s0.K();
        throw null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(long j) {
        AbstractC0115s0.L();
        throw null;
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        l((Double) obj);
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        this.a = this.b;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void end() {
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ boolean f() {
        return false;
    }

    @Override // j$.util.function.Supplier
    public final Object get() {
        return Double.valueOf(this.a);
    }

    @Override // j$.util.stream.L1
    public final void h(L1 l1) {
        accept(((C0128v1) l1).a);
    }

    @Override // j$.util.stream.Z1
    public final /* synthetic */ void l(Double d) {
        AbstractC0115s0.E(this, d);
    }
}
