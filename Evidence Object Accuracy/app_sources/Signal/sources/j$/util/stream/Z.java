package j$.util.stream;

import j$.util.A;
import j$.util.AbstractC0041s;
import j$.util.C0030g;
import j$.util.C0033j;
import j$.util.C0034k;
import j$.util.Spliterator;
import j$.util.T;
import j$.util.function.BiConsumer;
import j$.util.function.C0020c;
import j$.util.function.I;
import j$.util.function.IntFunction;
import j$.util.function.IntPredicate;
import j$.util.function.Supplier;
import j$.util.function.p;
import j$.util.function.q;
import j$.util.function.r;
import j$.util.function.x;

/* loaded from: classes2.dex */
public abstract class Z extends AbstractC0051c implements IntStream {
    public static final /* synthetic */ int l;

    public Z(Spliterator spliterator, int i) {
        super(spliterator, i, false);
    }

    public Z(AbstractC0051c cVar, int i) {
        super(cVar, i);
    }

    public static A M0(Spliterator spliterator) {
        if (spliterator instanceof A) {
            return (A) spliterator;
        }
        if (A3.a) {
            A3.a(AbstractC0051c.class, "using IntStream.adapt(Spliterator<Integer> s)");
            throw null;
        }
        throw new UnsupportedOperationException("IntStream.adapt(Spliterator<Integer> s)");
    }

    @Override // j$.util.stream.IntStream
    public void B(q qVar) {
        qVar.getClass();
        v0(new L(qVar, true));
    }

    @Override // j$.util.stream.AbstractC0051c
    final Spliterator J0(AbstractC0115s0 s0Var, C0042a aVar, boolean z) {
        return new b3(s0Var, aVar, z);
    }

    @Override // j$.util.stream.IntStream
    public final boolean anyMatch(IntPredicate intPredicate) {
        return ((Boolean) v0(AbstractC0115s0.l0(intPredicate, EnumC0104p0.ANY))).booleanValue();
    }

    @Override // j$.util.stream.IntStream
    public final C asDoubleStream() {
        return new C0130w(this, P2.p | P2.n, 1);
    }

    @Override // j$.util.stream.IntStream
    public final AbstractC0080j0 asLongStream() {
        return new U(this, P2.p | P2.n, 0);
    }

    @Override // j$.util.stream.IntStream
    public final C0033j average() {
        long[] jArr = (long[]) n(new C0047b(21), new C0047b(22), new C0047b(23));
        long j = jArr[0];
        if (j <= 0) {
            return C0033j.a();
        }
        double d = (double) jArr[1];
        double d2 = (double) j;
        Double.isNaN(d);
        Double.isNaN(d2);
        Double.isNaN(d);
        Double.isNaN(d2);
        return C0033j.d(d / d2);
    }

    @Override // j$.util.stream.IntStream
    public final AbstractC0080j0 b(x xVar) {
        xVar.getClass();
        return new C0126v(this, P2.p | P2.n, xVar, 1);
    }

    @Override // j$.util.stream.IntStream
    public final Stream boxed() {
        return v(new C0107q(10));
    }

    @Override // j$.util.stream.IntStream
    public final long count() {
        return ((AbstractC0072h0) b(new C0047b(20))).sum();
    }

    @Override // j$.util.stream.IntStream
    public final IntStream d(q qVar) {
        qVar.getClass();
        return new C0122u(this, 0, qVar, 1);
    }

    @Override // j$.util.stream.IntStream
    public final IntStream distinct() {
        return ((U1) ((U1) boxed()).distinct()).s(new C0047b(19));
    }

    @Override // j$.util.stream.IntStream
    public final int f(int i, p pVar) {
        pVar.getClass();
        return ((Integer) v0(new E1(2, pVar, i))).intValue();
    }

    @Override // j$.util.stream.IntStream
    public final C0034k findAny() {
        return (C0034k) v0(new D(false, 2, C0034k.a(), new C0107q(5), new C0047b(17)));
    }

    @Override // j$.util.stream.IntStream
    public final C0034k findFirst() {
        return (C0034k) v0(new D(true, 2, C0034k.a(), new C0107q(5), new C0047b(17)));
    }

    @Override // j$.util.stream.IntStream
    public final boolean g(IntPredicate intPredicate) {
        return ((Boolean) v0(AbstractC0115s0.l0(intPredicate, EnumC0104p0.ALL))).booleanValue();
    }

    @Override // j$.util.stream.IntStream
    public final IntStream h(C0020c cVar) {
        return new C0122u(this, P2.p | P2.n | P2.t, cVar, 3);
    }

    @Override // j$.util.stream.IntStream
    public final boolean i(IntPredicate intPredicate) {
        return ((Boolean) v0(AbstractC0115s0.l0(intPredicate, EnumC0104p0.NONE))).booleanValue();
    }

    @Override // j$.util.stream.AbstractC0071h
    public final AbstractC0041s iterator() {
        return T.g(spliterator());
    }

    @Override // j$.util.stream.IntStream
    public final C k(C0020c cVar) {
        cVar.getClass();
        return new C0114s(this, P2.p | P2.n, cVar, 4);
    }

    @Override // j$.util.stream.IntStream
    public final IntStream limit(long j) {
        if (j >= 0) {
            return AbstractC0115s0.k0(this, 0, j);
        }
        throw new IllegalArgumentException(Long.toString(j));
    }

    @Override // j$.util.stream.IntStream
    public final IntStream m(C0020c cVar) {
        cVar.getClass();
        return new C0122u(this, P2.p | P2.n, cVar, 2);
    }

    @Override // j$.util.stream.IntStream
    public final C0034k max() {
        return z(new C0107q(11));
    }

    @Override // j$.util.stream.IntStream
    public final C0034k min() {
        return z(new C0107q(6));
    }

    @Override // j$.util.stream.IntStream
    public final Object n(Supplier supplier, I i, BiConsumer biConsumer) {
        C0099o oVar = new C0099o(biConsumer, 1);
        supplier.getClass();
        i.getClass();
        return v0(new C0116s1(2, oVar, i, supplier, 4));
    }

    @Override // j$.util.stream.AbstractC0115s0
    public final AbstractC0131w0 o0(long j, IntFunction intFunction) {
        return AbstractC0112r1.s(j);
    }

    @Override // j$.util.stream.IntStream
    public final IntStream skip(long j) {
        if (j >= 0) {
            return j == 0 ? this : AbstractC0115s0.k0(this, j, -1);
        }
        throw new IllegalArgumentException(Long.toString(j));
    }

    @Override // j$.util.stream.IntStream
    public final IntStream sorted() {
        return new C0129v2(this);
    }

    @Override // j$.util.stream.AbstractC0051c, j$.util.stream.AbstractC0071h
    public final A spliterator() {
        return M0(super.spliterator());
    }

    @Override // j$.util.stream.IntStream
    public final int sum() {
        return f(0, new C0107q(7));
    }

    @Override // j$.util.stream.IntStream
    public final C0030g summaryStatistics() {
        return (C0030g) n(new C0052c0(18), new C0107q(8), new C0107q(9));
    }

    @Override // j$.util.stream.IntStream
    public final int[] toArray() {
        return (int[]) AbstractC0112r1.q((AbstractC0139y0) w0(new C0047b(24))).b();
    }

    @Override // j$.util.stream.AbstractC0071h
    public final AbstractC0071h unordered() {
        return !B0() ? this : new V(this, P2.r);
    }

    @Override // j$.util.stream.IntStream
    public final Stream v(IntFunction intFunction) {
        intFunction.getClass();
        return new C0118t(this, P2.p | P2.n, intFunction, 1);
    }

    @Override // j$.util.stream.IntStream
    public void w(r rVar) {
        rVar.getClass();
        v0(new L(rVar, false));
    }

    @Override // j$.util.stream.AbstractC0051c
    final B0 x0(AbstractC0115s0 s0Var, Spliterator spliterator, boolean z, IntFunction intFunction) {
        return AbstractC0112r1.j(s0Var, spliterator, z);
    }

    @Override // j$.util.stream.IntStream
    public final IntStream y(IntPredicate intPredicate) {
        intPredicate.getClass();
        return new C0122u(this, P2.t, intPredicate, 4);
    }

    @Override // j$.util.stream.AbstractC0051c
    final void y0(Spliterator spliterator, AbstractC0054c2 c2Var) {
        r rVar;
        A M0 = M0(spliterator);
        if (c2Var instanceof r) {
            rVar = (r) c2Var;
        } else if (!A3.a) {
            c2Var.getClass();
            rVar = new S(0, c2Var);
        } else {
            A3.a(AbstractC0051c.class, "using IntStream.adapt(Sink<Integer> s)");
            throw null;
        }
        while (!c2Var.f() && M0.g(rVar)) {
        }
    }

    @Override // j$.util.stream.IntStream
    public final C0034k z(p pVar) {
        pVar.getClass();
        return (C0034k) v0(new C0132w1(2, pVar, 2));
    }

    @Override // j$.util.stream.AbstractC0051c
    public final int z0() {
        return 2;
    }
}
