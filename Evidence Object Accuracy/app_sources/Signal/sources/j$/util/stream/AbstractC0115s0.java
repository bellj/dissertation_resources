package j$.util.stream;

import j$.time.b;
import j$.util.A;
import j$.util.C;
import j$.util.Spliterator;
import j$.util.function.AbstractC0024g;
import j$.util.function.B;
import j$.util.function.C0020c;
import j$.util.function.Consumer;
import j$.util.function.IntFunction;
import j$.util.function.IntPredicate;
import j$.util.function.Predicate;
import j$.util.function.r;
import j$.util.y;

/* renamed from: j$.util.stream.s0 */
/* loaded from: classes2.dex */
public abstract /* synthetic */ class AbstractC0115s0 {
    public static void D() {
        throw new IllegalStateException("called wrong accept method");
    }

    public static void E(Z1 z1, Double d) {
        if (!A3.a) {
            z1.accept(d.doubleValue());
        } else {
            A3.a(z1.getClass(), "{0} calling Sink.OfDouble.accept(Double)");
            throw null;
        }
    }

    public static void G(AbstractC0045a2 a2Var, Integer num) {
        if (!A3.a) {
            a2Var.accept(num.intValue());
        } else {
            A3.a(a2Var.getClass(), "{0} calling Sink.OfInt.accept(Integer)");
            throw null;
        }
    }

    public static void I(AbstractC0050b2 b2Var, Long l) {
        if (!A3.a) {
            b2Var.accept(l.longValue());
        } else {
            A3.a(b2Var.getClass(), "{0} calling Sink.OfLong.accept(Long)");
            throw null;
        }
    }

    public static void K() {
        throw new IllegalStateException("called wrong accept method");
    }

    public static void L() {
        throw new IllegalStateException("called wrong accept method");
    }

    public static Object[] M(A0 a0, IntFunction intFunction) {
        if (A3.a) {
            A3.a(a0.getClass(), "{0} calling Node.OfPrimitive.asArray");
            throw null;
        } else if (a0.count() < 2147483639) {
            Object[] objArr = (Object[]) intFunction.apply((int) a0.count());
            a0.i(objArr, 0);
            return objArr;
        } else {
            throw new IllegalArgumentException("Stream size exceeds max array size");
        }
    }

    public static void N(AbstractC0135x0 x0Var, Double[] dArr, int i) {
        if (!A3.a) {
            double[] dArr2 = (double[]) x0Var.b();
            for (int i2 = 0; i2 < dArr2.length; i2++) {
                dArr[i + i2] = Double.valueOf(dArr2[i2]);
            }
            return;
        }
        A3.a(x0Var.getClass(), "{0} calling Node.OfDouble.copyInto(Double[], int)");
        throw null;
    }

    public static void O(AbstractC0139y0 y0Var, Integer[] numArr, int i) {
        if (!A3.a) {
            int[] iArr = (int[]) y0Var.b();
            for (int i2 = 0; i2 < iArr.length; i2++) {
                numArr[i + i2] = Integer.valueOf(iArr[i2]);
            }
            return;
        }
        A3.a(y0Var.getClass(), "{0} calling Node.OfInt.copyInto(Integer[], int)");
        throw null;
    }

    public static void P(AbstractC0143z0 z0Var, Long[] lArr, int i) {
        if (!A3.a) {
            long[] jArr = (long[]) z0Var.b();
            for (int i2 = 0; i2 < jArr.length; i2++) {
                lArr[i + i2] = Long.valueOf(jArr[i2]);
            }
            return;
        }
        A3.a(z0Var.getClass(), "{0} calling Node.OfInt.copyInto(Long[], int)");
        throw null;
    }

    public static void Q(AbstractC0135x0 x0Var, Consumer consumer) {
        if (consumer instanceof AbstractC0024g) {
            x0Var.g((AbstractC0024g) consumer);
        } else if (!A3.a) {
            ((y) x0Var.mo148spliterator()).forEachRemaining(consumer);
        } else {
            A3.a(x0Var.getClass(), "{0} calling Node.OfLong.forEachRemaining(Consumer)");
            throw null;
        }
    }

    public static void R(AbstractC0139y0 y0Var, Consumer consumer) {
        if (consumer instanceof r) {
            y0Var.g((r) consumer);
        } else if (!A3.a) {
            ((A) y0Var.mo148spliterator()).forEachRemaining(consumer);
        } else {
            A3.a(y0Var.getClass(), "{0} calling Node.OfInt.forEachRemaining(Consumer)");
            throw null;
        }
    }

    public static void S(AbstractC0143z0 z0Var, Consumer consumer) {
        if (consumer instanceof B) {
            z0Var.g((B) consumer);
        } else if (!A3.a) {
            ((C) z0Var.mo148spliterator()).forEachRemaining(consumer);
        } else {
            A3.a(z0Var.getClass(), "{0} calling Node.OfLong.forEachRemaining(Consumer)");
            throw null;
        }
    }

    public static AbstractC0135x0 T(AbstractC0135x0 x0Var, long j, long j2) {
        if (j == 0 && j2 == x0Var.count()) {
            return x0Var;
        }
        long j3 = j2 - j;
        y yVar = (y) x0Var.mo148spliterator();
        AbstractC0119t0 m = AbstractC0112r1.m(j3);
        m.d(j3);
        for (int i = 0; ((long) i) < j && yVar.k(new e3(1)); i++) {
        }
        for (int i2 = 0; ((long) i2) < j3 && yVar.k(m); i2++) {
        }
        m.end();
        return m.mo149build();
    }

    public static AbstractC0139y0 U(AbstractC0139y0 y0Var, long j, long j2) {
        if (j == 0 && j2 == y0Var.count()) {
            return y0Var;
        }
        long j3 = j2 - j;
        A a = (A) y0Var.mo148spliterator();
        AbstractC0123u0 s = AbstractC0112r1.s(j3);
        s.d(j3);
        for (int i = 0; ((long) i) < j && a.g(new g3(1)); i++) {
        }
        for (int i2 = 0; ((long) i2) < j3 && a.g(s); i2++) {
        }
        s.end();
        return s.mo149build();
    }

    public static AbstractC0143z0 V(AbstractC0143z0 z0Var, long j, long j2) {
        if (j == 0 && j2 == z0Var.count()) {
            return z0Var;
        }
        long j3 = j2 - j;
        C c = (C) z0Var.mo148spliterator();
        AbstractC0127v0 t = AbstractC0112r1.t(j3);
        t.d(j3);
        for (int i = 0; ((long) i) < j && c.c(new i3(1)); i++) {
        }
        for (int i2 = 0; ((long) i2) < j3 && c.c(t); i2++) {
        }
        t.end();
        return t.mo149build();
    }

    public static B0 W(B0 b0, long j, long j2, IntFunction intFunction) {
        if (j == 0 && j2 == b0.count()) {
            return b0;
        }
        Spliterator spliterator = b0.mo148spliterator();
        long j3 = j2 - j;
        AbstractC0131w0 g = AbstractC0112r1.g(j3, intFunction);
        g.d(j3);
        for (int i = 0; ((long) i) < j && spliterator.a(new C0107q(18)); i++) {
        }
        for (int i2 = 0; ((long) i2) < j3 && spliterator.a(g); i2++) {
        }
        g.end();
        return g.mo149build();
    }

    public static long X(long j, long j2) {
        long j3 = j2 >= 0 ? j + j2 : Long.MAX_VALUE;
        if (j3 >= 0) {
            return j3;
        }
        return Long.MAX_VALUE;
    }

    public static Spliterator Y(int i, Spliterator spliterator, long j, long j2) {
        long j3 = j2 >= 0 ? j + j2 : Long.MAX_VALUE;
        long j4 = j3 >= 0 ? j3 : Long.MAX_VALUE;
        int[] iArr = AbstractC0090l2.a;
        if (i != 0) {
            int i2 = iArr[i - 1];
            if (i2 == 1) {
                return new l3(spliterator, j, j4);
            }
            if (i2 == 2) {
                return new h3((A) spliterator, j, j4);
            }
            if (i2 == 3) {
                return new j3((C) spliterator, j, j4);
            }
            if (i2 == 4) {
                return new f3((y) spliterator, j, j4);
            }
            StringBuilder a = b.a("Unknown shape ");
            a.append(b.b(i));
            throw new IllegalStateException(a.toString());
        }
        throw null;
    }

    public static long Z(long j, long j2, long j3) {
        if (j >= 0) {
            return Math.max(-1L, Math.min(j - j2, j3));
        }
        return -1;
    }

    public static C c0(y yVar) {
        return new C0134x(yVar, P2.d(yVar));
    }

    private static int e0(long j) {
        return (j != -1 ? P2.u : 0) | P2.t;
    }

    public static IntStream g0(A a) {
        return new W(a, P2.d(a));
    }

    public static AbstractC0080j0 h0(C c) {
        return new C0060e0(c, P2.d(c));
    }

    public static C i0(A a, long j, long j2) {
        if (j >= 0) {
            return new C0086k2(a, e0(j2), j, j2);
        }
        throw new IllegalArgumentException("Skip must be non-negative: " + j);
    }

    public static C0108q0 j0(C0020c cVar, EnumC0104p0 p0Var) {
        cVar.getClass();
        p0Var.getClass();
        return new C0108q0(4, p0Var, new C0083k(3, p0Var, cVar));
    }

    public static IntStream k0(AbstractC0051c cVar, long j, long j2) {
        if (j >= 0) {
            return new C0070g2(cVar, e0(j2), j, j2);
        }
        throw new IllegalArgumentException("Skip must be non-negative: " + j);
    }

    public static C0108q0 l0(IntPredicate intPredicate, EnumC0104p0 p0Var) {
        intPredicate.getClass();
        p0Var.getClass();
        return new C0108q0(2, p0Var, new C0083k(1, p0Var, intPredicate));
    }

    public static AbstractC0080j0 m0(AbstractC0072h0 h0Var, long j, long j2) {
        if (j >= 0) {
            return new C0078i2(h0Var, e0(j2), j, j2);
        }
        throw new IllegalArgumentException("Skip must be non-negative: " + j);
    }

    public static C0108q0 n0(C0020c cVar, EnumC0104p0 p0Var) {
        cVar.getClass();
        p0Var.getClass();
        return new C0108q0(3, p0Var, new C0083k(4, p0Var, cVar));
    }

    public static C0108q0 p0(Predicate predicate, EnumC0104p0 p0Var) {
        predicate.getClass();
        p0Var.getClass();
        return new C0108q0(1, p0Var, new C0083k(2, p0Var, predicate));
    }

    public static Stream q0(AbstractC0051c cVar, long j, long j2) {
        if (j >= 0) {
            return new C0062e2(cVar, e0(j2), j, j2);
        }
        throw new IllegalArgumentException("Skip must be non-negative: " + j);
    }

    public static Stream r0(Spliterator spliterator, boolean z) {
        spliterator.getClass();
        return new R1(spliterator, P2.d(spliterator), z);
    }

    /* access modifiers changed from: package-private */
    public abstract void a0(Spliterator spliterator, AbstractC0054c2 c2Var);

    /* access modifiers changed from: package-private */
    public abstract void b0(Spliterator spliterator, AbstractC0054c2 c2Var);

    /* access modifiers changed from: package-private */
    public abstract long d0(Spliterator spliterator);

    /* access modifiers changed from: package-private */
    public abstract int f0();

    /* access modifiers changed from: package-private */
    public abstract AbstractC0131w0 o0(long j, IntFunction intFunction);

    /* access modifiers changed from: package-private */
    public abstract AbstractC0054c2 s0(Spliterator spliterator, AbstractC0054c2 c2Var);

    /* access modifiers changed from: package-private */
    public abstract AbstractC0054c2 t0(AbstractC0054c2 c2Var);
}
