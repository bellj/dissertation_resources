package j$.util.stream;

import j$.util.function.BiFunction;
import j$.util.function.BinaryOperator;
import j$.util.function.Consumer;

/* renamed from: j$.util.stream.z1 */
/* loaded from: classes2.dex */
final class C0144z1 extends M1 implements L1 {
    final /* synthetic */ Object b;
    final /* synthetic */ BiFunction c;
    final /* synthetic */ BinaryOperator d;

    public C0144z1(Object obj, BiFunction biFunction, BinaryOperator binaryOperator) {
        this.b = obj;
        this.c = biFunction;
        this.d = binaryOperator;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(double d) {
        AbstractC0115s0.D();
        throw null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(int i) {
        AbstractC0115s0.K();
        throw null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(long j) {
        AbstractC0115s0.L();
        throw null;
    }

    @Override // j$.util.function.Consumer
    public final void accept(Object obj) {
        this.a = this.c.apply(this.a, obj);
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        this.a = this.b;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void end() {
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ boolean f() {
        return false;
    }

    @Override // j$.util.stream.L1
    public final void h(L1 l1) {
        this.a = this.d.apply(this.a, ((C0144z1) l1).a);
    }
}
