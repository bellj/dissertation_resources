package j$.util.stream;

import j$.util.function.C0020c;
import j$.util.function.D;

/* renamed from: j$.util.stream.m0 */
/* loaded from: classes2.dex */
final class C0092m0 extends AbstractC0100o0 implements AbstractC0050b2 {
    final /* synthetic */ EnumC0104p0 c;
    final /* synthetic */ D d;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0092m0(D d, EnumC0104p0 p0Var) {
        super(p0Var);
        this.c = p0Var;
        this.d = d;
    }

    @Override // j$.util.stream.AbstractC0100o0, j$.util.stream.AbstractC0054c2
    public final void accept(long j) {
        if (!this.a && ((C0020c) this.d).j(j) == this.c.a) {
            this.a = true;
            this.b = this.c.b;
        }
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        j((Long) obj);
    }

    @Override // j$.util.stream.AbstractC0050b2
    public final /* synthetic */ void j(Long l) {
        AbstractC0115s0.I(this, l);
    }
}
