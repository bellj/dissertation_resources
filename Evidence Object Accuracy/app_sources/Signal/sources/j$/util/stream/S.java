package j$.util.stream;

import j$.util.function.r;

/* loaded from: classes2.dex */
public final /* synthetic */ class S implements r {
    public final /* synthetic */ int a;
    public final /* synthetic */ AbstractC0054c2 b;

    public /* synthetic */ S(int i, AbstractC0054c2 c2Var) {
        this.a = i;
        this.b = c2Var;
    }

    @Override // j$.util.function.r
    public final void accept(int i) {
        switch (this.a) {
            case 0:
                this.b.accept(i);
                return;
            default:
                ((T) this.b).a.accept(i);
                return;
        }
    }
}
