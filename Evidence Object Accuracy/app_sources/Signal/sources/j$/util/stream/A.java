package j$.util.stream;

import j$.util.C0033j;
import j$.util.Spliterator;
import j$.util.T;
import j$.util.function.AbstractC0022e;
import j$.util.function.AbstractC0024g;
import j$.util.function.BiConsumer;
import j$.util.function.C0023f;
import j$.util.function.H;
import j$.util.function.IntFunction;
import j$.util.function.Supplier;
import j$.util.y;
import java.util.Iterator;

/* loaded from: classes2.dex */
public abstract class A extends AbstractC0051c implements C {
    public static final /* synthetic */ int l;

    public A(Spliterator spliterator, int i) {
        super(spliterator, i, false);
    }

    public A(AbstractC0051c cVar, int i) {
        super(cVar, i);
    }

    public static y M0(Spliterator spliterator) {
        if (spliterator instanceof y) {
            return (y) spliterator;
        }
        if (A3.a) {
            A3.a(AbstractC0051c.class, "using DoubleStream.adapt(Spliterator<Double> s)");
            throw null;
        }
        throw new UnsupportedOperationException("DoubleStream.adapt(Spliterator<Double> s)");
    }

    @Override // j$.util.stream.AbstractC0051c
    final Spliterator J0(AbstractC0115s0 s0Var, C0042a aVar, boolean z) {
        return new Z2(s0Var, aVar, z);
    }

    public final Object N0(Supplier supplier, H h, BiConsumer biConsumer) {
        C0099o oVar = new C0099o(biConsumer, 0);
        supplier.getClass();
        h.getClass();
        return v0(new C0116s1(4, oVar, h, supplier, 1));
    }

    public final C0033j O0(AbstractC0022e eVar) {
        eVar.getClass();
        return (C0033j) v0(new C0132w1(4, eVar, 0));
    }

    /* renamed from: P0 */
    public final y spliterator() {
        return M0(super.spliterator());
    }

    @Override // j$.util.stream.AbstractC0071h
    public final Iterator iterator() {
        return T.f(spliterator());
    }

    @Override // j$.util.stream.AbstractC0115s0
    public final AbstractC0131w0 o0(long j, IntFunction intFunction) {
        return AbstractC0112r1.m(j);
    }

    @Override // j$.util.stream.C
    public void r(AbstractC0024g gVar) {
        gVar.getClass();
        v0(new K(gVar, false));
    }

    @Override // j$.util.stream.C
    public void u(C0023f fVar) {
        fVar.getClass();
        v0(new K(fVar, true));
    }

    @Override // j$.util.stream.AbstractC0071h
    public final AbstractC0071h unordered() {
        return !B0() ? this : new C0130w(this, P2.r, 0);
    }

    @Override // j$.util.stream.AbstractC0051c
    final B0 x0(AbstractC0115s0 s0Var, Spliterator spliterator, boolean z, IntFunction intFunction) {
        return AbstractC0112r1.i(s0Var, spliterator, z);
    }

    @Override // j$.util.stream.AbstractC0051c
    final void y0(Spliterator spliterator, AbstractC0054c2 c2Var) {
        AbstractC0024g gVar;
        y M0 = M0(spliterator);
        if (c2Var instanceof AbstractC0024g) {
            gVar = (AbstractC0024g) c2Var;
        } else if (!A3.a) {
            c2Var.getClass();
            gVar = new C0103p(0, c2Var);
        } else {
            A3.a(AbstractC0051c.class, "using DoubleStream.adapt(Sink<Double> s)");
            throw null;
        }
        while (!c2Var.f() && M0.k(gVar)) {
        }
    }

    @Override // j$.util.stream.AbstractC0051c
    public final int z0() {
        return 4;
    }
}
