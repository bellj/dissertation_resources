package j$.util.stream;

import j$.util.C0034k;

/* loaded from: classes2.dex */
final class F extends I implements AbstractC0045a2 {
    @Override // j$.util.stream.I, j$.util.stream.AbstractC0054c2
    public final void accept(int i) {
        l(Integer.valueOf(i));
    }

    @Override // j$.util.function.Supplier
    public final Object get() {
        if (this.a) {
            return C0034k.d(((Integer) this.b).intValue());
        }
        return null;
    }
}
