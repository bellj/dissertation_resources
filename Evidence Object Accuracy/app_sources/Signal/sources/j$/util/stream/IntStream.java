package j$.util.stream;

import j$.util.A;
import j$.util.AbstractC0041s;
import j$.util.C0030g;
import j$.util.C0033j;
import j$.util.C0034k;
import j$.util.function.BiConsumer;
import j$.util.function.C0020c;
import j$.util.function.I;
import j$.util.function.IntFunction;
import j$.util.function.IntPredicate;
import j$.util.function.Supplier;
import j$.util.function.p;
import j$.util.function.q;
import j$.util.function.r;
import j$.util.function.x;

/* loaded from: classes2.dex */
public interface IntStream extends AbstractC0071h {
    void B(q qVar);

    boolean anyMatch(IntPredicate intPredicate);

    C asDoubleStream();

    AbstractC0080j0 asLongStream();

    C0033j average();

    AbstractC0080j0 b(x xVar);

    Stream boxed();

    long count();

    IntStream d(q qVar);

    IntStream distinct();

    int f(int i, p pVar);

    C0034k findAny();

    C0034k findFirst();

    boolean g(IntPredicate intPredicate);

    IntStream h(C0020c cVar);

    boolean i(IntPredicate intPredicate);

    @Override // j$.util.stream.AbstractC0071h
    AbstractC0041s iterator();

    C k(C0020c cVar);

    IntStream limit(long j);

    IntStream m(C0020c cVar);

    C0034k max();

    C0034k min();

    Object n(Supplier supplier, I i, BiConsumer biConsumer);

    @Override // j$.util.stream.AbstractC0071h
    IntStream parallel();

    @Override // j$.util.stream.AbstractC0071h
    IntStream sequential();

    IntStream skip(long j);

    IntStream sorted();

    @Override // j$.util.stream.AbstractC0071h
    A spliterator();

    int sum();

    C0030g summaryStatistics();

    int[] toArray();

    Stream v(IntFunction intFunction);

    void w(r rVar);

    IntStream y(IntPredicate intPredicate);

    C0034k z(p pVar);
}
