package j$.util.stream;

import j$.util.function.AbstractC0024g;
import j$.util.function.C0023f;

/* loaded from: classes2.dex */
public interface C extends AbstractC0071h {
    @Override // j$.util.stream.AbstractC0071h
    C parallel();

    void r(AbstractC0024g gVar);

    @Override // j$.util.stream.AbstractC0071h
    C sequential();

    void u(C0023f fVar);
}
