package j$.util.stream;

import j$.util.E;
import j$.util.Spliterator;
import java.util.ArrayDeque;

/* renamed from: j$.util.stream.c1 */
/* loaded from: classes2.dex */
public abstract class AbstractC0053c1 extends AbstractC0061e1 implements E {
    public AbstractC0053c1(A0 a0) {
        super(a0);
    }

    @Override // j$.util.E
    /* renamed from: forEachRemaining */
    public final void m(Object obj) {
        if (this.a != null) {
            if (this.d == null) {
                Spliterator spliterator = this.c;
                if (spliterator == null) {
                    ArrayDeque e = e();
                    while (true) {
                        A0 a0 = (A0) AbstractC0061e1.d(e);
                        if (a0 != null) {
                            a0.g(obj);
                        } else {
                            this.a = null;
                            return;
                        }
                    }
                } else {
                    ((E) spliterator).forEachRemaining(obj);
                }
            } else {
                do {
                } while (k(obj));
            }
        }
    }

    @Override // j$.util.E
    /* renamed from: tryAdvance */
    public final boolean k(Object obj) {
        A0 a0;
        if (!f()) {
            return false;
        }
        boolean tryAdvance = ((E) this.d).tryAdvance(obj);
        if (!tryAdvance) {
            if (this.c != null || (a0 = (A0) AbstractC0061e1.d(this.e)) == null) {
                this.a = null;
            } else {
                E spliterator = a0.mo148spliterator();
                this.d = spliterator;
                return spliterator.tryAdvance(obj);
            }
        }
        return tryAdvance;
    }
}
