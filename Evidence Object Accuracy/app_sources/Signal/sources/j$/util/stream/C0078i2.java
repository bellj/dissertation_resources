package j$.util.stream;

import j$.util.C;
import j$.util.Spliterator;
import j$.util.function.IntFunction;

/* renamed from: j$.util.stream.i2 */
/* loaded from: classes2.dex */
public final class C0078i2 extends AbstractC0064f0 {
    public static final /* synthetic */ int o;
    final /* synthetic */ long m;
    final /* synthetic */ long n;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0078i2(AbstractC0072h0 h0Var, int i, long j, long j2) {
        super(h0Var, i);
        this.m = j;
        this.n = j2;
    }

    static C Q0(C c, long j, long j2, long j3) {
        long j4;
        long j5;
        if (j <= j3) {
            long j6 = j3 - j;
            j4 = j2 >= 0 ? Math.min(j2, j6) : j6;
            j5 = 0;
        } else {
            j5 = j;
            j4 = j2;
        }
        return new p3(c, j5, j4);
    }

    @Override // j$.util.stream.AbstractC0051c
    final B0 D0(Spliterator spliterator, IntFunction intFunction, AbstractC0051c cVar) {
        long d0 = cVar.d0(spliterator);
        return (d0 <= 0 || !spliterator.hasCharacteristics(16384)) ? !P2.ORDERED.e(cVar.f0()) ? AbstractC0112r1.k(this, Q0((C) cVar.K0(spliterator), this.m, this.n, d0), true) : (B0) new C0094m2(this, cVar, spliterator, intFunction, this.m, this.n).invoke() : AbstractC0112r1.k(cVar, AbstractC0115s0.Y(cVar.A0(), spliterator, this.m, this.n), true);
    }

    @Override // j$.util.stream.AbstractC0051c
    final Spliterator E0(AbstractC0051c cVar, Spliterator spliterator) {
        long d0 = cVar.d0(spliterator);
        if (d0 <= 0 || !spliterator.hasCharacteristics(16384)) {
            return !P2.ORDERED.e(cVar.f0()) ? Q0((C) cVar.K0(spliterator), this.m, this.n, d0) : ((B0) new C0094m2(this, cVar, spliterator, new C0052c0(12), this.m, this.n).invoke()).mo148spliterator();
        }
        long j = this.m;
        return new j3((C) cVar.K0(spliterator), j, AbstractC0115s0.X(j, this.n));
    }

    @Override // j$.util.stream.AbstractC0051c
    public final AbstractC0054c2 G0(int i, AbstractC0054c2 c2Var) {
        return new C0074h2(this, c2Var);
    }
}
