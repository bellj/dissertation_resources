package j$.util.stream;

import j$.util.Spliterator;
import j$.util.function.IntFunction;

/* loaded from: classes2.dex */
public final class G0 extends H0 {
    public static final /* synthetic */ int k;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public G0(int i, Spliterator spliterator, AbstractC0115s0 s0Var) {
        super(s0Var, spliterator, new C0052c0(1), new C0052c0(2));
        if (i == 1) {
            super(s0Var, spliterator, new C0052c0(3), new C0052c0(4));
        } else if (i != 2) {
        } else {
            super(s0Var, spliterator, new C0052c0(5), new C0052c0(6));
        }
    }

    public /* synthetic */ G0(Spliterator spliterator, IntFunction intFunction, AbstractC0115s0 s0Var) {
        super(s0Var, spliterator, new C0042a(2, intFunction), new C0052c0(7));
    }
}
