package j$.util.stream;

import j$.util.Collection$EL;
import j$.util.Spliterator;
import j$.util.function.Consumer;
import j$.util.function.IntFunction;
import java.util.Collection;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public final class F0 implements B0 {
    private final Collection a;

    public F0(Collection collection) {
        this.a = collection;
    }

    @Override // j$.util.stream.B0
    public final B0 a(int i) {
        throw new IndexOutOfBoundsException();
    }

    @Override // j$.util.stream.B0
    public final long count() {
        return (long) this.a.size();
    }

    @Override // j$.util.stream.B0
    public final void forEach(Consumer consumer) {
        Collection$EL.a(this.a, consumer);
    }

    @Override // j$.util.stream.B0
    public final void i(Object[] objArr, int i) {
        for (Object obj : this.a) {
            i++;
            objArr[i] = obj;
        }
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ int k() {
        return 0;
    }

    @Override // j$.util.stream.B0
    public final Object[] m(IntFunction intFunction) {
        Collection collection = this.a;
        return collection.toArray((Object[]) intFunction.apply(collection.size()));
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ B0 n(long j, long j2, IntFunction intFunction) {
        return AbstractC0115s0.W(this, j, j2, intFunction);
    }

    @Override // j$.util.stream.B0
    /* renamed from: spliterator */
    public final Spliterator mo148spliterator() {
        return Collection$EL.stream(this.a).spliterator();
    }

    public final String toString() {
        return String.format("CollectionNode[%d][%s]", Integer.valueOf(this.a.size()), this.a);
    }
}
