package j$.util.stream;

import j$.util.Spliterator;
import j$.util.function.Consumer;
import java.util.concurrent.CountedCompleter;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.m1 */
/* loaded from: classes2.dex */
public abstract class AbstractC0093m1 extends CountedCompleter implements AbstractC0054c2 {
    protected final Spliterator a;
    protected final AbstractC0115s0 b;
    protected final long c;
    protected long d;
    protected long e;
    protected int f;
    protected int g;

    public AbstractC0093m1(int i, Spliterator spliterator, AbstractC0115s0 s0Var) {
        this.a = spliterator;
        this.b = s0Var;
        this.c = AbstractC0063f.f(spliterator.estimateSize());
        this.d = 0;
        this.e = (long) i;
    }

    public AbstractC0093m1(AbstractC0093m1 m1Var, Spliterator spliterator, long j, long j2, int i) {
        super(m1Var);
        this.a = spliterator;
        this.b = m1Var.b;
        this.c = m1Var.c;
        this.d = j;
        this.e = j2;
        if (j < 0 || j2 < 0 || (j + j2) - 1 >= ((long) i)) {
            throw new IllegalArgumentException(String.format("offset and length interval [%d, %d + %d) is not within array size interval [0, %d)", Long.valueOf(j), Long.valueOf(j), Long.valueOf(j2), Integer.valueOf(i)));
        }
    }

    abstract AbstractC0093m1 a(Spliterator spliterator, long j, long j2);

    @Override // j$.util.stream.AbstractC0054c2
    public /* synthetic */ void accept(double d) {
        AbstractC0115s0.D();
        throw null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public /* synthetic */ void accept(int i) {
        AbstractC0115s0.K();
        throw null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public /* synthetic */ void accept(long j) {
        AbstractC0115s0.L();
        throw null;
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // java.util.concurrent.CountedCompleter
    public final void compute() {
        Spliterator trySplit;
        Spliterator spliterator = this.a;
        AbstractC0093m1 m1Var = this;
        while (spliterator.estimateSize() > m1Var.c && (trySplit = spliterator.trySplit()) != null) {
            m1Var.setPendingCount(1);
            long estimateSize = trySplit.estimateSize();
            m1Var.a(trySplit, m1Var.d, estimateSize).fork();
            m1Var = m1Var.a(spliterator, m1Var.d + estimateSize, m1Var.e - estimateSize);
        }
        m1Var.b.s0(spliterator, m1Var);
        m1Var.propagateCompletion();
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        long j2 = this.e;
        if (j <= j2) {
            int i = (int) this.d;
            this.f = i;
            this.g = i + ((int) j2);
            return;
        }
        throw new IllegalStateException("size passed to Sink.begin exceeds array length");
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void end() {
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ boolean f() {
        return false;
    }
}
