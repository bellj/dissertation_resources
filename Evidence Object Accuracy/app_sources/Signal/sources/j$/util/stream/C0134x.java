package j$.util.stream;

import j$.util.Spliterator;
import j$.util.function.AbstractC0024g;
import j$.util.function.C0023f;

/* renamed from: j$.util.stream.x */
/* loaded from: classes2.dex */
public final class C0134x extends A {
    public C0134x(Spliterator spliterator, int i) {
        super(spliterator, i);
    }

    @Override // j$.util.stream.AbstractC0051c
    final boolean F0() {
        throw new UnsupportedOperationException();
    }

    @Override // j$.util.stream.AbstractC0051c
    public final AbstractC0054c2 G0(int i, AbstractC0054c2 c2Var) {
        throw new UnsupportedOperationException();
    }

    @Override // j$.util.stream.AbstractC0051c, j$.util.stream.AbstractC0071h
    public final /* bridge */ /* synthetic */ C parallel() {
        parallel();
        return this;
    }

    @Override // j$.util.stream.A, j$.util.stream.C
    public final void r(AbstractC0024g gVar) {
        if (!isParallel()) {
            A.M0(I0()).m(gVar);
        } else {
            super.r(gVar);
        }
    }

    @Override // j$.util.stream.AbstractC0051c, j$.util.stream.AbstractC0071h
    public final /* bridge */ /* synthetic */ C sequential() {
        sequential();
        return this;
    }

    @Override // j$.util.stream.A, j$.util.stream.C
    public final void u(C0023f fVar) {
        if (!isParallel()) {
            A.M0(I0()).m(fVar);
        } else {
            super.u(fVar);
        }
    }
}
