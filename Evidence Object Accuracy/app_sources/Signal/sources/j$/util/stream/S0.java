package j$.util.stream;

import j$.util.E;
import j$.util.Spliterator;
import j$.util.T;
import j$.util.function.Consumer;
import j$.util.function.IntFunction;

/* loaded from: classes2.dex */
final class S0 extends U0 implements AbstractC0143z0 {
    @Override // j$.util.stream.U0, j$.util.stream.B0
    public final A0 a(int i) {
        throw new IndexOutOfBoundsException();
    }

    @Override // j$.util.stream.A0
    public final Object b() {
        return AbstractC0112r1.f;
    }

    /* renamed from: d */
    public final /* synthetic */ void i(Long[] lArr, int i) {
        AbstractC0115s0.P(this, lArr, i);
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ void forEach(Consumer consumer) {
        AbstractC0115s0.S(this, consumer);
    }

    @Override // j$.util.stream.U0, j$.util.stream.B0
    public final /* synthetic */ B0 n(long j, long j2, IntFunction intFunction) {
        return AbstractC0115s0.V(this, j, j2);
    }

    @Override // j$.util.stream.B0
    /* renamed from: spliterator */
    public final E mo148spliterator() {
        return T.d();
    }

    @Override // j$.util.stream.B0
    /* renamed from: spliterator */
    public final Spliterator mo148spliterator() {
        return T.d();
    }
}
