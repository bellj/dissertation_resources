package j$.util.stream;

import j$.util.function.Consumer;
import j$.util.function.Predicate;
import j$.util.function.ToDoubleFunction;
import j$.util.function.ToIntFunction;
import j$.util.function.ToLongFunction;

/* loaded from: classes2.dex */
final class O1 extends Y1 {
    public final /* synthetic */ int b;
    final /* synthetic */ AbstractC0051c c;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ O1(AbstractC0051c cVar, AbstractC0054c2 c2Var, int i) {
        super(c2Var);
        this.b = i;
        this.c = cVar;
    }

    @Override // j$.util.function.Consumer
    public final void accept(Object obj) {
        switch (this.b) {
            case 0:
                ((Consumer) ((C0118t) this.c).n).accept(obj);
                this.a.accept((AbstractC0054c2) obj);
                return;
            case 1:
                if (((Predicate) ((C0118t) this.c).n).test(obj)) {
                    this.a.accept((AbstractC0054c2) obj);
                    return;
                }
                return;
            case 2:
                this.a.accept((AbstractC0054c2) ((Q1) this.c).n.apply(obj));
                return;
            case 3:
                this.a.accept(((ToIntFunction) ((C0122u) this.c).n).applyAsInt(obj));
                return;
            case 4:
                this.a.accept(((ToLongFunction) ((C0126v) this.c).n).applyAsLong(obj));
                return;
            case 5:
                this.a.accept(((ToDoubleFunction) ((C0114s) this.c).n).applyAsDouble(obj));
                return;
            default:
                Stream stream = (Stream) ((Q1) this.c).n.apply(obj);
                if (stream != null) {
                    try {
                        ((Stream) stream.sequential()).forEach(this.a);
                    } catch (Throwable th) {
                        try {
                            stream.close();
                        } catch (Throwable th2) {
                            th.addSuppressed(th2);
                        }
                        throw th;
                    }
                }
                if (stream != null) {
                    stream.close();
                    return;
                }
                return;
        }
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        switch (this.b) {
            case 1:
                this.a.d(-1);
                return;
            case 6:
                this.a.d(-1);
                return;
            default:
                this.a.d(j);
                return;
        }
    }
}
