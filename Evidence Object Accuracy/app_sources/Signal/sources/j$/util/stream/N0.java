package j$.util.stream;

import j$.util.E;
import j$.util.Spliterator;
import j$.util.T;
import j$.util.function.AbstractC0024g;
import j$.util.function.Consumer;
import j$.util.function.IntFunction;
import java.util.Arrays;

/* loaded from: classes2.dex */
public class N0 implements AbstractC0135x0 {
    final double[] a;
    int b;

    public N0(long j) {
        if (j < 2147483639) {
            this.a = new double[(int) j];
            this.b = 0;
            return;
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    public N0(double[] dArr) {
        this.a = dArr;
        this.b = dArr.length;
    }

    @Override // j$.util.stream.A0, j$.util.stream.B0
    public final A0 a(int i) {
        throw new IndexOutOfBoundsException();
    }

    @Override // j$.util.stream.A0
    public final Object b() {
        double[] dArr = this.a;
        int length = dArr.length;
        int i = this.b;
        return length == i ? dArr : Arrays.copyOf(dArr, i);
    }

    @Override // j$.util.stream.A0
    public final void c(int i, Object obj) {
        System.arraycopy(this.a, 0, (double[]) obj, i, this.b);
    }

    @Override // j$.util.stream.B0
    public final long count() {
        return (long) this.b;
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ void forEach(Consumer consumer) {
        AbstractC0115s0.Q(this, consumer);
    }

    @Override // j$.util.stream.A0
    public final void g(Object obj) {
        AbstractC0024g gVar = (AbstractC0024g) obj;
        for (int i = 0; i < this.b; i++) {
            gVar.accept(this.a[i]);
        }
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ int k() {
        return 0;
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ Object[] m(IntFunction intFunction) {
        return AbstractC0115s0.M(this, intFunction);
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ B0 n(long j, long j2, IntFunction intFunction) {
        return AbstractC0115s0.T(this, j, j2);
    }

    /* renamed from: o */
    public final /* synthetic */ void i(Double[] dArr, int i) {
        AbstractC0115s0.N(this, dArr, i);
    }

    @Override // j$.util.stream.A0, j$.util.stream.B0
    /* renamed from: spliterator */
    public final E mo148spliterator() {
        return T.j(this.a, 0, this.b);
    }

    @Override // j$.util.stream.A0, j$.util.stream.B0
    /* renamed from: spliterator */
    public final Spliterator mo148spliterator() {
        return T.j(this.a, 0, this.b);
    }

    public String toString() {
        return String.format("DoubleArrayNode[%d][%s]", Integer.valueOf(this.a.length - this.b), Arrays.toString(this.a));
    }
}
