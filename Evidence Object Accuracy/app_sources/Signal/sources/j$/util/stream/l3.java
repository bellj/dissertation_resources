package j$.util.stream;

import j$.util.AbstractC0037n;
import j$.util.Spliterator;
import j$.util.function.Consumer;
import java.util.Comparator;

/* loaded from: classes2.dex */
public final class l3 extends m3 implements Spliterator {
    public l3(Spliterator spliterator, long j, long j2) {
        super(spliterator, j, j2, 0, Math.min(spliterator.estimateSize(), j2));
    }

    private l3(Spliterator spliterator, long j, long j2, long j3, long j4) {
        super(spliterator, j, j2, j3, j4);
    }

    @Override // j$.util.Spliterator
    public final boolean a(Consumer consumer) {
        long j;
        consumer.getClass();
        if (this.a >= this.e) {
            return false;
        }
        while (true) {
            long j2 = this.a;
            j = this.d;
            if (j2 <= j) {
                break;
            }
            this.c.a(new C0052c0(15));
            this.d++;
        }
        if (j >= this.e) {
            return false;
        }
        this.d = j + 1;
        return this.c.a(consumer);
    }

    @Override // j$.util.stream.m3
    protected final Spliterator d(Spliterator spliterator, long j, long j2, long j3, long j4) {
        return new l3(spliterator, j, j2, j3, j4);
    }

    @Override // j$.util.Spliterator
    public final void forEachRemaining(Consumer consumer) {
        consumer.getClass();
        long j = this.a;
        long j2 = this.e;
        if (j < j2) {
            long j3 = this.d;
            if (j3 < j2) {
                if (j3 < j || this.c.estimateSize() + j3 > this.b) {
                    while (this.a > this.d) {
                        this.c.a(new C0052c0(14));
                        this.d++;
                    }
                    while (this.d < this.e) {
                        this.c.a(consumer);
                        this.d++;
                    }
                    return;
                }
                this.c.forEachRemaining(consumer);
                this.d = this.e;
            }
        }
    }

    @Override // j$.util.Spliterator
    public final Comparator getComparator() {
        throw new IllegalStateException();
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ long getExactSizeIfKnown() {
        return AbstractC0037n.h(this);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean hasCharacteristics(int i) {
        return AbstractC0037n.j(this, i);
    }
}
