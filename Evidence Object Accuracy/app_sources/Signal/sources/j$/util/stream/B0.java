package j$.util.stream;

import j$.util.Spliterator;
import j$.util.function.Consumer;
import j$.util.function.IntFunction;

/* loaded from: classes2.dex */
public interface B0 {
    B0 a(int i);

    long count();

    void forEach(Consumer consumer);

    void i(Object[] objArr, int i);

    int k();

    Object[] m(IntFunction intFunction);

    B0 n(long j, long j2, IntFunction intFunction);

    /* renamed from: spliterator */
    Spliterator mo148spliterator();
}
