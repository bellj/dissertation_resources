package j$.util.stream;

import j$.util.C0033j;

/* loaded from: classes2.dex */
final class E extends I implements Z1 {
    @Override // j$.util.stream.I, j$.util.stream.AbstractC0054c2
    public final void accept(double d) {
        l(Double.valueOf(d));
    }

    @Override // j$.util.function.Supplier
    public final Object get() {
        if (this.a) {
            return C0033j.d(((Double) this.b).doubleValue());
        }
        return null;
    }
}
