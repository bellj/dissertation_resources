package j$.util.stream;

import j$.util.Spliterator;
import j$.util.function.Consumer;
import j$.util.function.IntFunction;

/* loaded from: classes2.dex */
public final class M0 extends D0 {
    public M0(B0 b0, B0 b02) {
        super(b0, b02);
    }

    @Override // j$.util.stream.B0
    public final void forEach(Consumer consumer) {
        this.a.forEach(consumer);
        this.b.forEach(consumer);
    }

    @Override // j$.util.stream.B0
    public final void i(Object[] objArr, int i) {
        objArr.getClass();
        this.a.i(objArr, i);
        this.b.i(objArr, i + ((int) this.a.count()));
    }

    @Override // j$.util.stream.B0
    public final Object[] m(IntFunction intFunction) {
        long count = count();
        if (count < 2147483639) {
            Object[] objArr = (Object[]) intFunction.apply((int) count);
            i(objArr, 0);
            return objArr;
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    @Override // j$.util.stream.B0
    public final B0 n(long j, long j2, IntFunction intFunction) {
        if (j == 0 && j2 == count()) {
            return this;
        }
        long count = this.a.count();
        return j >= count ? this.b.n(j - count, j2 - count, intFunction) : j2 <= count ? this.a.n(j, j2, intFunction) : AbstractC0112r1.l(1, this.a.n(j, count, intFunction), this.b.n(0, j2 - count, intFunction));
    }

    @Override // j$.util.stream.B0
    /* renamed from: spliterator */
    public final Spliterator mo148spliterator() {
        return new C0057d1(this);
    }

    public final String toString() {
        return count() < 32 ? String.format("ConcNode[%s.%s]", this.a, this.b) : String.format("ConcNode[size=%d]", Long.valueOf(count()));
    }
}
