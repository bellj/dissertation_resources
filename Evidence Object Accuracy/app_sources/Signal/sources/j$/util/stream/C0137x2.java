package j$.util.stream;

import j$.util.Comparator;
import j$.util.Spliterator;
import j$.util.function.IntFunction;
import java.util.Arrays;
import java.util.Comparator;

/* renamed from: j$.util.stream.x2 */
/* loaded from: classes2.dex */
final class C0137x2 extends S1 {
    private final boolean m;
    private final Comparator n;

    public C0137x2(AbstractC0051c cVar) {
        super(cVar, P2.q | P2.o);
        this.m = true;
        this.n = Comparator.CC.a();
    }

    public C0137x2(AbstractC0051c cVar, java.util.Comparator comparator) {
        super(cVar, P2.q | P2.p);
        this.m = false;
        comparator.getClass();
        this.n = comparator;
    }

    @Override // j$.util.stream.AbstractC0051c
    public final B0 D0(Spliterator spliterator, IntFunction intFunction, AbstractC0051c cVar) {
        if (P2.SORTED.e(cVar.f0()) && this.m) {
            return cVar.u0(spliterator, false, intFunction);
        }
        Object[] m = cVar.u0(spliterator, true, intFunction).m(intFunction);
        Arrays.sort(m, this.n);
        return new E0(m);
    }

    @Override // j$.util.stream.AbstractC0051c
    public final AbstractC0054c2 G0(int i, AbstractC0054c2 c2Var) {
        c2Var.getClass();
        return (!P2.SORTED.e(i) || !this.m) ? P2.SIZED.e(i) ? new C2(c2Var, this.n) : new C0141y2(c2Var, this.n) : c2Var;
    }
}
