package j$.util.stream;

import j$.util.Spliterator;
import j$.util.function.IntFunction;
import java.util.Arrays;

/* renamed from: j$.util.stream.v2 */
/* loaded from: classes2.dex */
final class C0129v2 extends X {
    public C0129v2(AbstractC0051c cVar) {
        super(cVar, P2.q | P2.o);
    }

    @Override // j$.util.stream.AbstractC0051c
    public final B0 D0(Spliterator spliterator, IntFunction intFunction, AbstractC0051c cVar) {
        if (P2.SORTED.e(cVar.f0())) {
            return cVar.u0(spliterator, false, intFunction);
        }
        int[] iArr = (int[]) ((AbstractC0139y0) cVar.u0(spliterator, true, intFunction)).b();
        Arrays.sort(iArr);
        return new W0(iArr);
    }

    @Override // j$.util.stream.AbstractC0051c
    public final AbstractC0054c2 G0(int i, AbstractC0054c2 c2Var) {
        c2Var.getClass();
        return P2.SORTED.e(i) ? c2Var : P2.SIZED.e(i) ? new A2(c2Var) : new C0117s2(c2Var);
    }
}
