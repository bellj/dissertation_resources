package j$.util.stream;

import java.util.Arrays;

/* renamed from: j$.util.stream.r2 */
/* loaded from: classes2.dex */
final class C0113r2 extends AbstractC0098n2 {
    private F2 c;

    public C0113r2(AbstractC0054c2 c2Var) {
        super(c2Var);
    }

    @Override // j$.util.stream.Z1, j$.util.stream.AbstractC0054c2
    public final void accept(double d) {
        this.c.accept(d);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        if (j < 2147483639) {
            this.c = j > 0 ? new F2((int) j) : new F2();
            return;
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    @Override // j$.util.stream.V1, j$.util.stream.AbstractC0054c2
    public final void end() {
        double[] dArr = (double[]) this.c.b();
        Arrays.sort(dArr);
        this.a.d((long) dArr.length);
        int i = 0;
        if (!this.b) {
            int length = dArr.length;
            while (i < length) {
                this.a.accept(dArr[i]);
                i++;
            }
        } else {
            int length2 = dArr.length;
            while (i < length2) {
                double d = dArr[i];
                if (this.a.f()) {
                    break;
                }
                this.a.accept(d);
                i++;
            }
        }
        this.a.end();
    }
}
