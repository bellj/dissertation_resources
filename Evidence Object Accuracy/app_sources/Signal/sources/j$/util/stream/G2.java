package j$.util.stream;

import j$.util.A;
import j$.util.AbstractC0037n;
import j$.util.E;
import j$.util.T;
import j$.util.function.Consumer;
import j$.util.function.r;

/* loaded from: classes2.dex */
public final class G2 extends K2 implements A {
    final /* synthetic */ H2 g;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public G2(H2 h2, int i, int i2, int i3, int i4) {
        super(h2, i, i2, i3, i4);
        this.g = h2;
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.n(this, consumer);
    }

    @Override // j$.util.stream.K2
    final void d(int i, Object obj, Object obj2) {
        ((r) obj2).accept(((int[]) obj)[i]);
    }

    @Override // j$.util.stream.K2
    final E e(Object obj, int i, int i2) {
        return T.k((int[]) obj, i, i2 + i);
    }

    @Override // j$.util.stream.K2
    final E f(int i, int i2, int i3, int i4) {
        return new G2(this.g, i, i2, i3, i4);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.e(this, consumer);
    }
}
