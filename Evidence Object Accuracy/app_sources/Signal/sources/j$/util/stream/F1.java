package j$.util.stream;

import j$.util.function.Consumer;
import j$.util.function.p;

/* loaded from: classes2.dex */
final class F1 implements L1, AbstractC0045a2 {
    private int a;
    final /* synthetic */ int b;
    final /* synthetic */ p c;

    public F1(int i, p pVar) {
        this.b = i;
        this.c = pVar;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(double d) {
        AbstractC0115s0.D();
        throw null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void accept(int i) {
        this.a = this.c.applyAsInt(this.a, i);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(long j) {
        AbstractC0115s0.L();
        throw null;
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        e((Integer) obj);
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        this.a = this.b;
    }

    @Override // j$.util.stream.AbstractC0045a2
    public final /* synthetic */ void e(Integer num) {
        AbstractC0115s0.G(this, num);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void end() {
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ boolean f() {
        return false;
    }

    @Override // j$.util.function.Supplier
    public final Object get() {
        return Integer.valueOf(this.a);
    }

    @Override // j$.util.stream.L1
    public final void h(L1 l1) {
        accept(((F1) l1).a);
    }
}
