package j$.util.stream;

import j$.util.AbstractC0037n;
import j$.util.E;
import java.util.Comparator;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public abstract class k3 extends m3 implements E {
    public k3(E e, long j, long j2) {
        super(e, j, j2, 0, Math.min(e.estimateSize(), j2));
    }

    public k3(E e, long j, long j2, long j3, long j4) {
        super(e, j, j2, j3, j4);
    }

    protected abstract Object e();

    @Override // j$.util.E
    /* renamed from: forEachRemaining */
    public final void m(Object obj) {
        obj.getClass();
        long j = this.a;
        long j2 = this.e;
        if (j < j2) {
            long j3 = this.d;
            if (j3 < j2) {
                if (j3 < j || ((E) this.c).estimateSize() + j3 > this.b) {
                    while (this.a > this.d) {
                        ((E) this.c).tryAdvance(e());
                        this.d++;
                    }
                    while (this.d < this.e) {
                        ((E) this.c).tryAdvance(obj);
                        this.d++;
                    }
                    return;
                }
                ((E) this.c).forEachRemaining(obj);
                this.d = this.e;
            }
        }
    }

    @Override // j$.util.Spliterator
    public final Comparator getComparator() {
        throw new IllegalStateException();
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ long getExactSizeIfKnown() {
        return AbstractC0037n.h(this);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean hasCharacteristics(int i) {
        return AbstractC0037n.j(this, i);
    }

    @Override // j$.util.E
    /* renamed from: tryAdvance */
    public final boolean k(Object obj) {
        long j;
        obj.getClass();
        if (this.a >= this.e) {
            return false;
        }
        while (true) {
            long j2 = this.a;
            j = this.d;
            if (j2 <= j) {
                break;
            }
            ((E) this.c).tryAdvance(e());
            this.d++;
        }
        if (j >= this.e) {
            return false;
        }
        this.d = j + 1;
        return ((E) this.c).tryAdvance(obj);
    }
}
