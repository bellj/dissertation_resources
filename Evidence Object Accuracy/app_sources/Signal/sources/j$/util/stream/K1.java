package j$.util.stream;

import j$.util.C0035l;
import j$.util.function.Consumer;
import j$.util.function.z;

/* loaded from: classes2.dex */
final class K1 implements L1, AbstractC0050b2 {
    private boolean a;
    private long b;
    final /* synthetic */ z c;

    public K1(z zVar) {
        this.c = zVar;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(double d) {
        AbstractC0115s0.D();
        throw null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(int i) {
        AbstractC0115s0.K();
        throw null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void accept(long j) {
        if (this.a) {
            this.a = false;
        } else {
            j = this.c.applyAsLong(this.b, j);
        }
        this.b = j;
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        j((Long) obj);
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        this.a = true;
        this.b = 0;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void end() {
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ boolean f() {
        return false;
    }

    @Override // j$.util.function.Supplier
    public final Object get() {
        return this.a ? C0035l.a() : C0035l.d(this.b);
    }

    @Override // j$.util.stream.L1
    public final void h(L1 l1) {
        K1 k1 = (K1) l1;
        if (!k1.a) {
            accept(k1.b);
        }
    }

    @Override // j$.util.stream.AbstractC0050b2
    public final /* synthetic */ void j(Long l) {
        AbstractC0115s0.I(this, l);
    }
}
