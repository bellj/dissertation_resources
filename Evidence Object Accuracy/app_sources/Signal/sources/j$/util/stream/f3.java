package j$.util.stream;

import j$.util.AbstractC0037n;
import j$.util.Spliterator;
import j$.util.function.Consumer;
import j$.util.y;

/* loaded from: classes2.dex */
public final class f3 extends k3 implements y {
    public f3(y yVar, long j, long j2) {
        super(yVar, j, j2);
    }

    f3(y yVar, long j, long j2, long j3, long j4) {
        super(yVar, j, j2, j3, j4);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.l(this, consumer);
    }

    @Override // j$.util.stream.m3
    protected final Spliterator d(Spliterator spliterator, long j, long j2, long j3, long j4) {
        return new f3((y) spliterator, j, j2, j3, j4);
    }

    @Override // j$.util.stream.k3
    protected final Object e() {
        return new e3(0);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.d(this, consumer);
    }
}
