package j$.util.stream;

import j$.util.function.C0020c;
import j$.util.function.IntFunction;
import j$.util.function.IntPredicate;
import j$.util.function.r;
import j$.util.function.w;
import j$.util.function.x;
import j$.util.function.y;

/* loaded from: classes2.dex */
final class T extends W1 {
    public final /* synthetic */ int b;
    final /* synthetic */ AbstractC0051c c;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ T(AbstractC0051c cVar, AbstractC0054c2 c2Var, int i) {
        super(c2Var);
        this.b = i;
        this.c = cVar;
    }

    @Override // j$.util.stream.AbstractC0045a2, j$.util.stream.AbstractC0054c2
    public final void accept(int i) {
        switch (this.b) {
            case 0:
                this.a.accept((long) i);
                return;
            case 1:
                ((r) ((C0122u) this.c).n).accept(i);
                this.a.accept(i);
                return;
            case 2:
                this.a.accept((double) i);
                return;
            case 3:
                this.a.accept(((C0020c) ((y) ((C0122u) this.c).n)).f(i));
                return;
            case 4:
                this.a.accept((AbstractC0054c2) ((IntFunction) ((C0118t) this.c).n).apply(i));
                return;
            case 5:
                this.a.accept(((x) ((C0126v) this.c).n).applyAsLong(i));
                return;
            case 6:
                this.a.accept(((C0020c) ((w) ((C0114s) this.c).n)).c(i));
                return;
            case 7:
                IntStream intStream = (IntStream) ((IntFunction) ((C0122u) this.c).n).apply(i);
                if (intStream != null) {
                    try {
                        intStream.sequential().w(new S(1, this));
                    } catch (Throwable th) {
                        try {
                            intStream.close();
                        } catch (Throwable th2) {
                            th.addSuppressed(th2);
                        }
                        throw th;
                    }
                }
                if (intStream != null) {
                    intStream.close();
                    return;
                }
                return;
            default:
                if (((IntPredicate) ((C0122u) this.c).n).test(i)) {
                    this.a.accept(i);
                    return;
                }
                return;
        }
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        switch (this.b) {
            case 7:
                this.a.d(-1);
                return;
            case 8:
                this.a.d(-1);
                return;
            default:
                this.a.d(j);
                return;
        }
    }
}
