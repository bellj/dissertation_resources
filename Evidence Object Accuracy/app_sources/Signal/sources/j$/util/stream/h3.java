package j$.util.stream;

import j$.util.A;
import j$.util.AbstractC0037n;
import j$.util.Spliterator;
import j$.util.function.Consumer;

/* loaded from: classes2.dex */
public final class h3 extends k3 implements A {
    public h3(A a, long j, long j2) {
        super(a, j, j2);
    }

    h3(A a, long j, long j2, long j3, long j4) {
        super(a, j, j2, j3, j4);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.n(this, consumer);
    }

    @Override // j$.util.stream.m3
    protected final Spliterator d(Spliterator spliterator, long j, long j2, long j3, long j4) {
        return new h3((A) spliterator, j, j2, j3, j4);
    }

    @Override // j$.util.stream.k3
    protected final Object e() {
        return new g3(0);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.e(this, consumer);
    }
}
