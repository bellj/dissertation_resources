package j$.util.stream;

import j$.util.function.AbstractC0022e;
import j$.util.function.C0020c;

/* renamed from: j$.util.stream.u1 */
/* loaded from: classes2.dex */
final class C0124u1 extends AbstractC0112r1 {
    final /* synthetic */ AbstractC0022e h;
    final /* synthetic */ double i;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0124u1(int i, C0020c cVar, double d) {
        super(i);
        this.h = cVar;
        this.i = d;
    }

    @Override // j$.util.stream.AbstractC0112r1
    public final L1 u() {
        return new C0128v1(this.i, this.h);
    }
}
