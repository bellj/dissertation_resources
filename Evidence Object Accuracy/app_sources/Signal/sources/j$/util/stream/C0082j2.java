package j$.util.stream;

/* renamed from: j$.util.stream.j2 */
/* loaded from: classes2.dex */
final class C0082j2 extends V1 {
    long b;
    long c;
    final /* synthetic */ C0086k2 d;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0082j2(C0086k2 k2Var, AbstractC0054c2 c2Var) {
        super(c2Var);
        this.d = k2Var;
        this.b = k2Var.m;
        long j = k2Var.n;
        this.c = j < 0 ? Long.MAX_VALUE : j;
    }

    @Override // j$.util.stream.Z1, j$.util.stream.AbstractC0054c2
    public final void accept(double d) {
        long j = this.b;
        if (j == 0) {
            long j2 = this.c;
            if (j2 > 0) {
                this.c = j2 - 1;
                this.a.accept(d);
                return;
            }
            return;
        }
        this.b = j - 1;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        this.a.d(AbstractC0115s0.Z(j, this.d.m, this.c));
    }

    @Override // j$.util.stream.V1, j$.util.stream.AbstractC0054c2
    public final boolean f() {
        return this.c == 0 || this.a.f();
    }
}
