package j$.util.stream;

import j$.util.C0017f;
import j$.util.C0030g;
import j$.util.C0031h;
import j$.util.C0033j;
import j$.util.C0034k;
import j$.util.C0035l;
import j$.util.Optional;
import j$.util.function.BiConsumer;
import j$.util.function.C;
import j$.util.function.Consumer;
import j$.util.function.H;
import j$.util.function.I;
import j$.util.function.IntFunction;
import j$.util.function.J;
import j$.util.function.Predicate;
import j$.util.function.p;
import j$.util.function.z;

/* renamed from: j$.util.stream.q */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0107q implements H, BiConsumer, Predicate, p, I, IntFunction, z, C, J, Consumer {
    public final /* synthetic */ int a;

    public /* synthetic */ C0107q(int i) {
        this.a = i;
    }

    @Override // j$.util.function.I
    public final void a(int i, Object obj) {
        ((C0030g) obj).accept(i);
    }

    @Override // j$.util.function.Consumer
    public final void accept(Object obj) {
    }

    @Override // j$.util.function.H
    public final void accept(Object obj, double d) {
        ((C0017f) obj).accept(d);
    }

    @Override // j$.util.function.J
    public final void accept(Object obj, long j) {
        ((C0031h) obj).accept(j);
    }

    @Override // j$.util.function.BiConsumer
    public final void accept(Object obj, Object obj2) {
        switch (this.a) {
            case 1:
                ((C0017f) obj).a((C0017f) obj2);
                return;
            case 9:
                ((C0030g) obj).a((C0030g) obj2);
                return;
            default:
                ((C0031h) obj).a((C0031h) obj2);
                return;
        }
    }

    public final /* synthetic */ Predicate and(Predicate predicate) {
        switch (this.a) {
            case 2:
                return Predicate.CC.$default$and(this, predicate);
            case 3:
                return Predicate.CC.$default$and(this, predicate);
            case 4:
                return Predicate.CC.$default$and(this, predicate);
            default:
                return Predicate.CC.$default$and(this, predicate);
        }
    }

    public final /* synthetic */ BiConsumer andThen(BiConsumer biConsumer) {
        switch (this.a) {
            case 1:
                return BiConsumer.CC.$default$andThen(this, biConsumer);
            case 9:
                return BiConsumer.CC.$default$andThen(this, biConsumer);
            default:
                return BiConsumer.CC.$default$andThen(this, biConsumer);
        }
    }

    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    public final Object apply(int i) {
        return Integer.valueOf(i);
    }

    public final Object apply(long j) {
        return Long.valueOf(j);
    }

    public final int applyAsInt(int i, int i2) {
        switch (this.a) {
            case 6:
                return Math.min(i, i2);
            case 7:
                return i + i2;
            default:
                return Math.max(i, i2);
        }
    }

    public final long applyAsLong(long j, long j2) {
        switch (this.a) {
            case 12:
                return Math.min(j, j2);
            case 13:
                return Math.max(j, j2);
            default:
                return j + j2;
        }
    }

    public final /* synthetic */ Predicate negate() {
        switch (this.a) {
            case 2:
                return Predicate.CC.$default$negate(this);
            case 3:
                return Predicate.CC.$default$negate(this);
            case 4:
                return Predicate.CC.$default$negate(this);
            default:
                return Predicate.CC.$default$negate(this);
        }
    }

    public final /* synthetic */ Predicate or(Predicate predicate) {
        switch (this.a) {
            case 2:
                return Predicate.CC.$default$or(this, predicate);
            case 3:
                return Predicate.CC.$default$or(this, predicate);
            case 4:
                return Predicate.CC.$default$or(this, predicate);
            default:
                return Predicate.CC.$default$or(this, predicate);
        }
    }

    public final boolean test(Object obj) {
        switch (this.a) {
            case 2:
                return ((C0033j) obj).c();
            case 3:
                return ((C0035l) obj).c();
            case 4:
                return ((Optional) obj).isPresent();
            default:
                return ((C0034k) obj).c();
        }
    }
}
