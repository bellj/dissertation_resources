package j$.util.stream;

/* renamed from: j$.util.stream.p0 */
/* loaded from: classes2.dex */
public enum EnumC0104p0 {
    ANY(true, true),
    ALL(false, false),
    NONE(true, false);
    
    private final boolean a;
    private final boolean b;

    EnumC0104p0(boolean z, boolean z2) {
        this.a = z;
        this.b = z2;
    }
}
