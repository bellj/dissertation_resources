package j$.util.stream;

import j$.util.Spliterator;

/* renamed from: j$.util.stream.k1 */
/* loaded from: classes2.dex */
public final class C0085k1 extends AbstractC0093m1 implements AbstractC0050b2 {
    private final long[] h;

    public C0085k1(Spliterator spliterator, AbstractC0115s0 s0Var, long[] jArr) {
        super(jArr.length, spliterator, s0Var);
        this.h = jArr;
    }

    C0085k1(C0085k1 k1Var, Spliterator spliterator, long j, long j2) {
        super(k1Var, spliterator, j, j2, k1Var.h.length);
        this.h = k1Var.h;
    }

    @Override // j$.util.stream.AbstractC0093m1
    final AbstractC0093m1 a(Spliterator spliterator, long j, long j2) {
        return new C0085k1(this, spliterator, j, j2);
    }

    @Override // j$.util.stream.AbstractC0093m1, j$.util.stream.AbstractC0054c2
    public final void accept(long j) {
        int i = this.f;
        if (i < this.g) {
            long[] jArr = this.h;
            this.f = i + 1;
            jArr[i] = j;
            return;
        }
        throw new IndexOutOfBoundsException(Integer.toString(this.f));
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        j((Long) obj);
    }

    @Override // j$.util.stream.AbstractC0050b2
    public final /* synthetic */ void j(Long l) {
        AbstractC0115s0.I(this, l);
    }
}
