package j$.util.stream;

import java.util.Arrays;
import java.util.Comparator;

/* loaded from: classes2.dex */
final class C2 extends AbstractC0110q2 {
    private Object[] d;
    private int e;

    public C2(AbstractC0054c2 c2Var, Comparator comparator) {
        super(c2Var, comparator);
    }

    @Override // j$.util.function.Consumer
    public final void accept(Object obj) {
        Object[] objArr = this.d;
        int i = this.e;
        this.e = i + 1;
        objArr[i] = obj;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        if (j < 2147483639) {
            this.d = new Object[(int) j];
            return;
        }
        throw new IllegalArgumentException("Stream size exceeds max array size");
    }

    @Override // j$.util.stream.Y1, j$.util.stream.AbstractC0054c2
    public final void end() {
        int i = 0;
        Arrays.sort(this.d, 0, this.e, this.b);
        this.a.d((long) this.e);
        if (!this.c) {
            while (i < this.e) {
                this.a.accept((AbstractC0054c2) this.d[i]);
                i++;
            }
        } else {
            while (i < this.e && !this.a.f()) {
                this.a.accept((AbstractC0054c2) this.d[i]);
                i++;
            }
        }
        this.a.end();
        this.d = null;
    }
}
