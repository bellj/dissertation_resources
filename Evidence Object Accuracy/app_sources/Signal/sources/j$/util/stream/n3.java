package j$.util.stream;

import j$.util.AbstractC0037n;
import j$.util.Spliterator;
import j$.util.function.AbstractC0024g;
import j$.util.function.Consumer;
import j$.util.y;

/* loaded from: classes2.dex */
public final class n3 extends q3 implements y, AbstractC0024g {
    double e;

    public n3(y yVar, long j, long j2) {
        super(yVar, j, j2);
    }

    n3(y yVar, n3 n3Var) {
        super(yVar, n3Var);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.l(this, consumer);
    }

    @Override // j$.util.function.AbstractC0024g
    public final void accept(double d) {
        this.e = d;
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.d(this, consumer);
    }

    @Override // j$.util.stream.s3
    protected final Spliterator o(Spliterator spliterator) {
        return new n3((y) spliterator, this);
    }

    @Override // j$.util.stream.q3
    protected final void q(Object obj) {
        ((AbstractC0024g) obj).accept(this.e);
    }

    @Override // j$.util.stream.q3
    protected final U2 r() {
        return new R2();
    }
}
