package j$.util.stream;

import j$.util.function.B;
import j$.util.function.Consumer;

/* loaded from: classes2.dex */
public final /* synthetic */ class c3 implements AbstractC0050b2 {
    public final /* synthetic */ int a;
    public final /* synthetic */ B b;

    public /* synthetic */ c3(B b, int i) {
        this.a = i;
        this.b = b;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(double d) {
        switch (this.a) {
            case 0:
                AbstractC0115s0.D();
                throw null;
            default:
                AbstractC0115s0.D();
                throw null;
        }
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(int i) {
        switch (this.a) {
            case 0:
                AbstractC0115s0.K();
                throw null;
            default:
                AbstractC0115s0.K();
                throw null;
        }
    }

    @Override // j$.util.stream.AbstractC0050b2, j$.util.stream.AbstractC0054c2
    public final void accept(long j) {
        switch (this.a) {
            case 0:
                this.b.accept(j);
                return;
            default:
                ((J2) this.b).accept(j);
                return;
        }
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        switch (this.a) {
            case 0:
                j((Long) obj);
                return;
            default:
                j((Long) obj);
                return;
        }
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        switch (this.a) {
            case 0:
                return Consumer.CC.$default$andThen(this, consumer);
            default:
                return Consumer.CC.$default$andThen(this, consumer);
        }
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void d(long j) {
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void end() {
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ boolean f() {
        return false;
    }

    @Override // j$.util.stream.AbstractC0050b2
    public final /* synthetic */ void j(Long l) {
        switch (this.a) {
            case 0:
                AbstractC0115s0.I(this, l);
                return;
            default:
                AbstractC0115s0.I(this, l);
                return;
        }
    }
}
