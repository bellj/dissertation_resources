package j$.util.stream;

import j$.util.AbstractC0037n;
import j$.util.C;
import j$.util.Spliterator;
import j$.util.function.B;
import j$.util.function.Consumer;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public final class d3 extends Q2 implements C {
    d3(AbstractC0115s0 s0Var, Spliterator spliterator, boolean z) {
        super(s0Var, spliterator, z);
    }

    public d3(AbstractC0115s0 s0Var, C0042a aVar, boolean z) {
        super(s0Var, aVar, z);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.o(this, consumer);
    }

    @Override // j$.util.C
    /* renamed from: b */
    public final void forEachRemaining(B b) {
        if (this.h != null || this.i) {
            do {
            } while (tryAdvance(b));
            return;
        }
        b.getClass();
        f();
        this.b.s0(this.d, new c3(b, 0));
        this.i = true;
    }

    @Override // j$.util.C
    /* renamed from: c */
    public final boolean tryAdvance(B b) {
        b.getClass();
        boolean d = d();
        if (d) {
            J2 j2 = (J2) this.h;
            long j = this.g;
            int q = j2.q(j);
            b.accept((j2.c == 0 && q == 0) ? ((long[]) j2.e)[(int) j] : ((long[][]) j2.f)[q][(int) (j - j2.d[q])]);
        }
        return d;
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.f(this, consumer);
    }

    @Override // j$.util.stream.Q2
    final void h() {
        J2 j2 = new J2();
        this.h = j2;
        this.e = this.b.t0(new c3(j2, 1));
        this.f = new C0042a(6, this);
    }

    @Override // j$.util.stream.Q2
    final Q2 j(Spliterator spliterator) {
        return new d3(this.b, spliterator, this.a);
    }

    @Override // j$.util.stream.Q2, j$.util.Spliterator
    public final C trySplit() {
        return (C) super.trySplit();
    }
}
