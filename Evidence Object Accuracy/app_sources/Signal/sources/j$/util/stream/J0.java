package j$.util.stream;

import j$.util.E;
import j$.util.Spliterator;
import j$.util.function.Consumer;
import j$.util.function.IntFunction;

/* loaded from: classes2.dex */
public final class J0 extends L0 implements AbstractC0139y0 {
    public J0(AbstractC0139y0 y0Var, AbstractC0139y0 y0Var2) {
        super(y0Var, y0Var2);
    }

    /* renamed from: d */
    public final /* synthetic */ void i(Integer[] numArr, int i) {
        AbstractC0115s0.O(this, numArr, i);
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ void forEach(Consumer consumer) {
        AbstractC0115s0.R(this, consumer);
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ B0 n(long j, long j2, IntFunction intFunction) {
        return AbstractC0115s0.U(this, j, j2);
    }

    @Override // j$.util.stream.A0
    public final Object newArray(int i) {
        return new int[i];
    }

    @Override // j$.util.stream.B0
    /* renamed from: spliterator */
    public final E mo148spliterator() {
        return new C0044a1(this);
    }

    @Override // j$.util.stream.B0
    /* renamed from: spliterator */
    public final Spliterator mo148spliterator() {
        return new C0044a1(this);
    }
}
