package j$.util.stream;

import j$.util.T;
import j$.util.function.AbstractC0024g;
import j$.util.function.Consumer;
import j$.util.y;
import java.util.Arrays;
import java.util.Iterator;

/* loaded from: classes2.dex */
public class F2 extends L2 implements AbstractC0024g {
    public F2() {
    }

    public F2(int i) {
        super(i);
    }

    @Override // j$.util.function.AbstractC0024g
    public void accept(double d) {
        t();
        int i = this.b;
        this.b = i + 1;
        ((double[]) this.e)[i] = d;
    }

    @Override // j$.lang.Iterable
    public final void forEach(Consumer consumer) {
        if (consumer instanceof AbstractC0024g) {
            g((AbstractC0024g) consumer);
        } else if (!A3.a) {
            mo150spliterator().forEachRemaining(consumer);
        } else {
            A3.a(getClass(), "{0} calling SpinedBuffer.OfDouble.forEach(Consumer)");
            throw null;
        }
    }

    @Override // java.lang.Iterable, j$.lang.Iterable
    public final Iterator iterator() {
        return T.f(mo150spliterator());
    }

    @Override // j$.util.stream.L2
    public final Object newArray(int i) {
        return new double[i];
    }

    /* access modifiers changed from: protected */
    @Override // j$.util.stream.L2
    public final void o(Object obj, int i, int i2, Object obj2) {
        double[] dArr = (double[]) obj;
        AbstractC0024g gVar = (AbstractC0024g) obj2;
        while (i < i2) {
            gVar.accept(dArr[i]);
            i++;
        }
    }

    /* access modifiers changed from: protected */
    @Override // j$.util.stream.L2
    public final int p(Object obj) {
        return ((double[]) obj).length;
    }

    @Override // j$.util.stream.L2
    protected final Object[] s() {
        return new double[8];
    }

    @Override // java.lang.Object
    public final String toString() {
        double[] dArr = (double[]) b();
        return dArr.length < 200 ? String.format("%s[length=%d, chunks=%d]%s", getClass().getSimpleName(), Integer.valueOf(dArr.length), Integer.valueOf(this.c), Arrays.toString(dArr)) : String.format("%s[length=%d, chunks=%d]%s...", getClass().getSimpleName(), Integer.valueOf(dArr.length), Integer.valueOf(this.c), Arrays.toString(Arrays.copyOf(dArr, 200)));
    }

    /* renamed from: u */
    public y mo150spliterator() {
        return new E2(this, 0, this.c, 0, this.b);
    }
}
