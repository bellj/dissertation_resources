package j$.util.stream;

import j$.util.function.r;

/* loaded from: classes2.dex */
final class S2 extends U2 implements r {
    final int[] c = new int[128];

    @Override // j$.util.stream.U2
    public final void a(Object obj, long j) {
        r rVar = (r) obj;
        for (int i = 0; ((long) i) < j; i++) {
            rVar.accept(this.c[i]);
        }
    }

    @Override // j$.util.function.r
    public final void accept(int i) {
        int[] iArr = this.c;
        int i2 = this.b;
        this.b = i2 + 1;
        iArr[i2] = i;
    }
}
