package j$.util.stream;

/* renamed from: j$.util.stream.d2 */
/* loaded from: classes2.dex */
final class C0058d2 extends Y1 {
    long b;
    long c;
    final /* synthetic */ C0062e2 d;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0058d2(C0062e2 e2Var, AbstractC0054c2 c2Var) {
        super(c2Var);
        this.d = e2Var;
        this.b = e2Var.m;
        long j = e2Var.n;
        this.c = j < 0 ? Long.MAX_VALUE : j;
    }

    @Override // j$.util.function.Consumer
    public final void accept(Object obj) {
        long j = this.b;
        if (j == 0) {
            long j2 = this.c;
            if (j2 > 0) {
                this.c = j2 - 1;
                this.a.accept((AbstractC0054c2) obj);
                return;
            }
            return;
        }
        this.b = j - 1;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        this.a.d(AbstractC0115s0.Z(j, this.d.m, this.c));
    }

    @Override // j$.util.stream.Y1, j$.util.stream.AbstractC0054c2
    public final boolean f() {
        return this.c == 0 || this.a.f();
    }
}
