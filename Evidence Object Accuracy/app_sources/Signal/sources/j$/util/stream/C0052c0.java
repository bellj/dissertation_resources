package j$.util.stream;

import j$.util.C0017f;
import j$.util.C0030g;
import j$.util.C0031h;
import j$.util.function.AbstractC0022e;
import j$.util.function.AbstractC0025h;
import j$.util.function.BiConsumer;
import j$.util.function.BiFunction;
import j$.util.function.BinaryOperator;
import j$.util.function.C;
import j$.util.function.Consumer;
import j$.util.function.Function;
import j$.util.function.IntFunction;
import j$.util.function.Supplier;
import j$.util.function.ToLongFunction;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/* renamed from: j$.util.stream.c0 */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0052c0 implements BiConsumer, C, BinaryOperator, IntFunction, ToLongFunction, Consumer, Supplier, AbstractC0022e, AbstractC0025h {
    public final /* synthetic */ int a;

    public /* synthetic */ C0052c0(int i) {
        this.a = i;
    }

    @Override // j$.util.function.Consumer
    public final void accept(Object obj) {
    }

    @Override // j$.util.function.BiConsumer
    public final void accept(Object obj, Object obj2) {
        switch (this.a) {
            case 0:
                long[] jArr = (long[]) obj;
                long[] jArr2 = (long[]) obj2;
                jArr[0] = jArr[0] + jArr2[0];
                jArr[1] = jArr[1] + jArr2[1];
                return;
            case 16:
                ((Collection) obj).add(obj2);
                return;
            case 20:
                ((List) obj).add(obj2);
                return;
            case 23:
                ((Set) obj).add(obj2);
                return;
            case 25:
                ((LinkedHashSet) obj).add(obj2);
                return;
            default:
                ((LinkedHashSet) obj).addAll((LinkedHashSet) obj2);
                return;
        }
    }

    public final /* synthetic */ BiConsumer andThen(BiConsumer biConsumer) {
        switch (this.a) {
            case 0:
                return BiConsumer.CC.$default$andThen(this, biConsumer);
            case 16:
                return BiConsumer.CC.$default$andThen(this, biConsumer);
            case 20:
                return BiConsumer.CC.$default$andThen(this, biConsumer);
            case 23:
                return BiConsumer.CC.$default$andThen(this, biConsumer);
            case 25:
                return BiConsumer.CC.$default$andThen(this, biConsumer);
            default:
                return BiConsumer.CC.$default$andThen(this, biConsumer);
        }
    }

    public final /* synthetic */ BiFunction andThen(Function function) {
        switch (this.a) {
            case 2:
                return BiFunction.CC.$default$andThen(this, function);
            case 3:
            case 5:
            default:
                return BiFunction.CC.$default$andThen(this, function);
            case 4:
                return BiFunction.CC.$default$andThen(this, function);
            case 6:
                return BiFunction.CC.$default$andThen(this, function);
        }
    }

    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        switch (this.a) {
            case 14:
                return Consumer.CC.$default$andThen(this, consumer);
            default:
                return Consumer.CC.$default$andThen(this, consumer);
        }
    }

    public final Object apply(double d) {
        return Double.valueOf(d);
    }

    public final Object apply(int i) {
        switch (this.a) {
            case 8:
                int i2 = U1.l;
                return new Object[i];
            case 9:
            default:
                int i3 = C0086k2.o;
                return new Double[i];
            case 10:
                return new Object[i];
            case 11:
                int i4 = C0070g2.o;
                return new Integer[i];
            case 12:
                int i5 = C0078i2.o;
                return new Long[i];
        }
    }

    public final Object apply(long j) {
        switch (this.a) {
            case 1:
                return AbstractC0112r1.m(j);
            case 2:
            default:
                return AbstractC0112r1.t(j);
            case 3:
                return AbstractC0112r1.s(j);
        }
    }

    public final Object apply(Object obj, Object obj2) {
        switch (this.a) {
            case 2:
                return new I0((AbstractC0135x0) obj, (AbstractC0135x0) obj2);
            case 3:
            case 5:
            default:
                return new M0((B0) obj, (B0) obj2);
            case 4:
                return new J0((AbstractC0139y0) obj, (AbstractC0139y0) obj2);
            case 6:
                return new K0((AbstractC0143z0) obj, (AbstractC0143z0) obj2);
        }
    }

    public final double applyAsDouble(double d, double d2) {
        switch (this.a) {
            case 27:
                return Math.min(d, d2);
            default:
                return Math.max(d, d2);
        }
    }

    public final long applyAsLong(Object obj) {
        int i = U1.l;
        return 1;
    }

    public final Object get() {
        switch (this.a) {
            case 17:
                return new C0017f();
            case 18:
                return new C0030g();
            case 19:
                return new ArrayList();
            case 20:
            default:
                return new LinkedHashSet();
            case 21:
                return new C0031h();
            case 22:
                return new HashSet();
        }
    }
}
