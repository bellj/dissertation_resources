package j$.util.stream;

/* renamed from: j$.util.stream.l */
/* loaded from: classes2.dex */
final class C0087l extends Y1 {
    boolean b;
    Object c;

    public C0087l(AbstractC0054c2 c2Var) {
        super(c2Var);
    }

    @Override // j$.util.function.Consumer
    public final void accept(Object obj) {
        if (obj != null) {
            Object obj2 = this.c;
            if (obj2 == null || !obj.equals(obj2)) {
                AbstractC0054c2 c2Var = this.a;
                this.c = obj;
                c2Var.accept((AbstractC0054c2) obj);
            }
        } else if (!this.b) {
            this.b = true;
            AbstractC0054c2 c2Var2 = this.a;
            this.c = null;
            c2Var2.accept((AbstractC0054c2) null);
        }
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        this.b = false;
        this.c = null;
        this.a.d(-1);
    }

    @Override // j$.util.stream.Y1, j$.util.stream.AbstractC0054c2
    public final void end() {
        this.b = false;
        this.c = null;
        this.a.end();
    }
}
