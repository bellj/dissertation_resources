package j$.util.stream;

import j$.util.Spliterator;
import java.util.concurrent.CountedCompleter;
import java.util.concurrent.ForkJoinPool;

/* renamed from: j$.util.stream.f */
/* loaded from: classes2.dex */
public abstract class AbstractC0063f extends CountedCompleter {
    static final int g = (ForkJoinPool.getCommonPoolParallelism() << 2);
    protected final AbstractC0115s0 a;
    protected Spliterator b;
    protected long c;
    protected AbstractC0063f d;
    protected AbstractC0063f e;
    private Object f;

    public AbstractC0063f(AbstractC0063f fVar, Spliterator spliterator) {
        super(fVar);
        this.b = spliterator;
        this.a = fVar.a;
        this.c = fVar.c;
    }

    public AbstractC0063f(AbstractC0115s0 s0Var, Spliterator spliterator) {
        super(null);
        this.a = s0Var;
        this.b = spliterator;
        this.c = 0;
    }

    public static long f(long j) {
        long j2 = j / ((long) g);
        if (j2 > 0) {
            return j2;
        }
        return 1;
    }

    /* access modifiers changed from: protected */
    public abstract Object a();

    public Object b() {
        return this.f;
    }

    public final AbstractC0063f c() {
        return (AbstractC0063f) getCompleter();
    }

    @Override // java.util.concurrent.CountedCompleter
    public void compute() {
        Spliterator trySplit;
        Spliterator spliterator = this.b;
        long estimateSize = spliterator.estimateSize();
        long j = this.c;
        if (j == 0) {
            j = f(estimateSize);
            this.c = j;
        }
        boolean z = false;
        AbstractC0063f fVar = this;
        while (estimateSize > j && (trySplit = spliterator.trySplit()) != null) {
            AbstractC0063f d = fVar.d(trySplit);
            fVar.d = d;
            AbstractC0063f d2 = fVar.d(spliterator);
            fVar.e = d2;
            fVar.setPendingCount(1);
            if (z) {
                spliterator = trySplit;
                fVar = d;
                d = d2;
            } else {
                fVar = d2;
            }
            z = !z;
            d.fork();
            estimateSize = spliterator.estimateSize();
        }
        fVar.e(fVar.a());
        fVar.tryComplete();
    }

    /* access modifiers changed from: protected */
    public abstract AbstractC0063f d(Spliterator spliterator);

    public void e(Object obj) {
        this.f = obj;
    }

    @Override // java.util.concurrent.CountedCompleter, java.util.concurrent.ForkJoinTask
    public Object getRawResult() {
        return this.f;
    }

    @Override // java.util.concurrent.CountedCompleter
    public void onCompletion(CountedCompleter countedCompleter) {
        this.b = null;
        this.e = null;
        this.d = null;
    }

    @Override // java.util.concurrent.CountedCompleter, java.util.concurrent.ForkJoinTask
    protected final void setRawResult(Object obj) {
        if (obj != null) {
            throw new IllegalStateException();
        }
    }
}
