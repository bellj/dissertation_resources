package j$.util.stream;

import j$.util.Spliterator;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: j$.util.stream.r0 */
/* loaded from: classes2.dex */
final class C0111r0 extends AbstractC0055d {
    private final C0108q0 j;

    public C0111r0(C0108q0 q0Var, AbstractC0115s0 s0Var, Spliterator spliterator) {
        super(s0Var, spliterator);
        this.j = q0Var;
    }

    C0111r0(C0111r0 r0Var, Spliterator spliterator) {
        super(r0Var, spliterator);
        this.j = r0Var.j;
    }

    @Override // j$.util.stream.AbstractC0063f
    public final Object a() {
        Boolean valueOf;
        AbstractC0115s0 s0Var = this.a;
        AbstractC0100o0 o0Var = (AbstractC0100o0) this.j.b.get();
        s0Var.s0(this.b, o0Var);
        boolean z = o0Var.b;
        if (z == this.j.a.b && (valueOf = Boolean.valueOf(z)) != null) {
            AtomicReference atomicReference = this.h;
            while (!atomicReference.compareAndSet(null, valueOf) && atomicReference.get() == null) {
            }
        }
        return null;
    }

    @Override // j$.util.stream.AbstractC0063f
    public final AbstractC0063f d(Spliterator spliterator) {
        return new C0111r0(this, spliterator);
    }

    @Override // j$.util.stream.AbstractC0055d
    protected final Object i() {
        return Boolean.valueOf(!this.j.a.b);
    }
}
