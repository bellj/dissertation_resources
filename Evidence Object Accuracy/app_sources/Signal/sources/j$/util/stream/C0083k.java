package j$.util.stream;

import j$.util.concurrent.ConcurrentHashMap;
import j$.util.function.AbstractC0026i;
import j$.util.function.BiConsumer;
import j$.util.function.Consumer;
import j$.util.function.D;
import j$.util.function.IntPredicate;
import j$.util.function.Predicate;
import j$.util.function.Supplier;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: j$.util.stream.k */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0083k implements Consumer, Supplier {
    public final /* synthetic */ int a;
    public final /* synthetic */ Object b;
    public final /* synthetic */ Object c;

    public /* synthetic */ C0083k(int i, Object obj, Object obj2) {
        this.a = i;
        this.b = obj;
        this.c = obj2;
    }

    @Override // j$.util.function.Consumer
    public final void accept(Object obj) {
        switch (this.a) {
            case 0:
                AtomicBoolean atomicBoolean = (AtomicBoolean) this.b;
                ConcurrentHashMap concurrentHashMap = (ConcurrentHashMap) this.c;
                if (obj == null) {
                    atomicBoolean.set(true);
                    return;
                } else {
                    concurrentHashMap.putIfAbsent(obj, Boolean.TRUE);
                    return;
                }
            case 5:
                ((BiConsumer) this.b).accept(this.c, obj);
                return;
            default:
                ((X2) this.b).n((Consumer) this.c, obj);
                return;
        }
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        switch (this.a) {
            case 0:
                return Consumer.CC.$default$andThen(this, consumer);
            case 5:
                return Consumer.CC.$default$andThen(this, consumer);
            default:
                return Consumer.CC.$default$andThen(this, consumer);
        }
    }

    @Override // j$.util.function.Supplier
    public final Object get() {
        switch (this.a) {
            case 1:
                return new C0088l0((IntPredicate) this.c, (EnumC0104p0) this.b);
            case 2:
                return new C0084k0((Predicate) this.c, (EnumC0104p0) this.b);
            case 3:
                return new C0096n0((AbstractC0026i) this.c, (EnumC0104p0) this.b);
            default:
                return new C0092m0((D) this.c, (EnumC0104p0) this.b);
        }
    }
}
