package j$.util.stream;

import j$.util.function.BiConsumer;
import j$.util.function.BiFunction;
import j$.util.function.BinaryOperator;
import j$.util.function.H;
import j$.util.function.I;
import j$.util.function.J;
import j$.util.function.Supplier;

/* renamed from: j$.util.stream.s1 */
/* loaded from: classes2.dex */
public final class C0116s1 extends AbstractC0112r1 {
    public final /* synthetic */ int h;
    final /* synthetic */ Object i;
    final /* synthetic */ Object j;
    final /* synthetic */ Object k;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ C0116s1(int i, Object obj, Object obj2, Object obj3, int i2) {
        super(i);
        this.h = i2;
        this.i = obj;
        this.k = obj2;
        this.j = obj3;
    }

    @Override // j$.util.stream.AbstractC0112r1
    public final L1 u() {
        switch (this.h) {
            case 0:
                return new C0120t1((Supplier) this.j, (J) this.k, (BinaryOperator) this.i);
            case 1:
                return new C0140y1((Supplier) this.j, (H) this.k, (BinaryOperator) this.i);
            case 2:
                return new C0144z1(this.j, (BiFunction) this.k, (BinaryOperator) this.i);
            case 3:
                return new D1((Supplier) this.j, (BiConsumer) this.k, (BiConsumer) this.i);
            default:
                return new H1((Supplier) this.j, (I) this.k, (BinaryOperator) this.i);
        }
    }
}
