package j$.util.stream;

import j$.util.A;
import j$.util.T;
import j$.util.function.Consumer;
import j$.util.function.r;
import java.util.Arrays;
import java.util.Iterator;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public class H2 extends L2 implements r {
    public H2() {
    }

    public H2(int i) {
        super(i);
    }

    @Override // j$.util.function.r
    public void accept(int i) {
        t();
        int i2 = this.b;
        this.b = i2 + 1;
        ((int[]) this.e)[i2] = i;
    }

    @Override // j$.lang.Iterable
    public final void forEach(Consumer consumer) {
        if (consumer instanceof r) {
            g((r) consumer);
        } else if (!A3.a) {
            mo150spliterator().forEachRemaining(consumer);
        } else {
            A3.a(getClass(), "{0} calling SpinedBuffer.OfInt.forEach(Consumer)");
            throw null;
        }
    }

    @Override // java.lang.Iterable, j$.lang.Iterable
    public final Iterator iterator() {
        return T.g(mo150spliterator());
    }

    @Override // j$.util.stream.L2
    public final Object newArray(int i) {
        return new int[i];
    }

    @Override // j$.util.stream.L2
    public final void o(Object obj, int i, int i2, Object obj2) {
        int[] iArr = (int[]) obj;
        r rVar = (r) obj2;
        while (i < i2) {
            rVar.accept(iArr[i]);
            i++;
        }
    }

    @Override // j$.util.stream.L2
    public final int p(Object obj) {
        return ((int[]) obj).length;
    }

    @Override // j$.util.stream.L2
    protected final Object[] s() {
        return new int[8];
    }

    @Override // java.lang.Object
    public final String toString() {
        int[] iArr = (int[]) b();
        return iArr.length < 200 ? String.format("%s[length=%d, chunks=%d]%s", getClass().getSimpleName(), Integer.valueOf(iArr.length), Integer.valueOf(this.c), Arrays.toString(iArr)) : String.format("%s[length=%d, chunks=%d]%s...", getClass().getSimpleName(), Integer.valueOf(iArr.length), Integer.valueOf(this.c), Arrays.toString(Arrays.copyOf(iArr, 200)));
    }

    /* renamed from: u */
    public A mo150spliterator() {
        return new G2(this, 0, this.c, 0, this.b);
    }
}
