package j$.util.stream;

import j$.util.A;
import j$.util.AbstractC0037n;
import j$.util.Spliterator;
import j$.util.function.Consumer;
import j$.util.function.r;

/* loaded from: classes2.dex */
public final class o3 extends q3 implements A, r {
    int e;

    public o3(A a, long j, long j2) {
        super(a, j, j2);
    }

    o3(A a, o3 o3Var) {
        super(a, o3Var);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.n(this, consumer);
    }

    @Override // j$.util.function.r
    public final void accept(int i) {
        this.e = i;
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.e(this, consumer);
    }

    @Override // j$.util.stream.s3
    protected final Spliterator o(Spliterator spliterator) {
        return new o3((A) spliterator, this);
    }

    @Override // j$.util.stream.q3
    protected final void q(Object obj) {
        ((r) obj).accept(this.e);
    }

    @Override // j$.util.stream.q3
    protected final U2 r() {
        return new S2();
    }
}
