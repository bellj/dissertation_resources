package j$.util.stream;

import j$.util.Spliterator;
import j$.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountedCompleter;

/* loaded from: classes2.dex */
public final class P extends CountedCompleter {
    public static final /* synthetic */ int h;
    private final AbstractC0115s0 a;
    private Spliterator b;
    private final long c;
    private final ConcurrentHashMap d;
    private final AbstractC0054c2 e;
    private final P f;
    private B0 g;

    P(P p, Spliterator spliterator, P p2) {
        super(p);
        this.a = p.a;
        this.b = spliterator;
        this.c = p.c;
        this.d = p.d;
        this.e = p.e;
        this.f = p2;
    }

    public P(AbstractC0115s0 s0Var, Spliterator spliterator, AbstractC0054c2 c2Var) {
        super(null);
        this.a = s0Var;
        this.b = spliterator;
        this.c = AbstractC0063f.f(spliterator.estimateSize());
        this.d = new ConcurrentHashMap(Math.max(16, AbstractC0063f.g << 1));
        this.e = c2Var;
        this.f = null;
    }

    @Override // java.util.concurrent.CountedCompleter
    public final void compute() {
        Spliterator trySplit;
        Spliterator spliterator = this.b;
        long j = this.c;
        boolean z = false;
        P p = this;
        while (spliterator.estimateSize() > j && (trySplit = spliterator.trySplit()) != null) {
            P p2 = new P(p, trySplit, p.f);
            P p3 = new P(p, spliterator, p2);
            p.addToPendingCount(1);
            p3.addToPendingCount(1);
            p.d.put(p2, p3);
            if (p.f != null) {
                p2.addToPendingCount(1);
                if (p.d.replace(p.f, p, p2)) {
                    p.addToPendingCount(-1);
                } else {
                    p2.addToPendingCount(-1);
                }
            }
            if (z) {
                spliterator = trySplit;
                p = p2;
                p2 = p3;
            } else {
                p = p3;
            }
            z = !z;
            p2.fork();
        }
        if (p.getPendingCount() > 0) {
            C0047b bVar = new C0047b(18);
            AbstractC0115s0 s0Var = p.a;
            AbstractC0131w0 o0 = s0Var.o0(s0Var.d0(spliterator), bVar);
            p.a.s0(spliterator, o0);
            p.g = o0.mo149build();
            p.b = null;
        }
        p.tryComplete();
    }

    @Override // java.util.concurrent.CountedCompleter
    public final void onCompletion(CountedCompleter countedCompleter) {
        B0 b0 = this.g;
        if (b0 != null) {
            b0.forEach(this.e);
            this.g = null;
        } else {
            Spliterator spliterator = this.b;
            if (spliterator != null) {
                this.a.s0(spliterator, this.e);
                this.b = null;
            }
        }
        P p = (P) this.d.remove(this);
        if (p != null) {
            p.tryComplete();
        }
    }
}
