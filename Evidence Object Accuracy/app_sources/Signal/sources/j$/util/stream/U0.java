package j$.util.stream;

import j$.util.function.IntFunction;

/* loaded from: classes2.dex */
public abstract class U0 implements B0 {
    @Override // j$.util.stream.B0
    public B0 a(int i) {
        throw new IndexOutOfBoundsException();
    }

    public final void c(int i, Object obj) {
    }

    @Override // j$.util.stream.B0
    public final long count() {
        return 0;
    }

    public final void g(Object obj) {
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ int k() {
        return 0;
    }

    @Override // j$.util.stream.B0
    public final Object[] m(IntFunction intFunction) {
        return (Object[]) intFunction.apply(0);
    }

    @Override // j$.util.stream.B0
    public /* synthetic */ B0 n(long j, long j2, IntFunction intFunction) {
        return AbstractC0115s0.W(this, j, j2, intFunction);
    }
}
