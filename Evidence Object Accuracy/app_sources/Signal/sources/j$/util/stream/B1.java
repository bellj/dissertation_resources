package j$.util.stream;

import j$.util.function.BiConsumer;
import j$.util.function.BinaryOperator;
import j$.util.function.Supplier;
import j$.util.stream.Collector;

/* loaded from: classes2.dex */
final class B1 extends AbstractC0112r1 {
    final /* synthetic */ BinaryOperator h;
    final /* synthetic */ BiConsumer i;
    final /* synthetic */ Supplier j;
    final /* synthetic */ Collector k;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public B1(int i, BinaryOperator binaryOperator, BiConsumer biConsumer, Supplier supplier, Collector collector) {
        super(i);
        this.h = binaryOperator;
        this.i = biConsumer;
        this.j = supplier;
        this.k = collector;
    }

    @Override // j$.util.stream.AbstractC0112r1, j$.util.stream.x3
    public final int b() {
        if (this.k.characteristics().contains(Collector.Characteristics.UNORDERED)) {
            return P2.r;
        }
        return 0;
    }

    @Override // j$.util.stream.AbstractC0112r1
    public final L1 u() {
        return new C1(this.j, this.i, this.h);
    }
}
