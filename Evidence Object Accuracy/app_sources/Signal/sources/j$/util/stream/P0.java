package j$.util.stream;

import j$.util.E;
import j$.util.Spliterator;
import j$.util.function.AbstractC0024g;
import j$.util.function.Consumer;
import j$.util.function.IntFunction;
import j$.util.y;

/* loaded from: classes2.dex */
public final class P0 extends F2 implements AbstractC0135x0, AbstractC0119t0 {
    @Override // j$.util.stream.A0, j$.util.stream.B0
    public final A0 a(int i) {
        throw new IndexOutOfBoundsException();
    }

    @Override // j$.util.stream.F2, j$.util.function.AbstractC0024g
    public final void accept(double d) {
        super.accept(d);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(int i) {
        AbstractC0115s0.K();
        throw null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(long j) {
        AbstractC0115s0.L();
        throw null;
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        l((Double) obj);
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // j$.util.stream.L2, j$.util.stream.A0
    public final Object b() {
        return (double[]) super.b();
    }

    @Override // j$.util.stream.AbstractC0119t0, j$.util.stream.AbstractC0131w0
    /* renamed from: build */
    public final B0 mo149build() {
        return this;
    }

    @Override // j$.util.stream.AbstractC0119t0, j$.util.stream.AbstractC0131w0
    /* renamed from: build */
    public final AbstractC0135x0 mo149build() {
        return this;
    }

    @Override // j$.util.stream.L2, j$.util.stream.A0
    public final void c(int i, Object obj) {
        super.c(i, (double[]) obj);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        clear();
        r(j);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void end() {
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ boolean f() {
        return false;
    }

    @Override // j$.util.stream.L2, j$.util.stream.A0
    public final void g(Object obj) {
        super.g((AbstractC0024g) obj);
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ int k() {
        return 0;
    }

    @Override // j$.util.stream.Z1
    public final /* synthetic */ void l(Double d) {
        AbstractC0115s0.E(this, d);
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ Object[] m(IntFunction intFunction) {
        return AbstractC0115s0.M(this, intFunction);
    }

    @Override // j$.util.stream.B0
    public final /* synthetic */ B0 n(long j, long j2, IntFunction intFunction) {
        return AbstractC0115s0.T(this, j, j2);
    }

    @Override // j$.util.stream.F2, j$.util.stream.L2, java.lang.Iterable, j$.lang.Iterable
    /* renamed from: spliterator */
    public final E mo150spliterator() {
        return super.mo150spliterator();
    }

    @Override // j$.util.stream.F2, j$.util.stream.L2, java.lang.Iterable, j$.lang.Iterable
    /* renamed from: spliterator */
    public final Spliterator mo150spliterator() {
        return super.mo150spliterator();
    }

    @Override // j$.util.stream.F2
    public final y u() {
        return super.mo150spliterator();
    }

    /* renamed from: v */
    public final /* synthetic */ void i(Double[] dArr, int i) {
        AbstractC0115s0.N(this, dArr, i);
    }
}
