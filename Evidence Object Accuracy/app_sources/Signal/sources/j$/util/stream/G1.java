package j$.util.stream;

import j$.util.C0034k;
import j$.util.function.Consumer;
import j$.util.function.p;

/* loaded from: classes2.dex */
final class G1 implements L1, AbstractC0045a2 {
    private boolean a;
    private int b;
    final /* synthetic */ p c;

    public G1(p pVar) {
        this.c = pVar;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(double d) {
        AbstractC0115s0.D();
        throw null;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void accept(int i) {
        if (this.a) {
            this.a = false;
        } else {
            i = this.c.applyAsInt(this.b, i);
        }
        this.b = i;
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void accept(long j) {
        AbstractC0115s0.L();
        throw null;
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        e((Integer) obj);
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        this.a = true;
        this.b = 0;
    }

    @Override // j$.util.stream.AbstractC0045a2
    public final /* synthetic */ void e(Integer num) {
        AbstractC0115s0.G(this, num);
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ void end() {
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final /* synthetic */ boolean f() {
        return false;
    }

    @Override // j$.util.function.Supplier
    public final Object get() {
        return this.a ? C0034k.a() : C0034k.d(this.b);
    }

    @Override // j$.util.stream.L1
    public final void h(L1 l1) {
        G1 g1 = (G1) l1;
        if (!g1.a) {
            accept(g1.b);
        }
    }
}
