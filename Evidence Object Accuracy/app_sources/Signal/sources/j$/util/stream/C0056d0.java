package j$.util.stream;

import j$.util.function.B;
import j$.util.function.C;
import j$.util.function.C0020c;
import j$.util.function.D;
import j$.util.function.E;
import j$.util.function.F;
import j$.util.function.G;

/* renamed from: j$.util.stream.d0 */
/* loaded from: classes2.dex */
final class C0056d0 extends X1 {
    public final /* synthetic */ int b;
    final /* synthetic */ AbstractC0051c c;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ C0056d0(AbstractC0051c cVar, AbstractC0054c2 c2Var, int i) {
        super(c2Var);
        this.b = i;
        this.c = cVar;
    }

    @Override // j$.util.stream.AbstractC0050b2, j$.util.stream.AbstractC0054c2
    public final void accept(long j) {
        switch (this.b) {
            case 0:
                this.a.accept((double) j);
                return;
            case 1:
                this.a.accept(((G) ((C0126v) this.c).n).applyAsLong(j));
                return;
            case 2:
                this.a.accept((AbstractC0054c2) ((C) ((C0118t) this.c).n).apply(j));
                return;
            case 3:
                this.a.accept(((C0020c) ((F) ((C0122u) this.c).n)).g(j));
                return;
            case 4:
                this.a.accept(((C0020c) ((E) ((C0114s) this.c).n)).d(j));
                return;
            case 5:
                AbstractC0080j0 j0Var = (AbstractC0080j0) ((C) ((C0126v) this.c).n).apply(j);
                if (j0Var != null) {
                    try {
                        j0Var.sequential().p(new C0048b0(1, this));
                    } catch (Throwable th) {
                        try {
                            ((AbstractC0051c) j0Var).close();
                        } catch (Throwable th2) {
                            th.addSuppressed(th2);
                        }
                        throw th;
                    }
                }
                if (j0Var != null) {
                    ((AbstractC0051c) j0Var).close();
                    return;
                }
                return;
            case 6:
                if (((C0020c) ((D) ((C0126v) this.c).n)).j(j)) {
                    this.a.accept(j);
                    return;
                }
                return;
            default:
                ((B) ((C0126v) this.c).n).accept(j);
                this.a.accept(j);
                return;
        }
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        switch (this.b) {
            case 5:
                this.a.d(-1);
                return;
            case 6:
                this.a.d(-1);
                return;
            default:
                this.a.d(j);
                return;
        }
    }
}
