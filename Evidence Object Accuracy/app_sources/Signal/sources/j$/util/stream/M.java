package j$.util.stream;

import j$.util.function.B;

/* loaded from: classes2.dex */
public final class M extends O implements AbstractC0050b2 {
    final B b;

    public M(B b, boolean z) {
        super(z);
        this.b = b;
    }

    @Override // j$.util.stream.O, j$.util.stream.AbstractC0054c2
    public final void accept(long j) {
        this.b.accept(j);
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        j((Long) obj);
    }

    @Override // j$.util.stream.AbstractC0050b2
    public final /* synthetic */ void j(Long l) {
        AbstractC0115s0.I(this, l);
    }
}
