package j$.util.stream;

import j$.util.Spliterator;
import j$.util.function.q;
import j$.util.function.r;

/* loaded from: classes2.dex */
public final class W extends Z {
    public W(Spliterator spliterator, int i) {
        super(spliterator, i);
    }

    @Override // j$.util.stream.Z, j$.util.stream.IntStream
    public final void B(q qVar) {
        if (!isParallel()) {
            Z.M0(I0()).i(qVar);
        } else {
            super.B(qVar);
        }
    }

    @Override // j$.util.stream.AbstractC0051c
    final boolean F0() {
        throw new UnsupportedOperationException();
    }

    @Override // j$.util.stream.AbstractC0051c
    public final AbstractC0054c2 G0(int i, AbstractC0054c2 c2Var) {
        throw new UnsupportedOperationException();
    }

    @Override // j$.util.stream.AbstractC0051c, j$.util.stream.AbstractC0071h
    public final /* bridge */ /* synthetic */ IntStream parallel() {
        parallel();
        return this;
    }

    @Override // j$.util.stream.AbstractC0051c, j$.util.stream.AbstractC0071h
    public final /* bridge */ /* synthetic */ IntStream sequential() {
        sequential();
        return this;
    }

    @Override // j$.util.stream.Z, j$.util.stream.IntStream
    public final void w(r rVar) {
        if (!isParallel()) {
            Z.M0(I0()).i(rVar);
        } else {
            super.w(rVar);
        }
    }
}
