package j$.util.stream;

import j$.util.function.AbstractC0024g;
import j$.util.function.B;
import j$.util.function.Function;
import j$.util.function.r;
import java.util.HashSet;
import java.util.Set;

/* renamed from: j$.util.stream.m */
/* loaded from: classes2.dex */
final class C0091m extends Y1 {
    public final /* synthetic */ int b = 0;
    Object c;
    final /* synthetic */ AbstractC0051c d;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0091m(C0095n nVar, AbstractC0054c2 c2Var) {
        super(c2Var);
        this.d = nVar;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0091m(C0114s sVar, AbstractC0054c2 c2Var) {
        super(c2Var);
        this.d = sVar;
        this.c = new C0103p(0, c2Var);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0091m(C0122u uVar, AbstractC0054c2 c2Var) {
        super(c2Var);
        this.d = uVar;
        this.c = new S(0, c2Var);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public C0091m(C0126v vVar, AbstractC0054c2 c2Var) {
        super(c2Var);
        this.d = vVar;
        this.c = new C0048b0(0, c2Var);
    }

    @Override // j$.util.function.Consumer
    public final void accept(Object obj) {
        AbstractC0080j0 j0Var;
        switch (this.b) {
            case 0:
                if (!((Set) this.c).contains(obj)) {
                    ((Set) this.c).add(obj);
                    this.a.accept((AbstractC0054c2) obj);
                    return;
                }
                return;
            case 1:
                j0Var = (AbstractC0080j0) ((Function) ((C0126v) this.d).n).apply(obj);
                if (j0Var != null) {
                    try {
                        j0Var.sequential().p((B) this.c);
                    } catch (Throwable th) {
                        throw th;
                    }
                }
                if (j0Var != null) {
                    ((AbstractC0051c) j0Var).close();
                    return;
                }
                return;
            case 2:
                IntStream intStream = (IntStream) ((Function) ((C0122u) this.d).n).apply(obj);
                if (intStream != null) {
                    try {
                        intStream.sequential().w((r) this.c);
                    } catch (Throwable th2) {
                        try {
                            intStream.close();
                        } catch (Throwable th3) {
                            th2.addSuppressed(th3);
                        }
                        throw th2;
                    }
                }
                if (intStream != null) {
                    intStream.close();
                    return;
                }
                return;
            default:
                j0Var = (C) ((Function) ((C0114s) this.d).n).apply(obj);
                if (j0Var != null) {
                    try {
                        j0Var.sequential().r((AbstractC0024g) this.c);
                    } finally {
                        try {
                            ((AbstractC0051c) j0Var).close();
                        } catch (Throwable th4) {
                            th.addSuppressed(th4);
                        }
                    }
                }
                if (j0Var != null) {
                    ((AbstractC0051c) j0Var).close();
                    return;
                }
                return;
        }
    }

    @Override // j$.util.stream.AbstractC0054c2
    public final void d(long j) {
        switch (this.b) {
            case 0:
                this.c = new HashSet();
                this.a.d(-1);
                return;
            case 1:
                this.a.d(-1);
                return;
            case 2:
                this.a.d(-1);
                return;
            default:
                this.a.d(-1);
                return;
        }
    }

    @Override // j$.util.stream.Y1, j$.util.stream.AbstractC0054c2
    public final void end() {
        switch (this.b) {
            case 0:
                this.c = null;
                this.a.end();
                return;
            default:
                super.end();
                return;
        }
    }
}
