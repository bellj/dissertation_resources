package j$.util.stream;

import j$.lang.Iterable;
import j$.time.b;
import j$.util.Spliterator;
import j$.util.T;
import j$.util.function.Consumer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public class M2 extends AbstractC0059e implements Consumer, Iterable, Iterable {
    protected Object[] e = new Object[1 << 4];
    protected Object[][] f;

    @Override // j$.util.function.Consumer
    public void accept(Object obj) {
        long j;
        int i = this.b;
        Object[] objArr = this.e;
        if (i == objArr.length) {
            if (this.f == null) {
                Object[][] objArr2 = new Object[8];
                this.f = objArr2;
                this.d = new long[8];
                objArr2[0] = objArr;
            }
            int i2 = this.c;
            int i3 = i2 + 1;
            Object[][] objArr3 = this.f;
            if (i3 >= objArr3.length || objArr3[i3] == null) {
                if (i2 == 0) {
                    j = (long) objArr.length;
                } else {
                    j = ((long) objArr3[i2].length) + this.d[i2];
                }
                o(j + 1);
            }
            this.b = 0;
            int i4 = this.c + 1;
            this.c = i4;
            this.e = this.f[i4];
        }
        Object[] objArr4 = this.e;
        int i5 = this.b;
        this.b = i5 + 1;
        objArr4[i5] = obj;
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }

    @Override // j$.util.stream.AbstractC0059e
    public final void clear() {
        Object[][] objArr = this.f;
        if (objArr != null) {
            this.e = objArr[0];
            int i = 0;
            while (true) {
                Object[] objArr2 = this.e;
                if (i >= objArr2.length) {
                    break;
                }
                objArr2[i] = null;
                i++;
            }
            this.f = null;
            this.d = null;
        } else {
            for (int i2 = 0; i2 < this.b; i2++) {
                this.e[i2] = null;
            }
        }
        this.b = 0;
        this.c = 0;
    }

    @Override // j$.lang.Iterable
    public void forEach(Consumer consumer) {
        for (int i = 0; i < this.c; i++) {
            for (Object obj : this.f[i]) {
                consumer.accept(obj);
            }
        }
        for (int i2 = 0; i2 < this.b; i2++) {
            consumer.accept(this.e[i2]);
        }
    }

    @Override // java.lang.Iterable
    public final /* synthetic */ void forEach(java.util.function.Consumer consumer) {
        forEach(Consumer.VivifiedWrapper.convert(consumer));
    }

    @Override // java.lang.Iterable, j$.lang.Iterable
    public final Iterator iterator() {
        return T.i(mo150spliterator());
    }

    public final void o(long j) {
        int i = this.c;
        long length = i == 0 ? (long) this.e.length : this.d[i] + ((long) this.f[i].length);
        if (j > length) {
            if (this.f == null) {
                Object[][] objArr = new Object[8];
                this.f = objArr;
                this.d = new long[8];
                objArr[0] = this.e;
            }
            while (true) {
                i++;
                if (j > length) {
                    Object[][] objArr2 = this.f;
                    if (i >= objArr2.length) {
                        int length2 = objArr2.length * 2;
                        this.f = (Object[][]) Arrays.copyOf(objArr2, length2);
                        this.d = Arrays.copyOf(this.d, length2);
                    }
                    int min = 1 << ((i == 0 || i == 1) ? this.a : Math.min((this.a + i) - 1, 30));
                    Object[][] objArr3 = this.f;
                    objArr3[i] = new Object[min];
                    long[] jArr = this.d;
                    int i2 = i - 1;
                    jArr[i] = jArr[i2] + ((long) objArr3[i2].length);
                    length += (long) min;
                } else {
                    return;
                }
            }
        }
    }

    @Override // java.lang.Iterable, j$.lang.Iterable
    /* renamed from: spliterator */
    public Spliterator mo150spliterator() {
        return new D2(this, 0, this.c, 0, this.b);
    }

    @Override // java.lang.Object
    public final String toString() {
        ArrayList arrayList = new ArrayList();
        forEach(new C0042a(8, arrayList));
        StringBuilder a = b.a("SpinedBuffer:");
        a.append(arrayList.toString());
        return a.toString();
    }
}
