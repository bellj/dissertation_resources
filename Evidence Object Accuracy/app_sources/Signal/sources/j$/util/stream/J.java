package j$.util.stream;

import j$.util.Spliterator;
import java.util.concurrent.CountedCompleter;
import java.util.concurrent.atomic.AtomicReference;

/* loaded from: classes2.dex */
final class J extends AbstractC0055d {
    private final D j;

    public J(D d, AbstractC0115s0 s0Var, Spliterator spliterator) {
        super(s0Var, spliterator);
        this.j = d;
    }

    J(J j, Spliterator spliterator) {
        super(j, spliterator);
        this.j = j.j;
    }

    /* access modifiers changed from: protected */
    @Override // j$.util.stream.AbstractC0063f
    public final Object a() {
        boolean z;
        AbstractC0115s0 s0Var = this.a;
        y3 y3Var = (y3) this.j.d.get();
        s0Var.s0(this.b, y3Var);
        Object obj = y3Var.get();
        if (!this.j.a) {
            if (obj != null) {
                AtomicReference atomicReference = this.h;
                while (!atomicReference.compareAndSet(null, obj) && atomicReference.get() == null) {
                }
            }
            return null;
        } else if (obj == null) {
            return null;
        } else {
            J j = this;
            while (true) {
                if (j != null) {
                    AbstractC0063f c = j.c();
                    if (c != null && c.d != j) {
                        z = false;
                        break;
                    }
                    j = c;
                } else {
                    z = true;
                    break;
                }
            }
            if (z) {
                AtomicReference atomicReference2 = this.h;
                while (!atomicReference2.compareAndSet(null, obj) && atomicReference2.get() == null) {
                }
            } else {
                h();
            }
            return obj;
        }
    }

    /* access modifiers changed from: protected */
    @Override // j$.util.stream.AbstractC0063f
    public final AbstractC0063f d(Spliterator spliterator) {
        return new J(this, spliterator);
    }

    @Override // j$.util.stream.AbstractC0055d
    protected final Object i() {
        return this.j.b;
    }

    @Override // j$.util.stream.AbstractC0063f, java.util.concurrent.CountedCompleter
    public final void onCompletion(CountedCompleter countedCompleter) {
        boolean z;
        if (this.j.a) {
            J j = (J) this.d;
            J j2 = null;
            while (true) {
                if (j == j2) {
                    break;
                }
                Object b = j.b();
                if (b == null || !this.j.c.test(b)) {
                    j = (J) this.e;
                    j2 = j;
                } else {
                    e(b);
                    J j3 = this;
                    while (true) {
                        if (j3 != null) {
                            AbstractC0063f c = j3.c();
                            if (c != null && c.d != j3) {
                                z = false;
                                break;
                            }
                            j3 = c;
                        } else {
                            z = true;
                            break;
                        }
                    }
                    if (z) {
                        AtomicReference atomicReference = this.h;
                        while (!atomicReference.compareAndSet(null, b) && atomicReference.get() == null) {
                        }
                    } else {
                        h();
                    }
                }
            }
        }
        super.onCompletion(countedCompleter);
    }
}
