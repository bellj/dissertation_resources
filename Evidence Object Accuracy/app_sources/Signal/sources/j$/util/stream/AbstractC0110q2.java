package j$.util.stream;

import java.util.Comparator;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.q2 */
/* loaded from: classes2.dex */
public abstract class AbstractC0110q2 extends Y1 {
    protected final Comparator b;
    protected boolean c;

    public AbstractC0110q2(AbstractC0054c2 c2Var, Comparator comparator) {
        super(c2Var);
        this.b = comparator;
    }

    @Override // j$.util.stream.Y1, j$.util.stream.AbstractC0054c2
    public final boolean f() {
        this.c = true;
        return false;
    }
}
