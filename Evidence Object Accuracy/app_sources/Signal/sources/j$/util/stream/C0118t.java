package j$.util.stream;

/* access modifiers changed from: package-private */
/* renamed from: j$.util.stream.t */
/* loaded from: classes2.dex */
public final class C0118t extends T1 {
    public final /* synthetic */ int m;
    final /* synthetic */ Object n;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public /* synthetic */ C0118t(AbstractC0051c cVar, int i, Object obj, int i2) {
        super(cVar, i);
        this.m = i2;
        this.n = obj;
    }

    @Override // j$.util.stream.AbstractC0051c
    public final AbstractC0054c2 G0(int i, AbstractC0054c2 c2Var) {
        switch (this.m) {
            case 0:
                return new r(this, c2Var, 1);
            case 1:
                return new T(this, c2Var, 4);
            case 2:
                return new C0056d0(this, c2Var, 2);
            case 3:
                return new O1(this, c2Var, 0);
            default:
                return new O1(this, c2Var, 1);
        }
    }
}
