package j$.util.stream;

import j$.util.function.r;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public final class L extends O implements AbstractC0045a2 {
    final r b;

    public L(r rVar, boolean z) {
        super(z);
        this.b = rVar;
    }

    @Override // j$.util.stream.O, j$.util.stream.AbstractC0054c2
    public final void accept(int i) {
        this.b.accept(i);
    }

    @Override // j$.util.function.Consumer
    public final /* bridge */ /* synthetic */ void accept(Object obj) {
        e((Integer) obj);
    }

    @Override // j$.util.stream.AbstractC0045a2
    public final /* synthetic */ void e(Integer num) {
        AbstractC0115s0.G(this, num);
    }
}
