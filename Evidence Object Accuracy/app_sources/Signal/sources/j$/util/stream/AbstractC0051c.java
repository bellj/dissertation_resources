package j$.util.stream;

import j$.util.Spliterator;
import j$.util.function.IntFunction;

/* renamed from: j$.util.stream.c */
/* loaded from: classes2.dex */
public abstract class AbstractC0051c extends AbstractC0115s0 implements AbstractC0071h {
    private final AbstractC0051c a;
    private final AbstractC0051c b;
    protected final int c;
    private AbstractC0051c d;
    private int e;
    private int f;
    private Spliterator g;
    private boolean h;
    private boolean i;
    private Runnable j;
    private boolean k;

    public AbstractC0051c(Spliterator spliterator, int i, boolean z) {
        this.b = null;
        this.g = spliterator;
        this.a = this;
        int i2 = P2.g & i;
        this.c = i2;
        this.f = ((i2 << 1) ^ -1) & P2.l;
        this.e = 0;
        this.k = z;
    }

    public AbstractC0051c(AbstractC0051c cVar, int i) {
        if (!cVar.h) {
            cVar.h = true;
            cVar.d = this;
            this.b = cVar;
            this.c = P2.h & i;
            this.f = P2.a(i, cVar.f);
            AbstractC0051c cVar2 = cVar.a;
            this.a = cVar2;
            if (F0()) {
                cVar2.i = true;
            }
            this.e = cVar.e + 1;
            return;
        }
        throw new IllegalStateException("stream has already been operated upon or closed");
    }

    private Spliterator H0(int i) {
        int i2;
        int i3;
        AbstractC0051c cVar = this.a;
        Spliterator spliterator = cVar.g;
        if (spliterator != null) {
            cVar.g = null;
            if (cVar.k && cVar.i) {
                AbstractC0051c cVar2 = cVar.d;
                int i4 = 1;
                while (cVar != this) {
                    int i5 = cVar2.c;
                    if (cVar2.F0()) {
                        i4 = 0;
                        if (P2.SHORT_CIRCUIT.e(i5)) {
                            i5 &= P2.u ^ -1;
                        }
                        spliterator = cVar2.E0(cVar, spliterator);
                        if (spliterator.hasCharacteristics(64)) {
                            i3 = i5 & (P2.t ^ -1);
                            i2 = P2.s;
                        } else {
                            i3 = i5 & (P2.s ^ -1);
                            i2 = P2.t;
                        }
                        i5 = i3 | i2;
                    }
                    i4++;
                    cVar2.e = i4;
                    cVar2.f = P2.a(i5, cVar.f);
                    cVar2 = cVar2.d;
                    cVar = cVar2;
                }
            }
            if (i != 0) {
                this.f = P2.a(i, this.f);
            }
            return spliterator;
        }
        throw new IllegalStateException("source already consumed or closed");
    }

    public final int A0() {
        AbstractC0051c cVar = this;
        while (cVar.e > 0) {
            cVar = cVar.b;
        }
        return cVar.z0();
    }

    public final boolean B0() {
        return P2.ORDERED.e(this.f);
    }

    public final /* synthetic */ Spliterator C0() {
        return H0(0);
    }

    B0 D0(Spliterator spliterator, IntFunction intFunction, AbstractC0051c cVar) {
        throw new UnsupportedOperationException("Parallel evaluation is not supported");
    }

    Spliterator E0(AbstractC0051c cVar, Spliterator spliterator) {
        return D0(spliterator, new C0047b(0), cVar).mo148spliterator();
    }

    abstract boolean F0();

    /* access modifiers changed from: package-private */
    public abstract AbstractC0054c2 G0(int i, AbstractC0054c2 c2Var);

    public final Spliterator I0() {
        AbstractC0051c cVar = this.a;
        if (this != cVar) {
            throw new IllegalStateException();
        } else if (!this.h) {
            this.h = true;
            Spliterator spliterator = cVar.g;
            if (spliterator != null) {
                cVar.g = null;
                return spliterator;
            }
            throw new IllegalStateException("source already consumed or closed");
        } else {
            throw new IllegalStateException("stream has already been operated upon or closed");
        }
    }

    abstract Spliterator J0(AbstractC0115s0 s0Var, C0042a aVar, boolean z);

    public final Spliterator K0(Spliterator spliterator) {
        return this.e == 0 ? spliterator : J0(this, new C0042a(0, spliterator), this.a.k);
    }

    @Override // j$.util.stream.AbstractC0115s0
    public final void a0(Spliterator spliterator, AbstractC0054c2 c2Var) {
        c2Var.getClass();
        if (!P2.SHORT_CIRCUIT.e(this.f)) {
            c2Var.d(spliterator.getExactSizeIfKnown());
            spliterator.forEachRemaining(c2Var);
            c2Var.end();
            return;
        }
        b0(spliterator, c2Var);
    }

    @Override // j$.util.stream.AbstractC0115s0
    public final void b0(Spliterator spliterator, AbstractC0054c2 c2Var) {
        AbstractC0051c cVar = this;
        while (cVar.e > 0) {
            cVar = cVar.b;
        }
        c2Var.d(spliterator.getExactSizeIfKnown());
        cVar.y0(spliterator, c2Var);
        c2Var.end();
    }

    @Override // j$.util.stream.AbstractC0071h, java.lang.AutoCloseable
    public final void close() {
        this.h = true;
        this.g = null;
        AbstractC0051c cVar = this.a;
        Runnable runnable = cVar.j;
        if (runnable != null) {
            cVar.j = null;
            runnable.run();
        }
    }

    @Override // j$.util.stream.AbstractC0115s0
    public final long d0(Spliterator spliterator) {
        if (P2.SIZED.e(this.f)) {
            return spliterator.getExactSizeIfKnown();
        }
        return -1;
    }

    @Override // j$.util.stream.AbstractC0115s0
    public final int f0() {
        return this.f;
    }

    @Override // j$.util.stream.AbstractC0071h
    public final boolean isParallel() {
        return this.a.k;
    }

    @Override // j$.util.stream.AbstractC0071h
    public final AbstractC0071h onClose(Runnable runnable) {
        AbstractC0051c cVar = this.a;
        Runnable runnable2 = cVar.j;
        if (runnable2 != null) {
            runnable = new v3(runnable2, runnable);
        }
        cVar.j = runnable;
        return this;
    }

    @Override // j$.util.stream.AbstractC0071h
    public final AbstractC0071h parallel() {
        this.a.k = true;
        return this;
    }

    @Override // j$.util.stream.AbstractC0115s0
    public final AbstractC0054c2 s0(Spliterator spliterator, AbstractC0054c2 c2Var) {
        c2Var.getClass();
        a0(spliterator, t0(c2Var));
        return c2Var;
    }

    @Override // j$.util.stream.AbstractC0071h
    public final AbstractC0071h sequential() {
        this.a.k = false;
        return this;
    }

    @Override // j$.util.stream.AbstractC0071h
    public Spliterator spliterator() {
        if (!this.h) {
            this.h = true;
            AbstractC0051c cVar = this.a;
            if (this != cVar) {
                return J0(this, new C0042a(1, this), cVar.k);
            }
            Spliterator spliterator = cVar.g;
            if (spliterator != null) {
                cVar.g = null;
                return spliterator;
            }
            throw new IllegalStateException("source already consumed or closed");
        }
        throw new IllegalStateException("stream has already been operated upon or closed");
    }

    @Override // j$.util.stream.AbstractC0115s0
    public final AbstractC0054c2 t0(AbstractC0054c2 c2Var) {
        c2Var.getClass();
        for (AbstractC0051c cVar = this; cVar.e > 0; cVar = cVar.b) {
            c2Var = cVar.G0(cVar.b.f, c2Var);
        }
        return c2Var;
    }

    public final B0 u0(Spliterator spliterator, boolean z, IntFunction intFunction) {
        if (this.a.k) {
            return x0(this, spliterator, z, intFunction);
        }
        AbstractC0131w0 o0 = o0(d0(spliterator), intFunction);
        s0(spliterator, o0);
        return o0.mo149build();
    }

    public final Object v0(x3 x3Var) {
        if (!this.h) {
            this.h = true;
            return this.a.k ? x3Var.a(this, H0(x3Var.b())) : x3Var.c(this, H0(x3Var.b()));
        }
        throw new IllegalStateException("stream has already been operated upon or closed");
    }

    public final B0 w0(IntFunction intFunction) {
        if (!this.h) {
            this.h = true;
            if (!this.a.k || this.b == null || !F0()) {
                return u0(H0(0), true, intFunction);
            }
            this.e = 0;
            AbstractC0051c cVar = this.b;
            return D0(cVar.H0(0), intFunction, cVar);
        }
        throw new IllegalStateException("stream has already been operated upon or closed");
    }

    abstract B0 x0(AbstractC0115s0 s0Var, Spliterator spliterator, boolean z, IntFunction intFunction);

    abstract void y0(Spliterator spliterator, AbstractC0054c2 c2Var);

    /* access modifiers changed from: package-private */
    public abstract int z0();
}
