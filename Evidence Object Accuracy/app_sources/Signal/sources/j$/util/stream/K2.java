package j$.util.stream;

import j$.util.AbstractC0037n;
import j$.util.E;
import java.util.Comparator;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public abstract class K2 implements E {
    int a;
    final int b;
    int c;
    final int d;
    Object e;
    final /* synthetic */ L2 f;

    public K2(L2 l2, int i, int i2, int i3, int i4) {
        this.f = l2;
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = i4;
        Object[] objArr = l2.f;
        this.e = objArr == null ? l2.e : objArr[i];
    }

    @Override // j$.util.Spliterator
    public final int characteristics() {
        return 16464;
    }

    abstract void d(int i, Object obj, Object obj2);

    abstract E e(Object obj, int i, int i2);

    @Override // j$.util.Spliterator
    public final long estimateSize() {
        int i = this.a;
        int i2 = this.b;
        if (i == i2) {
            return ((long) this.d) - ((long) this.c);
        }
        long[] jArr = this.f.d;
        return ((jArr[i2] + ((long) this.d)) - jArr[i]) - ((long) this.c);
    }

    abstract E f(int i, int i2, int i3, int i4);

    @Override // j$.util.E
    /* renamed from: forEachRemaining */
    public final void m(Object obj) {
        int i;
        obj.getClass();
        int i2 = this.a;
        int i3 = this.b;
        if (i2 < i3 || (i2 == i3 && this.c < this.d)) {
            int i4 = this.c;
            while (true) {
                i = this.b;
                if (i2 >= i) {
                    break;
                }
                L2 l2 = this.f;
                Object obj2 = l2.f[i2];
                l2.o(obj2, i4, l2.p(obj2), obj);
                i4 = 0;
                i2++;
            }
            this.f.o(this.a == i ? this.e : this.f.f[i], i4, this.d, obj);
            this.a = this.b;
            this.c = this.d;
        }
    }

    @Override // j$.util.Spliterator
    public final Comparator getComparator() {
        throw new IllegalStateException();
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ long getExactSizeIfKnown() {
        return AbstractC0037n.h(this);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean hasCharacteristics(int i) {
        return AbstractC0037n.j(this, i);
    }

    @Override // j$.util.E
    /* renamed from: tryAdvance */
    public final boolean k(Object obj) {
        obj.getClass();
        int i = this.a;
        int i2 = this.b;
        if (i >= i2 && (i != i2 || this.c >= this.d)) {
            return false;
        }
        Object obj2 = this.e;
        int i3 = this.c;
        this.c = i3 + 1;
        d(i3, obj2, obj);
        if (this.c == this.f.p(this.e)) {
            this.c = 0;
            int i4 = this.a + 1;
            this.a = i4;
            Object[] objArr = this.f.f;
            if (objArr != null && i4 <= this.b) {
                this.e = objArr[i4];
            }
        }
        return true;
    }

    @Override // j$.util.E, j$.util.Spliterator
    public final E trySplit() {
        int i = this.a;
        int i2 = this.b;
        if (i < i2) {
            int i3 = this.c;
            L2 l2 = this.f;
            E f = f(i, i2 - 1, i3, l2.p(l2.f[i2 - 1]));
            int i4 = this.b;
            this.a = i4;
            this.c = 0;
            this.e = this.f.f[i4];
            return f;
        } else if (i != i2) {
            return null;
        } else {
            int i5 = this.d;
            int i6 = this.c;
            int i7 = (i5 - i6) / 2;
            if (i7 == 0) {
                return null;
            }
            E e = e(this.e, i6, i7);
            this.c += i7;
            return e;
        }
    }
}
