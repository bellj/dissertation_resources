package j$.util;

import j$.util.function.Consumer;

/* loaded from: classes2.dex */
public interface Iterator<E> {

    /* renamed from: j$.util.Iterator$-CC */
    /* loaded from: classes2.dex */
    public final /* synthetic */ class CC<E> {
        public static void $default$forEachRemaining(java.util.Iterator it, Consumer consumer) {
            consumer.getClass();
            while (it.hasNext()) {
                consumer.accept(it.next());
            }
        }
    }

    void forEachRemaining(Consumer<? super E> consumer);

    @Override // j$.util.Iterator
    boolean hasNext();

    @Override // j$.util.Iterator
    E next();

    @Override // j$.util.Iterator
    void remove();
}
