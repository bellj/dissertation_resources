package j$.util;

import j$.util.function.AbstractC0024g;
import j$.util.function.B;
import j$.util.function.Consumer;
import j$.util.function.r;

/* renamed from: j$.util.n */
/* loaded from: classes2.dex */
public abstract /* synthetic */ class AbstractC0037n {
    public static void d(y yVar, Consumer consumer) {
        if (consumer instanceof AbstractC0024g) {
            yVar.m((AbstractC0024g) consumer);
        } else if (!V.a) {
            consumer.getClass();
            yVar.m(new C0036m(consumer));
        } else {
            V.a(yVar.getClass(), "{0} calling Spliterator.OfDouble.forEachRemaining((DoubleConsumer) action::accept)");
            throw null;
        }
    }

    public static void e(A a, Consumer consumer) {
        if (consumer instanceof r) {
            a.i((r) consumer);
        } else if (!V.a) {
            consumer.getClass();
            a.i(new C0040q(consumer));
        } else {
            V.a(a.getClass(), "{0} calling Spliterator.OfInt.forEachRemaining((IntConsumer) action::accept)");
            throw null;
        }
    }

    public static void f(C c, Consumer consumer) {
        if (consumer instanceof B) {
            c.b((B) consumer);
        } else if (!V.a) {
            consumer.getClass();
            c.b(new C0146t(consumer));
        } else {
            V.a(c.getClass(), "{0} calling Spliterator.OfLong.forEachRemaining((LongConsumer) action::accept)");
            throw null;
        }
    }

    public static long h(Spliterator spliterator) {
        if ((spliterator.characteristics() & 64) == 0) {
            return -1;
        }
        return spliterator.estimateSize();
    }

    public static boolean j(Spliterator spliterator, int i) {
        return (spliterator.characteristics() & i) == i;
    }

    public static boolean l(y yVar, Consumer consumer) {
        if (consumer instanceof AbstractC0024g) {
            return yVar.k((AbstractC0024g) consumer);
        }
        if (!V.a) {
            consumer.getClass();
            return yVar.k(new C0036m(consumer));
        }
        V.a(yVar.getClass(), "{0} calling Spliterator.OfDouble.tryAdvance((DoubleConsumer) action::accept)");
        throw null;
    }

    public static boolean n(A a, Consumer consumer) {
        if (consumer instanceof r) {
            return a.g((r) consumer);
        }
        if (!V.a) {
            consumer.getClass();
            return a.g(new C0040q(consumer));
        }
        V.a(a.getClass(), "{0} calling Spliterator.OfInt.tryAdvance((IntConsumer) action::accept)");
        throw null;
    }

    public static boolean o(C c, Consumer consumer) {
        if (consumer instanceof B) {
            return c.c((B) consumer);
        }
        if (!V.a) {
            consumer.getClass();
            return c.c(new C0146t(consumer));
        }
        V.a(c.getClass(), "{0} calling Spliterator.OfLong.tryAdvance((LongConsumer) action::accept)");
        throw null;
    }

    public static boolean p(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public int characteristics() {
        return 16448;
    }

    public long estimateSize() {
        return 0;
    }

    public void forEachRemaining(Object obj) {
        obj.getClass();
    }

    public boolean tryAdvance(Object obj) {
        obj.getClass();
        return false;
    }

    @Override // j$.util.y, j$.util.E, j$.util.Spliterator
    public Spliterator trySplit() {
        return null;
    }
}
