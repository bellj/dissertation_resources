package j$.util;

import j$.util.function.B;
import j$.util.function.Consumer;

/* loaded from: classes2.dex */
public interface C extends E {
    @Override // j$.util.Spliterator
    boolean a(Consumer consumer);

    void b(B b);

    boolean c(B b);

    @Override // j$.util.Spliterator
    void forEachRemaining(Consumer consumer);

    @Override // j$.util.E, j$.util.Spliterator
    C trySplit();
}
