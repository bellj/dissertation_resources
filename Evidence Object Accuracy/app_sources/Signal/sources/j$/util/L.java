package j$.util;

import j$.util.function.AbstractC0024g;
import j$.util.function.Consumer;
import java.util.Comparator;

/* loaded from: classes2.dex */
final class L extends AbstractC0037n implements y {
    @Override // j$.util.y, j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.l(this, consumer);
    }

    @Override // j$.util.y, j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.d(this, consumer);
    }

    @Override // j$.util.Spliterator
    public final Comparator getComparator() {
        throw new IllegalStateException();
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ long getExactSizeIfKnown() {
        return AbstractC0037n.h(this);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean hasCharacteristics(int i) {
        return AbstractC0037n.j(this, i);
    }

    @Override // j$.util.y
    public final boolean k(AbstractC0024g gVar) {
        gVar.getClass();
        return false;
    }

    @Override // j$.util.y
    public final void m(AbstractC0024g gVar) {
        gVar.getClass();
    }
}
