package j$.util;

import j$.util.function.Consumer;
import j$.util.function.r;
import java.util.Comparator;

/* loaded from: classes2.dex */
final class M extends AbstractC0037n implements A {
    @Override // j$.util.A, j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.n(this, consumer);
    }

    @Override // j$.util.A, j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.e(this, consumer);
    }

    @Override // j$.util.A
    public final boolean g(r rVar) {
        rVar.getClass();
        return false;
    }

    @Override // j$.util.Spliterator
    public final Comparator getComparator() {
        throw new IllegalStateException();
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ long getExactSizeIfKnown() {
        return AbstractC0037n.h(this);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean hasCharacteristics(int i) {
        return AbstractC0037n.j(this, i);
    }

    @Override // j$.util.A
    public final void i(r rVar) {
        rVar.getClass();
    }
}
