package j$.util;

import j$.util.function.AbstractC0024g;
import j$.util.function.Consumer;

/* renamed from: j$.util.m */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0036m implements AbstractC0024g {
    public final /* synthetic */ Consumer a;

    public /* synthetic */ C0036m(Consumer consumer) {
        this.a = consumer;
    }

    @Override // j$.util.function.AbstractC0024g
    public final void accept(double d) {
        this.a.accept(Double.valueOf(d));
    }
}
