package j$.util;

import j$.util.function.Consumer;
import j$.util.function.IntFunction;
import j$.util.function.Predicate;
import j$.util.function.UnaryOperator;
import j$.util.stream.Stream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.ListIterator;

/* loaded from: classes2.dex */
public interface List<E> extends AbstractC0010a {

    /* renamed from: j$.util.List$-CC */
    /* loaded from: classes2.dex */
    public final /* synthetic */ class CC<E> {
        public static Spliterator $default$spliterator(java.util.List list) {
            list.getClass();
            return new Q(16, list);
        }
    }

    /* renamed from: j$.util.List$-EL */
    /* loaded from: classes2.dex */
    public final /* synthetic */ class EL {
        public static void sort(java.util.List list, Comparator comparator) {
            if (list instanceof List) {
                ((List) list).sort(comparator);
            } else if (DesugarCollections.b.isInstance(list)) {
                DesugarCollections.e(list, comparator);
            } else {
                Object[] array = list.toArray();
                Arrays.sort(array, comparator);
                ListIterator<E> listIterator = list.listIterator();
                for (Object obj : array) {
                    listIterator.next();
                    listIterator.set(obj);
                }
            }
        }
    }

    @Override // j$.util.List
    void add(int i, E e);

    boolean add(E e);

    boolean addAll(int i, Collection<? extends E> collection);

    boolean addAll(Collection<? extends E> collection);

    void clear();

    boolean contains(Object obj);

    boolean containsAll(Collection<?> collection);

    boolean equals(Object obj);

    @Override // j$.util.AbstractC0010a, j$.lang.Iterable
    /* synthetic */ void forEach(Consumer<? super E> consumer);

    E get(int i);

    int hashCode();

    int indexOf(Object obj);

    boolean isEmpty();

    @Override // j$.lang.Iterable
    Iterator<E> iterator();

    int lastIndexOf(Object obj);

    ListIterator<E> listIterator();

    ListIterator<E> listIterator(int i);

    /* synthetic */ Stream<E> parallelStream();

    E remove(int i);

    boolean remove(Object obj);

    boolean removeAll(Collection<?> collection);

    @Override // j$.util.AbstractC0010a
    /* synthetic */ boolean removeIf(Predicate<? super E> predicate);

    void replaceAll(UnaryOperator<E> unaryOperator);

    boolean retainAll(Collection<?> collection);

    E set(int i, E e);

    int size();

    void sort(Comparator<? super E> comparator);

    @Override // j$.util.AbstractC0010a, j$.lang.Iterable
    /* renamed from: spliterator */
    Spliterator<E> mo150spliterator();

    @Override // j$.util.AbstractC0010a
    /* synthetic */ Stream<E> stream();

    java.util.List<E> subList(int i, int i2);

    Object[] toArray();

    /* synthetic */ <T> T[] toArray(IntFunction<T[]> intFunction);

    <T> T[] toArray(T[] tArr);
}
