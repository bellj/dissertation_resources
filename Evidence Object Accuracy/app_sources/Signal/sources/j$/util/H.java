package j$.util;

import j$.util.function.B;
import j$.util.function.Consumer;
import java.util.NoSuchElementException;

/* loaded from: classes2.dex */
public final class H implements AbstractC0148v, B, Iterator {
    boolean a = false;
    long b;
    final /* synthetic */ C c;

    public H(C c) {
        this.c = c;
    }

    public final void a(B b) {
        b.getClass();
        while (hasNext()) {
            b.accept(nextLong());
        }
    }

    @Override // j$.util.function.B
    public final void accept(long j) {
        this.a = true;
        this.b = j;
    }

    @Override // j$.util.Iterator
    public final void forEachRemaining(Consumer consumer) {
        if (consumer instanceof B) {
            a((B) consumer);
            return;
        }
        consumer.getClass();
        if (!V.a) {
            a(new C0146t(consumer));
        } else {
            V.a(H.class, "{0} calling PrimitiveIterator.OfLong.forEachRemainingLong(action::accept)");
            throw null;
        }
    }

    @Override // java.util.Iterator, j$.util.Iterator
    public final boolean hasNext() {
        if (!this.a) {
            this.c.c(this);
        }
        return this.a;
    }

    @Override // java.util.Iterator, j$.util.Iterator
    public final Object next() {
        if (!V.a) {
            return Long.valueOf(nextLong());
        }
        V.a(H.class, "{0} calling PrimitiveIterator.OfLong.nextLong()");
        throw null;
    }

    public final long nextLong() {
        if (this.a || hasNext()) {
            this.a = false;
            return this.b;
        }
        throw new NoSuchElementException();
    }

    @Override // java.util.Iterator, j$.util.Iterator
    public final void remove() {
        throw new UnsupportedOperationException("remove");
    }
}
