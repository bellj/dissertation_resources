package j$.util;

import j$.time.a;
import j$.util.Map;
import j$.util.concurrent.u;
import j$.util.concurrent.v;
import j$.util.function.BiConsumer;
import j$.util.function.BiFunction;
import j$.util.function.Function;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

/* renamed from: j$.util.e */
/* loaded from: classes2.dex */
public final class C0016e implements Map, Serializable, Map {
    private final Map a;
    final Object b = this;
    private transient Set c;
    private transient Set d;
    private transient Collection e;

    public C0016e(Map map) {
        map.getClass();
        this.a = map;
    }

    private static Set a(Set set, Object obj) {
        if (DesugarCollections.f == null) {
            return Collections.synchronizedSet(set);
        }
        try {
            return (Set) DesugarCollections.f.newInstance(set, obj);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            throw new Error("Unable to instantiate a synchronized list.", e);
        }
    }

    @Override // java.util.Map, j$.util.Map
    public final void clear() {
        synchronized (this.b) {
            this.a.clear();
        }
    }

    @Override // j$.util.Map
    public final Object compute(Object obj, BiFunction biFunction) {
        Object obj2;
        synchronized (this.b) {
            Map map = this.a;
            if (map instanceof Map) {
                obj2 = ((Map) map).compute(obj, biFunction);
            } else if (map instanceof ConcurrentMap) {
                ConcurrentMap concurrentMap = (ConcurrentMap) map;
                biFunction.getClass();
                loop0: while (true) {
                    Object obj3 = concurrentMap.get(obj);
                    while (true) {
                        obj2 = biFunction.apply(obj, obj3);
                        if (obj2 != null) {
                            if (obj3 == null) {
                                obj3 = concurrentMap.putIfAbsent(obj, obj2);
                                if (obj3 == null) {
                                    break loop0;
                                }
                            } else if (concurrentMap.replace(obj, obj3, obj2)) {
                                break;
                            }
                        } else {
                            obj2 = null;
                            if ((obj3 == null && !concurrentMap.containsKey(obj)) || concurrentMap.remove(obj, obj3)) {
                                break;
                            }
                        }
                    }
                }
            } else {
                obj2 = Map.CC.$default$compute(map, obj, biFunction);
            }
        }
        return obj2;
    }

    public final /* synthetic */ Object compute(Object obj, java.util.function.BiFunction biFunction) {
        return compute(obj, BiFunction.VivifiedWrapper.convert(biFunction));
    }

    public final Object computeIfAbsent(Object obj, Function function) {
        Object obj2;
        Object apply;
        synchronized (this.b) {
            java.util.Map map = this.a;
            if (map instanceof Map) {
                obj2 = ((Map) map).computeIfAbsent(obj, function);
            } else if (map instanceof ConcurrentMap) {
                ConcurrentMap concurrentMap = (ConcurrentMap) map;
                function.getClass();
                obj2 = concurrentMap.get(obj);
                if (obj2 == null && (apply = function.apply(obj)) != null && (obj2 = concurrentMap.putIfAbsent(obj, apply)) == null) {
                    obj2 = apply;
                }
            } else {
                obj2 = Map.CC.$default$computeIfAbsent(map, obj, function);
            }
        }
        return obj2;
    }

    public final /* synthetic */ Object computeIfAbsent(Object obj, java.util.function.Function function) {
        return computeIfAbsent(obj, Function.VivifiedWrapper.convert(function));
    }

    public final Object computeIfPresent(Object obj, BiFunction biFunction) {
        Object obj2;
        synchronized (this.b) {
            java.util.Map map = this.a;
            if (map instanceof Map) {
                obj2 = ((Map) map).computeIfPresent(obj, biFunction);
            } else if (map instanceof ConcurrentMap) {
                ConcurrentMap concurrentMap = (ConcurrentMap) map;
                biFunction.getClass();
                while (true) {
                    Object obj3 = concurrentMap.get(obj);
                    if (obj3 == null) {
                        obj2 = obj3;
                        break;
                    }
                    Object apply = biFunction.apply(obj, obj3);
                    if (apply != null) {
                        if (concurrentMap.replace(obj, obj3, apply)) {
                            obj2 = apply;
                            break;
                        }
                    } else if (concurrentMap.remove(obj, obj3)) {
                        obj2 = null;
                        break;
                    }
                }
            } else {
                obj2 = Map.CC.$default$computeIfPresent(map, obj, biFunction);
            }
        }
        return obj2;
    }

    public final /* synthetic */ Object computeIfPresent(Object obj, java.util.function.BiFunction biFunction) {
        return computeIfPresent(obj, BiFunction.VivifiedWrapper.convert(biFunction));
    }

    public final boolean containsKey(Object obj) {
        boolean containsKey;
        synchronized (this.b) {
            containsKey = this.a.containsKey(obj);
        }
        return containsKey;
    }

    public final boolean containsValue(Object obj) {
        boolean containsValue;
        synchronized (this.b) {
            containsValue = this.a.containsValue(obj);
        }
        return containsValue;
    }

    public final Set entrySet() {
        Set set;
        synchronized (this.b) {
            if (this.d == null) {
                this.d = a(this.a.entrySet(), this.b);
            }
            set = this.d;
        }
        return set;
    }

    public final boolean equals(Object obj) {
        boolean equals;
        if (this == obj) {
            return true;
        }
        synchronized (this.b) {
            equals = this.a.equals(obj);
        }
        return equals;
    }

    public final void forEach(BiConsumer biConsumer) {
        synchronized (this.b) {
            java.util.Map map = this.a;
            if (map instanceof Map) {
                ((Map) map).forEach(biConsumer);
            } else if (map instanceof ConcurrentMap) {
                a.a((ConcurrentMap) map, biConsumer);
            } else {
                Map.CC.$default$forEach(map, biConsumer);
            }
        }
    }

    public final /* synthetic */ void forEach(java.util.function.BiConsumer biConsumer) {
        forEach(BiConsumer.VivifiedWrapper.convert(biConsumer));
    }

    public final Object get(Object obj) {
        Object obj2;
        synchronized (this.b) {
            obj2 = this.a.get(obj);
        }
        return obj2;
    }

    public final Object getOrDefault(Object obj, Object obj2) {
        Object orDefault;
        synchronized (this.b) {
            orDefault = Map.EL.getOrDefault(this.a, obj, obj2);
        }
        return orDefault;
    }

    public final int hashCode() {
        int hashCode;
        synchronized (this.b) {
            hashCode = this.a.hashCode();
        }
        return hashCode;
    }

    public final boolean isEmpty() {
        boolean isEmpty;
        synchronized (this.b) {
            isEmpty = this.a.isEmpty();
        }
        return isEmpty;
    }

    public final Set keySet() {
        Set set;
        synchronized (this.b) {
            if (this.c == null) {
                this.c = a(this.a.keySet(), this.b);
            }
            set = this.c;
        }
        return set;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0022, code lost:
        r3 = r7.apply(r2, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0026, code lost:
        if (r3 == null) goto L_0x0030;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002c, code lost:
        if (r1.replace(r5, r2, r3) == false) goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002e, code lost:
        r6 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0034, code lost:
        if (r1.remove(r5, r2) == false) goto L_0x001c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0036, code lost:
        r6 = null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object merge(java.lang.Object r5, java.lang.Object r6, j$.util.function.BiFunction r7) {
        /*
            r4 = this;
            java.lang.Object r0 = r4.b
            monitor-enter(r0)
            java.util.Map r1 = r4.a     // Catch: all -> 0x0046
            boolean r2 = r1 instanceof j$.util.Map     // Catch: all -> 0x0046
            if (r2 == 0) goto L_0x0010
            j$.util.Map r1 = (j$.util.Map) r1     // Catch: all -> 0x0046
            java.lang.Object r5 = r1.merge(r5, r6, r7)     // Catch: all -> 0x0046
            goto L_0x0044
        L_0x0010:
            boolean r2 = r1 instanceof java.util.concurrent.ConcurrentMap     // Catch: all -> 0x0046
            if (r2 == 0) goto L_0x0040
            java.util.concurrent.ConcurrentMap r1 = (java.util.concurrent.ConcurrentMap) r1     // Catch: all -> 0x0046
            r7.getClass()     // Catch: all -> 0x0046
            r6.getClass()     // Catch: all -> 0x0046
        L_0x001c:
            java.lang.Object r2 = r1.get(r5)     // Catch: all -> 0x0046
        L_0x0020:
            if (r2 == 0) goto L_0x0038
            java.lang.Object r3 = r7.apply(r2, r6)     // Catch: all -> 0x0046
            if (r3 == 0) goto L_0x0030
            boolean r2 = r1.replace(r5, r2, r3)     // Catch: all -> 0x0046
            if (r2 == 0) goto L_0x001c
            r6 = r3
            goto L_0x003e
        L_0x0030:
            boolean r2 = r1.remove(r5, r2)     // Catch: all -> 0x0046
            if (r2 == 0) goto L_0x001c
            r6 = 0
            goto L_0x003e
        L_0x0038:
            java.lang.Object r2 = r1.putIfAbsent(r5, r6)     // Catch: all -> 0x0046
            if (r2 != 0) goto L_0x0020
        L_0x003e:
            r5 = r6
            goto L_0x0044
        L_0x0040:
            java.lang.Object r5 = j$.util.Map.CC.$default$merge(r1, r5, r6, r7)     // Catch: all -> 0x0046
        L_0x0044:
            monitor-exit(r0)     // Catch: all -> 0x0046
            return r5
        L_0x0046:
            r5 = move-exception
            monitor-exit(r0)     // Catch: all -> 0x0046
            goto L_0x004a
        L_0x0049:
            throw r5
        L_0x004a:
            goto L_0x0049
        */
        throw new UnsupportedOperationException("Method not decompiled: j$.util.C0016e.merge(java.lang.Object, java.lang.Object, j$.util.function.BiFunction):java.lang.Object");
    }

    public final /* synthetic */ Object merge(Object obj, Object obj2, java.util.function.BiFunction biFunction) {
        return merge(obj, obj2, BiFunction.VivifiedWrapper.convert(biFunction));
    }

    public final Object put(Object obj, Object obj2) {
        Object put;
        synchronized (this.b) {
            put = this.a.put(obj, obj2);
        }
        return put;
    }

    public final void putAll(java.util.Map map) {
        synchronized (this.b) {
            this.a.putAll(map);
        }
    }

    public final Object putIfAbsent(Object obj, Object obj2) {
        Object a;
        synchronized (this.b) {
            a = Map.EL.a(this.a, obj, obj2);
        }
        return a;
    }

    public final Object remove(Object obj) {
        Object remove;
        synchronized (this.b) {
            remove = this.a.remove(obj);
        }
        return remove;
    }

    public final boolean remove(Object obj, Object obj2) {
        boolean remove;
        synchronized (this.b) {
            java.util.Map map = this.a;
            remove = map instanceof Map ? ((Map) map).remove(obj, obj2) : Map.CC.$default$remove(map, obj, obj2);
        }
        return remove;
    }

    public final Object replace(Object obj, Object obj2) {
        Object replace;
        synchronized (this.b) {
            java.util.Map map = this.a;
            replace = map instanceof Map ? ((Map) map).replace(obj, obj2) : Map.CC.$default$replace(map, obj, obj2);
        }
        return replace;
    }

    public final boolean replace(Object obj, Object obj2, Object obj3) {
        boolean replace;
        synchronized (this.b) {
            java.util.Map map = this.a;
            replace = map instanceof Map ? ((Map) map).replace(obj, obj2, obj3) : Map.CC.$default$replace(map, obj, obj2, obj3);
        }
        return replace;
    }

    public final void replaceAll(BiFunction biFunction) {
        synchronized (this.b) {
            java.util.Map map = this.a;
            if (map instanceof Map) {
                ((Map) map).replaceAll(biFunction);
            } else if (map instanceof ConcurrentMap) {
                ConcurrentMap concurrentMap = (ConcurrentMap) map;
                biFunction.getClass();
                u uVar = new u(0, concurrentMap, biFunction);
                if (concurrentMap instanceof v) {
                    ((v) concurrentMap).forEach(uVar);
                } else {
                    a.a(concurrentMap, uVar);
                }
            } else {
                Map.CC.$default$replaceAll(map, biFunction);
            }
        }
    }

    public final /* synthetic */ void replaceAll(java.util.function.BiFunction biFunction) {
        replaceAll(BiFunction.VivifiedWrapper.convert(biFunction));
    }

    public final int size() {
        int size;
        synchronized (this.b) {
            size = this.a.size();
        }
        return size;
    }

    public final String toString() {
        String obj;
        synchronized (this.b) {
            obj = this.a.toString();
        }
        return obj;
    }

    public final Collection values() {
        Collection collection;
        Throwable e;
        Collection collection2;
        synchronized (this.b) {
            if (this.e == null) {
                Collection values = this.a.values();
                Object obj = this.b;
                if (DesugarCollections.e == null) {
                    collection2 = Collections.synchronizedCollection(values);
                } else {
                    try {
                        collection2 = (Collection) DesugarCollections.e.newInstance(values, obj);
                    } catch (IllegalAccessException e2) {
                        e = e2;
                        throw new Error("Unable to instantiate a synchronized list.", e);
                    } catch (InstantiationException e3) {
                        e = e3;
                        throw new Error("Unable to instantiate a synchronized list.", e);
                    } catch (InvocationTargetException e4) {
                        e = e4;
                        throw new Error("Unable to instantiate a synchronized list.", e);
                    }
                }
                this.e = collection2;
            }
            collection = this.e;
        }
        return collection;
    }
}
