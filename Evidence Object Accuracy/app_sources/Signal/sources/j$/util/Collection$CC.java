package j$.util;

import j$.util.function.Predicate;
import j$.util.stream.AbstractC0115s0;
import j$.util.stream.Stream;
import java.util.Collection;
import java.util.Iterator;

/* renamed from: j$.util.Collection$-CC */
/* loaded from: classes2.dex */
public final /* synthetic */ class Collection$CC<E> {
    public static Stream $default$parallelStream(Collection collection) {
        return AbstractC0115s0.r0(Collection$EL.b(collection), true);
    }

    public static boolean $default$removeIf(Collection collection, Predicate predicate) {
        if (DesugarCollections.a.isInstance(collection)) {
            return DesugarCollections.d(collection, predicate);
        }
        predicate.getClass();
        boolean z = false;
        Iterator<E> it = collection.iterator();
        while (it.hasNext()) {
            if (predicate.test(it.next())) {
                it.remove();
                z = true;
            }
        }
        return z;
    }

    public static Stream $default$stream(Collection collection) {
        return AbstractC0115s0.r0(Collection$EL.b(collection), false);
    }
}
