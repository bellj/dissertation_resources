package j$.util;

import java.util.NoSuchElementException;

/* renamed from: j$.util.l */
/* loaded from: classes2.dex */
public final class C0035l {
    private static final C0035l c = new C0035l();
    private final boolean a;
    private final long b;

    private C0035l() {
        this.a = false;
        this.b = 0;
    }

    private C0035l(long j) {
        this.a = true;
        this.b = j;
    }

    public static C0035l a() {
        return c;
    }

    public static C0035l d(long j) {
        return new C0035l(j);
    }

    public final long b() {
        if (this.a) {
            return this.b;
        }
        throw new NoSuchElementException("No value present");
    }

    public final boolean c() {
        return this.a;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C0035l)) {
            return false;
        }
        C0035l lVar = (C0035l) obj;
        boolean z = this.a;
        if (!z || !lVar.a) {
            if (z == lVar.a) {
                return true;
            }
        } else if (this.b == lVar.b) {
            return true;
        }
        return false;
    }

    public final int hashCode() {
        if (!this.a) {
            return 0;
        }
        long j = this.b;
        return (int) (j ^ (j >>> 32));
    }

    public final String toString() {
        return this.a ? String.format("OptionalLong[%s]", Long.valueOf(this.b)) : "OptionalLong.empty";
    }
}
