package j$.util;

import j$.util.function.Consumer;
import j$.util.function.r;
import java.util.Comparator;

/* loaded from: classes2.dex */
public final class P implements A {
    private final int[] a;
    private int b;
    private final int c;
    private final int d;

    public P(int[] iArr, int i, int i2, int i3) {
        this.a = iArr;
        this.b = i;
        this.c = i2;
        this.d = i3 | 64 | 16384;
    }

    @Override // j$.util.A, j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.n(this, consumer);
    }

    @Override // j$.util.Spliterator
    public final int characteristics() {
        return this.d;
    }

    @Override // j$.util.Spliterator
    public final long estimateSize() {
        return (long) (this.c - this.b);
    }

    @Override // j$.util.A, j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.e(this, consumer);
    }

    @Override // j$.util.A
    /* renamed from: g */
    public final boolean tryAdvance(r rVar) {
        rVar.getClass();
        int i = this.b;
        if (i < 0 || i >= this.c) {
            return false;
        }
        int[] iArr = this.a;
        this.b = i + 1;
        rVar.accept(iArr[i]);
        return true;
    }

    @Override // j$.util.Spliterator
    public final Comparator getComparator() {
        if (AbstractC0037n.j(this, 4)) {
            return null;
        }
        throw new IllegalStateException();
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ long getExactSizeIfKnown() {
        return AbstractC0037n.h(this);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean hasCharacteristics(int i) {
        return AbstractC0037n.j(this, i);
    }

    @Override // j$.util.A
    /* renamed from: i */
    public final void forEachRemaining(r rVar) {
        int i;
        rVar.getClass();
        int[] iArr = this.a;
        int length = iArr.length;
        int i2 = this.c;
        if (length >= i2 && (i = this.b) >= 0) {
            this.b = i2;
            if (i < i2) {
                do {
                    rVar.accept(iArr[i]);
                    i++;
                } while (i < i2);
            }
        }
    }

    @Override // j$.util.A, j$.util.E, j$.util.Spliterator
    public final A trySplit() {
        int i = this.b;
        int i2 = (this.c + i) >>> 1;
        if (i >= i2) {
            return null;
        }
        int[] iArr = this.a;
        this.b = i2;
        return new P(iArr, i, i2, this.d);
    }
}
