package j$.util.concurrent;

/* loaded from: classes2.dex */
public final class s extends m {
    s e;
    s f;
    s g;
    s h;
    boolean i;

    public s(int i, Object obj, Object obj2, s sVar, s sVar2) {
        super(i, obj, obj2, sVar);
        this.e = sVar2;
    }

    @Override // j$.util.concurrent.m
    public final m a(int i, Object obj) {
        return b(i, obj, null);
    }

    public final s b(int i, Object obj, Class cls) {
        int compareComparables;
        if (obj == null) {
            return null;
        }
        s sVar = this;
        do {
            sVar = sVar.f;
            s sVar2 = sVar.g;
            int i2 = sVar.a;
            if (i2 <= i) {
                if (i2 >= i) {
                    Object obj2 = sVar.b;
                    if (obj2 == obj || (obj2 != null && obj.equals(obj2))) {
                        return sVar;
                    }
                    if (sVar != null) {
                        if (sVar2 != null) {
                            if ((cls == null && (cls = ConcurrentHashMap.comparableClassFor(obj)) == null) || (compareComparables = ConcurrentHashMap.compareComparables(cls, obj, obj2)) == 0) {
                                s b = sVar2.b(i, obj, cls);
                                if (b != null) {
                                    return b;
                                }
                            } else if (compareComparables >= 0) {
                                sVar = sVar2;
                            }
                        }
                    }
                }
                sVar = sVar2;
                continue;
            }
            continue;
        } while (sVar != null);
        return null;
    }
}
