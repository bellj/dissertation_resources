package j$.util.concurrent;

import j$.util.AbstractC0037n;
import j$.util.C;
import j$.util.function.Consumer;
import java.util.Comparator;

/* loaded from: classes2.dex */
final class B implements C {
    long a;
    final long b;
    final long c;
    final long d;

    public B(long j, long j2, long j3, long j4) {
        this.a = j;
        this.b = j2;
        this.c = j3;
        this.d = j4;
    }

    @Override // j$.util.C, j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.o(this, consumer);
    }

    @Override // j$.util.C
    /* renamed from: b */
    public final void forEachRemaining(j$.util.function.B b) {
        b.getClass();
        long j = this.a;
        long j2 = this.b;
        if (j < j2) {
            this.a = j2;
            long j3 = this.c;
            long j4 = this.d;
            ThreadLocalRandom current = ThreadLocalRandom.current();
            do {
                b.accept(current.e(j3, j4));
                j++;
            } while (j < j2);
        }
    }

    @Override // j$.util.C
    /* renamed from: c */
    public final boolean tryAdvance(j$.util.function.B b) {
        b.getClass();
        long j = this.a;
        if (j >= this.b) {
            return false;
        }
        b.accept(ThreadLocalRandom.current().e(this.c, this.d));
        this.a = j + 1;
        return true;
    }

    @Override // j$.util.Spliterator
    public final int characteristics() {
        return 17728;
    }

    /* renamed from: d */
    public final B trySplit() {
        long j = this.a;
        long j2 = (this.b + j) >>> 1;
        if (j2 <= j) {
            return null;
        }
        this.a = j2;
        return new B(j, j2, this.c, this.d);
    }

    @Override // j$.util.Spliterator
    public final long estimateSize() {
        return this.b - this.a;
    }

    @Override // j$.util.C, j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.f(this, consumer);
    }

    @Override // j$.util.Spliterator
    public final Comparator getComparator() {
        throw new IllegalStateException();
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ long getExactSizeIfKnown() {
        return AbstractC0037n.h(this);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean hasCharacteristics(int i) {
        return AbstractC0037n.j(this, i);
    }
}
