package j$.util.concurrent;

import j$.util.function.BiConsumer;
import j$.util.function.BiFunction;
import j$.util.function.Function;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamField;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import org.thoughtcrime.securesms.database.DraftDatabase;
import sun.misc.Unsafe;

/* loaded from: classes2.dex */
public class ConcurrentHashMap<K, V> extends AbstractMap<K, V> implements ConcurrentMap<K, V>, Serializable, v {
    private static final long ABASE;
    private static final int ASHIFT;
    private static final long BASECOUNT;
    private static final long CELLSBUSY;
    private static final long CELLVALUE;
    private static final int DEFAULT_CAPACITY;
    private static final int DEFAULT_CONCURRENCY_LEVEL;
    static final int HASH_BITS;
    private static final float LOAD_FACTOR;
    private static final int MAXIMUM_CAPACITY;
    static final int MAX_ARRAY_SIZE;
    private static final int MAX_RESIZERS = ((1 << (32 - 16)) - 1);
    private static final int MIN_TRANSFER_STRIDE;
    static final int MIN_TREEIFY_CAPACITY;
    static final int MOVED;
    static final int NCPU = Runtime.getRuntime().availableProcessors();
    static final int RESERVED;
    private static int RESIZE_STAMP_BITS;
    private static final int RESIZE_STAMP_SHIFT = (32 - 16);
    private static final long SIZECTL;
    private static final long TRANSFERINDEX;
    static final int TREEBIN;
    static final int TREEIFY_THRESHOLD;
    private static final Unsafe U;
    static final int UNTREEIFY_THRESHOLD;
    private static final ObjectStreamField[] serialPersistentFields;
    private static final long serialVersionUID;
    private volatile transient long baseCount;
    private volatile transient int cellsBusy;
    private volatile transient d[] counterCells;
    private transient f entrySet;
    private transient j keySet;
    private volatile transient m[] nextTable;
    private volatile transient int sizeCtl;
    volatile transient m[] table;
    private volatile transient int transferIndex;
    private transient t values;

    static {
        MAX_RESIZERS = (1 << (32 - 16)) - 1;
        RESIZE_STAMP_SHIFT = 32 - 16;
        NCPU = Runtime.getRuntime().availableProcessors();
        Class cls = Integer.TYPE;
        serialPersistentFields = new ObjectStreamField[]{new ObjectStreamField("segments", o[].class), new ObjectStreamField("segmentMask", cls), new ObjectStreamField("segmentShift", cls)};
        try {
            Unsafe c = w.c();
            U = c;
            SIZECTL = c.objectFieldOffset(ConcurrentHashMap.class.getDeclaredField("sizeCtl"));
            TRANSFERINDEX = c.objectFieldOffset(ConcurrentHashMap.class.getDeclaredField("transferIndex"));
            BASECOUNT = c.objectFieldOffset(ConcurrentHashMap.class.getDeclaredField("baseCount"));
            CELLSBUSY = c.objectFieldOffset(ConcurrentHashMap.class.getDeclaredField("cellsBusy"));
            CELLVALUE = c.objectFieldOffset(d.class.getDeclaredField(DraftDatabase.DRAFT_VALUE));
            ABASE = (long) c.arrayBaseOffset(m[].class);
            int arrayIndexScale = c.arrayIndexScale(m[].class);
            if (((arrayIndexScale - 1) & arrayIndexScale) == 0) {
                ASHIFT = 31 - Integer.numberOfLeadingZeros(arrayIndexScale);
                return;
            }
            throw new Error("data type scale not a power of two");
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    public ConcurrentHashMap() {
    }

    public ConcurrentHashMap(int i) {
        if (i >= 0) {
            this.sizeCtl = i >= 536870912 ? MAXIMUM_CAPACITY : tableSizeFor(i + (i >>> 1) + 1);
            return;
        }
        throw new IllegalArgumentException();
    }

    public ConcurrentHashMap(int i, float f) {
        this(i, f, 1);
    }

    public ConcurrentHashMap(int i, float f, int i2) {
        if (f <= 0.0f || i < 0 || i2 <= 0) {
            throw new IllegalArgumentException();
        }
        double d = (double) (((float) ((long) (i < i2 ? i2 : i))) / f);
        Double.isNaN(d);
        long j = (long) (d + 1.0d);
        this.sizeCtl = j >= 1073741824 ? MAXIMUM_CAPACITY : tableSizeFor((int) j);
    }

    public ConcurrentHashMap(Map<? extends K, ? extends V> map) {
        this.sizeCtl = 16;
        putAll(map);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0012, code lost:
        if (r1.compareAndSwapLong(r11, r3, r5, r9) == false) goto L_0x0014;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void addCount(long r12, int r14) {
        /*
            r11 = this;
            j$.util.concurrent.d[] r0 = r11.counterCells
            if (r0 != 0) goto L_0x0014
            sun.misc.Unsafe r1 = j$.util.concurrent.ConcurrentHashMap.U
            long r3 = j$.util.concurrent.ConcurrentHashMap.BASECOUNT
            long r5 = r11.baseCount
            long r9 = r5 + r12
            r2 = r11
            r7 = r9
            boolean r1 = r1.compareAndSwapLong(r2, r3, r5, r7)
            if (r1 != 0) goto L_0x003b
        L_0x0014:
            r1 = 1
            if (r0 == 0) goto L_0x0096
            int r2 = r0.length
            int r2 = r2 - r1
            if (r2 < 0) goto L_0x0096
            int r3 = j$.util.concurrent.ThreadLocalRandom.b()
            r2 = r2 & r3
            r4 = r0[r2]
            if (r4 == 0) goto L_0x0096
            sun.misc.Unsafe r3 = j$.util.concurrent.ConcurrentHashMap.U
            long r5 = j$.util.concurrent.ConcurrentHashMap.CELLVALUE
            long r7 = r4.value
            long r9 = r7 + r12
            boolean r0 = r3.compareAndSwapLong(r4, r5, r7, r9)
            if (r0 != 0) goto L_0x0034
            r1 = r0
            goto L_0x0096
        L_0x0034:
            if (r14 > r1) goto L_0x0037
            return
        L_0x0037:
            long r9 = r11.sumCount()
        L_0x003b:
            if (r14 < 0) goto L_0x0095
        L_0x003d:
            int r4 = r11.sizeCtl
            long r12 = (long) r4
            int r14 = (r9 > r12 ? 1 : (r9 == r12 ? 0 : -1))
            if (r14 < 0) goto L_0x0095
            j$.util.concurrent.m[] r12 = r11.table
            if (r12 == 0) goto L_0x0095
            int r13 = r12.length
            r14 = 1073741824(0x40000000, float:2.0)
            if (r13 >= r14) goto L_0x0095
            int r13 = resizeStamp(r13)
            if (r4 >= 0) goto L_0x007c
            int r14 = j$.util.concurrent.ConcurrentHashMap.RESIZE_STAMP_SHIFT
            int r14 = r4 >>> r14
            if (r14 != r13) goto L_0x0095
            int r14 = r13 + 1
            if (r4 == r14) goto L_0x0095
            int r14 = j$.util.concurrent.ConcurrentHashMap.MAX_RESIZERS
            int r13 = r13 + r14
            if (r4 == r13) goto L_0x0095
            j$.util.concurrent.m[] r13 = r11.nextTable
            if (r13 == 0) goto L_0x0095
            int r14 = r11.transferIndex
            if (r14 > 0) goto L_0x006b
            goto L_0x0095
        L_0x006b:
            sun.misc.Unsafe r0 = j$.util.concurrent.ConcurrentHashMap.U
            long r2 = j$.util.concurrent.ConcurrentHashMap.SIZECTL
            int r5 = r4 + 1
            r1 = r11
            boolean r14 = r0.compareAndSwapInt(r1, r2, r4, r5)
            if (r14 == 0) goto L_0x0090
            r11.transfer(r12, r13)
            goto L_0x0090
        L_0x007c:
            sun.misc.Unsafe r0 = j$.util.concurrent.ConcurrentHashMap.U
            long r2 = j$.util.concurrent.ConcurrentHashMap.SIZECTL
            int r14 = j$.util.concurrent.ConcurrentHashMap.RESIZE_STAMP_SHIFT
            int r13 = r13 << r14
            int r5 = r13 + 2
            r1 = r11
            boolean r13 = r0.compareAndSwapInt(r1, r2, r4, r5)
            if (r13 == 0) goto L_0x0090
            r13 = 0
            r11.transfer(r12, r13)
        L_0x0090:
            long r9 = r11.sumCount()
            goto L_0x003d
        L_0x0095:
            return
        L_0x0096:
            r11.fullAddCount(r12, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j$.util.concurrent.ConcurrentHashMap.addCount(long, int):void");
    }

    static final <K, V> boolean casTabAt(m[] mVarArr, int i, m mVar, m mVar2) {
        return AbstractC0013a.a(U, mVarArr, ABASE + (((long) i) << ASHIFT), mVar, mVar2);
    }

    public static Class<?> comparableClassFor(Object obj) {
        Type[] actualTypeArguments;
        if (!(obj instanceof Comparable)) {
            return null;
        }
        Class<?> cls = obj.getClass();
        if (cls == String.class) {
            return cls;
        }
        Type[] genericInterfaces = cls.getGenericInterfaces();
        if (genericInterfaces == null) {
            return null;
        }
        for (Type type : genericInterfaces) {
            if (type instanceof ParameterizedType) {
                ParameterizedType parameterizedType = (ParameterizedType) type;
                if (parameterizedType.getRawType() == Comparable.class && (actualTypeArguments = parameterizedType.getActualTypeArguments()) != null && actualTypeArguments.length == 1 && actualTypeArguments[0] == cls) {
                    return cls;
                }
            }
        }
        return null;
    }

    public static int compareComparables(Class<?> cls, Object obj, Object obj2) {
        if (obj2 == null || obj2.getClass() != cls) {
            return 0;
        }
        return ((Comparable) obj).compareTo(obj2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x009d, code lost:
        if (r24.counterCells != r7) goto L_0x00af;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x009f, code lost:
        r1 = new j$.util.concurrent.d[r8 << 1];
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00a4, code lost:
        if (r2 >= r8) goto L_0x00ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00a6, code lost:
        r1[r2] = r7[r2];
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00ad, code lost:
        r24.counterCells = r1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x001b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0102 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void fullAddCount(long r25, boolean r27) {
        /*
        // Method dump skipped, instructions count: 259
        */
        throw new UnsupportedOperationException("Method not decompiled: j$.util.concurrent.ConcurrentHashMap.fullAddCount(long, boolean):void");
    }

    private final m[] initTable() {
        while (true) {
            m[] mVarArr = this.table;
            if (mVarArr != null && mVarArr.length != 0) {
                return mVarArr;
            }
            int i = this.sizeCtl;
            if (i < 0) {
                Thread.yield();
            } else if (U.compareAndSwapInt(this, SIZECTL, i, -1)) {
                try {
                    m[] mVarArr2 = this.table;
                    if (mVarArr2 == null || mVarArr2.length == 0) {
                        int i2 = i > 0 ? i : 16;
                        m[] mVarArr3 = new m[i2];
                        this.table = mVarArr3;
                        i = i2 - (i2 >>> 2);
                        mVarArr2 = mVarArr3;
                    }
                    return mVarArr2;
                } finally {
                    this.sizeCtl = i;
                }
            }
        }
    }

    private void readObject(ObjectInputStream objectInputStream) {
        long j;
        int i;
        boolean z;
        Object obj;
        this.sizeCtl = -1;
        objectInputStream.defaultReadObject();
        long j2 = 0;
        long j3 = 0;
        m mVar = null;
        while (true) {
            Object readObject = objectInputStream.readObject();
            Object readObject2 = objectInputStream.readObject();
            j = 1;
            if (readObject == null || readObject2 == null) {
                break;
            }
            mVar = new m(spread(readObject.hashCode()), readObject, readObject2, mVar);
            j3++;
        }
        if (j3 == 0) {
            this.sizeCtl = 0;
            return;
        }
        if (j3 >= 536870912) {
            i = MAXIMUM_CAPACITY;
        } else {
            int i2 = (int) j3;
            i = tableSizeFor(i2 + (i2 >>> 1) + 1);
        }
        m[] mVarArr = new m[i];
        int i3 = i - 1;
        while (mVar != null) {
            mVar = mVar.d;
            int i4 = mVar.a;
            int i5 = i4 & i3;
            m tabAt = tabAt(mVarArr, i5);
            if (tabAt == null) {
                z = true;
            } else {
                Object obj2 = mVar.b;
                if (tabAt.a >= 0) {
                    int i6 = 0;
                    for (m mVar2 = tabAt; mVar2 != null; mVar2 = mVar2.d) {
                        if (mVar2.a == i4 && ((obj = mVar2.b) == obj2 || (obj != null && obj2.equals(obj)))) {
                            z = false;
                            break;
                        } else {
                            i6++;
                        }
                    }
                    z = true;
                    if (z && i6 >= 8) {
                        j2++;
                        mVar.d = tabAt;
                        m mVar3 = mVar;
                        s sVar = null;
                        s sVar2 = null;
                        while (mVar3 != null) {
                            s sVar3 = new s(mVar3.a, mVar3.b, mVar3.c, null, null);
                            sVar3.h = sVar2;
                            if (sVar2 == null) {
                                sVar = sVar3;
                            } else {
                                sVar2.d = sVar3;
                            }
                            mVar3 = mVar3.d;
                            sVar2 = sVar3;
                            j2 = j2;
                        }
                        setTabAt(mVarArr, i5, new r(sVar));
                    }
                } else if (((r) tabAt).f(i4, obj2, mVar.c) == null) {
                    j2 += j;
                }
                z = false;
            }
            if (z) {
                j2++;
                mVar.d = tabAt;
                setTabAt(mVarArr, i5, mVar);
            }
            j = 1;
        }
        this.table = mVarArr;
        this.sizeCtl = i - (i >>> 2);
        this.baseCount = j2;
    }

    static final int resizeStamp(int i) {
        return Integer.numberOfLeadingZeros(i) | (1 << (RESIZE_STAMP_BITS - 1));
    }

    static final <K, V> void setTabAt(m[] mVarArr, int i, m mVar) {
        U.putObjectVolatile(mVarArr, (((long) i) << ASHIFT) + ABASE, mVar);
    }

    static final int spread(int i) {
        return (i ^ (i >>> 16)) & Integer.MAX_VALUE;
    }

    public static final <K, V> m tabAt(m[] mVarArr, int i) {
        return (m) U.getObjectVolatile(mVarArr, (((long) i) << ASHIFT) + ABASE);
    }

    private static final int tableSizeFor(int i) {
        int i2 = i - 1;
        int i3 = i2 | (i2 >>> 1);
        int i4 = i3 | (i3 >>> 2);
        int i5 = i4 | (i4 >>> 4);
        int i6 = i5 | (i5 >>> 8);
        int i7 = i6 | (i6 >>> 16);
        if (i7 < 0) {
            return 1;
        }
        return i7 >= MAXIMUM_CAPACITY ? MAXIMUM_CAPACITY : 1 + i7;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:139:0x0104 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r6v17, types: [j$.util.concurrent.m] */
    /* JADX WARN: Type inference failed for: r6v18, types: [j$.util.concurrent.m] */
    /* JADX WARN: Type inference failed for: r1v23 */
    /* JADX WARN: Type inference failed for: r1v24, types: [j$.util.concurrent.m] */
    /* JADX WARN: Type inference failed for: r1v25 */
    /* JADX WARN: Type inference failed for: r1v26, types: [j$.util.concurrent.m] */
    /* JADX WARN: Type inference failed for: r6v22, types: [j$.util.concurrent.m] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void transfer(j$.util.concurrent.m[] r30, j$.util.concurrent.m[] r31) {
        /*
        // Method dump skipped, instructions count: 520
        */
        throw new UnsupportedOperationException("Method not decompiled: j$.util.concurrent.ConcurrentHashMap.transfer(j$.util.concurrent.m[], j$.util.concurrent.m[]):void");
    }

    private final void treeifyBin(m[] mVarArr, int i) {
        if (mVarArr != null) {
            int length = mVarArr.length;
            if (length < 64) {
                tryPresize(length << 1);
                return;
            }
            m tabAt = tabAt(mVarArr, i);
            if (tabAt != null && tabAt.a >= 0) {
                synchronized (tabAt) {
                    if (tabAt(mVarArr, i) == tabAt) {
                        s sVar = null;
                        m mVar = tabAt;
                        s sVar2 = null;
                        while (mVar != null) {
                            s sVar3 = new s(mVar.a, mVar.b, mVar.c, null, null);
                            sVar3.h = sVar2;
                            if (sVar2 == null) {
                                sVar = sVar3;
                            } else {
                                sVar2.d = sVar3;
                            }
                            mVar = mVar.d;
                            sVar2 = sVar3;
                        }
                        setTabAt(mVarArr, i, new r(sVar));
                    }
                }
            }
        }
    }

    private final void tryPresize(int i) {
        int length;
        m[] mVarArr;
        int tableSizeFor = i >= 536870912 ? MAXIMUM_CAPACITY : tableSizeFor(i + (i >>> 1) + 1);
        while (true) {
            int i2 = this.sizeCtl;
            if (i2 >= 0) {
                m[] mVarArr2 = this.table;
                if (mVarArr2 == null || (length = mVarArr2.length) == 0) {
                    int i3 = i2 > tableSizeFor ? i2 : tableSizeFor;
                    if (U.compareAndSwapInt(this, SIZECTL, i2, -1)) {
                        try {
                            if (this.table == mVarArr2) {
                                this.table = new m[i3];
                                i2 = i3 - (i3 >>> 2);
                            }
                        } finally {
                            this.sizeCtl = i2;
                        }
                    } else {
                        continue;
                    }
                } else if (tableSizeFor > i2 && length < MAXIMUM_CAPACITY) {
                    if (mVarArr2 == this.table) {
                        int resizeStamp = resizeStamp(length);
                        if (i2 < 0) {
                            if ((i2 >>> RESIZE_STAMP_SHIFT) == resizeStamp && i2 != resizeStamp + 1 && i2 != resizeStamp + MAX_RESIZERS && (mVarArr = this.nextTable) != null && this.transferIndex > 0) {
                                if (U.compareAndSwapInt(this, SIZECTL, i2, i2 + 1)) {
                                    transfer(mVarArr2, mVarArr);
                                }
                            } else {
                                return;
                            }
                        } else if (U.compareAndSwapInt(this, SIZECTL, i2, (resizeStamp << RESIZE_STAMP_SHIFT) + 2)) {
                            transfer(mVarArr2, null);
                        }
                    } else {
                        continue;
                    }
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    static <K, V> m untreeify(m mVar) {
        m mVar2 = null;
        m mVar3 = null;
        while (mVar != null) {
            m mVar4 = new m(mVar.a, mVar.b, mVar.c, null);
            if (mVar3 == null) {
                mVar2 = mVar4;
            } else {
                mVar3.d = mVar4;
            }
            mVar = mVar.d;
            mVar3 = mVar4;
        }
        return mVar2;
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        int i = 1;
        int i2 = 0;
        while (i < 16) {
            i2++;
            i <<= 1;
        }
        int i3 = 32 - i2;
        int i4 = i - 1;
        o[] oVarArr = new o[16];
        for (int i5 = 0; i5 < 16; i5++) {
            oVarArr[i5] = new o();
        }
        objectOutputStream.putFields().put("segments", oVarArr);
        objectOutputStream.putFields().put("segmentShift", i3);
        objectOutputStream.putFields().put("segmentMask", i4);
        objectOutputStream.writeFields();
        m[] mVarArr = this.table;
        if (mVarArr != null) {
            q qVar = new q(mVarArr, mVarArr.length, 0, mVarArr.length);
            while (true) {
                m d = qVar.d();
                if (d == null) {
                    break;
                }
                objectOutputStream.writeObject(d.b);
                objectOutputStream.writeObject(d.c);
            }
        }
        objectOutputStream.writeObject(null);
        objectOutputStream.writeObject(null);
    }

    @Override // java.util.AbstractMap, java.util.Map, j$.util.Map
    public void clear() {
        m tabAt;
        m[] mVarArr = this.table;
        long j = 0;
        loop0: while (true) {
            int i = 0;
            while (mVarArr != null && i < mVarArr.length) {
                tabAt = tabAt(mVarArr, i);
                if (tabAt == null) {
                    i++;
                } else {
                    int i2 = tabAt.a;
                    if (i2 == -1) {
                        break;
                    }
                    synchronized (tabAt) {
                        if (tabAt(mVarArr, i) == tabAt) {
                            for (m mVar = i2 >= 0 ? tabAt : tabAt instanceof r ? ((r) tabAt).f : null; mVar != null; mVar = mVar.d) {
                                j--;
                            }
                            i++;
                            setTabAt(mVarArr, i, null);
                        }
                    }
                }
            }
            mVarArr = helpTransfer(mVarArr, tabAt);
        }
        if (j != 0) {
            addCount(j, -1);
        }
    }

    @Override // j$.util.Map
    public V compute(K k, BiFunction<? super K, ? super V, ? extends V> biFunction) {
        m mVar;
        Object obj;
        if (k == null || biFunction == null) {
            throw null;
        }
        int spread = spread(k.hashCode());
        m[] mVarArr = this.table;
        int i = 0;
        V v = null;
        int i2 = 0;
        while (true) {
            if (mVarArr != null) {
                int length = mVarArr.length;
                if (length != 0) {
                    int i3 = (length - 1) & spread;
                    m tabAt = tabAt(mVarArr, i3);
                    if (tabAt == null) {
                        n nVar = new n();
                        synchronized (nVar) {
                            if (casTabAt(mVarArr, i3, null, nVar)) {
                                Object apply = biFunction.apply(k, null);
                                if (apply != null) {
                                    mVar = new m(spread, k, apply, null);
                                    i2 = 1;
                                } else {
                                    i2 = i2;
                                    mVar = null;
                                }
                                setTabAt(mVarArr, i3, mVar);
                                v = (V) apply;
                                i = 1;
                            }
                        }
                        if (i != 0) {
                            break;
                        }
                    } else {
                        int i4 = tabAt.a;
                        if (i4 == -1) {
                            mVarArr = helpTransfer(mVarArr, tabAt);
                        } else {
                            synchronized (tabAt) {
                                if (tabAt(mVarArr, i3) == tabAt) {
                                    if (i4 >= 0) {
                                        m mVar2 = null;
                                        m mVar3 = tabAt;
                                        i = 1;
                                        while (true) {
                                            if (mVar3.a != spread || ((obj = mVar3.b) != k && (obj == null || !k.equals(obj)))) {
                                                m mVar4 = mVar3.d;
                                                if (mVar4 == null) {
                                                    Object apply2 = biFunction.apply(k, null);
                                                    if (apply2 != null) {
                                                        mVar3.d = new m(spread, k, apply2, null);
                                                        v = (V) apply2;
                                                        i2 = 1;
                                                    } else {
                                                        v = (V) apply2;
                                                    }
                                                } else {
                                                    i++;
                                                    mVar2 = mVar3;
                                                    mVar3 = mVar4;
                                                }
                                            }
                                        }
                                        v = (V) biFunction.apply(k, (Object) mVar3.c);
                                        if (v != null) {
                                            mVar3.c = v;
                                        } else {
                                            m mVar5 = mVar3.d;
                                            if (mVar2 != null) {
                                                mVar2.d = mVar5;
                                            } else {
                                                setTabAt(mVarArr, i3, mVar5);
                                            }
                                            i2 = -1;
                                        }
                                    } else if (tabAt instanceof r) {
                                        r rVar = (r) tabAt;
                                        s sVar = rVar.e;
                                        s b = sVar != null ? sVar.b(spread, k, null) : null;
                                        Object apply3 = biFunction.apply(k, b == null ? (Object) null : (Object) b.c);
                                        if (apply3 == null) {
                                            if (b != null) {
                                                if (rVar.g(b)) {
                                                    setTabAt(mVarArr, i3, untreeify(rVar.f));
                                                }
                                                v = (V) apply3;
                                                i = 1;
                                                i2 = -1;
                                            }
                                            v = (V) apply3;
                                            i = 1;
                                        } else if (b != null) {
                                            b.c = apply3;
                                            v = (V) apply3;
                                            i = 1;
                                        } else {
                                            rVar.f(spread, k, apply3);
                                            v = (V) apply3;
                                            i = 1;
                                            i2 = 1;
                                        }
                                    }
                                }
                            }
                            if (i != 0) {
                                if (i >= 8) {
                                    treeifyBin(mVarArr, i3);
                                }
                            }
                        }
                    }
                }
            }
            mVarArr = initTable();
        }
        if (i2 != 0) {
            addCount((long) i2, i);
        }
        return v;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.Map, java.util.concurrent.ConcurrentMap
    public /* synthetic */ Object compute(Object obj, java.util.function.BiFunction biFunction) {
        return compute((ConcurrentHashMap<K, V>) obj, (BiFunction<? super ConcurrentHashMap<K, V>, ? super V, ? extends V>) BiFunction.VivifiedWrapper.convert(biFunction));
    }

    @Override // j$.util.Map
    public V computeIfAbsent(K k, Function<? super K, ? extends V> function) {
        s b;
        Object obj;
        if (k == null || function == null) {
            throw null;
        }
        int spread = spread(k.hashCode());
        m[] mVarArr = this.table;
        V v = null;
        int i = 0;
        while (true) {
            if (mVarArr != null) {
                int length = mVarArr.length;
                if (length != 0) {
                    int i2 = (length - 1) & spread;
                    m tabAt = tabAt(mVarArr, i2);
                    boolean z = true;
                    if (tabAt == null) {
                        n nVar = new n();
                        synchronized (nVar) {
                            if (casTabAt(mVarArr, i2, null, nVar)) {
                                Object apply = function.apply(k);
                                setTabAt(mVarArr, i2, apply != null ? new m(spread, k, apply, null) : null);
                                v = (V) apply;
                                i = 1;
                            }
                        }
                        if (i != 0) {
                            break;
                        }
                    } else {
                        int i3 = tabAt.a;
                        if (i3 == -1) {
                            mVarArr = helpTransfer(mVarArr, tabAt);
                        } else {
                            synchronized (tabAt) {
                                if (tabAt(mVarArr, i2) == tabAt) {
                                    if (i3 >= 0) {
                                        m mVar = tabAt;
                                        int i4 = 1;
                                        while (true) {
                                            if (mVar.a != spread || ((obj = mVar.b) != k && (obj == null || !k.equals(obj)))) {
                                                m mVar2 = mVar.d;
                                                if (mVar2 == null) {
                                                    Object apply2 = function.apply(k);
                                                    if (apply2 != null) {
                                                        mVar.d = new m(spread, k, apply2, null);
                                                        v = (V) apply2;
                                                    } else {
                                                        v = (V) apply2;
                                                    }
                                                } else {
                                                    i4++;
                                                    mVar = mVar2;
                                                }
                                            }
                                        }
                                        v = (V) mVar.c;
                                        z = false;
                                        i = i4;
                                    } else if (tabAt instanceof r) {
                                        i = 2;
                                        r rVar = (r) tabAt;
                                        s sVar = rVar.e;
                                        if (sVar == null || (b = sVar.b(spread, k, null)) == null) {
                                            Object apply3 = function.apply(k);
                                            if (apply3 != null) {
                                                rVar.f(spread, k, apply3);
                                                v = (V) apply3;
                                            } else {
                                                v = (V) apply3;
                                            }
                                        } else {
                                            v = (V) b.c;
                                        }
                                    }
                                }
                                z = false;
                            }
                            if (i != 0) {
                                if (i >= 8) {
                                    treeifyBin(mVarArr, i2);
                                }
                                if (!z) {
                                    return v;
                                }
                            }
                        }
                    }
                }
            }
            mVarArr = initTable();
        }
        if (v != null) {
            addCount(1, i);
        }
        return v;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.Map, java.util.concurrent.ConcurrentMap
    public /* synthetic */ Object computeIfAbsent(Object obj, java.util.function.Function function) {
        return computeIfAbsent((ConcurrentHashMap<K, V>) obj, (Function<? super ConcurrentHashMap<K, V>, ? extends V>) Function.VivifiedWrapper.convert(function));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x004a, code lost:
        r5 = (V) r15.apply(r14, (java.lang.Object) r8.c);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0050, code lost:
        if (r5 == null) goto L_0x0055;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0052, code lost:
        r8.c = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0055, code lost:
        r3 = r8.d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0057, code lost:
        if (r10 == null) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0059, code lost:
        r10.d = r3;
     */
    @Override // j$.util.Map
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public V computeIfPresent(K r14, j$.util.function.BiFunction<? super K, ? super V, ? extends V> r15) {
        /*
            r13 = this;
            r0 = 0
            if (r14 == 0) goto L_0x00a7
            if (r15 == 0) goto L_0x00a7
            int r1 = r14.hashCode()
            int r1 = spread(r1)
            j$.util.concurrent.m[] r2 = r13.table
            r3 = 0
            r5 = r0
            r4 = 0
        L_0x0012:
            if (r2 == 0) goto L_0x00a1
            int r6 = r2.length
            if (r6 != 0) goto L_0x0019
            goto L_0x00a1
        L_0x0019:
            int r6 = r6 + -1
            r6 = r6 & r1
            j$.util.concurrent.m r7 = tabAt(r2, r6)
            if (r7 != 0) goto L_0x0024
            goto L_0x0097
        L_0x0024:
            int r8 = r7.a
            r9 = -1
            if (r8 != r9) goto L_0x002e
            j$.util.concurrent.m[] r2 = r13.helpTransfer(r2, r7)
            goto L_0x0012
        L_0x002e:
            monitor-enter(r7)
            j$.util.concurrent.m r10 = tabAt(r2, r6)     // Catch: all -> 0x009e
            if (r10 != r7) goto L_0x0094
            if (r8 < 0) goto L_0x0067
            r4 = 1
            r10 = r0
            r8 = r7
        L_0x003a:
            int r11 = r8.a     // Catch: all -> 0x009e
            if (r11 != r1) goto L_0x005c
            java.lang.Object r11 = r8.b     // Catch: all -> 0x009e
            if (r11 == r14) goto L_0x004a
            if (r11 == 0) goto L_0x005c
            boolean r11 = r14.equals(r11)     // Catch: all -> 0x009e
            if (r11 == 0) goto L_0x005c
        L_0x004a:
            java.lang.Object r5 = r8.c     // Catch: all -> 0x009e
            java.lang.Object r5 = r15.apply(r14, r5)     // Catch: all -> 0x009e
            if (r5 == 0) goto L_0x0055
            r8.c = r5     // Catch: all -> 0x009e
            goto L_0x0094
        L_0x0055:
            j$.util.concurrent.m r3 = r8.d     // Catch: all -> 0x009e
            if (r10 == 0) goto L_0x0090
            r10.d = r3     // Catch: all -> 0x009e
            goto L_0x0093
        L_0x005c:
            j$.util.concurrent.m r10 = r8.d     // Catch: all -> 0x009e
            if (r10 != 0) goto L_0x0061
            goto L_0x0094
        L_0x0061:
            int r4 = r4 + 1
            r12 = r10
            r10 = r8
            r8 = r12
            goto L_0x003a
        L_0x0067:
            boolean r8 = r7 instanceof j$.util.concurrent.r     // Catch: all -> 0x009e
            if (r8 == 0) goto L_0x0094
            r4 = 2
            r8 = r7
            j$.util.concurrent.r r8 = (j$.util.concurrent.r) r8     // Catch: all -> 0x009e
            j$.util.concurrent.s r10 = r8.e     // Catch: all -> 0x009e
            if (r10 == 0) goto L_0x0094
            j$.util.concurrent.s r10 = r10.b(r1, r14, r0)     // Catch: all -> 0x009e
            if (r10 == 0) goto L_0x0094
            java.lang.Object r5 = r10.c     // Catch: all -> 0x009e
            java.lang.Object r5 = r15.apply(r14, r5)     // Catch: all -> 0x009e
            if (r5 == 0) goto L_0x0084
            r10.c = r5     // Catch: all -> 0x009e
            goto L_0x0094
        L_0x0084:
            boolean r3 = r8.g(r10)     // Catch: all -> 0x009e
            if (r3 == 0) goto L_0x0093
            j$.util.concurrent.s r3 = r8.f     // Catch: all -> 0x009e
            j$.util.concurrent.m r3 = untreeify(r3)     // Catch: all -> 0x009e
        L_0x0090:
            setTabAt(r2, r6, r3)     // Catch: all -> 0x009e
        L_0x0093:
            r3 = -1
        L_0x0094:
            monitor-exit(r7)     // Catch: all -> 0x009e
            if (r4 == 0) goto L_0x0012
        L_0x0097:
            if (r3 == 0) goto L_0x009d
            long r14 = (long) r3
            r13.addCount(r14, r4)
        L_0x009d:
            return r5
        L_0x009e:
            r14 = move-exception
            monitor-exit(r7)     // Catch: all -> 0x009e
            throw r14
        L_0x00a1:
            j$.util.concurrent.m[] r2 = r13.initTable()
            goto L_0x0012
        L_0x00a7:
            goto L_0x00a9
        L_0x00a8:
            throw r0
        L_0x00a9:
            goto L_0x00a8
        */
        throw new UnsupportedOperationException("Method not decompiled: j$.util.concurrent.ConcurrentHashMap.computeIfPresent(java.lang.Object, j$.util.function.BiFunction):java.lang.Object");
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.Map, java.util.concurrent.ConcurrentMap
    public /* synthetic */ Object computeIfPresent(Object obj, java.util.function.BiFunction biFunction) {
        return computeIfPresent((ConcurrentHashMap<K, V>) obj, (BiFunction<? super ConcurrentHashMap<K, V>, ? super V, ? extends V>) BiFunction.VivifiedWrapper.convert(biFunction));
    }

    public boolean contains(Object obj) {
        return containsValue(obj);
    }

    @Override // java.util.AbstractMap, java.util.Map, j$.util.Map
    public boolean containsKey(Object obj) {
        return get(obj) != null;
    }

    @Override // java.util.AbstractMap, java.util.Map, j$.util.Map
    public boolean containsValue(Object obj) {
        obj.getClass();
        m[] mVarArr = this.table;
        if (mVarArr != null) {
            q qVar = new q(mVarArr, mVarArr.length, 0, mVarArr.length);
            while (true) {
                m d = qVar.d();
                if (d == null) {
                    break;
                }
                Object obj2 = d.c;
                if (obj2 == obj) {
                    return true;
                }
                if (obj2 != null && obj.equals(obj2)) {
                    return true;
                }
            }
        }
        return false;
    }

    public Enumeration<V> elements() {
        m[] mVarArr = this.table;
        int length = mVarArr == null ? 0 : mVarArr.length;
        return new i(mVarArr, length, length, this, 1);
    }

    @Override // java.util.AbstractMap, java.util.Map, j$.util.Map
    public Set<Map.Entry<K, V>> entrySet() {
        f fVar = this.entrySet;
        if (fVar != null) {
            return fVar;
        }
        f fVar2 = new f(this);
        this.entrySet = fVar2;
        return fVar2;
    }

    @Override // java.util.AbstractMap, java.util.Map, java.lang.Object, j$.util.Map
    public boolean equals(Object obj) {
        V value;
        V v;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Map)) {
            return false;
        }
        Map map = (Map) obj;
        m[] mVarArr = this.table;
        int length = mVarArr == null ? 0 : mVarArr.length;
        q qVar = new q(mVarArr, length, 0, length);
        while (true) {
            m d = qVar.d();
            if (d != null) {
                Object obj2 = d.c;
                Object obj3 = map.get(d.b);
                if (obj3 == null || (obj3 != obj2 && !obj3.equals(obj2))) {
                    break;
                }
            } else {
                for (Map.Entry<K, V> entry : map.entrySet()) {
                    K key = entry.getKey();
                    if (key == null || (value = entry.getValue()) == null || (v = get(key)) == null || (value != v && !value.equals(v))) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    @Override // j$.util.concurrent.v, j$.util.Map
    public void forEach(BiConsumer<? super K, ? super V> biConsumer) {
        biConsumer.getClass();
        m[] mVarArr = this.table;
        if (mVarArr != null) {
            q qVar = new q(mVarArr, mVarArr.length, 0, mVarArr.length);
            while (true) {
                m d = qVar.d();
                if (d != null) {
                    biConsumer.accept((Object) d.b, (Object) d.c);
                } else {
                    return;
                }
            }
        }
    }

    @Override // java.util.Map, java.util.concurrent.ConcurrentMap
    public /* synthetic */ void forEach(java.util.function.BiConsumer biConsumer) {
        forEach(BiConsumer.VivifiedWrapper.convert(biConsumer));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x004d, code lost:
        return (V) r1.c;
     */
    @Override // java.util.AbstractMap, java.util.Map, j$.util.Map
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public V get(java.lang.Object r5) {
        /*
            r4 = this;
            int r0 = r5.hashCode()
            int r0 = spread(r0)
            j$.util.concurrent.m[] r1 = r4.table
            r2 = 0
            if (r1 == 0) goto L_0x004e
            int r3 = r1.length
            if (r3 <= 0) goto L_0x004e
            int r3 = r3 + -1
            r3 = r3 & r0
            j$.util.concurrent.m r1 = tabAt(r1, r3)
            if (r1 == 0) goto L_0x004e
            int r3 = r1.a
            if (r3 != r0) goto L_0x002c
            java.lang.Object r3 = r1.b
            if (r3 == r5) goto L_0x0029
            if (r3 == 0) goto L_0x0037
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x0037
        L_0x0029:
            java.lang.Object r5 = r1.c
            return r5
        L_0x002c:
            if (r3 >= 0) goto L_0x0037
            j$.util.concurrent.m r5 = r1.a(r0, r5)
            if (r5 == 0) goto L_0x0036
            java.lang.Object r2 = r5.c
        L_0x0036:
            return r2
        L_0x0037:
            j$.util.concurrent.m r1 = r1.d
            if (r1 == 0) goto L_0x004e
            int r3 = r1.a
            if (r3 != r0) goto L_0x0037
            java.lang.Object r3 = r1.b
            if (r3 == r5) goto L_0x004b
            if (r3 == 0) goto L_0x0037
            boolean r3 = r5.equals(r3)
            if (r3 == 0) goto L_0x0037
        L_0x004b:
            java.lang.Object r5 = r1.c
            return r5
        L_0x004e:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: j$.util.concurrent.ConcurrentHashMap.get(java.lang.Object):java.lang.Object");
    }

    @Override // java.util.Map, java.util.concurrent.ConcurrentMap, j$.util.Map
    public V getOrDefault(Object obj, V v) {
        V v2 = get(obj);
        return v2 == null ? v : v2;
    }

    @Override // java.util.AbstractMap, java.util.Map, java.lang.Object, j$.util.Map
    public int hashCode() {
        m[] mVarArr = this.table;
        int i = 0;
        if (mVarArr != null) {
            q qVar = new q(mVarArr, mVarArr.length, 0, mVarArr.length);
            while (true) {
                m d = qVar.d();
                if (d == null) {
                    break;
                }
                i += d.c.hashCode() ^ d.b.hashCode();
            }
        }
        return i;
    }

    final m[] helpTransfer(m[] mVarArr, m mVar) {
        m[] mVarArr2;
        int i;
        if (mVarArr == null || !(mVar instanceof h) || (mVarArr2 = ((h) mVar).e) == null) {
            return this.table;
        }
        int resizeStamp = resizeStamp(mVarArr.length);
        while (true) {
            if (mVarArr2 == this.nextTable && this.table == mVarArr && (i = this.sizeCtl) < 0 && (i >>> RESIZE_STAMP_SHIFT) == resizeStamp && i != resizeStamp + 1 && i != MAX_RESIZERS + resizeStamp && this.transferIndex > 0) {
                if (U.compareAndSwapInt(this, SIZECTL, i, i + 1)) {
                    transfer(mVarArr, mVarArr2);
                    break;
                }
            } else {
                break;
            }
        }
        return mVarArr2;
    }

    @Override // java.util.AbstractMap, java.util.Map, j$.util.Map
    public boolean isEmpty() {
        return sumCount() <= 0;
    }

    @Override // java.util.AbstractMap, java.util.Map, j$.util.Map
    public Set<K> keySet() {
        j jVar = this.keySet;
        if (jVar != null) {
            return jVar;
        }
        j jVar2 = new j(this);
        this.keySet = jVar2;
        return jVar2;
    }

    public Enumeration<K> keys() {
        m[] mVarArr = this.table;
        int length = mVarArr == null ? 0 : mVarArr.length;
        return new i(mVarArr, length, length, this, 0);
    }

    public long mappingCount() {
        long sumCount = sumCount();
        if (sumCount < 0) {
            return 0;
        }
        return sumCount;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:77:0x00d9 */
    /* JADX DEBUG: Multi-variable search result rejected for r2v2, resolved type: V */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v0, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r2v1 */
    @Override // j$.util.Map
    public V merge(K k, V v, BiFunction<? super V, ? super V, ? extends V> biFunction) {
        int i;
        Object obj;
        V v2 = v;
        if (k == null || v2 == null || biFunction == null) {
            throw null;
        }
        int spread = spread(k.hashCode());
        m[] mVarArr = this.table;
        int i2 = 0;
        Object obj2 = null;
        int i3 = 0;
        while (true) {
            if (mVarArr != null) {
                int length = mVarArr.length;
                if (length != 0) {
                    int i4 = (length - 1) & spread;
                    m tabAt = tabAt(mVarArr, i4);
                    i = 1;
                    if (tabAt != null) {
                        int i5 = tabAt.a;
                        if (i5 == -1) {
                            mVarArr = helpTransfer(mVarArr, tabAt);
                        } else {
                            synchronized (tabAt) {
                                if (tabAt(mVarArr, i4) == tabAt) {
                                    if (i5 >= 0) {
                                        m mVar = null;
                                        m mVar2 = tabAt;
                                        int i6 = 1;
                                        while (true) {
                                            if (mVar2.a != spread || ((obj = mVar2.b) != k && (obj == null || !k.equals(obj)))) {
                                                m mVar3 = mVar2.d;
                                                if (mVar3 == null) {
                                                    mVar2.d = new m(spread, k, v2, null);
                                                    obj2 = v2;
                                                    i3 = 1;
                                                    break;
                                                }
                                                i6++;
                                                mVar = mVar2;
                                                mVar2 = mVar3;
                                            }
                                        }
                                        obj2 = biFunction.apply((Object) mVar2.c, v2);
                                        if (obj2 != null) {
                                            mVar2.c = obj2;
                                        } else {
                                            m mVar4 = mVar2.d;
                                            if (mVar != null) {
                                                mVar.d = mVar4;
                                            } else {
                                                setTabAt(mVarArr, i4, mVar4);
                                            }
                                            i3 = -1;
                                        }
                                        i2 = i6;
                                    } else if (tabAt instanceof r) {
                                        i2 = 2;
                                        r rVar = (r) tabAt;
                                        s sVar = rVar.e;
                                        s b = sVar == null ? null : sVar.b(spread, k, null);
                                        Object apply = b == null ? v2 : biFunction.apply((Object) b.c, v2);
                                        if (apply == null) {
                                            if (b != null) {
                                                if (rVar.g(b)) {
                                                    setTabAt(mVarArr, i4, untreeify(rVar.f));
                                                }
                                                obj2 = apply;
                                                i3 = -1;
                                            }
                                            obj2 = apply;
                                        } else if (b != null) {
                                            b.c = apply;
                                            obj2 = apply;
                                        } else {
                                            rVar.f(spread, k, apply);
                                            obj2 = apply;
                                            i3 = 1;
                                        }
                                    }
                                }
                            }
                            if (i2 != 0) {
                                if (i2 >= 8) {
                                    treeifyBin(mVarArr, i4);
                                }
                                i = i3;
                                v2 = obj2;
                            }
                        }
                    } else if (casTabAt(mVarArr, i4, null, new m(spread, k, v2, null))) {
                        break;
                    }
                }
            }
            mVarArr = initTable();
        }
        if (i != 0) {
            addCount((long) i, i2);
        }
        return v2;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.Map, java.util.concurrent.ConcurrentMap
    public /* synthetic */ Object merge(Object obj, Object obj2, java.util.function.BiFunction biFunction) {
        return merge((ConcurrentHashMap<K, V>) obj, obj2, BiFunction.VivifiedWrapper.convert(biFunction));
    }

    @Override // java.util.AbstractMap, java.util.Map, j$.util.Map
    public V put(K k, V v) {
        return putVal(k, v, false);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: j$.util.concurrent.ConcurrentHashMap<K, V> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.AbstractMap, java.util.Map, j$.util.Map
    public void putAll(Map<? extends K, ? extends V> map) {
        tryPresize(map.size());
        for (Map.Entry<? extends K, ? extends V> entry : map.entrySet()) {
            putVal(entry.getKey(), entry.getValue(), false);
        }
    }

    @Override // java.util.Map, java.util.concurrent.ConcurrentMap, j$.util.Map
    public V putIfAbsent(K k, V v) {
        return putVal(k, v, true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0053, code lost:
        if (r11 == false) goto L_0x0055;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final V putVal(K r9, V r10, boolean r11) {
        /*
            r8 = this;
            r0 = 0
            if (r9 == 0) goto L_0x0098
            if (r10 == 0) goto L_0x0098
            int r1 = r9.hashCode()
            int r1 = spread(r1)
            r2 = 0
            j$.util.concurrent.m[] r3 = r8.table
        L_0x0010:
            if (r3 == 0) goto L_0x0092
            int r4 = r3.length
            if (r4 != 0) goto L_0x0017
            goto L_0x0092
        L_0x0017:
            int r4 = r4 + -1
            r4 = r4 & r1
            j$.util.concurrent.m r5 = tabAt(r3, r4)
            if (r5 != 0) goto L_0x002c
            j$.util.concurrent.m r5 = new j$.util.concurrent.m
            r5.<init>(r1, r9, r10, r0)
            boolean r4 = casTabAt(r3, r4, r0, r5)
            if (r4 == 0) goto L_0x0010
            goto L_0x0089
        L_0x002c:
            int r6 = r5.a
            r7 = -1
            if (r6 != r7) goto L_0x0036
            j$.util.concurrent.m[] r3 = r8.helpTransfer(r3, r5)
            goto L_0x0010
        L_0x0036:
            monitor-enter(r5)
            j$.util.concurrent.m r7 = tabAt(r3, r4)     // Catch: all -> 0x008f
            if (r7 != r5) goto L_0x007b
            if (r6 < 0) goto L_0x0068
            r2 = 1
            r6 = r5
        L_0x0041:
            int r7 = r6.a     // Catch: all -> 0x008f
            if (r7 != r1) goto L_0x0058
            java.lang.Object r7 = r6.b     // Catch: all -> 0x008f
            if (r7 == r9) goto L_0x0051
            if (r7 == 0) goto L_0x0058
            boolean r7 = r9.equals(r7)     // Catch: all -> 0x008f
            if (r7 == 0) goto L_0x0058
        L_0x0051:
            java.lang.Object r7 = r6.c     // Catch: all -> 0x008f
            if (r11 != 0) goto L_0x007c
        L_0x0055:
            r6.c = r10     // Catch: all -> 0x008f
            goto L_0x007c
        L_0x0058:
            j$.util.concurrent.m r7 = r6.d     // Catch: all -> 0x008f
            if (r7 != 0) goto L_0x0064
            j$.util.concurrent.m r7 = new j$.util.concurrent.m     // Catch: all -> 0x008f
            r7.<init>(r1, r9, r10, r0)     // Catch: all -> 0x008f
            r6.d = r7     // Catch: all -> 0x008f
            goto L_0x007b
        L_0x0064:
            int r2 = r2 + 1
            r6 = r7
            goto L_0x0041
        L_0x0068:
            boolean r6 = r5 instanceof j$.util.concurrent.r     // Catch: all -> 0x008f
            if (r6 == 0) goto L_0x007b
            r2 = 2
            r6 = r5
            j$.util.concurrent.r r6 = (j$.util.concurrent.r) r6     // Catch: all -> 0x008f
            j$.util.concurrent.s r6 = r6.f(r1, r9, r10)     // Catch: all -> 0x008f
            if (r6 == 0) goto L_0x007b
            java.lang.Object r7 = r6.c     // Catch: all -> 0x008f
            if (r11 != 0) goto L_0x007c
            goto L_0x0055
        L_0x007b:
            r7 = r0
        L_0x007c:
            monitor-exit(r5)     // Catch: all -> 0x008f
            if (r2 == 0) goto L_0x0010
            r9 = 8
            if (r2 < r9) goto L_0x0086
            r8.treeifyBin(r3, r4)
        L_0x0086:
            if (r7 == 0) goto L_0x0089
            return r7
        L_0x0089:
            r9 = 1
            r8.addCount(r9, r2)
            return r0
        L_0x008f:
            r9 = move-exception
            monitor-exit(r5)     // Catch: all -> 0x008f
            throw r9
        L_0x0092:
            j$.util.concurrent.m[] r3 = r8.initTable()
            goto L_0x0010
        L_0x0098:
            goto L_0x009a
        L_0x0099:
            throw r0
        L_0x009a:
            goto L_0x0099
        */
        throw new UnsupportedOperationException("Method not decompiled: j$.util.concurrent.ConcurrentHashMap.putVal(java.lang.Object, java.lang.Object, boolean):java.lang.Object");
    }

    @Override // java.util.AbstractMap, java.util.Map, j$.util.Map
    public V remove(Object obj) {
        return replaceNode(obj, null, null);
    }

    @Override // java.util.Map, java.util.concurrent.ConcurrentMap, j$.util.Map
    public boolean remove(Object obj, Object obj2) {
        obj.getClass();
        return (obj2 == null || replaceNode(obj, null, obj2) == null) ? false : true;
    }

    @Override // java.util.Map, java.util.concurrent.ConcurrentMap, j$.util.Map
    public V replace(K k, V v) {
        if (k != null && v != null) {
            return replaceNode(k, v, null);
        }
        throw null;
    }

    @Override // java.util.Map, java.util.concurrent.ConcurrentMap, j$.util.Map
    public boolean replace(K k, V v, V v2) {
        if (k != null && v != null && v2 != null) {
            return replaceNode(k, v2, v) != null;
        }
        throw null;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: j$.util.concurrent.ConcurrentHashMap<K, V> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // j$.util.Map
    public void replaceAll(BiFunction<? super K, ? super V, ? extends V> biFunction) {
        biFunction.getClass();
        m[] mVarArr = this.table;
        if (mVarArr != null) {
            q qVar = new q(mVarArr, mVarArr.length, 0, mVarArr.length);
            while (true) {
                m d = qVar.d();
                if (d != null) {
                    Object obj = (Object) d.c;
                    Object obj2 = (Object) d.b;
                    do {
                        Object apply = biFunction.apply(obj2, obj);
                        apply.getClass();
                        if (replaceNode(obj2, apply, obj) == null) {
                            obj = (Object) get(obj2);
                        }
                    } while (obj != null);
                } else {
                    return;
                }
            }
        }
    }

    @Override // java.util.Map, java.util.concurrent.ConcurrentMap
    public /* synthetic */ void replaceAll(java.util.function.BiFunction biFunction) {
        replaceAll(BiFunction.VivifiedWrapper.convert(biFunction));
    }

    public final V replaceNode(Object obj, V v, Object obj2) {
        int length;
        int i;
        m tabAt;
        V v2;
        s b;
        m untreeify;
        Object obj3;
        int spread = spread(obj.hashCode());
        m[] mVarArr = this.table;
        while (true) {
            if (mVarArr == null || (length = mVarArr.length) == 0 || (tabAt = tabAt(mVarArr, (i = (length - 1) & spread))) == null) {
                break;
            }
            int i2 = tabAt.a;
            if (i2 == -1) {
                mVarArr = helpTransfer(mVarArr, tabAt);
            } else {
                boolean z = false;
                synchronized (tabAt) {
                    if (tabAt(mVarArr, i) == tabAt) {
                        if (i2 >= 0) {
                            m mVar = null;
                            m mVar2 = tabAt;
                            while (true) {
                                if (mVar2.a != spread || ((obj3 = mVar2.b) != obj && (obj3 == null || !obj.equals(obj3)))) {
                                    m mVar3 = mVar2.d;
                                    if (mVar3 == null) {
                                        break;
                                    }
                                    mVar = mVar2;
                                    mVar2 = mVar3;
                                }
                            }
                            v2 = (V) mVar2.c;
                            if (obj2 == null || obj2 == v2 || (v2 != null && obj2.equals(v2))) {
                                if (v != null) {
                                    mVar2.c = v;
                                } else if (mVar != null) {
                                    mVar.d = mVar2.d;
                                } else {
                                    untreeify = mVar2.d;
                                    setTabAt(mVarArr, i, untreeify);
                                }
                                z = true;
                            }
                            v2 = null;
                            z = true;
                        } else if (tabAt instanceof r) {
                            r rVar = (r) tabAt;
                            s sVar = rVar.e;
                            if (sVar != null && (b = sVar.b(spread, obj, null)) != null) {
                                v2 = (V) b.c;
                                if (obj2 == null || obj2 == v2 || (v2 != null && obj2.equals(v2))) {
                                    if (v != null) {
                                        b.c = v;
                                    } else if (rVar.g(b)) {
                                        untreeify = untreeify(rVar.f);
                                        setTabAt(mVarArr, i, untreeify);
                                    }
                                    z = true;
                                }
                            }
                            v2 = null;
                            z = true;
                        }
                    }
                    v2 = null;
                }
                if (z) {
                    if (v2 != null) {
                        if (v == null) {
                            addCount(-1, -1);
                        }
                        return v2;
                    }
                }
            }
        }
        return null;
    }

    @Override // java.util.AbstractMap, java.util.Map, j$.util.Map
    public int size() {
        long sumCount = sumCount();
        if (sumCount < 0) {
            return 0;
        }
        if (sumCount > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        return (int) sumCount;
    }

    public final long sumCount() {
        d[] dVarArr = this.counterCells;
        long j = this.baseCount;
        if (dVarArr != null) {
            for (d dVar : dVarArr) {
                if (dVar != null) {
                    j += dVar.value;
                }
            }
        }
        return j;
    }

    @Override // java.util.AbstractMap, java.lang.Object
    public String toString() {
        m[] mVarArr = this.table;
        int length = mVarArr == null ? 0 : mVarArr.length;
        q qVar = new q(mVarArr, length, 0, length);
        StringBuilder sb = new StringBuilder();
        sb.append('{');
        m d = qVar.d();
        if (d != null) {
            while (true) {
                Object obj = d.b;
                Object obj2 = d.c;
                if (obj == this) {
                    obj = "(this Map)";
                }
                sb.append(obj);
                sb.append('=');
                if (obj2 == this) {
                    obj2 = "(this Map)";
                }
                sb.append(obj2);
                d = qVar.d();
                if (d == null) {
                    break;
                }
                sb.append(',');
                sb.append(' ');
            }
        }
        sb.append('}');
        return sb.toString();
    }

    @Override // java.util.AbstractMap, java.util.Map, j$.util.Map
    public Collection<V> values() {
        t tVar = this.values;
        if (tVar != null) {
            return tVar;
        }
        t tVar2 = new t(this);
        this.values = tVar2;
        return tVar2;
    }
}
