package j$.util.concurrent;

import j$.util.AbstractC0037n;
import j$.util.function.Consumer;
import j$.util.function.r;
import java.util.Comparator;

/* loaded from: classes2.dex */
final class A implements j$.util.A {
    long a;
    final long b;
    final int c;
    final int d;

    public A(long j, long j2, int i, int i2) {
        this.a = j;
        this.b = j2;
        this.c = i;
        this.d = i2;
    }

    @Override // j$.util.A, j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.n(this, consumer);
    }

    @Override // j$.util.Spliterator
    public final int characteristics() {
        return 17728;
    }

    /* renamed from: d */
    public final A trySplit() {
        long j = this.a;
        long j2 = (this.b + j) >>> 1;
        if (j2 <= j) {
            return null;
        }
        this.a = j2;
        return new A(j, j2, this.c, this.d);
    }

    @Override // j$.util.Spliterator
    public final long estimateSize() {
        return this.b - this.a;
    }

    @Override // j$.util.A, j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.e(this, consumer);
    }

    @Override // j$.util.A
    /* renamed from: g */
    public final boolean tryAdvance(r rVar) {
        rVar.getClass();
        long j = this.a;
        if (j >= this.b) {
            return false;
        }
        rVar.accept(ThreadLocalRandom.current().d(this.c, this.d));
        this.a = j + 1;
        return true;
    }

    @Override // j$.util.Spliterator
    public final Comparator getComparator() {
        throw new IllegalStateException();
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ long getExactSizeIfKnown() {
        return AbstractC0037n.h(this);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean hasCharacteristics(int i) {
        return AbstractC0037n.j(this, i);
    }

    @Override // j$.util.A
    /* renamed from: i */
    public final void forEachRemaining(r rVar) {
        rVar.getClass();
        long j = this.a;
        long j2 = this.b;
        if (j < j2) {
            this.a = j2;
            int i = this.c;
            int i2 = this.d;
            ThreadLocalRandom current = ThreadLocalRandom.current();
            do {
                rVar.accept(current.d(i, i2));
                j++;
            } while (j < j2);
        }
    }
}
