package j$.util.concurrent;

import j$.util.AbstractC0037n;
import j$.util.function.AbstractC0024g;
import j$.util.function.Consumer;
import j$.util.y;
import java.util.Comparator;

/* loaded from: classes2.dex */
final class z implements y {
    long a;
    final long b;
    final double c;
    final double d;

    public z(long j, long j2, double d, double d2) {
        this.a = j;
        this.b = j2;
        this.c = d;
        this.d = d2;
    }

    @Override // j$.util.y, j$.util.Spliterator
    public final /* synthetic */ boolean a(Consumer consumer) {
        return AbstractC0037n.l(this, consumer);
    }

    @Override // j$.util.Spliterator
    public final int characteristics() {
        return 17728;
    }

    /* renamed from: d */
    public final z trySplit() {
        long j = this.a;
        long j2 = (this.b + j) >>> 1;
        if (j2 <= j) {
            return null;
        }
        this.a = j2;
        return new z(j, j2, this.c, this.d);
    }

    @Override // j$.util.Spliterator
    public final long estimateSize() {
        return this.b - this.a;
    }

    @Override // j$.util.y, j$.util.Spliterator
    public final /* synthetic */ void forEachRemaining(Consumer consumer) {
        AbstractC0037n.d(this, consumer);
    }

    @Override // j$.util.Spliterator
    public final Comparator getComparator() {
        throw new IllegalStateException();
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ long getExactSizeIfKnown() {
        return AbstractC0037n.h(this);
    }

    @Override // j$.util.Spliterator
    public final /* synthetic */ boolean hasCharacteristics(int i) {
        return AbstractC0037n.j(this, i);
    }

    @Override // j$.util.y
    /* renamed from: k */
    public final boolean tryAdvance(AbstractC0024g gVar) {
        gVar.getClass();
        long j = this.a;
        if (j >= this.b) {
            return false;
        }
        gVar.accept(ThreadLocalRandom.current().c(this.c, this.d));
        this.a = j + 1;
        return true;
    }

    @Override // j$.util.y
    /* renamed from: m */
    public final void forEachRemaining(AbstractC0024g gVar) {
        gVar.getClass();
        long j = this.a;
        long j2 = this.b;
        if (j < j2) {
            this.a = j2;
            double d = this.c;
            double d2 = this.d;
            ThreadLocalRandom current = ThreadLocalRandom.current();
            do {
                gVar.accept(current.c(d, d2));
                j++;
            } while (j < j2);
        }
    }
}
