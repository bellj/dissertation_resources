package j$.util.concurrent;

import j$.util.AbstractC0010a;
import j$.util.Collection$CC;
import j$.util.Spliterator;
import j$.util.function.Consumer;
import j$.util.function.Predicate;
import j$.util.stream.Stream;
import java.util.Collection;
import java.util.Iterator;
import java.util.stream.Stream;

/* loaded from: classes2.dex */
final class t extends c implements AbstractC0010a {
    public t(ConcurrentHashMap concurrentHashMap) {
        super(concurrentHashMap);
    }

    @Override // java.util.Collection
    public final boolean add(Object obj) {
        throw new UnsupportedOperationException();
    }

    @Override // java.util.Collection
    public final boolean addAll(Collection collection) {
        throw new UnsupportedOperationException();
    }

    @Override // j$.util.concurrent.c, java.util.Collection
    public final boolean contains(Object obj) {
        return this.a.containsValue(obj);
    }

    @Override // j$.util.AbstractC0010a, j$.lang.Iterable
    public final void forEach(Consumer consumer) {
        consumer.getClass();
        m[] mVarArr = this.a.table;
        if (mVarArr != null) {
            q qVar = new q(mVarArr, mVarArr.length, 0, mVarArr.length);
            while (true) {
                m d = qVar.d();
                if (d != null) {
                    consumer.accept(d.c);
                } else {
                    return;
                }
            }
        }
    }

    @Override // java.lang.Iterable
    public final /* synthetic */ void forEach(java.util.function.Consumer consumer) {
        forEach(Consumer.VivifiedWrapper.convert(consumer));
    }

    @Override // j$.util.concurrent.c, java.util.Collection, java.lang.Iterable
    public final Iterator iterator() {
        ConcurrentHashMap concurrentHashMap = this.a;
        m[] mVarArr = concurrentHashMap.table;
        int length = mVarArr == null ? 0 : mVarArr.length;
        return new i(mVarArr, length, length, concurrentHashMap, 1);
    }

    @Override // java.util.Collection
    public final /* synthetic */ Stream parallelStream() {
        return Stream.Wrapper.convert(Collection$CC.$default$parallelStream(this));
    }

    @Override // java.util.Collection
    public final boolean remove(Object obj) {
        AbstractC0014b bVar;
        if (obj == null) {
            return false;
        }
        Iterator it = iterator();
        do {
            bVar = (AbstractC0014b) it;
            if (!bVar.hasNext()) {
                return false;
            }
        } while (!obj.equals(((i) it).next()));
        bVar.remove();
        return true;
    }

    @Override // j$.util.AbstractC0010a
    public final /* synthetic */ boolean removeIf(Predicate predicate) {
        return Collection$CC.$default$removeIf(this, predicate);
    }

    @Override // java.util.Collection
    public final /* synthetic */ boolean removeIf(java.util.function.Predicate predicate) {
        return Collection$CC.$default$removeIf(this, Predicate.VivifiedWrapper.convert(predicate));
    }

    @Override // java.util.Collection, java.lang.Iterable, j$.util.AbstractC0010a, j$.lang.Iterable
    /* renamed from: spliterator */
    public final Spliterator mo150spliterator() {
        ConcurrentHashMap concurrentHashMap = this.a;
        long sumCount = concurrentHashMap.sumCount();
        m[] mVarArr = concurrentHashMap.table;
        int length = mVarArr == null ? 0 : mVarArr.length;
        long j = 0;
        if (sumCount >= 0) {
            j = sumCount;
        }
        return new k(mVarArr, length, 0, length, j, 1);
    }
}
