package j$.util.concurrent;

/* renamed from: j$.util.concurrent.b */
/* loaded from: classes2.dex */
public abstract class AbstractC0014b extends q {
    final ConcurrentHashMap i;
    m j;

    public AbstractC0014b(m[] mVarArr, int i, int i2, ConcurrentHashMap concurrentHashMap) {
        super(mVarArr, i, 0, i2);
        this.i = concurrentHashMap;
        d();
    }

    public final boolean hasMoreElements() {
        return this.b != null;
    }

    public final boolean hasNext() {
        return this.b != null;
    }

    public final void remove() {
        m mVar = this.j;
        if (mVar != null) {
            this.j = null;
            this.i.replaceNode(mVar.b, null, null);
            return;
        }
        throw new IllegalStateException();
    }
}
