package j$.util.concurrent;

import java.util.concurrent.locks.LockSupport;
import sun.misc.Unsafe;

/* loaded from: classes2.dex */
public final class r extends m {
    private static final Unsafe h;
    private static final long i;
    s e;
    volatile s f;
    volatile Thread g;
    volatile int lockState;

    static {
        try {
            Unsafe c = w.c();
            h = c;
            i = c.objectFieldOffset(r.class.getDeclaredField("lockState"));
        } catch (Exception e) {
            throw new Error(e);
        }
    }

    public r(s sVar) {
        super(-2, null, null, null);
        int j;
        int compareComparables;
        this.f = sVar;
        sVar = null;
        while (sVar != null) {
            sVar = (s) sVar.d;
            sVar.g = null;
            sVar.f = null;
            if (sVar == null) {
                sVar.e = null;
                sVar.i = false;
            } else {
                Object obj = sVar.b;
                int i2 = sVar.a;
                s sVar2 = sVar;
                Class<?> cls = null;
                while (true) {
                    Object obj2 = sVar2.b;
                    int i3 = sVar2.a;
                    j = i3 > i2 ? -1 : i3 < i2 ? 1 : ((cls == null && (cls = ConcurrentHashMap.comparableClassFor(obj)) == null) || (compareComparables = ConcurrentHashMap.compareComparables(cls, obj, obj2)) == 0) ? j(obj, obj2) : compareComparables;
                    s sVar3 = j <= 0 ? sVar2.f : sVar2.g;
                    if (sVar3 == null) {
                        break;
                    }
                    sVar2 = sVar3;
                }
                sVar.e = sVar2;
                if (j <= 0) {
                    sVar2.f = sVar;
                } else {
                    sVar2.g = sVar;
                }
                sVar = c(sVar, sVar);
            }
        }
        this.e = sVar;
    }

    static s b(s sVar, s sVar) {
        while (sVar != null && sVar != sVar) {
            sVar = sVar.e;
            if (sVar == null) {
                sVar.i = false;
                return sVar;
            } else if (sVar.i) {
                sVar.i = false;
                return sVar;
            } else {
                s sVar2 = sVar.f;
                sVar2 = null;
                if (sVar2 == sVar) {
                    sVar2 = sVar.g;
                    if (sVar2 != null && sVar2.i) {
                        sVar2.i = false;
                        sVar.i = true;
                        sVar = h(sVar, sVar);
                        sVar = sVar.e;
                        sVar2 = sVar == null ? null : sVar.g;
                    }
                    if (sVar2 != null) {
                        s sVar3 = sVar2.f;
                        s sVar4 = sVar2.g;
                        if ((sVar4 != null && sVar4.i) || (sVar3 != null && sVar3.i)) {
                            if (sVar4 == null || !sVar4.i) {
                                if (sVar3 != null) {
                                    sVar3.i = false;
                                }
                                sVar2.i = true;
                                sVar = i(sVar, sVar2);
                                sVar = sVar.e;
                                if (sVar != null) {
                                    sVar2 = sVar.g;
                                }
                            }
                            if (sVar2 != null) {
                                sVar2.i = sVar == null ? false : sVar.i;
                                s sVar5 = sVar2.g;
                                if (sVar5 != null) {
                                    sVar5.i = false;
                                }
                            }
                            if (sVar != null) {
                                sVar.i = false;
                                sVar = h(sVar, sVar);
                            }
                            sVar = sVar;
                        }
                        sVar2.i = true;
                    }
                } else {
                    if (sVar2 != null && sVar2.i) {
                        sVar2.i = false;
                        sVar.i = true;
                        sVar = i(sVar, sVar);
                        sVar = sVar.e;
                        sVar2 = sVar == null ? null : sVar.f;
                    }
                    if (sVar2 != null) {
                        s sVar6 = sVar2.f;
                        s sVar7 = sVar2.g;
                        if ((sVar6 != null && sVar6.i) || (sVar7 != null && sVar7.i)) {
                            if (sVar6 == null || !sVar6.i) {
                                if (sVar7 != null) {
                                    sVar7.i = false;
                                }
                                sVar2.i = true;
                                sVar = h(sVar, sVar2);
                                sVar = sVar.e;
                                if (sVar != null) {
                                    sVar2 = sVar.f;
                                }
                            }
                            if (sVar2 != null) {
                                sVar2.i = sVar == null ? false : sVar.i;
                                s sVar8 = sVar2.f;
                                if (sVar8 != null) {
                                    sVar8.i = false;
                                }
                            }
                            if (sVar != null) {
                                sVar.i = false;
                                sVar = i(sVar, sVar);
                            }
                            sVar = sVar;
                        }
                        sVar2.i = true;
                    }
                }
            }
        }
        return sVar;
    }

    static s c(s sVar, s sVar2) {
        s sVar3;
        sVar2.i = true;
        while (true) {
            s sVar4 = sVar2.e;
            if (sVar4 == null) {
                sVar2.i = false;
                return sVar2;
            } else if (!sVar4.i || (sVar3 = sVar4.e) == null) {
                break;
            } else {
                s sVar5 = sVar3.f;
                if (sVar4 == sVar5) {
                    sVar5 = sVar3.g;
                    if (sVar5 == null || !sVar5.i) {
                        if (sVar2 == sVar4.g) {
                            sVar = h(sVar, sVar4);
                            s sVar6 = sVar4.e;
                            sVar3 = sVar6 == null ? null : sVar6.e;
                            sVar4 = sVar6;
                            sVar2 = sVar4;
                        }
                        if (sVar4 != null) {
                            sVar4.i = false;
                            if (sVar3 != null) {
                                sVar3.i = true;
                                sVar = i(sVar, sVar3);
                            }
                        }
                    } else {
                        sVar5.i = false;
                        sVar4.i = false;
                        sVar3.i = true;
                        sVar2 = sVar3;
                    }
                } else if (sVar5 == null || !sVar5.i) {
                    if (sVar2 == sVar4.f) {
                        sVar = i(sVar, sVar4);
                        s sVar7 = sVar4.e;
                        sVar3 = sVar7 == null ? null : sVar7.e;
                        sVar4 = sVar7;
                        sVar2 = sVar4;
                    }
                    if (sVar4 != null) {
                        sVar4.i = false;
                        if (sVar3 != null) {
                            sVar3.i = true;
                            sVar = h(sVar, sVar3);
                        }
                    }
                } else {
                    sVar5.i = false;
                    sVar4.i = false;
                    sVar3.i = true;
                    sVar2 = sVar3;
                }
            }
        }
        return sVar;
    }

    private final void d() {
        boolean z = false;
        while (true) {
            int i2 = this.lockState;
            if ((i2 & -3) == 0) {
                if (h.compareAndSwapInt(this, i, i2, 1)) {
                    break;
                }
            } else if ((i2 & 2) == 0) {
                if (h.compareAndSwapInt(this, i, i2, i2 | 2)) {
                    z = true;
                    this.g = Thread.currentThread();
                }
            } else if (z) {
                LockSupport.park(this);
            }
        }
        if (z) {
            this.g = null;
        }
    }

    private final void e() {
        if (!h.compareAndSwapInt(this, i, 0, 1)) {
            d();
        }
    }

    static s h(s sVar, s sVar2) {
        s sVar3 = sVar2.g;
        if (sVar3 != null) {
            s sVar4 = sVar3.f;
            sVar2.g = sVar4;
            if (sVar4 != null) {
                sVar4.e = sVar2;
            }
            s sVar5 = sVar2.e;
            sVar3.e = sVar5;
            if (sVar5 == null) {
                sVar3.i = false;
                sVar = sVar3;
            } else if (sVar5.f == sVar2) {
                sVar5.f = sVar3;
            } else {
                sVar5.g = sVar3;
            }
            sVar3.f = sVar2;
            sVar2.e = sVar3;
        }
        return sVar;
    }

    static s i(s sVar, s sVar2) {
        s sVar3 = sVar2.f;
        if (sVar3 != null) {
            s sVar4 = sVar3.g;
            sVar2.f = sVar4;
            if (sVar4 != null) {
                sVar4.e = sVar2;
            }
            s sVar5 = sVar2.e;
            sVar3.e = sVar5;
            if (sVar5 == null) {
                sVar3.i = false;
                sVar = sVar3;
            } else if (sVar5.g == sVar2) {
                sVar5.g = sVar3;
            } else {
                sVar5.f = sVar3;
            }
            sVar3.g = sVar2;
            sVar2.e = sVar3;
        }
        return sVar;
    }

    static int j(Object obj, Object obj2) {
        int compareTo;
        return (obj == null || obj2 == null || (compareTo = obj.getClass().getName().compareTo(obj2.getClass().getName())) == 0) ? System.identityHashCode(obj) <= System.identityHashCode(obj2) ? -1 : 1 : compareTo;
    }

    @Override // j$.util.concurrent.m
    public final m a(int i2, Object obj) {
        Object obj2;
        Thread thread;
        Thread thread2;
        s sVar = null;
        if (obj != null) {
            m mVar = this.f;
            while (mVar != null) {
                int i3 = this.lockState;
                if ((i3 & 3) == 0) {
                    Unsafe unsafe = h;
                    long j = i;
                    if (unsafe.compareAndSwapInt(this, j, i3, i3 + 4)) {
                        try {
                            s sVar2 = this.e;
                            if (sVar2 != null) {
                                sVar = sVar2.b(i2, obj, null);
                            }
                            if (w.a(unsafe, this, j) == 6 && (thread2 = this.g) != null) {
                                LockSupport.unpark(thread2);
                            }
                            return sVar;
                        } catch (Throwable th) {
                            if (w.a(h, this, i) == 6 && (thread = this.g) != null) {
                                LockSupport.unpark(thread);
                            }
                            throw th;
                        }
                    }
                } else if (mVar.a == i2 && ((obj2 = mVar.b) == obj || (obj2 != null && obj.equals(obj2)))) {
                    return mVar;
                } else {
                    mVar = mVar.d;
                }
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0060, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00a3, code lost:
        return null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final j$.util.concurrent.s f(int r16, java.lang.Object r17, java.lang.Object r18) {
        /*
            r15 = this;
            r1 = r15
            r0 = r16
            r4 = r17
            j$.util.concurrent.s r2 = r1.e
            r8 = 0
            r9 = 0
            r10 = r2
            r2 = r9
            r3 = 0
        L_0x000c:
            if (r10 != 0) goto L_0x0022
            j$.util.concurrent.s r8 = new j$.util.concurrent.s
            r6 = 0
            r7 = 0
            r2 = r8
            r3 = r16
            r4 = r17
            r5 = r18
            r2.<init>(r3, r4, r5, r6, r7)
            r1.e = r8
            r1.f = r8
            goto L_0x00a3
        L_0x0022:
            int r5 = r10.a
            r11 = 1
            if (r5 <= r0) goto L_0x002a
            r5 = -1
            r12 = -1
            goto L_0x0069
        L_0x002a:
            if (r5 >= r0) goto L_0x002e
            r12 = 1
            goto L_0x0069
        L_0x002e:
            java.lang.Object r5 = r10.b
            if (r5 == r4) goto L_0x00ab
            if (r5 == 0) goto L_0x003c
            boolean r6 = r4.equals(r5)
            if (r6 == 0) goto L_0x003c
            goto L_0x00ab
        L_0x003c:
            if (r2 != 0) goto L_0x0044
            java.lang.Class r2 = j$.util.concurrent.ConcurrentHashMap.comparableClassFor(r17)
            if (r2 == 0) goto L_0x004a
        L_0x0044:
            int r6 = j$.util.concurrent.ConcurrentHashMap.compareComparables(r2, r4, r5)
            if (r6 != 0) goto L_0x0068
        L_0x004a:
            if (r3 != 0) goto L_0x0062
            j$.util.concurrent.s r3 = r10.f
            if (r3 == 0) goto L_0x0056
            j$.util.concurrent.s r3 = r3.b(r0, r4, r2)
            if (r3 != 0) goto L_0x0060
        L_0x0056:
            j$.util.concurrent.s r3 = r10.g
            if (r3 == 0) goto L_0x0061
            j$.util.concurrent.s r3 = r3.b(r0, r4, r2)
            if (r3 == 0) goto L_0x0061
        L_0x0060:
            return r3
        L_0x0061:
            r3 = 1
        L_0x0062:
            int r5 = j(r4, r5)
            r12 = r5
            goto L_0x0069
        L_0x0068:
            r12 = r6
        L_0x0069:
            if (r12 > 0) goto L_0x006e
            j$.util.concurrent.s r5 = r10.f
            goto L_0x0070
        L_0x006e:
            j$.util.concurrent.s r5 = r10.g
        L_0x0070:
            if (r5 != 0) goto L_0x00a8
            j$.util.concurrent.s r13 = r1.f
            j$.util.concurrent.s r14 = new j$.util.concurrent.s
            r2 = r14
            r3 = r16
            r4 = r17
            r5 = r18
            r6 = r13
            r7 = r10
            r2.<init>(r3, r4, r5, r6, r7)
            r1.f = r14
            if (r13 == 0) goto L_0x0088
            r13.h = r14
        L_0x0088:
            if (r12 > 0) goto L_0x008d
            r10.f = r14
            goto L_0x008f
        L_0x008d:
            r10.g = r14
        L_0x008f:
            boolean r0 = r10.i
            if (r0 != 0) goto L_0x0096
            r14.i = r11
            goto L_0x00a3
        L_0x0096:
            r15.e()
            j$.util.concurrent.s r0 = r1.e     // Catch: all -> 0x00a4
            j$.util.concurrent.s r0 = c(r0, r14)     // Catch: all -> 0x00a4
            r1.e = r0     // Catch: all -> 0x00a4
            r1.lockState = r8
        L_0x00a3:
            return r9
        L_0x00a4:
            r0 = move-exception
            r1.lockState = r8
            throw r0
        L_0x00a8:
            r10 = r5
            goto L_0x000c
        L_0x00ab:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: j$.util.concurrent.r.f(int, java.lang.Object, java.lang.Object):j$.util.concurrent.s");
    }

    /* JADX WARNING: Removed duplicated region for block: B:56:0x008e A[Catch: all -> 0x00c8, TryCatch #0 {all -> 0x00c8, blocks: (B:21:0x0030, B:25:0x0039, B:28:0x003f, B:30:0x004d, B:31:0x0052, B:33:0x0058, B:35:0x005c, B:36:0x005f, B:37:0x0061, B:38:0x0065, B:40:0x006b, B:41:0x006d, B:44:0x0077, B:46:0x007b, B:47:0x007e, B:56:0x008e, B:59:0x0096, B:61:0x009a, B:62:0x009d, B:63:0x009f, B:64:0x00a5, B:67:0x00aa, B:68:0x00ae, B:70:0x00b2, B:72:0x00b6, B:74:0x00ba, B:75:0x00bd, B:77:0x00c1, B:78:0x00c3), top: B:85:0x0030 }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00aa A[Catch: all -> 0x00c8, TryCatch #0 {all -> 0x00c8, blocks: (B:21:0x0030, B:25:0x0039, B:28:0x003f, B:30:0x004d, B:31:0x0052, B:33:0x0058, B:35:0x005c, B:36:0x005f, B:37:0x0061, B:38:0x0065, B:40:0x006b, B:41:0x006d, B:44:0x0077, B:46:0x007b, B:47:0x007e, B:56:0x008e, B:59:0x0096, B:61:0x009a, B:62:0x009d, B:63:0x009f, B:64:0x00a5, B:67:0x00aa, B:68:0x00ae, B:70:0x00b2, B:72:0x00b6, B:74:0x00ba, B:75:0x00bd, B:77:0x00c1, B:78:0x00c3), top: B:85:0x0030 }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x00b2 A[Catch: all -> 0x00c8, TryCatch #0 {all -> 0x00c8, blocks: (B:21:0x0030, B:25:0x0039, B:28:0x003f, B:30:0x004d, B:31:0x0052, B:33:0x0058, B:35:0x005c, B:36:0x005f, B:37:0x0061, B:38:0x0065, B:40:0x006b, B:41:0x006d, B:44:0x0077, B:46:0x007b, B:47:0x007e, B:56:0x008e, B:59:0x0096, B:61:0x009a, B:62:0x009d, B:63:0x009f, B:64:0x00a5, B:67:0x00aa, B:68:0x00ae, B:70:0x00b2, B:72:0x00b6, B:74:0x00ba, B:75:0x00bd, B:77:0x00c1, B:78:0x00c3), top: B:85:0x0030 }] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x00ba A[Catch: all -> 0x00c8, TryCatch #0 {all -> 0x00c8, blocks: (B:21:0x0030, B:25:0x0039, B:28:0x003f, B:30:0x004d, B:31:0x0052, B:33:0x0058, B:35:0x005c, B:36:0x005f, B:37:0x0061, B:38:0x0065, B:40:0x006b, B:41:0x006d, B:44:0x0077, B:46:0x007b, B:47:0x007e, B:56:0x008e, B:59:0x0096, B:61:0x009a, B:62:0x009d, B:63:0x009f, B:64:0x00a5, B:67:0x00aa, B:68:0x00ae, B:70:0x00b2, B:72:0x00b6, B:74:0x00ba, B:75:0x00bd, B:77:0x00c1, B:78:0x00c3), top: B:85:0x0030 }] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x00bd A[Catch: all -> 0x00c8, TryCatch #0 {all -> 0x00c8, blocks: (B:21:0x0030, B:25:0x0039, B:28:0x003f, B:30:0x004d, B:31:0x0052, B:33:0x0058, B:35:0x005c, B:36:0x005f, B:37:0x0061, B:38:0x0065, B:40:0x006b, B:41:0x006d, B:44:0x0077, B:46:0x007b, B:47:0x007e, B:56:0x008e, B:59:0x0096, B:61:0x009a, B:62:0x009d, B:63:0x009f, B:64:0x00a5, B:67:0x00aa, B:68:0x00ae, B:70:0x00b2, B:72:0x00b6, B:74:0x00ba, B:75:0x00bd, B:77:0x00c1, B:78:0x00c3), top: B:85:0x0030 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean g(j$.util.concurrent.s r11) {
        /*
        // Method dump skipped, instructions count: 205
        */
        throw new UnsupportedOperationException("Method not decompiled: j$.util.concurrent.r.g(j$.util.concurrent.s):boolean");
    }
}
