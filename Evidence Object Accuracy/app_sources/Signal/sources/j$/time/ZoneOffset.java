package j$.time;

import j$.time.temporal.ChronoField;
import j$.time.temporal.TemporalAccessor;
import j$.time.temporal.TemporalField;
import j$.time.temporal.j;
import j$.time.temporal.k;
import j$.time.temporal.l;
import j$.time.temporal.n;
import j$.time.temporal.p;
import j$.time.temporal.q;
import j$.time.zone.c;
import j$.util.concurrent.ConcurrentHashMap;

/* loaded from: classes2.dex */
public final class ZoneOffset extends ZoneId implements TemporalAccessor, k, Comparable<ZoneOffset> {
    private static final ConcurrentHashMap d = new ConcurrentHashMap(16, 0.75f, 4);
    private static final ConcurrentHashMap e = new ConcurrentHashMap(16, 0.75f, 4);
    public static final ZoneOffset f = r(0);
    public static final ZoneOffset g = r(-64800);
    public static final ZoneOffset h = r(64800);
    private final int b;
    private final transient String c;

    private ZoneOffset(int i) {
        String str;
        this.b = i;
        if (i == 0) {
            str = "Z";
        } else {
            int abs = Math.abs(i);
            StringBuilder sb = new StringBuilder();
            int i2 = abs / 3600;
            int i3 = (abs / 60) % 60;
            sb.append(i < 0 ? "-" : "+");
            sb.append(i2 < 10 ? "0" : "");
            sb.append(i2);
            String str2 = ":0";
            sb.append(i3 < 10 ? str2 : ":");
            sb.append(i3);
            int i4 = abs % 60;
            if (i4 != 0) {
                sb.append(i4 >= 10 ? ":" : str2);
                sb.append(i4);
            }
            str = sb.toString();
        }
        this.c = str;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0096 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00b2  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00ba  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static j$.time.ZoneOffset p(java.lang.String r7) {
        /*
            if (r7 == 0) goto L_0x00bf
            j$.util.concurrent.ConcurrentHashMap r0 = j$.time.ZoneOffset.e
            java.lang.Object r0 = r0.get(r7)
            j$.time.ZoneOffset r0 = (j$.time.ZoneOffset) r0
            if (r0 == 0) goto L_0x000d
            return r0
        L_0x000d:
            int r0 = r7.length()
            r1 = 2
            r2 = 1
            r3 = 0
            if (r0 == r1) goto L_0x006a
            r1 = 3
            if (r0 == r1) goto L_0x0086
            r4 = 5
            if (r0 == r4) goto L_0x0061
            r5 = 6
            r6 = 4
            if (r0 == r5) goto L_0x0058
            r5 = 7
            if (r0 == r5) goto L_0x004b
            r1 = 9
            if (r0 != r1) goto L_0x0034
            int r0 = s(r7, r2, r3)
            int r1 = s(r7, r6, r2)
            int r2 = s(r7, r5, r2)
            goto L_0x008c
        L_0x0034:
            j$.time.e r0 = new j$.time.e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Invalid ID for ZoneOffset, invalid format: "
            r1.append(r2)
            r1.append(r7)
            java.lang.String r7 = r1.toString()
            r0.<init>(r7)
            throw r0
        L_0x004b:
            int r0 = s(r7, r2, r3)
            int r1 = s(r7, r1, r3)
            int r2 = s(r7, r4, r3)
            goto L_0x008c
        L_0x0058:
            int r0 = s(r7, r2, r3)
            int r1 = s(r7, r6, r2)
            goto L_0x008b
        L_0x0061:
            int r0 = s(r7, r2, r3)
            int r1 = s(r7, r1, r3)
            goto L_0x008b
        L_0x006a:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            char r1 = r7.charAt(r3)
            r0.append(r1)
            java.lang.String r1 = "0"
            r0.append(r1)
            char r7 = r7.charAt(r2)
            r0.append(r7)
            java.lang.String r7 = r0.toString()
        L_0x0086:
            int r0 = s(r7, r2, r3)
            r1 = 0
        L_0x008b:
            r2 = 0
        L_0x008c:
            char r3 = r7.charAt(r3)
            r4 = 43
            r5 = 45
            if (r3 == r4) goto L_0x00b0
            if (r3 != r5) goto L_0x0099
            goto L_0x00b0
        L_0x0099:
            j$.time.e r0 = new j$.time.e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Invalid ID for ZoneOffset, plus/minus not found when expected: "
            r1.append(r2)
            r1.append(r7)
            java.lang.String r7 = r1.toString()
            r0.<init>(r7)
            throw r0
        L_0x00b0:
            if (r3 != r5) goto L_0x00ba
            int r7 = -r0
            int r0 = -r1
            int r1 = -r2
            j$.time.ZoneOffset r7 = q(r7, r0, r1)
            return r7
        L_0x00ba:
            j$.time.ZoneOffset r7 = q(r0, r1, r2)
            return r7
        L_0x00bf:
            java.lang.NullPointerException r7 = new java.lang.NullPointerException
            java.lang.String r0 = "offsetId"
            r7.<init>(r0)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: j$.time.ZoneOffset.p(java.lang.String):j$.time.ZoneOffset");
    }

    public static ZoneOffset q(int i, int i2, int i3) {
        if (i < -18 || i > 18) {
            throw new e("Zone offset hours not in valid range: value " + i + " is not in the range -18 to 18");
        }
        if (i > 0) {
            if (i2 < 0 || i3 < 0) {
                throw new e("Zone offset minutes and seconds must be positive because hours is positive");
            }
        } else if (i < 0) {
            if (i2 > 0 || i3 > 0) {
                throw new e("Zone offset minutes and seconds must be negative because hours is negative");
            }
        } else if ((i2 > 0 && i3 < 0) || (i2 < 0 && i3 > 0)) {
            throw new e("Zone offset minutes and seconds must have the same sign");
        }
        if (i2 < -59 || i2 > 59) {
            throw new e("Zone offset minutes not in valid range: value " + i2 + " is not in the range -59 to 59");
        } else if (i3 < -59 || i3 > 59) {
            throw new e("Zone offset seconds not in valid range: value " + i3 + " is not in the range -59 to 59");
        } else if (Math.abs(i) != 18 || (i2 | i3) == 0) {
            return r((i2 * 60) + (i * 3600) + i3);
        } else {
            throw new e("Zone offset not in valid range: -18:00 to +18:00");
        }
    }

    public static ZoneOffset r(int i) {
        if (i < -64800 || i > 64800) {
            throw new e("Zone offset not in valid range: -18:00 to +18:00");
        } else if (i % 900 != 0) {
            return new ZoneOffset(i);
        } else {
            Integer valueOf = Integer.valueOf(i);
            ConcurrentHashMap concurrentHashMap = d;
            ZoneOffset zoneOffset = (ZoneOffset) concurrentHashMap.get(valueOf);
            if (zoneOffset != null) {
                return zoneOffset;
            }
            concurrentHashMap.putIfAbsent(valueOf, new ZoneOffset(i));
            ZoneOffset zoneOffset2 = (ZoneOffset) concurrentHashMap.get(valueOf);
            e.putIfAbsent(zoneOffset2.c, zoneOffset2);
            return zoneOffset2;
        }
    }

    private static int s(CharSequence charSequence, int i, boolean z) {
        if (!z || charSequence.charAt(i - 1) == ':') {
            char charAt = charSequence.charAt(i);
            char charAt2 = charSequence.charAt(i + 1);
            if (charAt < '0' || charAt > '9' || charAt2 < '0' || charAt2 > '9') {
                throw new e("Invalid ID for ZoneOffset, non numeric characters found: " + ((Object) charSequence));
            }
            return (charAt2 - '0') + ((charAt - '0') * 10);
        }
        throw new e("Invalid ID for ZoneOffset, colon not found when expected: " + ((Object) charSequence));
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final int c(TemporalField temporalField) {
        if (temporalField == ChronoField.OFFSET_SECONDS) {
            return this.b;
        }
        if (!(temporalField instanceof ChronoField)) {
            return l.c(this, temporalField).a(getLong(temporalField), temporalField);
        }
        throw new p("Unsupported field: " + temporalField);
    }

    @Override // java.lang.Comparable
    public final int compareTo(ZoneOffset zoneOffset) {
        return zoneOffset.b - this.b;
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final q e(TemporalField temporalField) {
        return l.c(this, temporalField);
    }

    @Override // j$.time.ZoneId, java.lang.Object
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof ZoneOffset) && this.b == ((ZoneOffset) obj).b;
    }

    @Override // j$.time.temporal.k
    public final j g(j jVar) {
        return jVar.a((long) this.b, ChronoField.OFFSET_SECONDS);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final long getLong(TemporalField temporalField) {
        if (temporalField == ChronoField.OFFSET_SECONDS) {
            return (long) this.b;
        }
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.h(this);
        }
        throw new p("Unsupported field: " + temporalField);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final boolean h(TemporalField temporalField) {
        return temporalField instanceof ChronoField ? temporalField == ChronoField.OFFSET_SECONDS : temporalField != null && temporalField.c(this);
    }

    @Override // j$.time.ZoneId, java.lang.Object
    public final int hashCode() {
        return this.b;
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final Object k(n nVar) {
        return (nVar == l.h() || nVar == l.j()) ? this : l.b(this, nVar);
    }

    @Override // j$.time.ZoneId
    public final String l() {
        return this.c;
    }

    @Override // j$.time.ZoneId
    public final c m() {
        return c.j(this);
    }

    public final int o() {
        return this.b;
    }

    @Override // j$.time.ZoneId, java.lang.Object
    public final String toString() {
        return this.c;
    }
}
