package j$.time.zone;

import j$.util.concurrent.ConcurrentHashMap;
import java.security.AccessController;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

/* loaded from: classes2.dex */
public abstract class g {
    private static final CopyOnWriteArrayList a;
    private static final ConcurrentHashMap b = new ConcurrentHashMap(512, 0.75f, 2);

    static {
        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
        a = copyOnWriteArrayList;
        b = new ConcurrentHashMap(512, 0.75f, 2);
        ArrayList arrayList = new ArrayList();
        AccessController.doPrivileged(new e(arrayList));
        copyOnWriteArrayList.addAll(arrayList);
    }

    public static HashSet a() {
        return new HashSet(b.keySet());
    }

    public static c b(String str, boolean z) {
        if (str != null) {
            ConcurrentHashMap concurrentHashMap = b;
            g gVar = (g) concurrentHashMap.get(str);
            if (gVar != null) {
                return gVar.c(str);
            }
            if (concurrentHashMap.isEmpty()) {
                throw new d("No time-zone data files registered");
            }
            throw new d("Unknown time-zone ID: " + str);
        }
        throw new NullPointerException("zoneId");
    }

    public static void e(g gVar) {
        if (gVar != null) {
            for (String str : gVar.d()) {
                if (str == null) {
                    throw new NullPointerException("zoneId");
                } else if (((g) b.putIfAbsent(str, gVar)) != null) {
                    throw new d("Unable to register zone as one already registered with that ID: " + str + ", currently loading from provider: " + gVar);
                }
            }
            a.add(gVar);
            return;
        }
        throw new NullPointerException("provider");
    }

    protected abstract c c(String str);

    protected abstract Set d();
}
