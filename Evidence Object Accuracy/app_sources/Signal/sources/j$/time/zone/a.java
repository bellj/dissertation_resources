package j$.time.zone;

import j$.time.Duration;
import j$.time.Instant;
import j$.time.LocalDateTime;
import j$.time.ZoneOffset;
import j$.time.b;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* loaded from: classes2.dex */
public final class a implements Comparable, Serializable {
    private final LocalDateTime a;
    private final ZoneOffset b;
    private final ZoneOffset c;

    public a(long j, ZoneOffset zoneOffset, ZoneOffset zoneOffset2) {
        this.a = LocalDateTime.r(j, 0, zoneOffset);
        this.b = zoneOffset;
        this.c = zoneOffset2;
    }

    public a(LocalDateTime localDateTime, ZoneOffset zoneOffset, ZoneOffset zoneOffset2) {
        this.a = localDateTime;
        this.b = zoneOffset;
        this.c = zoneOffset2;
    }

    public final LocalDateTime a() {
        return this.a.t((long) (this.c.o() - this.b.o()));
    }

    public final LocalDateTime c() {
        return this.a;
    }

    @Override // java.lang.Comparable
    public final int compareTo(Object obj) {
        a aVar = (a) obj;
        LocalDateTime localDateTime = this.a;
        Instant o = Instant.o(localDateTime.toEpochSecond(this.b), (long) localDateTime.toLocalTime().o());
        LocalDateTime localDateTime2 = aVar.a;
        return o.compareTo(Instant.o(localDateTime2.toEpochSecond(aVar.b), (long) localDateTime2.toLocalTime().o()));
    }

    public final Duration d() {
        return Duration.h((long) (this.c.o() - this.b.o()));
    }

    public final ZoneOffset e() {
        return this.c;
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof a)) {
            return false;
        }
        a aVar = (a) obj;
        return this.a.equals(aVar.a) && this.b.equals(aVar.b) && this.c.equals(aVar.c);
    }

    public final ZoneOffset f() {
        return this.b;
    }

    public final List g() {
        return h() ? Collections.emptyList() : Arrays.asList(this.b, this.c);
    }

    public final boolean h() {
        return this.c.o() > this.b.o();
    }

    @Override // java.lang.Object
    public final int hashCode() {
        return (this.a.hashCode() ^ this.b.hashCode()) ^ Integer.rotateLeft(this.c.hashCode(), 16);
    }

    public final long j() {
        return this.a.toEpochSecond(this.b);
    }

    @Override // java.lang.Object
    public final String toString() {
        StringBuilder a = b.a("Transition[");
        a.append(h() ? "Gap" : "Overlap");
        a.append(" at ");
        a.append(this.a);
        a.append(this.b);
        a.append(" to ");
        a.append(this.c);
        a.append(']');
        return a.toString();
    }
}
