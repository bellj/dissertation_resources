package j$.time.zone;

import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes2.dex */
final class e implements PrivilegedAction {
    final /* synthetic */ List a;

    public e(ArrayList arrayList) {
        this.a = arrayList;
    }

    @Override // java.security.PrivilegedAction
    public final Object run() {
        String property = System.getProperty("java.time.zone.DefaultZoneRulesProvider");
        if (property != null) {
            try {
                g gVar = (g) g.class.cast(Class.forName(property, true, g.class.getClassLoader()).newInstance());
                g.e(gVar);
                this.a.add(gVar);
                return null;
            } catch (Exception e) {
                throw new Error(e);
            }
        } else {
            g.e(new f());
            return null;
        }
    }
}
