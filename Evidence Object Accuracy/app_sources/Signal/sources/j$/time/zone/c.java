package j$.time.zone;

import j$.time.Instant;
import j$.time.LocalDateTime;
import j$.time.ZoneOffset;
import j$.time.a;
import j$.time.b;
import j$.time.d;
import j$.time.h;
import j$.util.AbstractC0037n;
import j$.util.concurrent.ConcurrentHashMap;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;
import org.thoughtcrime.securesms.util.MessageRecordUtil;

/* loaded from: classes2.dex */
public final class c implements Serializable {
    private static final long[] i = new long[0];
    private static final b[] j = new b[0];
    private static final LocalDateTime[] k = new LocalDateTime[0];
    private static final a[] l = new a[0];
    private final long[] a;
    private final ZoneOffset[] b;
    private final long[] c;
    private final LocalDateTime[] d;
    private final ZoneOffset[] e;
    private final b[] f;
    private final TimeZone g;
    private final transient ConcurrentHashMap h = new ConcurrentHashMap();

    private c(ZoneOffset zoneOffset) {
        this.b = r0;
        ZoneOffset[] zoneOffsetArr = {zoneOffset};
        long[] jArr = i;
        this.a = jArr;
        this.c = jArr;
        this.d = k;
        this.e = zoneOffsetArr;
        this.f = j;
        this.g = null;
    }

    public c(TimeZone timeZone) {
        this.b = r0;
        ZoneOffset[] zoneOffsetArr = {k(timeZone.getRawOffset())};
        long[] jArr = i;
        this.a = jArr;
        this.c = jArr;
        this.d = k;
        this.e = zoneOffsetArr;
        this.f = j;
        this.g = timeZone;
    }

    private static Object a(LocalDateTime localDateTime, a aVar) {
        LocalDateTime c = aVar.c();
        boolean h = aVar.h();
        boolean isBefore = localDateTime.isBefore(c);
        return h ? isBefore ? aVar.f() : localDateTime.isBefore(aVar.a()) ? aVar : aVar.e() : !isBefore ? aVar.e() : localDateTime.isBefore(aVar.a()) ? aVar.f() : aVar;
    }

    private a[] b(int i2) {
        Integer valueOf = Integer.valueOf(i2);
        a[] aVarArr = (a[]) this.h.get(valueOf);
        if (aVarArr != null) {
            return aVarArr;
        }
        if (this.g == null) {
            b[] bVarArr = this.f;
            a[] aVarArr2 = new a[bVarArr.length];
            if (bVarArr.length <= 0) {
                if (i2 < 2100) {
                    this.h.putIfAbsent(valueOf, aVarArr2);
                }
                return aVarArr2;
            }
            b bVar = bVarArr[0];
            throw null;
        } else if (i2 < 1800) {
            return l;
        } else {
            long epochSecond = LocalDateTime.o(i2 - 1).toEpochSecond(this.b[0]);
            int offset = this.g.getOffset(epochSecond * 1000);
            long j2 = 31968000 + epochSecond;
            a[] aVarArr3 = l;
            while (epochSecond < j2) {
                long j3 = 7776000 + epochSecond;
                if (offset != this.g.getOffset(j3 * 1000)) {
                    epochSecond = epochSecond;
                    while (j3 - epochSecond > 1) {
                        long e = a.e(j3 + epochSecond, 2);
                        if (this.g.getOffset(e * 1000) == offset) {
                            epochSecond = e;
                        } else {
                            j3 = e;
                        }
                        offset = offset;
                        j2 = j2;
                    }
                    j2 = j2;
                    if (this.g.getOffset(epochSecond * 1000) == offset) {
                        epochSecond = j3;
                    }
                    ZoneOffset k2 = k(offset);
                    offset = this.g.getOffset(epochSecond * 1000);
                    ZoneOffset k3 = k(offset);
                    if (c(epochSecond, k3) == i2) {
                        a[] aVarArr4 = (a[]) Arrays.copyOf(aVarArr3, aVarArr3.length + 1);
                        aVarArr4[aVarArr4.length - 1] = new a(epochSecond, k2, k3);
                        aVarArr3 = aVarArr4;
                    }
                } else {
                    j2 = j2;
                    epochSecond = j3;
                }
            }
            if (1916 <= i2 && i2 < 2100) {
                this.h.putIfAbsent(valueOf, aVarArr3);
            }
            return aVarArr3;
        }
    }

    private static int c(long j2, ZoneOffset zoneOffset) {
        return h.v(a.e(j2 + ((long) zoneOffset.o()), 86400)).r();
    }

    private Object e(LocalDateTime localDateTime) {
        Object obj = null;
        int i2 = 0;
        if (this.g != null) {
            a[] b = b(localDateTime.getYear());
            if (b.length == 0) {
                return k(this.g.getOffset(localDateTime.toEpochSecond(this.b[0]) * 1000));
            }
            int length = b.length;
            while (i2 < length) {
                a aVar = b[i2];
                Object a = a(localDateTime, aVar);
                if ((a instanceof a) || a.equals(aVar.f())) {
                    return a;
                }
                i2++;
                obj = a;
            }
            return obj;
        } else if (this.c.length == 0) {
            return this.b[0];
        } else {
            if (this.f.length > 0) {
                LocalDateTime[] localDateTimeArr = this.d;
                if (localDateTime.isAfter(localDateTimeArr[localDateTimeArr.length - 1])) {
                    a[] b2 = b(localDateTime.getYear());
                    int length2 = b2.length;
                    while (i2 < length2) {
                        a aVar2 = b2[i2];
                        Object a2 = a(localDateTime, aVar2);
                        if ((a2 instanceof a) || a2.equals(aVar2.f())) {
                            return a2;
                        }
                        i2++;
                        obj = a2;
                    }
                    return obj;
                }
            }
            int binarySearch = Arrays.binarySearch(this.d, localDateTime);
            if (binarySearch == -1) {
                return this.e[0];
            }
            if (binarySearch < 0) {
                binarySearch = (-binarySearch) - 2;
            } else {
                LocalDateTime[] localDateTimeArr2 = this.d;
                if (binarySearch < localDateTimeArr2.length - 1) {
                    int i3 = binarySearch + 1;
                    if (localDateTimeArr2[binarySearch].equals(localDateTimeArr2[i3])) {
                        binarySearch = i3;
                    }
                }
            }
            if ((binarySearch & 1) != 0) {
                return this.e[(binarySearch / 2) + 1];
            }
            LocalDateTime[] localDateTimeArr3 = this.d;
            LocalDateTime localDateTime2 = localDateTimeArr3[binarySearch];
            LocalDateTime localDateTime3 = localDateTimeArr3[binarySearch + 1];
            ZoneOffset[] zoneOffsetArr = this.e;
            int i4 = binarySearch / 2;
            ZoneOffset zoneOffset = zoneOffsetArr[i4];
            ZoneOffset zoneOffset2 = zoneOffsetArr[i4 + 1];
            return zoneOffset2.o() > zoneOffset.o() ? new a(localDateTime2, zoneOffset, zoneOffset2) : new a(localDateTime3, zoneOffset, zoneOffset2);
        }
    }

    public static c j(ZoneOffset zoneOffset) {
        if (zoneOffset != null) {
            return new c(zoneOffset);
        }
        throw new NullPointerException("offset");
    }

    private static ZoneOffset k(int i2) {
        return ZoneOffset.r(i2 / MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH);
    }

    public final ZoneOffset d(Instant instant) {
        TimeZone timeZone = this.g;
        if (timeZone != null) {
            return k(timeZone.getOffset(instant.toEpochMilli()));
        }
        if (this.c.length == 0) {
            return this.b[0];
        }
        long epochSecond = instant.getEpochSecond();
        if (this.f.length > 0) {
            long[] jArr = this.c;
            if (epochSecond > jArr[jArr.length - 1]) {
                ZoneOffset[] zoneOffsetArr = this.e;
                a[] b = b(c(epochSecond, zoneOffsetArr[zoneOffsetArr.length - 1]));
                a aVar = null;
                for (int i2 = 0; i2 < b.length; i2++) {
                    aVar = b[i2];
                    if (epochSecond < aVar.j()) {
                        return aVar.f();
                    }
                }
                return aVar.e();
            }
        }
        int binarySearch = Arrays.binarySearch(this.c, epochSecond);
        if (binarySearch < 0) {
            binarySearch = (-binarySearch) - 2;
        }
        return this.e[binarySearch + 1];
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof c)) {
            return false;
        }
        c cVar = (c) obj;
        return AbstractC0037n.p(this.g, cVar.g) && Arrays.equals(this.a, cVar.a) && Arrays.equals(this.b, cVar.b) && Arrays.equals(this.c, cVar.c) && Arrays.equals(this.e, cVar.e) && Arrays.equals(this.f, cVar.f);
    }

    public final a f(LocalDateTime localDateTime) {
        Object e = e(localDateTime);
        if (e instanceof a) {
            return (a) e;
        }
        return null;
    }

    public final List g(LocalDateTime localDateTime) {
        Object e = e(localDateTime);
        return e instanceof a ? ((a) e).g() : Collections.singletonList((ZoneOffset) e);
    }

    public final boolean h(Instant instant) {
        ZoneOffset zoneOffset;
        TimeZone timeZone = this.g;
        if (timeZone != null) {
            zoneOffset = k(timeZone.getRawOffset());
        } else if (this.c.length == 0) {
            zoneOffset = this.b[0];
        } else {
            int binarySearch = Arrays.binarySearch(this.a, instant.getEpochSecond());
            if (binarySearch < 0) {
                binarySearch = (-binarySearch) - 2;
            }
            zoneOffset = this.b[binarySearch + 1];
        }
        return !zoneOffset.equals(d(instant));
    }

    @Override // java.lang.Object
    public final int hashCode() {
        TimeZone timeZone = this.g;
        return (((((timeZone != null ? timeZone.hashCode() : 0) ^ Arrays.hashCode(this.a)) ^ Arrays.hashCode(this.b)) ^ Arrays.hashCode(this.c)) ^ Arrays.hashCode(this.e)) ^ Arrays.hashCode(this.f);
    }

    public final boolean i() {
        TimeZone timeZone = this.g;
        if (timeZone == null) {
            return this.c.length == 0;
        }
        if (!timeZone.useDaylightTime() && this.g.getDSTSavings() == 0) {
            Instant now = Instant.now();
            a aVar = null;
            if (this.g != null) {
                long epochSecond = now.getEpochSecond();
                if (now.n() > 0 && epochSecond < Long.MAX_VALUE) {
                    epochSecond++;
                }
                int c = c(epochSecond, d(now));
                a[] b = b(c);
                int length = b.length - 1;
                while (true) {
                    if (length >= 0) {
                        if (epochSecond > b[length].j()) {
                            aVar = b[length];
                            break;
                        }
                        length--;
                    } else if (c > 1800) {
                        a[] b2 = b(c - 1);
                        int length2 = b2.length - 1;
                        while (true) {
                            if (length2 < 0) {
                                long min = Math.min(epochSecond - 31104000, (d.b().a() / 1000) + 31968000);
                                int offset = this.g.getOffset((epochSecond - 1) * 1000);
                                long E = h.u(1800, 1, 1).E() * 86400;
                                while (true) {
                                    if (E > min) {
                                        break;
                                    }
                                    int offset2 = this.g.getOffset(min * 1000);
                                    if (offset != offset2) {
                                        int c2 = c(min, k(offset2));
                                        a[] b3 = b(c2 + 1);
                                        int length3 = b3.length - 1;
                                        while (true) {
                                            if (length3 < 0) {
                                                a[] b4 = b(c2);
                                                aVar = b4[b4.length - 1];
                                                break;
                                            } else if (epochSecond > b3[length3].j()) {
                                                aVar = b3[length3];
                                                break;
                                            } else {
                                                length3--;
                                            }
                                        }
                                    } else {
                                        min -= 7776000;
                                    }
                                }
                            } else if (epochSecond > b2[length2].j()) {
                                aVar = b2[length2];
                                break;
                            } else {
                                length2--;
                            }
                        }
                    }
                }
            } else if (this.c.length != 0) {
                long epochSecond2 = now.getEpochSecond();
                if (now.n() > 0 && epochSecond2 < Long.MAX_VALUE) {
                    epochSecond2++;
                }
                long[] jArr = this.c;
                long j2 = jArr[jArr.length - 1];
                if (this.f.length > 0 && epochSecond2 > j2) {
                    ZoneOffset[] zoneOffsetArr = this.e;
                    ZoneOffset zoneOffset = zoneOffsetArr[zoneOffsetArr.length - 1];
                    int c3 = c(epochSecond2, zoneOffset);
                    a[] b5 = b(c3);
                    int length4 = b5.length - 1;
                    while (true) {
                        if (length4 < 0) {
                            int i2 = c3 - 1;
                            if (i2 > c(j2, zoneOffset)) {
                                a[] b6 = b(i2);
                                aVar = b6[b6.length - 1];
                            }
                        } else if (epochSecond2 > b5[length4].j()) {
                            aVar = b5[length4];
                            break;
                        } else {
                            length4--;
                        }
                    }
                }
                int binarySearch = Arrays.binarySearch(this.c, epochSecond2);
                if (binarySearch < 0) {
                    binarySearch = (-binarySearch) - 1;
                }
                if (binarySearch > 0) {
                    int i3 = binarySearch - 1;
                    long j3 = this.c[i3];
                    ZoneOffset[] zoneOffsetArr2 = this.e;
                    aVar = new a(j3, zoneOffsetArr2[i3], zoneOffsetArr2[binarySearch]);
                }
            }
            if (aVar == null) {
                return true;
            }
        }
        return false;
    }

    @Override // java.lang.Object
    public final String toString() {
        StringBuilder sb;
        if (this.g != null) {
            sb = b.a("ZoneRules[timeZone=");
            sb.append(this.g.getID());
        } else {
            sb = b.a("ZoneRules[currentStandardOffset=");
            ZoneOffset[] zoneOffsetArr = this.b;
            sb.append(zoneOffsetArr[zoneOffsetArr.length - 1]);
        }
        sb.append("]");
        return sb.toString();
    }
}
