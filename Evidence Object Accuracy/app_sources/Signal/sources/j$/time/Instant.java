package j$.time;

import j$.time.format.DateTimeFormatter;
import j$.time.temporal.ChronoField;
import j$.time.temporal.TemporalAccessor;
import j$.time.temporal.TemporalAmount;
import j$.time.temporal.TemporalField;
import j$.time.temporal.a;
import j$.time.temporal.j;
import j$.time.temporal.k;
import j$.time.temporal.l;
import j$.time.temporal.n;
import j$.time.temporal.o;
import j$.time.temporal.p;
import j$.time.temporal.q;
import java.io.Serializable;
import org.thoughtcrime.securesms.util.MessageRecordUtil;

/* loaded from: classes2.dex */
public final class Instant implements j, k, Comparable<Instant>, Serializable {
    public static final Instant c = new Instant(0, 0);
    private final long a;
    private final int b;

    static {
        c = new Instant(0, 0);
        o(-31557014167219200L, 0);
        o(31556889864403199L, 999999999);
    }

    private Instant(long j, int i) {
        this.a = j;
        this.b = i;
    }

    public static Instant from(TemporalAccessor temporalAccessor) {
        if (temporalAccessor instanceof Instant) {
            return (Instant) temporalAccessor;
        }
        if (temporalAccessor != null) {
            try {
                return o(temporalAccessor.getLong(ChronoField.INSTANT_SECONDS), (long) temporalAccessor.c(ChronoField.NANO_OF_SECOND));
            } catch (e e) {
                throw new e("Unable to obtain Instant from TemporalAccessor: " + temporalAccessor + " of type " + temporalAccessor.getClass().getName(), e);
            }
        } else {
            throw new NullPointerException("temporal");
        }
    }

    private static Instant m(long j, int i) {
        if ((((long) i) | j) == 0) {
            return c;
        }
        if (j >= -31557014167219200L && j <= 31556889864403199L) {
            return new Instant(j, i);
        }
        throw new e("Instant exceeds minimum or maximum instant");
    }

    public static Instant now() {
        d.b();
        return ofEpochMilli(System.currentTimeMillis());
    }

    public static Instant o(long j, long j2) {
        return m(a.b(j, a.e(j2, 1000000000)), (int) a.c(j2, 1000000000));
    }

    public static Instant ofEpochMilli(long j) {
        return m(a.e(j, 1000), ((int) a.c(j, 1000)) * 1000000);
    }

    public static Instant ofEpochSecond(long j) {
        return m(j, 0);
    }

    private Instant p(long j, long j2) {
        if ((j | j2) == 0) {
            return this;
        }
        return o(a.b(a.b(this.a, j), j2 / 1000000000), ((long) this.b) + (j2 % 1000000000));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004a, code lost:
        if (r4 != r2.b) goto L_0x0054;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0052, code lost:
        if (r4 != r2.b) goto L_0x0054;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0054, code lost:
        r0 = r2.a;
     */
    @Override // j$.time.temporal.j
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final j$.time.temporal.j a(long r3, j$.time.temporal.TemporalField r5) {
        /*
            r2 = this;
            boolean r0 = r5 instanceof j$.time.temporal.ChronoField
            if (r0 == 0) goto L_0x0068
            r0 = r5
            j$.time.temporal.ChronoField r0 = (j$.time.temporal.ChronoField) r0
            r0.l(r3)
            int[] r1 = j$.time.f.a
            int r0 = r0.ordinal()
            r0 = r1[r0]
            r1 = 1
            if (r0 == r1) goto L_0x0057
            r1 = 2
            if (r0 == r1) goto L_0x004d
            r1 = 3
            if (r0 == r1) goto L_0x0042
            r1 = 4
            if (r0 != r1) goto L_0x002b
            long r0 = r2.a
            int r5 = (r3 > r0 ? 1 : (r3 == r0 ? 0 : -1))
            if (r5 == 0) goto L_0x0066
            int r5 = r2.b
            j$.time.Instant r3 = m(r3, r5)
            goto L_0x006e
        L_0x002b:
            j$.time.temporal.p r3 = new j$.time.temporal.p
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r0 = "Unsupported field: "
            r4.append(r0)
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r3.<init>(r4)
            throw r3
        L_0x0042:
            int r4 = (int) r3
            r3 = 1000000(0xf4240, float:1.401298E-39)
            int r4 = r4 * r3
            int r3 = r2.b
            if (r4 == r3) goto L_0x0066
            goto L_0x0054
        L_0x004d:
            int r4 = (int) r3
            int r4 = r4 * 1000
            int r3 = r2.b
            if (r4 == r3) goto L_0x0066
        L_0x0054:
            long r0 = r2.a
            goto L_0x0061
        L_0x0057:
            int r5 = r2.b
            long r0 = (long) r5
            int r5 = (r3 > r0 ? 1 : (r3 == r0 ? 0 : -1))
            if (r5 == 0) goto L_0x0066
            long r0 = r2.a
            int r4 = (int) r3
        L_0x0061:
            j$.time.Instant r3 = m(r0, r4)
            goto L_0x006e
        L_0x0066:
            r3 = r2
            goto L_0x006e
        L_0x0068:
            j$.time.temporal.j r3 = r5.j(r2, r3)
            j$.time.Instant r3 = (j$.time.Instant) r3
        L_0x006e:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: j$.time.Instant.a(long, j$.time.temporal.TemporalField):j$.time.temporal.j");
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final int c(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return l.c(this, temporalField).a(temporalField.h(this), temporalField);
        }
        int i = f.a[((ChronoField) temporalField).ordinal()];
        if (i == 1) {
            return this.b;
        }
        if (i == 2) {
            return this.b / MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH;
        }
        if (i == 3) {
            return this.b / 1000000;
        }
        if (i == 4) {
            ChronoField.INSTANT_SECONDS.k(this.a);
        }
        throw new p("Unsupported field: " + temporalField);
    }

    @Override // j$.time.temporal.j
    public final j d(h hVar) {
        return (Instant) hVar.g(this);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final q e(TemporalField temporalField) {
        return l.c(this, temporalField);
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Instant)) {
            return false;
        }
        Instant instant = (Instant) obj;
        return this.a == instant.a && this.b == instant.b;
    }

    @Override // j$.time.temporal.k
    public final j g(j jVar) {
        return jVar.a(this.a, ChronoField.INSTANT_SECONDS).a((long) this.b, ChronoField.NANO_OF_SECOND);
    }

    public long getEpochSecond() {
        return this.a;
    }

    @Override // j$.time.temporal.TemporalAccessor
    public long getLong(TemporalField temporalField) {
        int i;
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.h(this);
        }
        int i2 = f.a[((ChronoField) temporalField).ordinal()];
        if (i2 == 1) {
            i = this.b;
        } else if (i2 == 2) {
            i = this.b / MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH;
        } else if (i2 == 3) {
            i = this.b / 1000000;
        } else if (i2 == 4) {
            return this.a;
        } else {
            throw new p("Unsupported field: " + temporalField);
        }
        return (long) i;
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final boolean h(TemporalField temporalField) {
        return temporalField instanceof ChronoField ? temporalField == ChronoField.INSTANT_SECONDS || temporalField == ChronoField.NANO_OF_SECOND || temporalField == ChronoField.MICRO_OF_SECOND || temporalField == ChronoField.MILLI_OF_SECOND : temporalField != null && temporalField.c(this);
    }

    @Override // java.lang.Object
    public final int hashCode() {
        long j = this.a;
        return (this.b * 51) + ((int) (j ^ (j >>> 32)));
    }

    public boolean isBefore(Instant instant) {
        return compareTo(instant) < 0;
    }

    @Override // j$.time.temporal.j
    public final j j(long j, a aVar) {
        return j == Long.MIN_VALUE ? f(Long.MAX_VALUE, aVar).f(1, aVar) : f(-j, aVar);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final Object k(n nVar) {
        if (nVar == l.i()) {
            return a.NANOS;
        }
        if (nVar == l.d() || nVar == l.k() || nVar == l.j() || nVar == l.h() || nVar == l.e() || nVar == l.f()) {
            return null;
        }
        return nVar.a(this);
    }

    /* renamed from: l */
    public final int compareTo(Instant instant) {
        int compare = Long.compare(this.a, instant.a);
        return compare != 0 ? compare : this.b - instant.b;
    }

    public Instant minus(TemporalAmount temporalAmount) {
        return (Instant) temporalAmount.c(this);
    }

    public final int n() {
        return this.b;
    }

    public Instant plus(TemporalAmount temporalAmount) {
        return (Instant) temporalAmount.a(this);
    }

    /* renamed from: q */
    public final Instant f(long j, o oVar) {
        if (!(oVar instanceof a)) {
            return (Instant) oVar.c(this, j);
        }
        switch (f.b[((a) oVar).ordinal()]) {
            case 1:
                return p(0, j);
            case 2:
                return p(j / 1000000, (j % 1000000) * 1000);
            case 3:
                return p(j / 1000, (j % 1000) * 1000000);
            case 4:
                return p(j, 0);
            case 5:
                return p(a.d(j, 60), 0);
            case 6:
                return p(a.d(j, 3600), 0);
            case 7:
                return p(a.d(j, 43200), 0);
            case 8:
                return p(a.d(j, 86400), 0);
            default:
                throw new p("Unsupported unit: " + oVar);
        }
    }

    public long toEpochMilli() {
        long d;
        int i;
        long j = this.a;
        if (j >= 0 || this.b <= 0) {
            d = a.d(j, 1000);
            i = this.b / 1000000;
        } else {
            d = a.d(j + 1, 1000);
            i = (this.b / 1000000) - 1000;
        }
        return a.b(d, (long) i);
    }

    @Override // java.lang.Object
    public final String toString() {
        return DateTimeFormatter.ISO_INSTANT.format(this);
    }
}
