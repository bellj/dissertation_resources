package j$.time;

import j$.time.chrono.ChronoLocalDateTime;
import j$.time.chrono.e;
import j$.time.chrono.g;
import j$.time.temporal.ChronoField;
import j$.time.temporal.TemporalField;
import j$.time.temporal.j;
import j$.time.temporal.l;
import j$.time.temporal.n;
import j$.time.temporal.o;
import j$.time.temporal.p;
import j$.time.temporal.q;
import j$.time.zone.a;
import j$.time.zone.c;
import java.io.Serializable;
import java.util.List;

/* loaded from: classes2.dex */
public final class ZonedDateTime implements j, e, Serializable {
    private final LocalDateTime a;
    private final ZoneOffset b;
    private final ZoneId c;

    private ZonedDateTime(LocalDateTime localDateTime, ZoneId zoneId, ZoneOffset zoneOffset) {
        this.a = localDateTime;
        this.b = zoneOffset;
        this.c = zoneId;
    }

    private static ZonedDateTime g(long j, int i, ZoneId zoneId) {
        ZoneOffset d = zoneId.m().d(Instant.o(j, (long) i));
        return new ZonedDateTime(LocalDateTime.r(j, i, d), zoneId, d);
    }

    public static ZonedDateTime n(Instant instant, ZoneId zoneId) {
        if (instant == null) {
            throw new NullPointerException("instant");
        } else if (zoneId != null) {
            return g(instant.getEpochSecond(), instant.n(), zoneId);
        } else {
            throw new NullPointerException("zone");
        }
    }

    public static ZonedDateTime o(LocalDateTime localDateTime, ZoneId zoneId, ZoneOffset zoneOffset) {
        if (localDateTime == null) {
            throw new NullPointerException("localDateTime");
        } else if (zoneId == null) {
            throw new NullPointerException("zone");
        } else if (zoneId instanceof ZoneOffset) {
            return new ZonedDateTime(localDateTime, zoneId, (ZoneOffset) zoneId);
        } else {
            c m = zoneId.m();
            List g = m.g(localDateTime);
            if (g.size() == 1) {
                zoneOffset = (ZoneOffset) g.get(0);
            } else if (g.size() == 0) {
                a f = m.f(localDateTime);
                localDateTime = localDateTime.t(f.d().e());
                zoneOffset = f.e();
            } else if ((zoneOffset == null || !g.contains(zoneOffset)) && (zoneOffset = (ZoneOffset) g.get(0)) == null) {
                throw new NullPointerException("offset");
            }
            return new ZonedDateTime(localDateTime, zoneId, zoneOffset);
        }
    }

    public static ZonedDateTime of(LocalDateTime localDateTime, ZoneId zoneId) {
        return o(localDateTime, zoneId, null);
    }

    private ZonedDateTime q(ZoneOffset zoneOffset) {
        return (zoneOffset.equals(this.b) || !this.c.m().g(this.a).contains(zoneOffset)) ? this : new ZonedDateTime(this.a, this.c, zoneOffset);
    }

    @Override // j$.time.temporal.j
    public final j a(long j, TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return (ZonedDateTime) temporalField.j(this, j);
        }
        ChronoField chronoField = (ChronoField) temporalField;
        int i = p.a[chronoField.ordinal()];
        return i != 1 ? i != 2 ? o(this.a.a(j, temporalField), this.c, this.b) : q(ZoneOffset.r(chronoField.k(j))) : g(j, this.a.m(), this.c);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final int c(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return j$.time.chrono.c.a(this, temporalField);
        }
        int i = p.a[((ChronoField) temporalField).ordinal()];
        if (i != 1) {
            return i != 2 ? this.a.c(temporalField) : this.b.o();
        }
        throw new p("Invalid field 'InstantSeconds' for get() method, use getLong() instead");
    }

    @Override // java.lang.Comparable
    public final int compareTo(Object obj) {
        ZonedDateTime zonedDateTime = (ZonedDateTime) ((e) obj);
        int compare = Long.compare(r(), zonedDateTime.r());
        if (compare != 0) {
            return compare;
        }
        int o = toLocalTime().o() - zonedDateTime.toLocalTime().o();
        if (o != 0) {
            return o;
        }
        int compareTo = this.a.compareTo((ChronoLocalDateTime) zonedDateTime.a);
        if (compareTo != 0) {
            return compareTo;
        }
        int compareTo2 = this.c.l().compareTo(zonedDateTime.c.l());
        if (compareTo2 != 0) {
            return compareTo2;
        }
        s().getClass();
        g gVar = g.a;
        zonedDateTime.s().getClass();
        gVar.getClass();
        gVar.getClass();
        return 0;
    }

    @Override // j$.time.temporal.j
    public final j d(h hVar) {
        return o(LocalDateTime.q(hVar, this.a.toLocalTime()), this.c, this.b);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final q e(TemporalField temporalField) {
        return temporalField instanceof ChronoField ? (temporalField == ChronoField.INSTANT_SECONDS || temporalField == ChronoField.OFFSET_SECONDS) ? temporalField.e() : this.a.e(temporalField) : temporalField.d(this);
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ZonedDateTime)) {
            return false;
        }
        ZonedDateTime zonedDateTime = (ZonedDateTime) obj;
        return this.a.equals(zonedDateTime.a) && this.b.equals(zonedDateTime.b) && this.c.equals(zonedDateTime.c);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final long getLong(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.h(this);
        }
        int i = p.a[((ChronoField) temporalField).ordinal()];
        return i != 1 ? i != 2 ? this.a.getLong(temporalField) : (long) this.b.o() : r();
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final boolean h(TemporalField temporalField) {
        return (temporalField instanceof ChronoField) || (temporalField != null && temporalField.c(this));
    }

    @Override // java.lang.Object
    public final int hashCode() {
        return (this.a.hashCode() ^ this.b.hashCode()) ^ Integer.rotateLeft(this.c.hashCode(), 3);
    }

    @Override // j$.time.temporal.j
    public final j j(long j, j$.time.temporal.a aVar) {
        return j == Long.MIN_VALUE ? f(Long.MAX_VALUE, aVar).f(1, aVar) : f(-j, aVar);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final Object k(n nVar) {
        if (nVar == l.e()) {
            return s();
        }
        if (nVar == l.j() || nVar == l.k()) {
            return this.c;
        }
        if (nVar == l.h()) {
            return this.b;
        }
        if (nVar == l.f()) {
            return toLocalTime();
        }
        if (nVar != l.d()) {
            return nVar == l.i() ? j$.time.temporal.a.NANOS : nVar.a(this);
        }
        s().getClass();
        return g.a;
    }

    public final ZoneOffset l() {
        return this.b;
    }

    public final ZoneId m() {
        return this.c;
    }

    /* renamed from: p */
    public final ZonedDateTime f(long j, o oVar) {
        if (!(oVar instanceof j$.time.temporal.a)) {
            return (ZonedDateTime) oVar.c(this, j);
        }
        if (oVar.a()) {
            return o(this.a.f(j, oVar), this.c, this.b);
        }
        LocalDateTime s = this.a.f(j, oVar);
        ZoneOffset zoneOffset = this.b;
        ZoneId zoneId = this.c;
        if (s == null) {
            throw new NullPointerException("localDateTime");
        } else if (zoneOffset == null) {
            throw new NullPointerException("offset");
        } else if (zoneId != null) {
            return zoneId.m().g(s).contains(zoneOffset) ? new ZonedDateTime(s, zoneId, zoneOffset) : g(s.toEpochSecond(zoneOffset), s.m(), zoneId);
        } else {
            throw new NullPointerException("zone");
        }
    }

    public final long r() {
        return ((s().E() * 86400) + ((long) toLocalTime().y())) - ((long) l().o());
    }

    public final h s() {
        return this.a.v();
    }

    public final LocalDateTime t() {
        return this.a;
    }

    public final LocalTime toLocalTime() {
        return this.a.toLocalTime();
    }

    @Override // java.lang.Object
    public final String toString() {
        String str = this.a.toString() + this.b.toString();
        if (this.b == this.c) {
            return str;
        }
        return str + '[' + this.c.toString() + ']';
    }

    public final LocalDateTime u() {
        return this.a;
    }
}
