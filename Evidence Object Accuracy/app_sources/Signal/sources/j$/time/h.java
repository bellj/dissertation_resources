package j$.time;

import j$.time.chrono.b;
import j$.time.chrono.g;
import j$.time.temporal.ChronoField;
import j$.time.temporal.TemporalAccessor;
import j$.time.temporal.TemporalAmount;
import j$.time.temporal.TemporalField;
import j$.time.temporal.a;
import j$.time.temporal.j;
import j$.time.temporal.k;
import j$.time.temporal.l;
import j$.time.temporal.n;
import j$.time.temporal.o;
import j$.time.temporal.p;
import j$.time.temporal.q;
import java.io.Serializable;

/* loaded from: classes2.dex */
public final class h implements j, k, b, Serializable {
    public static final h d = u(-999999999, 1, 1);
    public static final h e = u(999999999, 12, 31);
    private final int a;
    private final short b;
    private final short c;

    private h(int i, int i2, int i3) {
        this.a = i;
        this.b = (short) i2;
        this.c = (short) i3;
    }

    private static h D(int i, int i2, int i3) {
        int i4;
        if (i2 != 2) {
            if (i2 == 4 || i2 == 6 || i2 == 9 || i2 == 11) {
                i4 = 30;
            }
            return new h(i, i2, i3);
        }
        g.a.getClass();
        i4 = g.d((long) i) ? 29 : 28;
        i3 = Math.min(i3, i4);
        return new h(i, i2, i3);
    }

    public static h n(TemporalAccessor temporalAccessor) {
        if (temporalAccessor != null) {
            h hVar = (h) temporalAccessor.k(l.e());
            if (hVar != null) {
                return hVar;
            }
            throw new e("Unable to obtain LocalDate from TemporalAccessor: " + temporalAccessor + " of type " + temporalAccessor.getClass().getName());
        }
        throw new NullPointerException("temporal");
    }

    private int o(TemporalField temporalField) {
        switch (g.a[((ChronoField) temporalField).ordinal()]) {
            case 1:
                return this.c;
            case 2:
                return q();
            case 3:
                return ((this.c - 1) / 7) + 1;
            case 4:
                int i = this.a;
                return i >= 1 ? i : 1 - i;
            case 5:
                return p().l();
            case 6:
                return ((this.c - 1) % 7) + 1;
            case 7:
                return ((q() - 1) % 7) + 1;
            case 8:
                throw new p("Invalid field 'EpochDay' for get() method, use getLong() instead");
            case 9:
                return ((q() - 1) / 7) + 1;
            case 10:
                return this.b;
            case 11:
                throw new p("Invalid field 'ProlepticMonth' for get() method, use getLong() instead");
            case 12:
                return this.a;
            case 13:
                return this.a >= 1 ? 1 : 0;
            default:
                throw new p("Unsupported field: " + temporalField);
        }
    }

    public static h u(int i, int i2, int i3) {
        long j = (long) i;
        ChronoField.YEAR.l(j);
        ChronoField.MONTH_OF_YEAR.l((long) i2);
        ChronoField.DAY_OF_MONTH.l((long) i3);
        int i4 = 28;
        if (i3 > 28) {
            i4 = 31;
            if (i2 == 2) {
                g.a.getClass();
                if (g.d(j)) {
                    i4 = 29;
                }
            } else if (i2 == 4 || i2 == 6 || i2 == 9 || i2 == 11) {
                i4 = 30;
            }
            if (i3 > i4) {
                if (i3 == 29) {
                    throw new e("Invalid date 'February 29' as '" + i + "' is not a leap year");
                }
                StringBuilder a = b.a("Invalid date '");
                a.append(l.n(i2).name());
                a.append(" ");
                a.append(i3);
                a.append("'");
                throw new e(a.toString());
            }
        }
        return new h(i, i2, i3);
    }

    public static h v(long j) {
        long j2;
        long j3 = (j + 719528) - 60;
        if (j3 < 0) {
            long j4 = ((j3 + 1) / 146097) - 1;
            j2 = j4 * 400;
            j3 += (-j4) * 146097;
        } else {
            j2 = 0;
        }
        long j5 = ((j3 * 400) + 591) / 146097;
        long j6 = j3 - ((j5 / 400) + (((j5 / 4) + (j5 * 365)) - (j5 / 100)));
        if (j6 < 0) {
            j5--;
            j6 = j3 - ((j5 / 400) + (((j5 / 4) + (365 * j5)) - (j5 / 100)));
        }
        int i = (int) j6;
        int i2 = ((i * 5) + 2) / 153;
        return new h(ChronoField.YEAR.k(j5 + j2 + ((long) (i2 / 10))), ((i2 + 2) % 12) + 1, (i - (((i2 * 306) + 5) / 10)) + 1);
    }

    public static h w(int i, int i2) {
        long j = (long) i;
        ChronoField.YEAR.l(j);
        ChronoField.DAY_OF_YEAR.l((long) i2);
        g.a.getClass();
        boolean d2 = g.d(j);
        if (i2 != 366 || d2) {
            l n = l.n(((i2 - 1) / 31) + 1);
            if (i2 > (n.m(d2) + n.l(d2)) - 1) {
                n = n.o();
            }
            return new h(i, n.ordinal() + 1, (i2 - n.l(d2)) + 1);
        }
        throw new e("Invalid date 'DayOfYear 366' as '" + i + "' is not a leap year");
    }

    public final h A(long j) {
        if (j == 0) {
            return this;
        }
        long j2 = (((long) this.a) * 12) + ((long) (this.b - 1)) + j;
        return D(ChronoField.YEAR.k(a.e(j2, 12)), ((int) a.c(j2, 12)) + 1, this.c);
    }

    public final h B(long j) {
        return z(a.d(j, 7));
    }

    public final h C(long j) {
        return j == 0 ? this : D(ChronoField.YEAR.k(((long) this.a) + j), this.b, this.c);
    }

    public final long E() {
        long j;
        long j2 = (long) this.a;
        long j3 = (long) this.b;
        long j4 = (365 * j2) + 0;
        if (j2 >= 0) {
            j = ((j2 + 399) / 400) + (((3 + j2) / 4) - ((99 + j2) / 100)) + j4;
        } else {
            j = j4 - ((j2 / -400) + ((j2 / -4) - (j2 / -100)));
        }
        long j5 = (((367 * j3) - 362) / 12) + j + ((long) (this.c - 1));
        if (j3 > 2) {
            j5--;
            if (!s()) {
                j5--;
            }
        }
        return j5 - 719528;
    }

    /* renamed from: F */
    public final h a(long j, TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return (h) temporalField.j(this, j);
        }
        ChronoField chronoField = (ChronoField) temporalField;
        chronoField.l(j);
        switch (g.a[chronoField.ordinal()]) {
            case 1:
                int i = (int) j;
                return this.c == i ? this : u(this.a, this.b, i);
            case 2:
                return H((int) j);
            case 3:
                return B(j - getLong(ChronoField.ALIGNED_WEEK_OF_MONTH));
            case 4:
                if (this.a < 1) {
                    j = 1 - j;
                }
                return I((int) j);
            case 5:
                return z(j - ((long) p().l()));
            case 6:
                return z(j - getLong(ChronoField.ALIGNED_DAY_OF_WEEK_IN_MONTH));
            case 7:
                return z(j - getLong(ChronoField.ALIGNED_DAY_OF_WEEK_IN_YEAR));
            case 8:
                return v(j);
            case 9:
                return B(j - getLong(ChronoField.ALIGNED_WEEK_OF_YEAR));
            case 10:
                int i2 = (int) j;
                if (this.b == i2) {
                    return this;
                }
                ChronoField.MONTH_OF_YEAR.l((long) i2);
                return D(this.a, i2, this.c);
            case 11:
                return A(j - (((((long) this.a) * 12) + ((long) this.b)) - 1));
            case 12:
                return I((int) j);
            case 13:
                return getLong(ChronoField.ERA) == j ? this : I(1 - this.a);
            default:
                throw new p("Unsupported field: " + temporalField);
        }
    }

    /* renamed from: G */
    public final h d(k kVar) {
        return kVar instanceof h ? (h) kVar : (h) kVar.g(this);
    }

    public final h H(int i) {
        return q() == i ? this : w(this.a, i);
    }

    public final h I(int i) {
        if (this.a == i) {
            return this;
        }
        ChronoField.YEAR.l((long) i);
        return D(i, this.b, this.c);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final int c(TemporalField temporalField) {
        return temporalField instanceof ChronoField ? o(temporalField) : l.a(this, temporalField);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final q e(TemporalField temporalField) {
        int i;
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.d(this);
        }
        ChronoField chronoField = (ChronoField) temporalField;
        if (chronoField.a()) {
            int i2 = g.a[chronoField.ordinal()];
            if (i2 == 1) {
                short s = this.b;
                i = s != 2 ? (s == 4 || s == 6 || s == 9 || s == 11) ? 30 : 31 : s() ? 29 : 28;
            } else if (i2 == 2) {
                i = s() ? 366 : 365;
            } else if (i2 == 3) {
                return q.i(1, (l.n(this.b) != l.FEBRUARY || s()) ? 5 : 4);
            } else if (i2 != 4) {
                return temporalField.e();
            } else {
                return q.i(1, this.a <= 0 ? 1000000000 : 999999999);
            }
            return q.i(1, (long) i);
        }
        throw new p("Unsupported field: " + temporalField);
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof h) && m((h) obj) == 0;
    }

    @Override // j$.time.temporal.k
    public final j g(j jVar) {
        return jVar.a(E(), ChronoField.EPOCH_DAY);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final long getLong(TemporalField temporalField) {
        return temporalField instanceof ChronoField ? temporalField == ChronoField.EPOCH_DAY ? E() : temporalField == ChronoField.PROLEPTIC_MONTH ? ((((long) this.a) * 12) + ((long) this.b)) - 1 : (long) o(temporalField) : temporalField.h(this);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final boolean h(TemporalField temporalField) {
        return temporalField instanceof ChronoField ? temporalField.a() : temporalField != null && temporalField.c(this);
    }

    @Override // java.lang.Object
    public final int hashCode() {
        int i = this.a;
        return (((i << 11) + (this.b << 6)) + this.c) ^ (i & -2048);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final Object k(n nVar) {
        if (nVar == l.e()) {
            return this;
        }
        if (nVar == l.k() || nVar == l.j() || nVar == l.h() || nVar == l.f()) {
            return null;
        }
        return nVar == l.d() ? g.a : nVar == l.i() ? a.DAYS : nVar.a(this);
    }

    /* renamed from: l */
    public final int compareTo(b bVar) {
        if (bVar instanceof h) {
            return m((h) bVar);
        }
        int compare = Long.compare(E(), ((h) bVar).E());
        if (compare != 0) {
            return compare;
        }
        g.a.getClass();
        return 0;
    }

    public final int m(h hVar) {
        int i = this.a - hVar.a;
        if (i != 0) {
            return i;
        }
        int i2 = this.b - hVar.b;
        return i2 == 0 ? this.c - hVar.c : i2;
    }

    public final DayOfWeek p() {
        return DayOfWeek.m(((int) a.c(E() + 3, 7)) + 1);
    }

    public final int q() {
        return (l.n(this.b).l(s()) + this.c) - 1;
    }

    public final int r() {
        return this.a;
    }

    public final boolean s() {
        g.a.getClass();
        return g.d((long) this.a);
    }

    /* renamed from: t */
    public final h j(long j, a aVar) {
        return j == Long.MIN_VALUE ? f(Long.MAX_VALUE, aVar).f(1, aVar) : f(-j, aVar);
    }

    @Override // java.lang.Object
    public final String toString() {
        int i;
        int i2 = this.a;
        short s = this.b;
        short s2 = this.c;
        int abs = Math.abs(i2);
        StringBuilder sb = new StringBuilder(10);
        if (abs < 1000) {
            if (i2 < 0) {
                sb.append(i2 - 10000);
                i = 1;
            } else {
                sb.append(i2 + 10000);
                i = 0;
            }
            sb.deleteCharAt(i);
        } else {
            if (i2 > 9999) {
                sb.append('+');
            }
            sb.append(i2);
        }
        String str = "-0";
        sb.append(s < 10 ? str : "-");
        sb.append((int) s);
        if (s2 >= 10) {
            str = "-";
        }
        sb.append(str);
        sb.append((int) s2);
        return sb.toString();
    }

    /* renamed from: x */
    public final h f(long j, o oVar) {
        if (!(oVar instanceof a)) {
            return (h) oVar.c(this, j);
        }
        switch (g.b[((a) oVar).ordinal()]) {
            case 1:
                return z(j);
            case 2:
                return B(j);
            case 3:
                return A(j);
            case 4:
                return C(j);
            case 5:
                return C(a.d(j, 10));
            case 6:
                return C(a.d(j, 100));
            case 7:
                return C(a.d(j, 1000));
            case 8:
                ChronoField chronoField = ChronoField.ERA;
                return a(a.b(getLong(chronoField), j), chronoField);
            default:
                throw new p("Unsupported unit: " + oVar);
        }
    }

    public final h y(TemporalAmount temporalAmount) {
        if (temporalAmount instanceof Period) {
            Period period = (Period) temporalAmount;
            return A(period.d()).z((long) period.b());
        } else if (temporalAmount != null) {
            return (h) temporalAmount.a(this);
        } else {
            throw new NullPointerException("amountToAdd");
        }
    }

    public final h z(long j) {
        return j == 0 ? this : v(a.b(E(), j));
    }
}
