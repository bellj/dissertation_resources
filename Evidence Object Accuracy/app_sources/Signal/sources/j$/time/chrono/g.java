package j$.time.chrono;

import java.io.Serializable;

/* loaded from: classes2.dex */
public final class g extends a implements Serializable {
    public static final g a = new g();

    private g() {
    }

    public static boolean d(long j) {
        return (3 & j) == 0 && (j % 100 != 0 || j % 400 == 0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0089, code lost:
        if (r2 > 0) goto L_0x00a8;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final j$.time.h e(java.util.HashMap r20, j$.time.format.G r21) {
        /*
        // Method dump skipped, instructions count: 1204
        */
        throw new UnsupportedOperationException("Method not decompiled: j$.time.chrono.g.e(java.util.HashMap, j$.time.format.G):j$.time.h");
    }
}
