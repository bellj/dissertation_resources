package j$.time.chrono;

import j$.time.LocalTime;
import j$.time.ZoneOffset;
import j$.time.chrono.b;
import j$.time.temporal.j;
import j$.time.temporal.k;

/* loaded from: classes2.dex */
public interface ChronoLocalDateTime<D extends b> extends j, k, Comparable<ChronoLocalDateTime<?>> {
    f b();

    int compareTo(ChronoLocalDateTime chronoLocalDateTime);

    b i();

    long toEpochSecond(ZoneOffset zoneOffset);

    LocalTime toLocalTime();
}
