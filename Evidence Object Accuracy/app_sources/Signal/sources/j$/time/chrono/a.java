package j$.time.chrono;

import j$.time.DayOfWeek;
import j$.time.e;
import j$.time.h;
import j$.time.temporal.ChronoField;
import j$.time.temporal.m;
import j$.util.concurrent.ConcurrentHashMap;
import java.util.HashMap;
import java.util.Locale;

/* loaded from: classes2.dex */
public abstract class a implements f {
    static {
        new ConcurrentHashMap();
        new ConcurrentHashMap();
        new Locale("ja", "JP", "JP");
    }

    public static void a(HashMap hashMap, ChronoField chronoField, long j) {
        Long l = (Long) hashMap.get(chronoField);
        if (l == null || l.longValue() == j) {
            hashMap.put(chronoField, Long.valueOf(j));
            return;
        }
        throw new e("Conflict found: " + chronoField + " " + l + " differs from " + chronoField + " " + j);
    }

    public static h c(h hVar, long j, long j2, long j3) {
        long j4;
        h x = hVar.f(j, j$.time.temporal.a.MONTHS);
        j$.time.temporal.a aVar = j$.time.temporal.a.WEEKS;
        h x2 = x.f(j2, aVar);
        if (j3 > 7) {
            j4 = j3 - 1;
            x2 = x2.f(j4 / 7, aVar);
        } else {
            if (j3 < 1) {
                x2 = x2.f(j$.time.a.f(j3, 7) / 7, aVar);
                j4 = j3 + 6;
            }
            return x2.d(new m(DayOfWeek.m((int) j3).l()));
        }
        j3 = (j4 % 7) + 1;
        return x2.d(new m(DayOfWeek.m((int) j3).l()));
    }

    @Override // java.lang.Comparable
    public final int compareTo(Object obj) {
        ((f) obj).getClass();
        return 0;
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof a)) {
            return false;
        }
        ((a) obj).getClass();
        return true;
    }

    @Override // java.lang.Object
    public final int hashCode() {
        return getClass().hashCode() ^ 72805;
    }

    @Override // java.lang.Object
    public final String toString() {
        return "ISO";
    }
}
