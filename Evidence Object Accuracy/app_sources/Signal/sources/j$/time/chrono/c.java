package j$.time.chrono;

import j$.time.ZonedDateTime;
import j$.time.temporal.ChronoField;
import j$.time.temporal.TemporalAccessor;
import j$.time.temporal.TemporalField;
import j$.time.temporal.l;
import j$.time.temporal.p;

/* loaded from: classes2.dex */
public abstract /* synthetic */ class c {
    public static int a(e eVar, TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return l.a(eVar, temporalField);
        }
        int i = d.a[((ChronoField) temporalField).ordinal()];
        if (i != 1) {
            ZonedDateTime zonedDateTime = (ZonedDateTime) eVar;
            return i != 2 ? zonedDateTime.u().c(temporalField) : zonedDateTime.l().o();
        }
        throw new p("Invalid field 'InstantSeconds' for get() method, use getLong() instead");
    }

    public static f b(TemporalAccessor temporalAccessor) {
        if (temporalAccessor != null) {
            f fVar = (f) temporalAccessor.k(l.d());
            return fVar != null ? fVar : g.a;
        }
        throw new NullPointerException("temporal");
    }
}
