package j$.time.temporal;

import j$.time.a;
import j$.time.chrono.g;
import j$.time.format.G;
import j$.time.h;
import java.util.HashMap;

/* loaded from: classes2.dex */
final class c extends g {
    public c() {
        super("DAY_OF_QUARTER", 0);
    }

    @Override // j$.time.temporal.TemporalField
    public final boolean c(TemporalAccessor temporalAccessor) {
        return temporalAccessor.h(ChronoField.DAY_OF_YEAR) && temporalAccessor.h(ChronoField.MONTH_OF_YEAR) && temporalAccessor.h(ChronoField.YEAR) && g.k(temporalAccessor);
    }

    @Override // j$.time.temporal.g, j$.time.temporal.TemporalField
    public final q d(TemporalAccessor temporalAccessor) {
        if (c(temporalAccessor)) {
            long j = temporalAccessor.getLong(g.QUARTER_OF_YEAR);
            if (j != 1) {
                return j == 2 ? q.i(1, 91) : (j == 3 || j == 4) ? q.i(1, 92) : e();
            }
            long j2 = temporalAccessor.getLong(ChronoField.YEAR);
            g.a.getClass();
            return g.d(j2) ? q.i(1, 91) : q.i(1, 90);
        }
        throw new p("Unsupported field: DayOfQuarter");
    }

    @Override // j$.time.temporal.TemporalField
    public final q e() {
        return q.k(90, 92);
    }

    @Override // j$.time.temporal.g, j$.time.temporal.TemporalField
    public final TemporalAccessor g(HashMap hashMap, TemporalAccessor temporalAccessor, G g) {
        h hVar;
        long j;
        ChronoField chronoField = ChronoField.YEAR;
        Long l = (Long) hashMap.get(chronoField);
        TemporalField temporalField = g.QUARTER_OF_YEAR;
        Long l2 = (Long) hashMap.get(temporalField);
        if (l == null || l2 == null) {
            return null;
        }
        int k = chronoField.k(l.longValue());
        long longValue = ((Long) hashMap.get(g.DAY_OF_QUARTER)).longValue();
        g.m(temporalAccessor);
        if (g == G.LENIENT) {
            hVar = h.u(k, 1, 1).A(a.d(a.f(l2.longValue(), 1), 3));
            j = a.f(longValue, 1);
        } else {
            hVar = h.u(k, ((temporalField.e().a(l2.longValue(), temporalField) - 1) * 3) + 1, 1);
            if (longValue < 1 || longValue > 90) {
                (g == G.STRICT ? d(hVar) : e()).b(longValue, this);
            }
            j = longValue - 1;
        }
        hashMap.remove(this);
        hashMap.remove(chronoField);
        hashMap.remove(temporalField);
        return hVar.z(j);
    }

    @Override // j$.time.temporal.TemporalField
    public final long h(TemporalAccessor temporalAccessor) {
        if (c(temporalAccessor)) {
            int c = temporalAccessor.c(ChronoField.DAY_OF_YEAR);
            int c2 = temporalAccessor.c(ChronoField.MONTH_OF_YEAR);
            long j = temporalAccessor.getLong(ChronoField.YEAR);
            int[] iArr = g.a;
            int i = (c2 - 1) / 3;
            g.a.getClass();
            return (long) (c - iArr[i + (g.d(j) ? 4 : 0)]);
        }
        throw new p("Unsupported field: DayOfQuarter");
    }

    @Override // j$.time.temporal.TemporalField
    public final j j(j jVar, long j) {
        long h = h(jVar);
        e().b(j, this);
        ChronoField chronoField = ChronoField.DAY_OF_YEAR;
        return jVar.a((j - h) + jVar.getLong(chronoField), chronoField);
    }

    @Override // java.lang.Enum, java.lang.Object
    public final String toString() {
        return "DayOfQuarter";
    }
}
