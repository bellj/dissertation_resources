package j$.time.temporal;

import j$.time.a;
import j$.time.format.G;
import j$.time.h;
import java.util.HashMap;

/* loaded from: classes2.dex */
final class e extends g {
    public e() {
        super("WEEK_OF_WEEK_BASED_YEAR", 2);
    }

    @Override // j$.time.temporal.TemporalField
    public final boolean c(TemporalAccessor temporalAccessor) {
        return temporalAccessor.h(ChronoField.EPOCH_DAY) && g.k(temporalAccessor);
    }

    @Override // j$.time.temporal.g, j$.time.temporal.TemporalField
    public final q d(TemporalAccessor temporalAccessor) {
        if (c(temporalAccessor)) {
            return g.n(h.n(temporalAccessor));
        }
        throw new p("Unsupported field: WeekOfWeekBasedYear");
    }

    @Override // j$.time.temporal.TemporalField
    public final q e() {
        return q.k(52, 53);
    }

    @Override // j$.time.temporal.g, j$.time.temporal.TemporalField
    public final TemporalAccessor g(HashMap hashMap, TemporalAccessor temporalAccessor, G g) {
        h hVar;
        long j;
        long j2;
        TemporalField temporalField = g.WEEK_BASED_YEAR;
        Long l = (Long) hashMap.get(temporalField);
        ChronoField chronoField = ChronoField.DAY_OF_WEEK;
        Long l2 = (Long) hashMap.get(chronoField);
        if (l == null || l2 == null) {
            return null;
        }
        int a = temporalField.e().a(l.longValue(), temporalField);
        long longValue = ((Long) hashMap.get(g.WEEK_OF_WEEK_BASED_YEAR)).longValue();
        g.m(temporalAccessor);
        h u = h.u(a, 1, 4);
        if (g == G.LENIENT) {
            long longValue2 = l2.longValue();
            if (longValue2 > 7) {
                j2 = longValue2 - 1;
                u = u.B(j2 / 7);
            } else {
                j = 1;
                if (longValue2 < 1) {
                    u = u.B(a.f(longValue2, 7) / 7);
                    j2 = longValue2 + 6;
                }
                hVar = u.B(a.f(longValue, j)).a(longValue2, chronoField);
            }
            j = 1;
            longValue2 = (j2 % 7) + 1;
            hVar = u.B(a.f(longValue, j)).a(longValue2, chronoField);
        } else {
            int k = chronoField.k(l2.longValue());
            if (longValue < 1 || longValue > 52) {
                (g == G.STRICT ? g.n(u) : e()).b(longValue, this);
            }
            hVar = u.B(longValue - 1).a((long) k, chronoField);
        }
        hashMap.remove(this);
        hashMap.remove(temporalField);
        hashMap.remove(chronoField);
        return hVar;
    }

    @Override // j$.time.temporal.TemporalField
    public final long h(TemporalAccessor temporalAccessor) {
        if (c(temporalAccessor)) {
            return (long) g.o(h.n(temporalAccessor));
        }
        throw new p("Unsupported field: WeekOfWeekBasedYear");
    }

    @Override // j$.time.temporal.TemporalField
    public final j j(j jVar, long j) {
        e().b(j, this);
        return jVar.f(a.f(j, h(jVar)), a.WEEKS);
    }

    @Override // java.lang.Enum, java.lang.Object
    public final String toString() {
        return "WeekOfWeekBasedYear";
    }
}
