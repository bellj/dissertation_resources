package j$.time.temporal;

import j$.time.Duration;
import j$.time.a;

/* loaded from: classes2.dex */
enum h implements o {
    WEEK_BASED_YEARS("WeekBasedYears"),
    QUARTER_YEARS("QuarterYears");
    
    private final String a;

    static {
        Duration duration = Duration.c;
    }

    h(String str) {
        this.a = str;
    }

    @Override // j$.time.temporal.o
    public final boolean a() {
        return true;
    }

    @Override // j$.time.temporal.o
    public final j c(j jVar, long j) {
        int i = b.a[ordinal()];
        if (i == 1) {
            TemporalField temporalField = i.c;
            return jVar.a(a.b((long) jVar.c(temporalField), j), temporalField);
        } else if (i == 2) {
            return jVar.f(j / 256, a.YEARS).f((j % 256) * 3, a.MONTHS);
        } else {
            throw new IllegalStateException("Unreachable");
        }
    }

    @Override // java.lang.Enum, java.lang.Object
    public final String toString() {
        return this.a;
    }
}
