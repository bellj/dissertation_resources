package j$.time.temporal;

/* loaded from: classes2.dex */
public interface TemporalAccessor {
    int c(TemporalField temporalField);

    q e(TemporalField temporalField);

    long getLong(TemporalField temporalField);

    boolean h(TemporalField temporalField);

    Object k(n nVar);
}
