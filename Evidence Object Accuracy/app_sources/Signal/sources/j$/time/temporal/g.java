package j$.time.temporal;

import j$.time.DayOfWeek;
import j$.time.chrono.a;
import j$.time.chrono.c;
import j$.time.e;
import j$.time.format.G;
import j$.time.h;
import java.util.HashMap;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public abstract class g extends Enum implements TemporalField {
    public static final g DAY_OF_QUARTER;
    public static final g QUARTER_OF_YEAR;
    public static final g WEEK_BASED_YEAR;
    public static final g WEEK_OF_WEEK_BASED_YEAR;
    private static final int[] a = {0, 90, 181, 273, 0, 91, 182, 274};
    private static final /* synthetic */ g[] b;

    static {
        c cVar = new c();
        DAY_OF_QUARTER = cVar;
        d dVar = new d();
        QUARTER_OF_YEAR = dVar;
        e eVar = new e();
        WEEK_OF_WEEK_BASED_YEAR = eVar;
        f fVar = new f();
        WEEK_BASED_YEAR = fVar;
        b = new g[]{cVar, dVar, eVar, fVar};
        a = new int[]{0, 90, 181, 273, 0, 91, 182, 274};
    }

    public g(String str, int i) {
        super(str, i);
    }

    public static boolean k(TemporalAccessor temporalAccessor) {
        return ((a) c.b(temporalAccessor)).equals(j$.time.chrono.g.a);
    }

    public static void m(TemporalAccessor temporalAccessor) {
        if (!((a) c.b(temporalAccessor)).equals(j$.time.chrono.g.a)) {
            throw new e("Resolve requires IsoChronology");
        }
    }

    public static q n(h hVar) {
        return q.i(1, (long) s(r(hVar)));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0056, code lost:
        if ((r0 == -3 || (r0 == -2 && r5.s())) == false) goto L_0x005a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int o(j$.time.h r5) {
        /*
            j$.time.DayOfWeek r0 = r5.p()
            int r0 = r0.ordinal()
            int r1 = r5.q()
            r2 = 1
            int r1 = r1 - r2
            int r0 = 3 - r0
            int r0 = r0 + r1
            int r3 = r0 / 7
            int r3 = r3 * 7
            int r0 = r0 - r3
            r3 = -3
            int r0 = r0 + r3
            if (r0 >= r3) goto L_0x001c
            int r0 = r0 + 7
        L_0x001c:
            if (r1 >= r0) goto L_0x003f
            r0 = 180(0xb4, float:2.52E-43)
            j$.time.h r5 = r5.H(r0)
            r0 = -1
            j$.time.h r5 = r5.C(r0)
            int r5 = r(r5)
            int r5 = s(r5)
            long r0 = (long) r5
            r2 = 1
            j$.time.temporal.q r5 = j$.time.temporal.q.i(r2, r0)
            long r0 = r5.d()
            int r5 = (int) r0
            goto L_0x005b
        L_0x003f:
            int r1 = r1 - r0
            int r1 = r1 / 7
            int r1 = r1 + r2
            r4 = 53
            if (r1 != r4) goto L_0x0059
            if (r0 == r3) goto L_0x0055
            r3 = -2
            if (r0 != r3) goto L_0x0053
            boolean r5 = r5.s()
            if (r5 == 0) goto L_0x0053
            goto L_0x0055
        L_0x0053:
            r5 = 0
            goto L_0x0056
        L_0x0055:
            r5 = 1
        L_0x0056:
            if (r5 != 0) goto L_0x0059
            goto L_0x005a
        L_0x0059:
            r2 = r1
        L_0x005a:
            r5 = r2
        L_0x005b:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: j$.time.temporal.g.o(j$.time.h):int");
    }

    public static int r(h hVar) {
        int r = hVar.r();
        int q = hVar.q();
        if (q <= 3) {
            return q - hVar.p().ordinal() < -2 ? r - 1 : r;
        }
        if (q < 363) {
            return r;
        }
        return ((q - 363) - (hVar.s() ? 1 : 0)) - hVar.p().ordinal() >= 0 ? r + 1 : r;
    }

    public static int s(int i) {
        h u = h.u(i, 1, 1);
        if (u.p() != DayOfWeek.THURSDAY) {
            return (u.p() != DayOfWeek.WEDNESDAY || !u.s()) ? 52 : 53;
        }
        return 53;
    }

    public static g valueOf(String str) {
        return (g) Enum.valueOf(g.class, str);
    }

    public static g[] values() {
        return (g[]) b.clone();
    }

    @Override // j$.time.temporal.TemporalField
    public final boolean a() {
        return true;
    }

    @Override // j$.time.temporal.TemporalField
    public q d(TemporalAccessor temporalAccessor) {
        return e();
    }

    @Override // j$.time.temporal.TemporalField
    public final boolean f() {
        return false;
    }

    @Override // j$.time.temporal.TemporalField
    public /* synthetic */ TemporalAccessor g(HashMap hashMap, TemporalAccessor temporalAccessor, G g) {
        return null;
    }
}
