package j$.time.temporal;

import j$.time.Duration;

/* loaded from: classes2.dex */
public enum a implements o {
    NANOS("Nanos"),
    MICROS("Micros"),
    MILLIS("Millis"),
    SECONDS("Seconds"),
    MINUTES("Minutes"),
    HOURS("Hours"),
    HALF_DAYS("HalfDays"),
    DAYS("Days"),
    WEEKS("Weeks"),
    MONTHS("Months"),
    YEARS("Years"),
    DECADES("Decades"),
    CENTURIES("Centuries"),
    MILLENNIA("Millennia"),
    ERAS("Eras"),
    FOREVER("Forever");
    
    private final String a;

    static {
        Duration.f(1);
        Duration.f(1000);
        Duration.f(1000000);
        Duration.g();
    }

    a(String str) {
        this.a = str;
    }

    @Override // j$.time.temporal.o
    public final boolean a() {
        return compareTo(DAYS) >= 0 && this != FOREVER;
    }

    @Override // j$.time.temporal.o
    public final j c(j jVar, long j) {
        return jVar.f(j, this);
    }

    @Override // java.lang.Enum, java.lang.Object
    public final String toString() {
        return this.a;
    }
}
