package j$.time.temporal;

import j$.time.a;
import j$.time.b;
import j$.time.chrono.c;
import j$.time.chrono.f;
import j$.time.chrono.g;
import j$.time.e;
import j$.time.format.G;
import j$.time.h;
import java.util.HashMap;

/* loaded from: classes2.dex */
public final class r implements TemporalField {
    private static final q f = q.i(1, 7);
    private static final q g = q.j(0, 4, 6);
    private static final q h = q.j(0, 52, 54);
    private static final q i = q.k(52, 53);
    private final String a;
    private final WeekFields b;
    private final o c;
    private final o d;
    private final q e;

    private r(String str, WeekFields weekFields, o oVar, o oVar2, q qVar) {
        this.a = str;
        this.b = weekFields;
        this.c = oVar;
        this.d = oVar2;
        this.e = qVar;
    }

    private static int b(int i2, int i3) {
        return ((i3 - 1) + (i2 + 7)) / 7;
    }

    private int i(TemporalAccessor temporalAccessor) {
        return l.g(temporalAccessor.c(ChronoField.DAY_OF_WEEK) - this.b.getFirstDayOfWeek().l()) + 1;
    }

    private int k(TemporalAccessor temporalAccessor) {
        int i2 = i(temporalAccessor);
        int c = temporalAccessor.c(ChronoField.YEAR);
        ChronoField chronoField = ChronoField.DAY_OF_YEAR;
        int c2 = temporalAccessor.c(chronoField);
        int v = v(c2, i2);
        int b = b(v, c2);
        if (b == 0) {
            return c - 1;
        }
        return b >= b(v, this.b.e() + ((int) temporalAccessor.e(chronoField).d())) ? c + 1 : c;
    }

    private long l(TemporalAccessor temporalAccessor) {
        int i2 = i(temporalAccessor);
        int c = temporalAccessor.c(ChronoField.DAY_OF_MONTH);
        return (long) b(v(c, i2), c);
    }

    private int m(TemporalAccessor temporalAccessor) {
        int i2 = i(temporalAccessor);
        ChronoField chronoField = ChronoField.DAY_OF_YEAR;
        int c = temporalAccessor.c(chronoField);
        int v = v(c, i2);
        int b = b(v, c);
        if (b == 0) {
            ((g) c.b(temporalAccessor)).getClass();
            return m(h.n(temporalAccessor).j((long) c, a.DAYS));
        } else if (b <= 50) {
            return b;
        } else {
            int b2 = b(v, this.b.e() + ((int) temporalAccessor.e(chronoField).d()));
            return b >= b2 ? (b - b2) + 1 : b;
        }
    }

    private long n(TemporalAccessor temporalAccessor) {
        int i2 = i(temporalAccessor);
        int c = temporalAccessor.c(ChronoField.DAY_OF_YEAR);
        return (long) b(v(c, i2), c);
    }

    public static r o(WeekFields weekFields) {
        return new r("DayOfWeek", weekFields, a.DAYS, a.WEEKS, f);
    }

    private h p(f fVar, int i2, int i3, int i4) {
        ((g) fVar).getClass();
        h u = h.u(i2, 1, 1);
        int v = v(1, i(u));
        return u.f((long) (((Math.min(i3, b(v, this.b.e() + (u.s() ? 366 : 365)) - 1) - 1) * 7) + (i4 - 1) + (-v)), a.DAYS);
    }

    public static r q(WeekFields weekFields) {
        return new r("WeekBasedYear", weekFields, i.d, a.FOREVER, ChronoField.YEAR.e());
    }

    public static r r(WeekFields weekFields) {
        return new r("WeekOfMonth", weekFields, a.WEEKS, a.MONTHS, g);
    }

    public static r s(WeekFields weekFields) {
        return new r("WeekOfWeekBasedYear", weekFields, a.WEEKS, i.d, i);
    }

    private q t(TemporalAccessor temporalAccessor, ChronoField chronoField) {
        int v = v(temporalAccessor.c(chronoField), i(temporalAccessor));
        q e = temporalAccessor.e(chronoField);
        return q.i((long) b(v, (int) e.e()), (long) b(v, (int) e.d()));
    }

    private q u(TemporalAccessor temporalAccessor) {
        ChronoField chronoField = ChronoField.DAY_OF_YEAR;
        if (!temporalAccessor.h(chronoField)) {
            return h;
        }
        int i2 = i(temporalAccessor);
        int c = temporalAccessor.c(chronoField);
        int v = v(c, i2);
        int b = b(v, c);
        if (b == 0) {
            ((g) c.b(temporalAccessor)).getClass();
            return u(h.n(temporalAccessor).j((long) (c + 7), a.DAYS));
        }
        int d = (int) temporalAccessor.e(chronoField).d();
        int b2 = b(v, this.b.e() + d);
        if (b < b2) {
            return q.i(1, (long) (b2 - 1));
        }
        ((g) c.b(temporalAccessor)).getClass();
        return u(h.n(temporalAccessor).f((long) ((d - c) + 1 + 7), a.DAYS));
    }

    private int v(int i2, int i3) {
        int g2 = l.g(i2 - i3);
        return g2 + 1 > this.b.e() ? 7 - g2 : -g2;
    }

    @Override // j$.time.temporal.TemporalField
    public final boolean a() {
        return true;
    }

    @Override // j$.time.temporal.TemporalField
    public final boolean c(TemporalAccessor temporalAccessor) {
        ChronoField chronoField;
        if (!temporalAccessor.h(ChronoField.DAY_OF_WEEK)) {
            return false;
        }
        o oVar = this.d;
        if (oVar == a.WEEKS) {
            return true;
        }
        if (oVar == a.MONTHS) {
            chronoField = ChronoField.DAY_OF_MONTH;
        } else if (oVar == a.YEARS || oVar == WeekFields.h) {
            chronoField = ChronoField.DAY_OF_YEAR;
        } else if (oVar != a.FOREVER) {
            return false;
        } else {
            chronoField = ChronoField.YEAR;
        }
        return temporalAccessor.h(chronoField);
    }

    @Override // j$.time.temporal.TemporalField
    public final q d(TemporalAccessor temporalAccessor) {
        o oVar = this.d;
        if (oVar == a.WEEKS) {
            return this.e;
        }
        if (oVar == a.MONTHS) {
            return t(temporalAccessor, ChronoField.DAY_OF_MONTH);
        }
        if (oVar == a.YEARS) {
            return t(temporalAccessor, ChronoField.DAY_OF_YEAR);
        }
        if (oVar == WeekFields.h) {
            return u(temporalAccessor);
        }
        if (oVar == a.FOREVER) {
            return ChronoField.YEAR.e();
        }
        StringBuilder a = b.a("unreachable, rangeUnit: ");
        a.append(this.d);
        a.append(", this: ");
        a.append(this);
        throw new IllegalStateException(a.toString());
    }

    @Override // j$.time.temporal.TemporalField
    public final q e() {
        return this.e;
    }

    @Override // j$.time.temporal.TemporalField
    public final boolean f() {
        return false;
    }

    @Override // j$.time.temporal.TemporalField
    public final TemporalAccessor g(HashMap hashMap, TemporalAccessor temporalAccessor, G g2) {
        h hVar;
        h hVar2;
        h hVar3;
        long longValue = ((Long) hashMap.get(this)).longValue();
        long j = (long) ((int) longValue);
        if (longValue == j) {
            o oVar = this.d;
            o oVar2 = a.WEEKS;
            if (oVar == oVar2) {
                hashMap.remove(this);
                hashMap.put(ChronoField.DAY_OF_WEEK, Long.valueOf((long) (l.g((this.e.a(longValue, this) - 1) + (this.b.getFirstDayOfWeek().l() - 1)) + 1)));
            } else {
                ChronoField chronoField = ChronoField.DAY_OF_WEEK;
                if (hashMap.containsKey(chronoField)) {
                    int g3 = l.g(chronoField.k(((Long) hashMap.get(chronoField)).longValue()) - this.b.getFirstDayOfWeek().l()) + 1;
                    f b = c.b(temporalAccessor);
                    ChronoField chronoField2 = ChronoField.YEAR;
                    if (hashMap.containsKey(chronoField2)) {
                        int k = chronoField2.k(((Long) hashMap.get(chronoField2)).longValue());
                        o oVar3 = this.d;
                        a aVar = a.MONTHS;
                        if (oVar3 == aVar) {
                            ChronoField chronoField3 = ChronoField.MONTH_OF_YEAR;
                            if (hashMap.containsKey(chronoField3)) {
                                long longValue2 = ((Long) hashMap.get(chronoField3)).longValue();
                                if (g2 == G.LENIENT) {
                                    ((g) b).getClass();
                                    h x = h.u(k, 1, 1).f(a.f(longValue2, 1), aVar);
                                    hVar3 = x.f(a.b(a.d(a.f(j, l(x)), 7), (long) (g3 - i(x))), a.DAYS);
                                } else {
                                    int k2 = chronoField3.k(longValue2);
                                    ((g) b).getClass();
                                    h u = h.u(k, k2, 1);
                                    h x2 = u.f((long) ((((int) (((long) this.e.a(j, this)) - l(u))) * 7) + (g3 - i(u))), a.DAYS);
                                    if (g2 != G.STRICT || x2.getLong(chronoField3) == longValue2) {
                                        hVar3 = x2;
                                    } else {
                                        throw new e("Strict mode rejected resolved date as it is in a different month");
                                    }
                                }
                                hashMap.remove(this);
                                hashMap.remove(chronoField2);
                                hashMap.remove(chronoField3);
                                hashMap.remove(chronoField);
                                return hVar3;
                            }
                        }
                        if (this.d == a.YEARS) {
                            ((g) b).getClass();
                            h u2 = h.u(k, 1, 1);
                            if (g2 == G.LENIENT) {
                                hVar2 = u2.f(a.b(a.d(a.f(j, n(u2)), 7), (long) (g3 - i(u2))), a.DAYS);
                            } else {
                                h x3 = u2.f((long) ((((int) (((long) this.e.a(j, this)) - n(u2))) * 7) + (g3 - i(u2))), a.DAYS);
                                if (g2 != G.STRICT || x3.getLong(chronoField2) == ((long) k)) {
                                    hVar2 = x3;
                                } else {
                                    throw new e("Strict mode rejected resolved date as it is in a different year");
                                }
                            }
                            hashMap.remove(this);
                            hashMap.remove(chronoField2);
                            hashMap.remove(chronoField);
                            return hVar2;
                        }
                    } else {
                        o oVar4 = this.d;
                        if ((oVar4 == WeekFields.h || oVar4 == a.FOREVER) && hashMap.containsKey(this.b.f) && hashMap.containsKey(this.b.e)) {
                            int a = ((r) this.b.f).e.a(((Long) hashMap.get(this.b.f)).longValue(), this.b.f);
                            if (g2 == G.LENIENT) {
                                hVar = p(b, a, 1, g3).f(a.f(((Long) hashMap.get(this.b.e)).longValue(), 1), oVar2);
                            } else {
                                h p = p(b, a, ((r) this.b.e).e.a(((Long) hashMap.get(this.b.e)).longValue(), this.b.e), g3);
                                if (g2 != G.STRICT || k(p) == a) {
                                    hVar = p;
                                } else {
                                    throw new e("Strict mode rejected resolved date as it is in a different week-based-year");
                                }
                            }
                            hashMap.remove(this);
                            hashMap.remove(this.b.f);
                            hashMap.remove(this.b.e);
                            hashMap.remove(chronoField);
                            return hVar;
                        }
                    }
                }
            }
            return null;
        }
        throw new ArithmeticException();
    }

    @Override // j$.time.temporal.TemporalField
    public final long h(TemporalAccessor temporalAccessor) {
        int i2;
        o oVar = this.d;
        if (oVar == a.WEEKS) {
            i2 = i(temporalAccessor);
        } else if (oVar == a.MONTHS) {
            return l(temporalAccessor);
        } else {
            if (oVar == a.YEARS) {
                return n(temporalAccessor);
            }
            if (oVar == WeekFields.h) {
                i2 = m(temporalAccessor);
            } else if (oVar == a.FOREVER) {
                i2 = k(temporalAccessor);
            } else {
                StringBuilder a = b.a("unreachable, rangeUnit: ");
                a.append(this.d);
                a.append(", this: ");
                a.append(this);
                throw new IllegalStateException(a.toString());
            }
        }
        return (long) i2;
    }

    @Override // j$.time.temporal.TemporalField
    public final j j(j jVar, long j) {
        int a = this.e.a(j, this);
        int c = jVar.c(this);
        if (a == c) {
            return jVar;
        }
        if (this.d != a.FOREVER) {
            return jVar.f((long) (a - c), this.c);
        }
        int c2 = jVar.c(this.b.c);
        return p(c.b(jVar), (int) j, jVar.c(this.b.e), c2);
    }

    public final String toString() {
        return this.a + "[" + this.b.toString() + "]";
    }
}
