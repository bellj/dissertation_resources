package j$.time.temporal;

import j$.time.LocalTime;
import j$.time.ZoneId;
import j$.time.ZoneOffset;
import j$.time.chrono.f;
import j$.time.h;

/* loaded from: classes2.dex */
public final /* synthetic */ class m implements n, k {
    public final /* synthetic */ int a;

    public /* synthetic */ m(int i) {
        this.a = i;
    }

    @Override // j$.time.temporal.n
    public Object a(TemporalAccessor temporalAccessor) {
        switch (this.a) {
            case 0:
                return (ZoneId) temporalAccessor.k(l.a);
            case 1:
                return (f) temporalAccessor.k(l.b);
            case 2:
                return (o) temporalAccessor.k(l.c);
            case 3:
                ChronoField chronoField = ChronoField.OFFSET_SECONDS;
                if (temporalAccessor.h(chronoField)) {
                    return ZoneOffset.r(temporalAccessor.c(chronoField));
                }
                return null;
            case 4:
                ZoneId zoneId = (ZoneId) temporalAccessor.k(l.a);
                return zoneId != null ? zoneId : (ZoneId) temporalAccessor.k(l.d);
            case 5:
                ChronoField chronoField2 = ChronoField.EPOCH_DAY;
                if (temporalAccessor.h(chronoField2)) {
                    return h.v(temporalAccessor.getLong(chronoField2));
                }
                return null;
            default:
                ChronoField chronoField3 = ChronoField.NANO_OF_DAY;
                if (temporalAccessor.h(chronoField3)) {
                    return LocalTime.r(temporalAccessor.getLong(chronoField3));
                }
                return null;
        }
    }

    @Override // j$.time.temporal.k
    public j g(j jVar) {
        int i = this.a;
        int c = jVar.c(ChronoField.DAY_OF_WEEK);
        if (c == i) {
            return jVar;
        }
        int i2 = c - i;
        return jVar.f((long) (i2 >= 0 ? 7 - i2 : -i2), a.DAYS);
    }
}
