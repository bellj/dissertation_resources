package j$.time.temporal;

/* loaded from: classes2.dex */
public abstract class i {
    public static final TemporalField a = g.QUARTER_OF_YEAR;
    public static final TemporalField b = g.WEEK_OF_WEEK_BASED_YEAR;
    public static final TemporalField c = g.WEEK_BASED_YEAR;
    public static final o d = h.WEEK_BASED_YEARS;

    static {
        g gVar = g.DAY_OF_QUARTER;
        a = g.QUARTER_OF_YEAR;
        b = g.WEEK_OF_WEEK_BASED_YEAR;
        c = g.WEEK_BASED_YEAR;
        d = h.WEEK_BASED_YEARS;
        h hVar = h.WEEK_BASED_YEARS;
    }
}
