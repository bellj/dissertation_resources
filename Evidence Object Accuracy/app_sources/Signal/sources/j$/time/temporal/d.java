package j$.time.temporal;

/* loaded from: classes2.dex */
final class d extends g {
    public d() {
        super("QUARTER_OF_YEAR", 1);
    }

    @Override // j$.time.temporal.TemporalField
    public final boolean c(TemporalAccessor temporalAccessor) {
        return temporalAccessor.h(ChronoField.MONTH_OF_YEAR) && g.k(temporalAccessor);
    }

    @Override // j$.time.temporal.TemporalField
    public final q e() {
        return q.i(1, 4);
    }

    @Override // j$.time.temporal.TemporalField
    public final long h(TemporalAccessor temporalAccessor) {
        if (c(temporalAccessor)) {
            return (temporalAccessor.getLong(ChronoField.MONTH_OF_YEAR) + 2) / 3;
        }
        throw new p("Unsupported field: QuarterOfYear");
    }

    @Override // j$.time.temporal.TemporalField
    public final j j(j jVar, long j) {
        long h = h(jVar);
        e().b(j, this);
        ChronoField chronoField = ChronoField.MONTH_OF_YEAR;
        return jVar.a(((j - h) * 3) + jVar.getLong(chronoField), chronoField);
    }

    @Override // java.lang.Enum, java.lang.Object
    public final String toString() {
        return "QuarterOfYear";
    }
}
