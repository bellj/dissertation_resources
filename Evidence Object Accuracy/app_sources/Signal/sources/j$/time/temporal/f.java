package j$.time.temporal;

import j$.time.h;

/* loaded from: classes2.dex */
final class f extends g {
    public f() {
        super("WEEK_BASED_YEAR", 3);
    }

    @Override // j$.time.temporal.TemporalField
    public final boolean c(TemporalAccessor temporalAccessor) {
        return temporalAccessor.h(ChronoField.EPOCH_DAY) && g.k(temporalAccessor);
    }

    @Override // j$.time.temporal.TemporalField
    public final q e() {
        return ChronoField.YEAR.e();
    }

    @Override // j$.time.temporal.TemporalField
    public final long h(TemporalAccessor temporalAccessor) {
        if (c(temporalAccessor)) {
            return (long) g.r(h.n(temporalAccessor));
        }
        throw new p("Unsupported field: WeekBasedYear");
    }

    @Override // j$.time.temporal.TemporalField
    public final j j(j jVar, long j) {
        if (c(jVar)) {
            int a = e().a(j, g.WEEK_BASED_YEAR);
            h n = h.n(jVar);
            ChronoField chronoField = ChronoField.DAY_OF_WEEK;
            int c = n.c(chronoField);
            int o = g.o(n);
            if (o == 53 && g.s(a) == 52) {
                o = 52;
            }
            h u = h.u(a, 1, 4);
            return jVar.d(u.z((long) (((o - 1) * 7) + (c - u.c(chronoField)))));
        }
        throw new p("Unsupported field: WeekBasedYear");
    }

    @Override // java.lang.Enum, java.lang.Object
    public final String toString() {
        return "WeekBasedYear";
    }
}
