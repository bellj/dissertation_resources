package j$.time.temporal;

import j$.time.e;

/* loaded from: classes2.dex */
public abstract class l {
    static final m a = new m(0);
    static final m b = new m(1);
    static final m c = new m(2);
    static final m d = new m(3);
    static final m e = new m(4);
    static final m f = new m(5);
    static final m g = new m(6);

    public static int a(TemporalAccessor temporalAccessor, TemporalField temporalField) {
        q e2 = temporalAccessor.e(temporalField);
        if (e2.g()) {
            long j = temporalAccessor.getLong(temporalField);
            if (e2.h(j)) {
                return (int) j;
            }
            throw new e("Invalid value for " + temporalField + " (valid values " + e2 + "): " + j);
        }
        throw new p("Invalid field " + temporalField + " for get() method, use getLong() instead");
    }

    public static Object b(TemporalAccessor temporalAccessor, n nVar) {
        if (nVar == a || nVar == b || nVar == c) {
            return null;
        }
        return nVar.a(temporalAccessor);
    }

    public static q c(TemporalAccessor temporalAccessor, TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            if (temporalAccessor.h(temporalField)) {
                return temporalField.e();
            }
            throw new p("Unsupported field: " + temporalField);
        } else if (temporalField != null) {
            return temporalField.d(temporalAccessor);
        } else {
            throw new NullPointerException("field");
        }
    }

    public static m d() {
        return b;
    }

    public static m e() {
        return f;
    }

    public static m f() {
        return g;
    }

    public static /* synthetic */ int g(int i) {
        int i2 = i % 7;
        if (i2 == 0) {
            return 0;
        }
        return (((i ^ 7) >> 31) | 1) > 0 ? i2 : i2 + 7;
    }

    public static m h() {
        return d;
    }

    public static m i() {
        return c;
    }

    public static m j() {
        return e;
    }

    public static m k() {
        return a;
    }
}
