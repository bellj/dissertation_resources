package j$.time.temporal;

import j$.time.format.G;
import java.util.HashMap;

/* loaded from: classes2.dex */
public interface TemporalField {
    boolean a();

    boolean c(TemporalAccessor temporalAccessor);

    q d(TemporalAccessor temporalAccessor);

    q e();

    boolean f();

    TemporalAccessor g(HashMap hashMap, TemporalAccessor temporalAccessor, G g);

    long h(TemporalAccessor temporalAccessor);

    j j(j jVar, long j);
}
