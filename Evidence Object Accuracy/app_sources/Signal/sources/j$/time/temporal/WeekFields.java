package j$.time.temporal;

import j$.time.DayOfWeek;
import j$.time.b;
import j$.util.concurrent.ConcurrentHashMap;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Locale;

/* loaded from: classes2.dex */
public final class WeekFields implements Serializable {
    private static final ConcurrentHashMap g = new ConcurrentHashMap(4, 0.75f, 2);
    public static final o h = i.d;
    private final DayOfWeek a;
    private final int b;
    private final transient TemporalField c = r.o(this);
    private final transient TemporalField d = r.r(this);
    private final transient TemporalField e;
    private final transient TemporalField f;

    static {
        g = new ConcurrentHashMap(4, 0.75f, 2);
        new WeekFields(DayOfWeek.MONDAY, 4);
        f(DayOfWeek.SUNDAY, 1);
        h = i.d;
    }

    private WeekFields(DayOfWeek dayOfWeek, int i) {
        a aVar = a.NANOS;
        this.e = r.s(this);
        this.f = r.q(this);
        if (dayOfWeek == null) {
            throw new NullPointerException("firstDayOfWeek");
        } else if (i < 1 || i > 7) {
            throw new IllegalArgumentException("Minimal number of days is invalid");
        } else {
            this.a = dayOfWeek;
            this.b = i;
        }
    }

    public static WeekFields f(DayOfWeek dayOfWeek, int i) {
        String str = dayOfWeek.toString() + i;
        ConcurrentHashMap concurrentHashMap = g;
        WeekFields weekFields = (WeekFields) concurrentHashMap.get(str);
        if (weekFields != null) {
            return weekFields;
        }
        concurrentHashMap.putIfAbsent(str, new WeekFields(dayOfWeek, i));
        return (WeekFields) concurrentHashMap.get(str);
    }

    public static WeekFields of(Locale locale) {
        if (locale != null) {
            Calendar instance = Calendar.getInstance(new Locale(locale.getLanguage(), locale.getCountry()));
            return f(DayOfWeek.SUNDAY.plus((long) (instance.getFirstDayOfWeek() - 1)), instance.getMinimalDaysInFirstWeek());
        }
        throw new NullPointerException("locale");
    }

    public final TemporalField d() {
        return this.c;
    }

    public final int e() {
        return this.b;
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof WeekFields) && hashCode() == obj.hashCode();
    }

    public final TemporalField g() {
        return this.f;
    }

    public DayOfWeek getFirstDayOfWeek() {
        return this.a;
    }

    public final TemporalField h() {
        return this.d;
    }

    @Override // java.lang.Object
    public final int hashCode() {
        return (this.a.ordinal() * 7) + this.b;
    }

    public final TemporalField i() {
        return this.e;
    }

    @Override // java.lang.Object
    public final String toString() {
        StringBuilder a = b.a("WeekFields[");
        a.append(this.a);
        a.append(',');
        a.append(this.b);
        a.append(']');
        return a.toString();
    }
}
