package j$.time;

import j$.time.chrono.ChronoLocalDateTime;
import j$.time.chrono.a;
import j$.time.chrono.b;
import j$.time.chrono.f;
import j$.time.chrono.g;
import j$.time.temporal.ChronoField;
import j$.time.temporal.TemporalAccessor;
import j$.time.temporal.TemporalAmount;
import j$.time.temporal.TemporalField;
import j$.time.temporal.j;
import j$.time.temporal.k;
import j$.time.temporal.l;
import j$.time.temporal.n;
import j$.time.temporal.o;
import j$.time.temporal.q;
import java.io.Serializable;

/* loaded from: classes2.dex */
public final class LocalDateTime implements j, k, ChronoLocalDateTime<h>, Serializable {
    public static final LocalDateTime c = q(h.d, LocalTime.e);
    public static final LocalDateTime d = q(h.e, LocalTime.f);
    private final h a;
    private final LocalTime b;

    private LocalDateTime(h hVar, LocalTime localTime) {
        this.a = hVar;
        this.b = localTime;
    }

    public static LocalDateTime from(TemporalAccessor temporalAccessor) {
        if (temporalAccessor instanceof LocalDateTime) {
            return (LocalDateTime) temporalAccessor;
        }
        if (temporalAccessor instanceof ZonedDateTime) {
            return ((ZonedDateTime) temporalAccessor).t();
        }
        if (temporalAccessor instanceof OffsetDateTime) {
            return ((OffsetDateTime) temporalAccessor).o();
        }
        try {
            return new LocalDateTime(h.n(temporalAccessor), LocalTime.from(temporalAccessor));
        } catch (e e) {
            throw new e("Unable to obtain LocalDateTime from TemporalAccessor: " + temporalAccessor + " of type " + temporalAccessor.getClass().getName(), e);
        }
    }

    private int l(LocalDateTime localDateTime) {
        int m = this.a.m(localDateTime.a);
        return m == 0 ? this.b.compareTo(localDateTime.toLocalTime()) : m;
    }

    public static LocalDateTime now() {
        c cVar = new c(ZoneId.systemDefault());
        Instant ofEpochMilli = Instant.ofEpochMilli(System.currentTimeMillis());
        return r(ofEpochMilli.getEpochSecond(), ofEpochMilli.n(), cVar.c().m().d(ofEpochMilli));
    }

    public static LocalDateTime o(int i) {
        return new LocalDateTime(h.u(i, 12, 31), LocalTime.of(0, 0));
    }

    public static LocalDateTime ofInstant(Instant instant, ZoneId zoneId) {
        if (instant == null) {
            throw new NullPointerException("instant");
        } else if (zoneId != null) {
            return r(instant.getEpochSecond(), instant.n(), zoneId.m().d(instant));
        } else {
            throw new NullPointerException("zone");
        }
    }

    public static LocalDateTime p(int i, int i2, int i3, int i4, int i5, int i6) {
        return new LocalDateTime(h.u(i, i2, i3), LocalTime.q(i4, i5, i6, 0));
    }

    public static LocalDateTime q(h hVar, LocalTime localTime) {
        if (hVar == null) {
            throw new NullPointerException("date");
        } else if (localTime != null) {
            return new LocalDateTime(hVar, localTime);
        } else {
            throw new NullPointerException("time");
        }
    }

    public static LocalDateTime r(long j, int i, ZoneOffset zoneOffset) {
        if (zoneOffset != null) {
            long j2 = (long) i;
            ChronoField.NANO_OF_SECOND.l(j2);
            long o = j + ((long) zoneOffset.o());
            return new LocalDateTime(h.v(a.e(o, 86400)), LocalTime.r((((long) ((int) a.c(o, 86400))) * 1000000000) + j2));
        }
        throw new NullPointerException("offset");
    }

    private LocalDateTime u(h hVar, long j, long j2, long j3, long j4) {
        h hVar2;
        LocalTime localTime;
        if ((j | j2 | j3 | j4) == 0) {
            localTime = this.b;
            hVar2 = hVar;
        } else {
            long j5 = j / 24;
            long j6 = j5 + (j2 / 1440) + (j3 / 86400) + (j4 / 86400000000000L);
            long j7 = (long) 1;
            long j8 = ((j % 24) * 3600000000000L) + ((j2 % 1440) * 60000000000L) + ((j3 % 86400) * 1000000000) + (j4 % 86400000000000L);
            long x = this.b.x();
            long j9 = (j8 * j7) + x;
            long e = a.e(j9, 86400000000000L) + (j6 * j7);
            long c2 = a.c(j9, 86400000000000L);
            localTime = c2 == x ? this.b : LocalTime.r(c2);
            hVar2 = hVar.z(e);
        }
        return y(hVar2, localTime);
    }

    private LocalDateTime y(h hVar, LocalTime localTime) {
        return (this.a == hVar && this.b == localTime) ? this : new LocalDateTime(hVar, localTime);
    }

    @Override // j$.time.chrono.ChronoLocalDateTime
    public final f b() {
        ((h) i()).getClass();
        return g.a;
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final int c(TemporalField temporalField) {
        return temporalField instanceof ChronoField ? ((ChronoField) temporalField).f() ? this.b.c(temporalField) : this.a.c(temporalField) : l.a(this, temporalField);
    }

    @Override // j$.time.chrono.ChronoLocalDateTime
    public int compareTo(ChronoLocalDateTime<?> chronoLocalDateTime) {
        if (chronoLocalDateTime instanceof LocalDateTime) {
            return l((LocalDateTime) chronoLocalDateTime);
        }
        int l = ((h) i()).compareTo(chronoLocalDateTime.i());
        if (l != 0) {
            return l;
        }
        int l2 = toLocalTime().compareTo(chronoLocalDateTime.toLocalTime());
        if (l2 != 0) {
            return l2;
        }
        f b = b();
        f b2 = chronoLocalDateTime.b();
        ((a) b).getClass();
        b2.getClass();
        return 0;
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final q e(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.d(this);
        }
        if (!((ChronoField) temporalField).f()) {
            return this.a.e(temporalField);
        }
        LocalTime localTime = this.b;
        localTime.getClass();
        return l.c(localTime, temporalField);
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LocalDateTime)) {
            return false;
        }
        LocalDateTime localDateTime = (LocalDateTime) obj;
        return this.a.equals(localDateTime.a) && this.b.equals(localDateTime.b);
    }

    @Override // j$.time.temporal.k
    public final j g(j jVar) {
        return jVar.a(this.a.E(), ChronoField.EPOCH_DAY).a(toLocalTime().x(), ChronoField.NANO_OF_DAY);
    }

    public DayOfWeek getDayOfWeek() {
        return this.a.p();
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final long getLong(TemporalField temporalField) {
        return temporalField instanceof ChronoField ? ((ChronoField) temporalField).f() ? this.b.getLong(temporalField) : this.a.getLong(temporalField) : temporalField.h(this);
    }

    public int getYear() {
        return this.a.r();
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final boolean h(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return temporalField != null && temporalField.c(this);
        }
        ChronoField chronoField = (ChronoField) temporalField;
        return chronoField.a() || chronoField.f();
    }

    @Override // java.lang.Object
    public int hashCode() {
        return this.a.hashCode() ^ this.b.hashCode();
    }

    @Override // j$.time.chrono.ChronoLocalDateTime
    public final b i() {
        return this.a;
    }

    public boolean isAfter(ChronoLocalDateTime<?> chronoLocalDateTime) {
        if (chronoLocalDateTime instanceof LocalDateTime) {
            return l((LocalDateTime) chronoLocalDateTime) > 0;
        }
        long E = ((h) i()).E();
        long E2 = ((h) chronoLocalDateTime.i()).E();
        return E > E2 || (E == E2 && toLocalTime().x() > chronoLocalDateTime.toLocalTime().x());
    }

    public boolean isBefore(ChronoLocalDateTime<?> chronoLocalDateTime) {
        if (chronoLocalDateTime instanceof LocalDateTime) {
            return l((LocalDateTime) chronoLocalDateTime) < 0;
        }
        long E = ((h) i()).E();
        long E2 = ((h) chronoLocalDateTime.i()).E();
        return E < E2 || (E == E2 && toLocalTime().x() < chronoLocalDateTime.toLocalTime().x());
    }

    public boolean isEqual(ChronoLocalDateTime<?> chronoLocalDateTime) {
        return chronoLocalDateTime instanceof LocalDateTime ? l((LocalDateTime) chronoLocalDateTime) == 0 : toLocalTime().x() == chronoLocalDateTime.toLocalTime().x() && ((h) i()).E() == ((h) chronoLocalDateTime.i()).E();
    }

    @Override // j$.time.temporal.j
    public final j j(long j, j$.time.temporal.a aVar) {
        return j == Long.MIN_VALUE ? f(Long.MAX_VALUE, aVar).f(1, aVar) : f(-j, aVar);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final Object k(n nVar) {
        if (nVar == l.e()) {
            return this.a;
        }
        if (nVar == l.k() || nVar == l.j() || nVar == l.h()) {
            return null;
        }
        return nVar == l.f() ? toLocalTime() : nVar == l.d() ? b() : nVar == l.i() ? j$.time.temporal.a.NANOS : nVar.a(this);
    }

    public final int m() {
        return this.b.o();
    }

    public LocalDateTime minusDays(long j) {
        return j == Long.MIN_VALUE ? plusDays(Long.MAX_VALUE).plusDays(1) : plusDays(-j);
    }

    public final int n() {
        return this.b.p();
    }

    public LocalDateTime plus(TemporalAmount temporalAmount) {
        if (temporalAmount instanceof Period) {
            return y(this.a.y((Period) temporalAmount), this.b);
        } else if (temporalAmount != null) {
            return (LocalDateTime) temporalAmount.a(this);
        } else {
            throw new NullPointerException("amountToAdd");
        }
    }

    public LocalDateTime plusDays(long j) {
        return y(this.a.z(j), this.b);
    }

    /* renamed from: s */
    public final LocalDateTime f(long j, o oVar) {
        if (!(oVar instanceof j$.time.temporal.a)) {
            return (LocalDateTime) oVar.c(this, j);
        }
        switch (i.a[((j$.time.temporal.a) oVar).ordinal()]) {
            case 1:
                return u(this.a, 0, 0, 0, j);
            case 2:
                LocalDateTime plusDays = plusDays(j / 86400000000L);
                return plusDays.u(plusDays.a, 0, 0, 0, (j % 86400000000L) * 1000);
            case 3:
                LocalDateTime plusDays2 = plusDays(j / 86400000);
                return plusDays2.u(plusDays2.a, 0, 0, 0, (j % 86400000) * 1000000);
            case 4:
                return t(j);
            case 5:
                return u(this.a, 0, j, 0, 0);
            case 6:
                return u(this.a, j, 0, 0, 0);
            case 7:
                LocalDateTime plusDays3 = plusDays(j / 256);
                return plusDays3.u(plusDays3.a, (j % 256) * 12, 0, 0, 0);
            default:
                return y(this.a.f(j, oVar), this.b);
        }
    }

    public final LocalDateTime t(long j) {
        return u(this.a, 0, 0, j, 0);
    }

    @Override // j$.time.chrono.ChronoLocalDateTime
    public long toEpochSecond(ZoneOffset zoneOffset) {
        if (zoneOffset != null) {
            return ((((h) i()).E() * 86400) + ((long) toLocalTime().y())) - ((long) zoneOffset.o());
        }
        throw new NullPointerException("offset");
    }

    @Override // j$.time.chrono.ChronoLocalDateTime
    public LocalTime toLocalTime() {
        return this.b;
    }

    @Override // java.lang.Object
    public final String toString() {
        return this.a.toString() + 'T' + this.b.toString();
    }

    public final h v() {
        return this.a;
    }

    /* renamed from: w */
    public final LocalDateTime a(long j, TemporalField temporalField) {
        return temporalField instanceof ChronoField ? ((ChronoField) temporalField).f() ? y(this.a, this.b.a(j, temporalField)) : y(this.a.a(j, temporalField), this.b) : (LocalDateTime) temporalField.j(this, j);
    }

    public LocalDateTime withHour(int i) {
        return y(this.a, this.b.A(i));
    }

    public LocalDateTime withMinute(int i) {
        return y(this.a, this.b.B(i));
    }

    public LocalDateTime withSecond(int i) {
        return y(this.a, this.b.D(i));
    }

    /* renamed from: x */
    public final LocalDateTime d(h hVar) {
        return y(hVar, this.b);
    }
}
