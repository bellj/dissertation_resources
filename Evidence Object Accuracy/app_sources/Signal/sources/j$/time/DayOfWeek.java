package j$.time;

import j$.time.format.TextStyle;
import j$.time.format.x;
import j$.time.temporal.ChronoField;
import j$.time.temporal.TemporalAccessor;
import j$.time.temporal.TemporalField;
import j$.time.temporal.a;
import j$.time.temporal.j;
import j$.time.temporal.k;
import j$.time.temporal.l;
import j$.time.temporal.n;
import j$.time.temporal.p;
import j$.time.temporal.q;
import java.util.Locale;

/* loaded from: classes2.dex */
public enum DayOfWeek implements TemporalAccessor, k {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;
    
    private static final DayOfWeek[] a = values();

    public static DayOfWeek m(int i) {
        if (i >= 1 && i <= 7) {
            return a[i - 1];
        }
        throw new e("Invalid value for DayOfWeek: " + i);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final int c(TemporalField temporalField) {
        return temporalField == ChronoField.DAY_OF_WEEK ? l() : l.a(this, temporalField);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final q e(TemporalField temporalField) {
        return temporalField == ChronoField.DAY_OF_WEEK ? temporalField.e() : l.c(this, temporalField);
    }

    @Override // j$.time.temporal.k
    public final j g(j jVar) {
        return jVar.a((long) l(), ChronoField.DAY_OF_WEEK);
    }

    public String getDisplayName(TextStyle textStyle, Locale locale) {
        x xVar = new x();
        xVar.l(ChronoField.DAY_OF_WEEK, textStyle);
        return xVar.w(locale).format(this);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final long getLong(TemporalField temporalField) {
        if (temporalField == ChronoField.DAY_OF_WEEK) {
            return (long) l();
        }
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.h(this);
        }
        throw new p("Unsupported field: " + temporalField);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final boolean h(TemporalField temporalField) {
        return temporalField instanceof ChronoField ? temporalField == ChronoField.DAY_OF_WEEK : temporalField != null && temporalField.c(this);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final Object k(n nVar) {
        return nVar == l.i() ? a.DAYS : l.b(this, nVar);
    }

    public final int l() {
        return ordinal() + 1;
    }

    public DayOfWeek minus(long j) {
        return plus(-(j % 7));
    }

    public DayOfWeek plus(long j) {
        return a[((((int) (j % 7)) + 7) + ordinal()) % 7];
    }
}
