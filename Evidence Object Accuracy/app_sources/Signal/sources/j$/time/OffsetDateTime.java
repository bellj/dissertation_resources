package j$.time;

import j$.time.chrono.ChronoLocalDateTime;
import j$.time.chrono.g;
import j$.time.temporal.ChronoField;
import j$.time.temporal.TemporalField;
import j$.time.temporal.a;
import j$.time.temporal.j;
import j$.time.temporal.k;
import j$.time.temporal.l;
import j$.time.temporal.n;
import j$.time.temporal.o;
import j$.time.temporal.p;
import j$.time.temporal.q;
import j$.time.zone.c;
import java.io.Serializable;

/* loaded from: classes2.dex */
public final class OffsetDateTime implements j, k, Comparable<OffsetDateTime>, Serializable {
    private final LocalDateTime a;
    private final ZoneOffset b;

    static {
        LocalDateTime localDateTime = LocalDateTime.c;
        ZoneOffset zoneOffset = ZoneOffset.h;
        localDateTime.getClass();
        l(localDateTime, zoneOffset);
        LocalDateTime localDateTime2 = LocalDateTime.d;
        ZoneOffset zoneOffset2 = ZoneOffset.g;
        localDateTime2.getClass();
        l(localDateTime2, zoneOffset2);
    }

    private OffsetDateTime(LocalDateTime localDateTime, ZoneOffset zoneOffset) {
        if (localDateTime != null) {
            this.a = localDateTime;
            if (zoneOffset != null) {
                this.b = zoneOffset;
                return;
            }
            throw new NullPointerException("offset");
        }
        throw new NullPointerException("dateTime");
    }

    public static OffsetDateTime l(LocalDateTime localDateTime, ZoneOffset zoneOffset) {
        return new OffsetDateTime(localDateTime, zoneOffset);
    }

    public static OffsetDateTime m(Instant instant, ZoneOffset zoneOffset) {
        if (instant == null) {
            throw new NullPointerException("instant");
        } else if (zoneOffset != null) {
            ZoneOffset d = c.j(zoneOffset).d(instant);
            return new OffsetDateTime(LocalDateTime.r(instant.getEpochSecond(), instant.n(), d), d);
        } else {
            throw new NullPointerException("zone");
        }
    }

    public static OffsetDateTime now(ZoneId zoneId) {
        if (zoneId != null) {
            c cVar = new c(zoneId);
            Instant ofEpochMilli = Instant.ofEpochMilli(System.currentTimeMillis());
            return m(ofEpochMilli, cVar.c().m().d(ofEpochMilli));
        }
        throw new NullPointerException("zone");
    }

    private OffsetDateTime p(LocalDateTime localDateTime, ZoneOffset zoneOffset) {
        return (this.a != localDateTime || !this.b.equals(zoneOffset)) ? new OffsetDateTime(localDateTime, zoneOffset) : this;
    }

    @Override // j$.time.temporal.j
    public final j a(long j, TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return (OffsetDateTime) temporalField.j(this, j);
        }
        ChronoField chronoField = (ChronoField) temporalField;
        int i = m.a[chronoField.ordinal()];
        return i != 1 ? i != 2 ? p(this.a.a(j, temporalField), this.b) : p(this.a, ZoneOffset.r(chronoField.k(j))) : m(Instant.o(j, (long) this.a.m()), this.b);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final int c(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return l.a(this, temporalField);
        }
        int i = m.a[((ChronoField) temporalField).ordinal()];
        if (i != 1) {
            return i != 2 ? this.a.c(temporalField) : getOffset().o();
        }
        throw new p("Invalid field 'InstantSeconds' for get() method, use getLong() instead");
    }

    @Override // java.lang.Comparable
    public final int compareTo(OffsetDateTime offsetDateTime) {
        int i;
        OffsetDateTime offsetDateTime2 = offsetDateTime;
        if (getOffset().equals(offsetDateTime2.getOffset())) {
            i = this.a.compareTo((ChronoLocalDateTime<?>) offsetDateTime2.a);
        } else {
            i = Long.compare(this.a.toEpochSecond(this.b), offsetDateTime2.a.toEpochSecond(offsetDateTime2.b));
            if (i == 0) {
                i = this.a.toLocalTime().o() - offsetDateTime2.a.toLocalTime().o();
            }
        }
        return i == 0 ? this.a.compareTo((ChronoLocalDateTime<?>) offsetDateTime2.a) : i;
    }

    @Override // j$.time.temporal.j
    public final j d(h hVar) {
        return p(this.a.d(hVar), this.b);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final q e(TemporalField temporalField) {
        return temporalField instanceof ChronoField ? (temporalField == ChronoField.INSTANT_SECONDS || temporalField == ChronoField.OFFSET_SECONDS) ? temporalField.e() : this.a.e(temporalField) : temporalField.d(this);
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof OffsetDateTime)) {
            return false;
        }
        OffsetDateTime offsetDateTime = (OffsetDateTime) obj;
        return this.a.equals(offsetDateTime.a) && this.b.equals(offsetDateTime.b);
    }

    @Override // j$.time.temporal.k
    public final j g(j jVar) {
        return jVar.a(this.a.v().E(), ChronoField.EPOCH_DAY).a(this.a.toLocalTime().x(), ChronoField.NANO_OF_DAY).a((long) getOffset().o(), ChronoField.OFFSET_SECONDS);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final long getLong(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.h(this);
        }
        int i = m.a[((ChronoField) temporalField).ordinal()];
        return i != 1 ? i != 2 ? this.a.getLong(temporalField) : (long) getOffset().o() : this.a.toEpochSecond(this.b);
    }

    public ZoneOffset getOffset() {
        return this.b;
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final boolean h(TemporalField temporalField) {
        return (temporalField instanceof ChronoField) || (temporalField != null && temporalField.c(this));
    }

    @Override // java.lang.Object
    public final int hashCode() {
        return this.a.hashCode() ^ this.b.hashCode();
    }

    @Override // j$.time.temporal.j
    public final j j(long j, a aVar) {
        return j == Long.MIN_VALUE ? f(Long.MAX_VALUE, aVar).f(1, aVar) : f(-j, aVar);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final Object k(n nVar) {
        if (nVar == l.h() || nVar == l.j()) {
            return getOffset();
        }
        if (nVar == l.k()) {
            return null;
        }
        return nVar == l.e() ? this.a.v() : nVar == l.f() ? this.a.toLocalTime() : nVar == l.d() ? g.a : nVar == l.i() ? a.NANOS : nVar.a(this);
    }

    /* renamed from: n */
    public final OffsetDateTime f(long j, o oVar) {
        return oVar instanceof a ? p(this.a.f(j, oVar), this.b) : (OffsetDateTime) oVar.c(this, j);
    }

    public final LocalDateTime o() {
        return this.a;
    }

    @Override // java.lang.Object
    public final String toString() {
        return this.a.toString() + this.b.toString();
    }
}
