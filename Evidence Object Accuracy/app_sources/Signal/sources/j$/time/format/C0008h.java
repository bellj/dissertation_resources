package j$.time.format;

import j$.time.b;
import j$.time.temporal.ChronoField;
import j$.time.temporal.TemporalField;
import j$.time.temporal.q;
import java.math.BigDecimal;
import java.math.RoundingMode;

/* renamed from: j$.time.format.h */
/* loaded from: classes2.dex */
public final class C0008h implements AbstractC0007g {
    private final TemporalField a;
    private final int b;
    private final int c;
    private final boolean d;

    public C0008h(ChronoField chronoField, int i, int i2, boolean z) {
        if (chronoField == null) {
            throw new NullPointerException("field");
        } else if (!chronoField.e().f()) {
            throw new IllegalArgumentException("Field must have a fixed set of values: " + chronoField);
        } else if (i < 0 || i > 9) {
            throw new IllegalArgumentException("Minimum width must be from 0 to 9 inclusive but was " + i);
        } else if (i2 < 1 || i2 > 9) {
            throw new IllegalArgumentException("Maximum width must be from 1 to 9 inclusive but was " + i2);
        } else if (i2 >= i) {
            this.a = chronoField;
            this.b = i;
            this.c = i2;
            this.d = z;
        } else {
            throw new IllegalArgumentException("Maximum width must exceed or equal the minimum width but " + i2 + " < " + i);
        }
    }

    @Override // j$.time.format.AbstractC0007g
    public final boolean a(B b, StringBuilder sb) {
        Long e = b.e(this.a);
        if (e == null) {
            return false;
        }
        E b2 = b.b();
        long longValue = e.longValue();
        q e2 = this.a.e();
        e2.b(longValue, this.a);
        BigDecimal valueOf = BigDecimal.valueOf(e2.e());
        BigDecimal divide = BigDecimal.valueOf(longValue).subtract(valueOf).divide(BigDecimal.valueOf(e2.d()).subtract(valueOf).add(BigDecimal.ONE), 9, RoundingMode.FLOOR);
        BigDecimal stripTrailingZeros = divide.compareTo(BigDecimal.ZERO) == 0 ? BigDecimal.ZERO : divide.stripTrailingZeros();
        if (stripTrailingZeros.scale() != 0) {
            String substring = stripTrailingZeros.setScale(Math.min(Math.max(stripTrailingZeros.scale(), this.b), this.c), RoundingMode.FLOOR).toPlainString().substring(2);
            b2.getClass();
            if (this.d) {
                sb.append('.');
            }
            sb.append(substring);
            return true;
        } else if (this.b <= 0) {
            return true;
        } else {
            if (this.d) {
                b2.getClass();
                sb.append('.');
            }
            for (int i = 0; i < this.b; i++) {
                b2.getClass();
                sb.append('0');
            }
            return true;
        }
    }

    @Override // j$.time.format.AbstractC0007g
    public final int c(y yVar, CharSequence charSequence, int i) {
        int i2;
        int i3 = yVar.l() ? this.b : 0;
        int i4 = yVar.l() ? this.c : 9;
        int length = charSequence.length();
        if (i == length) {
            return i3 > 0 ? i ^ -1 : i;
        }
        if (this.d) {
            char charAt = charSequence.charAt(i);
            yVar.g().getClass();
            if (charAt != '.') {
                return i3 > 0 ? i ^ -1 : i;
            }
            i++;
        }
        int i5 = i3 + i;
        if (i5 > length) {
            return i ^ -1;
        }
        int min = Math.min(i4 + i, length);
        int i6 = i;
        int i7 = 0;
        while (true) {
            if (i6 >= min) {
                i2 = i6;
                break;
            }
            int i8 = i6 + 1;
            int a = yVar.g().a(charSequence.charAt(i6));
            if (a >= 0) {
                i7 = (i7 * 10) + a;
                i6 = i8;
            } else if (i8 < i5) {
                return i ^ -1;
            } else {
                i2 = i8 - 1;
            }
        }
        BigDecimal movePointLeft = new BigDecimal(i7).movePointLeft(i2 - i);
        q e = this.a.e();
        BigDecimal valueOf = BigDecimal.valueOf(e.e());
        return yVar.o(this.a, movePointLeft.multiply(BigDecimal.valueOf(e.d()).subtract(valueOf).add(BigDecimal.ONE)).setScale(0, RoundingMode.FLOOR).add(valueOf).longValueExact(), i, i2);
    }

    public final String toString() {
        String str = this.d ? ",DecimalPoint" : "";
        StringBuilder a = b.a("Fraction(");
        a.append(this.a);
        a.append(",");
        a.append(this.b);
        a.append(",");
        a.append(this.c);
        a.append(str);
        a.append(")");
        return a.toString();
    }
}
