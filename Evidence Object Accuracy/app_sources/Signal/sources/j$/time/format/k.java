package j$.time.format;

import j$.time.b;
import j$.time.chrono.c;
import j$.time.chrono.f;
import j$.util.concurrent.ConcurrentHashMap;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

/* loaded from: classes2.dex */
public final class k implements AbstractC0007g {
    private static final ConcurrentHashMap c = new ConcurrentHashMap(16, 0.75f, 2);
    private final FormatStyle a = null;
    private final FormatStyle b;

    public k(FormatStyle formatStyle) {
        this.b = formatStyle;
    }

    private DateTimeFormatter b(Locale locale, f fVar) {
        StringBuilder sb = new StringBuilder();
        fVar.getClass();
        sb.append("ISO");
        sb.append('|');
        sb.append(locale.toString());
        sb.append('|');
        sb.append(this.a);
        sb.append(this.b);
        String sb2 = sb.toString();
        DateTimeFormatter dateTimeFormatter = (DateTimeFormatter) c.get(sb2);
        if (dateTimeFormatter != null) {
            return dateTimeFormatter;
        }
        FormatStyle formatStyle = this.a;
        FormatStyle formatStyle2 = this.b;
        if (formatStyle == null && formatStyle2 == null) {
            throw new IllegalArgumentException("Either dateStyle or timeStyle must be non-null");
        }
        DateFormat dateInstance = formatStyle2 == null ? DateFormat.getDateInstance(formatStyle.ordinal(), locale) : formatStyle == null ? DateFormat.getTimeInstance(formatStyle2.ordinal(), locale) : DateFormat.getDateTimeInstance(formatStyle.ordinal(), formatStyle2.ordinal(), locale);
        if (dateInstance instanceof SimpleDateFormat) {
            String pattern = ((SimpleDateFormat) dateInstance).toPattern();
            if (pattern == null) {
                pattern = null;
            } else {
                int i = 0;
                boolean z = true;
                boolean z2 = pattern.indexOf(66) != -1;
                if (pattern.indexOf(98) == -1) {
                    z = false;
                }
                if (z2 || z) {
                    StringBuilder sb3 = new StringBuilder(pattern.length());
                    char c2 = ' ';
                    while (i < pattern.length()) {
                        char charAt = pattern.charAt(i);
                        if (charAt == ' ' ? i == 0 || !(c2 == 'B' || c2 == 'b') : !(charAt == 'B' || charAt == 'b')) {
                            sb3.append(charAt);
                        }
                        i++;
                        c2 = charAt;
                    }
                    int length = sb3.length() - 1;
                    if (length >= 0 && sb3.charAt(length) == ' ') {
                        sb3.deleteCharAt(length);
                    }
                    pattern = sb3.toString();
                }
            }
            x xVar = new x();
            xVar.j(pattern);
            DateTimeFormatter w = xVar.w(locale);
            DateTimeFormatter dateTimeFormatter2 = (DateTimeFormatter) c.putIfAbsent(sb2, w);
            return dateTimeFormatter2 != null ? dateTimeFormatter2 : w;
        }
        throw new UnsupportedOperationException("Can't determine pattern from " + dateInstance);
    }

    @Override // j$.time.format.AbstractC0007g
    public final boolean a(B b, StringBuilder sb) {
        b(b.c(), c.b(b.d())).f().a(b, sb);
        return true;
    }

    @Override // j$.time.format.AbstractC0007g
    public final int c(y yVar, CharSequence charSequence, int i) {
        return b(yVar.i(), yVar.h()).f().c(yVar, charSequence, i);
    }

    public final String toString() {
        StringBuilder a = b.a("Localized(");
        Object obj = this.a;
        Object obj2 = "";
        if (obj == null) {
            obj = obj2;
        }
        a.append(obj);
        a.append(",");
        FormatStyle formatStyle = this.b;
        if (formatStyle != null) {
            obj2 = formatStyle;
        }
        a.append(obj2);
        a.append(")");
        return a.toString();
    }
}
