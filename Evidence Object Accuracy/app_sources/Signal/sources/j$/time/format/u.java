package j$.time.format;

import j$.time.temporal.TemporalField;
import j$.time.temporal.WeekFields;
import java.util.Locale;

/* loaded from: classes2.dex */
public final class u implements AbstractC0007g {
    private char a;
    private int b;

    public u(char c, int i) {
        this.a = c;
        this.b = i;
    }

    private l b(Locale locale) {
        TemporalField temporalField;
        WeekFields of = WeekFields.of(locale);
        char c = this.a;
        int i = 1;
        if (c == 'W') {
            temporalField = of.h();
        } else if (c == 'Y') {
            TemporalField g = of.g();
            int i2 = this.b;
            if (i2 == 2) {
                return new r(g, r.i, 0);
            }
            return new l(g, i2, 19, i2 < 4 ? 1 : 5, -1);
        } else if (c == 'c' || c == 'e') {
            temporalField = of.d();
        } else if (c == 'w') {
            temporalField = of.i();
        } else {
            throw new IllegalStateException("unreachable");
        }
        if (this.b == 2) {
            i = 2;
        }
        return new l(temporalField, i, 2, 4);
    }

    @Override // j$.time.format.AbstractC0007g
    public final boolean a(B b, StringBuilder sb) {
        return b(b.c()).a(b, sb);
    }

    @Override // j$.time.format.AbstractC0007g
    public final int c(y yVar, CharSequence charSequence, int i) {
        return b(yVar.i()).c(yVar, charSequence, i);
    }

    public final String toString() {
        String str;
        String b;
        StringBuilder sb = new StringBuilder(30);
        sb.append("Localized(");
        char c = this.a;
        if (c == 'Y') {
            int i = this.b;
            int i2 = 1;
            if (i == 1) {
                b = "WeekBasedYear";
            } else if (i == 2) {
                b = "ReducedValue(WeekBasedYear,2,2,2000-01-01)";
            } else {
                sb.append("WeekBasedYear,");
                sb.append(this.b);
                sb.append(",");
                sb.append(19);
                sb.append(",");
                if (this.b >= 4) {
                    i2 = 5;
                }
                b = H.b(i2);
            }
            sb.append(b);
        } else {
            if (c == 'W') {
                str = "WeekOfMonth";
            } else if (c == 'c' || c == 'e') {
                str = "DayOfWeek";
            } else {
                if (c == 'w') {
                    str = "WeekOfWeekBasedYear";
                }
                sb.append(",");
                sb.append(this.b);
            }
            sb.append(str);
            sb.append(",");
            sb.append(this.b);
        }
        sb.append(")");
        return sb.toString();
    }
}
