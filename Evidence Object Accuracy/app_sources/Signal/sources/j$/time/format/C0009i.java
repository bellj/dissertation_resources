package j$.time.format;

import j$.time.LocalDateTime;
import j$.time.ZoneOffset;
import j$.time.a;
import j$.time.temporal.ChronoField;
import j$.time.temporal.TemporalAccessor;
import java.util.Locale;

/* renamed from: j$.time.format.i */
/* loaded from: classes2.dex */
public final class C0009i implements AbstractC0007g {
    @Override // j$.time.format.AbstractC0007g
    public final boolean a(B b, StringBuilder sb) {
        Long e = b.e(ChronoField.INSTANT_SECONDS);
        TemporalAccessor d = b.d();
        ChronoField chronoField = ChronoField.NANO_OF_SECOND;
        Long valueOf = d.h(chronoField) ? Long.valueOf(b.d().getLong(chronoField)) : null;
        int i = 0;
        if (e == null) {
            return false;
        }
        long longValue = e.longValue();
        int k = chronoField.k(valueOf != null ? valueOf.longValue() : 0);
        if (longValue >= -62167219200L) {
            long j = (longValue - 315569520000L) + 62167219200L;
            long e2 = a.e(j, 315569520000L) + 1;
            LocalDateTime r = LocalDateTime.r(a.c(j, 315569520000L) - 62167219200L, 0, ZoneOffset.f);
            if (e2 > 0) {
                sb.append('+');
                sb.append(e2);
            }
            sb.append(r);
            if (r.n() == 0) {
                sb.append(":00");
            }
        } else {
            long j2 = longValue + 62167219200L;
            long j3 = j2 / 315569520000L;
            long j4 = j2 % 315569520000L;
            LocalDateTime r2 = LocalDateTime.r(j4 - 62167219200L, 0, ZoneOffset.f);
            int length = sb.length();
            sb.append(r2);
            if (r2.n() == 0) {
                sb.append(":00");
            }
            if (j3 < 0) {
                if (r2.getYear() == -10000) {
                    sb.replace(length, length + 2, Long.toString(j3 - 1));
                } else if (j4 == 0) {
                    sb.insert(length, j3);
                } else {
                    sb.insert(length + 1, Math.abs(j3));
                }
            }
        }
        if (k > 0) {
            sb.append('.');
            int i2 = 100000000;
            while (true) {
                if (k <= 0 && i % 3 == 0 && i >= -2) {
                    break;
                }
                int i3 = k / i2;
                sb.append((char) (i3 + 48));
                k -= i3 * i2;
                i2 /= 10;
                i++;
            }
        }
        sb.append('Z');
        return true;
    }

    @Override // j$.time.format.AbstractC0007g
    public final int c(y yVar, CharSequence charSequence, int i) {
        int i2;
        int i3;
        x xVar = new x();
        xVar.a(DateTimeFormatter.h);
        xVar.e('T');
        ChronoField chronoField = ChronoField.HOUR_OF_DAY;
        xVar.n(chronoField, 2);
        xVar.e(':');
        ChronoField chronoField2 = ChronoField.MINUTE_OF_HOUR;
        xVar.n(chronoField2, 2);
        xVar.e(':');
        ChronoField chronoField3 = ChronoField.SECOND_OF_MINUTE;
        xVar.n(chronoField3, 2);
        ChronoField chronoField4 = ChronoField.NANO_OF_SECOND;
        int i4 = 0;
        xVar.b(chronoField4, 0, 9, true);
        xVar.e('Z');
        C0006f f = xVar.w(Locale.getDefault()).f();
        y d = yVar.d();
        int c = f.c(d, charSequence, i);
        if (c < 0) {
            return c;
        }
        long longValue = d.j(ChronoField.YEAR).longValue();
        int intValue = d.j(ChronoField.MONTH_OF_YEAR).intValue();
        int intValue2 = d.j(ChronoField.DAY_OF_MONTH).intValue();
        int intValue3 = d.j(chronoField).intValue();
        int intValue4 = d.j(chronoField2).intValue();
        Long j = d.j(chronoField3);
        Long j2 = d.j(chronoField4);
        int intValue5 = j != null ? j.intValue() : 0;
        int intValue6 = j2 != null ? j2.intValue() : 0;
        if (intValue3 == 24 && intValue4 == 0 && intValue5 == 0 && intValue6 == 0) {
            i2 = intValue5;
            i4 = 1;
            i3 = 0;
        } else if (intValue3 == 23 && intValue4 == 59 && intValue5 == 60) {
            yVar.p();
            i3 = intValue3;
            i2 = 59;
        } else {
            i3 = intValue3;
            i2 = intValue5;
        }
        try {
            return yVar.o(chronoField4, (long) intValue6, i, yVar.o(ChronoField.INSTANT_SECONDS, a.d(longValue / 10000, 315569520000L) + LocalDateTime.p(((int) longValue) % 10000, intValue, intValue2, i3, intValue4, i2).plusDays((long) i4).toEpochSecond(ZoneOffset.f), i, c));
        } catch (RuntimeException unused) {
            return i ^ -1;
        }
    }

    public final String toString() {
        return "Instant()";
    }
}
