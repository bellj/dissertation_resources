package j$.time.format;

import j$.time.b;
import j$.time.e;
import j$.time.temporal.TemporalAccessor;
import j$.time.temporal.TemporalField;
import j$.time.temporal.n;
import java.util.Locale;

/* loaded from: classes2.dex */
public final class B {
    private TemporalAccessor a;
    private DateTimeFormatter b;
    private int c;

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0074  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public B(j$.time.temporal.TemporalAccessor r10, j$.time.format.DateTimeFormatter r11) {
        /*
        // Method dump skipped, instructions count: 274
        */
        throw new UnsupportedOperationException("Method not decompiled: j$.time.format.B.<init>(j$.time.temporal.TemporalAccessor, j$.time.format.DateTimeFormatter):void");
    }

    public final void a() {
        this.c--;
    }

    public final E b() {
        return this.b.b();
    }

    public final Locale c() {
        return this.b.c();
    }

    public final TemporalAccessor d() {
        return this.a;
    }

    public final Long e(TemporalField temporalField) {
        try {
            return Long.valueOf(this.a.getLong(temporalField));
        } catch (e e) {
            if (this.c > 0) {
                return null;
            }
            throw e;
        }
    }

    public final Object f(n nVar) {
        Object k = this.a.k(nVar);
        if (k != null || this.c != 0) {
            return k;
        }
        StringBuilder a = b.a("Unable to extract value: ");
        a.append(this.a.getClass());
        throw new e(a.toString());
    }

    public final void g() {
        this.c++;
    }

    public final String toString() {
        return this.a.toString();
    }
}
