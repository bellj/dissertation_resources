package j$.time.format;

import j$.time.b;

/* renamed from: j$.time.format.e */
/* loaded from: classes2.dex */
public final class C0005e implements AbstractC0007g {
    private final char a;

    public C0005e(char c) {
        this.a = c;
    }

    @Override // j$.time.format.AbstractC0007g
    public final boolean a(B b, StringBuilder sb) {
        sb.append(this.a);
        return true;
    }

    @Override // j$.time.format.AbstractC0007g
    public final int c(y yVar, CharSequence charSequence, int i) {
        if (i == charSequence.length()) {
            return i ^ -1;
        }
        char charAt = charSequence.charAt(i);
        return (charAt == this.a || (!yVar.k() && (Character.toUpperCase(charAt) == Character.toUpperCase(this.a) || Character.toLowerCase(charAt) == Character.toLowerCase(this.a)))) ? i + 1 : i ^ -1;
    }

    public final String toString() {
        if (this.a == '\'') {
            return "''";
        }
        StringBuilder a = b.a("'");
        a.append(this.a);
        a.append("'");
        return a.toString();
    }
}
