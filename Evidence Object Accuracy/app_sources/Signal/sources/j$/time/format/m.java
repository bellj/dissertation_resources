package j$.time.format;

import j$.time.b;
import j$.time.temporal.ChronoField;

/* loaded from: classes2.dex */
public final class m implements AbstractC0007g {
    static final String[] c = {"+HH", "+HHmm", "+HH:mm", "+HHMM", "+HH:MM", "+HHMMss", "+HH:MM:ss", "+HHMMSS", "+HH:MM:SS"};
    static final m d = new m("+HH:MM:ss", "Z");
    static final m e = new m("+HH:MM:ss", "0");
    private final String a;
    private final int b;

    public m(String str, String str2) {
        if (str != null) {
            int i = 0;
            while (true) {
                String[] strArr = c;
                if (i >= 9) {
                    throw new IllegalArgumentException("Invalid zone offset pattern: " + str);
                } else if (strArr[i].equals(str)) {
                    this.b = i;
                    this.a = str2;
                    return;
                } else {
                    i++;
                }
            }
        } else {
            throw new NullPointerException("pattern");
        }
    }

    private boolean b(int[] iArr, int i, CharSequence charSequence, boolean z) {
        int i2;
        int i3 = this.b;
        if ((i3 + 3) / 2 < i) {
            return false;
        }
        int i4 = iArr[0];
        if (i3 % 2 == 0 && i > 1) {
            int i5 = i4 + 1;
            if (i5 > charSequence.length() || charSequence.charAt(i4) != ':') {
                return z;
            }
            i4 = i5;
        }
        if (i4 + 2 > charSequence.length()) {
            return z;
        }
        int i6 = i4 + 1;
        char charAt = charSequence.charAt(i4);
        int i7 = i6 + 1;
        char charAt2 = charSequence.charAt(i6);
        if (charAt < '0' || charAt > '9' || charAt2 < '0' || charAt2 > '9' || (i2 = (charAt2 - '0') + ((charAt - '0') * 10)) < 0 || i2 > 59) {
            return z;
        }
        iArr[i] = i2;
        iArr[0] = i7;
        return false;
    }

    @Override // j$.time.format.AbstractC0007g
    public final boolean a(B b, StringBuilder sb) {
        Long e2 = b.e(ChronoField.OFFSET_SECONDS);
        if (e2 == null) {
            return false;
        }
        long longValue = e2.longValue();
        int i = (int) longValue;
        if (longValue == ((long) i)) {
            if (i != 0) {
                int abs = Math.abs((i / 3600) % 100);
                int abs2 = Math.abs((i / 60) % 60);
                int abs3 = Math.abs(i % 60);
                int length = sb.length();
                sb.append(i < 0 ? "-" : "+");
                sb.append((char) ((abs / 10) + 48));
                sb.append((char) ((abs % 10) + 48));
                int i2 = this.b;
                if (i2 >= 3 || (i2 >= 1 && abs2 > 0)) {
                    String str = ":";
                    sb.append(i2 % 2 == 0 ? str : "");
                    sb.append((char) ((abs2 / 10) + 48));
                    sb.append((char) ((abs2 % 10) + 48));
                    abs += abs2;
                    int i3 = this.b;
                    if (i3 >= 7 || (i3 >= 5 && abs3 > 0)) {
                        if (i3 % 2 != 0) {
                            str = "";
                        }
                        sb.append(str);
                        sb.append((char) ((abs3 / 10) + 48));
                        sb.append((char) ((abs3 % 10) + 48));
                        abs += abs3;
                    }
                }
                if (abs == 0) {
                    sb.setLength(length);
                }
                return true;
            }
            sb.append(this.a);
            return true;
        }
        throw new ArithmeticException();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002f, code lost:
        if (r15.s(r16, r17, r14.a, 0, r9) != false) goto L_0x008c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x006d  */
    @Override // j$.time.format.AbstractC0007g
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int c(j$.time.format.y r15, java.lang.CharSequence r16, int r17) {
        /*
            r14 = this;
            r0 = r14
            r7 = r16
            r8 = r17
            int r1 = r16.length()
            java.lang.String r2 = r0.a
            int r9 = r2.length()
            r10 = 0
            r12 = -1
            if (r9 != 0) goto L_0x001d
            if (r8 != r1) goto L_0x0032
            j$.time.temporal.ChronoField r1 = j$.time.temporal.ChronoField.OFFSET_SECONDS
            r2 = r1
            r6 = r8
            r3 = r10
            goto L_0x0093
        L_0x001d:
            if (r8 != r1) goto L_0x0022
            r1 = r8 ^ -1
            return r1
        L_0x0022:
            java.lang.String r4 = r0.a
            r5 = 0
            r1 = r15
            r2 = r16
            r3 = r17
            r6 = r9
            boolean r1 = r1.s(r2, r3, r4, r5, r6)
            if (r1 == 0) goto L_0x0032
            goto L_0x008c
        L_0x0032:
            char r1 = r16.charAt(r17)
            r2 = 43
            r3 = 45
            if (r1 == r2) goto L_0x003e
            if (r1 != r3) goto L_0x008a
        L_0x003e:
            r2 = 1
            if (r1 != r3) goto L_0x0043
            r1 = -1
            goto L_0x0044
        L_0x0043:
            r1 = 1
        L_0x0044:
            r3 = 4
            int[] r3 = new int[r3]
            int r4 = r8 + 1
            r5 = 0
            r3[r5] = r4
            boolean r4 = r14.b(r3, r2, r7, r2)
            r6 = 2
            r13 = 3
            if (r4 != 0) goto L_0x006a
            int r4 = r0.b
            if (r4 < r13) goto L_0x005a
            r4 = 1
            goto L_0x005b
        L_0x005a:
            r4 = 0
        L_0x005b:
            boolean r4 = r14.b(r3, r6, r7, r4)
            if (r4 != 0) goto L_0x006a
            boolean r4 = r14.b(r3, r13, r7, r5)
            if (r4 == 0) goto L_0x0068
            goto L_0x006a
        L_0x0068:
            r4 = 0
            goto L_0x006b
        L_0x006a:
            r4 = 1
        L_0x006b:
            if (r4 != 0) goto L_0x008a
            long r9 = (long) r1
            r1 = r3[r2]
            long r1 = (long) r1
            r11 = 3600(0xe10, double:1.7786E-320)
            long r1 = r1 * r11
            r4 = r3[r6]
            long r6 = (long) r4
            r11 = 60
            long r6 = r6 * r11
            long r6 = r6 + r1
            r1 = r3[r13]
            long r1 = (long) r1
            long r6 = r6 + r1
            long r6 = r6 * r9
            j$.time.temporal.ChronoField r1 = j$.time.temporal.ChronoField.OFFSET_SECONDS
            r2 = r3[r5]
            r3 = r6
            r6 = r2
            goto L_0x0092
        L_0x008a:
            if (r9 != 0) goto L_0x009b
        L_0x008c:
            j$.time.temporal.ChronoField r1 = j$.time.temporal.ChronoField.OFFSET_SECONDS
            int r2 = r8 + r9
            r6 = r2
            r3 = r10
        L_0x0092:
            r2 = r1
        L_0x0093:
            r1 = r15
            r5 = r17
            int r1 = r1.o(r2, r3, r5, r6)
            return r1
        L_0x009b:
            r1 = r8 ^ -1
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: j$.time.format.m.c(j$.time.format.y, java.lang.CharSequence, int):int");
    }

    public final String toString() {
        String replace = this.a.replace("'", "''");
        StringBuilder a = b.a("Offset(");
        a.append(c[this.b]);
        a.append(",'");
        a.append(replace);
        a.append("')");
        return a.toString();
    }
}
