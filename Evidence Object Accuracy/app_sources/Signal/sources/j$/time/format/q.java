package j$.time.format;

import j$.time.chrono.f;
import j$.util.function.Consumer;

/* loaded from: classes2.dex */
public final /* synthetic */ class q implements Consumer {
    public final /* synthetic */ r a;
    public final /* synthetic */ y b;
    public final /* synthetic */ long c;
    public final /* synthetic */ int d;
    public final /* synthetic */ int e;

    public /* synthetic */ q(r rVar, y yVar, long j, int i, int i2) {
        this.a = rVar;
        this.b = yVar;
        this.c = j;
        this.d = i;
        this.e = i2;
    }

    @Override // j$.util.function.Consumer
    public final void accept(Object obj) {
        f fVar = (f) obj;
        this.a.f(this.b, this.c, this.d, this.e);
    }

    @Override // j$.util.function.Consumer
    public final /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }
}
