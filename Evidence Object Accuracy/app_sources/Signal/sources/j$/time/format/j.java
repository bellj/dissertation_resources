package j$.time.format;

import j$.time.b;
import j$.time.temporal.ChronoField;
import j$.time.temporal.TemporalField;

/* loaded from: classes2.dex */
public final class j implements AbstractC0007g {
    public final /* synthetic */ int a;
    private final Object b;

    public /* synthetic */ j(int i, Object obj) {
        this.a = i;
        this.b = obj;
    }

    private static void b(StringBuilder sb, int i) {
        sb.append((char) ((i / 10) + 48));
        sb.append((char) ((i % 10) + 48));
    }

    static int d(CharSequence charSequence, int i) {
        char charAt = charSequence.charAt(i);
        if (charAt < '0' || charAt > '9') {
            return -1;
        }
        return charAt - '0';
    }

    @Override // j$.time.format.AbstractC0007g
    public final boolean a(B b, StringBuilder sb) {
        switch (this.a) {
            case 0:
                Long e = b.e(ChronoField.OFFSET_SECONDS);
                if (e == null) {
                    return false;
                }
                sb.append("GMT");
                long longValue = e.longValue();
                int i = (int) longValue;
                if (longValue != ((long) i)) {
                    throw new ArithmeticException();
                } else if (i == 0) {
                    return true;
                } else {
                    int abs = Math.abs((i / 3600) % 100);
                    int abs2 = Math.abs((i / 60) % 60);
                    int abs3 = Math.abs(i % 60);
                    sb.append(i < 0 ? "-" : "+");
                    if (((TextStyle) this.b) == TextStyle.FULL) {
                        b(sb, abs);
                        sb.append(':');
                        b(sb, abs2);
                        if (abs3 == 0) {
                            return true;
                        }
                    } else {
                        if (abs >= 10) {
                            sb.append((char) ((abs / 10) + 48));
                        }
                        sb.append((char) ((abs % 10) + 48));
                        if (abs2 == 0 && abs3 == 0) {
                            return true;
                        }
                        sb.append(':');
                        b(sb, abs2);
                        if (abs3 == 0) {
                            return true;
                        }
                    }
                    sb.append(':');
                    b(sb, abs3);
                    return true;
                }
            default:
                sb.append((String) this.b);
                return true;
        }
    }

    @Override // j$.time.format.AbstractC0007g
    public final int c(y yVar, CharSequence charSequence, int i) {
        int i2;
        long j;
        TemporalField temporalField;
        int i3;
        int i4;
        switch (this.a) {
            case 0:
                int length = charSequence.length() + i;
                if (yVar.s(charSequence, i, "GMT", 0, 3)) {
                    int i5 = i + 3;
                    if (i5 != length) {
                        char charAt = charSequence.charAt(i5);
                        if (charAt == '+') {
                            i3 = 1;
                        } else if (charAt == '-') {
                            i3 = -1;
                        }
                        int i6 = i5 + 1;
                        int i7 = 0;
                        if (((TextStyle) this.b) == TextStyle.FULL) {
                            int i8 = i6 + 1;
                            int d = d(charSequence, i6);
                            int i9 = i8 + 1;
                            int d2 = d(charSequence, i8);
                            if (d >= 0 && d2 >= 0) {
                                int i10 = i9 + 1;
                                if (charSequence.charAt(i9) == ':') {
                                    i4 = (d * 10) + d2;
                                    int i11 = i10 + 1;
                                    int d3 = d(charSequence, i10);
                                    i2 = i11 + 1;
                                    int d4 = d(charSequence, i11);
                                    if (d3 >= 0 && d4 >= 0) {
                                        i7 = (d3 * 10) + d4;
                                        int i12 = i2 + 2;
                                        if (i12 < length && charSequence.charAt(i2) == ':') {
                                            int d5 = d(charSequence, i2 + 1);
                                            int d6 = d(charSequence, i12);
                                            if (d5 >= 0 && d6 >= 0) {
                                                i7 = (d5 * 10) + d6;
                                                i2 += 3;
                                            }
                                        }
                                        j = ((((long) i7) * 60) + (((long) i4) * 3600) + ((long) i7)) * ((long) i3);
                                        temporalField = ChronoField.OFFSET_SECONDS;
                                    }
                                }
                            }
                        } else {
                            i2 = i6 + 1;
                            i4 = d(charSequence, i6);
                            if (i4 >= 0) {
                                if (i2 < length) {
                                    int d7 = d(charSequence, i2);
                                    if (d7 >= 0) {
                                        i4 = (i4 * 10) + d7;
                                        i2++;
                                    }
                                    int i13 = i2 + 2;
                                    if (i13 < length && charSequence.charAt(i2) == ':' && i13 < length && charSequence.charAt(i2) == ':') {
                                        int d8 = d(charSequence, i2 + 1);
                                        int d9 = d(charSequence, i13);
                                        if (d8 >= 0 && d9 >= 0) {
                                            i7 = (d8 * 10) + d9;
                                            i2 += 3;
                                            int i14 = i2 + 2;
                                            if (i14 < length && charSequence.charAt(i2) == ':') {
                                                int d10 = d(charSequence, i2 + 1);
                                                int d11 = d(charSequence, i14);
                                                if (d10 >= 0 && d11 >= 0) {
                                                    i7 = (d10 * 10) + d11;
                                                    i2 += 3;
                                                }
                                            }
                                            j = ((((long) i7) * 60) + (((long) i4) * 3600) + ((long) i7)) * ((long) i3);
                                            temporalField = ChronoField.OFFSET_SECONDS;
                                        }
                                    }
                                }
                                i7 = 0;
                                j = ((((long) i7) * 60) + (((long) i4) * 3600) + ((long) i7)) * ((long) i3);
                                temporalField = ChronoField.OFFSET_SECONDS;
                            }
                        }
                        return yVar.o(temporalField, j, i, i2);
                    }
                    temporalField = ChronoField.OFFSET_SECONDS;
                    j = 0;
                    i2 = i5;
                    return yVar.o(temporalField, j, i, i2);
                }
                return i ^ -1;
            default:
                if (i > charSequence.length() || i < 0) {
                    throw new IndexOutOfBoundsException();
                }
                String str = (String) this.b;
                return !yVar.s(charSequence, i, str, 0, str.length()) ? i ^ -1 : ((String) this.b).length() + i;
        }
    }

    public final String toString() {
        switch (this.a) {
            case 0:
                StringBuilder a = b.a("LocalizedOffset(");
                a.append((TextStyle) this.b);
                a.append(")");
                return a.toString();
            default:
                String replace = ((String) this.b).replace("'", "''");
                return "'" + replace + "'";
        }
    }
}
