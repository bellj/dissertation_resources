package j$.time.format;

/* loaded from: classes2.dex */
public enum s implements AbstractC0007g {
    SENSITIVE,
    INSENSITIVE,
    STRICT,
    LENIENT;

    @Override // j$.time.format.AbstractC0007g
    public final boolean a(B b, StringBuilder sb) {
        return true;
    }

    @Override // j$.time.format.AbstractC0007g
    public final int c(y yVar, CharSequence charSequence, int i) {
        int ordinal = ordinal();
        if (ordinal == 0) {
            yVar.m(true);
        } else if (ordinal == 1) {
            yVar.m(false);
        } else if (ordinal == 2) {
            yVar.q(true);
        } else if (ordinal == 3) {
            yVar.q(false);
        }
        return i;
    }

    @Override // java.lang.Enum, java.lang.Object
    public final String toString() {
        int ordinal = ordinal();
        if (ordinal == 0) {
            return "ParseCaseSensitive(true)";
        }
        if (ordinal == 1) {
            return "ParseCaseSensitive(false)";
        }
        if (ordinal == 2) {
            return "ParseStrict(true)";
        }
        if (ordinal == 3) {
            return "ParseStrict(false)";
        }
        throw new IllegalStateException("Unreachable");
    }
}
