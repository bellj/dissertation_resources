package j$.time.format;

import j$.time.temporal.TemporalField;
import java.util.Iterator;
import java.util.Locale;

/* renamed from: j$.time.format.b */
/* loaded from: classes2.dex */
public final class C0002b extends D {
    final /* synthetic */ C d;

    public C0002b(C c) {
        this.d = c;
    }

    @Override // j$.time.format.D
    public final String d(TemporalField temporalField, long j, TextStyle textStyle, Locale locale) {
        return this.d.a(j, textStyle);
    }

    @Override // j$.time.format.D
    public final Iterator e(TemporalField temporalField, TextStyle textStyle, Locale locale) {
        return this.d.b(textStyle);
    }
}
