package j$.time.format;

import java.text.ParsePosition;
import java.util.HashSet;
import java.util.Iterator;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public class p {
    protected String a;
    protected String b;
    protected char c;
    protected p d;
    protected p e;

    private p(String str, String str2, p pVar) {
        this.a = str;
        this.b = str2;
        this.d = pVar;
        this.c = str.length() == 0 ? 65535 : this.a.charAt(0);
    }

    public /* synthetic */ p(String str, String str2, p pVar, int i) {
        this(str, str2, pVar);
    }

    private boolean b(String str, String str2) {
        int i = 0;
        while (i < str.length() && i < this.a.length() && c(str.charAt(i), this.a.charAt(i))) {
            i++;
        }
        if (i != this.a.length()) {
            p e = e(this.a.substring(i), this.b, this.d);
            this.a = str.substring(0, i);
            this.d = e;
            if (i < str.length()) {
                this.d.e = e(str.substring(i), str2, null);
                this.b = null;
            } else {
                this.b = str2;
            }
            return true;
        } else if (i < str.length()) {
            String substring = str.substring(i);
            for (p pVar = this.d; pVar != null; pVar = pVar.e) {
                if (c(pVar.c, substring.charAt(0))) {
                    return pVar.b(substring, str2);
                }
            }
            p e2 = e(substring, str2, null);
            e2.e = this.d;
            this.d = e2;
            return true;
        } else {
            this.b = str2;
            return true;
        }
    }

    public static p f(y yVar) {
        return yVar.k() ? new p("", null, null) : new o();
    }

    public static p g(HashSet hashSet, y yVar) {
        p f = f(yVar);
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            f.b(str, str);
        }
        return f;
    }

    public final void a(String str, String str2) {
        b(str, str2);
    }

    protected boolean c(char c, char c2) {
        return c == c2;
    }

    public final String d(CharSequence charSequence, ParsePosition parsePosition) {
        int index = parsePosition.getIndex();
        int length = charSequence.length();
        if (!h(charSequence, index, length)) {
            return null;
        }
        int length2 = this.a.length() + index;
        p pVar = this.d;
        if (pVar != null && length2 != length) {
            while (true) {
                if (!c(pVar.c, charSequence.charAt(length2))) {
                    pVar = pVar.e;
                    if (pVar == null) {
                        break;
                    }
                } else {
                    parsePosition.setIndex(length2);
                    String d = pVar.d(charSequence, parsePosition);
                    if (d != null) {
                        return d;
                    }
                }
            }
        }
        parsePosition.setIndex(length2);
        return this.b;
    }

    protected p e(String str, String str2, p pVar) {
        return new p(str, str2, pVar);
    }

    protected boolean h(CharSequence charSequence, int i, int i2) {
        if (charSequence instanceof String) {
            return ((String) charSequence).startsWith(this.a, i);
        }
        int length = this.a.length();
        if (length > i2 - i) {
            return false;
        }
        int i3 = 0;
        while (true) {
            length--;
            if (length <= 0) {
                return true;
            }
            i3++;
            i++;
            if (!c(this.a.charAt(i3), charSequence.charAt(i))) {
                return false;
            }
        }
    }
}
