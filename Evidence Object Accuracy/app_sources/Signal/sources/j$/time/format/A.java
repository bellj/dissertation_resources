package j$.time.format;

import j$.time.ZoneId;
import j$.time.chrono.b;
import j$.time.chrono.f;
import j$.time.h;
import j$.time.temporal.TemporalAccessor;
import j$.time.temporal.TemporalField;
import j$.time.temporal.l;
import j$.time.temporal.n;
import j$.time.temporal.q;

/* access modifiers changed from: package-private */
/* loaded from: classes2.dex */
public final class A implements TemporalAccessor {
    final /* synthetic */ b a;
    final /* synthetic */ TemporalAccessor b;
    final /* synthetic */ f c;
    final /* synthetic */ ZoneId d;

    public A(h hVar, TemporalAccessor temporalAccessor, f fVar, ZoneId zoneId) {
        this.a = hVar;
        this.b = temporalAccessor;
        this.c = fVar;
        this.d = zoneId;
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final /* synthetic */ int c(TemporalField temporalField) {
        return l.a(this, temporalField);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final q e(TemporalField temporalField) {
        return (this.a == null || !temporalField.a()) ? this.b.e(temporalField) : ((h) this.a).e(temporalField);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final long getLong(TemporalField temporalField) {
        return (this.a == null || !temporalField.a()) ? this.b.getLong(temporalField) : ((h) this.a).getLong(temporalField);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final boolean h(TemporalField temporalField) {
        return (this.a == null || !temporalField.a()) ? this.b.h(temporalField) : ((h) this.a).h(temporalField);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final Object k(n nVar) {
        return nVar == l.d() ? this.c : nVar == l.k() ? this.d : nVar == l.i() ? this.b.k(nVar) : nVar.a(this);
    }
}
