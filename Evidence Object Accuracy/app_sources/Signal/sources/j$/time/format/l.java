package j$.time.format;

import j$.time.b;
import j$.time.e;
import j$.time.temporal.TemporalField;

/* loaded from: classes2.dex */
public class l implements AbstractC0007g {
    static final long[] f = {0, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000, 10000000000L};
    final TemporalField a;
    final int b;
    final int c;
    private final int d;
    final int e;

    public l(TemporalField temporalField, int i, int i2, int i3) {
        this.a = temporalField;
        this.b = i;
        this.c = i2;
        this.d = i3;
        this.e = 0;
    }

    public l(TemporalField temporalField, int i, int i2, int i3, int i4) {
        this.a = temporalField;
        this.b = i;
        this.c = i2;
        this.d = i3;
        this.e = i4;
    }

    @Override // j$.time.format.AbstractC0007g
    public final boolean a(B b, StringBuilder sb) {
        int i;
        Long e = b.e(this.a);
        if (e == null) {
            return false;
        }
        long d = d(b, e.longValue());
        E b2 = b.b();
        String l = d == Long.MIN_VALUE ? "9223372036854775808" : Long.toString(Math.abs(d));
        if (l.length() <= this.c) {
            b2.getClass();
            int i2 = (d > 0 ? 1 : (d == 0 ? 0 : -1));
            int[] iArr = AbstractC0004d.a;
            int a = H.a(this.d);
            if (i2 >= 0) {
                int i3 = iArr[a];
                if (i3 == 1 ? !((i = this.b) >= 19 || d < f[i]) : i3 == 2) {
                    sb.append('+');
                }
            } else {
                int i4 = iArr[a];
                if (i4 == 1 || i4 == 2 || i4 == 3) {
                    sb.append('-');
                } else if (i4 == 4) {
                    StringBuilder a2 = b.a("Field ");
                    a2.append(this.a);
                    a2.append(" cannot be printed as the value ");
                    a2.append(d);
                    a2.append(" cannot be negative according to the SignStyle");
                    throw new e(a2.toString());
                }
            }
            for (int i5 = 0; i5 < this.b - l.length(); i5++) {
                sb.append('0');
            }
            sb.append(l);
            return true;
        }
        StringBuilder a3 = b.a("Field ");
        a3.append(this.a);
        a3.append(" cannot be printed as the value ");
        a3.append(d);
        a3.append(" exceeds the maximum print width of ");
        a3.append(this.c);
        throw new e(a3.toString());
    }

    /* JADX WARNING: Removed duplicated region for block: B:125:0x0180  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x019d  */
    @Override // j$.time.format.AbstractC0007g
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int c(j$.time.format.y r20, java.lang.CharSequence r21, int r22) {
        /*
        // Method dump skipped, instructions count: 422
        */
        throw new UnsupportedOperationException("Method not decompiled: j$.time.format.l.c(j$.time.format.y, java.lang.CharSequence, int):int");
    }

    long d(B b, long j) {
        return j;
    }

    public boolean e(y yVar) {
        int i = this.e;
        return i == -1 || (i > 0 && this.b == this.c && this.d == 4);
    }

    int f(y yVar, long j, int i, int i2) {
        return yVar.o(this.a, j, i, i2);
    }

    public l g() {
        return this.e == -1 ? this : new l(this.a, this.b, this.c, this.d, -1);
    }

    public l h(int i) {
        return new l(this.a, this.b, this.c, this.d, this.e + i);
    }

    public String toString() {
        StringBuilder sb;
        int i = this.b;
        if (i == 1 && this.c == 19 && this.d == 1) {
            sb = b.a("Value(");
            sb.append(this.a);
        } else if (i == this.c && this.d == 4) {
            sb = b.a("Value(");
            sb.append(this.a);
            sb.append(",");
            sb.append(this.b);
        } else {
            sb = b.a("Value(");
            sb.append(this.a);
            sb.append(",");
            sb.append(this.b);
            sb.append(",");
            sb.append(this.c);
            sb.append(",");
            sb.append(H.b(this.d));
        }
        sb.append(")");
        return sb.toString();
    }
}
