package j$.time.format;

import j$.time.Instant;
import j$.time.ZoneId;
import j$.time.ZoneOffset;
import j$.time.temporal.ChronoField;
import j$.time.temporal.TemporalAccessor;
import j$.time.temporal.l;
import j$.time.zone.g;
import j$.util.concurrent.ConcurrentHashMap;
import java.lang.ref.SoftReference;
import java.text.DateFormatSymbols;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

/* loaded from: classes2.dex */
public final class w extends v {
    private static final ConcurrentHashMap h = new ConcurrentHashMap();
    private final TextStyle e;
    private final HashMap f = new HashMap();
    private final HashMap g = new HashMap();

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public w(j$.time.format.TextStyle r4) {
        /*
            r3 = this;
            j$.time.temporal.m r0 = j$.time.temporal.l.j()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "ZoneText("
            r1.append(r2)
            r1.append(r4)
            java.lang.String r2 = ")"
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r3.<init>(r0, r1)
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r3.f = r0
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r3.g = r0
            if (r4 == 0) goto L_0x0030
            r3.e = r4
            return
        L_0x0030:
            java.lang.NullPointerException r4 = new java.lang.NullPointerException
            java.lang.String r0 = "textStyle"
            r4.<init>(r0)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: j$.time.format.w.<init>(j$.time.format.TextStyle):void");
    }

    @Override // j$.time.format.v, j$.time.format.AbstractC0007g
    public final boolean a(B b, StringBuilder sb) {
        String[] strArr;
        ZoneId zoneId = (ZoneId) b.f(l.k());
        if (zoneId == null) {
            return false;
        }
        String l = zoneId.l();
        if (!(zoneId instanceof ZoneOffset)) {
            TemporalAccessor d = b.d();
            char c = d.h(ChronoField.INSTANT_SECONDS) ? zoneId.m().h(Instant.from(d)) ? (char) 1 : 0 : 2;
            Locale c2 = b.c();
            String str = null;
            Map map = null;
            if (this.e != TextStyle.NARROW) {
                ConcurrentHashMap concurrentHashMap = h;
                SoftReference softReference = (SoftReference) concurrentHashMap.get(l);
                if (softReference == null || (map = (Map) softReference.get()) == null || (strArr = (String[]) map.get(c2)) == null) {
                    TimeZone timeZone = TimeZone.getTimeZone(l);
                    String[] strArr2 = {l, timeZone.getDisplayName(false, 1, c2), timeZone.getDisplayName(false, 0, c2), timeZone.getDisplayName(true, 1, c2), timeZone.getDisplayName(true, 0, c2), l, l};
                    if (map == null) {
                        map = new ConcurrentHashMap();
                    }
                    map.put(c2, strArr2);
                    concurrentHashMap.put(l, new SoftReference(map));
                    strArr = strArr2;
                }
                int a = this.e.a();
                str = c != 0 ? c != 1 ? strArr[a + 5] : strArr[a + 3] : strArr[a + 1];
            }
            if (str != null) {
                l = str;
            }
        }
        sb.append(l);
        return true;
    }

    @Override // j$.time.format.v
    public final p b(y yVar) {
        p pVar;
        if (this.e == TextStyle.NARROW) {
            return super.b(yVar);
        }
        Locale i = yVar.i();
        boolean k = yVar.k();
        HashSet a = g.a();
        int size = a.size();
        HashMap hashMap = k ? this.f : this.g;
        Map.Entry entry = (Map.Entry) hashMap.get(i);
        if (entry == null || ((Integer) entry.getKey()).intValue() != size || (pVar = (p) ((SoftReference) entry.getValue()).get()) == null) {
            pVar = p.f(yVar);
            String[][] zoneStrings = DateFormatSymbols.getInstance(i).getZoneStrings();
            int length = zoneStrings.length;
            int i2 = 0;
            while (true) {
                int i3 = 2;
                if (i2 >= length) {
                    break;
                }
                String[] strArr = zoneStrings[i2];
                String str = strArr[0];
                if (a.contains(str)) {
                    pVar.a(str, str);
                    String a2 = I.a(str, i);
                    if (this.e == TextStyle.FULL) {
                        i3 = 1;
                    }
                    while (i3 < strArr.length) {
                        pVar.a(strArr[i3], a2);
                        i3 += 2;
                    }
                }
                i2++;
            }
            hashMap.put(i, new AbstractMap.SimpleImmutableEntry(Integer.valueOf(size), new SoftReference(pVar)));
        }
        return pVar;
    }
}
