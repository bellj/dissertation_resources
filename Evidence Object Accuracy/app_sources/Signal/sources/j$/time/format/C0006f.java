package j$.time.format;

import java.util.ArrayList;

/* renamed from: j$.time.format.f */
/* loaded from: classes2.dex */
public final class C0006f implements AbstractC0007g {
    private final AbstractC0007g[] a;
    private final boolean b;

    public C0006f(ArrayList arrayList, boolean z) {
        this((AbstractC0007g[]) arrayList.toArray(new AbstractC0007g[arrayList.size()]), z);
    }

    C0006f(AbstractC0007g[] gVarArr, boolean z) {
        this.a = gVarArr;
        this.b = z;
    }

    @Override // j$.time.format.AbstractC0007g
    public final boolean a(B b, StringBuilder sb) {
        int length = sb.length();
        if (this.b) {
            b.g();
        }
        try {
            for (AbstractC0007g gVar : this.a) {
                if (!gVar.a(b, sb)) {
                    sb.setLength(length);
                    return true;
                }
            }
            if (this.b) {
                b.a();
            }
            return true;
        } finally {
            if (this.b) {
                b.a();
            }
        }
    }

    public final C0006f b() {
        return !this.b ? this : new C0006f(this.a, false);
    }

    @Override // j$.time.format.AbstractC0007g
    public final int c(y yVar, CharSequence charSequence, int i) {
        if (this.b) {
            yVar.r();
            int i2 = i;
            for (AbstractC0007g gVar : this.a) {
                i2 = gVar.c(yVar, charSequence, i2);
                if (i2 < 0) {
                    yVar.f(false);
                    return i;
                }
            }
            yVar.f(true);
            return i2;
        }
        for (AbstractC0007g gVar2 : this.a) {
            i = gVar2.c(yVar, charSequence, i);
            if (i < 0) {
                break;
            }
        }
        return i;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.a != null) {
            sb.append(this.b ? "[" : "(");
            for (AbstractC0007g gVar : this.a) {
                sb.append(gVar);
            }
            sb.append(this.b ? "]" : ")");
        }
        return sb.toString();
    }
}
