package j$.time.format;

import j$.time.b;
import j$.time.e;

/* loaded from: classes2.dex */
public final class n implements AbstractC0007g {
    private final AbstractC0007g a;
    private final int b;
    private final char c;

    public n(AbstractC0007g gVar, int i, char c) {
        this.a = gVar;
        this.b = i;
        this.c = c;
    }

    @Override // j$.time.format.AbstractC0007g
    public final boolean a(B b, StringBuilder sb) {
        int length = sb.length();
        if (!this.a.a(b, sb)) {
            return false;
        }
        int length2 = sb.length() - length;
        if (length2 <= this.b) {
            for (int i = 0; i < this.b - length2; i++) {
                sb.insert(length, this.c);
            }
            return true;
        }
        throw new e("Cannot print as output of " + length2 + " characters exceeds pad width of " + this.b);
    }

    @Override // j$.time.format.AbstractC0007g
    public final int c(y yVar, CharSequence charSequence, int i) {
        boolean l = yVar.l();
        if (i > charSequence.length()) {
            throw new IndexOutOfBoundsException();
        } else if (i == charSequence.length()) {
            return i ^ -1;
        } else {
            int i2 = this.b + i;
            if (i2 > charSequence.length()) {
                if (l) {
                    return i ^ -1;
                }
                i2 = charSequence.length();
            }
            int i3 = i;
            while (i3 < i2 && yVar.b(charSequence.charAt(i3), this.c)) {
                i3++;
            }
            int c = this.a.c(yVar, charSequence.subSequence(0, i2), i3);
            return (c == i2 || !l) ? c : (i + i3) ^ -1;
        }
    }

    public final String toString() {
        String str;
        StringBuilder a = b.a("Pad(");
        a.append(this.a);
        a.append(",");
        a.append(this.b);
        if (this.c == ' ') {
            str = ")";
        } else {
            StringBuilder a2 = b.a(",'");
            a2.append(this.c);
            a2.append("')");
            str = a2.toString();
        }
        a.append(str);
        return a.toString();
    }
}
