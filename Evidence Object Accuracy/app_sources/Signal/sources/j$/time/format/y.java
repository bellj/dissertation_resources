package j$.time.format;

import j$.time.ZoneId;
import j$.time.chrono.f;
import j$.time.chrono.g;
import j$.time.temporal.ChronoField;
import j$.time.temporal.TemporalAccessor;
import j$.time.temporal.TemporalField;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Set;

/* loaded from: classes2.dex */
public final class y {
    private DateTimeFormatter a;
    private boolean b = true;
    private boolean c = true;
    private final ArrayList d;
    private ArrayList e;

    public y(DateTimeFormatter dateTimeFormatter) {
        ArrayList arrayList = new ArrayList();
        this.d = arrayList;
        this.e = null;
        this.a = dateTimeFormatter;
        arrayList.add(new F());
    }

    public static boolean c(char c, char c2) {
        return c == c2 || Character.toUpperCase(c) == Character.toUpperCase(c2) || Character.toLowerCase(c) == Character.toLowerCase(c2);
    }

    private F e() {
        ArrayList arrayList = this.d;
        return (F) arrayList.get(arrayList.size() - 1);
    }

    public final void a(q qVar) {
        if (this.e == null) {
            this.e = new ArrayList();
        }
        this.e.add(qVar);
    }

    public final boolean b(char c, char c2) {
        return this.b ? c == c2 : c(c, c2);
    }

    public final y d() {
        y yVar = new y(this.a);
        yVar.b = this.b;
        yVar.c = this.c;
        return yVar;
    }

    public final void f(boolean z) {
        ArrayList arrayList;
        int i;
        if (z) {
            arrayList = this.d;
            i = arrayList.size() - 2;
        } else {
            arrayList = this.d;
            i = arrayList.size() - 1;
        }
        arrayList.remove(i);
    }

    public final E g() {
        return this.a.b();
    }

    public final f h() {
        f fVar = e().c;
        if (fVar != null) {
            return fVar;
        }
        f a = this.a.a();
        return a == null ? g.a : a;
    }

    public final Locale i() {
        return this.a.c();
    }

    public final Long j(ChronoField chronoField) {
        return (Long) e().a.get(chronoField);
    }

    public final boolean k() {
        return this.b;
    }

    public final boolean l() {
        return this.c;
    }

    public final void m(boolean z) {
        this.b = z;
    }

    public final void n(ZoneId zoneId) {
        if (zoneId != null) {
            e().b = zoneId;
            return;
        }
        throw new NullPointerException("zone");
    }

    public final int o(TemporalField temporalField, long j, int i, int i2) {
        if (temporalField != null) {
            Long l = (Long) e().a.put(temporalField, Long.valueOf(j));
            return (l == null || l.longValue() == j) ? i2 : i ^ -1;
        }
        throw new NullPointerException("field");
    }

    public final void p() {
        e().getClass();
    }

    public final void q(boolean z) {
        this.c = z;
    }

    public final void r() {
        ArrayList arrayList = this.d;
        F e = e();
        e.getClass();
        F f = new F();
        f.a.putAll(e.a);
        f.b = e.b;
        f.c = e.c;
        arrayList.add(f);
    }

    public final boolean s(CharSequence charSequence, int i, CharSequence charSequence2, int i2, int i3) {
        if (i + i3 > charSequence.length() || i2 + i3 > charSequence2.length()) {
            return false;
        }
        if (this.b) {
            for (int i4 = 0; i4 < i3; i4++) {
                if (charSequence.charAt(i + i4) != charSequence2.charAt(i2 + i4)) {
                    return false;
                }
            }
            return true;
        }
        for (int i5 = 0; i5 < i3; i5++) {
            char charAt = charSequence.charAt(i + i5);
            char charAt2 = charSequence2.charAt(i2 + i5);
            if (!(charAt == charAt2 || Character.toUpperCase(charAt) == Character.toUpperCase(charAt2) || Character.toLowerCase(charAt) == Character.toLowerCase(charAt2))) {
                return false;
            }
        }
        return true;
    }

    public final TemporalAccessor t(G g, Set set) {
        F e = e();
        e.c = h();
        ZoneId zoneId = e.b;
        if (zoneId == null) {
            zoneId = this.a.d();
        }
        e.b = zoneId;
        e.l(g, set);
        return e;
    }

    public final String toString() {
        return e().toString();
    }
}
