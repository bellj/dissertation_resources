package j$.time.format;

import j$.time.chrono.g;
import j$.time.temporal.ChronoField;
import j$.time.temporal.TemporalField;
import j$.time.temporal.i;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;

/* loaded from: classes2.dex */
public final class x {
    private static final C0001a h = new C0001a();
    private static final HashMap i;
    public static final /* synthetic */ int j;
    private x a;
    private final x b;
    private final ArrayList c;
    private final boolean d;
    private int e;
    private char f;
    private int g;

    static {
        h = new C0001a();
        HashMap hashMap = new HashMap();
        i = hashMap;
        hashMap.put('G', ChronoField.ERA);
        hashMap.put('y', ChronoField.YEAR_OF_ERA);
        hashMap.put('u', ChronoField.YEAR);
        TemporalField temporalField = i.a;
        hashMap.put('Q', temporalField);
        hashMap.put('q', temporalField);
        ChronoField chronoField = ChronoField.MONTH_OF_YEAR;
        hashMap.put('M', chronoField);
        hashMap.put('L', chronoField);
        hashMap.put('D', ChronoField.DAY_OF_YEAR);
        hashMap.put('d', ChronoField.DAY_OF_MONTH);
        hashMap.put('F', ChronoField.ALIGNED_DAY_OF_WEEK_IN_MONTH);
        ChronoField chronoField2 = ChronoField.DAY_OF_WEEK;
        hashMap.put('E', chronoField2);
        hashMap.put('c', chronoField2);
        hashMap.put('e', chronoField2);
        hashMap.put('a', ChronoField.AMPM_OF_DAY);
        hashMap.put('H', ChronoField.HOUR_OF_DAY);
        hashMap.put('k', ChronoField.CLOCK_HOUR_OF_DAY);
        hashMap.put('K', ChronoField.HOUR_OF_AMPM);
        hashMap.put('h', ChronoField.CLOCK_HOUR_OF_AMPM);
        hashMap.put('m', ChronoField.MINUTE_OF_HOUR);
        hashMap.put('s', ChronoField.SECOND_OF_MINUTE);
        ChronoField chronoField3 = ChronoField.NANO_OF_SECOND;
        hashMap.put('S', chronoField3);
        hashMap.put('A', ChronoField.MILLI_OF_DAY);
        hashMap.put('n', chronoField3);
        hashMap.put('N', ChronoField.NANO_OF_DAY);
    }

    public x() {
        this.a = this;
        this.c = new ArrayList();
        this.g = -1;
        this.b = null;
        this.d = false;
    }

    private x(x xVar) {
        this.a = this;
        this.c = new ArrayList();
        this.g = -1;
        this.b = xVar;
        this.d = true;
    }

    private int d(AbstractC0007g gVar) {
        if (gVar != null) {
            x xVar = this.a;
            int i2 = xVar.e;
            if (i2 > 0) {
                gVar = new n(gVar, i2, xVar.f);
                xVar.e = 0;
                xVar.f = 0;
            }
            xVar.c.add(gVar);
            x xVar2 = this.a;
            xVar2.g = -1;
            return xVar2.c.size() - 1;
        }
        throw new NullPointerException("pp");
    }

    private void m(l lVar) {
        l lVar2;
        x xVar = this.a;
        int i2 = xVar.g;
        if (i2 >= 0) {
            l lVar3 = (l) xVar.c.get(i2);
            if (lVar.b == lVar.c && lVar.d == 4) {
                lVar2 = lVar3.h(lVar.c);
                d(lVar.g());
                this.a.g = i2;
            } else {
                lVar2 = lVar3.g();
                this.a.g = d(lVar);
            }
            this.a.c.set(i2, lVar2);
            return;
        }
        xVar.g = d(lVar);
    }

    private DateTimeFormatter x(Locale locale, G g, g gVar) {
        if (locale != null) {
            while (this.a.b != null) {
                q();
            }
            C0006f fVar = new C0006f(this.c, false);
            E e = E.a;
            return new DateTimeFormatter(fVar, locale, g, gVar);
        }
        throw new NullPointerException("locale");
    }

    public final void a(DateTimeFormatter dateTimeFormatter) {
        if (dateTimeFormatter != null) {
            d(dateTimeFormatter.f());
            return;
        }
        throw new NullPointerException("formatter");
    }

    public final void b(ChronoField chronoField, int i2, int i3, boolean z) {
        d(new C0008h(chronoField, i2, i3, z));
    }

    public final void c() {
        d(new C0009i());
    }

    public final void e(char c) {
        d(new C0005e(c));
    }

    public final void f(String str) {
        if (str == null) {
            throw new NullPointerException("literal");
        } else if (str.length() > 0) {
            d(str.length() == 1 ? new C0005e(str.charAt(0)) : new j(1, str));
        }
    }

    public final void g(FormatStyle formatStyle) {
        if (formatStyle != null) {
            d(new k(formatStyle));
            return;
        }
        throw new IllegalArgumentException("Either the date or time style must be non-null");
    }

    public final void h(String str, String str2) {
        d(new m(str, str2));
    }

    public final void i() {
        d(m.d);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:66:0x00e8, code lost:
        if (r3 == 1) goto L_0x01f6;
     */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x0299  */
    /* JADX WARNING: Removed duplicated region for block: B:234:0x03cb  */
    /* JADX WARNING: Removed duplicated region for block: B:275:0x02b4 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:290:0x03e4 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void j(java.lang.String r17) {
        /*
        // Method dump skipped, instructions count: 1138
        */
        throw new UnsupportedOperationException("Method not decompiled: j$.time.format.x.j(java.lang.String):void");
    }

    public final void k(ChronoField chronoField, HashMap hashMap) {
        if (chronoField != null) {
            LinkedHashMap linkedHashMap = new LinkedHashMap(hashMap);
            TextStyle textStyle = TextStyle.FULL;
            d(new t(chronoField, textStyle, new C0002b(new C(Collections.singletonMap(textStyle, linkedHashMap)))));
            return;
        }
        throw new NullPointerException("field");
    }

    public final void l(TemporalField temporalField, TextStyle textStyle) {
        if (temporalField == null) {
            throw new NullPointerException("field");
        } else if (textStyle != null) {
            d(new t(temporalField, textStyle, new D()));
        } else {
            throw new NullPointerException("textStyle");
        }
    }

    public final void n(TemporalField temporalField, int i2) {
        if (temporalField == null) {
            throw new NullPointerException("field");
        } else if (i2 < 1 || i2 > 19) {
            throw new IllegalArgumentException("The width must be from 1 to 19 inclusive but was " + i2);
        } else {
            m(new l(temporalField, i2, i2, 4));
        }
    }

    public final x o(TemporalField temporalField, int i2, int i3, int i4) {
        if (i2 == i3 && i4 == 4) {
            n(temporalField, i3);
            return this;
        } else if (temporalField == null) {
            throw new NullPointerException("field");
        } else if (i4 == 0) {
            throw new NullPointerException("signStyle");
        } else if (i2 < 1 || i2 > 19) {
            throw new IllegalArgumentException("The minimum width must be from 1 to 19 inclusive but was " + i2);
        } else if (i3 < 1 || i3 > 19) {
            throw new IllegalArgumentException("The maximum width must be from 1 to 19 inclusive but was " + i3);
        } else if (i3 >= i2) {
            m(new l(temporalField, i2, i3, i4));
            return this;
        } else {
            throw new IllegalArgumentException("The maximum width must exceed or equal the minimum width but " + i3 + " < " + i2);
        }
    }

    public final void p() {
        d(new v(h, "ZoneRegionId()"));
    }

    public final void q() {
        x xVar = this.a;
        if (xVar.b == null) {
            throw new IllegalStateException("Cannot call optionalEnd() as there was no previous call to optionalStart()");
        } else if (xVar.c.size() > 0) {
            x xVar2 = this.a;
            C0006f fVar = new C0006f(xVar2.c, xVar2.d);
            this.a = this.a.b;
            d(fVar);
        } else {
            this.a = this.a.b;
        }
    }

    public final void r() {
        x xVar = this.a;
        xVar.g = -1;
        this.a = new x(xVar);
    }

    public final void s() {
        d(s.INSENSITIVE);
    }

    public final void t() {
        d(s.SENSITIVE);
    }

    public final void u() {
        d(s.LENIENT);
    }

    public final DateTimeFormatter v(G g, g gVar) {
        return x(Locale.getDefault(), g, gVar);
    }

    public final DateTimeFormatter w(Locale locale) {
        return x(locale, G.SMART, null);
    }
}
