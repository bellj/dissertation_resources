package j$.time.format;

import j$.time.chrono.b;
import j$.time.chrono.c;
import j$.time.chrono.f;
import j$.time.chrono.g;
import j$.time.h;
import j$.time.temporal.TemporalField;
import org.thoughtcrime.securesms.service.webrtc.SignalCallManager;

/* loaded from: classes2.dex */
public final class r extends l {
    static final h i = h.u(SignalCallManager.BUSY_TONE_LENGTH, 1, 1);
    private final int g;
    private final b h;

    private r(TemporalField temporalField, int i2, int i3, int i4, b bVar, int i5) {
        super(temporalField, i2, i3, 4, i5);
        this.g = i4;
        this.h = bVar;
    }

    public r(TemporalField temporalField, h hVar) {
        this(temporalField, 2, 2, 0, hVar, 0);
    }

    public /* synthetic */ r(TemporalField temporalField, h hVar, int i2) {
        this(temporalField, 2, 2, 0, hVar, 0);
    }

    @Override // j$.time.format.l
    final long d(B b, long j) {
        long abs = Math.abs(j);
        int i2 = this.g;
        if (this.h != null) {
            f b2 = c.b(b.d());
            b bVar = this.h;
            ((g) b2).getClass();
            i2 = h.n(bVar).c(this.a);
        }
        long j2 = (long) i2;
        if (j >= j2) {
            long j3 = l.f[this.b];
            if (j < j2 + j3) {
                return abs % j3;
            }
        }
        return abs % l.f[this.c];
    }

    @Override // j$.time.format.l
    public final boolean e(y yVar) {
        if (!yVar.l()) {
            return false;
        }
        return super.e(yVar);
    }

    @Override // j$.time.format.l
    public final int f(y yVar, long j, int i2, int i3) {
        int i4 = this.g;
        if (this.h != null) {
            f h = yVar.h();
            b bVar = this.h;
            ((g) h).getClass();
            i4 = h.n(bVar).c(this.a);
            yVar.a(new q(this, yVar, j, i2, i3));
        }
        int i5 = i3 - i2;
        int i6 = this.b;
        if (i5 == i6 && j >= 0) {
            long j2 = l.f[i6];
            long j3 = (long) i4;
            long j4 = j3 - (j3 % j2);
            j = i4 > 0 ? j4 + j : j4 - j;
            if (j < j3) {
                j += j2;
            }
        }
        return yVar.o(this.a, j, i2, i3);
    }

    @Override // j$.time.format.l
    public final l g() {
        return this.e == -1 ? this : new r(this.a, this.b, this.c, this.g, this.h, -1);
    }

    @Override // j$.time.format.l
    public final l h(int i2) {
        return new r(this.a, this.b, this.c, this.g, this.h, this.e + i2);
    }

    @Override // j$.time.format.l
    public final String toString() {
        StringBuilder a = j$.time.b.a("ReducedValue(");
        a.append(this.a);
        a.append(",");
        a.append(this.b);
        a.append(",");
        a.append(this.c);
        a.append(",");
        Object obj = this.h;
        if (obj == null) {
            obj = Integer.valueOf(this.g);
        }
        a.append(obj);
        a.append(")");
        return a.toString();
    }
}
