package j$.time.format;

import j$.time.ZoneId;
import j$.time.ZoneOffset;
import j$.time.temporal.TemporalAccessor;
import j$.time.temporal.l;
import j$.time.temporal.n;

/* renamed from: j$.time.format.a */
/* loaded from: classes2.dex */
public final /* synthetic */ class C0001a implements n {
    @Override // j$.time.temporal.n
    public final Object a(TemporalAccessor temporalAccessor) {
        int i = x.j;
        ZoneId zoneId = (ZoneId) temporalAccessor.k(l.k());
        if (zoneId == null || (zoneId instanceof ZoneOffset)) {
            return null;
        }
        return zoneId;
    }
}
