package j$.time.format;

import j$.time.ZoneId;
import j$.time.chrono.f;
import j$.time.chrono.g;
import j$.time.e;
import j$.time.temporal.ChronoField;
import j$.time.temporal.TemporalAccessor;
import j$.time.temporal.i;
import java.io.IOException;
import java.text.ParsePosition;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.SearchDatabase;

/* loaded from: classes2.dex */
public final class DateTimeFormatter {
    public static final DateTimeFormatter ISO_INSTANT;
    public static final DateTimeFormatter h;
    private final C0006f a;
    private final Locale b;
    private final E c;
    private final G d;
    private final Set e = null;
    private final f f;
    private final ZoneId g;

    static {
        x xVar = new x();
        ChronoField chronoField = ChronoField.YEAR;
        xVar.o(chronoField, 4, 10, 5);
        xVar.e('-');
        ChronoField chronoField2 = ChronoField.MONTH_OF_YEAR;
        xVar.n(chronoField2, 2);
        xVar.e('-');
        ChronoField chronoField3 = ChronoField.DAY_OF_MONTH;
        xVar.n(chronoField3, 2);
        G g = G.STRICT;
        g gVar = g.a;
        DateTimeFormatter v = xVar.v(g, gVar);
        h = v;
        x xVar2 = new x();
        xVar2.s();
        xVar2.a(v);
        xVar2.i();
        xVar2.v(g, gVar);
        x xVar3 = new x();
        xVar3.s();
        xVar3.a(v);
        xVar3.r();
        xVar3.i();
        xVar3.v(g, gVar);
        x xVar4 = new x();
        ChronoField chronoField4 = ChronoField.HOUR_OF_DAY;
        xVar4.n(chronoField4, 2);
        xVar4.e(':');
        ChronoField chronoField5 = ChronoField.MINUTE_OF_HOUR;
        xVar4.n(chronoField5, 2);
        xVar4.r();
        xVar4.e(':');
        ChronoField chronoField6 = ChronoField.SECOND_OF_MINUTE;
        xVar4.n(chronoField6, 2);
        xVar4.r();
        xVar4.b(ChronoField.NANO_OF_SECOND, 0, 9, true);
        DateTimeFormatter v2 = xVar4.v(g, null);
        x xVar5 = new x();
        xVar5.s();
        xVar5.a(v2);
        xVar5.i();
        xVar5.v(g, null);
        x xVar6 = new x();
        xVar6.s();
        xVar6.a(v2);
        xVar6.r();
        xVar6.i();
        xVar6.v(g, null);
        x xVar7 = new x();
        xVar7.s();
        xVar7.a(v);
        xVar7.e('T');
        xVar7.a(v2);
        DateTimeFormatter v3 = xVar7.v(g, gVar);
        x xVar8 = new x();
        xVar8.s();
        xVar8.a(v3);
        xVar8.i();
        DateTimeFormatter v4 = xVar8.v(g, gVar);
        x xVar9 = new x();
        xVar9.a(v4);
        xVar9.r();
        xVar9.e('[');
        xVar9.t();
        xVar9.p();
        xVar9.e(']');
        xVar9.v(g, gVar);
        x xVar10 = new x();
        xVar10.a(v3);
        xVar10.r();
        xVar10.i();
        xVar10.r();
        xVar10.e('[');
        xVar10.t();
        xVar10.p();
        xVar10.e(']');
        xVar10.v(g, gVar);
        x xVar11 = new x();
        xVar11.s();
        xVar11.o(chronoField, 4, 10, 5);
        xVar11.e('-');
        xVar11.n(ChronoField.DAY_OF_YEAR, 3);
        xVar11.r();
        xVar11.i();
        xVar11.v(g, gVar);
        x xVar12 = new x();
        xVar12.s();
        xVar12.o(i.c, 4, 10, 5);
        xVar12.f("-W");
        xVar12.n(i.b, 2);
        xVar12.e('-');
        ChronoField chronoField7 = ChronoField.DAY_OF_WEEK;
        xVar12.n(chronoField7, 1);
        xVar12.r();
        xVar12.i();
        xVar12.v(g, gVar);
        x xVar13 = new x();
        xVar13.s();
        xVar13.c();
        ISO_INSTANT = xVar13.v(g, null);
        x xVar14 = new x();
        xVar14.s();
        xVar14.n(chronoField, 4);
        xVar14.n(chronoField2, 2);
        xVar14.n(chronoField3, 2);
        xVar14.r();
        xVar14.h("+HHMMss", "Z");
        xVar14.v(g, gVar);
        HashMap hashMap = new HashMap();
        hashMap.put(1L, "Mon");
        hashMap.put(2L, "Tue");
        hashMap.put(3L, "Wed");
        hashMap.put(4L, "Thu");
        hashMap.put(5L, "Fri");
        hashMap.put(6L, "Sat");
        hashMap.put(7L, "Sun");
        HashMap hashMap2 = new HashMap();
        hashMap2.put(1L, "Jan");
        hashMap2.put(2L, "Feb");
        hashMap2.put(3L, "Mar");
        hashMap2.put(4L, "Apr");
        hashMap2.put(5L, "May");
        hashMap2.put(6L, "Jun");
        hashMap2.put(7L, "Jul");
        hashMap2.put(8L, "Aug");
        hashMap2.put(9L, "Sep");
        hashMap2.put(10L, "Oct");
        hashMap2.put(11L, "Nov");
        hashMap2.put(12L, "Dec");
        x xVar15 = new x();
        xVar15.s();
        xVar15.u();
        xVar15.r();
        xVar15.k(chronoField7, hashMap);
        xVar15.f(", ");
        xVar15.q();
        xVar15.o(chronoField3, 1, 2, 4);
        xVar15.e(' ');
        xVar15.k(chronoField2, hashMap2);
        xVar15.e(' ');
        xVar15.n(chronoField, 4);
        xVar15.e(' ');
        xVar15.n(chronoField4, 2);
        xVar15.e(':');
        xVar15.n(chronoField5, 2);
        xVar15.r();
        xVar15.e(':');
        xVar15.n(chronoField6, 2);
        xVar15.q();
        xVar15.e(' ');
        xVar15.h("+HHMM", "GMT");
        xVar15.v(G.SMART, gVar);
    }

    public DateTimeFormatter(C0006f fVar, Locale locale, G g, g gVar) {
        E e = E.a;
        this.a = fVar;
        if (locale != null) {
            this.b = locale;
            this.c = e;
            if (g != null) {
                this.d = g;
                this.f = gVar;
                this.g = null;
                return;
            }
            throw new NullPointerException("resolverStyle");
        }
        throw new NullPointerException("locale");
    }

    private TemporalAccessor e(CharSequence charSequence) {
        String str;
        ParsePosition parsePosition = new ParsePosition(0);
        if (charSequence != null) {
            y yVar = new y(this);
            int c = this.a.c(yVar, charSequence, parsePosition.getIndex());
            if (c < 0) {
                parsePosition.setErrorIndex(c ^ -1);
                yVar = null;
            } else {
                parsePosition.setIndex(c);
            }
            if (yVar != null && parsePosition.getErrorIndex() < 0 && parsePosition.getIndex() >= charSequence.length()) {
                return yVar.t(this.d, this.e);
            }
            if (charSequence.length() > 64) {
                str = charSequence.subSequence(0, 64).toString() + SearchDatabase.SNIPPET_WRAP;
            } else {
                str = charSequence.toString();
            }
            if (parsePosition.getErrorIndex() >= 0) {
                String str2 = "Text '" + str + "' could not be parsed at index " + parsePosition.getErrorIndex();
                parsePosition.getErrorIndex();
                throw new z(str2, charSequence);
            }
            String str3 = "Text '" + str + "' could not be parsed, unparsed text found at index " + parsePosition.getIndex();
            parsePosition.getIndex();
            throw new z(str3, charSequence);
        }
        throw new NullPointerException(DraftDatabase.Draft.TEXT);
    }

    public static DateTimeFormatter ofLocalizedTime(FormatStyle formatStyle) {
        if (formatStyle != null) {
            x xVar = new x();
            xVar.g(formatStyle);
            return xVar.v(G.SMART, g.a);
        }
        throw new NullPointerException("timeStyle");
    }

    public static DateTimeFormatter ofPattern(String str) {
        x xVar = new x();
        xVar.j(str);
        return xVar.w(Locale.getDefault());
    }

    public static DateTimeFormatter ofPattern(String str, Locale locale) {
        x xVar = new x();
        xVar.j(str);
        return xVar.w(locale);
    }

    public final f a() {
        return this.f;
    }

    public final E b() {
        return this.c;
    }

    public final Locale c() {
        return this.b;
    }

    public final ZoneId d() {
        return this.g;
    }

    public final C0006f f() {
        return this.a.b();
    }

    public String format(TemporalAccessor temporalAccessor) {
        StringBuilder sb = new StringBuilder(32);
        if (temporalAccessor != null) {
            try {
                this.a.a(new B(temporalAccessor, this), sb);
                return sb.toString();
            } catch (IOException e) {
                throw new e(e.getMessage(), e);
            }
        } else {
            throw new NullPointerException("temporal");
        }
    }

    public TemporalAccessor parse(CharSequence charSequence) {
        String str;
        if (charSequence != null) {
            try {
                return e(charSequence);
            } catch (z e) {
                throw e;
            } catch (RuntimeException e2) {
                if (charSequence.length() > 64) {
                    str = charSequence.subSequence(0, 64).toString() + SearchDatabase.SNIPPET_WRAP;
                } else {
                    str = charSequence.toString();
                }
                throw new z("Text '" + str + "' could not be parsed: " + e2.getMessage(), charSequence, e2);
            }
        } else {
            throw new NullPointerException(DraftDatabase.Draft.TEXT);
        }
    }

    public final String toString() {
        String fVar = this.a.toString();
        return fVar.startsWith("[") ? fVar : fVar.substring(1, fVar.length() - 1);
    }
}
