package j$.time.format;

/* loaded from: classes2.dex */
public final class o extends p {
    public o() {
        super("", null, null, 0);
    }

    private o(String str, String str2, p pVar) {
        super(str, str2, pVar, 0);
    }

    @Override // j$.time.format.p
    protected final boolean c(char c, char c2) {
        return y.c(c, c2);
    }

    @Override // j$.time.format.p
    protected final p e(String str, String str2, p pVar) {
        return new o(str, str2, pVar);
    }

    @Override // j$.time.format.p
    protected final boolean h(CharSequence charSequence, int i, int i2) {
        int length = this.a.length();
        if (length > i2 - i) {
            return false;
        }
        int i3 = 0;
        while (true) {
            length--;
            if (length <= 0) {
                return true;
            }
            i3++;
            i++;
            if (!y.c(this.a.charAt(i3), charSequence.charAt(i))) {
                return false;
            }
        }
    }
}
