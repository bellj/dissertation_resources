package j$.time.format;

import j$.time.b;
import j$.time.chrono.f;
import j$.time.chrono.g;
import j$.time.temporal.ChronoField;
import j$.time.temporal.TemporalField;
import j$.time.temporal.l;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

/* loaded from: classes2.dex */
public final class t implements AbstractC0007g {
    private final TemporalField a;
    private final TextStyle b;
    private final D c;
    private volatile l d;

    public t(TemporalField temporalField, TextStyle textStyle, D d) {
        this.a = temporalField;
        this.b = textStyle;
        this.c = d;
    }

    @Override // j$.time.format.AbstractC0007g
    public final boolean a(B b, StringBuilder sb) {
        String str;
        g gVar;
        Long e = b.e(this.a);
        if (e == null) {
            return false;
        }
        f fVar = (f) b.d().k(l.d());
        if (fVar == null || fVar == (gVar = g.a)) {
            str = this.c.d(this.a, e.longValue(), this.b, b.c());
        } else {
            D d = this.c;
            TemporalField temporalField = this.a;
            long longValue = e.longValue();
            TextStyle textStyle = this.b;
            Locale c = b.c();
            d.getClass();
            str = (fVar == gVar || !(temporalField instanceof ChronoField)) ? d.d(temporalField, longValue, textStyle, c) : null;
        }
        if (str == null) {
            if (this.d == null) {
                this.d = new l(this.a, 1, 19, 1);
            }
            return this.d.a(b, sb);
        }
        sb.append(str);
        return true;
    }

    @Override // j$.time.format.AbstractC0007g
    public final int c(y yVar, CharSequence charSequence, int i) {
        f fVar;
        int length = charSequence.length();
        if (i < 0 || i > length) {
            throw new IndexOutOfBoundsException();
        }
        Iterator it = null;
        TextStyle textStyle = yVar.l() ? this.b : null;
        f h = yVar.h();
        if (h == null || h == (fVar = g.a)) {
            it = this.c.e(this.a, textStyle, yVar.i());
        } else {
            D d = this.c;
            TemporalField temporalField = this.a;
            Locale i2 = yVar.i();
            d.getClass();
            if (h == fVar || !(temporalField instanceof ChronoField)) {
                it = d.e(temporalField, textStyle, i2);
            }
        }
        if (it != null) {
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                String str = (String) entry.getKey();
                if (yVar.s(str, 0, charSequence, i, str.length())) {
                    return yVar.o(this.a, ((Long) entry.getValue()).longValue(), i, str.length() + i);
                }
            }
            if (yVar.l()) {
                return i ^ -1;
            }
        }
        if (this.d == null) {
            this.d = new l(this.a, 1, 19, 1);
        }
        return this.d.c(yVar, charSequence, i);
    }

    public final String toString() {
        Object obj;
        StringBuilder sb;
        if (this.b == TextStyle.FULL) {
            sb = b.a("Text(");
            obj = this.a;
        } else {
            sb = b.a("Text(");
            sb.append(this.a);
            sb.append(",");
            obj = this.b;
        }
        sb.append(obj);
        sb.append(")");
        return sb.toString();
    }
}
