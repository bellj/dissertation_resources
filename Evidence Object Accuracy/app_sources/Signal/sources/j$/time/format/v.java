package j$.time.format;

import j$.time.ZoneId;
import j$.time.ZoneOffset;
import j$.time.e;
import j$.time.temporal.ChronoField;
import j$.time.temporal.n;
import j$.time.zone.g;
import java.text.ParsePosition;
import java.util.AbstractMap;
import java.util.HashSet;

/* loaded from: classes2.dex */
public class v implements AbstractC0007g {
    private static volatile AbstractMap.SimpleImmutableEntry c;
    private static volatile AbstractMap.SimpleImmutableEntry d;
    private final n a;
    private final String b;

    public v(n nVar, String str) {
        this.a = nVar;
        this.b = str;
    }

    private static int d(y yVar, CharSequence charSequence, int i, int i2, m mVar) {
        String upperCase = charSequence.toString().substring(i, i2).toUpperCase();
        if (i2 < charSequence.length() && charSequence.charAt(i2) != '0' && !yVar.b(charSequence.charAt(i2), 'Z')) {
            y d2 = yVar.d();
            int c2 = mVar.c(d2, charSequence, i2);
            try {
                if (c2 >= 0) {
                    yVar.n(ZoneId.n(upperCase, ZoneOffset.r((int) d2.j(ChronoField.OFFSET_SECONDS).longValue())));
                    return c2;
                } else if (mVar == m.d) {
                    return i ^ -1;
                } else {
                    yVar.n(ZoneId.of(upperCase));
                    return i2;
                }
            } catch (e unused) {
                return i ^ -1;
            }
        } else {
            yVar.n(ZoneId.of(upperCase));
            return i2;
        }
    }

    @Override // j$.time.format.AbstractC0007g
    public boolean a(B b, StringBuilder sb) {
        ZoneId zoneId = (ZoneId) b.f(this.a);
        if (zoneId == null) {
            return false;
        }
        sb.append(zoneId.l());
        return true;
    }

    public p b(y yVar) {
        HashSet a = g.a();
        int size = a.size();
        AbstractMap.SimpleImmutableEntry simpleImmutableEntry = yVar.k() ? c : d;
        if (simpleImmutableEntry == null || ((Integer) simpleImmutableEntry.getKey()).intValue() != size) {
            synchronized (this) {
                try {
                    simpleImmutableEntry = yVar.k() ? c : d;
                    if (simpleImmutableEntry == null || ((Integer) simpleImmutableEntry.getKey()).intValue() != size) {
                        simpleImmutableEntry = new AbstractMap.SimpleImmutableEntry(Integer.valueOf(size), p.g(a, yVar));
                        if (yVar.k()) {
                            c = simpleImmutableEntry;
                        } else {
                            d = simpleImmutableEntry;
                        }
                    }
                } catch (Throwable th) {
                    throw th;
                }
            }
        }
        return (p) simpleImmutableEntry.getValue();
    }

    @Override // j$.time.format.AbstractC0007g
    public final int c(y yVar, CharSequence charSequence, int i) {
        int i2;
        int length = charSequence.length();
        if (i > length) {
            throw new IndexOutOfBoundsException();
        } else if (i == length) {
            return i ^ -1;
        } else {
            char charAt = charSequence.charAt(i);
            if (charAt == '+' || charAt == '-') {
                return d(yVar, charSequence, i, i, m.d);
            }
            int i3 = i + 2;
            if (length >= i3) {
                char charAt2 = charSequence.charAt(i + 1);
                if (yVar.b(charAt, 'U') && yVar.b(charAt2, 'T')) {
                    int i4 = i + 3;
                    return (length < i4 || !yVar.b(charSequence.charAt(i3), 'C')) ? d(yVar, charSequence, i, i3, m.e) : d(yVar, charSequence, i, i4, m.e);
                } else if (yVar.b(charAt, 'G') && length >= (i2 = i + 3) && yVar.b(charAt2, 'M') && yVar.b(charSequence.charAt(i3), 'T')) {
                    return d(yVar, charSequence, i, i2, m.e);
                }
            }
            p b = b(yVar);
            ParsePosition parsePosition = new ParsePosition(i);
            String d2 = b.d(charSequence, parsePosition);
            if (d2 != null) {
                yVar.n(ZoneId.of(d2));
                return parsePosition.getIndex();
            } else if (!yVar.b(charAt, 'Z')) {
                return i ^ -1;
            } else {
                yVar.n(ZoneOffset.f);
                return i + 1;
            }
        }
    }

    public final String toString() {
        return this.b;
    }
}
