package j$.time.format;

import j$.time.Instant;
import j$.time.LocalTime;
import j$.time.Period;
import j$.time.ZoneId;
import j$.time.ZoneOffset;
import j$.time.ZonedDateTime;
import j$.time.a;
import j$.time.chrono.b;
import j$.time.chrono.f;
import j$.time.chrono.g;
import j$.time.e;
import j$.time.h;
import j$.time.temporal.ChronoField;
import j$.time.temporal.TemporalAccessor;
import j$.time.temporal.TemporalField;
import j$.time.temporal.l;
import j$.time.temporal.n;
import j$.time.temporal.p;
import j$.time.temporal.q;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* loaded from: classes2.dex */
public final class F implements TemporalAccessor {
    final HashMap a = new HashMap();
    ZoneId b;
    f c;
    private G d;
    private b e;
    private LocalTime f;
    Period g = Period.d;

    private void g(TemporalAccessor temporalAccessor) {
        Iterator it = this.a.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            TemporalField temporalField = (TemporalField) entry.getKey();
            if (temporalAccessor.h(temporalField)) {
                try {
                    long j = temporalAccessor.getLong(temporalField);
                    long longValue = ((Long) entry.getValue()).longValue();
                    if (j == longValue) {
                        it.remove();
                    } else {
                        throw new e("Conflict found: Field " + temporalField + " " + j + " differs from " + temporalField + " " + longValue + " derived from " + temporalAccessor);
                    }
                } catch (RuntimeException unused) {
                }
            }
        }
    }

    private void m() {
        if (this.a.containsKey(ChronoField.INSTANT_SECONDS)) {
            ZoneId zoneId = this.b;
            if (zoneId == null) {
                Long l = (Long) this.a.get(ChronoField.OFFSET_SECONDS);
                if (l != null) {
                    zoneId = ZoneOffset.r(l.intValue());
                } else {
                    return;
                }
            }
            n(zoneId);
        }
    }

    private void n(ZoneId zoneId) {
        HashMap hashMap = this.a;
        ChronoField chronoField = ChronoField.INSTANT_SECONDS;
        Instant ofEpochSecond = Instant.ofEpochSecond(((Long) hashMap.remove(chronoField)).longValue());
        ((g) this.c).getClass();
        ZonedDateTime n = ZonedDateTime.n(ofEpochSecond, zoneId);
        r(n.s());
        s(chronoField, ChronoField.SECOND_OF_DAY, Long.valueOf((long) n.toLocalTime().y()));
    }

    private void o(long j, long j2, long j3, long j4) {
        LocalTime q;
        Period period;
        if (this.d == G.LENIENT) {
            long b = a.b(a.b(a.b(a.d(j, 3600000000000L), a.d(j2, 60000000000L)), a.d(j3, 1000000000)), j4);
            q = LocalTime.r(a.c(b, 86400000000000L));
            period = Period.ofDays((int) a.e(b, 86400000000000L));
        } else {
            int k = ChronoField.MINUTE_OF_HOUR.k(j2);
            int k2 = ChronoField.NANO_OF_SECOND.k(j4);
            if (this.d == G.SMART && j == 24 && k == 0 && j3 == 0 && k2 == 0) {
                q = LocalTime.g;
                period = Period.ofDays(1);
            } else {
                q = LocalTime.q(ChronoField.HOUR_OF_DAY.k(j), k, ChronoField.SECOND_OF_MINUTE.k(j3), k2);
                period = Period.d;
            }
        }
        q(q, period);
    }

    private void p() {
        Long l;
        ChronoField chronoField;
        HashMap hashMap = this.a;
        ChronoField chronoField2 = ChronoField.CLOCK_HOUR_OF_DAY;
        long j = 0;
        if (hashMap.containsKey(chronoField2)) {
            long longValue = ((Long) this.a.remove(chronoField2)).longValue();
            G g = this.d;
            if (g == G.STRICT || (g == G.SMART && longValue != 0)) {
                chronoField2.l(longValue);
            }
            ChronoField chronoField3 = ChronoField.HOUR_OF_DAY;
            if (longValue == 24) {
                longValue = 0;
            }
            s(chronoField2, chronoField3, Long.valueOf(longValue));
        }
        HashMap hashMap2 = this.a;
        ChronoField chronoField4 = ChronoField.CLOCK_HOUR_OF_AMPM;
        if (hashMap2.containsKey(chronoField4)) {
            long longValue2 = ((Long) this.a.remove(chronoField4)).longValue();
            G g2 = this.d;
            if (g2 == G.STRICT || (g2 == G.SMART && longValue2 != 0)) {
                chronoField4.l(longValue2);
            }
            ChronoField chronoField5 = ChronoField.HOUR_OF_AMPM;
            if (longValue2 != 12) {
                j = longValue2;
            }
            s(chronoField4, chronoField5, Long.valueOf(j));
        }
        HashMap hashMap3 = this.a;
        ChronoField chronoField6 = ChronoField.AMPM_OF_DAY;
        if (hashMap3.containsKey(chronoField6)) {
            HashMap hashMap4 = this.a;
            ChronoField chronoField7 = ChronoField.HOUR_OF_AMPM;
            if (hashMap4.containsKey(chronoField7)) {
                long longValue3 = ((Long) this.a.remove(chronoField6)).longValue();
                long longValue4 = ((Long) this.a.remove(chronoField7)).longValue();
                if (this.d == G.LENIENT) {
                    chronoField = ChronoField.HOUR_OF_DAY;
                    l = Long.valueOf(a.b(a.d(longValue3, 12), longValue4));
                } else {
                    chronoField6.l(longValue3);
                    chronoField7.l(longValue3);
                    chronoField = ChronoField.HOUR_OF_DAY;
                    l = Long.valueOf((longValue3 * 12) + longValue4);
                }
                s(chronoField6, chronoField, l);
            }
        }
        HashMap hashMap5 = this.a;
        ChronoField chronoField8 = ChronoField.NANO_OF_DAY;
        if (hashMap5.containsKey(chronoField8)) {
            long longValue5 = ((Long) this.a.remove(chronoField8)).longValue();
            if (this.d != G.LENIENT) {
                chronoField8.l(longValue5);
            }
            s(chronoField8, ChronoField.HOUR_OF_DAY, Long.valueOf(longValue5 / 3600000000000L));
            s(chronoField8, ChronoField.MINUTE_OF_HOUR, Long.valueOf((longValue5 / 60000000000L) % 60));
            s(chronoField8, ChronoField.SECOND_OF_MINUTE, Long.valueOf((longValue5 / 1000000000) % 60));
            s(chronoField8, ChronoField.NANO_OF_SECOND, Long.valueOf(longValue5 % 1000000000));
        }
        HashMap hashMap6 = this.a;
        ChronoField chronoField9 = ChronoField.MICRO_OF_DAY;
        if (hashMap6.containsKey(chronoField9)) {
            long longValue6 = ((Long) this.a.remove(chronoField9)).longValue();
            if (this.d != G.LENIENT) {
                chronoField9.l(longValue6);
            }
            s(chronoField9, ChronoField.SECOND_OF_DAY, Long.valueOf(longValue6 / 1000000));
            s(chronoField9, ChronoField.MICRO_OF_SECOND, Long.valueOf(longValue6 % 1000000));
        }
        HashMap hashMap7 = this.a;
        ChronoField chronoField10 = ChronoField.MILLI_OF_DAY;
        if (hashMap7.containsKey(chronoField10)) {
            long longValue7 = ((Long) this.a.remove(chronoField10)).longValue();
            if (this.d != G.LENIENT) {
                chronoField10.l(longValue7);
            }
            s(chronoField10, ChronoField.SECOND_OF_DAY, Long.valueOf(longValue7 / 1000));
            s(chronoField10, ChronoField.MILLI_OF_SECOND, Long.valueOf(longValue7 % 1000));
        }
        HashMap hashMap8 = this.a;
        ChronoField chronoField11 = ChronoField.SECOND_OF_DAY;
        if (hashMap8.containsKey(chronoField11)) {
            long longValue8 = ((Long) this.a.remove(chronoField11)).longValue();
            if (this.d != G.LENIENT) {
                chronoField11.l(longValue8);
            }
            s(chronoField11, ChronoField.HOUR_OF_DAY, Long.valueOf(longValue8 / 3600));
            s(chronoField11, ChronoField.MINUTE_OF_HOUR, Long.valueOf((longValue8 / 60) % 60));
            s(chronoField11, ChronoField.SECOND_OF_MINUTE, Long.valueOf(longValue8 % 60));
        }
        HashMap hashMap9 = this.a;
        ChronoField chronoField12 = ChronoField.MINUTE_OF_DAY;
        if (hashMap9.containsKey(chronoField12)) {
            long longValue9 = ((Long) this.a.remove(chronoField12)).longValue();
            if (this.d != G.LENIENT) {
                chronoField12.l(longValue9);
            }
            s(chronoField12, ChronoField.HOUR_OF_DAY, Long.valueOf(longValue9 / 60));
            s(chronoField12, ChronoField.MINUTE_OF_HOUR, Long.valueOf(longValue9 % 60));
        }
        HashMap hashMap10 = this.a;
        ChronoField chronoField13 = ChronoField.NANO_OF_SECOND;
        if (hashMap10.containsKey(chronoField13)) {
            long longValue10 = ((Long) this.a.get(chronoField13)).longValue();
            G g3 = this.d;
            G g4 = G.LENIENT;
            if (g3 != g4) {
                chronoField13.l(longValue10);
            }
            HashMap hashMap11 = this.a;
            ChronoField chronoField14 = ChronoField.MICRO_OF_SECOND;
            if (hashMap11.containsKey(chronoField14)) {
                long longValue11 = ((Long) this.a.remove(chronoField14)).longValue();
                if (this.d != g4) {
                    chronoField14.l(longValue11);
                }
                longValue10 = (longValue10 % 1000) + (longValue11 * 1000);
                s(chronoField14, chronoField13, Long.valueOf(longValue10));
            }
            HashMap hashMap12 = this.a;
            ChronoField chronoField15 = ChronoField.MILLI_OF_SECOND;
            if (hashMap12.containsKey(chronoField15)) {
                long longValue12 = ((Long) this.a.remove(chronoField15)).longValue();
                if (this.d != g4) {
                    chronoField15.l(longValue12);
                }
                s(chronoField15, chronoField13, Long.valueOf((longValue10 % 1000000) + (longValue12 * 1000000)));
            }
        }
        HashMap hashMap13 = this.a;
        ChronoField chronoField16 = ChronoField.HOUR_OF_DAY;
        if (hashMap13.containsKey(chronoField16)) {
            HashMap hashMap14 = this.a;
            ChronoField chronoField17 = ChronoField.MINUTE_OF_HOUR;
            if (hashMap14.containsKey(chronoField17)) {
                HashMap hashMap15 = this.a;
                ChronoField chronoField18 = ChronoField.SECOND_OF_MINUTE;
                if (hashMap15.containsKey(chronoField18) && this.a.containsKey(chronoField13)) {
                    o(((Long) this.a.remove(chronoField16)).longValue(), ((Long) this.a.remove(chronoField17)).longValue(), ((Long) this.a.remove(chronoField18)).longValue(), ((Long) this.a.remove(chronoField13)).longValue());
                }
            }
        }
    }

    private void q(LocalTime localTime, Period period) {
        LocalTime localTime2 = this.f;
        if (localTime2 == null) {
            this.f = localTime;
        } else if (localTime2.equals(localTime)) {
            Period period2 = this.g;
            period2.getClass();
            Period period3 = Period.d;
            boolean z = true;
            if (!(period2 == period3)) {
                period.getClass();
                if (period != period3) {
                    z = false;
                }
                if (!z && !this.g.equals(period)) {
                    StringBuilder a = j$.time.b.a("Conflict found: Fields resolved to different excess periods: ");
                    a.append(this.g);
                    a.append(" ");
                    a.append(period);
                    throw new e(a.toString());
                }
            }
        } else {
            StringBuilder a2 = j$.time.b.a("Conflict found: Fields resolved to different times: ");
            a2.append(this.f);
            a2.append(" ");
            a2.append(localTime);
            throw new e(a2.toString());
        }
        this.g = period;
    }

    private void r(b bVar) {
        b bVar2 = this.e;
        if (bVar2 != null) {
            if (bVar != null && !((h) bVar2).equals(bVar)) {
                StringBuilder a = j$.time.b.a("Conflict found: Fields resolved to two different dates: ");
                a.append(this.e);
                a.append(" ");
                a.append(bVar);
                throw new e(a.toString());
            }
        } else if (bVar != null) {
            if (((j$.time.chrono.a) this.c).equals(g.a)) {
                this.e = bVar;
                return;
            }
            StringBuilder a2 = j$.time.b.a("ChronoLocalDate must use the effective parsed chronology: ");
            a2.append(this.c);
            throw new e(a2.toString());
        }
    }

    private void s(ChronoField chronoField, ChronoField chronoField2, Long l) {
        Long l2 = (Long) this.a.put(chronoField2, l);
        if (l2 != null && l2.longValue() != l.longValue()) {
            throw new e("Conflict found: " + chronoField2 + " " + l2 + " differs from " + chronoField2 + " " + l + " while resolving  " + chronoField);
        }
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final /* synthetic */ int c(TemporalField temporalField) {
        return l.a(this, temporalField);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final /* synthetic */ q e(TemporalField temporalField) {
        return l.c(this, temporalField);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final long getLong(TemporalField temporalField) {
        if (temporalField != null) {
            Long l = (Long) this.a.get(temporalField);
            if (l != null) {
                return l.longValue();
            }
            b bVar = this.e;
            if (bVar != null && ((h) bVar).h(temporalField)) {
                return ((h) this.e).getLong(temporalField);
            }
            LocalTime localTime = this.f;
            if (localTime != null && localTime.h(temporalField)) {
                return this.f.getLong(temporalField);
            }
            if (!(temporalField instanceof ChronoField)) {
                return temporalField.h(this);
            }
            throw new p("Unsupported field: " + temporalField);
        }
        throw new NullPointerException("field");
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final boolean h(TemporalField temporalField) {
        b bVar;
        LocalTime localTime;
        return this.a.containsKey(temporalField) || ((bVar = this.e) != null && ((h) bVar).h(temporalField)) || (((localTime = this.f) != null && localTime.h(temporalField)) || (temporalField != null && !(temporalField instanceof ChronoField) && temporalField.c(this)));
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final Object k(n nVar) {
        if (nVar == l.k()) {
            return this.b;
        }
        if (nVar == l.d()) {
            return this.c;
        }
        if (nVar == l.e()) {
            b bVar = this.e;
            if (bVar != null) {
                return h.n(bVar);
            }
            return null;
        } else if (nVar == l.f()) {
            return this.f;
        } else {
            if (nVar == l.j() || nVar == l.h()) {
                return nVar.a(this);
            }
            if (nVar == l.i()) {
                return null;
            }
            return nVar.a(this);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:105:0x0267  */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x0269  */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x026c  */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x02a6  */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x02cf  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x017d  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0230  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x0237  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void l(j$.time.format.G r19, java.util.Set r20) {
        /*
        // Method dump skipped, instructions count: 830
        */
        throw new UnsupportedOperationException("Method not decompiled: j$.time.format.F.l(j$.time.format.G, java.util.Set):void");
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(64);
        sb.append(this.a);
        sb.append(',');
        sb.append(this.c);
        if (this.b != null) {
            sb.append(',');
            sb.append(this.b);
        }
        if (!(this.e == null && this.f == null)) {
            sb.append(" resolved to ");
            b bVar = this.e;
            if (bVar != null) {
                sb.append(bVar);
                if (this.f != null) {
                    sb.append('T');
                }
            }
            sb.append(this.f);
        }
        return sb.toString();
    }
}
