package j$.time;

import j$.time.temporal.ChronoField;
import j$.time.temporal.TemporalAccessor;
import j$.time.temporal.TemporalField;
import j$.time.temporal.a;
import j$.time.temporal.j;
import j$.time.temporal.k;
import j$.time.temporal.l;
import j$.time.temporal.n;
import j$.time.temporal.o;
import j$.time.temporal.p;
import j$.time.temporal.q;
import java.io.Serializable;
import org.thoughtcrime.securesms.util.MessageRecordUtil;

/* loaded from: classes2.dex */
public final class LocalTime implements j, k, Comparable<LocalTime>, Serializable {
    public static final LocalTime e;
    public static final LocalTime f = new LocalTime(23, 59, 59, 999999999);
    public static final LocalTime g;
    private static final LocalTime[] h = new LocalTime[24];
    private final byte a;
    private final byte b;
    private final byte c;
    private final int d;

    static {
        h = new LocalTime[24];
        int i = 0;
        while (true) {
            LocalTime[] localTimeArr = h;
            if (i < localTimeArr.length) {
                localTimeArr[i] = new LocalTime(i, 0, 0, 0);
                i++;
            } else {
                LocalTime localTime = localTimeArr[0];
                g = localTime;
                LocalTime localTime2 = localTimeArr[12];
                e = localTime;
                f = new LocalTime(23, 59, 59, 999999999);
                return;
            }
        }
    }

    private LocalTime(int i, int i2, int i3, int i4) {
        this.a = (byte) i;
        this.b = (byte) i2;
        this.c = (byte) i3;
        this.d = i4;
    }

    public static LocalTime from(TemporalAccessor temporalAccessor) {
        if (temporalAccessor != null) {
            LocalTime localTime = (LocalTime) temporalAccessor.k(l.f());
            if (localTime != null) {
                return localTime;
            }
            throw new e("Unable to obtain LocalTime from TemporalAccessor: " + temporalAccessor + " of type " + temporalAccessor.getClass().getName());
        }
        throw new NullPointerException("temporal");
    }

    private static LocalTime m(int i, int i2, int i3, int i4) {
        return ((i2 | i3) | i4) == 0 ? h[i] : new LocalTime(i, i2, i3, i4);
    }

    private int n(TemporalField temporalField) {
        switch (j.a[((ChronoField) temporalField).ordinal()]) {
            case 1:
                return this.d;
            case 2:
                throw new p("Invalid field 'NanoOfDay' for get() method, use getLong() instead");
            case 3:
                return this.d / MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH;
            case 4:
                throw new p("Invalid field 'MicroOfDay' for get() method, use getLong() instead");
            case 5:
                return this.d / 1000000;
            case 6:
                return (int) (x() / 1000000);
            case 7:
                return this.c;
            case 8:
                return y();
            case 9:
                return this.b;
            case 10:
                return (this.a * 60) + this.b;
            case 11:
                return this.a % 12;
            case 12:
                int i = this.a % 12;
                if (i % 12 == 0) {
                    return 12;
                }
                return i;
            case 13:
                return this.a;
            case 14:
                byte b = this.a;
                if (b == 0) {
                    return 24;
                }
                return b;
            case 15:
                return this.a / 12;
            default:
                throw new p("Unsupported field: " + temporalField);
        }
    }

    public static LocalTime of(int i, int i2) {
        ChronoField.HOUR_OF_DAY.l((long) i);
        if (i2 == 0) {
            return h[i];
        }
        ChronoField.MINUTE_OF_HOUR.l((long) i2);
        return new LocalTime(i, i2, 0, 0);
    }

    public static LocalTime q(int i, int i2, int i3, int i4) {
        ChronoField.HOUR_OF_DAY.l((long) i);
        ChronoField.MINUTE_OF_HOUR.l((long) i2);
        ChronoField.SECOND_OF_MINUTE.l((long) i3);
        ChronoField.NANO_OF_SECOND.l((long) i4);
        return m(i, i2, i3, i4);
    }

    public static LocalTime r(long j) {
        ChronoField.NANO_OF_DAY.l(j);
        int i = (int) (j / 3600000000000L);
        long j2 = j - (((long) i) * 3600000000000L);
        int i2 = (int) (j2 / 60000000000L);
        long j3 = j2 - (((long) i2) * 60000000000L);
        int i3 = (int) (j3 / 1000000000);
        return m(i, i2, i3, (int) (j3 - (((long) i3) * 1000000000)));
    }

    public final LocalTime A(int i) {
        if (this.a == i) {
            return this;
        }
        ChronoField.HOUR_OF_DAY.l((long) i);
        return m(i, this.b, this.c, this.d);
    }

    public final LocalTime B(int i) {
        if (this.b == i) {
            return this;
        }
        ChronoField.MINUTE_OF_HOUR.l((long) i);
        return m(this.a, i, this.c, this.d);
    }

    public final LocalTime C(int i) {
        if (this.d == i) {
            return this;
        }
        ChronoField.NANO_OF_SECOND.l((long) i);
        return m(this.a, this.b, this.c, i);
    }

    public final LocalTime D(int i) {
        if (this.c == i) {
            return this;
        }
        ChronoField.SECOND_OF_MINUTE.l((long) i);
        return m(this.a, this.b, i, this.d);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final int c(TemporalField temporalField) {
        return temporalField instanceof ChronoField ? n(temporalField) : l.a(this, temporalField);
    }

    @Override // j$.time.temporal.j
    public final j d(h hVar) {
        boolean z = hVar instanceof LocalTime;
        j jVar = hVar;
        if (!z) {
            jVar = hVar.g(this);
        }
        return (LocalTime) jVar;
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final q e(TemporalField temporalField) {
        return l.c(this, temporalField);
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LocalTime)) {
            return false;
        }
        LocalTime localTime = (LocalTime) obj;
        return this.a == localTime.a && this.b == localTime.b && this.c == localTime.c && this.d == localTime.d;
    }

    @Override // j$.time.temporal.k
    public final j g(j jVar) {
        return jVar.a(x(), ChronoField.NANO_OF_DAY);
    }

    public int getHour() {
        return this.a;
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final long getLong(TemporalField temporalField) {
        return temporalField instanceof ChronoField ? temporalField == ChronoField.NANO_OF_DAY ? x() : temporalField == ChronoField.MICRO_OF_DAY ? x() / 1000 : (long) n(temporalField) : temporalField.h(this);
    }

    public int getMinute() {
        return this.b;
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final boolean h(TemporalField temporalField) {
        return temporalField instanceof ChronoField ? temporalField.f() : temporalField != null && temporalField.c(this);
    }

    @Override // java.lang.Object
    public final int hashCode() {
        long x = x();
        return (int) (x ^ (x >>> 32));
    }

    @Override // j$.time.temporal.j
    public final j j(long j, a aVar) {
        return j == Long.MIN_VALUE ? f(Long.MAX_VALUE, aVar).f(1, aVar) : f(-j, aVar);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final Object k(n nVar) {
        if (nVar == l.d() || nVar == l.k() || nVar == l.j() || nVar == l.h()) {
            return null;
        }
        if (nVar == l.f()) {
            return this;
        }
        if (nVar == l.e()) {
            return null;
        }
        return nVar == l.i() ? a.NANOS : nVar.a(this);
    }

    /* renamed from: l */
    public final int compareTo(LocalTime localTime) {
        int compare = Integer.compare(this.a, localTime.a);
        if (compare != 0) {
            return compare;
        }
        int compare2 = Integer.compare(this.b, localTime.b);
        if (compare2 != 0) {
            return compare2;
        }
        int compare3 = Integer.compare(this.c, localTime.c);
        return compare3 == 0 ? Integer.compare(this.d, localTime.d) : compare3;
    }

    public final int o() {
        return this.d;
    }

    public final int p() {
        return this.c;
    }

    /* renamed from: s */
    public final LocalTime f(long j, o oVar) {
        if (!(oVar instanceof a)) {
            return (LocalTime) oVar.c(this, j);
        }
        switch (j.b[((a) oVar).ordinal()]) {
            case 1:
                return v(j);
            case 2:
                return v((j % 86400000000L) * 1000);
            case 3:
                return v((j % 86400000) * 1000000);
            case 4:
                return w(j);
            case 5:
                return u(j);
            case 6:
                return t(j);
            case 7:
                return t((j % 2) * 12);
            default:
                throw new p("Unsupported unit: " + oVar);
        }
    }

    public final LocalTime t(long j) {
        return j == 0 ? this : m(((((int) (j % 24)) + this.a) + 24) % 24, this.b, this.c, this.d);
    }

    @Override // java.lang.Object
    public final String toString() {
        int i;
        StringBuilder sb = new StringBuilder(18);
        byte b = this.a;
        byte b2 = this.b;
        byte b3 = this.c;
        int i2 = this.d;
        sb.append(b < 10 ? "0" : "");
        sb.append((int) b);
        String str = ":0";
        sb.append(b2 < 10 ? str : ":");
        sb.append((int) b2);
        if (b3 > 0 || i2 > 0) {
            if (b3 >= 10) {
                str = ":";
            }
            sb.append(str);
            sb.append((int) b3);
            if (i2 > 0) {
                sb.append('.');
                int i3 = 1000000;
                if (i2 % 1000000 == 0) {
                    i = (i2 / 1000000) + MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH;
                } else {
                    if (i2 % MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH == 0) {
                        i2 /= MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH;
                    } else {
                        i3 = 1000000000;
                    }
                    i = i2 + i3;
                }
                sb.append(Integer.toString(i).substring(1));
            }
        }
        return sb.toString();
    }

    public final LocalTime u(long j) {
        if (j == 0) {
            return this;
        }
        int i = (this.a * 60) + this.b;
        int i2 = ((((int) (j % 1440)) + i) + 1440) % 1440;
        return i == i2 ? this : m(i2 / 60, i2 % 60, this.c, this.d);
    }

    public final LocalTime v(long j) {
        if (j == 0) {
            return this;
        }
        long x = x();
        long j2 = (((j % 86400000000000L) + x) + 86400000000000L) % 86400000000000L;
        return x == j2 ? this : m((int) (j2 / 3600000000000L), (int) ((j2 / 60000000000L) % 60), (int) ((j2 / 1000000000) % 60), (int) (j2 % 1000000000));
    }

    public final LocalTime w(long j) {
        if (j == 0) {
            return this;
        }
        int i = (this.b * 60) + (this.a * 3600) + this.c;
        int i2 = ((((int) (j % 86400)) + i) + 86400) % 86400;
        return i == i2 ? this : m(i2 / 3600, (i2 / 60) % 60, i2 % 60, this.d);
    }

    public final long x() {
        return (((long) this.c) * 1000000000) + (((long) this.b) * 60000000000L) + (((long) this.a) * 3600000000000L) + ((long) this.d);
    }

    public final int y() {
        return (this.b * 60) + (this.a * 3600) + this.c;
    }

    /* renamed from: z */
    public final LocalTime a(long j, TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return (LocalTime) temporalField.j(this, j);
        }
        ChronoField chronoField = (ChronoField) temporalField;
        chronoField.l(j);
        switch (j.a[chronoField.ordinal()]) {
            case 1:
                return C((int) j);
            case 2:
                return r(j);
            case 3:
                return C(((int) j) * MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH);
            case 4:
                return r(j * 1000);
            case 5:
                return C(((int) j) * 1000000);
            case 6:
                return r(j * 1000000);
            case 7:
                return D((int) j);
            case 8:
                return w(j - ((long) y()));
            case 9:
                return B((int) j);
            case 10:
                return u(j - ((long) ((this.a * 60) + this.b)));
            case 11:
                return t(j - ((long) (this.a % 12)));
            case 12:
                if (j == 12) {
                    j = 0;
                }
                return t(j - ((long) (this.a % 12)));
            case 13:
                return A((int) j);
            case 14:
                if (j == 24) {
                    j = 0;
                }
                return A((int) j);
            case 15:
                return t((j - ((long) (this.a / 12))) * 12);
            default:
                throw new p("Unsupported field: " + temporalField);
        }
    }
}
