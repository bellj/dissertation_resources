package j$.time;

import j$.time.chrono.f;
import j$.time.chrono.g;
import j$.time.temporal.TemporalAccessor;
import j$.time.temporal.TemporalAmount;
import j$.time.temporal.a;
import j$.time.temporal.j;
import j$.time.temporal.l;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.regex.Pattern;

/* loaded from: classes2.dex */
public final class Period implements TemporalAmount, Serializable {
    public static final Period d = new Period(0);
    private final int a = 0;
    private final int b = 0;
    private final int c;

    static {
        d = new Period(0);
        Pattern.compile("([-+]?)P(?:([-+]?[0-9]+)Y)?(?:([-+]?[0-9]+)M)?(?:([-+]?[0-9]+)W)?(?:([-+]?[0-9]+)D)?", 2);
        Collections.unmodifiableList(Arrays.asList(a.YEARS, a.MONTHS, a.DAYS));
    }

    private Period(int i) {
        this.c = i;
    }

    private static void e(TemporalAccessor temporalAccessor) {
        if (temporalAccessor != null) {
            f fVar = (f) temporalAccessor.k(l.d());
            if (fVar != null && !g.a.equals(fVar)) {
                throw new e("Chronology mismatch, expected: ISO, actual: ISO");
            }
            return;
        }
        throw new NullPointerException("temporal");
    }

    public static Period ofDays(int i) {
        return (i | 0) == 0 ? d : new Period(i);
    }

    @Override // j$.time.temporal.TemporalAmount
    public final j a(j jVar) {
        a aVar;
        long j;
        e(jVar);
        if (this.b == 0) {
            int i = this.a;
            if (i != 0) {
                j = (long) i;
                aVar = a.YEARS;
                jVar = jVar.f(j, aVar);
            }
        } else {
            j = d();
            if (j != 0) {
                aVar = a.MONTHS;
                jVar = jVar.f(j, aVar);
            }
        }
        int i2 = this.c;
        return i2 != 0 ? jVar.f((long) i2, a.DAYS) : jVar;
    }

    public final int b() {
        return this.c;
    }

    @Override // j$.time.temporal.TemporalAmount
    public final j c(j jVar) {
        a aVar;
        long j;
        e(jVar);
        if (this.b == 0) {
            int i = this.a;
            if (i != 0) {
                j = (long) i;
                aVar = a.YEARS;
                jVar = jVar.j(j, aVar);
            }
        } else {
            j = d();
            if (j != 0) {
                aVar = a.MONTHS;
                jVar = jVar.j(j, aVar);
            }
        }
        int i2 = this.c;
        return i2 != 0 ? jVar.j((long) i2, a.DAYS) : jVar;
    }

    public final long d() {
        return (((long) this.a) * 12) + ((long) this.b);
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Period)) {
            return false;
        }
        Period period = (Period) obj;
        return this.a == period.a && this.b == period.b && this.c == period.c;
    }

    @Override // java.lang.Object
    public final int hashCode() {
        return Integer.rotateLeft(this.c, 16) + Integer.rotateLeft(this.b, 8) + this.a;
    }

    @Override // java.lang.Object
    public final String toString() {
        if (this == d) {
            return "P0D";
        }
        StringBuilder sb = new StringBuilder();
        sb.append('P');
        int i = this.a;
        if (i != 0) {
            sb.append(i);
            sb.append('Y');
        }
        int i2 = this.b;
        if (i2 != 0) {
            sb.append(i2);
            sb.append('M');
        }
        int i3 = this.c;
        if (i3 != 0) {
            sb.append(i3);
            sb.append('D');
        }
        return sb.toString();
    }
}
