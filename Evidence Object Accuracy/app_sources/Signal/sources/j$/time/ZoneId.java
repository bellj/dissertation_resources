package j$.time;

import j$.time.zone.c;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/* loaded from: classes2.dex */
public abstract class ZoneId implements Serializable {
    public static final Map a;

    static {
        HashMap hashMap = new HashMap(64);
        hashMap.put("ACT", "Australia/Darwin");
        hashMap.put("AET", "Australia/Sydney");
        hashMap.put("AGT", "America/Argentina/Buenos_Aires");
        hashMap.put("ART", "Africa/Cairo");
        hashMap.put("AST", "America/Anchorage");
        hashMap.put("BET", "America/Sao_Paulo");
        hashMap.put("BST", "Asia/Dhaka");
        hashMap.put("CAT", "Africa/Harare");
        hashMap.put("CNT", "America/St_Johns");
        hashMap.put("CST", "America/Chicago");
        hashMap.put("CTT", "Asia/Shanghai");
        hashMap.put("EAT", "Africa/Addis_Ababa");
        hashMap.put("ECT", "Europe/Paris");
        hashMap.put("IET", "America/Indiana/Indianapolis");
        hashMap.put("IST", "Asia/Kolkata");
        hashMap.put("JST", "Asia/Tokyo");
        hashMap.put("MIT", "Pacific/Apia");
        hashMap.put("NET", "Asia/Yerevan");
        hashMap.put("NST", "Pacific/Auckland");
        hashMap.put("PLT", "Asia/Karachi");
        hashMap.put("PNT", "America/Phoenix");
        hashMap.put("PRT", "America/Puerto_Rico");
        hashMap.put("PST", "America/Los_Angeles");
        hashMap.put("SST", "Pacific/Guadalcanal");
        hashMap.put("VST", "Asia/Ho_Chi_Minh");
        hashMap.put("EST", "-05:00");
        hashMap.put("MST", "-07:00");
        hashMap.put("HST", "-10:00");
        a = Collections.unmodifiableMap(hashMap);
    }

    public ZoneId() {
        if (getClass() != ZoneOffset.class && getClass() != o.class) {
            throw new AssertionError("Invalid subclass");
        }
    }

    public static ZoneId n(String str, ZoneOffset zoneOffset) {
        if (str == null) {
            throw new NullPointerException("prefix");
        } else if (zoneOffset == null) {
            throw new NullPointerException("offset");
        } else if (str.length() == 0) {
            return zoneOffset;
        } else {
            if (str.equals("GMT") || str.equals("UTC") || str.equals("UT")) {
                if (zoneOffset.o() != 0) {
                    str = str.concat(zoneOffset.l());
                }
                return new o(str, c.j(zoneOffset));
            }
            throw new IllegalArgumentException("prefix should be GMT, UTC or UT, is: " + str);
        }
    }

    public static ZoneId of(String str) {
        int i;
        if (str == null) {
            throw new NullPointerException("zoneId");
        } else if (str.length() <= 1 || str.startsWith("+") || str.startsWith("-")) {
            return ZoneOffset.p(str);
        } else {
            if (str.startsWith("UTC") || str.startsWith("GMT")) {
                i = 3;
            } else if (!str.startsWith("UT")) {
                return o.o(str);
            } else {
                i = 2;
            }
            String substring = str.substring(0, i);
            if (str.length() == i) {
                return n(substring, ZoneOffset.f);
            }
            if (str.charAt(i) != '+' && str.charAt(i) != '-') {
                return o.o(str);
            }
            try {
                ZoneOffset p = ZoneOffset.p(str.substring(i));
                ZoneOffset zoneOffset = ZoneOffset.f;
                return n(substring, p);
            } catch (e e) {
                throw new e("Invalid ID for offset-based ZoneId: " + str, e);
            }
        }
    }

    public static ZoneId systemDefault() {
        String id = TimeZone.getDefault().getID();
        Map map = a;
        if (id == null) {
            throw new NullPointerException("zoneId");
        } else if (map != null) {
            String str = (String) map.get(id);
            if (str != null) {
                id = str;
            }
            return of(id);
        } else {
            throw new NullPointerException("aliasMap");
        }
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof ZoneId) {
            return l().equals(((ZoneId) obj).l());
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return l().hashCode();
    }

    public abstract String l();

    public abstract c m();

    @Override // java.lang.Object
    public String toString() {
        return l();
    }
}
