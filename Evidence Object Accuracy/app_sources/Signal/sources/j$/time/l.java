package j$.time;

import j$.time.chrono.a;
import j$.time.chrono.c;
import j$.time.chrono.g;
import j$.time.temporal.ChronoField;
import j$.time.temporal.TemporalAccessor;
import j$.time.temporal.TemporalField;
import j$.time.temporal.j;
import j$.time.temporal.k;
import j$.time.temporal.n;
import j$.time.temporal.p;
import j$.time.temporal.q;

/* loaded from: classes2.dex */
public enum l implements TemporalAccessor, k {
    JANUARY,
    FEBRUARY,
    MARCH,
    APRIL,
    MAY,
    JUNE,
    JULY,
    AUGUST,
    SEPTEMBER,
    OCTOBER,
    NOVEMBER,
    DECEMBER;
    
    private static final l[] a = values();

    public static l n(int i) {
        if (i >= 1 && i <= 12) {
            return a[i - 1];
        }
        throw new e("Invalid value for MonthOfYear: " + i);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final int c(TemporalField temporalField) {
        return temporalField == ChronoField.MONTH_OF_YEAR ? ordinal() + 1 : j$.time.temporal.l.a(this, temporalField);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final q e(TemporalField temporalField) {
        return temporalField == ChronoField.MONTH_OF_YEAR ? temporalField.e() : j$.time.temporal.l.c(this, temporalField);
    }

    @Override // j$.time.temporal.k
    public final j g(j jVar) {
        if (((a) c.b(jVar)).equals(g.a)) {
            return jVar.a((long) (ordinal() + 1), ChronoField.MONTH_OF_YEAR);
        }
        throw new e("Adjustment only supported on ISO date-time");
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final long getLong(TemporalField temporalField) {
        if (temporalField == ChronoField.MONTH_OF_YEAR) {
            return (long) (ordinal() + 1);
        }
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.h(this);
        }
        throw new p("Unsupported field: " + temporalField);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final boolean h(TemporalField temporalField) {
        return temporalField instanceof ChronoField ? temporalField == ChronoField.MONTH_OF_YEAR : temporalField != null && temporalField.c(this);
    }

    @Override // j$.time.temporal.TemporalAccessor
    public final Object k(n nVar) {
        return nVar == j$.time.temporal.l.d() ? g.a : nVar == j$.time.temporal.l.i() ? j$.time.temporal.a.MONTHS : j$.time.temporal.l.b(this, nVar);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: boolean */
    /* JADX WARN: Multi-variable type inference failed */
    public final int l(boolean z) {
        switch (k.a[ordinal()]) {
            case 1:
                return 32;
            case 2:
                return (z ? 1 : 0) + 91;
            case 3:
                return z + 152;
            case 4:
                return z + 244;
            case 5:
                return z + 305;
            case 6:
                return 1;
            case 7:
                return z + 60;
            case 8:
                return z + 121;
            case 9:
                return z + 182;
            case 10:
                return z + 213;
            case 11:
                return z + 274;
            default:
                return z + 335;
        }
    }

    public final int m(boolean z) {
        int i = k.a[ordinal()];
        return i != 1 ? (i == 2 || i == 3 || i == 4 || i == 5) ? 30 : 31 : z ? 29 : 28;
    }

    public final l o() {
        return a[((((int) 1) + 12) + ordinal()) % 12];
    }
}
