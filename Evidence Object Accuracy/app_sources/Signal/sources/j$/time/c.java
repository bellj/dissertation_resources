package j$.time;

import java.io.Serializable;

/* loaded from: classes2.dex */
public final class c extends d implements Serializable {
    private final ZoneId a;

    public c(ZoneId zoneId) {
        this.a = zoneId;
    }

    @Override // j$.time.d
    public final long a() {
        return System.currentTimeMillis();
    }

    public final ZoneId c() {
        return this.a;
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (obj instanceof c) {
            return this.a.equals(((c) obj).a);
        }
        return false;
    }

    @Override // java.lang.Object
    public final int hashCode() {
        return this.a.hashCode() + 1;
    }

    @Override // java.lang.Object
    public final String toString() {
        StringBuilder a = b.a("SystemClock[");
        a.append(this.a);
        a.append("]");
        return a.toString();
    }
}
