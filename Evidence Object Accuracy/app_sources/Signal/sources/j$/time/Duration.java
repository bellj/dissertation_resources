package j$.time;

import j$.time.temporal.TemporalAmount;
import j$.time.temporal.a;
import j$.time.temporal.j;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.regex.Pattern;

/* loaded from: classes2.dex */
public final class Duration implements TemporalAmount, Comparable<Duration>, Serializable {
    public static final Duration c = new Duration(0, 0);
    private final long a;
    private final int b;

    static {
        c = new Duration(0, 0);
        BigInteger.valueOf(1000000000);
        Pattern.compile("([-+]?)P(?:([-+]?[0-9]+)D)?(T(?:([-+]?[0-9]+)H)?(?:([-+]?[0-9]+)M)?(?:([-+]?[0-9]+)(?:[.,]([0-9]{0,9}))?S)?)?", 2);
    }

    private Duration(long j, int i) {
        this.a = j;
        this.b = i;
    }

    private static Duration d(long j, int i) {
        return (((long) i) | j) == 0 ? c : new Duration(j, i);
    }

    public static Duration f(long j) {
        long j2 = j / 1000000000;
        int i = (int) (j % 1000000000);
        if (i < 0) {
            i = (int) (((long) i) + 1000000000);
            j2--;
        }
        return d(j2, i);
    }

    public static Duration g() {
        return d(a.b(Long.MAX_VALUE, a.e(999999999, 1000000000)), (int) a.c(999999999, 1000000000));
    }

    public static Duration h(long j) {
        return d(j, 0);
    }

    public static Duration ofHours(long j) {
        return d(a.d(j, 3600), 0);
    }

    public static Duration ofMinutes(long j) {
        return d(a.d(j, 60), 0);
    }

    @Override // j$.time.temporal.TemporalAmount
    public final j a(j jVar) {
        long j = this.a;
        if (j != 0) {
            jVar = jVar.f(j, a.SECONDS);
        }
        int i = this.b;
        return i != 0 ? jVar.f((long) i, a.NANOS) : jVar;
    }

    @Override // j$.time.temporal.TemporalAmount
    public final j c(j jVar) {
        long j = this.a;
        if (j != 0) {
            jVar = jVar.j(j, a.SECONDS);
        }
        int i = this.b;
        return i != 0 ? jVar.j((long) i, a.NANOS) : jVar;
    }

    @Override // java.lang.Comparable
    public final int compareTo(Duration duration) {
        Duration duration2 = duration;
        int compare = Long.compare(this.a, duration2.a);
        return compare != 0 ? compare : this.b - duration2.b;
    }

    public final long e() {
        return this.a;
    }

    @Override // java.lang.Object
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Duration)) {
            return false;
        }
        Duration duration = (Duration) obj;
        return this.a == duration.a && this.b == duration.b;
    }

    @Override // java.lang.Object
    public final int hashCode() {
        long j = this.a;
        return (this.b * 51) + ((int) (j ^ (j >>> 32)));
    }

    @Override // java.lang.Object
    public final String toString() {
        if (this == c) {
            return "PT0S";
        }
        long j = this.a;
        long j2 = j / 3600;
        int i = (int) ((j % 3600) / 60);
        int i2 = (int) (j % 60);
        StringBuilder sb = new StringBuilder(24);
        sb.append("PT");
        if (j2 != 0) {
            sb.append(j2);
            sb.append('H');
        }
        if (i != 0) {
            sb.append(i);
            sb.append('M');
        }
        if (i2 == 0 && this.b == 0 && sb.length() > 2) {
            return sb.toString();
        }
        if (i2 >= 0 || this.b <= 0) {
            sb.append(i2);
        } else if (i2 == -1) {
            sb.append("-0");
        } else {
            sb.append(i2 + 1);
        }
        if (this.b > 0) {
            int length = sb.length();
            sb.append(i2 < 0 ? 2000000000 - ((long) this.b) : ((long) this.b) + 1000000000);
            while (sb.charAt(sb.length() - 1) == '0') {
                sb.setLength(sb.length() - 1);
            }
            sb.setCharAt(length, '.');
        }
        sb.append('S');
        return sb.toString();
    }
}
