package j$.time;

import j$.time.zone.c;
import j$.time.zone.d;
import j$.time.zone.g;

/* loaded from: classes2.dex */
public final class o extends ZoneId {
    private final String b;
    private final transient c c;

    public o(String str, c cVar) {
        this.b = str;
        this.c = cVar;
    }

    public static o o(String str) {
        if (str != null) {
            int length = str.length();
            if (length >= 2) {
                for (int i = 0; i < length; i++) {
                    char charAt = str.charAt(i);
                    if ((charAt < 'a' || charAt > 'z') && ((charAt < 'A' || charAt > 'Z') && ((charAt != '/' || i == 0) && ((charAt < '0' || charAt > '9' || i == 0) && ((charAt != '~' || i == 0) && ((charAt != '.' || i == 0) && ((charAt != '_' || i == 0) && ((charAt != '+' || i == 0) && (charAt != '-' || i == 0))))))))) {
                        throw new e("Invalid ID for region-based ZoneId, invalid format: " + str);
                    }
                }
                try {
                    return new o(str, g.b(str, true));
                } catch (d e) {
                    throw e;
                }
            } else {
                throw new e("Invalid ID for region-based ZoneId, invalid format: " + str);
            }
        } else {
            throw new NullPointerException("zoneId");
        }
    }

    @Override // j$.time.ZoneId
    public final String l() {
        return this.b;
    }

    @Override // j$.time.ZoneId
    public final c m() {
        c cVar = this.c;
        return cVar != null ? cVar : g.b(this.b, false);
    }
}
