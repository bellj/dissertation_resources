package j$.lang;

import j$.util.DesugarCollections;
import j$.util.Spliterator;
import j$.util.T;
import j$.util.function.Consumer;
import java.util.Iterator;

/* loaded from: classes2.dex */
public interface Iterable<T> {

    /* renamed from: j$.lang.Iterable$-CC */
    /* loaded from: classes2.dex */
    public final /* synthetic */ class CC<T> {
        public static void $default$forEach(Iterable iterable, Consumer consumer) {
            if (DesugarCollections.a.isInstance(iterable)) {
                DesugarCollections.c(iterable, consumer);
                return;
            }
            consumer.getClass();
            Iterator<T> it = iterable.iterator();
            while (it.hasNext()) {
                consumer.accept(it.next());
            }
        }

        public static Spliterator $default$spliterator(Iterable iterable) {
            return T.n(iterable.iterator());
        }
    }

    void forEach(Consumer<? super T> consumer);

    @Override // j$.lang.Iterable
    Iterator<T> iterator();

    @Override // j$.lang.Iterable
    /* renamed from: spliterator */
    Spliterator<T> mo150spliterator();
}
