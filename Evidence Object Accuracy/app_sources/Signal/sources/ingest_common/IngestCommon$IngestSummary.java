package ingest_common;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import com.mobilecoin.api.MobileCoinAPI$CompressedRistretto;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class IngestCommon$IngestSummary extends GeneratedMessageLite<IngestCommon$IngestSummary, Builder> implements MessageLiteOrBuilder {
    private static final IngestCommon$IngestSummary DEFAULT_INSTANCE;
    public static final int EGRESS_PUBKEY_FIELD_NUMBER;
    public static final int INGEST_INVOCATION_ID_FIELD_NUMBER;
    public static final int INGRESS_PUBKEY_FIELD_NUMBER;
    public static final int KEX_RNG_VERSION_FIELD_NUMBER;
    public static final int MODE_FIELD_NUMBER;
    public static final int NEXT_BLOCK_INDEX_FIELD_NUMBER;
    private static volatile Parser<IngestCommon$IngestSummary> PARSER;
    public static final int PEERS_FIELD_NUMBER;
    public static final int PUBKEY_EXPIRY_WINDOW_FIELD_NUMBER;
    private ByteString egressPubkey_ = ByteString.EMPTY;
    private long ingestInvocationId_;
    private MobileCoinAPI$CompressedRistretto ingressPubkey_;
    private int kexRngVersion_;
    private int mode_;
    private long nextBlockIndex_;
    private Internal.ProtobufList<String> peers_ = GeneratedMessageLite.emptyProtobufList();
    private long pubkeyExpiryWindow_;

    private IngestCommon$IngestSummary() {
    }

    public int getModeValue() {
        return this.mode_;
    }

    public IngestCommon$IngestControllerMode getMode() {
        IngestCommon$IngestControllerMode forNumber = IngestCommon$IngestControllerMode.forNumber(this.mode_);
        return forNumber == null ? IngestCommon$IngestControllerMode.UNRECOGNIZED : forNumber;
    }

    public void setModeValue(int i) {
        this.mode_ = i;
    }

    public void setMode(IngestCommon$IngestControllerMode ingestCommon$IngestControllerMode) {
        ingestCommon$IngestControllerMode.getClass();
        this.mode_ = ingestCommon$IngestControllerMode.getNumber();
    }

    public void clearMode() {
        this.mode_ = 0;
    }

    public long getNextBlockIndex() {
        return this.nextBlockIndex_;
    }

    public void setNextBlockIndex(long j) {
        this.nextBlockIndex_ = j;
    }

    public void clearNextBlockIndex() {
        this.nextBlockIndex_ = 0;
    }

    public long getPubkeyExpiryWindow() {
        return this.pubkeyExpiryWindow_;
    }

    public void setPubkeyExpiryWindow(long j) {
        this.pubkeyExpiryWindow_ = j;
    }

    public void clearPubkeyExpiryWindow() {
        this.pubkeyExpiryWindow_ = 0;
    }

    public boolean hasIngressPubkey() {
        return this.ingressPubkey_ != null;
    }

    public MobileCoinAPI$CompressedRistretto getIngressPubkey() {
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto = this.ingressPubkey_;
        return mobileCoinAPI$CompressedRistretto == null ? MobileCoinAPI$CompressedRistretto.getDefaultInstance() : mobileCoinAPI$CompressedRistretto;
    }

    public void setIngressPubkey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        this.ingressPubkey_ = mobileCoinAPI$CompressedRistretto;
    }

    public void setIngressPubkey(MobileCoinAPI$CompressedRistretto.Builder builder) {
        this.ingressPubkey_ = builder.build();
    }

    public void mergeIngressPubkey(MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto) {
        mobileCoinAPI$CompressedRistretto.getClass();
        MobileCoinAPI$CompressedRistretto mobileCoinAPI$CompressedRistretto2 = this.ingressPubkey_;
        if (mobileCoinAPI$CompressedRistretto2 == null || mobileCoinAPI$CompressedRistretto2 == MobileCoinAPI$CompressedRistretto.getDefaultInstance()) {
            this.ingressPubkey_ = mobileCoinAPI$CompressedRistretto;
        } else {
            this.ingressPubkey_ = MobileCoinAPI$CompressedRistretto.newBuilder(this.ingressPubkey_).mergeFrom((MobileCoinAPI$CompressedRistretto.Builder) mobileCoinAPI$CompressedRistretto).buildPartial();
        }
    }

    public void clearIngressPubkey() {
        this.ingressPubkey_ = null;
    }

    public ByteString getEgressPubkey() {
        return this.egressPubkey_;
    }

    public void setEgressPubkey(ByteString byteString) {
        byteString.getClass();
        this.egressPubkey_ = byteString;
    }

    public void clearEgressPubkey() {
        this.egressPubkey_ = getDefaultInstance().getEgressPubkey();
    }

    public int getKexRngVersion() {
        return this.kexRngVersion_;
    }

    public void setKexRngVersion(int i) {
        this.kexRngVersion_ = i;
    }

    public void clearKexRngVersion() {
        this.kexRngVersion_ = 0;
    }

    public List<String> getPeersList() {
        return this.peers_;
    }

    public int getPeersCount() {
        return this.peers_.size();
    }

    public String getPeers(int i) {
        return this.peers_.get(i);
    }

    public ByteString getPeersBytes(int i) {
        return ByteString.copyFromUtf8(this.peers_.get(i));
    }

    private void ensurePeersIsMutable() {
        if (!this.peers_.isModifiable()) {
            this.peers_ = GeneratedMessageLite.mutableCopy(this.peers_);
        }
    }

    public void setPeers(int i, String str) {
        str.getClass();
        ensurePeersIsMutable();
        this.peers_.set(i, str);
    }

    public void addPeers(String str) {
        str.getClass();
        ensurePeersIsMutable();
        this.peers_.add(str);
    }

    public void addAllPeers(Iterable<String> iterable) {
        ensurePeersIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.peers_);
    }

    public void clearPeers() {
        this.peers_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void addPeersBytes(ByteString byteString) {
        byteString.getClass();
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        ensurePeersIsMutable();
        this.peers_.add(byteString.toStringUtf8());
    }

    public long getIngestInvocationId() {
        return this.ingestInvocationId_;
    }

    public void setIngestInvocationId(long j) {
        this.ingestInvocationId_ = j;
    }

    public void clearIngestInvocationId() {
        this.ingestInvocationId_ = 0;
    }

    public static IngestCommon$IngestSummary parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (IngestCommon$IngestSummary) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static IngestCommon$IngestSummary parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (IngestCommon$IngestSummary) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static IngestCommon$IngestSummary parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (IngestCommon$IngestSummary) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static IngestCommon$IngestSummary parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (IngestCommon$IngestSummary) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static IngestCommon$IngestSummary parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (IngestCommon$IngestSummary) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static IngestCommon$IngestSummary parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (IngestCommon$IngestSummary) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static IngestCommon$IngestSummary parseFrom(InputStream inputStream) throws IOException {
        return (IngestCommon$IngestSummary) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static IngestCommon$IngestSummary parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (IngestCommon$IngestSummary) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static IngestCommon$IngestSummary parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (IngestCommon$IngestSummary) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static IngestCommon$IngestSummary parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (IngestCommon$IngestSummary) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static IngestCommon$IngestSummary parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (IngestCommon$IngestSummary) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static IngestCommon$IngestSummary parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (IngestCommon$IngestSummary) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(IngestCommon$IngestSummary ingestCommon$IngestSummary) {
        return DEFAULT_INSTANCE.createBuilder(ingestCommon$IngestSummary);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<IngestCommon$IngestSummary, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(IngestCommon$1 ingestCommon$1) {
            this();
        }

        private Builder() {
            super(IngestCommon$IngestSummary.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (IngestCommon$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new IngestCommon$IngestSummary();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\b\u0000\u0000\u0001\b\b\u0000\u0001\u0000\u0001\f\u0002\u0003\u0003\u0003\u0004\t\u0005\n\u0006\u000b\u0007Ț\b\u0002", new Object[]{"mode_", "nextBlockIndex_", "pubkeyExpiryWindow_", "ingressPubkey_", "egressPubkey_", "kexRngVersion_", "peers_", "ingestInvocationId_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<IngestCommon$IngestSummary> parser = PARSER;
                if (parser == null) {
                    synchronized (IngestCommon$IngestSummary.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        IngestCommon$IngestSummary ingestCommon$IngestSummary = new IngestCommon$IngestSummary();
        DEFAULT_INSTANCE = ingestCommon$IngestSummary;
        GeneratedMessageLite.registerDefaultInstance(IngestCommon$IngestSummary.class, ingestCommon$IngestSummary);
    }

    public static IngestCommon$IngestSummary getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<IngestCommon$IngestSummary> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
