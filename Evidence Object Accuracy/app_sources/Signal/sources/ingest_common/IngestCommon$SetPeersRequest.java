package ingest_common;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class IngestCommon$SetPeersRequest extends GeneratedMessageLite<IngestCommon$SetPeersRequest, Builder> implements MessageLiteOrBuilder {
    private static final IngestCommon$SetPeersRequest DEFAULT_INSTANCE;
    public static final int INGEST_PEER_URIS_FIELD_NUMBER;
    private static volatile Parser<IngestCommon$SetPeersRequest> PARSER;
    private Internal.ProtobufList<String> ingestPeerUris_ = GeneratedMessageLite.emptyProtobufList();

    private IngestCommon$SetPeersRequest() {
    }

    public List<String> getIngestPeerUrisList() {
        return this.ingestPeerUris_;
    }

    public int getIngestPeerUrisCount() {
        return this.ingestPeerUris_.size();
    }

    public String getIngestPeerUris(int i) {
        return this.ingestPeerUris_.get(i);
    }

    public ByteString getIngestPeerUrisBytes(int i) {
        return ByteString.copyFromUtf8(this.ingestPeerUris_.get(i));
    }

    private void ensureIngestPeerUrisIsMutable() {
        if (!this.ingestPeerUris_.isModifiable()) {
            this.ingestPeerUris_ = GeneratedMessageLite.mutableCopy(this.ingestPeerUris_);
        }
    }

    public void setIngestPeerUris(int i, String str) {
        str.getClass();
        ensureIngestPeerUrisIsMutable();
        this.ingestPeerUris_.set(i, str);
    }

    public void addIngestPeerUris(String str) {
        str.getClass();
        ensureIngestPeerUrisIsMutable();
        this.ingestPeerUris_.add(str);
    }

    public void addAllIngestPeerUris(Iterable<String> iterable) {
        ensureIngestPeerUrisIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.ingestPeerUris_);
    }

    public void clearIngestPeerUris() {
        this.ingestPeerUris_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void addIngestPeerUrisBytes(ByteString byteString) {
        byteString.getClass();
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        ensureIngestPeerUrisIsMutable();
        this.ingestPeerUris_.add(byteString.toStringUtf8());
    }

    public static IngestCommon$SetPeersRequest parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (IngestCommon$SetPeersRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static IngestCommon$SetPeersRequest parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (IngestCommon$SetPeersRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static IngestCommon$SetPeersRequest parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (IngestCommon$SetPeersRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static IngestCommon$SetPeersRequest parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (IngestCommon$SetPeersRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static IngestCommon$SetPeersRequest parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (IngestCommon$SetPeersRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static IngestCommon$SetPeersRequest parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (IngestCommon$SetPeersRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static IngestCommon$SetPeersRequest parseFrom(InputStream inputStream) throws IOException {
        return (IngestCommon$SetPeersRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static IngestCommon$SetPeersRequest parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (IngestCommon$SetPeersRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static IngestCommon$SetPeersRequest parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (IngestCommon$SetPeersRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static IngestCommon$SetPeersRequest parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (IngestCommon$SetPeersRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static IngestCommon$SetPeersRequest parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (IngestCommon$SetPeersRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static IngestCommon$SetPeersRequest parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (IngestCommon$SetPeersRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(IngestCommon$SetPeersRequest ingestCommon$SetPeersRequest) {
        return DEFAULT_INSTANCE.createBuilder(ingestCommon$SetPeersRequest);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<IngestCommon$SetPeersRequest, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(IngestCommon$1 ingestCommon$1) {
            this();
        }

        private Builder() {
            super(IngestCommon$SetPeersRequest.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (IngestCommon$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new IngestCommon$SetPeersRequest();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001Ț", new Object[]{"ingestPeerUris_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<IngestCommon$SetPeersRequest> parser = PARSER;
                if (parser == null) {
                    synchronized (IngestCommon$SetPeersRequest.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        IngestCommon$SetPeersRequest ingestCommon$SetPeersRequest = new IngestCommon$SetPeersRequest();
        DEFAULT_INSTANCE = ingestCommon$SetPeersRequest;
        GeneratedMessageLite.registerDefaultInstance(IngestCommon$SetPeersRequest.class, ingestCommon$SetPeersRequest);
    }

    public static IngestCommon$SetPeersRequest getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<IngestCommon$SetPeersRequest> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
