package ingest_common;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import ingest_common.IngestCommon$IngestSummary;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class IngestCommon$IngestStateFile extends GeneratedMessageLite<IngestCommon$IngestStateFile, Builder> implements MessageLiteOrBuilder {
    private static final IngestCommon$IngestStateFile DEFAULT_INSTANCE;
    private static volatile Parser<IngestCommon$IngestStateFile> PARSER;
    public static final int SEALED_INGRESS_KEY_FIELD_NUMBER;
    public static final int SUMMARY_FIELD_NUMBER;
    private ByteString sealedIngressKey_ = ByteString.EMPTY;
    private IngestCommon$IngestSummary summary_;

    private IngestCommon$IngestStateFile() {
    }

    public boolean hasSummary() {
        return this.summary_ != null;
    }

    public IngestCommon$IngestSummary getSummary() {
        IngestCommon$IngestSummary ingestCommon$IngestSummary = this.summary_;
        return ingestCommon$IngestSummary == null ? IngestCommon$IngestSummary.getDefaultInstance() : ingestCommon$IngestSummary;
    }

    public void setSummary(IngestCommon$IngestSummary ingestCommon$IngestSummary) {
        ingestCommon$IngestSummary.getClass();
        this.summary_ = ingestCommon$IngestSummary;
    }

    public void setSummary(IngestCommon$IngestSummary.Builder builder) {
        this.summary_ = builder.build();
    }

    public void mergeSummary(IngestCommon$IngestSummary ingestCommon$IngestSummary) {
        ingestCommon$IngestSummary.getClass();
        IngestCommon$IngestSummary ingestCommon$IngestSummary2 = this.summary_;
        if (ingestCommon$IngestSummary2 == null || ingestCommon$IngestSummary2 == IngestCommon$IngestSummary.getDefaultInstance()) {
            this.summary_ = ingestCommon$IngestSummary;
        } else {
            this.summary_ = IngestCommon$IngestSummary.newBuilder(this.summary_).mergeFrom((IngestCommon$IngestSummary.Builder) ingestCommon$IngestSummary).buildPartial();
        }
    }

    public void clearSummary() {
        this.summary_ = null;
    }

    public ByteString getSealedIngressKey() {
        return this.sealedIngressKey_;
    }

    public void setSealedIngressKey(ByteString byteString) {
        byteString.getClass();
        this.sealedIngressKey_ = byteString;
    }

    public void clearSealedIngressKey() {
        this.sealedIngressKey_ = getDefaultInstance().getSealedIngressKey();
    }

    public static IngestCommon$IngestStateFile parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (IngestCommon$IngestStateFile) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static IngestCommon$IngestStateFile parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (IngestCommon$IngestStateFile) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static IngestCommon$IngestStateFile parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (IngestCommon$IngestStateFile) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static IngestCommon$IngestStateFile parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (IngestCommon$IngestStateFile) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static IngestCommon$IngestStateFile parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (IngestCommon$IngestStateFile) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static IngestCommon$IngestStateFile parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (IngestCommon$IngestStateFile) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static IngestCommon$IngestStateFile parseFrom(InputStream inputStream) throws IOException {
        return (IngestCommon$IngestStateFile) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static IngestCommon$IngestStateFile parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (IngestCommon$IngestStateFile) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static IngestCommon$IngestStateFile parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (IngestCommon$IngestStateFile) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static IngestCommon$IngestStateFile parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (IngestCommon$IngestStateFile) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static IngestCommon$IngestStateFile parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (IngestCommon$IngestStateFile) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static IngestCommon$IngestStateFile parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (IngestCommon$IngestStateFile) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(IngestCommon$IngestStateFile ingestCommon$IngestStateFile) {
        return DEFAULT_INSTANCE.createBuilder(ingestCommon$IngestStateFile);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<IngestCommon$IngestStateFile, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(IngestCommon$1 ingestCommon$1) {
            this();
        }

        private Builder() {
            super(IngestCommon$IngestStateFile.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (IngestCommon$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new IngestCommon$IngestStateFile();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\t\u0002\n", new Object[]{"summary_", "sealedIngressKey_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<IngestCommon$IngestStateFile> parser = PARSER;
                if (parser == null) {
                    synchronized (IngestCommon$IngestStateFile.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        IngestCommon$IngestStateFile ingestCommon$IngestStateFile = new IngestCommon$IngestStateFile();
        DEFAULT_INSTANCE = ingestCommon$IngestStateFile;
        GeneratedMessageLite.registerDefaultInstance(IngestCommon$IngestStateFile.class, ingestCommon$IngestStateFile);
    }

    public static IngestCommon$IngestStateFile getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<IngestCommon$IngestStateFile> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
