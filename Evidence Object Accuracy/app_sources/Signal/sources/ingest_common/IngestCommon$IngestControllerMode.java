package ingest_common;

import com.google.protobuf.Internal;

/* loaded from: classes3.dex */
public enum IngestCommon$IngestControllerMode implements Internal.EnumLite {
    Idle(0),
    Active(1),
    UNRECOGNIZED(-1);
    
    private static final Internal.EnumLiteMap<IngestCommon$IngestControllerMode> internalValueMap = new Internal.EnumLiteMap<IngestCommon$IngestControllerMode>() { // from class: ingest_common.IngestCommon$IngestControllerMode.1
        @Override // com.google.protobuf.Internal.EnumLiteMap
        public IngestCommon$IngestControllerMode findValueByNumber(int i) {
            return IngestCommon$IngestControllerMode.forNumber(i);
        }
    };
    private final int value;

    @Override // com.google.protobuf.Internal.EnumLite
    public final int getNumber() {
        if (this != UNRECOGNIZED) {
            return this.value;
        }
        throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
    }

    public static IngestCommon$IngestControllerMode forNumber(int i) {
        if (i == 0) {
            return Idle;
        }
        if (i != 1) {
            return null;
        }
        return Active;
    }

    IngestCommon$IngestControllerMode(int i) {
        this.value = i;
    }
}
