package kotlin.reflect.jvm.internal.impl.types;

import java.util.Collection;
import java.util.List;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.LazyThreadSafetyMode;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.reflect.jvm.internal.impl.builtins.KotlinBuiltIns;
import kotlin.reflect.jvm.internal.impl.descriptors.ClassifierDescriptor;
import kotlin.reflect.jvm.internal.impl.descriptors.SupertypeLoopChecker;
import kotlin.reflect.jvm.internal.impl.descriptors.TypeParameterDescriptor;
import kotlin.reflect.jvm.internal.impl.storage.NotNullLazyValue;
import kotlin.reflect.jvm.internal.impl.storage.StorageManager;
import kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor;
import kotlin.reflect.jvm.internal.impl.types.checker.KotlinTypeRefiner;

/* compiled from: AbstractTypeConstructor.kt */
/* loaded from: classes3.dex */
public abstract class AbstractTypeConstructor extends ClassifierBasedTypeConstructor {
    private final boolean shouldReportCyclicScopeWithCompanionWarning;
    private final NotNullLazyValue<Supertypes> supertypes;

    /* access modifiers changed from: protected */
    public abstract Collection<KotlinType> computeSupertypes();

    public KotlinType defaultSupertypeIfEmpty() {
        return null;
    }

    /* access modifiers changed from: protected */
    public abstract SupertypeLoopChecker getSupertypeLoopChecker();

    public List<KotlinType> processSupertypesWithoutCycles(List<KotlinType> list) {
        Intrinsics.checkNotNullParameter(list, "supertypes");
        return list;
    }

    public void reportScopesLoopError(KotlinType kotlinType) {
        Intrinsics.checkNotNullParameter(kotlinType, "type");
    }

    public void reportSupertypeLoopError(KotlinType kotlinType) {
        Intrinsics.checkNotNullParameter(kotlinType, "type");
    }

    public AbstractTypeConstructor(StorageManager storageManager) {
        Intrinsics.checkNotNullParameter(storageManager, "storageManager");
        this.supertypes = storageManager.createLazyValueWithPostCompute(new Function0<Supertypes>(this) { // from class: kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$1
            final /* synthetic */ AbstractTypeConstructor this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final AbstractTypeConstructor.Supertypes invoke() {
                return new AbstractTypeConstructor.Supertypes(this.this$0.computeSupertypes());
            }
        }, AbstractTypeConstructor$supertypes$2.INSTANCE, new Function1<Supertypes, Unit>(this) { // from class: kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3
            final /* synthetic */ AbstractTypeConstructor this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(AbstractTypeConstructor.Supertypes supertypes) {
                invoke(supertypes);
                return Unit.INSTANCE;
            }

            /*  JADX ERROR: Method code generation error
                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x001f: INVOKE  (r0v3 'findLoopsInSupertypesAndDisconnect' java.util.Collection<kotlin.reflect.jvm.internal.impl.types.KotlinType>) = 
                  (wrap: kotlin.reflect.jvm.internal.impl.descriptors.SupertypeLoopChecker : 0x0007: INVOKE  (r0v2 kotlin.reflect.jvm.internal.impl.descriptors.SupertypeLoopChecker A[REMOVE]) = 
                  (wrap: kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor : 0x0005: IGET  (r0v1 kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor A[REMOVE]) = (r7v0 'this' kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3 A[IMMUTABLE_TYPE, THIS]) kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3.this$0 kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor)
                 type: VIRTUAL call: kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor.getSupertypeLoopChecker():kotlin.reflect.jvm.internal.impl.descriptors.SupertypeLoopChecker)
                  (wrap: kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor : 0x000b: IGET  (r1v0 kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor A[REMOVE]) = (r7v0 'this' kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3 A[IMMUTABLE_TYPE, THIS]) kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3.this$0 kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor)
                  (wrap: java.util.Collection<kotlin.reflect.jvm.internal.impl.types.KotlinType> : 0x000d: INVOKE  (r2v0 java.util.Collection<kotlin.reflect.jvm.internal.impl.types.KotlinType> A[REMOVE]) = (r8v0 'supertypes' kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$Supertypes) type: VIRTUAL call: kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor.Supertypes.getAllSupertypes():java.util.Collection)
                  (wrap: kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3$resultWithoutCycles$1 : 0x0015: CONSTRUCTOR  (r3v0 kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3$resultWithoutCycles$1 A[REMOVE]) = 
                  (wrap: kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor : 0x0013: IGET  (r4v0 kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor A[REMOVE]) = (r7v0 'this' kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3 A[IMMUTABLE_TYPE, THIS]) kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3.this$0 kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor)
                 call: kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3$resultWithoutCycles$1.<init>(kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor):void type: CONSTRUCTOR)
                  (wrap: kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3$resultWithoutCycles$2 : 0x001c: CONSTRUCTOR  (r4v1 kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3$resultWithoutCycles$2 A[REMOVE]) = 
                  (wrap: kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor : 0x001a: IGET  (r5v0 kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor A[REMOVE]) = (r7v0 'this' kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3 A[IMMUTABLE_TYPE, THIS]) kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3.this$0 kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor)
                 call: kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3$resultWithoutCycles$2.<init>(kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor):void type: CONSTRUCTOR)
                 type: INTERFACE call: kotlin.reflect.jvm.internal.impl.descriptors.SupertypeLoopChecker.findLoopsInSupertypesAndDisconnect(kotlin.reflect.jvm.internal.impl.types.TypeConstructor, java.util.Collection, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1):java.util.Collection in method: kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3.invoke(kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$Supertypes):void, file: classes3.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0015: CONSTRUCTOR  (r3v0 kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3$resultWithoutCycles$1 A[REMOVE]) = 
                  (wrap: kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor : 0x0013: IGET  (r4v0 kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor A[REMOVE]) = (r7v0 'this' kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3 A[IMMUTABLE_TYPE, THIS]) kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3.this$0 kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor)
                 call: kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3$resultWithoutCycles$1.<init>(kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor):void type: CONSTRUCTOR in method: kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3.invoke(kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$Supertypes):void, file: classes3.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                	... 10 more
                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3$resultWithoutCycles$1, state: NOT_LOADED
                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                	... 16 more
                */
            public final void invoke(kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor.Supertypes r8) {
                /*
                    r7 = this;
                    java.lang.String r0 = "supertypes"
                    kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r8, r0)
                    kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor r0 = r7.this$0
                    kotlin.reflect.jvm.internal.impl.descriptors.SupertypeLoopChecker r0 = r0.getSupertypeLoopChecker()
                    kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor r1 = r7.this$0
                    java.util.Collection r2 = r8.getAllSupertypes()
                    kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3$resultWithoutCycles$1 r3 = new kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3$resultWithoutCycles$1
                    kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor r4 = r7.this$0
                    r3.<init>(r4)
                    kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3$resultWithoutCycles$2 r4 = new kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3$resultWithoutCycles$2
                    kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor r5 = r7.this$0
                    r4.<init>(r5)
                    java.util.Collection r0 = r0.findLoopsInSupertypesAndDisconnect(r1, r2, r3, r4)
                    boolean r1 = r0.isEmpty()
                    r2 = 0
                    if (r1 == 0) goto L_0x003e
                    kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor r0 = r7.this$0
                    kotlin.reflect.jvm.internal.impl.types.KotlinType r0 = r0.defaultSupertypeIfEmpty()
                    if (r0 == 0) goto L_0x0037
                    java.util.List r0 = kotlin.collections.CollectionsKt.listOf(r0)
                    goto L_0x0038
                L_0x0037:
                    r0 = r2
                L_0x0038:
                    if (r0 != 0) goto L_0x003e
                    java.util.List r0 = kotlin.collections.CollectionsKt.emptyList()
                L_0x003e:
                    kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor r1 = r7.this$0
                    boolean r1 = r1.getShouldReportCyclicScopeWithCompanionWarning()
                    if (r1 == 0) goto L_0x005d
                    kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor r1 = r7.this$0
                    kotlin.reflect.jvm.internal.impl.descriptors.SupertypeLoopChecker r1 = r1.getSupertypeLoopChecker()
                    kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor r3 = r7.this$0
                    kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3$2 r4 = new kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3$2
                    r4.<init>(r3)
                    kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3$3 r5 = new kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3$3
                    kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor r6 = r7.this$0
                    r5.<init>(r6)
                    r1.findLoopsInSupertypesAndDisconnect(r3, r0, r4, r5)
                L_0x005d:
                    kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor r1 = r7.this$0
                    boolean r3 = r0 instanceof java.util.List
                    if (r3 == 0) goto L_0x0066
                    r2 = r0
                    java.util.List r2 = (java.util.List) r2
                L_0x0066:
                    if (r2 != 0) goto L_0x006c
                    java.util.List r2 = kotlin.collections.CollectionsKt.toList(r0)
                L_0x006c:
                    java.util.List r0 = r1.processSupertypesWithoutCycles(r2)
                    r8.setSupertypesWithoutCycles(r0)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$supertypes$3.invoke(kotlin.reflect.jvm.internal.impl.types.AbstractTypeConstructor$Supertypes):void");
            }
        });
    }

    @Override // kotlin.reflect.jvm.internal.impl.types.TypeConstructor
    /* renamed from: getSupertypes */
    public List<KotlinType> mo169getSupertypes() {
        return this.supertypes.invoke().getSupertypesWithoutCycles();
    }

    @Override // kotlin.reflect.jvm.internal.impl.types.TypeConstructor
    public TypeConstructor refine(KotlinTypeRefiner kotlinTypeRefiner) {
        Intrinsics.checkNotNullParameter(kotlinTypeRefiner, "kotlinTypeRefiner");
        return new ModuleViewTypeConstructor(this, kotlinTypeRefiner);
    }

    /* compiled from: AbstractTypeConstructor.kt */
    /* loaded from: classes3.dex */
    public final class ModuleViewTypeConstructor implements TypeConstructor {
        private final KotlinTypeRefiner kotlinTypeRefiner;
        private final Lazy refinedSupertypes$delegate;
        final /* synthetic */ AbstractTypeConstructor this$0;

        public ModuleViewTypeConstructor(AbstractTypeConstructor abstractTypeConstructor, KotlinTypeRefiner kotlinTypeRefiner) {
            Intrinsics.checkNotNullParameter(kotlinTypeRefiner, "kotlinTypeRefiner");
            this.this$0 = abstractTypeConstructor;
            this.kotlinTypeRefiner = kotlinTypeRefiner;
            this.refinedSupertypes$delegate = LazyKt__LazyJVMKt.lazy(LazyThreadSafetyMode.PUBLICATION, new AbstractTypeConstructor$ModuleViewTypeConstructor$refinedSupertypes$2(this, abstractTypeConstructor));
        }

        private final List<KotlinType> getRefinedSupertypes() {
            return (List) this.refinedSupertypes$delegate.getValue();
        }

        @Override // kotlin.reflect.jvm.internal.impl.types.TypeConstructor
        public List<TypeParameterDescriptor> getParameters() {
            List<TypeParameterDescriptor> parameters = this.this$0.getParameters();
            Intrinsics.checkNotNullExpressionValue(parameters, "this@AbstractTypeConstructor.parameters");
            return parameters;
        }

        @Override // kotlin.reflect.jvm.internal.impl.types.TypeConstructor
        /* renamed from: getSupertypes */
        public List<KotlinType> mo169getSupertypes() {
            return getRefinedSupertypes();
        }

        @Override // kotlin.reflect.jvm.internal.impl.types.TypeConstructor
        public boolean isDenotable() {
            return this.this$0.isDenotable();
        }

        @Override // kotlin.reflect.jvm.internal.impl.types.TypeConstructor
        public ClassifierDescriptor getDeclarationDescriptor() {
            return this.this$0.getDeclarationDescriptor();
        }

        @Override // kotlin.reflect.jvm.internal.impl.types.TypeConstructor
        public KotlinBuiltIns getBuiltIns() {
            KotlinBuiltIns builtIns = this.this$0.getBuiltIns();
            Intrinsics.checkNotNullExpressionValue(builtIns, "this@AbstractTypeConstructor.builtIns");
            return builtIns;
        }

        @Override // kotlin.reflect.jvm.internal.impl.types.TypeConstructor
        public TypeConstructor refine(KotlinTypeRefiner kotlinTypeRefiner) {
            Intrinsics.checkNotNullParameter(kotlinTypeRefiner, "kotlinTypeRefiner");
            return this.this$0.refine(kotlinTypeRefiner);
        }

        public boolean equals(Object obj) {
            return this.this$0.equals(obj);
        }

        public int hashCode() {
            return this.this$0.hashCode();
        }

        public String toString() {
            return this.this$0.toString();
        }
    }

    /* compiled from: AbstractTypeConstructor.kt */
    /* loaded from: classes3.dex */
    public static final class Supertypes {
        private final Collection<KotlinType> allSupertypes;
        private List<? extends KotlinType> supertypesWithoutCycles = CollectionsKt__CollectionsJVMKt.listOf(ErrorUtils.ERROR_TYPE_FOR_LOOP_IN_SUPERTYPES);

        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.Collection<? extends kotlin.reflect.jvm.internal.impl.types.KotlinType> */
        /* JADX WARN: Multi-variable type inference failed */
        public Supertypes(Collection<? extends KotlinType> collection) {
            Intrinsics.checkNotNullParameter(collection, "allSupertypes");
            this.allSupertypes = collection;
        }

        public final Collection<KotlinType> getAllSupertypes() {
            return this.allSupertypes;
        }

        /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.util.List<? extends kotlin.reflect.jvm.internal.impl.types.KotlinType>, java.util.List<kotlin.reflect.jvm.internal.impl.types.KotlinType> */
        public final List<KotlinType> getSupertypesWithoutCycles() {
            return this.supertypesWithoutCycles;
        }

        public final void setSupertypesWithoutCycles(List<? extends KotlinType> list) {
            Intrinsics.checkNotNullParameter(list, "<set-?>");
            this.supertypesWithoutCycles = list;
        }
    }

    public final Collection<KotlinType> computeNeighbours(TypeConstructor typeConstructor, boolean z) {
        List list;
        AbstractTypeConstructor abstractTypeConstructor = typeConstructor instanceof AbstractTypeConstructor ? (AbstractTypeConstructor) typeConstructor : null;
        if (abstractTypeConstructor != null && (list = CollectionsKt___CollectionsKt.plus((Collection) abstractTypeConstructor.supertypes.invoke().getAllSupertypes(), (Iterable) abstractTypeConstructor.getAdditionalNeighboursInSupertypeGraph(z))) != null) {
            return list;
        }
        Collection<KotlinType> supertypes = typeConstructor.mo169getSupertypes();
        Intrinsics.checkNotNullExpressionValue(supertypes, "supertypes");
        return supertypes;
    }

    public boolean getShouldReportCyclicScopeWithCompanionWarning() {
        return this.shouldReportCyclicScopeWithCompanionWarning;
    }

    protected Collection<KotlinType> getAdditionalNeighboursInSupertypeGraph(boolean z) {
        return CollectionsKt__CollectionsKt.emptyList();
    }
}
