package kotlin.reflect.jvm.internal.impl.name;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.MapsKt__MapsJVMKt;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.collections.SetsKt___SetsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.RangesKt___RangesKt;

/* compiled from: StandardClassIds.kt */
/* loaded from: classes3.dex */
public final class StandardClassIds {
    private static final ClassId Annotation = StandardClassIdsKt.access$baseId("Annotation");
    private static final ClassId AnnotationRetention = StandardClassIdsKt.access$annotationId("AnnotationRetention");
    private static final ClassId AnnotationTarget = StandardClassIdsKt.access$annotationId("AnnotationTarget");
    private static final ClassId Any = StandardClassIdsKt.access$baseId("Any");
    private static final ClassId Array = StandardClassIdsKt.access$baseId("Array");
    private static final FqName BASE_ANNOTATION_PACKAGE;
    private static final FqName BASE_COLLECTIONS_PACKAGE;
    private static final FqName BASE_COROUTINES_PACKAGE;
    private static final FqName BASE_INTERNAL_IR_PACKAGE;
    private static final FqName BASE_INTERNAL_PACKAGE;
    private static final FqName BASE_JVM_INTERNAL_PACKAGE;
    private static final FqName BASE_JVM_PACKAGE;
    private static final FqName BASE_KOTLIN_PACKAGE;
    private static final FqName BASE_RANGES_PACKAGE;
    private static final FqName BASE_REFLECT_PACKAGE;
    private static final ClassId Boolean;
    private static final ClassId Byte;
    private static final ClassId Char;
    private static final ClassId CharRange = StandardClassIdsKt.access$rangesId("CharRange");
    private static final ClassId Cloneable = StandardClassIdsKt.access$baseId("Cloneable");
    private static final ClassId Collection = StandardClassIdsKt.access$collectionsId("Collection");
    private static final ClassId Comparable = StandardClassIdsKt.access$baseId("Comparable");
    private static final ClassId Continuation = StandardClassIdsKt.access$coroutinesId("Continuation");
    private static final ClassId Double;
    private static final ClassId Enum = StandardClassIdsKt.access$baseId("Enum");
    private static final ClassId Float;
    private static final ClassId Function = StandardClassIdsKt.access$baseId("Function");
    public static final StandardClassIds INSTANCE = new StandardClassIds();
    private static final ClassId Int;
    private static final ClassId IntRange = StandardClassIdsKt.access$rangesId("IntRange");
    private static final ClassId Iterable = StandardClassIdsKt.access$collectionsId("Iterable");
    private static final ClassId Iterator = StandardClassIdsKt.access$collectionsId("Iterator");
    private static final ClassId KCallable = StandardClassIdsKt.access$reflectId("KCallable");
    private static final ClassId KClass = StandardClassIdsKt.access$reflectId("KClass");
    private static final ClassId KFunction = StandardClassIdsKt.access$reflectId("KFunction");
    private static final ClassId KMutableProperty = StandardClassIdsKt.access$reflectId("KMutableProperty");
    private static final ClassId KMutableProperty0 = StandardClassIdsKt.access$reflectId("KMutableProperty0");
    private static final ClassId KMutableProperty1 = StandardClassIdsKt.access$reflectId("KMutableProperty1");
    private static final ClassId KMutableProperty2 = StandardClassIdsKt.access$reflectId("KMutableProperty2");
    private static final ClassId KProperty = StandardClassIdsKt.access$reflectId("KProperty");
    private static final ClassId KProperty0 = StandardClassIdsKt.access$reflectId("KProperty0");
    private static final ClassId KProperty1 = StandardClassIdsKt.access$reflectId("KProperty1");
    private static final ClassId KProperty2 = StandardClassIdsKt.access$reflectId("KProperty2");
    private static final ClassId List = StandardClassIdsKt.access$collectionsId("List");
    private static final ClassId ListIterator = StandardClassIdsKt.access$collectionsId("ListIterator");
    private static final ClassId Long;
    private static final ClassId LongRange = StandardClassIdsKt.access$rangesId("LongRange");
    private static final ClassId Map;
    private static final ClassId MapEntry;
    private static final ClassId MutableCollection = StandardClassIdsKt.access$collectionsId("MutableCollection");
    private static final ClassId MutableIterable = StandardClassIdsKt.access$collectionsId("MutableIterable");
    private static final ClassId MutableIterator = StandardClassIdsKt.access$collectionsId("MutableIterator");
    private static final ClassId MutableList = StandardClassIdsKt.access$collectionsId("MutableList");
    private static final ClassId MutableListIterator = StandardClassIdsKt.access$collectionsId("MutableListIterator");
    private static final ClassId MutableMap;
    private static final ClassId MutableMapEntry;
    private static final ClassId MutableSet = StandardClassIdsKt.access$collectionsId("MutableSet");
    private static final ClassId Nothing = StandardClassIdsKt.access$baseId("Nothing");
    private static final ClassId Number = StandardClassIdsKt.access$baseId("Number");
    private static final ClassId Result = StandardClassIdsKt.access$baseId("Result");
    private static final ClassId Set = StandardClassIdsKt.access$collectionsId("Set");
    private static final ClassId Short;
    private static final ClassId String = StandardClassIdsKt.access$baseId("String");
    private static final ClassId Throwable = StandardClassIdsKt.access$baseId("Throwable");
    private static final ClassId UByte;
    private static final ClassId UInt;
    private static final ClassId ULong;
    private static final ClassId UShort;
    private static final ClassId Unit = StandardClassIdsKt.access$baseId("Unit");
    private static final Set<FqName> builtInsPackages;
    private static final Set<ClassId> constantAllowedTypes = SetsKt___SetsKt.plus(SetsKt___SetsKt.plus((Set) primitiveTypes, (Iterable) unsignedTypes), String);
    private static final Map<ClassId, ClassId> elementTypeByPrimitiveArrayType;
    private static final Map<ClassId, ClassId> elementTypeByUnsignedArrayType;
    private static final Map<ClassId, ClassId> primitiveArrayTypeByElementType;
    private static final Set<ClassId> primitiveTypes;
    private static final Map<ClassId, ClassId> unsignedArrayTypeByElementType;
    private static final Set<ClassId> unsignedTypes;

    private StandardClassIds() {
    }

    static {
        INSTANCE = new StandardClassIds();
        FqName fqName = new FqName("kotlin");
        BASE_KOTLIN_PACKAGE = fqName;
        FqName child = fqName.child(Name.identifier("reflect"));
        Intrinsics.checkNotNullExpressionValue(child, "BASE_KOTLIN_PACKAGE.chil…me.identifier(\"reflect\"))");
        BASE_REFLECT_PACKAGE = child;
        FqName child2 = fqName.child(Name.identifier("collections"));
        Intrinsics.checkNotNullExpressionValue(child2, "BASE_KOTLIN_PACKAGE.chil…dentifier(\"collections\"))");
        BASE_COLLECTIONS_PACKAGE = child2;
        FqName child3 = fqName.child(Name.identifier("ranges"));
        Intrinsics.checkNotNullExpressionValue(child3, "BASE_KOTLIN_PACKAGE.chil…ame.identifier(\"ranges\"))");
        BASE_RANGES_PACKAGE = child3;
        FqName child4 = fqName.child(Name.identifier("jvm"));
        Intrinsics.checkNotNullExpressionValue(child4, "BASE_KOTLIN_PACKAGE.child(Name.identifier(\"jvm\"))");
        BASE_JVM_PACKAGE = child4;
        FqName child5 = child4.child(Name.identifier("internal"));
        Intrinsics.checkNotNullExpressionValue(child5, "BASE_JVM_PACKAGE.child(N…e.identifier(\"internal\"))");
        BASE_JVM_INTERNAL_PACKAGE = child5;
        FqName child6 = fqName.child(Name.identifier("annotation"));
        Intrinsics.checkNotNullExpressionValue(child6, "BASE_KOTLIN_PACKAGE.chil…identifier(\"annotation\"))");
        BASE_ANNOTATION_PACKAGE = child6;
        FqName child7 = fqName.child(Name.identifier("internal"));
        Intrinsics.checkNotNullExpressionValue(child7, "BASE_KOTLIN_PACKAGE.chil…e.identifier(\"internal\"))");
        BASE_INTERNAL_PACKAGE = child7;
        FqName child8 = child7.child(Name.identifier("ir"));
        Intrinsics.checkNotNullExpressionValue(child8, "BASE_INTERNAL_PACKAGE.child(Name.identifier(\"ir\"))");
        BASE_INTERNAL_IR_PACKAGE = child8;
        FqName child9 = fqName.child(Name.identifier("coroutines"));
        Intrinsics.checkNotNullExpressionValue(child9, "BASE_KOTLIN_PACKAGE.chil…identifier(\"coroutines\"))");
        BASE_COROUTINES_PACKAGE = child9;
        builtInsPackages = SetsKt__SetsKt.setOf((Object[]) new FqName[]{fqName, child2, child3, child6, child, child7, child9});
        Nothing = StandardClassIdsKt.baseId("Nothing");
        Unit = StandardClassIdsKt.baseId("Unit");
        Any = StandardClassIdsKt.baseId("Any");
        Enum = StandardClassIdsKt.baseId("Enum");
        Annotation = StandardClassIdsKt.baseId("Annotation");
        Array = StandardClassIdsKt.baseId("Array");
        ClassId classId = StandardClassIdsKt.baseId("Boolean");
        Boolean = classId;
        ClassId classId2 = StandardClassIdsKt.baseId("Char");
        Char = classId2;
        ClassId classId3 = StandardClassIdsKt.baseId("Byte");
        Byte = classId3;
        ClassId classId4 = StandardClassIdsKt.baseId("Short");
        Short = classId4;
        ClassId classId5 = StandardClassIdsKt.baseId("Int");
        Int = classId5;
        ClassId classId6 = StandardClassIdsKt.baseId("Long");
        Long = classId6;
        ClassId classId7 = StandardClassIdsKt.baseId("Float");
        Float = classId7;
        ClassId classId8 = StandardClassIdsKt.baseId("Double");
        Double = classId8;
        UByte = StandardClassIdsKt.unsignedId(classId3);
        UShort = StandardClassIdsKt.unsignedId(classId4);
        UInt = StandardClassIdsKt.unsignedId(classId5);
        ULong = StandardClassIdsKt.unsignedId(classId6);
        String = StandardClassIdsKt.baseId("String");
        Throwable = StandardClassIdsKt.baseId("Throwable");
        Cloneable = StandardClassIdsKt.baseId("Cloneable");
        KProperty = StandardClassIdsKt.reflectId("KProperty");
        KMutableProperty = StandardClassIdsKt.reflectId("KMutableProperty");
        KProperty0 = StandardClassIdsKt.reflectId("KProperty0");
        KMutableProperty0 = StandardClassIdsKt.reflectId("KMutableProperty0");
        KProperty1 = StandardClassIdsKt.reflectId("KProperty1");
        KMutableProperty1 = StandardClassIdsKt.reflectId("KMutableProperty1");
        KProperty2 = StandardClassIdsKt.reflectId("KProperty2");
        KMutableProperty2 = StandardClassIdsKt.reflectId("KMutableProperty2");
        KFunction = StandardClassIdsKt.reflectId("KFunction");
        KClass = StandardClassIdsKt.reflectId("KClass");
        KCallable = StandardClassIdsKt.reflectId("KCallable");
        Comparable = StandardClassIdsKt.baseId("Comparable");
        Number = StandardClassIdsKt.baseId("Number");
        Function = StandardClassIdsKt.baseId("Function");
        Set<ClassId> set = SetsKt__SetsKt.setOf((Object[]) new ClassId[]{classId, classId2, classId3, classId4, classId5, classId6, classId7, classId8});
        primitiveTypes = set;
        LinkedHashMap linkedHashMap = new LinkedHashMap(RangesKt___RangesKt.coerceAtLeast(MapsKt__MapsJVMKt.mapCapacity(CollectionsKt__IterablesKt.collectionSizeOrDefault(set, 10)), 16));
        for (Object obj : set) {
            Name shortClassName = ((ClassId) obj).getShortClassName();
            Intrinsics.checkNotNullExpressionValue(shortClassName, "id.shortClassName");
            linkedHashMap.put(obj, StandardClassIdsKt.primitiveArrayId(shortClassName));
        }
        primitiveArrayTypeByElementType = linkedHashMap;
        elementTypeByPrimitiveArrayType = StandardClassIdsKt.inverseMap(linkedHashMap);
        Set<ClassId> set2 = SetsKt__SetsKt.setOf((Object[]) new ClassId[]{UByte, UShort, UInt, ULong});
        unsignedTypes = set2;
        LinkedHashMap linkedHashMap2 = new LinkedHashMap(RangesKt___RangesKt.coerceAtLeast(MapsKt__MapsJVMKt.mapCapacity(CollectionsKt__IterablesKt.collectionSizeOrDefault(set2, 10)), 16));
        for (Object obj2 : set2) {
            Name shortClassName2 = ((ClassId) obj2).getShortClassName();
            Intrinsics.checkNotNullExpressionValue(shortClassName2, "id.shortClassName");
            linkedHashMap2.put(obj2, StandardClassIdsKt.primitiveArrayId(shortClassName2));
        }
        unsignedArrayTypeByElementType = linkedHashMap2;
        elementTypeByUnsignedArrayType = StandardClassIdsKt.inverseMap(linkedHashMap2);
        constantAllowedTypes = SetsKt___SetsKt.plus(SetsKt___SetsKt.plus((Set) primitiveTypes, (Iterable) unsignedTypes), String);
        Continuation = StandardClassIdsKt.coroutinesId("Continuation");
        Iterator = StandardClassIdsKt.collectionsId("Iterator");
        Iterable = StandardClassIdsKt.collectionsId("Iterable");
        Collection = StandardClassIdsKt.collectionsId("Collection");
        List = StandardClassIdsKt.collectionsId("List");
        ListIterator = StandardClassIdsKt.collectionsId("ListIterator");
        Set = StandardClassIdsKt.collectionsId("Set");
        ClassId classId9 = StandardClassIdsKt.collectionsId("Map");
        Map = classId9;
        MutableIterator = StandardClassIdsKt.collectionsId("MutableIterator");
        MutableIterable = StandardClassIdsKt.collectionsId("MutableIterable");
        MutableCollection = StandardClassIdsKt.collectionsId("MutableCollection");
        MutableList = StandardClassIdsKt.collectionsId("MutableList");
        MutableListIterator = StandardClassIdsKt.collectionsId("MutableListIterator");
        MutableSet = StandardClassIdsKt.collectionsId("MutableSet");
        ClassId classId10 = StandardClassIdsKt.collectionsId("MutableMap");
        MutableMap = classId10;
        ClassId createNestedClassId = classId9.createNestedClassId(Name.identifier("Entry"));
        Intrinsics.checkNotNullExpressionValue(createNestedClassId, "Map.createNestedClassId(Name.identifier(\"Entry\"))");
        MapEntry = createNestedClassId;
        ClassId createNestedClassId2 = classId10.createNestedClassId(Name.identifier("MutableEntry"));
        Intrinsics.checkNotNullExpressionValue(createNestedClassId2, "MutableMap.createNestedC…entifier(\"MutableEntry\"))");
        MutableMapEntry = createNestedClassId2;
        Result = StandardClassIdsKt.baseId("Result");
        IntRange = StandardClassIdsKt.rangesId("IntRange");
        LongRange = StandardClassIdsKt.rangesId("LongRange");
        CharRange = StandardClassIdsKt.rangesId("CharRange");
        AnnotationRetention = StandardClassIdsKt.annotationId("AnnotationRetention");
        AnnotationTarget = StandardClassIdsKt.annotationId("AnnotationTarget");
    }

    public final FqName getBASE_KOTLIN_PACKAGE() {
        return BASE_KOTLIN_PACKAGE;
    }

    public final FqName getBASE_REFLECT_PACKAGE() {
        return BASE_REFLECT_PACKAGE;
    }

    public final FqName getBASE_COLLECTIONS_PACKAGE() {
        return BASE_COLLECTIONS_PACKAGE;
    }

    public final FqName getBASE_RANGES_PACKAGE() {
        return BASE_RANGES_PACKAGE;
    }

    public final FqName getBASE_ANNOTATION_PACKAGE() {
        return BASE_ANNOTATION_PACKAGE;
    }

    public final FqName getBASE_COROUTINES_PACKAGE() {
        return BASE_COROUTINES_PACKAGE;
    }

    public final ClassId getArray() {
        return Array;
    }

    public final ClassId getKFunction() {
        return KFunction;
    }

    public final ClassId getKClass() {
        return KClass;
    }

    public final ClassId getMutableList() {
        return MutableList;
    }

    public final ClassId getMutableSet() {
        return MutableSet;
    }

    public final ClassId getMutableMap() {
        return MutableMap;
    }
}
