package kotlin.reflect.jvm.internal.impl.name;

/* compiled from: FqNamesUtil.kt */
/* loaded from: classes3.dex */
public enum State {
    BEGINNING,
    MIDDLE,
    AFTER_DOT
}
