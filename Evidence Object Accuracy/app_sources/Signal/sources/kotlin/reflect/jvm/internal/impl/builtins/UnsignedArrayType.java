package kotlin.reflect.jvm.internal.impl.builtins;

import kotlin.jvm.internal.Intrinsics;
import kotlin.reflect.jvm.internal.impl.name.ClassId;
import kotlin.reflect.jvm.internal.impl.name.Name;

/* compiled from: UnsignedType.kt */
/* JADX WARN: Init of enum UBYTEARRAY can be incorrect */
/* JADX WARN: Init of enum USHORTARRAY can be incorrect */
/* JADX WARN: Init of enum UINTARRAY can be incorrect */
/* JADX WARN: Init of enum ULONGARRAY can be incorrect */
/* loaded from: classes3.dex */
public enum UnsignedArrayType {
    UBYTEARRAY(r1),
    USHORTARRAY(r1),
    UINTARRAY(r1),
    ULONGARRAY(r1);
    
    private final ClassId classId;
    private final Name typeName;

    UnsignedArrayType(ClassId classId) {
        this.classId = classId;
        Name shortClassName = classId.getShortClassName();
        Intrinsics.checkNotNullExpressionValue(shortClassName, "classId.shortClassName");
        this.typeName = shortClassName;
    }

    static {
        Intrinsics.checkNotNullExpressionValue(ClassId.fromString("kotlin/UByteArray"), "fromString(\"kotlin/UByteArray\")");
        Intrinsics.checkNotNullExpressionValue(ClassId.fromString("kotlin/UShortArray"), "fromString(\"kotlin/UShortArray\")");
        Intrinsics.checkNotNullExpressionValue(ClassId.fromString("kotlin/UIntArray"), "fromString(\"kotlin/UIntArray\")");
        Intrinsics.checkNotNullExpressionValue(ClassId.fromString("kotlin/ULongArray"), "fromString(\"kotlin/ULongArray\")");
    }

    public final Name getTypeName() {
        return this.typeName;
    }
}
