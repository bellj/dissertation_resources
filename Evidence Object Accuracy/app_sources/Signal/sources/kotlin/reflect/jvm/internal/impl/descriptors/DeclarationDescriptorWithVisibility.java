package kotlin.reflect.jvm.internal.impl.descriptors;

/* loaded from: classes3.dex */
public interface DeclarationDescriptorWithVisibility extends DeclarationDescriptor {
    @Override // kotlin.reflect.jvm.internal.impl.descriptors.MemberDescriptor
    DescriptorVisibility getVisibility();
}
