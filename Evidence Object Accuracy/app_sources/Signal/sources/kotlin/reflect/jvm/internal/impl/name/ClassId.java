package kotlin.reflect.jvm.internal.impl.name;

/* loaded from: classes3.dex */
public final class ClassId {
    private final boolean local;
    private final FqName packageFqName;
    private final FqName relativeClassName;

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x009e A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00a7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static /* synthetic */ void $$$reportNull$$$0(int r10) {
        /*
            r0 = 9
            r1 = 7
            r2 = 6
            r3 = 5
            if (r10 == r3) goto L_0x0013
            if (r10 == r2) goto L_0x0013
            if (r10 == r1) goto L_0x0013
            if (r10 == r0) goto L_0x0013
            switch(r10) {
                case 13: goto L_0x0013;
                case 14: goto L_0x0013;
                case 15: goto L_0x0013;
                case 16: goto L_0x0013;
                default: goto L_0x0010;
            }
        L_0x0010:
            java.lang.String r4 = "Argument for @NotNull parameter '%s' of %s.%s must not be null"
            goto L_0x0015
        L_0x0013:
            java.lang.String r4 = "@NotNull method %s.%s must not return null"
        L_0x0015:
            r5 = 2
            if (r10 == r3) goto L_0x0023
            if (r10 == r2) goto L_0x0023
            if (r10 == r1) goto L_0x0023
            if (r10 == r0) goto L_0x0023
            switch(r10) {
                case 13: goto L_0x0023;
                case 14: goto L_0x0023;
                case 15: goto L_0x0023;
                case 16: goto L_0x0023;
                default: goto L_0x0021;
            }
        L_0x0021:
            r6 = 3
            goto L_0x0024
        L_0x0023:
            r6 = 2
        L_0x0024:
            java.lang.Object[] r6 = new java.lang.Object[r6]
            java.lang.String r7 = "kotlin/reflect/jvm/internal/impl/name/ClassId"
            r8 = 0
            switch(r10) {
                case 1: goto L_0x004d;
                case 2: goto L_0x0048;
                case 3: goto L_0x004d;
                case 4: goto L_0x0043;
                case 5: goto L_0x0040;
                case 6: goto L_0x0040;
                case 7: goto L_0x0040;
                case 8: goto L_0x003b;
                case 9: goto L_0x0040;
                case 10: goto L_0x0036;
                case 11: goto L_0x0031;
                case 12: goto L_0x0031;
                case 13: goto L_0x0040;
                case 14: goto L_0x0040;
                case 15: goto L_0x0040;
                case 16: goto L_0x0040;
                default: goto L_0x002c;
            }
        L_0x002c:
            java.lang.String r9 = "topLevelFqName"
            r6[r8] = r9
            goto L_0x0051
        L_0x0031:
            java.lang.String r9 = "string"
            r6[r8] = r9
            goto L_0x0051
        L_0x0036:
            java.lang.String r9 = "segment"
            r6[r8] = r9
            goto L_0x0051
        L_0x003b:
            java.lang.String r9 = "name"
            r6[r8] = r9
            goto L_0x0051
        L_0x0040:
            r6[r8] = r7
            goto L_0x0051
        L_0x0043:
            java.lang.String r9 = "topLevelName"
            r6[r8] = r9
            goto L_0x0051
        L_0x0048:
            java.lang.String r9 = "relativeClassName"
            r6[r8] = r9
            goto L_0x0051
        L_0x004d:
            java.lang.String r9 = "packageFqName"
            r6[r8] = r9
        L_0x0051:
            r8 = 1
            if (r10 == r3) goto L_0x0079
            if (r10 == r2) goto L_0x0074
            if (r10 == r1) goto L_0x006f
            if (r10 == r0) goto L_0x006a
            switch(r10) {
                case 13: goto L_0x0065;
                case 14: goto L_0x0065;
                case 15: goto L_0x0060;
                case 16: goto L_0x0060;
                default: goto L_0x005d;
            }
        L_0x005d:
            r6[r8] = r7
            goto L_0x007d
        L_0x0060:
            java.lang.String r7 = "asFqNameString"
            r6[r8] = r7
            goto L_0x007d
        L_0x0065:
            java.lang.String r7 = "asString"
            r6[r8] = r7
            goto L_0x007d
        L_0x006a:
            java.lang.String r7 = "asSingleFqName"
            r6[r8] = r7
            goto L_0x007d
        L_0x006f:
            java.lang.String r7 = "getShortClassName"
            r6[r8] = r7
            goto L_0x007d
        L_0x0074:
            java.lang.String r7 = "getRelativeClassName"
            r6[r8] = r7
            goto L_0x007d
        L_0x0079:
            java.lang.String r7 = "getPackageFqName"
            r6[r8] = r7
        L_0x007d:
            switch(r10) {
                case 1: goto L_0x0094;
                case 2: goto L_0x0094;
                case 3: goto L_0x0094;
                case 4: goto L_0x0094;
                case 5: goto L_0x0098;
                case 6: goto L_0x0098;
                case 7: goto L_0x0098;
                case 8: goto L_0x008f;
                case 9: goto L_0x0098;
                case 10: goto L_0x008a;
                case 11: goto L_0x0085;
                case 12: goto L_0x0085;
                case 13: goto L_0x0098;
                case 14: goto L_0x0098;
                case 15: goto L_0x0098;
                case 16: goto L_0x0098;
                default: goto L_0x0080;
            }
        L_0x0080:
            java.lang.String r7 = "topLevel"
            r6[r5] = r7
            goto L_0x0098
        L_0x0085:
            java.lang.String r7 = "fromString"
            r6[r5] = r7
            goto L_0x0098
        L_0x008a:
            java.lang.String r7 = "startsWith"
            r6[r5] = r7
            goto L_0x0098
        L_0x008f:
            java.lang.String r7 = "createNestedClassId"
            r6[r5] = r7
            goto L_0x0098
        L_0x0094:
            java.lang.String r7 = "<init>"
            r6[r5] = r7
        L_0x0098:
            java.lang.String r4 = java.lang.String.format(r4, r6)
            if (r10 == r3) goto L_0x00ad
            if (r10 == r2) goto L_0x00ad
            if (r10 == r1) goto L_0x00ad
            if (r10 == r0) goto L_0x00ad
            switch(r10) {
                case 13: goto L_0x00ad;
                case 14: goto L_0x00ad;
                case 15: goto L_0x00ad;
                case 16: goto L_0x00ad;
                default: goto L_0x00a7;
            }
        L_0x00a7:
            java.lang.IllegalArgumentException r10 = new java.lang.IllegalArgumentException
            r10.<init>(r4)
            goto L_0x00b2
        L_0x00ad:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            r10.<init>(r4)
        L_0x00b2:
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.reflect.jvm.internal.impl.name.ClassId.$$$reportNull$$$0(int):void");
    }

    public static ClassId topLevel(FqName fqName) {
        if (fqName == null) {
            $$$reportNull$$$0(0);
        }
        return new ClassId(fqName.parent(), fqName.shortName());
    }

    public ClassId(FqName fqName, FqName fqName2, boolean z) {
        if (fqName == null) {
            $$$reportNull$$$0(1);
        }
        if (fqName2 == null) {
            $$$reportNull$$$0(2);
        }
        this.packageFqName = fqName;
        this.relativeClassName = fqName2;
        this.local = z;
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public ClassId(FqName fqName, Name name) {
        this(fqName, FqName.topLevel(name), false);
        if (fqName == null) {
            $$$reportNull$$$0(3);
        }
        if (name == null) {
            $$$reportNull$$$0(4);
        }
    }

    public FqName getPackageFqName() {
        FqName fqName = this.packageFqName;
        if (fqName == null) {
            $$$reportNull$$$0(5);
        }
        return fqName;
    }

    public FqName getRelativeClassName() {
        FqName fqName = this.relativeClassName;
        if (fqName == null) {
            $$$reportNull$$$0(6);
        }
        return fqName;
    }

    public Name getShortClassName() {
        Name shortName = this.relativeClassName.shortName();
        if (shortName == null) {
            $$$reportNull$$$0(7);
        }
        return shortName;
    }

    public boolean isLocal() {
        return this.local;
    }

    public ClassId createNestedClassId(Name name) {
        if (name == null) {
            $$$reportNull$$$0(8);
        }
        return new ClassId(getPackageFqName(), this.relativeClassName.child(name), this.local);
    }

    public ClassId getOuterClassId() {
        FqName parent = this.relativeClassName.parent();
        if (parent.isRoot()) {
            return null;
        }
        return new ClassId(getPackageFqName(), parent, this.local);
    }

    public boolean isNestedClass() {
        return !this.relativeClassName.parent().isRoot();
    }

    public FqName asSingleFqName() {
        if (this.packageFqName.isRoot()) {
            FqName fqName = this.relativeClassName;
            if (fqName == null) {
                $$$reportNull$$$0(9);
            }
            return fqName;
        }
        return new FqName(this.packageFqName.asString() + "." + this.relativeClassName.asString());
    }

    public static ClassId fromString(String str) {
        if (str == null) {
            $$$reportNull$$$0(11);
        }
        return fromString(str, false);
    }

    public static ClassId fromString(String str, boolean z) {
        String str2;
        if (str == null) {
            $$$reportNull$$$0(12);
        }
        int lastIndexOf = str.lastIndexOf("/");
        if (lastIndexOf == -1) {
            str2 = "";
        } else {
            String replace = str.substring(0, lastIndexOf).replace('/', '.');
            str = str.substring(lastIndexOf + 1);
            str2 = replace;
        }
        return new ClassId(new FqName(str2), new FqName(str), z);
    }

    public String asString() {
        if (this.packageFqName.isRoot()) {
            String asString = this.relativeClassName.asString();
            if (asString == null) {
                $$$reportNull$$$0(13);
            }
            return asString;
        }
        String str = this.packageFqName.asString().replace('.', '/') + "/" + this.relativeClassName.asString();
        if (str == null) {
            $$$reportNull$$$0(14);
        }
        return str;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || ClassId.class != obj.getClass()) {
            return false;
        }
        ClassId classId = (ClassId) obj;
        if (!this.packageFqName.equals(classId.packageFqName) || !this.relativeClassName.equals(classId.relativeClassName) || this.local != classId.local) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (((this.packageFqName.hashCode() * 31) + this.relativeClassName.hashCode()) * 31) + Boolean.valueOf(this.local).hashCode();
    }

    public String toString() {
        if (!this.packageFqName.isRoot()) {
            return asString();
        }
        return "/" + asString();
    }
}
