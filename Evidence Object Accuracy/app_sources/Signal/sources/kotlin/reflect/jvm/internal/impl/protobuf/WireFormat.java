package kotlin.reflect.jvm.internal.impl.protobuf;

/* loaded from: classes3.dex */
public final class WireFormat {
    static final int MESSAGE_SET_ITEM_END_TAG = makeTag(1, 4);
    static final int MESSAGE_SET_ITEM_TAG = makeTag(1, 3);
    static final int MESSAGE_SET_MESSAGE_TAG = makeTag(3, 2);
    static final int MESSAGE_SET_TYPE_ID_TAG = makeTag(2, 0);

    public static int getTagFieldNumber(int i) {
        return i >>> 3;
    }

    public static int getTagWireType(int i) {
        return i & 7;
    }

    public static int makeTag(int i, int i2) {
        return (i << 3) | i2;
    }

    /* loaded from: classes3.dex */
    public enum JavaType {
        INT(0),
        LONG(0L),
        FLOAT(Float.valueOf(0.0f)),
        DOUBLE(Double.valueOf(0.0d)),
        BOOLEAN(Boolean.FALSE),
        STRING(""),
        BYTE_STRING(ByteString.EMPTY),
        ENUM(null),
        MESSAGE(null);
        
        private final Object defaultDefault;

        JavaType(Object obj) {
            this.defaultDefault = obj;
        }
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    /* loaded from: classes3.dex */
    public static class FieldType extends Enum<FieldType> {
        private final JavaType javaType;
        private final int wireType;

        public boolean isPackable() {
            return true;
        }

        static {
            FieldType fieldType = new FieldType(JavaType.DOUBLE, 1);
            DOUBLE = fieldType;
            FieldType fieldType2 = new FieldType(JavaType.FLOAT, 5);
            FLOAT = fieldType2;
            JavaType javaType = JavaType.LONG;
            FieldType fieldType3 = new FieldType(javaType, 0);
            INT64 = fieldType3;
            FieldType fieldType4 = new FieldType(javaType, 0);
            UINT64 = fieldType4;
            JavaType javaType2 = JavaType.INT;
            FieldType fieldType5 = new FieldType(javaType2, 0);
            INT32 = fieldType5;
            FieldType fieldType6 = new FieldType(javaType, 1);
            FIXED64 = fieldType6;
            FieldType fieldType7 = new FieldType(javaType2, 5);
            FIXED32 = fieldType7;
            FieldType fieldType8 = new FieldType(JavaType.BOOLEAN, 0);
            BOOL = fieldType8;
            AnonymousClass1 r4 = new FieldType(JavaType.STRING, 2) { // from class: kotlin.reflect.jvm.internal.impl.protobuf.WireFormat.FieldType.1
                @Override // kotlin.reflect.jvm.internal.impl.protobuf.WireFormat.FieldType
                public boolean isPackable() {
                    return false;
                }
            };
            STRING = r4;
            JavaType javaType3 = JavaType.MESSAGE;
            AnonymousClass2 r6 = new FieldType(javaType3, 3) { // from class: kotlin.reflect.jvm.internal.impl.protobuf.WireFormat.FieldType.2
                @Override // kotlin.reflect.jvm.internal.impl.protobuf.WireFormat.FieldType
                public boolean isPackable() {
                    return false;
                }
            };
            GROUP = r6;
            AnonymousClass3 r3 = new FieldType(javaType3, 2) { // from class: kotlin.reflect.jvm.internal.impl.protobuf.WireFormat.FieldType.3
                @Override // kotlin.reflect.jvm.internal.impl.protobuf.WireFormat.FieldType
                public boolean isPackable() {
                    return false;
                }
            };
            MESSAGE = r3;
            AnonymousClass4 r8 = new FieldType(JavaType.BYTE_STRING, 2) { // from class: kotlin.reflect.jvm.internal.impl.protobuf.WireFormat.FieldType.4
                @Override // kotlin.reflect.jvm.internal.impl.protobuf.WireFormat.FieldType
                public boolean isPackable() {
                    return false;
                }
            };
            BYTES = r8;
            FieldType fieldType9 = new FieldType(javaType2, 0);
            UINT32 = fieldType9;
            FieldType fieldType10 = new FieldType(JavaType.ENUM, 0);
            ENUM = fieldType10;
            FieldType fieldType11 = new FieldType(javaType2, 5);
            SFIXED32 = fieldType11;
            FieldType fieldType12 = new FieldType(javaType, 1);
            SFIXED64 = fieldType12;
            FieldType fieldType13 = new FieldType(javaType2, 0);
            SINT32 = fieldType13;
            FieldType fieldType14 = new FieldType(javaType, 0);
            SINT64 = fieldType14;
            $VALUES = new FieldType[]{fieldType, fieldType2, fieldType3, fieldType4, fieldType5, fieldType6, fieldType7, fieldType8, r4, r6, r3, r8, fieldType9, fieldType10, fieldType11, fieldType12, fieldType13, fieldType14};
        }

        private FieldType(String str, int i, JavaType javaType, int i2) {
            this.javaType = javaType;
            this.wireType = i2;
        }

        public JavaType getJavaType() {
            return this.javaType;
        }

        public int getWireType() {
            return this.wireType;
        }
    }
}
