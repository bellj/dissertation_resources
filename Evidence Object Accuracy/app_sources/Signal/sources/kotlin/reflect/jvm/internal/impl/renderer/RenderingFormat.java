package kotlin.reflect.jvm.internal.impl.renderer;

import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsJVMKt;

/* compiled from: DescriptorRenderer.kt */
/* loaded from: classes3.dex */
public abstract class RenderingFormat extends Enum<RenderingFormat> {
    private static final /* synthetic */ RenderingFormat[] $VALUES = $values();
    public static final RenderingFormat HTML = new HTML("HTML", 1);
    public static final RenderingFormat PLAIN = new PLAIN("PLAIN", 0);

    private static final /* synthetic */ RenderingFormat[] $values() {
        return new RenderingFormat[]{PLAIN, HTML};
    }

    public /* synthetic */ RenderingFormat(String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(str, i);
    }

    public static RenderingFormat valueOf(String str) {
        return (RenderingFormat) Enum.valueOf(RenderingFormat.class, str);
    }

    public static RenderingFormat[] values() {
        return (RenderingFormat[]) $VALUES.clone();
    }

    public abstract String escape(String str);

    /* compiled from: DescriptorRenderer.kt */
    /* loaded from: classes3.dex */
    static final class PLAIN extends RenderingFormat {
        @Override // kotlin.reflect.jvm.internal.impl.renderer.RenderingFormat
        public String escape(String str) {
            Intrinsics.checkNotNullParameter(str, "string");
            return str;
        }

        PLAIN(String str, int i) {
            super(str, i, null);
        }
    }

    private RenderingFormat(String str, int i) {
        super(str, i);
    }

    /* compiled from: DescriptorRenderer.kt */
    /* loaded from: classes3.dex */
    static final class HTML extends RenderingFormat {
        HTML(String str, int i) {
            super(str, i, null);
        }

        @Override // kotlin.reflect.jvm.internal.impl.renderer.RenderingFormat
        public String escape(String str) {
            Intrinsics.checkNotNullParameter(str, "string");
            return StringsKt__StringsJVMKt.replace$default(StringsKt__StringsJVMKt.replace$default(str, "<", "&lt;", false, 4, (Object) null), ">", "&gt;", false, 4, (Object) null);
        }
    }
}
