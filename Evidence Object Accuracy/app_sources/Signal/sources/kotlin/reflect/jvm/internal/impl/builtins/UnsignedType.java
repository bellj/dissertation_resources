package kotlin.reflect.jvm.internal.impl.builtins;

import kotlin.jvm.internal.Intrinsics;
import kotlin.reflect.jvm.internal.impl.name.ClassId;
import kotlin.reflect.jvm.internal.impl.name.FqName;
import kotlin.reflect.jvm.internal.impl.name.Name;

/* compiled from: UnsignedType.kt */
/* JADX WARN: Init of enum UBYTE can be incorrect */
/* JADX WARN: Init of enum USHORT can be incorrect */
/* JADX WARN: Init of enum UINT can be incorrect */
/* JADX WARN: Init of enum ULONG can be incorrect */
/* loaded from: classes3.dex */
public enum UnsignedType {
    UBYTE(r1),
    USHORT(r1),
    UINT(r1),
    ULONG(r1);
    
    private final ClassId arrayClassId;
    private final ClassId classId;
    private final Name typeName;

    UnsignedType(ClassId classId) {
        this.classId = classId;
        Name shortClassName = classId.getShortClassName();
        Intrinsics.checkNotNullExpressionValue(shortClassName, "classId.shortClassName");
        this.typeName = shortClassName;
        FqName packageFqName = classId.getPackageFqName();
        this.arrayClassId = new ClassId(packageFqName, Name.identifier(shortClassName.asString() + "Array"));
    }

    public final ClassId getClassId() {
        return this.classId;
    }

    static {
        Intrinsics.checkNotNullExpressionValue(ClassId.fromString("kotlin/UByte"), "fromString(\"kotlin/UByte\")");
        Intrinsics.checkNotNullExpressionValue(ClassId.fromString("kotlin/UShort"), "fromString(\"kotlin/UShort\")");
        Intrinsics.checkNotNullExpressionValue(ClassId.fromString("kotlin/UInt"), "fromString(\"kotlin/UInt\")");
        Intrinsics.checkNotNullExpressionValue(ClassId.fromString("kotlin/ULong"), "fromString(\"kotlin/ULong\")");
    }

    public final Name getTypeName() {
        return this.typeName;
    }

    public final ClassId getArrayClassId() {
        return this.arrayClassId;
    }
}
