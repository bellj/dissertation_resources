package kotlin.reflect.jvm.internal.impl.load.kotlin;

import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import kotlin.reflect.jvm.internal.impl.load.kotlin.AbstractBinaryClassAnnotationAndConstantLoader;

/* compiled from: AbstractBinaryClassAnnotationAndConstantLoader.kt */
/* loaded from: classes3.dex */
final class AbstractBinaryClassAnnotationAndConstantLoader$loadAnnotationDefaultValue$1 extends Lambda implements Function2<AbstractBinaryClassAnnotationAndConstantLoader.Storage<? extends A, ? extends C>, MemberSignature, C> {
    public static final AbstractBinaryClassAnnotationAndConstantLoader$loadAnnotationDefaultValue$1 INSTANCE = new AbstractBinaryClassAnnotationAndConstantLoader$loadAnnotationDefaultValue$1();

    AbstractBinaryClassAnnotationAndConstantLoader$loadAnnotationDefaultValue$1() {
        super(2);
    }

    @Override // kotlin.jvm.functions.Function2
    public /* bridge */ /* synthetic */ Object invoke(Object obj, Object obj2) {
        return invoke((AbstractBinaryClassAnnotationAndConstantLoader.Storage<? extends A, ? extends Object>) ((AbstractBinaryClassAnnotationAndConstantLoader.Storage) obj), (MemberSignature) obj2);
    }

    /* JADX WARN: Type inference failed for: r2v2, types: [C, java.lang.Object] */
    public final C invoke(AbstractBinaryClassAnnotationAndConstantLoader.Storage<? extends A, ? extends C> storage, MemberSignature memberSignature) {
        Intrinsics.checkNotNullParameter(storage, "$this$loadConstantFromProperty");
        Intrinsics.checkNotNullParameter(memberSignature, "it");
        return storage.getAnnotationParametersDefaultValues().get(memberSignature);
    }
}
