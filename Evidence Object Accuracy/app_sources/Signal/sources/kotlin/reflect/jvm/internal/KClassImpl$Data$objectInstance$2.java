package kotlin.reflect.jvm.internal;

import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Lambda;

/* compiled from: KClassImpl.kt */
@Metadata(d1 = {"\u0000\f\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\u0010\u0000\u001a\u0004\u0018\u0001H\u0001\"\b\b\u0000\u0010\u0001*\u00020\u0002H\n¢\u0006\u0004\b\u0003\u0010\u0004"}, d2 = {"<anonymous>", "T", "", "invoke", "()Ljava/lang/Object;"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class KClassImpl$Data$objectInstance$2 extends Lambda implements Function0<T> {
    final /* synthetic */ KClassImpl<T>.Data this$0;
    final /* synthetic */ KClassImpl<T> this$1;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public KClassImpl$Data$objectInstance$2(KClassImpl<T>.Data data, KClassImpl<T> kClassImpl) {
        super(0);
        this.this$0 = data;
        this.this$1 = kClassImpl;
    }

    /* JADX WARN: Type inference failed for: r0v3, types: [T, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // kotlin.jvm.functions.Function0
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final T invoke() {
        /*
            r4 = this;
            kotlin.reflect.jvm.internal.KClassImpl<T>$Data r0 = r4.this$0
            kotlin.reflect.jvm.internal.impl.descriptors.ClassDescriptor r0 = r0.getDescriptor()
            kotlin.reflect.jvm.internal.impl.descriptors.ClassKind r1 = r0.getKind()
            kotlin.reflect.jvm.internal.impl.descriptors.ClassKind r2 = kotlin.reflect.jvm.internal.impl.descriptors.ClassKind.OBJECT
            r3 = 0
            if (r1 == r2) goto L_0x0010
            return r3
        L_0x0010:
            boolean r1 = r0.isCompanionObject()
            if (r1 == 0) goto L_0x0035
            kotlin.reflect.jvm.internal.impl.builtins.CompanionObjectMapping r1 = kotlin.reflect.jvm.internal.impl.builtins.CompanionObjectMapping.INSTANCE
            boolean r1 = kotlin.reflect.jvm.internal.impl.builtins.CompanionObjectMappingUtilsKt.isMappedIntrinsicCompanionObject(r1, r0)
            if (r1 != 0) goto L_0x0035
            kotlin.reflect.jvm.internal.KClassImpl<T> r1 = r4.this$1
            java.lang.Class r1 = r1.getJClass()
            java.lang.Class r1 = r1.getEnclosingClass()
            kotlin.reflect.jvm.internal.impl.name.Name r0 = r0.getName()
            java.lang.String r0 = r0.asString()
            java.lang.reflect.Field r0 = r1.getDeclaredField(r0)
            goto L_0x0041
        L_0x0035:
            kotlin.reflect.jvm.internal.KClassImpl<T> r0 = r4.this$1
            java.lang.Class r0 = r0.getJClass()
            java.lang.String r1 = "INSTANCE"
            java.lang.reflect.Field r0 = r0.getDeclaredField(r1)
        L_0x0041:
            java.lang.Object r0 = r0.get(r3)
            if (r0 == 0) goto L_0x0048
            return r0
        L_0x0048:
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            java.lang.String r1 = "null cannot be cast to non-null type T of kotlin.reflect.jvm.internal.KClassImpl"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.reflect.jvm.internal.KClassImpl$Data$objectInstance$2.invoke():java.lang.Object");
    }
}
