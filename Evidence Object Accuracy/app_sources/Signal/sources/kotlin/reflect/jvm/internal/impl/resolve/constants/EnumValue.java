package kotlin.reflect.jvm.internal.impl.resolve.constants;

import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.reflect.jvm.internal.impl.descriptors.ClassDescriptor;
import kotlin.reflect.jvm.internal.impl.descriptors.FindClassInModuleKt;
import kotlin.reflect.jvm.internal.impl.descriptors.ModuleDescriptor;
import kotlin.reflect.jvm.internal.impl.name.ClassId;
import kotlin.reflect.jvm.internal.impl.name.Name;
import kotlin.reflect.jvm.internal.impl.resolve.DescriptorUtils;
import kotlin.reflect.jvm.internal.impl.types.ErrorUtils;
import kotlin.reflect.jvm.internal.impl.types.KotlinType;
import kotlin.reflect.jvm.internal.impl.types.SimpleType;

/* compiled from: constantValues.kt */
/* loaded from: classes3.dex */
public final class EnumValue extends ConstantValue<Pair<? extends ClassId, ? extends Name>> {
    private final ClassId enumClassId;
    private final Name enumEntryName;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public EnumValue(ClassId classId, Name name) {
        super(TuplesKt.to(classId, name));
        Intrinsics.checkNotNullParameter(classId, "enumClassId");
        Intrinsics.checkNotNullParameter(name, "enumEntryName");
        this.enumClassId = classId;
        this.enumEntryName = name;
    }

    public final Name getEnumEntryName() {
        return this.enumEntryName;
    }

    @Override // kotlin.reflect.jvm.internal.impl.resolve.constants.ConstantValue
    public KotlinType getType(ModuleDescriptor moduleDescriptor) {
        Intrinsics.checkNotNullParameter(moduleDescriptor, "module");
        ClassDescriptor findClassAcrossModuleDependencies = FindClassInModuleKt.findClassAcrossModuleDependencies(moduleDescriptor, this.enumClassId);
        SimpleType simpleType = null;
        if (findClassAcrossModuleDependencies != null) {
            if (!DescriptorUtils.isEnumClass(findClassAcrossModuleDependencies)) {
                findClassAcrossModuleDependencies = null;
            }
            if (findClassAcrossModuleDependencies != null) {
                simpleType = findClassAcrossModuleDependencies.getDefaultType();
            }
        }
        if (simpleType != null) {
            return simpleType;
        }
        SimpleType createErrorType = ErrorUtils.createErrorType("Containing class for error-class based enum entry " + this.enumClassId + '.' + this.enumEntryName);
        Intrinsics.checkNotNullExpressionValue(createErrorType, "createErrorType(\"Contain…mClassId.$enumEntryName\")");
        return createErrorType;
    }

    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: kotlin.reflect.jvm.internal.impl.name.Name : 0x0007: INVOKE  (r1v1 kotlin.reflect.jvm.internal.impl.name.Name A[REMOVE]) = 
      (wrap: kotlin.reflect.jvm.internal.impl.name.ClassId : 0x0005: IGET  (r1v0 kotlin.reflect.jvm.internal.impl.name.ClassId A[REMOVE]) = (r2v0 'this' kotlin.reflect.jvm.internal.impl.resolve.constants.EnumValue A[IMMUTABLE_TYPE, THIS]) kotlin.reflect.jvm.internal.impl.resolve.constants.EnumValue.enumClassId kotlin.reflect.jvm.internal.impl.name.ClassId)
     type: VIRTUAL call: kotlin.reflect.jvm.internal.impl.name.ClassId.getShortClassName():kotlin.reflect.jvm.internal.impl.name.Name), ('.' char), (wrap: kotlin.reflect.jvm.internal.impl.name.Name : 0x0013: IGET  (r1v3 kotlin.reflect.jvm.internal.impl.name.Name A[REMOVE]) = (r2v0 'this' kotlin.reflect.jvm.internal.impl.resolve.constants.EnumValue A[IMMUTABLE_TYPE, THIS]) kotlin.reflect.jvm.internal.impl.resolve.constants.EnumValue.enumEntryName kotlin.reflect.jvm.internal.impl.name.Name)] */
    @Override // kotlin.reflect.jvm.internal.impl.resolve.constants.ConstantValue
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.enumClassId.getShortClassName());
        sb.append('.');
        sb.append(this.enumEntryName);
        return sb.toString();
    }
}
