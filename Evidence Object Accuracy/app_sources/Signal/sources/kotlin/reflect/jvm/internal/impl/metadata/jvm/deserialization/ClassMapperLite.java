package kotlin.reflect.jvm.internal.impl.metadata.jvm.deserialization;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.internal.ProgressionUtilKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsJVMKt;

/* compiled from: ClassMapperLite.kt */
/* loaded from: classes3.dex */
public final class ClassMapperLite {
    public static final ClassMapperLite INSTANCE = new ClassMapperLite();

    /* renamed from: kotlin */
    private static final String f13kotlin = CollectionsKt___CollectionsKt.joinToString$default(CollectionsKt__CollectionsKt.listOf((Object[]) new Character[]{'k', 'o', 't', 'l', 'i', 'n'}), "", null, null, 0, null, null, 62, null);
    private static final Map<String, String> map;

    private ClassMapperLite() {
    }

    static {
        INSTANCE = new ClassMapperLite();
        f13kotlin = CollectionsKt___CollectionsKt.joinToString$default(CollectionsKt__CollectionsKt.listOf((Object[]) new Character[]{'k', 'o', 't', 'l', 'i', 'n'}), "", null, null, 0, null, null, 62, null);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        List list = CollectionsKt__CollectionsKt.listOf((Object[]) new String[]{"Boolean", "Z", "Char", "C", "Byte", "B", "Short", "S", "Int", "I", "Float", "F", "Long", "J", "Double", "D"});
        int progressionLastElement = ProgressionUtilKt.getProgressionLastElement(0, list.size() - 1, 2);
        if (progressionLastElement >= 0) {
            int i = 0;
            while (true) {
                StringBuilder sb = new StringBuilder();
                String str = f13kotlin;
                sb.append(str);
                sb.append('/');
                sb.append((String) list.get(i));
                int i2 = i + 1;
                linkedHashMap.put(sb.toString(), list.get(i2));
                linkedHashMap.put(str + '/' + ((String) list.get(i)) + "Array", '[' + ((String) list.get(i2)));
                if (i == progressionLastElement) {
                    break;
                }
                i += 2;
            }
        }
        linkedHashMap.put(f13kotlin + "/Unit", "V");
        m168map$lambda0$add(linkedHashMap, "Any", "java/lang/Object");
        m168map$lambda0$add(linkedHashMap, "Nothing", "java/lang/Void");
        m168map$lambda0$add(linkedHashMap, "Annotation", "java/lang/annotation/Annotation");
        for (String str2 : CollectionsKt__CollectionsKt.listOf((Object[]) new String[]{"String", "CharSequence", "Throwable", "Cloneable", "Number", "Comparable", "Enum"})) {
            m168map$lambda0$add(linkedHashMap, str2, "java/lang/" + str2);
        }
        for (String str3 : CollectionsKt__CollectionsKt.listOf((Object[]) new String[]{"Iterator", "Collection", "List", "Set", "Map", "ListIterator"})) {
            m168map$lambda0$add(linkedHashMap, "collections/" + str3, "java/util/" + str3);
            m168map$lambda0$add(linkedHashMap, "collections/Mutable" + str3, "java/util/" + str3);
        }
        m168map$lambda0$add(linkedHashMap, "collections/Iterable", "java/lang/Iterable");
        m168map$lambda0$add(linkedHashMap, "collections/MutableIterable", "java/lang/Iterable");
        m168map$lambda0$add(linkedHashMap, "collections/Map.Entry", "java/util/Map$Entry");
        m168map$lambda0$add(linkedHashMap, "collections/MutableMap.MutableEntry", "java/util/Map$Entry");
        for (int i3 = 0; i3 < 23; i3++) {
            StringBuilder sb2 = new StringBuilder();
            String str4 = f13kotlin;
            sb2.append(str4);
            sb2.append("/jvm/functions/Function");
            sb2.append(i3);
            m168map$lambda0$add(linkedHashMap, "Function" + i3, sb2.toString());
            m168map$lambda0$add(linkedHashMap, "reflect/KFunction" + i3, str4 + "/reflect/KFunction");
        }
        for (String str5 : CollectionsKt__CollectionsKt.listOf((Object[]) new String[]{"Char", "Byte", "Short", "Int", "Float", "Long", "Double", "String", "Enum"})) {
            m168map$lambda0$add(linkedHashMap, str5 + ".Companion", f13kotlin + "/jvm/internal/" + str5 + "CompanionObject");
        }
        map = linkedHashMap;
    }

    /* renamed from: map$lambda-0$add */
    private static final void m168map$lambda0$add(Map<String, String> map2, String str, String str2) {
        map2.put(f13kotlin + '/' + str, 'L' + str2 + ';');
    }

    @JvmStatic
    public static final String mapClass(String str) {
        Intrinsics.checkNotNullParameter(str, "classId");
        String str2 = map.get(str);
        if (str2 != null) {
            return str2;
        }
        return 'L' + StringsKt__StringsJVMKt.replace$default(str, '.', '$', false, 4, (Object) null) + ';';
    }
}
