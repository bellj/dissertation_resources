package kotlin.reflect.jvm.internal.impl.load.kotlin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import kotlin.jvm.internal.Intrinsics;
import kotlin.reflect.jvm.internal.impl.descriptors.ClassDescriptor;
import kotlin.reflect.jvm.internal.impl.descriptors.FindClassInModuleKt;
import kotlin.reflect.jvm.internal.impl.descriptors.ModuleDescriptor;
import kotlin.reflect.jvm.internal.impl.descriptors.NotFoundClasses;
import kotlin.reflect.jvm.internal.impl.descriptors.SourceElement;
import kotlin.reflect.jvm.internal.impl.descriptors.ValueParameterDescriptor;
import kotlin.reflect.jvm.internal.impl.descriptors.annotations.AnnotationDescriptor;
import kotlin.reflect.jvm.internal.impl.descriptors.annotations.AnnotationDescriptorImpl;
import kotlin.reflect.jvm.internal.impl.load.java.components.DescriptorResolverUtils;
import kotlin.reflect.jvm.internal.impl.load.kotlin.KotlinJvmBinaryClass;
import kotlin.reflect.jvm.internal.impl.metadata.ProtoBuf$Annotation;
import kotlin.reflect.jvm.internal.impl.metadata.deserialization.NameResolver;
import kotlin.reflect.jvm.internal.impl.name.ClassId;
import kotlin.reflect.jvm.internal.impl.name.Name;
import kotlin.reflect.jvm.internal.impl.resolve.constants.AnnotationValue;
import kotlin.reflect.jvm.internal.impl.resolve.constants.ByteValue;
import kotlin.reflect.jvm.internal.impl.resolve.constants.ClassLiteralValue;
import kotlin.reflect.jvm.internal.impl.resolve.constants.ConstantValue;
import kotlin.reflect.jvm.internal.impl.resolve.constants.ConstantValueFactory;
import kotlin.reflect.jvm.internal.impl.resolve.constants.EnumValue;
import kotlin.reflect.jvm.internal.impl.resolve.constants.ErrorValue;
import kotlin.reflect.jvm.internal.impl.resolve.constants.IntValue;
import kotlin.reflect.jvm.internal.impl.resolve.constants.KClassValue;
import kotlin.reflect.jvm.internal.impl.resolve.constants.LongValue;
import kotlin.reflect.jvm.internal.impl.resolve.constants.ShortValue;
import kotlin.reflect.jvm.internal.impl.resolve.constants.UByteValue;
import kotlin.reflect.jvm.internal.impl.resolve.constants.UIntValue;
import kotlin.reflect.jvm.internal.impl.resolve.constants.ULongValue;
import kotlin.reflect.jvm.internal.impl.resolve.constants.UShortValue;
import kotlin.reflect.jvm.internal.impl.serialization.deserialization.AnnotationDeserializer;
import kotlin.reflect.jvm.internal.impl.storage.StorageManager;
import kotlin.reflect.jvm.internal.impl.types.KotlinType;
import kotlin.reflect.jvm.internal.impl.utils.CollectionsKt;
import kotlin.text.StringsKt__StringsKt;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.PushDatabase;
import org.thoughtcrime.securesms.mediasend.MediaSendActivityResult;

/* compiled from: BinaryClassAnnotationAndConstantLoaderImpl.kt */
/* loaded from: classes3.dex */
public final class BinaryClassAnnotationAndConstantLoaderImpl extends AbstractBinaryClassAnnotationAndConstantLoader<AnnotationDescriptor, ConstantValue<?>> {
    private final AnnotationDeserializer annotationDeserializer;
    private final ModuleDescriptor module;
    private final NotFoundClasses notFoundClasses;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BinaryClassAnnotationAndConstantLoaderImpl(ModuleDescriptor moduleDescriptor, NotFoundClasses notFoundClasses, StorageManager storageManager, KotlinClassFinder kotlinClassFinder) {
        super(storageManager, kotlinClassFinder);
        Intrinsics.checkNotNullParameter(moduleDescriptor, "module");
        Intrinsics.checkNotNullParameter(notFoundClasses, "notFoundClasses");
        Intrinsics.checkNotNullParameter(storageManager, "storageManager");
        Intrinsics.checkNotNullParameter(kotlinClassFinder, "kotlinClassFinder");
        this.module = moduleDescriptor;
        this.notFoundClasses = notFoundClasses;
        this.annotationDeserializer = new AnnotationDeserializer(moduleDescriptor, notFoundClasses);
    }

    public AnnotationDescriptor loadTypeAnnotation(ProtoBuf$Annotation protoBuf$Annotation, NameResolver nameResolver) {
        Intrinsics.checkNotNullParameter(protoBuf$Annotation, "proto");
        Intrinsics.checkNotNullParameter(nameResolver, "nameResolver");
        return this.annotationDeserializer.deserializeAnnotation(protoBuf$Annotation, nameResolver);
    }

    public ConstantValue<?> loadConstant(String str, Object obj) {
        Intrinsics.checkNotNullParameter(str, "desc");
        Intrinsics.checkNotNullParameter(obj, "initializer");
        boolean z = false;
        if (StringsKt__StringsKt.contains$default((CharSequence) "ZBCS", (CharSequence) str, false, 2, (Object) null)) {
            int intValue = ((Integer) obj).intValue();
            int hashCode = str.hashCode();
            if (hashCode == 66) {
                if (str.equals("B")) {
                    obj = Byte.valueOf((byte) intValue);
                }
                throw new AssertionError(str);
            } else if (hashCode == 67) {
                if (str.equals("C")) {
                    obj = Character.valueOf((char) intValue);
                }
                throw new AssertionError(str);
            } else if (hashCode != 83) {
                if (hashCode == 90 && str.equals("Z")) {
                    if (intValue != 0) {
                        z = true;
                    }
                    obj = Boolean.valueOf(z);
                }
                throw new AssertionError(str);
            } else {
                if (str.equals("S")) {
                    obj = Short.valueOf((short) intValue);
                }
                throw new AssertionError(str);
            }
        }
        return ConstantValueFactory.INSTANCE.createConstantValue(obj);
    }

    public ConstantValue<?> transformToUnsignedConstant(ConstantValue<?> constantValue) {
        ConstantValue<?> uLongValue;
        Intrinsics.checkNotNullParameter(constantValue, "constant");
        if (constantValue instanceof ByteValue) {
            uLongValue = new UByteValue(((ByteValue) constantValue).getValue().byteValue());
        } else if (constantValue instanceof ShortValue) {
            uLongValue = new UShortValue(((ShortValue) constantValue).getValue().shortValue());
        } else if (constantValue instanceof IntValue) {
            uLongValue = new UIntValue(((IntValue) constantValue).getValue().intValue());
        } else if (!(constantValue instanceof LongValue)) {
            return constantValue;
        } else {
            uLongValue = new ULongValue(((LongValue) constantValue).getValue().longValue());
        }
        return uLongValue;
    }

    @Override // kotlin.reflect.jvm.internal.impl.load.kotlin.AbstractBinaryClassAnnotationAndConstantLoader
    public KotlinJvmBinaryClass.AnnotationArgumentVisitor loadAnnotation(ClassId classId, SourceElement sourceElement, List<AnnotationDescriptor> list) {
        Intrinsics.checkNotNullParameter(classId, "annotationClassId");
        Intrinsics.checkNotNullParameter(sourceElement, PushDatabase.SOURCE_E164);
        Intrinsics.checkNotNullParameter(list, MediaSendActivityResult.EXTRA_RESULT);
        return new AbstractAnnotationArgumentVisitor(this, resolveClass(classId), classId, list, sourceElement) { // from class: kotlin.reflect.jvm.internal.impl.load.kotlin.BinaryClassAnnotationAndConstantLoaderImpl$loadAnnotation$1
            final /* synthetic */ ClassDescriptor $annotationClass;
            final /* synthetic */ ClassId $annotationClassId;
            final /* synthetic */ List<AnnotationDescriptor> $result;
            final /* synthetic */ SourceElement $source;
            private final HashMap<Name, ConstantValue<?>> arguments = new HashMap<>();
            final /* synthetic */ BinaryClassAnnotationAndConstantLoaderImpl this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
                this.$annotationClass = r2;
                this.$annotationClassId = r3;
                this.$result = r4;
                this.$source = r5;
            }

            @Override // kotlin.reflect.jvm.internal.impl.load.kotlin.BinaryClassAnnotationAndConstantLoaderImpl.AbstractAnnotationArgumentVisitor
            public void visitConstantValue(Name name, ConstantValue<?> constantValue) {
                Intrinsics.checkNotNullParameter(constantValue, DraftDatabase.DRAFT_VALUE);
                if (name != null) {
                    this.arguments.put(name, constantValue);
                }
            }

            @Override // kotlin.reflect.jvm.internal.impl.load.kotlin.BinaryClassAnnotationAndConstantLoaderImpl.AbstractAnnotationArgumentVisitor
            public void visitArrayValue(Name name, ArrayList<ConstantValue<?>> arrayList) {
                Intrinsics.checkNotNullParameter(arrayList, "elements");
                if (name != null) {
                    ValueParameterDescriptor annotationParameterByName = DescriptorResolverUtils.getAnnotationParameterByName(name, this.$annotationClass);
                    if (annotationParameterByName != null) {
                        HashMap<Name, ConstantValue<?>> hashMap = this.arguments;
                        ConstantValueFactory constantValueFactory = ConstantValueFactory.INSTANCE;
                        List<? extends ConstantValue<?>> compact = CollectionsKt.compact(arrayList);
                        KotlinType type = annotationParameterByName.getType();
                        Intrinsics.checkNotNullExpressionValue(type, "parameter.type");
                        hashMap.put(name, constantValueFactory.createArrayValue(compact, type));
                    } else if (this.this$0.isImplicitRepeatableContainer(this.$annotationClassId) && Intrinsics.areEqual(name.asString(), DraftDatabase.DRAFT_VALUE)) {
                        ArrayList<AnnotationValue> arrayList2 = new ArrayList();
                        for (Object obj : arrayList) {
                            if (obj instanceof AnnotationValue) {
                                arrayList2.add(obj);
                            }
                        }
                        List<AnnotationDescriptor> list2 = this.$result;
                        for (AnnotationValue annotationValue : arrayList2) {
                            list2.add(annotationValue.getValue());
                        }
                    }
                }
            }

            @Override // kotlin.reflect.jvm.internal.impl.load.kotlin.KotlinJvmBinaryClass.AnnotationArgumentVisitor
            public void visitEnd() {
                if (!this.this$0.isRepeatableWithImplicitContainer(this.$annotationClassId, this.arguments) && !this.this$0.isImplicitRepeatableContainer(this.$annotationClassId)) {
                    this.$result.add(new AnnotationDescriptorImpl(this.$annotationClass.getDefaultType(), this.arguments, this.$source));
                }
            }
        };
    }

    /* compiled from: BinaryClassAnnotationAndConstantLoaderImpl.kt */
    /* loaded from: classes3.dex */
    public abstract class AbstractAnnotationArgumentVisitor implements KotlinJvmBinaryClass.AnnotationArgumentVisitor {
        public abstract void visitArrayValue(Name name, ArrayList<ConstantValue<?>> arrayList);

        public abstract void visitConstantValue(Name name, ConstantValue<?> constantValue);

        public AbstractAnnotationArgumentVisitor() {
            BinaryClassAnnotationAndConstantLoaderImpl.this = r1;
        }

        public void visit(Name name, Object obj) {
            visitConstantValue(name, BinaryClassAnnotationAndConstantLoaderImpl.this.createConstant(name, obj));
        }

        public void visitClassLiteral(Name name, ClassLiteralValue classLiteralValue) {
            Intrinsics.checkNotNullParameter(classLiteralValue, DraftDatabase.DRAFT_VALUE);
            visitConstantValue(name, new KClassValue(classLiteralValue));
        }

        public void visitEnum(Name name, ClassId classId, Name name2) {
            Intrinsics.checkNotNullParameter(classId, "enumClassId");
            Intrinsics.checkNotNullParameter(name2, "enumEntryName");
            visitConstantValue(name, new EnumValue(classId, name2));
        }

        public KotlinJvmBinaryClass.AnnotationArrayArgumentVisitor visitArray(Name name) {
            return new BinaryClassAnnotationAndConstantLoaderImpl$AbstractAnnotationArgumentVisitor$visitArray$1(BinaryClassAnnotationAndConstantLoaderImpl.this, name, this);
        }

        public KotlinJvmBinaryClass.AnnotationArgumentVisitor visitAnnotation(Name name, ClassId classId) {
            Intrinsics.checkNotNullParameter(classId, "classId");
            ArrayList arrayList = new ArrayList();
            BinaryClassAnnotationAndConstantLoaderImpl binaryClassAnnotationAndConstantLoaderImpl = BinaryClassAnnotationAndConstantLoaderImpl.this;
            SourceElement sourceElement = SourceElement.NO_SOURCE;
            Intrinsics.checkNotNullExpressionValue(sourceElement, "NO_SOURCE");
            KotlinJvmBinaryClass.AnnotationArgumentVisitor loadAnnotation = binaryClassAnnotationAndConstantLoaderImpl.loadAnnotation(classId, sourceElement, arrayList);
            Intrinsics.checkNotNull(loadAnnotation);
            return new BinaryClassAnnotationAndConstantLoaderImpl$AbstractAnnotationArgumentVisitor$visitAnnotation$1(loadAnnotation, this, name, arrayList);
        }
    }

    public final ConstantValue<?> createConstant(Name name, Object obj) {
        ConstantValue<?> createConstantValue = ConstantValueFactory.INSTANCE.createConstantValue(obj);
        if (createConstantValue != null) {
            return createConstantValue;
        }
        ErrorValue.Companion companion = ErrorValue.Companion;
        return companion.create("Unsupported annotation argument: " + name);
    }

    private final ClassDescriptor resolveClass(ClassId classId) {
        return FindClassInModuleKt.findNonGenericClassAcrossDependencies(this.module, classId, this.notFoundClasses);
    }
}
