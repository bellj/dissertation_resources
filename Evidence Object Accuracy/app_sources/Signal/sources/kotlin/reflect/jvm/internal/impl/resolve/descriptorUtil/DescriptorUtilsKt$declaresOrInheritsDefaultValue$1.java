package kotlin.reflect.jvm.internal.impl.resolve.descriptorUtil;

import java.util.ArrayList;
import java.util.Collection;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.reflect.jvm.internal.impl.descriptors.ValueParameterDescriptor;
import kotlin.reflect.jvm.internal.impl.utils.DFS;

/* compiled from: DescriptorUtils.kt */
/* loaded from: classes3.dex */
public final class DescriptorUtilsKt$declaresOrInheritsDefaultValue$1<N> implements DFS.Neighbors {
    public static final DescriptorUtilsKt$declaresOrInheritsDefaultValue$1<N> INSTANCE = new DescriptorUtilsKt$declaresOrInheritsDefaultValue$1<>();

    DescriptorUtilsKt$declaresOrInheritsDefaultValue$1() {
    }

    public final Iterable<ValueParameterDescriptor> getNeighbors(ValueParameterDescriptor valueParameterDescriptor) {
        Collection<ValueParameterDescriptor> overriddenDescriptors = valueParameterDescriptor.getOverriddenDescriptors();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(overriddenDescriptors, 10));
        for (ValueParameterDescriptor valueParameterDescriptor2 : overriddenDescriptors) {
            arrayList.add(valueParameterDescriptor2.getOriginal());
        }
        return arrayList;
    }
}
