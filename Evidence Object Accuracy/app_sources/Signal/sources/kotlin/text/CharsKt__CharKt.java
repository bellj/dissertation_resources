package kotlin.text;

import kotlin.Metadata;

/* compiled from: Char.kt */
/* access modifiers changed from: package-private */
@Metadata(bv = {}, d1 = {"\u0000\u000e\n\u0002\u0010\f\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\u001a\u001c\u0010\u0004\u001a\u00020\u0002*\u00020\u00002\u0006\u0010\u0001\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u0002¨\u0006\u0005"}, d2 = {"", "other", "", "ignoreCase", "equals", "kotlin-stdlib"}, k = 5, mv = {1, 6, 0}, xs = "kotlin/text/CharsKt")
/* loaded from: classes3.dex */
public class CharsKt__CharKt extends CharsKt__CharJVMKt {
    public static final boolean equals(char c, char c2, boolean z) {
        if (c == c2) {
            return true;
        }
        if (!z) {
            return false;
        }
        char upperCase = Character.toUpperCase(c);
        char upperCase2 = Character.toUpperCase(c2);
        if (upperCase == upperCase2 || Character.toLowerCase(upperCase) == Character.toLowerCase(upperCase2)) {
            return true;
        }
        return false;
    }
}
