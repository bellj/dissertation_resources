package kotlin.text;

import kotlin.Metadata;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* compiled from: Regex.kt */
/* access modifiers changed from: package-private */
@Metadata(bv = {}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\b\n\u0002\b\u0004\bb\u0018\u00002\u00020\u0001R\u0014\u0010\u0005\u001a\u00020\u00028&X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0004¨\u0006\u0006"}, d2 = {"Lkotlin/text/FlagEnum;", "", "", "getValue", "()I", DraftDatabase.DRAFT_VALUE, "kotlin-stdlib"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes3.dex */
public interface FlagEnum {
    int getValue();
}
