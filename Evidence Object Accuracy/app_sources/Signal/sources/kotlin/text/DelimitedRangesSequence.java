package kotlin.text;

import j$.util.Iterator;
import j$.util.function.Consumer;
import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.IntRange;
import kotlin.ranges.RangesKt___RangesKt;
import kotlin.sequences.Sequence;

/* compiled from: Strings.kt */
@Metadata(bv = {}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010(\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001BG\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\t\u001a\u00020\b\u0012\u0006\u0010\u000b\u001a\u00020\b\u0012&\u0010\u000e\u001a\"\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\b\u0012\u0012\u0012\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b\u0018\u00010\r0\f¢\u0006\u0004\b\u0010\u0010\u0011J\u000f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00020\u0003H\u0002R\u0014\u0010\u0006\u001a\u00020\u00058\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0006\u0010\u0007R\u0014\u0010\t\u001a\u00020\b8\u0002X\u0004¢\u0006\u0006\n\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\b8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\nR4\u0010\u000e\u001a\"\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\b\u0012\u0012\u0012\u0010\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b\u0018\u00010\r0\f8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000f¨\u0006\u0012"}, d2 = {"Lkotlin/text/DelimitedRangesSequence;", "Lkotlin/sequences/Sequence;", "Lkotlin/ranges/IntRange;", "", "iterator", "", "input", "Ljava/lang/CharSequence;", "", "startIndex", "I", "limit", "Lkotlin/Function2;", "Lkotlin/Pair;", "getNextMatch", "Lkotlin/jvm/functions/Function2;", "<init>", "(Ljava/lang/CharSequence;IILkotlin/jvm/functions/Function2;)V", "kotlin-stdlib"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes3.dex */
public final class DelimitedRangesSequence implements Sequence<IntRange> {
    private final Function2<CharSequence, Integer, Pair<Integer, Integer>> getNextMatch;
    private final CharSequence input;
    private final int limit;
    private final int startIndex;

    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: kotlin.jvm.functions.Function2<? super java.lang.CharSequence, ? super java.lang.Integer, kotlin.Pair<java.lang.Integer, java.lang.Integer>> */
    /* JADX WARN: Multi-variable type inference failed */
    public DelimitedRangesSequence(CharSequence charSequence, int i, int i2, Function2<? super CharSequence, ? super Integer, Pair<Integer, Integer>> function2) {
        Intrinsics.checkNotNullParameter(charSequence, "input");
        Intrinsics.checkNotNullParameter(function2, "getNextMatch");
        this.input = charSequence;
        this.startIndex = i;
        this.limit = i2;
        this.getNextMatch = function2;
    }

    @Override // kotlin.sequences.Sequence
    public Iterator<IntRange> iterator() {
        return new Object(this) { // from class: kotlin.text.DelimitedRangesSequence$iterator$1
            private int counter;
            private int currentStartIndex;
            private IntRange nextItem;
            private int nextSearchIndex;
            private int nextState = -1;
            final /* synthetic */ DelimitedRangesSequence this$0;

            @Override // j$.util.Iterator
            public /* synthetic */ void forEachRemaining(Consumer consumer) {
                Iterator.CC.$default$forEachRemaining(this, consumer);
            }

            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.util.function.Consumer] */
            @Override // java.util.Iterator
            public /* synthetic */ void forEachRemaining(java.util.function.Consumer<? super IntRange> consumer) {
                forEachRemaining(Consumer.VivifiedWrapper.convert(consumer));
            }

            @Override // java.util.Iterator, j$.util.Iterator
            public void remove() {
                throw new UnsupportedOperationException("Operation is not supported for read-only collection");
            }

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r3;
                int i = RangesKt___RangesKt.coerceIn(DelimitedRangesSequence.access$getStartIndex$p(r3), 0, DelimitedRangesSequence.access$getInput$p(r3).length());
                this.currentStartIndex = i;
                this.nextSearchIndex = i;
            }

            /* Code decompiled incorrectly, please refer to instructions dump. */
            private final void calcNext() {
                /*
                    r6 = this;
                    int r0 = r6.nextSearchIndex
                    r1 = 0
                    if (r0 >= 0) goto L_0x000c
                    r6.nextState = r1
                    r0 = 0
                    r6.nextItem = r0
                    goto L_0x009e
                L_0x000c:
                    kotlin.text.DelimitedRangesSequence r0 = r6.this$0
                    int r0 = kotlin.text.DelimitedRangesSequence.access$getLimit$p(r0)
                    r2 = -1
                    r3 = 1
                    if (r0 <= 0) goto L_0x0023
                    int r0 = r6.counter
                    int r0 = r0 + r3
                    r6.counter = r0
                    kotlin.text.DelimitedRangesSequence r4 = r6.this$0
                    int r4 = kotlin.text.DelimitedRangesSequence.access$getLimit$p(r4)
                    if (r0 >= r4) goto L_0x0031
                L_0x0023:
                    int r0 = r6.nextSearchIndex
                    kotlin.text.DelimitedRangesSequence r4 = r6.this$0
                    java.lang.CharSequence r4 = kotlin.text.DelimitedRangesSequence.access$getInput$p(r4)
                    int r4 = r4.length()
                    if (r0 <= r4) goto L_0x0047
                L_0x0031:
                    kotlin.ranges.IntRange r0 = new kotlin.ranges.IntRange
                    int r1 = r6.currentStartIndex
                    kotlin.text.DelimitedRangesSequence r4 = r6.this$0
                    java.lang.CharSequence r4 = kotlin.text.DelimitedRangesSequence.access$getInput$p(r4)
                    int r4 = kotlin.text.StringsKt.getLastIndex(r4)
                    r0.<init>(r1, r4)
                    r6.nextItem = r0
                    r6.nextSearchIndex = r2
                    goto L_0x009c
                L_0x0047:
                    kotlin.text.DelimitedRangesSequence r0 = r6.this$0
                    kotlin.jvm.functions.Function2 r0 = kotlin.text.DelimitedRangesSequence.access$getGetNextMatch$p(r0)
                    kotlin.text.DelimitedRangesSequence r4 = r6.this$0
                    java.lang.CharSequence r4 = kotlin.text.DelimitedRangesSequence.access$getInput$p(r4)
                    int r5 = r6.nextSearchIndex
                    java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
                    java.lang.Object r0 = r0.invoke(r4, r5)
                    kotlin.Pair r0 = (kotlin.Pair) r0
                    if (r0 != 0) goto L_0x0077
                    kotlin.ranges.IntRange r0 = new kotlin.ranges.IntRange
                    int r1 = r6.currentStartIndex
                    kotlin.text.DelimitedRangesSequence r4 = r6.this$0
                    java.lang.CharSequence r4 = kotlin.text.DelimitedRangesSequence.access$getInput$p(r4)
                    int r4 = kotlin.text.StringsKt.getLastIndex(r4)
                    r0.<init>(r1, r4)
                    r6.nextItem = r0
                    r6.nextSearchIndex = r2
                    goto L_0x009c
                L_0x0077:
                    java.lang.Object r2 = r0.component1()
                    java.lang.Number r2 = (java.lang.Number) r2
                    int r2 = r2.intValue()
                    java.lang.Object r0 = r0.component2()
                    java.lang.Number r0 = (java.lang.Number) r0
                    int r0 = r0.intValue()
                    int r4 = r6.currentStartIndex
                    kotlin.ranges.IntRange r4 = kotlin.ranges.RangesKt.until(r4, r2)
                    r6.nextItem = r4
                    int r2 = r2 + r0
                    r6.currentStartIndex = r2
                    if (r0 != 0) goto L_0x0099
                    r1 = 1
                L_0x0099:
                    int r2 = r2 + r1
                    r6.nextSearchIndex = r2
                L_0x009c:
                    r6.nextState = r3
                L_0x009e:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: kotlin.text.DelimitedRangesSequence$iterator$1.calcNext():void");
            }

            @Override // java.util.Iterator, j$.util.Iterator
            public IntRange next() {
                if (this.nextState == -1) {
                    calcNext();
                }
                if (this.nextState != 0) {
                    IntRange intRange = this.nextItem;
                    if (intRange != null) {
                        this.nextItem = null;
                        this.nextState = -1;
                        return intRange;
                    }
                    throw new NullPointerException("null cannot be cast to non-null type kotlin.ranges.IntRange");
                }
                throw new NoSuchElementException();
            }

            @Override // java.util.Iterator, j$.util.Iterator
            public boolean hasNext() {
                if (this.nextState == -1) {
                    calcNext();
                }
                return this.nextState == 1;
            }
        };
    }
}
