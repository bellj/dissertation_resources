package kotlin.collections;

import kotlin.Metadata;
import org.thoughtcrime.securesms.MediaPreviewActivity;

/* compiled from: ArraysJVM.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0010\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u001a\u0018\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0001\u001a\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0000H\u0001¨\u0006\u0005"}, d2 = {"", "toIndex", MediaPreviewActivity.SIZE_EXTRA, "", "copyOfRangeToIndexCheck", "kotlin-stdlib"}, k = 5, mv = {1, 6, 0}, xs = "kotlin/collections/ArraysKt")
/* loaded from: classes3.dex */
public class ArraysKt__ArraysJVMKt {
    public static final void copyOfRangeToIndexCheck(int i, int i2) {
        if (i > i2) {
            throw new IndexOutOfBoundsException("toIndex (" + i + ") is greater than size (" + i2 + ").");
        }
    }
}
