package kotlin.math;

import kotlin.Metadata;

/* compiled from: MathJVM.kt */
@Metadata(bv = {}, d1 = {"\u0000\f\n\u0002\u0010\u0007\n\u0002\u0010\b\n\u0002\b\u0002\u001a\f\u0010\u0002\u001a\u00020\u0001*\u00020\u0000H\u0007¨\u0006\u0003"}, d2 = {"", "", "roundToInt", "kotlin-stdlib"}, k = 5, mv = {1, 6, 0}, xs = "kotlin/math/MathKt")
/* loaded from: classes3.dex */
public class MathKt__MathJVMKt extends MathKt__MathHKt {
    public static int roundToInt(float f) {
        if (!Float.isNaN(f)) {
            return Math.round(f);
        }
        throw new IllegalArgumentException("Cannot round NaN value.");
    }
}
