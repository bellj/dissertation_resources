package kotlin.jvm.internal;

import java.io.Serializable;

/* loaded from: classes3.dex */
public final class Ref$LongRef implements Serializable {
    public long element;

    @Override // java.lang.Object
    public String toString() {
        return String.valueOf(this.element);
    }
}
