package kotlin.sequences;

import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: _SequencesJvm.kt */
@Metadata(bv = {}, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a(\u0010\u0004\u001a\b\u0012\u0004\u0012\u00028\u00000\u0001\"\u0004\b\u0000\u0010\u0000*\u0006\u0012\u0002\b\u00030\u00012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0002¨\u0006\u0005"}, d2 = {"R", "Lkotlin/sequences/Sequence;", "Ljava/lang/Class;", "klass", "filterIsInstance", "kotlin-stdlib"}, k = 5, mv = {1, 6, 0}, xs = "kotlin/sequences/SequencesKt")
/* loaded from: classes3.dex */
public class SequencesKt___SequencesJvmKt extends SequencesKt__SequencesKt {
    public static <R> Sequence<R> filterIsInstance(Sequence<?> sequence, Class<R> cls) {
        Intrinsics.checkNotNullParameter(sequence, "<this>");
        Intrinsics.checkNotNullParameter(cls, "klass");
        return SequencesKt___SequencesKt.filter(sequence, new Function1<Object, Boolean>(cls) { // from class: kotlin.sequences.SequencesKt___SequencesJvmKt$filterIsInstance$1
            final /* synthetic */ Class<R> $klass;

            /* access modifiers changed from: package-private */
            {
                this.$klass = r1;
            }

            @Override // kotlin.jvm.functions.Function1
            public final Boolean invoke(Object obj) {
                return Boolean.valueOf(this.$klass.isInstance(obj));
            }
        });
    }
}
