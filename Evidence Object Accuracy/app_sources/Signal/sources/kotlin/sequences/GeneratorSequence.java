package kotlin.sequences;

import j$.util.Iterator;
import j$.util.function.Consumer;
import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: Sequences.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010(\n\u0000\b\u0002\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B+\u0012\u000e\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00018\u00000\u0005\u0012\u0014\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0006\u0012\u0004\u0018\u00018\u00000\u0007¢\u0006\u0002\u0010\bJ\u000f\u0010\t\u001a\b\u0012\u0004\u0012\u00028\u00000\nH\u0002R\u0016\u0010\u0004\u001a\n\u0012\u0006\u0012\u0004\u0018\u00018\u00000\u0005X\u0004¢\u0006\u0002\n\u0000R\u001c\u0010\u0006\u001a\u0010\u0012\u0004\u0012\u00028\u0000\u0012\u0006\u0012\u0004\u0018\u00018\u00000\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lkotlin/sequences/GeneratorSequence;", "T", "", "Lkotlin/sequences/Sequence;", "getInitialValue", "Lkotlin/Function0;", "getNextValue", "Lkotlin/Function1;", "(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)V", "iterator", "", "kotlin-stdlib"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class GeneratorSequence<T> implements Sequence<T> {
    private final Function0<T> getInitialValue;
    private final Function1<T, T> getNextValue;

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: kotlin.jvm.functions.Function0<? extends T> */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: kotlin.jvm.functions.Function1<? super T, ? extends T> */
    /* JADX WARN: Multi-variable type inference failed */
    public GeneratorSequence(Function0<? extends T> function0, Function1<? super T, ? extends T> function1) {
        Intrinsics.checkNotNullParameter(function0, "getInitialValue");
        Intrinsics.checkNotNullParameter(function1, "getNextValue");
        this.getInitialValue = function0;
        this.getNextValue = function1;
    }

    @Override // kotlin.sequences.Sequence
    public Iterator<T> iterator() {
        return new Object(this) { // from class: kotlin.sequences.GeneratorSequence$iterator$1
            private T nextItem;
            private int nextState = -2;
            final /* synthetic */ GeneratorSequence<T> this$0;

            @Override // j$.util.Iterator
            public /* synthetic */ void forEachRemaining(Consumer consumer) {
                Iterator.CC.$default$forEachRemaining(this, consumer);
            }

            @Override // java.util.Iterator
            public /* synthetic */ void forEachRemaining(java.util.function.Consumer consumer) {
                forEachRemaining(Consumer.VivifiedWrapper.convert(consumer));
            }

            @Override // java.util.Iterator, j$.util.Iterator
            public void remove() {
                throw new UnsupportedOperationException("Operation is not supported for read-only collection");
            }

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* JADX DEBUG: Multi-variable search result rejected for r0v1, resolved type: T */
            /* JADX DEBUG: Multi-variable search result rejected for r0v11, resolved type: T */
            /* JADX DEBUG: Multi-variable search result rejected for r0v12, resolved type: T */
            /* JADX WARN: Multi-variable type inference failed */
            private final void calcNext() {
                T t;
                if (this.nextState == -2) {
                    t = GeneratorSequence.access$getGetInitialValue$p(this.this$0).invoke();
                } else {
                    Function1 access$getGetNextValue$p = GeneratorSequence.access$getGetNextValue$p(this.this$0);
                    T t2 = this.nextItem;
                    Intrinsics.checkNotNull(t2);
                    t = access$getGetNextValue$p.invoke(t2);
                }
                this.nextItem = t;
                this.nextState = t == 0 ? 0 : 1;
            }

            @Override // java.util.Iterator, j$.util.Iterator
            public T next() {
                if (this.nextState < 0) {
                    calcNext();
                }
                if (this.nextState != 0) {
                    T t = this.nextItem;
                    if (t != 0) {
                        this.nextState = -1;
                        return t;
                    }
                    throw new NullPointerException("null cannot be cast to non-null type T of kotlin.sequences.GeneratorSequence");
                }
                throw new NoSuchElementException();
            }

            @Override // java.util.Iterator, j$.util.Iterator
            public boolean hasNext() {
                if (this.nextState < 0) {
                    calcNext();
                }
                return this.nextState == 1;
            }
        };
    }
}
