package org.signal.paging;

/* loaded from: classes3.dex */
public interface PagingController<Key> {
    void onDataInvalidated();

    void onDataItemChanged(Key key);

    void onDataItemInserted(Key key, int i);

    void onDataNeededAroundIndex(int i);
}
