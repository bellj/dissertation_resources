package org.signal.paging;

import androidx.core.util.Pools$Pool;
import androidx.core.util.Pools$SynchronizedPool;
import java.util.BitSet;

/* loaded from: classes3.dex */
public class DataStatus {
    private static final Pools$Pool<BitSet> POOL = new Pools$SynchronizedPool(1);
    private int size;
    private final BitSet state;

    public static DataStatus obtain(int i) {
        BitSet acquire = POOL.acquire();
        if (acquire == null) {
            acquire = new BitSet(i);
        } else {
            acquire.clear();
        }
        return new DataStatus(i, acquire);
    }

    private DataStatus(int i, BitSet bitSet) {
        this.size = i;
        this.state = bitSet;
    }

    public void mark(int i) {
        this.state.set(i, true);
    }

    public void markRange(int i, int i2) {
        this.state.set(i, i2, true);
    }

    public int getEarliestUnmarkedIndexInRange(int i, int i2) {
        while (i < i2) {
            if (!this.state.get(i)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public int getLatestUnmarkedIndexInRange(int i, int i2) {
        for (int i3 = i2 - 1; i3 >= i; i3--) {
            if (!this.state.get(i3)) {
                return i3;
            }
        }
        return -1;
    }

    public void insertState(int i, boolean z) {
        if (i >= 0) {
            int i2 = this.size;
            if (i <= i2 + 1) {
                while (i2 > i) {
                    BitSet bitSet = this.state;
                    bitSet.set(i2, bitSet.get(i2 - 1));
                    i2--;
                }
                this.state.set(i, z);
                this.size++;
                return;
            }
        }
        throw new IndexOutOfBoundsException();
    }

    public int size() {
        return this.size;
    }

    public void recycle() {
        POOL.release(this.state);
    }
}
