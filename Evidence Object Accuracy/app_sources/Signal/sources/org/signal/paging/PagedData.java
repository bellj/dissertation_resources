package org.signal.paging;

import androidx.lifecycle.MutableLiveData;
import io.reactivex.rxjava3.subjects.BehaviorSubject;
import io.reactivex.rxjava3.subjects.Subject;
import java.util.List;
import java.util.Objects;

/* loaded from: classes3.dex */
public class PagedData<Key> {
    private final PagingController<Key> controller;

    public PagedData(PagingController<Key> pagingController) {
        this.controller = pagingController;
    }

    public static <Key, Data> LivePagedData<Key, Data> createForLiveData(PagedDataSource<Key, Data> pagedDataSource, PagingConfig pagingConfig) {
        MutableLiveData mutableLiveData = new MutableLiveData();
        return new LivePagedData<>(mutableLiveData, new BufferedPagingController(pagedDataSource, pagingConfig, new DataStream() { // from class: org.signal.paging.PagedData$$ExternalSyntheticLambda0
            @Override // org.signal.paging.DataStream
            public final void next(List list) {
                MutableLiveData.this.postValue(list);
            }
        }));
    }

    public static <Key, Data> ObservablePagedData<Key, Data> createForObservable(PagedDataSource<Key, Data> pagedDataSource, PagingConfig pagingConfig) {
        BehaviorSubject create = BehaviorSubject.create();
        Objects.requireNonNull(create);
        return new ObservablePagedData<>(create, new BufferedPagingController(pagedDataSource, pagingConfig, new DataStream() { // from class: org.signal.paging.PagedData$$ExternalSyntheticLambda1
            @Override // org.signal.paging.DataStream
            public final void next(List list) {
                Subject.this.onNext(list);
            }
        }));
    }

    public PagingController<Key> getController() {
        return this.controller;
    }
}
