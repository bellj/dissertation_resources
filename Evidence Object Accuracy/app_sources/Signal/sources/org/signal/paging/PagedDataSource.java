package org.signal.paging;

import java.util.List;

/* loaded from: classes3.dex */
public interface PagedDataSource<Key, Data> {

    /* loaded from: classes3.dex */
    public interface CancellationSignal {
        boolean isCanceled();
    }

    Key getKey(Data data);

    Data load(Key key);

    List<Data> load(int i, int i2, CancellationSignal cancellationSignal);

    int size();
}
