package org.signal.paging;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.signal.paging.PagedDataSource;

/* access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class FixedSizePagingController<Key, Data> implements PagingController<Key> {
    private static final Executor FETCH_EXECUTOR = SignalExecutors.newCachedSingleThreadExecutor("signal-FixedSizePagingController");
    private static final String TAG = Log.tag(FixedSizePagingController.class);
    private final PagingConfig config;
    private List<Data> data;
    private final PagedDataSource<Key, Data> dataSource;
    private final DataStream<Data> dataStream;
    private volatile boolean invalidated;
    private final Map<Key, Integer> keyToPosition = new HashMap();
    private final DataStatus loadState;

    public FixedSizePagingController(PagedDataSource<Key, Data> pagedDataSource, PagingConfig pagingConfig, DataStream<Data> dataStream, int i) {
        this.dataSource = pagedDataSource;
        this.config = pagingConfig;
        this.dataStream = dataStream;
        DataStatus obtain = DataStatus.obtain(i);
        this.loadState = obtain;
        this.data = new CompressedList(obtain.size());
    }

    @Override // org.signal.paging.PagingController
    public void onDataNeededAroundIndex(int i) {
        if (this.invalidated) {
            Log.w(TAG, buildDataNeededLog(i, "Invalidated! At very beginning."));
            return;
        }
        synchronized (this.loadState) {
            if (this.loadState.size() == 0) {
                this.dataStream.next(Collections.emptyList());
                return;
            }
            int pageSize = (i / this.config.pageSize()) * this.config.pageSize();
            int pageSize2 = this.config.pageSize() + pageSize;
            int bufferPages = this.config.bufferPages() * this.config.pageSize();
            int max = Math.max(0, pageSize - bufferPages);
            int min = Math.min(this.loadState.size(), pageSize2 + bufferPages);
            int earliestUnmarkedIndexInRange = this.loadState.getEarliestUnmarkedIndexInRange(max, min);
            if (earliestUnmarkedIndexInRange >= 0) {
                int latestUnmarkedIndexInRange = this.loadState.getLatestUnmarkedIndexInRange(Math.max(max, earliestUnmarkedIndexInRange), min) + 1;
                if (latestUnmarkedIndexInRange > earliestUnmarkedIndexInRange) {
                    this.loadState.size();
                    this.loadState.markRange(earliestUnmarkedIndexInRange, latestUnmarkedIndexInRange);
                    FETCH_EXECUTOR.execute(new Runnable(i, earliestUnmarkedIndexInRange, latestUnmarkedIndexInRange) { // from class: org.signal.paging.FixedSizePagingController$$ExternalSyntheticLambda2
                        public final /* synthetic */ int f$1;
                        public final /* synthetic */ int f$2;
                        public final /* synthetic */ int f$3;

                        {
                            this.f$1 = r2;
                            this.f$2 = r3;
                            this.f$3 = r4;
                        }

                        @Override // java.lang.Runnable
                        public final void run() {
                            FixedSizePagingController.this.lambda$onDataNeededAroundIndex$1(this.f$1, this.f$2, this.f$3);
                        }
                    });
                }
            }
        }
    }

    public /* synthetic */ void lambda$onDataNeededAroundIndex$1(int i, int i2, int i3) {
        if (this.invalidated) {
            Log.w(TAG, buildDataNeededLog(i, "Invalidated! At beginning of load task."));
            return;
        }
        List<Data> load = this.dataSource.load(i2, i3 - i2, new PagedDataSource.CancellationSignal() { // from class: org.signal.paging.FixedSizePagingController$$ExternalSyntheticLambda3
            @Override // org.signal.paging.PagedDataSource.CancellationSignal
            public final boolean isCanceled() {
                return FixedSizePagingController.this.lambda$onDataNeededAroundIndex$0();
            }
        });
        if (this.invalidated) {
            Log.w(TAG, buildDataNeededLog(i, "Invalidated! Just after data was loaded."));
            return;
        }
        CompressedList compressedList = new CompressedList(this.data);
        int min = Math.min(load.size(), this.data.size() - i2);
        for (int i4 = 0; i4 < min; i4++) {
            int i5 = i2 + i4;
            Data data = load.get(i4);
            compressedList.set(i5, data);
            this.keyToPosition.put(this.dataSource.getKey(data), Integer.valueOf(i5));
        }
        this.data = compressedList;
        this.dataStream.next(compressedList);
    }

    public /* synthetic */ boolean lambda$onDataNeededAroundIndex$0() {
        return this.invalidated;
    }

    @Override // org.signal.paging.PagingController
    public void onDataInvalidated() {
        if (!this.invalidated) {
            this.invalidated = true;
            this.loadState.recycle();
        }
    }

    @Override // org.signal.paging.PagingController
    public void onDataItemChanged(Key key) {
        FETCH_EXECUTOR.execute(new Runnable(key) { // from class: org.signal.paging.FixedSizePagingController$$ExternalSyntheticLambda0
            public final /* synthetic */ Object f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                FixedSizePagingController.this.lambda$onDataItemChanged$2(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$onDataItemChanged$2(Object obj) {
        Integer num = this.keyToPosition.get(obj);
        if (num == null) {
            String str = TAG;
            Log.w(str, "Notified of key " + obj + " but it wasn't in the cache!");
        } else if (this.invalidated) {
            String str2 = TAG;
            Log.w(str2, "Invalidated! Just before individual change was loaded for position " + num);
        } else {
            synchronized (this.loadState) {
                this.loadState.mark(num.intValue());
            }
            Data load = this.dataSource.load(obj);
            if (load == null) {
                String str3 = TAG;
                Log.w(str3, "Notified of key " + obj + " but the loaded item was null!");
            } else if (this.invalidated) {
                String str4 = TAG;
                Log.w(str4, "Invalidated! Just after individual change was loaded for position " + num);
            } else {
                CompressedList compressedList = new CompressedList(this.data);
                compressedList.set(num.intValue(), load);
                this.data = compressedList;
                this.dataStream.next(compressedList);
            }
        }
    }

    @Override // org.signal.paging.PagingController
    public void onDataItemInserted(Key key, int i) {
        FETCH_EXECUTOR.execute(new Runnable(i, key) { // from class: org.signal.paging.FixedSizePagingController$$ExternalSyntheticLambda1
            public final /* synthetic */ int f$1;
            public final /* synthetic */ Object f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                FixedSizePagingController.this.lambda$onDataItemInserted$3(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$onDataItemInserted$3(int i, Object obj) {
        if (i == -1) {
            i = this.data.size();
        }
        if (this.keyToPosition.containsKey(obj)) {
            String str = TAG;
            Log.w(str, "Notified of key " + obj + " being inserted at " + i + ", but the item already exists!");
        } else if (this.invalidated) {
            String str2 = TAG;
            Log.w(str2, "Invalidated! Just before individual insert was loaded for position " + i);
        } else {
            synchronized (this.loadState) {
                this.loadState.insertState(i, true);
            }
            Data load = this.dataSource.load(obj);
            if (load == null) {
                String str3 = TAG;
                Log.w(str3, "Notified of key " + obj + " being inserted at " + i + ", but the loaded item was null!");
            } else if (this.invalidated) {
                String str4 = TAG;
                Log.w(str4, "Invalidated! Just after individual insert was loaded for position " + i);
            } else {
                List<Data> compressedList = new CompressedList<>(this.data);
                compressedList.add(i, load);
                rebuildKeyToPositionMap(this.keyToPosition, compressedList, this.dataSource);
                this.data = compressedList;
                this.dataStream.next(compressedList);
            }
        }
    }

    private void rebuildKeyToPositionMap(Map<Key, Integer> map, List<Data> list, PagedDataSource<Key, Data> pagedDataSource) {
        map.clear();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Data data = list.get(i);
            if (data != null) {
                map.put(pagedDataSource.getKey(data), Integer.valueOf(i));
            }
        }
    }

    private String buildDataNeededLog(int i, String str) {
        return "[onDataNeededAroundIndex(" + i + "), size: " + this.loadState.size() + "] " + str;
    }
}
