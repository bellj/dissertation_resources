package org.signal.paging;

/* loaded from: classes3.dex */
public class ProxyPagingController<Key> implements PagingController<Key> {
    private PagingController<Key> proxied;

    @Override // org.signal.paging.PagingController
    public synchronized void onDataNeededAroundIndex(int i) {
        PagingController<Key> pagingController = this.proxied;
        if (pagingController != null) {
            pagingController.onDataNeededAroundIndex(i);
        }
    }

    @Override // org.signal.paging.PagingController
    public synchronized void onDataInvalidated() {
        PagingController<Key> pagingController = this.proxied;
        if (pagingController != null) {
            pagingController.onDataInvalidated();
        }
    }

    @Override // org.signal.paging.PagingController
    public void onDataItemChanged(Key key) {
        PagingController<Key> pagingController = this.proxied;
        if (pagingController != null) {
            pagingController.onDataItemChanged(key);
        }
    }

    @Override // org.signal.paging.PagingController
    public void onDataItemInserted(Key key, int i) {
        PagingController<Key> pagingController = this.proxied;
        if (pagingController != null) {
            pagingController.onDataItemInserted(key, i);
        }
    }

    public synchronized void set(PagingController<Key> pagingController) {
        this.proxied = pagingController;
    }
}
