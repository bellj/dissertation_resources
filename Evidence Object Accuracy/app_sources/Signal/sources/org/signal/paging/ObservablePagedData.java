package org.signal.paging;

import io.reactivex.rxjava3.core.Observable;
import java.util.List;

/* loaded from: classes3.dex */
public class ObservablePagedData<Key, Data> extends PagedData<Key> {
    private final Observable<List<Data>> data;

    public ObservablePagedData(Observable<List<Data>> observable, PagingController<Key> pagingController) {
        super(pagingController);
        this.data = observable;
    }

    public Observable<List<Data>> getData() {
        return this.data;
    }
}
