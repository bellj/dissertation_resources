package org.signal.paging;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/* loaded from: classes3.dex */
public class BufferedPagingController<Key, Data> implements PagingController<Key> {
    private PagingController<Key> activeController = null;
    private final PagingConfig config;
    private final PagedDataSource<Key, Data> dataSource;
    private final DataStream<Data> dataStream;
    private int lastRequestedIndex;
    private final Executor serializationExecutor = Executors.newSingleThreadExecutor();

    public BufferedPagingController(PagedDataSource<Key, Data> pagedDataSource, PagingConfig pagingConfig, DataStream<Data> dataStream) {
        this.dataSource = pagedDataSource;
        this.config = pagingConfig;
        this.dataStream = dataStream;
        this.lastRequestedIndex = pagingConfig.startIndex();
        onDataInvalidated();
    }

    @Override // org.signal.paging.PagingController
    public void onDataNeededAroundIndex(int i) {
        this.serializationExecutor.execute(new Runnable(i) { // from class: org.signal.paging.BufferedPagingController$$ExternalSyntheticLambda1
            public final /* synthetic */ int f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                BufferedPagingController.$r8$lambda$ZH85BaK50YTpOWNkCy4mMlgrpFQ(BufferedPagingController.this, this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$onDataNeededAroundIndex$0(int i) {
        this.lastRequestedIndex = i;
        this.activeController.onDataNeededAroundIndex(i);
    }

    @Override // org.signal.paging.PagingController
    public void onDataInvalidated() {
        this.serializationExecutor.execute(new Runnable() { // from class: org.signal.paging.BufferedPagingController$$ExternalSyntheticLambda3
            @Override // java.lang.Runnable
            public final void run() {
                BufferedPagingController.$r8$lambda$G8rug5SexbuP939rI5cW5vVT9HE(BufferedPagingController.this);
            }
        });
    }

    public /* synthetic */ void lambda$onDataInvalidated$1() {
        PagingController<Key> pagingController = this.activeController;
        if (pagingController != null) {
            pagingController.onDataInvalidated();
        }
        PagedDataSource<Key, Data> pagedDataSource = this.dataSource;
        FixedSizePagingController fixedSizePagingController = new FixedSizePagingController(pagedDataSource, this.config, this.dataStream, pagedDataSource.size());
        this.activeController = fixedSizePagingController;
        fixedSizePagingController.onDataNeededAroundIndex(this.lastRequestedIndex);
    }

    @Override // org.signal.paging.PagingController
    public void onDataItemChanged(Key key) {
        this.serializationExecutor.execute(new Runnable(key) { // from class: org.signal.paging.BufferedPagingController$$ExternalSyntheticLambda2
            public final /* synthetic */ Object f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                BufferedPagingController.m200$r8$lambda$gd1qug1t8cKMLsXXq5GExyPOng(BufferedPagingController.this, this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$onDataItemChanged$2(Object obj) {
        PagingController<Key> pagingController = this.activeController;
        if (pagingController != null) {
            pagingController.onDataItemChanged(obj);
        }
    }

    @Override // org.signal.paging.PagingController
    public void onDataItemInserted(Key key, int i) {
        this.serializationExecutor.execute(new Runnable(key, i) { // from class: org.signal.paging.BufferedPagingController$$ExternalSyntheticLambda0
            public final /* synthetic */ Object f$1;
            public final /* synthetic */ int f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                BufferedPagingController.m199$r8$lambda$216L7Yvl9AaH_MFvJaRAKV6U(BufferedPagingController.this, this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$onDataItemInserted$3(Object obj, int i) {
        PagingController<Key> pagingController = this.activeController;
        if (pagingController != null) {
            pagingController.onDataItemInserted(obj, i);
        }
    }
}
