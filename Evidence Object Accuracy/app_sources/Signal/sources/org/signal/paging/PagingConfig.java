package org.signal.paging;

/* loaded from: classes3.dex */
public final class PagingConfig {
    private final int bufferPages;
    private final int pageSize;
    private final int startIndex;

    private PagingConfig(Builder builder) {
        this.bufferPages = builder.bufferPages;
        this.startIndex = builder.startIndex;
        this.pageSize = builder.pageSize;
    }

    public int bufferPages() {
        return this.bufferPages;
    }

    public int pageSize() {
        return this.pageSize;
    }

    public int startIndex() {
        return this.startIndex;
    }

    /* loaded from: classes3.dex */
    public static class Builder {
        private int bufferPages = 1;
        private int pageSize = 50;
        private int startIndex = 0;

        public Builder setBufferPages(int i) {
            if (i >= 1) {
                this.bufferPages = i;
                return this;
            }
            throw new IllegalArgumentException("You must have at least one buffer page! Requested: " + i);
        }

        public Builder setPageSize(int i) {
            if (i >= 1) {
                this.pageSize = i;
                return this;
            }
            throw new IllegalArgumentException("You must have a page size of at least one! Requested: " + i);
        }

        public Builder setStartIndex(int i) {
            if (i >= 0) {
                this.startIndex = i;
                return this;
            }
            throw new IndexOutOfBoundsException("Requested: " + i);
        }

        public PagingConfig build() {
            return new PagingConfig(this);
        }
    }
}
