package org.signal.paging;

import java.util.List;

/* access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public interface DataStream<Data> {
    void next(List<Data> list);
}
