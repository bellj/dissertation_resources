package org.signal.paging;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes3.dex */
public class CompressedList<E> extends AbstractList<E> {
    private final List<E> wrapped;

    public CompressedList(List<E> list) {
        this.wrapped = new ArrayList(list);
    }

    public CompressedList(int i) {
        this.wrapped = new ArrayList(i);
        for (int i2 = 0; i2 < i; i2++) {
            this.wrapped.add(null);
        }
    }

    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection
    public int size() {
        return this.wrapped.size();
    }

    @Override // java.util.AbstractList, java.util.List
    public E get(int i) {
        return this.wrapped.get(i);
    }

    @Override // java.util.AbstractList, java.util.List
    public E set(int i, E e) {
        return this.wrapped.set(i, e);
    }

    @Override // java.util.AbstractList, java.util.List
    public void add(int i, E e) {
        this.wrapped.add(i, e);
    }
}
