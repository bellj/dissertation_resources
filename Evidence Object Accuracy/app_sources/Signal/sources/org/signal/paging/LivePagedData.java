package org.signal.paging;

import androidx.lifecycle.LiveData;
import java.util.List;

/* loaded from: classes3.dex */
public class LivePagedData<Key, Data> extends PagedData<Key> {
    private final LiveData<List<Data>> data;

    public LivePagedData(LiveData<List<Data>> liveData, PagingController<Key> pagingController) {
        super(pagingController);
        this.data = liveData;
    }

    public LiveData<List<Data>> getData() {
        return this.data;
    }
}
