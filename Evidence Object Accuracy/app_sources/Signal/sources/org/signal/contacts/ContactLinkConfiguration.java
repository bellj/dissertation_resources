package org.signal.contacts;

import android.accounts.Account;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: ContactLinkConfiguration.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0012\u0018\u00002\u00020\u0001Bi\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u0007\u0012\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u0007\u0012\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u0007\u0012\u0006\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u0005\u0012\u0006\u0010\f\u001a\u00020\u0005¢\u0006\u0002\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u000b\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0011R\u001d\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u001d\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0014R\u0011\u0010\n\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0011R\u001d\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0014R\u0011\u0010\f\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0011¨\u0006\u0019"}, d2 = {"Lorg/signal/contacts/ContactLinkConfiguration;", "", "account", "Landroid/accounts/Account;", "appName", "", "messagePrompt", "Lkotlin/Function1;", "callPrompt", "e164Formatter", "messageMimetype", "callMimetype", "syncTag", "(Landroid/accounts/Account;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAccount", "()Landroid/accounts/Account;", "getAppName", "()Ljava/lang/String;", "getCallMimetype", "getCallPrompt", "()Lkotlin/jvm/functions/Function1;", "getE164Formatter", "getMessageMimetype", "getMessagePrompt", "getSyncTag", "contacts_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ContactLinkConfiguration {
    private final Account account;
    private final String appName;
    private final String callMimetype;
    private final Function1<String, String> callPrompt;
    private final Function1<String, String> e164Formatter;
    private final String messageMimetype;
    private final Function1<String, String> messagePrompt;
    private final String syncTag;

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: kotlin.jvm.functions.Function1<? super java.lang.String, java.lang.String> */
    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: kotlin.jvm.functions.Function1<? super java.lang.String, java.lang.String> */
    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: kotlin.jvm.functions.Function1<? super java.lang.String, java.lang.String> */
    /* JADX WARN: Multi-variable type inference failed */
    public ContactLinkConfiguration(Account account, String str, Function1<? super String, String> function1, Function1<? super String, String> function12, Function1<? super String, String> function13, String str2, String str3, String str4) {
        Intrinsics.checkNotNullParameter(account, "account");
        Intrinsics.checkNotNullParameter(str, "appName");
        Intrinsics.checkNotNullParameter(function1, "messagePrompt");
        Intrinsics.checkNotNullParameter(function12, "callPrompt");
        Intrinsics.checkNotNullParameter(function13, "e164Formatter");
        Intrinsics.checkNotNullParameter(str2, "messageMimetype");
        Intrinsics.checkNotNullParameter(str3, "callMimetype");
        Intrinsics.checkNotNullParameter(str4, "syncTag");
        this.account = account;
        this.appName = str;
        this.messagePrompt = function1;
        this.callPrompt = function12;
        this.e164Formatter = function13;
        this.messageMimetype = str2;
        this.callMimetype = str3;
        this.syncTag = str4;
    }

    public final Account getAccount() {
        return this.account;
    }

    public final String getAppName() {
        return this.appName;
    }

    public final Function1<String, String> getMessagePrompt() {
        return this.messagePrompt;
    }

    public final Function1<String, String> getCallPrompt() {
        return this.callPrompt;
    }

    public final Function1<String, String> getE164Formatter() {
        return this.e164Formatter;
    }

    public final String getMessageMimetype() {
        return this.messageMimetype;
    }

    public final String getCallMimetype() {
        return this.callMimetype;
    }

    public final String getSyncTag() {
        return this.syncTag;
    }
}
