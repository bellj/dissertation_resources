package org.signal.contacts;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.ContactsContract;
import j$.util.Iterator;
import j$.util.function.Consumer;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.ArraysKt___ArraysJvmKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt__MutableCollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.MapsKt__MapsJVMKt;
import kotlin.io.CloseableKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.markers.KMappedMarker;
import org.signal.core.util.CursorExtensionsKt;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.database.CdsDatabase;
import org.thoughtcrime.securesms.database.EmojiSearchDatabase;
import org.thoughtcrime.securesms.registration.fragments.CountryPickerFragment;

/* compiled from: SystemContactsRepository.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\b\u0003\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0016\bÆ\u0002\u0018\u00002\u00020\u0001:\fBCDEFGHIJKLMB\t\b\u0002¢\u0006\u0004\b@\u0010AJ$\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u0004H\u0007J2\u0010\u000b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\t2\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u0004H\u0007J\u0016\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00050\f2\u0006\u0010\u0003\u001a\u00020\u0002H\u0007J\"\u0010\u0011\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u000e\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u0005H\u0007J\u0018\u0010\u0014\u001a\u00020\u00132\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u0010H\u0007J.\u0010\u001a\u001a\u00020\u00132\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0016\u001a\u00020\u00152\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00050\f2\u0006\u0010\u0019\u001a\u00020\u0018H\u0007J\u001a\u0010\u001e\u001a\u0004\u0018\u00010\u001d2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u001c\u001a\u00020\u001bH\u0007J\u001a\u0010\u001f\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u001c\u001a\u00020\u001bH\u0007J\u001e\u0010!\u001a\b\u0012\u0004\u0012\u00020 0\t2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u001c\u001a\u00020\u001bH\u0007J\u001e\u0010#\u001a\b\u0012\u0004\u0012\u00020\"0\t2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u001c\u001a\u00020\u001bH\u0007J\u001e\u0010%\u001a\b\u0012\u0004\u0012\u00020$0\t2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u001c\u001a\u00020\u001bH\u0007J\u001a\u0010'\u001a\u0004\u0018\u00010&2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u001c\u001a\u00020\u001bH\u0007J\"\u0010-\u001a\u00020,2\b\u0010(\u001a\u0004\u0018\u00010\u00052\u0006\u0010)\u001a\u00020\u001b2\u0006\u0010+\u001a\u00020*H\u0002J&\u00102\u001a\b\u0012\u0004\u0012\u00020,0\t2\u0006\u0010.\u001a\u00020*2\u0006\u0010/\u001a\u00020\u00152\u0006\u00101\u001a\u000200H\u0002J&\u00106\u001a\u00020\u00132\f\u00104\u001a\b\u0012\u0004\u0012\u00020,032\u0006\u0010\u0012\u001a\u00020\u00102\u0006\u00105\u001a\u00020\u001bH\u0002J8\u00109\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u000208072\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u00102\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u0004H\u0002J.\u0010;\u001a\u0004\u0018\u0001002\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010:\u001a\u00020\u00052\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00050\u0004H\u0002J\u001a\u0010<\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u001c\u001a\u00020\u001bH\u0002R\u001c\u0010>\u001a\n =*\u0004\u0018\u00010\u00050\u00058\u0002X\u0004¢\u0006\u0006\n\u0004\b>\u0010?¨\u0006N"}, d2 = {"Lorg/signal/contacts/SystemContactsRepository;", "", "Landroid/content/Context;", "context", "Lkotlin/Function1;", "", "e164Formatter", "Lorg/signal/contacts/SystemContactsRepository$ContactIterator;", "getAllSystemContacts", "", "queries", "getContactDetailsByQueries", "", "getAllDisplayNumbers", "applicationId", "accountDisplayName", "Landroid/accounts/Account;", "getOrCreateSystemAccount", "account", "", "removeDeletedRawContactsForAccount", "Lorg/signal/contacts/ContactLinkConfiguration;", "config", "targetE164s", "", "removeIfMissing", "addMessageAndCallLinksToContacts", "", "contactId", "Lorg/signal/contacts/SystemContactsRepository$NameDetails;", "getNameDetails", "getOrganizationName", "Lorg/signal/contacts/SystemContactsRepository$PhoneDetails;", "getPhoneDetails", "Lorg/signal/contacts/SystemContactsRepository$EmailDetails;", "getEmailDetails", "Lorg/signal/contacts/SystemContactsRepository$PostalAddressDetails;", "getPostalAddressDetails", "Landroid/net/Uri;", "getAvatarUri", "displayName", "rawContactId", "", "displayNameSource", "Landroid/content/ContentProviderOperation;", "buildUpdateDisplayNameOperations", "operationIndex", "linkConfig", "Lorg/signal/contacts/SystemContactsRepository$SystemContactInfo;", "systemContactInfo", "buildAddRawContactOperations", "", "operations", "rowId", "removeLinkedContact", "", "Lorg/signal/contacts/SystemContactsRepository$LinkedContactDetails;", "getLinkedContactsByE164", CdsDatabase.E164, "getSystemContactInfo", "getDisplayName", "kotlin.jvm.PlatformType", "TAG", "Ljava/lang/String;", "<init>", "()V", "ContactDetails", "ContactIterator", "ContactPhoneDetails", "CursorContactIterator", "EmailDetails", "EmptyContactIterator", "LinkedContactDetails", "NameDetails", "PhoneDetails", "PostalAddressDetails", "StructuredName", "SystemContactInfo", "contacts_release"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes3.dex */
public final class SystemContactsRepository {
    public static final SystemContactsRepository INSTANCE = new SystemContactsRepository();
    private static final String TAG = Log.tag(SystemContactsRepository.class);

    /* compiled from: SystemContactsRepository.kt */
    @Metadata(bv = {}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003¨\u0006\u0004"}, d2 = {"Lorg/signal/contacts/SystemContactsRepository$ContactIterator;", "", "Lorg/signal/contacts/SystemContactsRepository$ContactDetails;", "Ljava/io/Closeable;", "contacts_release"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes3.dex */
    public interface ContactIterator extends Iterator<ContactDetails>, Closeable, KMappedMarker {
    }

    private SystemContactsRepository() {
    }

    @JvmStatic
    public static final ContactIterator getAllSystemContacts(Context context, Function1<? super String, String> function1) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(function1, "e164Formatter");
        Cursor query = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{"mimetype", "data1", "display_name", "data3", "photo_uri", "_id", "lookup", "data2", "data2", "data3"}, "mimetype IN (?, ?)", SqlUtil.buildArgs("vnd.android.cursor.item/phone_v2", "vnd.android.cursor.item/name"), "lookup ASC, mimetype DESC, _id DESC");
        if (query == null) {
            return new EmptyContactIterator();
        }
        return new CursorContactIterator(query, function1);
    }

    @JvmStatic
    public static final ContactIterator getContactDetailsByQueries(Context context, List<String> list, Function1<? super String, String> function1) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(list, "queries");
        Intrinsics.checkNotNullParameter(function1, "e164Formatter");
        LinkedHashSet<String> linkedHashSet = new LinkedHashSet();
        for (String str : list) {
            Uri withAppendedPath = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_FILTER_URI, Uri.encode(str));
            Intrinsics.checkNotNullExpressionValue(withAppendedPath, "withAppendedPath(Contact…R_URI, Uri.encode(query))");
            Cursor query = context.getContentResolver().query(withAppendedPath, new String[]{"lookup"}, null, null, null);
            th = null;
            while (query != null) {
                try {
                    if (!query.moveToNext()) {
                        break;
                    }
                    String requireString = CursorExtensionsKt.requireString(query, "lookup");
                    if (requireString != null) {
                        linkedHashSet.add(requireString);
                    }
                } finally {
                    try {
                        throw th;
                    } finally {
                    }
                }
            }
            Unit unit = Unit.INSTANCE;
        }
        if (linkedHashSet.isEmpty()) {
            return new EmptyContactIterator();
        }
        Uri uri = ContactsContract.Data.CONTENT_URI;
        String[] strArr = {"mimetype", "data1", "display_name", "data3", "photo_uri", "_id", "lookup", "data2", "data2", "data3"};
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(linkedHashSet, 10));
        for (String str2 : linkedHashSet) {
            arrayList.add("?");
        }
        String str3 = "lookup IN (" + CollectionsKt___CollectionsKt.joinToString$default(arrayList, ",", null, null, 0, null, null, 62, null) + ") AND mimetype IN (?, ?)";
        Object[] array = linkedHashSet.toArray(new String[0]);
        if (array != null) {
            Cursor query2 = context.getContentResolver().query(uri, strArr, str3, (String[]) ArraysKt___ArraysJvmKt.plus(array, (Object[]) SqlUtil.buildArgs("vnd.android.cursor.item/phone_v2", "vnd.android.cursor.item/name")), "lookup ASC, mimetype DESC, _id DESC");
            if (query2 == null) {
                return new EmptyContactIterator();
            }
            return new CursorContactIterator(query2, function1);
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
    }

    @JvmStatic
    public static final Set<String> getAllDisplayNumbers(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        Cursor query = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{"data1"}, null, null, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                String requireString = CursorExtensionsKt.requireString(query, "data1");
                if (requireString != null) {
                    if (requireString.length() > 0) {
                        linkedHashSet.add(requireString);
                    }
                }
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        Unit unit = Unit.INSTANCE;
        th = null;
        return linkedHashSet;
    }

    @JvmStatic
    public static final Account getOrCreateSystemAccount(Context context, String str, String str2) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(str, "applicationId");
        Intrinsics.checkNotNullParameter(str2, "accountDisplayName");
        AccountManager accountManager = AccountManager.get(context);
        Intrinsics.checkNotNullExpressionValue(accountManager, "get(context)");
        Account[] accountsByType = accountManager.getAccountsByType(str);
        Intrinsics.checkNotNullExpressionValue(accountsByType, "accountManager.getAccountsByType(applicationId)");
        Account account = (accountsByType.length == 0) ^ true ? accountsByType[0] : null;
        if (account == null) {
            try {
                String str3 = TAG;
                Log.i(str3, "Attempting to create a new account...");
                Account account2 = new Account(str2, str);
                if (accountManager.addAccountExplicitly(account2, null, null)) {
                    Log.i(str3, "Successfully created a new account.");
                    ContentResolver.setIsSyncable(account2, "com.android.contacts", 1);
                    account = account2;
                } else {
                    Log.w(str3, "Failed to create a new account!");
                }
            } catch (SecurityException e) {
                Log.w(TAG, "Failed to add an account.", e);
            }
        }
        if (account != null && !ContentResolver.getSyncAutomatically(account, "com.android.contacts")) {
            Log.i(TAG, "Updated account to sync automatically.");
            ContentResolver.setSyncAutomatically(account, "com.android.contacts", true);
        }
        return account;
    }

    @JvmStatic
    public static final synchronized void removeDeletedRawContactsForAccount(Context context, Account account) {
        synchronized (SystemContactsRepository.class) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(account, "account");
            Uri build = ContactsContract.RawContacts.CONTENT_URI.buildUpon().appendQueryParameter("account_name", account.name).appendQueryParameter("account_type", account.type).appendQueryParameter("caller_is_syncadapter", "true").build();
            Cursor query = context.getContentResolver().query(build, new String[]{"_id", "sync1"}, "deleted = ?", SqlUtil.buildArgs(1), null);
            if (query != null) {
                while (query.moveToNext()) {
                    long requireLong = CursorExtensionsKt.requireLong(query, "_id");
                    String str = TAG;
                    Log.i(str, "Deleting raw contact: " + CursorExtensionsKt.requireString(query, "sync1") + ", " + requireLong);
                    context.getContentResolver().delete(build, "_id = ?", SqlUtil.buildArgs(requireLong));
                }
                Unit unit = Unit.INSTANCE;
                CloseableKt.closeFinally(query, null);
            }
        }
    }

    @JvmStatic
    public static final synchronized void addMessageAndCallLinksToContacts(Context context, ContactLinkConfiguration contactLinkConfiguration, Set<String> set, boolean z) throws RemoteException, OperationApplicationException {
        SystemContactsRepository systemContactsRepository;
        SystemContactInfo systemContactInfo;
        synchronized (SystemContactsRepository.class) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(contactLinkConfiguration, "config");
            Intrinsics.checkNotNullParameter(set, "targetE164s");
            ArrayList<ContentProviderOperation> arrayList = new ArrayList<>();
            Map<String, LinkedContactDetails> linkedContactsByE164 = INSTANCE.getLinkedContactsByE164(context, contactLinkConfiguration.getAccount(), contactLinkConfiguration.getE164Formatter());
            for (List<String> list : CollectionsKt___CollectionsKt.toList(CollectionsKt___CollectionsKt.chunked(set, 50))) {
                for (String str : list) {
                    if (!linkedContactsByE164.containsKey(str) && (systemContactInfo = (systemContactsRepository = INSTANCE).getSystemContactInfo(context, str, contactLinkConfiguration.getE164Formatter())) != null) {
                        String str2 = TAG;
                        Log.i(str2, "Adding number: " + str);
                        boolean unused = CollectionsKt__MutableCollectionsKt.addAll(arrayList, systemContactsRepository.buildAddRawContactOperations(arrayList.size(), contactLinkConfiguration, systemContactInfo));
                    }
                }
                if (!arrayList.isEmpty()) {
                    context.getContentResolver().applyBatch("com.android.contacts", arrayList);
                    arrayList.clear();
                }
            }
            for (Map.Entry<String, LinkedContactDetails> entry : linkedContactsByE164.entrySet()) {
                String key = entry.getKey();
                LinkedContactDetails value = entry.getValue();
                if (!set.contains(key)) {
                    if (z) {
                        String str3 = TAG;
                        Log.i(str3, "Removing number: " + key);
                        INSTANCE.removeLinkedContact(arrayList, contactLinkConfiguration.getAccount(), value.getId());
                    }
                } else if (!Objects.equals(value.getRawDisplayName(), value.getAggregateDisplayName())) {
                    String str4 = TAG;
                    Log.i(str4, "Updating display name: " + key);
                    arrayList.add(INSTANCE.buildUpdateDisplayNameOperations(value.getAggregateDisplayName(), value.getId(), value.getDisplayNameSource()));
                }
            }
            if (!arrayList.isEmpty()) {
                for (List list2 : CollectionsKt___CollectionsKt.chunked(arrayList, 50)) {
                    context.getContentResolver().applyBatch("com.android.contacts", new ArrayList<>(list2));
                }
            }
        }
    }

    /* JADX WARN: Type inference failed for: r14v3, types: [java.lang.Throwable, org.signal.contacts.SystemContactsRepository$NameDetails] */
    @JvmStatic
    public static final NameDetails getNameDetails(Context context, long j) {
        Intrinsics.checkNotNullParameter(context, "context");
        Cursor query = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{"data1", "data2", "data3", "data4", "data6", "data5"}, "contact_id = ? AND mimetype = ?", SqlUtil.buildArgs(Long.valueOf(j), "vnd.android.cursor.item/name"), null);
        th = 0;
        if (query == null) {
            return th;
        }
        try {
            return query.moveToFirst() ? new NameDetails(CursorExtensionsKt.requireString(query, "data1"), CursorExtensionsKt.requireString(query, "data2"), CursorExtensionsKt.requireString(query, "data3"), CursorExtensionsKt.requireString(query, "data4"), CursorExtensionsKt.requireString(query, "data6"), CursorExtensionsKt.requireString(query, "data5")) : th;
        } catch (Throwable th) {
            try {
                throw th;
            } finally {
                CloseableKt.closeFinally(query, th);
            }
        }
    }

    /* JADX WARN: Type inference failed for: r8v3, types: [java.lang.Throwable, java.lang.String] */
    @JvmStatic
    public static final String getOrganizationName(Context context, long j) {
        Intrinsics.checkNotNullParameter(context, "context");
        Cursor query = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{"data1"}, "contact_id = ? AND mimetype = ?", SqlUtil.buildArgs(Long.valueOf(j), "vnd.android.cursor.item/organization"), null);
        th = 0;
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    return query.getString(0);
                }
                Unit unit = Unit.INSTANCE;
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        return th;
    }

    @JvmStatic
    public static final List<PhoneDetails> getPhoneDetails(Context context, long j) {
        Intrinsics.checkNotNullParameter(context, "context");
        String[] buildArgs = SqlUtil.buildArgs(Long.valueOf(j), "vnd.android.cursor.item/phone_v2");
        ArrayList arrayList = new ArrayList();
        Cursor query = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{"data1", "data2", "data3"}, "contact_id = ? AND mimetype = ?", buildArgs, null);
        if (query != null) {
            th = null;
            while (query.moveToNext()) {
                try {
                    arrayList.add(new PhoneDetails(CursorExtensionsKt.requireString(query, "data1"), CursorExtensionsKt.requireInt(query, "data2"), CursorExtensionsKt.requireString(query, "data3")));
                } finally {
                    try {
                        throw th;
                    } finally {
                    }
                }
            }
            Unit unit = Unit.INSTANCE;
        }
        return arrayList;
    }

    @JvmStatic
    public static final List<EmailDetails> getEmailDetails(Context context, long j) {
        Intrinsics.checkNotNullParameter(context, "context");
        String[] buildArgs = SqlUtil.buildArgs(Long.valueOf(j), "vnd.android.cursor.item/email_v2");
        ArrayList arrayList = new ArrayList();
        Cursor query = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{"data1", "data2", "data3"}, "contact_id = ? AND mimetype = ?", buildArgs, null);
        if (query != null) {
            th = null;
            while (query.moveToNext()) {
                try {
                    arrayList.add(new EmailDetails(CursorExtensionsKt.requireString(query, "data1"), CursorExtensionsKt.requireInt(query, "data2"), CursorExtensionsKt.requireString(query, "data3")));
                } finally {
                    try {
                        throw th;
                    } finally {
                    }
                }
            }
            Unit unit = Unit.INSTANCE;
        }
        return arrayList;
    }

    @JvmStatic
    public static final List<PostalAddressDetails> getPostalAddressDetails(Context context, long j) {
        Intrinsics.checkNotNullParameter(context, "context");
        String[] buildArgs = SqlUtil.buildArgs(Long.valueOf(j), "vnd.android.cursor.item/postal-address_v2");
        ArrayList arrayList = new ArrayList();
        Cursor query = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{"data2", "data3", "data4", "data5", "data6", "data7", "data8", "data9", "data10"}, "contact_id = ? AND mimetype = ?", buildArgs, null);
        if (query != null) {
            th = null;
            while (query.moveToNext()) {
                try {
                    arrayList.add(new PostalAddressDetails(CursorExtensionsKt.requireInt(query, "data2"), CursorExtensionsKt.requireString(query, "data3"), CursorExtensionsKt.requireString(query, "data4"), CursorExtensionsKt.requireString(query, "data5"), CursorExtensionsKt.requireString(query, "data6"), CursorExtensionsKt.requireString(query, "data7"), CursorExtensionsKt.requireString(query, "data8"), CursorExtensionsKt.requireString(query, "data9"), CursorExtensionsKt.requireString(query, "data10")));
                } finally {
                    try {
                        throw th;
                    } finally {
                    }
                }
            }
            Unit unit = Unit.INSTANCE;
        }
        return arrayList;
    }

    /* JADX WARN: Type inference failed for: r8v3, types: [java.lang.Throwable, android.net.Uri] */
    @JvmStatic
    public static final Uri getAvatarUri(Context context, long j) {
        String string;
        Intrinsics.checkNotNullParameter(context, "context");
        Cursor query = context.getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{"photo_uri"}, "contact_id = ? AND mimetype = ?", SqlUtil.buildArgs(Long.valueOf(j), "vnd.android.cursor.item/photo"), null);
        th = 0;
        if (query != null) {
            try {
                if (query.moveToFirst() && (string = query.getString(0)) != null) {
                    return Uri.parse(string);
                }
                Unit unit = Unit.INSTANCE;
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        return th;
    }

    private final ContentProviderOperation buildUpdateDisplayNameOperations(String str, long j, int i) {
        Uri build = ContactsContract.Data.CONTENT_URI.buildUpon().appendQueryParameter("caller_is_syncadapter", "true").build();
        if (i != 40) {
            ContentProviderOperation build2 = ContentProviderOperation.newInsert(build).withValue("raw_contact_id", Long.valueOf(j)).withValue("data1", str).withValue("mimetype", "vnd.android.cursor.item/name").build();
            Intrinsics.checkNotNullExpressionValue(build2, "{\n      ContentProviderO…E)\n        .build()\n    }");
            return build2;
        }
        ContentProviderOperation build3 = ContentProviderOperation.newUpdate(build).withSelection("raw_contact_id = ? AND mimetype = ?", SqlUtil.buildArgs(String.valueOf(j), "vnd.android.cursor.item/name")).withValue("data1", str).withValue("mimetype", "vnd.android.cursor.item/name").build();
        Intrinsics.checkNotNullExpressionValue(build3, "{\n      ContentProviderO…E)\n        .build()\n    }");
        return build3;
    }

    private final List<ContentProviderOperation> buildAddRawContactOperations(int i, ContactLinkConfiguration contactLinkConfiguration, SystemContactInfo systemContactInfo) {
        Uri build = ContactsContract.Data.CONTENT_URI.buildUpon().appendQueryParameter("caller_is_syncadapter", "true").build();
        ContentProviderOperation build2 = ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI).withValue("account_name", contactLinkConfiguration.getAccount().name).withValue("account_type", contactLinkConfiguration.getAccount().type).withValue("sync1", systemContactInfo.getDisplayPhone()).withValue("sync4", "true").build();
        Intrinsics.checkNotNullExpressionValue(build2, "newInsert(ContactsContra…tring())\n        .build()");
        ContentProviderOperation build3 = ContentProviderOperation.newInsert(build).withValueBackReference("raw_contact_id", i).withValue("data1", systemContactInfo.getDisplayName()).withValue("mimetype", "vnd.android.cursor.item/name").build();
        Intrinsics.checkNotNullExpressionValue(build3, "newInsert(dataUri)\n     …EM_TYPE)\n        .build()");
        ContentProviderOperation build4 = ContentProviderOperation.newInsert(build).withValueBackReference("raw_contact_id", i).withValue("mimetype", "vnd.android.cursor.item/phone_v2").withValue("data1", systemContactInfo.getDisplayPhone()).withValue("data2", Integer.valueOf(systemContactInfo.getType())).withValue("data_sync2", contactLinkConfiguration.getSyncTag()).build();
        Intrinsics.checkNotNullExpressionValue(build4, "newInsert(dataUri)\n     …syncTag)\n        .build()");
        ContentProviderOperation build5 = ContentProviderOperation.newInsert(build).withValueBackReference("raw_contact_id", i).withValue("mimetype", contactLinkConfiguration.getMessageMimetype()).withValue("data1", systemContactInfo.getDisplayPhone()).withValue("data2", contactLinkConfiguration.getAppName()).withValue("data3", contactLinkConfiguration.getMessagePrompt().invoke(systemContactInfo.getDisplayPhone())).withYieldAllowed(true).build();
        Intrinsics.checkNotNullExpressionValue(build5, "newInsert(dataUri)\n     …ed(true)\n        .build()");
        ContentProviderOperation build6 = ContentProviderOperation.newInsert(build).withValueBackReference("raw_contact_id", i).withValue("mimetype", contactLinkConfiguration.getCallMimetype()).withValue("data1", systemContactInfo.getDisplayPhone()).withValue("data2", contactLinkConfiguration.getAppName()).withValue("data3", contactLinkConfiguration.getCallPrompt().invoke(systemContactInfo.getDisplayPhone())).withYieldAllowed(true).build();
        Intrinsics.checkNotNullExpressionValue(build6, "newInsert(dataUri)\n     …ed(true)\n        .build()");
        ContentProviderOperation build7 = ContentProviderOperation.newUpdate(ContactsContract.AggregationExceptions.CONTENT_URI).withValue("raw_contact_id1", Long.valueOf(systemContactInfo.getRawContactId())).withValueBackReference("raw_contact_id2", i).withValue("type", 1).build();
        Intrinsics.checkNotNullExpressionValue(build7, "newUpdate(ContactsContra…OGETHER)\n        .build()");
        return CollectionsKt__CollectionsKt.listOf((Object[]) new ContentProviderOperation[]{build2, build3, build4, build5, build6, build7});
    }

    private final void removeLinkedContact(List<ContentProviderOperation> list, Account account, long j) {
        ContentProviderOperation build = ContentProviderOperation.newDelete(ContactsContract.RawContacts.CONTENT_URI.buildUpon().appendQueryParameter("account_name", account.name).appendQueryParameter("account_type", account.type).appendQueryParameter("caller_is_syncadapter", "true").build()).withYieldAllowed(true).withSelection("_id = ?", SqlUtil.buildArgs(j)).build();
        Intrinsics.checkNotNullExpressionValue(build, "newDelete(\n        Conta…(rowId))\n        .build()");
        list.add(build);
    }

    private final Map<String, LinkedContactDetails> getLinkedContactsByE164(Context context, Account account, Function1<? super String, String> function1) {
        HashMap hashMap = new HashMap();
        Cursor query = context.getContentResolver().query(ContactsContract.RawContacts.CONTENT_URI.buildUpon().appendQueryParameter("account_name", account.name).appendQueryParameter("account_type", account.type).build(), new String[]{"_id", "sync1", "sync4", "contact_id", "display_name", "display_name_source"}, null, null, null);
        if (query != null) {
            th = null;
            while (query.moveToNext()) {
                try {
                    String requireString = CursorExtensionsKt.requireString(query, "sync1");
                    if (requireString != null) {
                        hashMap.put(function1.invoke(requireString), new LinkedContactDetails(CursorExtensionsKt.requireLong(query, "_id"), CursorExtensionsKt.requireString(query, "sync4"), CursorExtensionsKt.requireString(query, "display_name"), INSTANCE.getDisplayName(context, CursorExtensionsKt.requireLong(query, "contact_id")), CursorExtensionsKt.requireInt(query, "display_name_source")));
                    }
                } finally {
                    try {
                        throw th;
                    } finally {
                    }
                }
            }
            Unit unit = Unit.INSTANCE;
        }
        return hashMap;
    }

    /* JADX WARN: Type inference failed for: r3v1, types: [java.lang.Throwable, org.signal.contacts.SystemContactsRepository$SystemContactInfo] */
    private final SystemContactInfo getSystemContactInfo(Context context, String str, Function1<? super String, String> function1) {
        Cursor query = context.getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(str)), new String[]{"number", "_id", "display_name", "type"}, null, null, null);
        th = 0;
        if (query != null) {
            while (query.moveToNext()) {
                try {
                    String requireString = CursorExtensionsKt.requireString(query, "number");
                    if (requireString != null && Intrinsics.areEqual(function1.invoke(requireString), str)) {
                        query = context.getContentResolver().query(ContactsContract.RawContacts.CONTENT_URI, new String[]{"_id"}, "contact_id = ? ", SqlUtil.buildArgs(CursorExtensionsKt.requireLong(query, "_id")), null);
                        if (query == null) {
                            continue;
                        } else if (query.moveToNext()) {
                            String requireString2 = CursorExtensionsKt.requireString(query, "display_name");
                            Intrinsics.checkNotNullExpressionValue(query, "idCursor");
                            SystemContactInfo systemContactInfo = new SystemContactInfo(requireString2, requireString, CursorExtensionsKt.requireLong(query, "_id"), CursorExtensionsKt.requireInt(query, "type"));
                            CloseableKt.closeFinally(query, th);
                            return systemContactInfo;
                        } else {
                            Unit unit = Unit.INSTANCE;
                        }
                    }
                } finally {
                    try {
                        throw th;
                    } finally {
                    }
                }
            }
            Unit unit2 = Unit.INSTANCE;
        }
        return th;
    }

    /* JADX WARN: Type inference failed for: r9v1, types: [java.lang.Throwable, java.lang.String] */
    private final String getDisplayName(Context context, long j) {
        String[] buildArgs = SqlUtil.buildArgs(j);
        Cursor query = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, new String[]{"display_name"}, "_id = ?", buildArgs, null);
        th = 0;
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    return query.getString(0);
                }
                Unit unit = Unit.INSTANCE;
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        return th;
    }

    /* compiled from: SystemContactsRepository.kt */
    /* access modifiers changed from: private */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016J\t\u0010\u0005\u001a\u00020\u0006H\u0002J\t\u0010\u0007\u001a\u00020\bH\u0002¨\u0006\t"}, d2 = {"Lorg/signal/contacts/SystemContactsRepository$EmptyContactIterator;", "Lorg/signal/contacts/SystemContactsRepository$ContactIterator;", "()V", "close", "", "hasNext", "", "next", "Lorg/signal/contacts/SystemContactsRepository$ContactDetails;", "contacts_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class EmptyContactIterator implements ContactIterator, j$.util.Iterator {
        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
        }

        @Override // j$.util.Iterator
        public /* synthetic */ void forEachRemaining(Consumer consumer) {
            Iterator.CC.$default$forEachRemaining(this, consumer);
        }

        @Override // java.util.Iterator
        public /* synthetic */ void forEachRemaining(java.util.function.Consumer<? super ContactDetails> consumer) {
            forEachRemaining(Consumer.VivifiedWrapper.convert(consumer));
        }

        @Override // java.util.Iterator, j$.util.Iterator
        public boolean hasNext() {
            return false;
        }

        @Override // java.util.Iterator, j$.util.Iterator
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        @Override // java.util.Iterator, j$.util.Iterator
        public ContactDetails next() {
            throw new NoSuchElementException();
        }
    }

    /* compiled from: SystemContactsRepository.kt */
    /* access modifiers changed from: private */
    @Metadata(bv = {}, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\t\u001a\u00020\b\u0012\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\u0015¢\u0006\u0004\b\u0018\u0010\u0019J\t\u0010\u0003\u001a\u00020\u0002H\u0002J\t\u0010\u0005\u001a\u00020\u0004H\u0002J\b\u0010\u0007\u001a\u00020\u0006H\u0016J\u001c\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\f2\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\nJ\u0018\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\nJ\n\u0010\u0011\u001a\u00020\n*\u00020\bJ\n\u0010\u0012\u001a\u00020\u0002*\u00020\bJ\n\u0010\u0013\u001a\u00020\u0002*\u00020\bR\u0014\u0010\t\u001a\u00020\b8\u0002X\u0004¢\u0006\u0006\n\u0004\b\t\u0010\u0014R \u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\u00158\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017¨\u0006\u001a"}, d2 = {"Lorg/signal/contacts/SystemContactsRepository$CursorContactIterator;", "Lorg/signal/contacts/SystemContactsRepository$ContactIterator;", "", "hasNext", "Lorg/signal/contacts/SystemContactsRepository$ContactDetails;", "next", "", "close", "Landroid/database/Cursor;", "cursor", "", "lookupKey", "", "Lorg/signal/contacts/SystemContactsRepository$ContactPhoneDetails;", "readAllPhones", "Lorg/signal/contacts/SystemContactsRepository$StructuredName;", "readStructuredName", "getLookupKey", "isPhoneMimeType", "isNameMimeType", "Landroid/database/Cursor;", "Lkotlin/Function1;", "e164Formatter", "Lkotlin/jvm/functions/Function1;", "<init>", "(Landroid/database/Cursor;Lkotlin/jvm/functions/Function1;)V", "contacts_release"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes3.dex */
    public static final class CursorContactIterator implements ContactIterator, j$.util.Iterator {
        private final Cursor cursor;
        private final Function1<String, String> e164Formatter;

        @Override // j$.util.Iterator
        public /* synthetic */ void forEachRemaining(Consumer consumer) {
            Iterator.CC.$default$forEachRemaining(this, consumer);
        }

        @Override // java.util.Iterator
        public /* synthetic */ void forEachRemaining(java.util.function.Consumer<? super ContactDetails> consumer) {
            forEachRemaining(Consumer.VivifiedWrapper.convert(consumer));
        }

        @Override // java.util.Iterator, j$.util.Iterator
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: kotlin.jvm.functions.Function1<? super java.lang.String, java.lang.String> */
        /* JADX WARN: Multi-variable type inference failed */
        public CursorContactIterator(Cursor cursor, Function1<? super String, String> function1) {
            Intrinsics.checkNotNullParameter(cursor, "cursor");
            Intrinsics.checkNotNullParameter(function1, "e164Formatter");
            this.cursor = cursor;
            this.e164Formatter = function1;
            cursor.moveToFirst();
        }

        @Override // java.util.Iterator, j$.util.Iterator
        public boolean hasNext() {
            return !this.cursor.isAfterLast() && this.cursor.getPosition() >= 0;
        }

        @Override // java.util.Iterator, j$.util.Iterator
        public ContactDetails next() {
            if (this.cursor.isAfterLast() || this.cursor.getPosition() < 0) {
                throw new NoSuchElementException();
            }
            String lookupKey = getLookupKey(this.cursor);
            List<ContactPhoneDetails> readAllPhones = readAllPhones(this.cursor, lookupKey);
            StructuredName readStructuredName = readStructuredName(this.cursor, lookupKey);
            while (!this.cursor.isAfterLast() && this.cursor.getPosition() >= 0 && Intrinsics.areEqual(getLookupKey(this.cursor), lookupKey)) {
                this.cursor.moveToNext();
            }
            String str = null;
            String givenName = readStructuredName != null ? readStructuredName.getGivenName() : null;
            if (readStructuredName != null) {
                str = readStructuredName.getFamilyName();
            }
            return new ContactDetails(givenName, str, readAllPhones);
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            this.cursor.close();
        }

        public final List<ContactPhoneDetails> readAllPhones(Cursor cursor, String str) {
            Object obj;
            Intrinsics.checkNotNullParameter(cursor, "cursor");
            Intrinsics.checkNotNullParameter(str, "lookupKey");
            ArrayList arrayList = new ArrayList();
            while (!cursor.isAfterLast() && Intrinsics.areEqual(str, getLookupKey(cursor)) && isPhoneMimeType(cursor)) {
                String requireString = CursorExtensionsKt.requireString(cursor, "data1");
                if (requireString != null) {
                    if (requireString.length() > 0) {
                        Uri lookupUri = ContactsContract.Contacts.getLookupUri(CursorExtensionsKt.requireLong(cursor, "_id"), str);
                        Intrinsics.checkNotNullExpressionValue(lookupUri, "getLookupUri(cursor.requ…ds.Phone._ID), lookupKey)");
                        arrayList.add(new ContactPhoneDetails(lookupUri, CursorExtensionsKt.requireString(cursor, "display_name"), CursorExtensionsKt.requireString(cursor, "photo_uri"), this.e164Formatter.invoke(requireString), CursorExtensionsKt.requireInt(cursor, "data2"), CursorExtensionsKt.requireString(cursor, "data3")));
                        cursor.moveToNext();
                    }
                }
                Log.w(SystemContactsRepository.TAG, "Skipping phone entry with invalid number!");
                cursor.moveToNext();
            }
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (Object obj2 : arrayList) {
                String number = ((ContactPhoneDetails) obj2).getNumber();
                Object obj3 = linkedHashMap.get(number);
                if (obj3 == null) {
                    obj3 = new ArrayList();
                    linkedHashMap.put(number, obj3);
                }
                ((List) obj3).add(obj2);
            }
            LinkedHashMap linkedHashMap2 = new LinkedHashMap(MapsKt__MapsJVMKt.mapCapacity(linkedHashMap.size()));
            for (Map.Entry entry : linkedHashMap.entrySet()) {
                Object key = entry.getKey();
                java.util.Iterator it = ((Iterable) entry.getValue()).iterator();
                if (!it.hasNext()) {
                    obj = null;
                } else {
                    Object next = it.next();
                    if (!it.hasNext()) {
                        obj = next;
                    } else {
                        int type = ((ContactPhoneDetails) next).getType();
                        do {
                            Object next2 = it.next();
                            int type2 = ((ContactPhoneDetails) next2).getType();
                            if (type > type2) {
                                next = next2;
                                type = type2;
                            }
                        } while (it.hasNext());
                        obj = next;
                    }
                }
                Intrinsics.checkNotNull(obj);
                linkedHashMap2.put(key, (ContactPhoneDetails) obj);
            }
            return CollectionsKt___CollectionsKt.toList(linkedHashMap2.values());
        }

        public final StructuredName readStructuredName(Cursor cursor, String str) {
            Intrinsics.checkNotNullParameter(cursor, "cursor");
            Intrinsics.checkNotNullParameter(str, "lookupKey");
            if (cursor.isAfterLast() || !Intrinsics.areEqual(getLookupKey(cursor), str) || !isNameMimeType(cursor)) {
                return null;
            }
            return new StructuredName(CursorExtensionsKt.requireString(cursor, "data2"), CursorExtensionsKt.requireString(cursor, "data3"));
        }

        public final String getLookupKey(Cursor cursor) {
            Intrinsics.checkNotNullParameter(cursor, "<this>");
            return CursorExtensionsKt.requireNonNullString(cursor, "lookup");
        }

        public final boolean isPhoneMimeType(Cursor cursor) {
            Intrinsics.checkNotNullParameter(cursor, "<this>");
            return Intrinsics.areEqual(CursorExtensionsKt.requireString(cursor, "mimetype"), "vnd.android.cursor.item/phone_v2");
        }

        public final boolean isNameMimeType(Cursor cursor) {
            Intrinsics.checkNotNullParameter(cursor, "<this>");
            return Intrinsics.areEqual(CursorExtensionsKt.requireString(cursor, "mimetype"), "vnd.android.cursor.item/name");
        }
    }

    /* compiled from: SystemContactsRepository.kt */
    @Metadata(bv = {}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\b\b\u0018\u00002\u00020\u0001B)\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0002\u0012\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f¢\u0006\u0004\b\u0015\u0010\u0016J\t\u0010\u0003\u001a\u00020\u0002HÖ\u0001J\t\u0010\u0005\u001a\u00020\u0004HÖ\u0001J\u0013\u0010\b\u001a\u00020\u00072\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001HÖ\u0003R\u0019\u0010\t\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR\u0019\u0010\r\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\r\u0010\n\u001a\u0004\b\u000e\u0010\fR\u001d\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f8\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014¨\u0006\u0017"}, d2 = {"Lorg/signal/contacts/SystemContactsRepository$ContactDetails;", "", "", "toString", "", "hashCode", "other", "", "equals", "givenName", "Ljava/lang/String;", "getGivenName", "()Ljava/lang/String;", "familyName", "getFamilyName", "", "Lorg/signal/contacts/SystemContactsRepository$ContactPhoneDetails;", "numbers", "Ljava/util/List;", "getNumbers", "()Ljava/util/List;", "<init>", "(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V", "contacts_release"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes3.dex */
    public static final class ContactDetails {
        private final String familyName;
        private final String givenName;
        private final List<ContactPhoneDetails> numbers;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ContactDetails)) {
                return false;
            }
            ContactDetails contactDetails = (ContactDetails) obj;
            return Intrinsics.areEqual(this.givenName, contactDetails.givenName) && Intrinsics.areEqual(this.familyName, contactDetails.familyName) && Intrinsics.areEqual(this.numbers, contactDetails.numbers);
        }

        public int hashCode() {
            String str = this.givenName;
            int i = 0;
            int hashCode = (str == null ? 0 : str.hashCode()) * 31;
            String str2 = this.familyName;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return ((hashCode + i) * 31) + this.numbers.hashCode();
        }

        public String toString() {
            return "ContactDetails(givenName=" + this.givenName + ", familyName=" + this.familyName + ", numbers=" + this.numbers + ')';
        }

        public ContactDetails(String str, String str2, List<ContactPhoneDetails> list) {
            Intrinsics.checkNotNullParameter(list, "numbers");
            this.givenName = str;
            this.familyName = str2;
            this.numbers = list;
        }

        public final String getGivenName() {
            return this.givenName;
        }

        public final String getFamilyName() {
            return this.familyName;
        }

        public final List<ContactPhoneDetails> getNumbers() {
            return this.numbers;
        }
    }

    /* compiled from: SystemContactsRepository.kt */
    @Metadata(bv = {}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0015\b\b\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\n\u001a\u00020\t\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u0014\u001a\u00020\u0002\u0012\u0006\u0010\u0016\u001a\u00020\u0004\u0012\b\u0010\u001a\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u001c\u0010\u001dJ\t\u0010\u0003\u001a\u00020\u0002HÖ\u0001J\t\u0010\u0005\u001a\u00020\u0004HÖ\u0001J\u0013\u0010\b\u001a\u00020\u00072\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001HÖ\u0003R\u0017\u0010\n\u001a\u00020\t8\u0006¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\rR\u0019\u0010\u000e\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0012\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u000f\u001a\u0004\b\u0013\u0010\u0011R\u0017\u0010\u0014\u001a\u00020\u00028\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u000f\u001a\u0004\b\u0015\u0010\u0011R\u0017\u0010\u0016\u001a\u00020\u00048\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u0019\u0010\u001a\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\u001a\u0010\u000f\u001a\u0004\b\u001b\u0010\u0011¨\u0006\u001e"}, d2 = {"Lorg/signal/contacts/SystemContactsRepository$ContactPhoneDetails;", "", "", "toString", "", "hashCode", "other", "", "equals", "Landroid/net/Uri;", "contactUri", "Landroid/net/Uri;", "getContactUri", "()Landroid/net/Uri;", "displayName", "Ljava/lang/String;", "getDisplayName", "()Ljava/lang/String;", "photoUri", "getPhotoUri", "number", "getNumber", "type", "I", "getType", "()I", EmojiSearchDatabase.LABEL, "getLabel", "<init>", "(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V", "contacts_release"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes3.dex */
    public static final class ContactPhoneDetails {
        private final Uri contactUri;
        private final String displayName;
        private final String label;
        private final String number;
        private final String photoUri;
        private final int type;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ContactPhoneDetails)) {
                return false;
            }
            ContactPhoneDetails contactPhoneDetails = (ContactPhoneDetails) obj;
            return Intrinsics.areEqual(this.contactUri, contactPhoneDetails.contactUri) && Intrinsics.areEqual(this.displayName, contactPhoneDetails.displayName) && Intrinsics.areEqual(this.photoUri, contactPhoneDetails.photoUri) && Intrinsics.areEqual(this.number, contactPhoneDetails.number) && this.type == contactPhoneDetails.type && Intrinsics.areEqual(this.label, contactPhoneDetails.label);
        }

        public int hashCode() {
            int hashCode = this.contactUri.hashCode() * 31;
            String str = this.displayName;
            int i = 0;
            int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
            String str2 = this.photoUri;
            int hashCode3 = (((((hashCode2 + (str2 == null ? 0 : str2.hashCode())) * 31) + this.number.hashCode()) * 31) + this.type) * 31;
            String str3 = this.label;
            if (str3 != null) {
                i = str3.hashCode();
            }
            return hashCode3 + i;
        }

        public String toString() {
            return "ContactPhoneDetails(contactUri=" + this.contactUri + ", displayName=" + this.displayName + ", photoUri=" + this.photoUri + ", number=" + this.number + ", type=" + this.type + ", label=" + this.label + ')';
        }

        public ContactPhoneDetails(Uri uri, String str, String str2, String str3, int i, String str4) {
            Intrinsics.checkNotNullParameter(uri, "contactUri");
            Intrinsics.checkNotNullParameter(str3, "number");
            this.contactUri = uri;
            this.displayName = str;
            this.photoUri = str2;
            this.number = str3;
            this.type = i;
            this.label = str4;
        }

        public final Uri getContactUri() {
            return this.contactUri;
        }

        public final String getDisplayName() {
            return this.displayName;
        }

        public final String getPhotoUri() {
            return this.photoUri;
        }

        public final String getNumber() {
            return this.number;
        }

        public final int getType() {
            return this.type;
        }

        public final String getLabel() {
            return this.label;
        }
    }

    /* compiled from: SystemContactsRepository.kt */
    @Metadata(bv = {}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0012\b\b\u0018\u00002\u00020\u0001BC\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0013\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0015\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0017\u0010\u0018J\t\u0010\u0003\u001a\u00020\u0002HÖ\u0001J\t\u0010\u0005\u001a\u00020\u0004HÖ\u0001J\u0013\u0010\b\u001a\u00020\u00072\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001HÖ\u0003R\u0019\u0010\t\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR\u0019\u0010\r\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\r\u0010\n\u001a\u0004\b\u000e\u0010\fR\u0019\u0010\u000f\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\u000f\u0010\n\u001a\u0004\b\u0010\u0010\fR\u0019\u0010\u0011\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\u0011\u0010\n\u001a\u0004\b\u0012\u0010\fR\u0019\u0010\u0013\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\u0013\u0010\n\u001a\u0004\b\u0014\u0010\fR\u0019\u0010\u0015\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\u0015\u0010\n\u001a\u0004\b\u0016\u0010\f¨\u0006\u0019"}, d2 = {"Lorg/signal/contacts/SystemContactsRepository$NameDetails;", "", "", "toString", "", "hashCode", "other", "", "equals", "displayName", "Ljava/lang/String;", "getDisplayName", "()Ljava/lang/String;", "givenName", "getGivenName", "familyName", "getFamilyName", "prefix", "getPrefix", "suffix", "getSuffix", "middleName", "getMiddleName", "<init>", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "contacts_release"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes3.dex */
    public static final class NameDetails {
        private final String displayName;
        private final String familyName;
        private final String givenName;
        private final String middleName;
        private final String prefix;
        private final String suffix;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof NameDetails)) {
                return false;
            }
            NameDetails nameDetails = (NameDetails) obj;
            return Intrinsics.areEqual(this.displayName, nameDetails.displayName) && Intrinsics.areEqual(this.givenName, nameDetails.givenName) && Intrinsics.areEqual(this.familyName, nameDetails.familyName) && Intrinsics.areEqual(this.prefix, nameDetails.prefix) && Intrinsics.areEqual(this.suffix, nameDetails.suffix) && Intrinsics.areEqual(this.middleName, nameDetails.middleName);
        }

        public int hashCode() {
            String str = this.displayName;
            int i = 0;
            int hashCode = (str == null ? 0 : str.hashCode()) * 31;
            String str2 = this.givenName;
            int hashCode2 = (hashCode + (str2 == null ? 0 : str2.hashCode())) * 31;
            String str3 = this.familyName;
            int hashCode3 = (hashCode2 + (str3 == null ? 0 : str3.hashCode())) * 31;
            String str4 = this.prefix;
            int hashCode4 = (hashCode3 + (str4 == null ? 0 : str4.hashCode())) * 31;
            String str5 = this.suffix;
            int hashCode5 = (hashCode4 + (str5 == null ? 0 : str5.hashCode())) * 31;
            String str6 = this.middleName;
            if (str6 != null) {
                i = str6.hashCode();
            }
            return hashCode5 + i;
        }

        public String toString() {
            return "NameDetails(displayName=" + this.displayName + ", givenName=" + this.givenName + ", familyName=" + this.familyName + ", prefix=" + this.prefix + ", suffix=" + this.suffix + ", middleName=" + this.middleName + ')';
        }

        public NameDetails(String str, String str2, String str3, String str4, String str5, String str6) {
            this.displayName = str;
            this.givenName = str2;
            this.familyName = str3;
            this.prefix = str4;
            this.suffix = str5;
            this.middleName = str6;
        }

        public final String getDisplayName() {
            return this.displayName;
        }

        public final String getGivenName() {
            return this.givenName;
        }

        public final String getFamilyName() {
            return this.familyName;
        }

        public final String getPrefix() {
            return this.prefix;
        }

        public final String getSuffix() {
            return this.suffix;
        }

        public final String getMiddleName() {
            return this.middleName;
        }
    }

    /* compiled from: SystemContactsRepository.kt */
    @Metadata(bv = {}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000e\b\b\u0018\u00002\u00020\u0001B#\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\r\u001a\u00020\u0004\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\t\u0010\u0003\u001a\u00020\u0002HÖ\u0001J\t\u0010\u0005\u001a\u00020\u0004HÖ\u0001J\u0013\u0010\b\u001a\u00020\u00072\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001HÖ\u0003R\u0019\u0010\t\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR\u0017\u0010\r\u001a\u00020\u00048\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0011\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\u0011\u0010\n\u001a\u0004\b\u0012\u0010\f¨\u0006\u0015"}, d2 = {"Lorg/signal/contacts/SystemContactsRepository$PhoneDetails;", "", "", "toString", "", "hashCode", "other", "", "equals", "number", "Ljava/lang/String;", "getNumber", "()Ljava/lang/String;", "type", "I", "getType", "()I", EmojiSearchDatabase.LABEL, "getLabel", "<init>", "(Ljava/lang/String;ILjava/lang/String;)V", "contacts_release"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes3.dex */
    public static final class PhoneDetails {
        private final String label;
        private final String number;
        private final int type;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof PhoneDetails)) {
                return false;
            }
            PhoneDetails phoneDetails = (PhoneDetails) obj;
            return Intrinsics.areEqual(this.number, phoneDetails.number) && this.type == phoneDetails.type && Intrinsics.areEqual(this.label, phoneDetails.label);
        }

        public int hashCode() {
            String str = this.number;
            int i = 0;
            int hashCode = (((str == null ? 0 : str.hashCode()) * 31) + this.type) * 31;
            String str2 = this.label;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            return "PhoneDetails(number=" + this.number + ", type=" + this.type + ", label=" + this.label + ')';
        }

        public PhoneDetails(String str, int i, String str2) {
            this.number = str;
            this.type = i;
            this.label = str2;
        }

        public final String getNumber() {
            return this.number;
        }

        public final int getType() {
            return this.type;
        }

        public final String getLabel() {
            return this.label;
        }
    }

    /* compiled from: SystemContactsRepository.kt */
    @Metadata(bv = {}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000e\b\b\u0018\u00002\u00020\u0001B#\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\r\u001a\u00020\u0004\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u0013\u0010\u0014J\t\u0010\u0003\u001a\u00020\u0002HÖ\u0001J\t\u0010\u0005\u001a\u00020\u0004HÖ\u0001J\u0013\u0010\b\u001a\u00020\u00072\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001HÖ\u0003R\u0019\u0010\t\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR\u0017\u0010\r\u001a\u00020\u00048\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0011\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\u0011\u0010\n\u001a\u0004\b\u0012\u0010\f¨\u0006\u0015"}, d2 = {"Lorg/signal/contacts/SystemContactsRepository$EmailDetails;", "", "", "toString", "", "hashCode", "other", "", "equals", "address", "Ljava/lang/String;", "getAddress", "()Ljava/lang/String;", "type", "I", "getType", "()I", EmojiSearchDatabase.LABEL, "getLabel", "<init>", "(Ljava/lang/String;ILjava/lang/String;)V", "contacts_release"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes3.dex */
    public static final class EmailDetails {
        private final String address;
        private final String label;
        private final int type;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof EmailDetails)) {
                return false;
            }
            EmailDetails emailDetails = (EmailDetails) obj;
            return Intrinsics.areEqual(this.address, emailDetails.address) && this.type == emailDetails.type && Intrinsics.areEqual(this.label, emailDetails.label);
        }

        public int hashCode() {
            String str = this.address;
            int i = 0;
            int hashCode = (((str == null ? 0 : str.hashCode()) * 31) + this.type) * 31;
            String str2 = this.label;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            return "EmailDetails(address=" + this.address + ", type=" + this.type + ", label=" + this.label + ')';
        }

        public EmailDetails(String str, int i, String str2) {
            this.address = str;
            this.type = i;
            this.label = str2;
        }

        public final String getAddress() {
            return this.address;
        }

        public final int getType() {
            return this.type;
        }

        public final String getLabel() {
            return this.label;
        }
    }

    /* compiled from: SystemContactsRepository.kt */
    @Metadata(bv = {}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u001a\b\b\u0018\u00002\u00020\u0001B_\u0012\u0006\u0010\t\u001a\u00020\u0004\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0011\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0013\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0015\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0017\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0019\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u001b\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u001d\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u001f\u0010 J\t\u0010\u0003\u001a\u00020\u0002HÖ\u0001J\t\u0010\u0005\u001a\u00020\u0004HÖ\u0001J\u0013\u0010\b\u001a\u00020\u00072\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001HÖ\u0003R\u0017\u0010\t\u001a\u00020\u00048\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR\u0019\u0010\r\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u0019\u0010\u0011\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u000e\u001a\u0004\b\u0012\u0010\u0010R\u0019\u0010\u0013\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\u0013\u0010\u000e\u001a\u0004\b\u0014\u0010\u0010R\u0019\u0010\u0015\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\u0015\u0010\u000e\u001a\u0004\b\u0016\u0010\u0010R\u0019\u0010\u0017\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u000e\u001a\u0004\b\u0018\u0010\u0010R\u0019\u0010\u0019\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u000e\u001a\u0004\b\u001a\u0010\u0010R\u0019\u0010\u001b\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u000e\u001a\u0004\b\u001c\u0010\u0010R\u0019\u0010\u001d\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u000e\u001a\u0004\b\u001e\u0010\u0010¨\u0006!"}, d2 = {"Lorg/signal/contacts/SystemContactsRepository$PostalAddressDetails;", "", "", "toString", "", "hashCode", "other", "", "equals", "type", "I", "getType", "()I", EmojiSearchDatabase.LABEL, "Ljava/lang/String;", "getLabel", "()Ljava/lang/String;", "street", "getStreet", "poBox", "getPoBox", "neighborhood", "getNeighborhood", "city", "getCity", "region", "getRegion", "postal", "getPostal", CountryPickerFragment.KEY_COUNTRY, "getCountry", "<init>", "(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "contacts_release"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes3.dex */
    public static final class PostalAddressDetails {
        private final String city;
        private final String country;
        private final String label;
        private final String neighborhood;
        private final String poBox;
        private final String postal;
        private final String region;
        private final String street;
        private final int type;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof PostalAddressDetails)) {
                return false;
            }
            PostalAddressDetails postalAddressDetails = (PostalAddressDetails) obj;
            return this.type == postalAddressDetails.type && Intrinsics.areEqual(this.label, postalAddressDetails.label) && Intrinsics.areEqual(this.street, postalAddressDetails.street) && Intrinsics.areEqual(this.poBox, postalAddressDetails.poBox) && Intrinsics.areEqual(this.neighborhood, postalAddressDetails.neighborhood) && Intrinsics.areEqual(this.city, postalAddressDetails.city) && Intrinsics.areEqual(this.region, postalAddressDetails.region) && Intrinsics.areEqual(this.postal, postalAddressDetails.postal) && Intrinsics.areEqual(this.country, postalAddressDetails.country);
        }

        public int hashCode() {
            int i = this.type * 31;
            String str = this.label;
            int i2 = 0;
            int hashCode = (i + (str == null ? 0 : str.hashCode())) * 31;
            String str2 = this.street;
            int hashCode2 = (hashCode + (str2 == null ? 0 : str2.hashCode())) * 31;
            String str3 = this.poBox;
            int hashCode3 = (hashCode2 + (str3 == null ? 0 : str3.hashCode())) * 31;
            String str4 = this.neighborhood;
            int hashCode4 = (hashCode3 + (str4 == null ? 0 : str4.hashCode())) * 31;
            String str5 = this.city;
            int hashCode5 = (hashCode4 + (str5 == null ? 0 : str5.hashCode())) * 31;
            String str6 = this.region;
            int hashCode6 = (hashCode5 + (str6 == null ? 0 : str6.hashCode())) * 31;
            String str7 = this.postal;
            int hashCode7 = (hashCode6 + (str7 == null ? 0 : str7.hashCode())) * 31;
            String str8 = this.country;
            if (str8 != null) {
                i2 = str8.hashCode();
            }
            return hashCode7 + i2;
        }

        public String toString() {
            return "PostalAddressDetails(type=" + this.type + ", label=" + this.label + ", street=" + this.street + ", poBox=" + this.poBox + ", neighborhood=" + this.neighborhood + ", city=" + this.city + ", region=" + this.region + ", postal=" + this.postal + ", country=" + this.country + ')';
        }

        public PostalAddressDetails(int i, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8) {
            this.type = i;
            this.label = str;
            this.street = str2;
            this.poBox = str3;
            this.neighborhood = str4;
            this.city = str5;
            this.region = str6;
            this.postal = str7;
            this.country = str8;
        }

        public final int getType() {
            return this.type;
        }

        public final String getLabel() {
            return this.label;
        }

        public final String getStreet() {
            return this.street;
        }

        public final String getPoBox() {
            return this.poBox;
        }

        public final String getNeighborhood() {
            return this.neighborhood;
        }

        public final String getCity() {
            return this.city;
        }

        public final String getRegion() {
            return this.region;
        }

        public final String getPostal() {
            return this.postal;
        }

        public final String getCountry() {
            return this.country;
        }
    }

    /* compiled from: SystemContactsRepository.kt */
    @Metadata(bv = {}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0013\b\b\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\n\u001a\u00020\t\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0012\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\u0014\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\u0016\u001a\u00020\u0004¢\u0006\u0004\b\u001a\u0010\u001bJ\t\u0010\u0003\u001a\u00020\u0002HÖ\u0001J\t\u0010\u0005\u001a\u00020\u0004HÖ\u0001J\u0013\u0010\b\u001a\u00020\u00072\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001HÖ\u0003R\u0017\u0010\n\u001a\u00020\t8\u0006¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\rR\u0019\u0010\u000e\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u0019\u0010\u0012\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u000f\u001a\u0004\b\u0013\u0010\u0011R\u0019\u0010\u0014\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u000f\u001a\u0004\b\u0015\u0010\u0011R\u0017\u0010\u0016\u001a\u00020\u00048\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019¨\u0006\u001c"}, d2 = {"Lorg/signal/contacts/SystemContactsRepository$LinkedContactDetails;", "", "", "toString", "", "hashCode", "other", "", "equals", "", ContactRepository.ID_COLUMN, "J", "getId", "()J", "supportsVoice", "Ljava/lang/String;", "getSupportsVoice", "()Ljava/lang/String;", "rawDisplayName", "getRawDisplayName", "aggregateDisplayName", "getAggregateDisplayName", "displayNameSource", "I", "getDisplayNameSource", "()I", "<init>", "(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V", "contacts_release"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes3.dex */
    public static final class LinkedContactDetails {
        private final String aggregateDisplayName;
        private final int displayNameSource;
        private final long id;
        private final String rawDisplayName;
        private final String supportsVoice;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof LinkedContactDetails)) {
                return false;
            }
            LinkedContactDetails linkedContactDetails = (LinkedContactDetails) obj;
            return this.id == linkedContactDetails.id && Intrinsics.areEqual(this.supportsVoice, linkedContactDetails.supportsVoice) && Intrinsics.areEqual(this.rawDisplayName, linkedContactDetails.rawDisplayName) && Intrinsics.areEqual(this.aggregateDisplayName, linkedContactDetails.aggregateDisplayName) && this.displayNameSource == linkedContactDetails.displayNameSource;
        }

        public int hashCode() {
            int m = SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.id) * 31;
            String str = this.supportsVoice;
            int i = 0;
            int hashCode = (m + (str == null ? 0 : str.hashCode())) * 31;
            String str2 = this.rawDisplayName;
            int hashCode2 = (hashCode + (str2 == null ? 0 : str2.hashCode())) * 31;
            String str3 = this.aggregateDisplayName;
            if (str3 != null) {
                i = str3.hashCode();
            }
            return ((hashCode2 + i) * 31) + this.displayNameSource;
        }

        public String toString() {
            return "LinkedContactDetails(id=" + this.id + ", supportsVoice=" + this.supportsVoice + ", rawDisplayName=" + this.rawDisplayName + ", aggregateDisplayName=" + this.aggregateDisplayName + ", displayNameSource=" + this.displayNameSource + ')';
        }

        public LinkedContactDetails(long j, String str, String str2, String str3, int i) {
            this.id = j;
            this.supportsVoice = str;
            this.rawDisplayName = str2;
            this.aggregateDisplayName = str3;
            this.displayNameSource = i;
        }

        public final long getId() {
            return this.id;
        }

        public final String getRawDisplayName() {
            return this.rawDisplayName;
        }

        public final String getAggregateDisplayName() {
            return this.aggregateDisplayName;
        }

        public final int getDisplayNameSource() {
            return this.displayNameSource;
        }
    }

    /* compiled from: SystemContactsRepository.kt */
    @Metadata(bv = {}, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\t\n\u0002\b\u000b\b\b\u0018\u00002\u00020\u0001B)\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010\r\u001a\u00020\u0002\u0012\u0006\u0010\u0010\u001a\u00020\u000f\u0012\u0006\u0010\u0014\u001a\u00020\u0004¢\u0006\u0004\b\u0018\u0010\u0019J\t\u0010\u0003\u001a\u00020\u0002HÖ\u0001J\t\u0010\u0005\u001a\u00020\u0004HÖ\u0001J\u0013\u0010\b\u001a\u00020\u00072\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001HÖ\u0003R\u0019\u0010\t\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR\u0017\u0010\r\u001a\u00020\u00028\u0006¢\u0006\f\n\u0004\b\r\u0010\n\u001a\u0004\b\u000e\u0010\fR\u0017\u0010\u0010\u001a\u00020\u000f8\u0006¢\u0006\f\n\u0004\b\u0010\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u0017\u0010\u0014\u001a\u00020\u00048\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017¨\u0006\u001a"}, d2 = {"Lorg/signal/contacts/SystemContactsRepository$SystemContactInfo;", "", "", "toString", "", "hashCode", "other", "", "equals", "displayName", "Ljava/lang/String;", "getDisplayName", "()Ljava/lang/String;", "displayPhone", "getDisplayPhone", "", "rawContactId", "J", "getRawContactId", "()J", "type", "I", "getType", "()I", "<init>", "(Ljava/lang/String;Ljava/lang/String;JI)V", "contacts_release"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes3.dex */
    public static final class SystemContactInfo {
        private final String displayName;
        private final String displayPhone;
        private final long rawContactId;
        private final int type;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SystemContactInfo)) {
                return false;
            }
            SystemContactInfo systemContactInfo = (SystemContactInfo) obj;
            return Intrinsics.areEqual(this.displayName, systemContactInfo.displayName) && Intrinsics.areEqual(this.displayPhone, systemContactInfo.displayPhone) && this.rawContactId == systemContactInfo.rawContactId && this.type == systemContactInfo.type;
        }

        public int hashCode() {
            String str = this.displayName;
            return ((((((str == null ? 0 : str.hashCode()) * 31) + this.displayPhone.hashCode()) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.rawContactId)) * 31) + this.type;
        }

        public String toString() {
            return "SystemContactInfo(displayName=" + this.displayName + ", displayPhone=" + this.displayPhone + ", rawContactId=" + this.rawContactId + ", type=" + this.type + ')';
        }

        public SystemContactInfo(String str, String str2, long j, int i) {
            Intrinsics.checkNotNullParameter(str2, "displayPhone");
            this.displayName = str;
            this.displayPhone = str2;
            this.rawContactId = j;
            this.type = i;
        }

        public final String getDisplayName() {
            return this.displayName;
        }

        public final String getDisplayPhone() {
            return this.displayPhone;
        }

        public final long getRawContactId() {
            return this.rawContactId;
        }

        public final int getType() {
            return this.type;
        }
    }

    /* compiled from: SystemContactsRepository.kt */
    @Metadata(bv = {}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\b\b\u0018\u00002\u00020\u0001B\u001b\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0002\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\t\u0010\u0003\u001a\u00020\u0002HÖ\u0001J\t\u0010\u0005\u001a\u00020\u0004HÖ\u0001J\u0013\u0010\b\u001a\u00020\u00072\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001HÖ\u0003R\u0019\u0010\t\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR\u0019\u0010\r\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\r\u0010\n\u001a\u0004\b\u000e\u0010\f¨\u0006\u0011"}, d2 = {"Lorg/signal/contacts/SystemContactsRepository$StructuredName;", "", "", "toString", "", "hashCode", "other", "", "equals", "givenName", "Ljava/lang/String;", "getGivenName", "()Ljava/lang/String;", "familyName", "getFamilyName", "<init>", "(Ljava/lang/String;Ljava/lang/String;)V", "contacts_release"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes3.dex */
    public static final class StructuredName {
        private final String familyName;
        private final String givenName;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StructuredName)) {
                return false;
            }
            StructuredName structuredName = (StructuredName) obj;
            return Intrinsics.areEqual(this.givenName, structuredName.givenName) && Intrinsics.areEqual(this.familyName, structuredName.familyName);
        }

        public int hashCode() {
            String str = this.givenName;
            int i = 0;
            int hashCode = (str == null ? 0 : str.hashCode()) * 31;
            String str2 = this.familyName;
            if (str2 != null) {
                i = str2.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            return "StructuredName(givenName=" + this.givenName + ", familyName=" + this.familyName + ')';
        }

        public StructuredName(String str, String str2) {
            this.givenName = str;
            this.familyName = str2;
        }

        public final String getFamilyName() {
            return this.familyName;
        }

        public final String getGivenName() {
            return this.givenName;
        }
    }
}
