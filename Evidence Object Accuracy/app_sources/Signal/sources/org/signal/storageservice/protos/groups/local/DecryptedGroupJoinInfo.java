package org.signal.storageservice.protos.groups.local;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import org.signal.storageservice.protos.groups.AccessControl;

/* loaded from: classes3.dex */
public final class DecryptedGroupJoinInfo extends GeneratedMessageLite<DecryptedGroupJoinInfo, Builder> implements MessageLiteOrBuilder {
    public static final int ADDFROMINVITELINK_FIELD_NUMBER;
    public static final int AVATAR_FIELD_NUMBER;
    private static final DecryptedGroupJoinInfo DEFAULT_INSTANCE;
    public static final int DESCRIPTION_FIELD_NUMBER;
    public static final int ISANNOUNCEMENTGROUP_FIELD_NUMBER;
    public static final int MEMBERCOUNT_FIELD_NUMBER;
    private static volatile Parser<DecryptedGroupJoinInfo> PARSER;
    public static final int PENDINGADMINAPPROVAL_FIELD_NUMBER;
    public static final int REVISION_FIELD_NUMBER;
    public static final int TITLE_FIELD_NUMBER;
    private int addFromInviteLink_;
    private String avatar_ = "";
    private String description_ = "";
    private boolean isAnnouncementGroup_;
    private int memberCount_;
    private boolean pendingAdminApproval_;
    private int revision_;
    private String title_ = "";

    private DecryptedGroupJoinInfo() {
    }

    public String getTitle() {
        return this.title_;
    }

    public ByteString getTitleBytes() {
        return ByteString.copyFromUtf8(this.title_);
    }

    public void setTitle(String str) {
        str.getClass();
        this.title_ = str;
    }

    public void clearTitle() {
        this.title_ = getDefaultInstance().getTitle();
    }

    public void setTitleBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.title_ = byteString.toStringUtf8();
    }

    public String getAvatar() {
        return this.avatar_;
    }

    public ByteString getAvatarBytes() {
        return ByteString.copyFromUtf8(this.avatar_);
    }

    public void setAvatar(String str) {
        str.getClass();
        this.avatar_ = str;
    }

    public void clearAvatar() {
        this.avatar_ = getDefaultInstance().getAvatar();
    }

    public void setAvatarBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.avatar_ = byteString.toStringUtf8();
    }

    public int getMemberCount() {
        return this.memberCount_;
    }

    public void setMemberCount(int i) {
        this.memberCount_ = i;
    }

    public void clearMemberCount() {
        this.memberCount_ = 0;
    }

    public int getAddFromInviteLinkValue() {
        return this.addFromInviteLink_;
    }

    public AccessControl.AccessRequired getAddFromInviteLink() {
        AccessControl.AccessRequired forNumber = AccessControl.AccessRequired.forNumber(this.addFromInviteLink_);
        return forNumber == null ? AccessControl.AccessRequired.UNRECOGNIZED : forNumber;
    }

    public void setAddFromInviteLinkValue(int i) {
        this.addFromInviteLink_ = i;
    }

    public void setAddFromInviteLink(AccessControl.AccessRequired accessRequired) {
        this.addFromInviteLink_ = accessRequired.getNumber();
    }

    public void clearAddFromInviteLink() {
        this.addFromInviteLink_ = 0;
    }

    public int getRevision() {
        return this.revision_;
    }

    public void setRevision(int i) {
        this.revision_ = i;
    }

    public void clearRevision() {
        this.revision_ = 0;
    }

    public boolean getPendingAdminApproval() {
        return this.pendingAdminApproval_;
    }

    public void setPendingAdminApproval(boolean z) {
        this.pendingAdminApproval_ = z;
    }

    public void clearPendingAdminApproval() {
        this.pendingAdminApproval_ = false;
    }

    public String getDescription() {
        return this.description_;
    }

    public ByteString getDescriptionBytes() {
        return ByteString.copyFromUtf8(this.description_);
    }

    public void setDescription(String str) {
        str.getClass();
        this.description_ = str;
    }

    public void clearDescription() {
        this.description_ = getDefaultInstance().getDescription();
    }

    public void setDescriptionBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.description_ = byteString.toStringUtf8();
    }

    public boolean getIsAnnouncementGroup() {
        return this.isAnnouncementGroup_;
    }

    public void setIsAnnouncementGroup(boolean z) {
        this.isAnnouncementGroup_ = z;
    }

    public void clearIsAnnouncementGroup() {
        this.isAnnouncementGroup_ = false;
    }

    public static DecryptedGroupJoinInfo parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (DecryptedGroupJoinInfo) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static DecryptedGroupJoinInfo parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedGroupJoinInfo) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static DecryptedGroupJoinInfo parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (DecryptedGroupJoinInfo) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static DecryptedGroupJoinInfo parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedGroupJoinInfo) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static DecryptedGroupJoinInfo parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (DecryptedGroupJoinInfo) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static DecryptedGroupJoinInfo parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedGroupJoinInfo) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static DecryptedGroupJoinInfo parseFrom(InputStream inputStream) throws IOException {
        return (DecryptedGroupJoinInfo) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static DecryptedGroupJoinInfo parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedGroupJoinInfo) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static DecryptedGroupJoinInfo parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (DecryptedGroupJoinInfo) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static DecryptedGroupJoinInfo parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedGroupJoinInfo) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static DecryptedGroupJoinInfo parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (DecryptedGroupJoinInfo) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static DecryptedGroupJoinInfo parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedGroupJoinInfo) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(DecryptedGroupJoinInfo decryptedGroupJoinInfo) {
        return DEFAULT_INSTANCE.createBuilder(decryptedGroupJoinInfo);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<DecryptedGroupJoinInfo, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(DecryptedGroupJoinInfo.DEFAULT_INSTANCE);
        }

        public Builder setTitle(String str) {
            copyOnWrite();
            ((DecryptedGroupJoinInfo) this.instance).setTitle(str);
            return this;
        }

        public Builder setAvatar(String str) {
            copyOnWrite();
            ((DecryptedGroupJoinInfo) this.instance).setAvatar(str);
            return this;
        }

        public Builder setMemberCount(int i) {
            copyOnWrite();
            ((DecryptedGroupJoinInfo) this.instance).setMemberCount(i);
            return this;
        }

        public Builder setAddFromInviteLink(AccessControl.AccessRequired accessRequired) {
            copyOnWrite();
            ((DecryptedGroupJoinInfo) this.instance).setAddFromInviteLink(accessRequired);
            return this;
        }

        public Builder setRevision(int i) {
            copyOnWrite();
            ((DecryptedGroupJoinInfo) this.instance).setRevision(i);
            return this;
        }

        public Builder setPendingAdminApproval(boolean z) {
            copyOnWrite();
            ((DecryptedGroupJoinInfo) this.instance).setPendingAdminApproval(z);
            return this;
        }

        public Builder setDescription(String str) {
            copyOnWrite();
            ((DecryptedGroupJoinInfo) this.instance).setDescription(str);
            return this;
        }
    }

    /* renamed from: org.signal.storageservice.protos.groups.local.DecryptedGroupJoinInfo$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new DecryptedGroupJoinInfo();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\b\u0000\u0000\u0002\t\b\u0000\u0000\u0000\u0002Ȉ\u0003Ȉ\u0004\u000b\u0005\f\u0006\u000b\u0007\u0007\bȈ\t\u0007", new Object[]{"title_", "avatar_", "memberCount_", "addFromInviteLink_", "revision_", "pendingAdminApproval_", "description_", "isAnnouncementGroup_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<DecryptedGroupJoinInfo> parser = PARSER;
                if (parser == null) {
                    synchronized (DecryptedGroupJoinInfo.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        DecryptedGroupJoinInfo decryptedGroupJoinInfo = new DecryptedGroupJoinInfo();
        DEFAULT_INSTANCE = decryptedGroupJoinInfo;
        GeneratedMessageLite.registerDefaultInstance(DecryptedGroupJoinInfo.class, decryptedGroupJoinInfo);
    }

    public static DecryptedGroupJoinInfo getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<DecryptedGroupJoinInfo> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
