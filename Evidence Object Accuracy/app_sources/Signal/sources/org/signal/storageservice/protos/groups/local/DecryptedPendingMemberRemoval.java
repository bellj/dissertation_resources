package org.signal.storageservice.protos.groups.local;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class DecryptedPendingMemberRemoval extends GeneratedMessageLite<DecryptedPendingMemberRemoval, Builder> implements DecryptedPendingMemberRemovalOrBuilder {
    private static final DecryptedPendingMemberRemoval DEFAULT_INSTANCE;
    private static volatile Parser<DecryptedPendingMemberRemoval> PARSER;
    public static final int UUIDCIPHERTEXT_FIELD_NUMBER;
    public static final int UUID_FIELD_NUMBER;
    private ByteString uuidCipherText_;
    private ByteString uuid_;

    private DecryptedPendingMemberRemoval() {
        ByteString byteString = ByteString.EMPTY;
        this.uuid_ = byteString;
        this.uuidCipherText_ = byteString;
    }

    public ByteString getUuid() {
        return this.uuid_;
    }

    public void setUuid(ByteString byteString) {
        byteString.getClass();
        this.uuid_ = byteString;
    }

    public void clearUuid() {
        this.uuid_ = getDefaultInstance().getUuid();
    }

    public ByteString getUuidCipherText() {
        return this.uuidCipherText_;
    }

    public void setUuidCipherText(ByteString byteString) {
        byteString.getClass();
        this.uuidCipherText_ = byteString;
    }

    public void clearUuidCipherText() {
        this.uuidCipherText_ = getDefaultInstance().getUuidCipherText();
    }

    public static DecryptedPendingMemberRemoval parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (DecryptedPendingMemberRemoval) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static DecryptedPendingMemberRemoval parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedPendingMemberRemoval) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static DecryptedPendingMemberRemoval parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (DecryptedPendingMemberRemoval) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static DecryptedPendingMemberRemoval parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedPendingMemberRemoval) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static DecryptedPendingMemberRemoval parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (DecryptedPendingMemberRemoval) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static DecryptedPendingMemberRemoval parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedPendingMemberRemoval) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static DecryptedPendingMemberRemoval parseFrom(InputStream inputStream) throws IOException {
        return (DecryptedPendingMemberRemoval) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static DecryptedPendingMemberRemoval parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedPendingMemberRemoval) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static DecryptedPendingMemberRemoval parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (DecryptedPendingMemberRemoval) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static DecryptedPendingMemberRemoval parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedPendingMemberRemoval) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static DecryptedPendingMemberRemoval parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (DecryptedPendingMemberRemoval) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static DecryptedPendingMemberRemoval parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedPendingMemberRemoval) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(DecryptedPendingMemberRemoval decryptedPendingMemberRemoval) {
        return DEFAULT_INSTANCE.createBuilder(decryptedPendingMemberRemoval);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<DecryptedPendingMemberRemoval, Builder> implements DecryptedPendingMemberRemovalOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(DecryptedPendingMemberRemoval.DEFAULT_INSTANCE);
        }

        public Builder setUuid(ByteString byteString) {
            copyOnWrite();
            ((DecryptedPendingMemberRemoval) this.instance).setUuid(byteString);
            return this;
        }

        public Builder setUuidCipherText(ByteString byteString) {
            copyOnWrite();
            ((DecryptedPendingMemberRemoval) this.instance).setUuidCipherText(byteString);
            return this;
        }
    }

    /* renamed from: org.signal.storageservice.protos.groups.local.DecryptedPendingMemberRemoval$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new DecryptedPendingMemberRemoval();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\n\u0002\n", new Object[]{"uuid_", "uuidCipherText_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<DecryptedPendingMemberRemoval> parser = PARSER;
                if (parser == null) {
                    synchronized (DecryptedPendingMemberRemoval.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        DecryptedPendingMemberRemoval decryptedPendingMemberRemoval = new DecryptedPendingMemberRemoval();
        DEFAULT_INSTANCE = decryptedPendingMemberRemoval;
        GeneratedMessageLite.registerDefaultInstance(DecryptedPendingMemberRemoval.class, decryptedPendingMemberRemoval);
    }

    public static DecryptedPendingMemberRemoval getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<DecryptedPendingMemberRemoval> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
