package org.signal.storageservice.protos.groups.local;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;
import org.signal.storageservice.protos.groups.AccessControl;
import org.signal.storageservice.protos.groups.local.DecryptedApproveMember;
import org.signal.storageservice.protos.groups.local.DecryptedBannedMember;
import org.signal.storageservice.protos.groups.local.DecryptedMember;
import org.signal.storageservice.protos.groups.local.DecryptedModifyMemberRole;
import org.signal.storageservice.protos.groups.local.DecryptedPendingMember;
import org.signal.storageservice.protos.groups.local.DecryptedPendingMemberRemoval;
import org.signal.storageservice.protos.groups.local.DecryptedString;
import org.signal.storageservice.protos.groups.local.DecryptedTimer;

/* loaded from: classes3.dex */
public final class DecryptedGroupChange extends GeneratedMessageLite<DecryptedGroupChange, Builder> implements MessageLiteOrBuilder {
    private static final DecryptedGroupChange DEFAULT_INSTANCE;
    public static final int DELETEBANNEDMEMBERS_FIELD_NUMBER;
    public static final int DELETEMEMBERS_FIELD_NUMBER;
    public static final int DELETEPENDINGMEMBERS_FIELD_NUMBER;
    public static final int DELETEREQUESTINGMEMBERS_FIELD_NUMBER;
    public static final int EDITOR_FIELD_NUMBER;
    public static final int MODIFIEDPROFILEKEYS_FIELD_NUMBER;
    public static final int MODIFYMEMBERROLES_FIELD_NUMBER;
    public static final int NEWATTRIBUTEACCESS_FIELD_NUMBER;
    public static final int NEWAVATAR_FIELD_NUMBER;
    public static final int NEWBANNEDMEMBERS_FIELD_NUMBER;
    public static final int NEWDESCRIPTION_FIELD_NUMBER;
    public static final int NEWINVITELINKACCESS_FIELD_NUMBER;
    public static final int NEWINVITELINKPASSWORD_FIELD_NUMBER;
    public static final int NEWISANNOUNCEMENTGROUP_FIELD_NUMBER;
    public static final int NEWMEMBERACCESS_FIELD_NUMBER;
    public static final int NEWMEMBERS_FIELD_NUMBER;
    public static final int NEWPENDINGMEMBERS_FIELD_NUMBER;
    public static final int NEWREQUESTINGMEMBERS_FIELD_NUMBER;
    public static final int NEWTIMER_FIELD_NUMBER;
    public static final int NEWTITLE_FIELD_NUMBER;
    private static volatile Parser<DecryptedGroupChange> PARSER;
    public static final int PROMOTEPENDINGMEMBERS_FIELD_NUMBER;
    public static final int PROMOTEPENDINGPNIACIMEMBERS_FIELD_NUMBER;
    public static final int PROMOTEREQUESTINGMEMBERS_FIELD_NUMBER;
    public static final int REVISION_FIELD_NUMBER;
    private Internal.ProtobufList<DecryptedBannedMember> deleteBannedMembers_;
    private Internal.ProtobufList<ByteString> deleteMembers_ = GeneratedMessageLite.emptyProtobufList();
    private Internal.ProtobufList<DecryptedPendingMemberRemoval> deletePendingMembers_ = GeneratedMessageLite.emptyProtobufList();
    private Internal.ProtobufList<ByteString> deleteRequestingMembers_ = GeneratedMessageLite.emptyProtobufList();
    private ByteString editor_;
    private Internal.ProtobufList<DecryptedMember> modifiedProfileKeys_ = GeneratedMessageLite.emptyProtobufList();
    private Internal.ProtobufList<DecryptedModifyMemberRole> modifyMemberRoles_ = GeneratedMessageLite.emptyProtobufList();
    private int newAttributeAccess_;
    private DecryptedString newAvatar_;
    private Internal.ProtobufList<DecryptedBannedMember> newBannedMembers_;
    private DecryptedString newDescription_;
    private int newInviteLinkAccess_;
    private ByteString newInviteLinkPassword_;
    private int newIsAnnouncementGroup_;
    private int newMemberAccess_;
    private Internal.ProtobufList<DecryptedMember> newMembers_ = GeneratedMessageLite.emptyProtobufList();
    private Internal.ProtobufList<DecryptedPendingMember> newPendingMembers_ = GeneratedMessageLite.emptyProtobufList();
    private Internal.ProtobufList<DecryptedRequestingMember> newRequestingMembers_ = GeneratedMessageLite.emptyProtobufList();
    private DecryptedTimer newTimer_;
    private DecryptedString newTitle_;
    private Internal.ProtobufList<DecryptedMember> promotePendingMembers_ = GeneratedMessageLite.emptyProtobufList();
    private Internal.ProtobufList<DecryptedMember> promotePendingPniAciMembers_;
    private Internal.ProtobufList<DecryptedApproveMember> promoteRequestingMembers_ = GeneratedMessageLite.emptyProtobufList();
    private int revision_;

    private DecryptedGroupChange() {
        ByteString byteString = ByteString.EMPTY;
        this.editor_ = byteString;
        this.newInviteLinkPassword_ = byteString;
        this.newBannedMembers_ = GeneratedMessageLite.emptyProtobufList();
        this.deleteBannedMembers_ = GeneratedMessageLite.emptyProtobufList();
        this.promotePendingPniAciMembers_ = GeneratedMessageLite.emptyProtobufList();
    }

    public ByteString getEditor() {
        return this.editor_;
    }

    public void setEditor(ByteString byteString) {
        byteString.getClass();
        this.editor_ = byteString;
    }

    public void clearEditor() {
        this.editor_ = getDefaultInstance().getEditor();
    }

    public int getRevision() {
        return this.revision_;
    }

    public void setRevision(int i) {
        this.revision_ = i;
    }

    public void clearRevision() {
        this.revision_ = 0;
    }

    public List<DecryptedMember> getNewMembersList() {
        return this.newMembers_;
    }

    public List<? extends DecryptedMemberOrBuilder> getNewMembersOrBuilderList() {
        return this.newMembers_;
    }

    public int getNewMembersCount() {
        return this.newMembers_.size();
    }

    public DecryptedMember getNewMembers(int i) {
        return this.newMembers_.get(i);
    }

    public DecryptedMemberOrBuilder getNewMembersOrBuilder(int i) {
        return this.newMembers_.get(i);
    }

    private void ensureNewMembersIsMutable() {
        Internal.ProtobufList<DecryptedMember> protobufList = this.newMembers_;
        if (!protobufList.isModifiable()) {
            this.newMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setNewMembers(int i, DecryptedMember decryptedMember) {
        decryptedMember.getClass();
        ensureNewMembersIsMutable();
        this.newMembers_.set(i, decryptedMember);
    }

    public void addNewMembers(DecryptedMember decryptedMember) {
        decryptedMember.getClass();
        ensureNewMembersIsMutable();
        this.newMembers_.add(decryptedMember);
    }

    public void addNewMembers(int i, DecryptedMember decryptedMember) {
        decryptedMember.getClass();
        ensureNewMembersIsMutable();
        this.newMembers_.add(i, decryptedMember);
    }

    public void addAllNewMembers(Iterable<? extends DecryptedMember> iterable) {
        ensureNewMembersIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.newMembers_);
    }

    public void clearNewMembers() {
        this.newMembers_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeNewMembers(int i) {
        ensureNewMembersIsMutable();
        this.newMembers_.remove(i);
    }

    public List<ByteString> getDeleteMembersList() {
        return this.deleteMembers_;
    }

    public int getDeleteMembersCount() {
        return this.deleteMembers_.size();
    }

    public ByteString getDeleteMembers(int i) {
        return this.deleteMembers_.get(i);
    }

    private void ensureDeleteMembersIsMutable() {
        Internal.ProtobufList<ByteString> protobufList = this.deleteMembers_;
        if (!protobufList.isModifiable()) {
            this.deleteMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setDeleteMembers(int i, ByteString byteString) {
        byteString.getClass();
        ensureDeleteMembersIsMutable();
        this.deleteMembers_.set(i, byteString);
    }

    public void addDeleteMembers(ByteString byteString) {
        byteString.getClass();
        ensureDeleteMembersIsMutable();
        this.deleteMembers_.add(byteString);
    }

    public void addAllDeleteMembers(Iterable<? extends ByteString> iterable) {
        ensureDeleteMembersIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.deleteMembers_);
    }

    public void clearDeleteMembers() {
        this.deleteMembers_ = GeneratedMessageLite.emptyProtobufList();
    }

    public List<DecryptedModifyMemberRole> getModifyMemberRolesList() {
        return this.modifyMemberRoles_;
    }

    public List<? extends DecryptedModifyMemberRoleOrBuilder> getModifyMemberRolesOrBuilderList() {
        return this.modifyMemberRoles_;
    }

    public int getModifyMemberRolesCount() {
        return this.modifyMemberRoles_.size();
    }

    public DecryptedModifyMemberRole getModifyMemberRoles(int i) {
        return this.modifyMemberRoles_.get(i);
    }

    public DecryptedModifyMemberRoleOrBuilder getModifyMemberRolesOrBuilder(int i) {
        return this.modifyMemberRoles_.get(i);
    }

    private void ensureModifyMemberRolesIsMutable() {
        Internal.ProtobufList<DecryptedModifyMemberRole> protobufList = this.modifyMemberRoles_;
        if (!protobufList.isModifiable()) {
            this.modifyMemberRoles_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setModifyMemberRoles(int i, DecryptedModifyMemberRole decryptedModifyMemberRole) {
        decryptedModifyMemberRole.getClass();
        ensureModifyMemberRolesIsMutable();
        this.modifyMemberRoles_.set(i, decryptedModifyMemberRole);
    }

    public void addModifyMemberRoles(DecryptedModifyMemberRole decryptedModifyMemberRole) {
        decryptedModifyMemberRole.getClass();
        ensureModifyMemberRolesIsMutable();
        this.modifyMemberRoles_.add(decryptedModifyMemberRole);
    }

    public void addModifyMemberRoles(int i, DecryptedModifyMemberRole decryptedModifyMemberRole) {
        decryptedModifyMemberRole.getClass();
        ensureModifyMemberRolesIsMutable();
        this.modifyMemberRoles_.add(i, decryptedModifyMemberRole);
    }

    public void addAllModifyMemberRoles(Iterable<? extends DecryptedModifyMemberRole> iterable) {
        ensureModifyMemberRolesIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.modifyMemberRoles_);
    }

    public void clearModifyMemberRoles() {
        this.modifyMemberRoles_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeModifyMemberRoles(int i) {
        ensureModifyMemberRolesIsMutable();
        this.modifyMemberRoles_.remove(i);
    }

    public List<DecryptedMember> getModifiedProfileKeysList() {
        return this.modifiedProfileKeys_;
    }

    public List<? extends DecryptedMemberOrBuilder> getModifiedProfileKeysOrBuilderList() {
        return this.modifiedProfileKeys_;
    }

    public int getModifiedProfileKeysCount() {
        return this.modifiedProfileKeys_.size();
    }

    public DecryptedMember getModifiedProfileKeys(int i) {
        return this.modifiedProfileKeys_.get(i);
    }

    public DecryptedMemberOrBuilder getModifiedProfileKeysOrBuilder(int i) {
        return this.modifiedProfileKeys_.get(i);
    }

    private void ensureModifiedProfileKeysIsMutable() {
        Internal.ProtobufList<DecryptedMember> protobufList = this.modifiedProfileKeys_;
        if (!protobufList.isModifiable()) {
            this.modifiedProfileKeys_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setModifiedProfileKeys(int i, DecryptedMember decryptedMember) {
        decryptedMember.getClass();
        ensureModifiedProfileKeysIsMutable();
        this.modifiedProfileKeys_.set(i, decryptedMember);
    }

    public void addModifiedProfileKeys(DecryptedMember decryptedMember) {
        decryptedMember.getClass();
        ensureModifiedProfileKeysIsMutable();
        this.modifiedProfileKeys_.add(decryptedMember);
    }

    public void addModifiedProfileKeys(int i, DecryptedMember decryptedMember) {
        decryptedMember.getClass();
        ensureModifiedProfileKeysIsMutable();
        this.modifiedProfileKeys_.add(i, decryptedMember);
    }

    public void addAllModifiedProfileKeys(Iterable<? extends DecryptedMember> iterable) {
        ensureModifiedProfileKeysIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.modifiedProfileKeys_);
    }

    public void clearModifiedProfileKeys() {
        this.modifiedProfileKeys_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeModifiedProfileKeys(int i) {
        ensureModifiedProfileKeysIsMutable();
        this.modifiedProfileKeys_.remove(i);
    }

    public List<DecryptedPendingMember> getNewPendingMembersList() {
        return this.newPendingMembers_;
    }

    public List<? extends DecryptedPendingMemberOrBuilder> getNewPendingMembersOrBuilderList() {
        return this.newPendingMembers_;
    }

    public int getNewPendingMembersCount() {
        return this.newPendingMembers_.size();
    }

    public DecryptedPendingMember getNewPendingMembers(int i) {
        return this.newPendingMembers_.get(i);
    }

    public DecryptedPendingMemberOrBuilder getNewPendingMembersOrBuilder(int i) {
        return this.newPendingMembers_.get(i);
    }

    private void ensureNewPendingMembersIsMutable() {
        Internal.ProtobufList<DecryptedPendingMember> protobufList = this.newPendingMembers_;
        if (!protobufList.isModifiable()) {
            this.newPendingMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setNewPendingMembers(int i, DecryptedPendingMember decryptedPendingMember) {
        decryptedPendingMember.getClass();
        ensureNewPendingMembersIsMutable();
        this.newPendingMembers_.set(i, decryptedPendingMember);
    }

    public void addNewPendingMembers(DecryptedPendingMember decryptedPendingMember) {
        decryptedPendingMember.getClass();
        ensureNewPendingMembersIsMutable();
        this.newPendingMembers_.add(decryptedPendingMember);
    }

    public void addNewPendingMembers(int i, DecryptedPendingMember decryptedPendingMember) {
        decryptedPendingMember.getClass();
        ensureNewPendingMembersIsMutable();
        this.newPendingMembers_.add(i, decryptedPendingMember);
    }

    public void addAllNewPendingMembers(Iterable<? extends DecryptedPendingMember> iterable) {
        ensureNewPendingMembersIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.newPendingMembers_);
    }

    public void clearNewPendingMembers() {
        this.newPendingMembers_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeNewPendingMembers(int i) {
        ensureNewPendingMembersIsMutable();
        this.newPendingMembers_.remove(i);
    }

    public List<DecryptedPendingMemberRemoval> getDeletePendingMembersList() {
        return this.deletePendingMembers_;
    }

    public List<? extends DecryptedPendingMemberRemovalOrBuilder> getDeletePendingMembersOrBuilderList() {
        return this.deletePendingMembers_;
    }

    public int getDeletePendingMembersCount() {
        return this.deletePendingMembers_.size();
    }

    public DecryptedPendingMemberRemoval getDeletePendingMembers(int i) {
        return this.deletePendingMembers_.get(i);
    }

    public DecryptedPendingMemberRemovalOrBuilder getDeletePendingMembersOrBuilder(int i) {
        return this.deletePendingMembers_.get(i);
    }

    private void ensureDeletePendingMembersIsMutable() {
        Internal.ProtobufList<DecryptedPendingMemberRemoval> protobufList = this.deletePendingMembers_;
        if (!protobufList.isModifiable()) {
            this.deletePendingMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setDeletePendingMembers(int i, DecryptedPendingMemberRemoval decryptedPendingMemberRemoval) {
        decryptedPendingMemberRemoval.getClass();
        ensureDeletePendingMembersIsMutable();
        this.deletePendingMembers_.set(i, decryptedPendingMemberRemoval);
    }

    public void addDeletePendingMembers(DecryptedPendingMemberRemoval decryptedPendingMemberRemoval) {
        decryptedPendingMemberRemoval.getClass();
        ensureDeletePendingMembersIsMutable();
        this.deletePendingMembers_.add(decryptedPendingMemberRemoval);
    }

    public void addDeletePendingMembers(int i, DecryptedPendingMemberRemoval decryptedPendingMemberRemoval) {
        decryptedPendingMemberRemoval.getClass();
        ensureDeletePendingMembersIsMutable();
        this.deletePendingMembers_.add(i, decryptedPendingMemberRemoval);
    }

    public void addAllDeletePendingMembers(Iterable<? extends DecryptedPendingMemberRemoval> iterable) {
        ensureDeletePendingMembersIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.deletePendingMembers_);
    }

    public void clearDeletePendingMembers() {
        this.deletePendingMembers_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeDeletePendingMembers(int i) {
        ensureDeletePendingMembersIsMutable();
        this.deletePendingMembers_.remove(i);
    }

    public List<DecryptedMember> getPromotePendingMembersList() {
        return this.promotePendingMembers_;
    }

    public List<? extends DecryptedMemberOrBuilder> getPromotePendingMembersOrBuilderList() {
        return this.promotePendingMembers_;
    }

    public int getPromotePendingMembersCount() {
        return this.promotePendingMembers_.size();
    }

    public DecryptedMember getPromotePendingMembers(int i) {
        return this.promotePendingMembers_.get(i);
    }

    public DecryptedMemberOrBuilder getPromotePendingMembersOrBuilder(int i) {
        return this.promotePendingMembers_.get(i);
    }

    private void ensurePromotePendingMembersIsMutable() {
        Internal.ProtobufList<DecryptedMember> protobufList = this.promotePendingMembers_;
        if (!protobufList.isModifiable()) {
            this.promotePendingMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setPromotePendingMembers(int i, DecryptedMember decryptedMember) {
        decryptedMember.getClass();
        ensurePromotePendingMembersIsMutable();
        this.promotePendingMembers_.set(i, decryptedMember);
    }

    public void addPromotePendingMembers(DecryptedMember decryptedMember) {
        decryptedMember.getClass();
        ensurePromotePendingMembersIsMutable();
        this.promotePendingMembers_.add(decryptedMember);
    }

    public void addPromotePendingMembers(int i, DecryptedMember decryptedMember) {
        decryptedMember.getClass();
        ensurePromotePendingMembersIsMutable();
        this.promotePendingMembers_.add(i, decryptedMember);
    }

    public void addAllPromotePendingMembers(Iterable<? extends DecryptedMember> iterable) {
        ensurePromotePendingMembersIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.promotePendingMembers_);
    }

    public void clearPromotePendingMembers() {
        this.promotePendingMembers_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removePromotePendingMembers(int i) {
        ensurePromotePendingMembersIsMutable();
        this.promotePendingMembers_.remove(i);
    }

    public boolean hasNewTitle() {
        return this.newTitle_ != null;
    }

    public DecryptedString getNewTitle() {
        DecryptedString decryptedString = this.newTitle_;
        return decryptedString == null ? DecryptedString.getDefaultInstance() : decryptedString;
    }

    public void setNewTitle(DecryptedString decryptedString) {
        decryptedString.getClass();
        this.newTitle_ = decryptedString;
    }

    public void mergeNewTitle(DecryptedString decryptedString) {
        decryptedString.getClass();
        DecryptedString decryptedString2 = this.newTitle_;
        if (decryptedString2 == null || decryptedString2 == DecryptedString.getDefaultInstance()) {
            this.newTitle_ = decryptedString;
        } else {
            this.newTitle_ = DecryptedString.newBuilder(this.newTitle_).mergeFrom((DecryptedString.Builder) decryptedString).buildPartial();
        }
    }

    public void clearNewTitle() {
        this.newTitle_ = null;
    }

    public boolean hasNewAvatar() {
        return this.newAvatar_ != null;
    }

    public DecryptedString getNewAvatar() {
        DecryptedString decryptedString = this.newAvatar_;
        return decryptedString == null ? DecryptedString.getDefaultInstance() : decryptedString;
    }

    public void setNewAvatar(DecryptedString decryptedString) {
        decryptedString.getClass();
        this.newAvatar_ = decryptedString;
    }

    public void mergeNewAvatar(DecryptedString decryptedString) {
        decryptedString.getClass();
        DecryptedString decryptedString2 = this.newAvatar_;
        if (decryptedString2 == null || decryptedString2 == DecryptedString.getDefaultInstance()) {
            this.newAvatar_ = decryptedString;
        } else {
            this.newAvatar_ = DecryptedString.newBuilder(this.newAvatar_).mergeFrom((DecryptedString.Builder) decryptedString).buildPartial();
        }
    }

    public void clearNewAvatar() {
        this.newAvatar_ = null;
    }

    public boolean hasNewTimer() {
        return this.newTimer_ != null;
    }

    public DecryptedTimer getNewTimer() {
        DecryptedTimer decryptedTimer = this.newTimer_;
        return decryptedTimer == null ? DecryptedTimer.getDefaultInstance() : decryptedTimer;
    }

    public void setNewTimer(DecryptedTimer decryptedTimer) {
        decryptedTimer.getClass();
        this.newTimer_ = decryptedTimer;
    }

    public void mergeNewTimer(DecryptedTimer decryptedTimer) {
        decryptedTimer.getClass();
        DecryptedTimer decryptedTimer2 = this.newTimer_;
        if (decryptedTimer2 == null || decryptedTimer2 == DecryptedTimer.getDefaultInstance()) {
            this.newTimer_ = decryptedTimer;
        } else {
            this.newTimer_ = DecryptedTimer.newBuilder(this.newTimer_).mergeFrom((DecryptedTimer.Builder) decryptedTimer).buildPartial();
        }
    }

    public void clearNewTimer() {
        this.newTimer_ = null;
    }

    public int getNewAttributeAccessValue() {
        return this.newAttributeAccess_;
    }

    public AccessControl.AccessRequired getNewAttributeAccess() {
        AccessControl.AccessRequired forNumber = AccessControl.AccessRequired.forNumber(this.newAttributeAccess_);
        return forNumber == null ? AccessControl.AccessRequired.UNRECOGNIZED : forNumber;
    }

    public void setNewAttributeAccessValue(int i) {
        this.newAttributeAccess_ = i;
    }

    public void setNewAttributeAccess(AccessControl.AccessRequired accessRequired) {
        this.newAttributeAccess_ = accessRequired.getNumber();
    }

    public void clearNewAttributeAccess() {
        this.newAttributeAccess_ = 0;
    }

    public int getNewMemberAccessValue() {
        return this.newMemberAccess_;
    }

    public AccessControl.AccessRequired getNewMemberAccess() {
        AccessControl.AccessRequired forNumber = AccessControl.AccessRequired.forNumber(this.newMemberAccess_);
        return forNumber == null ? AccessControl.AccessRequired.UNRECOGNIZED : forNumber;
    }

    public void setNewMemberAccessValue(int i) {
        this.newMemberAccess_ = i;
    }

    public void setNewMemberAccess(AccessControl.AccessRequired accessRequired) {
        this.newMemberAccess_ = accessRequired.getNumber();
    }

    public void clearNewMemberAccess() {
        this.newMemberAccess_ = 0;
    }

    public int getNewInviteLinkAccessValue() {
        return this.newInviteLinkAccess_;
    }

    public AccessControl.AccessRequired getNewInviteLinkAccess() {
        AccessControl.AccessRequired forNumber = AccessControl.AccessRequired.forNumber(this.newInviteLinkAccess_);
        return forNumber == null ? AccessControl.AccessRequired.UNRECOGNIZED : forNumber;
    }

    public void setNewInviteLinkAccessValue(int i) {
        this.newInviteLinkAccess_ = i;
    }

    public void setNewInviteLinkAccess(AccessControl.AccessRequired accessRequired) {
        this.newInviteLinkAccess_ = accessRequired.getNumber();
    }

    public void clearNewInviteLinkAccess() {
        this.newInviteLinkAccess_ = 0;
    }

    public List<DecryptedRequestingMember> getNewRequestingMembersList() {
        return this.newRequestingMembers_;
    }

    public List<? extends DecryptedRequestingMemberOrBuilder> getNewRequestingMembersOrBuilderList() {
        return this.newRequestingMembers_;
    }

    public int getNewRequestingMembersCount() {
        return this.newRequestingMembers_.size();
    }

    public DecryptedRequestingMember getNewRequestingMembers(int i) {
        return this.newRequestingMembers_.get(i);
    }

    public DecryptedRequestingMemberOrBuilder getNewRequestingMembersOrBuilder(int i) {
        return this.newRequestingMembers_.get(i);
    }

    private void ensureNewRequestingMembersIsMutable() {
        Internal.ProtobufList<DecryptedRequestingMember> protobufList = this.newRequestingMembers_;
        if (!protobufList.isModifiable()) {
            this.newRequestingMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setNewRequestingMembers(int i, DecryptedRequestingMember decryptedRequestingMember) {
        decryptedRequestingMember.getClass();
        ensureNewRequestingMembersIsMutable();
        this.newRequestingMembers_.set(i, decryptedRequestingMember);
    }

    public void addNewRequestingMembers(DecryptedRequestingMember decryptedRequestingMember) {
        decryptedRequestingMember.getClass();
        ensureNewRequestingMembersIsMutable();
        this.newRequestingMembers_.add(decryptedRequestingMember);
    }

    public void addNewRequestingMembers(int i, DecryptedRequestingMember decryptedRequestingMember) {
        decryptedRequestingMember.getClass();
        ensureNewRequestingMembersIsMutable();
        this.newRequestingMembers_.add(i, decryptedRequestingMember);
    }

    public void addAllNewRequestingMembers(Iterable<? extends DecryptedRequestingMember> iterable) {
        ensureNewRequestingMembersIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.newRequestingMembers_);
    }

    public void clearNewRequestingMembers() {
        this.newRequestingMembers_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeNewRequestingMembers(int i) {
        ensureNewRequestingMembersIsMutable();
        this.newRequestingMembers_.remove(i);
    }

    public List<ByteString> getDeleteRequestingMembersList() {
        return this.deleteRequestingMembers_;
    }

    public int getDeleteRequestingMembersCount() {
        return this.deleteRequestingMembers_.size();
    }

    public ByteString getDeleteRequestingMembers(int i) {
        return this.deleteRequestingMembers_.get(i);
    }

    private void ensureDeleteRequestingMembersIsMutable() {
        Internal.ProtobufList<ByteString> protobufList = this.deleteRequestingMembers_;
        if (!protobufList.isModifiable()) {
            this.deleteRequestingMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setDeleteRequestingMembers(int i, ByteString byteString) {
        byteString.getClass();
        ensureDeleteRequestingMembersIsMutable();
        this.deleteRequestingMembers_.set(i, byteString);
    }

    public void addDeleteRequestingMembers(ByteString byteString) {
        byteString.getClass();
        ensureDeleteRequestingMembersIsMutable();
        this.deleteRequestingMembers_.add(byteString);
    }

    public void addAllDeleteRequestingMembers(Iterable<? extends ByteString> iterable) {
        ensureDeleteRequestingMembersIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.deleteRequestingMembers_);
    }

    public void clearDeleteRequestingMembers() {
        this.deleteRequestingMembers_ = GeneratedMessageLite.emptyProtobufList();
    }

    public List<DecryptedApproveMember> getPromoteRequestingMembersList() {
        return this.promoteRequestingMembers_;
    }

    public List<? extends DecryptedApproveMemberOrBuilder> getPromoteRequestingMembersOrBuilderList() {
        return this.promoteRequestingMembers_;
    }

    public int getPromoteRequestingMembersCount() {
        return this.promoteRequestingMembers_.size();
    }

    public DecryptedApproveMember getPromoteRequestingMembers(int i) {
        return this.promoteRequestingMembers_.get(i);
    }

    public DecryptedApproveMemberOrBuilder getPromoteRequestingMembersOrBuilder(int i) {
        return this.promoteRequestingMembers_.get(i);
    }

    private void ensurePromoteRequestingMembersIsMutable() {
        Internal.ProtobufList<DecryptedApproveMember> protobufList = this.promoteRequestingMembers_;
        if (!protobufList.isModifiable()) {
            this.promoteRequestingMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setPromoteRequestingMembers(int i, DecryptedApproveMember decryptedApproveMember) {
        decryptedApproveMember.getClass();
        ensurePromoteRequestingMembersIsMutable();
        this.promoteRequestingMembers_.set(i, decryptedApproveMember);
    }

    public void addPromoteRequestingMembers(DecryptedApproveMember decryptedApproveMember) {
        decryptedApproveMember.getClass();
        ensurePromoteRequestingMembersIsMutable();
        this.promoteRequestingMembers_.add(decryptedApproveMember);
    }

    public void addPromoteRequestingMembers(int i, DecryptedApproveMember decryptedApproveMember) {
        decryptedApproveMember.getClass();
        ensurePromoteRequestingMembersIsMutable();
        this.promoteRequestingMembers_.add(i, decryptedApproveMember);
    }

    public void addAllPromoteRequestingMembers(Iterable<? extends DecryptedApproveMember> iterable) {
        ensurePromoteRequestingMembersIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.promoteRequestingMembers_);
    }

    public void clearPromoteRequestingMembers() {
        this.promoteRequestingMembers_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removePromoteRequestingMembers(int i) {
        ensurePromoteRequestingMembersIsMutable();
        this.promoteRequestingMembers_.remove(i);
    }

    public ByteString getNewInviteLinkPassword() {
        return this.newInviteLinkPassword_;
    }

    public void setNewInviteLinkPassword(ByteString byteString) {
        byteString.getClass();
        this.newInviteLinkPassword_ = byteString;
    }

    public void clearNewInviteLinkPassword() {
        this.newInviteLinkPassword_ = getDefaultInstance().getNewInviteLinkPassword();
    }

    public boolean hasNewDescription() {
        return this.newDescription_ != null;
    }

    public DecryptedString getNewDescription() {
        DecryptedString decryptedString = this.newDescription_;
        return decryptedString == null ? DecryptedString.getDefaultInstance() : decryptedString;
    }

    public void setNewDescription(DecryptedString decryptedString) {
        decryptedString.getClass();
        this.newDescription_ = decryptedString;
    }

    public void mergeNewDescription(DecryptedString decryptedString) {
        decryptedString.getClass();
        DecryptedString decryptedString2 = this.newDescription_;
        if (decryptedString2 == null || decryptedString2 == DecryptedString.getDefaultInstance()) {
            this.newDescription_ = decryptedString;
        } else {
            this.newDescription_ = DecryptedString.newBuilder(this.newDescription_).mergeFrom((DecryptedString.Builder) decryptedString).buildPartial();
        }
    }

    public void clearNewDescription() {
        this.newDescription_ = null;
    }

    public int getNewIsAnnouncementGroupValue() {
        return this.newIsAnnouncementGroup_;
    }

    public EnabledState getNewIsAnnouncementGroup() {
        EnabledState forNumber = EnabledState.forNumber(this.newIsAnnouncementGroup_);
        return forNumber == null ? EnabledState.UNRECOGNIZED : forNumber;
    }

    public void setNewIsAnnouncementGroupValue(int i) {
        this.newIsAnnouncementGroup_ = i;
    }

    public void setNewIsAnnouncementGroup(EnabledState enabledState) {
        this.newIsAnnouncementGroup_ = enabledState.getNumber();
    }

    public void clearNewIsAnnouncementGroup() {
        this.newIsAnnouncementGroup_ = 0;
    }

    public List<DecryptedBannedMember> getNewBannedMembersList() {
        return this.newBannedMembers_;
    }

    public List<? extends DecryptedBannedMemberOrBuilder> getNewBannedMembersOrBuilderList() {
        return this.newBannedMembers_;
    }

    public int getNewBannedMembersCount() {
        return this.newBannedMembers_.size();
    }

    public DecryptedBannedMember getNewBannedMembers(int i) {
        return this.newBannedMembers_.get(i);
    }

    public DecryptedBannedMemberOrBuilder getNewBannedMembersOrBuilder(int i) {
        return this.newBannedMembers_.get(i);
    }

    private void ensureNewBannedMembersIsMutable() {
        Internal.ProtobufList<DecryptedBannedMember> protobufList = this.newBannedMembers_;
        if (!protobufList.isModifiable()) {
            this.newBannedMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setNewBannedMembers(int i, DecryptedBannedMember decryptedBannedMember) {
        decryptedBannedMember.getClass();
        ensureNewBannedMembersIsMutable();
        this.newBannedMembers_.set(i, decryptedBannedMember);
    }

    public void addNewBannedMembers(DecryptedBannedMember decryptedBannedMember) {
        decryptedBannedMember.getClass();
        ensureNewBannedMembersIsMutable();
        this.newBannedMembers_.add(decryptedBannedMember);
    }

    public void addNewBannedMembers(int i, DecryptedBannedMember decryptedBannedMember) {
        decryptedBannedMember.getClass();
        ensureNewBannedMembersIsMutable();
        this.newBannedMembers_.add(i, decryptedBannedMember);
    }

    public void addAllNewBannedMembers(Iterable<? extends DecryptedBannedMember> iterable) {
        ensureNewBannedMembersIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.newBannedMembers_);
    }

    public void clearNewBannedMembers() {
        this.newBannedMembers_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeNewBannedMembers(int i) {
        ensureNewBannedMembersIsMutable();
        this.newBannedMembers_.remove(i);
    }

    public List<DecryptedBannedMember> getDeleteBannedMembersList() {
        return this.deleteBannedMembers_;
    }

    public List<? extends DecryptedBannedMemberOrBuilder> getDeleteBannedMembersOrBuilderList() {
        return this.deleteBannedMembers_;
    }

    public int getDeleteBannedMembersCount() {
        return this.deleteBannedMembers_.size();
    }

    public DecryptedBannedMember getDeleteBannedMembers(int i) {
        return this.deleteBannedMembers_.get(i);
    }

    public DecryptedBannedMemberOrBuilder getDeleteBannedMembersOrBuilder(int i) {
        return this.deleteBannedMembers_.get(i);
    }

    private void ensureDeleteBannedMembersIsMutable() {
        Internal.ProtobufList<DecryptedBannedMember> protobufList = this.deleteBannedMembers_;
        if (!protobufList.isModifiable()) {
            this.deleteBannedMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setDeleteBannedMembers(int i, DecryptedBannedMember decryptedBannedMember) {
        decryptedBannedMember.getClass();
        ensureDeleteBannedMembersIsMutable();
        this.deleteBannedMembers_.set(i, decryptedBannedMember);
    }

    public void addDeleteBannedMembers(DecryptedBannedMember decryptedBannedMember) {
        decryptedBannedMember.getClass();
        ensureDeleteBannedMembersIsMutable();
        this.deleteBannedMembers_.add(decryptedBannedMember);
    }

    public void addDeleteBannedMembers(int i, DecryptedBannedMember decryptedBannedMember) {
        decryptedBannedMember.getClass();
        ensureDeleteBannedMembersIsMutable();
        this.deleteBannedMembers_.add(i, decryptedBannedMember);
    }

    public void addAllDeleteBannedMembers(Iterable<? extends DecryptedBannedMember> iterable) {
        ensureDeleteBannedMembersIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.deleteBannedMembers_);
    }

    public void clearDeleteBannedMembers() {
        this.deleteBannedMembers_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeDeleteBannedMembers(int i) {
        ensureDeleteBannedMembersIsMutable();
        this.deleteBannedMembers_.remove(i);
    }

    public List<DecryptedMember> getPromotePendingPniAciMembersList() {
        return this.promotePendingPniAciMembers_;
    }

    public List<? extends DecryptedMemberOrBuilder> getPromotePendingPniAciMembersOrBuilderList() {
        return this.promotePendingPniAciMembers_;
    }

    public int getPromotePendingPniAciMembersCount() {
        return this.promotePendingPniAciMembers_.size();
    }

    public DecryptedMember getPromotePendingPniAciMembers(int i) {
        return this.promotePendingPniAciMembers_.get(i);
    }

    public DecryptedMemberOrBuilder getPromotePendingPniAciMembersOrBuilder(int i) {
        return this.promotePendingPniAciMembers_.get(i);
    }

    private void ensurePromotePendingPniAciMembersIsMutable() {
        Internal.ProtobufList<DecryptedMember> protobufList = this.promotePendingPniAciMembers_;
        if (!protobufList.isModifiable()) {
            this.promotePendingPniAciMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setPromotePendingPniAciMembers(int i, DecryptedMember decryptedMember) {
        decryptedMember.getClass();
        ensurePromotePendingPniAciMembersIsMutable();
        this.promotePendingPniAciMembers_.set(i, decryptedMember);
    }

    public void addPromotePendingPniAciMembers(DecryptedMember decryptedMember) {
        decryptedMember.getClass();
        ensurePromotePendingPniAciMembersIsMutable();
        this.promotePendingPniAciMembers_.add(decryptedMember);
    }

    public void addPromotePendingPniAciMembers(int i, DecryptedMember decryptedMember) {
        decryptedMember.getClass();
        ensurePromotePendingPniAciMembersIsMutable();
        this.promotePendingPniAciMembers_.add(i, decryptedMember);
    }

    public void addAllPromotePendingPniAciMembers(Iterable<? extends DecryptedMember> iterable) {
        ensurePromotePendingPniAciMembersIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.promotePendingPniAciMembers_);
    }

    public void clearPromotePendingPniAciMembers() {
        this.promotePendingPniAciMembers_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removePromotePendingPniAciMembers(int i) {
        ensurePromotePendingPniAciMembersIsMutable();
        this.promotePendingPniAciMembers_.remove(i);
    }

    public static DecryptedGroupChange parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (DecryptedGroupChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static DecryptedGroupChange parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedGroupChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static DecryptedGroupChange parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (DecryptedGroupChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static DecryptedGroupChange parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedGroupChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static DecryptedGroupChange parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (DecryptedGroupChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static DecryptedGroupChange parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedGroupChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static DecryptedGroupChange parseFrom(InputStream inputStream) throws IOException {
        return (DecryptedGroupChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static DecryptedGroupChange parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedGroupChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static DecryptedGroupChange parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (DecryptedGroupChange) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static DecryptedGroupChange parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedGroupChange) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static DecryptedGroupChange parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (DecryptedGroupChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static DecryptedGroupChange parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedGroupChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(DecryptedGroupChange decryptedGroupChange) {
        return DEFAULT_INSTANCE.createBuilder(decryptedGroupChange);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<DecryptedGroupChange, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(DecryptedGroupChange.DEFAULT_INSTANCE);
        }

        public Builder setEditor(ByteString byteString) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).setEditor(byteString);
            return this;
        }

        public Builder setRevision(int i) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).setRevision(i);
            return this;
        }

        public List<DecryptedMember> getNewMembersList() {
            return Collections.unmodifiableList(((DecryptedGroupChange) this.instance).getNewMembersList());
        }

        public Builder addNewMembers(DecryptedMember decryptedMember) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).addNewMembers(decryptedMember);
            return this;
        }

        public Builder addNewMembers(DecryptedMember.Builder builder) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).addNewMembers(builder.build());
            return this;
        }

        public Builder removeNewMembers(int i) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).removeNewMembers(i);
            return this;
        }

        public List<ByteString> getDeleteMembersList() {
            return Collections.unmodifiableList(((DecryptedGroupChange) this.instance).getDeleteMembersList());
        }

        public Builder addDeleteMembers(ByteString byteString) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).addDeleteMembers(byteString);
            return this;
        }

        public Builder addAllDeleteMembers(Iterable<? extends ByteString> iterable) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).addAllDeleteMembers(iterable);
            return this;
        }

        public Builder clearDeleteMembers() {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).clearDeleteMembers();
            return this;
        }

        public Builder addModifyMemberRoles(DecryptedModifyMemberRole.Builder builder) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).addModifyMemberRoles(builder.build());
            return this;
        }

        public Builder removeModifyMemberRoles(int i) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).removeModifyMemberRoles(i);
            return this;
        }

        public Builder addModifiedProfileKeys(DecryptedMember decryptedMember) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).addModifiedProfileKeys(decryptedMember);
            return this;
        }

        public Builder addModifiedProfileKeys(DecryptedMember.Builder builder) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).addModifiedProfileKeys(builder.build());
            return this;
        }

        public Builder removeModifiedProfileKeys(int i) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).removeModifiedProfileKeys(i);
            return this;
        }

        public Builder addNewPendingMembers(DecryptedPendingMember decryptedPendingMember) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).addNewPendingMembers(decryptedPendingMember);
            return this;
        }

        public Builder addNewPendingMembers(DecryptedPendingMember.Builder builder) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).addNewPendingMembers(builder.build());
            return this;
        }

        public Builder removeNewPendingMembers(int i) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).removeNewPendingMembers(i);
            return this;
        }

        public Builder addDeletePendingMembers(DecryptedPendingMemberRemoval.Builder builder) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).addDeletePendingMembers(builder.build());
            return this;
        }

        public Builder removeDeletePendingMembers(int i) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).removeDeletePendingMembers(i);
            return this;
        }

        public Builder addPromotePendingMembers(DecryptedMember decryptedMember) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).addPromotePendingMembers(decryptedMember);
            return this;
        }

        public Builder addPromotePendingMembers(int i, DecryptedMember decryptedMember) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).addPromotePendingMembers(i, decryptedMember);
            return this;
        }

        public Builder addPromotePendingMembers(DecryptedMember.Builder builder) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).addPromotePendingMembers(builder.build());
            return this;
        }

        public Builder removePromotePendingMembers(int i) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).removePromotePendingMembers(i);
            return this;
        }

        public Builder setNewTitle(DecryptedString.Builder builder) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).setNewTitle(builder.build());
            return this;
        }

        public Builder clearNewTitle() {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).clearNewTitle();
            return this;
        }

        public Builder setNewAvatar(DecryptedString.Builder builder) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).setNewAvatar(builder.build());
            return this;
        }

        public Builder clearNewAvatar() {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).clearNewAvatar();
            return this;
        }

        public Builder setNewTimer(DecryptedTimer decryptedTimer) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).setNewTimer(decryptedTimer);
            return this;
        }

        public Builder setNewTimer(DecryptedTimer.Builder builder) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).setNewTimer(builder.build());
            return this;
        }

        public Builder clearNewTimer() {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).clearNewTimer();
            return this;
        }

        public Builder setNewAttributeAccess(AccessControl.AccessRequired accessRequired) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).setNewAttributeAccess(accessRequired);
            return this;
        }

        public Builder clearNewAttributeAccess() {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).clearNewAttributeAccess();
            return this;
        }

        public Builder setNewMemberAccess(AccessControl.AccessRequired accessRequired) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).setNewMemberAccess(accessRequired);
            return this;
        }

        public Builder clearNewMemberAccess() {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).clearNewMemberAccess();
            return this;
        }

        public Builder setNewInviteLinkAccess(AccessControl.AccessRequired accessRequired) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).setNewInviteLinkAccess(accessRequired);
            return this;
        }

        public Builder clearNewInviteLinkAccess() {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).clearNewInviteLinkAccess();
            return this;
        }

        public List<DecryptedRequestingMember> getNewRequestingMembersList() {
            return Collections.unmodifiableList(((DecryptedGroupChange) this.instance).getNewRequestingMembersList());
        }

        public Builder addNewRequestingMembers(DecryptedRequestingMember decryptedRequestingMember) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).addNewRequestingMembers(decryptedRequestingMember);
            return this;
        }

        public Builder removeNewRequestingMembers(int i) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).removeNewRequestingMembers(i);
            return this;
        }

        public List<ByteString> getDeleteRequestingMembersList() {
            return Collections.unmodifiableList(((DecryptedGroupChange) this.instance).getDeleteRequestingMembersList());
        }

        public Builder addDeleteRequestingMembers(ByteString byteString) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).addDeleteRequestingMembers(byteString);
            return this;
        }

        public Builder addAllDeleteRequestingMembers(Iterable<? extends ByteString> iterable) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).addAllDeleteRequestingMembers(iterable);
            return this;
        }

        public Builder clearDeleteRequestingMembers() {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).clearDeleteRequestingMembers();
            return this;
        }

        public Builder addPromoteRequestingMembers(DecryptedApproveMember.Builder builder) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).addPromoteRequestingMembers(builder.build());
            return this;
        }

        public Builder removePromoteRequestingMembers(int i) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).removePromoteRequestingMembers(i);
            return this;
        }

        public Builder setNewInviteLinkPassword(ByteString byteString) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).setNewInviteLinkPassword(byteString);
            return this;
        }

        public Builder setNewDescription(DecryptedString.Builder builder) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).setNewDescription(builder.build());
            return this;
        }

        public Builder clearNewDescription() {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).clearNewDescription();
            return this;
        }

        public Builder setNewIsAnnouncementGroup(EnabledState enabledState) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).setNewIsAnnouncementGroup(enabledState);
            return this;
        }

        public Builder clearNewIsAnnouncementGroup() {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).clearNewIsAnnouncementGroup();
            return this;
        }

        public Builder addNewBannedMembers(DecryptedBannedMember decryptedBannedMember) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).addNewBannedMembers(decryptedBannedMember);
            return this;
        }

        public Builder addNewBannedMembers(DecryptedBannedMember.Builder builder) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).addNewBannedMembers(builder.build());
            return this;
        }

        public Builder removeNewBannedMembers(int i) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).removeNewBannedMembers(i);
            return this;
        }

        public Builder addDeleteBannedMembers(DecryptedBannedMember decryptedBannedMember) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).addDeleteBannedMembers(decryptedBannedMember);
            return this;
        }

        public Builder removeDeleteBannedMembers(int i) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).removeDeleteBannedMembers(i);
            return this;
        }

        public Builder addPromotePendingPniAciMembers(DecryptedMember.Builder builder) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).addPromotePendingPniAciMembers(builder.build());
            return this;
        }

        public Builder removePromotePendingPniAciMembers(int i) {
            copyOnWrite();
            ((DecryptedGroupChange) this.instance).removePromotePendingPniAciMembers(i);
            return this;
        }
    }

    /* renamed from: org.signal.storageservice.protos.groups.local.DecryptedGroupChange$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new DecryptedGroupChange();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0018\u0000\u0000\u0001\u0018\u0018\u0000\r\u0000\u0001\n\u0002\u000b\u0003\u001b\u0004\u001c\u0005\u001b\u0006\u001b\u0007\u001b\b\u001b\t\u001b\n\t\u000b\t\f\t\r\f\u000e\f\u000f\f\u0010\u001b\u0011\u001c\u0012\u001b\u0013\n\u0014\t\u0015\f\u0016\u001b\u0017\u001b\u0018\u001b", new Object[]{"editor_", "revision_", "newMembers_", DecryptedMember.class, "deleteMembers_", "modifyMemberRoles_", DecryptedModifyMemberRole.class, "modifiedProfileKeys_", DecryptedMember.class, "newPendingMembers_", DecryptedPendingMember.class, "deletePendingMembers_", DecryptedPendingMemberRemoval.class, "promotePendingMembers_", DecryptedMember.class, "newTitle_", "newAvatar_", "newTimer_", "newAttributeAccess_", "newMemberAccess_", "newInviteLinkAccess_", "newRequestingMembers_", DecryptedRequestingMember.class, "deleteRequestingMembers_", "promoteRequestingMembers_", DecryptedApproveMember.class, "newInviteLinkPassword_", "newDescription_", "newIsAnnouncementGroup_", "newBannedMembers_", DecryptedBannedMember.class, "deleteBannedMembers_", DecryptedBannedMember.class, "promotePendingPniAciMembers_", DecryptedMember.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<DecryptedGroupChange> parser = PARSER;
                if (parser == null) {
                    synchronized (DecryptedGroupChange.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        DecryptedGroupChange decryptedGroupChange = new DecryptedGroupChange();
        DEFAULT_INSTANCE = decryptedGroupChange;
        GeneratedMessageLite.registerDefaultInstance(DecryptedGroupChange.class, decryptedGroupChange);
    }

    public static DecryptedGroupChange getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<DecryptedGroupChange> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
