package org.signal.storageservice.protos.groups.local;

import com.google.protobuf.MessageLiteOrBuilder;

/* loaded from: classes3.dex */
public interface DecryptedRequestingMemberOrBuilder extends MessageLiteOrBuilder {
}
