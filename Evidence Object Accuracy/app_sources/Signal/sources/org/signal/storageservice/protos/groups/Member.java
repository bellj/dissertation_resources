package org.signal.storageservice.protos.groups;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class Member extends GeneratedMessageLite<Member, Builder> implements MemberOrBuilder {
    private static final Member DEFAULT_INSTANCE;
    public static final int JOINEDATREVISION_FIELD_NUMBER;
    private static volatile Parser<Member> PARSER;
    public static final int PRESENTATION_FIELD_NUMBER;
    public static final int PROFILEKEY_FIELD_NUMBER;
    public static final int ROLE_FIELD_NUMBER;
    public static final int USERID_FIELD_NUMBER;
    private int joinedAtRevision_;
    private ByteString presentation_;
    private ByteString profileKey_;
    private int role_;
    private ByteString userId_;

    private Member() {
        ByteString byteString = ByteString.EMPTY;
        this.userId_ = byteString;
        this.profileKey_ = byteString;
        this.presentation_ = byteString;
    }

    /* loaded from: classes3.dex */
    public enum Role implements Internal.EnumLite {
        UNKNOWN(0),
        DEFAULT(1),
        ADMINISTRATOR(2),
        UNRECOGNIZED(-1);
        
        private static final Internal.EnumLiteMap<Role> internalValueMap = new Internal.EnumLiteMap<Role>() { // from class: org.signal.storageservice.protos.groups.Member.Role.1
            @Override // com.google.protobuf.Internal.EnumLiteMap
            public Role findValueByNumber(int i) {
                return Role.forNumber(i);
            }
        };
        private final int value;

        @Override // com.google.protobuf.Internal.EnumLite
        public final int getNumber() {
            if (this != UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }

        public static Role forNumber(int i) {
            if (i == 0) {
                return UNKNOWN;
            }
            if (i == 1) {
                return DEFAULT;
            }
            if (i != 2) {
                return null;
            }
            return ADMINISTRATOR;
        }

        Role(int i) {
            this.value = i;
        }
    }

    public ByteString getUserId() {
        return this.userId_;
    }

    public void setUserId(ByteString byteString) {
        byteString.getClass();
        this.userId_ = byteString;
    }

    public void clearUserId() {
        this.userId_ = getDefaultInstance().getUserId();
    }

    public int getRoleValue() {
        return this.role_;
    }

    public Role getRole() {
        Role forNumber = Role.forNumber(this.role_);
        return forNumber == null ? Role.UNRECOGNIZED : forNumber;
    }

    public void setRoleValue(int i) {
        this.role_ = i;
    }

    public void setRole(Role role) {
        this.role_ = role.getNumber();
    }

    public void clearRole() {
        this.role_ = 0;
    }

    public ByteString getProfileKey() {
        return this.profileKey_;
    }

    public void setProfileKey(ByteString byteString) {
        byteString.getClass();
        this.profileKey_ = byteString;
    }

    public void clearProfileKey() {
        this.profileKey_ = getDefaultInstance().getProfileKey();
    }

    public ByteString getPresentation() {
        return this.presentation_;
    }

    public void setPresentation(ByteString byteString) {
        byteString.getClass();
        this.presentation_ = byteString;
    }

    public void clearPresentation() {
        this.presentation_ = getDefaultInstance().getPresentation();
    }

    public int getJoinedAtRevision() {
        return this.joinedAtRevision_;
    }

    public void setJoinedAtRevision(int i) {
        this.joinedAtRevision_ = i;
    }

    public void clearJoinedAtRevision() {
        this.joinedAtRevision_ = 0;
    }

    public static Member parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Member) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Member parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Member) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Member parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Member) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Member parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Member) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Member parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Member) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Member parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Member) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Member parseFrom(InputStream inputStream) throws IOException {
        return (Member) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Member parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Member) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Member parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Member) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Member parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Member) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Member parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Member) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Member parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Member) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Member member) {
        return DEFAULT_INSTANCE.createBuilder(member);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Member, Builder> implements MemberOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(Member.DEFAULT_INSTANCE);
        }

        public Builder setUserId(ByteString byteString) {
            copyOnWrite();
            ((Member) this.instance).setUserId(byteString);
            return this;
        }

        public Builder setRole(Role role) {
            copyOnWrite();
            ((Member) this.instance).setRole(role);
            return this;
        }

        public Builder setPresentation(ByteString byteString) {
            copyOnWrite();
            ((Member) this.instance).setPresentation(byteString);
            return this;
        }
    }

    /* renamed from: org.signal.storageservice.protos.groups.Member$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Member();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0005\u0000\u0000\u0001\u0005\u0005\u0000\u0000\u0000\u0001\n\u0002\f\u0003\n\u0004\n\u0005\u000b", new Object[]{"userId_", "role_", "profileKey_", "presentation_", "joinedAtRevision_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Member> parser = PARSER;
                if (parser == null) {
                    synchronized (Member.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Member member = new Member();
        DEFAULT_INSTANCE = member;
        GeneratedMessageLite.registerDefaultInstance(Member.class, member);
    }

    public static Member getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Member> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
