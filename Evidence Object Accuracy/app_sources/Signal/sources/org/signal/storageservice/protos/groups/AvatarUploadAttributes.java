package org.signal.storageservice.protos.groups;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class AvatarUploadAttributes extends GeneratedMessageLite<AvatarUploadAttributes, Builder> implements MessageLiteOrBuilder {
    public static final int ACL_FIELD_NUMBER;
    public static final int ALGORITHM_FIELD_NUMBER;
    public static final int CREDENTIAL_FIELD_NUMBER;
    public static final int DATE_FIELD_NUMBER;
    private static final AvatarUploadAttributes DEFAULT_INSTANCE;
    public static final int KEY_FIELD_NUMBER;
    private static volatile Parser<AvatarUploadAttributes> PARSER;
    public static final int POLICY_FIELD_NUMBER;
    public static final int SIGNATURE_FIELD_NUMBER;
    private String acl_ = "";
    private String algorithm_ = "";
    private String credential_ = "";
    private String date_ = "";
    private String key_ = "";
    private String policy_ = "";
    private String signature_ = "";

    private AvatarUploadAttributes() {
    }

    public String getKey() {
        return this.key_;
    }

    public ByteString getKeyBytes() {
        return ByteString.copyFromUtf8(this.key_);
    }

    public void setKey(String str) {
        str.getClass();
        this.key_ = str;
    }

    public void clearKey() {
        this.key_ = getDefaultInstance().getKey();
    }

    public void setKeyBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.key_ = byteString.toStringUtf8();
    }

    public String getCredential() {
        return this.credential_;
    }

    public ByteString getCredentialBytes() {
        return ByteString.copyFromUtf8(this.credential_);
    }

    public void setCredential(String str) {
        str.getClass();
        this.credential_ = str;
    }

    public void clearCredential() {
        this.credential_ = getDefaultInstance().getCredential();
    }

    public void setCredentialBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.credential_ = byteString.toStringUtf8();
    }

    public String getAcl() {
        return this.acl_;
    }

    public ByteString getAclBytes() {
        return ByteString.copyFromUtf8(this.acl_);
    }

    public void setAcl(String str) {
        str.getClass();
        this.acl_ = str;
    }

    public void clearAcl() {
        this.acl_ = getDefaultInstance().getAcl();
    }

    public void setAclBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.acl_ = byteString.toStringUtf8();
    }

    public String getAlgorithm() {
        return this.algorithm_;
    }

    public ByteString getAlgorithmBytes() {
        return ByteString.copyFromUtf8(this.algorithm_);
    }

    public void setAlgorithm(String str) {
        str.getClass();
        this.algorithm_ = str;
    }

    public void clearAlgorithm() {
        this.algorithm_ = getDefaultInstance().getAlgorithm();
    }

    public void setAlgorithmBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.algorithm_ = byteString.toStringUtf8();
    }

    public String getDate() {
        return this.date_;
    }

    public ByteString getDateBytes() {
        return ByteString.copyFromUtf8(this.date_);
    }

    public void setDate(String str) {
        str.getClass();
        this.date_ = str;
    }

    public void clearDate() {
        this.date_ = getDefaultInstance().getDate();
    }

    public void setDateBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.date_ = byteString.toStringUtf8();
    }

    public String getPolicy() {
        return this.policy_;
    }

    public ByteString getPolicyBytes() {
        return ByteString.copyFromUtf8(this.policy_);
    }

    public void setPolicy(String str) {
        str.getClass();
        this.policy_ = str;
    }

    public void clearPolicy() {
        this.policy_ = getDefaultInstance().getPolicy();
    }

    public void setPolicyBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.policy_ = byteString.toStringUtf8();
    }

    public String getSignature() {
        return this.signature_;
    }

    public ByteString getSignatureBytes() {
        return ByteString.copyFromUtf8(this.signature_);
    }

    public void setSignature(String str) {
        str.getClass();
        this.signature_ = str;
    }

    public void clearSignature() {
        this.signature_ = getDefaultInstance().getSignature();
    }

    public void setSignatureBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.signature_ = byteString.toStringUtf8();
    }

    public static AvatarUploadAttributes parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (AvatarUploadAttributes) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static AvatarUploadAttributes parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (AvatarUploadAttributes) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static AvatarUploadAttributes parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (AvatarUploadAttributes) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static AvatarUploadAttributes parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (AvatarUploadAttributes) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static AvatarUploadAttributes parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (AvatarUploadAttributes) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static AvatarUploadAttributes parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (AvatarUploadAttributes) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static AvatarUploadAttributes parseFrom(InputStream inputStream) throws IOException {
        return (AvatarUploadAttributes) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static AvatarUploadAttributes parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (AvatarUploadAttributes) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static AvatarUploadAttributes parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (AvatarUploadAttributes) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static AvatarUploadAttributes parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (AvatarUploadAttributes) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static AvatarUploadAttributes parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (AvatarUploadAttributes) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static AvatarUploadAttributes parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (AvatarUploadAttributes) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(AvatarUploadAttributes avatarUploadAttributes) {
        return DEFAULT_INSTANCE.createBuilder(avatarUploadAttributes);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<AvatarUploadAttributes, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(AvatarUploadAttributes.DEFAULT_INSTANCE);
        }
    }

    /* renamed from: org.signal.storageservice.protos.groups.AvatarUploadAttributes$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new AvatarUploadAttributes();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0007\u0000\u0000\u0001\u0007\u0007\u0000\u0000\u0000\u0001Ȉ\u0002Ȉ\u0003Ȉ\u0004Ȉ\u0005Ȉ\u0006Ȉ\u0007Ȉ", new Object[]{"key_", "credential_", "acl_", "algorithm_", "date_", "policy_", "signature_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<AvatarUploadAttributes> parser = PARSER;
                if (parser == null) {
                    synchronized (AvatarUploadAttributes.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        AvatarUploadAttributes avatarUploadAttributes = new AvatarUploadAttributes();
        DEFAULT_INSTANCE = avatarUploadAttributes;
        GeneratedMessageLite.registerDefaultInstance(AvatarUploadAttributes.class, avatarUploadAttributes);
    }

    public static AvatarUploadAttributes getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<AvatarUploadAttributes> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
