package org.signal.storageservice.protos.groups;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class GroupAttributeBlob extends GeneratedMessageLite<GroupAttributeBlob, Builder> implements MessageLiteOrBuilder {
    public static final int AVATAR_FIELD_NUMBER;
    private static final GroupAttributeBlob DEFAULT_INSTANCE;
    public static final int DESCRIPTION_FIELD_NUMBER;
    public static final int DISAPPEARINGMESSAGESDURATION_FIELD_NUMBER;
    private static volatile Parser<GroupAttributeBlob> PARSER;
    public static final int TITLE_FIELD_NUMBER;
    private int contentCase_ = 0;
    private Object content_;

    private GroupAttributeBlob() {
    }

    /* loaded from: classes3.dex */
    public enum ContentCase {
        TITLE(1),
        AVATAR(2),
        DISAPPEARINGMESSAGESDURATION(3),
        DESCRIPTION(4),
        CONTENT_NOT_SET(0);
        
        private final int value;

        ContentCase(int i) {
            this.value = i;
        }

        public static ContentCase forNumber(int i) {
            if (i == 0) {
                return CONTENT_NOT_SET;
            }
            if (i == 1) {
                return TITLE;
            }
            if (i == 2) {
                return AVATAR;
            }
            if (i == 3) {
                return DISAPPEARINGMESSAGESDURATION;
            }
            if (i != 4) {
                return null;
            }
            return DESCRIPTION;
        }
    }

    public ContentCase getContentCase() {
        return ContentCase.forNumber(this.contentCase_);
    }

    public void clearContent() {
        this.contentCase_ = 0;
        this.content_ = null;
    }

    public boolean hasTitle() {
        return this.contentCase_ == 1;
    }

    public String getTitle() {
        return this.contentCase_ == 1 ? (String) this.content_ : "";
    }

    public ByteString getTitleBytes() {
        return ByteString.copyFromUtf8(this.contentCase_ == 1 ? (String) this.content_ : "");
    }

    public void setTitle(String str) {
        str.getClass();
        this.contentCase_ = 1;
        this.content_ = str;
    }

    public void clearTitle() {
        if (this.contentCase_ == 1) {
            this.contentCase_ = 0;
            this.content_ = null;
        }
    }

    public void setTitleBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.content_ = byteString.toStringUtf8();
        this.contentCase_ = 1;
    }

    public boolean hasAvatar() {
        return this.contentCase_ == 2;
    }

    public ByteString getAvatar() {
        if (this.contentCase_ == 2) {
            return (ByteString) this.content_;
        }
        return ByteString.EMPTY;
    }

    public void setAvatar(ByteString byteString) {
        byteString.getClass();
        this.contentCase_ = 2;
        this.content_ = byteString;
    }

    public void clearAvatar() {
        if (this.contentCase_ == 2) {
            this.contentCase_ = 0;
            this.content_ = null;
        }
    }

    public boolean hasDisappearingMessagesDuration() {
        return this.contentCase_ == 3;
    }

    public int getDisappearingMessagesDuration() {
        if (this.contentCase_ == 3) {
            return ((Integer) this.content_).intValue();
        }
        return 0;
    }

    public void setDisappearingMessagesDuration(int i) {
        this.contentCase_ = 3;
        this.content_ = Integer.valueOf(i);
    }

    public void clearDisappearingMessagesDuration() {
        if (this.contentCase_ == 3) {
            this.contentCase_ = 0;
            this.content_ = null;
        }
    }

    public boolean hasDescription() {
        return this.contentCase_ == 4;
    }

    public String getDescription() {
        return this.contentCase_ == 4 ? (String) this.content_ : "";
    }

    public ByteString getDescriptionBytes() {
        return ByteString.copyFromUtf8(this.contentCase_ == 4 ? (String) this.content_ : "");
    }

    public void setDescription(String str) {
        str.getClass();
        this.contentCase_ = 4;
        this.content_ = str;
    }

    public void clearDescription() {
        if (this.contentCase_ == 4) {
            this.contentCase_ = 0;
            this.content_ = null;
        }
    }

    public void setDescriptionBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.content_ = byteString.toStringUtf8();
        this.contentCase_ = 4;
    }

    public static GroupAttributeBlob parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (GroupAttributeBlob) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static GroupAttributeBlob parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (GroupAttributeBlob) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static GroupAttributeBlob parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (GroupAttributeBlob) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static GroupAttributeBlob parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (GroupAttributeBlob) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static GroupAttributeBlob parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (GroupAttributeBlob) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static GroupAttributeBlob parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (GroupAttributeBlob) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static GroupAttributeBlob parseFrom(InputStream inputStream) throws IOException {
        return (GroupAttributeBlob) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static GroupAttributeBlob parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (GroupAttributeBlob) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static GroupAttributeBlob parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (GroupAttributeBlob) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static GroupAttributeBlob parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (GroupAttributeBlob) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static GroupAttributeBlob parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (GroupAttributeBlob) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static GroupAttributeBlob parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (GroupAttributeBlob) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(GroupAttributeBlob groupAttributeBlob) {
        return DEFAULT_INSTANCE.createBuilder(groupAttributeBlob);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<GroupAttributeBlob, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(GroupAttributeBlob.DEFAULT_INSTANCE);
        }

        public Builder setTitle(String str) {
            copyOnWrite();
            ((GroupAttributeBlob) this.instance).setTitle(str);
            return this;
        }

        public Builder setAvatar(ByteString byteString) {
            copyOnWrite();
            ((GroupAttributeBlob) this.instance).setAvatar(byteString);
            return this;
        }

        public Builder setDisappearingMessagesDuration(int i) {
            copyOnWrite();
            ((GroupAttributeBlob) this.instance).setDisappearingMessagesDuration(i);
            return this;
        }

        public Builder setDescription(String str) {
            copyOnWrite();
            ((GroupAttributeBlob) this.instance).setDescription(str);
            return this;
        }
    }

    /* renamed from: org.signal.storageservice.protos.groups.GroupAttributeBlob$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new GroupAttributeBlob();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0004\u0001\u0000\u0001\u0004\u0004\u0000\u0000\u0000\u0001Ȼ\u0000\u0002=\u0000\u0003>\u0000\u0004Ȼ\u0000", new Object[]{"content_", "contentCase_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<GroupAttributeBlob> parser = PARSER;
                if (parser == null) {
                    synchronized (GroupAttributeBlob.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        GroupAttributeBlob groupAttributeBlob = new GroupAttributeBlob();
        DEFAULT_INSTANCE = groupAttributeBlob;
        GeneratedMessageLite.registerDefaultInstance(GroupAttributeBlob.class, groupAttributeBlob);
    }

    public static GroupAttributeBlob getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<GroupAttributeBlob> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
