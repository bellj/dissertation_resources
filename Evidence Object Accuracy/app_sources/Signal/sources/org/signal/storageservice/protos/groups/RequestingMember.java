package org.signal.storageservice.protos.groups;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class RequestingMember extends GeneratedMessageLite<RequestingMember, Builder> implements RequestingMemberOrBuilder {
    private static final RequestingMember DEFAULT_INSTANCE;
    private static volatile Parser<RequestingMember> PARSER;
    public static final int PRESENTATION_FIELD_NUMBER;
    public static final int PROFILEKEY_FIELD_NUMBER;
    public static final int TIMESTAMP_FIELD_NUMBER;
    public static final int USERID_FIELD_NUMBER;
    private ByteString presentation_;
    private ByteString profileKey_;
    private long timestamp_;
    private ByteString userId_;

    private RequestingMember() {
        ByteString byteString = ByteString.EMPTY;
        this.userId_ = byteString;
        this.profileKey_ = byteString;
        this.presentation_ = byteString;
    }

    public ByteString getUserId() {
        return this.userId_;
    }

    public void setUserId(ByteString byteString) {
        byteString.getClass();
        this.userId_ = byteString;
    }

    public void clearUserId() {
        this.userId_ = getDefaultInstance().getUserId();
    }

    public ByteString getProfileKey() {
        return this.profileKey_;
    }

    public void setProfileKey(ByteString byteString) {
        byteString.getClass();
        this.profileKey_ = byteString;
    }

    public void clearProfileKey() {
        this.profileKey_ = getDefaultInstance().getProfileKey();
    }

    public ByteString getPresentation() {
        return this.presentation_;
    }

    public void setPresentation(ByteString byteString) {
        byteString.getClass();
        this.presentation_ = byteString;
    }

    public void clearPresentation() {
        this.presentation_ = getDefaultInstance().getPresentation();
    }

    public long getTimestamp() {
        return this.timestamp_;
    }

    public void setTimestamp(long j) {
        this.timestamp_ = j;
    }

    public void clearTimestamp() {
        this.timestamp_ = 0;
    }

    public static RequestingMember parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (RequestingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static RequestingMember parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (RequestingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static RequestingMember parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (RequestingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static RequestingMember parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (RequestingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static RequestingMember parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (RequestingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static RequestingMember parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (RequestingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static RequestingMember parseFrom(InputStream inputStream) throws IOException {
        return (RequestingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static RequestingMember parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (RequestingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static RequestingMember parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (RequestingMember) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static RequestingMember parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (RequestingMember) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static RequestingMember parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (RequestingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static RequestingMember parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (RequestingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(RequestingMember requestingMember) {
        return DEFAULT_INSTANCE.createBuilder(requestingMember);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<RequestingMember, Builder> implements RequestingMemberOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(RequestingMember.DEFAULT_INSTANCE);
        }

        public Builder setPresentation(ByteString byteString) {
            copyOnWrite();
            ((RequestingMember) this.instance).setPresentation(byteString);
            return this;
        }
    }

    /* renamed from: org.signal.storageservice.protos.groups.RequestingMember$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new RequestingMember();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0000\u0000\u0001\n\u0002\n\u0003\n\u0004\u0003", new Object[]{"userId_", "profileKey_", "presentation_", "timestamp_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<RequestingMember> parser = PARSER;
                if (parser == null) {
                    synchronized (RequestingMember.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        RequestingMember requestingMember = new RequestingMember();
        DEFAULT_INSTANCE = requestingMember;
        GeneratedMessageLite.registerDefaultInstance(RequestingMember.class, requestingMember);
    }

    public static RequestingMember getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<RequestingMember> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
