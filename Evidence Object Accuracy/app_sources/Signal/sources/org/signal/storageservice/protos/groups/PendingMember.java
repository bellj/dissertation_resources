package org.signal.storageservice.protos.groups;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import org.signal.storageservice.protos.groups.Member;

/* loaded from: classes3.dex */
public final class PendingMember extends GeneratedMessageLite<PendingMember, Builder> implements PendingMemberOrBuilder {
    public static final int ADDEDBYUSERID_FIELD_NUMBER;
    private static final PendingMember DEFAULT_INSTANCE;
    public static final int MEMBER_FIELD_NUMBER;
    private static volatile Parser<PendingMember> PARSER;
    public static final int TIMESTAMP_FIELD_NUMBER;
    private ByteString addedByUserId_ = ByteString.EMPTY;
    private Member member_;
    private long timestamp_;

    private PendingMember() {
    }

    public boolean hasMember() {
        return this.member_ != null;
    }

    public Member getMember() {
        Member member = this.member_;
        return member == null ? Member.getDefaultInstance() : member;
    }

    public void setMember(Member member) {
        member.getClass();
        this.member_ = member;
    }

    public void mergeMember(Member member) {
        member.getClass();
        Member member2 = this.member_;
        if (member2 == null || member2 == Member.getDefaultInstance()) {
            this.member_ = member;
        } else {
            this.member_ = Member.newBuilder(this.member_).mergeFrom((Member.Builder) member).buildPartial();
        }
    }

    public void clearMember() {
        this.member_ = null;
    }

    public ByteString getAddedByUserId() {
        return this.addedByUserId_;
    }

    public void setAddedByUserId(ByteString byteString) {
        byteString.getClass();
        this.addedByUserId_ = byteString;
    }

    public void clearAddedByUserId() {
        this.addedByUserId_ = getDefaultInstance().getAddedByUserId();
    }

    public long getTimestamp() {
        return this.timestamp_;
    }

    public void setTimestamp(long j) {
        this.timestamp_ = j;
    }

    public void clearTimestamp() {
        this.timestamp_ = 0;
    }

    public static PendingMember parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (PendingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static PendingMember parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (PendingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static PendingMember parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (PendingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static PendingMember parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (PendingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static PendingMember parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (PendingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static PendingMember parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (PendingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static PendingMember parseFrom(InputStream inputStream) throws IOException {
        return (PendingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static PendingMember parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (PendingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static PendingMember parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (PendingMember) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static PendingMember parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (PendingMember) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static PendingMember parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (PendingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static PendingMember parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (PendingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(PendingMember pendingMember) {
        return DEFAULT_INSTANCE.createBuilder(pendingMember);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<PendingMember, Builder> implements PendingMemberOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(PendingMember.DEFAULT_INSTANCE);
        }

        public Builder setMember(Member member) {
            copyOnWrite();
            ((PendingMember) this.instance).setMember(member);
            return this;
        }

        public Builder setAddedByUserId(ByteString byteString) {
            copyOnWrite();
            ((PendingMember) this.instance).setAddedByUserId(byteString);
            return this;
        }
    }

    /* renamed from: org.signal.storageservice.protos.groups.PendingMember$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new PendingMember();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\t\u0002\n\u0003\u0003", new Object[]{"member_", "addedByUserId_", "timestamp_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<PendingMember> parser = PARSER;
                if (parser == null) {
                    synchronized (PendingMember.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        PendingMember pendingMember = new PendingMember();
        DEFAULT_INSTANCE = pendingMember;
        GeneratedMessageLite.registerDefaultInstance(PendingMember.class, pendingMember);
    }

    public static PendingMember getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<PendingMember> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
