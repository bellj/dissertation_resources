package org.signal.storageservice.protos.groups.local;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import org.signal.storageservice.protos.groups.Member;

/* loaded from: classes3.dex */
public final class DecryptedApproveMember extends GeneratedMessageLite<DecryptedApproveMember, Builder> implements DecryptedApproveMemberOrBuilder {
    private static final DecryptedApproveMember DEFAULT_INSTANCE;
    private static volatile Parser<DecryptedApproveMember> PARSER;
    public static final int ROLE_FIELD_NUMBER;
    public static final int UUID_FIELD_NUMBER;
    private int role_;
    private ByteString uuid_ = ByteString.EMPTY;

    private DecryptedApproveMember() {
    }

    public ByteString getUuid() {
        return this.uuid_;
    }

    public void setUuid(ByteString byteString) {
        byteString.getClass();
        this.uuid_ = byteString;
    }

    public void clearUuid() {
        this.uuid_ = getDefaultInstance().getUuid();
    }

    public int getRoleValue() {
        return this.role_;
    }

    public Member.Role getRole() {
        Member.Role forNumber = Member.Role.forNumber(this.role_);
        return forNumber == null ? Member.Role.UNRECOGNIZED : forNumber;
    }

    public void setRoleValue(int i) {
        this.role_ = i;
    }

    public void setRole(Member.Role role) {
        this.role_ = role.getNumber();
    }

    public void clearRole() {
        this.role_ = 0;
    }

    public static DecryptedApproveMember parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (DecryptedApproveMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static DecryptedApproveMember parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedApproveMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static DecryptedApproveMember parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (DecryptedApproveMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static DecryptedApproveMember parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedApproveMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static DecryptedApproveMember parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (DecryptedApproveMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static DecryptedApproveMember parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedApproveMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static DecryptedApproveMember parseFrom(InputStream inputStream) throws IOException {
        return (DecryptedApproveMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static DecryptedApproveMember parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedApproveMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static DecryptedApproveMember parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (DecryptedApproveMember) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static DecryptedApproveMember parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedApproveMember) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static DecryptedApproveMember parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (DecryptedApproveMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static DecryptedApproveMember parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedApproveMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(DecryptedApproveMember decryptedApproveMember) {
        return DEFAULT_INSTANCE.createBuilder(decryptedApproveMember);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<DecryptedApproveMember, Builder> implements DecryptedApproveMemberOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(DecryptedApproveMember.DEFAULT_INSTANCE);
        }

        public Builder setUuid(ByteString byteString) {
            copyOnWrite();
            ((DecryptedApproveMember) this.instance).setUuid(byteString);
            return this;
        }

        public Builder setRole(Member.Role role) {
            copyOnWrite();
            ((DecryptedApproveMember) this.instance).setRole(role);
            return this;
        }
    }

    /* renamed from: org.signal.storageservice.protos.groups.local.DecryptedApproveMember$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new DecryptedApproveMember();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\n\u0002\f", new Object[]{"uuid_", "role_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<DecryptedApproveMember> parser = PARSER;
                if (parser == null) {
                    synchronized (DecryptedApproveMember.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        DecryptedApproveMember decryptedApproveMember = new DecryptedApproveMember();
        DEFAULT_INSTANCE = decryptedApproveMember;
        GeneratedMessageLite.registerDefaultInstance(DecryptedApproveMember.class, decryptedApproveMember);
    }

    public static DecryptedApproveMember getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<DecryptedApproveMember> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
