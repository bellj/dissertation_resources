package org.signal.storageservice.protos.groups.local;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import org.signal.storageservice.protos.groups.Member;

/* loaded from: classes3.dex */
public final class DecryptedMember extends GeneratedMessageLite<DecryptedMember, Builder> implements DecryptedMemberOrBuilder {
    private static final DecryptedMember DEFAULT_INSTANCE;
    public static final int JOINEDATREVISION_FIELD_NUMBER;
    private static volatile Parser<DecryptedMember> PARSER;
    public static final int PNI_FIELD_NUMBER;
    public static final int PROFILEKEY_FIELD_NUMBER;
    public static final int ROLE_FIELD_NUMBER;
    public static final int UUID_FIELD_NUMBER;
    private int joinedAtRevision_;
    private ByteString pni_;
    private ByteString profileKey_;
    private int role_;
    private ByteString uuid_;

    private DecryptedMember() {
        ByteString byteString = ByteString.EMPTY;
        this.uuid_ = byteString;
        this.profileKey_ = byteString;
        this.pni_ = byteString;
    }

    public ByteString getUuid() {
        return this.uuid_;
    }

    public void setUuid(ByteString byteString) {
        byteString.getClass();
        this.uuid_ = byteString;
    }

    public void clearUuid() {
        this.uuid_ = getDefaultInstance().getUuid();
    }

    public int getRoleValue() {
        return this.role_;
    }

    public Member.Role getRole() {
        Member.Role forNumber = Member.Role.forNumber(this.role_);
        return forNumber == null ? Member.Role.UNRECOGNIZED : forNumber;
    }

    public void setRoleValue(int i) {
        this.role_ = i;
    }

    public void setRole(Member.Role role) {
        this.role_ = role.getNumber();
    }

    public void clearRole() {
        this.role_ = 0;
    }

    public ByteString getProfileKey() {
        return this.profileKey_;
    }

    public void setProfileKey(ByteString byteString) {
        byteString.getClass();
        this.profileKey_ = byteString;
    }

    public void clearProfileKey() {
        this.profileKey_ = getDefaultInstance().getProfileKey();
    }

    public int getJoinedAtRevision() {
        return this.joinedAtRevision_;
    }

    public void setJoinedAtRevision(int i) {
        this.joinedAtRevision_ = i;
    }

    public void clearJoinedAtRevision() {
        this.joinedAtRevision_ = 0;
    }

    public ByteString getPni() {
        return this.pni_;
    }

    public void setPni(ByteString byteString) {
        byteString.getClass();
        this.pni_ = byteString;
    }

    public void clearPni() {
        this.pni_ = getDefaultInstance().getPni();
    }

    public static DecryptedMember parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (DecryptedMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static DecryptedMember parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static DecryptedMember parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (DecryptedMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static DecryptedMember parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static DecryptedMember parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (DecryptedMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static DecryptedMember parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static DecryptedMember parseFrom(InputStream inputStream) throws IOException {
        return (DecryptedMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static DecryptedMember parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static DecryptedMember parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (DecryptedMember) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static DecryptedMember parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedMember) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static DecryptedMember parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (DecryptedMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static DecryptedMember parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(DecryptedMember decryptedMember) {
        return DEFAULT_INSTANCE.createBuilder(decryptedMember);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<DecryptedMember, Builder> implements DecryptedMemberOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(DecryptedMember.DEFAULT_INSTANCE);
        }

        public Builder setUuid(ByteString byteString) {
            copyOnWrite();
            ((DecryptedMember) this.instance).setUuid(byteString);
            return this;
        }

        public Builder setRole(Member.Role role) {
            copyOnWrite();
            ((DecryptedMember) this.instance).setRole(role);
            return this;
        }

        public Builder setProfileKey(ByteString byteString) {
            copyOnWrite();
            ((DecryptedMember) this.instance).setProfileKey(byteString);
            return this;
        }

        public Builder setJoinedAtRevision(int i) {
            copyOnWrite();
            ((DecryptedMember) this.instance).setJoinedAtRevision(i);
            return this;
        }

        public Builder setPni(ByteString byteString) {
            copyOnWrite();
            ((DecryptedMember) this.instance).setPni(byteString);
            return this;
        }
    }

    /* renamed from: org.signal.storageservice.protos.groups.local.DecryptedMember$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new DecryptedMember();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0005\u0000\u0000\u0001\u0006\u0005\u0000\u0000\u0000\u0001\n\u0002\f\u0003\n\u0005\u000b\u0006\n", new Object[]{"uuid_", "role_", "profileKey_", "joinedAtRevision_", "pni_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<DecryptedMember> parser = PARSER;
                if (parser == null) {
                    synchronized (DecryptedMember.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        DecryptedMember decryptedMember = new DecryptedMember();
        DEFAULT_INSTANCE = decryptedMember;
        GeneratedMessageLite.registerDefaultInstance(DecryptedMember.class, decryptedMember);
    }

    public static DecryptedMember getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<DecryptedMember> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
