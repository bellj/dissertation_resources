package org.signal.storageservice.protos.groups.local;

import com.google.protobuf.Internal;

/* loaded from: classes3.dex */
public enum EnabledState implements Internal.EnumLite {
    UNKNOWN(0),
    ENABLED(1),
    DISABLED(2),
    UNRECOGNIZED(-1);
    
    private static final Internal.EnumLiteMap<EnabledState> internalValueMap = new Internal.EnumLiteMap<EnabledState>() { // from class: org.signal.storageservice.protos.groups.local.EnabledState.1
        @Override // com.google.protobuf.Internal.EnumLiteMap
        public EnabledState findValueByNumber(int i) {
            return EnabledState.forNumber(i);
        }
    };
    private final int value;

    @Override // com.google.protobuf.Internal.EnumLite
    public final int getNumber() {
        if (this != UNRECOGNIZED) {
            return this.value;
        }
        throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
    }

    public static EnabledState forNumber(int i) {
        if (i == 0) {
            return UNKNOWN;
        }
        if (i == 1) {
            return ENABLED;
        }
        if (i != 2) {
            return null;
        }
        return DISABLED;
    }

    EnabledState(int i) {
        this.value = i;
    }
}
