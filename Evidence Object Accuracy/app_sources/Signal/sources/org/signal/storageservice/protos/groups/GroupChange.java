package org.signal.storageservice.protos.groups;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;
import org.signal.storageservice.protos.groups.AccessControl;
import org.signal.storageservice.protos.groups.BannedMember;
import org.signal.storageservice.protos.groups.Member;
import org.signal.storageservice.protos.groups.PendingMember;
import org.signal.storageservice.protos.groups.RequestingMember;

/* loaded from: classes3.dex */
public final class GroupChange extends GeneratedMessageLite<GroupChange, Builder> implements MessageLiteOrBuilder {
    public static final int ACTIONS_FIELD_NUMBER;
    public static final int CHANGEEPOCH_FIELD_NUMBER;
    private static final GroupChange DEFAULT_INSTANCE;
    private static volatile Parser<GroupChange> PARSER;
    public static final int SERVERSIGNATURE_FIELD_NUMBER;
    private ByteString actions_;
    private int changeEpoch_;
    private ByteString serverSignature_;

    private GroupChange() {
        ByteString byteString = ByteString.EMPTY;
        this.actions_ = byteString;
        this.serverSignature_ = byteString;
    }

    /* loaded from: classes3.dex */
    public static final class Actions extends GeneratedMessageLite<Actions, Builder> implements MessageLiteOrBuilder {
        public static final int ADDBANNEDMEMBERS_FIELD_NUMBER;
        public static final int ADDMEMBERS_FIELD_NUMBER;
        public static final int ADDPENDINGMEMBERS_FIELD_NUMBER;
        public static final int ADDREQUESTINGMEMBERS_FIELD_NUMBER;
        private static final Actions DEFAULT_INSTANCE;
        public static final int DELETEBANNEDMEMBERS_FIELD_NUMBER;
        public static final int DELETEMEMBERS_FIELD_NUMBER;
        public static final int DELETEPENDINGMEMBERS_FIELD_NUMBER;
        public static final int DELETEREQUESTINGMEMBERS_FIELD_NUMBER;
        public static final int MODIFYADDFROMINVITELINKACCESS_FIELD_NUMBER;
        public static final int MODIFYANNOUNCEMENTSONLY_FIELD_NUMBER;
        public static final int MODIFYATTRIBUTESACCESS_FIELD_NUMBER;
        public static final int MODIFYAVATAR_FIELD_NUMBER;
        public static final int MODIFYDESCRIPTION_FIELD_NUMBER;
        public static final int MODIFYDISAPPEARINGMESSAGESTIMER_FIELD_NUMBER;
        public static final int MODIFYINVITELINKPASSWORD_FIELD_NUMBER;
        public static final int MODIFYMEMBERACCESS_FIELD_NUMBER;
        public static final int MODIFYMEMBERPROFILEKEYS_FIELD_NUMBER;
        public static final int MODIFYMEMBERROLES_FIELD_NUMBER;
        public static final int MODIFYTITLE_FIELD_NUMBER;
        private static volatile Parser<Actions> PARSER;
        public static final int PROMOTEPENDINGMEMBERS_FIELD_NUMBER;
        public static final int PROMOTEPENDINGPNIACIMEMBERS_FIELD_NUMBER;
        public static final int PROMOTEREQUESTINGMEMBERS_FIELD_NUMBER;
        public static final int REVISION_FIELD_NUMBER;
        public static final int SOURCEUUID_FIELD_NUMBER;
        private Internal.ProtobufList<AddBannedMemberAction> addBannedMembers_ = GeneratedMessageLite.emptyProtobufList();
        private Internal.ProtobufList<AddMemberAction> addMembers_ = GeneratedMessageLite.emptyProtobufList();
        private Internal.ProtobufList<AddPendingMemberAction> addPendingMembers_ = GeneratedMessageLite.emptyProtobufList();
        private Internal.ProtobufList<AddRequestingMemberAction> addRequestingMembers_ = GeneratedMessageLite.emptyProtobufList();
        private Internal.ProtobufList<DeleteBannedMemberAction> deleteBannedMembers_ = GeneratedMessageLite.emptyProtobufList();
        private Internal.ProtobufList<DeleteMemberAction> deleteMembers_ = GeneratedMessageLite.emptyProtobufList();
        private Internal.ProtobufList<DeletePendingMemberAction> deletePendingMembers_ = GeneratedMessageLite.emptyProtobufList();
        private Internal.ProtobufList<DeleteRequestingMemberAction> deleteRequestingMembers_ = GeneratedMessageLite.emptyProtobufList();
        private ModifyAddFromInviteLinkAccessControlAction modifyAddFromInviteLinkAccess_;
        private ModifyAnnouncementsOnlyAction modifyAnnouncementsOnly_;
        private ModifyAttributesAccessControlAction modifyAttributesAccess_;
        private ModifyAvatarAction modifyAvatar_;
        private ModifyDescriptionAction modifyDescription_;
        private ModifyDisappearingMessagesTimerAction modifyDisappearingMessagesTimer_;
        private ModifyInviteLinkPasswordAction modifyInviteLinkPassword_;
        private ModifyMembersAccessControlAction modifyMemberAccess_;
        private Internal.ProtobufList<ModifyMemberProfileKeyAction> modifyMemberProfileKeys_ = GeneratedMessageLite.emptyProtobufList();
        private Internal.ProtobufList<ModifyMemberRoleAction> modifyMemberRoles_ = GeneratedMessageLite.emptyProtobufList();
        private ModifyTitleAction modifyTitle_;
        private Internal.ProtobufList<PromotePendingMemberAction> promotePendingMembers_ = GeneratedMessageLite.emptyProtobufList();
        private Internal.ProtobufList<PromotePendingPniAciMemberProfileKeyAction> promotePendingPniAciMembers_ = GeneratedMessageLite.emptyProtobufList();
        private Internal.ProtobufList<PromoteRequestingMemberAction> promoteRequestingMembers_ = GeneratedMessageLite.emptyProtobufList();
        private int revision_;
        private ByteString sourceUuid_ = ByteString.EMPTY;

        /* loaded from: classes3.dex */
        public interface AddBannedMemberActionOrBuilder extends MessageLiteOrBuilder {
        }

        /* loaded from: classes3.dex */
        public interface AddMemberActionOrBuilder extends MessageLiteOrBuilder {
        }

        /* loaded from: classes3.dex */
        public interface AddPendingMemberActionOrBuilder extends MessageLiteOrBuilder {
        }

        /* loaded from: classes3.dex */
        public interface AddRequestingMemberActionOrBuilder extends MessageLiteOrBuilder {
        }

        /* loaded from: classes3.dex */
        public interface DeleteBannedMemberActionOrBuilder extends MessageLiteOrBuilder {
        }

        /* loaded from: classes3.dex */
        public interface DeleteMemberActionOrBuilder extends MessageLiteOrBuilder {
        }

        /* loaded from: classes3.dex */
        public interface DeletePendingMemberActionOrBuilder extends MessageLiteOrBuilder {
        }

        /* loaded from: classes3.dex */
        public interface DeleteRequestingMemberActionOrBuilder extends MessageLiteOrBuilder {
        }

        /* loaded from: classes3.dex */
        public interface ModifyMemberProfileKeyActionOrBuilder extends MessageLiteOrBuilder {
        }

        /* loaded from: classes3.dex */
        public interface ModifyMemberRoleActionOrBuilder extends MessageLiteOrBuilder {
        }

        /* loaded from: classes3.dex */
        public interface PromotePendingMemberActionOrBuilder extends MessageLiteOrBuilder {
        }

        /* loaded from: classes3.dex */
        public interface PromotePendingPniAciMemberProfileKeyActionOrBuilder extends MessageLiteOrBuilder {
        }

        /* loaded from: classes3.dex */
        public interface PromoteRequestingMemberActionOrBuilder extends MessageLiteOrBuilder {
        }

        private Actions() {
        }

        /* loaded from: classes3.dex */
        public static final class AddMemberAction extends GeneratedMessageLite<AddMemberAction, Builder> implements AddMemberActionOrBuilder {
            public static final int ADDED_FIELD_NUMBER;
            private static final AddMemberAction DEFAULT_INSTANCE;
            public static final int JOINFROMINVITELINK_FIELD_NUMBER;
            private static volatile Parser<AddMemberAction> PARSER;
            private Member added_;
            private boolean joinFromInviteLink_;

            private AddMemberAction() {
            }

            public boolean hasAdded() {
                return this.added_ != null;
            }

            public Member getAdded() {
                Member member = this.added_;
                return member == null ? Member.getDefaultInstance() : member;
            }

            public void setAdded(Member member) {
                member.getClass();
                this.added_ = member;
            }

            public void mergeAdded(Member member) {
                member.getClass();
                Member member2 = this.added_;
                if (member2 == null || member2 == Member.getDefaultInstance()) {
                    this.added_ = member;
                } else {
                    this.added_ = Member.newBuilder(this.added_).mergeFrom((Member.Builder) member).buildPartial();
                }
            }

            public void clearAdded() {
                this.added_ = null;
            }

            public boolean getJoinFromInviteLink() {
                return this.joinFromInviteLink_;
            }

            public void setJoinFromInviteLink(boolean z) {
                this.joinFromInviteLink_ = z;
            }

            public void clearJoinFromInviteLink() {
                this.joinFromInviteLink_ = false;
            }

            public static AddMemberAction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (AddMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static AddMemberAction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (AddMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static AddMemberAction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (AddMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static AddMemberAction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (AddMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static AddMemberAction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (AddMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static AddMemberAction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (AddMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static AddMemberAction parseFrom(InputStream inputStream) throws IOException {
                return (AddMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static AddMemberAction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (AddMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static AddMemberAction parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (AddMemberAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static AddMemberAction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (AddMemberAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static AddMemberAction parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (AddMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static AddMemberAction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (AddMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(AddMemberAction addMemberAction) {
                return DEFAULT_INSTANCE.createBuilder(addMemberAction);
            }

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<AddMemberAction, Builder> implements AddMemberActionOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(AddMemberAction.DEFAULT_INSTANCE);
                }

                public Builder setAdded(Member.Builder builder) {
                    copyOnWrite();
                    ((AddMemberAction) this.instance).setAdded(builder.build());
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new AddMemberAction();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\t\u0002\u0007", new Object[]{"added_", "joinFromInviteLink_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<AddMemberAction> parser = PARSER;
                        if (parser == null) {
                            synchronized (AddMemberAction.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                AddMemberAction addMemberAction = new AddMemberAction();
                DEFAULT_INSTANCE = addMemberAction;
                GeneratedMessageLite.registerDefaultInstance(AddMemberAction.class, addMemberAction);
            }

            public static AddMemberAction getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<AddMemberAction> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes3.dex */
        public static final class DeleteMemberAction extends GeneratedMessageLite<DeleteMemberAction, Builder> implements DeleteMemberActionOrBuilder {
            private static final DeleteMemberAction DEFAULT_INSTANCE;
            public static final int DELETEDUSERID_FIELD_NUMBER;
            private static volatile Parser<DeleteMemberAction> PARSER;
            private ByteString deletedUserId_ = ByteString.EMPTY;

            private DeleteMemberAction() {
            }

            public ByteString getDeletedUserId() {
                return this.deletedUserId_;
            }

            public void setDeletedUserId(ByteString byteString) {
                byteString.getClass();
                this.deletedUserId_ = byteString;
            }

            public void clearDeletedUserId() {
                this.deletedUserId_ = getDefaultInstance().getDeletedUserId();
            }

            public static DeleteMemberAction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (DeleteMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static DeleteMemberAction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (DeleteMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static DeleteMemberAction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (DeleteMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static DeleteMemberAction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (DeleteMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static DeleteMemberAction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (DeleteMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static DeleteMemberAction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (DeleteMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static DeleteMemberAction parseFrom(InputStream inputStream) throws IOException {
                return (DeleteMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static DeleteMemberAction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (DeleteMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static DeleteMemberAction parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (DeleteMemberAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static DeleteMemberAction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (DeleteMemberAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static DeleteMemberAction parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (DeleteMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static DeleteMemberAction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (DeleteMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(DeleteMemberAction deleteMemberAction) {
                return DEFAULT_INSTANCE.createBuilder(deleteMemberAction);
            }

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<DeleteMemberAction, Builder> implements DeleteMemberActionOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(DeleteMemberAction.DEFAULT_INSTANCE);
                }

                public Builder setDeletedUserId(ByteString byteString) {
                    copyOnWrite();
                    ((DeleteMemberAction) this.instance).setDeletedUserId(byteString);
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new DeleteMemberAction();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\n", new Object[]{"deletedUserId_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<DeleteMemberAction> parser = PARSER;
                        if (parser == null) {
                            synchronized (DeleteMemberAction.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                DeleteMemberAction deleteMemberAction = new DeleteMemberAction();
                DEFAULT_INSTANCE = deleteMemberAction;
                GeneratedMessageLite.registerDefaultInstance(DeleteMemberAction.class, deleteMemberAction);
            }

            public static DeleteMemberAction getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<DeleteMemberAction> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes3.dex */
        public static final class ModifyMemberRoleAction extends GeneratedMessageLite<ModifyMemberRoleAction, Builder> implements ModifyMemberRoleActionOrBuilder {
            private static final ModifyMemberRoleAction DEFAULT_INSTANCE;
            private static volatile Parser<ModifyMemberRoleAction> PARSER;
            public static final int ROLE_FIELD_NUMBER;
            public static final int USERID_FIELD_NUMBER;
            private int role_;
            private ByteString userId_ = ByteString.EMPTY;

            private ModifyMemberRoleAction() {
            }

            public ByteString getUserId() {
                return this.userId_;
            }

            public void setUserId(ByteString byteString) {
                byteString.getClass();
                this.userId_ = byteString;
            }

            public void clearUserId() {
                this.userId_ = getDefaultInstance().getUserId();
            }

            public int getRoleValue() {
                return this.role_;
            }

            public Member.Role getRole() {
                Member.Role forNumber = Member.Role.forNumber(this.role_);
                return forNumber == null ? Member.Role.UNRECOGNIZED : forNumber;
            }

            public void setRoleValue(int i) {
                this.role_ = i;
            }

            public void setRole(Member.Role role) {
                this.role_ = role.getNumber();
            }

            public void clearRole() {
                this.role_ = 0;
            }

            public static ModifyMemberRoleAction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (ModifyMemberRoleAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static ModifyMemberRoleAction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyMemberRoleAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static ModifyMemberRoleAction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (ModifyMemberRoleAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static ModifyMemberRoleAction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyMemberRoleAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static ModifyMemberRoleAction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (ModifyMemberRoleAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static ModifyMemberRoleAction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyMemberRoleAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static ModifyMemberRoleAction parseFrom(InputStream inputStream) throws IOException {
                return (ModifyMemberRoleAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static ModifyMemberRoleAction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyMemberRoleAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static ModifyMemberRoleAction parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (ModifyMemberRoleAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static ModifyMemberRoleAction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyMemberRoleAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static ModifyMemberRoleAction parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (ModifyMemberRoleAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static ModifyMemberRoleAction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyMemberRoleAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(ModifyMemberRoleAction modifyMemberRoleAction) {
                return DEFAULT_INSTANCE.createBuilder(modifyMemberRoleAction);
            }

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<ModifyMemberRoleAction, Builder> implements ModifyMemberRoleActionOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(ModifyMemberRoleAction.DEFAULT_INSTANCE);
                }

                public Builder setUserId(ByteString byteString) {
                    copyOnWrite();
                    ((ModifyMemberRoleAction) this.instance).setUserId(byteString);
                    return this;
                }

                public Builder setRole(Member.Role role) {
                    copyOnWrite();
                    ((ModifyMemberRoleAction) this.instance).setRole(role);
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new ModifyMemberRoleAction();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\n\u0002\f", new Object[]{"userId_", "role_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<ModifyMemberRoleAction> parser = PARSER;
                        if (parser == null) {
                            synchronized (ModifyMemberRoleAction.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                ModifyMemberRoleAction modifyMemberRoleAction = new ModifyMemberRoleAction();
                DEFAULT_INSTANCE = modifyMemberRoleAction;
                GeneratedMessageLite.registerDefaultInstance(ModifyMemberRoleAction.class, modifyMemberRoleAction);
            }

            public static ModifyMemberRoleAction getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<ModifyMemberRoleAction> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes3.dex */
        public static final class ModifyMemberProfileKeyAction extends GeneratedMessageLite<ModifyMemberProfileKeyAction, Builder> implements ModifyMemberProfileKeyActionOrBuilder {
            private static final ModifyMemberProfileKeyAction DEFAULT_INSTANCE;
            private static volatile Parser<ModifyMemberProfileKeyAction> PARSER;
            public static final int PRESENTATION_FIELD_NUMBER;
            public static final int PROFILE_KEY_FIELD_NUMBER;
            public static final int USER_ID_FIELD_NUMBER;
            private ByteString presentation_;
            private ByteString profileKey_;
            private ByteString userId_;

            private ModifyMemberProfileKeyAction() {
                ByteString byteString = ByteString.EMPTY;
                this.presentation_ = byteString;
                this.userId_ = byteString;
                this.profileKey_ = byteString;
            }

            public ByteString getPresentation() {
                return this.presentation_;
            }

            public void setPresentation(ByteString byteString) {
                byteString.getClass();
                this.presentation_ = byteString;
            }

            public void clearPresentation() {
                this.presentation_ = getDefaultInstance().getPresentation();
            }

            public ByteString getUserId() {
                return this.userId_;
            }

            public void setUserId(ByteString byteString) {
                byteString.getClass();
                this.userId_ = byteString;
            }

            public void clearUserId() {
                this.userId_ = getDefaultInstance().getUserId();
            }

            public ByteString getProfileKey() {
                return this.profileKey_;
            }

            public void setProfileKey(ByteString byteString) {
                byteString.getClass();
                this.profileKey_ = byteString;
            }

            public void clearProfileKey() {
                this.profileKey_ = getDefaultInstance().getProfileKey();
            }

            public static ModifyMemberProfileKeyAction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (ModifyMemberProfileKeyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static ModifyMemberProfileKeyAction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyMemberProfileKeyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static ModifyMemberProfileKeyAction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (ModifyMemberProfileKeyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static ModifyMemberProfileKeyAction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyMemberProfileKeyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static ModifyMemberProfileKeyAction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (ModifyMemberProfileKeyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static ModifyMemberProfileKeyAction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyMemberProfileKeyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static ModifyMemberProfileKeyAction parseFrom(InputStream inputStream) throws IOException {
                return (ModifyMemberProfileKeyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static ModifyMemberProfileKeyAction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyMemberProfileKeyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static ModifyMemberProfileKeyAction parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (ModifyMemberProfileKeyAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static ModifyMemberProfileKeyAction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyMemberProfileKeyAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static ModifyMemberProfileKeyAction parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (ModifyMemberProfileKeyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static ModifyMemberProfileKeyAction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyMemberProfileKeyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(ModifyMemberProfileKeyAction modifyMemberProfileKeyAction) {
                return DEFAULT_INSTANCE.createBuilder(modifyMemberProfileKeyAction);
            }

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<ModifyMemberProfileKeyAction, Builder> implements ModifyMemberProfileKeyActionOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(ModifyMemberProfileKeyAction.DEFAULT_INSTANCE);
                }

                public Builder setPresentation(ByteString byteString) {
                    copyOnWrite();
                    ((ModifyMemberProfileKeyAction) this.instance).setPresentation(byteString);
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new ModifyMemberProfileKeyAction();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\n\u0002\n\u0003\n", new Object[]{"presentation_", "userId_", "profileKey_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<ModifyMemberProfileKeyAction> parser = PARSER;
                        if (parser == null) {
                            synchronized (ModifyMemberProfileKeyAction.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                ModifyMemberProfileKeyAction modifyMemberProfileKeyAction = new ModifyMemberProfileKeyAction();
                DEFAULT_INSTANCE = modifyMemberProfileKeyAction;
                GeneratedMessageLite.registerDefaultInstance(ModifyMemberProfileKeyAction.class, modifyMemberProfileKeyAction);
            }

            public static ModifyMemberProfileKeyAction getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<ModifyMemberProfileKeyAction> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes3.dex */
        public static final class AddPendingMemberAction extends GeneratedMessageLite<AddPendingMemberAction, Builder> implements AddPendingMemberActionOrBuilder {
            public static final int ADDED_FIELD_NUMBER;
            private static final AddPendingMemberAction DEFAULT_INSTANCE;
            private static volatile Parser<AddPendingMemberAction> PARSER;
            private PendingMember added_;

            private AddPendingMemberAction() {
            }

            public boolean hasAdded() {
                return this.added_ != null;
            }

            public PendingMember getAdded() {
                PendingMember pendingMember = this.added_;
                return pendingMember == null ? PendingMember.getDefaultInstance() : pendingMember;
            }

            public void setAdded(PendingMember pendingMember) {
                pendingMember.getClass();
                this.added_ = pendingMember;
            }

            public void mergeAdded(PendingMember pendingMember) {
                pendingMember.getClass();
                PendingMember pendingMember2 = this.added_;
                if (pendingMember2 == null || pendingMember2 == PendingMember.getDefaultInstance()) {
                    this.added_ = pendingMember;
                } else {
                    this.added_ = PendingMember.newBuilder(this.added_).mergeFrom((PendingMember.Builder) pendingMember).buildPartial();
                }
            }

            public void clearAdded() {
                this.added_ = null;
            }

            public static AddPendingMemberAction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (AddPendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static AddPendingMemberAction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (AddPendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static AddPendingMemberAction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (AddPendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static AddPendingMemberAction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (AddPendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static AddPendingMemberAction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (AddPendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static AddPendingMemberAction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (AddPendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static AddPendingMemberAction parseFrom(InputStream inputStream) throws IOException {
                return (AddPendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static AddPendingMemberAction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (AddPendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static AddPendingMemberAction parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (AddPendingMemberAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static AddPendingMemberAction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (AddPendingMemberAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static AddPendingMemberAction parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (AddPendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static AddPendingMemberAction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (AddPendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(AddPendingMemberAction addPendingMemberAction) {
                return DEFAULT_INSTANCE.createBuilder(addPendingMemberAction);
            }

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<AddPendingMemberAction, Builder> implements AddPendingMemberActionOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(AddPendingMemberAction.DEFAULT_INSTANCE);
                }

                public Builder setAdded(PendingMember.Builder builder) {
                    copyOnWrite();
                    ((AddPendingMemberAction) this.instance).setAdded(builder.build());
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new AddPendingMemberAction();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\t", new Object[]{"added_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<AddPendingMemberAction> parser = PARSER;
                        if (parser == null) {
                            synchronized (AddPendingMemberAction.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                AddPendingMemberAction addPendingMemberAction = new AddPendingMemberAction();
                DEFAULT_INSTANCE = addPendingMemberAction;
                GeneratedMessageLite.registerDefaultInstance(AddPendingMemberAction.class, addPendingMemberAction);
            }

            public static AddPendingMemberAction getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<AddPendingMemberAction> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes3.dex */
        public static final class DeletePendingMemberAction extends GeneratedMessageLite<DeletePendingMemberAction, Builder> implements DeletePendingMemberActionOrBuilder {
            private static final DeletePendingMemberAction DEFAULT_INSTANCE;
            public static final int DELETEDUSERID_FIELD_NUMBER;
            private static volatile Parser<DeletePendingMemberAction> PARSER;
            private ByteString deletedUserId_ = ByteString.EMPTY;

            private DeletePendingMemberAction() {
            }

            public ByteString getDeletedUserId() {
                return this.deletedUserId_;
            }

            public void setDeletedUserId(ByteString byteString) {
                byteString.getClass();
                this.deletedUserId_ = byteString;
            }

            public void clearDeletedUserId() {
                this.deletedUserId_ = getDefaultInstance().getDeletedUserId();
            }

            public static DeletePendingMemberAction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (DeletePendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static DeletePendingMemberAction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (DeletePendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static DeletePendingMemberAction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (DeletePendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static DeletePendingMemberAction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (DeletePendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static DeletePendingMemberAction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (DeletePendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static DeletePendingMemberAction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (DeletePendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static DeletePendingMemberAction parseFrom(InputStream inputStream) throws IOException {
                return (DeletePendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static DeletePendingMemberAction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (DeletePendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static DeletePendingMemberAction parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (DeletePendingMemberAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static DeletePendingMemberAction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (DeletePendingMemberAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static DeletePendingMemberAction parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (DeletePendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static DeletePendingMemberAction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (DeletePendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(DeletePendingMemberAction deletePendingMemberAction) {
                return DEFAULT_INSTANCE.createBuilder(deletePendingMemberAction);
            }

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<DeletePendingMemberAction, Builder> implements DeletePendingMemberActionOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(DeletePendingMemberAction.DEFAULT_INSTANCE);
                }

                public Builder setDeletedUserId(ByteString byteString) {
                    copyOnWrite();
                    ((DeletePendingMemberAction) this.instance).setDeletedUserId(byteString);
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new DeletePendingMemberAction();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\n", new Object[]{"deletedUserId_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<DeletePendingMemberAction> parser = PARSER;
                        if (parser == null) {
                            synchronized (DeletePendingMemberAction.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                DeletePendingMemberAction deletePendingMemberAction = new DeletePendingMemberAction();
                DEFAULT_INSTANCE = deletePendingMemberAction;
                GeneratedMessageLite.registerDefaultInstance(DeletePendingMemberAction.class, deletePendingMemberAction);
            }

            public static DeletePendingMemberAction getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<DeletePendingMemberAction> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes3.dex */
        public static final class PromotePendingMemberAction extends GeneratedMessageLite<PromotePendingMemberAction, Builder> implements PromotePendingMemberActionOrBuilder {
            private static final PromotePendingMemberAction DEFAULT_INSTANCE;
            private static volatile Parser<PromotePendingMemberAction> PARSER;
            public static final int PRESENTATION_FIELD_NUMBER;
            public static final int PROFILE_KEY_FIELD_NUMBER;
            public static final int USER_ID_FIELD_NUMBER;
            private ByteString presentation_;
            private ByteString profileKey_;
            private ByteString userId_;

            private PromotePendingMemberAction() {
                ByteString byteString = ByteString.EMPTY;
                this.presentation_ = byteString;
                this.userId_ = byteString;
                this.profileKey_ = byteString;
            }

            public ByteString getPresentation() {
                return this.presentation_;
            }

            public void setPresentation(ByteString byteString) {
                byteString.getClass();
                this.presentation_ = byteString;
            }

            public void clearPresentation() {
                this.presentation_ = getDefaultInstance().getPresentation();
            }

            public ByteString getUserId() {
                return this.userId_;
            }

            public void setUserId(ByteString byteString) {
                byteString.getClass();
                this.userId_ = byteString;
            }

            public void clearUserId() {
                this.userId_ = getDefaultInstance().getUserId();
            }

            public ByteString getProfileKey() {
                return this.profileKey_;
            }

            public void setProfileKey(ByteString byteString) {
                byteString.getClass();
                this.profileKey_ = byteString;
            }

            public void clearProfileKey() {
                this.profileKey_ = getDefaultInstance().getProfileKey();
            }

            public static PromotePendingMemberAction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (PromotePendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static PromotePendingMemberAction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (PromotePendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static PromotePendingMemberAction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (PromotePendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static PromotePendingMemberAction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (PromotePendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static PromotePendingMemberAction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (PromotePendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static PromotePendingMemberAction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (PromotePendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static PromotePendingMemberAction parseFrom(InputStream inputStream) throws IOException {
                return (PromotePendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static PromotePendingMemberAction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (PromotePendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static PromotePendingMemberAction parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (PromotePendingMemberAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static PromotePendingMemberAction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (PromotePendingMemberAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static PromotePendingMemberAction parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (PromotePendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static PromotePendingMemberAction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (PromotePendingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(PromotePendingMemberAction promotePendingMemberAction) {
                return DEFAULT_INSTANCE.createBuilder(promotePendingMemberAction);
            }

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<PromotePendingMemberAction, Builder> implements PromotePendingMemberActionOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(PromotePendingMemberAction.DEFAULT_INSTANCE);
                }

                public Builder setPresentation(ByteString byteString) {
                    copyOnWrite();
                    ((PromotePendingMemberAction) this.instance).setPresentation(byteString);
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new PromotePendingMemberAction();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\n\u0002\n\u0003\n", new Object[]{"presentation_", "userId_", "profileKey_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<PromotePendingMemberAction> parser = PARSER;
                        if (parser == null) {
                            synchronized (PromotePendingMemberAction.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                PromotePendingMemberAction promotePendingMemberAction = new PromotePendingMemberAction();
                DEFAULT_INSTANCE = promotePendingMemberAction;
                GeneratedMessageLite.registerDefaultInstance(PromotePendingMemberAction.class, promotePendingMemberAction);
            }

            public static PromotePendingMemberAction getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<PromotePendingMemberAction> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes3.dex */
        public static final class PromotePendingPniAciMemberProfileKeyAction extends GeneratedMessageLite<PromotePendingPniAciMemberProfileKeyAction, Builder> implements PromotePendingPniAciMemberProfileKeyActionOrBuilder {
            private static final PromotePendingPniAciMemberProfileKeyAction DEFAULT_INSTANCE;
            private static volatile Parser<PromotePendingPniAciMemberProfileKeyAction> PARSER;
            public static final int PNI_FIELD_NUMBER;
            public static final int PRESENTATION_FIELD_NUMBER;
            public static final int PROFILEKEY_FIELD_NUMBER;
            public static final int USERID_FIELD_NUMBER;
            private ByteString pni_;
            private ByteString presentation_;
            private ByteString profileKey_;
            private ByteString userId_;

            private PromotePendingPniAciMemberProfileKeyAction() {
                ByteString byteString = ByteString.EMPTY;
                this.presentation_ = byteString;
                this.userId_ = byteString;
                this.pni_ = byteString;
                this.profileKey_ = byteString;
            }

            public ByteString getPresentation() {
                return this.presentation_;
            }

            public void setPresentation(ByteString byteString) {
                byteString.getClass();
                this.presentation_ = byteString;
            }

            public void clearPresentation() {
                this.presentation_ = getDefaultInstance().getPresentation();
            }

            public ByteString getUserId() {
                return this.userId_;
            }

            public void setUserId(ByteString byteString) {
                byteString.getClass();
                this.userId_ = byteString;
            }

            public void clearUserId() {
                this.userId_ = getDefaultInstance().getUserId();
            }

            public ByteString getPni() {
                return this.pni_;
            }

            public void setPni(ByteString byteString) {
                byteString.getClass();
                this.pni_ = byteString;
            }

            public void clearPni() {
                this.pni_ = getDefaultInstance().getPni();
            }

            public ByteString getProfileKey() {
                return this.profileKey_;
            }

            public void setProfileKey(ByteString byteString) {
                byteString.getClass();
                this.profileKey_ = byteString;
            }

            public void clearProfileKey() {
                this.profileKey_ = getDefaultInstance().getProfileKey();
            }

            public static PromotePendingPniAciMemberProfileKeyAction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (PromotePendingPniAciMemberProfileKeyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static PromotePendingPniAciMemberProfileKeyAction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (PromotePendingPniAciMemberProfileKeyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static PromotePendingPniAciMemberProfileKeyAction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (PromotePendingPniAciMemberProfileKeyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static PromotePendingPniAciMemberProfileKeyAction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (PromotePendingPniAciMemberProfileKeyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static PromotePendingPniAciMemberProfileKeyAction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (PromotePendingPniAciMemberProfileKeyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static PromotePendingPniAciMemberProfileKeyAction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (PromotePendingPniAciMemberProfileKeyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static PromotePendingPniAciMemberProfileKeyAction parseFrom(InputStream inputStream) throws IOException {
                return (PromotePendingPniAciMemberProfileKeyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static PromotePendingPniAciMemberProfileKeyAction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (PromotePendingPniAciMemberProfileKeyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static PromotePendingPniAciMemberProfileKeyAction parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (PromotePendingPniAciMemberProfileKeyAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static PromotePendingPniAciMemberProfileKeyAction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (PromotePendingPniAciMemberProfileKeyAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static PromotePendingPniAciMemberProfileKeyAction parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (PromotePendingPniAciMemberProfileKeyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static PromotePendingPniAciMemberProfileKeyAction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (PromotePendingPniAciMemberProfileKeyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(PromotePendingPniAciMemberProfileKeyAction promotePendingPniAciMemberProfileKeyAction) {
                return DEFAULT_INSTANCE.createBuilder(promotePendingPniAciMemberProfileKeyAction);
            }

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<PromotePendingPniAciMemberProfileKeyAction, Builder> implements PromotePendingPniAciMemberProfileKeyActionOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(PromotePendingPniAciMemberProfileKeyAction.DEFAULT_INSTANCE);
                }

                public Builder setPresentation(ByteString byteString) {
                    copyOnWrite();
                    ((PromotePendingPniAciMemberProfileKeyAction) this.instance).setPresentation(byteString);
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new PromotePendingPniAciMemberProfileKeyAction();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0000\u0000\u0001\n\u0002\n\u0003\n\u0004\n", new Object[]{"presentation_", "userId_", "pni_", "profileKey_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<PromotePendingPniAciMemberProfileKeyAction> parser = PARSER;
                        if (parser == null) {
                            synchronized (PromotePendingPniAciMemberProfileKeyAction.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                PromotePendingPniAciMemberProfileKeyAction promotePendingPniAciMemberProfileKeyAction = new PromotePendingPniAciMemberProfileKeyAction();
                DEFAULT_INSTANCE = promotePendingPniAciMemberProfileKeyAction;
                GeneratedMessageLite.registerDefaultInstance(PromotePendingPniAciMemberProfileKeyAction.class, promotePendingPniAciMemberProfileKeyAction);
            }

            public static PromotePendingPniAciMemberProfileKeyAction getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<PromotePendingPniAciMemberProfileKeyAction> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes3.dex */
        public static final class AddRequestingMemberAction extends GeneratedMessageLite<AddRequestingMemberAction, Builder> implements AddRequestingMemberActionOrBuilder {
            public static final int ADDED_FIELD_NUMBER;
            private static final AddRequestingMemberAction DEFAULT_INSTANCE;
            private static volatile Parser<AddRequestingMemberAction> PARSER;
            private RequestingMember added_;

            private AddRequestingMemberAction() {
            }

            public boolean hasAdded() {
                return this.added_ != null;
            }

            public RequestingMember getAdded() {
                RequestingMember requestingMember = this.added_;
                return requestingMember == null ? RequestingMember.getDefaultInstance() : requestingMember;
            }

            public void setAdded(RequestingMember requestingMember) {
                requestingMember.getClass();
                this.added_ = requestingMember;
            }

            public void mergeAdded(RequestingMember requestingMember) {
                requestingMember.getClass();
                RequestingMember requestingMember2 = this.added_;
                if (requestingMember2 == null || requestingMember2 == RequestingMember.getDefaultInstance()) {
                    this.added_ = requestingMember;
                } else {
                    this.added_ = RequestingMember.newBuilder(this.added_).mergeFrom((RequestingMember.Builder) requestingMember).buildPartial();
                }
            }

            public void clearAdded() {
                this.added_ = null;
            }

            public static AddRequestingMemberAction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (AddRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static AddRequestingMemberAction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (AddRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static AddRequestingMemberAction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (AddRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static AddRequestingMemberAction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (AddRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static AddRequestingMemberAction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (AddRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static AddRequestingMemberAction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (AddRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static AddRequestingMemberAction parseFrom(InputStream inputStream) throws IOException {
                return (AddRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static AddRequestingMemberAction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (AddRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static AddRequestingMemberAction parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (AddRequestingMemberAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static AddRequestingMemberAction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (AddRequestingMemberAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static AddRequestingMemberAction parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (AddRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static AddRequestingMemberAction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (AddRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(AddRequestingMemberAction addRequestingMemberAction) {
                return DEFAULT_INSTANCE.createBuilder(addRequestingMemberAction);
            }

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<AddRequestingMemberAction, Builder> implements AddRequestingMemberActionOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(AddRequestingMemberAction.DEFAULT_INSTANCE);
                }

                public Builder setAdded(RequestingMember.Builder builder) {
                    copyOnWrite();
                    ((AddRequestingMemberAction) this.instance).setAdded(builder.build());
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new AddRequestingMemberAction();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\t", new Object[]{"added_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<AddRequestingMemberAction> parser = PARSER;
                        if (parser == null) {
                            synchronized (AddRequestingMemberAction.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                AddRequestingMemberAction addRequestingMemberAction = new AddRequestingMemberAction();
                DEFAULT_INSTANCE = addRequestingMemberAction;
                GeneratedMessageLite.registerDefaultInstance(AddRequestingMemberAction.class, addRequestingMemberAction);
            }

            public static AddRequestingMemberAction getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<AddRequestingMemberAction> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes3.dex */
        public static final class DeleteRequestingMemberAction extends GeneratedMessageLite<DeleteRequestingMemberAction, Builder> implements DeleteRequestingMemberActionOrBuilder {
            private static final DeleteRequestingMemberAction DEFAULT_INSTANCE;
            public static final int DELETEDUSERID_FIELD_NUMBER;
            private static volatile Parser<DeleteRequestingMemberAction> PARSER;
            private ByteString deletedUserId_ = ByteString.EMPTY;

            private DeleteRequestingMemberAction() {
            }

            public ByteString getDeletedUserId() {
                return this.deletedUserId_;
            }

            public void setDeletedUserId(ByteString byteString) {
                byteString.getClass();
                this.deletedUserId_ = byteString;
            }

            public void clearDeletedUserId() {
                this.deletedUserId_ = getDefaultInstance().getDeletedUserId();
            }

            public static DeleteRequestingMemberAction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (DeleteRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static DeleteRequestingMemberAction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (DeleteRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static DeleteRequestingMemberAction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (DeleteRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static DeleteRequestingMemberAction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (DeleteRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static DeleteRequestingMemberAction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (DeleteRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static DeleteRequestingMemberAction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (DeleteRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static DeleteRequestingMemberAction parseFrom(InputStream inputStream) throws IOException {
                return (DeleteRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static DeleteRequestingMemberAction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (DeleteRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static DeleteRequestingMemberAction parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (DeleteRequestingMemberAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static DeleteRequestingMemberAction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (DeleteRequestingMemberAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static DeleteRequestingMemberAction parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (DeleteRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static DeleteRequestingMemberAction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (DeleteRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(DeleteRequestingMemberAction deleteRequestingMemberAction) {
                return DEFAULT_INSTANCE.createBuilder(deleteRequestingMemberAction);
            }

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<DeleteRequestingMemberAction, Builder> implements DeleteRequestingMemberActionOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(DeleteRequestingMemberAction.DEFAULT_INSTANCE);
                }

                public Builder setDeletedUserId(ByteString byteString) {
                    copyOnWrite();
                    ((DeleteRequestingMemberAction) this.instance).setDeletedUserId(byteString);
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new DeleteRequestingMemberAction();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\n", new Object[]{"deletedUserId_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<DeleteRequestingMemberAction> parser = PARSER;
                        if (parser == null) {
                            synchronized (DeleteRequestingMemberAction.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                DeleteRequestingMemberAction deleteRequestingMemberAction = new DeleteRequestingMemberAction();
                DEFAULT_INSTANCE = deleteRequestingMemberAction;
                GeneratedMessageLite.registerDefaultInstance(DeleteRequestingMemberAction.class, deleteRequestingMemberAction);
            }

            public static DeleteRequestingMemberAction getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<DeleteRequestingMemberAction> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes3.dex */
        public static final class PromoteRequestingMemberAction extends GeneratedMessageLite<PromoteRequestingMemberAction, Builder> implements PromoteRequestingMemberActionOrBuilder {
            private static final PromoteRequestingMemberAction DEFAULT_INSTANCE;
            private static volatile Parser<PromoteRequestingMemberAction> PARSER;
            public static final int ROLE_FIELD_NUMBER;
            public static final int USERID_FIELD_NUMBER;
            private int role_;
            private ByteString userId_ = ByteString.EMPTY;

            private PromoteRequestingMemberAction() {
            }

            public ByteString getUserId() {
                return this.userId_;
            }

            public void setUserId(ByteString byteString) {
                byteString.getClass();
                this.userId_ = byteString;
            }

            public void clearUserId() {
                this.userId_ = getDefaultInstance().getUserId();
            }

            public int getRoleValue() {
                return this.role_;
            }

            public Member.Role getRole() {
                Member.Role forNumber = Member.Role.forNumber(this.role_);
                return forNumber == null ? Member.Role.UNRECOGNIZED : forNumber;
            }

            public void setRoleValue(int i) {
                this.role_ = i;
            }

            public void setRole(Member.Role role) {
                this.role_ = role.getNumber();
            }

            public void clearRole() {
                this.role_ = 0;
            }

            public static PromoteRequestingMemberAction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (PromoteRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static PromoteRequestingMemberAction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (PromoteRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static PromoteRequestingMemberAction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (PromoteRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static PromoteRequestingMemberAction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (PromoteRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static PromoteRequestingMemberAction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (PromoteRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static PromoteRequestingMemberAction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (PromoteRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static PromoteRequestingMemberAction parseFrom(InputStream inputStream) throws IOException {
                return (PromoteRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static PromoteRequestingMemberAction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (PromoteRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static PromoteRequestingMemberAction parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (PromoteRequestingMemberAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static PromoteRequestingMemberAction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (PromoteRequestingMemberAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static PromoteRequestingMemberAction parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (PromoteRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static PromoteRequestingMemberAction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (PromoteRequestingMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(PromoteRequestingMemberAction promoteRequestingMemberAction) {
                return DEFAULT_INSTANCE.createBuilder(promoteRequestingMemberAction);
            }

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<PromoteRequestingMemberAction, Builder> implements PromoteRequestingMemberActionOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(PromoteRequestingMemberAction.DEFAULT_INSTANCE);
                }

                public Builder setUserId(ByteString byteString) {
                    copyOnWrite();
                    ((PromoteRequestingMemberAction) this.instance).setUserId(byteString);
                    return this;
                }

                public Builder setRole(Member.Role role) {
                    copyOnWrite();
                    ((PromoteRequestingMemberAction) this.instance).setRole(role);
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new PromoteRequestingMemberAction();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\n\u0002\f", new Object[]{"userId_", "role_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<PromoteRequestingMemberAction> parser = PARSER;
                        if (parser == null) {
                            synchronized (PromoteRequestingMemberAction.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                PromoteRequestingMemberAction promoteRequestingMemberAction = new PromoteRequestingMemberAction();
                DEFAULT_INSTANCE = promoteRequestingMemberAction;
                GeneratedMessageLite.registerDefaultInstance(PromoteRequestingMemberAction.class, promoteRequestingMemberAction);
            }

            public static PromoteRequestingMemberAction getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<PromoteRequestingMemberAction> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes3.dex */
        public static final class AddBannedMemberAction extends GeneratedMessageLite<AddBannedMemberAction, Builder> implements AddBannedMemberActionOrBuilder {
            public static final int ADDED_FIELD_NUMBER;
            private static final AddBannedMemberAction DEFAULT_INSTANCE;
            private static volatile Parser<AddBannedMemberAction> PARSER;
            private BannedMember added_;

            private AddBannedMemberAction() {
            }

            public boolean hasAdded() {
                return this.added_ != null;
            }

            public BannedMember getAdded() {
                BannedMember bannedMember = this.added_;
                return bannedMember == null ? BannedMember.getDefaultInstance() : bannedMember;
            }

            public void setAdded(BannedMember bannedMember) {
                bannedMember.getClass();
                this.added_ = bannedMember;
            }

            public void mergeAdded(BannedMember bannedMember) {
                bannedMember.getClass();
                BannedMember bannedMember2 = this.added_;
                if (bannedMember2 == null || bannedMember2 == BannedMember.getDefaultInstance()) {
                    this.added_ = bannedMember;
                } else {
                    this.added_ = BannedMember.newBuilder(this.added_).mergeFrom((BannedMember.Builder) bannedMember).buildPartial();
                }
            }

            public void clearAdded() {
                this.added_ = null;
            }

            public static AddBannedMemberAction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (AddBannedMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static AddBannedMemberAction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (AddBannedMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static AddBannedMemberAction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (AddBannedMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static AddBannedMemberAction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (AddBannedMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static AddBannedMemberAction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (AddBannedMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static AddBannedMemberAction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (AddBannedMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static AddBannedMemberAction parseFrom(InputStream inputStream) throws IOException {
                return (AddBannedMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static AddBannedMemberAction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (AddBannedMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static AddBannedMemberAction parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (AddBannedMemberAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static AddBannedMemberAction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (AddBannedMemberAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static AddBannedMemberAction parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (AddBannedMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static AddBannedMemberAction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (AddBannedMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(AddBannedMemberAction addBannedMemberAction) {
                return DEFAULT_INSTANCE.createBuilder(addBannedMemberAction);
            }

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<AddBannedMemberAction, Builder> implements AddBannedMemberActionOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(AddBannedMemberAction.DEFAULT_INSTANCE);
                }

                public Builder setAdded(BannedMember bannedMember) {
                    copyOnWrite();
                    ((AddBannedMemberAction) this.instance).setAdded(bannedMember);
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new AddBannedMemberAction();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\t", new Object[]{"added_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<AddBannedMemberAction> parser = PARSER;
                        if (parser == null) {
                            synchronized (AddBannedMemberAction.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                AddBannedMemberAction addBannedMemberAction = new AddBannedMemberAction();
                DEFAULT_INSTANCE = addBannedMemberAction;
                GeneratedMessageLite.registerDefaultInstance(AddBannedMemberAction.class, addBannedMemberAction);
            }

            public static AddBannedMemberAction getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<AddBannedMemberAction> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes3.dex */
        public static final class DeleteBannedMemberAction extends GeneratedMessageLite<DeleteBannedMemberAction, Builder> implements DeleteBannedMemberActionOrBuilder {
            private static final DeleteBannedMemberAction DEFAULT_INSTANCE;
            public static final int DELETEDUSERID_FIELD_NUMBER;
            private static volatile Parser<DeleteBannedMemberAction> PARSER;
            private ByteString deletedUserId_ = ByteString.EMPTY;

            private DeleteBannedMemberAction() {
            }

            public ByteString getDeletedUserId() {
                return this.deletedUserId_;
            }

            public void setDeletedUserId(ByteString byteString) {
                byteString.getClass();
                this.deletedUserId_ = byteString;
            }

            public void clearDeletedUserId() {
                this.deletedUserId_ = getDefaultInstance().getDeletedUserId();
            }

            public static DeleteBannedMemberAction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (DeleteBannedMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static DeleteBannedMemberAction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (DeleteBannedMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static DeleteBannedMemberAction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (DeleteBannedMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static DeleteBannedMemberAction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (DeleteBannedMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static DeleteBannedMemberAction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (DeleteBannedMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static DeleteBannedMemberAction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (DeleteBannedMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static DeleteBannedMemberAction parseFrom(InputStream inputStream) throws IOException {
                return (DeleteBannedMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static DeleteBannedMemberAction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (DeleteBannedMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static DeleteBannedMemberAction parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (DeleteBannedMemberAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static DeleteBannedMemberAction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (DeleteBannedMemberAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static DeleteBannedMemberAction parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (DeleteBannedMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static DeleteBannedMemberAction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (DeleteBannedMemberAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(DeleteBannedMemberAction deleteBannedMemberAction) {
                return DEFAULT_INSTANCE.createBuilder(deleteBannedMemberAction);
            }

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<DeleteBannedMemberAction, Builder> implements DeleteBannedMemberActionOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(DeleteBannedMemberAction.DEFAULT_INSTANCE);
                }

                public Builder setDeletedUserId(ByteString byteString) {
                    copyOnWrite();
                    ((DeleteBannedMemberAction) this.instance).setDeletedUserId(byteString);
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new DeleteBannedMemberAction();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\n", new Object[]{"deletedUserId_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<DeleteBannedMemberAction> parser = PARSER;
                        if (parser == null) {
                            synchronized (DeleteBannedMemberAction.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                DeleteBannedMemberAction deleteBannedMemberAction = new DeleteBannedMemberAction();
                DEFAULT_INSTANCE = deleteBannedMemberAction;
                GeneratedMessageLite.registerDefaultInstance(DeleteBannedMemberAction.class, deleteBannedMemberAction);
            }

            public static DeleteBannedMemberAction getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<DeleteBannedMemberAction> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes3.dex */
        public static final class ModifyTitleAction extends GeneratedMessageLite<ModifyTitleAction, Builder> implements MessageLiteOrBuilder {
            private static final ModifyTitleAction DEFAULT_INSTANCE;
            private static volatile Parser<ModifyTitleAction> PARSER;
            public static final int TITLE_FIELD_NUMBER;
            private ByteString title_ = ByteString.EMPTY;

            private ModifyTitleAction() {
            }

            public ByteString getTitle() {
                return this.title_;
            }

            public void setTitle(ByteString byteString) {
                byteString.getClass();
                this.title_ = byteString;
            }

            public void clearTitle() {
                this.title_ = getDefaultInstance().getTitle();
            }

            public static ModifyTitleAction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (ModifyTitleAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static ModifyTitleAction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyTitleAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static ModifyTitleAction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (ModifyTitleAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static ModifyTitleAction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyTitleAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static ModifyTitleAction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (ModifyTitleAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static ModifyTitleAction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyTitleAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static ModifyTitleAction parseFrom(InputStream inputStream) throws IOException {
                return (ModifyTitleAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static ModifyTitleAction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyTitleAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static ModifyTitleAction parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (ModifyTitleAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static ModifyTitleAction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyTitleAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static ModifyTitleAction parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (ModifyTitleAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static ModifyTitleAction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyTitleAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(ModifyTitleAction modifyTitleAction) {
                return DEFAULT_INSTANCE.createBuilder(modifyTitleAction);
            }

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<ModifyTitleAction, Builder> implements MessageLiteOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(ModifyTitleAction.DEFAULT_INSTANCE);
                }

                public Builder setTitle(ByteString byteString) {
                    copyOnWrite();
                    ((ModifyTitleAction) this.instance).setTitle(byteString);
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new ModifyTitleAction();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\n", new Object[]{"title_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<ModifyTitleAction> parser = PARSER;
                        if (parser == null) {
                            synchronized (ModifyTitleAction.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                ModifyTitleAction modifyTitleAction = new ModifyTitleAction();
                DEFAULT_INSTANCE = modifyTitleAction;
                GeneratedMessageLite.registerDefaultInstance(ModifyTitleAction.class, modifyTitleAction);
            }

            public static ModifyTitleAction getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<ModifyTitleAction> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes3.dex */
        public static final class ModifyDescriptionAction extends GeneratedMessageLite<ModifyDescriptionAction, Builder> implements MessageLiteOrBuilder {
            private static final ModifyDescriptionAction DEFAULT_INSTANCE;
            public static final int DESCRIPTION_FIELD_NUMBER;
            private static volatile Parser<ModifyDescriptionAction> PARSER;
            private ByteString description_ = ByteString.EMPTY;

            private ModifyDescriptionAction() {
            }

            public ByteString getDescription() {
                return this.description_;
            }

            public void setDescription(ByteString byteString) {
                byteString.getClass();
                this.description_ = byteString;
            }

            public void clearDescription() {
                this.description_ = getDefaultInstance().getDescription();
            }

            public static ModifyDescriptionAction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (ModifyDescriptionAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static ModifyDescriptionAction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyDescriptionAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static ModifyDescriptionAction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (ModifyDescriptionAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static ModifyDescriptionAction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyDescriptionAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static ModifyDescriptionAction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (ModifyDescriptionAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static ModifyDescriptionAction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyDescriptionAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static ModifyDescriptionAction parseFrom(InputStream inputStream) throws IOException {
                return (ModifyDescriptionAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static ModifyDescriptionAction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyDescriptionAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static ModifyDescriptionAction parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (ModifyDescriptionAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static ModifyDescriptionAction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyDescriptionAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static ModifyDescriptionAction parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (ModifyDescriptionAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static ModifyDescriptionAction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyDescriptionAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(ModifyDescriptionAction modifyDescriptionAction) {
                return DEFAULT_INSTANCE.createBuilder(modifyDescriptionAction);
            }

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<ModifyDescriptionAction, Builder> implements MessageLiteOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(ModifyDescriptionAction.DEFAULT_INSTANCE);
                }

                public Builder setDescription(ByteString byteString) {
                    copyOnWrite();
                    ((ModifyDescriptionAction) this.instance).setDescription(byteString);
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new ModifyDescriptionAction();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\n", new Object[]{"description_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<ModifyDescriptionAction> parser = PARSER;
                        if (parser == null) {
                            synchronized (ModifyDescriptionAction.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                ModifyDescriptionAction modifyDescriptionAction = new ModifyDescriptionAction();
                DEFAULT_INSTANCE = modifyDescriptionAction;
                GeneratedMessageLite.registerDefaultInstance(ModifyDescriptionAction.class, modifyDescriptionAction);
            }

            public static ModifyDescriptionAction getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<ModifyDescriptionAction> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes3.dex */
        public static final class ModifyAvatarAction extends GeneratedMessageLite<ModifyAvatarAction, Builder> implements MessageLiteOrBuilder {
            public static final int AVATAR_FIELD_NUMBER;
            private static final ModifyAvatarAction DEFAULT_INSTANCE;
            private static volatile Parser<ModifyAvatarAction> PARSER;
            private String avatar_ = "";

            private ModifyAvatarAction() {
            }

            public String getAvatar() {
                return this.avatar_;
            }

            public ByteString getAvatarBytes() {
                return ByteString.copyFromUtf8(this.avatar_);
            }

            public void setAvatar(String str) {
                str.getClass();
                this.avatar_ = str;
            }

            public void clearAvatar() {
                this.avatar_ = getDefaultInstance().getAvatar();
            }

            public void setAvatarBytes(ByteString byteString) {
                AbstractMessageLite.checkByteStringIsUtf8(byteString);
                this.avatar_ = byteString.toStringUtf8();
            }

            public static ModifyAvatarAction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (ModifyAvatarAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static ModifyAvatarAction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyAvatarAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static ModifyAvatarAction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (ModifyAvatarAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static ModifyAvatarAction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyAvatarAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static ModifyAvatarAction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (ModifyAvatarAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static ModifyAvatarAction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyAvatarAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static ModifyAvatarAction parseFrom(InputStream inputStream) throws IOException {
                return (ModifyAvatarAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static ModifyAvatarAction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyAvatarAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static ModifyAvatarAction parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (ModifyAvatarAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static ModifyAvatarAction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyAvatarAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static ModifyAvatarAction parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (ModifyAvatarAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static ModifyAvatarAction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyAvatarAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(ModifyAvatarAction modifyAvatarAction) {
                return DEFAULT_INSTANCE.createBuilder(modifyAvatarAction);
            }

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<ModifyAvatarAction, Builder> implements MessageLiteOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(ModifyAvatarAction.DEFAULT_INSTANCE);
                }

                public Builder setAvatar(String str) {
                    copyOnWrite();
                    ((ModifyAvatarAction) this.instance).setAvatar(str);
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new ModifyAvatarAction();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001Ȉ", new Object[]{"avatar_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<ModifyAvatarAction> parser = PARSER;
                        if (parser == null) {
                            synchronized (ModifyAvatarAction.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                ModifyAvatarAction modifyAvatarAction = new ModifyAvatarAction();
                DEFAULT_INSTANCE = modifyAvatarAction;
                GeneratedMessageLite.registerDefaultInstance(ModifyAvatarAction.class, modifyAvatarAction);
            }

            public static ModifyAvatarAction getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<ModifyAvatarAction> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes3.dex */
        public static final class ModifyDisappearingMessagesTimerAction extends GeneratedMessageLite<ModifyDisappearingMessagesTimerAction, Builder> implements MessageLiteOrBuilder {
            private static final ModifyDisappearingMessagesTimerAction DEFAULT_INSTANCE;
            private static volatile Parser<ModifyDisappearingMessagesTimerAction> PARSER;
            public static final int TIMER_FIELD_NUMBER;
            private ByteString timer_ = ByteString.EMPTY;

            private ModifyDisappearingMessagesTimerAction() {
            }

            public ByteString getTimer() {
                return this.timer_;
            }

            public void setTimer(ByteString byteString) {
                byteString.getClass();
                this.timer_ = byteString;
            }

            public void clearTimer() {
                this.timer_ = getDefaultInstance().getTimer();
            }

            public static ModifyDisappearingMessagesTimerAction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (ModifyDisappearingMessagesTimerAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static ModifyDisappearingMessagesTimerAction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyDisappearingMessagesTimerAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static ModifyDisappearingMessagesTimerAction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (ModifyDisappearingMessagesTimerAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static ModifyDisappearingMessagesTimerAction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyDisappearingMessagesTimerAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static ModifyDisappearingMessagesTimerAction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (ModifyDisappearingMessagesTimerAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static ModifyDisappearingMessagesTimerAction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyDisappearingMessagesTimerAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static ModifyDisappearingMessagesTimerAction parseFrom(InputStream inputStream) throws IOException {
                return (ModifyDisappearingMessagesTimerAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static ModifyDisappearingMessagesTimerAction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyDisappearingMessagesTimerAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static ModifyDisappearingMessagesTimerAction parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (ModifyDisappearingMessagesTimerAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static ModifyDisappearingMessagesTimerAction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyDisappearingMessagesTimerAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static ModifyDisappearingMessagesTimerAction parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (ModifyDisappearingMessagesTimerAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static ModifyDisappearingMessagesTimerAction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyDisappearingMessagesTimerAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(ModifyDisappearingMessagesTimerAction modifyDisappearingMessagesTimerAction) {
                return DEFAULT_INSTANCE.createBuilder(modifyDisappearingMessagesTimerAction);
            }

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<ModifyDisappearingMessagesTimerAction, Builder> implements MessageLiteOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(ModifyDisappearingMessagesTimerAction.DEFAULT_INSTANCE);
                }

                public Builder setTimer(ByteString byteString) {
                    copyOnWrite();
                    ((ModifyDisappearingMessagesTimerAction) this.instance).setTimer(byteString);
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new ModifyDisappearingMessagesTimerAction();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\n", new Object[]{"timer_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<ModifyDisappearingMessagesTimerAction> parser = PARSER;
                        if (parser == null) {
                            synchronized (ModifyDisappearingMessagesTimerAction.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                ModifyDisappearingMessagesTimerAction modifyDisappearingMessagesTimerAction = new ModifyDisappearingMessagesTimerAction();
                DEFAULT_INSTANCE = modifyDisappearingMessagesTimerAction;
                GeneratedMessageLite.registerDefaultInstance(ModifyDisappearingMessagesTimerAction.class, modifyDisappearingMessagesTimerAction);
            }

            public static ModifyDisappearingMessagesTimerAction getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<ModifyDisappearingMessagesTimerAction> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes3.dex */
        public static final class ModifyAttributesAccessControlAction extends GeneratedMessageLite<ModifyAttributesAccessControlAction, Builder> implements MessageLiteOrBuilder {
            public static final int ATTRIBUTESACCESS_FIELD_NUMBER;
            private static final ModifyAttributesAccessControlAction DEFAULT_INSTANCE;
            private static volatile Parser<ModifyAttributesAccessControlAction> PARSER;
            private int attributesAccess_;

            private ModifyAttributesAccessControlAction() {
            }

            public int getAttributesAccessValue() {
                return this.attributesAccess_;
            }

            public AccessControl.AccessRequired getAttributesAccess() {
                AccessControl.AccessRequired forNumber = AccessControl.AccessRequired.forNumber(this.attributesAccess_);
                return forNumber == null ? AccessControl.AccessRequired.UNRECOGNIZED : forNumber;
            }

            public void setAttributesAccessValue(int i) {
                this.attributesAccess_ = i;
            }

            public void setAttributesAccess(AccessControl.AccessRequired accessRequired) {
                this.attributesAccess_ = accessRequired.getNumber();
            }

            public void clearAttributesAccess() {
                this.attributesAccess_ = 0;
            }

            public static ModifyAttributesAccessControlAction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (ModifyAttributesAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static ModifyAttributesAccessControlAction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyAttributesAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static ModifyAttributesAccessControlAction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (ModifyAttributesAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static ModifyAttributesAccessControlAction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyAttributesAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static ModifyAttributesAccessControlAction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (ModifyAttributesAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static ModifyAttributesAccessControlAction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyAttributesAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static ModifyAttributesAccessControlAction parseFrom(InputStream inputStream) throws IOException {
                return (ModifyAttributesAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static ModifyAttributesAccessControlAction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyAttributesAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static ModifyAttributesAccessControlAction parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (ModifyAttributesAccessControlAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static ModifyAttributesAccessControlAction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyAttributesAccessControlAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static ModifyAttributesAccessControlAction parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (ModifyAttributesAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static ModifyAttributesAccessControlAction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyAttributesAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(ModifyAttributesAccessControlAction modifyAttributesAccessControlAction) {
                return DEFAULT_INSTANCE.createBuilder(modifyAttributesAccessControlAction);
            }

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<ModifyAttributesAccessControlAction, Builder> implements MessageLiteOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(ModifyAttributesAccessControlAction.DEFAULT_INSTANCE);
                }

                public Builder setAttributesAccess(AccessControl.AccessRequired accessRequired) {
                    copyOnWrite();
                    ((ModifyAttributesAccessControlAction) this.instance).setAttributesAccess(accessRequired);
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new ModifyAttributesAccessControlAction();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\f", new Object[]{"attributesAccess_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<ModifyAttributesAccessControlAction> parser = PARSER;
                        if (parser == null) {
                            synchronized (ModifyAttributesAccessControlAction.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                ModifyAttributesAccessControlAction modifyAttributesAccessControlAction = new ModifyAttributesAccessControlAction();
                DEFAULT_INSTANCE = modifyAttributesAccessControlAction;
                GeneratedMessageLite.registerDefaultInstance(ModifyAttributesAccessControlAction.class, modifyAttributesAccessControlAction);
            }

            public static ModifyAttributesAccessControlAction getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<ModifyAttributesAccessControlAction> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes3.dex */
        public static final class ModifyMembersAccessControlAction extends GeneratedMessageLite<ModifyMembersAccessControlAction, Builder> implements MessageLiteOrBuilder {
            private static final ModifyMembersAccessControlAction DEFAULT_INSTANCE;
            public static final int MEMBERSACCESS_FIELD_NUMBER;
            private static volatile Parser<ModifyMembersAccessControlAction> PARSER;
            private int membersAccess_;

            private ModifyMembersAccessControlAction() {
            }

            public int getMembersAccessValue() {
                return this.membersAccess_;
            }

            public AccessControl.AccessRequired getMembersAccess() {
                AccessControl.AccessRequired forNumber = AccessControl.AccessRequired.forNumber(this.membersAccess_);
                return forNumber == null ? AccessControl.AccessRequired.UNRECOGNIZED : forNumber;
            }

            public void setMembersAccessValue(int i) {
                this.membersAccess_ = i;
            }

            public void setMembersAccess(AccessControl.AccessRequired accessRequired) {
                this.membersAccess_ = accessRequired.getNumber();
            }

            public void clearMembersAccess() {
                this.membersAccess_ = 0;
            }

            public static ModifyMembersAccessControlAction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (ModifyMembersAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static ModifyMembersAccessControlAction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyMembersAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static ModifyMembersAccessControlAction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (ModifyMembersAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static ModifyMembersAccessControlAction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyMembersAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static ModifyMembersAccessControlAction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (ModifyMembersAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static ModifyMembersAccessControlAction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyMembersAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static ModifyMembersAccessControlAction parseFrom(InputStream inputStream) throws IOException {
                return (ModifyMembersAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static ModifyMembersAccessControlAction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyMembersAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static ModifyMembersAccessControlAction parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (ModifyMembersAccessControlAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static ModifyMembersAccessControlAction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyMembersAccessControlAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static ModifyMembersAccessControlAction parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (ModifyMembersAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static ModifyMembersAccessControlAction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyMembersAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(ModifyMembersAccessControlAction modifyMembersAccessControlAction) {
                return DEFAULT_INSTANCE.createBuilder(modifyMembersAccessControlAction);
            }

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<ModifyMembersAccessControlAction, Builder> implements MessageLiteOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(ModifyMembersAccessControlAction.DEFAULT_INSTANCE);
                }

                public Builder setMembersAccess(AccessControl.AccessRequired accessRequired) {
                    copyOnWrite();
                    ((ModifyMembersAccessControlAction) this.instance).setMembersAccess(accessRequired);
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new ModifyMembersAccessControlAction();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\f", new Object[]{"membersAccess_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<ModifyMembersAccessControlAction> parser = PARSER;
                        if (parser == null) {
                            synchronized (ModifyMembersAccessControlAction.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                ModifyMembersAccessControlAction modifyMembersAccessControlAction = new ModifyMembersAccessControlAction();
                DEFAULT_INSTANCE = modifyMembersAccessControlAction;
                GeneratedMessageLite.registerDefaultInstance(ModifyMembersAccessControlAction.class, modifyMembersAccessControlAction);
            }

            public static ModifyMembersAccessControlAction getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<ModifyMembersAccessControlAction> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes3.dex */
        public static final class ModifyAddFromInviteLinkAccessControlAction extends GeneratedMessageLite<ModifyAddFromInviteLinkAccessControlAction, Builder> implements MessageLiteOrBuilder {
            public static final int ADDFROMINVITELINKACCESS_FIELD_NUMBER;
            private static final ModifyAddFromInviteLinkAccessControlAction DEFAULT_INSTANCE;
            private static volatile Parser<ModifyAddFromInviteLinkAccessControlAction> PARSER;
            private int addFromInviteLinkAccess_;

            private ModifyAddFromInviteLinkAccessControlAction() {
            }

            public int getAddFromInviteLinkAccessValue() {
                return this.addFromInviteLinkAccess_;
            }

            public AccessControl.AccessRequired getAddFromInviteLinkAccess() {
                AccessControl.AccessRequired forNumber = AccessControl.AccessRequired.forNumber(this.addFromInviteLinkAccess_);
                return forNumber == null ? AccessControl.AccessRequired.UNRECOGNIZED : forNumber;
            }

            public void setAddFromInviteLinkAccessValue(int i) {
                this.addFromInviteLinkAccess_ = i;
            }

            public void setAddFromInviteLinkAccess(AccessControl.AccessRequired accessRequired) {
                this.addFromInviteLinkAccess_ = accessRequired.getNumber();
            }

            public void clearAddFromInviteLinkAccess() {
                this.addFromInviteLinkAccess_ = 0;
            }

            public static ModifyAddFromInviteLinkAccessControlAction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (ModifyAddFromInviteLinkAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static ModifyAddFromInviteLinkAccessControlAction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyAddFromInviteLinkAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static ModifyAddFromInviteLinkAccessControlAction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (ModifyAddFromInviteLinkAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static ModifyAddFromInviteLinkAccessControlAction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyAddFromInviteLinkAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static ModifyAddFromInviteLinkAccessControlAction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (ModifyAddFromInviteLinkAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static ModifyAddFromInviteLinkAccessControlAction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyAddFromInviteLinkAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static ModifyAddFromInviteLinkAccessControlAction parseFrom(InputStream inputStream) throws IOException {
                return (ModifyAddFromInviteLinkAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static ModifyAddFromInviteLinkAccessControlAction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyAddFromInviteLinkAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static ModifyAddFromInviteLinkAccessControlAction parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (ModifyAddFromInviteLinkAccessControlAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static ModifyAddFromInviteLinkAccessControlAction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyAddFromInviteLinkAccessControlAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static ModifyAddFromInviteLinkAccessControlAction parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (ModifyAddFromInviteLinkAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static ModifyAddFromInviteLinkAccessControlAction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyAddFromInviteLinkAccessControlAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(ModifyAddFromInviteLinkAccessControlAction modifyAddFromInviteLinkAccessControlAction) {
                return DEFAULT_INSTANCE.createBuilder(modifyAddFromInviteLinkAccessControlAction);
            }

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<ModifyAddFromInviteLinkAccessControlAction, Builder> implements MessageLiteOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(ModifyAddFromInviteLinkAccessControlAction.DEFAULT_INSTANCE);
                }

                public Builder setAddFromInviteLinkAccess(AccessControl.AccessRequired accessRequired) {
                    copyOnWrite();
                    ((ModifyAddFromInviteLinkAccessControlAction) this.instance).setAddFromInviteLinkAccess(accessRequired);
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new ModifyAddFromInviteLinkAccessControlAction();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\f", new Object[]{"addFromInviteLinkAccess_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<ModifyAddFromInviteLinkAccessControlAction> parser = PARSER;
                        if (parser == null) {
                            synchronized (ModifyAddFromInviteLinkAccessControlAction.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                ModifyAddFromInviteLinkAccessControlAction modifyAddFromInviteLinkAccessControlAction = new ModifyAddFromInviteLinkAccessControlAction();
                DEFAULT_INSTANCE = modifyAddFromInviteLinkAccessControlAction;
                GeneratedMessageLite.registerDefaultInstance(ModifyAddFromInviteLinkAccessControlAction.class, modifyAddFromInviteLinkAccessControlAction);
            }

            public static ModifyAddFromInviteLinkAccessControlAction getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<ModifyAddFromInviteLinkAccessControlAction> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes3.dex */
        public static final class ModifyInviteLinkPasswordAction extends GeneratedMessageLite<ModifyInviteLinkPasswordAction, Builder> implements MessageLiteOrBuilder {
            private static final ModifyInviteLinkPasswordAction DEFAULT_INSTANCE;
            public static final int INVITELINKPASSWORD_FIELD_NUMBER;
            private static volatile Parser<ModifyInviteLinkPasswordAction> PARSER;
            private ByteString inviteLinkPassword_ = ByteString.EMPTY;

            private ModifyInviteLinkPasswordAction() {
            }

            public ByteString getInviteLinkPassword() {
                return this.inviteLinkPassword_;
            }

            public void setInviteLinkPassword(ByteString byteString) {
                byteString.getClass();
                this.inviteLinkPassword_ = byteString;
            }

            public void clearInviteLinkPassword() {
                this.inviteLinkPassword_ = getDefaultInstance().getInviteLinkPassword();
            }

            public static ModifyInviteLinkPasswordAction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (ModifyInviteLinkPasswordAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static ModifyInviteLinkPasswordAction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyInviteLinkPasswordAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static ModifyInviteLinkPasswordAction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (ModifyInviteLinkPasswordAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static ModifyInviteLinkPasswordAction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyInviteLinkPasswordAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static ModifyInviteLinkPasswordAction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (ModifyInviteLinkPasswordAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static ModifyInviteLinkPasswordAction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyInviteLinkPasswordAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static ModifyInviteLinkPasswordAction parseFrom(InputStream inputStream) throws IOException {
                return (ModifyInviteLinkPasswordAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static ModifyInviteLinkPasswordAction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyInviteLinkPasswordAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static ModifyInviteLinkPasswordAction parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (ModifyInviteLinkPasswordAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static ModifyInviteLinkPasswordAction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyInviteLinkPasswordAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static ModifyInviteLinkPasswordAction parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (ModifyInviteLinkPasswordAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static ModifyInviteLinkPasswordAction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyInviteLinkPasswordAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(ModifyInviteLinkPasswordAction modifyInviteLinkPasswordAction) {
                return DEFAULT_INSTANCE.createBuilder(modifyInviteLinkPasswordAction);
            }

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<ModifyInviteLinkPasswordAction, Builder> implements MessageLiteOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(ModifyInviteLinkPasswordAction.DEFAULT_INSTANCE);
                }

                public Builder setInviteLinkPassword(ByteString byteString) {
                    copyOnWrite();
                    ((ModifyInviteLinkPasswordAction) this.instance).setInviteLinkPassword(byteString);
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new ModifyInviteLinkPasswordAction();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\n", new Object[]{"inviteLinkPassword_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<ModifyInviteLinkPasswordAction> parser = PARSER;
                        if (parser == null) {
                            synchronized (ModifyInviteLinkPasswordAction.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                ModifyInviteLinkPasswordAction modifyInviteLinkPasswordAction = new ModifyInviteLinkPasswordAction();
                DEFAULT_INSTANCE = modifyInviteLinkPasswordAction;
                GeneratedMessageLite.registerDefaultInstance(ModifyInviteLinkPasswordAction.class, modifyInviteLinkPasswordAction);
            }

            public static ModifyInviteLinkPasswordAction getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<ModifyInviteLinkPasswordAction> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        /* loaded from: classes3.dex */
        public static final class ModifyAnnouncementsOnlyAction extends GeneratedMessageLite<ModifyAnnouncementsOnlyAction, Builder> implements MessageLiteOrBuilder {
            public static final int ANNOUNCEMENTSONLY_FIELD_NUMBER;
            private static final ModifyAnnouncementsOnlyAction DEFAULT_INSTANCE;
            private static volatile Parser<ModifyAnnouncementsOnlyAction> PARSER;
            private boolean announcementsOnly_;

            private ModifyAnnouncementsOnlyAction() {
            }

            public boolean getAnnouncementsOnly() {
                return this.announcementsOnly_;
            }

            public void setAnnouncementsOnly(boolean z) {
                this.announcementsOnly_ = z;
            }

            public void clearAnnouncementsOnly() {
                this.announcementsOnly_ = false;
            }

            public static ModifyAnnouncementsOnlyAction parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (ModifyAnnouncementsOnlyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static ModifyAnnouncementsOnlyAction parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyAnnouncementsOnlyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static ModifyAnnouncementsOnlyAction parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (ModifyAnnouncementsOnlyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static ModifyAnnouncementsOnlyAction parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyAnnouncementsOnlyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static ModifyAnnouncementsOnlyAction parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (ModifyAnnouncementsOnlyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static ModifyAnnouncementsOnlyAction parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (ModifyAnnouncementsOnlyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static ModifyAnnouncementsOnlyAction parseFrom(InputStream inputStream) throws IOException {
                return (ModifyAnnouncementsOnlyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static ModifyAnnouncementsOnlyAction parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyAnnouncementsOnlyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static ModifyAnnouncementsOnlyAction parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (ModifyAnnouncementsOnlyAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static ModifyAnnouncementsOnlyAction parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyAnnouncementsOnlyAction) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static ModifyAnnouncementsOnlyAction parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (ModifyAnnouncementsOnlyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static ModifyAnnouncementsOnlyAction parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (ModifyAnnouncementsOnlyAction) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(ModifyAnnouncementsOnlyAction modifyAnnouncementsOnlyAction) {
                return DEFAULT_INSTANCE.createBuilder(modifyAnnouncementsOnlyAction);
            }

            /* loaded from: classes3.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<ModifyAnnouncementsOnlyAction, Builder> implements MessageLiteOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(ModifyAnnouncementsOnlyAction.DEFAULT_INSTANCE);
                }

                public Builder setAnnouncementsOnly(boolean z) {
                    copyOnWrite();
                    ((ModifyAnnouncementsOnlyAction) this.instance).setAnnouncementsOnly(z);
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new ModifyAnnouncementsOnlyAction();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001\u0007", new Object[]{"announcementsOnly_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<ModifyAnnouncementsOnlyAction> parser = PARSER;
                        if (parser == null) {
                            synchronized (ModifyAnnouncementsOnlyAction.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                ModifyAnnouncementsOnlyAction modifyAnnouncementsOnlyAction = new ModifyAnnouncementsOnlyAction();
                DEFAULT_INSTANCE = modifyAnnouncementsOnlyAction;
                GeneratedMessageLite.registerDefaultInstance(ModifyAnnouncementsOnlyAction.class, modifyAnnouncementsOnlyAction);
            }

            public static ModifyAnnouncementsOnlyAction getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<ModifyAnnouncementsOnlyAction> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        public ByteString getSourceUuid() {
            return this.sourceUuid_;
        }

        public void setSourceUuid(ByteString byteString) {
            byteString.getClass();
            this.sourceUuid_ = byteString;
        }

        public void clearSourceUuid() {
            this.sourceUuid_ = getDefaultInstance().getSourceUuid();
        }

        public int getRevision() {
            return this.revision_;
        }

        public void setRevision(int i) {
            this.revision_ = i;
        }

        public void clearRevision() {
            this.revision_ = 0;
        }

        public List<AddMemberAction> getAddMembersList() {
            return this.addMembers_;
        }

        public List<? extends AddMemberActionOrBuilder> getAddMembersOrBuilderList() {
            return this.addMembers_;
        }

        public int getAddMembersCount() {
            return this.addMembers_.size();
        }

        public AddMemberAction getAddMembers(int i) {
            return this.addMembers_.get(i);
        }

        public AddMemberActionOrBuilder getAddMembersOrBuilder(int i) {
            return this.addMembers_.get(i);
        }

        private void ensureAddMembersIsMutable() {
            Internal.ProtobufList<AddMemberAction> protobufList = this.addMembers_;
            if (!protobufList.isModifiable()) {
                this.addMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setAddMembers(int i, AddMemberAction addMemberAction) {
            addMemberAction.getClass();
            ensureAddMembersIsMutable();
            this.addMembers_.set(i, addMemberAction);
        }

        public void addAddMembers(AddMemberAction addMemberAction) {
            addMemberAction.getClass();
            ensureAddMembersIsMutable();
            this.addMembers_.add(addMemberAction);
        }

        public void addAddMembers(int i, AddMemberAction addMemberAction) {
            addMemberAction.getClass();
            ensureAddMembersIsMutable();
            this.addMembers_.add(i, addMemberAction);
        }

        public void addAllAddMembers(Iterable<? extends AddMemberAction> iterable) {
            ensureAddMembersIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.addMembers_);
        }

        public void clearAddMembers() {
            this.addMembers_ = GeneratedMessageLite.emptyProtobufList();
        }

        public void removeAddMembers(int i) {
            ensureAddMembersIsMutable();
            this.addMembers_.remove(i);
        }

        public List<DeleteMemberAction> getDeleteMembersList() {
            return this.deleteMembers_;
        }

        public List<? extends DeleteMemberActionOrBuilder> getDeleteMembersOrBuilderList() {
            return this.deleteMembers_;
        }

        public int getDeleteMembersCount() {
            return this.deleteMembers_.size();
        }

        public DeleteMemberAction getDeleteMembers(int i) {
            return this.deleteMembers_.get(i);
        }

        public DeleteMemberActionOrBuilder getDeleteMembersOrBuilder(int i) {
            return this.deleteMembers_.get(i);
        }

        private void ensureDeleteMembersIsMutable() {
            Internal.ProtobufList<DeleteMemberAction> protobufList = this.deleteMembers_;
            if (!protobufList.isModifiable()) {
                this.deleteMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setDeleteMembers(int i, DeleteMemberAction deleteMemberAction) {
            deleteMemberAction.getClass();
            ensureDeleteMembersIsMutable();
            this.deleteMembers_.set(i, deleteMemberAction);
        }

        public void addDeleteMembers(DeleteMemberAction deleteMemberAction) {
            deleteMemberAction.getClass();
            ensureDeleteMembersIsMutable();
            this.deleteMembers_.add(deleteMemberAction);
        }

        public void addDeleteMembers(int i, DeleteMemberAction deleteMemberAction) {
            deleteMemberAction.getClass();
            ensureDeleteMembersIsMutable();
            this.deleteMembers_.add(i, deleteMemberAction);
        }

        public void addAllDeleteMembers(Iterable<? extends DeleteMemberAction> iterable) {
            ensureDeleteMembersIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.deleteMembers_);
        }

        public void clearDeleteMembers() {
            this.deleteMembers_ = GeneratedMessageLite.emptyProtobufList();
        }

        public void removeDeleteMembers(int i) {
            ensureDeleteMembersIsMutable();
            this.deleteMembers_.remove(i);
        }

        public List<ModifyMemberRoleAction> getModifyMemberRolesList() {
            return this.modifyMemberRoles_;
        }

        public List<? extends ModifyMemberRoleActionOrBuilder> getModifyMemberRolesOrBuilderList() {
            return this.modifyMemberRoles_;
        }

        public int getModifyMemberRolesCount() {
            return this.modifyMemberRoles_.size();
        }

        public ModifyMemberRoleAction getModifyMemberRoles(int i) {
            return this.modifyMemberRoles_.get(i);
        }

        public ModifyMemberRoleActionOrBuilder getModifyMemberRolesOrBuilder(int i) {
            return this.modifyMemberRoles_.get(i);
        }

        private void ensureModifyMemberRolesIsMutable() {
            Internal.ProtobufList<ModifyMemberRoleAction> protobufList = this.modifyMemberRoles_;
            if (!protobufList.isModifiable()) {
                this.modifyMemberRoles_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setModifyMemberRoles(int i, ModifyMemberRoleAction modifyMemberRoleAction) {
            modifyMemberRoleAction.getClass();
            ensureModifyMemberRolesIsMutable();
            this.modifyMemberRoles_.set(i, modifyMemberRoleAction);
        }

        public void addModifyMemberRoles(ModifyMemberRoleAction modifyMemberRoleAction) {
            modifyMemberRoleAction.getClass();
            ensureModifyMemberRolesIsMutable();
            this.modifyMemberRoles_.add(modifyMemberRoleAction);
        }

        public void addModifyMemberRoles(int i, ModifyMemberRoleAction modifyMemberRoleAction) {
            modifyMemberRoleAction.getClass();
            ensureModifyMemberRolesIsMutable();
            this.modifyMemberRoles_.add(i, modifyMemberRoleAction);
        }

        public void addAllModifyMemberRoles(Iterable<? extends ModifyMemberRoleAction> iterable) {
            ensureModifyMemberRolesIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.modifyMemberRoles_);
        }

        public void clearModifyMemberRoles() {
            this.modifyMemberRoles_ = GeneratedMessageLite.emptyProtobufList();
        }

        public void removeModifyMemberRoles(int i) {
            ensureModifyMemberRolesIsMutable();
            this.modifyMemberRoles_.remove(i);
        }

        public List<ModifyMemberProfileKeyAction> getModifyMemberProfileKeysList() {
            return this.modifyMemberProfileKeys_;
        }

        public List<? extends ModifyMemberProfileKeyActionOrBuilder> getModifyMemberProfileKeysOrBuilderList() {
            return this.modifyMemberProfileKeys_;
        }

        public int getModifyMemberProfileKeysCount() {
            return this.modifyMemberProfileKeys_.size();
        }

        public ModifyMemberProfileKeyAction getModifyMemberProfileKeys(int i) {
            return this.modifyMemberProfileKeys_.get(i);
        }

        public ModifyMemberProfileKeyActionOrBuilder getModifyMemberProfileKeysOrBuilder(int i) {
            return this.modifyMemberProfileKeys_.get(i);
        }

        private void ensureModifyMemberProfileKeysIsMutable() {
            Internal.ProtobufList<ModifyMemberProfileKeyAction> protobufList = this.modifyMemberProfileKeys_;
            if (!protobufList.isModifiable()) {
                this.modifyMemberProfileKeys_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setModifyMemberProfileKeys(int i, ModifyMemberProfileKeyAction modifyMemberProfileKeyAction) {
            modifyMemberProfileKeyAction.getClass();
            ensureModifyMemberProfileKeysIsMutable();
            this.modifyMemberProfileKeys_.set(i, modifyMemberProfileKeyAction);
        }

        public void addModifyMemberProfileKeys(ModifyMemberProfileKeyAction modifyMemberProfileKeyAction) {
            modifyMemberProfileKeyAction.getClass();
            ensureModifyMemberProfileKeysIsMutable();
            this.modifyMemberProfileKeys_.add(modifyMemberProfileKeyAction);
        }

        public void addModifyMemberProfileKeys(int i, ModifyMemberProfileKeyAction modifyMemberProfileKeyAction) {
            modifyMemberProfileKeyAction.getClass();
            ensureModifyMemberProfileKeysIsMutable();
            this.modifyMemberProfileKeys_.add(i, modifyMemberProfileKeyAction);
        }

        public void addAllModifyMemberProfileKeys(Iterable<? extends ModifyMemberProfileKeyAction> iterable) {
            ensureModifyMemberProfileKeysIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.modifyMemberProfileKeys_);
        }

        public void clearModifyMemberProfileKeys() {
            this.modifyMemberProfileKeys_ = GeneratedMessageLite.emptyProtobufList();
        }

        public void removeModifyMemberProfileKeys(int i) {
            ensureModifyMemberProfileKeysIsMutable();
            this.modifyMemberProfileKeys_.remove(i);
        }

        public List<AddPendingMemberAction> getAddPendingMembersList() {
            return this.addPendingMembers_;
        }

        public List<? extends AddPendingMemberActionOrBuilder> getAddPendingMembersOrBuilderList() {
            return this.addPendingMembers_;
        }

        public int getAddPendingMembersCount() {
            return this.addPendingMembers_.size();
        }

        public AddPendingMemberAction getAddPendingMembers(int i) {
            return this.addPendingMembers_.get(i);
        }

        public AddPendingMemberActionOrBuilder getAddPendingMembersOrBuilder(int i) {
            return this.addPendingMembers_.get(i);
        }

        private void ensureAddPendingMembersIsMutable() {
            Internal.ProtobufList<AddPendingMemberAction> protobufList = this.addPendingMembers_;
            if (!protobufList.isModifiable()) {
                this.addPendingMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setAddPendingMembers(int i, AddPendingMemberAction addPendingMemberAction) {
            addPendingMemberAction.getClass();
            ensureAddPendingMembersIsMutable();
            this.addPendingMembers_.set(i, addPendingMemberAction);
        }

        public void addAddPendingMembers(AddPendingMemberAction addPendingMemberAction) {
            addPendingMemberAction.getClass();
            ensureAddPendingMembersIsMutable();
            this.addPendingMembers_.add(addPendingMemberAction);
        }

        public void addAddPendingMembers(int i, AddPendingMemberAction addPendingMemberAction) {
            addPendingMemberAction.getClass();
            ensureAddPendingMembersIsMutable();
            this.addPendingMembers_.add(i, addPendingMemberAction);
        }

        public void addAllAddPendingMembers(Iterable<? extends AddPendingMemberAction> iterable) {
            ensureAddPendingMembersIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.addPendingMembers_);
        }

        public void clearAddPendingMembers() {
            this.addPendingMembers_ = GeneratedMessageLite.emptyProtobufList();
        }

        public void removeAddPendingMembers(int i) {
            ensureAddPendingMembersIsMutable();
            this.addPendingMembers_.remove(i);
        }

        public List<DeletePendingMemberAction> getDeletePendingMembersList() {
            return this.deletePendingMembers_;
        }

        public List<? extends DeletePendingMemberActionOrBuilder> getDeletePendingMembersOrBuilderList() {
            return this.deletePendingMembers_;
        }

        public int getDeletePendingMembersCount() {
            return this.deletePendingMembers_.size();
        }

        public DeletePendingMemberAction getDeletePendingMembers(int i) {
            return this.deletePendingMembers_.get(i);
        }

        public DeletePendingMemberActionOrBuilder getDeletePendingMembersOrBuilder(int i) {
            return this.deletePendingMembers_.get(i);
        }

        private void ensureDeletePendingMembersIsMutable() {
            Internal.ProtobufList<DeletePendingMemberAction> protobufList = this.deletePendingMembers_;
            if (!protobufList.isModifiable()) {
                this.deletePendingMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setDeletePendingMembers(int i, DeletePendingMemberAction deletePendingMemberAction) {
            deletePendingMemberAction.getClass();
            ensureDeletePendingMembersIsMutable();
            this.deletePendingMembers_.set(i, deletePendingMemberAction);
        }

        public void addDeletePendingMembers(DeletePendingMemberAction deletePendingMemberAction) {
            deletePendingMemberAction.getClass();
            ensureDeletePendingMembersIsMutable();
            this.deletePendingMembers_.add(deletePendingMemberAction);
        }

        public void addDeletePendingMembers(int i, DeletePendingMemberAction deletePendingMemberAction) {
            deletePendingMemberAction.getClass();
            ensureDeletePendingMembersIsMutable();
            this.deletePendingMembers_.add(i, deletePendingMemberAction);
        }

        public void addAllDeletePendingMembers(Iterable<? extends DeletePendingMemberAction> iterable) {
            ensureDeletePendingMembersIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.deletePendingMembers_);
        }

        public void clearDeletePendingMembers() {
            this.deletePendingMembers_ = GeneratedMessageLite.emptyProtobufList();
        }

        public void removeDeletePendingMembers(int i) {
            ensureDeletePendingMembersIsMutable();
            this.deletePendingMembers_.remove(i);
        }

        public List<PromotePendingMemberAction> getPromotePendingMembersList() {
            return this.promotePendingMembers_;
        }

        public List<? extends PromotePendingMemberActionOrBuilder> getPromotePendingMembersOrBuilderList() {
            return this.promotePendingMembers_;
        }

        public int getPromotePendingMembersCount() {
            return this.promotePendingMembers_.size();
        }

        public PromotePendingMemberAction getPromotePendingMembers(int i) {
            return this.promotePendingMembers_.get(i);
        }

        public PromotePendingMemberActionOrBuilder getPromotePendingMembersOrBuilder(int i) {
            return this.promotePendingMembers_.get(i);
        }

        private void ensurePromotePendingMembersIsMutable() {
            Internal.ProtobufList<PromotePendingMemberAction> protobufList = this.promotePendingMembers_;
            if (!protobufList.isModifiable()) {
                this.promotePendingMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setPromotePendingMembers(int i, PromotePendingMemberAction promotePendingMemberAction) {
            promotePendingMemberAction.getClass();
            ensurePromotePendingMembersIsMutable();
            this.promotePendingMembers_.set(i, promotePendingMemberAction);
        }

        public void addPromotePendingMembers(PromotePendingMemberAction promotePendingMemberAction) {
            promotePendingMemberAction.getClass();
            ensurePromotePendingMembersIsMutable();
            this.promotePendingMembers_.add(promotePendingMemberAction);
        }

        public void addPromotePendingMembers(int i, PromotePendingMemberAction promotePendingMemberAction) {
            promotePendingMemberAction.getClass();
            ensurePromotePendingMembersIsMutable();
            this.promotePendingMembers_.add(i, promotePendingMemberAction);
        }

        public void addAllPromotePendingMembers(Iterable<? extends PromotePendingMemberAction> iterable) {
            ensurePromotePendingMembersIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.promotePendingMembers_);
        }

        public void clearPromotePendingMembers() {
            this.promotePendingMembers_ = GeneratedMessageLite.emptyProtobufList();
        }

        public void removePromotePendingMembers(int i) {
            ensurePromotePendingMembersIsMutable();
            this.promotePendingMembers_.remove(i);
        }

        public boolean hasModifyTitle() {
            return this.modifyTitle_ != null;
        }

        public ModifyTitleAction getModifyTitle() {
            ModifyTitleAction modifyTitleAction = this.modifyTitle_;
            return modifyTitleAction == null ? ModifyTitleAction.getDefaultInstance() : modifyTitleAction;
        }

        public void setModifyTitle(ModifyTitleAction modifyTitleAction) {
            modifyTitleAction.getClass();
            this.modifyTitle_ = modifyTitleAction;
        }

        public void mergeModifyTitle(ModifyTitleAction modifyTitleAction) {
            modifyTitleAction.getClass();
            ModifyTitleAction modifyTitleAction2 = this.modifyTitle_;
            if (modifyTitleAction2 == null || modifyTitleAction2 == ModifyTitleAction.getDefaultInstance()) {
                this.modifyTitle_ = modifyTitleAction;
            } else {
                this.modifyTitle_ = ModifyTitleAction.newBuilder(this.modifyTitle_).mergeFrom((ModifyTitleAction.Builder) modifyTitleAction).buildPartial();
            }
        }

        public void clearModifyTitle() {
            this.modifyTitle_ = null;
        }

        public boolean hasModifyAvatar() {
            return this.modifyAvatar_ != null;
        }

        public ModifyAvatarAction getModifyAvatar() {
            ModifyAvatarAction modifyAvatarAction = this.modifyAvatar_;
            return modifyAvatarAction == null ? ModifyAvatarAction.getDefaultInstance() : modifyAvatarAction;
        }

        public void setModifyAvatar(ModifyAvatarAction modifyAvatarAction) {
            modifyAvatarAction.getClass();
            this.modifyAvatar_ = modifyAvatarAction;
        }

        public void mergeModifyAvatar(ModifyAvatarAction modifyAvatarAction) {
            modifyAvatarAction.getClass();
            ModifyAvatarAction modifyAvatarAction2 = this.modifyAvatar_;
            if (modifyAvatarAction2 == null || modifyAvatarAction2 == ModifyAvatarAction.getDefaultInstance()) {
                this.modifyAvatar_ = modifyAvatarAction;
            } else {
                this.modifyAvatar_ = ModifyAvatarAction.newBuilder(this.modifyAvatar_).mergeFrom((ModifyAvatarAction.Builder) modifyAvatarAction).buildPartial();
            }
        }

        public void clearModifyAvatar() {
            this.modifyAvatar_ = null;
        }

        public boolean hasModifyDisappearingMessagesTimer() {
            return this.modifyDisappearingMessagesTimer_ != null;
        }

        public ModifyDisappearingMessagesTimerAction getModifyDisappearingMessagesTimer() {
            ModifyDisappearingMessagesTimerAction modifyDisappearingMessagesTimerAction = this.modifyDisappearingMessagesTimer_;
            return modifyDisappearingMessagesTimerAction == null ? ModifyDisappearingMessagesTimerAction.getDefaultInstance() : modifyDisappearingMessagesTimerAction;
        }

        public void setModifyDisappearingMessagesTimer(ModifyDisappearingMessagesTimerAction modifyDisappearingMessagesTimerAction) {
            modifyDisappearingMessagesTimerAction.getClass();
            this.modifyDisappearingMessagesTimer_ = modifyDisappearingMessagesTimerAction;
        }

        public void mergeModifyDisappearingMessagesTimer(ModifyDisappearingMessagesTimerAction modifyDisappearingMessagesTimerAction) {
            modifyDisappearingMessagesTimerAction.getClass();
            ModifyDisappearingMessagesTimerAction modifyDisappearingMessagesTimerAction2 = this.modifyDisappearingMessagesTimer_;
            if (modifyDisappearingMessagesTimerAction2 == null || modifyDisappearingMessagesTimerAction2 == ModifyDisappearingMessagesTimerAction.getDefaultInstance()) {
                this.modifyDisappearingMessagesTimer_ = modifyDisappearingMessagesTimerAction;
            } else {
                this.modifyDisappearingMessagesTimer_ = ModifyDisappearingMessagesTimerAction.newBuilder(this.modifyDisappearingMessagesTimer_).mergeFrom((ModifyDisappearingMessagesTimerAction.Builder) modifyDisappearingMessagesTimerAction).buildPartial();
            }
        }

        public void clearModifyDisappearingMessagesTimer() {
            this.modifyDisappearingMessagesTimer_ = null;
        }

        public boolean hasModifyAttributesAccess() {
            return this.modifyAttributesAccess_ != null;
        }

        public ModifyAttributesAccessControlAction getModifyAttributesAccess() {
            ModifyAttributesAccessControlAction modifyAttributesAccessControlAction = this.modifyAttributesAccess_;
            return modifyAttributesAccessControlAction == null ? ModifyAttributesAccessControlAction.getDefaultInstance() : modifyAttributesAccessControlAction;
        }

        public void setModifyAttributesAccess(ModifyAttributesAccessControlAction modifyAttributesAccessControlAction) {
            modifyAttributesAccessControlAction.getClass();
            this.modifyAttributesAccess_ = modifyAttributesAccessControlAction;
        }

        public void mergeModifyAttributesAccess(ModifyAttributesAccessControlAction modifyAttributesAccessControlAction) {
            modifyAttributesAccessControlAction.getClass();
            ModifyAttributesAccessControlAction modifyAttributesAccessControlAction2 = this.modifyAttributesAccess_;
            if (modifyAttributesAccessControlAction2 == null || modifyAttributesAccessControlAction2 == ModifyAttributesAccessControlAction.getDefaultInstance()) {
                this.modifyAttributesAccess_ = modifyAttributesAccessControlAction;
            } else {
                this.modifyAttributesAccess_ = ModifyAttributesAccessControlAction.newBuilder(this.modifyAttributesAccess_).mergeFrom((ModifyAttributesAccessControlAction.Builder) modifyAttributesAccessControlAction).buildPartial();
            }
        }

        public void clearModifyAttributesAccess() {
            this.modifyAttributesAccess_ = null;
        }

        public boolean hasModifyMemberAccess() {
            return this.modifyMemberAccess_ != null;
        }

        public ModifyMembersAccessControlAction getModifyMemberAccess() {
            ModifyMembersAccessControlAction modifyMembersAccessControlAction = this.modifyMemberAccess_;
            return modifyMembersAccessControlAction == null ? ModifyMembersAccessControlAction.getDefaultInstance() : modifyMembersAccessControlAction;
        }

        public void setModifyMemberAccess(ModifyMembersAccessControlAction modifyMembersAccessControlAction) {
            modifyMembersAccessControlAction.getClass();
            this.modifyMemberAccess_ = modifyMembersAccessControlAction;
        }

        public void mergeModifyMemberAccess(ModifyMembersAccessControlAction modifyMembersAccessControlAction) {
            modifyMembersAccessControlAction.getClass();
            ModifyMembersAccessControlAction modifyMembersAccessControlAction2 = this.modifyMemberAccess_;
            if (modifyMembersAccessControlAction2 == null || modifyMembersAccessControlAction2 == ModifyMembersAccessControlAction.getDefaultInstance()) {
                this.modifyMemberAccess_ = modifyMembersAccessControlAction;
            } else {
                this.modifyMemberAccess_ = ModifyMembersAccessControlAction.newBuilder(this.modifyMemberAccess_).mergeFrom((ModifyMembersAccessControlAction.Builder) modifyMembersAccessControlAction).buildPartial();
            }
        }

        public void clearModifyMemberAccess() {
            this.modifyMemberAccess_ = null;
        }

        public boolean hasModifyAddFromInviteLinkAccess() {
            return this.modifyAddFromInviteLinkAccess_ != null;
        }

        public ModifyAddFromInviteLinkAccessControlAction getModifyAddFromInviteLinkAccess() {
            ModifyAddFromInviteLinkAccessControlAction modifyAddFromInviteLinkAccessControlAction = this.modifyAddFromInviteLinkAccess_;
            return modifyAddFromInviteLinkAccessControlAction == null ? ModifyAddFromInviteLinkAccessControlAction.getDefaultInstance() : modifyAddFromInviteLinkAccessControlAction;
        }

        public void setModifyAddFromInviteLinkAccess(ModifyAddFromInviteLinkAccessControlAction modifyAddFromInviteLinkAccessControlAction) {
            modifyAddFromInviteLinkAccessControlAction.getClass();
            this.modifyAddFromInviteLinkAccess_ = modifyAddFromInviteLinkAccessControlAction;
        }

        public void mergeModifyAddFromInviteLinkAccess(ModifyAddFromInviteLinkAccessControlAction modifyAddFromInviteLinkAccessControlAction) {
            modifyAddFromInviteLinkAccessControlAction.getClass();
            ModifyAddFromInviteLinkAccessControlAction modifyAddFromInviteLinkAccessControlAction2 = this.modifyAddFromInviteLinkAccess_;
            if (modifyAddFromInviteLinkAccessControlAction2 == null || modifyAddFromInviteLinkAccessControlAction2 == ModifyAddFromInviteLinkAccessControlAction.getDefaultInstance()) {
                this.modifyAddFromInviteLinkAccess_ = modifyAddFromInviteLinkAccessControlAction;
            } else {
                this.modifyAddFromInviteLinkAccess_ = ModifyAddFromInviteLinkAccessControlAction.newBuilder(this.modifyAddFromInviteLinkAccess_).mergeFrom((ModifyAddFromInviteLinkAccessControlAction.Builder) modifyAddFromInviteLinkAccessControlAction).buildPartial();
            }
        }

        public void clearModifyAddFromInviteLinkAccess() {
            this.modifyAddFromInviteLinkAccess_ = null;
        }

        public List<AddRequestingMemberAction> getAddRequestingMembersList() {
            return this.addRequestingMembers_;
        }

        public List<? extends AddRequestingMemberActionOrBuilder> getAddRequestingMembersOrBuilderList() {
            return this.addRequestingMembers_;
        }

        public int getAddRequestingMembersCount() {
            return this.addRequestingMembers_.size();
        }

        public AddRequestingMemberAction getAddRequestingMembers(int i) {
            return this.addRequestingMembers_.get(i);
        }

        public AddRequestingMemberActionOrBuilder getAddRequestingMembersOrBuilder(int i) {
            return this.addRequestingMembers_.get(i);
        }

        private void ensureAddRequestingMembersIsMutable() {
            Internal.ProtobufList<AddRequestingMemberAction> protobufList = this.addRequestingMembers_;
            if (!protobufList.isModifiable()) {
                this.addRequestingMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setAddRequestingMembers(int i, AddRequestingMemberAction addRequestingMemberAction) {
            addRequestingMemberAction.getClass();
            ensureAddRequestingMembersIsMutable();
            this.addRequestingMembers_.set(i, addRequestingMemberAction);
        }

        public void addAddRequestingMembers(AddRequestingMemberAction addRequestingMemberAction) {
            addRequestingMemberAction.getClass();
            ensureAddRequestingMembersIsMutable();
            this.addRequestingMembers_.add(addRequestingMemberAction);
        }

        public void addAddRequestingMembers(int i, AddRequestingMemberAction addRequestingMemberAction) {
            addRequestingMemberAction.getClass();
            ensureAddRequestingMembersIsMutable();
            this.addRequestingMembers_.add(i, addRequestingMemberAction);
        }

        public void addAllAddRequestingMembers(Iterable<? extends AddRequestingMemberAction> iterable) {
            ensureAddRequestingMembersIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.addRequestingMembers_);
        }

        public void clearAddRequestingMembers() {
            this.addRequestingMembers_ = GeneratedMessageLite.emptyProtobufList();
        }

        public void removeAddRequestingMembers(int i) {
            ensureAddRequestingMembersIsMutable();
            this.addRequestingMembers_.remove(i);
        }

        public List<DeleteRequestingMemberAction> getDeleteRequestingMembersList() {
            return this.deleteRequestingMembers_;
        }

        public List<? extends DeleteRequestingMemberActionOrBuilder> getDeleteRequestingMembersOrBuilderList() {
            return this.deleteRequestingMembers_;
        }

        public int getDeleteRequestingMembersCount() {
            return this.deleteRequestingMembers_.size();
        }

        public DeleteRequestingMemberAction getDeleteRequestingMembers(int i) {
            return this.deleteRequestingMembers_.get(i);
        }

        public DeleteRequestingMemberActionOrBuilder getDeleteRequestingMembersOrBuilder(int i) {
            return this.deleteRequestingMembers_.get(i);
        }

        private void ensureDeleteRequestingMembersIsMutable() {
            Internal.ProtobufList<DeleteRequestingMemberAction> protobufList = this.deleteRequestingMembers_;
            if (!protobufList.isModifiable()) {
                this.deleteRequestingMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setDeleteRequestingMembers(int i, DeleteRequestingMemberAction deleteRequestingMemberAction) {
            deleteRequestingMemberAction.getClass();
            ensureDeleteRequestingMembersIsMutable();
            this.deleteRequestingMembers_.set(i, deleteRequestingMemberAction);
        }

        public void addDeleteRequestingMembers(DeleteRequestingMemberAction deleteRequestingMemberAction) {
            deleteRequestingMemberAction.getClass();
            ensureDeleteRequestingMembersIsMutable();
            this.deleteRequestingMembers_.add(deleteRequestingMemberAction);
        }

        public void addDeleteRequestingMembers(int i, DeleteRequestingMemberAction deleteRequestingMemberAction) {
            deleteRequestingMemberAction.getClass();
            ensureDeleteRequestingMembersIsMutable();
            this.deleteRequestingMembers_.add(i, deleteRequestingMemberAction);
        }

        public void addAllDeleteRequestingMembers(Iterable<? extends DeleteRequestingMemberAction> iterable) {
            ensureDeleteRequestingMembersIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.deleteRequestingMembers_);
        }

        public void clearDeleteRequestingMembers() {
            this.deleteRequestingMembers_ = GeneratedMessageLite.emptyProtobufList();
        }

        public void removeDeleteRequestingMembers(int i) {
            ensureDeleteRequestingMembersIsMutable();
            this.deleteRequestingMembers_.remove(i);
        }

        public List<PromoteRequestingMemberAction> getPromoteRequestingMembersList() {
            return this.promoteRequestingMembers_;
        }

        public List<? extends PromoteRequestingMemberActionOrBuilder> getPromoteRequestingMembersOrBuilderList() {
            return this.promoteRequestingMembers_;
        }

        public int getPromoteRequestingMembersCount() {
            return this.promoteRequestingMembers_.size();
        }

        public PromoteRequestingMemberAction getPromoteRequestingMembers(int i) {
            return this.promoteRequestingMembers_.get(i);
        }

        public PromoteRequestingMemberActionOrBuilder getPromoteRequestingMembersOrBuilder(int i) {
            return this.promoteRequestingMembers_.get(i);
        }

        private void ensurePromoteRequestingMembersIsMutable() {
            Internal.ProtobufList<PromoteRequestingMemberAction> protobufList = this.promoteRequestingMembers_;
            if (!protobufList.isModifiable()) {
                this.promoteRequestingMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setPromoteRequestingMembers(int i, PromoteRequestingMemberAction promoteRequestingMemberAction) {
            promoteRequestingMemberAction.getClass();
            ensurePromoteRequestingMembersIsMutable();
            this.promoteRequestingMembers_.set(i, promoteRequestingMemberAction);
        }

        public void addPromoteRequestingMembers(PromoteRequestingMemberAction promoteRequestingMemberAction) {
            promoteRequestingMemberAction.getClass();
            ensurePromoteRequestingMembersIsMutable();
            this.promoteRequestingMembers_.add(promoteRequestingMemberAction);
        }

        public void addPromoteRequestingMembers(int i, PromoteRequestingMemberAction promoteRequestingMemberAction) {
            promoteRequestingMemberAction.getClass();
            ensurePromoteRequestingMembersIsMutable();
            this.promoteRequestingMembers_.add(i, promoteRequestingMemberAction);
        }

        public void addAllPromoteRequestingMembers(Iterable<? extends PromoteRequestingMemberAction> iterable) {
            ensurePromoteRequestingMembersIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.promoteRequestingMembers_);
        }

        public void clearPromoteRequestingMembers() {
            this.promoteRequestingMembers_ = GeneratedMessageLite.emptyProtobufList();
        }

        public void removePromoteRequestingMembers(int i) {
            ensurePromoteRequestingMembersIsMutable();
            this.promoteRequestingMembers_.remove(i);
        }

        public boolean hasModifyInviteLinkPassword() {
            return this.modifyInviteLinkPassword_ != null;
        }

        public ModifyInviteLinkPasswordAction getModifyInviteLinkPassword() {
            ModifyInviteLinkPasswordAction modifyInviteLinkPasswordAction = this.modifyInviteLinkPassword_;
            return modifyInviteLinkPasswordAction == null ? ModifyInviteLinkPasswordAction.getDefaultInstance() : modifyInviteLinkPasswordAction;
        }

        public void setModifyInviteLinkPassword(ModifyInviteLinkPasswordAction modifyInviteLinkPasswordAction) {
            modifyInviteLinkPasswordAction.getClass();
            this.modifyInviteLinkPassword_ = modifyInviteLinkPasswordAction;
        }

        public void mergeModifyInviteLinkPassword(ModifyInviteLinkPasswordAction modifyInviteLinkPasswordAction) {
            modifyInviteLinkPasswordAction.getClass();
            ModifyInviteLinkPasswordAction modifyInviteLinkPasswordAction2 = this.modifyInviteLinkPassword_;
            if (modifyInviteLinkPasswordAction2 == null || modifyInviteLinkPasswordAction2 == ModifyInviteLinkPasswordAction.getDefaultInstance()) {
                this.modifyInviteLinkPassword_ = modifyInviteLinkPasswordAction;
            } else {
                this.modifyInviteLinkPassword_ = ModifyInviteLinkPasswordAction.newBuilder(this.modifyInviteLinkPassword_).mergeFrom((ModifyInviteLinkPasswordAction.Builder) modifyInviteLinkPasswordAction).buildPartial();
            }
        }

        public void clearModifyInviteLinkPassword() {
            this.modifyInviteLinkPassword_ = null;
        }

        public boolean hasModifyDescription() {
            return this.modifyDescription_ != null;
        }

        public ModifyDescriptionAction getModifyDescription() {
            ModifyDescriptionAction modifyDescriptionAction = this.modifyDescription_;
            return modifyDescriptionAction == null ? ModifyDescriptionAction.getDefaultInstance() : modifyDescriptionAction;
        }

        public void setModifyDescription(ModifyDescriptionAction modifyDescriptionAction) {
            modifyDescriptionAction.getClass();
            this.modifyDescription_ = modifyDescriptionAction;
        }

        public void mergeModifyDescription(ModifyDescriptionAction modifyDescriptionAction) {
            modifyDescriptionAction.getClass();
            ModifyDescriptionAction modifyDescriptionAction2 = this.modifyDescription_;
            if (modifyDescriptionAction2 == null || modifyDescriptionAction2 == ModifyDescriptionAction.getDefaultInstance()) {
                this.modifyDescription_ = modifyDescriptionAction;
            } else {
                this.modifyDescription_ = ModifyDescriptionAction.newBuilder(this.modifyDescription_).mergeFrom((ModifyDescriptionAction.Builder) modifyDescriptionAction).buildPartial();
            }
        }

        public void clearModifyDescription() {
            this.modifyDescription_ = null;
        }

        public boolean hasModifyAnnouncementsOnly() {
            return this.modifyAnnouncementsOnly_ != null;
        }

        public ModifyAnnouncementsOnlyAction getModifyAnnouncementsOnly() {
            ModifyAnnouncementsOnlyAction modifyAnnouncementsOnlyAction = this.modifyAnnouncementsOnly_;
            return modifyAnnouncementsOnlyAction == null ? ModifyAnnouncementsOnlyAction.getDefaultInstance() : modifyAnnouncementsOnlyAction;
        }

        public void setModifyAnnouncementsOnly(ModifyAnnouncementsOnlyAction modifyAnnouncementsOnlyAction) {
            modifyAnnouncementsOnlyAction.getClass();
            this.modifyAnnouncementsOnly_ = modifyAnnouncementsOnlyAction;
        }

        public void mergeModifyAnnouncementsOnly(ModifyAnnouncementsOnlyAction modifyAnnouncementsOnlyAction) {
            modifyAnnouncementsOnlyAction.getClass();
            ModifyAnnouncementsOnlyAction modifyAnnouncementsOnlyAction2 = this.modifyAnnouncementsOnly_;
            if (modifyAnnouncementsOnlyAction2 == null || modifyAnnouncementsOnlyAction2 == ModifyAnnouncementsOnlyAction.getDefaultInstance()) {
                this.modifyAnnouncementsOnly_ = modifyAnnouncementsOnlyAction;
            } else {
                this.modifyAnnouncementsOnly_ = ModifyAnnouncementsOnlyAction.newBuilder(this.modifyAnnouncementsOnly_).mergeFrom((ModifyAnnouncementsOnlyAction.Builder) modifyAnnouncementsOnlyAction).buildPartial();
            }
        }

        public void clearModifyAnnouncementsOnly() {
            this.modifyAnnouncementsOnly_ = null;
        }

        public List<AddBannedMemberAction> getAddBannedMembersList() {
            return this.addBannedMembers_;
        }

        public List<? extends AddBannedMemberActionOrBuilder> getAddBannedMembersOrBuilderList() {
            return this.addBannedMembers_;
        }

        public int getAddBannedMembersCount() {
            return this.addBannedMembers_.size();
        }

        public AddBannedMemberAction getAddBannedMembers(int i) {
            return this.addBannedMembers_.get(i);
        }

        public AddBannedMemberActionOrBuilder getAddBannedMembersOrBuilder(int i) {
            return this.addBannedMembers_.get(i);
        }

        private void ensureAddBannedMembersIsMutable() {
            Internal.ProtobufList<AddBannedMemberAction> protobufList = this.addBannedMembers_;
            if (!protobufList.isModifiable()) {
                this.addBannedMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setAddBannedMembers(int i, AddBannedMemberAction addBannedMemberAction) {
            addBannedMemberAction.getClass();
            ensureAddBannedMembersIsMutable();
            this.addBannedMembers_.set(i, addBannedMemberAction);
        }

        public void addAddBannedMembers(AddBannedMemberAction addBannedMemberAction) {
            addBannedMemberAction.getClass();
            ensureAddBannedMembersIsMutable();
            this.addBannedMembers_.add(addBannedMemberAction);
        }

        public void addAddBannedMembers(int i, AddBannedMemberAction addBannedMemberAction) {
            addBannedMemberAction.getClass();
            ensureAddBannedMembersIsMutable();
            this.addBannedMembers_.add(i, addBannedMemberAction);
        }

        public void addAllAddBannedMembers(Iterable<? extends AddBannedMemberAction> iterable) {
            ensureAddBannedMembersIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.addBannedMembers_);
        }

        public void clearAddBannedMembers() {
            this.addBannedMembers_ = GeneratedMessageLite.emptyProtobufList();
        }

        public void removeAddBannedMembers(int i) {
            ensureAddBannedMembersIsMutable();
            this.addBannedMembers_.remove(i);
        }

        public List<DeleteBannedMemberAction> getDeleteBannedMembersList() {
            return this.deleteBannedMembers_;
        }

        public List<? extends DeleteBannedMemberActionOrBuilder> getDeleteBannedMembersOrBuilderList() {
            return this.deleteBannedMembers_;
        }

        public int getDeleteBannedMembersCount() {
            return this.deleteBannedMembers_.size();
        }

        public DeleteBannedMemberAction getDeleteBannedMembers(int i) {
            return this.deleteBannedMembers_.get(i);
        }

        public DeleteBannedMemberActionOrBuilder getDeleteBannedMembersOrBuilder(int i) {
            return this.deleteBannedMembers_.get(i);
        }

        private void ensureDeleteBannedMembersIsMutable() {
            Internal.ProtobufList<DeleteBannedMemberAction> protobufList = this.deleteBannedMembers_;
            if (!protobufList.isModifiable()) {
                this.deleteBannedMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setDeleteBannedMembers(int i, DeleteBannedMemberAction deleteBannedMemberAction) {
            deleteBannedMemberAction.getClass();
            ensureDeleteBannedMembersIsMutable();
            this.deleteBannedMembers_.set(i, deleteBannedMemberAction);
        }

        public void addDeleteBannedMembers(DeleteBannedMemberAction deleteBannedMemberAction) {
            deleteBannedMemberAction.getClass();
            ensureDeleteBannedMembersIsMutable();
            this.deleteBannedMembers_.add(deleteBannedMemberAction);
        }

        public void addDeleteBannedMembers(int i, DeleteBannedMemberAction deleteBannedMemberAction) {
            deleteBannedMemberAction.getClass();
            ensureDeleteBannedMembersIsMutable();
            this.deleteBannedMembers_.add(i, deleteBannedMemberAction);
        }

        public void addAllDeleteBannedMembers(Iterable<? extends DeleteBannedMemberAction> iterable) {
            ensureDeleteBannedMembersIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.deleteBannedMembers_);
        }

        public void clearDeleteBannedMembers() {
            this.deleteBannedMembers_ = GeneratedMessageLite.emptyProtobufList();
        }

        public void removeDeleteBannedMembers(int i) {
            ensureDeleteBannedMembersIsMutable();
            this.deleteBannedMembers_.remove(i);
        }

        public List<PromotePendingPniAciMemberProfileKeyAction> getPromotePendingPniAciMembersList() {
            return this.promotePendingPniAciMembers_;
        }

        public List<? extends PromotePendingPniAciMemberProfileKeyActionOrBuilder> getPromotePendingPniAciMembersOrBuilderList() {
            return this.promotePendingPniAciMembers_;
        }

        public int getPromotePendingPniAciMembersCount() {
            return this.promotePendingPniAciMembers_.size();
        }

        public PromotePendingPniAciMemberProfileKeyAction getPromotePendingPniAciMembers(int i) {
            return this.promotePendingPniAciMembers_.get(i);
        }

        public PromotePendingPniAciMemberProfileKeyActionOrBuilder getPromotePendingPniAciMembersOrBuilder(int i) {
            return this.promotePendingPniAciMembers_.get(i);
        }

        private void ensurePromotePendingPniAciMembersIsMutable() {
            Internal.ProtobufList<PromotePendingPniAciMemberProfileKeyAction> protobufList = this.promotePendingPniAciMembers_;
            if (!protobufList.isModifiable()) {
                this.promotePendingPniAciMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setPromotePendingPniAciMembers(int i, PromotePendingPniAciMemberProfileKeyAction promotePendingPniAciMemberProfileKeyAction) {
            promotePendingPniAciMemberProfileKeyAction.getClass();
            ensurePromotePendingPniAciMembersIsMutable();
            this.promotePendingPniAciMembers_.set(i, promotePendingPniAciMemberProfileKeyAction);
        }

        public void addPromotePendingPniAciMembers(PromotePendingPniAciMemberProfileKeyAction promotePendingPniAciMemberProfileKeyAction) {
            promotePendingPniAciMemberProfileKeyAction.getClass();
            ensurePromotePendingPniAciMembersIsMutable();
            this.promotePendingPniAciMembers_.add(promotePendingPniAciMemberProfileKeyAction);
        }

        public void addPromotePendingPniAciMembers(int i, PromotePendingPniAciMemberProfileKeyAction promotePendingPniAciMemberProfileKeyAction) {
            promotePendingPniAciMemberProfileKeyAction.getClass();
            ensurePromotePendingPniAciMembersIsMutable();
            this.promotePendingPniAciMembers_.add(i, promotePendingPniAciMemberProfileKeyAction);
        }

        public void addAllPromotePendingPniAciMembers(Iterable<? extends PromotePendingPniAciMemberProfileKeyAction> iterable) {
            ensurePromotePendingPniAciMembersIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.promotePendingPniAciMembers_);
        }

        public void clearPromotePendingPniAciMembers() {
            this.promotePendingPniAciMembers_ = GeneratedMessageLite.emptyProtobufList();
        }

        public void removePromotePendingPniAciMembers(int i) {
            ensurePromotePendingPniAciMembersIsMutable();
            this.promotePendingPniAciMembers_.remove(i);
        }

        public static Actions parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (Actions) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static Actions parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Actions) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static Actions parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (Actions) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static Actions parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Actions) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static Actions parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (Actions) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static Actions parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Actions) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static Actions parseFrom(InputStream inputStream) throws IOException {
            return (Actions) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Actions parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Actions) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Actions parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Actions) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Actions parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Actions) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Actions parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (Actions) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static Actions parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Actions) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(Actions actions) {
            return DEFAULT_INSTANCE.createBuilder(actions);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<Actions, Builder> implements MessageLiteOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(Actions.DEFAULT_INSTANCE);
            }

            public Builder setSourceUuid(ByteString byteString) {
                copyOnWrite();
                ((Actions) this.instance).setSourceUuid(byteString);
                return this;
            }

            public Builder setRevision(int i) {
                copyOnWrite();
                ((Actions) this.instance).setRevision(i);
                return this;
            }

            public List<AddMemberAction> getAddMembersList() {
                return Collections.unmodifiableList(((Actions) this.instance).getAddMembersList());
            }

            public int getAddMembersCount() {
                return ((Actions) this.instance).getAddMembersCount();
            }

            public AddMemberAction getAddMembers(int i) {
                return ((Actions) this.instance).getAddMembers(i);
            }

            public Builder setAddMembers(int i, AddMemberAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).setAddMembers(i, builder.build());
                return this;
            }

            public Builder addAddMembers(AddMemberAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).addAddMembers(builder.build());
                return this;
            }

            public Builder removeAddMembers(int i) {
                copyOnWrite();
                ((Actions) this.instance).removeAddMembers(i);
                return this;
            }

            public Builder addDeleteMembers(DeleteMemberAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).addDeleteMembers(builder.build());
                return this;
            }

            public Builder removeDeleteMembers(int i) {
                copyOnWrite();
                ((Actions) this.instance).removeDeleteMembers(i);
                return this;
            }

            public Builder addModifyMemberRoles(ModifyMemberRoleAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).addModifyMemberRoles(builder.build());
                return this;
            }

            public Builder removeModifyMemberRoles(int i) {
                copyOnWrite();
                ((Actions) this.instance).removeModifyMemberRoles(i);
                return this;
            }

            public Builder addModifyMemberProfileKeys(ModifyMemberProfileKeyAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).addModifyMemberProfileKeys(builder.build());
                return this;
            }

            public Builder removeModifyMemberProfileKeys(int i) {
                copyOnWrite();
                ((Actions) this.instance).removeModifyMemberProfileKeys(i);
                return this;
            }

            public Builder addAddPendingMembers(AddPendingMemberAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).addAddPendingMembers(builder.build());
                return this;
            }

            public Builder removeAddPendingMembers(int i) {
                copyOnWrite();
                ((Actions) this.instance).removeAddPendingMembers(i);
                return this;
            }

            public Builder addDeletePendingMembers(DeletePendingMemberAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).addDeletePendingMembers(builder.build());
                return this;
            }

            public Builder removeDeletePendingMembers(int i) {
                copyOnWrite();
                ((Actions) this.instance).removeDeletePendingMembers(i);
                return this;
            }

            public Builder addPromotePendingMembers(PromotePendingMemberAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).addPromotePendingMembers(builder.build());
                return this;
            }

            public Builder addPromotePendingMembers(int i, PromotePendingMemberAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).addPromotePendingMembers(i, builder.build());
                return this;
            }

            public Builder removePromotePendingMembers(int i) {
                copyOnWrite();
                ((Actions) this.instance).removePromotePendingMembers(i);
                return this;
            }

            public Builder setModifyTitle(ModifyTitleAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).setModifyTitle(builder.build());
                return this;
            }

            public Builder clearModifyTitle() {
                copyOnWrite();
                ((Actions) this.instance).clearModifyTitle();
                return this;
            }

            public Builder setModifyAvatar(ModifyAvatarAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).setModifyAvatar(builder.build());
                return this;
            }

            public Builder clearModifyAvatar() {
                copyOnWrite();
                ((Actions) this.instance).clearModifyAvatar();
                return this;
            }

            public Builder setModifyDisappearingMessagesTimer(ModifyDisappearingMessagesTimerAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).setModifyDisappearingMessagesTimer(builder.build());
                return this;
            }

            public Builder clearModifyDisappearingMessagesTimer() {
                copyOnWrite();
                ((Actions) this.instance).clearModifyDisappearingMessagesTimer();
                return this;
            }

            public Builder setModifyAttributesAccess(ModifyAttributesAccessControlAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).setModifyAttributesAccess(builder.build());
                return this;
            }

            public Builder clearModifyAttributesAccess() {
                copyOnWrite();
                ((Actions) this.instance).clearModifyAttributesAccess();
                return this;
            }

            public Builder setModifyMemberAccess(ModifyMembersAccessControlAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).setModifyMemberAccess(builder.build());
                return this;
            }

            public Builder clearModifyMemberAccess() {
                copyOnWrite();
                ((Actions) this.instance).clearModifyMemberAccess();
                return this;
            }

            public Builder setModifyAddFromInviteLinkAccess(ModifyAddFromInviteLinkAccessControlAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).setModifyAddFromInviteLinkAccess(builder.build());
                return this;
            }

            public Builder clearModifyAddFromInviteLinkAccess() {
                copyOnWrite();
                ((Actions) this.instance).clearModifyAddFromInviteLinkAccess();
                return this;
            }

            public List<AddRequestingMemberAction> getAddRequestingMembersList() {
                return Collections.unmodifiableList(((Actions) this.instance).getAddRequestingMembersList());
            }

            public Builder addAddRequestingMembers(AddRequestingMemberAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).addAddRequestingMembers(builder.build());
                return this;
            }

            public Builder removeAddRequestingMembers(int i) {
                copyOnWrite();
                ((Actions) this.instance).removeAddRequestingMembers(i);
                return this;
            }

            public Builder addDeleteRequestingMembers(DeleteRequestingMemberAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).addDeleteRequestingMembers(builder.build());
                return this;
            }

            public Builder removeDeleteRequestingMembers(int i) {
                copyOnWrite();
                ((Actions) this.instance).removeDeleteRequestingMembers(i);
                return this;
            }

            public Builder addPromoteRequestingMembers(PromoteRequestingMemberAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).addPromoteRequestingMembers(builder.build());
                return this;
            }

            public Builder removePromoteRequestingMembers(int i) {
                copyOnWrite();
                ((Actions) this.instance).removePromoteRequestingMembers(i);
                return this;
            }

            public Builder setModifyInviteLinkPassword(ModifyInviteLinkPasswordAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).setModifyInviteLinkPassword(builder.build());
                return this;
            }

            public Builder setModifyDescription(ModifyDescriptionAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).setModifyDescription(builder.build());
                return this;
            }

            public Builder clearModifyDescription() {
                copyOnWrite();
                ((Actions) this.instance).clearModifyDescription();
                return this;
            }

            public Builder setModifyAnnouncementsOnly(ModifyAnnouncementsOnlyAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).setModifyAnnouncementsOnly(builder.build());
                return this;
            }

            public Builder clearModifyAnnouncementsOnly() {
                copyOnWrite();
                ((Actions) this.instance).clearModifyAnnouncementsOnly();
                return this;
            }

            public Builder addAddBannedMembers(AddBannedMemberAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).addAddBannedMembers(builder.build());
                return this;
            }

            public Builder removeAddBannedMembers(int i) {
                copyOnWrite();
                ((Actions) this.instance).removeAddBannedMembers(i);
                return this;
            }

            public Builder addDeleteBannedMembers(DeleteBannedMemberAction deleteBannedMemberAction) {
                copyOnWrite();
                ((Actions) this.instance).addDeleteBannedMembers(deleteBannedMemberAction);
                return this;
            }

            public Builder addDeleteBannedMembers(DeleteBannedMemberAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).addDeleteBannedMembers(builder.build());
                return this;
            }

            public Builder removeDeleteBannedMembers(int i) {
                copyOnWrite();
                ((Actions) this.instance).removeDeleteBannedMembers(i);
                return this;
            }

            public Builder addPromotePendingPniAciMembers(PromotePendingPniAciMemberProfileKeyAction.Builder builder) {
                copyOnWrite();
                ((Actions) this.instance).addPromotePendingPniAciMembers(builder.build());
                return this;
            }

            public Builder removePromotePendingPniAciMembers(int i) {
                copyOnWrite();
                ((Actions) this.instance).removePromotePendingPniAciMembers(i);
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new Actions();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0018\u0000\u0000\u0001\u0018\u0018\u0000\r\u0000\u0001\n\u0002\u000b\u0003\u001b\u0004\u001b\u0005\u001b\u0006\u001b\u0007\u001b\b\u001b\t\u001b\n\t\u000b\t\f\t\r\t\u000e\t\u000f\t\u0010\u001b\u0011\u001b\u0012\u001b\u0013\t\u0014\t\u0015\t\u0016\u001b\u0017\u001b\u0018\u001b", new Object[]{"sourceUuid_", "revision_", "addMembers_", AddMemberAction.class, "deleteMembers_", DeleteMemberAction.class, "modifyMemberRoles_", ModifyMemberRoleAction.class, "modifyMemberProfileKeys_", ModifyMemberProfileKeyAction.class, "addPendingMembers_", AddPendingMemberAction.class, "deletePendingMembers_", DeletePendingMemberAction.class, "promotePendingMembers_", PromotePendingMemberAction.class, "modifyTitle_", "modifyAvatar_", "modifyDisappearingMessagesTimer_", "modifyAttributesAccess_", "modifyMemberAccess_", "modifyAddFromInviteLinkAccess_", "addRequestingMembers_", AddRequestingMemberAction.class, "deleteRequestingMembers_", DeleteRequestingMemberAction.class, "promoteRequestingMembers_", PromoteRequestingMemberAction.class, "modifyInviteLinkPassword_", "modifyDescription_", "modifyAnnouncementsOnly_", "addBannedMembers_", AddBannedMemberAction.class, "deleteBannedMembers_", DeleteBannedMemberAction.class, "promotePendingPniAciMembers_", PromotePendingPniAciMemberProfileKeyAction.class});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<Actions> parser = PARSER;
                    if (parser == null) {
                        synchronized (Actions.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            Actions actions = new Actions();
            DEFAULT_INSTANCE = actions;
            GeneratedMessageLite.registerDefaultInstance(Actions.class, actions);
        }

        public static Actions getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<Actions> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* renamed from: org.signal.storageservice.protos.groups.GroupChange$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    public ByteString getActions() {
        return this.actions_;
    }

    public void setActions(ByteString byteString) {
        byteString.getClass();
        this.actions_ = byteString;
    }

    public void clearActions() {
        this.actions_ = getDefaultInstance().getActions();
    }

    public ByteString getServerSignature() {
        return this.serverSignature_;
    }

    public void setServerSignature(ByteString byteString) {
        byteString.getClass();
        this.serverSignature_ = byteString;
    }

    public void clearServerSignature() {
        this.serverSignature_ = getDefaultInstance().getServerSignature();
    }

    public int getChangeEpoch() {
        return this.changeEpoch_;
    }

    public void setChangeEpoch(int i) {
        this.changeEpoch_ = i;
    }

    public void clearChangeEpoch() {
        this.changeEpoch_ = 0;
    }

    public static GroupChange parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (GroupChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static GroupChange parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (GroupChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static GroupChange parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (GroupChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static GroupChange parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (GroupChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static GroupChange parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (GroupChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static GroupChange parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (GroupChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static GroupChange parseFrom(InputStream inputStream) throws IOException {
        return (GroupChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static GroupChange parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (GroupChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static GroupChange parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (GroupChange) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static GroupChange parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (GroupChange) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static GroupChange parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (GroupChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static GroupChange parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (GroupChange) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(GroupChange groupChange) {
        return DEFAULT_INSTANCE.createBuilder(groupChange);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<GroupChange, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(GroupChange.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new GroupChange();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\n\u0002\n\u0003\u000b", new Object[]{"actions_", "serverSignature_", "changeEpoch_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<GroupChange> parser = PARSER;
                if (parser == null) {
                    synchronized (GroupChange.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        GroupChange groupChange = new GroupChange();
        DEFAULT_INSTANCE = groupChange;
        GeneratedMessageLite.registerDefaultInstance(GroupChange.class, groupChange);
    }

    public static GroupChange getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<GroupChange> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
