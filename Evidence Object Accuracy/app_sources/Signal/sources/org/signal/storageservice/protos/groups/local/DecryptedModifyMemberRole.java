package org.signal.storageservice.protos.groups.local;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import org.signal.storageservice.protos.groups.Member;

/* loaded from: classes3.dex */
public final class DecryptedModifyMemberRole extends GeneratedMessageLite<DecryptedModifyMemberRole, Builder> implements DecryptedModifyMemberRoleOrBuilder {
    private static final DecryptedModifyMemberRole DEFAULT_INSTANCE;
    private static volatile Parser<DecryptedModifyMemberRole> PARSER;
    public static final int ROLE_FIELD_NUMBER;
    public static final int UUID_FIELD_NUMBER;
    private int role_;
    private ByteString uuid_ = ByteString.EMPTY;

    private DecryptedModifyMemberRole() {
    }

    public ByteString getUuid() {
        return this.uuid_;
    }

    public void setUuid(ByteString byteString) {
        byteString.getClass();
        this.uuid_ = byteString;
    }

    public void clearUuid() {
        this.uuid_ = getDefaultInstance().getUuid();
    }

    public int getRoleValue() {
        return this.role_;
    }

    public Member.Role getRole() {
        Member.Role forNumber = Member.Role.forNumber(this.role_);
        return forNumber == null ? Member.Role.UNRECOGNIZED : forNumber;
    }

    public void setRoleValue(int i) {
        this.role_ = i;
    }

    public void setRole(Member.Role role) {
        this.role_ = role.getNumber();
    }

    public void clearRole() {
        this.role_ = 0;
    }

    public static DecryptedModifyMemberRole parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (DecryptedModifyMemberRole) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static DecryptedModifyMemberRole parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedModifyMemberRole) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static DecryptedModifyMemberRole parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (DecryptedModifyMemberRole) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static DecryptedModifyMemberRole parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedModifyMemberRole) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static DecryptedModifyMemberRole parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (DecryptedModifyMemberRole) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static DecryptedModifyMemberRole parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedModifyMemberRole) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static DecryptedModifyMemberRole parseFrom(InputStream inputStream) throws IOException {
        return (DecryptedModifyMemberRole) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static DecryptedModifyMemberRole parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedModifyMemberRole) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static DecryptedModifyMemberRole parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (DecryptedModifyMemberRole) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static DecryptedModifyMemberRole parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedModifyMemberRole) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static DecryptedModifyMemberRole parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (DecryptedModifyMemberRole) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static DecryptedModifyMemberRole parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedModifyMemberRole) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(DecryptedModifyMemberRole decryptedModifyMemberRole) {
        return DEFAULT_INSTANCE.createBuilder(decryptedModifyMemberRole);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<DecryptedModifyMemberRole, Builder> implements DecryptedModifyMemberRoleOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(DecryptedModifyMemberRole.DEFAULT_INSTANCE);
        }

        public Builder setUuid(ByteString byteString) {
            copyOnWrite();
            ((DecryptedModifyMemberRole) this.instance).setUuid(byteString);
            return this;
        }

        public Builder setRole(Member.Role role) {
            copyOnWrite();
            ((DecryptedModifyMemberRole) this.instance).setRole(role);
            return this;
        }
    }

    /* renamed from: org.signal.storageservice.protos.groups.local.DecryptedModifyMemberRole$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new DecryptedModifyMemberRole();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\n\u0002\f", new Object[]{"uuid_", "role_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<DecryptedModifyMemberRole> parser = PARSER;
                if (parser == null) {
                    synchronized (DecryptedModifyMemberRole.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        DecryptedModifyMemberRole decryptedModifyMemberRole = new DecryptedModifyMemberRole();
        DEFAULT_INSTANCE = decryptedModifyMemberRole;
        GeneratedMessageLite.registerDefaultInstance(DecryptedModifyMemberRole.class, decryptedModifyMemberRole);
    }

    public static DecryptedModifyMemberRole getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<DecryptedModifyMemberRole> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
