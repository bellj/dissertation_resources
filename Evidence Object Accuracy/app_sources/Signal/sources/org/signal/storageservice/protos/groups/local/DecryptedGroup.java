package org.signal.storageservice.protos.groups.local;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;
import org.signal.storageservice.protos.groups.AccessControl;
import org.signal.storageservice.protos.groups.local.DecryptedMember;
import org.signal.storageservice.protos.groups.local.DecryptedRequestingMember;
import org.signal.storageservice.protos.groups.local.DecryptedTimer;

/* loaded from: classes3.dex */
public final class DecryptedGroup extends GeneratedMessageLite<DecryptedGroup, Builder> implements MessageLiteOrBuilder {
    public static final int ACCESSCONTROL_FIELD_NUMBER;
    public static final int AVATAR_FIELD_NUMBER;
    public static final int BANNEDMEMBERS_FIELD_NUMBER;
    private static final DecryptedGroup DEFAULT_INSTANCE;
    public static final int DESCRIPTION_FIELD_NUMBER;
    public static final int DISAPPEARINGMESSAGESTIMER_FIELD_NUMBER;
    public static final int INVITELINKPASSWORD_FIELD_NUMBER;
    public static final int ISANNOUNCEMENTGROUP_FIELD_NUMBER;
    public static final int MEMBERS_FIELD_NUMBER;
    private static volatile Parser<DecryptedGroup> PARSER;
    public static final int PENDINGMEMBERS_FIELD_NUMBER;
    public static final int REQUESTINGMEMBERS_FIELD_NUMBER;
    public static final int REVISION_FIELD_NUMBER;
    public static final int TITLE_FIELD_NUMBER;
    private AccessControl accessControl_;
    private String avatar_ = "";
    private Internal.ProtobufList<DecryptedBannedMember> bannedMembers_ = GeneratedMessageLite.emptyProtobufList();
    private String description_ = "";
    private DecryptedTimer disappearingMessagesTimer_;
    private ByteString inviteLinkPassword_ = ByteString.EMPTY;
    private int isAnnouncementGroup_;
    private Internal.ProtobufList<DecryptedMember> members_ = GeneratedMessageLite.emptyProtobufList();
    private Internal.ProtobufList<DecryptedPendingMember> pendingMembers_ = GeneratedMessageLite.emptyProtobufList();
    private Internal.ProtobufList<DecryptedRequestingMember> requestingMembers_ = GeneratedMessageLite.emptyProtobufList();
    private int revision_;
    private String title_ = "";

    private DecryptedGroup() {
    }

    public String getTitle() {
        return this.title_;
    }

    public ByteString getTitleBytes() {
        return ByteString.copyFromUtf8(this.title_);
    }

    public void setTitle(String str) {
        str.getClass();
        this.title_ = str;
    }

    public void clearTitle() {
        this.title_ = getDefaultInstance().getTitle();
    }

    public void setTitleBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.title_ = byteString.toStringUtf8();
    }

    public String getAvatar() {
        return this.avatar_;
    }

    public ByteString getAvatarBytes() {
        return ByteString.copyFromUtf8(this.avatar_);
    }

    public void setAvatar(String str) {
        str.getClass();
        this.avatar_ = str;
    }

    public void clearAvatar() {
        this.avatar_ = getDefaultInstance().getAvatar();
    }

    public void setAvatarBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.avatar_ = byteString.toStringUtf8();
    }

    public boolean hasDisappearingMessagesTimer() {
        return this.disappearingMessagesTimer_ != null;
    }

    public DecryptedTimer getDisappearingMessagesTimer() {
        DecryptedTimer decryptedTimer = this.disappearingMessagesTimer_;
        return decryptedTimer == null ? DecryptedTimer.getDefaultInstance() : decryptedTimer;
    }

    public void setDisappearingMessagesTimer(DecryptedTimer decryptedTimer) {
        decryptedTimer.getClass();
        this.disappearingMessagesTimer_ = decryptedTimer;
    }

    public void mergeDisappearingMessagesTimer(DecryptedTimer decryptedTimer) {
        decryptedTimer.getClass();
        DecryptedTimer decryptedTimer2 = this.disappearingMessagesTimer_;
        if (decryptedTimer2 == null || decryptedTimer2 == DecryptedTimer.getDefaultInstance()) {
            this.disappearingMessagesTimer_ = decryptedTimer;
        } else {
            this.disappearingMessagesTimer_ = DecryptedTimer.newBuilder(this.disappearingMessagesTimer_).mergeFrom((DecryptedTimer.Builder) decryptedTimer).buildPartial();
        }
    }

    public void clearDisappearingMessagesTimer() {
        this.disappearingMessagesTimer_ = null;
    }

    public boolean hasAccessControl() {
        return this.accessControl_ != null;
    }

    public AccessControl getAccessControl() {
        AccessControl accessControl = this.accessControl_;
        return accessControl == null ? AccessControl.getDefaultInstance() : accessControl;
    }

    public void setAccessControl(AccessControl accessControl) {
        accessControl.getClass();
        this.accessControl_ = accessControl;
    }

    public void mergeAccessControl(AccessControl accessControl) {
        accessControl.getClass();
        AccessControl accessControl2 = this.accessControl_;
        if (accessControl2 == null || accessControl2 == AccessControl.getDefaultInstance()) {
            this.accessControl_ = accessControl;
        } else {
            this.accessControl_ = AccessControl.newBuilder(this.accessControl_).mergeFrom((AccessControl.Builder) accessControl).buildPartial();
        }
    }

    public void clearAccessControl() {
        this.accessControl_ = null;
    }

    public int getRevision() {
        return this.revision_;
    }

    public void setRevision(int i) {
        this.revision_ = i;
    }

    public void clearRevision() {
        this.revision_ = 0;
    }

    public List<DecryptedMember> getMembersList() {
        return this.members_;
    }

    public List<? extends DecryptedMemberOrBuilder> getMembersOrBuilderList() {
        return this.members_;
    }

    public int getMembersCount() {
        return this.members_.size();
    }

    public DecryptedMember getMembers(int i) {
        return this.members_.get(i);
    }

    public DecryptedMemberOrBuilder getMembersOrBuilder(int i) {
        return this.members_.get(i);
    }

    private void ensureMembersIsMutable() {
        Internal.ProtobufList<DecryptedMember> protobufList = this.members_;
        if (!protobufList.isModifiable()) {
            this.members_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setMembers(int i, DecryptedMember decryptedMember) {
        decryptedMember.getClass();
        ensureMembersIsMutable();
        this.members_.set(i, decryptedMember);
    }

    public void addMembers(DecryptedMember decryptedMember) {
        decryptedMember.getClass();
        ensureMembersIsMutable();
        this.members_.add(decryptedMember);
    }

    public void addMembers(int i, DecryptedMember decryptedMember) {
        decryptedMember.getClass();
        ensureMembersIsMutable();
        this.members_.add(i, decryptedMember);
    }

    public void addAllMembers(Iterable<? extends DecryptedMember> iterable) {
        ensureMembersIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.members_);
    }

    public void clearMembers() {
        this.members_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeMembers(int i) {
        ensureMembersIsMutable();
        this.members_.remove(i);
    }

    public List<DecryptedPendingMember> getPendingMembersList() {
        return this.pendingMembers_;
    }

    public List<? extends DecryptedPendingMemberOrBuilder> getPendingMembersOrBuilderList() {
        return this.pendingMembers_;
    }

    public int getPendingMembersCount() {
        return this.pendingMembers_.size();
    }

    public DecryptedPendingMember getPendingMembers(int i) {
        return this.pendingMembers_.get(i);
    }

    public DecryptedPendingMemberOrBuilder getPendingMembersOrBuilder(int i) {
        return this.pendingMembers_.get(i);
    }

    private void ensurePendingMembersIsMutable() {
        Internal.ProtobufList<DecryptedPendingMember> protobufList = this.pendingMembers_;
        if (!protobufList.isModifiable()) {
            this.pendingMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setPendingMembers(int i, DecryptedPendingMember decryptedPendingMember) {
        decryptedPendingMember.getClass();
        ensurePendingMembersIsMutable();
        this.pendingMembers_.set(i, decryptedPendingMember);
    }

    public void addPendingMembers(DecryptedPendingMember decryptedPendingMember) {
        decryptedPendingMember.getClass();
        ensurePendingMembersIsMutable();
        this.pendingMembers_.add(decryptedPendingMember);
    }

    public void addPendingMembers(int i, DecryptedPendingMember decryptedPendingMember) {
        decryptedPendingMember.getClass();
        ensurePendingMembersIsMutable();
        this.pendingMembers_.add(i, decryptedPendingMember);
    }

    public void addAllPendingMembers(Iterable<? extends DecryptedPendingMember> iterable) {
        ensurePendingMembersIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.pendingMembers_);
    }

    public void clearPendingMembers() {
        this.pendingMembers_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removePendingMembers(int i) {
        ensurePendingMembersIsMutable();
        this.pendingMembers_.remove(i);
    }

    public List<DecryptedRequestingMember> getRequestingMembersList() {
        return this.requestingMembers_;
    }

    public List<? extends DecryptedRequestingMemberOrBuilder> getRequestingMembersOrBuilderList() {
        return this.requestingMembers_;
    }

    public int getRequestingMembersCount() {
        return this.requestingMembers_.size();
    }

    public DecryptedRequestingMember getRequestingMembers(int i) {
        return this.requestingMembers_.get(i);
    }

    public DecryptedRequestingMemberOrBuilder getRequestingMembersOrBuilder(int i) {
        return this.requestingMembers_.get(i);
    }

    private void ensureRequestingMembersIsMutable() {
        Internal.ProtobufList<DecryptedRequestingMember> protobufList = this.requestingMembers_;
        if (!protobufList.isModifiable()) {
            this.requestingMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setRequestingMembers(int i, DecryptedRequestingMember decryptedRequestingMember) {
        decryptedRequestingMember.getClass();
        ensureRequestingMembersIsMutable();
        this.requestingMembers_.set(i, decryptedRequestingMember);
    }

    public void addRequestingMembers(DecryptedRequestingMember decryptedRequestingMember) {
        decryptedRequestingMember.getClass();
        ensureRequestingMembersIsMutable();
        this.requestingMembers_.add(decryptedRequestingMember);
    }

    public void addRequestingMembers(int i, DecryptedRequestingMember decryptedRequestingMember) {
        decryptedRequestingMember.getClass();
        ensureRequestingMembersIsMutable();
        this.requestingMembers_.add(i, decryptedRequestingMember);
    }

    public void addAllRequestingMembers(Iterable<? extends DecryptedRequestingMember> iterable) {
        ensureRequestingMembersIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.requestingMembers_);
    }

    public void clearRequestingMembers() {
        this.requestingMembers_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeRequestingMembers(int i) {
        ensureRequestingMembersIsMutable();
        this.requestingMembers_.remove(i);
    }

    public ByteString getInviteLinkPassword() {
        return this.inviteLinkPassword_;
    }

    public void setInviteLinkPassword(ByteString byteString) {
        byteString.getClass();
        this.inviteLinkPassword_ = byteString;
    }

    public void clearInviteLinkPassword() {
        this.inviteLinkPassword_ = getDefaultInstance().getInviteLinkPassword();
    }

    public String getDescription() {
        return this.description_;
    }

    public ByteString getDescriptionBytes() {
        return ByteString.copyFromUtf8(this.description_);
    }

    public void setDescription(String str) {
        str.getClass();
        this.description_ = str;
    }

    public void clearDescription() {
        this.description_ = getDefaultInstance().getDescription();
    }

    public void setDescriptionBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.description_ = byteString.toStringUtf8();
    }

    public int getIsAnnouncementGroupValue() {
        return this.isAnnouncementGroup_;
    }

    public EnabledState getIsAnnouncementGroup() {
        EnabledState forNumber = EnabledState.forNumber(this.isAnnouncementGroup_);
        return forNumber == null ? EnabledState.UNRECOGNIZED : forNumber;
    }

    public void setIsAnnouncementGroupValue(int i) {
        this.isAnnouncementGroup_ = i;
    }

    public void setIsAnnouncementGroup(EnabledState enabledState) {
        this.isAnnouncementGroup_ = enabledState.getNumber();
    }

    public void clearIsAnnouncementGroup() {
        this.isAnnouncementGroup_ = 0;
    }

    public List<DecryptedBannedMember> getBannedMembersList() {
        return this.bannedMembers_;
    }

    public List<? extends DecryptedBannedMemberOrBuilder> getBannedMembersOrBuilderList() {
        return this.bannedMembers_;
    }

    public int getBannedMembersCount() {
        return this.bannedMembers_.size();
    }

    public DecryptedBannedMember getBannedMembers(int i) {
        return this.bannedMembers_.get(i);
    }

    public DecryptedBannedMemberOrBuilder getBannedMembersOrBuilder(int i) {
        return this.bannedMembers_.get(i);
    }

    private void ensureBannedMembersIsMutable() {
        Internal.ProtobufList<DecryptedBannedMember> protobufList = this.bannedMembers_;
        if (!protobufList.isModifiable()) {
            this.bannedMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setBannedMembers(int i, DecryptedBannedMember decryptedBannedMember) {
        decryptedBannedMember.getClass();
        ensureBannedMembersIsMutable();
        this.bannedMembers_.set(i, decryptedBannedMember);
    }

    public void addBannedMembers(DecryptedBannedMember decryptedBannedMember) {
        decryptedBannedMember.getClass();
        ensureBannedMembersIsMutable();
        this.bannedMembers_.add(decryptedBannedMember);
    }

    public void addBannedMembers(int i, DecryptedBannedMember decryptedBannedMember) {
        decryptedBannedMember.getClass();
        ensureBannedMembersIsMutable();
        this.bannedMembers_.add(i, decryptedBannedMember);
    }

    public void addAllBannedMembers(Iterable<? extends DecryptedBannedMember> iterable) {
        ensureBannedMembersIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.bannedMembers_);
    }

    public void clearBannedMembers() {
        this.bannedMembers_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeBannedMembers(int i) {
        ensureBannedMembersIsMutable();
        this.bannedMembers_.remove(i);
    }

    public static DecryptedGroup parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (DecryptedGroup) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static DecryptedGroup parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedGroup) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static DecryptedGroup parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (DecryptedGroup) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static DecryptedGroup parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedGroup) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static DecryptedGroup parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (DecryptedGroup) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static DecryptedGroup parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedGroup) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static DecryptedGroup parseFrom(InputStream inputStream) throws IOException {
        return (DecryptedGroup) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static DecryptedGroup parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedGroup) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static DecryptedGroup parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (DecryptedGroup) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static DecryptedGroup parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedGroup) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static DecryptedGroup parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (DecryptedGroup) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static DecryptedGroup parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedGroup) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(DecryptedGroup decryptedGroup) {
        return DEFAULT_INSTANCE.createBuilder(decryptedGroup);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<DecryptedGroup, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(DecryptedGroup.DEFAULT_INSTANCE);
        }

        public Builder setTitle(String str) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).setTitle(str);
            return this;
        }

        public Builder setAvatar(String str) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).setAvatar(str);
            return this;
        }

        public Builder setDisappearingMessagesTimer(DecryptedTimer decryptedTimer) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).setDisappearingMessagesTimer(decryptedTimer);
            return this;
        }

        public Builder setDisappearingMessagesTimer(DecryptedTimer.Builder builder) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).setDisappearingMessagesTimer(builder.build());
            return this;
        }

        public AccessControl getAccessControl() {
            return ((DecryptedGroup) this.instance).getAccessControl();
        }

        public Builder setAccessControl(AccessControl accessControl) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).setAccessControl(accessControl);
            return this;
        }

        public Builder setAccessControl(AccessControl.Builder builder) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).setAccessControl(builder.build());
            return this;
        }

        public Builder setRevision(int i) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).setRevision(i);
            return this;
        }

        public List<DecryptedMember> getMembersList() {
            return Collections.unmodifiableList(((DecryptedGroup) this.instance).getMembersList());
        }

        public DecryptedMember getMembers(int i) {
            return ((DecryptedGroup) this.instance).getMembers(i);
        }

        public Builder setMembers(int i, DecryptedMember decryptedMember) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).setMembers(i, decryptedMember);
            return this;
        }

        public Builder setMembers(int i, DecryptedMember.Builder builder) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).setMembers(i, builder.build());
            return this;
        }

        public Builder addMembers(DecryptedMember decryptedMember) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).addMembers(decryptedMember);
            return this;
        }

        public Builder addMembers(DecryptedMember.Builder builder) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).addMembers(builder.build());
            return this;
        }

        public Builder addAllMembers(Iterable<? extends DecryptedMember> iterable) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).addAllMembers(iterable);
            return this;
        }

        public Builder clearMembers() {
            copyOnWrite();
            ((DecryptedGroup) this.instance).clearMembers();
            return this;
        }

        public Builder removeMembers(int i) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).removeMembers(i);
            return this;
        }

        public List<DecryptedPendingMember> getPendingMembersList() {
            return Collections.unmodifiableList(((DecryptedGroup) this.instance).getPendingMembersList());
        }

        public int getPendingMembersCount() {
            return ((DecryptedGroup) this.instance).getPendingMembersCount();
        }

        public DecryptedPendingMember getPendingMembers(int i) {
            return ((DecryptedGroup) this.instance).getPendingMembers(i);
        }

        public Builder addPendingMembers(DecryptedPendingMember decryptedPendingMember) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).addPendingMembers(decryptedPendingMember);
            return this;
        }

        public Builder addAllPendingMembers(Iterable<? extends DecryptedPendingMember> iterable) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).addAllPendingMembers(iterable);
            return this;
        }

        public Builder removePendingMembers(int i) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).removePendingMembers(i);
            return this;
        }

        public List<DecryptedRequestingMember> getRequestingMembersList() {
            return Collections.unmodifiableList(((DecryptedGroup) this.instance).getRequestingMembersList());
        }

        public int getRequestingMembersCount() {
            return ((DecryptedGroup) this.instance).getRequestingMembersCount();
        }

        public DecryptedRequestingMember getRequestingMembers(int i) {
            return ((DecryptedGroup) this.instance).getRequestingMembers(i);
        }

        public Builder addRequestingMembers(DecryptedRequestingMember.Builder builder) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).addRequestingMembers(builder.build());
            return this;
        }

        public Builder addAllRequestingMembers(Iterable<? extends DecryptedRequestingMember> iterable) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).addAllRequestingMembers(iterable);
            return this;
        }

        public Builder removeRequestingMembers(int i) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).removeRequestingMembers(i);
            return this;
        }

        public Builder setInviteLinkPassword(ByteString byteString) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).setInviteLinkPassword(byteString);
            return this;
        }

        public Builder setDescription(String str) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).setDescription(str);
            return this;
        }

        public Builder setIsAnnouncementGroup(EnabledState enabledState) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).setIsAnnouncementGroup(enabledState);
            return this;
        }

        public List<DecryptedBannedMember> getBannedMembersList() {
            return Collections.unmodifiableList(((DecryptedGroup) this.instance).getBannedMembersList());
        }

        public Builder addBannedMembers(DecryptedBannedMember decryptedBannedMember) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).addBannedMembers(decryptedBannedMember);
            return this;
        }

        public Builder addAllBannedMembers(Iterable<? extends DecryptedBannedMember> iterable) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).addAllBannedMembers(iterable);
            return this;
        }

        public Builder removeBannedMembers(int i) {
            copyOnWrite();
            ((DecryptedGroup) this.instance).removeBannedMembers(i);
            return this;
        }
    }

    /* renamed from: org.signal.storageservice.protos.groups.local.DecryptedGroup$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new DecryptedGroup();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\f\u0000\u0000\u0002\r\f\u0000\u0004\u0000\u0002Ȉ\u0003Ȉ\u0004\t\u0005\t\u0006\u000b\u0007\u001b\b\u001b\t\u001b\n\n\u000bȈ\f\f\r\u001b", new Object[]{"title_", "avatar_", "disappearingMessagesTimer_", "accessControl_", "revision_", "members_", DecryptedMember.class, "pendingMembers_", DecryptedPendingMember.class, "requestingMembers_", DecryptedRequestingMember.class, "inviteLinkPassword_", "description_", "isAnnouncementGroup_", "bannedMembers_", DecryptedBannedMember.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<DecryptedGroup> parser = PARSER;
                if (parser == null) {
                    synchronized (DecryptedGroup.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        DecryptedGroup decryptedGroup = new DecryptedGroup();
        DEFAULT_INSTANCE = decryptedGroup;
        GeneratedMessageLite.registerDefaultInstance(DecryptedGroup.class, decryptedGroup);
    }

    public static DecryptedGroup getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<DecryptedGroup> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
