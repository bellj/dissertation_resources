package org.signal.storageservice.protos.groups.local;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import org.signal.storageservice.protos.groups.Member;

/* loaded from: classes3.dex */
public final class DecryptedPendingMember extends GeneratedMessageLite<DecryptedPendingMember, Builder> implements DecryptedPendingMemberOrBuilder {
    public static final int ADDEDBYUUID_FIELD_NUMBER;
    private static final DecryptedPendingMember DEFAULT_INSTANCE;
    private static volatile Parser<DecryptedPendingMember> PARSER;
    public static final int ROLE_FIELD_NUMBER;
    public static final int TIMESTAMP_FIELD_NUMBER;
    public static final int UUIDCIPHERTEXT_FIELD_NUMBER;
    public static final int UUID_FIELD_NUMBER;
    private ByteString addedByUuid_;
    private int role_;
    private long timestamp_;
    private ByteString uuidCipherText_;
    private ByteString uuid_;

    private DecryptedPendingMember() {
        ByteString byteString = ByteString.EMPTY;
        this.uuid_ = byteString;
        this.addedByUuid_ = byteString;
        this.uuidCipherText_ = byteString;
    }

    public ByteString getUuid() {
        return this.uuid_;
    }

    public void setUuid(ByteString byteString) {
        byteString.getClass();
        this.uuid_ = byteString;
    }

    public void clearUuid() {
        this.uuid_ = getDefaultInstance().getUuid();
    }

    public int getRoleValue() {
        return this.role_;
    }

    public Member.Role getRole() {
        Member.Role forNumber = Member.Role.forNumber(this.role_);
        return forNumber == null ? Member.Role.UNRECOGNIZED : forNumber;
    }

    public void setRoleValue(int i) {
        this.role_ = i;
    }

    public void setRole(Member.Role role) {
        this.role_ = role.getNumber();
    }

    public void clearRole() {
        this.role_ = 0;
    }

    public ByteString getAddedByUuid() {
        return this.addedByUuid_;
    }

    public void setAddedByUuid(ByteString byteString) {
        byteString.getClass();
        this.addedByUuid_ = byteString;
    }

    public void clearAddedByUuid() {
        this.addedByUuid_ = getDefaultInstance().getAddedByUuid();
    }

    public long getTimestamp() {
        return this.timestamp_;
    }

    public void setTimestamp(long j) {
        this.timestamp_ = j;
    }

    public void clearTimestamp() {
        this.timestamp_ = 0;
    }

    public ByteString getUuidCipherText() {
        return this.uuidCipherText_;
    }

    public void setUuidCipherText(ByteString byteString) {
        byteString.getClass();
        this.uuidCipherText_ = byteString;
    }

    public void clearUuidCipherText() {
        this.uuidCipherText_ = getDefaultInstance().getUuidCipherText();
    }

    public static DecryptedPendingMember parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (DecryptedPendingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static DecryptedPendingMember parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedPendingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static DecryptedPendingMember parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (DecryptedPendingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static DecryptedPendingMember parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedPendingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static DecryptedPendingMember parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (DecryptedPendingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static DecryptedPendingMember parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (DecryptedPendingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static DecryptedPendingMember parseFrom(InputStream inputStream) throws IOException {
        return (DecryptedPendingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static DecryptedPendingMember parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedPendingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static DecryptedPendingMember parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (DecryptedPendingMember) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static DecryptedPendingMember parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedPendingMember) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static DecryptedPendingMember parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (DecryptedPendingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static DecryptedPendingMember parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (DecryptedPendingMember) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(DecryptedPendingMember decryptedPendingMember) {
        return DEFAULT_INSTANCE.createBuilder(decryptedPendingMember);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<DecryptedPendingMember, Builder> implements DecryptedPendingMemberOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(DecryptedPendingMember.DEFAULT_INSTANCE);
        }

        public Builder setUuid(ByteString byteString) {
            copyOnWrite();
            ((DecryptedPendingMember) this.instance).setUuid(byteString);
            return this;
        }

        public Builder setRole(Member.Role role) {
            copyOnWrite();
            ((DecryptedPendingMember) this.instance).setRole(role);
            return this;
        }

        public Builder setAddedByUuid(ByteString byteString) {
            copyOnWrite();
            ((DecryptedPendingMember) this.instance).setAddedByUuid(byteString);
            return this;
        }

        public Builder setTimestamp(long j) {
            copyOnWrite();
            ((DecryptedPendingMember) this.instance).setTimestamp(j);
            return this;
        }

        public Builder setUuidCipherText(ByteString byteString) {
            copyOnWrite();
            ((DecryptedPendingMember) this.instance).setUuidCipherText(byteString);
            return this;
        }
    }

    /* renamed from: org.signal.storageservice.protos.groups.local.DecryptedPendingMember$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new DecryptedPendingMember();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0005\u0000\u0000\u0001\u0005\u0005\u0000\u0000\u0000\u0001\n\u0002\f\u0003\n\u0004\u0003\u0005\n", new Object[]{"uuid_", "role_", "addedByUuid_", "timestamp_", "uuidCipherText_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<DecryptedPendingMember> parser = PARSER;
                if (parser == null) {
                    synchronized (DecryptedPendingMember.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        DecryptedPendingMember decryptedPendingMember = new DecryptedPendingMember();
        DEFAULT_INSTANCE = decryptedPendingMember;
        GeneratedMessageLite.registerDefaultInstance(DecryptedPendingMember.class, decryptedPendingMember);
    }

    public static DecryptedPendingMember getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<DecryptedPendingMember> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
