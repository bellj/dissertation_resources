package org.signal.storageservice.protos.groups;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;
import org.signal.storageservice.protos.groups.Group;
import org.signal.storageservice.protos.groups.GroupChange;

/* loaded from: classes3.dex */
public final class GroupChanges extends GeneratedMessageLite<GroupChanges, Builder> implements MessageLiteOrBuilder {
    private static final GroupChanges DEFAULT_INSTANCE;
    public static final int GROUPCHANGES_FIELD_NUMBER;
    private static volatile Parser<GroupChanges> PARSER;
    private Internal.ProtobufList<GroupChangeState> groupChanges_ = GeneratedMessageLite.emptyProtobufList();

    /* loaded from: classes3.dex */
    public interface GroupChangeStateOrBuilder extends MessageLiteOrBuilder {
    }

    private GroupChanges() {
    }

    /* loaded from: classes3.dex */
    public static final class GroupChangeState extends GeneratedMessageLite<GroupChangeState, Builder> implements GroupChangeStateOrBuilder {
        private static final GroupChangeState DEFAULT_INSTANCE;
        public static final int GROUPCHANGE_FIELD_NUMBER;
        public static final int GROUPSTATE_FIELD_NUMBER;
        private static volatile Parser<GroupChangeState> PARSER;
        private GroupChange groupChange_;
        private Group groupState_;

        private GroupChangeState() {
        }

        public boolean hasGroupChange() {
            return this.groupChange_ != null;
        }

        public GroupChange getGroupChange() {
            GroupChange groupChange = this.groupChange_;
            return groupChange == null ? GroupChange.getDefaultInstance() : groupChange;
        }

        public void setGroupChange(GroupChange groupChange) {
            groupChange.getClass();
            this.groupChange_ = groupChange;
        }

        public void mergeGroupChange(GroupChange groupChange) {
            groupChange.getClass();
            GroupChange groupChange2 = this.groupChange_;
            if (groupChange2 == null || groupChange2 == GroupChange.getDefaultInstance()) {
                this.groupChange_ = groupChange;
            } else {
                this.groupChange_ = GroupChange.newBuilder(this.groupChange_).mergeFrom((GroupChange.Builder) groupChange).buildPartial();
            }
        }

        public void clearGroupChange() {
            this.groupChange_ = null;
        }

        public boolean hasGroupState() {
            return this.groupState_ != null;
        }

        public Group getGroupState() {
            Group group = this.groupState_;
            return group == null ? Group.getDefaultInstance() : group;
        }

        public void setGroupState(Group group) {
            group.getClass();
            this.groupState_ = group;
        }

        public void mergeGroupState(Group group) {
            group.getClass();
            Group group2 = this.groupState_;
            if (group2 == null || group2 == Group.getDefaultInstance()) {
                this.groupState_ = group;
            } else {
                this.groupState_ = Group.newBuilder(this.groupState_).mergeFrom((Group.Builder) group).buildPartial();
            }
        }

        public void clearGroupState() {
            this.groupState_ = null;
        }

        public static GroupChangeState parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (GroupChangeState) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static GroupChangeState parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (GroupChangeState) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static GroupChangeState parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (GroupChangeState) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static GroupChangeState parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (GroupChangeState) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static GroupChangeState parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (GroupChangeState) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static GroupChangeState parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (GroupChangeState) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static GroupChangeState parseFrom(InputStream inputStream) throws IOException {
            return (GroupChangeState) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static GroupChangeState parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (GroupChangeState) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static GroupChangeState parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (GroupChangeState) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static GroupChangeState parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (GroupChangeState) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static GroupChangeState parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (GroupChangeState) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static GroupChangeState parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (GroupChangeState) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(GroupChangeState groupChangeState) {
            return DEFAULT_INSTANCE.createBuilder(groupChangeState);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<GroupChangeState, Builder> implements GroupChangeStateOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(GroupChangeState.DEFAULT_INSTANCE);
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new GroupChangeState();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\t\u0002\t", new Object[]{"groupChange_", "groupState_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<GroupChangeState> parser = PARSER;
                    if (parser == null) {
                        synchronized (GroupChangeState.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            GroupChangeState groupChangeState = new GroupChangeState();
            DEFAULT_INSTANCE = groupChangeState;
            GeneratedMessageLite.registerDefaultInstance(GroupChangeState.class, groupChangeState);
        }

        public static GroupChangeState getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<GroupChangeState> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* renamed from: org.signal.storageservice.protos.groups.GroupChanges$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    public List<GroupChangeState> getGroupChangesList() {
        return this.groupChanges_;
    }

    public List<? extends GroupChangeStateOrBuilder> getGroupChangesOrBuilderList() {
        return this.groupChanges_;
    }

    public int getGroupChangesCount() {
        return this.groupChanges_.size();
    }

    public GroupChangeState getGroupChanges(int i) {
        return this.groupChanges_.get(i);
    }

    public GroupChangeStateOrBuilder getGroupChangesOrBuilder(int i) {
        return this.groupChanges_.get(i);
    }

    private void ensureGroupChangesIsMutable() {
        Internal.ProtobufList<GroupChangeState> protobufList = this.groupChanges_;
        if (!protobufList.isModifiable()) {
            this.groupChanges_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setGroupChanges(int i, GroupChangeState groupChangeState) {
        groupChangeState.getClass();
        ensureGroupChangesIsMutable();
        this.groupChanges_.set(i, groupChangeState);
    }

    public void addGroupChanges(GroupChangeState groupChangeState) {
        groupChangeState.getClass();
        ensureGroupChangesIsMutable();
        this.groupChanges_.add(groupChangeState);
    }

    public void addGroupChanges(int i, GroupChangeState groupChangeState) {
        groupChangeState.getClass();
        ensureGroupChangesIsMutable();
        this.groupChanges_.add(i, groupChangeState);
    }

    public void addAllGroupChanges(Iterable<? extends GroupChangeState> iterable) {
        ensureGroupChangesIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.groupChanges_);
    }

    public void clearGroupChanges() {
        this.groupChanges_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeGroupChanges(int i) {
        ensureGroupChangesIsMutable();
        this.groupChanges_.remove(i);
    }

    public static GroupChanges parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (GroupChanges) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static GroupChanges parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (GroupChanges) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static GroupChanges parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (GroupChanges) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static GroupChanges parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (GroupChanges) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static GroupChanges parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (GroupChanges) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static GroupChanges parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (GroupChanges) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static GroupChanges parseFrom(InputStream inputStream) throws IOException {
        return (GroupChanges) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static GroupChanges parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (GroupChanges) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static GroupChanges parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (GroupChanges) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static GroupChanges parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (GroupChanges) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static GroupChanges parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (GroupChanges) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static GroupChanges parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (GroupChanges) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(GroupChanges groupChanges) {
        return DEFAULT_INSTANCE.createBuilder(groupChanges);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<GroupChanges, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(GroupChanges.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new GroupChanges();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"groupChanges_", GroupChangeState.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<GroupChanges> parser = PARSER;
                if (parser == null) {
                    synchronized (GroupChanges.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        GroupChanges groupChanges = new GroupChanges();
        DEFAULT_INSTANCE = groupChanges;
        GeneratedMessageLite.registerDefaultInstance(GroupChanges.class, groupChanges);
    }

    public static GroupChanges getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<GroupChanges> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
