package org.signal.storageservice.protos.groups;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;
import org.signal.storageservice.protos.groups.AccessControl;
import org.signal.storageservice.protos.groups.Member;
import org.signal.storageservice.protos.groups.PendingMember;

/* loaded from: classes3.dex */
public final class Group extends GeneratedMessageLite<Group, Builder> implements MessageLiteOrBuilder {
    public static final int ACCESSCONTROL_FIELD_NUMBER;
    public static final int ANNOUNCEMENTSONLY_FIELD_NUMBER;
    public static final int AVATAR_FIELD_NUMBER;
    public static final int BANNEDMEMBERS_FIELD_NUMBER;
    private static final Group DEFAULT_INSTANCE;
    public static final int DESCRIPTION_FIELD_NUMBER;
    public static final int DISAPPEARINGMESSAGESTIMER_FIELD_NUMBER;
    public static final int INVITELINKPASSWORD_FIELD_NUMBER;
    public static final int MEMBERS_FIELD_NUMBER;
    private static volatile Parser<Group> PARSER;
    public static final int PENDINGMEMBERS_FIELD_NUMBER;
    public static final int PUBLICKEY_FIELD_NUMBER;
    public static final int REQUESTINGMEMBERS_FIELD_NUMBER;
    public static final int REVISION_FIELD_NUMBER;
    public static final int TITLE_FIELD_NUMBER;
    private AccessControl accessControl_;
    private boolean announcementsOnly_;
    private String avatar_ = "";
    private Internal.ProtobufList<BannedMember> bannedMembers_;
    private ByteString description_;
    private ByteString disappearingMessagesTimer_;
    private ByteString inviteLinkPassword_;
    private Internal.ProtobufList<Member> members_;
    private Internal.ProtobufList<PendingMember> pendingMembers_;
    private ByteString publicKey_;
    private Internal.ProtobufList<RequestingMember> requestingMembers_;
    private int revision_;
    private ByteString title_;

    private Group() {
        ByteString byteString = ByteString.EMPTY;
        this.publicKey_ = byteString;
        this.title_ = byteString;
        this.disappearingMessagesTimer_ = byteString;
        this.members_ = GeneratedMessageLite.emptyProtobufList();
        this.pendingMembers_ = GeneratedMessageLite.emptyProtobufList();
        this.requestingMembers_ = GeneratedMessageLite.emptyProtobufList();
        this.inviteLinkPassword_ = byteString;
        this.description_ = byteString;
        this.bannedMembers_ = GeneratedMessageLite.emptyProtobufList();
    }

    public ByteString getPublicKey() {
        return this.publicKey_;
    }

    public void setPublicKey(ByteString byteString) {
        byteString.getClass();
        this.publicKey_ = byteString;
    }

    public void clearPublicKey() {
        this.publicKey_ = getDefaultInstance().getPublicKey();
    }

    public ByteString getTitle() {
        return this.title_;
    }

    public void setTitle(ByteString byteString) {
        byteString.getClass();
        this.title_ = byteString;
    }

    public void clearTitle() {
        this.title_ = getDefaultInstance().getTitle();
    }

    public String getAvatar() {
        return this.avatar_;
    }

    public ByteString getAvatarBytes() {
        return ByteString.copyFromUtf8(this.avatar_);
    }

    public void setAvatar(String str) {
        str.getClass();
        this.avatar_ = str;
    }

    public void clearAvatar() {
        this.avatar_ = getDefaultInstance().getAvatar();
    }

    public void setAvatarBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.avatar_ = byteString.toStringUtf8();
    }

    public ByteString getDisappearingMessagesTimer() {
        return this.disappearingMessagesTimer_;
    }

    public void setDisappearingMessagesTimer(ByteString byteString) {
        byteString.getClass();
        this.disappearingMessagesTimer_ = byteString;
    }

    public void clearDisappearingMessagesTimer() {
        this.disappearingMessagesTimer_ = getDefaultInstance().getDisappearingMessagesTimer();
    }

    public boolean hasAccessControl() {
        return this.accessControl_ != null;
    }

    public AccessControl getAccessControl() {
        AccessControl accessControl = this.accessControl_;
        return accessControl == null ? AccessControl.getDefaultInstance() : accessControl;
    }

    public void setAccessControl(AccessControl accessControl) {
        accessControl.getClass();
        this.accessControl_ = accessControl;
    }

    public void mergeAccessControl(AccessControl accessControl) {
        accessControl.getClass();
        AccessControl accessControl2 = this.accessControl_;
        if (accessControl2 == null || accessControl2 == AccessControl.getDefaultInstance()) {
            this.accessControl_ = accessControl;
        } else {
            this.accessControl_ = AccessControl.newBuilder(this.accessControl_).mergeFrom((AccessControl.Builder) accessControl).buildPartial();
        }
    }

    public void clearAccessControl() {
        this.accessControl_ = null;
    }

    public int getRevision() {
        return this.revision_;
    }

    public void setRevision(int i) {
        this.revision_ = i;
    }

    public void clearRevision() {
        this.revision_ = 0;
    }

    public List<Member> getMembersList() {
        return this.members_;
    }

    public List<? extends MemberOrBuilder> getMembersOrBuilderList() {
        return this.members_;
    }

    public int getMembersCount() {
        return this.members_.size();
    }

    public Member getMembers(int i) {
        return this.members_.get(i);
    }

    public MemberOrBuilder getMembersOrBuilder(int i) {
        return this.members_.get(i);
    }

    private void ensureMembersIsMutable() {
        Internal.ProtobufList<Member> protobufList = this.members_;
        if (!protobufList.isModifiable()) {
            this.members_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setMembers(int i, Member member) {
        member.getClass();
        ensureMembersIsMutable();
        this.members_.set(i, member);
    }

    public void addMembers(Member member) {
        member.getClass();
        ensureMembersIsMutable();
        this.members_.add(member);
    }

    public void addMembers(int i, Member member) {
        member.getClass();
        ensureMembersIsMutable();
        this.members_.add(i, member);
    }

    public void addAllMembers(Iterable<? extends Member> iterable) {
        ensureMembersIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.members_);
    }

    public void clearMembers() {
        this.members_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeMembers(int i) {
        ensureMembersIsMutable();
        this.members_.remove(i);
    }

    public List<PendingMember> getPendingMembersList() {
        return this.pendingMembers_;
    }

    public List<? extends PendingMemberOrBuilder> getPendingMembersOrBuilderList() {
        return this.pendingMembers_;
    }

    public int getPendingMembersCount() {
        return this.pendingMembers_.size();
    }

    public PendingMember getPendingMembers(int i) {
        return this.pendingMembers_.get(i);
    }

    public PendingMemberOrBuilder getPendingMembersOrBuilder(int i) {
        return this.pendingMembers_.get(i);
    }

    private void ensurePendingMembersIsMutable() {
        Internal.ProtobufList<PendingMember> protobufList = this.pendingMembers_;
        if (!protobufList.isModifiable()) {
            this.pendingMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setPendingMembers(int i, PendingMember pendingMember) {
        pendingMember.getClass();
        ensurePendingMembersIsMutable();
        this.pendingMembers_.set(i, pendingMember);
    }

    public void addPendingMembers(PendingMember pendingMember) {
        pendingMember.getClass();
        ensurePendingMembersIsMutable();
        this.pendingMembers_.add(pendingMember);
    }

    public void addPendingMembers(int i, PendingMember pendingMember) {
        pendingMember.getClass();
        ensurePendingMembersIsMutable();
        this.pendingMembers_.add(i, pendingMember);
    }

    public void addAllPendingMembers(Iterable<? extends PendingMember> iterable) {
        ensurePendingMembersIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.pendingMembers_);
    }

    public void clearPendingMembers() {
        this.pendingMembers_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removePendingMembers(int i) {
        ensurePendingMembersIsMutable();
        this.pendingMembers_.remove(i);
    }

    public List<RequestingMember> getRequestingMembersList() {
        return this.requestingMembers_;
    }

    public List<? extends RequestingMemberOrBuilder> getRequestingMembersOrBuilderList() {
        return this.requestingMembers_;
    }

    public int getRequestingMembersCount() {
        return this.requestingMembers_.size();
    }

    public RequestingMember getRequestingMembers(int i) {
        return this.requestingMembers_.get(i);
    }

    public RequestingMemberOrBuilder getRequestingMembersOrBuilder(int i) {
        return this.requestingMembers_.get(i);
    }

    private void ensureRequestingMembersIsMutable() {
        Internal.ProtobufList<RequestingMember> protobufList = this.requestingMembers_;
        if (!protobufList.isModifiable()) {
            this.requestingMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setRequestingMembers(int i, RequestingMember requestingMember) {
        requestingMember.getClass();
        ensureRequestingMembersIsMutable();
        this.requestingMembers_.set(i, requestingMember);
    }

    public void addRequestingMembers(RequestingMember requestingMember) {
        requestingMember.getClass();
        ensureRequestingMembersIsMutable();
        this.requestingMembers_.add(requestingMember);
    }

    public void addRequestingMembers(int i, RequestingMember requestingMember) {
        requestingMember.getClass();
        ensureRequestingMembersIsMutable();
        this.requestingMembers_.add(i, requestingMember);
    }

    public void addAllRequestingMembers(Iterable<? extends RequestingMember> iterable) {
        ensureRequestingMembersIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.requestingMembers_);
    }

    public void clearRequestingMembers() {
        this.requestingMembers_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeRequestingMembers(int i) {
        ensureRequestingMembersIsMutable();
        this.requestingMembers_.remove(i);
    }

    public ByteString getInviteLinkPassword() {
        return this.inviteLinkPassword_;
    }

    public void setInviteLinkPassword(ByteString byteString) {
        byteString.getClass();
        this.inviteLinkPassword_ = byteString;
    }

    public void clearInviteLinkPassword() {
        this.inviteLinkPassword_ = getDefaultInstance().getInviteLinkPassword();
    }

    public ByteString getDescription() {
        return this.description_;
    }

    public void setDescription(ByteString byteString) {
        byteString.getClass();
        this.description_ = byteString;
    }

    public void clearDescription() {
        this.description_ = getDefaultInstance().getDescription();
    }

    public boolean getAnnouncementsOnly() {
        return this.announcementsOnly_;
    }

    public void setAnnouncementsOnly(boolean z) {
        this.announcementsOnly_ = z;
    }

    public void clearAnnouncementsOnly() {
        this.announcementsOnly_ = false;
    }

    public List<BannedMember> getBannedMembersList() {
        return this.bannedMembers_;
    }

    public List<? extends BannedMemberOrBuilder> getBannedMembersOrBuilderList() {
        return this.bannedMembers_;
    }

    public int getBannedMembersCount() {
        return this.bannedMembers_.size();
    }

    public BannedMember getBannedMembers(int i) {
        return this.bannedMembers_.get(i);
    }

    public BannedMemberOrBuilder getBannedMembersOrBuilder(int i) {
        return this.bannedMembers_.get(i);
    }

    private void ensureBannedMembersIsMutable() {
        Internal.ProtobufList<BannedMember> protobufList = this.bannedMembers_;
        if (!protobufList.isModifiable()) {
            this.bannedMembers_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setBannedMembers(int i, BannedMember bannedMember) {
        bannedMember.getClass();
        ensureBannedMembersIsMutable();
        this.bannedMembers_.set(i, bannedMember);
    }

    public void addBannedMembers(BannedMember bannedMember) {
        bannedMember.getClass();
        ensureBannedMembersIsMutable();
        this.bannedMembers_.add(bannedMember);
    }

    public void addBannedMembers(int i, BannedMember bannedMember) {
        bannedMember.getClass();
        ensureBannedMembersIsMutable();
        this.bannedMembers_.add(i, bannedMember);
    }

    public void addAllBannedMembers(Iterable<? extends BannedMember> iterable) {
        ensureBannedMembersIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.bannedMembers_);
    }

    public void clearBannedMembers() {
        this.bannedMembers_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeBannedMembers(int i) {
        ensureBannedMembersIsMutable();
        this.bannedMembers_.remove(i);
    }

    public static Group parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (Group) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static Group parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Group) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static Group parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (Group) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static Group parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Group) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static Group parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (Group) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static Group parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (Group) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static Group parseFrom(InputStream inputStream) throws IOException {
        return (Group) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Group parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Group) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Group parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (Group) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static Group parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Group) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static Group parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (Group) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static Group parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (Group) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(Group group) {
        return DEFAULT_INSTANCE.createBuilder(group);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<Group, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(Group.DEFAULT_INSTANCE);
        }

        public Builder setPublicKey(ByteString byteString) {
            copyOnWrite();
            ((Group) this.instance).setPublicKey(byteString);
            return this;
        }

        public Builder setTitle(ByteString byteString) {
            copyOnWrite();
            ((Group) this.instance).setTitle(byteString);
            return this;
        }

        public Builder setAvatar(String str) {
            copyOnWrite();
            ((Group) this.instance).setAvatar(str);
            return this;
        }

        public Builder setDisappearingMessagesTimer(ByteString byteString) {
            copyOnWrite();
            ((Group) this.instance).setDisappearingMessagesTimer(byteString);
            return this;
        }

        public Builder setAccessControl(AccessControl.Builder builder) {
            copyOnWrite();
            ((Group) this.instance).setAccessControl(builder.build());
            return this;
        }

        public Builder setRevision(int i) {
            copyOnWrite();
            ((Group) this.instance).setRevision(i);
            return this;
        }

        public Builder addMembers(Member.Builder builder) {
            copyOnWrite();
            ((Group) this.instance).addMembers(builder.build());
            return this;
        }

        public Builder addPendingMembers(PendingMember.Builder builder) {
            copyOnWrite();
            ((Group) this.instance).addPendingMembers(builder.build());
            return this;
        }
    }

    /* renamed from: org.signal.storageservice.protos.groups.Group$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new Group();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\r\u0000\u0000\u0001\r\r\u0000\u0004\u0000\u0001\n\u0002\n\u0003Ȉ\u0004\n\u0005\t\u0006\u000b\u0007\u001b\b\u001b\t\u001b\n\n\u000b\n\f\u0007\r\u001b", new Object[]{"publicKey_", "title_", "avatar_", "disappearingMessagesTimer_", "accessControl_", "revision_", "members_", Member.class, "pendingMembers_", PendingMember.class, "requestingMembers_", RequestingMember.class, "inviteLinkPassword_", "description_", "announcementsOnly_", "bannedMembers_", BannedMember.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<Group> parser = PARSER;
                if (parser == null) {
                    synchronized (Group.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        Group group = new Group();
        DEFAULT_INSTANCE = group;
        GeneratedMessageLite.registerDefaultInstance(Group.class, group);
    }

    public static Group getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<Group> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
