package org.signal.storageservice.protos.groups;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class GroupInviteLink extends GeneratedMessageLite<GroupInviteLink, Builder> implements MessageLiteOrBuilder {
    private static final GroupInviteLink DEFAULT_INSTANCE;
    private static volatile Parser<GroupInviteLink> PARSER;
    public static final int V1CONTENTS_FIELD_NUMBER;
    private int contentsCase_ = 0;
    private Object contents_;

    private GroupInviteLink() {
    }

    /* loaded from: classes3.dex */
    public static final class GroupInviteLinkContentsV1 extends GeneratedMessageLite<GroupInviteLinkContentsV1, Builder> implements MessageLiteOrBuilder {
        private static final GroupInviteLinkContentsV1 DEFAULT_INSTANCE;
        public static final int GROUPMASTERKEY_FIELD_NUMBER;
        public static final int INVITELINKPASSWORD_FIELD_NUMBER;
        private static volatile Parser<GroupInviteLinkContentsV1> PARSER;
        private ByteString groupMasterKey_;
        private ByteString inviteLinkPassword_;

        private GroupInviteLinkContentsV1() {
            ByteString byteString = ByteString.EMPTY;
            this.groupMasterKey_ = byteString;
            this.inviteLinkPassword_ = byteString;
        }

        public ByteString getGroupMasterKey() {
            return this.groupMasterKey_;
        }

        public void setGroupMasterKey(ByteString byteString) {
            byteString.getClass();
            this.groupMasterKey_ = byteString;
        }

        public void clearGroupMasterKey() {
            this.groupMasterKey_ = getDefaultInstance().getGroupMasterKey();
        }

        public ByteString getInviteLinkPassword() {
            return this.inviteLinkPassword_;
        }

        public void setInviteLinkPassword(ByteString byteString) {
            byteString.getClass();
            this.inviteLinkPassword_ = byteString;
        }

        public void clearInviteLinkPassword() {
            this.inviteLinkPassword_ = getDefaultInstance().getInviteLinkPassword();
        }

        public static GroupInviteLinkContentsV1 parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (GroupInviteLinkContentsV1) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static GroupInviteLinkContentsV1 parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (GroupInviteLinkContentsV1) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static GroupInviteLinkContentsV1 parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (GroupInviteLinkContentsV1) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static GroupInviteLinkContentsV1 parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (GroupInviteLinkContentsV1) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static GroupInviteLinkContentsV1 parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (GroupInviteLinkContentsV1) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static GroupInviteLinkContentsV1 parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (GroupInviteLinkContentsV1) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static GroupInviteLinkContentsV1 parseFrom(InputStream inputStream) throws IOException {
            return (GroupInviteLinkContentsV1) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static GroupInviteLinkContentsV1 parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (GroupInviteLinkContentsV1) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static GroupInviteLinkContentsV1 parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (GroupInviteLinkContentsV1) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static GroupInviteLinkContentsV1 parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (GroupInviteLinkContentsV1) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static GroupInviteLinkContentsV1 parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (GroupInviteLinkContentsV1) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static GroupInviteLinkContentsV1 parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (GroupInviteLinkContentsV1) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(GroupInviteLinkContentsV1 groupInviteLinkContentsV1) {
            return DEFAULT_INSTANCE.createBuilder(groupInviteLinkContentsV1);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<GroupInviteLinkContentsV1, Builder> implements MessageLiteOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(GroupInviteLinkContentsV1.DEFAULT_INSTANCE);
            }

            public Builder setGroupMasterKey(ByteString byteString) {
                copyOnWrite();
                ((GroupInviteLinkContentsV1) this.instance).setGroupMasterKey(byteString);
                return this;
            }

            public Builder setInviteLinkPassword(ByteString byteString) {
                copyOnWrite();
                ((GroupInviteLinkContentsV1) this.instance).setInviteLinkPassword(byteString);
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new GroupInviteLinkContentsV1();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\n\u0002\n", new Object[]{"groupMasterKey_", "inviteLinkPassword_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<GroupInviteLinkContentsV1> parser = PARSER;
                    if (parser == null) {
                        synchronized (GroupInviteLinkContentsV1.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            GroupInviteLinkContentsV1 groupInviteLinkContentsV1 = new GroupInviteLinkContentsV1();
            DEFAULT_INSTANCE = groupInviteLinkContentsV1;
            GeneratedMessageLite.registerDefaultInstance(GroupInviteLinkContentsV1.class, groupInviteLinkContentsV1);
        }

        public static GroupInviteLinkContentsV1 getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<GroupInviteLinkContentsV1> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* renamed from: org.signal.storageservice.protos.groups.GroupInviteLink$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public enum ContentsCase {
        V1CONTENTS(1),
        CONTENTS_NOT_SET(0);
        
        private final int value;

        ContentsCase(int i) {
            this.value = i;
        }

        public static ContentsCase forNumber(int i) {
            if (i == 0) {
                return CONTENTS_NOT_SET;
            }
            if (i != 1) {
                return null;
            }
            return V1CONTENTS;
        }
    }

    public ContentsCase getContentsCase() {
        return ContentsCase.forNumber(this.contentsCase_);
    }

    public void clearContents() {
        this.contentsCase_ = 0;
        this.contents_ = null;
    }

    public boolean hasV1Contents() {
        return this.contentsCase_ == 1;
    }

    public GroupInviteLinkContentsV1 getV1Contents() {
        if (this.contentsCase_ == 1) {
            return (GroupInviteLinkContentsV1) this.contents_;
        }
        return GroupInviteLinkContentsV1.getDefaultInstance();
    }

    public void setV1Contents(GroupInviteLinkContentsV1 groupInviteLinkContentsV1) {
        groupInviteLinkContentsV1.getClass();
        this.contents_ = groupInviteLinkContentsV1;
        this.contentsCase_ = 1;
    }

    public void mergeV1Contents(GroupInviteLinkContentsV1 groupInviteLinkContentsV1) {
        groupInviteLinkContentsV1.getClass();
        if (this.contentsCase_ != 1 || this.contents_ == GroupInviteLinkContentsV1.getDefaultInstance()) {
            this.contents_ = groupInviteLinkContentsV1;
        } else {
            this.contents_ = GroupInviteLinkContentsV1.newBuilder((GroupInviteLinkContentsV1) this.contents_).mergeFrom((GroupInviteLinkContentsV1.Builder) groupInviteLinkContentsV1).buildPartial();
        }
        this.contentsCase_ = 1;
    }

    public void clearV1Contents() {
        if (this.contentsCase_ == 1) {
            this.contentsCase_ = 0;
            this.contents_ = null;
        }
    }

    public static GroupInviteLink parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (GroupInviteLink) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static GroupInviteLink parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (GroupInviteLink) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static GroupInviteLink parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (GroupInviteLink) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static GroupInviteLink parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (GroupInviteLink) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static GroupInviteLink parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (GroupInviteLink) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static GroupInviteLink parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (GroupInviteLink) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static GroupInviteLink parseFrom(InputStream inputStream) throws IOException {
        return (GroupInviteLink) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static GroupInviteLink parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (GroupInviteLink) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static GroupInviteLink parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (GroupInviteLink) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static GroupInviteLink parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (GroupInviteLink) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static GroupInviteLink parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (GroupInviteLink) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static GroupInviteLink parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (GroupInviteLink) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(GroupInviteLink groupInviteLink) {
        return DEFAULT_INSTANCE.createBuilder(groupInviteLink);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<GroupInviteLink, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(GroupInviteLink.DEFAULT_INSTANCE);
        }

        public Builder setV1Contents(GroupInviteLinkContentsV1.Builder builder) {
            copyOnWrite();
            ((GroupInviteLink) this.instance).setV1Contents(builder.build());
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new GroupInviteLink();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0001\u0000\u0001\u0001\u0001\u0000\u0000\u0000\u0001<\u0000", new Object[]{"contents_", "contentsCase_", GroupInviteLinkContentsV1.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<GroupInviteLink> parser = PARSER;
                if (parser == null) {
                    synchronized (GroupInviteLink.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        GroupInviteLink groupInviteLink = new GroupInviteLink();
        DEFAULT_INSTANCE = groupInviteLink;
        GeneratedMessageLite.registerDefaultInstance(GroupInviteLink.class, groupInviteLink);
    }

    public static GroupInviteLink getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<GroupInviteLink> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
