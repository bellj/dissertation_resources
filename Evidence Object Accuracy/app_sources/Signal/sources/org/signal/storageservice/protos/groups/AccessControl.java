package org.signal.storageservice.protos.groups;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class AccessControl extends GeneratedMessageLite<AccessControl, Builder> implements MessageLiteOrBuilder {
    public static final int ADDFROMINVITELINK_FIELD_NUMBER;
    public static final int ATTRIBUTES_FIELD_NUMBER;
    private static final AccessControl DEFAULT_INSTANCE;
    public static final int MEMBERS_FIELD_NUMBER;
    private static volatile Parser<AccessControl> PARSER;
    private int addFromInviteLink_;
    private int attributes_;
    private int members_;

    private AccessControl() {
    }

    /* loaded from: classes3.dex */
    public enum AccessRequired implements Internal.EnumLite {
        UNKNOWN(0),
        ANY(1),
        MEMBER(2),
        ADMINISTRATOR(3),
        UNSATISFIABLE(4),
        UNRECOGNIZED(-1);
        
        private static final Internal.EnumLiteMap<AccessRequired> internalValueMap = new Internal.EnumLiteMap<AccessRequired>() { // from class: org.signal.storageservice.protos.groups.AccessControl.AccessRequired.1
            @Override // com.google.protobuf.Internal.EnumLiteMap
            public AccessRequired findValueByNumber(int i) {
                return AccessRequired.forNumber(i);
            }
        };
        private final int value;

        @Override // com.google.protobuf.Internal.EnumLite
        public final int getNumber() {
            if (this != UNRECOGNIZED) {
                return this.value;
            }
            throw new IllegalArgumentException("Can't get the number of an unknown enum value.");
        }

        public static AccessRequired forNumber(int i) {
            if (i == 0) {
                return UNKNOWN;
            }
            if (i == 1) {
                return ANY;
            }
            if (i == 2) {
                return MEMBER;
            }
            if (i == 3) {
                return ADMINISTRATOR;
            }
            if (i != 4) {
                return null;
            }
            return UNSATISFIABLE;
        }

        AccessRequired(int i) {
            this.value = i;
        }
    }

    public int getAttributesValue() {
        return this.attributes_;
    }

    public AccessRequired getAttributes() {
        AccessRequired forNumber = AccessRequired.forNumber(this.attributes_);
        return forNumber == null ? AccessRequired.UNRECOGNIZED : forNumber;
    }

    public void setAttributesValue(int i) {
        this.attributes_ = i;
    }

    public void setAttributes(AccessRequired accessRequired) {
        this.attributes_ = accessRequired.getNumber();
    }

    public void clearAttributes() {
        this.attributes_ = 0;
    }

    public int getMembersValue() {
        return this.members_;
    }

    public AccessRequired getMembers() {
        AccessRequired forNumber = AccessRequired.forNumber(this.members_);
        return forNumber == null ? AccessRequired.UNRECOGNIZED : forNumber;
    }

    public void setMembersValue(int i) {
        this.members_ = i;
    }

    public void setMembers(AccessRequired accessRequired) {
        this.members_ = accessRequired.getNumber();
    }

    public void clearMembers() {
        this.members_ = 0;
    }

    public int getAddFromInviteLinkValue() {
        return this.addFromInviteLink_;
    }

    public AccessRequired getAddFromInviteLink() {
        AccessRequired forNumber = AccessRequired.forNumber(this.addFromInviteLink_);
        return forNumber == null ? AccessRequired.UNRECOGNIZED : forNumber;
    }

    public void setAddFromInviteLinkValue(int i) {
        this.addFromInviteLink_ = i;
    }

    public void setAddFromInviteLink(AccessRequired accessRequired) {
        this.addFromInviteLink_ = accessRequired.getNumber();
    }

    public void clearAddFromInviteLink() {
        this.addFromInviteLink_ = 0;
    }

    public static AccessControl parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (AccessControl) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static AccessControl parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (AccessControl) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static AccessControl parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (AccessControl) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static AccessControl parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (AccessControl) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static AccessControl parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (AccessControl) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static AccessControl parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (AccessControl) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static AccessControl parseFrom(InputStream inputStream) throws IOException {
        return (AccessControl) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static AccessControl parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (AccessControl) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static AccessControl parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (AccessControl) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static AccessControl parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (AccessControl) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static AccessControl parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (AccessControl) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static AccessControl parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (AccessControl) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(AccessControl accessControl) {
        return DEFAULT_INSTANCE.createBuilder(accessControl);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<AccessControl, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(AccessControl.DEFAULT_INSTANCE);
        }

        public Builder setAttributesValue(int i) {
            copyOnWrite();
            ((AccessControl) this.instance).setAttributesValue(i);
            return this;
        }

        public Builder setAttributes(AccessRequired accessRequired) {
            copyOnWrite();
            ((AccessControl) this.instance).setAttributes(accessRequired);
            return this;
        }

        public Builder setMembersValue(int i) {
            copyOnWrite();
            ((AccessControl) this.instance).setMembersValue(i);
            return this;
        }

        public Builder setMembers(AccessRequired accessRequired) {
            copyOnWrite();
            ((AccessControl) this.instance).setMembers(accessRequired);
            return this;
        }

        public Builder setAddFromInviteLink(AccessRequired accessRequired) {
            copyOnWrite();
            ((AccessControl) this.instance).setAddFromInviteLink(accessRequired);
            return this;
        }
    }

    /* renamed from: org.signal.storageservice.protos.groups.AccessControl$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new AccessControl();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\f\u0002\f\u0003\f", new Object[]{"attributes_", "members_", "addFromInviteLink_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<AccessControl> parser = PARSER;
                if (parser == null) {
                    synchronized (AccessControl.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        AccessControl accessControl = new AccessControl();
        DEFAULT_INSTANCE = accessControl;
        GeneratedMessageLite.registerDefaultInstance(AccessControl.class, accessControl);
    }

    public static AccessControl getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<AccessControl> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
