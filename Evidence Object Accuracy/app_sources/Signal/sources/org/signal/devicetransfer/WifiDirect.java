package org.signal.devicetransfer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceInfo;
import android.net.wifi.p2p.nsd.WifiP2pDnsSdServiceRequest;
import android.os.Build;
import android.os.HandlerThread;
import android.text.TextUtils;
import androidx.core.content.ContextCompat;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.signal.devicetransfer.WifiDirect;
import org.signal.devicetransfer.WifiDirectUnavailableException;

/* loaded from: classes3.dex */
public final class WifiDirect {
    private static final long SAFE_FOR_LONG_AWAIT_TIMEOUT = TimeUnit.SECONDS.toMillis(5);
    private static final Pattern SERVICE_INSTANCE_PATTERN = Pattern.compile("_devicetransfer(\\._(.+))?\\._signal\\.org");
    private static final String TAG = Log.tag(WifiDirect.class);
    private static final IntentFilter intentFilter = new IntentFilter() { // from class: org.signal.devicetransfer.WifiDirect.1
        {
            addAction("android.net.wifi.p2p.CONNECTION_STATE_CHANGE");
        }
    };
    private WifiP2pManager.Channel channel;
    private WifiDirectConnectionListener connectionListener;
    private final Context context;
    private WifiP2pManager manager;
    private WifiP2pDnsSdServiceRequest serviceRequest;
    private WifiDirectCallbacks wifiDirectCallbacks;
    private final HandlerThread wifiDirectCallbacksHandler = SignalExecutors.getAndStartHandlerThread("wifi-direct-cb");

    /* loaded from: classes3.dex */
    public enum AvailableStatus {
        FEATURE_NOT_AVAILABLE,
        WIFI_MANAGER_NOT_AVAILABLE,
        FINE_LOCATION_PERMISSION_NOT_GRANTED,
        WIFI_DIRECT_NOT_AVAILABLE,
        AVAILABLE
    }

    /* loaded from: classes3.dex */
    public interface ManagerRetry {
        void call(WifiP2pManager.Channel channel, WifiP2pManager.ActionListener actionListener);
    }

    /* loaded from: classes3.dex */
    public interface WifiDirectConnectionListener {
        void onConnectionChanged(NetworkInfo networkInfo);

        void onNetworkConnected(WifiP2pInfo wifiP2pInfo);

        void onNetworkFailure();

        void onServiceDiscovered(WifiP2pDevice wifiP2pDevice, String str);
    }

    public static /* synthetic */ void lambda$discoverService$0(String str, Map map, WifiP2pDevice wifiP2pDevice) {
    }

    public static AvailableStatus getAvailability(Context context) {
        if (!context.getPackageManager().hasSystemFeature("android.hardware.wifi.direct")) {
            Log.i(TAG, "Feature not available");
            return AvailableStatus.FEATURE_NOT_AVAILABLE;
        }
        WifiManager wifiManager = (WifiManager) ContextCompat.getSystemService(context, WifiManager.class);
        if (wifiManager == null) {
            Log.i(TAG, "WifiManager not available");
            return AvailableStatus.WIFI_MANAGER_NOT_AVAILABLE;
        }
        int i = Build.VERSION.SDK_INT;
        if (i >= 23 && context.checkSelfPermission("android.permission.ACCESS_FINE_LOCATION") != 0) {
            Log.i(TAG, "Fine location permission required");
            return AvailableStatus.FINE_LOCATION_PERMISSION_NOT_GRANTED;
        } else if (i <= 23 || wifiManager.isP2pSupported()) {
            return AvailableStatus.AVAILABLE;
        } else {
            return AvailableStatus.WIFI_DIRECT_NOT_AVAILABLE;
        }
    }

    public WifiDirect(Context context) {
        this.context = context.getApplicationContext();
    }

    public synchronized void initialize(WifiDirectConnectionListener wifiDirectConnectionListener) throws WifiDirectUnavailableException {
        if (isInitialized()) {
            Log.w(TAG, "Already initialized, do not need to initialize twice");
            return;
        }
        this.connectionListener = wifiDirectConnectionListener;
        WifiP2pManager wifiP2pManager = (WifiP2pManager) ContextCompat.getSystemService(this.context, WifiP2pManager.class);
        this.manager = wifiP2pManager;
        if (wifiP2pManager != null) {
            this.wifiDirectCallbacks = new WifiDirectCallbacks(wifiDirectConnectionListener);
            WifiP2pManager.Channel initialize = this.manager.initialize(this.context, this.wifiDirectCallbacksHandler.getLooper(), this.wifiDirectCallbacks);
            this.channel = initialize;
            if (initialize != null) {
                this.context.registerReceiver(this.wifiDirectCallbacks, intentFilter);
                return;
            }
            Log.i(TAG, "Unable to initialize channel");
            shutdown();
            throw new WifiDirectUnavailableException(WifiDirectUnavailableException.Reason.CHANNEL_INITIALIZATION);
        }
        Log.i(TAG, "Unable to get WifiP2pManager");
        shutdown();
        throw new WifiDirectUnavailableException(WifiDirectUnavailableException.Reason.WIFI_P2P_MANAGER);
    }

    public synchronized void shutdown() {
        Log.d(TAG, "Shutting down");
        this.connectionListener = null;
        WifiP2pManager wifiP2pManager = this.manager;
        if (wifiP2pManager != null) {
            Objects.requireNonNull(wifiP2pManager);
            WifiDirect$$ExternalSyntheticLambda0 wifiDirect$$ExternalSyntheticLambda0 = new WifiDirect$$ExternalSyntheticLambda0(wifiP2pManager);
            long j = SAFE_FOR_LONG_AWAIT_TIMEOUT;
            retrySync(wifiDirect$$ExternalSyntheticLambda0, "clear service requests", j);
            WifiP2pManager wifiP2pManager2 = this.manager;
            Objects.requireNonNull(wifiP2pManager2);
            retrySync(new WifiDirect$$ExternalSyntheticLambda1(wifiP2pManager2), "stop peer discovery", j);
            WifiP2pManager wifiP2pManager3 = this.manager;
            Objects.requireNonNull(wifiP2pManager3);
            retrySync(new WifiDirect$$ExternalSyntheticLambda2(wifiP2pManager3), "clear local services", j);
            if (Build.VERSION.SDK_INT < 27) {
                WifiP2pManager wifiP2pManager4 = this.manager;
                Objects.requireNonNull(wifiP2pManager4);
                retrySync(new ManagerRetry(wifiP2pManager4) { // from class: org.signal.devicetransfer.WifiDirect$$ExternalSyntheticLambda3
                    public final /* synthetic */ WifiP2pManager f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // org.signal.devicetransfer.WifiDirect.ManagerRetry
                    public final void call(WifiP2pManager.Channel channel, WifiP2pManager.ActionListener actionListener) {
                        this.f$0.removeGroup(channel, actionListener);
                    }
                }, "remove group", j);
                this.channel = null;
            }
            this.manager = null;
        }
        WifiP2pManager.Channel channel = this.channel;
        if (channel != null && Build.VERSION.SDK_INT >= 27) {
            channel.close();
            this.channel = null;
        }
        WifiDirectCallbacks wifiDirectCallbacks = this.wifiDirectCallbacks;
        if (wifiDirectCallbacks != null) {
            wifiDirectCallbacks.clearConnectionListener();
            this.context.unregisterReceiver(this.wifiDirectCallbacks);
            this.wifiDirectCallbacks = null;
        }
        this.wifiDirectCallbacksHandler.quit();
        this.wifiDirectCallbacksHandler.interrupt();
    }

    public synchronized void startDiscoveryService(String str) throws WifiDirectUnavailableException {
        ensureInitialized();
        WifiP2pDnsSdServiceInfo newInstance = WifiP2pDnsSdServiceInfo.newInstance(buildServiceInstanceName(str), "_presence._tcp", Collections.emptyMap());
        long j = SAFE_FOR_LONG_AWAIT_TIMEOUT;
        SyncActionListener syncActionListener = new SyncActionListener("add local service", j);
        this.manager.addLocalService(this.channel, newInstance, syncActionListener);
        SyncActionListener syncActionListener2 = new SyncActionListener("discover peers", j);
        this.manager.discoverPeers(this.channel, syncActionListener2);
        if (!syncActionListener.successful() || !syncActionListener2.successful()) {
            throw new WifiDirectUnavailableException(WifiDirectUnavailableException.Reason.SERVICE_START);
        }
    }

    public synchronized void stopDiscoveryService() throws WifiDirectUnavailableException {
        ensureInitialized();
        WifiP2pManager wifiP2pManager = this.manager;
        Objects.requireNonNull(wifiP2pManager);
        retryAsync(new WifiDirect$$ExternalSyntheticLambda1(wifiP2pManager), "stop peer discovery");
        WifiP2pManager wifiP2pManager2 = this.manager;
        Objects.requireNonNull(wifiP2pManager2);
        retryAsync(new WifiDirect$$ExternalSyntheticLambda2(wifiP2pManager2), "clear local services");
    }

    public synchronized void discoverService() throws WifiDirectUnavailableException {
        ensureInitialized();
        if (this.serviceRequest != null) {
            Log.w(TAG, "Discover service already called and active.");
            return;
        }
        WifiDirect$$ExternalSyntheticLambda4 wifiDirect$$ExternalSyntheticLambda4 = new WifiP2pManager.DnsSdTxtRecordListener() { // from class: org.signal.devicetransfer.WifiDirect$$ExternalSyntheticLambda4
            @Override // android.net.wifi.p2p.WifiP2pManager.DnsSdTxtRecordListener
            public final void onDnsSdTxtRecordAvailable(String str, Map map, WifiP2pDevice wifiP2pDevice) {
                WifiDirect.lambda$discoverService$0(str, map, wifiP2pDevice);
            }
        };
        this.manager.setDnsSdResponseListeners(this.channel, new WifiP2pManager.DnsSdServiceResponseListener() { // from class: org.signal.devicetransfer.WifiDirect$$ExternalSyntheticLambda5
            @Override // android.net.wifi.p2p.WifiP2pManager.DnsSdServiceResponseListener
            public final void onDnsSdServiceAvailable(String str, String str2, WifiP2pDevice wifiP2pDevice) {
                WifiDirect.this.lambda$discoverService$1(str, str2, wifiP2pDevice);
            }
        }, wifiDirect$$ExternalSyntheticLambda4);
        this.serviceRequest = WifiP2pDnsSdServiceRequest.newInstance();
        long j = SAFE_FOR_LONG_AWAIT_TIMEOUT;
        SyncActionListener syncActionListener = new SyncActionListener("add service request", j);
        this.manager.addServiceRequest(this.channel, this.serviceRequest, syncActionListener);
        SyncActionListener syncActionListener2 = new SyncActionListener("discover services", j);
        this.manager.discoverServices(this.channel, syncActionListener2);
        if (!syncActionListener.successful() || !syncActionListener2.successful()) {
            this.manager.removeServiceRequest(this.channel, this.serviceRequest, null);
            this.serviceRequest = null;
            throw new WifiDirectUnavailableException(WifiDirectUnavailableException.Reason.SERVICE_DISCOVERY_START);
        }
    }

    public /* synthetic */ void lambda$discoverService$1(String str, String str2, WifiP2pDevice wifiP2pDevice) {
        String isInstanceNameMatching = isInstanceNameMatching(str);
        if (isInstanceNameMatching != null) {
            Log.d(TAG, "Service found!");
            WifiDirectConnectionListener wifiDirectConnectionListener = this.connectionListener;
            if (wifiDirectConnectionListener != null) {
                wifiDirectConnectionListener.onServiceDiscovered(wifiP2pDevice, isInstanceNameMatching);
                return;
            }
            return;
        }
        Log.d(TAG, "Found unusable service, ignoring.");
    }

    public synchronized void stopServiceDiscovery() throws WifiDirectUnavailableException {
        ensureInitialized();
        WifiP2pManager wifiP2pManager = this.manager;
        Objects.requireNonNull(wifiP2pManager);
        retryAsync(new WifiDirect$$ExternalSyntheticLambda0(wifiP2pManager), "clear service requests");
    }

    public synchronized void connect(String str) throws WifiDirectUnavailableException {
        ensureInitialized();
        WifiP2pConfig wifiP2pConfig = new WifiP2pConfig();
        wifiP2pConfig.deviceAddress = str;
        wifiP2pConfig.wps.setup = 0;
        wifiP2pConfig.groupOwnerIntent = 0;
        WifiP2pDnsSdServiceRequest wifiP2pDnsSdServiceRequest = this.serviceRequest;
        if (wifiP2pDnsSdServiceRequest != null) {
            this.manager.removeServiceRequest(this.channel, wifiP2pDnsSdServiceRequest, LoggingActionListener.message("Remote service request"));
            this.serviceRequest = null;
        }
        SyncActionListener syncActionListener = new SyncActionListener("service connect", SAFE_FOR_LONG_AWAIT_TIMEOUT);
        this.manager.connect(this.channel, wifiP2pConfig, syncActionListener);
        if (syncActionListener.successful()) {
            Log.i(TAG, "Successfully connected to service.");
        } else {
            throw new WifiDirectUnavailableException(WifiDirectUnavailableException.Reason.SERVICE_CONNECT_FAILURE);
        }
    }

    public synchronized void requestNetworkInfo() throws WifiDirectUnavailableException {
        ensureInitialized();
        this.manager.requestConnectionInfo(this.channel, new WifiP2pManager.ConnectionInfoListener() { // from class: org.signal.devicetransfer.WifiDirect$$ExternalSyntheticLambda6
            @Override // android.net.wifi.p2p.WifiP2pManager.ConnectionInfoListener
            public final void onConnectionInfoAvailable(WifiP2pInfo wifiP2pInfo) {
                WifiDirect.this.lambda$requestNetworkInfo$2(wifiP2pInfo);
            }
        });
    }

    public /* synthetic */ void lambda$requestNetworkInfo$2(WifiP2pInfo wifiP2pInfo) {
        String str = TAG;
        Log.i(str, "Connection information available. group_formed: " + wifiP2pInfo.groupFormed + " group_owner: " + wifiP2pInfo.isGroupOwner);
        WifiDirectConnectionListener wifiDirectConnectionListener = this.connectionListener;
        if (wifiDirectConnectionListener != null) {
            wifiDirectConnectionListener.onNetworkConnected(wifiP2pInfo);
        }
    }

    private synchronized void retrySync(ManagerRetry managerRetry, String str, long j) {
        int i = 3;
        while (true) {
            int i2 = i - 1;
            if (i > 0) {
                if (!isNotInitialized()) {
                    SyncActionListener syncActionListener = new SyncActionListener(str, j);
                    managerRetry.call(this.channel, syncActionListener);
                    if (syncActionListener.successful() || syncActionListener.failureReason == -2) {
                        break;
                    }
                    ThreadUtil.sleep(TimeUnit.SECONDS.toMillis(1));
                    i = i2;
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    public /* synthetic */ void lambda$retryAsync$3(ManagerRetry managerRetry, String str) {
        retrySync(managerRetry, str, 50);
    }

    private void retryAsync(ManagerRetry managerRetry, String str) {
        SignalExecutors.BOUNDED.execute(new Runnable(managerRetry, str) { // from class: org.signal.devicetransfer.WifiDirect$$ExternalSyntheticLambda7
            public final /* synthetic */ WifiDirect.ManagerRetry f$1;
            public final /* synthetic */ String f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                WifiDirect.this.lambda$retryAsync$3(this.f$1, this.f$2);
            }
        });
    }

    private synchronized boolean isInitialized() {
        boolean z;
        if (this.manager != null) {
            if (this.channel != null) {
                z = true;
            }
        }
        z = false;
        return z;
    }

    private synchronized boolean isNotInitialized() {
        boolean z;
        if (this.manager != null) {
            if (this.channel != null) {
                z = false;
            }
        }
        z = true;
        return z;
    }

    private void ensureInitialized() throws WifiDirectUnavailableException {
        if (isNotInitialized()) {
            Log.w(TAG, "WiFi Direct has not been initialized.");
            throw new WifiDirectUnavailableException(WifiDirectUnavailableException.Reason.SERVICE_NOT_INITIALIZED);
        }
    }

    static String buildServiceInstanceName(String str) {
        if (TextUtils.isEmpty(str)) {
            return "_devicetransfer%%EXTRA_INFO%%._signal.org".replace("%%EXTRA_INFO%%", "");
        }
        return "_devicetransfer%%EXTRA_INFO%%._signal.org".replace("%%EXTRA_INFO%%", "._" + str);
    }

    static String isInstanceNameMatching(String str) {
        Matcher matcher = SERVICE_INSTANCE_PATTERN.matcher(str);
        if (!matcher.matches()) {
            return null;
        }
        String group = matcher.group(2);
        return TextUtils.isEmpty(group) ? "" : group;
    }

    /* loaded from: classes3.dex */
    public static class WifiDirectCallbacks extends BroadcastReceiver implements WifiP2pManager.ChannelListener {
        private WifiDirectConnectionListener connectionListener;

        public WifiDirectCallbacks(WifiDirectConnectionListener wifiDirectConnectionListener) {
            this.connectionListener = wifiDirectConnectionListener;
        }

        public void clearConnectionListener() {
            this.connectionListener = null;
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null && "android.net.wifi.p2p.CONNECTION_STATE_CHANGE".equals(action)) {
                WifiDirectConnectionListener wifiDirectConnectionListener = this.connectionListener;
                NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra("networkInfo");
                if (networkInfo == null) {
                    Log.w(WifiDirect.TAG, "WiFi P2P broadcast connection changed action with null network info.");
                } else if (wifiDirectConnectionListener != null) {
                    wifiDirectConnectionListener.onConnectionChanged(networkInfo);
                }
            }
        }

        @Override // android.net.wifi.p2p.WifiP2pManager.ChannelListener
        public void onChannelDisconnected() {
            WifiDirectConnectionListener wifiDirectConnectionListener = this.connectionListener;
            if (wifiDirectConnectionListener != null) {
                wifiDirectConnectionListener.onNetworkFailure();
            }
        }
    }

    /* loaded from: classes3.dex */
    public static class SyncActionListener extends LoggingActionListener {
        private final long awaitTimeout;
        private volatile int failureReason = -1;
        private final CountDownLatch sync;

        public SyncActionListener(String str, long j) {
            super(str);
            this.awaitTimeout = j;
            this.sync = new CountDownLatch(1);
        }

        @Override // org.signal.devicetransfer.WifiDirect.LoggingActionListener, android.net.wifi.p2p.WifiP2pManager.ActionListener
        public void onSuccess() {
            super.onSuccess();
            this.sync.countDown();
        }

        @Override // org.signal.devicetransfer.WifiDirect.LoggingActionListener, android.net.wifi.p2p.WifiP2pManager.ActionListener
        public void onFailure(int i) {
            super.onFailure(i);
            this.failureReason = i;
            this.sync.countDown();
        }

        public boolean successful() {
            try {
                if (!this.sync.await(this.awaitTimeout, TimeUnit.MILLISECONDS)) {
                    String str = WifiDirect.TAG;
                    Log.i(str, "SyncListener [" + this.message + "] timed out after " + this.awaitTimeout + "ms");
                    this.failureReason = -2;
                    return false;
                }
            } catch (InterruptedException unused) {
                String str2 = WifiDirect.TAG;
                Log.i(str2, "SyncListener [" + this.message + "] interrupted");
            }
            if (this.failureReason < 0) {
                return true;
            }
            return false;
        }
    }

    /* loaded from: classes3.dex */
    public static class LoggingActionListener implements WifiP2pManager.ActionListener {
        protected final String message;

        public static LoggingActionListener message(String str) {
            return new LoggingActionListener(str);
        }

        public LoggingActionListener(String str) {
            this.message = str;
        }

        @Override // android.net.wifi.p2p.WifiP2pManager.ActionListener
        public void onSuccess() {
            String str = WifiDirect.TAG;
            Log.i(str, this.message + " success");
        }

        @Override // android.net.wifi.p2p.WifiP2pManager.ActionListener
        public void onFailure(int i) {
            String str = WifiDirect.TAG;
            Log.w(str, this.message + " failure_reason: " + i);
        }
    }
}
