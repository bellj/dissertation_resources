package org.signal.devicetransfer;

/* loaded from: classes3.dex */
public final class KeyGenerationFailedException extends Throwable {
    public KeyGenerationFailedException(Exception exc) {
        super(exc);
    }
}
