package org.signal.devicetransfer;

import android.os.Handler;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;

/* loaded from: classes3.dex */
public final class IpExchange {
    public static IpExchangeThread giveIp(String str, int i, Handler handler, int i2) {
        IpExchangeThread ipExchangeThread = new IpExchangeThread(str, i, false, handler, i2);
        ipExchangeThread.start();
        return ipExchangeThread;
    }

    public static IpExchangeThread getIp(String str, int i, Handler handler, int i2) {
        IpExchangeThread ipExchangeThread = new IpExchangeThread(str, i, true, handler, i2);
        ipExchangeThread.start();
        return ipExchangeThread;
    }

    /* loaded from: classes3.dex */
    public static class IpExchangeThread extends Thread {
        private static final String TAG = Log.tag(IpExchangeThread.class);
        private volatile Socket client;
        private final Handler handler;
        private volatile boolean isRunning;
        private final int message;
        private final boolean needsIp;
        private final int port;
        private final String serverHostAddress;
        private volatile ServerSocket serverSocket;

        public IpExchangeThread(String str, int i, boolean z, Handler handler, int i2) {
            this.serverHostAddress = str;
            this.port = i;
            this.needsIp = z;
            this.handler = handler;
            this.message = i2;
        }

        @Override // java.lang.Thread, java.lang.Runnable
        public void run() {
            ServerSocket serverSocket;
            Log.i(TAG, "Running...");
            this.isRunning = true;
            while (shouldKeepRunning()) {
                Log.i(TAG, "Attempting to startup networking...");
                try {
                    try {
                        if (this.needsIp) {
                            getIp();
                        } else {
                            sendIp();
                        }
                        if (this.client != null && !this.client.isClosed()) {
                            try {
                                this.client.close();
                            } catch (IOException unused) {
                            }
                        }
                    } catch (Throwable th) {
                        if (this.client != null && !this.client.isClosed()) {
                            try {
                                this.client.close();
                            } catch (IOException unused2) {
                            }
                        }
                        if (this.serverSocket != null) {
                            try {
                                this.serverSocket.close();
                            } catch (IOException unused3) {
                            }
                        }
                        throw th;
                    }
                } catch (Exception e) {
                    Log.w(TAG, e);
                    if (this.client != null && !this.client.isClosed()) {
                        try {
                            this.client.close();
                        } catch (IOException unused4) {
                        }
                    }
                    if (this.serverSocket != null) {
                        serverSocket = this.serverSocket;
                    }
                }
                if (this.serverSocket != null) {
                    try {
                        serverSocket = this.serverSocket;
                        serverSocket.close();
                    } catch (IOException unused5) {
                    }
                }
                if (shouldKeepRunning()) {
                    ThreadUtil.interruptableSleep(TimeUnit.SECONDS.toMillis(3));
                }
            }
            Log.i(TAG, "Exiting");
        }

        private void sendIp() throws IOException {
            this.client = new Socket();
            this.client.bind(null);
            this.client.connect(new InetSocketAddress(this.serverHostAddress, this.port), 10000);
            this.handler.sendEmptyMessage(this.message);
            Log.i(TAG, "Done!!");
            this.isRunning = false;
        }

        private void getIp() throws IOException {
            this.serverSocket = new ServerSocket(this.port);
            while (shouldKeepRunning() && !this.serverSocket.isClosed()) {
                String str = TAG;
                Log.i(str, "Waiting for client socket accept...");
                try {
                    Socket accept = this.serverSocket.accept();
                    Log.i(str, "Client connected, obtaining IP address");
                    String hostAddress = accept.getInetAddress().getHostAddress();
                    Handler handler = this.handler;
                    handler.sendMessage(handler.obtainMessage(this.message, hostAddress));
                    accept.close();
                } catch (IOException e) {
                    if (this.isRunning) {
                        Log.i(TAG, "Error connecting with client or server socket closed.", e);
                    } else {
                        Log.i(TAG, "Server shutting down...");
                    }
                }
            }
        }

        public void shutdown() {
            this.isRunning = false;
            try {
                if (this.client != null) {
                    this.client.close();
                }
                if (this.serverSocket != null) {
                    this.serverSocket.close();
                }
            } catch (IOException e) {
                Log.w(TAG, "Error shutting down", e);
            }
            interrupt();
        }

        private boolean shouldKeepRunning() {
            return !isInterrupted() && this.isRunning;
        }
    }
}
