package org.signal.devicetransfer;

import android.net.wifi.p2p.WifiP2pManager;
import org.signal.devicetransfer.WifiDirect;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class WifiDirect$$ExternalSyntheticLambda1 implements WifiDirect.ManagerRetry {
    public final /* synthetic */ WifiP2pManager f$0;

    public /* synthetic */ WifiDirect$$ExternalSyntheticLambda1(WifiP2pManager wifiP2pManager) {
        this.f$0 = wifiP2pManager;
    }

    @Override // org.signal.devicetransfer.WifiDirect.ManagerRetry
    public final void call(WifiP2pManager.Channel channel, WifiP2pManager.ActionListener actionListener) {
        this.f$0.stopPeerDiscovery(channel, actionListener);
    }
}
