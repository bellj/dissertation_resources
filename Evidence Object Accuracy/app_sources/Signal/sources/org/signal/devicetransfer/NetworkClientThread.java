package org.signal.devicetransfer;

import android.content.Context;
import android.os.Handler;
import javax.net.ssl.SSLSocket;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.logging.Log;
import org.signal.devicetransfer.DeviceTransferAuthentication;

/* loaded from: classes3.dex */
public final class NetworkClientThread extends Thread {
    private static final String TAG = Log.tag(NetworkClientThread.class);
    private volatile SSLSocket client;
    private final ClientTask clientTask;
    private final Context context;
    private final Handler handler;
    private volatile boolean isRunning;
    private volatile Boolean isVerified;
    private final int port;
    private final String serverHostAddress;
    private boolean success;
    private final Object verificationLock = new Object();

    public NetworkClientThread(Context context, ClientTask clientTask, String str, int i, Handler handler) {
        this.context = context;
        this.clientTask = clientTask;
        this.serverHostAddress = str;
        this.port = i;
        this.handler = handler;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00f8, code lost:
        if (r11.success != false) goto L_0x0109;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0107, code lost:
        if (r11.success == false) goto L_0x010e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0109, code lost:
        r11.clientTask.success();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x010e, code lost:
        org.signal.core.util.StreamUtil.close(r11.client);
        r11.handler.sendEmptyMessage(1002);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x011c, code lost:
        if (shouldKeepRunning() == false) goto L_0x000d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x011e, code lost:
        org.signal.core.util.ThreadUtil.interruptableSleep(java.util.concurrent.TimeUnit.SECONDS.toMillis(3));
     */
    @Override // java.lang.Thread, java.lang.Runnable
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
        // Method dump skipped, instructions count: 334
        */
        throw new UnsupportedOperationException("Method not decompiled: org.signal.devicetransfer.NetworkClientThread.run():void");
    }

    private void awaitAuthenticationCodeVerification() throws DeviceTransferAuthentication.DeviceTransferAuthenticationException {
        synchronized (this.verificationLock) {
            while (this.isVerified == null) {
                try {
                    this.verificationLock.wait();
                } catch (InterruptedException e) {
                    throw new DeviceTransferAuthentication.DeviceTransferAuthenticationException(e);
                }
            }
            if (!this.isVerified.booleanValue()) {
                throw new DeviceTransferAuthentication.DeviceTransferAuthenticationException("User verification failed");
            }
        }
    }

    public void setVerified(boolean z) {
        this.isVerified = Boolean.valueOf(z);
        synchronized (this.verificationLock) {
            this.verificationLock.notify();
        }
    }

    public void shutdown() {
        this.isRunning = false;
        StreamUtil.close(this.client);
        interrupt();
    }

    private boolean shouldKeepRunning() {
        return !isInterrupted() && this.isRunning;
    }
}
