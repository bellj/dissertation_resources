package org.signal.devicetransfer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Arrays;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.signal.core.util.StreamUtil;

/* loaded from: classes3.dex */
public final class DeviceTransferAuthentication {
    public static int generateClientAuthenticationCode(byte[] bArr, InputStream inputStream, OutputStream outputStream) throws DeviceTransferAuthenticationException, IOException {
        Client client = new Client(bArr);
        outputStream.write(client.getCommitment());
        outputStream.flush();
        byte[] bArr2 = new byte[32];
        StreamUtil.readFully(inputStream, bArr2, 32);
        outputStream.write(client.setServerRandomAndGetClientRandom(bArr2));
        outputStream.flush();
        return client.computeShortAuthenticationCode();
    }

    public static int generateServerAuthenticationCode(byte[] bArr, InputStream inputStream, OutputStream outputStream) throws DeviceTransferAuthenticationException, IOException {
        byte[] bArr2 = new byte[32];
        StreamUtil.readFully(inputStream, bArr2, 32);
        Server server = new Server(bArr, bArr2);
        outputStream.write(server.getRandom());
        outputStream.flush();
        byte[] bArr3 = new byte[32];
        StreamUtil.readFully(inputStream, bArr3, 32);
        server.setClientRandom(bArr3);
        return server.computeShortAuthenticationCode();
    }

    public static Mac getMac(byte[] bArr) throws DeviceTransferAuthenticationException {
        try {
            Mac instance = Mac.getInstance("HmacSHA256");
            instance.init(new SecretKeySpec(bArr, "HmacSHA256"));
            return instance;
        } catch (Exception e) {
            throw new DeviceTransferAuthenticationException(e);
        }
    }

    public static int computeShortAuthenticationCode(byte[] bArr, byte[] bArr2) throws DeviceTransferAuthenticationException {
        byte[] doFinal = getMac(bArr).doFinal(bArr2);
        ByteBuffer wrap = ByteBuffer.wrap(doFinal);
        wrap.order(ByteOrder.BIG_ENDIAN);
        return wrap.getInt(doFinal.length - 4) & 8388607;
    }

    public static byte[] copyOf(byte[] bArr) {
        return Arrays.copyOf(bArr, bArr.length);
    }

    public static void validateLength(byte[] bArr) throws DeviceTransferAuthenticationException {
        if (bArr.length != 32) {
            throw new DeviceTransferAuthenticationException("invalid digest length");
        }
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes3.dex */
    public static final class Server {
        private final byte[] certificate;
        private final byte[] clientCommitment;
        private byte[] clientRandom;
        private final byte[] random;

        public Server(byte[] bArr, byte[] bArr2) throws DeviceTransferAuthenticationException {
            DeviceTransferAuthentication.validateLength(bArr2);
            this.certificate = DeviceTransferAuthentication.copyOf(bArr);
            this.clientCommitment = DeviceTransferAuthentication.copyOf(bArr2);
            SecureRandom secureRandom = new SecureRandom();
            byte[] bArr3 = new byte[32];
            this.random = bArr3;
            secureRandom.nextBytes(bArr3);
        }

        public byte[] getRandom() {
            return DeviceTransferAuthentication.copyOf(this.random);
        }

        public void setClientRandom(byte[] bArr) throws DeviceTransferAuthenticationException {
            DeviceTransferAuthentication.validateLength(bArr);
            this.clientRandom = DeviceTransferAuthentication.copyOf(bArr);
        }

        public void verifyClientRandom() throws DeviceTransferAuthenticationException {
            byte[] bArr = this.clientRandom;
            if (bArr != null) {
                if (!MessageDigest.isEqual(this.clientCommitment, DeviceTransferAuthentication.getMac(DeviceTransferAuthentication.copyOf(bArr)).doFinal(DeviceTransferAuthentication.copyOf(this.certificate)))) {
                    throw new DeviceTransferAuthenticationException("commitments do not match, do not proceed");
                }
                return;
            }
            throw new DeviceTransferAuthenticationException("no client random set");
        }

        public int computeShortAuthenticationCode() throws DeviceTransferAuthenticationException {
            verifyClientRandom();
            return DeviceTransferAuthentication.computeShortAuthenticationCode(DeviceTransferAuthentication.copyOf(this.clientRandom), DeviceTransferAuthentication.copyOf(this.random));
        }
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes3.dex */
    public static final class Client {
        private final byte[] commitment;
        private final byte[] random;
        private byte[] serverRandom;

        public Client(byte[] bArr) throws DeviceTransferAuthenticationException {
            SecureRandom secureRandom = new SecureRandom();
            byte[] bArr2 = new byte[32];
            this.random = bArr2;
            secureRandom.nextBytes(bArr2);
            this.commitment = DeviceTransferAuthentication.getMac(DeviceTransferAuthentication.copyOf(bArr2)).doFinal(DeviceTransferAuthentication.copyOf(bArr));
        }

        public byte[] getCommitment() {
            return DeviceTransferAuthentication.copyOf(this.commitment);
        }

        public byte[] setServerRandomAndGetClientRandom(byte[] bArr) throws DeviceTransferAuthenticationException {
            DeviceTransferAuthentication.validateLength(bArr);
            this.serverRandom = DeviceTransferAuthentication.copyOf(bArr);
            return DeviceTransferAuthentication.copyOf(this.random);
        }

        public int computeShortAuthenticationCode() throws DeviceTransferAuthenticationException {
            if (this.serverRandom != null) {
                return DeviceTransferAuthentication.computeShortAuthenticationCode(DeviceTransferAuthentication.copyOf(this.random), DeviceTransferAuthentication.copyOf(this.serverRandom));
            }
            throw new DeviceTransferAuthenticationException("no server random set");
        }
    }

    /* loaded from: classes3.dex */
    public static final class DeviceTransferAuthenticationException extends Exception {
        public DeviceTransferAuthenticationException(String str) {
            super(str);
        }

        public DeviceTransferAuthenticationException(Throwable th) {
            super(th);
        }
    }
}
