package org.signal.devicetransfer;

/* loaded from: classes3.dex */
public interface ShutdownCallback {
    void shutdown();
}
