package org.signal.devicetransfer;

import android.content.Context;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.greenrobot.eventbus.EventBus;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.signal.devicetransfer.IpExchange;
import org.signal.devicetransfer.WifiDirect;
import org.signal.devicetransfer.WifiDirectUnavailableException;

/* access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public final class DeviceTransferClient implements Handler.Callback {
    private static final String TAG = Log.tag(DeviceTransferClient.class);
    private final Runnable autoRestart;
    private final ClientTask clientTask;
    private NetworkClientThread clientThread;
    private HandlerThread commandAndControlThread;
    private final Context context;
    private final Handler handler;
    private IpExchange.IpExchangeThread ipExchangeThread;
    private int remotePort;
    private final ShutdownCallback shutdownCallback;
    private final AtomicBoolean started = new AtomicBoolean(false);
    private final AtomicBoolean stopped = new AtomicBoolean(false);
    private WifiDirect wifiDirect;

    private static void update(TransferStatus transferStatus) {
        String str = TAG;
        Log.d(str, "transferStatus: " + transferStatus.getTransferMode().name());
        EventBus.getDefault().postSticky(transferStatus);
    }

    public DeviceTransferClient(Context context, ClientTask clientTask, ShutdownCallback shutdownCallback) {
        this.context = context;
        this.clientTask = clientTask;
        this.shutdownCallback = shutdownCallback;
        this.commandAndControlThread = SignalExecutors.getAndStartHandlerThread("client-cnc");
        this.handler = new Handler(this.commandAndControlThread.getLooper(), this);
        this.autoRestart = new Runnable() { // from class: org.signal.devicetransfer.DeviceTransferClient$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                DeviceTransferClient.this.lambda$new$0();
            }
        };
    }

    public /* synthetic */ void lambda$new$0() {
        Log.i(TAG, "Restarting WiFi Direct since we haven't found anything yet and it could be us.");
        this.handler.sendEmptyMessage(5);
    }

    public void start() {
        if (this.started.compareAndSet(false, true)) {
            update(TransferStatus.ready());
            this.handler.sendEmptyMessage(0);
        }
    }

    public void stop() {
        if (this.stopped.compareAndSet(false, true)) {
            this.handler.sendEmptyMessage(1);
        }
    }

    public void setVerified(boolean z) {
        if (!this.stopped.get()) {
            Handler handler = this.handler;
            handler.sendMessage(handler.obtainMessage(8, Boolean.valueOf(z)));
        }
    }

    private void shutdown() {
        stopIpExchange();
        stopNetworkClient();
        stopWifiDirect();
        if (this.commandAndControlThread != null) {
            Log.i(TAG, "Shutting down command and control");
            this.commandAndControlThread.quit();
            this.commandAndControlThread.interrupt();
            this.commandAndControlThread = null;
        }
        EventBus.getDefault().removeStickyEvent(TransferStatus.class);
    }

    private void internalShutdown() {
        shutdown();
        ShutdownCallback shutdownCallback = this.shutdownCallback;
        if (shutdownCallback != null) {
            shutdownCallback.shutdown();
        }
    }

    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        String str = TAG;
        Log.d(str, "Handle message: " + message.what);
        int i = message.what;
        switch (i) {
            case 0:
                startWifiDirect();
                return false;
            case 1:
                shutdown();
                return false;
            case 2:
                startNetworkClient((String) message.obj);
                return false;
            case 3:
                stopNetworkClient();
                return false;
            case 4:
                stopServiceDiscovery();
                connectToService((String) message.obj, message.arg1);
                return false;
            case 5:
                stopNetworkClient();
                stopWifiDirect();
                startWifiDirect();
                return false;
            case 6:
                startIpExchange((String) message.obj);
                return false;
            case 7:
                ipExchangeSuccessful((String) message.obj);
                return false;
            case 8:
                NetworkClientThread networkClientThread = this.clientThread;
                if (networkClientThread == null) {
                    return false;
                }
                networkClientThread.setVerified(((Boolean) message.obj).booleanValue());
                return false;
            case 9:
                requestNetworkInfo(((Boolean) message.obj).booleanValue());
                return false;
            default:
                switch (i) {
                    case 1001:
                        update(TransferStatus.serviceConnected());
                        return false;
                    case 1002:
                        update(TransferStatus.networkConnected());
                        return false;
                    case 1003:
                        update(TransferStatus.verificationRequired((Integer) message.obj));
                        return false;
                    case 1004:
                        update(TransferStatus.shutdown());
                        internalShutdown();
                        return false;
                    default:
                        internalShutdown();
                        throw new AssertionError("Unknown message: " + message.what);
                }
        }
    }

    private void startWifiDirect() {
        if (this.wifiDirect != null) {
            Log.e(TAG, "Client already started");
            return;
        }
        update(TransferStatus.startingUp());
        try {
            WifiDirect wifiDirect = new WifiDirect(this.context);
            this.wifiDirect = wifiDirect;
            wifiDirect.initialize(new WifiDirectListener());
            this.wifiDirect.discoverService();
            Log.i(TAG, "Started service discovery, searching for service...");
            update(TransferStatus.discovery());
            this.handler.postDelayed(this.autoRestart, TimeUnit.SECONDS.toMillis(15));
        } catch (WifiDirectUnavailableException e) {
            Log.e(TAG, e);
            internalShutdown();
            if (e.getReason() == WifiDirectUnavailableException.Reason.CHANNEL_INITIALIZATION || e.getReason() == WifiDirectUnavailableException.Reason.WIFI_P2P_MANAGER) {
                update(TransferStatus.unavailable());
            } else {
                update(TransferStatus.failed());
            }
        }
    }

    private void stopServiceDiscovery() {
        if (this.wifiDirect != null) {
            try {
                Log.i(TAG, "Stopping service discovery");
                this.wifiDirect.stopServiceDiscovery();
            } catch (WifiDirectUnavailableException unused) {
                internalShutdown();
                update(TransferStatus.failed());
            }
        }
    }

    private void stopWifiDirect() {
        this.handler.removeCallbacks(this.autoRestart);
        if (this.wifiDirect != null) {
            Log.i(TAG, "Shutting down WiFi Direct");
            this.wifiDirect.shutdown();
            this.wifiDirect = null;
        }
    }

    private void startNetworkClient(String str) {
        if (this.clientThread != null) {
            Log.i(TAG, "Client already running");
            return;
        }
        Log.i(TAG, "Connection established, spinning up network client.");
        NetworkClientThread networkClientThread = new NetworkClientThread(this.context, this.clientTask, str, this.remotePort, this.handler);
        this.clientThread = networkClientThread;
        networkClientThread.start();
    }

    private void stopNetworkClient() {
        if (this.clientThread != null) {
            Log.i(TAG, "Shutting down ClientThread");
            this.clientThread.shutdown();
            try {
                this.clientThread.join(TimeUnit.SECONDS.toMillis(1));
            } catch (InterruptedException e) {
                Log.i(TAG, "Client thread took too long to shutdown", e);
            }
            this.clientThread = null;
        }
    }

    private void connectToService(String str, int i) {
        if (this.wifiDirect == null) {
            Log.w(TAG, "WifiDirect is not initialized, we shouldn't be here.");
        } else if (this.clientThread != null) {
            Log.i(TAG, "Client is running we shouldn't be connecting again");
        } else {
            this.handler.removeCallbacks(this.autoRestart);
            int i2 = 5;
            while (true) {
                int i3 = i2 - 1;
                if (i2 > 0) {
                    try {
                        this.wifiDirect.connect(str);
                        update(TransferStatus.networkConnected());
                        this.remotePort = i;
                        return;
                    } catch (WifiDirectUnavailableException unused) {
                        Log.w(TAG, "Unable to connect, tries: " + i3);
                        try {
                            Thread.sleep(TimeUnit.SECONDS.toMillis(2));
                            i2 = i3;
                        } catch (InterruptedException unused2) {
                            Log.i(TAG, "Interrupted while connecting to service, bail now!");
                            return;
                        }
                    }
                } else {
                    Handler handler = this.handler;
                    handler.sendMessage(handler.obtainMessage(5));
                    return;
                }
            }
        }
    }

    private void requestNetworkInfo(boolean z) {
        if (this.wifiDirect != null) {
            if (z) {
                Log.i(TAG, "Network connected, requesting network info");
                try {
                    this.wifiDirect.requestNetworkInfo();
                } catch (WifiDirectUnavailableException e) {
                    Log.e(TAG, e);
                    internalShutdown();
                    update(TransferStatus.failed());
                }
            } else {
                Log.i(TAG, "Network disconnected");
                this.handler.sendEmptyMessage(3);
            }
        }
    }

    private void startIpExchange(String str) {
        this.ipExchangeThread = IpExchange.getIp(str, this.remotePort, this.handler, 7);
    }

    private void stopIpExchange() {
        IpExchange.IpExchangeThread ipExchangeThread = this.ipExchangeThread;
        if (ipExchangeThread != null) {
            ipExchangeThread.shutdown();
            try {
                this.ipExchangeThread.join(TimeUnit.SECONDS.toMillis(1));
            } catch (InterruptedException e) {
                Log.i(TAG, "IP Exchange thread took too long to shutdown", e);
            }
            this.ipExchangeThread = null;
        }
    }

    private void ipExchangeSuccessful(String str) {
        stopIpExchange();
        Handler handler = this.handler;
        handler.sendMessage(handler.obtainMessage(2, str));
    }

    /* loaded from: classes3.dex */
    public final class WifiDirectListener implements WifiDirect.WifiDirectConnectionListener {
        WifiDirectListener() {
            DeviceTransferClient.this = r1;
        }

        @Override // org.signal.devicetransfer.WifiDirect.WifiDirectConnectionListener
        public void onServiceDiscovered(WifiP2pDevice wifiP2pDevice, String str) {
            DeviceTransferClient.this.handler.sendMessage(DeviceTransferClient.this.handler.obtainMessage(4, Integer.parseInt(str), 0, wifiP2pDevice.deviceAddress));
        }

        @Override // org.signal.devicetransfer.WifiDirect.WifiDirectConnectionListener
        public void onNetworkConnected(WifiP2pInfo wifiP2pInfo) {
            if (wifiP2pInfo.isGroupOwner) {
                DeviceTransferClient.this.handler.sendEmptyMessage(6);
            } else {
                DeviceTransferClient.this.handler.sendMessage(DeviceTransferClient.this.handler.obtainMessage(2, wifiP2pInfo.groupOwnerAddress.getHostAddress()));
            }
        }

        @Override // org.signal.devicetransfer.WifiDirect.WifiDirectConnectionListener
        public void onNetworkFailure() {
            DeviceTransferClient.this.handler.sendEmptyMessage(3);
        }

        @Override // org.signal.devicetransfer.WifiDirect.WifiDirectConnectionListener
        public void onConnectionChanged(NetworkInfo networkInfo) {
            DeviceTransferClient.this.handler.sendMessage(DeviceTransferClient.this.handler.obtainMessage(9, Boolean.valueOf(networkInfo.isConnected())));
        }
    }
}
