package org.signal.devicetransfer;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.signal.libsignal.devicetransfer.DeviceTransferKey;

/* loaded from: classes3.dex */
public final class SelfSignedIdentity {
    public static SelfSignedKeys create() throws KeyGenerationFailedException {
        try {
            DeviceTransferKey deviceTransferKey = new DeviceTransferKey();
            return new SelfSignedKeys(deviceTransferKey.generateCertificate("SignalTransfer", 1), KeyFactory.getInstance("RSA").generatePrivate(new PKCS8EncodedKeySpec(deviceTransferKey.keyMaterial())));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new KeyGenerationFailedException(e);
        }
    }

    public static SSLServerSocketFactory getServerSocketFactory(SelfSignedKeys selfSignedKeys) throws GeneralSecurityException, IOException {
        Certificate generateCertificate = CertificateFactory.getInstance("X509").generateCertificate(new ByteArrayInputStream(selfSignedKeys.getX509Encoded()));
        KeyStore instance = KeyStore.getInstance("BKS");
        instance.load(null);
        instance.setKeyEntry("client", selfSignedKeys.getPrivateKey(), null, new Certificate[]{generateCertificate});
        KeyManagerFactory instance2 = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        instance2.init(instance, null);
        SSLContext instance3 = SSLContext.getInstance("TLS");
        instance3.init(instance2.getKeyManagers(), null, new SecureRandom());
        return instance3.getServerSocketFactory();
    }

    public static SSLSocketFactory getApprovingSocketFactory(ApprovingTrustManager approvingTrustManager) throws GeneralSecurityException {
        SSLContext instance = SSLContext.getInstance("TLS");
        instance.init(null, new TrustManager[]{approvingTrustManager}, new SecureRandom());
        return instance.getSocketFactory();
    }

    /* loaded from: classes3.dex */
    public static final class SelfSignedKeys {
        private final PrivateKey privateKey;
        private final byte[] x509Encoded;

        public SelfSignedKeys(byte[] bArr, PrivateKey privateKey) {
            this.x509Encoded = bArr;
            this.privateKey = privateKey;
        }

        public byte[] getX509Encoded() {
            return this.x509Encoded;
        }

        public PrivateKey getPrivateKey() {
            return this.privateKey;
        }
    }

    /* loaded from: classes3.dex */
    static final class ApprovingTrustManager implements X509TrustManager {
        private X509Certificate x509Certificate;

        @Override // javax.net.ssl.X509TrustManager
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }

        @Override // javax.net.ssl.X509TrustManager
        public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
            throw new CertificateException();
        }

        @Override // javax.net.ssl.X509TrustManager
        public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
            if (x509CertificateArr.length == 1) {
                this.x509Certificate = x509CertificateArr[0];
                return;
            }
            throw new CertificateException("More than 1 x509 certificate");
        }

        public X509Certificate getX509Certificate() {
            return this.x509Certificate;
        }
    }
}
