package org.signal.devicetransfer;

/* loaded from: classes3.dex */
public final class WifiDirectUnavailableException extends Exception {
    private final Reason reason;

    /* loaded from: classes3.dex */
    public enum Reason {
        WIFI_P2P_MANAGER,
        CHANNEL_INITIALIZATION,
        SERVICE_DISCOVERY_START,
        SERVICE_START,
        SERVICE_CONNECT_FAILURE,
        SERVICE_CREATE_GROUP,
        SERVICE_NOT_INITIALIZED
    }

    public WifiDirectUnavailableException(Reason reason) {
        this.reason = reason;
    }

    public Reason getReason() {
        return this.reason;
    }
}
