package org.signal.devicetransfer;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PowerManager;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import java.util.concurrent.TimeUnit;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.signal.devicetransfer.TransferStatus;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;

/* loaded from: classes.dex */
public class DeviceToDeviceTransferService extends Service implements ShutdownCallback {
    private static final String TAG = Log.tag(DeviceToDeviceTransferService.class);
    private DeviceTransferClient client;
    private TransferNotificationData notificationData;
    private PendingIntent pendingIntent;
    private DeviceTransferServer server;
    private PowerManager.WakeLock wakeLock;

    public static void startServer(Context context, ServerTask serverTask, TransferNotificationData transferNotificationData, PendingIntent pendingIntent) {
        Intent intent = new Intent(context, DeviceToDeviceTransferService.class);
        intent.setAction(NotificationProfileDatabase.NotificationProfileScheduleTable.START).putExtra("extra_task", serverTask).putExtra("extra_notification_data", transferNotificationData).putExtra("extra_pending_intent", pendingIntent);
        context.startService(intent);
    }

    public static void startClient(Context context, ClientTask clientTask, TransferNotificationData transferNotificationData, PendingIntent pendingIntent) {
        Intent intent = new Intent(context, DeviceToDeviceTransferService.class);
        intent.setAction("start_client").putExtra("extra_task", clientTask).putExtra("extra_notification_data", transferNotificationData).putExtra("extra_pending_intent", pendingIntent);
        context.startService(intent);
    }

    public static void setAuthenticationCodeVerified(Context context, boolean z) {
        Intent intent = new Intent(context, DeviceToDeviceTransferService.class);
        intent.setAction("set_verified").putExtra("is_verified", z);
        context.startService(intent);
    }

    public static void stop(Context context) {
        context.startService(new Intent(context, DeviceToDeviceTransferService.class).setAction("stop"));
    }

    @Override // android.app.Service
    public void onCreate() {
        super.onCreate();
        Log.v(TAG, "onCreate");
        EventBus.getDefault().register(this);
    }

    @Subscribe(sticky = BuildConfig.PLAY_STORE_DISABLED, threadMode = ThreadMode.MAIN)
    public void onEventMainThread(TransferStatus transferStatus) {
        updateNotification(transferStatus);
    }

    @Override // android.app.Service
    public void onDestroy() {
        Log.v(TAG, "onDestroy");
        EventBus.getDefault().unregister(this);
        DeviceTransferClient deviceTransferClient = this.client;
        if (deviceTransferClient != null) {
            deviceTransferClient.stop();
            this.client = null;
        }
        DeviceTransferServer deviceTransferServer = this.server;
        if (deviceTransferServer != null) {
            deviceTransferServer.stop();
            this.server = null;
        }
        PowerManager.WakeLock wakeLock = this.wakeLock;
        if (wakeLock != null) {
            wakeLock.release();
        }
        super.onDestroy();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004c, code lost:
        if (r8.equals(org.thoughtcrime.securesms.database.NotificationProfileDatabase.NotificationProfileScheduleTable.START) == false) goto L_0x0037;
     */
    @Override // android.app.Service
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int onStartCommand(android.content.Intent r6, int r7, int r8) {
        /*
        // Method dump skipped, instructions count: 280
        */
        throw new UnsupportedOperationException("Method not decompiled: org.signal.devicetransfer.DeviceToDeviceTransferService.onStartCommand(android.content.Intent, int, int):int");
    }

    @Override // org.signal.devicetransfer.ShutdownCallback
    public void shutdown() {
        Log.i(TAG, "Shutdown");
        ThreadUtil.runOnMain(new Runnable() { // from class: org.signal.devicetransfer.DeviceToDeviceTransferService$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                DeviceToDeviceTransferService.$r8$lambda$bssneNPeuuVYCNLe5vxUF8tfx0M(DeviceToDeviceTransferService.this);
            }
        });
    }

    public /* synthetic */ void lambda$shutdown$0() {
        stopForeground(true);
        stopSelf();
    }

    private void acquireWakeLock() {
        PowerManager powerManager;
        if (this.wakeLock == null && (powerManager = (PowerManager) ContextCompat.getSystemService(this, PowerManager.class)) != null) {
            this.wakeLock = powerManager.newWakeLock(1, "signal:d2dpartial");
        }
        if (!this.wakeLock.isHeld()) {
            this.wakeLock.acquire(TimeUnit.HOURS.toMillis(2));
        }
    }

    private void updateNotification(TransferStatus transferStatus) {
        TransferNotificationData transferNotificationData = this.notificationData;
        if (transferNotificationData == null) {
            return;
        }
        if (this.client != null || this.server != null) {
            startForeground(transferNotificationData.notificationId, createNotification(transferStatus, this.notificationData));
        }
    }

    private Notification createNotification(TransferStatus transferStatus, TransferNotificationData transferNotificationData) {
        String str;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, transferNotificationData.channelId);
        switch (AnonymousClass1.$SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode[transferStatus.getTransferMode().ordinal()]) {
            case 1:
                str = getString(R$string.DeviceToDeviceTransferService_status_ready);
                break;
            case 2:
                str = getString(R$string.DeviceToDeviceTransferService_status_starting_up);
                break;
            case 3:
                str = getString(R$string.DeviceToDeviceTransferService_status_discovery);
                break;
            case 4:
                str = getString(R$string.DeviceToDeviceTransferService_status_network_connected);
                break;
            case 5:
                str = getString(R$string.DeviceToDeviceTransferService_status_verification_required);
                break;
            case 6:
                str = getString(R$string.DeviceToDeviceTransferService_status_service_connected);
                break;
            case 7:
            case 8:
            case 9:
            case 10:
                String str2 = TAG;
                Log.d(str2, "Intentionally no notification text for: " + transferStatus.getTransferMode());
                str = "";
                break;
            default:
                throw new AssertionError("No notification text for: " + transferStatus.getTransferMode());
        }
        builder.setSmallIcon(transferNotificationData.icon).setOngoing(true).setContentTitle(getString(R$string.DeviceToDeviceTransferService_content_title)).setContentText(str).setContentIntent(this.pendingIntent);
        return builder.build();
    }

    /* renamed from: org.signal.devicetransfer.DeviceToDeviceTransferService$1 */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode;

        static {
            int[] iArr = new int[TransferStatus.TransferMode.values().length];
            $SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode = iArr;
            try {
                iArr[TransferStatus.TransferMode.READY.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode[TransferStatus.TransferMode.STARTING_UP.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode[TransferStatus.TransferMode.DISCOVERY.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode[TransferStatus.TransferMode.NETWORK_CONNECTED.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode[TransferStatus.TransferMode.VERIFICATION_REQUIRED.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode[TransferStatus.TransferMode.SERVICE_CONNECTED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode[TransferStatus.TransferMode.UNAVAILABLE.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode[TransferStatus.TransferMode.FAILED.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode[TransferStatus.TransferMode.SERVICE_DISCONNECTED.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode[TransferStatus.TransferMode.SHUTDOWN.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
        }
    }

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException();
    }

    /* loaded from: classes3.dex */
    public static class TransferNotificationData implements Parcelable {
        public static final Parcelable.Creator<TransferNotificationData> CREATOR = new Parcelable.Creator<TransferNotificationData>() { // from class: org.signal.devicetransfer.DeviceToDeviceTransferService.TransferNotificationData.1
            @Override // android.os.Parcelable.Creator
            public TransferNotificationData createFromParcel(Parcel parcel) {
                return new TransferNotificationData(parcel.readInt(), parcel.readString(), parcel.readInt());
            }

            @Override // android.os.Parcelable.Creator
            public TransferNotificationData[] newArray(int i) {
                return new TransferNotificationData[i];
            }
        };
        private final String channelId;
        private final int icon;
        private final int notificationId;

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        public TransferNotificationData(int i, String str, int i2) {
            this.notificationId = i;
            this.channelId = str;
            this.icon = i2;
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.notificationId);
            parcel.writeString(this.channelId);
            parcel.writeInt(this.icon);
        }
    }
}
