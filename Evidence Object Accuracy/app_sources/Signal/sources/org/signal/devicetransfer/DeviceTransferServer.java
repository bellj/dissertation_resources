package org.signal.devicetransfer;

import android.content.Context;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.greenrobot.eventbus.EventBus;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.signal.devicetransfer.IpExchange;
import org.signal.devicetransfer.SelfSignedIdentity;
import org.signal.devicetransfer.WifiDirect;
import org.signal.devicetransfer.WifiDirectUnavailableException;

/* access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public final class DeviceTransferServer implements Handler.Callback {
    private static final String TAG = Log.tag(DeviceTransferServer.class);
    private HandlerThread commandAndControlThread;
    private final Context context;
    private final Handler handler;
    private IpExchange.IpExchangeThread ipExchangeThread;
    private final ServerTask serverTask;
    private NetworkServerThread serverThread;
    private final ShutdownCallback shutdownCallback;
    private final AtomicBoolean started = new AtomicBoolean(false);
    private final AtomicBoolean stopped = new AtomicBoolean(false);
    private WifiDirect wifiDirect;

    private static void update(TransferStatus transferStatus) {
        String str = TAG;
        Log.d(str, "transferStatus: " + transferStatus.getTransferMode().name());
        EventBus.getDefault().postSticky(transferStatus);
    }

    public DeviceTransferServer(Context context, ServerTask serverTask, ShutdownCallback shutdownCallback) {
        this.context = context;
        this.serverTask = serverTask;
        this.shutdownCallback = shutdownCallback;
        this.commandAndControlThread = SignalExecutors.getAndStartHandlerThread("server-cnc");
        this.handler = new Handler(this.commandAndControlThread.getLooper(), this);
    }

    public void start() {
        if (this.started.compareAndSet(false, true)) {
            update(TransferStatus.ready());
            this.handler.sendEmptyMessage(0);
        }
    }

    public void stop() {
        if (this.stopped.compareAndSet(false, true)) {
            this.handler.sendEmptyMessage(1);
        }
    }

    public void setVerified(boolean z) {
        if (!this.stopped.get()) {
            Handler handler = this.handler;
            handler.sendMessage(handler.obtainMessage(5, Boolean.valueOf(z)));
        }
    }

    private void shutdown() {
        stopIpExchange();
        stopServer();
        stopWifiDirect();
        if (this.commandAndControlThread != null) {
            Log.i(TAG, "Shutting down command and control");
            this.commandAndControlThread.quit();
            this.commandAndControlThread.interrupt();
            this.commandAndControlThread = null;
        }
        EventBus.getDefault().removeStickyEvent(TransferStatus.class);
    }

    private void internalShutdown() {
        shutdown();
        ShutdownCallback shutdownCallback = this.shutdownCallback;
        if (shutdownCallback != null) {
            shutdownCallback.shutdown();
        }
    }

    @Override // android.os.Handler.Callback
    public boolean handleMessage(Message message) {
        String str = TAG;
        Log.d(str, "Handle message: " + message.what);
        int i = message.what;
        if (i == 0) {
            startNetworkServer();
            return false;
        } else if (i == 1) {
            shutdown();
            return false;
        } else if (i == 2) {
            startIpExchange((String) message.obj);
            return false;
        } else if (i == 3) {
            ipExchangeSuccessful();
            return false;
        } else if (i == 5) {
            NetworkServerThread networkServerThread = this.serverThread;
            if (networkServerThread == null) {
                return false;
            }
            networkServerThread.setVerified(((Boolean) message.obj).booleanValue());
            return false;
        } else if (i != 6) {
            switch (i) {
                case 1001:
                    startWifiDirect(message.arg1);
                    return false;
                case 1002:
                    update(TransferStatus.shutdown());
                    internalShutdown();
                    return false;
                case 1003:
                    stopDiscoveryService();
                    update(TransferStatus.serviceConnected());
                    return false;
                case 1004:
                    update(TransferStatus.networkConnected());
                    return false;
                case 1005:
                    update(TransferStatus.verificationRequired((Integer) message.obj));
                    return false;
                default:
                    internalShutdown();
                    throw new AssertionError("Unknown message: " + message.what);
            }
        } else {
            requestNetworkInfo(((Boolean) message.obj).booleanValue());
            return false;
        }
    }

    private void startWifiDirect(int i) {
        if (this.wifiDirect != null) {
            Log.e(TAG, "Server already started");
            return;
        }
        try {
            WifiDirect wifiDirect = new WifiDirect(this.context);
            this.wifiDirect = wifiDirect;
            wifiDirect.initialize(new WifiDirectListener());
            this.wifiDirect.startDiscoveryService(String.valueOf(i));
            Log.i(TAG, "Started discovery service, waiting for connections...");
            update(TransferStatus.discovery());
        } catch (WifiDirectUnavailableException e) {
            Log.e(TAG, e);
            internalShutdown();
            if (e.getReason() == WifiDirectUnavailableException.Reason.CHANNEL_INITIALIZATION || e.getReason() == WifiDirectUnavailableException.Reason.WIFI_P2P_MANAGER) {
                update(TransferStatus.unavailable());
            } else {
                update(TransferStatus.failed());
            }
        }
    }

    private void stopDiscoveryService() {
        if (this.wifiDirect != null) {
            try {
                Log.i(TAG, "Stopping discovery service");
                this.wifiDirect.stopDiscoveryService();
            } catch (WifiDirectUnavailableException unused) {
                internalShutdown();
                update(TransferStatus.failed());
            }
        }
    }

    private void stopWifiDirect() {
        if (this.wifiDirect != null) {
            Log.i(TAG, "Shutting down WiFi Direct");
            this.wifiDirect.shutdown();
            this.wifiDirect = null;
        }
    }

    private void requestNetworkInfo(boolean z) {
        WifiDirect wifiDirect = this.wifiDirect;
        if (wifiDirect != null && z) {
            try {
                wifiDirect.requestNetworkInfo();
            } catch (WifiDirectUnavailableException e) {
                Log.e(TAG, e);
                internalShutdown();
                update(TransferStatus.failed());
            }
        }
    }

    private void startNetworkServer() {
        if (this.serverThread != null) {
            Log.i(TAG, "Server already running");
            return;
        }
        try {
            update(TransferStatus.startingUp());
            SelfSignedIdentity.SelfSignedKeys create = SelfSignedIdentity.create();
            Log.i(TAG, "Spinning up network server.");
            NetworkServerThread networkServerThread = new NetworkServerThread(this.context, this.serverTask, create, this.handler);
            this.serverThread = networkServerThread;
            networkServerThread.start();
        } catch (KeyGenerationFailedException e) {
            Log.w(TAG, "Error generating keys", e);
            internalShutdown();
            update(TransferStatus.failed());
        }
    }

    private void stopServer() {
        if (this.serverThread != null) {
            Log.i(TAG, "Shutting down ServerThread");
            this.serverThread.shutdown();
            try {
                this.serverThread.join(TimeUnit.SECONDS.toMillis(1));
            } catch (InterruptedException e) {
                Log.i(TAG, "Server thread took too long to shutdown", e);
            }
            this.serverThread = null;
        }
    }

    private void startIpExchange(String str) {
        this.ipExchangeThread = IpExchange.giveIp(str, this.serverThread.getLocalPort(), this.handler, 3);
    }

    private void stopIpExchange() {
        IpExchange.IpExchangeThread ipExchangeThread = this.ipExchangeThread;
        if (ipExchangeThread != null) {
            ipExchangeThread.shutdown();
            try {
                this.ipExchangeThread.join(TimeUnit.SECONDS.toMillis(1));
            } catch (InterruptedException e) {
                Log.i(TAG, "IP Exchange thread took too long to shutdown", e);
            }
            this.ipExchangeThread = null;
        }
    }

    private void ipExchangeSuccessful() {
        stopIpExchange();
    }

    /* loaded from: classes3.dex */
    public final class WifiDirectListener implements WifiDirect.WifiDirectConnectionListener {
        @Override // org.signal.devicetransfer.WifiDirect.WifiDirectConnectionListener
        public void onServiceDiscovered(WifiP2pDevice wifiP2pDevice, String str) {
        }

        WifiDirectListener() {
            DeviceTransferServer.this = r1;
        }

        @Override // org.signal.devicetransfer.WifiDirect.WifiDirectConnectionListener
        public void onNetworkConnected(WifiP2pInfo wifiP2pInfo) {
            if (!wifiP2pInfo.isGroupOwner) {
                DeviceTransferServer.this.handler.sendMessage(DeviceTransferServer.this.handler.obtainMessage(2, wifiP2pInfo.groupOwnerAddress.getHostAddress()));
            }
        }

        @Override // org.signal.devicetransfer.WifiDirect.WifiDirectConnectionListener
        public void onNetworkFailure() {
            DeviceTransferServer.this.handler.sendEmptyMessage(4);
        }

        @Override // org.signal.devicetransfer.WifiDirect.WifiDirectConnectionListener
        public void onConnectionChanged(NetworkInfo networkInfo) {
            DeviceTransferServer.this.handler.sendMessage(DeviceTransferServer.this.handler.obtainMessage(6, Boolean.valueOf(networkInfo.isConnected())));
        }
    }
}
