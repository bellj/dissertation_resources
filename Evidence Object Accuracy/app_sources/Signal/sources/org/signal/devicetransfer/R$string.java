package org.signal.devicetransfer;

/* loaded from: classes3.dex */
public final class R$string {
    public static final int DeviceToDeviceTransferService_content_title;
    public static final int DeviceToDeviceTransferService_status_discovery;
    public static final int DeviceToDeviceTransferService_status_network_connected;
    public static final int DeviceToDeviceTransferService_status_ready;
    public static final int DeviceToDeviceTransferService_status_service_connected;
    public static final int DeviceToDeviceTransferService_status_starting_up;
    public static final int DeviceToDeviceTransferService_status_verification_required;
}
