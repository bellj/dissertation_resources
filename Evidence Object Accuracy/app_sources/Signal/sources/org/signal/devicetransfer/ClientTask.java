package org.signal.devicetransfer;

import android.content.Context;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;

/* loaded from: classes3.dex */
public interface ClientTask extends Serializable {
    void run(Context context, OutputStream outputStream) throws IOException;

    void success();
}
