package org.signal.devicetransfer;

import android.content.Context;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

/* loaded from: classes3.dex */
public interface ServerTask extends Serializable {
    void run(Context context, InputStream inputStream) throws IOException;
}
