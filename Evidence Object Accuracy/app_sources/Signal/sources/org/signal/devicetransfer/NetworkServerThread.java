package org.signal.devicetransfer;

import android.content.Context;
import android.os.Handler;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.logging.Log;
import org.signal.devicetransfer.DeviceTransferAuthentication;
import org.signal.devicetransfer.SelfSignedIdentity;

/* loaded from: classes3.dex */
public final class NetworkServerThread extends Thread {
    private static final String TAG = Log.tag(NetworkServerThread.class);
    private volatile Socket clientSocket;
    private final Context context;
    private final Handler handler;
    private volatile boolean isRunning;
    private volatile Boolean isVerified;
    private final SelfSignedIdentity.SelfSignedKeys keys;
    private volatile ServerSocket serverSocket;
    private final ServerTask serverTask;
    private final Object verificationLock = new Object();

    public NetworkServerThread(Context context, ServerTask serverTask, SelfSignedIdentity.SelfSignedKeys selfSignedKeys, Handler handler) {
        this.context = context;
        this.serverTask = serverTask;
        this.keys = selfSignedKeys;
        this.handler = handler;
    }

    /* JADX INFO: finally extract failed */
    @Override // java.lang.Thread, java.lang.Runnable
    public void run() {
        Handler handler;
        try {
            String str = TAG;
            Log.i(str, "Server thread running");
            this.isRunning = true;
            Log.i(str, "Starting up server socket...");
            try {
                this.serverSocket = SelfSignedIdentity.getServerSocketFactory(this.keys).createServerSocket(0);
                Handler handler2 = this.handler;
                handler2.sendMessage(handler2.obtainMessage(1001, this.serverSocket.getLocalPort(), 0));
                while (true) {
                    if (!shouldKeepRunning() || this.serverSocket.isClosed()) {
                        break;
                    }
                    String str2 = TAG;
                    Log.i(str2, "Waiting for client socket accept...");
                    try {
                        try {
                            this.clientSocket = this.serverSocket.accept();
                        } catch (IOException e) {
                            if (this.isRunning) {
                                Log.i(TAG, "Error connecting with client or server socket closed.", e);
                            } else {
                                Log.i(TAG, "Server shutting down...");
                            }
                            StreamUtil.close(this.clientSocket);
                            handler = this.handler;
                        }
                        if (this.isRunning) {
                            InputStream inputStream = this.clientSocket.getInputStream();
                            OutputStream outputStream = this.clientSocket.getOutputStream();
                            int generateServerAuthenticationCode = DeviceTransferAuthentication.generateServerAuthenticationCode(this.keys.getX509Encoded(), inputStream, outputStream);
                            Handler handler3 = this.handler;
                            handler3.sendMessage(handler3.obtainMessage(1005, Integer.valueOf(generateServerAuthenticationCode)));
                            Log.i(str2, "Waiting for user to verify sas");
                            awaitAuthenticationCodeVerification();
                            Log.d(str2, "Waiting for client to tell us they also verified");
                            outputStream.write(67);
                            outputStream.flush();
                            try {
                                if (inputStream.read() == -1) {
                                    Log.w(str2, "Something happened waiting for client to verify");
                                    throw new DeviceTransferAuthentication.DeviceTransferAuthenticationException("client disconnected while we waited");
                                    break;
                                }
                                this.handler.sendEmptyMessage(1003);
                                this.serverTask.run(this.context, inputStream);
                                outputStream.write(83);
                                outputStream.flush();
                                StreamUtil.close(this.clientSocket);
                                handler = this.handler;
                                handler.sendEmptyMessage(1004);
                            } catch (IOException e2) {
                                Log.w(TAG, "Something happened waiting for client to verify", e2);
                                throw new DeviceTransferAuthentication.DeviceTransferAuthenticationException(e2);
                                break;
                            }
                        } else {
                            break;
                        }
                    } finally {
                        StreamUtil.close(this.clientSocket);
                        this.handler.sendEmptyMessage(1004);
                    }
                }
            } catch (Exception e3) {
                Log.w(TAG, e3);
            }
            StreamUtil.close(this.serverSocket);
            Log.i(TAG, "Server exiting");
            this.isRunning = false;
            this.handler.sendEmptyMessage(1002);
        } catch (Throwable th) {
            StreamUtil.close(this.serverSocket);
            throw th;
        }
    }

    private void awaitAuthenticationCodeVerification() throws DeviceTransferAuthentication.DeviceTransferAuthenticationException {
        synchronized (this.verificationLock) {
            while (this.isVerified == null) {
                try {
                    this.verificationLock.wait();
                } catch (InterruptedException e) {
                    throw new DeviceTransferAuthentication.DeviceTransferAuthenticationException(e);
                }
            }
            if (!this.isVerified.booleanValue()) {
                throw new DeviceTransferAuthentication.DeviceTransferAuthenticationException("User verification failed");
            }
        }
    }

    private boolean shouldKeepRunning() {
        return !isInterrupted() && this.isRunning;
    }

    public int getLocalPort() {
        ServerSocket serverSocket = this.serverSocket;
        if (serverSocket != null) {
            return serverSocket.getLocalPort();
        }
        return 0;
    }

    public void setVerified(boolean z) {
        this.isVerified = Boolean.valueOf(z);
        synchronized (this.verificationLock) {
            this.verificationLock.notify();
        }
    }

    public void shutdown() {
        this.isRunning = false;
        StreamUtil.close(this.clientSocket);
        StreamUtil.close(this.serverSocket);
        interrupt();
    }
}
