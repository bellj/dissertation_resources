package org.signal.devicetransfer;

/* loaded from: classes3.dex */
public class TransferStatus {
    private final int authenticationCode;
    private final TransferMode transferMode;

    /* loaded from: classes3.dex */
    public enum TransferMode {
        UNAVAILABLE,
        FAILED,
        READY,
        STARTING_UP,
        DISCOVERY,
        NETWORK_CONNECTED,
        VERIFICATION_REQUIRED,
        SERVICE_CONNECTED,
        SERVICE_DISCONNECTED,
        SHUTDOWN
    }

    private TransferStatus(TransferMode transferMode) {
        this(transferMode, 0);
    }

    private TransferStatus(int i) {
        this(TransferMode.VERIFICATION_REQUIRED, i);
    }

    private TransferStatus(TransferMode transferMode, int i) {
        this.transferMode = transferMode;
        this.authenticationCode = i;
    }

    public TransferMode getTransferMode() {
        return this.transferMode;
    }

    public int getAuthenticationCode() {
        return this.authenticationCode;
    }

    public static TransferStatus ready() {
        return new TransferStatus(TransferMode.READY);
    }

    public static TransferStatus serviceConnected() {
        return new TransferStatus(TransferMode.SERVICE_CONNECTED);
    }

    public static TransferStatus networkConnected() {
        return new TransferStatus(TransferMode.NETWORK_CONNECTED);
    }

    public static TransferStatus verificationRequired(Integer num) {
        return new TransferStatus(num.intValue());
    }

    public static TransferStatus startingUp() {
        return new TransferStatus(TransferMode.STARTING_UP);
    }

    public static TransferStatus discovery() {
        return new TransferStatus(TransferMode.DISCOVERY);
    }

    public static TransferStatus unavailable() {
        return new TransferStatus(TransferMode.UNAVAILABLE);
    }

    public static TransferStatus shutdown() {
        return new TransferStatus(TransferMode.SHUTDOWN);
    }

    public static TransferStatus failed() {
        return new TransferStatus(TransferMode.FAILED);
    }
}
