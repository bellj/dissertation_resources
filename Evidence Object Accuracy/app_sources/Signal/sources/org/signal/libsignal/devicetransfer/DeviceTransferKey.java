package org.signal.libsignal.devicetransfer;

import org.signal.libsignal.internal.Native;

/* loaded from: classes3.dex */
public class DeviceTransferKey {
    byte[] keyMaterial = Native.DeviceTransfer_GeneratePrivateKey();

    public byte[] keyMaterial() {
        return this.keyMaterial;
    }

    public byte[] generateCertificate(String str, int i) {
        return Native.DeviceTransfer_GenerateCertificate(this.keyMaterial, str, i);
    }
}
