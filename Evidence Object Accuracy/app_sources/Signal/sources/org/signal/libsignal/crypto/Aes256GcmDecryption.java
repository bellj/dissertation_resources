package org.signal.libsignal.crypto;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;
import org.signal.libsignal.protocol.InvalidKeyException;

/* loaded from: classes3.dex */
public class Aes256GcmDecryption implements NativeHandleGuard.Owner {
    private long unsafeHandle;

    public Aes256GcmDecryption(byte[] bArr, byte[] bArr2, byte[] bArr3) throws InvalidKeyException {
        this.unsafeHandle = Native.Aes256GcmDecryption_New(bArr, bArr2, bArr3);
    }

    protected void finalize() {
        Native.Aes256GcmDecryption_Destroy(this.unsafeHandle);
    }

    @Override // org.signal.libsignal.internal.NativeHandleGuard.Owner
    public long unsafeNativeHandleWithoutGuard() {
        return this.unsafeHandle;
    }

    public void decrypt(byte[] bArr, int i, int i2) {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            Native.Aes256GcmDecryption_Update(nativeHandleGuard.nativeHandle(), bArr, i, i2);
            nativeHandleGuard.close();
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public boolean verifyTag(byte[] bArr) {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            boolean Aes256GcmDecryption_VerifyTag = Native.Aes256GcmDecryption_VerifyTag(nativeHandleGuard.nativeHandle(), bArr);
            Native.Aes256GcmDecryption_Destroy(nativeHandleGuard.nativeHandle());
            this.unsafeHandle = 0;
            nativeHandleGuard.close();
            return Aes256GcmDecryption_VerifyTag;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }
}
