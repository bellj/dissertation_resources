package org.signal.libsignal.protocol.util;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/* loaded from: classes3.dex */
public class KeyHelper {
    private KeyHelper() {
    }

    public static int generateRegistrationId(boolean z) {
        try {
            SecureRandom instance = SecureRandom.getInstance("SHA1PRNG");
            if (z) {
                return instance.nextInt(2147483646) + 1;
            }
            return instance.nextInt(16380) + 1;
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }
}
