package org.signal.libsignal.protocol.fingerprint;

import org.signal.libsignal.internal.Native;

/* loaded from: classes3.dex */
public class ScannableFingerprint {
    private final byte[] encodedFingerprint;

    public ScannableFingerprint(byte[] bArr) {
        this.encodedFingerprint = bArr;
    }

    public byte[] getSerialized() {
        return this.encodedFingerprint;
    }

    public boolean compareTo(byte[] bArr) throws FingerprintVersionMismatchException, FingerprintParsingException {
        return Native.ScannableFingerprint_Compare(this.encodedFingerprint, bArr);
    }
}
