package org.signal.libsignal.protocol;

/* loaded from: classes3.dex */
public class InvalidVersionException extends Exception {
    public InvalidVersionException(String str) {
        super(str);
    }
}
