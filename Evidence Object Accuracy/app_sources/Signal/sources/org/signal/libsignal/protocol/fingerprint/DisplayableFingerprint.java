package org.signal.libsignal.protocol.fingerprint;

/* loaded from: classes3.dex */
public class DisplayableFingerprint {
    private String displayString;

    public DisplayableFingerprint(String str) {
        this.displayString = str;
    }

    public String getDisplayText() {
        return this.displayString;
    }
}
