package org.signal.libsignal.protocol.fingerprint;

/* loaded from: classes3.dex */
public class FingerprintVersionMismatchException extends Exception {
    private final int ourVersion;
    private final int theirVersion;

    public FingerprintVersionMismatchException(int i, int i2) {
        this.theirVersion = i;
        this.ourVersion = i2;
    }

    public int getTheirVersion() {
        return this.theirVersion;
    }

    public int getOurVersion() {
        return this.ourVersion;
    }
}
