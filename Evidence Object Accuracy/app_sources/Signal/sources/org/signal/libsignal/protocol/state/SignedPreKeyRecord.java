package org.signal.libsignal.protocol.state;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.ecc.ECKeyPair;
import org.signal.libsignal.protocol.ecc.ECPrivateKey;
import org.signal.libsignal.protocol.ecc.ECPublicKey;

/* loaded from: classes3.dex */
public class SignedPreKeyRecord implements NativeHandleGuard.Owner {
    private final long unsafeHandle;

    protected void finalize() {
        Native.SignedPreKeyRecord_Destroy(this.unsafeHandle);
    }

    public SignedPreKeyRecord(int i, long j, ECKeyPair eCKeyPair, byte[] bArr) {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(eCKeyPair.getPublicKey());
        try {
            NativeHandleGuard nativeHandleGuard2 = new NativeHandleGuard(eCKeyPair.getPrivateKey());
            this.unsafeHandle = Native.SignedPreKeyRecord_New(i, j, nativeHandleGuard.nativeHandle(), nativeHandleGuard2.nativeHandle(), bArr);
            nativeHandleGuard2.close();
            nativeHandleGuard.close();
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public SignedPreKeyRecord(byte[] bArr) throws InvalidMessageException {
        this.unsafeHandle = Native.SignedPreKeyRecord_Deserialize(bArr);
    }

    public int getId() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int SignedPreKeyRecord_GetId = Native.SignedPreKeyRecord_GetId(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SignedPreKeyRecord_GetId;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public long getTimestamp() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            long SignedPreKeyRecord_GetTimestamp = Native.SignedPreKeyRecord_GetTimestamp(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SignedPreKeyRecord_GetTimestamp;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public ECKeyPair getKeyPair() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            ECKeyPair eCKeyPair = new ECKeyPair(new ECPublicKey(Native.SignedPreKeyRecord_GetPublicKey(nativeHandleGuard.nativeHandle())), new ECPrivateKey(Native.SignedPreKeyRecord_GetPrivateKey(nativeHandleGuard.nativeHandle())));
            nativeHandleGuard.close();
            return eCKeyPair;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] getSignature() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] SignedPreKeyRecord_GetSignature = Native.SignedPreKeyRecord_GetSignature(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SignedPreKeyRecord_GetSignature;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] serialize() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] SignedPreKeyRecord_GetSerialized = Native.SignedPreKeyRecord_GetSerialized(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SignedPreKeyRecord_GetSerialized;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    @Override // org.signal.libsignal.internal.NativeHandleGuard.Owner
    public long unsafeNativeHandleWithoutGuard() {
        return this.unsafeHandle;
    }
}
