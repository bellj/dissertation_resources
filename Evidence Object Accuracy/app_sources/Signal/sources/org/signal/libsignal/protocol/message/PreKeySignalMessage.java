package org.signal.libsignal.protocol.message;

import j$.util.Optional;
import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.InvalidVersionException;
import org.signal.libsignal.protocol.LegacyMessageException;
import org.signal.libsignal.protocol.ecc.ECPublicKey;

/* loaded from: classes3.dex */
public class PreKeySignalMessage implements CiphertextMessage, NativeHandleGuard.Owner {
    private final long unsafeHandle;

    @Override // org.signal.libsignal.protocol.message.CiphertextMessage
    public int getType() {
        return 3;
    }

    protected void finalize() {
        Native.PreKeySignalMessage_Destroy(this.unsafeHandle);
    }

    public PreKeySignalMessage(byte[] bArr) throws InvalidMessageException, InvalidVersionException, LegacyMessageException, InvalidKeyException {
        this.unsafeHandle = Native.PreKeySignalMessage_Deserialize(bArr);
    }

    public PreKeySignalMessage(long j) {
        this.unsafeHandle = j;
    }

    public int getMessageVersion() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int PreKeySignalMessage_GetVersion = Native.PreKeySignalMessage_GetVersion(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return PreKeySignalMessage_GetVersion;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public IdentityKey getIdentityKey() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            IdentityKey identityKey = new IdentityKey(Native.PreKeySignalMessage_GetIdentityKey(nativeHandleGuard.nativeHandle()));
            nativeHandleGuard.close();
            return identityKey;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public int getRegistrationId() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int PreKeySignalMessage_GetRegistrationId = Native.PreKeySignalMessage_GetRegistrationId(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return PreKeySignalMessage_GetRegistrationId;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public Optional<Integer> getPreKeyId() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int PreKeySignalMessage_GetPreKeyId = Native.PreKeySignalMessage_GetPreKeyId(nativeHandleGuard.nativeHandle());
            if (PreKeySignalMessage_GetPreKeyId < 0) {
                Optional<Integer> empty = Optional.empty();
                nativeHandleGuard.close();
                return empty;
            }
            Optional<Integer> of = Optional.of(Integer.valueOf(PreKeySignalMessage_GetPreKeyId));
            nativeHandleGuard.close();
            return of;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public int getSignedPreKeyId() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int PreKeySignalMessage_GetSignedPreKeyId = Native.PreKeySignalMessage_GetSignedPreKeyId(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return PreKeySignalMessage_GetSignedPreKeyId;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public ECPublicKey getBaseKey() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            ECPublicKey eCPublicKey = new ECPublicKey(Native.PreKeySignalMessage_GetBaseKey(nativeHandleGuard.nativeHandle()));
            nativeHandleGuard.close();
            return eCPublicKey;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public SignalMessage getWhisperMessage() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            SignalMessage signalMessage = new SignalMessage(Native.PreKeySignalMessage_GetSignalMessage(nativeHandleGuard.nativeHandle()));
            nativeHandleGuard.close();
            return signalMessage;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    @Override // org.signal.libsignal.protocol.message.CiphertextMessage
    public byte[] serialize() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] PreKeySignalMessage_GetSerialized = Native.PreKeySignalMessage_GetSerialized(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return PreKeySignalMessage_GetSerialized;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    @Override // org.signal.libsignal.protocol.message.CiphertextMessage, org.signal.libsignal.internal.NativeHandleGuard.Owner
    public long unsafeNativeHandleWithoutGuard() {
        return this.unsafeHandle;
    }
}
