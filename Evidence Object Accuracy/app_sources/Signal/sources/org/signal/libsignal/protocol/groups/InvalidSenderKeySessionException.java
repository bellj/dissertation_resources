package org.signal.libsignal.protocol.groups;

import java.util.UUID;

/* loaded from: classes3.dex */
public class InvalidSenderKeySessionException extends IllegalStateException {
    private final UUID distributionId;

    public InvalidSenderKeySessionException(UUID uuid, String str) {
        super(str);
        this.distributionId = uuid;
    }

    public UUID getDistributionId() {
        return this.distributionId;
    }
}
