package org.signal.libsignal.protocol.fingerprint;

/* loaded from: classes3.dex */
public class Fingerprint {
    private final DisplayableFingerprint displayableFingerprint;
    private final ScannableFingerprint scannableFingerprint;

    public Fingerprint(DisplayableFingerprint displayableFingerprint, ScannableFingerprint scannableFingerprint) {
        this.displayableFingerprint = displayableFingerprint;
        this.scannableFingerprint = scannableFingerprint;
    }

    public DisplayableFingerprint getDisplayableFingerprint() {
        return this.displayableFingerprint;
    }

    public ScannableFingerprint getScannableFingerprint() {
        return this.scannableFingerprint;
    }
}
