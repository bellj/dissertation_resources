package org.signal.libsignal.protocol.ecc;

import org.signal.libsignal.protocol.InvalidKeyException;

/* loaded from: classes3.dex */
public class Curve {
    public static final int DJB_TYPE;

    public static ECKeyPair generateKeyPair() {
        ECPrivateKey generate = ECPrivateKey.generate();
        return new ECKeyPair(generate.publicKey(), generate);
    }

    public static ECPublicKey decodePoint(byte[] bArr, int i) throws InvalidKeyException {
        if (bArr != null && bArr.length - i >= 1) {
            return new ECPublicKey(bArr, i);
        }
        throw new InvalidKeyException("No key type identifier");
    }

    public static ECPrivateKey decodePrivatePoint(byte[] bArr) {
        return new ECPrivateKey(bArr);
    }

    public static byte[] calculateAgreement(ECPublicKey eCPublicKey, ECPrivateKey eCPrivateKey) throws InvalidKeyException {
        if (eCPublicKey == null) {
            throw new InvalidKeyException("public value is null");
        } else if (eCPrivateKey != null) {
            return eCPrivateKey.calculateAgreement(eCPublicKey);
        } else {
            throw new InvalidKeyException("private value is null");
        }
    }

    public static boolean verifySignature(ECPublicKey eCPublicKey, byte[] bArr, byte[] bArr2) throws InvalidKeyException {
        if (eCPublicKey != null && bArr != null && bArr2 != null) {
            return eCPublicKey.verifySignature(bArr, bArr2);
        }
        throw new InvalidKeyException("Values must not be null");
    }

    public static byte[] calculateSignature(ECPrivateKey eCPrivateKey, byte[] bArr) throws InvalidKeyException {
        if (eCPrivateKey != null && bArr != null) {
            return eCPrivateKey.calculateSignature(bArr);
        }
        throw new InvalidKeyException("Values must not be null");
    }
}
