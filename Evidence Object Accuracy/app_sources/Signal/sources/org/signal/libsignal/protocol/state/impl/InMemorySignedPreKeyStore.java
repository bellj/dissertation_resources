package org.signal.libsignal.protocol.state.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.signal.libsignal.protocol.InvalidKeyIdException;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.state.SignedPreKeyRecord;
import org.signal.libsignal.protocol.state.SignedPreKeyStore;

/* loaded from: classes3.dex */
public class InMemorySignedPreKeyStore implements SignedPreKeyStore {
    private final Map<Integer, byte[]> store = new HashMap();

    @Override // org.signal.libsignal.protocol.state.SignedPreKeyStore
    public SignedPreKeyRecord loadSignedPreKey(int i) throws InvalidKeyIdException {
        try {
            if (this.store.containsKey(Integer.valueOf(i))) {
                return new SignedPreKeyRecord(this.store.get(Integer.valueOf(i)));
            }
            throw new InvalidKeyIdException("No such signedprekeyrecord! " + i);
        } catch (InvalidMessageException e) {
            throw new AssertionError(e);
        }
    }

    @Override // org.signal.libsignal.protocol.state.SignedPreKeyStore
    public List<SignedPreKeyRecord> loadSignedPreKeys() {
        try {
            LinkedList linkedList = new LinkedList();
            for (byte[] bArr : this.store.values()) {
                linkedList.add(new SignedPreKeyRecord(bArr));
            }
            return linkedList;
        } catch (InvalidMessageException e) {
            throw new AssertionError(e);
        }
    }

    @Override // org.signal.libsignal.protocol.state.SignedPreKeyStore
    public void storeSignedPreKey(int i, SignedPreKeyRecord signedPreKeyRecord) {
        this.store.put(Integer.valueOf(i), signedPreKeyRecord.serialize());
    }

    @Override // org.signal.libsignal.protocol.state.SignedPreKeyStore
    public boolean containsSignedPreKey(int i) {
        return this.store.containsKey(Integer.valueOf(i));
    }

    @Override // org.signal.libsignal.protocol.state.SignedPreKeyStore
    public void removeSignedPreKey(int i) {
        this.store.remove(Integer.valueOf(i));
    }
}
