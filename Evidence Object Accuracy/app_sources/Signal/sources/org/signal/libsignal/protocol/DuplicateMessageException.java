package org.signal.libsignal.protocol;

/* loaded from: classes3.dex */
public class DuplicateMessageException extends Exception {
    public DuplicateMessageException(String str) {
        super(str);
    }
}
