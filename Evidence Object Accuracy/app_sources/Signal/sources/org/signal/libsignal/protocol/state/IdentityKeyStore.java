package org.signal.libsignal.protocol.state;

import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.IdentityKeyPair;
import org.signal.libsignal.protocol.SignalProtocolAddress;

/* loaded from: classes3.dex */
public interface IdentityKeyStore {

    /* loaded from: classes3.dex */
    public enum Direction {
        SENDING,
        RECEIVING
    }

    IdentityKey getIdentity(SignalProtocolAddress signalProtocolAddress);

    IdentityKeyPair getIdentityKeyPair();

    int getLocalRegistrationId();

    boolean isTrustedIdentity(SignalProtocolAddress signalProtocolAddress, IdentityKey identityKey, Direction direction);

    boolean saveIdentity(SignalProtocolAddress signalProtocolAddress, IdentityKey identityKey);
}
