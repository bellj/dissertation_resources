package org.signal.libsignal.protocol.logging;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.UnknownHostException;

/* loaded from: classes3.dex */
public class Log {
    private Log() {
    }

    public static void v(String str, String str2) {
        log(2, str, str2);
    }

    public static void v(String str, String str2, Throwable th) {
        log(2, str, str2 + '\n' + getStackTraceString(th));
    }

    public static void d(String str, String str2) {
        log(3, str, str2);
    }

    public static void d(String str, String str2, Throwable th) {
        log(3, str, str2 + '\n' + getStackTraceString(th));
    }

    public static void i(String str, String str2) {
        log(4, str, str2);
    }

    public static void i(String str, String str2, Throwable th) {
        log(4, str, str2 + '\n' + getStackTraceString(th));
    }

    public static void w(String str, String str2) {
        log(5, str, str2);
    }

    public static void w(String str, String str2, Throwable th) {
        log(5, str, str2 + '\n' + getStackTraceString(th));
    }

    public static void w(String str, Throwable th) {
        log(5, str, getStackTraceString(th));
    }

    public static void e(String str, String str2) {
        log(6, str, str2);
    }

    public static void e(String str, String str2, Throwable th) {
        log(6, str, str2 + '\n' + getStackTraceString(th));
    }

    private static String getStackTraceString(Throwable th) {
        if (th == null) {
            return "";
        }
        for (Throwable th2 = th; th2 != null; th2 = th2.getCause()) {
            if (th2 instanceof UnknownHostException) {
                return "";
            }
        }
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        th.printStackTrace(printWriter);
        printWriter.flush();
        return stringWriter.toString();
    }

    private static void log(int i, String str, String str2) {
        SignalProtocolLogger provider = SignalProtocolLoggerProvider.getProvider();
        if (provider != null) {
            provider.log(i, str, str2);
        }
    }
}
