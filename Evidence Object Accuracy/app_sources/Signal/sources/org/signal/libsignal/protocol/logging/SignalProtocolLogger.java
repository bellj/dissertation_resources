package org.signal.libsignal.protocol.logging;

/* loaded from: classes.dex */
public interface SignalProtocolLogger {
    public static final int ASSERT;
    public static final int DEBUG;
    public static final int ERROR;
    public static final int INFO;
    public static final int VERBOSE;
    public static final int WARN;

    void log(int i, String str, String str2);
}
