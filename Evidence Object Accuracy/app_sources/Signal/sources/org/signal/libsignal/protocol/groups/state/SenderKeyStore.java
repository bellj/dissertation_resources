package org.signal.libsignal.protocol.groups.state;

import java.util.UUID;
import org.signal.libsignal.protocol.SignalProtocolAddress;

/* loaded from: classes3.dex */
public interface SenderKeyStore {
    SenderKeyRecord loadSenderKey(SignalProtocolAddress signalProtocolAddress, UUID uuid);

    void storeSenderKey(SignalProtocolAddress signalProtocolAddress, UUID uuid, SenderKeyRecord senderKeyRecord);
}
