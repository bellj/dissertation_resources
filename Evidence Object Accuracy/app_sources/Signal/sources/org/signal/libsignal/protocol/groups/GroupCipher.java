package org.signal.libsignal.protocol.groups;

import java.util.UUID;
import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;
import org.signal.libsignal.protocol.DuplicateMessageException;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.LegacyMessageException;
import org.signal.libsignal.protocol.NoSessionException;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.groups.state.SenderKeyStore;
import org.signal.libsignal.protocol.message.CiphertextMessage;

/* loaded from: classes3.dex */
public class GroupCipher {
    private final SignalProtocolAddress sender;
    private final SenderKeyStore senderKeyStore;

    public GroupCipher(SenderKeyStore senderKeyStore, SignalProtocolAddress signalProtocolAddress) {
        this.senderKeyStore = senderKeyStore;
        this.sender = signalProtocolAddress;
    }

    public CiphertextMessage encrypt(UUID uuid, byte[] bArr) throws NoSessionException {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this.sender);
        try {
            CiphertextMessage GroupCipher_EncryptMessage = Native.GroupCipher_EncryptMessage(nativeHandleGuard.nativeHandle(), uuid, bArr, this.senderKeyStore, null);
            nativeHandleGuard.close();
            return GroupCipher_EncryptMessage;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] decrypt(byte[] bArr) throws LegacyMessageException, DuplicateMessageException, InvalidMessageException, NoSessionException {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this.sender);
        try {
            byte[] GroupCipher_DecryptMessage = Native.GroupCipher_DecryptMessage(nativeHandleGuard.nativeHandle(), bArr, this.senderKeyStore, null);
            nativeHandleGuard.close();
            return GroupCipher_DecryptMessage;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }
}
