package org.signal.libsignal.protocol.fingerprint;

import org.signal.libsignal.protocol.IdentityKey;

/* loaded from: classes3.dex */
public interface FingerprintGenerator {
    Fingerprint createFor(int i, byte[] bArr, IdentityKey identityKey, byte[] bArr2, IdentityKey identityKey2);
}
