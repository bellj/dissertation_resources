package org.signal.libsignal.protocol.util;

/* loaded from: classes3.dex */
public class Pair<T1, T2> {
    private final T1 v1;
    private final T2 v2;

    public Pair(T1 t1, T2 t2) {
        this.v1 = t1;
        this.v2 = t2;
    }

    public T1 first() {
        return this.v1;
    }

    public T2 second() {
        return this.v2;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Pair) {
            Pair pair = (Pair) obj;
            if (equal(pair.first(), first()) && equal(pair.second(), second())) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        return first().hashCode() ^ second().hashCode();
    }

    private boolean equal(Object obj, Object obj2) {
        if (obj == null && obj2 == null) {
            return true;
        }
        if (obj == null || obj2 == null) {
            return false;
        }
        return obj.equals(obj2);
    }
}
