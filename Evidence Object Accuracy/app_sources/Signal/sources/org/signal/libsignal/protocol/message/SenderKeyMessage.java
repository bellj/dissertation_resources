package org.signal.libsignal.protocol.message;

import java.util.UUID;
import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.InvalidVersionException;
import org.signal.libsignal.protocol.LegacyMessageException;
import org.signal.libsignal.protocol.ecc.ECPublicKey;

/* loaded from: classes3.dex */
public class SenderKeyMessage implements CiphertextMessage, NativeHandleGuard.Owner {
    private final long unsafeHandle;

    @Override // org.signal.libsignal.protocol.message.CiphertextMessage
    public int getType() {
        return 7;
    }

    protected void finalize() {
        Native.SenderKeyMessage_Destroy(this.unsafeHandle);
    }

    public SenderKeyMessage(long j) {
        this.unsafeHandle = j;
    }

    @Override // org.signal.libsignal.protocol.message.CiphertextMessage, org.signal.libsignal.internal.NativeHandleGuard.Owner
    public long unsafeNativeHandleWithoutGuard() {
        return this.unsafeHandle;
    }

    public SenderKeyMessage(byte[] bArr) throws InvalidMessageException, InvalidVersionException, LegacyMessageException {
        this.unsafeHandle = Native.SenderKeyMessage_Deserialize(bArr);
    }

    public UUID getDistributionId() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            UUID SenderKeyMessage_GetDistributionId = Native.SenderKeyMessage_GetDistributionId(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SenderKeyMessage_GetDistributionId;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public int getChainId() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int SenderKeyMessage_GetChainId = Native.SenderKeyMessage_GetChainId(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SenderKeyMessage_GetChainId;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public int getIteration() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int SenderKeyMessage_GetIteration = Native.SenderKeyMessage_GetIteration(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SenderKeyMessage_GetIteration;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] getCipherText() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] SenderKeyMessage_GetCipherText = Native.SenderKeyMessage_GetCipherText(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SenderKeyMessage_GetCipherText;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public void verifySignature(ECPublicKey eCPublicKey) throws InvalidMessageException {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            NativeHandleGuard nativeHandleGuard2 = new NativeHandleGuard(eCPublicKey);
            if (Native.SenderKeyMessage_VerifySignature(nativeHandleGuard.nativeHandle(), nativeHandleGuard2.nativeHandle())) {
                nativeHandleGuard2.close();
                nativeHandleGuard.close();
                return;
            }
            throw new InvalidMessageException("Invalid signature!");
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    @Override // org.signal.libsignal.protocol.message.CiphertextMessage
    public byte[] serialize() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] SenderKeyMessage_GetSerialized = Native.SenderKeyMessage_GetSerialized(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SenderKeyMessage_GetSerialized;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }
}
