package org.signal.libsignal.protocol.ecc;

import java.util.Arrays;
import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;

/* loaded from: classes3.dex */
public class ECPublicKey implements Comparable<ECPublicKey>, NativeHandleGuard.Owner {
    public static final int KEY_SIZE;
    private final long unsafeHandle;

    public ECPublicKey(byte[] bArr, int i) {
        this.unsafeHandle = Native.ECPublicKey_Deserialize(bArr, i);
    }

    public ECPublicKey(byte[] bArr) {
        this.unsafeHandle = Native.ECPublicKey_Deserialize(bArr, 0);
    }

    public static ECPublicKey fromPublicKeyBytes(byte[] bArr) {
        byte[] bArr2 = new byte[33];
        bArr2[0] = 5;
        System.arraycopy(bArr, 0, bArr2, 1, 32);
        return new ECPublicKey(Native.ECPublicKey_Deserialize(bArr2, 0));
    }

    public ECPublicKey(long j) {
        if (j != 0) {
            this.unsafeHandle = j;
            return;
        }
        throw null;
    }

    @Override // java.lang.Object
    protected void finalize() {
        Native.ECPublicKey_Destroy(this.unsafeHandle);
    }

    public boolean verifySignature(byte[] bArr, byte[] bArr2) {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            boolean ECPublicKey_Verify = Native.ECPublicKey_Verify(nativeHandleGuard.nativeHandle(), bArr, bArr2);
            nativeHandleGuard.close();
            return ECPublicKey_Verify;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] serialize() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] ECPublicKey_Serialize = Native.ECPublicKey_Serialize(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return ECPublicKey_Serialize;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] getPublicKeyBytes() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] ECPublicKey_GetPublicKeyBytes = Native.ECPublicKey_GetPublicKeyBytes(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return ECPublicKey_GetPublicKeyBytes;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public int getType() {
        return serialize()[0];
    }

    @Override // org.signal.libsignal.internal.NativeHandleGuard.Owner
    public long unsafeNativeHandleWithoutGuard() {
        return this.unsafeHandle;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof ECPublicKey)) {
            return Arrays.equals(serialize(), ((ECPublicKey) obj).serialize());
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Arrays.hashCode(serialize());
    }

    public int compareTo(ECPublicKey eCPublicKey) {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            NativeHandleGuard nativeHandleGuard2 = new NativeHandleGuard(eCPublicKey);
            int ECPublicKey_Compare = Native.ECPublicKey_Compare(nativeHandleGuard.nativeHandle(), nativeHandleGuard2.nativeHandle());
            nativeHandleGuard2.close();
            nativeHandleGuard.close();
            return ECPublicKey_Compare;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }
}
