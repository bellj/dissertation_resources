package org.signal.libsignal.protocol;

/* loaded from: classes3.dex */
public class InvalidMacException extends Exception {
    public InvalidMacException(String str) {
        super(str);
    }

    public InvalidMacException(Throwable th) {
        super(th);
    }
}
