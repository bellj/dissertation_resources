package org.signal.libsignal.protocol.state.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.NoSessionException;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.state.SessionRecord;
import org.signal.libsignal.protocol.state.SessionStore;

/* loaded from: classes3.dex */
public class InMemorySessionStore implements SessionStore {
    private Map<SignalProtocolAddress, byte[]> sessions = new HashMap();

    @Override // org.signal.libsignal.protocol.state.SessionStore
    public synchronized SessionRecord loadSession(SignalProtocolAddress signalProtocolAddress) {
        try {
            if (containsSession(signalProtocolAddress)) {
                return new SessionRecord(this.sessions.get(signalProtocolAddress));
            }
            return new SessionRecord();
        } catch (InvalidMessageException e) {
            throw new AssertionError(e);
        }
    }

    @Override // org.signal.libsignal.protocol.state.SessionStore
    public synchronized List<SessionRecord> loadExistingSessions(List<SignalProtocolAddress> list) throws NoSessionException {
        LinkedList linkedList;
        linkedList = new LinkedList();
        for (SignalProtocolAddress signalProtocolAddress : list) {
            byte[] bArr = this.sessions.get(signalProtocolAddress);
            if (bArr != null) {
                try {
                    linkedList.add(new SessionRecord(bArr));
                } catch (InvalidMessageException e) {
                    throw new AssertionError(e);
                }
            } else {
                throw new NoSessionException("no session for " + signalProtocolAddress);
            }
        }
        return linkedList;
    }

    @Override // org.signal.libsignal.protocol.state.SessionStore
    public synchronized List<Integer> getSubDeviceSessions(String str) {
        LinkedList linkedList;
        linkedList = new LinkedList();
        for (SignalProtocolAddress signalProtocolAddress : this.sessions.keySet()) {
            if (signalProtocolAddress.getName().equals(str) && signalProtocolAddress.getDeviceId() != 1) {
                linkedList.add(Integer.valueOf(signalProtocolAddress.getDeviceId()));
            }
        }
        return linkedList;
    }

    @Override // org.signal.libsignal.protocol.state.SessionStore
    public synchronized void storeSession(SignalProtocolAddress signalProtocolAddress, SessionRecord sessionRecord) {
        this.sessions.put(signalProtocolAddress, sessionRecord.serialize());
    }

    @Override // org.signal.libsignal.protocol.state.SessionStore
    public synchronized boolean containsSession(SignalProtocolAddress signalProtocolAddress) {
        return this.sessions.containsKey(signalProtocolAddress);
    }

    @Override // org.signal.libsignal.protocol.state.SessionStore
    public synchronized void deleteSession(SignalProtocolAddress signalProtocolAddress) {
        this.sessions.remove(signalProtocolAddress);
    }

    @Override // org.signal.libsignal.protocol.state.SessionStore
    public synchronized void deleteAllSessions(String str) {
        for (SignalProtocolAddress signalProtocolAddress : this.sessions.keySet()) {
            if (signalProtocolAddress.getName().equals(str)) {
                this.sessions.remove(signalProtocolAddress);
            }
        }
    }
}
