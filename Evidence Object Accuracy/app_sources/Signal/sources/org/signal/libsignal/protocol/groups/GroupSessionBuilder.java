package org.signal.libsignal.protocol.groups;

import java.util.UUID;
import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.groups.state.SenderKeyStore;
import org.signal.libsignal.protocol.message.SenderKeyDistributionMessage;

/* loaded from: classes3.dex */
public class GroupSessionBuilder {
    private final SenderKeyStore senderKeyStore;

    public GroupSessionBuilder(SenderKeyStore senderKeyStore) {
        this.senderKeyStore = senderKeyStore;
    }

    public void process(SignalProtocolAddress signalProtocolAddress, SenderKeyDistributionMessage senderKeyDistributionMessage) {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(signalProtocolAddress);
        try {
            NativeHandleGuard nativeHandleGuard2 = new NativeHandleGuard(senderKeyDistributionMessage);
            Native.GroupSessionBuilder_ProcessSenderKeyDistributionMessage(nativeHandleGuard.nativeHandle(), nativeHandleGuard2.nativeHandle(), this.senderKeyStore, null);
            nativeHandleGuard2.close();
            nativeHandleGuard.close();
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public SenderKeyDistributionMessage create(SignalProtocolAddress signalProtocolAddress, UUID uuid) {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(signalProtocolAddress);
        try {
            SenderKeyDistributionMessage senderKeyDistributionMessage = new SenderKeyDistributionMessage(Native.GroupSessionBuilder_CreateSenderKeyDistributionMessage(nativeHandleGuard.nativeHandle(), uuid, this.senderKeyStore, null));
            nativeHandleGuard.close();
            return senderKeyDistributionMessage;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }
}
