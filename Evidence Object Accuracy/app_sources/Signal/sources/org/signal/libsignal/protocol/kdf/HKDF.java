package org.signal.libsignal.protocol.kdf;

import org.signal.libsignal.internal.Native;

/* loaded from: classes3.dex */
public abstract class HKDF {
    public static byte[] deriveSecrets(byte[] bArr, byte[] bArr2, int i) {
        return Native.HKDF_DeriveSecrets(i, bArr, bArr2, null);
    }

    public static byte[] deriveSecrets(byte[] bArr, byte[] bArr2, byte[] bArr3, int i) {
        return Native.HKDF_DeriveSecrets(i, bArr, bArr3, bArr2);
    }
}
