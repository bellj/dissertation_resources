package org.signal.libsignal.protocol;

/* loaded from: classes3.dex */
public class UntrustedIdentityException extends Exception {
    private final IdentityKey key;
    private final String name;

    public UntrustedIdentityException(String str, IdentityKey identityKey) {
        this.name = str;
        this.key = identityKey;
    }

    public UntrustedIdentityException(String str) {
        this.name = str;
        this.key = null;
    }

    public IdentityKey getUntrustedIdentity() {
        return this.key;
    }

    public String getName() {
        return this.name;
    }
}
