package org.signal.libsignal.protocol.state;

import java.util.List;
import org.signal.libsignal.protocol.NoSessionException;
import org.signal.libsignal.protocol.SignalProtocolAddress;

/* loaded from: classes3.dex */
public interface SessionStore {
    boolean containsSession(SignalProtocolAddress signalProtocolAddress);

    void deleteAllSessions(String str);

    void deleteSession(SignalProtocolAddress signalProtocolAddress);

    List<Integer> getSubDeviceSessions(String str);

    List<SessionRecord> loadExistingSessions(List<SignalProtocolAddress> list) throws NoSessionException;

    SessionRecord loadSession(SignalProtocolAddress signalProtocolAddress);

    void storeSession(SignalProtocolAddress signalProtocolAddress, SessionRecord sessionRecord);
}
