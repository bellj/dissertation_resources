package org.signal.libsignal.protocol.ecc;

/* loaded from: classes3.dex */
public class ECKeyPair {
    private final ECPrivateKey privateKey;
    private final ECPublicKey publicKey;

    public ECKeyPair(ECPublicKey eCPublicKey, ECPrivateKey eCPrivateKey) {
        this.publicKey = eCPublicKey;
        this.privateKey = eCPrivateKey;
    }

    public ECPublicKey getPublicKey() {
        return this.publicKey;
    }

    public ECPrivateKey getPrivateKey() {
        return this.privateKey;
    }
}
