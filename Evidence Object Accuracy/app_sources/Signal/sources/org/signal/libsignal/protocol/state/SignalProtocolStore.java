package org.signal.libsignal.protocol.state;

import org.signal.libsignal.protocol.groups.state.SenderKeyStore;

/* loaded from: classes3.dex */
public interface SignalProtocolStore extends IdentityKeyStore, PreKeyStore, SessionStore, SignedPreKeyStore, SenderKeyStore {
}
