package org.signal.libsignal.protocol;

/* loaded from: classes3.dex */
public class LegacyMessageException extends Exception {
    public LegacyMessageException(String str) {
        super(str);
    }
}
