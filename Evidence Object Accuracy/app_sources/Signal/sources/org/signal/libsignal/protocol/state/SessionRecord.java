package org.signal.libsignal.protocol.state;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.IdentityKeyPair;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.ecc.ECKeyPair;
import org.signal.libsignal.protocol.ecc.ECPublicKey;

/* loaded from: classes3.dex */
public class SessionRecord implements NativeHandleGuard.Owner {
    private final long unsafeHandle;

    protected void finalize() {
        Native.SessionRecord_Destroy(this.unsafeHandle);
    }

    public SessionRecord() {
        this.unsafeHandle = Native.SessionRecord_NewFresh();
    }

    private SessionRecord(long j) {
        this.unsafeHandle = j;
    }

    public static SessionRecord fromSingleSessionState(byte[] bArr) throws InvalidMessageException {
        return new SessionRecord(Native.SessionRecord_FromSingleSessionState(bArr));
    }

    public SessionRecord(byte[] bArr) throws InvalidMessageException {
        this.unsafeHandle = Native.SessionRecord_Deserialize(bArr);
    }

    public void archiveCurrentState() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            Native.SessionRecord_ArchiveCurrentState(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public int getSessionVersion() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int SessionRecord_GetSessionVersion = Native.SessionRecord_GetSessionVersion(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SessionRecord_GetSessionVersion;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public int getRemoteRegistrationId() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int SessionRecord_GetRemoteRegistrationId = Native.SessionRecord_GetRemoteRegistrationId(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SessionRecord_GetRemoteRegistrationId;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public int getLocalRegistrationId() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int SessionRecord_GetLocalRegistrationId = Native.SessionRecord_GetLocalRegistrationId(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SessionRecord_GetLocalRegistrationId;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public IdentityKey getRemoteIdentityKey() {
        try {
            NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
            byte[] SessionRecord_GetRemoteIdentityKeyPublic = Native.SessionRecord_GetRemoteIdentityKeyPublic(nativeHandleGuard.nativeHandle());
            if (SessionRecord_GetRemoteIdentityKeyPublic == null) {
                nativeHandleGuard.close();
                return null;
            }
            IdentityKey identityKey = new IdentityKey(SessionRecord_GetRemoteIdentityKeyPublic);
            nativeHandleGuard.close();
            return identityKey;
        } catch (InvalidKeyException e) {
            throw new AssertionError(e);
        }
    }

    public IdentityKey getLocalIdentityKey() {
        try {
            NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
            IdentityKey identityKey = new IdentityKey(Native.SessionRecord_GetLocalIdentityKeyPublic(nativeHandleGuard.nativeHandle()));
            nativeHandleGuard.close();
            return identityKey;
        } catch (InvalidKeyException e) {
            throw new AssertionError(e);
        }
    }

    public boolean hasSenderChain() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            boolean SessionRecord_HasSenderChain = Native.SessionRecord_HasSenderChain(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SessionRecord_HasSenderChain;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public boolean currentRatchetKeyMatches(ECPublicKey eCPublicKey) {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            NativeHandleGuard nativeHandleGuard2 = new NativeHandleGuard(eCPublicKey);
            boolean SessionRecord_CurrentRatchetKeyMatches = Native.SessionRecord_CurrentRatchetKeyMatches(nativeHandleGuard.nativeHandle(), nativeHandleGuard2.nativeHandle());
            nativeHandleGuard2.close();
            nativeHandleGuard.close();
            return SessionRecord_CurrentRatchetKeyMatches;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] serialize() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] SessionRecord_Serialize = Native.SessionRecord_Serialize(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SessionRecord_Serialize;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] getReceiverChainKeyValue(ECPublicKey eCPublicKey) {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            NativeHandleGuard nativeHandleGuard2 = new NativeHandleGuard(eCPublicKey);
            byte[] SessionRecord_GetReceiverChainKeyValue = Native.SessionRecord_GetReceiverChainKeyValue(nativeHandleGuard.nativeHandle(), nativeHandleGuard2.nativeHandle());
            nativeHandleGuard2.close();
            nativeHandleGuard.close();
            return SessionRecord_GetReceiverChainKeyValue;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] getSenderChainKeyValue() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] SessionRecord_GetSenderChainKeyValue = Native.SessionRecord_GetSenderChainKeyValue(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SessionRecord_GetSenderChainKeyValue;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] getAliceBaseKey() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] SessionRecord_GetAliceBaseKey = Native.SessionRecord_GetAliceBaseKey(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SessionRecord_GetAliceBaseKey;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public static SessionRecord initializeAliceSession(IdentityKeyPair identityKeyPair, ECKeyPair eCKeyPair, IdentityKey identityKey, ECPublicKey eCPublicKey, ECPublicKey eCPublicKey2) {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(identityKeyPair.getPrivateKey());
        try {
            NativeHandleGuard nativeHandleGuard2 = new NativeHandleGuard(identityKeyPair.getPublicKey().getPublicKey());
            NativeHandleGuard nativeHandleGuard3 = new NativeHandleGuard(eCKeyPair.getPrivateKey());
            try {
                NativeHandleGuard nativeHandleGuard4 = new NativeHandleGuard(eCKeyPair.getPublicKey());
                nativeHandleGuard3 = new NativeHandleGuard(identityKey.getPublicKey());
                try {
                    NativeHandleGuard nativeHandleGuard5 = new NativeHandleGuard(eCPublicKey);
                    NativeHandleGuard nativeHandleGuard6 = new NativeHandleGuard(eCPublicKey2);
                    SessionRecord sessionRecord = new SessionRecord(Native.SessionRecord_InitializeAliceSession(nativeHandleGuard.nativeHandle(), nativeHandleGuard2.nativeHandle(), nativeHandleGuard3.nativeHandle(), nativeHandleGuard4.nativeHandle(), nativeHandleGuard3.nativeHandle(), nativeHandleGuard5.nativeHandle(), nativeHandleGuard6.nativeHandle()));
                    nativeHandleGuard6.close();
                    nativeHandleGuard5.close();
                    nativeHandleGuard3.close();
                    nativeHandleGuard4.close();
                    nativeHandleGuard3.close();
                    nativeHandleGuard2.close();
                    nativeHandleGuard.close();
                    return sessionRecord;
                } finally {
                }
            } finally {
            }
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public static SessionRecord initializeBobSession(IdentityKeyPair identityKeyPair, ECKeyPair eCKeyPair, ECKeyPair eCKeyPair2, IdentityKey identityKey, ECPublicKey eCPublicKey) {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(identityKeyPair.getPrivateKey());
        try {
            NativeHandleGuard nativeHandleGuard2 = new NativeHandleGuard(identityKeyPair.getPublicKey().getPublicKey());
            NativeHandleGuard nativeHandleGuard3 = new NativeHandleGuard(eCKeyPair.getPrivateKey());
            try {
                NativeHandleGuard nativeHandleGuard4 = new NativeHandleGuard(eCKeyPair.getPublicKey());
                nativeHandleGuard3 = new NativeHandleGuard(eCKeyPair2.getPrivateKey());
                try {
                    NativeHandleGuard nativeHandleGuard5 = new NativeHandleGuard(eCKeyPair2.getPublicKey());
                    NativeHandleGuard nativeHandleGuard6 = new NativeHandleGuard(identityKey.getPublicKey());
                    nativeHandleGuard3 = new NativeHandleGuard(eCPublicKey);
                    try {
                        SessionRecord sessionRecord = new SessionRecord(Native.SessionRecord_InitializeBobSession(nativeHandleGuard.nativeHandle(), nativeHandleGuard2.nativeHandle(), nativeHandleGuard3.nativeHandle(), nativeHandleGuard4.nativeHandle(), nativeHandleGuard3.nativeHandle(), nativeHandleGuard5.nativeHandle(), nativeHandleGuard6.nativeHandle(), nativeHandleGuard3.nativeHandle()));
                        nativeHandleGuard3.close();
                        nativeHandleGuard6.close();
                        nativeHandleGuard5.close();
                        nativeHandleGuard3.close();
                        nativeHandleGuard4.close();
                        nativeHandleGuard3.close();
                        nativeHandleGuard2.close();
                        nativeHandleGuard.close();
                        return sessionRecord;
                    } finally {
                    }
                } finally {
                }
            } finally {
                try {
                    nativeHandleGuard3.close();
                } catch (Throwable th) {
                    th.addSuppressed(th);
                }
            }
        } catch (Throwable th2) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th3) {
                th2.addSuppressed(th3);
            }
            throw th2;
        }
    }

    @Override // org.signal.libsignal.internal.NativeHandleGuard.Owner
    public long unsafeNativeHandleWithoutGuard() {
        return this.unsafeHandle;
    }
}
