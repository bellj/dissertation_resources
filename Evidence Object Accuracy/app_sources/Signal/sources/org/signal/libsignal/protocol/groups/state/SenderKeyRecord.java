package org.signal.libsignal.protocol.groups.state;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;
import org.signal.libsignal.protocol.InvalidMessageException;

/* loaded from: classes3.dex */
public class SenderKeyRecord implements NativeHandleGuard.Owner {
    private final long unsafeHandle;

    protected void finalize() {
        Native.SenderKeyRecord_Destroy(this.unsafeHandle);
    }

    public SenderKeyRecord(long j) {
        this.unsafeHandle = j;
    }

    public SenderKeyRecord(byte[] bArr) throws InvalidMessageException {
        this.unsafeHandle = Native.SenderKeyRecord_Deserialize(bArr);
    }

    public byte[] serialize() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] SenderKeyRecord_GetSerialized = Native.SenderKeyRecord_GetSerialized(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SenderKeyRecord_GetSerialized;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    @Override // org.signal.libsignal.internal.NativeHandleGuard.Owner
    public long unsafeNativeHandleWithoutGuard() {
        return this.unsafeHandle;
    }
}
