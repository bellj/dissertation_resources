package org.signal.libsignal.protocol.fingerprint;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.protocol.IdentityKey;

/* loaded from: classes3.dex */
public class NumericFingerprintGenerator implements FingerprintGenerator {
    private final int iterations;

    public NumericFingerprintGenerator(int i) {
        this.iterations = i;
    }

    @Override // org.signal.libsignal.protocol.fingerprint.FingerprintGenerator
    public Fingerprint createFor(int i, byte[] bArr, IdentityKey identityKey, byte[] bArr2, IdentityKey identityKey2) {
        long NumericFingerprintGenerator_New = Native.NumericFingerprintGenerator_New(this.iterations, i, bArr, identityKey.serialize(), bArr2, identityKey2.serialize());
        DisplayableFingerprint displayableFingerprint = new DisplayableFingerprint(Native.NumericFingerprintGenerator_GetDisplayString(NumericFingerprintGenerator_New));
        ScannableFingerprint scannableFingerprint = new ScannableFingerprint(Native.NumericFingerprintGenerator_GetScannableEncoding(NumericFingerprintGenerator_New));
        Native.NumericFingerprintGenerator_Destroy(NumericFingerprintGenerator_New);
        return new Fingerprint(displayableFingerprint, scannableFingerprint);
    }
}
