package org.signal.libsignal.protocol;

/* loaded from: classes3.dex */
public class NoSessionException extends Exception {
    public NoSessionException(String str) {
        super(str);
    }

    public NoSessionException(Exception exc) {
        super(exc);
    }
}
