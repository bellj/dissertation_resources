package org.signal.libsignal.protocol.state.impl;

import java.util.HashMap;
import java.util.Map;
import org.signal.libsignal.protocol.InvalidKeyIdException;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.state.PreKeyRecord;
import org.signal.libsignal.protocol.state.PreKeyStore;

/* loaded from: classes3.dex */
public class InMemoryPreKeyStore implements PreKeyStore {
    private final Map<Integer, byte[]> store = new HashMap();

    @Override // org.signal.libsignal.protocol.state.PreKeyStore
    public PreKeyRecord loadPreKey(int i) throws InvalidKeyIdException {
        try {
            if (this.store.containsKey(Integer.valueOf(i))) {
                return new PreKeyRecord(this.store.get(Integer.valueOf(i)));
            }
            throw new InvalidKeyIdException("No such prekeyrecord!");
        } catch (InvalidMessageException e) {
            throw new AssertionError(e);
        }
    }

    @Override // org.signal.libsignal.protocol.state.PreKeyStore
    public void storePreKey(int i, PreKeyRecord preKeyRecord) {
        this.store.put(Integer.valueOf(i), preKeyRecord.serialize());
    }

    @Override // org.signal.libsignal.protocol.state.PreKeyStore
    public boolean containsPreKey(int i) {
        return this.store.containsKey(Integer.valueOf(i));
    }

    @Override // org.signal.libsignal.protocol.state.PreKeyStore
    public void removePreKey(int i) {
        this.store.remove(Integer.valueOf(i));
    }
}
