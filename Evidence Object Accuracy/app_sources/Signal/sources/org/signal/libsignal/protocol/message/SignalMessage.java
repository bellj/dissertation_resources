package org.signal.libsignal.protocol.message;

import javax.crypto.spec.SecretKeySpec;
import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.InvalidVersionException;
import org.signal.libsignal.protocol.LegacyMessageException;
import org.signal.libsignal.protocol.ecc.ECPublicKey;
import org.signal.libsignal.protocol.util.ByteUtil;

/* loaded from: classes3.dex */
public class SignalMessage implements CiphertextMessage, NativeHandleGuard.Owner {
    private final long unsafeHandle;

    @Override // org.signal.libsignal.protocol.message.CiphertextMessage
    public int getType() {
        return 2;
    }

    protected void finalize() {
        Native.SignalMessage_Destroy(this.unsafeHandle);
    }

    public SignalMessage(byte[] bArr) throws InvalidMessageException, InvalidVersionException, InvalidKeyException, LegacyMessageException {
        this.unsafeHandle = Native.SignalMessage_Deserialize(bArr);
    }

    public SignalMessage(long j) {
        this.unsafeHandle = j;
    }

    public ECPublicKey getSenderRatchetKey() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            ECPublicKey eCPublicKey = new ECPublicKey(Native.SignalMessage_GetSenderRatchetKey(nativeHandleGuard.nativeHandle()));
            nativeHandleGuard.close();
            return eCPublicKey;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public int getMessageVersion() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int SignalMessage_GetMessageVersion = Native.SignalMessage_GetMessageVersion(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SignalMessage_GetMessageVersion;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public int getCounter() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int SignalMessage_GetCounter = Native.SignalMessage_GetCounter(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SignalMessage_GetCounter;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] getBody() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] SignalMessage_GetBody = Native.SignalMessage_GetBody(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SignalMessage_GetBody;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public void verifyMac(IdentityKey identityKey, IdentityKey identityKey2, SecretKeySpec secretKeySpec) throws InvalidMessageException, InvalidKeyException {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            NativeHandleGuard nativeHandleGuard2 = new NativeHandleGuard(identityKey.getPublicKey());
            nativeHandleGuard = new NativeHandleGuard(identityKey2.getPublicKey());
            try {
                if (Native.SignalMessage_VerifyMac(nativeHandleGuard.nativeHandle(), nativeHandleGuard2.nativeHandle(), nativeHandleGuard.nativeHandle(), secretKeySpec.getEncoded())) {
                    nativeHandleGuard.close();
                    nativeHandleGuard2.close();
                    nativeHandleGuard.close();
                    return;
                }
                throw new InvalidMessageException("Bad Mac!");
            } finally {
            }
        } finally {
        }
    }

    @Override // org.signal.libsignal.protocol.message.CiphertextMessage
    public byte[] serialize() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] SignalMessage_GetSerialized = Native.SignalMessage_GetSerialized(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SignalMessage_GetSerialized;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    @Override // org.signal.libsignal.protocol.message.CiphertextMessage, org.signal.libsignal.internal.NativeHandleGuard.Owner
    public long unsafeNativeHandleWithoutGuard() {
        return this.unsafeHandle;
    }

    public static boolean isLegacy(byte[] bArr) {
        if (bArr == null || bArr.length < 1 || ByteUtil.highBitsToInt(bArr[0]) == 3) {
            return false;
        }
        return true;
    }
}
