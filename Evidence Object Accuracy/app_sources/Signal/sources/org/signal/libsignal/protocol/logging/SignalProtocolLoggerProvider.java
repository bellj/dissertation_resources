package org.signal.libsignal.protocol.logging;

/* loaded from: classes.dex */
public class SignalProtocolLoggerProvider {
    private static SignalProtocolLogger provider;

    public static SignalProtocolLogger getProvider() {
        return provider;
    }

    public static void setProvider(SignalProtocolLogger signalProtocolLogger) {
        provider = signalProtocolLogger;
    }
}
