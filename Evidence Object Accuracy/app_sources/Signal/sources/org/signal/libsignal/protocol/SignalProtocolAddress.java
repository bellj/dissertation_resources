package org.signal.libsignal.protocol;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;

/* loaded from: classes3.dex */
public class SignalProtocolAddress implements NativeHandleGuard.Owner {
    private final long unsafeHandle;

    public SignalProtocolAddress(String str, int i) {
        this.unsafeHandle = Native.ProtocolAddress_New(str, i);
    }

    public SignalProtocolAddress(long j) {
        this.unsafeHandle = j;
    }

    protected void finalize() {
        Native.ProtocolAddress_Destroy(this.unsafeHandle);
    }

    public String getName() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            String ProtocolAddress_Name = Native.ProtocolAddress_Name(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return ProtocolAddress_Name;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public int getDeviceId() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int ProtocolAddress_DeviceId = Native.ProtocolAddress_DeviceId(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return ProtocolAddress_DeviceId;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public String toString() {
        return getName() + "." + getDeviceId();
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof SignalProtocolAddress)) {
            return false;
        }
        SignalProtocolAddress signalProtocolAddress = (SignalProtocolAddress) obj;
        if (!getName().equals(signalProtocolAddress.getName()) || getDeviceId() != signalProtocolAddress.getDeviceId()) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return getName().hashCode() ^ getDeviceId();
    }

    @Override // org.signal.libsignal.internal.NativeHandleGuard.Owner
    public long unsafeNativeHandleWithoutGuard() {
        return this.unsafeHandle;
    }
}
