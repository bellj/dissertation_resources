package org.signal.libsignal.protocol;

/* loaded from: classes3.dex */
public class InvalidSessionException extends IllegalStateException {
    public InvalidSessionException(String str) {
        super(str);
    }
}
