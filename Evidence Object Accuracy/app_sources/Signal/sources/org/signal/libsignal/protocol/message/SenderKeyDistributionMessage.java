package org.signal.libsignal.protocol.message;

import java.util.UUID;
import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.InvalidVersionException;
import org.signal.libsignal.protocol.LegacyMessageException;
import org.signal.libsignal.protocol.ecc.ECPublicKey;

/* loaded from: classes3.dex */
public class SenderKeyDistributionMessage implements NativeHandleGuard.Owner {
    private final long unsafeHandle;

    protected void finalize() {
        Native.SenderKeyDistributionMessage_Destroy(this.unsafeHandle);
    }

    public SenderKeyDistributionMessage(long j) {
        this.unsafeHandle = j;
    }

    public SenderKeyDistributionMessage(byte[] bArr) throws InvalidMessageException, InvalidVersionException, LegacyMessageException, InvalidKeyException {
        this.unsafeHandle = Native.SenderKeyDistributionMessage_Deserialize(bArr);
    }

    public byte[] serialize() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] SenderKeyDistributionMessage_GetSerialized = Native.SenderKeyDistributionMessage_GetSerialized(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SenderKeyDistributionMessage_GetSerialized;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public UUID getDistributionId() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            UUID SenderKeyMessage_GetDistributionId = Native.SenderKeyMessage_GetDistributionId(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SenderKeyMessage_GetDistributionId;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public int getIteration() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int SenderKeyDistributionMessage_GetIteration = Native.SenderKeyDistributionMessage_GetIteration(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SenderKeyDistributionMessage_GetIteration;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] getChainKey() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] SenderKeyDistributionMessage_GetChainKey = Native.SenderKeyDistributionMessage_GetChainKey(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SenderKeyDistributionMessage_GetChainKey;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public ECPublicKey getSignatureKey() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            ECPublicKey eCPublicKey = new ECPublicKey(Native.SenderKeyDistributionMessage_GetSignatureKey(nativeHandleGuard.nativeHandle()));
            nativeHandleGuard.close();
            return eCPublicKey;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public int getChainId() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int SenderKeyDistributionMessage_GetChainId = Native.SenderKeyDistributionMessage_GetChainId(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SenderKeyDistributionMessage_GetChainId;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    @Override // org.signal.libsignal.internal.NativeHandleGuard.Owner
    public long unsafeNativeHandleWithoutGuard() {
        return this.unsafeHandle;
    }
}
