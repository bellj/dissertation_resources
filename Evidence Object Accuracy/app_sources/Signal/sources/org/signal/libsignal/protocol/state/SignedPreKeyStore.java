package org.signal.libsignal.protocol.state;

import java.util.List;
import org.signal.libsignal.protocol.InvalidKeyIdException;

/* loaded from: classes3.dex */
public interface SignedPreKeyStore {
    boolean containsSignedPreKey(int i);

    SignedPreKeyRecord loadSignedPreKey(int i) throws InvalidKeyIdException;

    List<SignedPreKeyRecord> loadSignedPreKeys();

    void removeSignedPreKey(int i);

    void storeSignedPreKey(int i, SignedPreKeyRecord signedPreKeyRecord);
}
