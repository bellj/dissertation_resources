package org.signal.libsignal.protocol.groups.state;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.util.Pair;

/* loaded from: classes3.dex */
public class InMemorySenderKeyStore implements SenderKeyStore {
    private final Map<Pair<SignalProtocolAddress, UUID>, SenderKeyRecord> store = new HashMap();

    @Override // org.signal.libsignal.protocol.groups.state.SenderKeyStore
    public void storeSenderKey(SignalProtocolAddress signalProtocolAddress, UUID uuid, SenderKeyRecord senderKeyRecord) {
        this.store.put(new Pair<>(signalProtocolAddress, uuid), senderKeyRecord);
    }

    @Override // org.signal.libsignal.protocol.groups.state.SenderKeyStore
    public SenderKeyRecord loadSenderKey(SignalProtocolAddress signalProtocolAddress, UUID uuid) {
        try {
            SenderKeyRecord senderKeyRecord = this.store.get(new Pair(signalProtocolAddress, uuid));
            if (senderKeyRecord == null) {
                return null;
            }
            return new SenderKeyRecord(senderKeyRecord.serialize());
        } catch (InvalidMessageException e) {
            throw new AssertionError(e);
        }
    }
}
