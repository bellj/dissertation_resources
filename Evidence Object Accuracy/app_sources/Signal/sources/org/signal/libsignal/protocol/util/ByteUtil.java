package org.signal.libsignal.protocol.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;

/* loaded from: classes3.dex */
public class ByteUtil {
    public static int highBitsToInt(byte b) {
        return (b & 255) >> 4;
    }

    public static byte intsToByteHighAndLow(int i, int i2) {
        return (byte) (((i << 4) | i2) & 255);
    }

    public static byte[] longToByteArray(long j) {
        return new byte[]{(byte) ((int) (j >> 56)), (byte) ((int) (j >> 48)), (byte) ((int) (j >> 40)), (byte) ((int) (j >> 32)), (byte) ((int) (j >> 24)), (byte) ((int) (j >> 16)), (byte) ((int) (j >> 8)), (byte) ((int) j)};
    }

    public static byte[] combine(byte[]... bArr) {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            for (byte[] bArr2 : bArr) {
                byteArrayOutputStream.write(bArr2);
            }
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    public static byte[][] split(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i];
        System.arraycopy(bArr, 0, bArr2, 0, i);
        byte[] bArr3 = new byte[i2];
        byte[][] bArr4 = {bArr2, bArr3};
        System.arraycopy(bArr, i, bArr3, 0, i2);
        return bArr4;
    }

    public static byte[][] split(byte[] bArr, int i, int i2, int i3) throws ParseException {
        if (bArr != null && i >= 0 && i2 >= 0 && i3 >= 0) {
            int i4 = i + i2;
            if (bArr.length >= i4 + i3) {
                byte[] bArr2 = new byte[i];
                System.arraycopy(bArr, 0, bArr2, 0, i);
                byte[] bArr3 = new byte[i2];
                System.arraycopy(bArr, i, bArr3, 0, i2);
                byte[] bArr4 = new byte[i3];
                byte[][] bArr5 = {bArr2, bArr3, bArr4};
                System.arraycopy(bArr, i4, bArr4, 0, i3);
                return bArr5;
            }
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Input too small: ");
        sb.append(bArr == null ? null : Hex.toString(bArr));
        throw new ParseException(sb.toString(), 0);
    }

    public static byte[] trim(byte[] bArr, int i) {
        byte[] bArr2 = new byte[i];
        System.arraycopy(bArr, 0, bArr2, 0, i);
        return bArr2;
    }

    public static long byteArray5ToLong(byte[] bArr, int i) {
        return (((long) bArr[i + 4]) & 255) | ((((long) bArr[i]) & 255) << 32) | ((((long) bArr[i + 1]) & 255) << 24) | ((((long) bArr[i + 2]) & 255) << 16) | ((((long) bArr[i + 3]) & 255) << 8);
    }
}
