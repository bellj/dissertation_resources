package org.signal.libsignal.protocol.state.impl;

import java.util.HashMap;
import java.util.Map;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.IdentityKeyPair;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.state.IdentityKeyStore;

/* loaded from: classes3.dex */
public class InMemoryIdentityKeyStore implements IdentityKeyStore {
    private final IdentityKeyPair identityKeyPair;
    private final int localRegistrationId;
    private final Map<SignalProtocolAddress, IdentityKey> trustedKeys = new HashMap();

    public InMemoryIdentityKeyStore(IdentityKeyPair identityKeyPair, int i) {
        this.identityKeyPair = identityKeyPair;
        this.localRegistrationId = i;
    }

    @Override // org.signal.libsignal.protocol.state.IdentityKeyStore
    public IdentityKeyPair getIdentityKeyPair() {
        return this.identityKeyPair;
    }

    @Override // org.signal.libsignal.protocol.state.IdentityKeyStore
    public int getLocalRegistrationId() {
        return this.localRegistrationId;
    }

    @Override // org.signal.libsignal.protocol.state.IdentityKeyStore
    public boolean saveIdentity(SignalProtocolAddress signalProtocolAddress, IdentityKey identityKey) {
        if (identityKey.equals(this.trustedKeys.get(signalProtocolAddress))) {
            return false;
        }
        this.trustedKeys.put(signalProtocolAddress, identityKey);
        return true;
    }

    @Override // org.signal.libsignal.protocol.state.IdentityKeyStore
    public boolean isTrustedIdentity(SignalProtocolAddress signalProtocolAddress, IdentityKey identityKey, IdentityKeyStore.Direction direction) {
        IdentityKey identityKey2 = this.trustedKeys.get(signalProtocolAddress);
        return identityKey2 == null || identityKey2.equals(identityKey);
    }

    @Override // org.signal.libsignal.protocol.state.IdentityKeyStore
    public IdentityKey getIdentity(SignalProtocolAddress signalProtocolAddress) {
        return this.trustedKeys.get(signalProtocolAddress);
    }
}
