package org.signal.libsignal.protocol.message;

/* loaded from: classes3.dex */
public interface CiphertextMessage {
    public static final int CURRENT_VERSION;
    public static final int ENCRYPTED_MESSAGE_OVERHEAD;
    public static final int PLAINTEXT_CONTENT_TYPE;
    public static final int PREKEY_TYPE;
    public static final int SENDERKEY_TYPE;
    public static final int WHISPER_TYPE;

    int getType();

    byte[] serialize();

    @Override // org.signal.libsignal.internal.NativeHandleGuard.Owner
    long unsafeNativeHandleWithoutGuard();
}
