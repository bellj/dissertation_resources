package org.signal.libsignal.protocol;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;
import org.signal.libsignal.protocol.state.IdentityKeyStore;
import org.signal.libsignal.protocol.state.PreKeyBundle;
import org.signal.libsignal.protocol.state.PreKeyStore;
import org.signal.libsignal.protocol.state.SessionStore;
import org.signal.libsignal.protocol.state.SignalProtocolStore;
import org.signal.libsignal.protocol.state.SignedPreKeyStore;

/* loaded from: classes3.dex */
public class SessionBuilder {
    private static final String TAG;
    private final IdentityKeyStore identityKeyStore;
    private final PreKeyStore preKeyStore;
    private final SignalProtocolAddress remoteAddress;
    private final SessionStore sessionStore;
    private final SignedPreKeyStore signedPreKeyStore;

    public SessionBuilder(SessionStore sessionStore, PreKeyStore preKeyStore, SignedPreKeyStore signedPreKeyStore, IdentityKeyStore identityKeyStore, SignalProtocolAddress signalProtocolAddress) {
        this.sessionStore = sessionStore;
        this.preKeyStore = preKeyStore;
        this.signedPreKeyStore = signedPreKeyStore;
        this.identityKeyStore = identityKeyStore;
        this.remoteAddress = signalProtocolAddress;
    }

    public SessionBuilder(SignalProtocolStore signalProtocolStore, SignalProtocolAddress signalProtocolAddress) {
        this(signalProtocolStore, signalProtocolStore, signalProtocolStore, signalProtocolStore, signalProtocolAddress);
    }

    public void process(PreKeyBundle preKeyBundle) throws InvalidKeyException, UntrustedIdentityException {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(preKeyBundle);
        try {
            NativeHandleGuard nativeHandleGuard2 = new NativeHandleGuard(this.remoteAddress);
            Native.SessionBuilder_ProcessPreKeyBundle(nativeHandleGuard.nativeHandle(), nativeHandleGuard2.nativeHandle(), this.sessionStore, this.identityKeyStore, null);
            nativeHandleGuard2.close();
            nativeHandleGuard.close();
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }
}
