package org.signal.libsignal.protocol.state;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.ecc.ECPublicKey;

/* loaded from: classes3.dex */
public class PreKeyBundle implements NativeHandleGuard.Owner {
    private final long unsafeHandle;

    protected void finalize() {
        Native.PreKeyBundle_Destroy(this.unsafeHandle);
    }

    public PreKeyBundle(int i, int i2, int i3, ECPublicKey eCPublicKey, int i4, ECPublicKey eCPublicKey2, byte[] bArr, IdentityKey identityKey) {
        Throwable th;
        Throwable th2;
        Throwable th3;
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(eCPublicKey);
        try {
            try {
                NativeHandleGuard nativeHandleGuard2 = new NativeHandleGuard(eCPublicKey2);
                try {
                    try {
                        NativeHandleGuard nativeHandleGuard3 = new NativeHandleGuard(identityKey.getPublicKey());
                        try {
                            try {
                                this.unsafeHandle = Native.PreKeyBundle_New(i, i2, i3, nativeHandleGuard.nativeHandle(), i4, nativeHandleGuard2.nativeHandle(), bArr, nativeHandleGuard3.nativeHandle());
                                nativeHandleGuard3.close();
                                nativeHandleGuard2.close();
                                nativeHandleGuard.close();
                            } catch (Throwable th4) {
                                th3 = th4;
                                nativeHandleGuard3.close();
                                throw th3;
                            }
                        } catch (Throwable th5) {
                            th3 = th5;
                        }
                    } catch (Throwable th6) {
                        th2 = th6;
                        nativeHandleGuard2.close();
                        throw th2;
                    }
                } catch (Throwable th7) {
                    th2 = th7;
                    nativeHandleGuard2.close();
                    throw th2;
                }
            } catch (Throwable th8) {
                th = th8;
                try {
                    nativeHandleGuard.close();
                } catch (Throwable th9) {
                    th.addSuppressed(th9);
                }
                throw th;
            }
        } catch (Throwable th10) {
            th = th10;
            nativeHandleGuard.close();
            throw th;
        }
    }

    public int getDeviceId() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int PreKeyBundle_GetDeviceId = Native.PreKeyBundle_GetDeviceId(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return PreKeyBundle_GetDeviceId;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public int getPreKeyId() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int PreKeyBundle_GetPreKeyId = Native.PreKeyBundle_GetPreKeyId(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return PreKeyBundle_GetPreKeyId;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public ECPublicKey getPreKey() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            long PreKeyBundle_GetPreKeyPublic = Native.PreKeyBundle_GetPreKeyPublic(nativeHandleGuard.nativeHandle());
            if (PreKeyBundle_GetPreKeyPublic != 0) {
                ECPublicKey eCPublicKey = new ECPublicKey(PreKeyBundle_GetPreKeyPublic);
                nativeHandleGuard.close();
                return eCPublicKey;
            }
            nativeHandleGuard.close();
            return null;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public int getSignedPreKeyId() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int PreKeyBundle_GetSignedPreKeyId = Native.PreKeyBundle_GetSignedPreKeyId(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return PreKeyBundle_GetSignedPreKeyId;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public ECPublicKey getSignedPreKey() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            ECPublicKey eCPublicKey = new ECPublicKey(Native.PreKeyBundle_GetSignedPreKeyPublic(nativeHandleGuard.nativeHandle()));
            nativeHandleGuard.close();
            return eCPublicKey;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] getSignedPreKeySignature() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] PreKeyBundle_GetSignedPreKeySignature = Native.PreKeyBundle_GetSignedPreKeySignature(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return PreKeyBundle_GetSignedPreKeySignature;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public IdentityKey getIdentityKey() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            IdentityKey identityKey = new IdentityKey(new ECPublicKey(Native.PreKeyBundle_GetIdentityKey(nativeHandleGuard.nativeHandle())));
            nativeHandleGuard.close();
            return identityKey;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public int getRegistrationId() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int PreKeyBundle_GetRegistrationId = Native.PreKeyBundle_GetRegistrationId(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return PreKeyBundle_GetRegistrationId;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    @Override // org.signal.libsignal.internal.NativeHandleGuard.Owner
    public long unsafeNativeHandleWithoutGuard() {
        return this.unsafeHandle;
    }
}
