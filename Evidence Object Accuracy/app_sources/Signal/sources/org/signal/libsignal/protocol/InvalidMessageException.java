package org.signal.libsignal.protocol;

import java.util.List;

/* loaded from: classes3.dex */
public class InvalidMessageException extends Exception {
    public InvalidMessageException() {
    }

    public InvalidMessageException(String str) {
        super(str);
    }

    public InvalidMessageException(Throwable th) {
        super(th);
    }

    public InvalidMessageException(String str, Throwable th) {
        super(str, th);
    }

    public InvalidMessageException(String str, List<Exception> list) {
        super(str, list.get(0));
    }
}
