package org.signal.libsignal.protocol;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;
import org.signal.libsignal.protocol.message.CiphertextMessage;
import org.signal.libsignal.protocol.message.PreKeySignalMessage;
import org.signal.libsignal.protocol.message.SignalMessage;
import org.signal.libsignal.protocol.state.IdentityKeyStore;
import org.signal.libsignal.protocol.state.PreKeyStore;
import org.signal.libsignal.protocol.state.SessionStore;
import org.signal.libsignal.protocol.state.SignalProtocolStore;
import org.signal.libsignal.protocol.state.SignedPreKeyStore;

/* loaded from: classes3.dex */
public class SessionCipher {
    private final IdentityKeyStore identityKeyStore;
    private final PreKeyStore preKeyStore;
    private final SignalProtocolAddress remoteAddress;
    private final SessionStore sessionStore;
    private final SignedPreKeyStore signedPreKeyStore;

    public SessionCipher(SessionStore sessionStore, PreKeyStore preKeyStore, SignedPreKeyStore signedPreKeyStore, IdentityKeyStore identityKeyStore, SignalProtocolAddress signalProtocolAddress) {
        this.sessionStore = sessionStore;
        this.preKeyStore = preKeyStore;
        this.identityKeyStore = identityKeyStore;
        this.remoteAddress = signalProtocolAddress;
        this.signedPreKeyStore = signedPreKeyStore;
    }

    public SessionCipher(SignalProtocolStore signalProtocolStore, SignalProtocolAddress signalProtocolAddress) {
        this(signalProtocolStore, signalProtocolStore, signalProtocolStore, signalProtocolStore, signalProtocolAddress);
    }

    public CiphertextMessage encrypt(byte[] bArr) throws UntrustedIdentityException {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this.remoteAddress);
        try {
            CiphertextMessage SessionCipher_EncryptMessage = Native.SessionCipher_EncryptMessage(bArr, nativeHandleGuard.nativeHandle(), this.sessionStore, this.identityKeyStore, null);
            nativeHandleGuard.close();
            return SessionCipher_EncryptMessage;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] decrypt(PreKeySignalMessage preKeySignalMessage) throws DuplicateMessageException, InvalidMessageException, InvalidKeyIdException, InvalidKeyException, UntrustedIdentityException {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(preKeySignalMessage);
        try {
            NativeHandleGuard nativeHandleGuard2 = new NativeHandleGuard(this.remoteAddress);
            byte[] SessionCipher_DecryptPreKeySignalMessage = Native.SessionCipher_DecryptPreKeySignalMessage(nativeHandleGuard.nativeHandle(), nativeHandleGuard2.nativeHandle(), this.sessionStore, this.identityKeyStore, this.preKeyStore, this.signedPreKeyStore, null);
            nativeHandleGuard2.close();
            nativeHandleGuard.close();
            return SessionCipher_DecryptPreKeySignalMessage;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] decrypt(SignalMessage signalMessage) throws InvalidMessageException, InvalidVersionException, DuplicateMessageException, NoSessionException, UntrustedIdentityException {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(signalMessage);
        try {
            NativeHandleGuard nativeHandleGuard2 = new NativeHandleGuard(this.remoteAddress);
            byte[] SessionCipher_DecryptSignalMessage = Native.SessionCipher_DecryptSignalMessage(nativeHandleGuard.nativeHandle(), nativeHandleGuard2.nativeHandle(), this.sessionStore, this.identityKeyStore, null);
            nativeHandleGuard2.close();
            nativeHandleGuard.close();
            return SessionCipher_DecryptSignalMessage;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public int getRemoteRegistrationId() {
        if (this.sessionStore.containsSession(this.remoteAddress)) {
            return this.sessionStore.loadSession(this.remoteAddress).getRemoteRegistrationId();
        }
        throw new IllegalStateException(String.format("No session for (%s)!", this.remoteAddress));
    }

    public int getSessionVersion() {
        if (this.sessionStore.containsSession(this.remoteAddress)) {
            return this.sessionStore.loadSession(this.remoteAddress).getSessionVersion();
        }
        throw new IllegalStateException(String.format("No session for (%s)!", this.remoteAddress));
    }
}
