package org.signal.libsignal.protocol;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;
import org.signal.libsignal.protocol.ecc.Curve;
import org.signal.libsignal.protocol.ecc.ECPublicKey;
import org.signal.libsignal.protocol.util.Hex;

/* loaded from: classes3.dex */
public class IdentityKey {
    private final ECPublicKey publicKey;

    public IdentityKey(ECPublicKey eCPublicKey) {
        this.publicKey = eCPublicKey;
    }

    public IdentityKey(byte[] bArr, int i) throws InvalidKeyException {
        this.publicKey = Curve.decodePoint(bArr, i);
    }

    public IdentityKey(byte[] bArr) throws InvalidKeyException {
        this.publicKey = Curve.decodePoint(bArr, 0);
    }

    public IdentityKey(long j) {
        this.publicKey = new ECPublicKey(j);
    }

    public ECPublicKey getPublicKey() {
        return this.publicKey;
    }

    public byte[] serialize() {
        return this.publicKey.serialize();
    }

    public String getFingerprint() {
        return Hex.toString(this.publicKey.serialize());
    }

    public boolean verifyAlternateIdentity(IdentityKey identityKey, byte[] bArr) {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this.publicKey);
        try {
            NativeHandleGuard nativeHandleGuard2 = new NativeHandleGuard(identityKey.publicKey);
            boolean IdentityKey_VerifyAlternateIdentity = Native.IdentityKey_VerifyAlternateIdentity(nativeHandleGuard.nativeHandle(), nativeHandleGuard2.nativeHandle(), bArr);
            nativeHandleGuard2.close();
            nativeHandleGuard.close();
            return IdentityKey_VerifyAlternateIdentity;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public boolean equals(Object obj) {
        if (obj != null && (obj instanceof IdentityKey)) {
            return this.publicKey.equals(((IdentityKey) obj).getPublicKey());
        }
        return false;
    }

    public int hashCode() {
        return this.publicKey.hashCode();
    }
}
