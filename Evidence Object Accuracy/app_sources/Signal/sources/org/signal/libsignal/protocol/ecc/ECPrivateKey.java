package org.signal.libsignal.protocol.ecc;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;

/* loaded from: classes3.dex */
public class ECPrivateKey implements NativeHandleGuard.Owner {
    private final long unsafeHandle;

    public static ECPrivateKey generate() {
        return new ECPrivateKey(Native.ECPrivateKey_Generate());
    }

    public ECPrivateKey(byte[] bArr) {
        this.unsafeHandle = Native.ECPrivateKey_Deserialize(bArr);
    }

    public ECPrivateKey(long j) {
        if (j != 0) {
            this.unsafeHandle = j;
            return;
        }
        throw null;
    }

    protected void finalize() {
        Native.ECPrivateKey_Destroy(this.unsafeHandle);
    }

    public byte[] serialize() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] ECPrivateKey_Serialize = Native.ECPrivateKey_Serialize(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return ECPrivateKey_Serialize;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] calculateSignature(byte[] bArr) {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] ECPrivateKey_Sign = Native.ECPrivateKey_Sign(nativeHandleGuard.nativeHandle(), bArr);
            nativeHandleGuard.close();
            return ECPrivateKey_Sign;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] calculateAgreement(ECPublicKey eCPublicKey) {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            NativeHandleGuard nativeHandleGuard2 = new NativeHandleGuard(eCPublicKey);
            byte[] ECPrivateKey_Agree = Native.ECPrivateKey_Agree(nativeHandleGuard.nativeHandle(), nativeHandleGuard2.nativeHandle());
            nativeHandleGuard2.close();
            nativeHandleGuard.close();
            return ECPrivateKey_Agree;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    @Override // org.signal.libsignal.internal.NativeHandleGuard.Owner
    public long unsafeNativeHandleWithoutGuard() {
        return this.unsafeHandle;
    }

    public ECPublicKey publicKey() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            ECPublicKey eCPublicKey = new ECPublicKey(Native.ECPrivateKey_GetPublicKey(nativeHandleGuard.nativeHandle()));
            nativeHandleGuard.close();
            return eCPublicKey;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }
}
