package org.signal.libsignal.protocol;

/* loaded from: classes3.dex */
public class InvalidKeyIdException extends Exception {
    public InvalidKeyIdException(String str) {
        super(str);
    }

    public InvalidKeyIdException(Throwable th) {
        super(th);
    }
}
