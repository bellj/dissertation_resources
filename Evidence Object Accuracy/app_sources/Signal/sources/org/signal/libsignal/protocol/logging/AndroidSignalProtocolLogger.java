package org.signal.libsignal.protocol.logging;

import android.util.Log;
import android.util.SparseIntArray;

/* loaded from: classes3.dex */
public class AndroidSignalProtocolLogger implements SignalProtocolLogger {
    private static final SparseIntArray PRIORITY_MAP = new SparseIntArray(5) { // from class: org.signal.libsignal.protocol.logging.AndroidSignalProtocolLogger.1
        {
            put(4, 4);
            put(7, 7);
            put(3, 3);
            put(2, 2);
            put(5, 5);
        }
    };

    @Override // org.signal.libsignal.protocol.logging.SignalProtocolLogger
    public void log(int i, String str, String str2) {
        Log.println(PRIORITY_MAP.get(i, 5), str, str2);
    }
}
