package org.signal.libsignal.protocol;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;
import org.signal.libsignal.protocol.ecc.Curve;
import org.signal.libsignal.protocol.ecc.ECKeyPair;
import org.signal.libsignal.protocol.ecc.ECPrivateKey;

/* loaded from: classes3.dex */
public class IdentityKeyPair {
    private final ECPrivateKey privateKey;
    private final IdentityKey publicKey;

    public IdentityKeyPair(IdentityKey identityKey, ECPrivateKey eCPrivateKey) {
        this.publicKey = identityKey;
        this.privateKey = eCPrivateKey;
    }

    public IdentityKeyPair(byte[] bArr) {
        long[] IdentityKeyPair_Deserialize = Native.IdentityKeyPair_Deserialize(bArr);
        long j = IdentityKeyPair_Deserialize[0];
        long j2 = IdentityKeyPair_Deserialize[1];
        this.publicKey = new IdentityKey(j);
        this.privateKey = new ECPrivateKey(j2);
    }

    public static IdentityKeyPair generate() {
        ECKeyPair generateKeyPair = Curve.generateKeyPair();
        return new IdentityKeyPair(new IdentityKey(generateKeyPair.getPublicKey()), generateKeyPair.getPrivateKey());
    }

    public IdentityKey getPublicKey() {
        return this.publicKey;
    }

    public ECPrivateKey getPrivateKey() {
        return this.privateKey;
    }

    public byte[] serialize() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this.publicKey.getPublicKey());
        try {
            NativeHandleGuard nativeHandleGuard2 = new NativeHandleGuard(this.privateKey);
            byte[] IdentityKeyPair_Serialize = Native.IdentityKeyPair_Serialize(nativeHandleGuard.nativeHandle(), nativeHandleGuard2.nativeHandle());
            nativeHandleGuard2.close();
            nativeHandleGuard.close();
            return IdentityKeyPair_Serialize;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] signAlternateIdentity(IdentityKey identityKey) {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this.publicKey.getPublicKey());
        try {
            NativeHandleGuard nativeHandleGuard2 = new NativeHandleGuard(this.privateKey);
            nativeHandleGuard = new NativeHandleGuard(identityKey.getPublicKey());
            try {
                byte[] IdentityKeyPair_SignAlternateIdentity = Native.IdentityKeyPair_SignAlternateIdentity(nativeHandleGuard.nativeHandle(), nativeHandleGuard2.nativeHandle(), nativeHandleGuard.nativeHandle());
                nativeHandleGuard.close();
                nativeHandleGuard2.close();
                nativeHandleGuard.close();
                return IdentityKeyPair_SignAlternateIdentity;
            } finally {
            }
        } finally {
        }
    }
}
