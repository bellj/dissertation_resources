package org.signal.libsignal.protocol.fingerprint;

/* loaded from: classes3.dex */
public class FingerprintParsingException extends Exception {
    public FingerprintParsingException(String str) {
        super(str);
    }
}
