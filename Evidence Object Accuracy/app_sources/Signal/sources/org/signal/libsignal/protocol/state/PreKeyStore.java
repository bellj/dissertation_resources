package org.signal.libsignal.protocol.state;

import org.signal.libsignal.protocol.InvalidKeyIdException;

/* loaded from: classes3.dex */
public interface PreKeyStore {
    boolean containsPreKey(int i);

    PreKeyRecord loadPreKey(int i) throws InvalidKeyIdException;

    void removePreKey(int i);

    void storePreKey(int i, PreKeyRecord preKeyRecord);
}
