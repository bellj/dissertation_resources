package org.signal.libsignal.protocol.state;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.ecc.ECKeyPair;
import org.signal.libsignal.protocol.ecc.ECPrivateKey;
import org.signal.libsignal.protocol.ecc.ECPublicKey;

/* loaded from: classes3.dex */
public class PreKeyRecord implements NativeHandleGuard.Owner {
    private final long unsafeHandle;

    protected void finalize() {
        Native.PreKeyRecord_Destroy(this.unsafeHandle);
    }

    public PreKeyRecord(int i, ECKeyPair eCKeyPair) {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(eCKeyPair.getPublicKey());
        try {
            NativeHandleGuard nativeHandleGuard2 = new NativeHandleGuard(eCKeyPair.getPrivateKey());
            this.unsafeHandle = Native.PreKeyRecord_New(i, nativeHandleGuard.nativeHandle(), nativeHandleGuard2.nativeHandle());
            nativeHandleGuard2.close();
            nativeHandleGuard.close();
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public PreKeyRecord(byte[] bArr) throws InvalidMessageException {
        this.unsafeHandle = Native.PreKeyRecord_Deserialize(bArr);
    }

    public int getId() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int PreKeyRecord_GetId = Native.PreKeyRecord_GetId(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return PreKeyRecord_GetId;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public ECKeyPair getKeyPair() throws InvalidKeyException {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            ECKeyPair eCKeyPair = new ECKeyPair(new ECPublicKey(Native.PreKeyRecord_GetPublicKey(nativeHandleGuard.nativeHandle())), new ECPrivateKey(Native.PreKeyRecord_GetPrivateKey(nativeHandleGuard.nativeHandle())));
            nativeHandleGuard.close();
            return eCKeyPair;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] serialize() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] PreKeyRecord_GetSerialized = Native.PreKeyRecord_GetSerialized(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return PreKeyRecord_GetSerialized;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    @Override // org.signal.libsignal.internal.NativeHandleGuard.Owner
    public long unsafeNativeHandleWithoutGuard() {
        return this.unsafeHandle;
    }
}
