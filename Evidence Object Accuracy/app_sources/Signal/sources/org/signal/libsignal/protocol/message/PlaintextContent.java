package org.signal.libsignal.protocol.message;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.InvalidVersionException;

/* loaded from: classes3.dex */
public final class PlaintextContent implements CiphertextMessage, NativeHandleGuard.Owner {
    private final long unsafeHandle;

    @Override // org.signal.libsignal.protocol.message.CiphertextMessage
    public int getType() {
        return 8;
    }

    protected void finalize() {
        Native.PlaintextContent_Destroy(this.unsafeHandle);
    }

    @Override // org.signal.libsignal.protocol.message.CiphertextMessage, org.signal.libsignal.internal.NativeHandleGuard.Owner
    public long unsafeNativeHandleWithoutGuard() {
        return this.unsafeHandle;
    }

    private PlaintextContent(long j) {
        this.unsafeHandle = j;
    }

    public PlaintextContent(DecryptionErrorMessage decryptionErrorMessage) {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(decryptionErrorMessage);
        try {
            this.unsafeHandle = Native.PlaintextContent_FromDecryptionErrorMessage(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public PlaintextContent(byte[] bArr) throws InvalidMessageException, InvalidVersionException {
        this.unsafeHandle = Native.PlaintextContent_Deserialize(bArr);
    }

    @Override // org.signal.libsignal.protocol.message.CiphertextMessage
    public byte[] serialize() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] PlaintextContent_GetSerialized = Native.PlaintextContent_GetSerialized(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return PlaintextContent_GetSerialized;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] getBody() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] PlaintextContent_GetBody = Native.PlaintextContent_GetBody(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return PlaintextContent_GetBody;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }
}
