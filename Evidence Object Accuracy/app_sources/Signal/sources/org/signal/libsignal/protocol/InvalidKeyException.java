package org.signal.libsignal.protocol;

/* loaded from: classes3.dex */
public class InvalidKeyException extends Exception {
    public InvalidKeyException() {
    }

    public InvalidKeyException(String str) {
        super(str);
    }

    public InvalidKeyException(Throwable th) {
        super(th);
    }

    public InvalidKeyException(String str, Throwable th) {
        super(str, th);
    }
}
