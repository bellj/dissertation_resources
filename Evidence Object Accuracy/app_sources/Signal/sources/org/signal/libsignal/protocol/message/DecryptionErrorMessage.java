package org.signal.libsignal.protocol.message;

import j$.util.Optional;
import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.ecc.ECPublicKey;

/* loaded from: classes3.dex */
public final class DecryptionErrorMessage implements NativeHandleGuard.Owner {
    final long unsafeHandle;

    protected void finalize() {
        Native.DecryptionErrorMessage_Destroy(this.unsafeHandle);
    }

    @Override // org.signal.libsignal.internal.NativeHandleGuard.Owner
    public long unsafeNativeHandleWithoutGuard() {
        return this.unsafeHandle;
    }

    DecryptionErrorMessage(long j) {
        this.unsafeHandle = j;
    }

    public DecryptionErrorMessage(byte[] bArr) throws InvalidMessageException {
        this.unsafeHandle = Native.DecryptionErrorMessage_Deserialize(bArr);
    }

    public static DecryptionErrorMessage forOriginalMessage(byte[] bArr, int i, long j, int i2) {
        return new DecryptionErrorMessage(Native.DecryptionErrorMessage_ForOriginalMessage(bArr, i, j, i2));
    }

    public byte[] serialize() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] DecryptionErrorMessage_GetSerialized = Native.DecryptionErrorMessage_GetSerialized(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return DecryptionErrorMessage_GetSerialized;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public Optional<ECPublicKey> getRatchetKey() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            long DecryptionErrorMessage_GetRatchetKey = Native.DecryptionErrorMessage_GetRatchetKey(nativeHandleGuard.nativeHandle());
            if (DecryptionErrorMessage_GetRatchetKey == 0) {
                Optional<ECPublicKey> empty = Optional.empty();
                nativeHandleGuard.close();
                return empty;
            }
            Optional<ECPublicKey> of = Optional.of(new ECPublicKey(DecryptionErrorMessage_GetRatchetKey));
            nativeHandleGuard.close();
            return of;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public long getTimestamp() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            long DecryptionErrorMessage_GetTimestamp = Native.DecryptionErrorMessage_GetTimestamp(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return DecryptionErrorMessage_GetTimestamp;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public int getDeviceId() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int DecryptionErrorMessage_GetDeviceId = Native.DecryptionErrorMessage_GetDeviceId(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return DecryptionErrorMessage_GetDeviceId;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public static DecryptionErrorMessage extractFromSerializedContent(byte[] bArr) throws InvalidMessageException {
        return new DecryptionErrorMessage(Native.DecryptionErrorMessage_ExtractFromSerializedContent(bArr));
    }
}
