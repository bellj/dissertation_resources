package org.signal.libsignal.protocol;

/* loaded from: classes3.dex */
public class InvalidRegistrationIdException extends Exception {
    private final SignalProtocolAddress address;

    public InvalidRegistrationIdException(SignalProtocolAddress signalProtocolAddress, String str) {
        super(str);
        this.address = signalProtocolAddress;
    }

    public SignalProtocolAddress getAddress() {
        return this.address;
    }
}
