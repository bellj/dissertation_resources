package org.signal.libsignal.hsmenclave;

/* loaded from: classes3.dex */
public class EnclaveCommunicationFailureException extends Exception {
    public EnclaveCommunicationFailureException(String str) {
        super(str);
    }

    public EnclaveCommunicationFailureException(Throwable th) {
        super(th);
    }
}
