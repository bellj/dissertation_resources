package org.signal.libsignal.hsmenclave;

/* loaded from: classes3.dex */
public class TrustedCodeMismatchException extends Exception {
    public TrustedCodeMismatchException(String str) {
        super(str);
    }

    public TrustedCodeMismatchException(Throwable th) {
        super(th);
    }
}
