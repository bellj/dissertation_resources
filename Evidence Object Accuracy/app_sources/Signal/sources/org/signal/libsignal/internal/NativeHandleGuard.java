package org.signal.libsignal.internal;

/* loaded from: classes3.dex */
public class NativeHandleGuard implements AutoCloseable {
    private final Owner owner;

    /* loaded from: classes3.dex */
    public interface Owner {
        long unsafeNativeHandleWithoutGuard();
    }

    public NativeHandleGuard(Owner owner) {
        this.owner = owner;
    }

    public long nativeHandle() {
        Owner owner = this.owner;
        if (owner == null) {
            return 0;
        }
        return owner.unsafeNativeHandleWithoutGuard();
    }

    @Override // java.lang.AutoCloseable
    public void close() {
        Native.keepAlive(this.owner);
    }
}
