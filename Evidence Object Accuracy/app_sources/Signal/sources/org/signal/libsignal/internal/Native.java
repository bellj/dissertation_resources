package org.signal.libsignal.internal;

import androidx.recyclerview.widget.RecyclerView;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.attribute.FileAttribute;
import java.util.Locale;
import java.util.UUID;
import org.signal.libsignal.protocol.groups.state.SenderKeyStore;
import org.signal.libsignal.protocol.logging.Log;
import org.signal.libsignal.protocol.message.CiphertextMessage;
import org.signal.libsignal.protocol.state.IdentityKeyStore;
import org.signal.libsignal.protocol.state.PreKeyStore;
import org.signal.libsignal.protocol.state.SessionStore;
import org.signal.libsignal.protocol.state.SignedPreKeyStore;

/* loaded from: classes3.dex */
public final class Native {
    public static native void Aes256GcmDecryption_Destroy(long j);

    public static native long Aes256GcmDecryption_New(byte[] bArr, byte[] bArr2, byte[] bArr3);

    public static native void Aes256GcmDecryption_Update(long j, byte[] bArr, int i, int i2);

    public static native boolean Aes256GcmDecryption_VerifyTag(long j, byte[] bArr);

    public static native void AuthCredentialPresentation_CheckValidContents(byte[] bArr);

    public static native void AuthCredentialWithPniResponse_CheckValidContents(byte[] bArr);

    public static native void AuthCredentialWithPni_CheckValidContents(byte[] bArr);

    public static native void Cds2ClientState_CompleteHandshake(long j, byte[] bArr);

    public static native void Cds2ClientState_Destroy(long j);

    public static native byte[] Cds2ClientState_EstablishedRecv(long j, byte[] bArr);

    public static native byte[] Cds2ClientState_EstablishedSend(long j, byte[] bArr);

    public static native byte[] Cds2ClientState_InitialRequest(long j);

    public static native long Cds2ClientState_New(byte[] bArr, byte[] bArr2, byte[] bArr3, long j);

    public static native long DecryptionErrorMessage_Deserialize(byte[] bArr);

    public static native void DecryptionErrorMessage_Destroy(long j);

    public static native long DecryptionErrorMessage_ExtractFromSerializedContent(byte[] bArr);

    public static native long DecryptionErrorMessage_ForOriginalMessage(byte[] bArr, int i, long j, int i2);

    public static native int DecryptionErrorMessage_GetDeviceId(long j);

    public static native long DecryptionErrorMessage_GetRatchetKey(long j);

    public static native byte[] DecryptionErrorMessage_GetSerialized(long j);

    public static native long DecryptionErrorMessage_GetTimestamp(long j);

    public static native byte[] DeviceTransfer_GenerateCertificate(byte[] bArr, String str, int i);

    public static native byte[] DeviceTransfer_GeneratePrivateKey();

    public static native byte[] ECPrivateKey_Agree(long j, long j2);

    public static native long ECPrivateKey_Deserialize(byte[] bArr);

    public static native void ECPrivateKey_Destroy(long j);

    public static native long ECPrivateKey_Generate();

    public static native long ECPrivateKey_GetPublicKey(long j);

    public static native byte[] ECPrivateKey_Serialize(long j);

    public static native byte[] ECPrivateKey_Sign(long j, byte[] bArr);

    public static native int ECPublicKey_Compare(long j, long j2);

    public static native long ECPublicKey_Deserialize(byte[] bArr, int i);

    public static native void ECPublicKey_Destroy(long j);

    public static native byte[] ECPublicKey_GetPublicKeyBytes(long j);

    public static native byte[] ECPublicKey_Serialize(long j);

    public static native boolean ECPublicKey_Verify(long j, byte[] bArr, byte[] bArr2);

    public static native void ExpiringProfileKeyCredentialResponse_CheckValidContents(byte[] bArr);

    public static native void ExpiringProfileKeyCredential_CheckValidContents(byte[] bArr);

    public static native long ExpiringProfileKeyCredential_GetExpirationTime(byte[] bArr);

    public static native byte[] GroupCipher_DecryptMessage(long j, byte[] bArr, SenderKeyStore senderKeyStore, Object obj);

    public static native CiphertextMessage GroupCipher_EncryptMessage(long j, UUID uuid, byte[] bArr, SenderKeyStore senderKeyStore, Object obj);

    public static native void GroupPublicParams_CheckValidContents(byte[] bArr);

    public static native byte[] GroupPublicParams_GetGroupIdentifier(byte[] bArr);

    public static native void GroupSecretParams_CheckValidContents(byte[] bArr);

    public static native byte[] GroupSecretParams_DecryptBlobWithPadding(byte[] bArr, byte[] bArr2);

    public static native byte[] GroupSecretParams_DecryptProfileKey(byte[] bArr, byte[] bArr2, UUID uuid);

    public static native UUID GroupSecretParams_DecryptUuid(byte[] bArr, byte[] bArr2);

    public static native byte[] GroupSecretParams_DeriveFromMasterKey(byte[] bArr);

    public static native byte[] GroupSecretParams_EncryptBlobWithPaddingDeterministic(byte[] bArr, byte[] bArr2, byte[] bArr3, int i);

    public static native byte[] GroupSecretParams_EncryptUuid(byte[] bArr, UUID uuid);

    public static native byte[] GroupSecretParams_GenerateDeterministic(byte[] bArr);

    public static native byte[] GroupSecretParams_GetMasterKey(byte[] bArr);

    public static native byte[] GroupSecretParams_GetPublicParams(byte[] bArr);

    public static native long GroupSessionBuilder_CreateSenderKeyDistributionMessage(long j, UUID uuid, SenderKeyStore senderKeyStore, Object obj);

    public static native void GroupSessionBuilder_ProcessSenderKeyDistributionMessage(long j, long j2, SenderKeyStore senderKeyStore, Object obj);

    public static native byte[] HKDF_DeriveSecrets(int i, byte[] bArr, byte[] bArr2, byte[] bArr3);

    public static native long[] IdentityKeyPair_Deserialize(byte[] bArr);

    public static native byte[] IdentityKeyPair_Serialize(long j, long j2);

    public static native byte[] IdentityKeyPair_SignAlternateIdentity(long j, long j2, long j3);

    public static native boolean IdentityKey_VerifyAlternateIdentity(long j, long j2, byte[] bArr);

    public static native void Logger_Initialize(int i, Class cls);

    public static native void NumericFingerprintGenerator_Destroy(long j);

    public static native String NumericFingerprintGenerator_GetDisplayString(long j);

    public static native byte[] NumericFingerprintGenerator_GetScannableEncoding(long j);

    public static native long NumericFingerprintGenerator_New(int i, int i2, byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4);

    public static native long PlaintextContent_Deserialize(byte[] bArr);

    public static native byte[] PlaintextContent_DeserializeAndGetContent(byte[] bArr);

    public static native void PlaintextContent_Destroy(long j);

    public static native long PlaintextContent_FromDecryptionErrorMessage(long j);

    public static native byte[] PlaintextContent_GetBody(long j);

    public static native byte[] PlaintextContent_GetSerialized(long j);

    public static native void PreKeyBundle_Destroy(long j);

    public static native int PreKeyBundle_GetDeviceId(long j);

    public static native long PreKeyBundle_GetIdentityKey(long j);

    public static native int PreKeyBundle_GetPreKeyId(long j);

    public static native long PreKeyBundle_GetPreKeyPublic(long j);

    public static native int PreKeyBundle_GetRegistrationId(long j);

    public static native int PreKeyBundle_GetSignedPreKeyId(long j);

    public static native long PreKeyBundle_GetSignedPreKeyPublic(long j);

    public static native byte[] PreKeyBundle_GetSignedPreKeySignature(long j);

    public static native long PreKeyBundle_New(int i, int i2, int i3, long j, int i4, long j2, byte[] bArr, long j3);

    public static native long PreKeyRecord_Deserialize(byte[] bArr);

    public static native void PreKeyRecord_Destroy(long j);

    public static native int PreKeyRecord_GetId(long j);

    public static native long PreKeyRecord_GetPrivateKey(long j);

    public static native long PreKeyRecord_GetPublicKey(long j);

    public static native byte[] PreKeyRecord_GetSerialized(long j);

    public static native long PreKeyRecord_New(int i, long j, long j2);

    public static native long PreKeySignalMessage_Deserialize(byte[] bArr);

    public static native void PreKeySignalMessage_Destroy(long j);

    public static native long PreKeySignalMessage_GetBaseKey(long j);

    public static native long PreKeySignalMessage_GetIdentityKey(long j);

    public static native int PreKeySignalMessage_GetPreKeyId(long j);

    public static native int PreKeySignalMessage_GetRegistrationId(long j);

    public static native byte[] PreKeySignalMessage_GetSerialized(long j);

    public static native long PreKeySignalMessage_GetSignalMessage(long j);

    public static native int PreKeySignalMessage_GetSignedPreKeyId(long j);

    public static native int PreKeySignalMessage_GetVersion(long j);

    public static native void ProfileKeyCiphertext_CheckValidContents(byte[] bArr);

    public static native void ProfileKeyCommitment_CheckValidContents(byte[] bArr);

    public static native void ProfileKeyCredentialPresentation_CheckValidContents(byte[] bArr);

    public static native byte[] ProfileKeyCredentialPresentation_GetProfileKeyCiphertext(byte[] bArr);

    public static native byte[] ProfileKeyCredentialPresentation_GetUuidCiphertext(byte[] bArr);

    public static native void ProfileKeyCredentialRequestContext_CheckValidContents(byte[] bArr);

    public static native byte[] ProfileKeyCredentialRequestContext_GetRequest(byte[] bArr);

    public static native void ProfileKeyCredentialRequest_CheckValidContents(byte[] bArr);

    public static native void ProfileKeyCredential_CheckValidContents(byte[] bArr);

    public static native void ProfileKey_CheckValidContents(byte[] bArr);

    public static native byte[] ProfileKey_GetCommitment(byte[] bArr, UUID uuid);

    public static native byte[] ProfileKey_GetProfileKeyVersion(byte[] bArr, UUID uuid);

    public static native void ProtocolAddress_Destroy(long j);

    public static native int ProtocolAddress_DeviceId(long j);

    public static native String ProtocolAddress_Name(long j);

    public static native long ProtocolAddress_New(String str, int i);

    public static native void ReceiptCredentialPresentation_CheckValidContents(byte[] bArr);

    public static native long ReceiptCredentialPresentation_GetReceiptExpirationTime(byte[] bArr);

    public static native long ReceiptCredentialPresentation_GetReceiptLevel(byte[] bArr);

    public static native void ReceiptCredentialRequestContext_CheckValidContents(byte[] bArr);

    public static native byte[] ReceiptCredentialRequestContext_GetRequest(byte[] bArr);

    public static native void ReceiptCredentialRequest_CheckValidContents(byte[] bArr);

    public static native void ReceiptCredentialResponse_CheckValidContents(byte[] bArr);

    public static native void ReceiptCredential_CheckValidContents(byte[] bArr);

    public static native long ReceiptCredential_GetReceiptExpirationTime(byte[] bArr);

    public static native long ReceiptCredential_GetReceiptLevel(byte[] bArr);

    public static native boolean ScannableFingerprint_Compare(byte[] bArr, byte[] bArr2);

    public static native long SealedSessionCipher_DecryptToUsmc(byte[] bArr, IdentityKeyStore identityKeyStore, Object obj);

    public static native byte[] SealedSessionCipher_Encrypt(long j, long j2, IdentityKeyStore identityKeyStore, Object obj);

    public static native byte[] SealedSessionCipher_MultiRecipientEncrypt(long[] jArr, long[] jArr2, long j, IdentityKeyStore identityKeyStore, Object obj);

    public static native long SenderCertificate_Deserialize(byte[] bArr);

    public static native void SenderCertificate_Destroy(long j);

    public static native int SenderCertificate_GetDeviceId(long j);

    public static native long SenderCertificate_GetExpiration(long j);

    public static native String SenderCertificate_GetSenderE164(long j);

    public static native String SenderCertificate_GetSenderUuid(long j);

    public static native boolean SenderCertificate_Validate(long j, long j2, long j3);

    public static native long SenderKeyDistributionMessage_Deserialize(byte[] bArr);

    public static native void SenderKeyDistributionMessage_Destroy(long j);

    public static native int SenderKeyDistributionMessage_GetChainId(long j);

    public static native byte[] SenderKeyDistributionMessage_GetChainKey(long j);

    public static native int SenderKeyDistributionMessage_GetIteration(long j);

    public static native byte[] SenderKeyDistributionMessage_GetSerialized(long j);

    public static native long SenderKeyDistributionMessage_GetSignatureKey(long j);

    public static native long SenderKeyMessage_Deserialize(byte[] bArr);

    public static native void SenderKeyMessage_Destroy(long j);

    public static native int SenderKeyMessage_GetChainId(long j);

    public static native byte[] SenderKeyMessage_GetCipherText(long j);

    public static native UUID SenderKeyMessage_GetDistributionId(long j);

    public static native int SenderKeyMessage_GetIteration(long j);

    public static native byte[] SenderKeyMessage_GetSerialized(long j);

    public static native boolean SenderKeyMessage_VerifySignature(long j, long j2);

    public static native long SenderKeyRecord_Deserialize(byte[] bArr);

    public static native void SenderKeyRecord_Destroy(long j);

    public static native byte[] SenderKeyRecord_GetSerialized(long j);

    public static native void ServerPublicParams_CheckValidContents(byte[] bArr);

    public static native byte[] ServerPublicParams_CreateAuthCredentialWithPniPresentationDeterministic(byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4);

    public static native byte[] ServerPublicParams_CreateExpiringProfileKeyCredentialPresentationDeterministic(byte[] bArr, byte[] bArr2, byte[] bArr3, byte[] bArr4);

    public static native byte[] ServerPublicParams_CreateProfileKeyCredentialRequestContextDeterministic(byte[] bArr, byte[] bArr2, UUID uuid, byte[] bArr3);

    public static native byte[] ServerPublicParams_CreateReceiptCredentialPresentationDeterministic(byte[] bArr, byte[] bArr2, byte[] bArr3);

    public static native byte[] ServerPublicParams_CreateReceiptCredentialRequestContextDeterministic(byte[] bArr, byte[] bArr2, byte[] bArr3);

    public static native byte[] ServerPublicParams_ReceiveAuthCredentialWithPni(byte[] bArr, UUID uuid, UUID uuid2, long j, byte[] bArr2);

    public static native byte[] ServerPublicParams_ReceiveExpiringProfileKeyCredential(byte[] bArr, byte[] bArr2, byte[] bArr3, long j);

    public static native byte[] ServerPublicParams_ReceiveProfileKeyCredential(byte[] bArr, byte[] bArr2, byte[] bArr3);

    public static native byte[] ServerPublicParams_ReceiveReceiptCredential(byte[] bArr, byte[] bArr2, byte[] bArr3);

    public static native void ServerPublicParams_VerifySignature(byte[] bArr, byte[] bArr2, byte[] bArr3);

    public static native void SessionBuilder_ProcessPreKeyBundle(long j, long j2, SessionStore sessionStore, IdentityKeyStore identityKeyStore, Object obj);

    public static native byte[] SessionCipher_DecryptPreKeySignalMessage(long j, long j2, SessionStore sessionStore, IdentityKeyStore identityKeyStore, PreKeyStore preKeyStore, SignedPreKeyStore signedPreKeyStore, Object obj);

    public static native byte[] SessionCipher_DecryptSignalMessage(long j, long j2, SessionStore sessionStore, IdentityKeyStore identityKeyStore, Object obj);

    public static native CiphertextMessage SessionCipher_EncryptMessage(byte[] bArr, long j, SessionStore sessionStore, IdentityKeyStore identityKeyStore, Object obj);

    public static native void SessionRecord_ArchiveCurrentState(long j);

    public static native boolean SessionRecord_CurrentRatchetKeyMatches(long j, long j2);

    public static native long SessionRecord_Deserialize(byte[] bArr);

    public static native void SessionRecord_Destroy(long j);

    public static native long SessionRecord_FromSingleSessionState(byte[] bArr);

    public static native byte[] SessionRecord_GetAliceBaseKey(long j);

    public static native byte[] SessionRecord_GetLocalIdentityKeyPublic(long j);

    public static native int SessionRecord_GetLocalRegistrationId(long j);

    public static native byte[] SessionRecord_GetReceiverChainKeyValue(long j, long j2);

    public static native byte[] SessionRecord_GetRemoteIdentityKeyPublic(long j);

    public static native int SessionRecord_GetRemoteRegistrationId(long j);

    public static native byte[] SessionRecord_GetSenderChainKeyValue(long j);

    public static native int SessionRecord_GetSessionVersion(long j);

    public static native boolean SessionRecord_HasSenderChain(long j);

    public static native long SessionRecord_InitializeAliceSession(long j, long j2, long j3, long j4, long j5, long j6, long j7);

    public static native long SessionRecord_InitializeBobSession(long j, long j2, long j3, long j4, long j5, long j6, long j7, long j8);

    public static native long SessionRecord_NewFresh();

    public static native byte[] SessionRecord_Serialize(long j);

    public static native long SignalMessage_Deserialize(byte[] bArr);

    public static native void SignalMessage_Destroy(long j);

    public static native byte[] SignalMessage_GetBody(long j);

    public static native int SignalMessage_GetCounter(long j);

    public static native int SignalMessage_GetMessageVersion(long j);

    public static native long SignalMessage_GetSenderRatchetKey(long j);

    public static native byte[] SignalMessage_GetSerialized(long j);

    public static native boolean SignalMessage_VerifyMac(long j, long j2, long j3, byte[] bArr);

    public static native long SignedPreKeyRecord_Deserialize(byte[] bArr);

    public static native void SignedPreKeyRecord_Destroy(long j);

    public static native int SignedPreKeyRecord_GetId(long j);

    public static native long SignedPreKeyRecord_GetPrivateKey(long j);

    public static native long SignedPreKeyRecord_GetPublicKey(long j);

    public static native byte[] SignedPreKeyRecord_GetSerialized(long j);

    public static native byte[] SignedPreKeyRecord_GetSignature(long j);

    public static native long SignedPreKeyRecord_GetTimestamp(long j);

    public static native long SignedPreKeyRecord_New(int i, long j, long j2, long j3, byte[] bArr);

    public static native void UnidentifiedSenderMessageContent_Destroy(long j);

    public static native int UnidentifiedSenderMessageContent_GetContentHint(long j);

    public static native byte[] UnidentifiedSenderMessageContent_GetContents(long j);

    public static native byte[] UnidentifiedSenderMessageContent_GetGroupId(long j);

    public static native int UnidentifiedSenderMessageContent_GetMsgType(long j);

    public static native long UnidentifiedSenderMessageContent_GetSenderCert(long j);

    public static native long UnidentifiedSenderMessageContent_New(CiphertextMessage ciphertextMessage, long j, int i, byte[] bArr);

    public static native void UuidCiphertext_CheckValidContents(byte[] bArr);

    public static native void keepAlive(Object obj);

    private static void copyToTempFileAndLoad(InputStream inputStream, String str) throws IOException {
        File file = Files.createTempFile("resource", str, new FileAttribute[0]).toFile();
        file.deleteOnExit();
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        try {
            byte[] bArr = new byte[RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT];
            while (true) {
                int read = inputStream.read(bArr);
                if (read != -1) {
                    fileOutputStream.write(bArr, 0, read);
                } else {
                    fileOutputStream.close();
                    System.load(file.getAbsolutePath());
                    return;
                }
            }
        } catch (Throwable th) {
            try {
                fileOutputStream.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    private static void loadLibrary() {
        try {
            String str = System.getProperty("os.name").toLowerCase(Locale.ROOT).startsWith("mac os") ? ".dylib" : ".so";
            InputStream resourceAsStream = Native.class.getResourceAsStream("/libsignal_jni" + str);
            if (resourceAsStream != null) {
                copyToTempFileAndLoad(resourceAsStream, str);
            } else {
                System.loadLibrary("signal_jni");
            }
            if (resourceAsStream != null) {
                resourceAsStream.close();
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    static {
        loadLibrary();
        Logger_Initialize(4, Log.class);
    }

    private Native() {
    }
}
