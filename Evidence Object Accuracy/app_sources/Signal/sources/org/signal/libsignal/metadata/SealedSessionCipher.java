package org.signal.libsignal.metadata;

import j$.util.Optional;
import java.util.List;
import java.util.UUID;
import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;
import org.signal.libsignal.metadata.certificate.CertificateValidator;
import org.signal.libsignal.metadata.protocol.UnidentifiedSenderMessageContent;
import org.signal.libsignal.protocol.DuplicateMessageException;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.InvalidKeyIdException;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.InvalidRegistrationIdException;
import org.signal.libsignal.protocol.InvalidVersionException;
import org.signal.libsignal.protocol.LegacyMessageException;
import org.signal.libsignal.protocol.NoSessionException;
import org.signal.libsignal.protocol.SessionCipher;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.UntrustedIdentityException;
import org.signal.libsignal.protocol.groups.GroupCipher;
import org.signal.libsignal.protocol.message.PreKeySignalMessage;
import org.signal.libsignal.protocol.message.SignalMessage;
import org.signal.libsignal.protocol.state.SessionRecord;
import org.signal.libsignal.protocol.state.SignalProtocolStore;

/* loaded from: classes3.dex */
public class SealedSessionCipher {
    private final int localDeviceId;
    private final String localE164Address;
    private final String localUuidAddress;
    private final SignalProtocolStore signalProtocolStore;

    public SealedSessionCipher(SignalProtocolStore signalProtocolStore, UUID uuid, String str, int i) {
        this.signalProtocolStore = signalProtocolStore;
        this.localUuidAddress = uuid.toString();
        this.localE164Address = str;
        this.localDeviceId = i;
    }

    public byte[] encrypt(SignalProtocolAddress signalProtocolAddress, UnidentifiedSenderMessageContent unidentifiedSenderMessageContent) throws InvalidKeyException, UntrustedIdentityException {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(signalProtocolAddress);
        try {
            NativeHandleGuard nativeHandleGuard2 = new NativeHandleGuard(unidentifiedSenderMessageContent);
            byte[] SealedSessionCipher_Encrypt = Native.SealedSessionCipher_Encrypt(nativeHandleGuard.nativeHandle(), nativeHandleGuard2.nativeHandle(), this.signalProtocolStore, null);
            nativeHandleGuard2.close();
            nativeHandleGuard.close();
            return SealedSessionCipher_Encrypt;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] multiRecipientEncrypt(List<SignalProtocolAddress> list, UnidentifiedSenderMessageContent unidentifiedSenderMessageContent) throws InvalidKeyException, InvalidRegistrationIdException, NoSessionException, UntrustedIdentityException {
        List<SessionRecord> loadExistingSessions = this.signalProtocolStore.loadExistingSessions(list);
        long[] jArr = new long[list.size()];
        int i = 0;
        int i2 = 0;
        for (SignalProtocolAddress signalProtocolAddress : list) {
            jArr[i2] = signalProtocolAddress.unsafeNativeHandleWithoutGuard();
            i2++;
        }
        long[] jArr2 = new long[loadExistingSessions.size()];
        for (SessionRecord sessionRecord : loadExistingSessions) {
            jArr2[i] = sessionRecord.unsafeNativeHandleWithoutGuard();
            i++;
        }
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(unidentifiedSenderMessageContent);
        try {
            byte[] SealedSessionCipher_MultiRecipientEncrypt = Native.SealedSessionCipher_MultiRecipientEncrypt(jArr, jArr2, nativeHandleGuard.nativeHandle(), this.signalProtocolStore, null);
            Native.keepAlive(list);
            Native.keepAlive(loadExistingSessions);
            nativeHandleGuard.close();
            return SealedSessionCipher_MultiRecipientEncrypt;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public DecryptionResult decrypt(CertificateValidator certificateValidator, byte[] bArr, long j) throws InvalidMetadataMessageException, InvalidMetadataVersionException, ProtocolInvalidMessageException, ProtocolInvalidKeyException, ProtocolNoSessionException, ProtocolLegacyMessageException, ProtocolInvalidVersionException, ProtocolDuplicateMessageException, ProtocolInvalidKeyIdException, ProtocolUntrustedIdentityException, SelfSendException {
        try {
            UnidentifiedSenderMessageContent unidentifiedSenderMessageContent = new UnidentifiedSenderMessageContent(Native.SealedSessionCipher_DecryptToUsmc(bArr, this.signalProtocolStore, null));
            certificateValidator.validate(unidentifiedSenderMessageContent.getSenderCertificate(), j);
            String str = this.localE164Address;
            boolean z = str != null && str.equals(unidentifiedSenderMessageContent.getSenderCertificate().getSenderE164().orElse(null));
            boolean equals = this.localUuidAddress.equals(unidentifiedSenderMessageContent.getSenderCertificate().getSenderUuid());
            if ((z || equals) && unidentifiedSenderMessageContent.getSenderCertificate().getSenderDeviceId() == this.localDeviceId) {
                throw new SelfSendException();
            }
            try {
                return new DecryptionResult(unidentifiedSenderMessageContent.getSenderCertificate().getSenderUuid(), unidentifiedSenderMessageContent.getSenderCertificate().getSenderE164(), unidentifiedSenderMessageContent.getSenderCertificate().getSenderDeviceId(), unidentifiedSenderMessageContent.getType(), unidentifiedSenderMessageContent.getGroupId(), decrypt(unidentifiedSenderMessageContent));
            } catch (DuplicateMessageException e) {
                throw new ProtocolDuplicateMessageException(e, unidentifiedSenderMessageContent);
            } catch (InvalidKeyException e2) {
                throw new ProtocolInvalidKeyException(e2, unidentifiedSenderMessageContent);
            } catch (InvalidKeyIdException e3) {
                throw new ProtocolInvalidKeyIdException(e3, unidentifiedSenderMessageContent);
            } catch (InvalidMessageException e4) {
                throw new ProtocolInvalidMessageException(e4, unidentifiedSenderMessageContent);
            } catch (InvalidVersionException e5) {
                throw new ProtocolInvalidVersionException(e5, unidentifiedSenderMessageContent);
            } catch (LegacyMessageException e6) {
                throw new ProtocolLegacyMessageException(e6, unidentifiedSenderMessageContent);
            } catch (NoSessionException e7) {
                throw new ProtocolNoSessionException(e7, unidentifiedSenderMessageContent);
            } catch (UntrustedIdentityException e8) {
                throw new ProtocolUntrustedIdentityException(e8, unidentifiedSenderMessageContent);
            }
        } catch (Exception e9) {
            throw new InvalidMetadataMessageException(e9);
        }
    }

    public int getSessionVersion(SignalProtocolAddress signalProtocolAddress) {
        return new SessionCipher(this.signalProtocolStore, signalProtocolAddress).getSessionVersion();
    }

    public int getRemoteRegistrationId(SignalProtocolAddress signalProtocolAddress) {
        return new SessionCipher(this.signalProtocolStore, signalProtocolAddress).getRemoteRegistrationId();
    }

    private byte[] decrypt(UnidentifiedSenderMessageContent unidentifiedSenderMessageContent) throws InvalidVersionException, InvalidMessageException, InvalidKeyException, DuplicateMessageException, InvalidKeyIdException, UntrustedIdentityException, LegacyMessageException, NoSessionException {
        SignalProtocolAddress signalProtocolAddress = new SignalProtocolAddress(unidentifiedSenderMessageContent.getSenderCertificate().getSenderUuid(), unidentifiedSenderMessageContent.getSenderCertificate().getSenderDeviceId());
        int type = unidentifiedSenderMessageContent.getType();
        if (type == 2) {
            return new SessionCipher(this.signalProtocolStore, signalProtocolAddress).decrypt(new SignalMessage(unidentifiedSenderMessageContent.getContent()));
        }
        if (type == 3) {
            return new SessionCipher(this.signalProtocolStore, signalProtocolAddress).decrypt(new PreKeySignalMessage(unidentifiedSenderMessageContent.getContent()));
        }
        if (type == 7) {
            return new GroupCipher(this.signalProtocolStore, signalProtocolAddress).decrypt(unidentifiedSenderMessageContent.getContent());
        }
        if (type == 8) {
            return Native.PlaintextContent_DeserializeAndGetContent(unidentifiedSenderMessageContent.getContent());
        }
        throw new InvalidMessageException("Unknown type: " + unidentifiedSenderMessageContent.getType());
    }

    /* loaded from: classes3.dex */
    public static class DecryptionResult {
        private final int deviceId;
        private final Optional<byte[]> groupId;
        private final int messageType;
        private final byte[] paddedMessage;
        private final Optional<String> senderE164;
        private final String senderUuid;

        private DecryptionResult(String str, Optional<String> optional, int i, int i2, Optional<byte[]> optional2, byte[] bArr) {
            this.senderUuid = str;
            this.senderE164 = optional;
            this.deviceId = i;
            this.messageType = i2;
            this.groupId = optional2;
            this.paddedMessage = bArr;
        }

        public String getSenderUuid() {
            return this.senderUuid;
        }

        public Optional<String> getSenderE164() {
            return this.senderE164;
        }

        public int getDeviceId() {
            return this.deviceId;
        }

        public int getCiphertextMessageType() {
            return this.messageType;
        }

        public byte[] getPaddedMessage() {
            return this.paddedMessage;
        }

        public Optional<byte[]> getGroupId() {
            return this.groupId;
        }
    }
}
