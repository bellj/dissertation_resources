package org.signal.libsignal.metadata;

import j$.util.Optional;
import org.signal.libsignal.metadata.protocol.UnidentifiedSenderMessageContent;

/* loaded from: classes3.dex */
public abstract class ProtocolException extends Exception {
    private final Optional<UnidentifiedSenderMessageContent> content;
    private final String sender;
    private final int senderDevice;

    public ProtocolException(Exception exc, String str, int i) {
        super(exc);
        this.content = Optional.empty();
        this.sender = str;
        this.senderDevice = i;
    }

    public ProtocolException(Exception exc, UnidentifiedSenderMessageContent unidentifiedSenderMessageContent) {
        super(exc);
        this.content = Optional.of(unidentifiedSenderMessageContent);
        this.sender = unidentifiedSenderMessageContent.getSenderCertificate().getSender();
        this.senderDevice = unidentifiedSenderMessageContent.getSenderCertificate().getSenderDeviceId();
    }

    public Optional<UnidentifiedSenderMessageContent> getUnidentifiedSenderMessageContent() {
        return this.content;
    }

    public String getSender() {
        return this.sender;
    }

    public int getSenderDevice() {
        return this.senderDevice;
    }

    public int getContentHint() {
        if (this.content.isPresent()) {
            return this.content.get().getContentHint();
        }
        return 0;
    }

    public Optional<byte[]> getGroupId() {
        if (this.content.isPresent()) {
            return this.content.get().getGroupId();
        }
        return Optional.empty();
    }
}
