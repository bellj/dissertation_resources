package org.signal.libsignal.metadata;

import org.signal.libsignal.metadata.protocol.UnidentifiedSenderMessageContent;
import org.signal.libsignal.protocol.InvalidKeyException;

/* loaded from: classes3.dex */
public class ProtocolInvalidKeyException extends ProtocolException {
    public ProtocolInvalidKeyException(InvalidKeyException invalidKeyException, String str, int i) {
        super(invalidKeyException, str, i);
    }

    public ProtocolInvalidKeyException(InvalidKeyException invalidKeyException, UnidentifiedSenderMessageContent unidentifiedSenderMessageContent) {
        super(invalidKeyException, unidentifiedSenderMessageContent);
    }
}
