package org.signal.libsignal.metadata;

import org.signal.libsignal.metadata.protocol.UnidentifiedSenderMessageContent;

/* loaded from: classes3.dex */
public class ProtocolDuplicateMessageException extends ProtocolException {
    public ProtocolDuplicateMessageException(Exception exc, String str, int i) {
        super(exc, str, i);
    }

    public ProtocolDuplicateMessageException(Exception exc, UnidentifiedSenderMessageContent unidentifiedSenderMessageContent) {
        super(exc, unidentifiedSenderMessageContent);
    }
}
