package org.signal.libsignal.metadata;

import org.signal.libsignal.metadata.protocol.UnidentifiedSenderMessageContent;
import org.signal.libsignal.protocol.UntrustedIdentityException;

/* loaded from: classes3.dex */
public class ProtocolUntrustedIdentityException extends ProtocolException {
    public ProtocolUntrustedIdentityException(UntrustedIdentityException untrustedIdentityException, String str, int i) {
        super(untrustedIdentityException, str, i);
    }

    public ProtocolUntrustedIdentityException(UntrustedIdentityException untrustedIdentityException, UnidentifiedSenderMessageContent unidentifiedSenderMessageContent) {
        super(untrustedIdentityException, unidentifiedSenderMessageContent);
    }
}
