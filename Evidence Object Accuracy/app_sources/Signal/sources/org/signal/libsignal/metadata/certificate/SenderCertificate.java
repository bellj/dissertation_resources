package org.signal.libsignal.metadata.certificate;

import j$.util.Optional;
import j$.util.function.Supplier;
import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;

/* loaded from: classes3.dex */
public class SenderCertificate implements NativeHandleGuard.Owner {
    private final long unsafeHandle;

    protected void finalize() {
        Native.SenderCertificate_Destroy(this.unsafeHandle);
    }

    @Override // org.signal.libsignal.internal.NativeHandleGuard.Owner
    public long unsafeNativeHandleWithoutGuard() {
        return this.unsafeHandle;
    }

    public SenderCertificate(byte[] bArr) throws InvalidCertificateException {
        try {
            this.unsafeHandle = Native.SenderCertificate_Deserialize(bArr);
        } catch (Exception e) {
            throw new InvalidCertificateException(e);
        }
    }

    public SenderCertificate(long j) {
        this.unsafeHandle = j;
    }

    public int getSenderDeviceId() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int SenderCertificate_GetDeviceId = Native.SenderCertificate_GetDeviceId(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SenderCertificate_GetDeviceId;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public String getSenderUuid() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            String SenderCertificate_GetSenderUuid = Native.SenderCertificate_GetSenderUuid(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SenderCertificate_GetSenderUuid;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public Optional<String> getSenderE164() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            Optional<String> ofNullable = Optional.ofNullable(Native.SenderCertificate_GetSenderE164(nativeHandleGuard.nativeHandle()));
            nativeHandleGuard.close();
            return ofNullable;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public String getSender() {
        return getSenderE164().orElseGet(new Supplier() { // from class: org.signal.libsignal.metadata.certificate.SenderCertificate$$ExternalSyntheticLambda0
            @Override // j$.util.function.Supplier
            public final Object get() {
                return SenderCertificate.this.getSenderUuid();
            }
        });
    }

    public long getExpiration() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            long SenderCertificate_GetExpiration = Native.SenderCertificate_GetExpiration(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return SenderCertificate_GetExpiration;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }
}
