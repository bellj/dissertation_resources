package org.signal.libsignal.metadata;

import org.signal.libsignal.metadata.protocol.UnidentifiedSenderMessageContent;

/* loaded from: classes3.dex */
public class ProtocolInvalidKeyIdException extends ProtocolException {
    public ProtocolInvalidKeyIdException(Exception exc, String str, int i) {
        super(exc, str, i);
    }

    public ProtocolInvalidKeyIdException(Exception exc, UnidentifiedSenderMessageContent unidentifiedSenderMessageContent) {
        super(exc, unidentifiedSenderMessageContent);
    }
}
