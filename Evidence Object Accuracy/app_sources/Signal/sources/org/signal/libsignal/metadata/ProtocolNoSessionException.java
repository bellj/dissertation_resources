package org.signal.libsignal.metadata;

import org.signal.libsignal.metadata.protocol.UnidentifiedSenderMessageContent;
import org.signal.libsignal.protocol.NoSessionException;

/* loaded from: classes3.dex */
public class ProtocolNoSessionException extends ProtocolException {
    public ProtocolNoSessionException(NoSessionException noSessionException, String str, int i) {
        super(noSessionException, str, i);
    }

    public ProtocolNoSessionException(NoSessionException noSessionException, UnidentifiedSenderMessageContent unidentifiedSenderMessageContent) {
        super(noSessionException, unidentifiedSenderMessageContent);
    }
}
