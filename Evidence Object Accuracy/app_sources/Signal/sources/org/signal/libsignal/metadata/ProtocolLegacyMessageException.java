package org.signal.libsignal.metadata;

import org.signal.libsignal.metadata.protocol.UnidentifiedSenderMessageContent;
import org.signal.libsignal.protocol.LegacyMessageException;

/* loaded from: classes3.dex */
public class ProtocolLegacyMessageException extends ProtocolException {
    public ProtocolLegacyMessageException(LegacyMessageException legacyMessageException, String str, int i) {
        super(legacyMessageException, str, i);
    }

    public ProtocolLegacyMessageException(LegacyMessageException legacyMessageException, UnidentifiedSenderMessageContent unidentifiedSenderMessageContent) {
        super(legacyMessageException, unidentifiedSenderMessageContent);
    }
}
