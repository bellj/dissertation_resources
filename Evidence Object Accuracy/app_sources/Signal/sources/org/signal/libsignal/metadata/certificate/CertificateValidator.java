package org.signal.libsignal.metadata.certificate;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;
import org.signal.libsignal.protocol.ecc.ECPublicKey;

/* loaded from: classes3.dex */
public class CertificateValidator {
    private final ECPublicKey trustRoot;

    public CertificateValidator(ECPublicKey eCPublicKey) {
        this.trustRoot = eCPublicKey;
    }

    public void validate(SenderCertificate senderCertificate, long j) throws InvalidCertificateException {
        try {
            NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(senderCertificate);
            NativeHandleGuard nativeHandleGuard2 = new NativeHandleGuard(this.trustRoot);
            try {
                if (Native.SenderCertificate_Validate(nativeHandleGuard.nativeHandle(), nativeHandleGuard2.nativeHandle(), j)) {
                    nativeHandleGuard2.close();
                    nativeHandleGuard.close();
                    return;
                }
                throw new InvalidCertificateException("Validation failed");
            } catch (Throwable th) {
                try {
                    nativeHandleGuard2.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        } catch (Exception e) {
            throw new InvalidCertificateException(e);
        }
    }
}
