package org.signal.libsignal.metadata;

import org.signal.libsignal.metadata.protocol.UnidentifiedSenderMessageContent;
import org.signal.libsignal.protocol.InvalidVersionException;

/* loaded from: classes3.dex */
public class ProtocolInvalidVersionException extends ProtocolException {
    public ProtocolInvalidVersionException(InvalidVersionException invalidVersionException, String str, int i) {
        super(invalidVersionException, str, i);
    }

    public ProtocolInvalidVersionException(InvalidVersionException invalidVersionException, UnidentifiedSenderMessageContent unidentifiedSenderMessageContent) {
        super(invalidVersionException, unidentifiedSenderMessageContent);
    }
}
