package org.signal.libsignal.metadata.protocol;

import j$.util.Optional;
import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;
import org.signal.libsignal.metadata.certificate.SenderCertificate;
import org.signal.libsignal.protocol.message.CiphertextMessage;

/* loaded from: classes3.dex */
public class UnidentifiedSenderMessageContent implements NativeHandleGuard.Owner {
    private final long unsafeHandle;

    protected void finalize() {
        Native.UnidentifiedSenderMessageContent_Destroy(this.unsafeHandle);
    }

    public UnidentifiedSenderMessageContent(long j) {
        this.unsafeHandle = j;
    }

    @Override // org.signal.libsignal.internal.NativeHandleGuard.Owner
    public long unsafeNativeHandleWithoutGuard() {
        return this.unsafeHandle;
    }

    public UnidentifiedSenderMessageContent(CiphertextMessage ciphertextMessage, SenderCertificate senderCertificate, int i, Optional<byte[]> optional) {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(senderCertificate);
        try {
            this.unsafeHandle = Native.UnidentifiedSenderMessageContent_New(ciphertextMessage, nativeHandleGuard.nativeHandle(), i, optional.orElse(null));
            nativeHandleGuard.close();
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public int getType() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int UnidentifiedSenderMessageContent_GetMsgType = Native.UnidentifiedSenderMessageContent_GetMsgType(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return UnidentifiedSenderMessageContent_GetMsgType;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public SenderCertificate getSenderCertificate() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            SenderCertificate senderCertificate = new SenderCertificate(Native.UnidentifiedSenderMessageContent_GetSenderCert(nativeHandleGuard.nativeHandle()));
            nativeHandleGuard.close();
            return senderCertificate;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] getContent() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] UnidentifiedSenderMessageContent_GetContents = Native.UnidentifiedSenderMessageContent_GetContents(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return UnidentifiedSenderMessageContent_GetContents;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public int getContentHint() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            int UnidentifiedSenderMessageContent_GetContentHint = Native.UnidentifiedSenderMessageContent_GetContentHint(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return UnidentifiedSenderMessageContent_GetContentHint;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public Optional<byte[]> getGroupId() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            Optional<byte[]> ofNullable = Optional.ofNullable(Native.UnidentifiedSenderMessageContent_GetGroupId(nativeHandleGuard.nativeHandle()));
            nativeHandleGuard.close();
            return ofNullable;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }
}
