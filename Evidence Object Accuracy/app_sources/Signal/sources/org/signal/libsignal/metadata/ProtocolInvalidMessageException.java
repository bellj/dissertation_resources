package org.signal.libsignal.metadata;

import org.signal.libsignal.metadata.protocol.UnidentifiedSenderMessageContent;
import org.signal.libsignal.protocol.InvalidMessageException;

/* loaded from: classes3.dex */
public class ProtocolInvalidMessageException extends ProtocolException {
    public ProtocolInvalidMessageException(InvalidMessageException invalidMessageException, String str, int i) {
        super(invalidMessageException, str, i);
    }

    public ProtocolInvalidMessageException(InvalidMessageException invalidMessageException, UnidentifiedSenderMessageContent unidentifiedSenderMessageContent) {
        super(invalidMessageException, unidentifiedSenderMessageContent);
    }
}
