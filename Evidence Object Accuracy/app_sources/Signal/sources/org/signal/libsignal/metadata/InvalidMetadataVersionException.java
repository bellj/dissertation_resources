package org.signal.libsignal.metadata;

/* loaded from: classes3.dex */
public class InvalidMetadataVersionException extends Exception {
    public InvalidMetadataVersionException(String str) {
        super(str);
    }
}
