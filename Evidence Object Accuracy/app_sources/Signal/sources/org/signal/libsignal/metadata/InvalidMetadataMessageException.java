package org.signal.libsignal.metadata;

/* loaded from: classes3.dex */
public class InvalidMetadataMessageException extends Exception {
    public InvalidMetadataMessageException(String str) {
        super(str);
    }

    public InvalidMetadataMessageException(Exception exc) {
        super(exc);
    }
}
