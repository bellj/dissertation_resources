package org.signal.libsignal.metadata.certificate;

/* loaded from: classes3.dex */
public class InvalidCertificateException extends Exception {
    public InvalidCertificateException(String str) {
        super(str);
    }

    public InvalidCertificateException(Exception exc) {
        super(exc);
    }
}
