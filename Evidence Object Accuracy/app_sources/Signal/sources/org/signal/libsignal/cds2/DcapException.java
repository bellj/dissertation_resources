package org.signal.libsignal.cds2;

/* loaded from: classes3.dex */
public class DcapException extends Exception {
    public DcapException(String str) {
        super(str);
    }

    public DcapException(Throwable th) {
        super(th);
    }
}
