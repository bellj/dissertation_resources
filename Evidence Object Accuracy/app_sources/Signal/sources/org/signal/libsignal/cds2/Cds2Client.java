package org.signal.libsignal.cds2;

import j$.time.Instant;
import org.signal.libsignal.internal.Native;
import org.signal.libsignal.internal.NativeHandleGuard;

/* loaded from: classes3.dex */
public class Cds2Client implements NativeHandleGuard.Owner {
    private final long unsafeHandle;

    public static Cds2Client create_NOT_FOR_PRODUCTION(byte[] bArr, byte[] bArr2, byte[] bArr3, Instant instant) throws AttestationDataException {
        return new Cds2Client(bArr, bArr2, bArr3, instant);
    }

    private Cds2Client(byte[] bArr, byte[] bArr2, byte[] bArr3, Instant instant) throws AttestationDataException {
        this.unsafeHandle = Native.Cds2ClientState_New(bArr, bArr2, bArr3, instant.toEpochMilli());
    }

    protected void finalize() {
        Native.Cds2ClientState_Destroy(this.unsafeHandle);
    }

    @Override // org.signal.libsignal.internal.NativeHandleGuard.Owner
    public long unsafeNativeHandleWithoutGuard() {
        return this.unsafeHandle;
    }

    public byte[] initialRequest() {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] Cds2ClientState_InitialRequest = Native.Cds2ClientState_InitialRequest(nativeHandleGuard.nativeHandle());
            nativeHandleGuard.close();
            return Cds2ClientState_InitialRequest;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public void completeHandshake(byte[] bArr) throws Cds2CommunicationFailureException {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            Native.Cds2ClientState_CompleteHandshake(nativeHandleGuard.nativeHandle(), bArr);
            nativeHandleGuard.close();
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] establishedSend(byte[] bArr) throws Cds2CommunicationFailureException {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] Cds2ClientState_EstablishedSend = Native.Cds2ClientState_EstablishedSend(nativeHandleGuard.nativeHandle(), bArr);
            nativeHandleGuard.close();
            return Cds2ClientState_EstablishedSend;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public byte[] establishedRecv(byte[] bArr) throws Cds2CommunicationFailureException {
        NativeHandleGuard nativeHandleGuard = new NativeHandleGuard(this);
        try {
            byte[] Cds2ClientState_EstablishedRecv = Native.Cds2ClientState_EstablishedRecv(nativeHandleGuard.nativeHandle(), bArr);
            nativeHandleGuard.close();
            return Cds2ClientState_EstablishedRecv;
        } catch (Throwable th) {
            try {
                nativeHandleGuard.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }
}
