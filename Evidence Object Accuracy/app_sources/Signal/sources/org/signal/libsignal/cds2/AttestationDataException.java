package org.signal.libsignal.cds2;

/* loaded from: classes3.dex */
public class AttestationDataException extends Exception {
    public AttestationDataException(String str) {
        super(str);
    }

    public AttestationDataException(Throwable th) {
        super(th);
    }
}
