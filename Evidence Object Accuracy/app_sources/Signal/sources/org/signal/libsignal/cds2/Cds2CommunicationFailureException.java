package org.signal.libsignal.cds2;

/* loaded from: classes3.dex */
public class Cds2CommunicationFailureException extends Exception {
    public Cds2CommunicationFailureException(String str) {
        super(str);
    }

    public Cds2CommunicationFailureException(Throwable th) {
        super(th);
    }
}
