package org.signal.libsignal.zkgroup.profiles;

import j$.time.Instant;
import org.signal.libsignal.internal.Native;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.internal.ByteArray;

/* loaded from: classes3.dex */
public final class ExpiringProfileKeyCredential extends ByteArray {
    public ExpiringProfileKeyCredential(byte[] bArr) throws InvalidInputException {
        super(bArr);
        Native.ExpiringProfileKeyCredential_CheckValidContents(bArr);
    }

    public Instant getExpirationTime() {
        return Instant.ofEpochSecond(Native.ExpiringProfileKeyCredential_GetExpirationTime(this.contents));
    }
}
