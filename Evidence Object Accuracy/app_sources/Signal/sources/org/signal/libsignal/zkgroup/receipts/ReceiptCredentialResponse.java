package org.signal.libsignal.zkgroup.receipts;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.internal.ByteArray;

/* loaded from: classes3.dex */
public final class ReceiptCredentialResponse extends ByteArray {
    public ReceiptCredentialResponse(byte[] bArr) throws InvalidInputException {
        super(bArr);
        Native.ReceiptCredentialResponse_CheckValidContents(bArr);
    }
}
