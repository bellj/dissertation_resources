package org.signal.libsignal.zkgroup.profiles;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.internal.ByteArray;

/* loaded from: classes3.dex */
public final class ProfileKeyCredential extends ByteArray {
    public ProfileKeyCredential(byte[] bArr) throws InvalidInputException {
        super(bArr);
        Native.ProfileKeyCredential_CheckValidContents(bArr);
    }
}
