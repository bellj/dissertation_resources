package org.signal.libsignal.zkgroup.receipts;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.internal.ByteArray;

/* loaded from: classes3.dex */
public final class ReceiptCredentialPresentation extends ByteArray {
    public ReceiptCredentialPresentation(byte[] bArr) throws InvalidInputException {
        super(bArr);
        Native.ReceiptCredentialPresentation_CheckValidContents(bArr);
    }

    public long getReceiptExpirationTime() {
        return Native.ReceiptCredentialPresentation_GetReceiptExpirationTime(this.contents);
    }

    public long getReceiptLevel() {
        return Native.ReceiptCredentialPresentation_GetReceiptLevel(this.contents);
    }
}
