package org.signal.libsignal.zkgroup;

import org.signal.libsignal.zkgroup.internal.ByteArray;

/* loaded from: classes3.dex */
public final class NotarySignature extends ByteArray {
    public NotarySignature(byte[] bArr) throws InvalidInputException {
        super(bArr, 64);
    }
}
