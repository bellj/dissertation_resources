package org.signal.libsignal.zkgroup.receipts;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.internal.ByteArray;

/* loaded from: classes3.dex */
public final class ReceiptCredentialRequestContext extends ByteArray {
    public ReceiptCredentialRequestContext(byte[] bArr) throws InvalidInputException {
        super(bArr, 177);
        Native.ReceiptCredentialRequestContext_CheckValidContents(bArr);
    }

    public ReceiptCredentialRequest getRequest() {
        try {
            return new ReceiptCredentialRequest(Native.ReceiptCredentialRequestContext_GetRequest(this.contents));
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }

    @Override // org.signal.libsignal.zkgroup.internal.ByteArray
    public byte[] serialize() {
        return (byte[]) this.contents.clone();
    }
}
