package org.signal.libsignal.zkgroup.profiles;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.internal.ByteArray;

/* loaded from: classes3.dex */
public final class ProfileKeyCredentialRequestContext extends ByteArray {
    public ProfileKeyCredentialRequestContext(byte[] bArr) throws InvalidInputException {
        super(bArr);
        Native.ProfileKeyCredentialRequestContext_CheckValidContents(bArr);
    }

    public ProfileKeyCredentialRequest getRequest() {
        try {
            return new ProfileKeyCredentialRequest(Native.ProfileKeyCredentialRequestContext_GetRequest(this.contents));
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }
}
