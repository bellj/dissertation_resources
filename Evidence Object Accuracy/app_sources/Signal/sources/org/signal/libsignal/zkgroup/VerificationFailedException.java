package org.signal.libsignal.zkgroup;

/* loaded from: classes3.dex */
public class VerificationFailedException extends Exception {
    public VerificationFailedException() {
    }

    public VerificationFailedException(String str) {
        super(str);
    }
}
