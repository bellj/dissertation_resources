package org.signal.libsignal.zkgroup.groups;

import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.internal.ByteArray;

/* loaded from: classes3.dex */
public final class GroupMasterKey extends ByteArray {
    public GroupMasterKey(byte[] bArr) throws InvalidInputException {
        super(bArr, 32);
    }
}
