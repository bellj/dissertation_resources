package org.signal.libsignal.zkgroup.internal;

import java.util.Arrays;
import java.util.Locale;
import org.signal.libsignal.zkgroup.InvalidInputException;

/* loaded from: classes.dex */
public abstract class ByteArray {
    protected final byte[] contents;

    public ByteArray(byte[] bArr) {
        this.contents = (byte[]) bArr.clone();
    }

    public ByteArray(byte[] bArr, int i) throws InvalidInputException {
        this.contents = cloneArrayOfLength(bArr, i);
    }

    private static byte[] cloneArrayOfLength(byte[] bArr, int i) throws InvalidInputException {
        if (bArr.length == i) {
            return (byte[]) bArr.clone();
        }
        throw new InvalidInputException(String.format(Locale.US, "Length of array supplied was %d expected %d", Integer.valueOf(bArr.length), Integer.valueOf(i)));
    }

    public byte[] getInternalContentsForJNI() {
        return this.contents;
    }

    public byte[] serialize() {
        return (byte[]) this.contents.clone();
    }

    public int hashCode() {
        return (getClass().hashCode() * 31) + Arrays.hashCode(this.contents);
    }

    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ByteArray byteArray = (ByteArray) obj;
        if (this.contents == byteArray.getInternalContentsForJNI()) {
            return true;
        }
        if (this.contents.length != byteArray.getInternalContentsForJNI().length) {
            return false;
        }
        int i = 0;
        int i2 = 0;
        while (true) {
            byte[] bArr = this.contents;
            if (i >= bArr.length) {
                break;
            }
            i2 |= bArr[i] ^ byteArray.getInternalContentsForJNI()[i];
            i++;
        }
        if (i2 == 0) {
            return true;
        }
        return false;
    }
}
