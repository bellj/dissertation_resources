package org.signal.libsignal.zkgroup.auth;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.internal.ByteArray;

/* loaded from: classes3.dex */
public final class AuthCredentialPresentation extends ByteArray {
    public AuthCredentialPresentation(byte[] bArr) throws InvalidInputException {
        super(bArr);
        Native.AuthCredentialPresentation_CheckValidContents(bArr);
    }
}
