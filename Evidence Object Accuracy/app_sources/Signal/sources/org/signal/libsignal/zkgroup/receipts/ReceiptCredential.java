package org.signal.libsignal.zkgroup.receipts;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.internal.ByteArray;

/* loaded from: classes3.dex */
public final class ReceiptCredential extends ByteArray {
    public ReceiptCredential(byte[] bArr) throws InvalidInputException {
        super(bArr);
        Native.ReceiptCredential_CheckValidContents(bArr);
    }

    public long getReceiptExpirationTime() {
        return Native.ReceiptCredential_GetReceiptExpirationTime(this.contents);
    }

    public long getReceiptLevel() {
        return Native.ReceiptCredential_GetReceiptLevel(this.contents);
    }
}
