package org.signal.libsignal.zkgroup.receipts;

import java.security.SecureRandom;
import org.signal.libsignal.internal.Native;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.ServerPublicParams;
import org.signal.libsignal.zkgroup.VerificationFailedException;

/* loaded from: classes3.dex */
public class ClientZkReceiptOperations {
    private final ServerPublicParams serverPublicParams;

    public ClientZkReceiptOperations(ServerPublicParams serverPublicParams) {
        this.serverPublicParams = serverPublicParams;
    }

    public ReceiptCredentialRequestContext createReceiptCredentialRequestContext(SecureRandom secureRandom, ReceiptSerial receiptSerial) throws VerificationFailedException {
        byte[] bArr = new byte[32];
        secureRandom.nextBytes(bArr);
        try {
            return new ReceiptCredentialRequestContext(Native.ServerPublicParams_CreateReceiptCredentialRequestContextDeterministic(this.serverPublicParams.getInternalContentsForJNI(), bArr, receiptSerial.getInternalContentsForJNI()));
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }

    public ReceiptCredential receiveReceiptCredential(ReceiptCredentialRequestContext receiptCredentialRequestContext, ReceiptCredentialResponse receiptCredentialResponse) throws VerificationFailedException {
        try {
            return new ReceiptCredential(Native.ServerPublicParams_ReceiveReceiptCredential(this.serverPublicParams.getInternalContentsForJNI(), receiptCredentialRequestContext.getInternalContentsForJNI(), receiptCredentialResponse.getInternalContentsForJNI()));
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }

    public ReceiptCredentialPresentation createReceiptCredentialPresentation(ReceiptCredential receiptCredential) throws VerificationFailedException {
        return createReceiptCredentialPresentation(new SecureRandom(), receiptCredential);
    }

    public ReceiptCredentialPresentation createReceiptCredentialPresentation(SecureRandom secureRandom, ReceiptCredential receiptCredential) throws VerificationFailedException {
        byte[] bArr = new byte[32];
        secureRandom.nextBytes(bArr);
        try {
            return new ReceiptCredentialPresentation(Native.ServerPublicParams_CreateReceiptCredentialPresentationDeterministic(this.serverPublicParams.getInternalContentsForJNI(), bArr, receiptCredential.getInternalContentsForJNI()));
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }
}
