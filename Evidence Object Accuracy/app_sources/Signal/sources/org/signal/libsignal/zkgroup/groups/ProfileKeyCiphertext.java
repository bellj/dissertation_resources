package org.signal.libsignal.zkgroup.groups;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.internal.ByteArray;

/* loaded from: classes3.dex */
public final class ProfileKeyCiphertext extends ByteArray {
    public ProfileKeyCiphertext(byte[] bArr) throws InvalidInputException {
        super(bArr);
        Native.ProfileKeyCiphertext_CheckValidContents(bArr);
    }
}
