package org.signal.libsignal.zkgroup.groups;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.internal.ByteArray;

/* loaded from: classes3.dex */
public final class GroupPublicParams extends ByteArray {
    public GroupPublicParams(byte[] bArr) throws InvalidInputException {
        super(bArr);
        Native.GroupPublicParams_CheckValidContents(bArr);
    }

    public GroupIdentifier getGroupIdentifier() {
        try {
            return new GroupIdentifier(Native.GroupPublicParams_GetGroupIdentifier(this.contents));
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }
}
