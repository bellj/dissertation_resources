package org.signal.libsignal.zkgroup;

/* loaded from: classes3.dex */
public class InvalidInputException extends Exception {
    public InvalidInputException() {
    }

    public InvalidInputException(String str) {
        super(str);
    }
}
