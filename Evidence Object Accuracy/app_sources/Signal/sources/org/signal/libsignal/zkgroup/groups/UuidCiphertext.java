package org.signal.libsignal.zkgroup.groups;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.internal.ByteArray;

/* loaded from: classes3.dex */
public final class UuidCiphertext extends ByteArray {
    public UuidCiphertext(byte[] bArr) throws InvalidInputException {
        super(bArr);
        Native.UuidCiphertext_CheckValidContents(bArr);
    }
}
