package org.signal.libsignal.zkgroup.auth;

import java.security.SecureRandom;
import java.util.UUID;
import org.signal.libsignal.internal.Native;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.ServerPublicParams;
import org.signal.libsignal.zkgroup.VerificationFailedException;
import org.signal.libsignal.zkgroup.groups.GroupSecretParams;

/* loaded from: classes3.dex */
public class ClientZkAuthOperations {
    private final ServerPublicParams serverPublicParams;

    public ClientZkAuthOperations(ServerPublicParams serverPublicParams) {
        this.serverPublicParams = serverPublicParams;
    }

    public AuthCredentialWithPni receiveAuthCredentialWithPni(UUID uuid, UUID uuid2, long j, AuthCredentialWithPniResponse authCredentialWithPniResponse) throws VerificationFailedException {
        try {
            return new AuthCredentialWithPni(Native.ServerPublicParams_ReceiveAuthCredentialWithPni(this.serverPublicParams.getInternalContentsForJNI(), uuid, uuid2, j, authCredentialWithPniResponse.getInternalContentsForJNI()));
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }

    public AuthCredentialPresentation createAuthCredentialPresentation(SecureRandom secureRandom, GroupSecretParams groupSecretParams, AuthCredentialWithPni authCredentialWithPni) {
        byte[] bArr = new byte[32];
        secureRandom.nextBytes(bArr);
        try {
            return new AuthCredentialPresentation(Native.ServerPublicParams_CreateAuthCredentialWithPniPresentationDeterministic(this.serverPublicParams.getInternalContentsForJNI(), bArr, groupSecretParams.getInternalContentsForJNI(), authCredentialWithPni.getInternalContentsForJNI()));
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }
}
