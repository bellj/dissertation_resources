package org.signal.libsignal.zkgroup.profiles;

import j$.time.Instant;
import java.security.SecureRandom;
import java.util.UUID;
import org.signal.libsignal.internal.Native;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.ServerPublicParams;
import org.signal.libsignal.zkgroup.VerificationFailedException;
import org.signal.libsignal.zkgroup.groups.GroupSecretParams;

/* loaded from: classes3.dex */
public class ClientZkProfileOperations {
    private final ServerPublicParams serverPublicParams;

    public ClientZkProfileOperations(ServerPublicParams serverPublicParams) {
        this.serverPublicParams = serverPublicParams;
    }

    public ProfileKeyCredentialRequestContext createProfileKeyCredentialRequestContext(SecureRandom secureRandom, UUID uuid, ProfileKey profileKey) {
        byte[] bArr = new byte[32];
        secureRandom.nextBytes(bArr);
        try {
            return new ProfileKeyCredentialRequestContext(Native.ServerPublicParams_CreateProfileKeyCredentialRequestContextDeterministic(this.serverPublicParams.getInternalContentsForJNI(), bArr, uuid, profileKey.getInternalContentsForJNI()));
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }

    public ProfileKeyCredential receiveProfileKeyCredential(ProfileKeyCredentialRequestContext profileKeyCredentialRequestContext, ProfileKeyCredentialResponse profileKeyCredentialResponse) throws VerificationFailedException {
        if (profileKeyCredentialResponse != null) {
            try {
                return new ProfileKeyCredential(Native.ServerPublicParams_ReceiveProfileKeyCredential(this.serverPublicParams.getInternalContentsForJNI(), profileKeyCredentialRequestContext.getInternalContentsForJNI(), profileKeyCredentialResponse.getInternalContentsForJNI()));
            } catch (InvalidInputException e) {
                throw new AssertionError(e);
            }
        } else {
            throw new VerificationFailedException();
        }
    }

    public ExpiringProfileKeyCredential receiveExpiringProfileKeyCredential(ProfileKeyCredentialRequestContext profileKeyCredentialRequestContext, ExpiringProfileKeyCredentialResponse expiringProfileKeyCredentialResponse) throws VerificationFailedException {
        return receiveExpiringProfileKeyCredential(profileKeyCredentialRequestContext, expiringProfileKeyCredentialResponse, Instant.now());
    }

    public ExpiringProfileKeyCredential receiveExpiringProfileKeyCredential(ProfileKeyCredentialRequestContext profileKeyCredentialRequestContext, ExpiringProfileKeyCredentialResponse expiringProfileKeyCredentialResponse, Instant instant) throws VerificationFailedException {
        if (expiringProfileKeyCredentialResponse != null) {
            try {
                return new ExpiringProfileKeyCredential(Native.ServerPublicParams_ReceiveExpiringProfileKeyCredential(this.serverPublicParams.getInternalContentsForJNI(), profileKeyCredentialRequestContext.getInternalContentsForJNI(), expiringProfileKeyCredentialResponse.getInternalContentsForJNI(), instant.getEpochSecond()));
            } catch (InvalidInputException e) {
                throw new AssertionError(e);
            }
        } else {
            throw new VerificationFailedException();
        }
    }

    public ProfileKeyCredentialPresentation createProfileKeyCredentialPresentation(SecureRandom secureRandom, GroupSecretParams groupSecretParams, ExpiringProfileKeyCredential expiringProfileKeyCredential) {
        byte[] bArr = new byte[32];
        secureRandom.nextBytes(bArr);
        try {
            return new ProfileKeyCredentialPresentation(Native.ServerPublicParams_CreateExpiringProfileKeyCredentialPresentationDeterministic(this.serverPublicParams.getInternalContentsForJNI(), bArr, groupSecretParams.getInternalContentsForJNI(), expiringProfileKeyCredential.getInternalContentsForJNI()));
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }
}
