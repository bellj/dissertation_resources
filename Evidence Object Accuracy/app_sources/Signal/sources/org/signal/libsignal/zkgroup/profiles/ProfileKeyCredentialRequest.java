package org.signal.libsignal.zkgroup.profiles;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.internal.ByteArray;

/* loaded from: classes3.dex */
public final class ProfileKeyCredentialRequest extends ByteArray {
    public ProfileKeyCredentialRequest(byte[] bArr) throws InvalidInputException {
        super(bArr);
        Native.ProfileKeyCredentialRequest_CheckValidContents(bArr);
    }
}
