package org.signal.libsignal.zkgroup.groups;

import java.security.SecureRandom;
import org.signal.libsignal.internal.Native;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.internal.ByteArray;

/* loaded from: classes3.dex */
public final class GroupSecretParams extends ByteArray {
    public static GroupSecretParams generate() {
        return generate(new SecureRandom());
    }

    public static GroupSecretParams generate(SecureRandom secureRandom) {
        byte[] bArr = new byte[32];
        secureRandom.nextBytes(bArr);
        try {
            return new GroupSecretParams(Native.GroupSecretParams_GenerateDeterministic(bArr));
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }

    public static GroupSecretParams deriveFromMasterKey(GroupMasterKey groupMasterKey) {
        try {
            return new GroupSecretParams(Native.GroupSecretParams_DeriveFromMasterKey(groupMasterKey.getInternalContentsForJNI()));
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }

    public GroupSecretParams(byte[] bArr) throws InvalidInputException {
        super(bArr);
        Native.GroupSecretParams_CheckValidContents(bArr);
    }

    public GroupMasterKey getMasterKey() {
        try {
            return new GroupMasterKey(Native.GroupSecretParams_GetMasterKey(this.contents));
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }

    public GroupPublicParams getPublicParams() {
        try {
            return new GroupPublicParams(Native.GroupSecretParams_GetPublicParams(this.contents));
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }

    @Override // org.signal.libsignal.zkgroup.internal.ByteArray
    public byte[] serialize() {
        return (byte[]) this.contents.clone();
    }
}
