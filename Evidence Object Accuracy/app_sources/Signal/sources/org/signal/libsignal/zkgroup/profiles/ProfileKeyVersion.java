package org.signal.libsignal.zkgroup.profiles;

import java.io.UnsupportedEncodingException;
import org.signal.libsignal.zkgroup.InvalidInputException;

/* loaded from: classes3.dex */
public final class ProfileKeyVersion {
    private byte[] contents;

    public ProfileKeyVersion(byte[] bArr) throws InvalidInputException {
        if (bArr.length == 64) {
            this.contents = (byte[]) bArr.clone();
            return;
        }
        throw new InvalidInputException("bad length");
    }

    public String serialize() {
        try {
            return new String(this.contents, "UTF-8");
        } catch (UnsupportedEncodingException unused) {
            throw new AssertionError();
        }
    }
}
