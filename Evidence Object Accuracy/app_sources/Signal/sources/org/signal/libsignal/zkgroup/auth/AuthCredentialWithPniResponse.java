package org.signal.libsignal.zkgroup.auth;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.internal.ByteArray;

/* loaded from: classes3.dex */
public final class AuthCredentialWithPniResponse extends ByteArray {
    public AuthCredentialWithPniResponse(byte[] bArr) throws InvalidInputException {
        super(bArr);
        Native.AuthCredentialWithPniResponse_CheckValidContents(bArr);
    }
}
