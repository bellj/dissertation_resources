package org.signal.libsignal.zkgroup.receipts;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.internal.ByteArray;

/* loaded from: classes3.dex */
public final class ReceiptCredentialRequest extends ByteArray {
    public ReceiptCredentialRequest(byte[] bArr) throws InvalidInputException {
        super(bArr);
        Native.ReceiptCredentialRequest_CheckValidContents(bArr);
    }
}
