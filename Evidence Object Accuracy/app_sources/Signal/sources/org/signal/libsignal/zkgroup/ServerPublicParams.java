package org.signal.libsignal.zkgroup;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.zkgroup.internal.ByteArray;

/* loaded from: classes3.dex */
public final class ServerPublicParams extends ByteArray {
    public ServerPublicParams(byte[] bArr) throws InvalidInputException {
        super(bArr);
        Native.ServerPublicParams_CheckValidContents(bArr);
    }

    public void verifySignature(byte[] bArr, NotarySignature notarySignature) throws VerificationFailedException {
        Native.ServerPublicParams_VerifySignature(this.contents, bArr, notarySignature.getInternalContentsForJNI());
    }
}
