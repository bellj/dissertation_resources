package org.signal.libsignal.zkgroup.profiles;

import org.signal.libsignal.internal.Native;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.groups.ProfileKeyCiphertext;
import org.signal.libsignal.zkgroup.groups.UuidCiphertext;
import org.signal.libsignal.zkgroup.internal.ByteArray;

/* loaded from: classes3.dex */
public final class ProfileKeyCredentialPresentation extends ByteArray {
    public ProfileKeyCredentialPresentation(byte[] bArr) throws InvalidInputException {
        super(bArr);
        Native.ProfileKeyCredentialPresentation_CheckValidContents(bArr);
    }

    public UuidCiphertext getUuidCiphertext() {
        try {
            return new UuidCiphertext(Native.ProfileKeyCredentialPresentation_GetUuidCiphertext(this.contents));
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }

    public ProfileKeyCiphertext getProfileKeyCiphertext() {
        try {
            return new ProfileKeyCiphertext(Native.ProfileKeyCredentialPresentation_GetProfileKeyCiphertext(this.contents));
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }
}
