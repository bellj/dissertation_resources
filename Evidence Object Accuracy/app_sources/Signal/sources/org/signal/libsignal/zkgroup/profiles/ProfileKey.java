package org.signal.libsignal.zkgroup.profiles;

import java.util.UUID;
import org.signal.libsignal.internal.Native;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.internal.ByteArray;

/* loaded from: classes.dex */
public final class ProfileKey extends ByteArray {
    public ProfileKey(byte[] bArr) throws InvalidInputException {
        super(bArr);
        Native.ProfileKey_CheckValidContents(bArr);
    }

    public ProfileKeyCommitment getCommitment(UUID uuid) {
        try {
            return new ProfileKeyCommitment(Native.ProfileKey_GetCommitment(this.contents, uuid));
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }

    public ProfileKeyVersion getProfileKeyVersion(UUID uuid) {
        try {
            return new ProfileKeyVersion(Native.ProfileKey_GetProfileKeyVersion(this.contents, uuid));
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }
}
