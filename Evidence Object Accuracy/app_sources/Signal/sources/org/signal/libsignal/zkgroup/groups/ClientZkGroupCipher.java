package org.signal.libsignal.zkgroup.groups;

import java.security.SecureRandom;
import java.util.UUID;
import org.signal.libsignal.internal.Native;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.VerificationFailedException;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;

/* loaded from: classes3.dex */
public class ClientZkGroupCipher {
    private final GroupSecretParams groupSecretParams;

    public ClientZkGroupCipher(GroupSecretParams groupSecretParams) {
        this.groupSecretParams = groupSecretParams;
    }

    public UuidCiphertext encryptUuid(UUID uuid) {
        try {
            return new UuidCiphertext(Native.GroupSecretParams_EncryptUuid(this.groupSecretParams.getInternalContentsForJNI(), uuid));
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }

    public UUID decryptUuid(UuidCiphertext uuidCiphertext) throws VerificationFailedException {
        return Native.GroupSecretParams_DecryptUuid(this.groupSecretParams.getInternalContentsForJNI(), uuidCiphertext.getInternalContentsForJNI());
    }

    public ProfileKey decryptProfileKey(ProfileKeyCiphertext profileKeyCiphertext, UUID uuid) throws VerificationFailedException {
        try {
            return new ProfileKey(Native.GroupSecretParams_DecryptProfileKey(this.groupSecretParams.getInternalContentsForJNI(), profileKeyCiphertext.getInternalContentsForJNI(), uuid));
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }

    public byte[] encryptBlob(byte[] bArr) throws VerificationFailedException {
        return encryptBlob(new SecureRandom(), bArr);
    }

    public byte[] encryptBlob(SecureRandom secureRandom, byte[] bArr) throws VerificationFailedException {
        byte[] bArr2 = new byte[32];
        secureRandom.nextBytes(bArr2);
        return Native.GroupSecretParams_EncryptBlobWithPaddingDeterministic(this.groupSecretParams.getInternalContentsForJNI(), bArr2, bArr, 0);
    }

    public byte[] decryptBlob(byte[] bArr) throws VerificationFailedException {
        return Native.GroupSecretParams_DecryptBlobWithPadding(this.groupSecretParams.getInternalContentsForJNI(), bArr);
    }
}
