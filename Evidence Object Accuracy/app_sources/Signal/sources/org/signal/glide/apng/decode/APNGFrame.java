package org.signal.glide.apng.decode;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.CRC32;
import org.signal.glide.apng.io.APNGReader;
import org.signal.glide.apng.io.APNGWriter;
import org.signal.glide.common.decode.Frame;

/* loaded from: classes3.dex */
public class APNGFrame extends Frame<APNGReader, APNGWriter> {
    private static ThreadLocal<CRC32> sCRC32 = new ThreadLocal<>();
    private static final byte[] sPNGEndChunk = {0, 0, 0, 0, 73, 69, 78, 68, -82, 66, 96, -126};
    private static final byte[] sPNGSignatures = {-119, 80, 78, 71, 13, 10, 26, 10};
    public final byte blend_op;
    public final byte dispose_op;
    byte[] ihdrData;
    List<Chunk> imageChunks = new ArrayList();
    List<Chunk> prefixChunks = new ArrayList();

    private CRC32 getCRC32() {
        CRC32 crc32 = sCRC32.get();
        if (crc32 != null) {
            return crc32;
        }
        CRC32 crc322 = new CRC32();
        sCRC32.set(crc322);
        return crc322;
    }

    public APNGFrame(APNGReader aPNGReader, FCTLChunk fCTLChunk) {
        super(aPNGReader);
        this.blend_op = fCTLChunk.blend_op;
        this.dispose_op = fCTLChunk.dispose_op;
        int i = fCTLChunk.delay_num * 1000;
        short s = fCTLChunk.delay_den;
        this.frameDuration = i / (s == 0 ? 100 : s);
        this.frameWidth = fCTLChunk.width;
        this.frameHeight = fCTLChunk.height;
        this.frameX = fCTLChunk.x_offset;
        this.frameY = fCTLChunk.y_offset;
    }

    private int encode(APNGWriter aPNGWriter) throws IOException {
        int i;
        int i2 = 33;
        for (Chunk chunk : this.prefixChunks) {
            i2 += chunk.length + 12;
        }
        for (Chunk chunk2 : this.imageChunks) {
            if (chunk2 instanceof IDATChunk) {
                i = chunk2.length + 12;
            } else if (chunk2 instanceof FDATChunk) {
                i = chunk2.length + 8;
            }
            i2 += i;
        }
        int length = i2 + sPNGEndChunk.length;
        aPNGWriter.reset(length);
        aPNGWriter.putBytes(sPNGSignatures);
        aPNGWriter.writeInt(13);
        int position = aPNGWriter.position();
        aPNGWriter.writeFourCC(IHDRChunk.ID);
        aPNGWriter.writeInt(this.frameWidth);
        aPNGWriter.writeInt(this.frameHeight);
        aPNGWriter.putBytes(this.ihdrData);
        CRC32 crc32 = getCRC32();
        crc32.reset();
        crc32.update(aPNGWriter.toByteArray(), position, 17);
        aPNGWriter.writeInt((int) crc32.getValue());
        for (Chunk chunk3 : this.prefixChunks) {
            if (!(chunk3 instanceof IENDChunk)) {
                ((APNGReader) this.reader).reset();
                ((APNGReader) this.reader).skip((long) chunk3.offset);
                ((APNGReader) this.reader).read(aPNGWriter.toByteArray(), aPNGWriter.position(), chunk3.length + 12);
                aPNGWriter.skip(chunk3.length + 12);
            }
        }
        for (Chunk chunk4 : this.imageChunks) {
            if (chunk4 instanceof IDATChunk) {
                ((APNGReader) this.reader).reset();
                ((APNGReader) this.reader).skip((long) chunk4.offset);
                ((APNGReader) this.reader).read(aPNGWriter.toByteArray(), aPNGWriter.position(), chunk4.length + 12);
                aPNGWriter.skip(chunk4.length + 12);
            } else if (chunk4 instanceof FDATChunk) {
                aPNGWriter.writeInt(chunk4.length - 4);
                int position2 = aPNGWriter.position();
                aPNGWriter.writeFourCC(IDATChunk.ID);
                ((APNGReader) this.reader).reset();
                ((APNGReader) this.reader).skip((long) (chunk4.offset + 4 + 4 + 4));
                ((APNGReader) this.reader).read(aPNGWriter.toByteArray(), aPNGWriter.position(), chunk4.length - 4);
                aPNGWriter.skip(chunk4.length - 4);
                crc32.reset();
                crc32.update(aPNGWriter.toByteArray(), position2, chunk4.length);
                aPNGWriter.writeInt((int) crc32.getValue());
            }
        }
        aPNGWriter.putBytes(sPNGEndChunk);
        return length;
    }

    public Bitmap draw(Canvas canvas, Paint paint, int i, Bitmap bitmap, APNGWriter aPNGWriter) {
        try {
            int encode = encode(aPNGWriter);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = false;
            options.inSampleSize = i;
            options.inMutable = true;
            options.inBitmap = bitmap;
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(aPNGWriter.toByteArray(), 0, encode, options);
            float f = (float) i;
            canvas.drawBitmap(decodeByteArray, ((float) this.frameX) / f, ((float) this.frameY) / f, paint);
            return decodeByteArray;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
