package org.signal.glide.apng.decode;

import java.io.IOException;
import org.signal.glide.apng.io.APNGReader;

/* loaded from: classes3.dex */
public class FCTLChunk extends Chunk {
    static final int ID = Chunk.fourCCToInt("fcTL");
    byte blend_op;
    short delay_den;
    short delay_num;
    byte dispose_op;
    int height;
    int sequence_number;
    int width;
    int x_offset;
    int y_offset;

    @Override // org.signal.glide.apng.decode.Chunk
    void innerParse(APNGReader aPNGReader) throws IOException {
        this.sequence_number = aPNGReader.readInt();
        this.width = aPNGReader.readInt();
        this.height = aPNGReader.readInt();
        this.x_offset = aPNGReader.readInt();
        this.y_offset = aPNGReader.readInt();
        this.delay_num = aPNGReader.readShort();
        this.delay_den = aPNGReader.readShort();
        this.dispose_op = aPNGReader.peek();
        this.blend_op = aPNGReader.peek();
    }
}
