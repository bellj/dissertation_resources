package org.signal.glide.apng;

import org.signal.glide.apng.decode.APNGDecoder;
import org.signal.glide.common.FrameAnimationDrawable;

/* loaded from: classes3.dex */
public class APNGDrawable extends FrameAnimationDrawable<APNGDecoder> {
    public APNGDrawable(APNGDecoder aPNGDecoder) {
        super(aPNGDecoder);
    }
}
