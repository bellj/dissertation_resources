package org.signal.glide.apng.decode;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.signal.glide.apng.io.APNGReader;
import org.signal.glide.apng.io.APNGWriter;
import org.signal.glide.common.decode.Frame;
import org.signal.glide.common.decode.FrameSeqDecoder;
import org.signal.glide.common.io.Reader;
import org.signal.glide.common.loader.Loader;

/* loaded from: classes3.dex */
public class APNGDecoder extends FrameSeqDecoder<APNGReader, APNGWriter> {
    private static final String TAG = Log.tag(APNGDecoder.class);
    private APNGWriter apngWriter;
    private int mLoopCount;
    private final Paint paint;
    private SnapShot snapShot = new SnapShot();

    /* loaded from: classes3.dex */
    public class SnapShot {
        ByteBuffer byteBuffer;
        byte dispose_op;
        Rect dstRect;

        private SnapShot() {
            APNGDecoder.this = r1;
            this.dstRect = new Rect();
        }
    }

    public APNGDecoder(Loader loader, FrameSeqDecoder.RenderListener renderListener) {
        super(loader, renderListener);
        Paint paint = new Paint();
        this.paint = paint;
        paint.setAntiAlias(true);
    }

    @Override // org.signal.glide.common.decode.FrameSeqDecoder
    public APNGWriter getWriter() {
        if (this.apngWriter == null) {
            this.apngWriter = new APNGWriter();
        }
        return this.apngWriter;
    }

    @Override // org.signal.glide.common.decode.FrameSeqDecoder
    public APNGReader getReader(Reader reader) {
        return new APNGReader(reader);
    }

    @Override // org.signal.glide.common.decode.FrameSeqDecoder
    protected int getLoopCount() {
        return this.mLoopCount;
    }

    @Override // org.signal.glide.common.decode.FrameSeqDecoder
    protected void release() {
        this.snapShot.byteBuffer = null;
        this.apngWriter = null;
    }

    public Rect read(APNGReader aPNGReader) throws IOException {
        List<Chunk> parse = APNGParser.parse(aPNGReader);
        ArrayList arrayList = new ArrayList();
        byte[] bArr = new byte[0];
        Iterator<Chunk> it = parse.iterator();
        APNGFrame aPNGFrame = null;
        boolean z = false;
        int i = 0;
        int i2 = 0;
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Chunk next = it.next();
            if (next instanceof ACTLChunk) {
                this.mLoopCount = ((ACTLChunk) next).num_plays;
                z = true;
            } else if (next instanceof FCTLChunk) {
                aPNGFrame = new APNGFrame(aPNGReader, (FCTLChunk) next);
                aPNGFrame.prefixChunks = arrayList;
                aPNGFrame.ihdrData = bArr;
                this.frames.add(aPNGFrame);
            } else if (next instanceof FDATChunk) {
                if (aPNGFrame != null) {
                    aPNGFrame.imageChunks.add(next);
                }
            } else if (next instanceof IDATChunk) {
                if (!z) {
                    StillFrame stillFrame = new StillFrame(aPNGReader);
                    stillFrame.frameWidth = i;
                    stillFrame.frameHeight = i2;
                    this.frames.add(stillFrame);
                    this.mLoopCount = 1;
                    break;
                } else if (aPNGFrame != null) {
                    aPNGFrame.imageChunks.add(next);
                }
            } else if (next instanceof IHDRChunk) {
                IHDRChunk iHDRChunk = (IHDRChunk) next;
                i = iHDRChunk.width;
                i2 = iHDRChunk.height;
                bArr = iHDRChunk.data;
            } else if (!(next instanceof IENDChunk)) {
                arrayList.add(next);
            }
        }
        int i3 = i * i2;
        int i4 = this.sampleSize;
        this.frameBuffer = ByteBuffer.allocate(((i3 / (i4 * i4)) + 1) * 4);
        SnapShot snapShot = this.snapShot;
        int i5 = this.sampleSize;
        snapShot.byteBuffer = ByteBuffer.allocate(((i3 / (i5 * i5)) + 1) * 4);
        return new Rect(0, 0, i, i2);
    }

    @Override // org.signal.glide.common.decode.FrameSeqDecoder
    protected void renderFrame(Frame frame) {
        if (frame != null && this.fullRect != null) {
            try {
                Bitmap obtainBitmap = obtainBitmap(this.fullRect.width() / this.sampleSize, this.fullRect.height() / this.sampleSize);
                Canvas canvas = this.cachedCanvas.get(obtainBitmap);
                if (canvas == null) {
                    canvas = new Canvas(obtainBitmap);
                    this.cachedCanvas.put(obtainBitmap, canvas);
                }
                if (frame instanceof APNGFrame) {
                    this.frameBuffer.rewind();
                    obtainBitmap.copyPixelsFromBuffer(this.frameBuffer);
                    if (this.frameIndex == 0) {
                        canvas.drawColor(0, PorterDuff.Mode.CLEAR);
                    } else {
                        canvas.save();
                        canvas.clipRect(this.snapShot.dstRect);
                        SnapShot snapShot = this.snapShot;
                        byte b = snapShot.dispose_op;
                        if (b == 1) {
                            canvas.drawColor(0, PorterDuff.Mode.CLEAR);
                        } else if (b == 2) {
                            snapShot.byteBuffer.rewind();
                            obtainBitmap.copyPixelsFromBuffer(this.snapShot.byteBuffer);
                        }
                        canvas.restore();
                    }
                    if (((APNGFrame) frame).dispose_op == 2) {
                        SnapShot snapShot2 = this.snapShot;
                        if (snapShot2.dispose_op != 2) {
                            snapShot2.byteBuffer.rewind();
                            obtainBitmap.copyPixelsToBuffer(this.snapShot.byteBuffer);
                        }
                    }
                    this.snapShot.dispose_op = ((APNGFrame) frame).dispose_op;
                    canvas.save();
                    if (((APNGFrame) frame).blend_op == 0) {
                        int i = frame.frameX;
                        int i2 = this.sampleSize;
                        int i3 = frame.frameY;
                        canvas.clipRect(i / i2, i3 / i2, (i + frame.frameWidth) / i2, (i3 + frame.frameHeight) / i2);
                        canvas.drawColor(0, PorterDuff.Mode.CLEAR);
                    }
                    Rect rect = this.snapShot.dstRect;
                    int i4 = frame.frameX;
                    int i5 = this.sampleSize;
                    int i6 = frame.frameY;
                    rect.set(i4 / i5, i6 / i5, (i4 + frame.frameWidth) / i5, (i6 + frame.frameHeight) / i5);
                    canvas.restore();
                }
                Bitmap obtainBitmap2 = obtainBitmap(frame.frameWidth, frame.frameHeight);
                recycleBitmap(frame.draw(canvas, this.paint, this.sampleSize, obtainBitmap2, getWriter()));
                recycleBitmap(obtainBitmap2);
                this.frameBuffer.rewind();
                obtainBitmap.copyPixelsToBuffer(this.frameBuffer);
                recycleBitmap(obtainBitmap);
            } catch (Throwable th) {
                Log.e(TAG, "Failed to render!", th);
            }
        }
    }
}
