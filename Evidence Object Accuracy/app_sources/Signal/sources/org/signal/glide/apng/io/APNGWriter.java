package org.signal.glide.apng.io;

import java.nio.ByteOrder;
import org.signal.glide.common.io.ByteBufferWriter;

/* loaded from: classes3.dex */
public class APNGWriter extends ByteBufferWriter {
    public void writeFourCC(int i) {
        putByte((byte) (i & 255));
        putByte((byte) ((i >> 8) & 255));
        putByte((byte) ((i >> 16) & 255));
        putByte((byte) ((i >> 24) & 255));
    }

    public void writeInt(int i) {
        putByte((byte) ((i >> 24) & 255));
        putByte((byte) ((i >> 16) & 255));
        putByte((byte) ((i >> 8) & 255));
        putByte((byte) (i & 255));
    }

    @Override // org.signal.glide.common.io.ByteBufferWriter
    public void reset(int i) {
        super.reset(i);
        this.byteBuffer.order(ByteOrder.BIG_ENDIAN);
    }
}
