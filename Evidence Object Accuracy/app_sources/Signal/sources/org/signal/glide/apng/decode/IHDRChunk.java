package org.signal.glide.apng.decode;

import java.io.IOException;
import org.signal.glide.apng.io.APNGReader;

/* loaded from: classes3.dex */
public class IHDRChunk extends Chunk {
    static final int ID = Chunk.fourCCToInt("IHDR");
    byte[] data = new byte[5];
    int height;
    int width;

    @Override // org.signal.glide.apng.decode.Chunk
    void innerParse(APNGReader aPNGReader) throws IOException {
        this.width = aPNGReader.readInt();
        this.height = aPNGReader.readInt();
        byte[] bArr = this.data;
        aPNGReader.read(bArr, 0, bArr.length);
    }
}
