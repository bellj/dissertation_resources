package org.signal.glide.apng.decode;

import java.io.IOException;
import org.signal.glide.apng.io.APNGReader;

/* loaded from: classes3.dex */
public class FDATChunk extends Chunk {
    static final int ID = Chunk.fourCCToInt("fdAT");
    int sequence_number;

    @Override // org.signal.glide.apng.decode.Chunk
    void innerParse(APNGReader aPNGReader) throws IOException {
        this.sequence_number = aPNGReader.readInt();
    }
}
