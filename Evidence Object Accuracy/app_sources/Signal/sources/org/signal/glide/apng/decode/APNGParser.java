package org.signal.glide.apng.decode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.signal.glide.apng.io.APNGReader;
import org.signal.glide.common.io.Reader;

/* loaded from: classes3.dex */
public class APNGParser {

    /* loaded from: classes3.dex */
    public static class FormatException extends IOException {
        FormatException() {
            super("APNG Format error");
        }
    }

    public static boolean isAPNG(Reader reader) {
        APNGReader aPNGReader = reader instanceof APNGReader ? (APNGReader) reader : new APNGReader(reader);
        try {
            if (!aPNGReader.matchFourCC("PNG") || !aPNGReader.matchFourCC("\r\n\u001a\n")) {
                throw new FormatException();
            }
            while (aPNGReader.available() > 0) {
                if (parseChunk(aPNGReader) instanceof ACTLChunk) {
                    return true;
                }
            }
            return false;
        } catch (IOException unused) {
            return false;
        }
    }

    public static List<Chunk> parse(APNGReader aPNGReader) throws IOException {
        if (!aPNGReader.matchFourCC("PNG") || !aPNGReader.matchFourCC("\r\n\u001a\n")) {
            throw new FormatException();
        }
        ArrayList arrayList = new ArrayList();
        while (aPNGReader.available() > 0) {
            arrayList.add(parseChunk(aPNGReader));
        }
        return arrayList;
    }

    private static Chunk parseChunk(APNGReader aPNGReader) throws IOException {
        Chunk chunk;
        int position = aPNGReader.position();
        int readInt = aPNGReader.readInt();
        int readFourCC = aPNGReader.readFourCC();
        if (readFourCC == ACTLChunk.ID) {
            chunk = new ACTLChunk();
        } else if (readFourCC == FCTLChunk.ID) {
            chunk = new FCTLChunk();
        } else if (readFourCC == FDATChunk.ID) {
            chunk = new FDATChunk();
        } else if (readFourCC == IDATChunk.ID) {
            chunk = new IDATChunk();
        } else if (readFourCC == IENDChunk.ID) {
            chunk = new IENDChunk();
        } else if (readFourCC == IHDRChunk.ID) {
            chunk = new IHDRChunk();
        } else {
            chunk = new Chunk();
        }
        chunk.offset = position;
        chunk.fourcc = readFourCC;
        chunk.length = readInt;
        chunk.parse(aPNGReader);
        chunk.crc = aPNGReader.readInt();
        return chunk;
    }
}
