package org.signal.glide.apng.decode;

import java.io.IOException;
import org.signal.glide.apng.io.APNGReader;

/* loaded from: classes3.dex */
public class ACTLChunk extends Chunk {
    static final int ID = Chunk.fourCCToInt("acTL");
    int num_frames;
    int num_plays;

    @Override // org.signal.glide.apng.decode.Chunk
    void innerParse(APNGReader aPNGReader) throws IOException {
        this.num_frames = aPNGReader.readInt();
        this.num_plays = aPNGReader.readInt();
    }
}
