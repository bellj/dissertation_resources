package org.signal.glide.apng.decode;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import java.io.IOException;
import org.signal.glide.apng.io.APNGReader;
import org.signal.glide.apng.io.APNGWriter;
import org.signal.glide.common.decode.Frame;

/* loaded from: classes3.dex */
public class StillFrame extends Frame<APNGReader, APNGWriter> {
    public StillFrame(APNGReader aPNGReader) {
        super(aPNGReader);
    }

    public Bitmap draw(Canvas canvas, Paint paint, int i, Bitmap bitmap, APNGWriter aPNGWriter) {
        IOException e;
        Bitmap decodeStream;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        options.inSampleSize = i;
        options.inMutable = true;
        options.inBitmap = bitmap;
        Bitmap bitmap2 = null;
        try {
            ((APNGReader) this.reader).reset();
            decodeStream = BitmapFactory.decodeStream(((APNGReader) this.reader).toInputStream(), null, options);
        } catch (IOException e2) {
            e = e2;
        }
        try {
            paint.setXfermode(null);
            canvas.drawBitmap(decodeStream, 0.0f, 0.0f, paint);
            return decodeStream;
        } catch (IOException e3) {
            e = e3;
            bitmap2 = decodeStream;
            e.printStackTrace();
            return bitmap2;
        }
    }
}
