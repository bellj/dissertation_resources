package org.signal.glide;

/* loaded from: classes.dex */
public final class SignalGlideCodecs {
    private static Log$Provider logProvider = Log$Provider.EMPTY;

    public static void setLogProvider(Log$Provider log$Provider) {
        logProvider = log$Provider;
    }
}
