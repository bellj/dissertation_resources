package org.signal.glide.common.decode;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import org.signal.glide.common.io.Reader;
import org.signal.glide.common.io.Writer;

/* loaded from: classes3.dex */
public abstract class Frame<R extends Reader, W extends Writer> {
    public int frameDuration;
    public int frameHeight;
    public int frameWidth;
    public int frameX;
    public int frameY;
    protected final R reader;

    public abstract Bitmap draw(Canvas canvas, Paint paint, int i, Bitmap bitmap, W w);

    public Frame(R r) {
        this.reader = r;
    }
}
