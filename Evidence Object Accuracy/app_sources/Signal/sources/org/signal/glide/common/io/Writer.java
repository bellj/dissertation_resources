package org.signal.glide.common.io;

import java.io.IOException;

/* loaded from: classes3.dex */
public interface Writer {
    void close() throws IOException;
}
