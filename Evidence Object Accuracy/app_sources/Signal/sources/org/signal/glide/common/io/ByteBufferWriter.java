package org.signal.glide.common.io;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/* loaded from: classes3.dex */
public class ByteBufferWriter implements Writer {
    protected ByteBuffer byteBuffer;

    @Override // org.signal.glide.common.io.Writer
    public void close() {
    }

    public ByteBufferWriter() {
        reset(10240);
    }

    public void putByte(byte b) {
        this.byteBuffer.put(b);
    }

    public void putBytes(byte[] bArr) {
        this.byteBuffer.put(bArr);
    }

    public int position() {
        return this.byteBuffer.position();
    }

    public void skip(int i) {
        this.byteBuffer.position(i + position());
    }

    public byte[] toByteArray() {
        return this.byteBuffer.array();
    }

    public void reset(int i) {
        ByteBuffer byteBuffer = this.byteBuffer;
        if (byteBuffer == null || i > byteBuffer.capacity()) {
            ByteBuffer allocate = ByteBuffer.allocate(i);
            this.byteBuffer = allocate;
            allocate.order(ByteOrder.LITTLE_ENDIAN);
        }
        this.byteBuffer.clear();
    }
}
