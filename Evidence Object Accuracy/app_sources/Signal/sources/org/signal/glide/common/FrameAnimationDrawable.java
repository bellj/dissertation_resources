package org.signal.glide.common;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.DrawFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import androidx.vectordrawable.graphics.drawable.Animatable2Compat$AnimationCallback;
import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.Set;
import org.signal.core.util.logging.Log;
import org.signal.glide.common.decode.FrameSeqDecoder;

/* loaded from: classes3.dex */
public abstract class FrameAnimationDrawable<Decoder extends FrameSeqDecoder> extends Drawable implements Animatable, FrameSeqDecoder.RenderListener {
    private static final String TAG = Log.tag(FrameAnimationDrawable.class);
    private Set<Animatable2Compat$AnimationCallback> animationCallbacks = new HashSet();
    private boolean autoPlay = true;
    private Bitmap bitmap;
    private DrawFilter drawFilter = new PaintFlagsDrawFilter(0, 3);
    private final Decoder frameSeqDecoder;
    private Runnable invalidateRunnable = new Runnable() { // from class: org.signal.glide.common.FrameAnimationDrawable.2
        @Override // java.lang.Runnable
        public void run() {
            FrameAnimationDrawable.this.invalidateSelf();
        }
    };
    private Matrix matrix = new Matrix();
    private final Paint paint;
    private Handler uiHandler = new Handler(Looper.getMainLooper()) { // from class: org.signal.glide.common.FrameAnimationDrawable.1
        @Override // android.os.Handler
        public void handleMessage(Message message) {
            int i = message.what;
            if (i == 1) {
                for (Animatable2Compat$AnimationCallback animatable2Compat$AnimationCallback : FrameAnimationDrawable.this.animationCallbacks) {
                    animatable2Compat$AnimationCallback.onAnimationStart(FrameAnimationDrawable.this);
                }
            } else if (i == 2) {
                for (Animatable2Compat$AnimationCallback animatable2Compat$AnimationCallback2 : FrameAnimationDrawable.this.animationCallbacks) {
                    animatable2Compat$AnimationCallback2.onAnimationEnd(FrameAnimationDrawable.this);
                }
            }
        }
    };

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -3;
    }

    public FrameAnimationDrawable(Decoder decoder) {
        Paint paint = new Paint();
        this.paint = paint;
        paint.setAntiAlias(true);
        this.frameSeqDecoder = decoder;
    }

    public void setAutoPlay(boolean z) {
        this.autoPlay = z;
    }

    public void setLoopLimit(int i) {
        this.frameSeqDecoder.setLoopLimit(i);
    }

    @Override // android.graphics.drawable.Animatable
    public void start() {
        if (this.autoPlay) {
            this.frameSeqDecoder.start();
            return;
        }
        this.frameSeqDecoder.addRenderListener(this);
        if (!this.frameSeqDecoder.isRunning()) {
            this.frameSeqDecoder.start();
        }
    }

    @Override // android.graphics.drawable.Animatable
    public void stop() {
        if (this.autoPlay) {
            this.frameSeqDecoder.stop();
            return;
        }
        this.frameSeqDecoder.removeRenderListener(this);
        this.frameSeqDecoder.stopIfNeeded();
    }

    @Override // android.graphics.drawable.Animatable
    public boolean isRunning() {
        return this.frameSeqDecoder.isRunning();
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        Bitmap bitmap = this.bitmap;
        if (bitmap != null && !bitmap.isRecycled()) {
            canvas.setDrawFilter(this.drawFilter);
            canvas.drawBitmap(this.bitmap, this.matrix, this.paint);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setBounds(int i, int i2, int i3, int i4) {
        super.setBounds(i, i2, i3, i4);
        boolean desiredSize = this.frameSeqDecoder.setDesiredSize(getBounds().width(), getBounds().height());
        this.matrix.setScale(((((float) getBounds().width()) * 1.0f) * ((float) this.frameSeqDecoder.getSampleSize())) / ((float) this.frameSeqDecoder.getBounds().width()), ((((float) getBounds().height()) * 1.0f) * ((float) this.frameSeqDecoder.getSampleSize())) / ((float) this.frameSeqDecoder.getBounds().height()));
        if (desiredSize) {
            this.bitmap = Bitmap.createBitmap(this.frameSeqDecoder.getBounds().width() / this.frameSeqDecoder.getSampleSize(), this.frameSeqDecoder.getBounds().height() / this.frameSeqDecoder.getSampleSize(), Bitmap.Config.ARGB_8888);
        }
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        this.paint.setAlpha(i);
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        this.paint.setColorFilter(colorFilter);
    }

    @Override // org.signal.glide.common.decode.FrameSeqDecoder.RenderListener
    public void onStart() {
        Message.obtain(this.uiHandler, 1).sendToTarget();
    }

    @Override // org.signal.glide.common.decode.FrameSeqDecoder.RenderListener
    public void onRender(ByteBuffer byteBuffer) {
        if (isRunning()) {
            Bitmap bitmap = this.bitmap;
            if (bitmap == null || bitmap.isRecycled()) {
                this.bitmap = Bitmap.createBitmap(this.frameSeqDecoder.getBounds().width() / this.frameSeqDecoder.getSampleSize(), this.frameSeqDecoder.getBounds().height() / this.frameSeqDecoder.getSampleSize(), Bitmap.Config.ARGB_8888);
            }
            byteBuffer.rewind();
            if (byteBuffer.remaining() < this.bitmap.getByteCount()) {
                Log.e(TAG, "onRender:Buffer not large enough for pixels");
                return;
            }
            this.bitmap.copyPixelsFromBuffer(byteBuffer);
            this.uiHandler.post(this.invalidateRunnable);
        }
    }

    @Override // org.signal.glide.common.decode.FrameSeqDecoder.RenderListener
    public void onEnd() {
        Message.obtain(this.uiHandler, 2).sendToTarget();
    }

    @Override // android.graphics.drawable.Drawable
    public boolean setVisible(boolean z, boolean z2) {
        if (this.autoPlay) {
            if (z) {
                if (!isRunning()) {
                    start();
                }
            } else if (isRunning()) {
                stop();
            }
        }
        return super.setVisible(z, z2);
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicWidth() {
        try {
            return this.frameSeqDecoder.getBounds().width();
        } catch (Exception unused) {
            return 0;
        }
    }

    @Override // android.graphics.drawable.Drawable
    public int getIntrinsicHeight() {
        try {
            return this.frameSeqDecoder.getBounds().height();
        } catch (Exception unused) {
            return 0;
        }
    }
}
