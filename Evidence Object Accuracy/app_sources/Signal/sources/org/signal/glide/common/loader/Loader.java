package org.signal.glide.common.loader;

import java.io.IOException;
import org.signal.glide.common.io.Reader;

/* loaded from: classes3.dex */
public interface Loader {
    Reader obtain() throws IOException;
}
