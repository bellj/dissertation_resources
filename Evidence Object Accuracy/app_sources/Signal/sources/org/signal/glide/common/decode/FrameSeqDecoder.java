package org.signal.glide.common.decode;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.LockSupport;
import org.signal.core.util.logging.Log;
import org.signal.glide.common.executor.FrameDecoderExecutor;
import org.signal.glide.common.io.Reader;
import org.signal.glide.common.io.Writer;
import org.signal.glide.common.loader.Loader;

/* loaded from: classes3.dex */
public abstract class FrameSeqDecoder<R extends Reader, W extends Writer> {
    private static final Rect RECT_EMPTY = new Rect();
    private static final String TAG = Log.tag(FrameSeqDecoder.class);
    private Set<Bitmap> cacheBitmaps = new HashSet();
    protected Map<Bitmap, Canvas> cachedCanvas = new WeakHashMap();
    private boolean finished = false;
    protected ByteBuffer frameBuffer;
    protected int frameIndex = -1;
    protected List<Frame> frames = new ArrayList();
    protected volatile Rect fullRect;
    private Integer loopLimit = null;
    private final Loader mLoader;
    private R mReader = null;
    private volatile State mState = State.IDLE;
    private W mWriter = getWriter();
    private AtomicBoolean paused = new AtomicBoolean(true);
    private int playCount;
    private Set<RenderListener> renderListeners = new HashSet();
    private Runnable renderTask = new Runnable() { // from class: org.signal.glide.common.decode.FrameSeqDecoder.1
        @Override // java.lang.Runnable
        public void run() {
            if (!FrameSeqDecoder.this.paused.get()) {
                if (FrameSeqDecoder.this.canStep()) {
                    long currentTimeMillis = System.currentTimeMillis();
                    FrameSeqDecoder.this.workerHandler.postDelayed(this, Math.max(0L, FrameSeqDecoder.this.step() - (System.currentTimeMillis() - currentTimeMillis)));
                    for (RenderListener renderListener : FrameSeqDecoder.this.renderListeners) {
                        renderListener.onRender(FrameSeqDecoder.this.frameBuffer);
                    }
                    return;
                }
                FrameSeqDecoder.this.stop();
            }
        }
    };
    protected int sampleSize = 1;
    private final int taskId;
    private final Handler workerHandler;

    /* loaded from: classes3.dex */
    public interface RenderListener {
        void onEnd();

        void onRender(ByteBuffer byteBuffer);

        void onStart();
    }

    /* loaded from: classes3.dex */
    public enum State {
        IDLE,
        RUNNING,
        INITIALIZING,
        FINISHING
    }

    private String debugInfo() {
        return "";
    }

    protected abstract int getLoopCount();

    protected abstract R getReader(Reader reader);

    protected abstract W getWriter();

    protected abstract Rect read(R r) throws IOException;

    protected abstract void release();

    protected abstract void renderFrame(Frame frame);

    public Loader getLoader() {
        return this.mLoader;
    }

    public Bitmap obtainBitmap(int i, int i2) {
        Iterator<Bitmap> it = this.cacheBitmaps.iterator();
        Bitmap bitmap = null;
        while (it.hasNext()) {
            int i3 = i * i2 * 4;
            Bitmap next = it.next();
            if (next == null || next.getAllocationByteCount() < i3) {
                bitmap = next;
            } else {
                it.remove();
                if (!(next.getWidth() == i && next.getHeight() == i2)) {
                    next.reconfigure(i, i2, Bitmap.Config.ARGB_8888);
                }
                next.eraseColor(0);
                return next;
            }
        }
        try {
            return Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return bitmap;
        }
    }

    public void recycleBitmap(Bitmap bitmap) {
        if (bitmap != null && !this.cacheBitmaps.contains(bitmap)) {
            this.cacheBitmaps.add(bitmap);
        }
    }

    public FrameSeqDecoder(Loader loader, RenderListener renderListener) {
        this.mLoader = loader;
        if (renderListener != null) {
            this.renderListeners.add(renderListener);
        }
        int generateTaskId = FrameDecoderExecutor.getInstance().generateTaskId();
        this.taskId = generateTaskId;
        this.workerHandler = new Handler(FrameDecoderExecutor.getInstance().getLooper(generateTaskId));
    }

    public void addRenderListener(final RenderListener renderListener) {
        this.workerHandler.post(new Runnable() { // from class: org.signal.glide.common.decode.FrameSeqDecoder.2
            @Override // java.lang.Runnable
            public void run() {
                FrameSeqDecoder.this.renderListeners.add(renderListener);
            }
        });
    }

    public void removeRenderListener(final RenderListener renderListener) {
        this.workerHandler.post(new Runnable() { // from class: org.signal.glide.common.decode.FrameSeqDecoder.3
            @Override // java.lang.Runnable
            public void run() {
                FrameSeqDecoder.this.renderListeners.remove(renderListener);
            }
        });
    }

    public void stopIfNeeded() {
        this.workerHandler.post(new Runnable() { // from class: org.signal.glide.common.decode.FrameSeqDecoder.4
            @Override // java.lang.Runnable
            public void run() {
                if (FrameSeqDecoder.this.renderListeners.size() == 0) {
                    FrameSeqDecoder.this.stop();
                }
            }
        });
    }

    public Rect getBounds() {
        if (this.fullRect == null) {
            if (this.mState == State.FINISHING) {
                Log.e(TAG, "In finishing,do not interrupt");
            }
            final Thread currentThread = Thread.currentThread();
            this.workerHandler.post(new Runnable() { // from class: org.signal.glide.common.decode.FrameSeqDecoder.5
                /* JADX DEBUG: Multi-variable search result rejected for r0v8, resolved type: org.signal.glide.common.decode.FrameSeqDecoder */
                /* JADX WARN: Multi-variable type inference failed */
                @Override // java.lang.Runnable
                public void run() {
                    try {
                        try {
                            if (FrameSeqDecoder.this.fullRect == null) {
                                if (FrameSeqDecoder.this.mReader == null) {
                                    FrameSeqDecoder frameSeqDecoder = FrameSeqDecoder.this;
                                    frameSeqDecoder.mReader = frameSeqDecoder.getReader(frameSeqDecoder.mLoader.obtain());
                                } else {
                                    FrameSeqDecoder.this.mReader.reset();
                                }
                                FrameSeqDecoder frameSeqDecoder2 = FrameSeqDecoder.this;
                                frameSeqDecoder2.initCanvasBounds(frameSeqDecoder2.read(frameSeqDecoder2.mReader));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            FrameSeqDecoder.this.fullRect = FrameSeqDecoder.RECT_EMPTY;
                        }
                    } finally {
                        LockSupport.unpark(currentThread);
                    }
                }
            });
            LockSupport.park(currentThread);
        }
        return this.fullRect;
    }

    public void initCanvasBounds(Rect rect) {
        this.fullRect = rect;
        int width = rect.width() * rect.height();
        int i = this.sampleSize;
        this.frameBuffer = ByteBuffer.allocate(((width / (i * i)) + 1) * 4);
        if (this.mWriter == null) {
            this.mWriter = getWriter();
        }
    }

    private int getFrameCount() {
        return this.frames.size();
    }

    public void start() {
        State state;
        if (this.fullRect != RECT_EMPTY) {
            if (this.mState == State.RUNNING || this.mState == (state = State.INITIALIZING)) {
                String str = TAG;
                Log.i(str, debugInfo() + " Already started");
                return;
            }
            if (this.mState == State.FINISHING) {
                String str2 = TAG;
                Log.e(str2, debugInfo() + " Processing,wait for finish at " + this.mState);
            }
            this.mState = state;
            if (Looper.myLooper() == this.workerHandler.getLooper()) {
                innerStart();
            } else {
                this.workerHandler.post(new Runnable() { // from class: org.signal.glide.common.decode.FrameSeqDecoder.6
                    @Override // java.lang.Runnable
                    public void run() {
                        FrameSeqDecoder.this.innerStart();
                    }
                });
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public void innerStart() {
        this.paused.compareAndSet(true, false);
        long currentTimeMillis = System.currentTimeMillis();
        try {
            if (this.frames.size() == 0) {
                R r = this.mReader;
                if (r == null) {
                    this.mReader = getReader(this.mLoader.obtain());
                } else {
                    r.reset();
                }
                initCanvasBounds(read(this.mReader));
            }
            String str = TAG;
            Log.i(str, debugInfo() + " Set state to RUNNING,cost " + (System.currentTimeMillis() - currentTimeMillis));
            this.mState = State.RUNNING;
            if (getNumPlays() == 0 || !this.finished) {
                this.frameIndex = -1;
                this.renderTask.run();
                for (RenderListener renderListener : this.renderListeners) {
                    renderListener.onStart();
                }
                return;
            }
            Log.i(str, debugInfo() + " No need to started");
        } catch (Throwable th) {
            String str2 = TAG;
            Log.i(str2, debugInfo() + " Set state to RUNNING,cost " + (System.currentTimeMillis() - currentTimeMillis));
            this.mState = State.RUNNING;
            throw th;
        }
    }

    public void innerStop() {
        this.workerHandler.removeCallbacks(this.renderTask);
        this.frames.clear();
        for (Bitmap bitmap : this.cacheBitmaps) {
            if (bitmap != null && !bitmap.isRecycled()) {
                bitmap.recycle();
            }
        }
        this.cacheBitmaps.clear();
        if (this.frameBuffer != null) {
            this.frameBuffer = null;
        }
        this.cachedCanvas.clear();
        try {
            R r = this.mReader;
            if (r != null) {
                r.close();
                this.mReader = null;
            }
            W w = this.mWriter;
            if (w != null) {
                w.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        release();
        this.mState = State.IDLE;
        for (RenderListener renderListener : this.renderListeners) {
            renderListener.onEnd();
        }
    }

    public void stop() {
        if (this.fullRect != RECT_EMPTY) {
            State state = this.mState;
            State state2 = State.FINISHING;
            if (state == state2 || this.mState == State.IDLE) {
                String str = TAG;
                Log.i(str, debugInfo() + "No need to stop");
                return;
            }
            if (this.mState == State.INITIALIZING) {
                String str2 = TAG;
                Log.e(str2, debugInfo() + "Processing,wait for finish at " + this.mState);
            }
            this.mState = state2;
            if (Looper.myLooper() == this.workerHandler.getLooper()) {
                innerStop();
            } else {
                this.workerHandler.post(new Runnable() { // from class: org.signal.glide.common.decode.FrameSeqDecoder.7
                    @Override // java.lang.Runnable
                    public void run() {
                        FrameSeqDecoder.this.innerStop();
                    }
                });
            }
        }
    }

    public boolean isRunning() {
        return this.mState == State.RUNNING || this.mState == State.INITIALIZING;
    }

    public void setLoopLimit(int i) {
        this.loopLimit = Integer.valueOf(i);
    }

    public int getSampleSize() {
        return this.sampleSize;
    }

    public boolean setDesiredSize(int i, int i2) {
        int desiredSample = getDesiredSample(i, i2);
        if (desiredSample == this.sampleSize) {
            return false;
        }
        this.sampleSize = desiredSample;
        final boolean isRunning = isRunning();
        this.workerHandler.removeCallbacks(this.renderTask);
        this.workerHandler.post(new Runnable() { // from class: org.signal.glide.common.decode.FrameSeqDecoder.8
            /* JADX DEBUG: Multi-variable search result rejected for r0v2, resolved type: org.signal.glide.common.decode.FrameSeqDecoder */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // java.lang.Runnable
            public void run() {
                FrameSeqDecoder.this.innerStop();
                try {
                    FrameSeqDecoder frameSeqDecoder = FrameSeqDecoder.this;
                    frameSeqDecoder.initCanvasBounds(frameSeqDecoder.read(frameSeqDecoder.getReader(frameSeqDecoder.mLoader.obtain())));
                    if (isRunning) {
                        FrameSeqDecoder.this.innerStart();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        return true;
    }

    protected int getDesiredSample(int i, int i2) {
        int i3 = 1;
        if (i != 0 && i2 != 0) {
            int min = Math.min(getBounds().width() / i, getBounds().height() / i2);
            while (true) {
                int i4 = i3 * 2;
                if (i4 > min) {
                    break;
                }
                i3 = i4;
            }
        }
        return i3;
    }

    private int getNumPlays() {
        Integer num = this.loopLimit;
        return num != null ? num.intValue() : getLoopCount();
    }

    public boolean canStep() {
        if (!isRunning() || this.frames.size() == 0) {
            return false;
        }
        if (getNumPlays() <= 0 || this.playCount < getNumPlays() - 1) {
            return true;
        }
        if (this.playCount == getNumPlays() - 1 && this.frameIndex < getFrameCount() - 1) {
            return true;
        }
        this.finished = true;
        return false;
    }

    public long step() {
        int i = this.frameIndex + 1;
        this.frameIndex = i;
        if (i >= getFrameCount()) {
            this.frameIndex = 0;
            this.playCount++;
        }
        Frame frame = getFrame(this.frameIndex);
        if (frame == null) {
            return 0;
        }
        renderFrame(frame);
        return (long) frame.frameDuration;
    }

    private Frame getFrame(int i) {
        if (i < 0 || i >= this.frames.size()) {
            return null;
        }
        return this.frames.get(i);
    }
}
