package org.signal.glide.common.io;

import java.io.IOException;
import java.io.InputStream;

/* loaded from: classes3.dex */
public class FilterReader implements Reader {
    protected Reader reader;

    public FilterReader(Reader reader) {
        this.reader = reader;
    }

    @Override // org.signal.glide.common.io.Reader
    public long skip(long j) throws IOException {
        return this.reader.skip(j);
    }

    @Override // org.signal.glide.common.io.Reader
    public byte peek() throws IOException {
        return this.reader.peek();
    }

    @Override // org.signal.glide.common.io.Reader
    public void reset() throws IOException {
        this.reader.reset();
    }

    @Override // org.signal.glide.common.io.Reader
    public int position() {
        return this.reader.position();
    }

    @Override // org.signal.glide.common.io.Reader
    public int read(byte[] bArr, int i, int i2) throws IOException {
        return this.reader.read(bArr, i, i2);
    }

    @Override // org.signal.glide.common.io.Reader
    public int available() throws IOException {
        return this.reader.available();
    }

    @Override // org.signal.glide.common.io.Reader
    public void close() throws IOException {
        this.reader.close();
    }

    @Override // org.signal.glide.common.io.Reader
    public InputStream toInputStream() throws IOException {
        reset();
        return this.reader.toInputStream();
    }
}
