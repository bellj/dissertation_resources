package org.signal.glide.common.loader;

import java.io.IOException;
import java.nio.ByteBuffer;
import org.signal.glide.common.io.ByteBufferReader;
import org.signal.glide.common.io.Reader;

/* loaded from: classes3.dex */
public abstract class ByteBufferLoader implements Loader {
    public abstract ByteBuffer getByteBuffer();

    @Override // org.signal.glide.common.loader.Loader
    public Reader obtain() throws IOException {
        return new ByteBufferReader(getByteBuffer());
    }
}
