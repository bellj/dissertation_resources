package org.signal.donations;

import com.google.android.gms.wallet.PaymentData;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONObject;
import org.signal.donations.StripeApi;

/* compiled from: GooglePayPaymentSource.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0004\b\u0007\u0010\bJ\b\u0010\u0003\u001a\u00020\u0002H\u0016R\u0014\u0010\u0005\u001a\u00020\u00048\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lorg/signal/donations/GooglePayPaymentSource;", "Lorg/signal/donations/StripeApi$PaymentSource;", "Lorg/json/JSONObject;", "parameterize", "Lcom/google/android/gms/wallet/PaymentData;", "paymentData", "Lcom/google/android/gms/wallet/PaymentData;", "<init>", "(Lcom/google/android/gms/wallet/PaymentData;)V", "donations_release"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes3.dex */
public final class GooglePayPaymentSource implements StripeApi.PaymentSource {
    private final PaymentData paymentData;

    public GooglePayPaymentSource(PaymentData paymentData) {
        Intrinsics.checkNotNullParameter(paymentData, "paymentData");
        this.paymentData = paymentData;
    }

    @Override // org.signal.donations.StripeApi.PaymentSource
    public JSONObject parameterize() {
        JSONObject jSONObject = new JSONObject(this.paymentData.toJson()).getJSONObject("paymentMethodData").getJSONObject("tokenizationData");
        Intrinsics.checkNotNullExpressionValue(jSONObject, "paymentMethodJsonData.ge…bject(\"tokenizationData\")");
        return jSONObject;
    }
}
