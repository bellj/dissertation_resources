package org.signal.donations;

import androidx.recyclerview.widget.ItemTouchHelper;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.CompletableSource;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.io.CloseableKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsJVMKt;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.ByteString;
import org.json.JSONObject;
import org.signal.core.util.logging.Log;
import org.signal.core.util.money.FiatMoney;
import org.signal.donations.GooglePayApi;
import org.signal.donations.StripeApi;
import org.signal.donations.StripeError;
import org.thoughtcrime.securesms.components.webrtc.WebRtcCallView;
import org.thoughtcrime.securesms.contacts.ContactRepository;

/* compiled from: StripeApi.kt */
@Metadata(d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010$\n\u0002\b\f\u0018\u0000 )2\u00020\u0001:\u000b)*+,-./0123B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u0016\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010J\u0016\u0010\u0011\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u0013J\u001c\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00160\u00152\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aJ\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020 0\u0015J\u0014\u0010!\u001a\u0004\u0018\u00010\"2\b\u0010#\u001a\u0004\u0018\u00010\u001eH\u0002J\u0014\u0010$\u001a\u0004\u0018\u00010\u001e2\b\u0010#\u001a\u0004\u0018\u00010\u001eH\u0002J$\u0010%\u001a\u00020\u001c2\u0006\u0010&\u001a\u00020\u001e2\u0012\u0010'\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u001e0(H\u0002R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u00064"}, d2 = {"Lorg/signal/donations/StripeApi;", "", "configuration", "Lorg/signal/donations/StripeApi$Configuration;", "paymentIntentFetcher", "Lorg/signal/donations/StripeApi$PaymentIntentFetcher;", "setupIntentHelper", "Lorg/signal/donations/StripeApi$SetupIntentHelper;", "okHttpClient", "Lokhttp3/OkHttpClient;", "(Lorg/signal/donations/StripeApi$Configuration;Lorg/signal/donations/StripeApi$PaymentIntentFetcher;Lorg/signal/donations/StripeApi$SetupIntentHelper;Lokhttp3/OkHttpClient;)V", "confirmPaymentIntent", "Lio/reactivex/rxjava3/core/Completable;", "paymentSource", "Lorg/signal/donations/StripeApi$PaymentSource;", "paymentIntent", "Lorg/signal/donations/StripeApi$PaymentIntent;", "confirmSetupIntent", "setupIntent", "Lorg/signal/donations/StripeApi$SetupIntent;", "createPaymentIntent", "Lio/reactivex/rxjava3/core/Single;", "Lorg/signal/donations/StripeApi$CreatePaymentIntentResult;", "price", "Lorg/signal/core/util/money/FiatMoney;", "level", "", "createPaymentMethod", "Lokhttp3/Response;", "createPaymentMethodAndParseId", "", "createSetupIntent", "Lorg/signal/donations/StripeApi$CreateSetupIntentResult;", "parseDeclineCode", "Lorg/signal/donations/StripeDeclineCode;", "body", "parseErrorCode", "postForm", "endpoint", "parameters", "", "Companion", "Configuration", "CreatePaymentIntentResult", "CreateSetupIntentResult", "Gateway", "PaymentIntent", "PaymentIntentFetcher", "PaymentSource", "SetupIntent", "SetupIntentHelper", "Validation", "donations_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class StripeApi {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(StripeApi.class);
    private final Configuration configuration;
    private final OkHttpClient okHttpClient;
    private final PaymentIntentFetcher paymentIntentFetcher;
    private final SetupIntentHelper setupIntentHelper;

    /* compiled from: StripeApi.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\bf\u0018\u00002\u00020\u0001J\u001e\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH&¨\u0006\t"}, d2 = {"Lorg/signal/donations/StripeApi$PaymentIntentFetcher;", "", "fetchPaymentIntent", "Lio/reactivex/rxjava3/core/Single;", "Lorg/signal/donations/StripeApi$PaymentIntent;", "price", "Lorg/signal/core/util/money/FiatMoney;", "level", "", "donations_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public interface PaymentIntentFetcher {
        Single<PaymentIntent> fetchPaymentIntent(FiatMoney fiatMoney, long j);
    }

    /* compiled from: StripeApi.kt */
    @Metadata(bv = {}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\b\u0010\u0003\u001a\u00020\u0002H&¨\u0006\u0004"}, d2 = {"Lorg/signal/donations/StripeApi$PaymentSource;", "", "Lorg/json/JSONObject;", "parameterize", "donations_release"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes3.dex */
    public interface PaymentSource {
        JSONObject parameterize();
    }

    /* compiled from: StripeApi.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\bf\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H&J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH&¨\u0006\t"}, d2 = {"Lorg/signal/donations/StripeApi$SetupIntentHelper;", "", "fetchSetupIntent", "Lio/reactivex/rxjava3/core/Single;", "Lorg/signal/donations/StripeApi$SetupIntent;", "setDefaultPaymentMethod", "Lio/reactivex/rxjava3/core/Completable;", "paymentMethodId", "", "donations_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public interface SetupIntentHelper {
        Single<SetupIntent> fetchSetupIntent();

        Completable setDefaultPaymentMethod(String str);
    }

    public StripeApi(Configuration configuration, PaymentIntentFetcher paymentIntentFetcher, SetupIntentHelper setupIntentHelper, OkHttpClient okHttpClient) {
        Intrinsics.checkNotNullParameter(configuration, "configuration");
        Intrinsics.checkNotNullParameter(paymentIntentFetcher, "paymentIntentFetcher");
        Intrinsics.checkNotNullParameter(setupIntentHelper, "setupIntentHelper");
        Intrinsics.checkNotNullParameter(okHttpClient, "okHttpClient");
        this.configuration = configuration;
        this.paymentIntentFetcher = paymentIntentFetcher;
        this.setupIntentHelper = setupIntentHelper;
        this.okHttpClient = okHttpClient;
    }

    /* compiled from: StripeApi.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/signal/donations/StripeApi$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "donations_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    /* compiled from: StripeApi.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u0003\u0004\u0005\u0006B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0004\u0007\b\t\n¨\u0006\u000b"}, d2 = {"Lorg/signal/donations/StripeApi$CreatePaymentIntentResult;", "", "()V", "AmountIsTooLarge", "AmountIsTooSmall", "CurrencyIsNotSupported", "Success", "Lorg/signal/donations/StripeApi$CreatePaymentIntentResult$AmountIsTooSmall;", "Lorg/signal/donations/StripeApi$CreatePaymentIntentResult$AmountIsTooLarge;", "Lorg/signal/donations/StripeApi$CreatePaymentIntentResult$CurrencyIsNotSupported;", "Lorg/signal/donations/StripeApi$CreatePaymentIntentResult$Success;", "donations_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static abstract class CreatePaymentIntentResult {
        public /* synthetic */ CreatePaymentIntentResult(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* compiled from: StripeApi.kt */
        @Metadata(bv = {}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\u000f\u0010\u0010J\t\u0010\u0003\u001a\u00020\u0002HÖ\u0001J\t\u0010\u0005\u001a\u00020\u0004HÖ\u0001J\u0013\u0010\t\u001a\u00020\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÖ\u0003R\u0017\u0010\u000b\u001a\u00020\n8\u0006¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lorg/signal/donations/StripeApi$CreatePaymentIntentResult$AmountIsTooSmall;", "Lorg/signal/donations/StripeApi$CreatePaymentIntentResult;", "", "toString", "", "hashCode", "", "other", "", "equals", "Lorg/signal/core/util/money/FiatMoney;", "amount", "Lorg/signal/core/util/money/FiatMoney;", "getAmount", "()Lorg/signal/core/util/money/FiatMoney;", "<init>", "(Lorg/signal/core/util/money/FiatMoney;)V", "donations_release"}, k = 1, mv = {1, 6, 0})
        /* loaded from: classes3.dex */
        public static final class AmountIsTooSmall extends CreatePaymentIntentResult {
            private final FiatMoney amount;

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof AmountIsTooSmall) && Intrinsics.areEqual(this.amount, ((AmountIsTooSmall) obj).amount);
            }

            public int hashCode() {
                return this.amount.hashCode();
            }

            public String toString() {
                return "AmountIsTooSmall(amount=" + this.amount + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public AmountIsTooSmall(FiatMoney fiatMoney) {
                super(null);
                Intrinsics.checkNotNullParameter(fiatMoney, "amount");
                this.amount = fiatMoney;
            }
        }

        private CreatePaymentIntentResult() {
        }

        /* compiled from: StripeApi.kt */
        @Metadata(bv = {}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\u000f\u0010\u0010J\t\u0010\u0003\u001a\u00020\u0002HÖ\u0001J\t\u0010\u0005\u001a\u00020\u0004HÖ\u0001J\u0013\u0010\t\u001a\u00020\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÖ\u0003R\u0017\u0010\u000b\u001a\u00020\n8\u0006¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lorg/signal/donations/StripeApi$CreatePaymentIntentResult$AmountIsTooLarge;", "Lorg/signal/donations/StripeApi$CreatePaymentIntentResult;", "", "toString", "", "hashCode", "", "other", "", "equals", "Lorg/signal/core/util/money/FiatMoney;", "amount", "Lorg/signal/core/util/money/FiatMoney;", "getAmount", "()Lorg/signal/core/util/money/FiatMoney;", "<init>", "(Lorg/signal/core/util/money/FiatMoney;)V", "donations_release"}, k = 1, mv = {1, 6, 0})
        /* loaded from: classes3.dex */
        public static final class AmountIsTooLarge extends CreatePaymentIntentResult {
            private final FiatMoney amount;

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof AmountIsTooLarge) && Intrinsics.areEqual(this.amount, ((AmountIsTooLarge) obj).amount);
            }

            public int hashCode() {
                return this.amount.hashCode();
            }

            public String toString() {
                return "AmountIsTooLarge(amount=" + this.amount + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public AmountIsTooLarge(FiatMoney fiatMoney) {
                super(null);
                Intrinsics.checkNotNullParameter(fiatMoney, "amount");
                this.amount = fiatMoney;
            }
        }

        /* compiled from: StripeApi.kt */
        @Metadata(bv = {}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\b\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\t\u0010\u0003\u001a\u00020\u0002HÖ\u0001J\t\u0010\u0005\u001a\u00020\u0004HÖ\u0001J\u0013\u0010\t\u001a\u00020\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÖ\u0003R\u0017\u0010\n\u001a\u00020\u00028\u0006¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\r¨\u0006\u0010"}, d2 = {"Lorg/signal/donations/StripeApi$CreatePaymentIntentResult$CurrencyIsNotSupported;", "Lorg/signal/donations/StripeApi$CreatePaymentIntentResult;", "", "toString", "", "hashCode", "", "other", "", "equals", "currencyCode", "Ljava/lang/String;", "getCurrencyCode", "()Ljava/lang/String;", "<init>", "(Ljava/lang/String;)V", "donations_release"}, k = 1, mv = {1, 6, 0})
        /* loaded from: classes3.dex */
        public static final class CurrencyIsNotSupported extends CreatePaymentIntentResult {
            private final String currencyCode;

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof CurrencyIsNotSupported) && Intrinsics.areEqual(this.currencyCode, ((CurrencyIsNotSupported) obj).currencyCode);
            }

            public int hashCode() {
                return this.currencyCode.hashCode();
            }

            public String toString() {
                return "CurrencyIsNotSupported(currencyCode=" + this.currencyCode + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public CurrencyIsNotSupported(String str) {
                super(null);
                Intrinsics.checkNotNullParameter(str, "currencyCode");
                this.currencyCode = str;
            }
        }

        /* compiled from: StripeApi.kt */
        @Metadata(bv = {}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\u000f\u0010\u0010J\t\u0010\u0003\u001a\u00020\u0002HÖ\u0001J\t\u0010\u0005\u001a\u00020\u0004HÖ\u0001J\u0013\u0010\t\u001a\u00020\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÖ\u0003R\u0017\u0010\u000b\u001a\u00020\n8\u0006¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lorg/signal/donations/StripeApi$CreatePaymentIntentResult$Success;", "Lorg/signal/donations/StripeApi$CreatePaymentIntentResult;", "", "toString", "", "hashCode", "", "other", "", "equals", "Lorg/signal/donations/StripeApi$PaymentIntent;", "paymentIntent", "Lorg/signal/donations/StripeApi$PaymentIntent;", "getPaymentIntent", "()Lorg/signal/donations/StripeApi$PaymentIntent;", "<init>", "(Lorg/signal/donations/StripeApi$PaymentIntent;)V", "donations_release"}, k = 1, mv = {1, 6, 0})
        /* loaded from: classes3.dex */
        public static final class Success extends CreatePaymentIntentResult {
            private final PaymentIntent paymentIntent;

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof Success) && Intrinsics.areEqual(this.paymentIntent, ((Success) obj).paymentIntent);
            }

            public int hashCode() {
                return this.paymentIntent.hashCode();
            }

            public String toString() {
                return "Success(paymentIntent=" + this.paymentIntent + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Success(PaymentIntent paymentIntent) {
                super(null);
                Intrinsics.checkNotNullParameter(paymentIntent, "paymentIntent");
                this.paymentIntent = paymentIntent;
            }

            public final PaymentIntent getPaymentIntent() {
                return this.paymentIntent;
            }
        }
    }

    /* compiled from: StripeApi.kt */
    @Metadata(bv = {}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\u000e\u0010\u000fJ\t\u0010\u0003\u001a\u00020\u0002HÖ\u0001J\t\u0010\u0005\u001a\u00020\u0004HÖ\u0001J\u0013\u0010\b\u001a\u00020\u00072\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001HÖ\u0003R\u0017\u0010\n\u001a\u00020\t8\u0006¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\r¨\u0006\u0010"}, d2 = {"Lorg/signal/donations/StripeApi$CreateSetupIntentResult;", "", "", "toString", "", "hashCode", "other", "", "equals", "Lorg/signal/donations/StripeApi$SetupIntent;", "setupIntent", "Lorg/signal/donations/StripeApi$SetupIntent;", "getSetupIntent", "()Lorg/signal/donations/StripeApi$SetupIntent;", "<init>", "(Lorg/signal/donations/StripeApi$SetupIntent;)V", "donations_release"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes3.dex */
    public static final class CreateSetupIntentResult {
        private final SetupIntent setupIntent;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof CreateSetupIntentResult) && Intrinsics.areEqual(this.setupIntent, ((CreateSetupIntentResult) obj).setupIntent);
        }

        public int hashCode() {
            return this.setupIntent.hashCode();
        }

        public String toString() {
            return "CreateSetupIntentResult(setupIntent=" + this.setupIntent + ')';
        }

        public CreateSetupIntentResult(SetupIntent setupIntent) {
            Intrinsics.checkNotNullParameter(setupIntent, "setupIntent");
            this.setupIntent = setupIntent;
        }

        public final SetupIntent getSetupIntent() {
            return this.setupIntent;
        }
    }

    public final Single<CreateSetupIntentResult> createSetupIntent() {
        Single<CreateSetupIntentResult> subscribeOn = this.setupIntentHelper.fetchSetupIntent().map(new Function() { // from class: org.signal.donations.StripeApi$$ExternalSyntheticLambda3
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return StripeApi.m194createSetupIntent$lambda0((StripeApi.SetupIntent) obj);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "setupIntentHelper\n      …scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: createSetupIntent$lambda-0 */
    public static final CreateSetupIntentResult m194createSetupIntent$lambda0(SetupIntent setupIntent) {
        Intrinsics.checkNotNullExpressionValue(setupIntent, "it");
        return new CreateSetupIntentResult(setupIntent);
    }

    public final Completable confirmSetupIntent(PaymentSource paymentSource, SetupIntent setupIntent) {
        Intrinsics.checkNotNullParameter(paymentSource, "paymentSource");
        Intrinsics.checkNotNullParameter(setupIntent, "setupIntent");
        Completable flatMapCompletable = Single.fromCallable(new Callable(paymentSource, setupIntent) { // from class: org.signal.donations.StripeApi$$ExternalSyntheticLambda1
            public final /* synthetic */ StripeApi.PaymentSource f$1;
            public final /* synthetic */ StripeApi.SetupIntent f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return StripeApi.m191confirmSetupIntent$lambda1(StripeApi.this, this.f$1, this.f$2);
            }
        }).flatMapCompletable(new Function() { // from class: org.signal.donations.StripeApi$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return StripeApi.m192confirmSetupIntent$lambda2(StripeApi.this, (String) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(flatMapCompletable, "fromCallable {\n    val p…aultPaymentMethod(it)\n  }");
        return flatMapCompletable;
    }

    /* renamed from: confirmSetupIntent$lambda-1 */
    public static final String m191confirmSetupIntent$lambda1(StripeApi stripeApi, PaymentSource paymentSource, SetupIntent setupIntent) {
        Intrinsics.checkNotNullParameter(stripeApi, "this$0");
        Intrinsics.checkNotNullParameter(paymentSource, "$paymentSource");
        Intrinsics.checkNotNullParameter(setupIntent, "$setupIntent");
        String createPaymentMethodAndParseId = stripeApi.createPaymentMethodAndParseId(paymentSource);
        Map<String, String> map = MapsKt__MapsKt.mapOf(TuplesKt.to("client_secret", setupIntent.getClientSecret()), TuplesKt.to("payment_method", createPaymentMethodAndParseId));
        stripeApi.postForm("setup_intents/" + setupIntent.getId() + "/confirm", map);
        return createPaymentMethodAndParseId;
    }

    /* renamed from: confirmSetupIntent$lambda-2 */
    public static final CompletableSource m192confirmSetupIntent$lambda2(StripeApi stripeApi, String str) {
        Intrinsics.checkNotNullParameter(stripeApi, "this$0");
        SetupIntentHelper setupIntentHelper = stripeApi.setupIntentHelper;
        Intrinsics.checkNotNullExpressionValue(str, "it");
        return setupIntentHelper.setDefaultPaymentMethod(str);
    }

    public final Single<CreatePaymentIntentResult> createPaymentIntent(FiatMoney fiatMoney, long j) {
        Single single;
        Intrinsics.checkNotNullParameter(fiatMoney, "price");
        Validation validation = Validation.INSTANCE;
        if (validation.isAmountTooSmall(fiatMoney)) {
            Single<CreatePaymentIntentResult> just = Single.just(new CreatePaymentIntentResult.AmountIsTooSmall(fiatMoney));
            Intrinsics.checkNotNullExpressionValue(just, "{\n      Single.just(Crea…tIsTooSmall(price))\n    }");
            return just;
        } else if (validation.isAmountTooLarge(fiatMoney)) {
            Single<CreatePaymentIntentResult> just2 = Single.just(new CreatePaymentIntentResult.AmountIsTooLarge(fiatMoney));
            Intrinsics.checkNotNullExpressionValue(just2, "{\n      Single.just(Crea…tIsTooLarge(price))\n    }");
            return just2;
        } else {
            List<String> supportedCurrencyCodes = validation.getSupportedCurrencyCodes();
            String currencyCode = fiatMoney.getCurrency().getCurrencyCode();
            Intrinsics.checkNotNullExpressionValue(currencyCode, "price.currency.currencyCode");
            Locale locale = Locale.ROOT;
            Intrinsics.checkNotNullExpressionValue(locale, "ROOT");
            String upperCase = currencyCode.toUpperCase(locale);
            Intrinsics.checkNotNullExpressionValue(upperCase, "this as java.lang.String).toUpperCase(locale)");
            if (!supportedCurrencyCodes.contains(upperCase)) {
                String currencyCode2 = fiatMoney.getCurrency().getCurrencyCode();
                Intrinsics.checkNotNullExpressionValue(currencyCode2, "price.currency.currencyCode");
                single = Single.just(new CreatePaymentIntentResult.CurrencyIsNotSupported(currencyCode2));
            } else {
                single = this.paymentIntentFetcher.fetchPaymentIntent(fiatMoney, j).map(new Function() { // from class: org.signal.donations.StripeApi$$ExternalSyntheticLambda0
                    @Override // io.reactivex.rxjava3.functions.Function
                    public final Object apply(Object obj) {
                        return StripeApi.m193createPaymentIntent$lambda3((StripeApi.PaymentIntent) obj);
                    }
                });
            }
            Single<CreatePaymentIntentResult> subscribeOn = single.subscribeOn(Schedulers.io());
            Intrinsics.checkNotNullExpressionValue(subscribeOn, "if (!Validation.supporte…scribeOn(Schedulers.io())");
            return subscribeOn;
        }
    }

    /* renamed from: createPaymentIntent$lambda-3 */
    public static final CreatePaymentIntentResult m193createPaymentIntent$lambda3(PaymentIntent paymentIntent) {
        Intrinsics.checkNotNullExpressionValue(paymentIntent, "it");
        return new CreatePaymentIntentResult.Success(paymentIntent);
    }

    public final Completable confirmPaymentIntent(PaymentSource paymentSource, PaymentIntent paymentIntent) {
        Intrinsics.checkNotNullParameter(paymentSource, "paymentSource");
        Intrinsics.checkNotNullParameter(paymentIntent, "paymentIntent");
        Completable subscribeOn = Completable.fromAction(new Action(paymentSource, paymentIntent) { // from class: org.signal.donations.StripeApi$$ExternalSyntheticLambda4
            public final /* synthetic */ StripeApi.PaymentSource f$1;
            public final /* synthetic */ StripeApi.PaymentIntent f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                StripeApi.m190confirmPaymentIntent$lambda4(StripeApi.this, this.f$1, this.f$2);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromAction {\n    val pay…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: confirmPaymentIntent$lambda-4 */
    public static final void m190confirmPaymentIntent$lambda4(StripeApi stripeApi, PaymentSource paymentSource, PaymentIntent paymentIntent) {
        Intrinsics.checkNotNullParameter(stripeApi, "this$0");
        Intrinsics.checkNotNullParameter(paymentSource, "$paymentSource");
        Intrinsics.checkNotNullParameter(paymentIntent, "$paymentIntent");
        Map<String, String> map = MapsKt__MapsKt.mutableMapOf(TuplesKt.to("client_secret", paymentIntent.getClientSecret()), TuplesKt.to("payment_method", stripeApi.createPaymentMethodAndParseId(paymentSource)));
        stripeApi.postForm("payment_intents/" + paymentIntent.getId() + "/confirm", map);
    }

    /* JADX INFO: finally extract failed */
    private final String createPaymentMethodAndParseId(PaymentSource paymentSource) {
        Response createPaymentMethod = createPaymentMethod(paymentSource);
        try {
            ResponseBody body = createPaymentMethod.body();
            if (body != null) {
                String string = body.string();
                Intrinsics.checkNotNullExpressionValue(string, "body.string()");
                String string2 = new JSONObject(StringsKt__StringsJVMKt.replace$default(string, "\n", "", false, 4, (Object) null)).getString(ContactRepository.ID_COLUMN);
                CloseableKt.closeFinally(createPaymentMethod, null);
                Intrinsics.checkNotNullExpressionValue(string2, "createPaymentMethod(paym…sponseError\n      }\n    }");
                return string2;
            }
            throw StripeError.FailedToParsePaymentMethodResponseError.INSTANCE;
        } finally {
            try {
                throw th;
            } catch (Throwable th) {
            }
        }
    }

    private final Response createPaymentMethod(PaymentSource paymentSource) {
        Pair[] pairArr = new Pair[2];
        Object obj = paymentSource.parameterize().get("token");
        if (obj != null) {
            pairArr[0] = TuplesKt.to("card[token]", new JSONObject(StringsKt__StringsJVMKt.replace$default((String) obj, "\n", "", false, 4, (Object) null)).getString(ContactRepository.ID_COLUMN));
            pairArr[1] = TuplesKt.to("type", "card");
            return postForm("payment_methods", MapsKt__MapsKt.mutableMapOf(pairArr));
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.String");
    }

    private final Response postForm(String str, Map<String, String> map) {
        FormBody.Builder builder = new FormBody.Builder();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            builder.add(entry.getKey(), entry.getValue());
        }
        Request.Builder builder2 = new Request.Builder();
        Request.Builder url = builder2.url(this.configuration.getBaseUrl() + '/' + str);
        StringBuilder sb = new StringBuilder();
        sb.append("Basic ");
        sb.append(ByteString.encodeUtf8(this.configuration.getPublishableKey() + ':').base64());
        Response execute = this.okHttpClient.newCall(url.addHeader("Authorization", sb.toString()).post(builder.build()).build()).execute();
        if (execute.isSuccessful()) {
            Intrinsics.checkNotNullExpressionValue(execute, "response");
            return execute;
        }
        ResponseBody body = execute.body();
        String string = body != null ? body.string() : null;
        throw new StripeError.PostError(execute.code(), parseErrorCode(string), parseDeclineCode(string));
    }

    private final String parseErrorCode(String str) {
        if (str == null) {
            Log.d(TAG, "parseErrorCode: No body.", true);
            return null;
        }
        try {
            return new JSONObject(str).getJSONObject("error").getString("code");
        } catch (Exception e) {
            Log.d(TAG, "parseErrorCode: Failed to parse error.", e, true);
            return null;
        }
    }

    private final StripeDeclineCode parseDeclineCode(String str) {
        if (str == null) {
            Log.d(TAG, "parseDeclineCode: No body.", true);
            return null;
        }
        try {
            return StripeDeclineCode.Companion.getFromCode(new JSONObject(str).getJSONObject("error").getString("decline_code"));
        } catch (Exception e) {
            Log.d(TAG, "parseDeclineCode: Failed to parse decline code.", e, true);
            return null;
        }
    }

    /* compiled from: StripeApi.kt */
    @Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010J\u000e\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00070\n¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f¨\u0006\u0012"}, d2 = {"Lorg/signal/donations/StripeApi$Validation;", "", "()V", "MAX_AMOUNT", "Ljava/math/BigDecimal;", "minimumIntegralChargePerCurrencyCode", "", "", "", "supportedCurrencyCodes", "", "getSupportedCurrencyCodes", "()Ljava/util/List;", "isAmountTooLarge", "", "fiatMoney", "Lorg/signal/core/util/money/FiatMoney;", "isAmountTooSmall", "donations_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Validation {
        public static final Validation INSTANCE = new Validation();
        private static final BigDecimal MAX_AMOUNT = new BigDecimal(99999999);
        private static final Map<String, Integer> minimumIntegralChargePerCurrencyCode;
        private static final List<String> supportedCurrencyCodes = CollectionsKt__CollectionsKt.listOf((Object[]) new String[]{"USD", "AED", "AFN", "ALL", "AMD", "ANG", "AOA", "ARS", "AUD", "AWG", "AZN", "BAM", "BBD", "BDT", "BGN", "BIF", "BMD", "BND", "BOB", "BRL", "BSD", "BWP", "BZD", "CAD", "CDF", "CHF", "CLP", "CNY", "COP", "CRC", "CVE", "CZK", "DJF", "DKK", "DOP", "DZD", "EGP", "ETB", "EUR", "FJD", "FKP", "GBP", "GEL", "GIP", "GMD", "GNF", "GTQ", "GYD", "HKD", "HNL", "HRK", "HTG", "HUF", "IDR", "ILS", "INR", "ISK", "JMD", "JPY", "KES", "KGS", "KHR", "KMF", "KRW", "KYD", "KZT", "LAK", "LBP", "LKR", "LRD", "LSL", "MAD", "MDL", "MGA", "MKD", "MMK", "MNT", "MOP", "MRO", "MUR", "MVR", "MWK", "MXN", "MYR", "MZN", "NAD", "NGN", "NIO", "NOK", "NPR", "NZD", "PAB", "PEN", "PGK", "PHP", "PKR", "PLN", "PYG", "QAR", "RON", "RSD", "RUB", "RWF", "SAR", "SBD", "SCR", "SEK", "SGD", "SHP", "SLL", "SOS", "SRD", "STD", "SZL", "THB", "TJS", "TOP", "TRY", "TTD", "TWD", "TZS", "UAH", "UGX", "UYU", "UZS", "VND", "VUV", "WST", "XAF", "XCD", "XOF", "XPF", "YER", "ZAR", "ZMW"});

        private Validation() {
        }

        static {
            INSTANCE = new Validation();
            MAX_AMOUNT = new BigDecimal(99999999);
            Integer valueOf = Integer.valueOf((int) WebRtcCallView.PIP_RESIZE_DURATION);
            minimumIntegralChargePerCurrencyCode = MapsKt__MapsKt.mapOf(TuplesKt.to("USD", 50), TuplesKt.to("AED", 200), TuplesKt.to("AUD", 50), TuplesKt.to("BGN", 100), TuplesKt.to("BRL", 50), TuplesKt.to("CAD", 50), TuplesKt.to("CHF", 50), TuplesKt.to("CZK", 1500), TuplesKt.to("DKK", Integer.valueOf((int) ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION)), TuplesKt.to("EUR", 50), TuplesKt.to("GBP", 30), TuplesKt.to("HKD", 400), TuplesKt.to("HUF", 17500), TuplesKt.to("INR", 50), TuplesKt.to("JPY", 50), TuplesKt.to("MXN", 10), TuplesKt.to("MYR", 2), TuplesKt.to("NOK", valueOf), TuplesKt.to("NZD", 50), TuplesKt.to("PLN", 200), TuplesKt.to("RON", 200), TuplesKt.to("SEK", valueOf), TuplesKt.to("SGD", 50));
            supportedCurrencyCodes = CollectionsKt__CollectionsKt.listOf((Object[]) new String[]{"USD", "AED", "AFN", "ALL", "AMD", "ANG", "AOA", "ARS", "AUD", "AWG", "AZN", "BAM", "BBD", "BDT", "BGN", "BIF", "BMD", "BND", "BOB", "BRL", "BSD", "BWP", "BZD", "CAD", "CDF", "CHF", "CLP", "CNY", "COP", "CRC", "CVE", "CZK", "DJF", "DKK", "DOP", "DZD", "EGP", "ETB", "EUR", "FJD", "FKP", "GBP", "GEL", "GIP", "GMD", "GNF", "GTQ", "GYD", "HKD", "HNL", "HRK", "HTG", "HUF", "IDR", "ILS", "INR", "ISK", "JMD", "JPY", "KES", "KGS", "KHR", "KMF", "KRW", "KYD", "KZT", "LAK", "LBP", "LKR", "LRD", "LSL", "MAD", "MDL", "MGA", "MKD", "MMK", "MNT", "MOP", "MRO", "MUR", "MVR", "MWK", "MXN", "MYR", "MZN", "NAD", "NGN", "NIO", "NOK", "NPR", "NZD", "PAB", "PEN", "PGK", "PHP", "PKR", "PLN", "PYG", "QAR", "RON", "RSD", "RUB", "RWF", "SAR", "SBD", "SCR", "SEK", "SGD", "SHP", "SLL", "SOS", "SRD", "STD", "SZL", "THB", "TJS", "TOP", "TRY", "TTD", "TWD", "TZS", "UAH", "UGX", "UYU", "UZS", "VND", "VUV", "WST", "XAF", "XCD", "XOF", "XPF", "YER", "ZAR", "ZMW"});
        }

        public final boolean isAmountTooLarge(FiatMoney fiatMoney) {
            Intrinsics.checkNotNullParameter(fiatMoney, "fiatMoney");
            String minimumUnitPrecisionString = fiatMoney.getMinimumUnitPrecisionString();
            Intrinsics.checkNotNullExpressionValue(minimumUnitPrecisionString, "fiatMoney.minimumUnitPrecisionString");
            return new BigDecimal(minimumUnitPrecisionString).compareTo(MAX_AMOUNT) > 0;
        }

        public final boolean isAmountTooSmall(FiatMoney fiatMoney) {
            Intrinsics.checkNotNullParameter(fiatMoney, "fiatMoney");
            String minimumUnitPrecisionString = fiatMoney.getMinimumUnitPrecisionString();
            Intrinsics.checkNotNullExpressionValue(minimumUnitPrecisionString, "fiatMoney.minimumUnitPrecisionString");
            BigDecimal bigDecimal = new BigDecimal(minimumUnitPrecisionString);
            Integer num = minimumIntegralChargePerCurrencyCode.get(fiatMoney.getCurrency().getCurrencyCode());
            return bigDecimal.compareTo(new BigDecimal(num != null ? num.intValue() : 50)) < 0;
        }

        public final List<String> getSupportedCurrencyCodes() {
            return supportedCurrencyCodes;
        }
    }

    /* compiled from: StripeApi.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010$\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0014\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00070\u000bH\u0016R\u001a\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/signal/donations/StripeApi$Gateway;", "Lorg/signal/donations/GooglePayApi$Gateway;", "configuration", "Lorg/signal/donations/StripeApi$Configuration;", "(Lorg/signal/donations/StripeApi$Configuration;)V", "allowedCardNetworks", "", "", "getAllowedCardNetworks", "()Ljava/util/List;", "getTokenizationSpecificationParameters", "", "donations_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Gateway implements GooglePayApi.Gateway {
        private final List<String> allowedCardNetworks = CollectionsKt__CollectionsKt.listOf((Object[]) new String[]{"AMEX", "DISCOVER", "JCB", "MASTERCARD", "VISA"});
        private final Configuration configuration;

        public Gateway(Configuration configuration) {
            Intrinsics.checkNotNullParameter(configuration, "configuration");
            this.configuration = configuration;
        }

        @Override // org.signal.donations.GooglePayApi.Gateway
        public Map<String, String> getTokenizationSpecificationParameters() {
            return MapsKt__MapsKt.mapOf(TuplesKt.to("gateway", "stripe"), TuplesKt.to("stripe:version", this.configuration.getVersion()), TuplesKt.to("stripe:publishableKey", this.configuration.getPublishableKey()));
        }

        @Override // org.signal.donations.GooglePayApi.Gateway
        public List<String> getAllowedCardNetworks() {
            return this.allowedCardNetworks;
        }
    }

    /* compiled from: StripeApi.kt */
    @Metadata(bv = {}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\f\b\b\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\b\b\u0002\u0010\r\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u0002¢\u0006\u0004\b\u0011\u0010\u0012J\t\u0010\u0003\u001a\u00020\u0002HÖ\u0001J\t\u0010\u0005\u001a\u00020\u0004HÖ\u0001J\u0013\u0010\b\u001a\u00020\u00072\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001HÖ\u0003R\u0017\u0010\t\u001a\u00020\u00028\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR\u0017\u0010\r\u001a\u00020\u00028\u0006¢\u0006\f\n\u0004\b\r\u0010\n\u001a\u0004\b\u000e\u0010\fR\u0017\u0010\u000f\u001a\u00020\u00028\u0006¢\u0006\f\n\u0004\b\u000f\u0010\n\u001a\u0004\b\u0010\u0010\f¨\u0006\u0013"}, d2 = {"Lorg/signal/donations/StripeApi$Configuration;", "", "", "toString", "", "hashCode", "other", "", "equals", "publishableKey", "Ljava/lang/String;", "getPublishableKey", "()Ljava/lang/String;", "baseUrl", "getBaseUrl", "version", "getVersion", "<init>", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "donations_release"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes3.dex */
    public static final class Configuration {
        private final String baseUrl;
        private final String publishableKey;
        private final String version;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Configuration)) {
                return false;
            }
            Configuration configuration = (Configuration) obj;
            return Intrinsics.areEqual(this.publishableKey, configuration.publishableKey) && Intrinsics.areEqual(this.baseUrl, configuration.baseUrl) && Intrinsics.areEqual(this.version, configuration.version);
        }

        public int hashCode() {
            return (((this.publishableKey.hashCode() * 31) + this.baseUrl.hashCode()) * 31) + this.version.hashCode();
        }

        public String toString() {
            return "Configuration(publishableKey=" + this.publishableKey + ", baseUrl=" + this.baseUrl + ", version=" + this.version + ')';
        }

        public Configuration(String str, String str2, String str3) {
            Intrinsics.checkNotNullParameter(str, "publishableKey");
            Intrinsics.checkNotNullParameter(str2, "baseUrl");
            Intrinsics.checkNotNullParameter(str3, "version");
            this.publishableKey = str;
            this.baseUrl = str2;
            this.version = str3;
        }

        public /* synthetic */ Configuration(String str, String str2, String str3, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(str, (i & 2) != 0 ? "https://api.stripe.com/v1" : str2, (i & 4) != 0 ? "2018-10-31" : str3);
        }

        public final String getPublishableKey() {
            return this.publishableKey;
        }

        public final String getBaseUrl() {
            return this.baseUrl;
        }

        public final String getVersion() {
            return this.version;
        }
    }

    /* compiled from: StripeApi.kt */
    @Metadata(bv = {}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\b\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\t\u0010\u0003\u001a\u00020\u0002HÖ\u0001J\t\u0010\u0005\u001a\u00020\u0004HÖ\u0001J\u0013\u0010\b\u001a\u00020\u00072\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001HÖ\u0003R\u0017\u0010\t\u001a\u00020\u00028\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR\u0017\u0010\r\u001a\u00020\u00028\u0006¢\u0006\f\n\u0004\b\r\u0010\n\u001a\u0004\b\u000e\u0010\f¨\u0006\u0011"}, d2 = {"Lorg/signal/donations/StripeApi$PaymentIntent;", "", "", "toString", "", "hashCode", "other", "", "equals", ContactRepository.ID_COLUMN, "Ljava/lang/String;", "getId", "()Ljava/lang/String;", "clientSecret", "getClientSecret", "<init>", "(Ljava/lang/String;Ljava/lang/String;)V", "donations_release"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes3.dex */
    public static final class PaymentIntent {
        private final String clientSecret;
        private final String id;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof PaymentIntent)) {
                return false;
            }
            PaymentIntent paymentIntent = (PaymentIntent) obj;
            return Intrinsics.areEqual(this.id, paymentIntent.id) && Intrinsics.areEqual(this.clientSecret, paymentIntent.clientSecret);
        }

        public int hashCode() {
            return (this.id.hashCode() * 31) + this.clientSecret.hashCode();
        }

        public String toString() {
            return "PaymentIntent(id=" + this.id + ", clientSecret=" + this.clientSecret + ')';
        }

        public PaymentIntent(String str, String str2) {
            Intrinsics.checkNotNullParameter(str, ContactRepository.ID_COLUMN);
            Intrinsics.checkNotNullParameter(str2, "clientSecret");
            this.id = str;
            this.clientSecret = str2;
        }

        public final String getId() {
            return this.id;
        }

        public final String getClientSecret() {
            return this.clientSecret;
        }
    }

    /* compiled from: StripeApi.kt */
    @Metadata(bv = {}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\b\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\t\u001a\u00020\u0002\u0012\u0006\u0010\r\u001a\u00020\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\t\u0010\u0003\u001a\u00020\u0002HÖ\u0001J\t\u0010\u0005\u001a\u00020\u0004HÖ\u0001J\u0013\u0010\b\u001a\u00020\u00072\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001HÖ\u0003R\u0017\u0010\t\u001a\u00020\u00028\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR\u0017\u0010\r\u001a\u00020\u00028\u0006¢\u0006\f\n\u0004\b\r\u0010\n\u001a\u0004\b\u000e\u0010\f¨\u0006\u0011"}, d2 = {"Lorg/signal/donations/StripeApi$SetupIntent;", "", "", "toString", "", "hashCode", "other", "", "equals", ContactRepository.ID_COLUMN, "Ljava/lang/String;", "getId", "()Ljava/lang/String;", "clientSecret", "getClientSecret", "<init>", "(Ljava/lang/String;Ljava/lang/String;)V", "donations_release"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes3.dex */
    public static final class SetupIntent {
        private final String clientSecret;
        private final String id;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SetupIntent)) {
                return false;
            }
            SetupIntent setupIntent = (SetupIntent) obj;
            return Intrinsics.areEqual(this.id, setupIntent.id) && Intrinsics.areEqual(this.clientSecret, setupIntent.clientSecret);
        }

        public int hashCode() {
            return (this.id.hashCode() * 31) + this.clientSecret.hashCode();
        }

        public String toString() {
            return "SetupIntent(id=" + this.id + ", clientSecret=" + this.clientSecret + ')';
        }

        public SetupIntent(String str, String str2) {
            Intrinsics.checkNotNullParameter(str, ContactRepository.ID_COLUMN);
            Intrinsics.checkNotNullParameter(str2, "clientSecret");
            this.id = str;
            this.clientSecret = str2;
        }

        public final String getId() {
            return this.id;
        }

        public final String getClientSecret() {
            return this.clientSecret;
        }
    }
}
