package org.signal.donations;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;

/* compiled from: StripeError.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00060\u0001j\u0002`\u0002:\u0002\u0006\u0007B\u000f\b\u0004\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005\u0001\u0002\b\t¨\u0006\n"}, d2 = {"Lorg/signal/donations/StripeError;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "message", "", "(Ljava/lang/String;)V", "FailedToParsePaymentMethodResponseError", "PostError", "Lorg/signal/donations/StripeError$FailedToParsePaymentMethodResponseError;", "Lorg/signal/donations/StripeError$PostError;", "donations_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public abstract class StripeError extends Exception {
    public /* synthetic */ StripeError(String str, DefaultConstructorMarker defaultConstructorMarker) {
        this(str);
    }

    /* compiled from: StripeError.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/signal/donations/StripeError$FailedToParsePaymentMethodResponseError;", "Lorg/signal/donations/StripeError;", "()V", "donations_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class FailedToParsePaymentMethodResponseError extends StripeError {
        public static final FailedToParsePaymentMethodResponseError INSTANCE = new FailedToParsePaymentMethodResponseError();

        private FailedToParsePaymentMethodResponseError() {
            super("Failed to parse payment method response", null);
        }
    }

    private StripeError(String str) {
        super(str);
    }

    /* compiled from: StripeError.kt */
    @Metadata(bv = {}, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\u0003\u001a\u00020\u0002\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\r\u001a\u0004\u0018\u00010\f¢\u0006\u0004\b\u0011\u0010\u0012R\u0017\u0010\u0003\u001a\u00020\u00028\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u0004\u0018\u00010\u00078\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u0004\u0018\u00010\f8\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u0013"}, d2 = {"Lorg/signal/donations/StripeError$PostError;", "Lorg/signal/donations/StripeError;", "", "statusCode", "I", "getStatusCode", "()I", "", "errorCode", "Ljava/lang/String;", "getErrorCode", "()Ljava/lang/String;", "Lorg/signal/donations/StripeDeclineCode;", "declineCode", "Lorg/signal/donations/StripeDeclineCode;", "getDeclineCode", "()Lorg/signal/donations/StripeDeclineCode;", "<init>", "(ILjava/lang/String;Lorg/signal/donations/StripeDeclineCode;)V", "donations_release"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes3.dex */
    public static final class PostError extends StripeError {
        private final StripeDeclineCode declineCode;
        private final String errorCode;
        private final int statusCode;

        public PostError(int i, String str, StripeDeclineCode stripeDeclineCode) {
            super("postForm failed with code: " + i + ". errorCode: " + str + ". declineCode: " + stripeDeclineCode, null);
            this.statusCode = i;
            this.errorCode = str;
            this.declineCode = stripeDeclineCode;
        }

        public final StripeDeclineCode getDeclineCode() {
            return this.declineCode;
        }

        public final String getErrorCode() {
            return this.errorCode;
        }
    }
}
