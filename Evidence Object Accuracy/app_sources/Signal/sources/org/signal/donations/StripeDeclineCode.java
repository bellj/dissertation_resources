package org.signal.donations;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: StripeDeclineCode.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u00062\u00020\u0001:\u0004\u0005\u0006\u0007\bB\u0007\b\u0004¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u0001\u0002\t\n¨\u0006\u000b"}, d2 = {"Lorg/signal/donations/StripeDeclineCode;", "", "()V", "isKnown", "", "Code", "Companion", "Known", "Unknown", "Lorg/signal/donations/StripeDeclineCode$Known;", "Lorg/signal/donations/StripeDeclineCode$Unknown;", "donations_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public abstract class StripeDeclineCode {
    public static final Companion Companion = new Companion(null);

    public /* synthetic */ StripeDeclineCode(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    private StripeDeclineCode() {
    }

    /* compiled from: StripeDeclineCode.kt */
    @Metadata(bv = {}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u000b\u001a\u00020\n¢\u0006\u0004\b\u000f\u0010\u0010J\t\u0010\u0003\u001a\u00020\u0002HÖ\u0001J\t\u0010\u0005\u001a\u00020\u0004HÖ\u0001J\u0013\u0010\t\u001a\u00020\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÖ\u0003R\u0017\u0010\u000b\u001a\u00020\n8\u0006¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lorg/signal/donations/StripeDeclineCode$Known;", "Lorg/signal/donations/StripeDeclineCode;", "", "toString", "", "hashCode", "", "other", "", "equals", "Lorg/signal/donations/StripeDeclineCode$Code;", "code", "Lorg/signal/donations/StripeDeclineCode$Code;", "getCode", "()Lorg/signal/donations/StripeDeclineCode$Code;", "<init>", "(Lorg/signal/donations/StripeDeclineCode$Code;)V", "donations_release"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes3.dex */
    public static final class Known extends StripeDeclineCode {
        private final Code code;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof Known) && this.code == ((Known) obj).code;
        }

        public int hashCode() {
            return this.code.hashCode();
        }

        public String toString() {
            return "Known(code=" + this.code + ')';
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Known(Code code) {
            super(null);
            Intrinsics.checkNotNullParameter(code, "code");
            this.code = code;
        }

        public final Code getCode() {
            return this.code;
        }
    }

    /* compiled from: StripeDeclineCode.kt */
    @Metadata(bv = {}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\b\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\n\u001a\u00020\u0002¢\u0006\u0004\b\u000e\u0010\u000fJ\t\u0010\u0003\u001a\u00020\u0002HÖ\u0001J\t\u0010\u0005\u001a\u00020\u0004HÖ\u0001J\u0013\u0010\t\u001a\u00020\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006HÖ\u0003R\u0017\u0010\n\u001a\u00020\u00028\u0006¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\r¨\u0006\u0010"}, d2 = {"Lorg/signal/donations/StripeDeclineCode$Unknown;", "Lorg/signal/donations/StripeDeclineCode;", "", "toString", "", "hashCode", "", "other", "", "equals", "code", "Ljava/lang/String;", "getCode", "()Ljava/lang/String;", "<init>", "(Ljava/lang/String;)V", "donations_release"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes3.dex */
    public static final class Unknown extends StripeDeclineCode {
        private final String code;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof Unknown) && Intrinsics.areEqual(this.code, ((Unknown) obj).code);
        }

        public int hashCode() {
            return this.code.hashCode();
        }

        public String toString() {
            return "Unknown(code=" + this.code + ')';
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Unknown(String str) {
            super(null);
            Intrinsics.checkNotNullParameter(str, "code");
            this.code = str;
        }
    }

    public final boolean isKnown() {
        return this instanceof Known;
    }

    /* compiled from: StripeDeclineCode.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b+\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011j\u0002\b\u0012j\u0002\b\u0013j\u0002\b\u0014j\u0002\b\u0015j\u0002\b\u0016j\u0002\b\u0017j\u0002\b\u0018j\u0002\b\u0019j\u0002\b\u001aj\u0002\b\u001bj\u0002\b\u001cj\u0002\b\u001dj\u0002\b\u001ej\u0002\b\u001fj\u0002\b j\u0002\b!j\u0002\b\"j\u0002\b#j\u0002\b$j\u0002\b%j\u0002\b&j\u0002\b'j\u0002\b(j\u0002\b)j\u0002\b*j\u0002\b+j\u0002\b,j\u0002\b-¨\u0006."}, d2 = {"Lorg/signal/donations/StripeDeclineCode$Code;", "", "code", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getCode", "()Ljava/lang/String;", "AUTHENTICATION_REQUIRED", "APPROVE_WITH_ID", "CALL_ISSUER", "CARD_NOT_SUPPORTED", "CARD_VELOCITY_EXCEEDED", "CURRENCY_NOT_SUPPORTED", "DO_NOT_HONOR", "DO_NOT_TRY_AGAIN", "DUPLICATE_TRANSACTION", "EXPIRED_CARD", "FRAUDULENT", "GENERIC_DECLINE", "INCORRECT_NUMBER", "INCORRECT_CVC", "INSUFFICIENT_FUNDS", "INVALID_ACCOUNT", "INVALID_AMOUNT", "INVALID_CVC", "INVALID_EXPIRY_MONTH", "INVALID_EXPIRY_YEAR", "INVALID_NUMBER", "ISSUER_NOT_AVAILABLE", "LOST_CARD", "MERCHANT_BLACKLIST", "NEW_ACCOUNT_INFORMATION_AVAILABLE", "NO_ACTION_TAKEN", "NOT_PERMITTED", "PROCESSING_ERROR", "REENTER_TRANSACTION", "RESTRICTED_CARD", "REVOCATION_OF_ALL_AUTHORIZATIONS", "REVOCATION_OF_AUTHORIZATION", "SECURITY_VIOLATION", "SERVICE_NOT_ALLOWED", "STOLEN_CARD", "STOP_PAYMENT_ORDER", "TRANSACTION_NOT_ALLOWED", "TRY_AGAIN_LATER", "WITHDRAWAL_COUNT_LIMIT_EXCEEDED", "donations_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public enum Code {
        AUTHENTICATION_REQUIRED("authentication_required"),
        APPROVE_WITH_ID("approve_with_id"),
        CALL_ISSUER("call_issuer"),
        CARD_NOT_SUPPORTED("card_not_supported"),
        CARD_VELOCITY_EXCEEDED("card_velocity_exceeded"),
        CURRENCY_NOT_SUPPORTED("currency_not_supported"),
        DO_NOT_HONOR("do_not_honor"),
        DO_NOT_TRY_AGAIN("do_not_try_again"),
        DUPLICATE_TRANSACTION("duplicate_transaction"),
        EXPIRED_CARD("expired_card"),
        FRAUDULENT("fraudulent"),
        GENERIC_DECLINE("generic_decline"),
        INCORRECT_NUMBER("incorrect_number"),
        INCORRECT_CVC("incorrect_cvc"),
        INSUFFICIENT_FUNDS("insufficient_funds"),
        INVALID_ACCOUNT("invalid_account"),
        INVALID_AMOUNT("invalid_amount"),
        INVALID_CVC("invalid_cvc"),
        INVALID_EXPIRY_MONTH("invalid_expiry_month"),
        INVALID_EXPIRY_YEAR("invalid_expiry_year"),
        INVALID_NUMBER("invalid_number"),
        ISSUER_NOT_AVAILABLE("issuer_not_available"),
        LOST_CARD("lost_card"),
        MERCHANT_BLACKLIST("merchant_blacklist"),
        NEW_ACCOUNT_INFORMATION_AVAILABLE("new_account_information_available"),
        NO_ACTION_TAKEN("no_action_taken"),
        NOT_PERMITTED("not_permitted"),
        PROCESSING_ERROR("processing_error"),
        REENTER_TRANSACTION("reenter_transaction"),
        RESTRICTED_CARD("restricted_card"),
        REVOCATION_OF_ALL_AUTHORIZATIONS("revocation_of_all_authorizations"),
        REVOCATION_OF_AUTHORIZATION("revocation_of_authorization"),
        SECURITY_VIOLATION("security_violation"),
        SERVICE_NOT_ALLOWED("service_not_allowed"),
        STOLEN_CARD("stolen_card"),
        STOP_PAYMENT_ORDER("stop_payment_order"),
        TRANSACTION_NOT_ALLOWED("transaction_not_allowed"),
        TRY_AGAIN_LATER("try_again_later"),
        WITHDRAWAL_COUNT_LIMIT_EXCEEDED("withdrawal_count_limit_exceeded");
        
        private final String code;

        Code(String str) {
            this.code = str;
        }

        public final String getCode() {
            return this.code;
        }
    }

    /* compiled from: StripeDeclineCode.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006¨\u0006\u0007"}, d2 = {"Lorg/signal/donations/StripeDeclineCode$Companion;", "", "()V", "getFromCode", "Lorg/signal/donations/StripeDeclineCode;", "code", "", "donations_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final StripeDeclineCode getFromCode(String str) {
            Code code;
            if (str == null) {
                return new Unknown("null");
            }
            Code[] values = Code.values();
            int i = 0;
            int length = values.length;
            while (true) {
                if (i >= length) {
                    code = null;
                    break;
                }
                code = values[i];
                if (Intrinsics.areEqual(code.getCode(), str)) {
                    break;
                }
                i++;
            }
            return code != null ? new Known(code) : new Unknown(str);
        }
    }
}
