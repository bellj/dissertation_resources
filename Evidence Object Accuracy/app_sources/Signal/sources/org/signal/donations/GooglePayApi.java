package org.signal.donations;

import android.app.Activity;
import android.content.Intent;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.gms.wallet.PaymentDataRequest;
import com.google.android.gms.wallet.PaymentsClient;
import com.google.android.gms.wallet.Wallet;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.json.JSONArray;
import org.json.JSONObject;
import org.signal.core.util.logging.Log;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.database.EmojiSearchDatabase;

/* compiled from: GooglePayApi.kt */
@Metadata(bv = {}, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 $2\u00020\u0001:\u0005$%&'(B\u001f\u0012\u0006\u0010\u0018\u001a\u00020\u0017\u0012\u0006\u0010\u001b\u001a\u00020\u001a\u0012\u0006\u0010!\u001a\u00020 ¢\u0006\u0004\b\"\u0010#J\u0018\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002J\u0018\u0010\b\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002J\b\u0010\t\u001a\u00020\u0006H\u0002J\b\u0010\n\u001a\u00020\u0006H\u0002J\b\u0010\u000b\u001a\u00020\u0006H\u0002J\u001e\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\fJ0\u0010\u0016\u001a\u00020\u000e2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\f2\b\u0010\u0012\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0013\u001a\u00020\f2\u0006\u0010\u0015\u001a\u00020\u0014R\u0014\u0010\u0018\u001a\u00020\u00178\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0018\u0010\u0019R\u0014\u0010\u001b\u001a\u00020\u001a8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u0014\u0010\u001e\u001a\u00020\u001d8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001f¨\u0006)"}, d2 = {"Lorg/signal/donations/GooglePayApi;", "", "Lorg/signal/core/util/money/FiatMoney;", "price", "", EmojiSearchDatabase.LABEL, "Lorg/json/JSONObject;", "getPaymentDataRequest", "getTransactionInfo", "gatewayTokenizationSpecification", "baseCardPaymentMethod", "cardPaymentMethod", "", "requestCode", "", "requestPayment", "resultCode", "Landroid/content/Intent;", "data", "expectedRequestCode", "Lorg/signal/donations/GooglePayApi$PaymentRequestCallback;", "paymentRequestCallback", "onActivityResult", "Landroid/app/Activity;", "activity", "Landroid/app/Activity;", "Lorg/signal/donations/GooglePayApi$Gateway;", "gateway", "Lorg/signal/donations/GooglePayApi$Gateway;", "Lcom/google/android/gms/wallet/PaymentsClient;", "paymentsClient", "Lcom/google/android/gms/wallet/PaymentsClient;", "Lorg/signal/donations/GooglePayApi$Configuration;", "configuration", "<init>", "(Landroid/app/Activity;Lorg/signal/donations/GooglePayApi$Gateway;Lorg/signal/donations/GooglePayApi$Configuration;)V", "Companion", "Configuration", "Gateway", "GooglePayException", "PaymentRequestCallback", "donations_release"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes3.dex */
public final class GooglePayApi {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(GooglePayApi.class);
    private static final JSONArray allowedCardAuthMethods = new JSONArray((Collection) CollectionsKt__CollectionsKt.listOf((Object[]) new String[]{"PAN_ONLY", "CRYPTOGRAM_3DS"}));
    private static final JSONObject baseRequest;
    private static final JSONObject merchantInfo;
    private final Activity activity;
    private final Gateway gateway;
    private final PaymentsClient paymentsClient;

    /* compiled from: GooglePayApi.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010$\n\u0000\bf\u0018\u00002\u00020\u0001J\u0014\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\bH&R\u0018\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006¨\u0006\t"}, d2 = {"Lorg/signal/donations/GooglePayApi$Gateway;", "", "allowedCardNetworks", "", "", "getAllowedCardNetworks", "()Ljava/util/List;", "getTokenizationSpecificationParameters", "", "donations_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public interface Gateway {
        List<String> getAllowedCardNetworks();

        Map<String, String> getTokenizationSpecificationParameters();
    }

    /* compiled from: GooglePayApi.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\tH&¨\u0006\n"}, d2 = {"Lorg/signal/donations/GooglePayApi$PaymentRequestCallback;", "", "onCancelled", "", "onError", "googlePayException", "Lorg/signal/donations/GooglePayApi$GooglePayException;", "onSuccess", "paymentData", "Lcom/google/android/gms/wallet/PaymentData;", "donations_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public interface PaymentRequestCallback {
        void onCancelled();

        void onError(GooglePayException googlePayException);

        void onSuccess(PaymentData paymentData);
    }

    public GooglePayApi(Activity activity, Gateway gateway, Configuration configuration) {
        Intrinsics.checkNotNullParameter(activity, "activity");
        Intrinsics.checkNotNullParameter(gateway, "gateway");
        Intrinsics.checkNotNullParameter(configuration, "configuration");
        this.activity = activity;
        this.gateway = gateway;
        PaymentsClient paymentsClient = Wallet.getPaymentsClient(activity, new Wallet.WalletOptions.Builder().setEnvironment(configuration.getWalletEnvironment()).build());
        Intrinsics.checkNotNullExpressionValue(paymentsClient, "getPaymentsClient(activity, walletOptions)");
        this.paymentsClient = paymentsClient;
    }

    public final void requestPayment(FiatMoney fiatMoney, String str, int i) {
        Intrinsics.checkNotNullParameter(fiatMoney, "price");
        Intrinsics.checkNotNullParameter(str, EmojiSearchDatabase.LABEL);
        AutoResolveHelper.resolveTask(this.paymentsClient.loadPaymentData(PaymentDataRequest.fromJson(getPaymentDataRequest(fiatMoney, str).toString())), this.activity, i);
    }

    public final void onActivityResult(int i, int i2, Intent intent, int i3, PaymentRequestCallback paymentRequestCallback) {
        PaymentData fromIntent;
        Status statusFromIntent;
        Intrinsics.checkNotNullParameter(paymentRequestCallback, "paymentRequestCallback");
        if (i == i3) {
            if (i2 == -1) {
                Unit unit = null;
                if (!(intent == null || (fromIntent = PaymentData.getFromIntent(intent)) == null)) {
                    Intrinsics.checkNotNullExpressionValue(fromIntent, "getFromIntent(intent)");
                    paymentRequestCallback.onSuccess(fromIntent);
                    unit = Unit.INSTANCE;
                }
                if (unit == null) {
                    paymentRequestCallback.onError(new GooglePayException(-1, "No data returned from Google Pay"));
                }
            } else if (i2 == 0) {
                paymentRequestCallback.onCancelled();
            } else if (i2 == 1 && (statusFromIntent = AutoResolveHelper.getStatusFromIntent(intent)) != null) {
                String str = TAG;
                Log.w(str, "loadPaymentData failed with error code " + statusFromIntent.getStatusCode() + ": " + statusFromIntent.getStatusMessage());
                paymentRequestCallback.onError(new GooglePayException(statusFromIntent.getStatusCode(), statusFromIntent.getStatusMessage()));
            }
        }
    }

    private final JSONObject getPaymentDataRequest(FiatMoney fiatMoney, String str) {
        JSONObject jSONObject = baseRequest;
        jSONObject.put("merchantInfo", merchantInfo);
        jSONObject.put("allowedPaymentMethods", new JSONArray().put(cardPaymentMethod()));
        jSONObject.put("transactionInfo", getTransactionInfo(fiatMoney, str));
        jSONObject.put("emailRequired", false);
        jSONObject.put("shippingAddressRequired", false);
        return jSONObject;
    }

    private final JSONObject getTransactionInfo(FiatMoney fiatMoney, String str) {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("currencyCode", fiatMoney.getCurrency().getCurrencyCode());
        jSONObject.put("countryCode", "US");
        jSONObject.put("totalPriceStatus", "FINAL");
        jSONObject.put("totalPrice", fiatMoney.getDefaultPrecisionString(Locale.US));
        jSONObject.put("totalPriceLabel", str);
        jSONObject.put("checkoutOption", "COMPLETE_IMMEDIATE_PURCHASE");
        return jSONObject;
    }

    private final JSONObject gatewayTokenizationSpecification() {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("type", "PAYMENT_GATEWAY");
        jSONObject.put("parameters", new JSONObject(this.gateway.getTokenizationSpecificationParameters()));
        return jSONObject;
    }

    private final JSONObject baseCardPaymentMethod() {
        JSONObject jSONObject = new JSONObject();
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put("allowedAuthMethods", allowedCardAuthMethods);
        jSONObject2.put("allowedCardNetworks", new JSONArray((Collection) this.gateway.getAllowedCardNetworks()));
        jSONObject2.put("billingAddressRequired", false);
        jSONObject.put("type", "CARD");
        jSONObject.put("parameters", jSONObject2);
        return jSONObject;
    }

    private final JSONObject cardPaymentMethod() {
        JSONObject baseCardPaymentMethod = baseCardPaymentMethod();
        baseCardPaymentMethod.put("tokenizationSpecification", gatewayTokenizationSpecification());
        return baseCardPaymentMethod;
    }

    /* compiled from: GooglePayApi.kt */
    @Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0006*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/signal/donations/GooglePayApi$Companion;", "", "()V", "MERCHANT_NAME", "", "TAG", "kotlin.jvm.PlatformType", "allowedCardAuthMethods", "Lorg/json/JSONArray;", "baseRequest", "Lorg/json/JSONObject;", "merchantInfo", "donations_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    static {
        Companion = new Companion(null);
        TAG = Log.tag(GooglePayApi.class);
        JSONObject put = new JSONObject().put("merchantName", "Signal");
        Intrinsics.checkNotNullExpressionValue(put, "JSONObject().put(\"merchantName\", MERCHANT_NAME)");
        merchantInfo = put;
        allowedCardAuthMethods = new JSONArray((Collection) CollectionsKt__CollectionsKt.listOf((Object[]) new String[]{"PAN_ONLY", "CRYPTOGRAM_3DS"}));
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("apiVersion", 2);
        jSONObject.put("apiVersionMinor", 0);
        baseRequest = jSONObject;
    }

    /* compiled from: GooglePayApi.kt */
    @Metadata(bv = {}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\b\b\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\t\u001a\u00020\u0004¢\u0006\u0004\b\r\u0010\u000eJ\t\u0010\u0003\u001a\u00020\u0002HÖ\u0001J\t\u0010\u0005\u001a\u00020\u0004HÖ\u0001J\u0013\u0010\b\u001a\u00020\u00072\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001HÖ\u0003R\u0017\u0010\t\u001a\u00020\u00048\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\f¨\u0006\u000f"}, d2 = {"Lorg/signal/donations/GooglePayApi$Configuration;", "", "", "toString", "", "hashCode", "other", "", "equals", "walletEnvironment", "I", "getWalletEnvironment", "()I", "<init>", "(I)V", "donations_release"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes3.dex */
    public static final class Configuration {
        private final int walletEnvironment;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof Configuration) && this.walletEnvironment == ((Configuration) obj).walletEnvironment;
        }

        public int hashCode() {
            return this.walletEnvironment;
        }

        public String toString() {
            return "Configuration(walletEnvironment=" + this.walletEnvironment + ')';
        }

        public Configuration(int i) {
            this.walletEnvironment = i;
        }

        public final int getWalletEnvironment() {
            return this.walletEnvironment;
        }
    }

    /* compiled from: GooglePayApi.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00060\u0001j\u0002`\u0002B\u0017\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\u0007¨\u0006\b"}, d2 = {"Lorg/signal/donations/GooglePayApi$GooglePayException;", "Ljava/lang/Exception;", "Lkotlin/Exception;", "code", "", "message", "", "(ILjava/lang/String;)V", "donations_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class GooglePayException extends Exception {
        public GooglePayException(int i, String str) {
            super(str);
        }
    }
}
