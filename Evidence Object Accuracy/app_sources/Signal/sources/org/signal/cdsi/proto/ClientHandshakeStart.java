package org.signal.cdsi.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class ClientHandshakeStart extends GeneratedMessageLite<ClientHandshakeStart, Builder> implements MessageLiteOrBuilder {
    private static final ClientHandshakeStart DEFAULT_INSTANCE;
    public static final int ENDORSEMENT_FIELD_NUMBER;
    public static final int EVIDENCE_FIELD_NUMBER;
    private static volatile Parser<ClientHandshakeStart> PARSER;
    public static final int PUBKEY_FIELD_NUMBER;
    private ByteString endorsement_;
    private ByteString evidence_;
    private ByteString pubkey_;

    private ClientHandshakeStart() {
        ByteString byteString = ByteString.EMPTY;
        this.pubkey_ = byteString;
        this.evidence_ = byteString;
        this.endorsement_ = byteString;
    }

    public ByteString getPubkey() {
        return this.pubkey_;
    }

    public void setPubkey(ByteString byteString) {
        byteString.getClass();
        this.pubkey_ = byteString;
    }

    public void clearPubkey() {
        this.pubkey_ = getDefaultInstance().getPubkey();
    }

    public ByteString getEvidence() {
        return this.evidence_;
    }

    public void setEvidence(ByteString byteString) {
        byteString.getClass();
        this.evidence_ = byteString;
    }

    public void clearEvidence() {
        this.evidence_ = getDefaultInstance().getEvidence();
    }

    public ByteString getEndorsement() {
        return this.endorsement_;
    }

    public void setEndorsement(ByteString byteString) {
        byteString.getClass();
        this.endorsement_ = byteString;
    }

    public void clearEndorsement() {
        this.endorsement_ = getDefaultInstance().getEndorsement();
    }

    public static ClientHandshakeStart parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ClientHandshakeStart) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ClientHandshakeStart parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ClientHandshakeStart) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ClientHandshakeStart parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ClientHandshakeStart) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ClientHandshakeStart parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ClientHandshakeStart) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ClientHandshakeStart parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ClientHandshakeStart) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ClientHandshakeStart parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ClientHandshakeStart) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ClientHandshakeStart parseFrom(InputStream inputStream) throws IOException {
        return (ClientHandshakeStart) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ClientHandshakeStart parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ClientHandshakeStart) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ClientHandshakeStart parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ClientHandshakeStart) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ClientHandshakeStart parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ClientHandshakeStart) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ClientHandshakeStart parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ClientHandshakeStart) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ClientHandshakeStart parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ClientHandshakeStart) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ClientHandshakeStart clientHandshakeStart) {
        return DEFAULT_INSTANCE.createBuilder(clientHandshakeStart);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ClientHandshakeStart, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(ClientHandshakeStart.DEFAULT_INSTANCE);
        }
    }

    /* renamed from: org.signal.cdsi.proto.ClientHandshakeStart$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ClientHandshakeStart();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\n\u0002\n\u0003\n", new Object[]{"pubkey_", "evidence_", "endorsement_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ClientHandshakeStart> parser = PARSER;
                if (parser == null) {
                    synchronized (ClientHandshakeStart.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ClientHandshakeStart clientHandshakeStart = new ClientHandshakeStart();
        DEFAULT_INSTANCE = clientHandshakeStart;
        GeneratedMessageLite.registerDefaultInstance(ClientHandshakeStart.class, clientHandshakeStart);
    }

    public static ClientHandshakeStart getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ClientHandshakeStart> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
