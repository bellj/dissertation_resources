package org.signal.cdsi.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class ClientResponse extends GeneratedMessageLite<ClientResponse, Builder> implements MessageLiteOrBuilder {
    private static final ClientResponse DEFAULT_INSTANCE;
    public static final int E164_PNI_ACI_TRIPLES_FIELD_NUMBER;
    private static volatile Parser<ClientResponse> PARSER;
    public static final int RETRY_AFTER_SECS_FIELD_NUMBER;
    public static final int TOKEN_FIELD_NUMBER;
    private ByteString e164PniAciTriples_;
    private int retryAfterSecs_;
    private ByteString token_;

    private ClientResponse() {
        ByteString byteString = ByteString.EMPTY;
        this.e164PniAciTriples_ = byteString;
        this.token_ = byteString;
    }

    public ByteString getE164PniAciTriples() {
        return this.e164PniAciTriples_;
    }

    public void setE164PniAciTriples(ByteString byteString) {
        byteString.getClass();
        this.e164PniAciTriples_ = byteString;
    }

    public void clearE164PniAciTriples() {
        this.e164PniAciTriples_ = getDefaultInstance().getE164PniAciTriples();
    }

    public int getRetryAfterSecs() {
        return this.retryAfterSecs_;
    }

    public void setRetryAfterSecs(int i) {
        this.retryAfterSecs_ = i;
    }

    public void clearRetryAfterSecs() {
        this.retryAfterSecs_ = 0;
    }

    public ByteString getToken() {
        return this.token_;
    }

    public void setToken(ByteString byteString) {
        byteString.getClass();
        this.token_ = byteString;
    }

    public void clearToken() {
        this.token_ = getDefaultInstance().getToken();
    }

    public static ClientResponse parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ClientResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ClientResponse parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ClientResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ClientResponse parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ClientResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ClientResponse parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ClientResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ClientResponse parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ClientResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ClientResponse parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ClientResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ClientResponse parseFrom(InputStream inputStream) throws IOException {
        return (ClientResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ClientResponse parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ClientResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ClientResponse parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ClientResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ClientResponse parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ClientResponse) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ClientResponse parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ClientResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ClientResponse parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ClientResponse) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ClientResponse clientResponse) {
        return DEFAULT_INSTANCE.createBuilder(clientResponse);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ClientResponse, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(ClientResponse.DEFAULT_INSTANCE);
        }
    }

    /* renamed from: org.signal.cdsi.proto.ClientResponse$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ClientResponse();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\n\u0002\u0004\u0003\n", new Object[]{"e164PniAciTriples_", "retryAfterSecs_", "token_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ClientResponse> parser = PARSER;
                if (parser == null) {
                    synchronized (ClientResponse.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ClientResponse clientResponse = new ClientResponse();
        DEFAULT_INSTANCE = clientResponse;
        GeneratedMessageLite.registerDefaultInstance(ClientResponse.class, clientResponse);
    }

    public static ClientResponse getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ClientResponse> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
