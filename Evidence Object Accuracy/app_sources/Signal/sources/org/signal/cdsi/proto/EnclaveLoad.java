package org.signal.cdsi.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class EnclaveLoad extends GeneratedMessageLite<EnclaveLoad, Builder> implements MessageLiteOrBuilder {
    public static final int CLEAR_ALL_FIELD_NUMBER;
    private static final EnclaveLoad DEFAULT_INSTANCE;
    public static final int E164_ACI_PNI_UAK_TUPLES_FIELD_NUMBER;
    private static volatile Parser<EnclaveLoad> PARSER;
    public static final int SHARED_TOKEN_SECRET_FIELD_NUMBER;
    private boolean clearAll_;
    private ByteString e164AciPniUakTuples_;
    private ByteString sharedTokenSecret_;

    private EnclaveLoad() {
        ByteString byteString = ByteString.EMPTY;
        this.e164AciPniUakTuples_ = byteString;
        this.sharedTokenSecret_ = byteString;
    }

    public boolean getClearAll() {
        return this.clearAll_;
    }

    public void setClearAll(boolean z) {
        this.clearAll_ = z;
    }

    public void clearClearAll() {
        this.clearAll_ = false;
    }

    public ByteString getE164AciPniUakTuples() {
        return this.e164AciPniUakTuples_;
    }

    public void setE164AciPniUakTuples(ByteString byteString) {
        byteString.getClass();
        this.e164AciPniUakTuples_ = byteString;
    }

    public void clearE164AciPniUakTuples() {
        this.e164AciPniUakTuples_ = getDefaultInstance().getE164AciPniUakTuples();
    }

    public ByteString getSharedTokenSecret() {
        return this.sharedTokenSecret_;
    }

    public void setSharedTokenSecret(ByteString byteString) {
        byteString.getClass();
        this.sharedTokenSecret_ = byteString;
    }

    public void clearSharedTokenSecret() {
        this.sharedTokenSecret_ = getDefaultInstance().getSharedTokenSecret();
    }

    public static EnclaveLoad parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (EnclaveLoad) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static EnclaveLoad parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (EnclaveLoad) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static EnclaveLoad parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (EnclaveLoad) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static EnclaveLoad parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (EnclaveLoad) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static EnclaveLoad parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (EnclaveLoad) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static EnclaveLoad parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (EnclaveLoad) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static EnclaveLoad parseFrom(InputStream inputStream) throws IOException {
        return (EnclaveLoad) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static EnclaveLoad parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (EnclaveLoad) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static EnclaveLoad parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (EnclaveLoad) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static EnclaveLoad parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (EnclaveLoad) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static EnclaveLoad parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (EnclaveLoad) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static EnclaveLoad parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (EnclaveLoad) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(EnclaveLoad enclaveLoad) {
        return DEFAULT_INSTANCE.createBuilder(enclaveLoad);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<EnclaveLoad, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(EnclaveLoad.DEFAULT_INSTANCE);
        }
    }

    /* renamed from: org.signal.cdsi.proto.EnclaveLoad$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new EnclaveLoad();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0003\u0000\u0000\u0001\u0003\u0003\u0000\u0000\u0000\u0001\u0007\u0002\n\u0003\n", new Object[]{"clearAll_", "e164AciPniUakTuples_", "sharedTokenSecret_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<EnclaveLoad> parser = PARSER;
                if (parser == null) {
                    synchronized (EnclaveLoad.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        EnclaveLoad enclaveLoad = new EnclaveLoad();
        DEFAULT_INSTANCE = enclaveLoad;
        GeneratedMessageLite.registerDefaultInstance(EnclaveLoad.class, enclaveLoad);
    }

    public static EnclaveLoad getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<EnclaveLoad> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
