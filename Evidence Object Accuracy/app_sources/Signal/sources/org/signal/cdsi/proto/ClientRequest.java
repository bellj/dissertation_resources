package org.signal.cdsi.proto;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class ClientRequest extends GeneratedMessageLite<ClientRequest, Builder> implements MessageLiteOrBuilder {
    public static final int ACI_UAK_PAIRS_FIELD_NUMBER;
    private static final ClientRequest DEFAULT_INSTANCE;
    public static final int DISCARD_E164S_FIELD_NUMBER;
    public static final int NEW_E164S_FIELD_NUMBER;
    private static volatile Parser<ClientRequest> PARSER;
    public static final int PREV_E164S_FIELD_NUMBER;
    public static final int TOKEN_ACK_FIELD_NUMBER;
    public static final int TOKEN_FIELD_NUMBER;
    private ByteString aciUakPairs_;
    private ByteString discardE164S_;
    private ByteString newE164S_;
    private ByteString prevE164S_;
    private boolean tokenAck_;
    private ByteString token_;

    private ClientRequest() {
        ByteString byteString = ByteString.EMPTY;
        this.aciUakPairs_ = byteString;
        this.prevE164S_ = byteString;
        this.newE164S_ = byteString;
        this.discardE164S_ = byteString;
        this.token_ = byteString;
    }

    public ByteString getAciUakPairs() {
        return this.aciUakPairs_;
    }

    public void setAciUakPairs(ByteString byteString) {
        byteString.getClass();
        this.aciUakPairs_ = byteString;
    }

    public void clearAciUakPairs() {
        this.aciUakPairs_ = getDefaultInstance().getAciUakPairs();
    }

    public ByteString getPrevE164S() {
        return this.prevE164S_;
    }

    public void setPrevE164S(ByteString byteString) {
        byteString.getClass();
        this.prevE164S_ = byteString;
    }

    public void clearPrevE164S() {
        this.prevE164S_ = getDefaultInstance().getPrevE164S();
    }

    public ByteString getNewE164S() {
        return this.newE164S_;
    }

    public void setNewE164S(ByteString byteString) {
        byteString.getClass();
        this.newE164S_ = byteString;
    }

    public void clearNewE164S() {
        this.newE164S_ = getDefaultInstance().getNewE164S();
    }

    public ByteString getDiscardE164S() {
        return this.discardE164S_;
    }

    public void setDiscardE164S(ByteString byteString) {
        byteString.getClass();
        this.discardE164S_ = byteString;
    }

    public void clearDiscardE164S() {
        this.discardE164S_ = getDefaultInstance().getDiscardE164S();
    }

    public ByteString getToken() {
        return this.token_;
    }

    public void setToken(ByteString byteString) {
        byteString.getClass();
        this.token_ = byteString;
    }

    public void clearToken() {
        this.token_ = getDefaultInstance().getToken();
    }

    public boolean getTokenAck() {
        return this.tokenAck_;
    }

    public void setTokenAck(boolean z) {
        this.tokenAck_ = z;
    }

    public void clearTokenAck() {
        this.tokenAck_ = false;
    }

    public static ClientRequest parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ClientRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ClientRequest parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ClientRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ClientRequest parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ClientRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ClientRequest parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ClientRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ClientRequest parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ClientRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ClientRequest parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ClientRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ClientRequest parseFrom(InputStream inputStream) throws IOException {
        return (ClientRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ClientRequest parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ClientRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ClientRequest parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ClientRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ClientRequest parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ClientRequest) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ClientRequest parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ClientRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ClientRequest parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ClientRequest) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ClientRequest clientRequest) {
        return DEFAULT_INSTANCE.createBuilder(clientRequest);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ClientRequest, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(ClientRequest.DEFAULT_INSTANCE);
        }

        public Builder setAciUakPairs(ByteString byteString) {
            copyOnWrite();
            ((ClientRequest) this.instance).setAciUakPairs(byteString);
            return this;
        }

        public Builder setPrevE164S(ByteString byteString) {
            copyOnWrite();
            ((ClientRequest) this.instance).setPrevE164S(byteString);
            return this;
        }

        public Builder setNewE164S(ByteString byteString) {
            copyOnWrite();
            ((ClientRequest) this.instance).setNewE164S(byteString);
            return this;
        }

        public Builder setDiscardE164S(ByteString byteString) {
            copyOnWrite();
            ((ClientRequest) this.instance).setDiscardE164S(byteString);
            return this;
        }

        public Builder setToken(ByteString byteString) {
            copyOnWrite();
            ((ClientRequest) this.instance).setToken(byteString);
            return this;
        }

        public Builder setTokenAck(boolean z) {
            copyOnWrite();
            ((ClientRequest) this.instance).setTokenAck(z);
            return this;
        }
    }

    /* renamed from: org.signal.cdsi.proto.ClientRequest$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ClientRequest();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0006\u0000\u0000\u0001\u0007\u0006\u0000\u0000\u0000\u0001\n\u0002\n\u0003\n\u0004\n\u0006\n\u0007\u0007", new Object[]{"aciUakPairs_", "prevE164S_", "newE164S_", "discardE164S_", "token_", "tokenAck_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ClientRequest> parser = PARSER;
                if (parser == null) {
                    synchronized (ClientRequest.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ClientRequest clientRequest = new ClientRequest();
        DEFAULT_INSTANCE = clientRequest;
        GeneratedMessageLite.registerDefaultInstance(ClientRequest.class, clientRequest);
    }

    public static ClientRequest getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ClientRequest> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
