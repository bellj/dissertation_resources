package org.signal.argon2;

/* loaded from: classes3.dex */
public enum Version {
    V10(16),
    V13(19),
    LATEST(19);
    
    final int nativeValue;

    Version(int i) {
        this.nativeValue = i;
    }
}
