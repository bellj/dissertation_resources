package org.signal.argon2;

import androidx.recyclerview.widget.RecyclerView;
import java.util.Locale;

/* loaded from: classes3.dex */
public final class Argon2 {
    private final int hashLength;
    private final int mCostKiB;
    private final int parallelism;
    private final int tCostIterations;
    private final Type type;
    private final Version version;

    private Argon2(Builder builder) {
        this.tCostIterations = builder.tCostIterations;
        this.mCostKiB = builder.mCostKiB;
        this.parallelism = builder.parallelism;
        this.hashLength = builder.hashLength;
        this.type = builder.type;
        this.version = builder.version;
    }

    public static boolean verify(String str, byte[] bArr) throws UnknownTypeException {
        return verify(str, bArr, Type.fromEncoded(str));
    }

    public static boolean verify(String str, byte[] bArr, Type type) {
        if (str == null) {
            throw new IllegalArgumentException();
        } else if (bArr != null) {
            return Argon2Native.verify(str, (byte[]) bArr.clone(), type.nativeValue) == 0;
        } else {
            throw new IllegalArgumentException();
        }
    }

    /* loaded from: classes3.dex */
    public static class Builder {
        private int hashLength = 32;
        private int mCostKiB = RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT;
        private int parallelism = 1;
        private int tCostIterations = 3;
        private Type type = Type.Argon2i;
        private final Version version;

        public Builder(Version version) {
            this.version = version;
        }

        public Builder type(Type type) {
            this.type = type;
            return this;
        }

        public Builder parallelism(int i) {
            this.parallelism = i;
            return this;
        }

        public Builder memoryCostKiB(int i) {
            if (i < 4) {
                throw new IllegalArgumentException("kib too small, minimum 4");
            } else if (i % 4 == 0) {
                this.mCostKiB = i;
                return this;
            } else {
                throw new IllegalArgumentException("kib must be multiple of 4");
            }
        }

        public Builder memoryCost(MemoryCost memoryCost) {
            return memoryCostKiB(memoryCost.getKiB());
        }

        public Builder iterations(int i) {
            this.tCostIterations = i;
            return this;
        }

        public Builder hashLength(int i) {
            this.hashLength = i;
            return this;
        }

        public Argon2 build() {
            return new Argon2(this);
        }
    }

    public Result hash(byte[] bArr, byte[] bArr2) throws Argon2Exception {
        if (bArr2 == null) {
            throw new IllegalArgumentException();
        } else if (bArr != null) {
            StringBuffer stringBuffer = new StringBuffer();
            byte[] bArr3 = new byte[this.hashLength];
            int hash = Argon2Native.hash(this.tCostIterations, this.mCostKiB, this.parallelism, (byte[]) bArr.clone(), (byte[]) bArr2.clone(), bArr3, stringBuffer, this.type.nativeValue, this.version.nativeValue);
            if (hash == 0) {
                return new Result(stringBuffer.toString(), bArr3);
            }
            throw new Argon2Exception(hash, Argon2Native.resultToString(hash));
        } else {
            throw new IllegalArgumentException();
        }
    }

    /* loaded from: classes3.dex */
    public final class Result {
        private final String encoded;
        private final byte[] hash;

        private Result(String str, byte[] bArr) {
            Argon2.this = r1;
            this.encoded = str;
            this.hash = bArr;
        }

        public String getEncoded() {
            return this.encoded;
        }

        public byte[] getHash() {
            return this.hash;
        }

        public String getHashHex() {
            return Argon2.toHex(this.hash);
        }

        public String toString() {
            return String.format(Locale.US, "Type:           %s%nIterations:     %d%nMemory:         %d KiB%nParallelism:    %d%nHash:           %s%nEncoded:        %s%n", Argon2.this.type, Integer.valueOf(Argon2.this.tCostIterations), Integer.valueOf(Argon2.this.mCostKiB), Integer.valueOf(Argon2.this.parallelism), getHashHex(), this.encoded);
        }
    }

    public static String toHex(byte[] bArr) {
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        for (byte b : bArr) {
            sb.append(String.format(Locale.US, "%02x", Byte.valueOf(b)));
        }
        return sb.toString();
    }
}
