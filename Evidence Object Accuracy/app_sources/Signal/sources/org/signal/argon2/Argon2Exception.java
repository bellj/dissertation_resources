package org.signal.argon2;

import java.util.Locale;

/* loaded from: classes3.dex */
public final class Argon2Exception extends Exception {
    Argon2Exception(String str) {
        super(str);
    }

    public Argon2Exception(int i, String str) {
        this(String.format(Locale.US, "Argon failed %d: %s", Integer.valueOf(i), str));
    }
}
