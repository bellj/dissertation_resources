package org.signal.argon2;

/* access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public final class Argon2Native {
    /* access modifiers changed from: package-private */
    public static native int hash(int i, int i2, int i3, byte[] bArr, byte[] bArr2, byte[] bArr3, StringBuffer stringBuffer, int i4, int i5);

    /* access modifiers changed from: package-private */
    public static native String resultToString(int i);

    /* access modifiers changed from: package-private */
    public static native int verify(String str, byte[] bArr, int i);

    static {
        System.loadLibrary("argon2");
    }
}
