package org.signal.argon2;

/* loaded from: classes3.dex */
public final class MemoryCost {
    private final int kib;

    public static MemoryCost KiB(int i) {
        return new MemoryCost(i);
    }

    public static MemoryCost MiB(int i) {
        return new MemoryCost(i * 1024);
    }

    private MemoryCost(int i) {
        this.kib = i;
    }

    public int getKiB() {
        return this.kib;
    }
}
