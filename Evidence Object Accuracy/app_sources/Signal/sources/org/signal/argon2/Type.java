package org.signal.argon2;

/* loaded from: classes3.dex */
public enum Type {
    Argon2d(0),
    Argon2i(1),
    Argon2id(2);
    
    final int nativeValue;

    Type(int i) {
        this.nativeValue = i;
    }

    public static Type fromEncoded(String str) throws UnknownTypeException {
        if (str == null) {
            throw new IllegalArgumentException();
        } else if (str.startsWith("$argon2id$")) {
            return Argon2id;
        } else {
            if (str.startsWith("$argon2i$")) {
                return Argon2i;
            }
            if (str.startsWith("$argon2d$")) {
                return Argon2d;
            }
            throw new UnknownTypeException();
        }
    }
}
