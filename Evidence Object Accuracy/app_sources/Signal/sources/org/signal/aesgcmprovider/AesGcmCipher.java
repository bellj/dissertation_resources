package org.signal.aesgcmprovider;

import java.nio.ByteBuffer;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidParameterSpecException;
import javax.crypto.BadPaddingException;
import javax.crypto.CipherSpi;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.GCMParameterSpec;

/* loaded from: classes3.dex */
public class AesGcmCipher extends CipherSpi {
    private GCMParameterSpec algorithmParameterSpec;
    private long cipherContext;
    private boolean encrypt;
    private ReserveBuffer reserveBuffer;

    private native void destroy(long j);

    private native boolean finishDecrypt(long j, byte[] bArr, int i);

    private native void finishEncrypt(long j, byte[] bArr, int i);

    private native long initializeCipher(boolean z, byte[] bArr, int i, byte[] bArr2);

    private native void update(long j, byte[] bArr, int i, int i2, byte[] bArr2, int i3, int i4);

    private native void updateAAD(long j, byte[] bArr, int i, int i2);

    @Override // javax.crypto.CipherSpi
    protected int engineGetBlockSize() {
        return 16;
    }

    @Override // javax.crypto.CipherSpi
    protected void engineSetMode(String str) throws NoSuchAlgorithmException {
        if (!"GCM".equals(str)) {
            throw new NoSuchAlgorithmException();
        }
    }

    @Override // javax.crypto.CipherSpi
    protected void engineSetPadding(String str) throws NoSuchPaddingException {
        if (!"NoPadding".equals(str)) {
            throw new NoSuchPaddingException();
        }
    }

    @Override // javax.crypto.CipherSpi
    protected int engineGetOutputSize(int i) {
        return i + (this.algorithmParameterSpec.getTLen() / 8);
    }

    @Override // javax.crypto.CipherSpi
    protected byte[] engineGetIV() {
        return this.algorithmParameterSpec.getIV();
    }

    @Override // javax.crypto.CipherSpi
    protected AlgorithmParameters engineGetParameters() {
        try {
            AlgorithmParameters instance = AlgorithmParameters.getInstance("GCM");
            instance.init(this.algorithmParameterSpec);
            return instance;
        } catch (NoSuchAlgorithmException | InvalidParameterSpecException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override // javax.crypto.CipherSpi
    protected void engineInit(int i, Key key, SecureRandom secureRandom) throws InvalidKeyException {
        try {
            engineInit(i, key, (AlgorithmParameterSpec) null, secureRandom);
        } catch (InvalidAlgorithmParameterException e) {
            throw new InvalidKeyException(e);
        }
    }

    @Override // javax.crypto.CipherSpi
    protected void engineInit(int i, Key key, AlgorithmParameters algorithmParameters, SecureRandom secureRandom) throws InvalidAlgorithmParameterException, InvalidKeyException {
        try {
            engineInit(i, key, algorithmParameters.getParameterSpec(GCMParameterSpec.class), secureRandom);
        } catch (InvalidParameterSpecException e) {
            throw new InvalidAlgorithmParameterException(e);
        }
    }

    @Override // javax.crypto.CipherSpi
    protected void engineInit(int i, Key key, AlgorithmParameterSpec algorithmParameterSpec, SecureRandom secureRandom) throws InvalidAlgorithmParameterException, InvalidKeyException {
        if (algorithmParameterSpec == null && i == 2) {
            throw new InvalidAlgorithmParameterException("Required GCMParameterSpec for decryption");
        }
        if (algorithmParameterSpec == null) {
            byte[] bArr = new byte[12];
            secureRandom.nextBytes(bArr);
            algorithmParameterSpec = new GCMParameterSpec(128, bArr);
        } else if (!(algorithmParameterSpec instanceof GCMParameterSpec)) {
            throw new InvalidAlgorithmParameterException("Required GCMParameterSpec");
        }
        GCMParameterSpec gCMParameterSpec = (GCMParameterSpec) algorithmParameterSpec;
        if (gCMParameterSpec.getIV().length != 12) {
            throw new InvalidAlgorithmParameterException("Only IV of 12 is supported");
        } else if (key.getEncoded().length == 16 || key.getEncoded().length == 32) {
            this.algorithmParameterSpec = gCMParameterSpec;
            boolean z = true;
            if (i != 1) {
                z = false;
            }
            this.encrypt = z;
            this.cipherContext = initializeCipher(z, key.getEncoded(), key.getEncoded().length, gCMParameterSpec.getIV());
            if (!this.encrypt) {
                this.reserveBuffer = new ReserveBuffer(gCMParameterSpec.getTLen() / 8);
            }
        } else {
            throw new InvalidAlgorithmParameterException("Only keys of 16 bytes or 32 bytes are supported");
        }
    }

    @Override // javax.crypto.CipherSpi
    protected byte[] engineUpdate(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            return new byte[0];
        }
        if (this.encrypt) {
            byte[] bArr2 = new byte[i2];
            update(this.cipherContext, bArr, i, i2, bArr2, 0, i2);
            return bArr2;
        }
        byte[] update = this.reserveBuffer.update(bArr, i, i2);
        int length = update.length;
        byte[] bArr3 = new byte[length];
        update(this.cipherContext, update, 0, update.length, bArr3, 0, length);
        return bArr3;
    }

    @Override // javax.crypto.CipherSpi
    protected int engineUpdate(byte[] bArr, int i, int i2, byte[] bArr2, int i3) throws ShortBufferException {
        byte[] engineUpdate = engineUpdate(bArr, i, i2);
        if (engineUpdate.length <= bArr2.length - i3) {
            System.arraycopy(engineUpdate, 0, bArr2, i3, engineUpdate.length);
            return engineUpdate.length;
        }
        throw new ShortBufferException("Needed: " + engineUpdate.length + " but provided: " + (bArr2.length - i3));
    }

    @Override // javax.crypto.CipherSpi
    protected byte[] engineDoFinal(byte[] bArr, int i, int i2) throws BadPaddingException, IllegalBlockSizeException {
        byte[] engineUpdate = engineUpdate(bArr, i, i2);
        if (this.encrypt) {
            int tLen = this.algorithmParameterSpec.getTLen() / 8;
            byte[] bArr2 = new byte[tLen];
            finishEncrypt(this.cipherContext, bArr2, tLen);
            byte[] bArr3 = new byte[engineUpdate.length + tLen];
            System.arraycopy(engineUpdate, 0, bArr3, 0, engineUpdate.length);
            System.arraycopy(bArr2, 0, bArr3, engineUpdate.length, tLen);
            return bArr3;
        } else if (this.reserveBuffer.getAvailable() == this.algorithmParameterSpec.getTLen() / 8) {
            int tLen2 = this.algorithmParameterSpec.getTLen() / 8;
            byte[] bArr4 = new byte[tLen2];
            this.reserveBuffer.read(bArr4, 0, tLen2);
            if (finishDecrypt(this.cipherContext, bArr4, tLen2)) {
                return engineUpdate;
            }
            throw new BadPaddingException("Incorrect tag!");
        } else {
            throw new BadPaddingException("Original ciphertext shorter than tag length");
        }
    }

    @Override // javax.crypto.CipherSpi
    protected int engineDoFinal(byte[] bArr, int i, int i2, byte[] bArr2, int i3) throws BadPaddingException, IllegalBlockSizeException, ShortBufferException {
        byte[] engineDoFinal = engineDoFinal(bArr, i, i2);
        if (engineDoFinal.length <= bArr2.length - i3) {
            System.arraycopy(engineDoFinal, 0, bArr2, i3, engineDoFinal.length);
            return engineDoFinal.length;
        }
        throw new ShortBufferException("Needed: " + engineDoFinal.length + " but provided: " + (bArr2.length - i3));
    }

    @Override // javax.crypto.CipherSpi
    protected void engineUpdateAAD(byte[] bArr, int i, int i2) {
        updateAAD(this.cipherContext, bArr, i, i2);
    }

    @Override // javax.crypto.CipherSpi
    protected void engineUpdateAAD(ByteBuffer byteBuffer) {
        int limit;
        if (byteBuffer != null && (limit = byteBuffer.limit() - byteBuffer.position()) > 0) {
            if (byteBuffer.hasArray()) {
                updateAAD(this.cipherContext, byteBuffer.array(), addExact(byteBuffer.arrayOffset(), byteBuffer.position()), limit);
                byteBuffer.position(byteBuffer.limit());
                return;
            }
            byte[] bArr = new byte[limit];
            byteBuffer.get(bArr);
            updateAAD(this.cipherContext, bArr, 0, limit);
        }
    }

    private static int addExact(int i, int i2) {
        int i3 = i + i2;
        if (((i ^ i3) & (i2 ^ i3)) >= 0) {
            return i3;
        }
        throw new ArithmeticException("integer overflow");
    }

    @Override // java.lang.Object
    protected void finalize() {
        long j = this.cipherContext;
        if (j != 0) {
            destroy(j);
        }
    }
}
