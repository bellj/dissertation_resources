package org.signal.aesgcmprovider;

import java.security.Provider;

/* loaded from: classes.dex */
public final class AesGcmProvider extends Provider {
    static {
        System.loadLibrary("aesgcm");
    }

    public AesGcmProvider() {
        super("AesGcmProvider", 1.0d, "AES GCM BoringSSL-backed provider to work around decisions made by Conscrypt");
        put("Cipher.AES/GCM/NoPadding", "org.signal.aesgcmprovider.AesGcmCipher");
    }
}
