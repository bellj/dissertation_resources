package org.signal.aesgcmprovider;

/* access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class ReserveBuffer {
    private int available = 0;
    private final byte[] reserve;
    private int writePointer = 0;

    public ReserveBuffer(int i) {
        this.reserve = new byte[i];
    }

    public byte[] update(byte[] bArr, int i, int i2) {
        int min = Math.min(this.reserve.length, i2);
        int max = Math.max(min - capacity(), 0);
        int i3 = i2 - min;
        byte[] bArr2 = new byte[max + i3];
        read(bArr2, 0, max);
        System.arraycopy(bArr, i, bArr2, max, i3);
        write(bArr, i + i3, min);
        return bArr2;
    }

    private void write(byte[] bArr, int i, int i2) {
        int min = Math.min(this.reserve.length - this.writePointer, i2);
        System.arraycopy(bArr, i, this.reserve, this.writePointer, min);
        System.arraycopy(bArr, i + min, this.reserve, 0, i2 - min);
        this.writePointer = (this.writePointer + i2) % this.reserve.length;
        this.available += i2;
    }

    public void read(byte[] bArr, int i, int i2) {
        int i3 = this.writePointer - this.available;
        if (i3 < 0) {
            i3 += this.reserve.length;
        }
        int min = Math.min(this.reserve.length - i3, i2);
        System.arraycopy(this.reserve, i3, bArr, i, min);
        System.arraycopy(this.reserve, 0, bArr, i + min, i2 - min);
        this.available -= i2;
    }

    public int capacity() {
        return this.reserve.length - this.available;
    }

    public int getAvailable() {
        return this.available;
    }
}
