package org.signal.core.util.logging;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.thoughtcrime.securesms.contacts.ContactRepository;

/* loaded from: classes3.dex */
public final class Scrubber {
    private static final Pattern CRUDE_EMAIL_PATTERN = Pattern.compile("\\b([^\\s/])([^\\s/]*@[^\\s]+)");
    private static final Pattern DOMAIN_PATTERN = Pattern.compile("([a-z0-9]+\\.)+([a-z0-9\\-]*[a-z\\-][a-z0-9\\-]*)", 2);
    private static final Pattern E164_PATTERN = Pattern.compile("(\\+|%2B)(\\d{5,13})(\\d{2})");
    private static final Pattern GROUP_ID_V1_PATTERN = Pattern.compile("(__)(textsecure_group__![^\\s]+)([^\\s]{2})");
    private static final Pattern GROUP_ID_V2_PATTERN = Pattern.compile("(__)(signal_group__v2__![^\\s]+)([^\\s]{2})");
    private static final Pattern IPV4_PATTERN = Pattern.compile("\\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\b");
    private static final Set<String> TOP_100_TLDS = new HashSet(Arrays.asList("com", "net", "org", "jp", "de", "uk", "fr", "br", "it", "ru", "es", "me", "gov", "pl", "ca", "au", "cn", "co", "in", "nl", "edu", "info", "eu", "ch", ContactRepository.ID_COLUMN, "at", "kr", "cz", "mx", "be", "tv", "se", "tr", "tw", "al", "ua", "ir", "vn", "cl", "sk", "ly", "cc", "to", "no", "fi", "us", "pt", "dk", "ar", "hu", "tk", "gr", "il", "news", "ro", "my", "biz", "ie", "za", "nz", "sg", "ee", "th", "io", "xyz", "pe", "bg", "hk", "lt", "link", "ph", "club", "si", "site", "mobi", "by", "cat", "wiki", "la", "ga", "xxx", "cf", "hr", "ng", "jobs", "online", "kz", "ug", "gq", "ae", "is", "lv", "pro", "fm", "tips", "ms", "sa", "app"));
    private static final Pattern UUID_PATTERN = Pattern.compile("(JOB::)?([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{10})([0-9a-f]{2})", 2);

    /* loaded from: classes3.dex */
    public interface ProcessMatch {
        void scrubMatch(Matcher matcher, StringBuilder sb);
    }

    public static CharSequence scrub(CharSequence charSequence) {
        return scrubIpv4(scrubDomains(scrubUuids(scrubGroupsV2(scrubGroupsV1(scrubEmail(scrubE164(charSequence)))))));
    }

    private static CharSequence scrubE164(CharSequence charSequence) {
        return scrub(charSequence, E164_PATTERN, new ProcessMatch() { // from class: org.signal.core.util.logging.Scrubber$$ExternalSyntheticLambda1
            @Override // org.signal.core.util.logging.Scrubber.ProcessMatch
            public final void scrubMatch(Matcher matcher, StringBuilder sb) {
                Scrubber.lambda$scrubE164$0(matcher, sb);
            }
        });
    }

    public static /* synthetic */ void lambda$scrubE164$0(Matcher matcher, StringBuilder sb) {
        sb.append(matcher.group(1));
        sb.append("*************", 0, matcher.group(2).length());
        sb.append(matcher.group(3));
    }

    private static CharSequence scrubEmail(CharSequence charSequence) {
        return scrub(charSequence, CRUDE_EMAIL_PATTERN, new ProcessMatch() { // from class: org.signal.core.util.logging.Scrubber$$ExternalSyntheticLambda4
            @Override // org.signal.core.util.logging.Scrubber.ProcessMatch
            public final void scrubMatch(Matcher matcher, StringBuilder sb) {
                Scrubber.lambda$scrubEmail$1(matcher, sb);
            }
        });
    }

    public static /* synthetic */ void lambda$scrubEmail$1(Matcher matcher, StringBuilder sb) {
        sb.append(matcher.group(1));
        sb.append("...@...");
    }

    private static CharSequence scrubGroupsV1(CharSequence charSequence) {
        return scrub(charSequence, GROUP_ID_V1_PATTERN, new ProcessMatch() { // from class: org.signal.core.util.logging.Scrubber$$ExternalSyntheticLambda6
            @Override // org.signal.core.util.logging.Scrubber.ProcessMatch
            public final void scrubMatch(Matcher matcher, StringBuilder sb) {
                Scrubber.lambda$scrubGroupsV1$2(matcher, sb);
            }
        });
    }

    public static /* synthetic */ void lambda$scrubGroupsV1$2(Matcher matcher, StringBuilder sb) {
        sb.append(matcher.group(1));
        sb.append("...group...");
        sb.append(matcher.group(3));
    }

    private static CharSequence scrubGroupsV2(CharSequence charSequence) {
        return scrub(charSequence, GROUP_ID_V2_PATTERN, new ProcessMatch() { // from class: org.signal.core.util.logging.Scrubber$$ExternalSyntheticLambda5
            @Override // org.signal.core.util.logging.Scrubber.ProcessMatch
            public final void scrubMatch(Matcher matcher, StringBuilder sb) {
                Scrubber.lambda$scrubGroupsV2$3(matcher, sb);
            }
        });
    }

    public static /* synthetic */ void lambda$scrubGroupsV2$3(Matcher matcher, StringBuilder sb) {
        sb.append(matcher.group(1));
        sb.append("...group_v2...");
        sb.append(matcher.group(3));
    }

    private static CharSequence scrubUuids(CharSequence charSequence) {
        return scrub(charSequence, UUID_PATTERN, new ProcessMatch() { // from class: org.signal.core.util.logging.Scrubber$$ExternalSyntheticLambda2
            @Override // org.signal.core.util.logging.Scrubber.ProcessMatch
            public final void scrubMatch(Matcher matcher, StringBuilder sb) {
                Scrubber.lambda$scrubUuids$4(matcher, sb);
            }
        });
    }

    public static /* synthetic */ void lambda$scrubUuids$4(Matcher matcher, StringBuilder sb) {
        if (matcher.group(1) == null || matcher.group(1).isEmpty()) {
            sb.append("********-****-****-****-**********");
            sb.append(matcher.group(3));
            return;
        }
        sb.append(matcher.group(1));
        sb.append(matcher.group(2));
        sb.append(matcher.group(3));
    }

    private static CharSequence scrubDomains(CharSequence charSequence) {
        return scrub(charSequence, DOMAIN_PATTERN, new ProcessMatch() { // from class: org.signal.core.util.logging.Scrubber$$ExternalSyntheticLambda0
            @Override // org.signal.core.util.logging.Scrubber.ProcessMatch
            public final void scrubMatch(Matcher matcher, StringBuilder sb) {
                Scrubber.lambda$scrubDomains$5(matcher, sb);
            }
        });
    }

    public static /* synthetic */ void lambda$scrubDomains$5(Matcher matcher, StringBuilder sb) {
        String group = matcher.group(0);
        if (matcher.groupCount() != 2 || !TOP_100_TLDS.contains(matcher.group(2).toLowerCase(Locale.US)) || group.endsWith("whispersystems.org") || group.endsWith("signal.org")) {
            sb.append(group);
            return;
        }
        sb.append("***.");
        sb.append(matcher.group(2));
    }

    private static CharSequence scrubIpv4(CharSequence charSequence) {
        return scrub(charSequence, IPV4_PATTERN, new ProcessMatch() { // from class: org.signal.core.util.logging.Scrubber$$ExternalSyntheticLambda3
            @Override // org.signal.core.util.logging.Scrubber.ProcessMatch
            public final void scrubMatch(Matcher matcher, StringBuilder sb) {
                sb.append("...ipv4...");
            }
        });
    }

    private static CharSequence scrub(CharSequence charSequence, Pattern pattern, ProcessMatch processMatch) {
        StringBuilder sb = new StringBuilder(charSequence.length());
        Matcher matcher = pattern.matcher(charSequence);
        int i = 0;
        while (matcher.find()) {
            sb.append(charSequence, i, matcher.start());
            processMatch.scrubMatch(matcher, sb);
            i = matcher.end();
        }
        if (i == 0) {
            return charSequence;
        }
        sb.append(charSequence, i, charSequence.length());
        return sb;
    }
}
