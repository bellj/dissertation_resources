package org.signal.core.util.concurrent;

import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.Executor;

/* loaded from: classes3.dex */
public final class LatestPrioritizedSerialExecutor {
    private Runnable active;
    private final Executor executor;
    private final Queue<PriorityRunnable> tasks = new PriorityQueue();

    public LatestPrioritizedSerialExecutor(Executor executor) {
        this.executor = executor;
    }

    public synchronized void execute(int i, final Runnable runnable) {
        Iterator<PriorityRunnable> it = this.tasks.iterator();
        while (it.hasNext()) {
            if (it.next().getPriority() == i) {
                it.remove();
            }
        }
        this.tasks.offer(new PriorityRunnable(i) { // from class: org.signal.core.util.concurrent.LatestPrioritizedSerialExecutor.1
            @Override // java.lang.Runnable
            public void run() {
                try {
                    runnable.run();
                } finally {
                    LatestPrioritizedSerialExecutor.this.scheduleNext();
                }
            }
        });
        if (this.active == null) {
            scheduleNext();
        }
    }

    public synchronized void scheduleNext() {
        PriorityRunnable poll = this.tasks.poll();
        this.active = poll;
        if (poll != null) {
            this.executor.execute(poll);
        }
    }

    /* loaded from: classes3.dex */
    public static abstract class PriorityRunnable implements Runnable, Comparable<PriorityRunnable> {
        private final int priority;

        public PriorityRunnable(int i) {
            this.priority = i;
        }

        public int getPriority() {
            return this.priority;
        }

        public final int compareTo(PriorityRunnable priorityRunnable) {
            return priorityRunnable.getPriority() - getPriority();
        }
    }
}
