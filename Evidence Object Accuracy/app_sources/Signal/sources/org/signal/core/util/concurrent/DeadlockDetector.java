package org.signal.core.util.concurrent;

import android.os.Handler;
import java.lang.Thread;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import kotlin.Metadata;
import kotlin.collections.SetsKt__SetsJVMKt;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.ExceptionUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;

/* compiled from: DeadlockDetector.kt */
@Metadata(bv = {}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010#\n\u0002\b\u0002\n\u0002\u0010\"\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0010\u0018\u0000 )2\u00020\u0001:\u0002)*B\u0017\u0012\u0006\u0010\u000e\u001a\u00020\r\u0012\u0006\u0010\u0011\u001a\u00020\u0010¢\u0006\u0004\b'\u0010(J\b\u0010\u0003\u001a\u00020\u0002H\u0002J\f\u0010\u0006\u001a\u00020\u0005*\u00020\u0004H\u0002J\u0019\u0010\t\u001a\u00020\u0005*\b\u0012\u0004\u0012\u00020\b0\u0007H\u0002¢\u0006\u0004\b\t\u0010\nJ\u0006\u0010\u000b\u001a\u00020\u0002J\u0006\u0010\f\u001a\u00020\u0002R\u0014\u0010\u000e\u001a\u00020\r8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0014\u0010\u0011\u001a\u00020\u00108\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0013\u001a\u00020\u00058\u0002@\u0002X\u000e¢\u0006\u0006\n\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00100\u00158\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0016\u0010\u0017R\u001a\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00040\u00188\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0019\u0010\u0017R6\u0010\u001c\u001a\u0016\u0012\u0004\u0012\u00020\u001b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u0007\u0018\u00010\u001a8\u0006@\u0006X\u000e¢\u0006\u0012\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R\"\u0010\"\u001a\u00020\u00108\u0006@\u0006X\u000e¢\u0006\u0012\n\u0004\b\"\u0010\u0012\u001a\u0004\b#\u0010$\"\u0004\b%\u0010&¨\u0006+"}, d2 = {"Lorg/signal/core/util/concurrent/DeadlockDetector;", "", "", "poll", "Ljava/lang/Thread$State;", "", "isWaiting", "", "Ljava/lang/StackTraceElement;", "hasPotentialLock", "([Ljava/lang/StackTraceElement;)Z", NotificationProfileDatabase.NotificationProfileScheduleTable.START, "stop", "Landroid/os/Handler;", "handler", "Landroid/os/Handler;", "", "pollingInterval", "J", "running", "Z", "", "previouslyBlocked", "Ljava/util/Set;", "", "waitingStates", "", "Ljava/lang/Thread;", "lastThreadDump", "Ljava/util/Map;", "getLastThreadDump", "()Ljava/util/Map;", "setLastThreadDump", "(Ljava/util/Map;)V", "lastThreadDumpTime", "getLastThreadDumpTime", "()J", "setLastThreadDumpTime", "(J)V", "<init>", "(Landroid/os/Handler;J)V", "Companion", "ExecutorInfo", "core-util_release"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes.dex */
public final class DeadlockDetector {
    private static final Set<String> BLOCK_BLOCKLIST = SetsKt__SetsJVMKt.setOf("HeapTaskDaemon");
    private static final Set<ExecutorInfo> CHECK_FULLNESS_EXECUTORS;
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(DeadlockDetector.class);
    private final Handler handler;
    private volatile Map<Thread, StackTraceElement[]> lastThreadDump;
    private volatile long lastThreadDumpTime = -1;
    private final long pollingInterval;
    private final Set<Long> previouslyBlocked = new LinkedHashSet();
    private boolean running;
    private final Set<Thread.State> waitingStates = SetsKt__SetsKt.setOf((Object[]) new Thread.State[]{Thread.State.WAITING, Thread.State.TIMED_WAITING});

    public DeadlockDetector(Handler handler, long j) {
        Intrinsics.checkNotNullParameter(handler, "handler");
        this.handler = handler;
        this.pollingInterval = j;
    }

    public final Map<Thread, StackTraceElement[]> getLastThreadDump() {
        return this.lastThreadDump;
    }

    public final long getLastThreadDumpTime() {
        return this.lastThreadDumpTime;
    }

    public final void start() {
        Log.d(TAG, "Beginning deadlock monitoring.");
        this.running = true;
        this.handler.postDelayed(new DeadlockDetector$$ExternalSyntheticLambda0(this), this.pollingInterval);
    }

    public final void stop() {
        Log.d(TAG, "Ending deadlock monitoring.");
        this.running = false;
        this.handler.removeCallbacksAndMessages(null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0052, code lost:
        if (hasPotentialLock(r10) != false) goto L_0x0054;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x012e, code lost:
        r9 = r5.get(r13);
        kotlin.jvm.internal.Intrinsics.checkNotNull(r9);
        r10.add(new kotlin.Pair(r13, (java.lang.StackTraceElement[]) r9));
     */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x0057 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x001c A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void poll() {
        /*
        // Method dump skipped, instructions count: 606
        */
        throw new UnsupportedOperationException("Method not decompiled: org.signal.core.util.concurrent.DeadlockDetector.poll():void");
    }

    /* compiled from: DeadlockDetector.kt */
    @Metadata(bv = {}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\b\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\n\u001a\u00020\t\u0012\u0006\u0010\u000e\u001a\u00020\u0002¢\u0006\u0004\b\u0012\u0010\u0013J\t\u0010\u0003\u001a\u00020\u0002HÖ\u0001J\t\u0010\u0005\u001a\u00020\u0004HÖ\u0001J\u0013\u0010\b\u001a\u00020\u00072\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001HÖ\u0003R\u0017\u0010\n\u001a\u00020\t8\u0006¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\f\u0010\rR\u0017\u0010\u000e\u001a\u00020\u00028\u0006¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011¨\u0006\u0014"}, d2 = {"Lorg/signal/core/util/concurrent/DeadlockDetector$ExecutorInfo;", "", "", "toString", "", "hashCode", "other", "", "equals", "Ljava/util/concurrent/ExecutorService;", "executor", "Ljava/util/concurrent/ExecutorService;", "getExecutor", "()Ljava/util/concurrent/ExecutorService;", "namePrefix", "Ljava/lang/String;", "getNamePrefix", "()Ljava/lang/String;", "<init>", "(Ljava/util/concurrent/ExecutorService;Ljava/lang/String;)V", "core-util_release"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes3.dex */
    public static final class ExecutorInfo {
        private final ExecutorService executor;
        private final String namePrefix;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ExecutorInfo)) {
                return false;
            }
            ExecutorInfo executorInfo = (ExecutorInfo) obj;
            return Intrinsics.areEqual(this.executor, executorInfo.executor) && Intrinsics.areEqual(this.namePrefix, executorInfo.namePrefix);
        }

        public int hashCode() {
            return (this.executor.hashCode() * 31) + this.namePrefix.hashCode();
        }

        public String toString() {
            return "ExecutorInfo(executor=" + this.executor + ", namePrefix=" + this.namePrefix + ')';
        }

        public ExecutorInfo(ExecutorService executorService, String str) {
            Intrinsics.checkNotNullParameter(executorService, "executor");
            Intrinsics.checkNotNullParameter(str, "namePrefix");
            this.executor = executorService;
            this.namePrefix = str;
        }

        public final ExecutorService getExecutor() {
            return this.executor;
        }

        public final String getNamePrefix() {
            return this.namePrefix;
        }
    }

    private final boolean isWaiting(Thread.State state) {
        return this.waitingStates.contains(state);
    }

    /* compiled from: DeadlockDetector.kt */
    @Metadata(d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J*\u0010\f\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\u00052\u0018\u0010\u000e\u001a\u0014\u0012\u0004\u0012\u00020\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u00110\u000fH\u0002J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00070\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tXT¢\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\n \u000b*\u0004\u0018\u00010\u00050\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0017"}, d2 = {"Lorg/signal/core/util/concurrent/DeadlockDetector$Companion;", "", "()V", "BLOCK_BLOCKLIST", "", "", "CHECK_FULLNESS_EXECUTORS", "Lorg/signal/core/util/concurrent/DeadlockDetector$ExecutorInfo;", "CONCERNING_QUEUE_THRESHOLD", "", "TAG", "kotlin.jvm.PlatformType", "buildLogString", "description", RecipientDatabase.BLOCKED, "", "Ljava/lang/Thread;", "", "Ljava/lang/StackTraceElement;", "isExecutorFull", "", "executor", "Ljava/util/concurrent/ExecutorService;", "core-util_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(r5v0 java.lang.StackTraceElement), ('\n' char)] */
        public final String buildLogString(String str, Map<Thread, StackTraceElement[]> map) {
            StackTraceElement[] stackTraceElementArr;
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("\n");
            for (Map.Entry<Thread, StackTraceElement[]> entry : map.entrySet()) {
                sb.append("-- [" + entry.getKey().getId() + "] " + entry.getKey().getName() + " | " + entry.getKey().getState() + '\n');
                Throwable th = TracedThreads.INSTANCE.getCallerStackTraces().get(Long.valueOf(entry.getKey().getId()));
                if (th != null) {
                    stackTraceElementArr = ExceptionUtil.joinStackTrace(entry.getValue(), th.getStackTrace());
                    Intrinsics.checkNotNullExpressionValue(stackTraceElementArr, "{\n          ExceptionUti…ble.stackTrace)\n        }");
                } else {
                    stackTraceElementArr = entry.getValue();
                }
                for (StackTraceElement stackTraceElement : stackTraceElementArr) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(stackTraceElement);
                    sb2.append('\n');
                    sb.append(sb2.toString());
                }
                sb.append("\n");
            }
            String sb3 = sb.toString();
            Intrinsics.checkNotNullExpressionValue(sb3, "stringBuilder.toString()");
            return sb3;
        }

        public final boolean isExecutorFull(ExecutorService executorService) {
            if (!(executorService instanceof TracingExecutorService) || ((TracingExecutorService) executorService).getQueue().size() <= 4) {
                return false;
            }
            return true;
        }
    }

    static {
        Companion = new Companion(null);
        TAG = Log.tag(DeadlockDetector.class);
        ExecutorService executorService = SignalExecutors.BOUNDED;
        Intrinsics.checkNotNullExpressionValue(executorService, "BOUNDED");
        ExecutorService executorService2 = SignalExecutors.BOUNDED_IO;
        Intrinsics.checkNotNullExpressionValue(executorService2, "BOUNDED_IO");
        CHECK_FULLNESS_EXECUTORS = SetsKt__SetsKt.setOf((Object[]) new ExecutorInfo[]{new ExecutorInfo(executorService, "signal-bounded-"), new ExecutorInfo(executorService2, "signal-io-bounded")});
        BLOCK_BLOCKLIST = SetsKt__SetsJVMKt.setOf("HeapTaskDaemon");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x003a, code lost:
        if (kotlin.text.StringsKt__StringsKt.contains$default((java.lang.CharSequence) r4, (java.lang.CharSequence) "IncomingMessageObserver", false, 2, (java.lang.Object) null) != false) goto L_0x003f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0044 A[LOOP:0: B:3:0x0003->B:15:0x0044, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0042 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean hasPotentialLock(java.lang.StackTraceElement[] r11) {
        /*
            r10 = this;
            int r0 = r11.length
            r1 = 0
            r2 = 0
        L_0x0003:
            r3 = 1
            if (r2 >= r0) goto L_0x0047
            r4 = r11[r2]
            java.lang.String r5 = r4.getMethodName()
            java.lang.String r6 = "it.methodName"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r5, r6)
            java.lang.String r7 = "lock"
            r8 = 2
            r9 = 0
            boolean r5 = kotlin.text.StringsKt.startsWith$default(r5, r7, r1, r8, r9)
            if (r5 != 0) goto L_0x003f
            java.lang.String r5 = r4.getMethodName()
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r5, r6)
            java.lang.String r6 = "waitForConnection"
            boolean r5 = kotlin.text.StringsKt.startsWith$default(r5, r6, r1, r8, r9)
            if (r5 == 0) goto L_0x003d
            java.lang.String r4 = r4.getClassName()
            java.lang.String r5 = "it.className"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r4, r5)
            java.lang.String r5 = "IncomingMessageObserver"
            boolean r4 = kotlin.text.StringsKt.contains$default(r4, r5, r1, r8, r9)
            if (r4 == 0) goto L_0x003d
            goto L_0x003f
        L_0x003d:
            r4 = 0
            goto L_0x0040
        L_0x003f:
            r4 = 1
        L_0x0040:
            if (r4 == 0) goto L_0x0044
            r1 = 1
            goto L_0x0047
        L_0x0044:
            int r2 = r2 + 1
            goto L_0x0003
        L_0x0047:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: org.signal.core.util.concurrent.DeadlockDetector.hasPotentialLock(java.lang.StackTraceElement[]):boolean");
    }
}
