package org.signal.core.util;

import android.os.Build;
import j$.util.Map;
import java.util.Map;

/* loaded from: classes3.dex */
public final class MapUtil {
    public static <K, V> V getOrDefault(Map<K, V> map, K k, V v) {
        if (Build.VERSION.SDK_INT >= 24) {
            return (V) Map.EL.getOrDefault(map, k, v);
        }
        V v2 = map.get(k);
        return v2 == null ? v : v2;
    }
}
