package org.signal.core.util;

import android.database.Cursor;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteQueryBuilder;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: SQLiteDatabaseExtensions.kt */
@Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001BI\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\u0006\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\n\u001a\u00020\u0006\u0012\u0006\u0010\u000b\u001a\u00020\u0006¢\u0006\u0002\u0010\fJ\u0006\u0010\u000e\u001a\u00020\u000fR\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0004¢\u0006\u0004\n\u0002\u0010\rR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0004¢\u0006\u0004\n\u0002\u0010\r¨\u0006\u0010"}, d2 = {"Lorg/signal/core/util/SelectBuilderPart5;", "", "db", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "columns", "", "", "tableName", "where", "whereArgs", "orderBy", "limit", "(Landroidx/sqlite/db/SupportSQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "[Ljava/lang/String;", "run", "Landroid/database/Cursor;", "core-util_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class SelectBuilderPart5 {
    private final String[] columns;
    private final SupportSQLiteDatabase db;
    private final String limit;
    private final String orderBy;
    private final String tableName;
    private final String where;
    private final String[] whereArgs;

    public SelectBuilderPart5(SupportSQLiteDatabase supportSQLiteDatabase, String[] strArr, String str, String str2, String[] strArr2, String str3, String str4) {
        Intrinsics.checkNotNullParameter(supportSQLiteDatabase, "db");
        Intrinsics.checkNotNullParameter(strArr, "columns");
        Intrinsics.checkNotNullParameter(str, "tableName");
        Intrinsics.checkNotNullParameter(str2, "where");
        Intrinsics.checkNotNullParameter(strArr2, "whereArgs");
        Intrinsics.checkNotNullParameter(str3, "orderBy");
        Intrinsics.checkNotNullParameter(str4, "limit");
        this.db = supportSQLiteDatabase;
        this.columns = strArr;
        this.tableName = str;
        this.where = str2;
        this.whereArgs = strArr2;
        this.orderBy = str3;
        this.limit = str4;
    }

    public final Cursor run() {
        Cursor query = this.db.query(SupportSQLiteQueryBuilder.builder(this.tableName).columns(this.columns).selection(this.where, this.whereArgs).orderBy(this.orderBy).limit(this.limit).create());
        Intrinsics.checkNotNullExpressionValue(query, "db.query(\n      SupportS…)\n        .create()\n    )");
        return query;
    }
}
