package org.signal.core.util.logging;

/* loaded from: classes.dex */
public final class Log {
    private static final Logger NOOP_LOGGER = new NoopLogger();
    private static InternalCheck internalCheck;
    private static Logger logger = new AndroidLogger();

    /* loaded from: classes.dex */
    public interface InternalCheck {
        boolean isInternal();
    }

    public static void initialize(InternalCheck internalCheck2, Logger... loggerArr) {
        internalCheck = internalCheck2;
        logger = new CompoundLogger(loggerArr);
    }

    public static void v(String str, String str2) {
        v(str, str2, null);
    }

    public static void d(String str, String str2) {
        d(str, str2, (Throwable) null);
    }

    public static void i(String str, String str2) {
        i(str, str2, (Throwable) null);
    }

    public static void w(String str, String str2) {
        w(str, str2, (Throwable) null);
    }

    public static void e(String str, String str2) {
        e(str, str2, null);
    }

    public static void i(String str, Throwable th) {
        i(str, (String) null, th);
    }

    public static void w(String str, Throwable th) {
        w(str, (String) null, th);
    }

    public static void e(String str, Throwable th) {
        e(str, null, th);
    }

    public static void v(String str, String str2, Throwable th) {
        logger.v(str, str2, th);
    }

    public static void d(String str, String str2, Throwable th) {
        logger.d(str, str2, th);
    }

    public static void i(String str, String str2, Throwable th) {
        logger.i(str, str2, th);
    }

    public static void w(String str, String str2, Throwable th) {
        logger.w(str, str2, th);
    }

    public static void e(String str, String str2, Throwable th) {
        logger.e(str, str2, th);
    }

    public static void d(String str, String str2, boolean z) {
        logger.d(str, str2, z);
    }

    public static void i(String str, String str2, boolean z) {
        logger.i(str, str2, z);
    }

    public static void w(String str, String str2, boolean z) {
        logger.w(str, str2, z);
    }

    public static void d(String str, String str2, Throwable th, boolean z) {
        logger.d(str, str2, th, z);
    }

    public static void i(String str, String str2, Throwable th, boolean z) {
        logger.i(str, str2, th, z);
    }

    public static void w(String str, String str2, Throwable th, boolean z) {
        logger.w(str, str2, th, z);
    }

    public static void e(String str, String str2, Throwable th, boolean z) {
        logger.e(str, str2, th, z);
    }

    public static String tag(Class<?> cls) {
        String simpleName = cls.getSimpleName();
        return simpleName.length() > 23 ? simpleName.substring(0, 23) : simpleName;
    }

    public static Logger internal() {
        if (internalCheck.isInternal()) {
            return logger;
        }
        return NOOP_LOGGER;
    }

    public static void blockUntilAllWritesFinished() {
        logger.flush();
    }

    /* loaded from: classes.dex */
    public static abstract class Logger {
        public abstract void d(String str, String str2, Throwable th, boolean z);

        public abstract void e(String str, String str2, Throwable th, boolean z);

        public abstract void flush();

        public abstract void i(String str, String str2, Throwable th, boolean z);

        public abstract void v(String str, String str2, Throwable th, boolean z);

        public abstract void w(String str, String str2, Throwable th, boolean z);

        public void v(String str, String str2, boolean z) {
            v(str, str2, null, z);
        }

        public void d(String str, String str2, boolean z) {
            d(str, str2, null, z);
        }

        public void i(String str, String str2, boolean z) {
            i(str, str2, null, z);
        }

        public void w(String str, String str2, boolean z) {
            w(str, str2, null, z);
        }

        public void e(String str, String str2, boolean z) {
            e(str, str2, null, z);
        }

        public void v(String str, String str2, Throwable th) {
            v(str, str2, th, false);
        }

        public void d(String str, String str2, Throwable th) {
            d(str, str2, th, false);
        }

        public void i(String str, String str2, Throwable th) {
            i(str, str2, th, false);
        }

        public void w(String str, String str2, Throwable th) {
            w(str, str2, th, false);
        }

        public void e(String str, String str2, Throwable th) {
            e(str, str2, th, false);
        }

        public void v(String str, String str2) {
            v(str, str2, (Throwable) null);
        }

        public void d(String str, String str2) {
            d(str, str2, (Throwable) null);
        }

        public void i(String str, String str2) {
            i(str, str2, (Throwable) null);
        }

        public void w(String str, String str2) {
            w(str, str2, (Throwable) null);
        }

        public void e(String str, String str2) {
            e(str, str2, (Throwable) null);
        }
    }
}
