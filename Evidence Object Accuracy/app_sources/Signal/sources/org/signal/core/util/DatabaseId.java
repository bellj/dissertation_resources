package org.signal.core.util;

/* loaded from: classes.dex */
public interface DatabaseId {
    String serialize();
}
