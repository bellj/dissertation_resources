package org.signal.core.util;

import android.database.Cursor;
import j$.util.Optional;
import j$.util.function.Function;

/* loaded from: classes3.dex */
public final class CursorUtil {
    public static String requireString(Cursor cursor, String str) {
        return cursor.getString(cursor.getColumnIndexOrThrow(str));
    }

    public static int requireInt(Cursor cursor, String str) {
        return cursor.getInt(cursor.getColumnIndexOrThrow(str));
    }

    public static float requireFloat(Cursor cursor, String str) {
        return cursor.getFloat(cursor.getColumnIndexOrThrow(str));
    }

    public static long requireLong(Cursor cursor, String str) {
        return cursor.getLong(cursor.getColumnIndexOrThrow(str));
    }

    public static boolean requireBoolean(Cursor cursor, String str) {
        return requireInt(cursor, str) != 0;
    }

    public static byte[] requireBlob(Cursor cursor, String str) {
        return cursor.getBlob(cursor.getColumnIndexOrThrow(str));
    }

    public static Optional<String> getString(Cursor cursor, String str) {
        if (cursor.getColumnIndex(str) < 0) {
            return Optional.empty();
        }
        return Optional.ofNullable(requireString(cursor, str));
    }

    public static Optional<Integer> getInt(Cursor cursor, String str) {
        if (cursor.getColumnIndex(str) < 0) {
            return Optional.empty();
        }
        return Optional.of(Integer.valueOf(requireInt(cursor, str)));
    }

    public static Optional<Boolean> getBoolean(Cursor cursor, String str) {
        if (cursor.getColumnIndex(str) < 0) {
            return Optional.empty();
        }
        return Optional.of(Boolean.valueOf(requireBoolean(cursor, str)));
    }

    public static Optional<byte[]> getBlob(Cursor cursor, String str) {
        if (cursor.getColumnIndex(str) < 0) {
            return Optional.empty();
        }
        return Optional.ofNullable(requireBlob(cursor, str));
    }

    public static String readRowAsString(Cursor cursor) {
        StringBuilder sb = new StringBuilder();
        int columnCount = cursor.getColumnCount();
        for (int i = 0; i < columnCount; i++) {
            sb.append(cursor.getString(i));
            if (i < columnCount - 1) {
                sb.append(" | ");
            }
        }
        return sb.toString();
    }

    public static <T> T getAggregateOrDefault(Cursor cursor, T t, Function<Integer, T> function) {
        return cursor.moveToFirst() ? function.apply(0) : t;
    }
}
