package org.signal.core.util;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import java.util.Locale;

/* loaded from: classes3.dex */
public final class ResourceUtil {
    public static Resources getEnglishResources(Context context) {
        return getResources(context, Locale.ENGLISH);
    }

    public static Resources getResources(Context context, Locale locale) {
        Configuration configuration = new Configuration(context.getResources().getConfiguration());
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration).getResources();
    }
}
