package org.signal.core.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.MediaPreviewActivity;

/* compiled from: FontUtil.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0010\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007¨\u0006\b"}, d2 = {"Lorg/signal/core/util/FontUtil;", "", "", MediaPreviewActivity.SIZE_EXTRA, "", "canRenderEmojiAtFontSize", "<init>", "()V", "core-util_release"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes3.dex */
public final class FontUtil {
    public static final FontUtil INSTANCE = new FontUtil();

    private FontUtil() {
    }

    @JvmStatic
    public static final boolean canRenderEmojiAtFontSize(float f) {
        Bitmap createBitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
        Intrinsics.checkNotNullExpressionValue(createBitmap, "createBitmap(1, 1, Bitmap.Config.ARGB_8888)");
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        paint.setTextSize(f);
        paint.setTextAlign(Paint.Align.CENTER);
        float abs = Math.abs(paint.ascent());
        float abs2 = Math.abs(paint.descent());
        canvas.drawColor(0, PorterDuff.Mode.CLEAR);
        canvas.drawText("🌍", 0.5f, (((abs + abs2) / 2.0f) + 0.5f) - abs2, paint);
        if (createBitmap.getPixel(0, 0) != 0) {
            return true;
        }
        return false;
    }
}
