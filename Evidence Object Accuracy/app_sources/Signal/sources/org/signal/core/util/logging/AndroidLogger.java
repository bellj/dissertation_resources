package org.signal.core.util.logging;

import org.signal.core.util.logging.Log;

/* loaded from: classes.dex */
public final class AndroidLogger extends Log.Logger {
    @Override // org.signal.core.util.logging.Log.Logger
    public void flush() {
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void v(String str, String str2, Throwable th, boolean z) {
        android.util.Log.v(str, str2, th);
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void d(String str, String str2, Throwable th, boolean z) {
        android.util.Log.d(str, str2, th);
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void i(String str, String str2, Throwable th, boolean z) {
        android.util.Log.i(str, str2, th);
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void w(String str, String str2, Throwable th, boolean z) {
        android.util.Log.w(str, str2, th);
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void e(String str, String str2, Throwable th, boolean z) {
        android.util.Log.e(str, str2, th);
    }
}
