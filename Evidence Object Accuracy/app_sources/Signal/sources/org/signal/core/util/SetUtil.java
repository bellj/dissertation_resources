package org.signal.core.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/* loaded from: classes3.dex */
public final class SetUtil {
    public static <E> Set<E> intersection(Collection<E> collection, Collection<E> collection2) {
        LinkedHashSet linkedHashSet = new LinkedHashSet(collection);
        linkedHashSet.retainAll(collection2);
        return linkedHashSet;
    }

    public static <E> Set<E> difference(Collection<E> collection, Collection<E> collection2) {
        LinkedHashSet linkedHashSet = new LinkedHashSet(collection);
        linkedHashSet.removeAll(collection2);
        return linkedHashSet;
    }

    public static <E> Set<E> union(Set<E> set, Set<E> set2) {
        LinkedHashSet linkedHashSet = new LinkedHashSet(set);
        linkedHashSet.addAll(set2);
        return linkedHashSet;
    }

    @SafeVarargs
    public static <E> HashSet<E> newHashSet(E... eArr) {
        return new HashSet<>(Arrays.asList(eArr));
    }
}
