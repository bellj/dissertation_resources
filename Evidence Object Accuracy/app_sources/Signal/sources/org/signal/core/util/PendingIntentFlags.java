package org.signal.core.util;

import android.os.Build;
import kotlin.Metadata;
import net.zetetic.database.sqlcipher.SQLiteDatabase;

/* compiled from: PendingIntentFlags.kt */
@Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004J\u0006\u0010\u0005\u001a\u00020\u0004J\u0006\u0010\u0006\u001a\u00020\u0004¨\u0006\u0007"}, d2 = {"Lorg/signal/core/util/PendingIntentFlags;", "", "()V", "cancelCurrent", "", "mutable", "updateCurrent", "core-util_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class PendingIntentFlags {
    public static final PendingIntentFlags INSTANCE = new PendingIntentFlags();

    private PendingIntentFlags() {
    }

    public final int updateCurrent() {
        return mutable() | 134217728;
    }

    public final int cancelCurrent() {
        return mutable() | SQLiteDatabase.CREATE_IF_NECESSARY;
    }

    public final int mutable() {
        return Build.VERSION.SDK_INT >= 31 ? 33554432 : 0;
    }
}
