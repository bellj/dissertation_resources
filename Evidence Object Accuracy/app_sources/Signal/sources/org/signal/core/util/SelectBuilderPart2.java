package org.signal.core.util;

import android.database.Cursor;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteQueryBuilder;
import java.util.Arrays;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: SQLiteDatabaseExtensions.kt */
@Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0006¢\u0006\u0002\u0010\bJ\u0006\u0010\n\u001a\u00020\u000bJ'\u0010\f\u001a\u00020\r2\u0006\u0010\f\u001a\u00020\u00062\u0012\u0010\u000e\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00010\u0005\"\u00020\u0001¢\u0006\u0002\u0010\u000fR\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0004¢\u0006\u0004\n\u0002\u0010\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lorg/signal/core/util/SelectBuilderPart2;", "", "db", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "columns", "", "", "tableName", "(Landroidx/sqlite/db/SupportSQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;)V", "[Ljava/lang/String;", "run", "Landroid/database/Cursor;", "where", "Lorg/signal/core/util/SelectBuilderPart3;", "whereArgs", "(Ljava/lang/String;[Ljava/lang/Object;)Lorg/signal/core/util/SelectBuilderPart3;", "core-util_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class SelectBuilderPart2 {
    private final String[] columns;
    private final SupportSQLiteDatabase db;
    private final String tableName;

    public SelectBuilderPart2(SupportSQLiteDatabase supportSQLiteDatabase, String[] strArr, String str) {
        Intrinsics.checkNotNullParameter(supportSQLiteDatabase, "db");
        Intrinsics.checkNotNullParameter(strArr, "columns");
        Intrinsics.checkNotNullParameter(str, "tableName");
        this.db = supportSQLiteDatabase;
        this.columns = strArr;
        this.tableName = str;
    }

    public final SelectBuilderPart3 where(String str, Object... objArr) {
        Intrinsics.checkNotNullParameter(str, "where");
        Intrinsics.checkNotNullParameter(objArr, "whereArgs");
        return new SelectBuilderPart3(this.db, this.columns, this.tableName, str, SqlUtil.buildArgs(Arrays.copyOf(objArr, objArr.length)));
    }

    public final Cursor run() {
        Cursor query = this.db.query(SupportSQLiteQueryBuilder.builder(this.tableName).columns(this.columns).create());
        Intrinsics.checkNotNullExpressionValue(query, "db.query(\n      SupportS…)\n        .create()\n    )");
        return query;
    }
}
