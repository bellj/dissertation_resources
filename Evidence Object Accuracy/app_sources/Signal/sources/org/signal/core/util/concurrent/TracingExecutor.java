package org.signal.core.util.concurrent;

import java.lang.Thread;
import java.util.concurrent.Executor;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: TracingExecutor.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\b\b\u0000\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\u0006\u001a\u00020\u0001¢\u0006\u0004\b\n\u0010\u000bJ\u0012\u0010\u0005\u001a\u00020\u00042\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0016R\u0017\u0010\u0006\u001a\u00020\u00018\u0006¢\u0006\f\n\u0004\b\u0006\u0010\u0007\u001a\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lorg/signal/core/util/concurrent/TracingExecutor;", "Ljava/util/concurrent/Executor;", "Ljava/lang/Runnable;", "command", "", "execute", "wrapped", "Ljava/util/concurrent/Executor;", "getWrapped", "()Ljava/util/concurrent/Executor;", "<init>", "(Ljava/util/concurrent/Executor;)V", "core-util_release"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes3.dex */
public final class TracingExecutor implements Executor {
    private final Executor wrapped;

    public TracingExecutor(Executor executor) {
        Intrinsics.checkNotNullParameter(executor, "wrapped");
        this.wrapped = executor;
    }

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        this.wrapped.execute(new Runnable(new Throwable(), runnable) { // from class: org.signal.core.util.concurrent.TracingExecutor$$ExternalSyntheticLambda0
            public final /* synthetic */ Throwable f$0;
            public final /* synthetic */ Runnable f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                TracingExecutor.m184execute$lambda0(this.f$0, this.f$1);
            }
        });
    }

    /* renamed from: execute$lambda-0 */
    public static final void m184execute$lambda0(Throwable th, Runnable runnable) {
        Intrinsics.checkNotNullParameter(th, "$callerStackTrace");
        Thread currentThread = Thread.currentThread();
        Intrinsics.checkNotNullExpressionValue(currentThread, "currentThread()");
        Thread.UncaughtExceptionHandler uncaughtExceptionHandler = currentThread.getUncaughtExceptionHandler();
        if (uncaughtExceptionHandler instanceof TracingUncaughtExceptionHandler) {
            uncaughtExceptionHandler = ((TracingUncaughtExceptionHandler) uncaughtExceptionHandler).getOriginalHandler();
        }
        currentThread.setUncaughtExceptionHandler(new TracingUncaughtExceptionHandler(uncaughtExceptionHandler, th));
        TracedThreads tracedThreads = TracedThreads.INSTANCE;
        tracedThreads.getCallerStackTraces().put(Long.valueOf(currentThread.getId()), th);
        if (runnable != null) {
            try {
                runnable.run();
            } catch (Throwable th2) {
                TracedThreads.INSTANCE.getCallerStackTraces().remove(Long.valueOf(currentThread.getId()));
                throw th2;
            }
        }
        tracedThreads.getCallerStackTraces().remove(Long.valueOf(currentThread.getId()));
    }
}
