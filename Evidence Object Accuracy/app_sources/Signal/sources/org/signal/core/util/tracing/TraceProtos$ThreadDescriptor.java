package org.signal.core.util.tracing;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class TraceProtos$ThreadDescriptor extends GeneratedMessageLite<TraceProtos$ThreadDescriptor, Builder> implements MessageLiteOrBuilder {
    private static final TraceProtos$ThreadDescriptor DEFAULT_INSTANCE;
    private static volatile Parser<TraceProtos$ThreadDescriptor> PARSER;
    public static final int PID_FIELD_NUMBER;
    public static final int THREAD_NAME_FIELD_NUMBER;
    public static final int TID_FIELD_NUMBER;
    private int bitField0_;
    private int pid_;
    private String threadName_ = "";
    private int tid_;

    private TraceProtos$ThreadDescriptor() {
    }

    public boolean hasPid() {
        return (this.bitField0_ & 1) != 0;
    }

    public int getPid() {
        return this.pid_;
    }

    public void setPid(int i) {
        this.bitField0_ |= 1;
        this.pid_ = i;
    }

    public void clearPid() {
        this.bitField0_ &= -2;
        this.pid_ = 0;
    }

    public boolean hasTid() {
        return (this.bitField0_ & 2) != 0;
    }

    public int getTid() {
        return this.tid_;
    }

    public void setTid(int i) {
        this.bitField0_ |= 2;
        this.tid_ = i;
    }

    public void clearTid() {
        this.bitField0_ &= -3;
        this.tid_ = 0;
    }

    public boolean hasThreadName() {
        return (this.bitField0_ & 4) != 0;
    }

    public String getThreadName() {
        return this.threadName_;
    }

    public ByteString getThreadNameBytes() {
        return ByteString.copyFromUtf8(this.threadName_);
    }

    public void setThreadName(String str) {
        str.getClass();
        this.bitField0_ |= 4;
        this.threadName_ = str;
    }

    public void clearThreadName() {
        this.bitField0_ &= -5;
        this.threadName_ = getDefaultInstance().getThreadName();
    }

    public void setThreadNameBytes(ByteString byteString) {
        this.threadName_ = byteString.toStringUtf8();
        this.bitField0_ |= 4;
    }

    public static TraceProtos$ThreadDescriptor parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (TraceProtos$ThreadDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static TraceProtos$ThreadDescriptor parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TraceProtos$ThreadDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static TraceProtos$ThreadDescriptor parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (TraceProtos$ThreadDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static TraceProtos$ThreadDescriptor parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TraceProtos$ThreadDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static TraceProtos$ThreadDescriptor parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (TraceProtos$ThreadDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static TraceProtos$ThreadDescriptor parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TraceProtos$ThreadDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static TraceProtos$ThreadDescriptor parseFrom(InputStream inputStream) throws IOException {
        return (TraceProtos$ThreadDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static TraceProtos$ThreadDescriptor parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TraceProtos$ThreadDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static TraceProtos$ThreadDescriptor parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (TraceProtos$ThreadDescriptor) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static TraceProtos$ThreadDescriptor parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TraceProtos$ThreadDescriptor) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static TraceProtos$ThreadDescriptor parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (TraceProtos$ThreadDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static TraceProtos$ThreadDescriptor parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TraceProtos$ThreadDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(TraceProtos$ThreadDescriptor traceProtos$ThreadDescriptor) {
        return DEFAULT_INSTANCE.createBuilder(traceProtos$ThreadDescriptor);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<TraceProtos$ThreadDescriptor, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(TraceProtos$1 traceProtos$1) {
            this();
        }

        private Builder() {
            super(TraceProtos$ThreadDescriptor.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (TraceProtos$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new TraceProtos$ThreadDescriptor();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0003\u0000\u0001\u0001\u0005\u0003\u0000\u0000\u0000\u0001င\u0000\u0002င\u0001\u0005ဈ\u0002", new Object[]{"bitField0_", "pid_", "tid_", "threadName_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<TraceProtos$ThreadDescriptor> parser = PARSER;
                if (parser == null) {
                    synchronized (TraceProtos$ThreadDescriptor.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        TraceProtos$ThreadDescriptor traceProtos$ThreadDescriptor = new TraceProtos$ThreadDescriptor();
        DEFAULT_INSTANCE = traceProtos$ThreadDescriptor;
        GeneratedMessageLite.registerDefaultInstance(TraceProtos$ThreadDescriptor.class, traceProtos$ThreadDescriptor);
    }

    public static TraceProtos$ThreadDescriptor getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<TraceProtos$ThreadDescriptor> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
