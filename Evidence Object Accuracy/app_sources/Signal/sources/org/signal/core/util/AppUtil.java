package org.signal.core.util;

import android.content.Context;
import android.content.Intent;
import net.zetetic.database.sqlcipher.SQLiteDatabase;

/* loaded from: classes3.dex */
public final class AppUtil {
    public static void restart(Context context) {
        Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
        launchIntentForPackage.addFlags(SQLiteDatabase.CREATE_IF_NECESSARY);
        context.startActivity(launchIntentForPackage);
        Runtime.getRuntime().exit(0);
    }
}
