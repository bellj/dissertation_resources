package org.signal.core.util;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/* loaded from: classes3.dex */
public class ShakeDetector implements SensorEventListener {
    private Sensor accelerometer;
    private final Listener listener;
    private final SampleQueue queue = new SampleQueue();
    private SensorManager sensorManager;

    /* loaded from: classes.dex */
    public interface Listener {
        void onShakeDetected();
    }

    @Override // android.hardware.SensorEventListener
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public ShakeDetector(Listener listener) {
        this.listener = listener;
    }

    public boolean start(SensorManager sensorManager) {
        if (this.accelerometer != null) {
            return true;
        }
        Sensor defaultSensor = sensorManager.getDefaultSensor(1);
        this.accelerometer = defaultSensor;
        if (defaultSensor != null) {
            this.sensorManager = sensorManager;
            sensorManager.registerListener(this, defaultSensor, 0);
        }
        if (this.accelerometer != null) {
            return true;
        }
        return false;
    }

    public void stop() {
        if (this.accelerometer != null) {
            this.queue.clear();
            this.sensorManager.unregisterListener(this, this.accelerometer);
            this.sensorManager = null;
            this.accelerometer = null;
        }
    }

    @Override // android.hardware.SensorEventListener
    public void onSensorChanged(SensorEvent sensorEvent) {
        boolean isAccelerating = isAccelerating(sensorEvent);
        this.queue.add(sensorEvent.timestamp, isAccelerating);
        if (this.queue.isShaking()) {
            this.queue.clear();
            this.listener.onShakeDetected();
        }
    }

    private boolean isAccelerating(SensorEvent sensorEvent) {
        float[] fArr = sensorEvent.values;
        float f = fArr[0];
        float f2 = fArr[1];
        float f3 = fArr[2];
        if (((double) ((f * f) + (f2 * f2) + (f3 * f3))) > 169.0d) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes3.dex */
    public static class SampleQueue {
        private int acceleratingCount;
        private Sample newest;
        private Sample oldest;
        private final SamplePool pool = new SamplePool();
        private int sampleCount;

        SampleQueue() {
        }

        void add(long j, boolean z) {
            purge(j - 500000000);
            Sample acquire = this.pool.acquire();
            acquire.timestamp = j;
            acquire.accelerating = z;
            acquire.next = null;
            Sample sample = this.newest;
            if (sample != null) {
                sample.next = acquire;
            }
            this.newest = acquire;
            if (this.oldest == null) {
                this.oldest = acquire;
            }
            this.sampleCount++;
            if (z) {
                this.acceleratingCount++;
            }
        }

        void clear() {
            while (true) {
                Sample sample = this.oldest;
                if (sample != null) {
                    this.oldest = sample.next;
                    this.pool.release(sample);
                } else {
                    this.newest = null;
                    this.sampleCount = 0;
                    this.acceleratingCount = 0;
                    return;
                }
            }
        }

        void purge(long j) {
            Sample sample;
            while (true) {
                int i = this.sampleCount;
                if (i >= 4 && (sample = this.oldest) != null && j - sample.timestamp > 0) {
                    if (sample.accelerating) {
                        this.acceleratingCount--;
                    }
                    this.sampleCount = i - 1;
                    Sample sample2 = sample.next;
                    this.oldest = sample2;
                    if (sample2 == null) {
                        this.newest = null;
                    }
                    this.pool.release(sample);
                } else {
                    return;
                }
            }
        }

        boolean isShaking() {
            Sample sample;
            Sample sample2 = this.newest;
            if (!(sample2 == null || (sample = this.oldest) == null || sample2.timestamp - sample.timestamp < 250000000)) {
                int i = this.acceleratingCount;
                int i2 = this.sampleCount;
                if (i >= (i2 >> 1) + (i2 >> 2)) {
                    return true;
                }
            }
            return false;
        }
    }

    /* loaded from: classes3.dex */
    public static class Sample {
        boolean accelerating;
        Sample next;
        long timestamp;

        Sample() {
        }
    }

    /* loaded from: classes3.dex */
    public static class SamplePool {
        private Sample head;

        SamplePool() {
        }

        Sample acquire() {
            Sample sample = this.head;
            if (sample == null) {
                return new Sample();
            }
            this.head = sample.next;
            return sample;
        }

        void release(Sample sample) {
            sample.next = this.head;
            this.head = sample;
        }
    }
}
