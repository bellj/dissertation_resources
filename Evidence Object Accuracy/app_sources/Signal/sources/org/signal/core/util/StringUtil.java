package org.signal.core.util;

import android.text.TextUtils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Pattern;

/* loaded from: classes3.dex */
public final class StringUtil {
    private static final Pattern ALL_ASCII_PATTERN = Pattern.compile("^[\\x00-\\x7F]*$");
    private static final Set<Character> WHITESPACE = SetUtil.newHashSet((char) 8206, (char) 8207, (char) 8199, (char) 8203, (char) 10240);

    /* access modifiers changed from: private */
    /* loaded from: classes3.dex */
    public static final class Bidi {
        private static final int FSI = "⁨".codePointAt(0);
        private static final Set<Integer> ISOLATES = SetUtil.newHashSet(Integer.valueOf("⁦".codePointAt(0)), Integer.valueOf("⁧".codePointAt(0)), Integer.valueOf("⁨".codePointAt(0)));
        private static final Set<Integer> OVERRIDES = SetUtil.newHashSet(Integer.valueOf("‪".codePointAt(0)), Integer.valueOf("‫".codePointAt(0)), Integer.valueOf("‭".codePointAt(0)), Integer.valueOf("‮".codePointAt(0)));
        private static final int PDF = "‬".codePointAt(0);
        private static final int PDI = "⁩".codePointAt(0);
    }

    public static String trimToFit(String str, int i) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        if (str.getBytes(StandardCharsets.UTF_8).length <= i) {
            return str;
        }
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            Iterator<String> it = new CharacterIterable(str).iterator();
            while (it.hasNext()) {
                byte[] bytes = it.next().getBytes(StandardCharsets.UTF_8);
                if (byteArrayOutputStream.size() + bytes.length > i) {
                    break;
                }
                byteArrayOutputStream.write(bytes);
            }
            String byteArrayOutputStream2 = byteArrayOutputStream.toString();
            byteArrayOutputStream.close();
            return byteArrayOutputStream2;
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003a, code lost:
        if (r1 >= (r3.length() - 1)) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003d, code lost:
        return r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.CharSequence trim(java.lang.CharSequence r3) {
        /*
            int r0 = r3.length()
            if (r0 != 0) goto L_0x0007
            return r3
        L_0x0007:
            r0 = 0
            int r1 = r3.length()
            int r1 = r1 + -1
        L_0x000e:
            int r2 = r3.length()
            if (r0 >= r2) goto L_0x0021
            char r2 = r3.charAt(r0)
            boolean r2 = java.lang.Character.isWhitespace(r2)
            if (r2 == 0) goto L_0x0021
            int r0 = r0 + 1
            goto L_0x000e
        L_0x0021:
            if (r1 < 0) goto L_0x0032
            if (r1 <= r0) goto L_0x0032
            char r2 = r3.charAt(r1)
            boolean r2 = java.lang.Character.isWhitespace(r2)
            if (r2 == 0) goto L_0x0032
            int r1 = r1 + -1
            goto L_0x0021
        L_0x0032:
            if (r0 > 0) goto L_0x003e
            int r2 = r3.length()
            int r2 = r2 + -1
            if (r1 >= r2) goto L_0x003d
            goto L_0x003e
        L_0x003d:
            return r3
        L_0x003e:
            int r1 = r1 + 1
            java.lang.CharSequence r3 = r3.subSequence(r0, r1)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: org.signal.core.util.StringUtil.trim(java.lang.CharSequence):java.lang.CharSequence");
    }

    public static boolean isVisuallyEmpty(String str) {
        if (str == null || str.length() == 0 || indexOfFirstNonEmptyChar(str) == -1) {
            return true;
        }
        return false;
    }

    public static String trimToVisualBounds(String str) {
        int indexOfFirstNonEmptyChar = indexOfFirstNonEmptyChar(str);
        if (indexOfFirstNonEmptyChar == -1) {
            return "";
        }
        return str.substring(indexOfFirstNonEmptyChar, indexOfLastNonEmptyChar(str) + 1);
    }

    private static int indexOfFirstNonEmptyChar(String str) {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            if (!isVisuallyEmpty(str.charAt(i))) {
                return i;
            }
        }
        return -1;
    }

    private static int indexOfLastNonEmptyChar(String str) {
        for (int length = str.length() - 1; length >= 0; length--) {
            if (!isVisuallyEmpty(str.charAt(length))) {
                return length;
            }
        }
        return -1;
    }

    public static boolean isVisuallyEmpty(char c) {
        return Character.isWhitespace(c) || WHITESPACE.contains(Character.valueOf(c));
    }

    public static String codePointToString(int i) {
        return new String(Character.toChars(i));
    }

    public static boolean hasMixedTextDirection(CharSequence charSequence) {
        if (charSequence == null) {
            return false;
        }
        Boolean bool = null;
        int codePointCount = Character.codePointCount(charSequence, 0, charSequence.length());
        for (int i = 0; i < codePointCount; i++) {
            int codePointAt = Character.codePointAt(charSequence, i);
            byte directionality = Character.getDirectionality(codePointAt);
            boolean isLetter = Character.isLetter(codePointAt);
            boolean z = true;
            if (bool != null && bool.booleanValue() && directionality != 0 && isLetter) {
                return true;
            }
            if (!(bool == null || bool.booleanValue() || directionality == 1 || !isLetter)) {
                return true;
            }
            if (isLetter) {
                if (directionality != 0) {
                    z = false;
                }
                bool = Boolean.valueOf(z);
            }
        }
        return false;
    }

    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    public static String isolateBidi(String str) {
        if (str == null) {
            return "";
        }
        if (isEmpty(str)) {
            return str;
        }
        if (ALL_ASCII_PATTERN.matcher(str).matches()) {
            return str;
        }
        int codePointCount = str.codePointCount(0, str.length());
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        for (int i5 = 0; i5 < codePointCount; i5++) {
            int codePointAt = str.codePointAt(i5);
            if (Bidi.OVERRIDES.contains(Integer.valueOf(codePointAt))) {
                i++;
            } else if (codePointAt == Bidi.PDF) {
                i2++;
            } else if (Bidi.ISOLATES.contains(Integer.valueOf(codePointAt))) {
                i3++;
            } else if (codePointAt == Bidi.PDI) {
                i4++;
            }
        }
        StringBuilder sb = new StringBuilder();
        while (i > i2) {
            sb.appendCodePoint(Bidi.PDF);
            i2++;
        }
        while (i3 > i4) {
            sb.appendCodePoint(Bidi.FSI);
            i4++;
        }
        StringBuilder appendCodePoint = new StringBuilder().appendCodePoint(Bidi.FSI);
        appendCodePoint.append(str);
        appendCodePoint.append((CharSequence) sb);
        return appendCodePoint.appendCodePoint(Bidi.PDI).toString();
    }

    public static String stripBidiProtection(String str) {
        if (str == null) {
            return null;
        }
        return str.replaceAll("[\\u2068\\u2069\\u202c]", "");
    }

    public static String stripBidiIndicator(String str) {
        return str.replace("‏", "");
    }

    public static CharSequence trimSequence(CharSequence charSequence) {
        int length = charSequence.length();
        int i = 0;
        while (i < length && charSequence.charAt(i) <= ' ') {
            i++;
        }
        while (i < length && charSequence.charAt(length - 1) <= ' ') {
            length--;
        }
        return (i > 0 || length < charSequence.length()) ? charSequence.subSequence(i, length) : charSequence;
    }

    public static CharSequence abbreviateInMiddle(CharSequence charSequence, int i) {
        if (charSequence == null || charSequence.length() <= i) {
            return charSequence;
        }
        int i2 = i - 1;
        int i3 = i2 / 2;
        return ((Object) charSequence.subSequence(0, i3)) + "…" + ((Object) charSequence.subSequence(charSequence.length() - (i2 - i3), charSequence.length()));
    }

    public static int getGraphemeCount(CharSequence charSequence) {
        BreakIteratorCompat instance = BreakIteratorCompat.getInstance();
        instance.setText(charSequence);
        return instance.countBreaks();
    }
}
