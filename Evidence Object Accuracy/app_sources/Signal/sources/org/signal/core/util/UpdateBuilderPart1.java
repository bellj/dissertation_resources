package org.signal.core.util;

import android.content.ContentValues;
import androidx.core.content.ContentValuesKt;
import androidx.sqlite.db.SupportSQLiteDatabase;
import java.util.Arrays;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: SQLiteDatabaseExtensions.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\tJ;\u0010\u0007\u001a\u00020\b2.\u0010\u0007\u001a\u0018\u0012\u0014\b\u0001\u0012\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u000b0\n\"\u0010\u0012\u0004\u0012\u00020\u0005\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u000b¢\u0006\u0002\u0010\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/signal/core/util/UpdateBuilderPart1;", "", "db", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "tableName", "", "(Landroidx/sqlite/db/SupportSQLiteDatabase;Ljava/lang/String;)V", "values", "Lorg/signal/core/util/UpdateBuilderPart2;", "Landroid/content/ContentValues;", "", "Lkotlin/Pair;", "([Lkotlin/Pair;)Lorg/signal/core/util/UpdateBuilderPart2;", "core-util_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class UpdateBuilderPart1 {
    private final SupportSQLiteDatabase db;
    private final String tableName;

    public UpdateBuilderPart1(SupportSQLiteDatabase supportSQLiteDatabase, String str) {
        Intrinsics.checkNotNullParameter(supportSQLiteDatabase, "db");
        Intrinsics.checkNotNullParameter(str, "tableName");
        this.db = supportSQLiteDatabase;
        this.tableName = str;
    }

    public final UpdateBuilderPart2 values(ContentValues contentValues) {
        Intrinsics.checkNotNullParameter(contentValues, "values");
        return new UpdateBuilderPart2(this.db, this.tableName, contentValues);
    }

    public final UpdateBuilderPart2 values(Pair<String, ? extends Object>... pairArr) {
        Intrinsics.checkNotNullParameter(pairArr, "values");
        return new UpdateBuilderPart2(this.db, this.tableName, ContentValuesKt.contentValuesOf((Pair[]) Arrays.copyOf(pairArr, pairArr.length)));
    }
}
