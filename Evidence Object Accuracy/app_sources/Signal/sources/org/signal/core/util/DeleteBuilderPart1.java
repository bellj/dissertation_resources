package org.signal.core.util;

import androidx.sqlite.db.SupportSQLiteDatabase;
import java.util.Arrays;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: SQLiteDatabaseExtensions.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u0007\u001a\u00020\bJ'\u0010\t\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\u00052\u0012\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00010\f\"\u00020\u0001¢\u0006\u0002\u0010\rR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lorg/signal/core/util/DeleteBuilderPart1;", "", "db", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "tableName", "", "(Landroidx/sqlite/db/SupportSQLiteDatabase;Ljava/lang/String;)V", "run", "", "where", "Lorg/signal/core/util/DeleteBuilderPart2;", "whereArgs", "", "(Ljava/lang/String;[Ljava/lang/Object;)Lorg/signal/core/util/DeleteBuilderPart2;", "core-util_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class DeleteBuilderPart1 {
    private final SupportSQLiteDatabase db;
    private final String tableName;

    public DeleteBuilderPart1(SupportSQLiteDatabase supportSQLiteDatabase, String str) {
        Intrinsics.checkNotNullParameter(supportSQLiteDatabase, "db");
        Intrinsics.checkNotNullParameter(str, "tableName");
        this.db = supportSQLiteDatabase;
        this.tableName = str;
    }

    public final DeleteBuilderPart2 where(String str, Object... objArr) {
        Intrinsics.checkNotNullParameter(str, "where");
        Intrinsics.checkNotNullParameter(objArr, "whereArgs");
        return new DeleteBuilderPart2(this.db, this.tableName, str, SqlUtil.buildArgs(Arrays.copyOf(objArr, objArr.length)));
    }

    public final int run() {
        return this.db.delete(this.tableName, null, null);
    }
}
