package org.signal.core.util.concurrent;

import androidx.lifecycle.Lifecycle;
import java.util.concurrent.Executor;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.concurrent.SimpleTask;

/* loaded from: classes3.dex */
public class SimpleTask {

    /* loaded from: classes3.dex */
    public interface BackgroundTask<E> {
        E run();
    }

    /* loaded from: classes3.dex */
    public interface ForegroundTask<E> {
        void run(E e);
    }

    public static <E> void run(Lifecycle lifecycle, BackgroundTask<E> backgroundTask, ForegroundTask<E> foregroundTask) {
        if (isValid(lifecycle)) {
            SignalExecutors.BOUNDED.execute(new Runnable(lifecycle, foregroundTask) { // from class: org.signal.core.util.concurrent.SimpleTask$$ExternalSyntheticLambda1
                public final /* synthetic */ Lifecycle f$1;
                public final /* synthetic */ SimpleTask.ForegroundTask f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    SimpleTask.$r8$lambda$apmfpgqoFpbYCMY0RJkcKP8U8Hw(SimpleTask.BackgroundTask.this, this.f$1, this.f$2);
                }
            });
        }
    }

    public static /* synthetic */ void lambda$run$1(BackgroundTask backgroundTask, Lifecycle lifecycle, ForegroundTask foregroundTask) {
        Object run = backgroundTask.run();
        if (isValid(lifecycle)) {
            ThreadUtil.runOnMain(new Runnable(foregroundTask, run) { // from class: org.signal.core.util.concurrent.SimpleTask$$ExternalSyntheticLambda3
                public final /* synthetic */ SimpleTask.ForegroundTask f$1;
                public final /* synthetic */ Object f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    SimpleTask.$r8$lambda$lF69t8y3DMy8ar7C2ujewLrrhTs(Lifecycle.this, this.f$1, this.f$2);
                }
            });
        }
    }

    public static /* synthetic */ void lambda$run$0(Lifecycle lifecycle, ForegroundTask foregroundTask, Object obj) {
        if (isValid(lifecycle)) {
            foregroundTask.run(obj);
        }
    }

    public static <E> void run(BackgroundTask<E> backgroundTask, ForegroundTask<E> foregroundTask) {
        run(SignalExecutors.BOUNDED, backgroundTask, foregroundTask);
    }

    public static <E> void run(Executor executor, BackgroundTask<E> backgroundTask, ForegroundTask<E> foregroundTask) {
        executor.execute(new Runnable(foregroundTask) { // from class: org.signal.core.util.concurrent.SimpleTask$$ExternalSyntheticLambda0
            public final /* synthetic */ SimpleTask.ForegroundTask f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SimpleTask.m183$r8$lambda$jvZ08nsHlQ_qvURex9q6uvOXs(SimpleTask.BackgroundTask.this, this.f$1);
            }
        });
    }

    public static /* synthetic */ void lambda$run$3(BackgroundTask backgroundTask, ForegroundTask foregroundTask) {
        ThreadUtil.runOnMain(new Runnable(backgroundTask.run()) { // from class: org.signal.core.util.concurrent.SimpleTask$$ExternalSyntheticLambda2
            public final /* synthetic */ Object f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SimpleTask.m182$r8$lambda$5EnOtL_RDkT3AhG5joH_4sUBmQ(SimpleTask.ForegroundTask.this, this.f$1);
            }
        });
    }

    private static boolean isValid(Lifecycle lifecycle) {
        return lifecycle.getCurrentState().isAtLeast(Lifecycle.State.CREATED);
    }
}
