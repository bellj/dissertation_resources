package org.signal.core.util;

import java.io.IOException;

/* loaded from: classes3.dex */
public class Hex {
    static final String EOL = System.getProperty("line.separator");
    private static final char[] HEX_DIGITS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public static String toString(byte[] bArr) {
        return toString(bArr, 0, bArr.length);
    }

    public static String toString(byte[] bArr, int i, int i2) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i3 = 0; i3 < i2; i3++) {
            appendHexChar(stringBuffer, bArr[i + i3]);
            stringBuffer.append(' ');
        }
        return stringBuffer.toString();
    }

    public static String toStringCondensed(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b : bArr) {
            appendHexChar(stringBuffer, b);
        }
        return stringBuffer.toString();
    }

    public static byte[] fromStringCondensed(String str) throws IOException {
        char[] charArray = str.toCharArray();
        int length = charArray.length;
        if ((length & 1) == 0) {
            byte[] bArr = new byte[length >> 1];
            int i = 0;
            int i2 = 0;
            while (i < length) {
                int i3 = i + 1;
                i = i3 + 1;
                bArr[i2] = (byte) (((Character.digit(charArray[i], 16) << 4) | Character.digit(charArray[i3], 16)) & 255);
                i2++;
            }
            return bArr;
        }
        throw new IOException("Odd number of characters.");
    }

    public static byte[] fromStringOrThrow(String str) {
        try {
            return fromStringCondensed(str);
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    private static void appendHexChar(StringBuffer stringBuffer, int i) {
        char[] cArr = HEX_DIGITS;
        stringBuffer.append(cArr[(i >> 4) & 15]);
        stringBuffer.append(cArr[i & 15]);
    }
}
