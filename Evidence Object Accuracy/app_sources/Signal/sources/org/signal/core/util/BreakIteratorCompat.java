package org.signal.core.util;

import android.icu.text.BreakIterator;
import android.os.Build;
import java.util.Iterator;

/* loaded from: classes3.dex */
public abstract class BreakIteratorCompat implements Iterable<CharSequence> {
    private CharSequence charSequence;

    public abstract int first();

    public abstract int next();

    public void setText(CharSequence charSequence) {
        this.charSequence = charSequence;
    }

    public static BreakIteratorCompat getInstance() {
        if (Build.VERSION.SDK_INT >= 24) {
            return new AndroidIcuBreakIterator();
        }
        return new FallbackBreakIterator();
    }

    public int countBreaks() {
        first();
        int i = 0;
        while (next() != -1) {
            i++;
        }
        return i;
    }

    @Override // java.lang.Iterable
    public Iterator<CharSequence> iterator() {
        return new Iterator<CharSequence>() { // from class: org.signal.core.util.BreakIteratorCompat.1
            int index1;
            int index2;

            {
                this.index1 = r2.first();
                this.index2 = r2.next();
            }

            @Override // java.util.Iterator
            public boolean hasNext() {
                return this.index2 != -1;
            }

            @Override // java.util.Iterator
            public CharSequence next() {
                String subSequence = this.index2 != -1 ? BreakIteratorCompat.this.charSequence.subSequence(this.index1, this.index2) : "";
                this.index1 = this.index2;
                this.index2 = BreakIteratorCompat.this.next();
                return subSequence;
            }
        };
    }

    public final CharSequence take(int i) {
        if (i <= 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(this.charSequence.length());
        int i2 = 0;
        Iterator<CharSequence> it = iterator();
        while (it.hasNext()) {
            sb.append(it.next());
            i2++;
            if (i2 >= i) {
                break;
            }
        }
        return sb.toString();
    }

    /* access modifiers changed from: private */
    /* loaded from: classes3.dex */
    public static class AndroidIcuBreakIterator extends BreakIteratorCompat {
        private final BreakIterator breakIterator;

        private AndroidIcuBreakIterator() {
            this.breakIterator = BreakIterator.getCharacterInstance();
        }

        @Override // org.signal.core.util.BreakIteratorCompat
        public int first() {
            return this.breakIterator.first();
        }

        @Override // org.signal.core.util.BreakIteratorCompat
        public int next() {
            return this.breakIterator.next();
        }

        @Override // org.signal.core.util.BreakIteratorCompat
        public void setText(CharSequence charSequence) {
            BreakIteratorCompat.super.setText(charSequence);
            if (Build.VERSION.SDK_INT >= 29) {
                this.breakIterator.setText(charSequence);
            } else {
                this.breakIterator.setText(charSequence.toString());
            }
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes3.dex */
    public static class FallbackBreakIterator extends BreakIteratorCompat {
        private final java.text.BreakIterator breakIterator;

        private FallbackBreakIterator() {
            this.breakIterator = java.text.BreakIterator.getCharacterInstance();
        }

        @Override // org.signal.core.util.BreakIteratorCompat
        public int first() {
            return this.breakIterator.first();
        }

        @Override // org.signal.core.util.BreakIteratorCompat
        public int next() {
            return this.breakIterator.next();
        }

        @Override // org.signal.core.util.BreakIteratorCompat
        public void setText(CharSequence charSequence) {
            BreakIteratorCompat.super.setText(charSequence);
            this.breakIterator.setText(charSequence.toString());
        }
    }
}
