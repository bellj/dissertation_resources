package org.signal.core.util;

import androidx.sqlite.db.SupportSQLiteDatabase;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: SQLiteDatabaseExtensions.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0006R\u0016\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0004¢\u0006\u0004\n\u0002\u0010\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/signal/core/util/SelectBuilderPart1;", "", "db", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "columns", "", "", "(Landroidx/sqlite/db/SupportSQLiteDatabase;[Ljava/lang/String;)V", "[Ljava/lang/String;", "from", "Lorg/signal/core/util/SelectBuilderPart2;", "tableName", "core-util_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class SelectBuilderPart1 {
    private final String[] columns;
    private final SupportSQLiteDatabase db;

    public SelectBuilderPart1(SupportSQLiteDatabase supportSQLiteDatabase, String[] strArr) {
        Intrinsics.checkNotNullParameter(supportSQLiteDatabase, "db");
        Intrinsics.checkNotNullParameter(strArr, "columns");
        this.db = supportSQLiteDatabase;
        this.columns = strArr;
    }

    public final SelectBuilderPart2 from(String str) {
        Intrinsics.checkNotNullParameter(str, "tableName");
        return new SelectBuilderPart2(this.db, this.columns, str);
    }
}
