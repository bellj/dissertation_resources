package org.signal.core.util.tracing;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class TraceProtos$TrackEvent extends GeneratedMessageLite<TraceProtos$TrackEvent, Builder> implements MessageLiteOrBuilder {
    public static final int CATEGORIES_FIELD_NUMBER;
    public static final int CATEGORY_IIDS_FIELD_NUMBER;
    public static final int COUNTER_VALUE_FIELD_NUMBER;
    public static final int DEBUG_ANNOTATIONS_FIELD_NUMBER;
    private static final TraceProtos$TrackEvent DEFAULT_INSTANCE;
    public static final int NAME_FIELD_NUMBER;
    public static final int NAME_IID_FIELD_NUMBER;
    private static volatile Parser<TraceProtos$TrackEvent> PARSER;
    public static final int THREAD_TIME_ABSOLUTE_US_FIELD_NUMBER;
    public static final int THREAD_TIME_DELTA_US_FIELD_NUMBER;
    public static final int TIMESTAMP_ABSOLUTE_US_FIELD_NUMBER;
    public static final int TIMESTAMP_DELTA_US_FIELD_NUMBER;
    public static final int TRACK_UUID_FIELD_NUMBER;
    public static final int TYPE_FIELD_NUMBER;
    private int bitField0_;
    private Internal.ProtobufList<String> categories_ = GeneratedMessageLite.emptyProtobufList();
    private Internal.LongList categoryIids_ = GeneratedMessageLite.emptyLongList();
    private long counterValue_;
    private Internal.ProtobufList<TraceProtos$DebugAnnotation> debugAnnotations_ = GeneratedMessageLite.emptyProtobufList();
    private int nameFieldCase_ = 0;
    private Object nameField_;
    private int threadTimeCase_ = 0;
    private Object threadTime_;
    private int timestampCase_ = 0;
    private Object timestamp_;
    private long trackUuid_;
    private int type_;

    private TraceProtos$TrackEvent() {
    }

    /* loaded from: classes3.dex */
    public enum Type implements Internal.EnumLite {
        TYPE_UNSPECIFIED(0),
        TYPE_SLICE_BEGIN(1),
        TYPE_SLICE_END(2),
        TYPE_INSTANT(3),
        TYPE_COUNTER(4);
        
        private static final Internal.EnumLiteMap<Type> internalValueMap = new Internal.EnumLiteMap<Type>() { // from class: org.signal.core.util.tracing.TraceProtos.TrackEvent.Type.1
            @Override // com.google.protobuf.Internal.EnumLiteMap
            public Type findValueByNumber(int i) {
                return Type.forNumber(i);
            }
        };
        private final int value;

        @Override // com.google.protobuf.Internal.EnumLite
        public final int getNumber() {
            return this.value;
        }

        public static Type forNumber(int i) {
            if (i == 0) {
                return TYPE_UNSPECIFIED;
            }
            if (i == 1) {
                return TYPE_SLICE_BEGIN;
            }
            if (i == 2) {
                return TYPE_SLICE_END;
            }
            if (i == 3) {
                return TYPE_INSTANT;
            }
            if (i != 4) {
                return null;
            }
            return TYPE_COUNTER;
        }

        public static Internal.EnumVerifier internalGetVerifier() {
            return TypeVerifier.INSTANCE;
        }

        /* loaded from: classes3.dex */
        public static final class TypeVerifier implements Internal.EnumVerifier {
            static final Internal.EnumVerifier INSTANCE = new TypeVerifier();

            private TypeVerifier() {
            }

            @Override // com.google.protobuf.Internal.EnumVerifier
            public boolean isInRange(int i) {
                return Type.forNumber(i) != null;
            }
        }

        Type(int i) {
            this.value = i;
        }
    }

    /* loaded from: classes3.dex */
    public enum NameFieldCase {
        NAME_IID(10),
        NAME(23),
        NAMEFIELD_NOT_SET(0);
        
        private final int value;

        NameFieldCase(int i) {
            this.value = i;
        }

        public static NameFieldCase forNumber(int i) {
            if (i == 0) {
                return NAMEFIELD_NOT_SET;
            }
            if (i == 10) {
                return NAME_IID;
            }
            if (i != 23) {
                return null;
            }
            return NAME;
        }
    }

    public NameFieldCase getNameFieldCase() {
        return NameFieldCase.forNumber(this.nameFieldCase_);
    }

    public void clearNameField() {
        this.nameFieldCase_ = 0;
        this.nameField_ = null;
    }

    /* loaded from: classes3.dex */
    public enum TimestampCase {
        TIMESTAMP_DELTA_US(1),
        TIMESTAMP_ABSOLUTE_US(16),
        TIMESTAMP_NOT_SET(0);
        
        private final int value;

        TimestampCase(int i) {
            this.value = i;
        }

        public static TimestampCase forNumber(int i) {
            if (i == 0) {
                return TIMESTAMP_NOT_SET;
            }
            if (i == 1) {
                return TIMESTAMP_DELTA_US;
            }
            if (i != 16) {
                return null;
            }
            return TIMESTAMP_ABSOLUTE_US;
        }
    }

    public TimestampCase getTimestampCase() {
        return TimestampCase.forNumber(this.timestampCase_);
    }

    public void clearTimestamp() {
        this.timestampCase_ = 0;
        this.timestamp_ = null;
    }

    /* loaded from: classes3.dex */
    public enum ThreadTimeCase {
        THREAD_TIME_DELTA_US(2),
        THREAD_TIME_ABSOLUTE_US(17),
        THREADTIME_NOT_SET(0);
        
        private final int value;

        ThreadTimeCase(int i) {
            this.value = i;
        }

        public static ThreadTimeCase forNumber(int i) {
            if (i == 0) {
                return THREADTIME_NOT_SET;
            }
            if (i == 2) {
                return THREAD_TIME_DELTA_US;
            }
            if (i != 17) {
                return null;
            }
            return THREAD_TIME_ABSOLUTE_US;
        }
    }

    public ThreadTimeCase getThreadTimeCase() {
        return ThreadTimeCase.forNumber(this.threadTimeCase_);
    }

    public void clearThreadTime() {
        this.threadTimeCase_ = 0;
        this.threadTime_ = null;
    }

    public List<Long> getCategoryIidsList() {
        return this.categoryIids_;
    }

    public int getCategoryIidsCount() {
        return this.categoryIids_.size();
    }

    public long getCategoryIids(int i) {
        return this.categoryIids_.getLong(i);
    }

    private void ensureCategoryIidsIsMutable() {
        Internal.LongList longList = this.categoryIids_;
        if (!longList.isModifiable()) {
            this.categoryIids_ = GeneratedMessageLite.mutableCopy(longList);
        }
    }

    public void setCategoryIids(int i, long j) {
        ensureCategoryIidsIsMutable();
        this.categoryIids_.setLong(i, j);
    }

    public void addCategoryIids(long j) {
        ensureCategoryIidsIsMutable();
        this.categoryIids_.addLong(j);
    }

    public void addAllCategoryIids(Iterable<? extends Long> iterable) {
        ensureCategoryIidsIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.categoryIids_);
    }

    public void clearCategoryIids() {
        this.categoryIids_ = GeneratedMessageLite.emptyLongList();
    }

    public List<String> getCategoriesList() {
        return this.categories_;
    }

    public int getCategoriesCount() {
        return this.categories_.size();
    }

    public String getCategories(int i) {
        return this.categories_.get(i);
    }

    public ByteString getCategoriesBytes(int i) {
        return ByteString.copyFromUtf8(this.categories_.get(i));
    }

    private void ensureCategoriesIsMutable() {
        Internal.ProtobufList<String> protobufList = this.categories_;
        if (!protobufList.isModifiable()) {
            this.categories_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setCategories(int i, String str) {
        str.getClass();
        ensureCategoriesIsMutable();
        this.categories_.set(i, str);
    }

    public void addCategories(String str) {
        str.getClass();
        ensureCategoriesIsMutable();
        this.categories_.add(str);
    }

    public void addAllCategories(Iterable<String> iterable) {
        ensureCategoriesIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.categories_);
    }

    public void clearCategories() {
        this.categories_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void addCategoriesBytes(ByteString byteString) {
        ensureCategoriesIsMutable();
        this.categories_.add(byteString.toStringUtf8());
    }

    public List<TraceProtos$DebugAnnotation> getDebugAnnotationsList() {
        return this.debugAnnotations_;
    }

    public List<? extends TraceProtos$DebugAnnotationOrBuilder> getDebugAnnotationsOrBuilderList() {
        return this.debugAnnotations_;
    }

    public int getDebugAnnotationsCount() {
        return this.debugAnnotations_.size();
    }

    public TraceProtos$DebugAnnotation getDebugAnnotations(int i) {
        return this.debugAnnotations_.get(i);
    }

    public TraceProtos$DebugAnnotationOrBuilder getDebugAnnotationsOrBuilder(int i) {
        return this.debugAnnotations_.get(i);
    }

    private void ensureDebugAnnotationsIsMutable() {
        Internal.ProtobufList<TraceProtos$DebugAnnotation> protobufList = this.debugAnnotations_;
        if (!protobufList.isModifiable()) {
            this.debugAnnotations_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setDebugAnnotations(int i, TraceProtos$DebugAnnotation traceProtos$DebugAnnotation) {
        traceProtos$DebugAnnotation.getClass();
        ensureDebugAnnotationsIsMutable();
        this.debugAnnotations_.set(i, traceProtos$DebugAnnotation);
    }

    public void addDebugAnnotations(TraceProtos$DebugAnnotation traceProtos$DebugAnnotation) {
        traceProtos$DebugAnnotation.getClass();
        ensureDebugAnnotationsIsMutable();
        this.debugAnnotations_.add(traceProtos$DebugAnnotation);
    }

    public void addDebugAnnotations(int i, TraceProtos$DebugAnnotation traceProtos$DebugAnnotation) {
        traceProtos$DebugAnnotation.getClass();
        ensureDebugAnnotationsIsMutable();
        this.debugAnnotations_.add(i, traceProtos$DebugAnnotation);
    }

    public void addAllDebugAnnotations(Iterable<? extends TraceProtos$DebugAnnotation> iterable) {
        ensureDebugAnnotationsIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.debugAnnotations_);
    }

    public void clearDebugAnnotations() {
        this.debugAnnotations_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeDebugAnnotations(int i) {
        ensureDebugAnnotationsIsMutable();
        this.debugAnnotations_.remove(i);
    }

    public boolean hasNameIid() {
        return this.nameFieldCase_ == 10;
    }

    public long getNameIid() {
        if (this.nameFieldCase_ == 10) {
            return ((Long) this.nameField_).longValue();
        }
        return 0;
    }

    public void setNameIid(long j) {
        this.nameFieldCase_ = 10;
        this.nameField_ = Long.valueOf(j);
    }

    public void clearNameIid() {
        if (this.nameFieldCase_ == 10) {
            this.nameFieldCase_ = 0;
            this.nameField_ = null;
        }
    }

    public boolean hasName() {
        return this.nameFieldCase_ == 23;
    }

    public String getName() {
        return this.nameFieldCase_ == 23 ? (String) this.nameField_ : "";
    }

    public ByteString getNameBytes() {
        return ByteString.copyFromUtf8(this.nameFieldCase_ == 23 ? (String) this.nameField_ : "");
    }

    public void setName(String str) {
        str.getClass();
        this.nameFieldCase_ = 23;
        this.nameField_ = str;
    }

    public void clearName() {
        if (this.nameFieldCase_ == 23) {
            this.nameFieldCase_ = 0;
            this.nameField_ = null;
        }
    }

    public void setNameBytes(ByteString byteString) {
        this.nameField_ = byteString.toStringUtf8();
        this.nameFieldCase_ = 23;
    }

    public boolean hasType() {
        return (this.bitField0_ & 4) != 0;
    }

    public Type getType() {
        Type forNumber = Type.forNumber(this.type_);
        return forNumber == null ? Type.TYPE_UNSPECIFIED : forNumber;
    }

    public void setType(Type type) {
        this.type_ = type.getNumber();
        this.bitField0_ |= 4;
    }

    public void clearType() {
        this.bitField0_ &= -5;
        this.type_ = 0;
    }

    public boolean hasTrackUuid() {
        return (this.bitField0_ & 8) != 0;
    }

    public long getTrackUuid() {
        return this.trackUuid_;
    }

    public void setTrackUuid(long j) {
        this.bitField0_ |= 8;
        this.trackUuid_ = j;
    }

    public void clearTrackUuid() {
        this.bitField0_ &= -9;
        this.trackUuid_ = 0;
    }

    public boolean hasCounterValue() {
        return (this.bitField0_ & 16) != 0;
    }

    public long getCounterValue() {
        return this.counterValue_;
    }

    public void setCounterValue(long j) {
        this.bitField0_ |= 16;
        this.counterValue_ = j;
    }

    public void clearCounterValue() {
        this.bitField0_ &= -17;
        this.counterValue_ = 0;
    }

    public boolean hasTimestampDeltaUs() {
        return this.timestampCase_ == 1;
    }

    public long getTimestampDeltaUs() {
        if (this.timestampCase_ == 1) {
            return ((Long) this.timestamp_).longValue();
        }
        return 0;
    }

    public void setTimestampDeltaUs(long j) {
        this.timestampCase_ = 1;
        this.timestamp_ = Long.valueOf(j);
    }

    public void clearTimestampDeltaUs() {
        if (this.timestampCase_ == 1) {
            this.timestampCase_ = 0;
            this.timestamp_ = null;
        }
    }

    public boolean hasTimestampAbsoluteUs() {
        return this.timestampCase_ == 16;
    }

    public long getTimestampAbsoluteUs() {
        if (this.timestampCase_ == 16) {
            return ((Long) this.timestamp_).longValue();
        }
        return 0;
    }

    public void setTimestampAbsoluteUs(long j) {
        this.timestampCase_ = 16;
        this.timestamp_ = Long.valueOf(j);
    }

    public void clearTimestampAbsoluteUs() {
        if (this.timestampCase_ == 16) {
            this.timestampCase_ = 0;
            this.timestamp_ = null;
        }
    }

    public boolean hasThreadTimeDeltaUs() {
        return this.threadTimeCase_ == 2;
    }

    public long getThreadTimeDeltaUs() {
        if (this.threadTimeCase_ == 2) {
            return ((Long) this.threadTime_).longValue();
        }
        return 0;
    }

    public void setThreadTimeDeltaUs(long j) {
        this.threadTimeCase_ = 2;
        this.threadTime_ = Long.valueOf(j);
    }

    public void clearThreadTimeDeltaUs() {
        if (this.threadTimeCase_ == 2) {
            this.threadTimeCase_ = 0;
            this.threadTime_ = null;
        }
    }

    public boolean hasThreadTimeAbsoluteUs() {
        return this.threadTimeCase_ == 17;
    }

    public long getThreadTimeAbsoluteUs() {
        if (this.threadTimeCase_ == 17) {
            return ((Long) this.threadTime_).longValue();
        }
        return 0;
    }

    public void setThreadTimeAbsoluteUs(long j) {
        this.threadTimeCase_ = 17;
        this.threadTime_ = Long.valueOf(j);
    }

    public void clearThreadTimeAbsoluteUs() {
        if (this.threadTimeCase_ == 17) {
            this.threadTimeCase_ = 0;
            this.threadTime_ = null;
        }
    }

    public static TraceProtos$TrackEvent parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (TraceProtos$TrackEvent) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static TraceProtos$TrackEvent parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TraceProtos$TrackEvent) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static TraceProtos$TrackEvent parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (TraceProtos$TrackEvent) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static TraceProtos$TrackEvent parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TraceProtos$TrackEvent) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static TraceProtos$TrackEvent parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (TraceProtos$TrackEvent) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static TraceProtos$TrackEvent parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TraceProtos$TrackEvent) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static TraceProtos$TrackEvent parseFrom(InputStream inputStream) throws IOException {
        return (TraceProtos$TrackEvent) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static TraceProtos$TrackEvent parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TraceProtos$TrackEvent) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static TraceProtos$TrackEvent parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (TraceProtos$TrackEvent) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static TraceProtos$TrackEvent parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TraceProtos$TrackEvent) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static TraceProtos$TrackEvent parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (TraceProtos$TrackEvent) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static TraceProtos$TrackEvent parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TraceProtos$TrackEvent) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(TraceProtos$TrackEvent traceProtos$TrackEvent) {
        return DEFAULT_INSTANCE.createBuilder(traceProtos$TrackEvent);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<TraceProtos$TrackEvent, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(TraceProtos$1 traceProtos$1) {
            this();
        }

        private Builder() {
            super(TraceProtos$TrackEvent.DEFAULT_INSTANCE);
        }

        public Builder addDebugAnnotations(TraceProtos$DebugAnnotation traceProtos$DebugAnnotation) {
            copyOnWrite();
            ((TraceProtos$TrackEvent) this.instance).addDebugAnnotations(traceProtos$DebugAnnotation);
            return this;
        }

        public Builder setName(String str) {
            copyOnWrite();
            ((TraceProtos$TrackEvent) this.instance).setName(str);
            return this;
        }

        public Builder setType(Type type) {
            copyOnWrite();
            ((TraceProtos$TrackEvent) this.instance).setType(type);
            return this;
        }

        public Builder setTrackUuid(long j) {
            copyOnWrite();
            ((TraceProtos$TrackEvent) this.instance).setTrackUuid(j);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (TraceProtos$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new TraceProtos$TrackEvent();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\f\u0003\u0001\u0001\u001e\f\u0000\u0003\u0000\u0001ဵ\u0001\u0002ဵ\u0002\u0003\u0015\u0004\u001b\tဌ\u0002\nံ\u0000\u000bဃ\u0003\u0010ဵ\u0001\u0011ဵ\u0002\u0016\u001a\u0017ျ\u0000\u001eဂ\u0004", new Object[]{"nameField_", "nameFieldCase_", "timestamp_", "timestampCase_", "threadTime_", "threadTimeCase_", "bitField0_", "categoryIids_", "debugAnnotations_", TraceProtos$DebugAnnotation.class, "type_", Type.internalGetVerifier(), "trackUuid_", "categories_", "counterValue_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<TraceProtos$TrackEvent> parser = PARSER;
                if (parser == null) {
                    synchronized (TraceProtos$TrackEvent.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        TraceProtos$TrackEvent traceProtos$TrackEvent = new TraceProtos$TrackEvent();
        DEFAULT_INSTANCE = traceProtos$TrackEvent;
        GeneratedMessageLite.registerDefaultInstance(TraceProtos$TrackEvent.class, traceProtos$TrackEvent);
    }

    public static TraceProtos$TrackEvent getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<TraceProtos$TrackEvent> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
