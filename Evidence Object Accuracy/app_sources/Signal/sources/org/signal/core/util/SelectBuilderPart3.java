package org.signal.core.util;

import android.database.Cursor;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteQueryBuilder;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: SQLiteDatabaseExtensions.kt */
@Metadata(bv = {}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\t\u0018\u00002\u00020\u0001B;\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00020\n\u0012\u0006\u0010\r\u001a\u00020\u0002\u0012\u0006\u0010\u000f\u001a\u00020\u0002\u0012\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00020\n¢\u0006\u0004\b\u0011\u0010\u0012J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002J\u0006\u0010\u0006\u001a\u00020\u0005R\u0014\u0010\b\u001a\u00020\u00078\u0002X\u0004¢\u0006\u0006\n\u0004\b\b\u0010\tR\u001a\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00020\n8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0014\u0010\r\u001a\u00020\u00028\u0002X\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0014\u0010\u000f\u001a\u00020\u00028\u0002X\u0004¢\u0006\u0006\n\u0004\b\u000f\u0010\u000eR\u001a\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00020\n8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\f¨\u0006\u0013"}, d2 = {"Lorg/signal/core/util/SelectBuilderPart3;", "", "", "orderBy", "Lorg/signal/core/util/SelectBuilderPart4a;", "Landroid/database/Cursor;", "run", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "db", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "", "columns", "[Ljava/lang/String;", "tableName", "Ljava/lang/String;", "where", "whereArgs", "<init>", "(Landroidx/sqlite/db/SupportSQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V", "core-util_release"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes3.dex */
public final class SelectBuilderPart3 {
    private final String[] columns;
    private final SupportSQLiteDatabase db;
    private final String tableName;
    private final String where;
    private final String[] whereArgs;

    public SelectBuilderPart3(SupportSQLiteDatabase supportSQLiteDatabase, String[] strArr, String str, String str2, String[] strArr2) {
        Intrinsics.checkNotNullParameter(supportSQLiteDatabase, "db");
        Intrinsics.checkNotNullParameter(strArr, "columns");
        Intrinsics.checkNotNullParameter(str, "tableName");
        Intrinsics.checkNotNullParameter(str2, "where");
        Intrinsics.checkNotNullParameter(strArr2, "whereArgs");
        this.db = supportSQLiteDatabase;
        this.columns = strArr;
        this.tableName = str;
        this.where = str2;
        this.whereArgs = strArr2;
    }

    public final SelectBuilderPart4a orderBy(String str) {
        Intrinsics.checkNotNullParameter(str, "orderBy");
        return new SelectBuilderPart4a(this.db, this.columns, this.tableName, this.where, this.whereArgs, str);
    }

    public final Cursor run() {
        Cursor query = this.db.query(SupportSQLiteQueryBuilder.builder(this.tableName).columns(this.columns).selection(this.where, this.whereArgs).create());
        Intrinsics.checkNotNullExpressionValue(query, "db.query(\n      SupportS…)\n        .create()\n    )");
        return query;
    }
}
