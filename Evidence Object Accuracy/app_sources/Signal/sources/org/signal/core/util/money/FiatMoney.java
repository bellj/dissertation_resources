package org.signal.core.util.money;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;
import java.util.Objects;

/* loaded from: classes3.dex */
public class FiatMoney {
    private final BigDecimal amount;
    private final Currency currency;
    private final long timestamp;

    public FiatMoney(BigDecimal bigDecimal, Currency currency) {
        this(bigDecimal, currency, 0);
    }

    public FiatMoney(BigDecimal bigDecimal, Currency currency, long j) {
        this.amount = bigDecimal;
        this.currency = currency;
        this.timestamp = j;
    }

    public BigDecimal getAmount() {
        return this.amount;
    }

    public Currency getCurrency() {
        return this.currency;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public String getDefaultPrecisionString(Locale locale) {
        NumberFormat instance = NumberFormat.getInstance(locale);
        instance.setMinimumFractionDigits(this.currency.getDefaultFractionDigits());
        instance.setGroupingUsed(false);
        return instance.format(this.amount);
    }

    public String getMinimumUnitPrecisionString() {
        NumberFormat instance = NumberFormat.getInstance(Locale.US);
        instance.setMaximumFractionDigits(0);
        instance.setGroupingUsed(false);
        return instance.format(this.amount.multiply(BigDecimal.TEN.pow(this.currency.getCurrencyCode().equals("UGX") ? 2 : this.currency.getDefaultFractionDigits())));
    }

    public static boolean equals(FiatMoney fiatMoney, FiatMoney fiatMoney2) {
        return Objects.equals(fiatMoney.amount, fiatMoney2.amount) && Objects.equals(fiatMoney.currency, fiatMoney2.currency) && Objects.equals(Long.valueOf(fiatMoney.timestamp), Long.valueOf(fiatMoney2.timestamp));
    }

    public String toString() {
        return "FiatMoney{amount=" + this.amount + ", currency=" + this.currency + ", timestamp=" + this.timestamp + '}';
    }
}
