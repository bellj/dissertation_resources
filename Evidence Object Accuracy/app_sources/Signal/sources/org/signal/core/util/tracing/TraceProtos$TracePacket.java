package org.signal.core.util.tracing;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import org.signal.core.util.tracing.TraceProtos$TrackDescriptor;
import org.signal.core.util.tracing.TraceProtos$TrackEvent;

/* loaded from: classes3.dex */
public final class TraceProtos$TracePacket extends GeneratedMessageLite<TraceProtos$TracePacket, Builder> implements TraceProtos$TracePacketOrBuilder {
    private static final TraceProtos$TracePacket DEFAULT_INSTANCE;
    private static volatile Parser<TraceProtos$TracePacket> PARSER;
    public static final int SYNCHRONIZATION_MARKER_FIELD_NUMBER;
    public static final int TIMESTAMP_CLOCK_ID_FIELD_NUMBER;
    public static final int TIMESTAMP_FIELD_NUMBER;
    public static final int TRACK_DESCRIPTOR_FIELD_NUMBER;
    public static final int TRACK_EVENT_FIELD_NUMBER;
    public static final int TRUSTED_PACKET_SEQUENCE_ID_FIELD_NUMBER;
    private int bitField0_;
    private int dataCase_ = 0;
    private Object data_;
    private int optionalTrustedPacketSequenceIdCase_ = 0;
    private Object optionalTrustedPacketSequenceId_;
    private int timestampClockId_;
    private long timestamp_;

    private TraceProtos$TracePacket() {
    }

    /* loaded from: classes3.dex */
    public enum DataCase {
        TRACK_EVENT(11),
        TRACK_DESCRIPTOR(60),
        SYNCHRONIZATION_MARKER(36),
        DATA_NOT_SET(0);
        
        private final int value;

        DataCase(int i) {
            this.value = i;
        }

        public static DataCase forNumber(int i) {
            if (i == 0) {
                return DATA_NOT_SET;
            }
            if (i == 11) {
                return TRACK_EVENT;
            }
            if (i == 36) {
                return SYNCHRONIZATION_MARKER;
            }
            if (i != 60) {
                return null;
            }
            return TRACK_DESCRIPTOR;
        }
    }

    public DataCase getDataCase() {
        return DataCase.forNumber(this.dataCase_);
    }

    public void clearData() {
        this.dataCase_ = 0;
        this.data_ = null;
    }

    /* loaded from: classes3.dex */
    public enum OptionalTrustedPacketSequenceIdCase {
        TRUSTED_PACKET_SEQUENCE_ID(10),
        OPTIONALTRUSTEDPACKETSEQUENCEID_NOT_SET(0);
        
        private final int value;

        OptionalTrustedPacketSequenceIdCase(int i) {
            this.value = i;
        }

        public static OptionalTrustedPacketSequenceIdCase forNumber(int i) {
            if (i == 0) {
                return OPTIONALTRUSTEDPACKETSEQUENCEID_NOT_SET;
            }
            if (i != 10) {
                return null;
            }
            return TRUSTED_PACKET_SEQUENCE_ID;
        }
    }

    public OptionalTrustedPacketSequenceIdCase getOptionalTrustedPacketSequenceIdCase() {
        return OptionalTrustedPacketSequenceIdCase.forNumber(this.optionalTrustedPacketSequenceIdCase_);
    }

    public void clearOptionalTrustedPacketSequenceId() {
        this.optionalTrustedPacketSequenceIdCase_ = 0;
        this.optionalTrustedPacketSequenceId_ = null;
    }

    public boolean hasTimestamp() {
        return (this.bitField0_ & 1) != 0;
    }

    public long getTimestamp() {
        return this.timestamp_;
    }

    public void setTimestamp(long j) {
        this.bitField0_ |= 1;
        this.timestamp_ = j;
    }

    public void clearTimestamp() {
        this.bitField0_ &= -2;
        this.timestamp_ = 0;
    }

    public boolean hasTimestampClockId() {
        return (this.bitField0_ & 2) != 0;
    }

    public int getTimestampClockId() {
        return this.timestampClockId_;
    }

    public void setTimestampClockId(int i) {
        this.bitField0_ |= 2;
        this.timestampClockId_ = i;
    }

    public void clearTimestampClockId() {
        this.bitField0_ &= -3;
        this.timestampClockId_ = 0;
    }

    public boolean hasTrackEvent() {
        return this.dataCase_ == 11;
    }

    public TraceProtos$TrackEvent getTrackEvent() {
        if (this.dataCase_ == 11) {
            return (TraceProtos$TrackEvent) this.data_;
        }
        return TraceProtos$TrackEvent.getDefaultInstance();
    }

    public void setTrackEvent(TraceProtos$TrackEvent traceProtos$TrackEvent) {
        traceProtos$TrackEvent.getClass();
        this.data_ = traceProtos$TrackEvent;
        this.dataCase_ = 11;
    }

    public void mergeTrackEvent(TraceProtos$TrackEvent traceProtos$TrackEvent) {
        traceProtos$TrackEvent.getClass();
        if (this.dataCase_ != 11 || this.data_ == TraceProtos$TrackEvent.getDefaultInstance()) {
            this.data_ = traceProtos$TrackEvent;
        } else {
            this.data_ = TraceProtos$TrackEvent.newBuilder((TraceProtos$TrackEvent) this.data_).mergeFrom((TraceProtos$TrackEvent.Builder) traceProtos$TrackEvent).buildPartial();
        }
        this.dataCase_ = 11;
    }

    public void clearTrackEvent() {
        if (this.dataCase_ == 11) {
            this.dataCase_ = 0;
            this.data_ = null;
        }
    }

    public boolean hasTrackDescriptor() {
        return this.dataCase_ == 60;
    }

    public TraceProtos$TrackDescriptor getTrackDescriptor() {
        if (this.dataCase_ == 60) {
            return (TraceProtos$TrackDescriptor) this.data_;
        }
        return TraceProtos$TrackDescriptor.getDefaultInstance();
    }

    public void setTrackDescriptor(TraceProtos$TrackDescriptor traceProtos$TrackDescriptor) {
        traceProtos$TrackDescriptor.getClass();
        this.data_ = traceProtos$TrackDescriptor;
        this.dataCase_ = 60;
    }

    public void mergeTrackDescriptor(TraceProtos$TrackDescriptor traceProtos$TrackDescriptor) {
        traceProtos$TrackDescriptor.getClass();
        if (this.dataCase_ != 60 || this.data_ == TraceProtos$TrackDescriptor.getDefaultInstance()) {
            this.data_ = traceProtos$TrackDescriptor;
        } else {
            this.data_ = TraceProtos$TrackDescriptor.newBuilder((TraceProtos$TrackDescriptor) this.data_).mergeFrom((TraceProtos$TrackDescriptor.Builder) traceProtos$TrackDescriptor).buildPartial();
        }
        this.dataCase_ = 60;
    }

    public void clearTrackDescriptor() {
        if (this.dataCase_ == 60) {
            this.dataCase_ = 0;
            this.data_ = null;
        }
    }

    public boolean hasSynchronizationMarker() {
        return this.dataCase_ == 36;
    }

    public ByteString getSynchronizationMarker() {
        if (this.dataCase_ == 36) {
            return (ByteString) this.data_;
        }
        return ByteString.EMPTY;
    }

    public void setSynchronizationMarker(ByteString byteString) {
        byteString.getClass();
        this.dataCase_ = 36;
        this.data_ = byteString;
    }

    public void clearSynchronizationMarker() {
        if (this.dataCase_ == 36) {
            this.dataCase_ = 0;
            this.data_ = null;
        }
    }

    public boolean hasTrustedPacketSequenceId() {
        return this.optionalTrustedPacketSequenceIdCase_ == 10;
    }

    public int getTrustedPacketSequenceId() {
        if (this.optionalTrustedPacketSequenceIdCase_ == 10) {
            return ((Integer) this.optionalTrustedPacketSequenceId_).intValue();
        }
        return 0;
    }

    public void setTrustedPacketSequenceId(int i) {
        this.optionalTrustedPacketSequenceIdCase_ = 10;
        this.optionalTrustedPacketSequenceId_ = Integer.valueOf(i);
    }

    public void clearTrustedPacketSequenceId() {
        if (this.optionalTrustedPacketSequenceIdCase_ == 10) {
            this.optionalTrustedPacketSequenceIdCase_ = 0;
            this.optionalTrustedPacketSequenceId_ = null;
        }
    }

    public static TraceProtos$TracePacket parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (TraceProtos$TracePacket) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static TraceProtos$TracePacket parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TraceProtos$TracePacket) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static TraceProtos$TracePacket parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (TraceProtos$TracePacket) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static TraceProtos$TracePacket parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TraceProtos$TracePacket) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static TraceProtos$TracePacket parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (TraceProtos$TracePacket) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static TraceProtos$TracePacket parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TraceProtos$TracePacket) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static TraceProtos$TracePacket parseFrom(InputStream inputStream) throws IOException {
        return (TraceProtos$TracePacket) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static TraceProtos$TracePacket parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TraceProtos$TracePacket) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static TraceProtos$TracePacket parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (TraceProtos$TracePacket) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static TraceProtos$TracePacket parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TraceProtos$TracePacket) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static TraceProtos$TracePacket parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (TraceProtos$TracePacket) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static TraceProtos$TracePacket parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TraceProtos$TracePacket) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(TraceProtos$TracePacket traceProtos$TracePacket) {
        return DEFAULT_INSTANCE.createBuilder(traceProtos$TracePacket);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<TraceProtos$TracePacket, Builder> implements TraceProtos$TracePacketOrBuilder {
        /* synthetic */ Builder(TraceProtos$1 traceProtos$1) {
            this();
        }

        private Builder() {
            super(TraceProtos$TracePacket.DEFAULT_INSTANCE);
        }

        public Builder setTimestamp(long j) {
            copyOnWrite();
            ((TraceProtos$TracePacket) this.instance).setTimestamp(j);
            return this;
        }

        public Builder setTrackEvent(TraceProtos$TrackEvent.Builder builder) {
            copyOnWrite();
            ((TraceProtos$TracePacket) this.instance).setTrackEvent(builder.build());
            return this;
        }

        public Builder setTrackDescriptor(TraceProtos$TrackDescriptor.Builder builder) {
            copyOnWrite();
            ((TraceProtos$TracePacket) this.instance).setTrackDescriptor(builder.build());
            return this;
        }

        public Builder setSynchronizationMarker(ByteString byteString) {
            copyOnWrite();
            ((TraceProtos$TracePacket) this.instance).setSynchronizationMarker(byteString);
            return this;
        }

        public Builder setTrustedPacketSequenceId(int i) {
            copyOnWrite();
            ((TraceProtos$TracePacket) this.instance).setTrustedPacketSequenceId(i);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (TraceProtos$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new TraceProtos$TracePacket();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0006\u0002\u0001\b<\u0006\u0000\u0000\u0000\bဃ\u0000\nှ\u0001\u000bြ\u0000$ွ\u0000:ဋ\u0001<ြ\u0000", new Object[]{"data_", "dataCase_", "optionalTrustedPacketSequenceId_", "optionalTrustedPacketSequenceIdCase_", "bitField0_", "timestamp_", TraceProtos$TrackEvent.class, "timestampClockId_", TraceProtos$TrackDescriptor.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<TraceProtos$TracePacket> parser = PARSER;
                if (parser == null) {
                    synchronized (TraceProtos$TracePacket.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        TraceProtos$TracePacket traceProtos$TracePacket = new TraceProtos$TracePacket();
        DEFAULT_INSTANCE = traceProtos$TracePacket;
        GeneratedMessageLite.registerDefaultInstance(TraceProtos$TracePacket.class, traceProtos$TracePacket);
    }

    public static TraceProtos$TracePacket getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<TraceProtos$TracePacket> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
