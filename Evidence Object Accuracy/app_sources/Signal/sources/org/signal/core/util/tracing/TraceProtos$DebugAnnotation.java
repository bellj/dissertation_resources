package org.signal.core.util.tracing;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class TraceProtos$DebugAnnotation extends GeneratedMessageLite<TraceProtos$DebugAnnotation, Builder> implements TraceProtos$DebugAnnotationOrBuilder {
    public static final int BOOL_VALUE_FIELD_NUMBER;
    private static final TraceProtos$DebugAnnotation DEFAULT_INSTANCE;
    public static final int DOUBLE_VALUE_FIELD_NUMBER;
    public static final int INT_VALUE_FIELD_NUMBER;
    public static final int NAME_FIELD_NUMBER;
    public static final int NAME_IID_FIELD_NUMBER;
    public static final int NESTED_VALUE_FIELD_NUMBER;
    private static volatile Parser<TraceProtos$DebugAnnotation> PARSER;
    public static final int POINTER_VALUE_FIELD_NUMBER;
    public static final int STRING_VALUE_FIELD_NUMBER;
    public static final int UINT_VALUE_FIELD_NUMBER;
    private int bitField0_;
    private int nameFieldCase_ = 0;
    private Object nameField_;
    private int valueCase_ = 0;
    private Object value_;

    /* loaded from: classes3.dex */
    public interface NestedValueOrBuilder extends MessageLiteOrBuilder {
    }

    private TraceProtos$DebugAnnotation() {
    }

    /* loaded from: classes3.dex */
    public static final class NestedValue extends GeneratedMessageLite<NestedValue, Builder> implements NestedValueOrBuilder {
        public static final int ARRAY_VALUES_FIELD_NUMBER;
        public static final int BOOL_VALUE_FIELD_NUMBER;
        private static final NestedValue DEFAULT_INSTANCE;
        public static final int DICT_KEYS_FIELD_NUMBER;
        public static final int DICT_VALUES_FIELD_NUMBER;
        public static final int DOUBLE_VALUE_FIELD_NUMBER;
        public static final int INT_VALUE_FIELD_NUMBER;
        public static final int NESTED_TYPE_FIELD_NUMBER;
        private static volatile Parser<NestedValue> PARSER;
        public static final int STRING_VALUE_FIELD_NUMBER;
        private Internal.ProtobufList<NestedValue> arrayValues_ = GeneratedMessageLite.emptyProtobufList();
        private int bitField0_;
        private boolean boolValue_;
        private Internal.ProtobufList<String> dictKeys_ = GeneratedMessageLite.emptyProtobufList();
        private Internal.ProtobufList<NestedValue> dictValues_ = GeneratedMessageLite.emptyProtobufList();
        private double doubleValue_;
        private long intValue_;
        private int nestedType_;
        private String stringValue_ = "";

        private NestedValue() {
        }

        /* loaded from: classes3.dex */
        public enum NestedType implements Internal.EnumLite {
            UNSPECIFIED(0),
            DICT(1),
            ARRAY(2);
            
            private static final Internal.EnumLiteMap<NestedType> internalValueMap = new Internal.EnumLiteMap<NestedType>() { // from class: org.signal.core.util.tracing.TraceProtos.DebugAnnotation.NestedValue.NestedType.1
                @Override // com.google.protobuf.Internal.EnumLiteMap
                public NestedType findValueByNumber(int i) {
                    return NestedType.forNumber(i);
                }
            };
            private final int value;

            @Override // com.google.protobuf.Internal.EnumLite
            public final int getNumber() {
                return this.value;
            }

            public static NestedType forNumber(int i) {
                if (i == 0) {
                    return UNSPECIFIED;
                }
                if (i == 1) {
                    return DICT;
                }
                if (i != 2) {
                    return null;
                }
                return ARRAY;
            }

            public static Internal.EnumVerifier internalGetVerifier() {
                return NestedTypeVerifier.INSTANCE;
            }

            /* loaded from: classes3.dex */
            public static final class NestedTypeVerifier implements Internal.EnumVerifier {
                static final Internal.EnumVerifier INSTANCE = new NestedTypeVerifier();

                private NestedTypeVerifier() {
                }

                @Override // com.google.protobuf.Internal.EnumVerifier
                public boolean isInRange(int i) {
                    return NestedType.forNumber(i) != null;
                }
            }

            NestedType(int i) {
                this.value = i;
            }
        }

        public boolean hasNestedType() {
            return (this.bitField0_ & 1) != 0;
        }

        public NestedType getNestedType() {
            NestedType forNumber = NestedType.forNumber(this.nestedType_);
            return forNumber == null ? NestedType.UNSPECIFIED : forNumber;
        }

        public void setNestedType(NestedType nestedType) {
            this.nestedType_ = nestedType.getNumber();
            this.bitField0_ |= 1;
        }

        public void clearNestedType() {
            this.bitField0_ &= -2;
            this.nestedType_ = 0;
        }

        public List<String> getDictKeysList() {
            return this.dictKeys_;
        }

        public int getDictKeysCount() {
            return this.dictKeys_.size();
        }

        public String getDictKeys(int i) {
            return this.dictKeys_.get(i);
        }

        public ByteString getDictKeysBytes(int i) {
            return ByteString.copyFromUtf8(this.dictKeys_.get(i));
        }

        private void ensureDictKeysIsMutable() {
            Internal.ProtobufList<String> protobufList = this.dictKeys_;
            if (!protobufList.isModifiable()) {
                this.dictKeys_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setDictKeys(int i, String str) {
            str.getClass();
            ensureDictKeysIsMutable();
            this.dictKeys_.set(i, str);
        }

        public void addDictKeys(String str) {
            str.getClass();
            ensureDictKeysIsMutable();
            this.dictKeys_.add(str);
        }

        public void addAllDictKeys(Iterable<String> iterable) {
            ensureDictKeysIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.dictKeys_);
        }

        public void clearDictKeys() {
            this.dictKeys_ = GeneratedMessageLite.emptyProtobufList();
        }

        public void addDictKeysBytes(ByteString byteString) {
            ensureDictKeysIsMutable();
            this.dictKeys_.add(byteString.toStringUtf8());
        }

        public List<NestedValue> getDictValuesList() {
            return this.dictValues_;
        }

        public List<? extends NestedValueOrBuilder> getDictValuesOrBuilderList() {
            return this.dictValues_;
        }

        public int getDictValuesCount() {
            return this.dictValues_.size();
        }

        public NestedValue getDictValues(int i) {
            return this.dictValues_.get(i);
        }

        public NestedValueOrBuilder getDictValuesOrBuilder(int i) {
            return this.dictValues_.get(i);
        }

        private void ensureDictValuesIsMutable() {
            Internal.ProtobufList<NestedValue> protobufList = this.dictValues_;
            if (!protobufList.isModifiable()) {
                this.dictValues_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setDictValues(int i, NestedValue nestedValue) {
            nestedValue.getClass();
            ensureDictValuesIsMutable();
            this.dictValues_.set(i, nestedValue);
        }

        public void addDictValues(NestedValue nestedValue) {
            nestedValue.getClass();
            ensureDictValuesIsMutable();
            this.dictValues_.add(nestedValue);
        }

        public void addDictValues(int i, NestedValue nestedValue) {
            nestedValue.getClass();
            ensureDictValuesIsMutable();
            this.dictValues_.add(i, nestedValue);
        }

        public void addAllDictValues(Iterable<? extends NestedValue> iterable) {
            ensureDictValuesIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.dictValues_);
        }

        public void clearDictValues() {
            this.dictValues_ = GeneratedMessageLite.emptyProtobufList();
        }

        public void removeDictValues(int i) {
            ensureDictValuesIsMutable();
            this.dictValues_.remove(i);
        }

        public List<NestedValue> getArrayValuesList() {
            return this.arrayValues_;
        }

        public List<? extends NestedValueOrBuilder> getArrayValuesOrBuilderList() {
            return this.arrayValues_;
        }

        public int getArrayValuesCount() {
            return this.arrayValues_.size();
        }

        public NestedValue getArrayValues(int i) {
            return this.arrayValues_.get(i);
        }

        public NestedValueOrBuilder getArrayValuesOrBuilder(int i) {
            return this.arrayValues_.get(i);
        }

        private void ensureArrayValuesIsMutable() {
            Internal.ProtobufList<NestedValue> protobufList = this.arrayValues_;
            if (!protobufList.isModifiable()) {
                this.arrayValues_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setArrayValues(int i, NestedValue nestedValue) {
            nestedValue.getClass();
            ensureArrayValuesIsMutable();
            this.arrayValues_.set(i, nestedValue);
        }

        public void addArrayValues(NestedValue nestedValue) {
            nestedValue.getClass();
            ensureArrayValuesIsMutable();
            this.arrayValues_.add(nestedValue);
        }

        public void addArrayValues(int i, NestedValue nestedValue) {
            nestedValue.getClass();
            ensureArrayValuesIsMutable();
            this.arrayValues_.add(i, nestedValue);
        }

        public void addAllArrayValues(Iterable<? extends NestedValue> iterable) {
            ensureArrayValuesIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.arrayValues_);
        }

        public void clearArrayValues() {
            this.arrayValues_ = GeneratedMessageLite.emptyProtobufList();
        }

        public void removeArrayValues(int i) {
            ensureArrayValuesIsMutable();
            this.arrayValues_.remove(i);
        }

        public boolean hasIntValue() {
            return (this.bitField0_ & 2) != 0;
        }

        public long getIntValue() {
            return this.intValue_;
        }

        public void setIntValue(long j) {
            this.bitField0_ |= 2;
            this.intValue_ = j;
        }

        public void clearIntValue() {
            this.bitField0_ &= -3;
            this.intValue_ = 0;
        }

        public boolean hasDoubleValue() {
            return (this.bitField0_ & 4) != 0;
        }

        public double getDoubleValue() {
            return this.doubleValue_;
        }

        public void setDoubleValue(double d) {
            this.bitField0_ |= 4;
            this.doubleValue_ = d;
        }

        public void clearDoubleValue() {
            this.bitField0_ &= -5;
            this.doubleValue_ = 0.0d;
        }

        public boolean hasBoolValue() {
            return (this.bitField0_ & 8) != 0;
        }

        public boolean getBoolValue() {
            return this.boolValue_;
        }

        public void setBoolValue(boolean z) {
            this.bitField0_ |= 8;
            this.boolValue_ = z;
        }

        public void clearBoolValue() {
            this.bitField0_ &= -9;
            this.boolValue_ = false;
        }

        public boolean hasStringValue() {
            return (this.bitField0_ & 16) != 0;
        }

        public String getStringValue() {
            return this.stringValue_;
        }

        public ByteString getStringValueBytes() {
            return ByteString.copyFromUtf8(this.stringValue_);
        }

        public void setStringValue(String str) {
            str.getClass();
            this.bitField0_ |= 16;
            this.stringValue_ = str;
        }

        public void clearStringValue() {
            this.bitField0_ &= -17;
            this.stringValue_ = getDefaultInstance().getStringValue();
        }

        public void setStringValueBytes(ByteString byteString) {
            this.stringValue_ = byteString.toStringUtf8();
            this.bitField0_ |= 16;
        }

        public static NestedValue parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (NestedValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static NestedValue parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (NestedValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static NestedValue parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (NestedValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static NestedValue parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (NestedValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static NestedValue parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (NestedValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static NestedValue parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (NestedValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static NestedValue parseFrom(InputStream inputStream) throws IOException {
            return (NestedValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static NestedValue parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (NestedValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static NestedValue parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (NestedValue) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static NestedValue parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (NestedValue) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static NestedValue parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (NestedValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static NestedValue parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (NestedValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(NestedValue nestedValue) {
            return DEFAULT_INSTANCE.createBuilder(nestedValue);
        }

        /* loaded from: classes3.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<NestedValue, Builder> implements NestedValueOrBuilder {
            /* synthetic */ Builder(TraceProtos$1 traceProtos$1) {
                this();
            }

            private Builder() {
                super(NestedValue.DEFAULT_INSTANCE);
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (TraceProtos$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new NestedValue();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0003\u0000\u0001ဌ\u0000\u0002\u001a\u0003\u001b\u0004\u001b\u0005ဂ\u0001\u0006က\u0002\u0007ဇ\u0003\bဈ\u0004", new Object[]{"bitField0_", "nestedType_", NestedType.internalGetVerifier(), "dictKeys_", "dictValues_", NestedValue.class, "arrayValues_", NestedValue.class, "intValue_", "doubleValue_", "boolValue_", "stringValue_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<NestedValue> parser = PARSER;
                    if (parser == null) {
                        synchronized (NestedValue.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            NestedValue nestedValue = new NestedValue();
            DEFAULT_INSTANCE = nestedValue;
            GeneratedMessageLite.registerDefaultInstance(NestedValue.class, nestedValue);
        }

        public static NestedValue getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<NestedValue> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* loaded from: classes3.dex */
    public enum NameFieldCase {
        NAME_IID(1),
        NAME(10),
        NAMEFIELD_NOT_SET(0);
        
        private final int value;

        NameFieldCase(int i) {
            this.value = i;
        }

        public static NameFieldCase forNumber(int i) {
            if (i == 0) {
                return NAMEFIELD_NOT_SET;
            }
            if (i == 1) {
                return NAME_IID;
            }
            if (i != 10) {
                return null;
            }
            return NAME;
        }
    }

    public NameFieldCase getNameFieldCase() {
        return NameFieldCase.forNumber(this.nameFieldCase_);
    }

    public void clearNameField() {
        this.nameFieldCase_ = 0;
        this.nameField_ = null;
    }

    /* loaded from: classes3.dex */
    public enum ValueCase {
        BOOL_VALUE(2),
        UINT_VALUE(3),
        INT_VALUE(4),
        DOUBLE_VALUE(5),
        STRING_VALUE(6),
        POINTER_VALUE(7),
        NESTED_VALUE(8),
        VALUE_NOT_SET(0);
        
        private final int value;

        ValueCase(int i) {
            this.value = i;
        }

        public static ValueCase forNumber(int i) {
            if (i == 0) {
                return VALUE_NOT_SET;
            }
            switch (i) {
                case 2:
                    return BOOL_VALUE;
                case 3:
                    return UINT_VALUE;
                case 4:
                    return INT_VALUE;
                case 5:
                    return DOUBLE_VALUE;
                case 6:
                    return STRING_VALUE;
                case 7:
                    return POINTER_VALUE;
                case 8:
                    return NESTED_VALUE;
                default:
                    return null;
            }
        }
    }

    public ValueCase getValueCase() {
        return ValueCase.forNumber(this.valueCase_);
    }

    public void clearValue() {
        this.valueCase_ = 0;
        this.value_ = null;
    }

    public boolean hasNameIid() {
        return this.nameFieldCase_ == 1;
    }

    public long getNameIid() {
        if (this.nameFieldCase_ == 1) {
            return ((Long) this.nameField_).longValue();
        }
        return 0;
    }

    public void setNameIid(long j) {
        this.nameFieldCase_ = 1;
        this.nameField_ = Long.valueOf(j);
    }

    public void clearNameIid() {
        if (this.nameFieldCase_ == 1) {
            this.nameFieldCase_ = 0;
            this.nameField_ = null;
        }
    }

    public boolean hasName() {
        return this.nameFieldCase_ == 10;
    }

    public String getName() {
        return this.nameFieldCase_ == 10 ? (String) this.nameField_ : "";
    }

    public ByteString getNameBytes() {
        return ByteString.copyFromUtf8(this.nameFieldCase_ == 10 ? (String) this.nameField_ : "");
    }

    public void setName(String str) {
        str.getClass();
        this.nameFieldCase_ = 10;
        this.nameField_ = str;
    }

    public void clearName() {
        if (this.nameFieldCase_ == 10) {
            this.nameFieldCase_ = 0;
            this.nameField_ = null;
        }
    }

    public void setNameBytes(ByteString byteString) {
        this.nameField_ = byteString.toStringUtf8();
        this.nameFieldCase_ = 10;
    }

    public boolean hasBoolValue() {
        return this.valueCase_ == 2;
    }

    public boolean getBoolValue() {
        if (this.valueCase_ == 2) {
            return ((Boolean) this.value_).booleanValue();
        }
        return false;
    }

    public void setBoolValue(boolean z) {
        this.valueCase_ = 2;
        this.value_ = Boolean.valueOf(z);
    }

    public void clearBoolValue() {
        if (this.valueCase_ == 2) {
            this.valueCase_ = 0;
            this.value_ = null;
        }
    }

    public boolean hasUintValue() {
        return this.valueCase_ == 3;
    }

    public long getUintValue() {
        if (this.valueCase_ == 3) {
            return ((Long) this.value_).longValue();
        }
        return 0;
    }

    public void setUintValue(long j) {
        this.valueCase_ = 3;
        this.value_ = Long.valueOf(j);
    }

    public void clearUintValue() {
        if (this.valueCase_ == 3) {
            this.valueCase_ = 0;
            this.value_ = null;
        }
    }

    public boolean hasIntValue() {
        return this.valueCase_ == 4;
    }

    public long getIntValue() {
        if (this.valueCase_ == 4) {
            return ((Long) this.value_).longValue();
        }
        return 0;
    }

    public void setIntValue(long j) {
        this.valueCase_ = 4;
        this.value_ = Long.valueOf(j);
    }

    public void clearIntValue() {
        if (this.valueCase_ == 4) {
            this.valueCase_ = 0;
            this.value_ = null;
        }
    }

    public boolean hasDoubleValue() {
        return this.valueCase_ == 5;
    }

    public double getDoubleValue() {
        if (this.valueCase_ == 5) {
            return ((Double) this.value_).doubleValue();
        }
        return 0.0d;
    }

    public void setDoubleValue(double d) {
        this.valueCase_ = 5;
        this.value_ = Double.valueOf(d);
    }

    public void clearDoubleValue() {
        if (this.valueCase_ == 5) {
            this.valueCase_ = 0;
            this.value_ = null;
        }
    }

    public boolean hasStringValue() {
        return this.valueCase_ == 6;
    }

    public String getStringValue() {
        return this.valueCase_ == 6 ? (String) this.value_ : "";
    }

    public ByteString getStringValueBytes() {
        return ByteString.copyFromUtf8(this.valueCase_ == 6 ? (String) this.value_ : "");
    }

    public void setStringValue(String str) {
        str.getClass();
        this.valueCase_ = 6;
        this.value_ = str;
    }

    public void clearStringValue() {
        if (this.valueCase_ == 6) {
            this.valueCase_ = 0;
            this.value_ = null;
        }
    }

    public void setStringValueBytes(ByteString byteString) {
        this.value_ = byteString.toStringUtf8();
        this.valueCase_ = 6;
    }

    public boolean hasPointerValue() {
        return this.valueCase_ == 7;
    }

    public long getPointerValue() {
        if (this.valueCase_ == 7) {
            return ((Long) this.value_).longValue();
        }
        return 0;
    }

    public void setPointerValue(long j) {
        this.valueCase_ = 7;
        this.value_ = Long.valueOf(j);
    }

    public void clearPointerValue() {
        if (this.valueCase_ == 7) {
            this.valueCase_ = 0;
            this.value_ = null;
        }
    }

    public boolean hasNestedValue() {
        return this.valueCase_ == 8;
    }

    public NestedValue getNestedValue() {
        if (this.valueCase_ == 8) {
            return (NestedValue) this.value_;
        }
        return NestedValue.getDefaultInstance();
    }

    public void setNestedValue(NestedValue nestedValue) {
        nestedValue.getClass();
        this.value_ = nestedValue;
        this.valueCase_ = 8;
    }

    public void mergeNestedValue(NestedValue nestedValue) {
        nestedValue.getClass();
        if (this.valueCase_ != 8 || this.value_ == NestedValue.getDefaultInstance()) {
            this.value_ = nestedValue;
        } else {
            this.value_ = NestedValue.newBuilder((NestedValue) this.value_).mergeFrom((NestedValue.Builder) nestedValue).buildPartial();
        }
        this.valueCase_ = 8;
    }

    public void clearNestedValue() {
        if (this.valueCase_ == 8) {
            this.valueCase_ = 0;
            this.value_ = null;
        }
    }

    public static TraceProtos$DebugAnnotation parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (TraceProtos$DebugAnnotation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static TraceProtos$DebugAnnotation parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TraceProtos$DebugAnnotation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static TraceProtos$DebugAnnotation parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (TraceProtos$DebugAnnotation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static TraceProtos$DebugAnnotation parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TraceProtos$DebugAnnotation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static TraceProtos$DebugAnnotation parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (TraceProtos$DebugAnnotation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static TraceProtos$DebugAnnotation parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TraceProtos$DebugAnnotation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static TraceProtos$DebugAnnotation parseFrom(InputStream inputStream) throws IOException {
        return (TraceProtos$DebugAnnotation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static TraceProtos$DebugAnnotation parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TraceProtos$DebugAnnotation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static TraceProtos$DebugAnnotation parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (TraceProtos$DebugAnnotation) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static TraceProtos$DebugAnnotation parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TraceProtos$DebugAnnotation) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static TraceProtos$DebugAnnotation parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (TraceProtos$DebugAnnotation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static TraceProtos$DebugAnnotation parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TraceProtos$DebugAnnotation) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(TraceProtos$DebugAnnotation traceProtos$DebugAnnotation) {
        return DEFAULT_INSTANCE.createBuilder(traceProtos$DebugAnnotation);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<TraceProtos$DebugAnnotation, Builder> implements TraceProtos$DebugAnnotationOrBuilder {
        /* synthetic */ Builder(TraceProtos$1 traceProtos$1) {
            this();
        }

        private Builder() {
            super(TraceProtos$DebugAnnotation.DEFAULT_INSTANCE);
        }

        public Builder setName(String str) {
            copyOnWrite();
            ((TraceProtos$DebugAnnotation) this.instance).setName(str);
            return this;
        }

        public Builder setStringValue(String str) {
            copyOnWrite();
            ((TraceProtos$DebugAnnotation) this.instance).setStringValue(str);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (TraceProtos$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new TraceProtos$DebugAnnotation();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\t\u0002\u0001\u0001\n\t\u0000\u0000\u0000\u0001ံ\u0000\u0002်\u0001\u0003ံ\u0001\u0004ဵ\u0001\u0005ဳ\u0001\u0006ျ\u0001\u0007ံ\u0001\bြ\u0001\nျ\u0000", new Object[]{"nameField_", "nameFieldCase_", "value_", "valueCase_", "bitField0_", NestedValue.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<TraceProtos$DebugAnnotation> parser = PARSER;
                if (parser == null) {
                    synchronized (TraceProtos$DebugAnnotation.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        TraceProtos$DebugAnnotation traceProtos$DebugAnnotation = new TraceProtos$DebugAnnotation();
        DEFAULT_INSTANCE = traceProtos$DebugAnnotation;
        GeneratedMessageLite.registerDefaultInstance(TraceProtos$DebugAnnotation.class, traceProtos$DebugAnnotation);
    }

    public static TraceProtos$DebugAnnotation getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<TraceProtos$DebugAnnotation> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
