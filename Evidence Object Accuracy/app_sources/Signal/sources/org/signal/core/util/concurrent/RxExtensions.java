package org.signal.core.util.concurrent;

import io.reactivex.rxjava3.core.Single;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: RxExtensions.kt */
@Metadata(d1 = {"\u0000\u0010\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u001f\u0010\u0000\u001a\u0002H\u0001\"\b\b\u0000\u0010\u0001*\u00020\u0002*\b\u0012\u0004\u0012\u0002H\u00010\u0003¢\u0006\u0002\u0010\u0004¨\u0006\u0005"}, d2 = {"safeBlockingGet", "T", "", "Lio/reactivex/rxjava3/core/Single;", "(Lio/reactivex/rxjava3/core/Single;)Ljava/lang/Object;", "core-util_release"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class RxExtensions {
    public static final <T> T safeBlockingGet(Single<T> single) throws InterruptedException {
        Intrinsics.checkNotNullParameter(single, "<this>");
        try {
            T blockingGet = single.blockingGet();
            Intrinsics.checkNotNullExpressionValue(blockingGet, "blockingGet()");
            return blockingGet;
        } catch (RuntimeException e) {
            Throwable cause = e.getCause();
            if (cause instanceof InterruptedException) {
                throw cause;
            }
            throw e;
        }
    }
}
