package org.signal.core.util;

import android.database.Cursor;
import androidx.sqlite.db.SupportSQLiteDatabase;
import java.util.Arrays;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: SQLiteDatabaseExtensions.kt */
@Metadata(d1 = {"\u00008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\u0012\u0010\u0005\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0004\u001a#\u0010\b\u001a\u00020\t*\u00020\u00022\u0012\u0010\n\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00040\u000b\"\u00020\u0004¢\u0006\u0002\u0010\f\u001a\u0012\u0010\r\u001a\u00020\u000e*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a3\u0010\u000f\u001a\u0002H\u0010\"\b\b\u0000\u0010\u0011*\u00020\u0002\"\u0004\b\u0001\u0010\u0010*\u0002H\u00112\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u0002H\u0011\u0012\u0004\u0012\u0002H\u00100\u0013¢\u0006\u0002\u0010\u0014¨\u0006\u0015"}, d2 = {"delete", "Lorg/signal/core/util/DeleteBuilderPart1;", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "tableName", "", "getTableRowCount", "", "table", "select", "Lorg/signal/core/util/SelectBuilderPart1;", "columns", "", "(Landroidx/sqlite/db/SupportSQLiteDatabase;[Ljava/lang/String;)Lorg/signal/core/util/SelectBuilderPart1;", "update", "Lorg/signal/core/util/UpdateBuilderPart1;", "withinTransaction", "R", "T", "block", "Lkotlin/Function1;", "(Landroidx/sqlite/db/SupportSQLiteDatabase;Lkotlin/jvm/functions/Function1;)Ljava/lang/Object;", "core-util_release"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class SQLiteDatabaseExtensionsKt {
    public static final <T extends SupportSQLiteDatabase, R> R withinTransaction(T t, Function1<? super T, ? extends R> function1) {
        Intrinsics.checkNotNullParameter(t, "<this>");
        Intrinsics.checkNotNullParameter(function1, "block");
        t.beginTransaction();
        try {
            R r = (R) function1.invoke(t);
            t.setTransactionSuccessful();
            return r;
        } finally {
            t.endTransaction();
        }
    }

    public static final int getTableRowCount(SupportSQLiteDatabase supportSQLiteDatabase, String str) {
        Intrinsics.checkNotNullParameter(supportSQLiteDatabase, "<this>");
        Intrinsics.checkNotNullParameter(str, "table");
        Cursor query = supportSQLiteDatabase.query("SELECT COUNT(*) FROM " + str);
        try {
            int i = 0;
            if (query.moveToFirst()) {
                i = query.getInt(0);
            }
            th = null;
            return i;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public static final SelectBuilderPart1 select(SupportSQLiteDatabase supportSQLiteDatabase, String... strArr) {
        Intrinsics.checkNotNullParameter(supportSQLiteDatabase, "<this>");
        Intrinsics.checkNotNullParameter(strArr, "columns");
        return new SelectBuilderPart1(supportSQLiteDatabase, (String[]) Arrays.copyOf(strArr, strArr.length));
    }

    public static final UpdateBuilderPart1 update(SupportSQLiteDatabase supportSQLiteDatabase, String str) {
        Intrinsics.checkNotNullParameter(supportSQLiteDatabase, "<this>");
        Intrinsics.checkNotNullParameter(str, "tableName");
        return new UpdateBuilderPart1(supportSQLiteDatabase, str);
    }

    public static final DeleteBuilderPart1 delete(SupportSQLiteDatabase supportSQLiteDatabase, String str) {
        Intrinsics.checkNotNullParameter(supportSQLiteDatabase, "<this>");
        Intrinsics.checkNotNullParameter(str, "tableName");
        return new DeleteBuilderPart1(supportSQLiteDatabase, str);
    }
}
