package org.signal.core.util.logging;

import org.signal.core.util.logging.Log;

/* loaded from: classes.dex */
public class CompoundLogger extends Log.Logger {
    private final Log.Logger[] loggers;

    public CompoundLogger(Log.Logger... loggerArr) {
        this.loggers = loggerArr;
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void v(String str, String str2, Throwable th, boolean z) {
        for (Log.Logger logger : this.loggers) {
            logger.v(str, str2, th, z);
        }
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void d(String str, String str2, Throwable th, boolean z) {
        for (Log.Logger logger : this.loggers) {
            logger.d(str, str2, th, z);
        }
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void i(String str, String str2, Throwable th, boolean z) {
        for (Log.Logger logger : this.loggers) {
            logger.i(str, str2, th, z);
        }
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void w(String str, String str2, Throwable th, boolean z) {
        for (Log.Logger logger : this.loggers) {
            logger.w(str, str2, th, z);
        }
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void e(String str, String str2, Throwable th, boolean z) {
        for (Log.Logger logger : this.loggers) {
            logger.e(str, str2, th, z);
        }
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void v(String str, String str2, Throwable th) {
        for (Log.Logger logger : this.loggers) {
            logger.v(str, str2, th);
        }
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void d(String str, String str2, Throwable th) {
        for (Log.Logger logger : this.loggers) {
            logger.d(str, str2, th);
        }
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void i(String str, String str2, Throwable th) {
        for (Log.Logger logger : this.loggers) {
            logger.i(str, str2, th);
        }
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void w(String str, String str2, Throwable th) {
        for (Log.Logger logger : this.loggers) {
            logger.w(str, str2, th);
        }
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void e(String str, String str2, Throwable th) {
        for (Log.Logger logger : this.loggers) {
            logger.e(str, str2, th);
        }
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void v(String str, String str2) {
        for (Log.Logger logger : this.loggers) {
            logger.v(str, str2);
        }
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void d(String str, String str2) {
        for (Log.Logger logger : this.loggers) {
            logger.d(str, str2);
        }
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void i(String str, String str2) {
        for (Log.Logger logger : this.loggers) {
            logger.i(str, str2);
        }
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void w(String str, String str2) {
        for (Log.Logger logger : this.loggers) {
            logger.w(str, str2);
        }
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void e(String str, String str2) {
        for (Log.Logger logger : this.loggers) {
            logger.e(str, str2);
        }
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void flush() {
        for (Log.Logger logger : this.loggers) {
            logger.flush();
        }
    }
}
