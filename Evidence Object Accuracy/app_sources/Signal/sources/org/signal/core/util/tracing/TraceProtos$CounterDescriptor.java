package org.signal.core.util.tracing;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class TraceProtos$CounterDescriptor extends GeneratedMessageLite<TraceProtos$CounterDescriptor, Builder> implements MessageLiteOrBuilder {
    public static final int CATEGORIES_FIELD_NUMBER;
    private static final TraceProtos$CounterDescriptor DEFAULT_INSTANCE;
    public static final int IS_INCREMENTAL_FIELD_NUMBER;
    private static volatile Parser<TraceProtos$CounterDescriptor> PARSER;
    public static final int TYPE_FIELD_NUMBER;
    public static final int UNIT_FIELD_NUMBER;
    public static final int UNIT_MULTIPLIER_FIELD_NUMBER;
    private int bitField0_;
    private Internal.ProtobufList<String> categories_ = GeneratedMessageLite.emptyProtobufList();
    private boolean isIncremental_;
    private int type_;
    private long unitMultiplier_;
    private int unit_;

    private TraceProtos$CounterDescriptor() {
    }

    /* loaded from: classes3.dex */
    public enum BuiltinCounterType implements Internal.EnumLite {
        COUNTER_UNSPECIFIED(0),
        COUNTER_THREAD_TIME_NS(1),
        COUNTER_THREAD_INSTRUCTION_COUNT(2);
        
        private static final Internal.EnumLiteMap<BuiltinCounterType> internalValueMap = new Internal.EnumLiteMap<BuiltinCounterType>() { // from class: org.signal.core.util.tracing.TraceProtos.CounterDescriptor.BuiltinCounterType.1
            @Override // com.google.protobuf.Internal.EnumLiteMap
            public BuiltinCounterType findValueByNumber(int i) {
                return BuiltinCounterType.forNumber(i);
            }
        };
        private final int value;

        @Override // com.google.protobuf.Internal.EnumLite
        public final int getNumber() {
            return this.value;
        }

        public static BuiltinCounterType forNumber(int i) {
            if (i == 0) {
                return COUNTER_UNSPECIFIED;
            }
            if (i == 1) {
                return COUNTER_THREAD_TIME_NS;
            }
            if (i != 2) {
                return null;
            }
            return COUNTER_THREAD_INSTRUCTION_COUNT;
        }

        public static Internal.EnumVerifier internalGetVerifier() {
            return BuiltinCounterTypeVerifier.INSTANCE;
        }

        /* loaded from: classes3.dex */
        public static final class BuiltinCounterTypeVerifier implements Internal.EnumVerifier {
            static final Internal.EnumVerifier INSTANCE = new BuiltinCounterTypeVerifier();

            private BuiltinCounterTypeVerifier() {
            }

            @Override // com.google.protobuf.Internal.EnumVerifier
            public boolean isInRange(int i) {
                return BuiltinCounterType.forNumber(i) != null;
            }
        }

        BuiltinCounterType(int i) {
            this.value = i;
        }
    }

    /* loaded from: classes3.dex */
    public enum Unit implements Internal.EnumLite {
        UNIT_UNSPECIFIED(0),
        UNIT_TIME_NS(1),
        UNIT_COUNT(2),
        UNIT_SIZE_BYTES(3);
        
        private static final Internal.EnumLiteMap<Unit> internalValueMap = new Internal.EnumLiteMap<Unit>() { // from class: org.signal.core.util.tracing.TraceProtos.CounterDescriptor.Unit.1
            @Override // com.google.protobuf.Internal.EnumLiteMap
            public Unit findValueByNumber(int i) {
                return Unit.forNumber(i);
            }
        };
        private final int value;

        @Override // com.google.protobuf.Internal.EnumLite
        public final int getNumber() {
            return this.value;
        }

        public static Unit forNumber(int i) {
            if (i == 0) {
                return UNIT_UNSPECIFIED;
            }
            if (i == 1) {
                return UNIT_TIME_NS;
            }
            if (i == 2) {
                return UNIT_COUNT;
            }
            if (i != 3) {
                return null;
            }
            return UNIT_SIZE_BYTES;
        }

        public static Internal.EnumVerifier internalGetVerifier() {
            return UnitVerifier.INSTANCE;
        }

        /* loaded from: classes3.dex */
        public static final class UnitVerifier implements Internal.EnumVerifier {
            static final Internal.EnumVerifier INSTANCE = new UnitVerifier();

            private UnitVerifier() {
            }

            @Override // com.google.protobuf.Internal.EnumVerifier
            public boolean isInRange(int i) {
                return Unit.forNumber(i) != null;
            }
        }

        Unit(int i) {
            this.value = i;
        }
    }

    public boolean hasType() {
        return (this.bitField0_ & 1) != 0;
    }

    public BuiltinCounterType getType() {
        BuiltinCounterType forNumber = BuiltinCounterType.forNumber(this.type_);
        return forNumber == null ? BuiltinCounterType.COUNTER_UNSPECIFIED : forNumber;
    }

    public void setType(BuiltinCounterType builtinCounterType) {
        this.type_ = builtinCounterType.getNumber();
        this.bitField0_ |= 1;
    }

    public void clearType() {
        this.bitField0_ &= -2;
        this.type_ = 0;
    }

    public List<String> getCategoriesList() {
        return this.categories_;
    }

    public int getCategoriesCount() {
        return this.categories_.size();
    }

    public String getCategories(int i) {
        return this.categories_.get(i);
    }

    public ByteString getCategoriesBytes(int i) {
        return ByteString.copyFromUtf8(this.categories_.get(i));
    }

    private void ensureCategoriesIsMutable() {
        Internal.ProtobufList<String> protobufList = this.categories_;
        if (!protobufList.isModifiable()) {
            this.categories_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setCategories(int i, String str) {
        str.getClass();
        ensureCategoriesIsMutable();
        this.categories_.set(i, str);
    }

    public void addCategories(String str) {
        str.getClass();
        ensureCategoriesIsMutable();
        this.categories_.add(str);
    }

    public void addAllCategories(Iterable<String> iterable) {
        ensureCategoriesIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.categories_);
    }

    public void clearCategories() {
        this.categories_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void addCategoriesBytes(ByteString byteString) {
        ensureCategoriesIsMutable();
        this.categories_.add(byteString.toStringUtf8());
    }

    public boolean hasUnit() {
        return (this.bitField0_ & 2) != 0;
    }

    public Unit getUnit() {
        Unit forNumber = Unit.forNumber(this.unit_);
        return forNumber == null ? Unit.UNIT_UNSPECIFIED : forNumber;
    }

    public void setUnit(Unit unit) {
        this.unit_ = unit.getNumber();
        this.bitField0_ |= 2;
    }

    public void clearUnit() {
        this.bitField0_ &= -3;
        this.unit_ = 0;
    }

    public boolean hasUnitMultiplier() {
        return (this.bitField0_ & 4) != 0;
    }

    public long getUnitMultiplier() {
        return this.unitMultiplier_;
    }

    public void setUnitMultiplier(long j) {
        this.bitField0_ |= 4;
        this.unitMultiplier_ = j;
    }

    public void clearUnitMultiplier() {
        this.bitField0_ &= -5;
        this.unitMultiplier_ = 0;
    }

    public boolean hasIsIncremental() {
        return (this.bitField0_ & 8) != 0;
    }

    public boolean getIsIncremental() {
        return this.isIncremental_;
    }

    public void setIsIncremental(boolean z) {
        this.bitField0_ |= 8;
        this.isIncremental_ = z;
    }

    public void clearIsIncremental() {
        this.bitField0_ &= -9;
        this.isIncremental_ = false;
    }

    public static TraceProtos$CounterDescriptor parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (TraceProtos$CounterDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static TraceProtos$CounterDescriptor parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TraceProtos$CounterDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static TraceProtos$CounterDescriptor parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (TraceProtos$CounterDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static TraceProtos$CounterDescriptor parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TraceProtos$CounterDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static TraceProtos$CounterDescriptor parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (TraceProtos$CounterDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static TraceProtos$CounterDescriptor parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TraceProtos$CounterDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static TraceProtos$CounterDescriptor parseFrom(InputStream inputStream) throws IOException {
        return (TraceProtos$CounterDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static TraceProtos$CounterDescriptor parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TraceProtos$CounterDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static TraceProtos$CounterDescriptor parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (TraceProtos$CounterDescriptor) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static TraceProtos$CounterDescriptor parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TraceProtos$CounterDescriptor) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static TraceProtos$CounterDescriptor parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (TraceProtos$CounterDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static TraceProtos$CounterDescriptor parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TraceProtos$CounterDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(TraceProtos$CounterDescriptor traceProtos$CounterDescriptor) {
        return DEFAULT_INSTANCE.createBuilder(traceProtos$CounterDescriptor);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<TraceProtos$CounterDescriptor, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(TraceProtos$1 traceProtos$1) {
            this();
        }

        private Builder() {
            super(TraceProtos$CounterDescriptor.DEFAULT_INSTANCE);
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (TraceProtos$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new TraceProtos$CounterDescriptor();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0001\u0000\u0001ဌ\u0000\u0002\u001a\u0003ဌ\u0001\u0004ဂ\u0002\u0005ဇ\u0003", new Object[]{"bitField0_", "type_", BuiltinCounterType.internalGetVerifier(), "categories_", "unit_", Unit.internalGetVerifier(), "unitMultiplier_", "isIncremental_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<TraceProtos$CounterDescriptor> parser = PARSER;
                if (parser == null) {
                    synchronized (TraceProtos$CounterDescriptor.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        TraceProtos$CounterDescriptor traceProtos$CounterDescriptor = new TraceProtos$CounterDescriptor();
        DEFAULT_INSTANCE = traceProtos$CounterDescriptor;
        GeneratedMessageLite.registerDefaultInstance(TraceProtos$CounterDescriptor.class, traceProtos$CounterDescriptor);
    }

    public static TraceProtos$CounterDescriptor getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<TraceProtos$CounterDescriptor> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
