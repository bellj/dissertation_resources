package org.signal.core.util;

import android.content.ContentValues;
import android.database.Cursor;
import android.text.TextUtils;
import androidx.sqlite.db.SupportSQLiteDatabase;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt__MutableCollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.io.CloseableKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import me.leolin.shortcutbadger.impl.AdwHomeBadger;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;

/* compiled from: SqlUtil.kt */
@Metadata(bv = {}, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\b\n\u0002\b\u0019\bÆ\u0002\u0018\u00002\u00020\u0001:\u00017B\t\b\u0002¢\u0006\u0004\b5\u00106J\u0018\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0007J \u0010\t\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0004H\u0007J-\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00040\n2\u0016\u0010\u000b\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00010\u00010\n\"\u0004\u0018\u00010\u0001H\u0007¢\u0006\u0004\b\f\u0010\rJ\u001d\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00040\n2\u0006\u0010\u000f\u001a\u00020\u000eH\u0007¢\u0006\u0004\b\f\u0010\u0010J\u0010\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u0004H\u0007J\u0010\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u0004H\u0002J-\u0010\u0019\u001a\u00020\u00182\u0006\u0010\u0014\u001a\u00020\u00042\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00040\n2\u0006\u0010\u0017\u001a\u00020\u0016H\u0007¢\u0006\u0004\b\u0019\u0010\u001aJ&\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00180\u001d2\u0006\u0010\b\u001a\u00020\u00042\u000e\u0010\u001c\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u001bH\u0007J.\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00180\u001d2\u0006\u0010\b\u001a\u00020\u00042\u000e\u0010\u001c\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u001b2\u0006\u0010 \u001a\u00020\u001fH\u0007J \u0010!\u001a\u00020\u00182\u0006\u0010\b\u001a\u00020\u00042\u000e\u0010\u001c\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00010\u001bH\u0007J*\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00180\u001d2\u0006\u0010\u0011\u001a\u00020\u00042\u0012\u0010\"\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\n0\u001dH\u0007J2\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00180\u001d2\u0006\u0010\u0011\u001a\u00020\u00042\u0012\u0010\"\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\n0\u001d2\u0006\u0010$\u001a\u00020\u001fH\u0007J$\u0010%\u001a\u00020\u00182\u0006\u0010\u0011\u001a\u00020\u00042\u0012\u0010\"\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\n0\u001dH\u0002J+\u0010'\u001a\u00020\u00182\u0006\u0010&\u001a\u00020\u00042\u0012\u0010\u0015\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00010\n\"\u00020\u0001H\u0007¢\u0006\u0004\b'\u0010(J+\u0010*\u001a\b\u0012\u0004\u0012\u00020\u00040\n2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00040\n2\u0006\u0010)\u001a\u00020\u0004H\u0007¢\u0006\u0004\b*\u0010+J9\u0010.\u001a\b\u0012\u0004\u0012\u00020\u00180\u001d2\u0006\u0010,\u001a\u00020\u00042\f\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00040\n2\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00160\u001dH\u0007¢\u0006\u0004\b.\u0010/JA\u0010.\u001a\b\u0012\u0004\u0012\u00020\u00180\u001d2\u0006\u0010,\u001a\u00020\u00042\f\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00040\n2\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00160\u001d2\u0006\u0010$\u001a\u00020\u001fH\u0007¢\u0006\u0004\b.\u00100J3\u00101\u001a\u00020\u00182\u0006\u0010,\u001a\u00020\u00042\f\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00040\n2\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00160\u001dH\u0002¢\u0006\u0004\b1\u00102R\u001a\u00103\u001a\b\u0012\u0004\u0012\u00020\u00040\n8\u0006X\u0004¢\u0006\u0006\n\u0004\b3\u00104¨\u00068"}, d2 = {"Lorg/signal/core/util/SqlUtil;", "", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "db", "", "table", "", "tableExists", "column", "columnExists", "", "objects", "buildArgs", "([Ljava/lang/Object;)[Ljava/lang/String;", "", "argument", "(J)[Ljava/lang/String;", "query", "buildCaseInsensitiveGlobPattern", "getAccentuatedCharRegex", "selection", MultiselectForwardFragment.ARGS, "Landroid/content/ContentValues;", "contentValues", "Lorg/signal/core/util/SqlUtil$Query;", "buildTrueUpdateQuery", "(Ljava/lang/String;[Ljava/lang/String;Landroid/content/ContentValues;)Lorg/signal/core/util/SqlUtil$Query;", "", "values", "", "buildCollectionQuery", "", "maxSize", "buildSingleCollectionQuery", "argList", "buildCustomCollectionQuery", "maxQueryArgs", "buildSingleCustomCollectionQuery", "where", "buildQuery", "(Ljava/lang/String;[Ljava/lang/Object;)Lorg/signal/core/util/SqlUtil$Query;", "addition", "appendArg", "([Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;", "tableName", "columns", "buildBulkInsert", "(Ljava/lang/String;[Ljava/lang/String;Ljava/util/List;)Ljava/util/List;", "(Ljava/lang/String;[Ljava/lang/String;Ljava/util/List;I)Ljava/util/List;", "buildSingleBulkInsert", "(Ljava/lang/String;[Ljava/lang/String;Ljava/util/List;)Lorg/signal/core/util/SqlUtil$Query;", AdwHomeBadger.COUNT, "[Ljava/lang/String;", "<init>", "()V", "Query", "core-util_release"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes3.dex */
public final class SqlUtil {
    public static final String[] COUNT = {"COUNT(*)"};
    public static final SqlUtil INSTANCE = new SqlUtil();

    private SqlUtil() {
    }

    @JvmStatic
    public static final boolean tableExists(SupportSQLiteDatabase supportSQLiteDatabase, String str) {
        Intrinsics.checkNotNullParameter(supportSQLiteDatabase, "db");
        Intrinsics.checkNotNullParameter(str, "table");
        boolean z = false;
        Cursor query = supportSQLiteDatabase.query("SELECT name FROM sqlite_master WHERE type=? AND name=?", new String[]{"table", str});
        if (query != null) {
            try {
                if (query.moveToNext()) {
                    z = true;
                }
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
        th = null;
        return z;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: androidx.sqlite.db.SupportSQLiteDatabase */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v4, types: [java.lang.Throwable, java.lang.Object[]] */
    @JvmStatic
    public static final boolean columnExists(SupportSQLiteDatabase supportSQLiteDatabase, String str, String str2) {
        Intrinsics.checkNotNullParameter(supportSQLiteDatabase, "db");
        Intrinsics.checkNotNullParameter(str, "table");
        Intrinsics.checkNotNullParameter(str2, "column");
        th = 0;
        Cursor query = supportSQLiteDatabase.query("PRAGMA table_info(" + str + ')', th);
        try {
            int columnIndexOrThrow = query.getColumnIndexOrThrow("name");
            while (query.moveToNext()) {
                if (Intrinsics.areEqual(query.getString(columnIndexOrThrow), str2)) {
                    return true;
                }
            }
            Unit unit = Unit.INSTANCE;
            CloseableKt.closeFinally(query, th);
            return false;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    @JvmStatic
    public static final String[] buildArgs(long j) {
        return new String[]{String.valueOf(j)};
    }

    /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [('*' char), (wrap: java.lang.Object : ?: CAST (java.lang.Object) (r0v2 java.lang.StringBuilder)), ('*' char)] */
    @JvmStatic
    public static final String buildCaseInsensitiveGlobPattern(String str) {
        Intrinsics.checkNotNullParameter(str, "query");
        if (TextUtils.isEmpty(str)) {
            return "*";
        }
        StringBuilder sb = new StringBuilder();
        int codePointCount = str.codePointCount(0, str.length());
        for (int i = 0; i < codePointCount; i++) {
            String codePointToString = StringUtil.codePointToString(str.codePointAt(i));
            Intrinsics.checkNotNullExpressionValue(codePointToString, "codePointToString(query.codePointAt(i))");
            sb.append("[");
            Locale locale = Locale.getDefault();
            Intrinsics.checkNotNullExpressionValue(locale, "getDefault()");
            String lowerCase = codePointToString.toLowerCase(locale);
            Intrinsics.checkNotNullExpressionValue(lowerCase, "this as java.lang.String).toLowerCase(locale)");
            sb.append(lowerCase);
            Locale locale2 = Locale.getDefault();
            Intrinsics.checkNotNullExpressionValue(locale2, "getDefault()");
            String upperCase = codePointToString.toUpperCase(locale2);
            Intrinsics.checkNotNullExpressionValue(upperCase, "this as java.lang.String).toUpperCase(locale)");
            sb.append(upperCase);
            SqlUtil sqlUtil = INSTANCE;
            Locale locale3 = Locale.getDefault();
            Intrinsics.checkNotNullExpressionValue(locale3, "getDefault()");
            String lowerCase2 = codePointToString.toLowerCase(locale3);
            Intrinsics.checkNotNullExpressionValue(lowerCase2, "this as java.lang.String).toLowerCase(locale)");
            sb.append(sqlUtil.getAccentuatedCharRegex(lowerCase2));
            sb.append("]");
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append('*');
        sb2.append((Object) sb);
        sb2.append('*');
        return sb2.toString();
    }

    private final String getAccentuatedCharRegex(String str) {
        int hashCode = str.hashCode();
        if (hashCode != 945) {
            if (hashCode != 949) {
                if (hashCode != 951) {
                    if (hashCode != 953) {
                        if (hashCode != 959) {
                            if (hashCode != 965) {
                                if (hashCode != 969) {
                                    if (hashCode != 962) {
                                        if (hashCode != 963) {
                                            switch (hashCode) {
                                                case 97:
                                                    if (str.equals("a")) {
                                                        return "À-Åà-åĀ-ąǍǎǞ-ǡǺ-ǻȀ-ȃȦȧȺɐ-ɒḀḁẚẠ-ặ";
                                                    }
                                                    break;
                                                case 98:
                                                    if (str.equals("b")) {
                                                        return "ßƀ-ƅɃɓḂ-ḇ";
                                                    }
                                                    break;
                                                case 99:
                                                    if (str.equals("c")) {
                                                        return "çÇĆ-čƆ-ƈȻȼɔḈḉ";
                                                    }
                                                    break;
                                                case 100:
                                                    if (str.equals("d")) {
                                                        return "ÐðĎ-đƉ-ƍȡɖɗḊ-ḓ";
                                                    }
                                                    break;
                                                case 101:
                                                    if (str.equals("e")) {
                                                        return "È-Ëè-ëĒ-ěƎ-ƐǝȄ-ȇȨȩɆɇɘ-ɞḔ-ḝẸ-ệ";
                                                    }
                                                    break;
                                                case 102:
                                                    if (str.equals("f")) {
                                                        return "ƑƒḞḟ";
                                                    }
                                                    break;
                                                case 103:
                                                    if (str.equals("g")) {
                                                        return "Ĝ-ģƓǤ-ǧǴǵḠḡ";
                                                    }
                                                    break;
                                                case 104:
                                                    if (str.equals("h")) {
                                                        return "Ĥ-ħƕǶȞȟḢ-ḫẖ";
                                                    }
                                                    break;
                                                case 105:
                                                    if (str.equals("i")) {
                                                        return "Ì-Ïì-ïĨ-ıƖƗǏǐȈ-ȋɨɪḬ-ḯỈ-ị";
                                                    }
                                                    break;
                                                case 106:
                                                    if (str.equals("j")) {
                                                        return "ĴĵǰȷɈɉɟ";
                                                    }
                                                    break;
                                                case 107:
                                                    if (str.equals("k")) {
                                                        return "Ķ-ĸƘƙǨǩḰ-ḵ";
                                                    }
                                                    break;
                                                case 108:
                                                    if (str.equals("l")) {
                                                        return "Ĺ-łƚȴȽɫ-ɭḶ-ḽ";
                                                    }
                                                    break;
                                                case 109:
                                                    if (str.equals("m")) {
                                                        return "Ɯɯ-ɱḾ-ṃ";
                                                    }
                                                    break;
                                                case 110:
                                                    if (str.equals("n")) {
                                                        return "ÑñŃ-ŋƝƞǸǹȠȵɲ-ɴṄ-ṋ";
                                                    }
                                                    break;
                                                case 111:
                                                    if (str.equals("o")) {
                                                        return "Ò-ÖØò-öøŌ-őƟ-ơǑǒǪ-ǭǾǿȌ-ȏȪ-ȱṌ-ṓỌ-ợ";
                                                    }
                                                    break;
                                                case R.styleable.AppCompatTheme_tooltipForegroundColor /* 112 */:
                                                    if (str.equals("p")) {
                                                        return "ƤƥṔ-ṗ";
                                                    }
                                                    break;
                                                case R.styleable.AppCompatTheme_tooltipFrameBackground /* 113 */:
                                                    str.equals("q");
                                                    break;
                                                case 114:
                                                    if (str.equals("r")) {
                                                        return "Ŕ-řƦȐ-ȓɌɍṘ-ṟ";
                                                    }
                                                    break;
                                                case 115:
                                                    if (str.equals("s")) {
                                                        return "Ś-šƧƨȘșȿṠ-ṩ";
                                                    }
                                                    break;
                                                case 116:
                                                    if (str.equals("t")) {
                                                        return "Ţ-ŧƫ-ƮȚțȾṪ-ṱẗ";
                                                    }
                                                    break;
                                                case 117:
                                                    if (str.equals("u")) {
                                                        return "Ù-Üù-üŨ-ųƯ-ƱǓ-ǜȔ-ȗɄṲ-ṻỤ-ự";
                                                    }
                                                    break;
                                                case 118:
                                                    if (str.equals("v")) {
                                                        return "ƲɅṼ-ṿ";
                                                    }
                                                    break;
                                                case 119:
                                                    if (str.equals("w")) {
                                                        return "ŴŵẀ-ẉẘ";
                                                    }
                                                    break;
                                                case 120:
                                                    if (str.equals("x")) {
                                                        return "Ẋ-ẍ";
                                                    }
                                                    break;
                                                case 121:
                                                    if (str.equals("y")) {
                                                        return "ÝýÿŶ-ŸƔƳƴȲȳɎɏẎẏỲ-ỹỾỿẙ";
                                                    }
                                                    break;
                                                case 122:
                                                    if (str.equals("z")) {
                                                        return "Ź-žƵƶɀẐ-ẕ";
                                                    }
                                                    break;
                                            }
                                        } else if (str.equals("σ")) {
                                            return "Σςσ";
                                        }
                                    } else if (str.equals("ς")) {
                                        return "Σςσ";
                                    }
                                } else if (str.equals("ω")) {
                                    return "ΏΩωώὠ-Ὧὼώᾠ-ᾯῲ-ῴῶῷῺ-ῼ";
                                }
                            } else if (str.equals("υ")) {
                                return "ΎΥΫυϋύὐ-ὗὙὛὝὟὺύῠ-ΰῦ-Ύ";
                            }
                        } else if (str.equals("ο")) {
                            return "ΌΟοόὀ-ὅὈ-ὍὸόῸΌ";
                        }
                    } else if (str.equals("ι")) {
                        return "ΊΐΙΪίιϊἰ-Ἷὶίῐ-ΐῖ-Ί";
                    }
                } else if (str.equals("η")) {
                    return "ΉΗήηἠ-Ἧὴήᾐ-ᾟἠ-Ἧὴήᾐ-ᾟῂῃῄῆῇῊΉῌ";
                }
            } else if (str.equals("ε")) {
                return "ΈΕέεἐ-ἕἘ-ἝὲέῈΈ";
            }
        } else if (str.equals("α")) {
            return "ΆΑάαἀ-Ἇὰάᾀ-ᾏᾰ-ᾴᾶ-ᾼ";
        }
        return "";
    }

    @JvmStatic
    public static final Query buildTrueUpdateQuery(String str, String[] strArr, ContentValues contentValues) {
        Intrinsics.checkNotNullParameter(str, "selection");
        Intrinsics.checkNotNullParameter(strArr, MultiselectForwardFragment.ARGS);
        Intrinsics.checkNotNullParameter(contentValues, "contentValues");
        StringBuilder sb = new StringBuilder();
        Set<Map.Entry<String, Object>> valueSet = contentValues.valueSet();
        ArrayList arrayList = new ArrayList(strArr.length + valueSet.size());
        boolean unused = CollectionsKt__MutableCollectionsKt.addAll(arrayList, strArr);
        int i = 0;
        for (Map.Entry<String, Object> entry : valueSet) {
            Intrinsics.checkNotNullExpressionValue(entry, "valueSet");
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value == null) {
                sb.append(key);
                sb.append(" NOT NULL");
            } else if (value instanceof byte[]) {
                sb.append("hex(");
                sb.append(key);
                sb.append(") != ? OR ");
                sb.append(key);
                sb.append(" IS NULL");
                String stringCondensed = Hex.toStringCondensed((byte[]) value);
                Intrinsics.checkNotNullExpressionValue(stringCondensed, "toStringCondensed(value)");
                Locale locale = Locale.US;
                Intrinsics.checkNotNullExpressionValue(locale, "US");
                String upperCase = stringCondensed.toUpperCase(locale);
                Intrinsics.checkNotNullExpressionValue(upperCase, "this as java.lang.String).toUpperCase(locale)");
                arrayList.add(upperCase);
            } else {
                sb.append(key);
                sb.append(" != ? OR ");
                sb.append(key);
                sb.append(" IS NULL");
                arrayList.add(value.toString());
            }
            if (i != valueSet.size() - 1) {
                sb.append(" OR ");
            }
            i++;
        }
        String str2 = '(' + str + ") AND (" + ((Object) sb) + ')';
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            return new Query(str2, (String[]) array);
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
    }

    @JvmStatic
    public static final List<Query> buildCollectionQuery(String str, Collection<? extends Object> collection) {
        Intrinsics.checkNotNullParameter(str, "column");
        Intrinsics.checkNotNullParameter(collection, "values");
        return buildCollectionQuery(str, collection, 999);
    }

    @JvmStatic
    public static final List<Query> buildCollectionQuery(String str, Collection<? extends Object> collection, int i) {
        Intrinsics.checkNotNullParameter(str, "column");
        Intrinsics.checkNotNullParameter(collection, "values");
        if (!collection.isEmpty()) {
            List<List> list = CollectionsKt___CollectionsKt.chunked(collection, i);
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
            for (List list2 : list) {
                arrayList.add(buildSingleCollectionQuery(str, list2));
            }
            return arrayList;
        }
        throw new IllegalArgumentException("Must have values!".toString());
    }

    @JvmStatic
    public static final Query buildSingleCollectionQuery(String str, Collection<? extends Object> collection) {
        Intrinsics.checkNotNullParameter(str, "column");
        Intrinsics.checkNotNullParameter(collection, "values");
        if (!collection.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            int size = collection.size();
            Object[] objArr = new Object[size];
            int i = 0;
            for (Object obj : collection) {
                sb.append("?");
                objArr[i] = obj;
                if (i != collection.size() - 1) {
                    sb.append(", ");
                }
                i++;
            }
            return new Query(str + " IN (" + ((Object) sb) + ')', buildArgs(Arrays.copyOf(objArr, size)));
        }
        throw new IllegalArgumentException("Must have values!".toString());
    }

    @JvmStatic
    public static final List<Query> buildCustomCollectionQuery(String str, List<String[]> list) {
        Intrinsics.checkNotNullParameter(str, "query");
        Intrinsics.checkNotNullParameter(list, "argList");
        return buildCustomCollectionQuery(str, list, 999);
    }

    @JvmStatic
    public static final List<Query> buildCustomCollectionQuery(String str, List<String[]> list, int i) {
        Intrinsics.checkNotNullParameter(str, "query");
        Intrinsics.checkNotNullParameter(list, "argList");
        Object collect = Collection$EL.stream(ListUtil.chunk(list, i / list.get(0).length)).map(new Function(str) { // from class: org.signal.core.util.SqlUtil$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return SqlUtil.m179buildCustomCollectionQuery$lambda8(this.f$0, (List) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList());
        Intrinsics.checkNotNullExpressionValue(collect, "chunk(argList, batchSize…lect(Collectors.toList())");
        return (List) collect;
    }

    /* renamed from: buildCustomCollectionQuery$lambda-8 */
    public static final Query m179buildCustomCollectionQuery$lambda8(String str, List list) {
        Intrinsics.checkNotNullParameter(str, "$query");
        SqlUtil sqlUtil = INSTANCE;
        Intrinsics.checkNotNullExpressionValue(list, "argBatch");
        return sqlUtil.buildSingleCustomCollectionQuery(str, list);
    }

    private final Query buildSingleCustomCollectionQuery(String str, List<String[]> list) {
        StringBuilder sb = new StringBuilder();
        ArrayList arrayList = new ArrayList();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            sb.append("(");
            sb.append(str);
            sb.append(")");
            if (i < size - 1) {
                sb.append(" OR ");
            }
            for (String str2 : list.get(i)) {
                arrayList.add(str2);
            }
        }
        String sb2 = sb.toString();
        Intrinsics.checkNotNullExpressionValue(sb2, "outputQuery.toString()");
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            return new Query(sb2, (String[]) array);
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
    }

    @JvmStatic
    public static final Query buildQuery(String str, Object... objArr) {
        Intrinsics.checkNotNullParameter(str, "where");
        Intrinsics.checkNotNullParameter(objArr, MultiselectForwardFragment.ARGS);
        return new Query(str, buildArgs(Arrays.copyOf(objArr, objArr.length)));
    }

    @JvmStatic
    public static final String[] appendArg(String[] strArr, String str) {
        Intrinsics.checkNotNullParameter(strArr, MultiselectForwardFragment.ARGS);
        Intrinsics.checkNotNullParameter(str, "addition");
        List list = ArraysKt___ArraysKt.toMutableList(strArr);
        list.add(str);
        Object[] array = list.toArray(new String[0]);
        if (array != null) {
            return (String[]) array;
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
    }

    @JvmStatic
    public static final List<Query> buildBulkInsert(String str, String[] strArr, List<ContentValues> list) {
        Intrinsics.checkNotNullParameter(str, "tableName");
        Intrinsics.checkNotNullParameter(strArr, "columns");
        Intrinsics.checkNotNullParameter(list, "contentValues");
        return buildBulkInsert(str, strArr, list, 999);
    }

    @JvmStatic
    public static final List<Query> buildBulkInsert(String str, String[] strArr, List<ContentValues> list, int i) {
        Intrinsics.checkNotNullParameter(str, "tableName");
        Intrinsics.checkNotNullParameter(strArr, "columns");
        Intrinsics.checkNotNullParameter(list, "contentValues");
        List<List<ContentValues>> list2 = CollectionsKt___CollectionsKt.chunked(list, i / strArr.length);
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list2, 10));
        for (List<ContentValues> list3 : list2) {
            arrayList.add(INSTANCE.buildSingleBulkInsert(str, strArr, list3));
        }
        return CollectionsKt___CollectionsKt.toList(arrayList);
    }

    private final Query buildSingleBulkInsert(String str, String[] strArr, List<ContentValues> list) {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(str);
        sb.append(" (");
        int length = strArr.length;
        for (int i = 0; i < length; i++) {
            sb.append(strArr[i]);
            if (i < strArr.length - 1) {
                sb.append(", ");
            }
        }
        sb.append(") VALUES ");
        StringBuilder sb2 = new StringBuilder();
        sb2.append("(");
        int length2 = strArr.length;
        for (int i2 = 0; i2 < length2; i2++) {
            sb2.append("?");
            if (i2 < strArr.length - 1) {
                sb2.append(", ");
            }
        }
        sb2.append(")");
        int size = list.size();
        for (int i3 = 0; i3 < size; i3++) {
            sb.append((CharSequence) sb2);
            if (i3 < size - 1) {
                sb.append(", ");
            }
        }
        String sb3 = sb.toString();
        Intrinsics.checkNotNullExpressionValue(sb3, "builder.toString()");
        ArrayList arrayList = new ArrayList();
        for (ContentValues contentValues : list) {
            for (String str2 : strArr) {
                arrayList.add(contentValues.get(str2) != null ? contentValues.get(str2).toString() : "null");
            }
        }
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            return new Query(sb3, (String[]) array);
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
    }

    /* compiled from: SqlUtil.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005¢\u0006\u0002\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0019\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005¢\u0006\n\n\u0002\u0010\u000b\u001a\u0004\b\t\u0010\n¨\u0006\f"}, d2 = {"Lorg/signal/core/util/SqlUtil$Query;", "", "where", "", "whereArgs", "", "(Ljava/lang/String;[Ljava/lang/String;)V", "getWhere", "()Ljava/lang/String;", "getWhereArgs", "()[Ljava/lang/String;", "[Ljava/lang/String;", "core-util_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Query {
        private final String where;
        private final String[] whereArgs;

        public Query(String str, String[] strArr) {
            Intrinsics.checkNotNullParameter(str, "where");
            Intrinsics.checkNotNullParameter(strArr, "whereArgs");
            this.where = str;
            this.whereArgs = strArr;
        }

        public final String getWhere() {
            return this.where;
        }

        public final String[] getWhereArgs() {
            return this.whereArgs;
        }
    }

    @JvmStatic
    public static final String[] buildArgs(Object... objArr) {
        String str;
        Intrinsics.checkNotNullParameter(objArr, "objects");
        ArrayList arrayList = new ArrayList(objArr.length);
        for (Object obj : objArr) {
            if (obj != null) {
                if (obj instanceof DatabaseId) {
                    DatabaseId databaseId = (DatabaseId) obj;
                    Intrinsics.checkNotNull(databaseId);
                    str = databaseId.serialize();
                } else {
                    str = obj.toString();
                }
                arrayList.add(str);
            } else {
                throw new NullPointerException("Cannot have null arg!");
            }
        }
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            return (String[]) array;
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
    }
}
