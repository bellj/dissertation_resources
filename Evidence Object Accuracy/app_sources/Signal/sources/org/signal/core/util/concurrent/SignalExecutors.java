package org.signal.core.util.concurrent;

import android.os.HandlerThread;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.signal.core.util.ThreadUtil;

/* loaded from: classes.dex */
public final class SignalExecutors {
    public static final ExecutorService BOUNDED = ThreadUtil.trace(Executors.newFixedThreadPool(4, new NumberedThreadFactory("signal-bounded")));
    public static final ExecutorService BOUNDED_IO = ThreadUtil.trace(newCachedBoundedExecutor("signal-io-bounded", 1, 32, 30));
    public static final ExecutorService SERIAL = ThreadUtil.trace(Executors.newSingleThreadExecutor(new NumberedThreadFactory("signal-serial")));
    public static final ExecutorService UNBOUNDED = ThreadUtil.trace(Executors.newCachedThreadPool(new NumberedThreadFactory("signal-unbounded")));

    public static /* synthetic */ Thread lambda$newCachedSingleThreadExecutor$0(String str, Runnable runnable) {
        return new Thread(runnable, str);
    }

    public static ExecutorService newCachedSingleThreadExecutor(String str) {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 1, 15, TimeUnit.SECONDS, new LinkedBlockingQueue(), new ThreadFactory(str) { // from class: org.signal.core.util.concurrent.SignalExecutors$$ExternalSyntheticLambda1
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.util.concurrent.ThreadFactory
            public final Thread newThread(Runnable runnable) {
                return SignalExecutors.lambda$newCachedSingleThreadExecutor$0(this.f$0, runnable);
            }
        });
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        return threadPoolExecutor;
    }

    public static ExecutorService newCachedBoundedExecutor(String str, int i, int i2, int i3) {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(i, i2, (long) i3, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>() { // from class: org.signal.core.util.concurrent.SignalExecutors.1
            public boolean offer(Runnable runnable) {
                if (isEmpty()) {
                    return super.offer((AnonymousClass1) runnable);
                }
                return false;
            }
        }, new NumberedThreadFactory(str));
        threadPoolExecutor.setRejectedExecutionHandler(new RejectedExecutionHandler() { // from class: org.signal.core.util.concurrent.SignalExecutors$$ExternalSyntheticLambda0
            @Override // java.util.concurrent.RejectedExecutionHandler
            public final void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor2) {
                SignalExecutors.lambda$newCachedBoundedExecutor$1(runnable, threadPoolExecutor2);
            }
        });
        return threadPoolExecutor;
    }

    public static /* synthetic */ void lambda$newCachedBoundedExecutor$1(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
        try {
            threadPoolExecutor.getQueue().put(runnable);
        } catch (InterruptedException unused) {
            Thread.currentThread().interrupt();
        }
    }

    public static HandlerThread getAndStartHandlerThread(String str) {
        HandlerThread handlerThread = new HandlerThread(str);
        handlerThread.start();
        return handlerThread;
    }

    /* loaded from: classes3.dex */
    public static class NumberedThreadFactory implements ThreadFactory {
        private final String baseName;
        private final AtomicInteger counter = new AtomicInteger();

        NumberedThreadFactory(String str) {
            this.baseName = str;
        }

        @Override // java.util.concurrent.ThreadFactory
        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, this.baseName + "-" + this.counter.getAndIncrement());
        }
    }
}
