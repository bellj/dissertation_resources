package org.signal.core.util.tracing;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* loaded from: classes3.dex */
public final class TraceProtos$Trace extends GeneratedMessageLite<TraceProtos$Trace, Builder> implements MessageLiteOrBuilder {
    private static final TraceProtos$Trace DEFAULT_INSTANCE;
    public static final int PACKET_FIELD_NUMBER;
    private static volatile Parser<TraceProtos$Trace> PARSER;
    private Internal.ProtobufList<TraceProtos$TracePacket> packet_ = GeneratedMessageLite.emptyProtobufList();

    private TraceProtos$Trace() {
    }

    public List<TraceProtos$TracePacket> getPacketList() {
        return this.packet_;
    }

    public List<? extends TraceProtos$TracePacketOrBuilder> getPacketOrBuilderList() {
        return this.packet_;
    }

    public int getPacketCount() {
        return this.packet_.size();
    }

    public TraceProtos$TracePacket getPacket(int i) {
        return this.packet_.get(i);
    }

    public TraceProtos$TracePacketOrBuilder getPacketOrBuilder(int i) {
        return this.packet_.get(i);
    }

    private void ensurePacketIsMutable() {
        Internal.ProtobufList<TraceProtos$TracePacket> protobufList = this.packet_;
        if (!protobufList.isModifiable()) {
            this.packet_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setPacket(int i, TraceProtos$TracePacket traceProtos$TracePacket) {
        traceProtos$TracePacket.getClass();
        ensurePacketIsMutable();
        this.packet_.set(i, traceProtos$TracePacket);
    }

    public void addPacket(TraceProtos$TracePacket traceProtos$TracePacket) {
        traceProtos$TracePacket.getClass();
        ensurePacketIsMutable();
        this.packet_.add(traceProtos$TracePacket);
    }

    public void addPacket(int i, TraceProtos$TracePacket traceProtos$TracePacket) {
        traceProtos$TracePacket.getClass();
        ensurePacketIsMutable();
        this.packet_.add(i, traceProtos$TracePacket);
    }

    public void addAllPacket(Iterable<? extends TraceProtos$TracePacket> iterable) {
        ensurePacketIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.packet_);
    }

    public void clearPacket() {
        this.packet_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removePacket(int i) {
        ensurePacketIsMutable();
        this.packet_.remove(i);
    }

    public static TraceProtos$Trace parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (TraceProtos$Trace) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static TraceProtos$Trace parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TraceProtos$Trace) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static TraceProtos$Trace parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (TraceProtos$Trace) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static TraceProtos$Trace parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TraceProtos$Trace) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static TraceProtos$Trace parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (TraceProtos$Trace) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static TraceProtos$Trace parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TraceProtos$Trace) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static TraceProtos$Trace parseFrom(InputStream inputStream) throws IOException {
        return (TraceProtos$Trace) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static TraceProtos$Trace parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TraceProtos$Trace) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static TraceProtos$Trace parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (TraceProtos$Trace) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static TraceProtos$Trace parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TraceProtos$Trace) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static TraceProtos$Trace parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (TraceProtos$Trace) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static TraceProtos$Trace parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TraceProtos$Trace) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(TraceProtos$Trace traceProtos$Trace) {
        return DEFAULT_INSTANCE.createBuilder(traceProtos$Trace);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<TraceProtos$Trace, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(TraceProtos$1 traceProtos$1) {
            this();
        }

        private Builder() {
            super(TraceProtos$Trace.DEFAULT_INSTANCE);
        }

        public Builder addPacket(TraceProtos$TracePacket traceProtos$TracePacket) {
            copyOnWrite();
            ((TraceProtos$Trace) this.instance).addPacket(traceProtos$TracePacket);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (TraceProtos$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new TraceProtos$Trace();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"packet_", TraceProtos$TracePacket.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<TraceProtos$Trace> parser = PARSER;
                if (parser == null) {
                    synchronized (TraceProtos$Trace.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        TraceProtos$Trace traceProtos$Trace = new TraceProtos$Trace();
        DEFAULT_INSTANCE = traceProtos$Trace;
        GeneratedMessageLite.registerDefaultInstance(TraceProtos$Trace.class, traceProtos$Trace);
    }

    public static TraceProtos$Trace getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<TraceProtos$Trace> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
