package org.signal.core.util;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import java.util.Locale;

/* loaded from: classes3.dex */
public final class TranslationDetection {
    private final Configuration configurationLocal;
    private final Resources resourcesEn;
    private final Resources resourcesLocal;

    public TranslationDetection(Context context) {
        Resources resources = context.getResources();
        this.resourcesLocal = resources;
        this.configurationLocal = resources.getConfiguration();
        this.resourcesEn = ResourceUtil.getEnglishResources(context);
    }

    public TranslationDetection(Context context, Locale locale) {
        Resources resources = ResourceUtil.getResources(context.getApplicationContext(), locale);
        this.resourcesLocal = resources;
        this.configurationLocal = resources.getConfiguration();
        this.resourcesEn = ResourceUtil.getEnglishResources(context);
    }

    public boolean textExistsInUsersLanguage(int i) {
        if (configSupportsEnglish()) {
            return true;
        }
        return !this.resourcesEn.getString(i).equals(this.resourcesLocal.getString(i));
    }

    public boolean textExistsInUsersLanguage(int... iArr) {
        for (int i : iArr) {
            if (!textExistsInUsersLanguage(i)) {
                return false;
            }
        }
        return true;
    }

    protected boolean configSupportsEnglish() {
        Locale firstMatch;
        if (this.configurationLocal.locale.getLanguage().equals("en")) {
            return true;
        }
        if (Build.VERSION.SDK_INT < 24 || (firstMatch = this.configurationLocal.getLocales().getFirstMatch(new String[]{"en"})) == null || !firstMatch.getLanguage().equals("en")) {
            return false;
        }
        return true;
    }
}
