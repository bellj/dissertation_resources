package org.signal.core.util;

/* loaded from: classes3.dex */
public class Conversions {
    public static byte[] mediumToByteArray(int i) {
        byte[] bArr = new byte[3];
        mediumToByteArray(bArr, 0, i);
        return bArr;
    }

    public static int mediumToByteArray(byte[] bArr, int i, int i2) {
        bArr[i + 2] = (byte) i2;
        bArr[i + 1] = (byte) (i2 >> 8);
        bArr[i] = (byte) (i2 >> 16);
        return 3;
    }

    public static byte[] intToByteArray(int i) {
        byte[] bArr = new byte[4];
        intToByteArray(bArr, 0, i);
        return bArr;
    }

    public static int intToByteArray(byte[] bArr, int i, int i2) {
        bArr[i + 3] = (byte) i2;
        bArr[i + 2] = (byte) (i2 >> 8);
        bArr[i + 1] = (byte) (i2 >> 16);
        bArr[i] = (byte) (i2 >> 24);
        return 4;
    }

    public static byte[] longToByteArray(long j) {
        byte[] bArr = new byte[8];
        longToByteArray(bArr, 0, j);
        return bArr;
    }

    public static int longToByteArray(byte[] bArr, int i, long j) {
        bArr[i + 7] = (byte) ((int) j);
        bArr[i + 6] = (byte) ((int) (j >> 8));
        bArr[i + 5] = (byte) ((int) (j >> 16));
        bArr[i + 4] = (byte) ((int) (j >> 24));
        bArr[i + 3] = (byte) ((int) (j >> 32));
        bArr[i + 2] = (byte) ((int) (j >> 40));
        bArr[i + 1] = (byte) ((int) (j >> 48));
        bArr[i] = (byte) ((int) (j >> 56));
        return 8;
    }

    public static int longTo4ByteArray(byte[] bArr, int i, long j) {
        bArr[i + 3] = (byte) ((int) j);
        bArr[i + 2] = (byte) ((int) (j >> 8));
        bArr[i + 1] = (byte) ((int) (j >> 16));
        bArr[i + 0] = (byte) ((int) (j >> 24));
        return 4;
    }

    public static int byteArrayToMedium(byte[] bArr, int i) {
        return (bArr[i + 2] & 255) | ((bArr[i] & 255) << 16) | ((bArr[i + 1] & 255) << 8);
    }

    public static int byteArrayToInt(byte[] bArr) {
        return byteArrayToInt(bArr, 0);
    }

    public static int byteArrayToInt(byte[] bArr, int i) {
        return (bArr[i + 3] & 255) | ((bArr[i] & 255) << 24) | ((bArr[i + 1] & 255) << 16) | ((bArr[i + 2] & 255) << 8);
    }

    public static long byteArrayToLong(byte[] bArr) {
        return byteArrayToLong(bArr, 0);
    }

    public static long byteArrayToLong(byte[] bArr, int i) {
        return (((long) bArr[i + 7]) & 255) | ((((long) bArr[i]) & 255) << 56) | ((((long) bArr[i + 1]) & 255) << 48) | ((((long) bArr[i + 2]) & 255) << 40) | ((((long) bArr[i + 3]) & 255) << 32) | ((((long) bArr[i + 4]) & 255) << 24) | ((((long) bArr[i + 5]) & 255) << 16) | ((((long) bArr[i + 6]) & 255) << 8);
    }
}
