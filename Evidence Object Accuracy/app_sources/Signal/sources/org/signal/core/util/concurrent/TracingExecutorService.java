package org.signal.core.util.concurrent;

import java.lang.Thread;
import java.util.Collection;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: TracingExecutorService.kt */
@Metadata(bv = {}, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u001f\n\u0002\u0018\u0002\n\u0002\u0010\u001e\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\b\b\u0000\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010 \u001a\u00020\u0001¢\u0006\u0004\b.\u0010/J!\u0010\b\u001a\u00020\u00072\u0006\u0010\u0003\u001a\u00020\u00022\u000e\u0010\u0006\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004H\u0001JÝ\u0001\u0010\u0011\u001a^\u0012(\u0012&\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000 \u0005*\u0012\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000\u0018\u00010\u000f0\u000f \u0005*.\u0012(\u0012&\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000 \u0005*\u0012\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000\u0018\u00010\u000f0\u000f\u0018\u00010\u00100\u000e\"\u0010\b\u0000\u0010\n*\n \u0005*\u0004\u0018\u00010\t0\t2d\u0010\u0003\u001a`\u0012*\b\u0001\u0012&\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000 \u0005*\u0012\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000\u0018\u00010\f0\f \u0005*.\u0012(\u0012&\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000 \u0005*\u0012\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000\u0018\u00010\f0\f\u0018\u00010\r0\u000bH\u0001Jõ\u0001\u0010\u0011\u001a^\u0012(\u0012&\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000 \u0005*\u0012\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000\u0018\u00010\u000f0\u000f \u0005*.\u0012(\u0012&\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000 \u0005*\u0012\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000\u0018\u00010\u000f0\u000f\u0018\u00010\u00100\u000e\"\u0010\b\u0000\u0010\n*\n \u0005*\u0004\u0018\u00010\t0\t2d\u0010\u0003\u001a`\u0012*\b\u0001\u0012&\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000 \u0005*\u0012\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000\u0018\u00010\f0\f \u0005*.\u0012(\u0012&\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000 \u0005*\u0012\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000\u0018\u00010\f0\f\u0018\u00010\r0\u000b2\u0006\u0010\u0006\u001a\u00020\u00022\u000e\u0010\u0012\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004H\u0001J\u0001\u0010\u0013\u001a\n \u0005*\u0004\u0018\u00018\u00008\u0000\"\u0010\b\u0000\u0010\n*\n \u0005*\u0004\u0018\u00010\t0\t2d\u0010\u0003\u001a`\u0012*\b\u0001\u0012&\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000 \u0005*\u0012\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000\u0018\u00010\f0\f \u0005*.\u0012(\u0012&\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000 \u0005*\u0012\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000\u0018\u00010\f0\f\u0018\u00010\r0\u000bH\u0001¢\u0006\u0004\b\u0013\u0010\u0014J¨\u0001\u0010\u0013\u001a\n \u0005*\u0004\u0018\u00018\u00008\u0000\"\u0010\b\u0000\u0010\n*\n \u0005*\u0004\u0018\u00010\t0\t2d\u0010\u0003\u001a`\u0012*\b\u0001\u0012&\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000 \u0005*\u0012\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000\u0018\u00010\f0\f \u0005*.\u0012(\u0012&\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000 \u0005*\u0012\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000\u0018\u00010\f0\f\u0018\u00010\r0\u000b2\u0006\u0010\u0006\u001a\u00020\u00022\u000e\u0010\u0012\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004H\u0001¢\u0006\u0004\b\u0013\u0010\u0015J\t\u0010\u0016\u001a\u00020\u0007H\u0001J\t\u0010\u0017\u001a\u00020\u0007H\u0001J\t\u0010\u0019\u001a\u00020\u0018H\u0001J-\u0010\u001b\u001a&\u0012\f\u0012\n \u0005*\u0004\u0018\u00010\u001a0\u001a \u0005*\u0012\u0012\f\u0012\n \u0005*\u0004\u0018\u00010\u001a0\u001a\u0018\u00010\u00100\u000eH\u0001J)\u0010\u001c\u001a\u0012\u0012\u0002\b\u0003 \u0005*\b\u0012\u0002\b\u0003\u0018\u00010\u000f0\u000f2\u000e\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u001a0\u001aH\u0001Jf\u0010\u001c\u001a&\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000 \u0005*\u0012\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000\u0018\u00010\u000f0\u000f\"\u0010\b\u0000\u0010\n*\n \u0005*\u0004\u0018\u00010\t0\t2\u000e\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u001a0\u001a2\u000e\u0010\u0006\u001a\n \u0005*\u0004\u0018\u00018\u00008\u0000H\u0001¢\u0006\u0004\b\u001c\u0010\u001dJk\u0010\u001c\u001a&\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000 \u0005*\u0012\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000\u0018\u00010\u000f0\u000f\"\u0010\b\u0000\u0010\n*\n \u0005*\u0004\u0018\u00010\t0\t2*\u0010\u0003\u001a&\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000 \u0005*\u0012\u0012\f\u0012\n \u0005*\u0004\u0018\u00018\u00008\u0000\u0018\u00010\f0\fH\u0001J\u0012\u0010\u001f\u001a\u00020\u00182\b\u0010\u001e\u001a\u0004\u0018\u00010\u001aH\u0016R\u0017\u0010 \u001a\u00020\u00018\u0006¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#R\u0017\u0010'\u001a\b\u0012\u0004\u0012\u00020\u001a0$8F¢\u0006\u0006\u001a\u0004\b%\u0010&R\u0011\u0010+\u001a\u00020(8F¢\u0006\u0006\u001a\u0004\b)\u0010*R\u0011\u0010-\u001a\u00020(8F¢\u0006\u0006\u001a\u0004\b,\u0010*¨\u00060"}, d2 = {"Lorg/signal/core/util/concurrent/TracingExecutorService;", "Ljava/util/concurrent/ExecutorService;", "", "p0", "Ljava/util/concurrent/TimeUnit;", "kotlin.jvm.PlatformType", "p1", "", "awaitTermination", "", "T", "", "Ljava/util/concurrent/Callable;", "", "", "Ljava/util/concurrent/Future;", "", "invokeAll", "p2", "invokeAny", "(Ljava/util/Collection;)Ljava/lang/Object;", "(Ljava/util/Collection;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;", "isShutdown", "isTerminated", "", "shutdown", "Ljava/lang/Runnable;", "shutdownNow", "submit", "(Ljava/lang/Runnable;Ljava/lang/Object;)Ljava/util/concurrent/Future;", "command", "execute", "wrapped", "Ljava/util/concurrent/ExecutorService;", "getWrapped", "()Ljava/util/concurrent/ExecutorService;", "Ljava/util/Queue;", "getQueue", "()Ljava/util/Queue;", "queue", "", "getActiveCount", "()I", "activeCount", "getMaximumPoolSize", "maximumPoolSize", "<init>", "(Ljava/util/concurrent/ExecutorService;)V", "core-util_release"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes3.dex */
public final class TracingExecutorService implements ExecutorService {
    private final ExecutorService wrapped;

    @Override // java.util.concurrent.ExecutorService
    public boolean awaitTermination(long j, TimeUnit timeUnit) {
        return this.wrapped.awaitTermination(j, timeUnit);
    }

    @Override // java.util.concurrent.ExecutorService
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection) {
        return this.wrapped.invokeAll(collection);
    }

    @Override // java.util.concurrent.ExecutorService
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> collection, long j, TimeUnit timeUnit) {
        return this.wrapped.invokeAll(collection, j, timeUnit);
    }

    @Override // java.util.concurrent.ExecutorService
    public <T> T invokeAny(Collection<? extends Callable<T>> collection) {
        return (T) this.wrapped.invokeAny(collection);
    }

    @Override // java.util.concurrent.ExecutorService
    public <T> T invokeAny(Collection<? extends Callable<T>> collection, long j, TimeUnit timeUnit) {
        return (T) this.wrapped.invokeAny(collection, j, timeUnit);
    }

    @Override // java.util.concurrent.ExecutorService
    public boolean isShutdown() {
        return this.wrapped.isShutdown();
    }

    @Override // java.util.concurrent.ExecutorService
    public boolean isTerminated() {
        return this.wrapped.isTerminated();
    }

    @Override // java.util.concurrent.ExecutorService
    public void shutdown() {
        this.wrapped.shutdown();
    }

    @Override // java.util.concurrent.ExecutorService
    public List<Runnable> shutdownNow() {
        return this.wrapped.shutdownNow();
    }

    @Override // java.util.concurrent.ExecutorService
    public Future<?> submit(Runnable runnable) {
        return this.wrapped.submit(runnable);
    }

    @Override // java.util.concurrent.ExecutorService
    public <T> Future<T> submit(Runnable runnable, T t) {
        return this.wrapped.submit(runnable, t);
    }

    @Override // java.util.concurrent.ExecutorService
    public <T> Future<T> submit(Callable<T> callable) {
        return this.wrapped.submit(callable);
    }

    public TracingExecutorService(ExecutorService executorService) {
        Intrinsics.checkNotNullParameter(executorService, "wrapped");
        this.wrapped = executorService;
    }

    @Override // java.util.concurrent.Executor
    public void execute(Runnable runnable) {
        this.wrapped.execute(new Runnable(new Throwable(), runnable) { // from class: org.signal.core.util.concurrent.TracingExecutorService$$ExternalSyntheticLambda0
            public final /* synthetic */ Throwable f$0;
            public final /* synthetic */ Runnable f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                TracingExecutorService.m185execute$lambda0(this.f$0, this.f$1);
            }
        });
    }

    /* renamed from: execute$lambda-0 */
    public static final void m185execute$lambda0(Throwable th, Runnable runnable) {
        Intrinsics.checkNotNullParameter(th, "$callerStackTrace");
        Thread currentThread = Thread.currentThread();
        Intrinsics.checkNotNullExpressionValue(currentThread, "currentThread()");
        Thread.UncaughtExceptionHandler uncaughtExceptionHandler = currentThread.getUncaughtExceptionHandler();
        if (uncaughtExceptionHandler instanceof TracingUncaughtExceptionHandler) {
            uncaughtExceptionHandler = ((TracingUncaughtExceptionHandler) uncaughtExceptionHandler).getOriginalHandler();
        }
        currentThread.setUncaughtExceptionHandler(new TracingUncaughtExceptionHandler(uncaughtExceptionHandler, th));
        TracedThreads tracedThreads = TracedThreads.INSTANCE;
        tracedThreads.getCallerStackTraces().put(Long.valueOf(currentThread.getId()), th);
        if (runnable != null) {
            try {
                runnable.run();
            } catch (Throwable th2) {
                TracedThreads.INSTANCE.getCallerStackTraces().remove(Long.valueOf(currentThread.getId()));
                throw th2;
            }
        }
        tracedThreads.getCallerStackTraces().remove(Long.valueOf(currentThread.getId()));
    }

    public final Queue<Runnable> getQueue() {
        ExecutorService executorService = this.wrapped;
        if (!(executorService instanceof ThreadPoolExecutor)) {
            return new LinkedBlockingQueue();
        }
        BlockingQueue<Runnable> queue = ((ThreadPoolExecutor) executorService).getQueue();
        Intrinsics.checkNotNullExpressionValue(queue, "wrapped.queue");
        return queue;
    }

    public final int getActiveCount() {
        ExecutorService executorService = this.wrapped;
        if (executorService instanceof ThreadPoolExecutor) {
            return ((ThreadPoolExecutor) executorService).getActiveCount();
        }
        return 0;
    }

    public final int getMaximumPoolSize() {
        ExecutorService executorService = this.wrapped;
        if (executorService instanceof ThreadPoolExecutor) {
            return ((ThreadPoolExecutor) executorService).getMaximumPoolSize();
        }
        return 0;
    }
}
