package org.signal.core.util;

import android.content.ContentValues;
import androidx.sqlite.db.SupportSQLiteDatabase;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: SQLiteDatabaseExtensions.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B3\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0005\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00050\n¢\u0006\u0002\u0010\u000bJ\u0010\u0010\r\u001a\u00020\u000e2\b\b\u0002\u0010\u000f\u001a\u00020\u000eR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00050\nX\u0004¢\u0006\u0004\n\u0002\u0010\f¨\u0006\u0010"}, d2 = {"Lorg/signal/core/util/UpdateBuilderPart3;", "", "db", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "tableName", "", "values", "Landroid/content/ContentValues;", "where", "whereArgs", "", "(Landroidx/sqlite/db/SupportSQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)V", "[Ljava/lang/String;", "run", "", "conflictStrategy", "core-util_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class UpdateBuilderPart3 {
    private final SupportSQLiteDatabase db;
    private final String tableName;
    private final ContentValues values;
    private final String where;
    private final String[] whereArgs;

    public UpdateBuilderPart3(SupportSQLiteDatabase supportSQLiteDatabase, String str, ContentValues contentValues, String str2, String[] strArr) {
        Intrinsics.checkNotNullParameter(supportSQLiteDatabase, "db");
        Intrinsics.checkNotNullParameter(str, "tableName");
        Intrinsics.checkNotNullParameter(contentValues, "values");
        Intrinsics.checkNotNullParameter(str2, "where");
        Intrinsics.checkNotNullParameter(strArr, "whereArgs");
        this.db = supportSQLiteDatabase;
        this.tableName = str;
        this.values = contentValues;
        this.where = str2;
        this.whereArgs = strArr;
    }

    public static /* synthetic */ int run$default(UpdateBuilderPart3 updateBuilderPart3, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = 0;
        }
        return updateBuilderPart3.run(i);
    }

    public final int run(int i) {
        return this.db.update(this.tableName, i, this.values, this.where, this.whereArgs);
    }
}
