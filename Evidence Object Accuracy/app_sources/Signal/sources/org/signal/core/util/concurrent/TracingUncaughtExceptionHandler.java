package org.signal.core.util.concurrent;

import java.lang.Thread;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.ExceptionUtil;
import org.thoughtcrime.securesms.database.ThreadDatabase;

/* compiled from: TracingUncaughtExceptionHandler.kt */
@Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u0001B\u0017\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0018\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u0004H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0001¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\r"}, d2 = {"Lorg/signal/core/util/concurrent/TracingUncaughtExceptionHandler;", "Ljava/lang/Thread$UncaughtExceptionHandler;", "originalHandler", "callerStackTrace", "", "(Ljava/lang/Thread$UncaughtExceptionHandler;Ljava/lang/Throwable;)V", "getOriginalHandler", "()Ljava/lang/Thread$UncaughtExceptionHandler;", "uncaughtException", "", ThreadDatabase.TABLE_NAME, "Ljava/lang/Thread;", "exception", "core-util_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class TracingUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
    private final Throwable callerStackTrace;
    private final Thread.UncaughtExceptionHandler originalHandler;

    public TracingUncaughtExceptionHandler(Thread.UncaughtExceptionHandler uncaughtExceptionHandler, Throwable th) {
        Intrinsics.checkNotNullParameter(th, "callerStackTrace");
        this.originalHandler = uncaughtExceptionHandler;
        this.callerStackTrace = th;
    }

    public final Thread.UncaughtExceptionHandler getOriginalHandler() {
        return this.originalHandler;
    }

    @Override // java.lang.Thread.UncaughtExceptionHandler
    public void uncaughtException(Thread thread, Throwable th) {
        Intrinsics.checkNotNullParameter(thread, ThreadDatabase.TABLE_NAME);
        Intrinsics.checkNotNullParameter(th, "exception");
        Throwable joinStackTrace = ExceptionUtil.joinStackTrace(th, this.callerStackTrace);
        Thread.UncaughtExceptionHandler uncaughtExceptionHandler = this.originalHandler;
        if (uncaughtExceptionHandler != null) {
            uncaughtExceptionHandler.uncaughtException(thread, joinStackTrace);
        }
    }
}
