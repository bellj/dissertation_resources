package org.signal.core.util;

import android.content.ContentValues;
import androidx.sqlite.db.SupportSQLiteDatabase;
import java.util.Arrays;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: SQLiteDatabaseExtensions.kt */
@Metadata(bv = {}, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\t\u001a\u00020\b\u0012\u0006\u0010\u000b\u001a\u00020\u0002\u0012\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u0010\u0010\u0011J)\u0010\u0003\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0012\u0010\u0005\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00010\u0004\"\u00020\u0001¢\u0006\u0004\b\u0003\u0010\u0007R\u0014\u0010\t\u001a\u00020\b8\u0002X\u0004¢\u0006\u0006\n\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\u00028\u0002X\u0004¢\u0006\u0006\n\u0004\b\u000b\u0010\fR\u0014\u0010\u000e\u001a\u00020\r8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000f¨\u0006\u0012"}, d2 = {"Lorg/signal/core/util/UpdateBuilderPart2;", "", "", "where", "", "whereArgs", "Lorg/signal/core/util/UpdateBuilderPart3;", "(Ljava/lang/String;[Ljava/lang/Object;)Lorg/signal/core/util/UpdateBuilderPart3;", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "db", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "tableName", "Ljava/lang/String;", "Landroid/content/ContentValues;", "values", "Landroid/content/ContentValues;", "<init>", "(Landroidx/sqlite/db/SupportSQLiteDatabase;Ljava/lang/String;Landroid/content/ContentValues;)V", "core-util_release"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes3.dex */
public final class UpdateBuilderPart2 {
    private final SupportSQLiteDatabase db;
    private final String tableName;
    private final ContentValues values;

    public UpdateBuilderPart2(SupportSQLiteDatabase supportSQLiteDatabase, String str, ContentValues contentValues) {
        Intrinsics.checkNotNullParameter(supportSQLiteDatabase, "db");
        Intrinsics.checkNotNullParameter(str, "tableName");
        Intrinsics.checkNotNullParameter(contentValues, "values");
        this.db = supportSQLiteDatabase;
        this.tableName = str;
        this.values = contentValues;
    }

    public final UpdateBuilderPart3 where(String str, Object... objArr) {
        Intrinsics.checkNotNullParameter(str, "where");
        Intrinsics.checkNotNullParameter(objArr, "whereArgs");
        return new UpdateBuilderPart3(this.db, this.tableName, this.values, str, SqlUtil.buildArgs(Arrays.copyOf(objArr, objArr.length)));
    }
}
