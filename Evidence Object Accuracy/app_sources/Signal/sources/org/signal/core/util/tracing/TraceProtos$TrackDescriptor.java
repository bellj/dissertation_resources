package org.signal.core.util.tracing;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import org.signal.core.util.tracing.TraceProtos$CounterDescriptor;
import org.signal.core.util.tracing.TraceProtos$ThreadDescriptor;

/* loaded from: classes3.dex */
public final class TraceProtos$TrackDescriptor extends GeneratedMessageLite<TraceProtos$TrackDescriptor, Builder> implements MessageLiteOrBuilder {
    public static final int COUNTER_FIELD_NUMBER;
    private static final TraceProtos$TrackDescriptor DEFAULT_INSTANCE;
    public static final int NAME_FIELD_NUMBER;
    public static final int PARENT_UUID_FIELD_NUMBER;
    private static volatile Parser<TraceProtos$TrackDescriptor> PARSER;
    public static final int THREAD_FIELD_NUMBER;
    public static final int UUID_FIELD_NUMBER;
    private int bitField0_;
    private TraceProtos$CounterDescriptor counter_;
    private String name_ = "";
    private long parentUuid_;
    private TraceProtos$ThreadDescriptor thread_;
    private long uuid_;

    private TraceProtos$TrackDescriptor() {
    }

    public boolean hasUuid() {
        return (this.bitField0_ & 1) != 0;
    }

    public long getUuid() {
        return this.uuid_;
    }

    public void setUuid(long j) {
        this.bitField0_ |= 1;
        this.uuid_ = j;
    }

    public void clearUuid() {
        this.bitField0_ &= -2;
        this.uuid_ = 0;
    }

    public boolean hasParentUuid() {
        return (this.bitField0_ & 2) != 0;
    }

    public long getParentUuid() {
        return this.parentUuid_;
    }

    public void setParentUuid(long j) {
        this.bitField0_ |= 2;
        this.parentUuid_ = j;
    }

    public void clearParentUuid() {
        this.bitField0_ &= -3;
        this.parentUuid_ = 0;
    }

    public boolean hasName() {
        return (this.bitField0_ & 4) != 0;
    }

    public String getName() {
        return this.name_;
    }

    public ByteString getNameBytes() {
        return ByteString.copyFromUtf8(this.name_);
    }

    public void setName(String str) {
        str.getClass();
        this.bitField0_ |= 4;
        this.name_ = str;
    }

    public void clearName() {
        this.bitField0_ &= -5;
        this.name_ = getDefaultInstance().getName();
    }

    public void setNameBytes(ByteString byteString) {
        this.name_ = byteString.toStringUtf8();
        this.bitField0_ |= 4;
    }

    public boolean hasThread() {
        return (this.bitField0_ & 8) != 0;
    }

    public TraceProtos$ThreadDescriptor getThread() {
        TraceProtos$ThreadDescriptor traceProtos$ThreadDescriptor = this.thread_;
        return traceProtos$ThreadDescriptor == null ? TraceProtos$ThreadDescriptor.getDefaultInstance() : traceProtos$ThreadDescriptor;
    }

    public void setThread(TraceProtos$ThreadDescriptor traceProtos$ThreadDescriptor) {
        traceProtos$ThreadDescriptor.getClass();
        this.thread_ = traceProtos$ThreadDescriptor;
        this.bitField0_ |= 8;
    }

    public void mergeThread(TraceProtos$ThreadDescriptor traceProtos$ThreadDescriptor) {
        traceProtos$ThreadDescriptor.getClass();
        TraceProtos$ThreadDescriptor traceProtos$ThreadDescriptor2 = this.thread_;
        if (traceProtos$ThreadDescriptor2 == null || traceProtos$ThreadDescriptor2 == TraceProtos$ThreadDescriptor.getDefaultInstance()) {
            this.thread_ = traceProtos$ThreadDescriptor;
        } else {
            this.thread_ = TraceProtos$ThreadDescriptor.newBuilder(this.thread_).mergeFrom((TraceProtos$ThreadDescriptor.Builder) traceProtos$ThreadDescriptor).buildPartial();
        }
        this.bitField0_ |= 8;
    }

    public void clearThread() {
        this.thread_ = null;
        this.bitField0_ &= -9;
    }

    public boolean hasCounter() {
        return (this.bitField0_ & 16) != 0;
    }

    public TraceProtos$CounterDescriptor getCounter() {
        TraceProtos$CounterDescriptor traceProtos$CounterDescriptor = this.counter_;
        return traceProtos$CounterDescriptor == null ? TraceProtos$CounterDescriptor.getDefaultInstance() : traceProtos$CounterDescriptor;
    }

    public void setCounter(TraceProtos$CounterDescriptor traceProtos$CounterDescriptor) {
        traceProtos$CounterDescriptor.getClass();
        this.counter_ = traceProtos$CounterDescriptor;
        this.bitField0_ |= 16;
    }

    public void mergeCounter(TraceProtos$CounterDescriptor traceProtos$CounterDescriptor) {
        traceProtos$CounterDescriptor.getClass();
        TraceProtos$CounterDescriptor traceProtos$CounterDescriptor2 = this.counter_;
        if (traceProtos$CounterDescriptor2 == null || traceProtos$CounterDescriptor2 == TraceProtos$CounterDescriptor.getDefaultInstance()) {
            this.counter_ = traceProtos$CounterDescriptor;
        } else {
            this.counter_ = TraceProtos$CounterDescriptor.newBuilder(this.counter_).mergeFrom((TraceProtos$CounterDescriptor.Builder) traceProtos$CounterDescriptor).buildPartial();
        }
        this.bitField0_ |= 16;
    }

    public void clearCounter() {
        this.counter_ = null;
        this.bitField0_ &= -17;
    }

    public static TraceProtos$TrackDescriptor parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (TraceProtos$TrackDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static TraceProtos$TrackDescriptor parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TraceProtos$TrackDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static TraceProtos$TrackDescriptor parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (TraceProtos$TrackDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static TraceProtos$TrackDescriptor parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TraceProtos$TrackDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static TraceProtos$TrackDescriptor parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (TraceProtos$TrackDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static TraceProtos$TrackDescriptor parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (TraceProtos$TrackDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static TraceProtos$TrackDescriptor parseFrom(InputStream inputStream) throws IOException {
        return (TraceProtos$TrackDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static TraceProtos$TrackDescriptor parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TraceProtos$TrackDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static TraceProtos$TrackDescriptor parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (TraceProtos$TrackDescriptor) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static TraceProtos$TrackDescriptor parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TraceProtos$TrackDescriptor) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static TraceProtos$TrackDescriptor parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (TraceProtos$TrackDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static TraceProtos$TrackDescriptor parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (TraceProtos$TrackDescriptor) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(TraceProtos$TrackDescriptor traceProtos$TrackDescriptor) {
        return DEFAULT_INSTANCE.createBuilder(traceProtos$TrackDescriptor);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<TraceProtos$TrackDescriptor, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(TraceProtos$1 traceProtos$1) {
            this();
        }

        private Builder() {
            super(TraceProtos$TrackDescriptor.DEFAULT_INSTANCE);
        }

        public Builder setUuid(long j) {
            copyOnWrite();
            ((TraceProtos$TrackDescriptor) this.instance).setUuid(j);
            return this;
        }

        public Builder setName(String str) {
            copyOnWrite();
            ((TraceProtos$TrackDescriptor) this.instance).setName(str);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (TraceProtos$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new TraceProtos$TrackDescriptor();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0005\u0000\u0001\u0001\b\u0005\u0000\u0000\u0000\u0001ဃ\u0000\u0002ဈ\u0002\u0004ဉ\u0003\u0005ဃ\u0001\bဉ\u0004", new Object[]{"bitField0_", "uuid_", "name_", "thread_", "parentUuid_", "counter_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<TraceProtos$TrackDescriptor> parser = PARSER;
                if (parser == null) {
                    synchronized (TraceProtos$TrackDescriptor.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        TraceProtos$TrackDescriptor traceProtos$TrackDescriptor = new TraceProtos$TrackDescriptor();
        DEFAULT_INSTANCE = traceProtos$TrackDescriptor;
        GeneratedMessageLite.registerDefaultInstance(TraceProtos$TrackDescriptor.class, traceProtos$TrackDescriptor);
    }

    public static TraceProtos$TrackDescriptor getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<TraceProtos$TrackDescriptor> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
