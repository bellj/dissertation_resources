package org.signal.core.util;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import org.signal.core.util.concurrent.TracingExecutor;
import org.signal.core.util.concurrent.TracingExecutorService;

/* loaded from: classes3.dex */
public final class ThreadUtil {
    public static volatile boolean enforceAssertions;
    private static volatile Handler handler;

    private ThreadUtil() {
    }

    private static Handler getHandler() {
        if (handler == null) {
            synchronized (ThreadUtil.class) {
                if (handler == null) {
                    handler = new Handler(Looper.getMainLooper());
                }
            }
        }
        return handler;
    }

    public static boolean isMainThread() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    public static void assertMainThread() {
        if (!isMainThread() && enforceAssertions) {
            throw new AssertionError("Must run on main thread.");
        }
    }

    public static void assertNotMainThread() {
        if (isMainThread() && enforceAssertions) {
            throw new AssertionError("Cannot run on main thread.");
        }
    }

    public static void postToMain(Runnable runnable) {
        getHandler().post(runnable);
    }

    public static void runOnMain(Runnable runnable) {
        if (isMainThread()) {
            runnable.run();
        } else {
            getHandler().post(runnable);
        }
    }

    public static void runOnMainDelayed(Runnable runnable, long j) {
        getHandler().postDelayed(runnable, j);
    }

    public static void cancelRunnableOnMain(Runnable runnable) {
        getHandler().removeCallbacks(runnable);
    }

    public static void runOnMainSync(Runnable runnable) {
        if (isMainThread()) {
            runnable.run();
            return;
        }
        CountDownLatch countDownLatch = new CountDownLatch(1);
        runOnMain(new Runnable(runnable, countDownLatch) { // from class: org.signal.core.util.ThreadUtil$$ExternalSyntheticLambda0
            public final /* synthetic */ Runnable f$0;
            public final /* synthetic */ CountDownLatch f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ThreadUtil.$r8$lambda$KLNcOCwumB7juFNy50KHIsesWd4(this.f$0, this.f$1);
            }
        });
        try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            throw new AssertionError(e);
        }
    }

    public static /* synthetic */ void lambda$runOnMainSync$0(Runnable runnable, CountDownLatch countDownLatch) {
        try {
            runnable.run();
        } finally {
            countDownLatch.countDown();
        }
    }

    public static void sleep(long j) {
        try {
            Thread.sleep(j);
        } catch (InterruptedException e) {
            throw new AssertionError(e);
        }
    }

    public static void interruptableSleep(long j) {
        try {
            Thread.sleep(j);
        } catch (InterruptedException unused) {
        }
    }

    public static Executor trace(Executor executor) {
        return new TracingExecutor(executor);
    }

    public static ExecutorService trace(ExecutorService executorService) {
        return new TracingExecutorService(executorService);
    }
}
