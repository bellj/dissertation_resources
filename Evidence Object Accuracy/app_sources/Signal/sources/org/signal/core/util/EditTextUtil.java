package org.signal.core.util;

import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.InputFilter;
import android.widget.EditText;
import android.widget.TextView;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;

/* loaded from: classes3.dex */
public final class EditTextUtil {
    public static void addGraphemeClusterLimitFilter(EditText editText, int i) {
        ArrayList arrayList = new ArrayList(Arrays.asList(editText.getFilters()));
        arrayList.add(new GraphemeClusterLimitFilter(i));
        editText.setFilters((InputFilter[]) arrayList.toArray(new InputFilter[0]));
    }

    public static void setCursorColor(EditText editText, int i) {
        if (Build.VERSION.SDK_INT >= 29) {
            Drawable textCursorDrawable = editText.getTextCursorDrawable();
            if (textCursorDrawable != null) {
                Drawable mutate = textCursorDrawable.mutate();
                mutate.setColorFilter(new PorterDuffColorFilter(i, PorterDuff.Mode.SRC_IN));
                editText.setTextCursorDrawable(mutate);
                return;
            }
            return;
        }
        setCursorColorViaReflection(editText, i);
    }

    private static void setCursorColorViaReflection(EditText editText, int i) {
        try {
            Field declaredField = TextView.class.getDeclaredField("mCursorDrawableRes");
            declaredField.setAccessible(true);
            int i2 = declaredField.getInt(editText);
            Field declaredField2 = TextView.class.getDeclaredField("mEditor");
            declaredField2.setAccessible(true);
            Object obj = declaredField2.get(editText);
            Field declaredField3 = obj.getClass().getDeclaredField("mCursorDrawable");
            declaredField3.setAccessible(true);
            Drawable[] drawableArr = {editText.getContext().getResources().getDrawable(i2), editText.getContext().getResources().getDrawable(i2)};
            drawableArr[0].setColorFilter(i, PorterDuff.Mode.SRC_IN);
            drawableArr[1].setColorFilter(i, PorterDuff.Mode.SRC_IN);
            declaredField3.set(obj, drawableArr);
        } catch (Throwable unused) {
        }
    }
}
