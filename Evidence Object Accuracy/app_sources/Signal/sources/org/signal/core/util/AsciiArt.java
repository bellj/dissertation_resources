package org.signal.core.util;

import android.database.Cursor;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt__MutableCollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsJVMKt;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* compiled from: AsciiArt.kt */
@Metadata(bv = {}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00022\u00020\u0001:\u0002\u0002\u0003¨\u0006\u0004"}, d2 = {"Lorg/signal/core/util/AsciiArt;", "", "Companion", "Table", "core-util_release"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes3.dex */
public final class AsciiArt {
    public static final Companion Companion = new Companion(null);

    @JvmStatic
    public static final String tableFor(Cursor cursor) {
        return Companion.tableFor(cursor);
    }

    /* compiled from: AsciiArt.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0002\u0018\u00002\u00020\u0001B'\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0012\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u0003¢\u0006\u0002\u0010\u0006J\b\u0010\u0007\u001a\u00020\u0004H\u0016R\u0014\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lorg/signal/core/util/AsciiArt$Table;", "", "columns", "", "", "rows", "(Ljava/util/List;Ljava/util/List;)V", "toString", "core-util_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Table {
        private final List<String> columns;
        private final List<List<String>> rows;

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends java.util.List<java.lang.String>> */
        /* JADX WARN: Multi-variable type inference failed */
        public Table(List<String> list, List<? extends List<String>> list2) {
            Intrinsics.checkNotNullParameter(list, "columns");
            Intrinsics.checkNotNullParameter(list2, "rows");
            this.columns = list;
            this.rows = list2;
        }

        public String toString() {
            List<String> list = this.columns;
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
            for (String str : list) {
                arrayList.add(Integer.valueOf(str.length()));
            }
            int[] iArr = CollectionsKt___CollectionsKt.toIntArray(arrayList);
            Iterator<T> it = this.rows.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                List list2 = (List) it.next();
                int i = 0;
                for (int i2 : iArr) {
                    i++;
                    iArr[i] = Math.max(((String) list2.get(i)).length(), i2);
                }
            }
            StringBuilder sb = new StringBuilder();
            int i3 = 0;
            for (Object obj : this.columns) {
                i3++;
                if (i3 < 0) {
                    CollectionsKt__CollectionsKt.throwIndexOverflow();
                }
                sb.append("|");
                sb.append(" ");
                sb.append(AsciiArt.Companion.rightPad((String) obj, iArr[i3]));
                sb.append(" ");
            }
            sb.append("|");
            sb.append("\n");
            for (int i4 : iArr) {
                sb.append("|");
                sb.append(StringsKt__StringsJVMKt.repeat("-", i4 + 2));
            }
            sb.append("|");
            sb.append("\n");
            Iterator<T> it2 = this.rows.iterator();
            while (it2.hasNext()) {
                int i5 = 0;
                for (Object obj2 : (List) it2.next()) {
                    i5++;
                    if (i5 < 0) {
                        CollectionsKt__CollectionsKt.throwIndexOverflow();
                    }
                    sb.append("|");
                    sb.append(" ");
                    sb.append(AsciiArt.Companion.rightPad((String) obj2, iArr[i5]));
                    sb.append(" ");
                }
                sb.append("|");
                sb.append("\n");
            }
            String sb2 = sb.toString();
            Intrinsics.checkNotNullExpressionValue(sb2, "builder.toString()");
            return sb2;
        }
    }

    /* compiled from: AsciiArt.kt */
    @Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\tH\u0002J\u0010\u0010\n\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\fH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/signal/core/util/AsciiArt$Companion;", "", "()V", "COLUMN_DIVIDER", "", "ROW_DIVIDER", "rightPad", DraftDatabase.DRAFT_VALUE, "length", "", "tableFor", "cursor", "Landroid/database/Cursor;", "core-util_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final String tableFor(Cursor cursor) {
            Intrinsics.checkNotNullParameter(cursor, "cursor");
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            String[] columnNames = cursor.getColumnNames();
            Intrinsics.checkNotNullExpressionValue(columnNames, "cursor.columnNames");
            boolean unused = CollectionsKt__MutableCollectionsKt.addAll(arrayList, columnNames);
            while (cursor.moveToNext()) {
                ArrayList arrayList3 = new ArrayList();
                int size = arrayList.size();
                for (int i = 0; i < size; i++) {
                    arrayList3.add(cursor.getString(i));
                }
                arrayList2.add(arrayList3);
            }
            return new Table(arrayList, arrayList2).toString();
        }

        public final String rightPad(String str, int i) {
            if (str.length() >= i) {
                return str;
            }
            StringBuilder sb = new StringBuilder(str);
            while (sb.length() < i) {
                sb.append(" ");
            }
            String sb2 = sb.toString();
            Intrinsics.checkNotNullExpressionValue(sb2, "out.toString()");
            return sb2;
        }
    }
}
