package org.signal.core.util;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* loaded from: classes3.dex */
public abstract class DimensionUnit extends Enum<DimensionUnit> {
    public abstract float toPixels(float f);
}
