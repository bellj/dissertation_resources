package org.signal.core.util.concurrent;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class DeadlockDetector$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ DeadlockDetector f$0;

    public /* synthetic */ DeadlockDetector$$ExternalSyntheticLambda0(DeadlockDetector deadlockDetector) {
        this.f$0 = deadlockDetector;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.poll();
    }
}
