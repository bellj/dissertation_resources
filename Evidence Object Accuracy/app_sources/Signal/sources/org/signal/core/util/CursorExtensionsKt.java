package org.signal.core.util;

import android.database.Cursor;
import j$.util.Optional;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: CursorExtensions.kt */
@Metadata(bv = {}, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0012\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u001a\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u0001*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001\u001a\u0012\u0010\u0004\u001a\u00020\u0001*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001\u001a\u0018\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00010\u0005*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001\u001a\u0012\u0010\b\u001a\u00020\u0007*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001\u001a\u0018\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00070\u0005*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001\u001a\u0012\u0010\u000b\u001a\u00020\n*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001\u001a\u0012\u0010\r\u001a\u00020\f*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001\u001a\u0018\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\f0\u0005*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001\u001a\u0014\u0010\u0010\u001a\u0004\u0018\u00010\u000f*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001\u001a\u0012\u0010\u0011\u001a\u00020\u000f*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001\u001a\u0018\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0005*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001\u001a-\u0010\u0016\u001a\u00028\u0000\"\u0004\b\u0000\u0010\u0013*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u00012\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00028\u00000\u0014¢\u0006\u0004\b\u0016\u0010\u0017\u001a\n\u0010\u0018\u001a\u00020\u0007*\u00020\f¨\u0006\u0019"}, d2 = {"Landroid/database/Cursor;", "", "column", "requireString", "requireNonNullString", "j$/util/Optional", "optionalString", "", "requireInt", "optionalInt", "", "requireLong", "", "requireBoolean", "optionalBoolean", "", "requireBlob", "requireNonNullBlob", "optionalBlob", "T", "Lorg/signal/core/util/LongSerializer;", "serializer", "requireObject", "(Landroid/database/Cursor;Ljava/lang/String;Lorg/signal/core/util/LongSerializer;)Ljava/lang/Object;", "toInt", "core-util_release"}, k = 2, mv = {1, 6, 0})
/* loaded from: classes3.dex */
public final class CursorExtensionsKt {
    public static final int toInt(boolean z) {
        return z ? 1 : 0;
    }

    public static final String requireString(Cursor cursor, String str) {
        Intrinsics.checkNotNullParameter(cursor, "<this>");
        Intrinsics.checkNotNullParameter(str, "column");
        return CursorUtil.requireString(cursor, str);
    }

    public static final String requireNonNullString(Cursor cursor, String str) {
        Intrinsics.checkNotNullParameter(cursor, "<this>");
        Intrinsics.checkNotNullParameter(str, "column");
        String requireString = CursorUtil.requireString(cursor, str);
        Intrinsics.checkNotNull(requireString);
        return requireString;
    }

    public static final Optional<String> optionalString(Cursor cursor, String str) {
        Intrinsics.checkNotNullParameter(cursor, "<this>");
        Intrinsics.checkNotNullParameter(str, "column");
        Optional<String> string = CursorUtil.getString(cursor, str);
        Intrinsics.checkNotNullExpressionValue(string, "getString(this, column)");
        return string;
    }

    public static final int requireInt(Cursor cursor, String str) {
        Intrinsics.checkNotNullParameter(cursor, "<this>");
        Intrinsics.checkNotNullParameter(str, "column");
        return CursorUtil.requireInt(cursor, str);
    }

    public static final Optional<Integer> optionalInt(Cursor cursor, String str) {
        Intrinsics.checkNotNullParameter(cursor, "<this>");
        Intrinsics.checkNotNullParameter(str, "column");
        Optional<Integer> optional = CursorUtil.getInt(cursor, str);
        Intrinsics.checkNotNullExpressionValue(optional, "getInt(this, column)");
        return optional;
    }

    public static final long requireLong(Cursor cursor, String str) {
        Intrinsics.checkNotNullParameter(cursor, "<this>");
        Intrinsics.checkNotNullParameter(str, "column");
        return CursorUtil.requireLong(cursor, str);
    }

    public static final boolean requireBoolean(Cursor cursor, String str) {
        Intrinsics.checkNotNullParameter(cursor, "<this>");
        Intrinsics.checkNotNullParameter(str, "column");
        return CursorUtil.requireInt(cursor, str) != 0;
    }

    public static final Optional<Boolean> optionalBoolean(Cursor cursor, String str) {
        Intrinsics.checkNotNullParameter(cursor, "<this>");
        Intrinsics.checkNotNullParameter(str, "column");
        Optional<Boolean> optional = CursorUtil.getBoolean(cursor, str);
        Intrinsics.checkNotNullExpressionValue(optional, "getBoolean(this, column)");
        return optional;
    }

    public static final byte[] requireBlob(Cursor cursor, String str) {
        Intrinsics.checkNotNullParameter(cursor, "<this>");
        Intrinsics.checkNotNullParameter(str, "column");
        return CursorUtil.requireBlob(cursor, str);
    }

    public static final byte[] requireNonNullBlob(Cursor cursor, String str) {
        Intrinsics.checkNotNullParameter(cursor, "<this>");
        Intrinsics.checkNotNullParameter(str, "column");
        byte[] requireBlob = CursorUtil.requireBlob(cursor, str);
        Intrinsics.checkNotNull(requireBlob);
        return requireBlob;
    }

    public static final Optional<byte[]> optionalBlob(Cursor cursor, String str) {
        Intrinsics.checkNotNullParameter(cursor, "<this>");
        Intrinsics.checkNotNullParameter(str, "column");
        Optional<byte[]> blob = CursorUtil.getBlob(cursor, str);
        Intrinsics.checkNotNullExpressionValue(blob, "getBlob(this, column)");
        return blob;
    }

    public static final <T> T requireObject(Cursor cursor, String str, LongSerializer<T> longSerializer) {
        Intrinsics.checkNotNullParameter(cursor, "<this>");
        Intrinsics.checkNotNullParameter(str, "column");
        Intrinsics.checkNotNullParameter(longSerializer, "serializer");
        return longSerializer.deserialize(Long.valueOf(CursorUtil.requireLong(cursor, str)));
    }
}
