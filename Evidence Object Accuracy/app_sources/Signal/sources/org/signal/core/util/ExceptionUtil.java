package org.signal.core.util;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import org.signal.core.util.logging.Scrubber;

/* loaded from: classes3.dex */
public final class ExceptionUtil {
    public static <E extends Throwable> E joinStackTrace(E e, Throwable th) {
        e.setStackTrace(joinStackTrace(e.getStackTrace(), th.getStackTrace()));
        return e;
    }

    public static StackTraceElement[] joinStackTrace(StackTraceElement[] stackTraceElementArr, StackTraceElement[] stackTraceElementArr2) {
        StackTraceElement[] stackTraceElementArr3 = new StackTraceElement[stackTraceElementArr.length + stackTraceElementArr2.length + 2];
        System.arraycopy(stackTraceElementArr, 0, stackTraceElementArr3, 0, stackTraceElementArr.length);
        stackTraceElementArr3[stackTraceElementArr.length] = new StackTraceElement("[[ ↑↑ Original Trace ↑↑ ]]", "", "", 0);
        stackTraceElementArr3[stackTraceElementArr.length + 1] = new StackTraceElement("[[ ↓↓ Inferred Trace ↓↓ ]]", "", "", 0);
        System.arraycopy(stackTraceElementArr2, 0, stackTraceElementArr3, stackTraceElementArr.length + 2, stackTraceElementArr2.length);
        return stackTraceElementArr3;
    }

    public static <E extends Throwable> E joinStackTraceAndMessage(E e) {
        StackTraceElement[] stackTrace = e.getStackTrace();
        StackTraceElement[] stackTraceElementArr = new StackTraceElement[stackTrace.length + 3];
        System.arraycopy(stackTrace, 0, stackTraceElementArr, 0, stackTrace.length);
        String charSequence = Scrubber.scrub(e.getMessage() != null ? e.getMessage() : "null").toString();
        if (charSequence.startsWith("Context.startForegroundService")) {
            try {
                charSequence = charSequence.substring(charSequence.lastIndexOf(46) + 1, charSequence.length() - 1) + " did not call startForeground";
            } catch (Exception unused) {
            }
        }
        stackTraceElementArr[stackTrace.length] = new StackTraceElement("[[ ↑↑ Original Trace ↑↑ ]]", "", "", 0);
        stackTraceElementArr[stackTrace.length + 1] = new StackTraceElement("[[ ↓↓ Exception Message ↓↓ ]]", "", "", 0);
        stackTraceElementArr[stackTrace.length + 2] = new StackTraceElement(charSequence, "", "", 0);
        e.setStackTrace(stackTraceElementArr);
        return e;
    }

    public static String convertThrowableToString(Throwable th) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        th.printStackTrace(new PrintStream(byteArrayOutputStream));
        return byteArrayOutputStream.toString();
    }
}
