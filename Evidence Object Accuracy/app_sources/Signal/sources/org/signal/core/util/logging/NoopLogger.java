package org.signal.core.util.logging;

import org.signal.core.util.logging.Log;

/* loaded from: classes.dex */
class NoopLogger extends Log.Logger {
    @Override // org.signal.core.util.logging.Log.Logger
    public void d(String str, String str2, Throwable th, boolean z) {
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void e(String str, String str2, Throwable th, boolean z) {
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void flush() {
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void i(String str, String str2, Throwable th, boolean z) {
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void v(String str, String str2, Throwable th, boolean z) {
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void w(String str, String str2, Throwable th, boolean z) {
    }
}
