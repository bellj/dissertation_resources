package org.signal.core.util;

import android.icu.text.BreakIterator;
import android.os.Build;
import java.util.Iterator;

/* loaded from: classes3.dex */
public final class CharacterIterable implements Iterable<String> {
    private final String string;

    /* loaded from: classes3.dex */
    public interface BreakIteratorCompat {
        int first();

        boolean isDone(int i);

        int next();
    }

    public CharacterIterable(String str) {
        this.string = str;
    }

    @Override // java.lang.Iterable
    public Iterator<String> iterator() {
        return new CharacterIterator();
    }

    /* access modifiers changed from: private */
    /* loaded from: classes3.dex */
    public class CharacterIterator implements Iterator<String> {
        private final BreakIteratorCompat breakIterator;
        private int lastIndex = -2;

        CharacterIterator() {
            BreakIteratorCompat breakIteratorCompat;
            CharacterIterable.this = r3;
            if (Build.VERSION.SDK_INT >= 24) {
                breakIteratorCompat = new AndroidIcuBreakIterator(r3.string);
            } else {
                breakIteratorCompat = new FallbackBreakIterator(r3.string);
            }
            this.breakIterator = breakIteratorCompat;
        }

        @Override // java.util.Iterator
        public boolean hasNext() {
            if (this.lastIndex == -2) {
                this.lastIndex = this.breakIterator.first();
            }
            return !this.breakIterator.isDone(this.lastIndex);
        }

        @Override // java.util.Iterator
        public String next() {
            int i = this.lastIndex;
            this.lastIndex = this.breakIterator.next();
            return CharacterIterable.this.string.substring(i, this.lastIndex);
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes3.dex */
    public static class AndroidIcuBreakIterator implements BreakIteratorCompat {
        private final BreakIterator breakIterator;

        @Override // org.signal.core.util.CharacterIterable.BreakIteratorCompat
        public boolean isDone(int i) {
            return i == -1;
        }

        public AndroidIcuBreakIterator(String str) {
            BreakIterator characterInstance = BreakIterator.getCharacterInstance();
            this.breakIterator = characterInstance;
            characterInstance.setText(str);
        }

        @Override // org.signal.core.util.CharacterIterable.BreakIteratorCompat
        public int first() {
            return this.breakIterator.first();
        }

        @Override // org.signal.core.util.CharacterIterable.BreakIteratorCompat
        public int next() {
            return this.breakIterator.next();
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes3.dex */
    public static class FallbackBreakIterator implements BreakIteratorCompat {
        private final java.text.BreakIterator breakIterator;

        @Override // org.signal.core.util.CharacterIterable.BreakIteratorCompat
        public boolean isDone(int i) {
            return i == -1;
        }

        public FallbackBreakIterator(String str) {
            java.text.BreakIterator characterInstance = java.text.BreakIterator.getCharacterInstance();
            this.breakIterator = characterInstance;
            characterInstance.setText(str);
        }

        @Override // org.signal.core.util.CharacterIterable.BreakIteratorCompat
        public int first() {
            return this.breakIterator.first();
        }

        @Override // org.signal.core.util.CharacterIterable.BreakIteratorCompat
        public int next() {
            return this.breakIterator.next();
        }
    }
}
