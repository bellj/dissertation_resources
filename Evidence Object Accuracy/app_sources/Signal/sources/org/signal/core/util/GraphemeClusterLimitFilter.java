package org.signal.core.util;

import android.text.InputFilter;
import android.text.Spanned;
import org.signal.core.util.logging.Log;

/* loaded from: classes3.dex */
public final class GraphemeClusterLimitFilter implements InputFilter {
    private static final String TAG = Log.tag(GraphemeClusterLimitFilter.class);
    private final BreakIteratorCompat breakIteratorCompat = BreakIteratorCompat.getInstance();
    private final int max;

    public GraphemeClusterLimitFilter(int i) {
        this.max = i;
    }

    @Override // android.text.InputFilter
    public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        CharSequence subSequence = charSequence.subSequence(i, i2);
        CharSequence subSequence2 = spanned.subSequence(0, i3);
        CharSequence subSequence3 = spanned.subSequence(i4, spanned.length());
        this.breakIteratorCompat.setText(String.format("%s%s%s", subSequence2, subSequence, subSequence3));
        int countBreaks = this.breakIteratorCompat.countBreaks();
        if (countBreaks <= this.max) {
            return charSequence;
        }
        this.breakIteratorCompat.setText(subSequence);
        CharSequence take = this.breakIteratorCompat.take(this.breakIteratorCompat.countBreaks() - (countBreaks - this.max));
        this.breakIteratorCompat.setText(String.format("%s%s%s", subSequence2, take, subSequence3));
        int countBreaks2 = this.breakIteratorCompat.countBreaks();
        if (countBreaks2 <= this.max) {
            return take;
        }
        String str = TAG;
        Log.w(str, "Failed to create string under the required length " + countBreaks2);
        return "";
    }
}
