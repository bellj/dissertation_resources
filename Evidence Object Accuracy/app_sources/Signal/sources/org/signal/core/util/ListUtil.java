package org.signal.core.util;

import java.util.ArrayList;
import java.util.List;

/* loaded from: classes3.dex */
public final class ListUtil {
    public static <E> List<List<E>> chunk(List<E> list, int i) {
        ArrayList arrayList = new ArrayList(list.size() / i);
        int i2 = 0;
        while (i2 < list.size()) {
            int i3 = i2 + i;
            arrayList.add(list.subList(i2, Math.min(list.size(), i3)));
            i2 = i3;
        }
        return arrayList;
    }
}
