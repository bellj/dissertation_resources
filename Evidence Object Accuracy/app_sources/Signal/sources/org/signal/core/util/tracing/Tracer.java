package org.signal.core.util.tracing;

import android.os.SystemClock;
import com.google.protobuf.ByteString;
import j$.util.concurrent.ConcurrentHashMap;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.signal.core.util.tracing.TraceProtos$DebugAnnotation;
import org.signal.core.util.tracing.TraceProtos$Trace;
import org.signal.core.util.tracing.TraceProtos$TrackEvent;
import org.thoughtcrime.securesms.database.AttachmentDatabase;

/* loaded from: classes.dex */
public final class Tracer {
    private static final Tracer INSTANCE = new Tracer();
    private static final long SYNCHRONIZATION_INTERVAL = TimeUnit.SECONDS.toNanos(3);
    private static final byte[] SYNCHRONIZATION_MARKER = toByteArray(UUID.fromString("82477a76-b28d-42ba-81dc-33326d57a079"));
    private final Clock clock = new Clock() { // from class: org.signal.core.util.tracing.Tracer$$ExternalSyntheticLambda0
        @Override // org.signal.core.util.tracing.Tracer.Clock
        public final long getTimeNanos() {
            return SystemClock.elapsedRealtimeNanos();
        }
    };
    private final AtomicInteger eventCount = new AtomicInteger(0);
    private final Queue<TraceProtos$TracePacket> eventPackets = new ConcurrentLinkedQueue();
    private long lastSyncTime;
    private long maxBufferSize = 3500;
    private final Map<Long, TraceProtos$TracePacket> threadPackets = new ConcurrentHashMap();

    /* loaded from: classes3.dex */
    public interface Clock {
        long getTimeNanos();
    }

    private Tracer() {
    }

    public static Tracer getInstance() {
        return INSTANCE;
    }

    public void setMaxBufferSize(long j) {
        this.maxBufferSize = j;
    }

    public void start(String str) {
        start(str, Thread.currentThread().getId(), (Map<String, String>) null);
    }

    public void start(String str, String str2, String str3) {
        start(str, Thread.currentThread().getId(), str2, str3);
    }

    public void start(String str, long j, String str2, String str3) {
        start(str, j, Collections.singletonMap(str2, str3));
    }

    public void start(String str, Map<String, String> map) {
        start(str, Thread.currentThread().getId(), map);
    }

    public void start(String str, long j, Map<String, String> map) {
        long timeNanos = this.clock.getTimeNanos();
        if (timeNanos - this.lastSyncTime > SYNCHRONIZATION_INTERVAL) {
            addPacket(forSynchronization(timeNanos));
            this.lastSyncTime = timeNanos;
        }
        if (!this.threadPackets.containsKey(Long.valueOf(j))) {
            this.threadPackets.put(Long.valueOf(j), forTrackId(j));
        }
        addPacket(forMethodStart(str, timeNanos, j, map));
    }

    public void end(String str) {
        addPacket(forMethodEnd(str, this.clock.getTimeNanos(), Thread.currentThread().getId()));
    }

    public void end(String str, long j) {
        addPacket(forMethodEnd(str, this.clock.getTimeNanos(), j));
    }

    public byte[] serialize() {
        TraceProtos$Trace.Builder newBuilder = TraceProtos$Trace.newBuilder();
        for (TraceProtos$TracePacket traceProtos$TracePacket : this.threadPackets.values()) {
            newBuilder.addPacket(traceProtos$TracePacket);
        }
        for (TraceProtos$TracePacket traceProtos$TracePacket2 : this.eventPackets) {
            newBuilder.addPacket(traceProtos$TracePacket2);
        }
        newBuilder.addPacket(forSynchronization(this.clock.getTimeNanos()));
        return newBuilder.build().toByteArray();
    }

    private void addPacket(TraceProtos$TracePacket traceProtos$TracePacket) {
        this.eventPackets.add(traceProtos$TracePacket);
        for (int incrementAndGet = this.eventCount.incrementAndGet(); ((long) incrementAndGet) > this.maxBufferSize; incrementAndGet--) {
            this.eventPackets.poll();
            this.eventCount.decrementAndGet();
        }
    }

    private TraceProtos$TracePacket forTrackId(long j) {
        if (j == AttachmentDatabase.PREUPLOAD_MESSAGE_ID) {
            return forTrack(j, "Database Lock");
        }
        Thread currentThread = Thread.currentThread();
        return forTrack(currentThread.getId(), currentThread.getName());
    }

    private static TraceProtos$TracePacket forTrack(long j, String str) {
        return TraceProtos$TracePacket.newBuilder().setTrustedPacketSequenceId(1).setTrackDescriptor(TraceProtos$TrackDescriptor.newBuilder().setUuid(j).setName(str)).build();
    }

    private static TraceProtos$TracePacket forMethodStart(String str, long j, long j2, Map<String, String> map) {
        TraceProtos$TrackEvent.Builder type = TraceProtos$TrackEvent.newBuilder().setTrackUuid(j2).setName(str).setType(TraceProtos$TrackEvent.Type.TYPE_SLICE_BEGIN);
        if (map != null) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                type.addDebugAnnotations(debugAnnotation(entry.getKey(), entry.getValue()));
            }
        }
        return TraceProtos$TracePacket.newBuilder().setTrustedPacketSequenceId(1).setTimestamp(j).setTrackEvent(type).build();
    }

    private static TraceProtos$DebugAnnotation debugAnnotation(String str, String str2) {
        TraceProtos$DebugAnnotation.Builder name = TraceProtos$DebugAnnotation.newBuilder().setName(str);
        if (str2 == null) {
            str2 = "";
        }
        return name.setStringValue(str2).build();
    }

    private static TraceProtos$TracePacket forMethodEnd(String str, long j, long j2) {
        return TraceProtos$TracePacket.newBuilder().setTrustedPacketSequenceId(1).setTimestamp(j).setTrackEvent(TraceProtos$TrackEvent.newBuilder().setTrackUuid(j2).setName(str).setType(TraceProtos$TrackEvent.Type.TYPE_SLICE_END)).build();
    }

    private static TraceProtos$TracePacket forSynchronization(long j) {
        return TraceProtos$TracePacket.newBuilder().setTrustedPacketSequenceId(1).setTimestamp(j).setSynchronizationMarker(ByteString.copyFrom(SYNCHRONIZATION_MARKER)).build();
    }

    public static byte[] toByteArray(UUID uuid) {
        ByteBuffer wrap = ByteBuffer.wrap(new byte[16]);
        wrap.putLong(uuid.getMostSignificantBits());
        wrap.putLong(uuid.getLeastSignificantBits());
        return wrap.array();
    }
}
