package org.signal.core.util;

import j$.util.Optional;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: OptionalExtensions.kt */
@Metadata(bv = {}, d1 = {"\u0000\u000e\n\u0000\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\u001a*\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0001\"\u0004\b\u0000\u0010\u0000*\b\u0012\u0004\u0012\u00028\u00000\u00012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00028\u00000\u0001\u001a\u0016\u0010\u0005\u001a\u00020\u0004\"\u0004\b\u0000\u0010\u0000*\b\u0012\u0004\u0012\u00028\u00000\u0001¨\u0006\u0006"}, d2 = {"E", "j$/util/Optional", "other", "or", "", "isAbsent", "core-util_release"}, k = 2, mv = {1, 6, 0})
/* loaded from: classes3.dex */
public final class OptionalExtensionsKt {
    public static final <E> Optional<E> or(Optional<E> optional, Optional<E> optional2) {
        Intrinsics.checkNotNullParameter(optional, "<this>");
        Intrinsics.checkNotNullParameter(optional2, "other");
        return optional.isPresent() ? optional : optional2;
    }

    public static final <E> boolean isAbsent(Optional<E> optional) {
        Intrinsics.checkNotNullParameter(optional, "<this>");
        return !optional.isPresent();
    }
}
