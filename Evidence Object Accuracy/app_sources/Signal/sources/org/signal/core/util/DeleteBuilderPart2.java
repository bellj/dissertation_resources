package org.signal.core.util;

import androidx.sqlite.db.SupportSQLiteDatabase;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: SQLiteDatabaseExtensions.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\b¢\u0006\u0002\u0010\tJ\u0006\u0010\u000b\u001a\u00020\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\bX\u0004¢\u0006\u0004\n\u0002\u0010\n¨\u0006\r"}, d2 = {"Lorg/signal/core/util/DeleteBuilderPart2;", "", "db", "Landroidx/sqlite/db/SupportSQLiteDatabase;", "tableName", "", "where", "whereArgs", "", "(Landroidx/sqlite/db/SupportSQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V", "[Ljava/lang/String;", "run", "", "core-util_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class DeleteBuilderPart2 {
    private final SupportSQLiteDatabase db;
    private final String tableName;
    private final String where;
    private final String[] whereArgs;

    public DeleteBuilderPart2(SupportSQLiteDatabase supportSQLiteDatabase, String str, String str2, String[] strArr) {
        Intrinsics.checkNotNullParameter(supportSQLiteDatabase, "db");
        Intrinsics.checkNotNullParameter(str, "tableName");
        Intrinsics.checkNotNullParameter(str2, "where");
        Intrinsics.checkNotNullParameter(strArr, "whereArgs");
        this.db = supportSQLiteDatabase;
        this.tableName = str;
        this.where = str2;
        this.whereArgs = strArr;
    }

    public final int run() {
        return this.db.delete(this.tableName, this.where, this.whereArgs);
    }
}
