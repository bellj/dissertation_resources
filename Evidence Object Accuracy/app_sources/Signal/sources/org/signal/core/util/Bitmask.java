package org.signal.core.util;

import java.util.Locale;

/* loaded from: classes3.dex */
public final class Bitmask {
    private static long twoToThe(long j) {
        return (long) (1 << ((int) j));
    }

    public static long read(long j, int i, int i2) {
        boolean z = i2 >= 0;
        checkArgument(z, "Must have a positive bit size! size: " + i2);
        int i3 = i * i2;
        checkArgument(i3 + i2 <= 64 && i >= 0, String.format(Locale.US, "Your position is out of bounds! position: %d, flagBitSize: %d", Integer.valueOf(i), Integer.valueOf(i2)));
        return (j >>> i3) & (twoToThe((long) i2) - 1);
    }

    public static long update(long j, int i, int i2, long j2) {
        boolean z = i2 >= 0;
        checkArgument(z, "Must have a positive bit size! size: " + i2);
        boolean z2 = j2 >= 0;
        checkArgument(z2, "Value must be positive! value: " + j2);
        long j3 = (long) i2;
        boolean z3 = j2 < twoToThe(j3);
        Locale locale = Locale.US;
        checkArgument(z3, String.format(locale, "Value is larger than you can hold for the given bitsize! value: %d, flagBitSize: %d", Long.valueOf(j2), Integer.valueOf(i2)));
        int i3 = i * i2;
        checkArgument(i3 + i2 <= 64 && i >= 0, String.format(locale, "Your position is out of bounds! position: %d, flagBitSize: %d", Integer.valueOf(i), Integer.valueOf(i2)));
        return (j & (((twoToThe(j3) - 1) << i3) ^ -1)) | (j2 << i3);
    }

    private static void checkArgument(boolean z, String str) {
        if (!z) {
            throw new IllegalArgumentException(str);
        }
    }
}
