package org.signal.protos.resumableuploads;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes3.dex */
public final class ResumableUploads$ResumableUpload extends GeneratedMessageLite<ResumableUploads$ResumableUpload, Builder> implements MessageLiteOrBuilder {
    public static final int CDNKEY_FIELD_NUMBER;
    public static final int CDNNUMBER_FIELD_NUMBER;
    private static final ResumableUploads$ResumableUpload DEFAULT_INSTANCE;
    public static final int IV_FIELD_NUMBER;
    public static final int LOCATION_FIELD_NUMBER;
    private static volatile Parser<ResumableUploads$ResumableUpload> PARSER;
    public static final int SECRETKEY_FIELD_NUMBER;
    public static final int TIMEOUT_FIELD_NUMBER;
    private String cdnKey_ = "";
    private int cdnNumber_;
    private ByteString iv_;
    private String location_ = "";
    private ByteString secretKey_;
    private long timeout_;

    private ResumableUploads$ResumableUpload() {
        ByteString byteString = ByteString.EMPTY;
        this.secretKey_ = byteString;
        this.iv_ = byteString;
    }

    public ByteString getSecretKey() {
        return this.secretKey_;
    }

    public void setSecretKey(ByteString byteString) {
        byteString.getClass();
        this.secretKey_ = byteString;
    }

    public void clearSecretKey() {
        this.secretKey_ = getDefaultInstance().getSecretKey();
    }

    public ByteString getIv() {
        return this.iv_;
    }

    public void setIv(ByteString byteString) {
        byteString.getClass();
        this.iv_ = byteString;
    }

    public void clearIv() {
        this.iv_ = getDefaultInstance().getIv();
    }

    public String getCdnKey() {
        return this.cdnKey_;
    }

    public ByteString getCdnKeyBytes() {
        return ByteString.copyFromUtf8(this.cdnKey_);
    }

    public void setCdnKey(String str) {
        str.getClass();
        this.cdnKey_ = str;
    }

    public void clearCdnKey() {
        this.cdnKey_ = getDefaultInstance().getCdnKey();
    }

    public void setCdnKeyBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.cdnKey_ = byteString.toStringUtf8();
    }

    public int getCdnNumber() {
        return this.cdnNumber_;
    }

    public void setCdnNumber(int i) {
        this.cdnNumber_ = i;
    }

    public void clearCdnNumber() {
        this.cdnNumber_ = 0;
    }

    public String getLocation() {
        return this.location_;
    }

    public ByteString getLocationBytes() {
        return ByteString.copyFromUtf8(this.location_);
    }

    public void setLocation(String str) {
        str.getClass();
        this.location_ = str;
    }

    public void clearLocation() {
        this.location_ = getDefaultInstance().getLocation();
    }

    public void setLocationBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.location_ = byteString.toStringUtf8();
    }

    public long getTimeout() {
        return this.timeout_;
    }

    public void setTimeout(long j) {
        this.timeout_ = j;
    }

    public void clearTimeout() {
        this.timeout_ = 0;
    }

    public static ResumableUploads$ResumableUpload parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (ResumableUploads$ResumableUpload) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static ResumableUploads$ResumableUpload parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ResumableUploads$ResumableUpload) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static ResumableUploads$ResumableUpload parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (ResumableUploads$ResumableUpload) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static ResumableUploads$ResumableUpload parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ResumableUploads$ResumableUpload) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static ResumableUploads$ResumableUpload parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (ResumableUploads$ResumableUpload) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static ResumableUploads$ResumableUpload parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (ResumableUploads$ResumableUpload) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static ResumableUploads$ResumableUpload parseFrom(InputStream inputStream) throws IOException {
        return (ResumableUploads$ResumableUpload) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ResumableUploads$ResumableUpload parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ResumableUploads$ResumableUpload) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ResumableUploads$ResumableUpload parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (ResumableUploads$ResumableUpload) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static ResumableUploads$ResumableUpload parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ResumableUploads$ResumableUpload) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static ResumableUploads$ResumableUpload parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (ResumableUploads$ResumableUpload) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static ResumableUploads$ResumableUpload parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (ResumableUploads$ResumableUpload) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(ResumableUploads$ResumableUpload resumableUploads$ResumableUpload) {
        return DEFAULT_INSTANCE.createBuilder(resumableUploads$ResumableUpload);
    }

    /* loaded from: classes3.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<ResumableUploads$ResumableUpload, Builder> implements MessageLiteOrBuilder {
        /* synthetic */ Builder(ResumableUploads$1 resumableUploads$1) {
            this();
        }

        private Builder() {
            super(ResumableUploads$ResumableUpload.DEFAULT_INSTANCE);
        }

        public Builder setSecretKey(ByteString byteString) {
            copyOnWrite();
            ((ResumableUploads$ResumableUpload) this.instance).setSecretKey(byteString);
            return this;
        }

        public Builder setIv(ByteString byteString) {
            copyOnWrite();
            ((ResumableUploads$ResumableUpload) this.instance).setIv(byteString);
            return this;
        }

        public Builder setCdnKey(String str) {
            copyOnWrite();
            ((ResumableUploads$ResumableUpload) this.instance).setCdnKey(str);
            return this;
        }

        public Builder setCdnNumber(int i) {
            copyOnWrite();
            ((ResumableUploads$ResumableUpload) this.instance).setCdnNumber(i);
            return this;
        }

        public Builder setLocation(String str) {
            copyOnWrite();
            ((ResumableUploads$ResumableUpload) this.instance).setLocation(str);
            return this;
        }

        public Builder setTimeout(long j) {
            copyOnWrite();
            ((ResumableUploads$ResumableUpload) this.instance).setTimeout(j);
            return this;
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (ResumableUploads$1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new ResumableUploads$ResumableUpload();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0006\u0000\u0000\u0001\u0006\u0006\u0000\u0000\u0000\u0001\n\u0002\n\u0003Ȉ\u0004\u000b\u0005Ȉ\u0006\u0003", new Object[]{"secretKey_", "iv_", "cdnKey_", "cdnNumber_", "location_", "timeout_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<ResumableUploads$ResumableUpload> parser = PARSER;
                if (parser == null) {
                    synchronized (ResumableUploads$ResumableUpload.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        ResumableUploads$ResumableUpload resumableUploads$ResumableUpload = new ResumableUploads$ResumableUpload();
        DEFAULT_INSTANCE = resumableUploads$ResumableUpload;
        GeneratedMessageLite.registerDefaultInstance(ResumableUploads$ResumableUpload.class, resumableUploads$ResumableUpload);
    }

    public static ResumableUploads$ResumableUpload getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<ResumableUploads$ResumableUpload> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
