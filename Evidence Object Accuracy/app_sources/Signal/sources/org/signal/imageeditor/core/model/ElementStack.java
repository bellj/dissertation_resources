package org.signal.imageeditor.core.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Stack;

/* loaded from: classes3.dex */
public final class ElementStack implements Parcelable {
    public static final Parcelable.Creator<ElementStack> CREATOR = new Parcelable.Creator<ElementStack>() { // from class: org.signal.imageeditor.core.model.ElementStack.1
        @Override // android.os.Parcelable.Creator
        public ElementStack createFromParcel(Parcel parcel) {
            return new ElementStack(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public ElementStack[] newArray(int i) {
            return new ElementStack[i];
        }
    };
    private final int limit;
    private final Stack<byte[]> stack;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public ElementStack(int i) {
        this.stack = new Stack<>();
        this.limit = i;
    }

    private ElementStack(Parcel parcel) {
        this(parcel.readInt());
        int readInt = parcel.readInt();
        for (int i = 0; i < readInt; i++) {
            this.stack.add(i, parcel.createByteArray());
        }
    }

    public boolean tryPush(EditorElement editorElement) {
        byte[] bytes = getBytes(editorElement);
        boolean z = this.stack.isEmpty() || !Arrays.equals(bytes, this.stack.peek());
        if (z) {
            this.stack.push(bytes);
            if (this.stack.size() > this.limit) {
                this.stack.remove(1);
            }
        }
        return z;
    }

    public static byte[] getBytes(Parcelable parcelable) {
        Parcel obtain = Parcel.obtain();
        try {
            obtain.writeParcelable(parcelable, 0);
            return obtain.marshall();
        } finally {
            obtain.recycle();
        }
    }

    public EditorElement pop(EditorElement editorElement) {
        if (this.stack.empty()) {
            return null;
        }
        byte[] bytes = getBytes(editorElement);
        byte[] bArr = null;
        while (!this.stack.empty() && bArr == null) {
            byte[] pop = this.stack.pop();
            if (!Arrays.equals(pop, bytes)) {
                bArr = pop;
            }
        }
        if (bArr == null) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        try {
            obtain.unmarshall(bArr, 0, bArr.length);
            obtain.setDataPosition(0);
            return (EditorElement) obtain.readParcelable(EditorElement.class.getClassLoader());
        } finally {
            obtain.recycle();
        }
    }

    public void clear() {
        this.stack.clear();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.limit);
        int size = this.stack.size();
        parcel.writeInt(size);
        for (int i2 = 0; i2 < size; i2++) {
            parcel.writeByteArray(this.stack.get(i2));
        }
    }

    public boolean stackContainsStateDifferentFrom(EditorElement editorElement) {
        if (this.stack.isEmpty()) {
            return false;
        }
        byte[] bytes = getBytes(editorElement);
        Iterator<byte[]> it = this.stack.iterator();
        while (it.hasNext()) {
            if (!Arrays.equals(it.next(), bytes)) {
                return true;
            }
        }
        return false;
    }
}
