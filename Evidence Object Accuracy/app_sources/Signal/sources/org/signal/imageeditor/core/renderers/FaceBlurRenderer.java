package org.signal.imageeditor.core.renderers;

import android.os.Parcel;
import android.os.Parcelable;
import org.signal.imageeditor.core.Bounds;
import org.signal.imageeditor.core.Renderer;
import org.signal.imageeditor.core.RendererContext;

/* loaded from: classes3.dex */
public final class FaceBlurRenderer implements Renderer {
    public static final Parcelable.Creator<FaceBlurRenderer> CREATOR = new Parcelable.Creator<FaceBlurRenderer>() { // from class: org.signal.imageeditor.core.renderers.FaceBlurRenderer.1
        @Override // android.os.Parcelable.Creator
        public FaceBlurRenderer createFromParcel(Parcel parcel) {
            return new FaceBlurRenderer();
        }

        @Override // android.os.Parcelable.Creator
        public FaceBlurRenderer[] newArray(int i) {
            return new FaceBlurRenderer[i];
        }
    };

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
    }

    @Override // org.signal.imageeditor.core.Renderer
    public void render(RendererContext rendererContext) {
        rendererContext.canvas.drawRect(Bounds.FULL_BOUNDS, rendererContext.getMaskPaint());
    }

    @Override // org.signal.imageeditor.core.Renderer
    public boolean hitTest(float f, float f2) {
        return Bounds.FULL_BOUNDS.contains(f, f2);
    }
}
