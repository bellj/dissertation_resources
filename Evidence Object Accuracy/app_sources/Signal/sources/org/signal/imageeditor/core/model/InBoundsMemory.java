package org.signal.imageeditor.core.model;

import android.graphics.Matrix;

/* loaded from: classes3.dex */
public final class InBoundsMemory {
    private final Matrix lastGoodMainImage = new Matrix();
    private final Matrix lastGoodUserCrop = new Matrix();

    public void push(EditorElement editorElement, EditorElement editorElement2) {
        if (editorElement == null) {
            this.lastGoodMainImage.reset();
        } else {
            this.lastGoodMainImage.set(editorElement.getLocalMatrix());
            this.lastGoodMainImage.preConcat(editorElement.getEditorMatrix());
        }
        this.lastGoodUserCrop.set(editorElement2.getLocalMatrix());
        this.lastGoodUserCrop.preConcat(editorElement2.getEditorMatrix());
    }

    public void restore(EditorElement editorElement, EditorElement editorElement2, Runnable runnable) {
        if (editorElement != null) {
            editorElement.animateLocalTo(this.lastGoodMainImage, runnable);
        }
        editorElement2.animateLocalTo(this.lastGoodUserCrop, runnable);
    }

    public Matrix getLastKnownGoodMainImageMatrix() {
        return new Matrix(this.lastGoodMainImage);
    }
}
