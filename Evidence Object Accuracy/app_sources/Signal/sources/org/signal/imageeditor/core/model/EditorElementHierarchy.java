package org.signal.imageeditor.core.model;

import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;
import androidx.core.graphics.ColorUtils;
import org.signal.imageeditor.R$color;
import org.signal.imageeditor.core.Bounds;
import org.signal.imageeditor.core.Renderer;
import org.signal.imageeditor.core.SelectableRenderer;
import org.signal.imageeditor.core.model.EditorElement;
import org.signal.imageeditor.core.model.ThumbRenderer;
import org.signal.imageeditor.core.renderers.CropAreaRenderer;
import org.signal.imageeditor.core.renderers.FillRenderer;
import org.signal.imageeditor.core.renderers.InverseFillRenderer;
import org.signal.imageeditor.core.renderers.OvalGuideRenderer;
import org.signal.imageeditor.core.renderers.SelectedElementGuideRenderer;
import org.signal.imageeditor.core.renderers.TrashRenderer;

/* loaded from: classes3.dex */
public final class EditorElementHierarchy {
    private final EditorElement blackout;
    private final EditorElement cropEditorElement;
    private final EditorElement fade;
    private final EditorElement flipRotate;
    private final EditorElement imageCrop;
    private final EditorElement imageRoot;
    private final EditorElement overlay;
    private final EditorElement root;
    private EditorElement selectedElement;
    private final EditorElement selection;
    private final EditorElement thumbs;
    private final EditorElement trash;
    private final EditorElement view;

    /* loaded from: classes3.dex */
    public enum CropStyle {
        RECTANGLE,
        CIRCLE,
        PINCH_AND_PAN
    }

    public static EditorElementHierarchy create(int i) {
        return new EditorElementHierarchy(createRoot(CropStyle.RECTANGLE, i));
    }

    public static EditorElementHierarchy createForCircleEditing(int i) {
        return new EditorElementHierarchy(createRoot(CropStyle.CIRCLE, i));
    }

    public static EditorElementHierarchy createForPinchAndPanCropping(int i) {
        return new EditorElementHierarchy(createRoot(CropStyle.PINCH_AND_PAN, i));
    }

    public static EditorElementHierarchy create(EditorElement editorElement) {
        return new EditorElementHierarchy(editorElement);
    }

    private EditorElementHierarchy(EditorElement editorElement) {
        this.root = editorElement;
        EditorElement child = editorElement.getChild(0);
        this.view = child;
        EditorElement child2 = child.getChild(0);
        this.flipRotate = child2;
        this.imageRoot = child2.getChild(0);
        EditorElement child3 = child2.getChild(1);
        this.overlay = child3;
        EditorElement child4 = child3.getChild(0);
        this.imageCrop = child4;
        this.selection = child3.getChild(1);
        EditorElement child5 = child4.getChild(0);
        this.cropEditorElement = child5;
        this.blackout = child5.getChild(0);
        this.thumbs = child5.getChild(1);
        this.fade = child5.getChild(2);
        this.trash = child5.getChild(3);
    }

    private static EditorElement createRoot(CropStyle cropStyle, int i) {
        EditorElement editorElement = new EditorElement((Renderer) null);
        EditorElement editorElement2 = new EditorElement((Renderer) null);
        editorElement.addElement(editorElement2);
        EditorElement editorElement3 = new EditorElement((Renderer) null);
        editorElement2.addElement(editorElement3);
        editorElement3.addElement(new EditorElement((Renderer) null));
        EditorElement editorElement4 = new EditorElement((Renderer) null);
        editorElement3.addElement(editorElement4);
        EditorElement editorElement5 = new EditorElement((Renderer) null);
        editorElement4.addElement(editorElement5);
        editorElement4.addElement(new EditorElement((Renderer) null));
        boolean z = cropStyle == CropStyle.RECTANGLE;
        EditorElement editorElement6 = new EditorElement(new CropAreaRenderer(ColorUtils.setAlphaComponent(i, 127), z));
        editorElement6.getFlags().setRotateLocked(true).setAspectLocked(true).setSelectable(false).setVisible(false).persist();
        editorElement5.addElement(editorElement6);
        EditorElement editorElement7 = new EditorElement(new FillRenderer(ColorUtils.setAlphaComponent(i, 102)), 1);
        editorElement7.getFlags().setSelectable(false).setEditable(false).setVisible(false).persist();
        editorElement6.addElement(editorElement7);
        EditorElement editorElement8 = new EditorElement(new TrashRenderer(), 3);
        editorElement8.getFlags().setSelectable(false).setEditable(false).setVisible(false).persist();
        editorElement6.addElement(editorElement8);
        EditorElement editorElement9 = new EditorElement(new InverseFillRenderer(ColorUtils.setAlphaComponent(i, 255)));
        editorElement9.getFlags().setSelectable(false).setEditable(false).persist();
        editorElement6.addElement(editorElement9);
        if (cropStyle == CropStyle.PINCH_AND_PAN) {
            editorElement6.addElement(new EditorElement((Renderer) null));
        } else {
            editorElement6.addElement(createThumbs(editorElement6, z));
            if (cropStyle == CropStyle.CIRCLE) {
                EditorElement editorElement10 = new EditorElement(new OvalGuideRenderer(R$color.crop_circle_guide_color));
                editorElement10.getFlags().setSelectable(false).persist();
                editorElement6.addElement(editorElement10);
            }
        }
        return editorElement;
    }

    private static EditorElement createThumbs(EditorElement editorElement, boolean z) {
        EditorElement editorElement2 = new EditorElement((Renderer) null);
        editorElement2.getFlags().setChildrenVisible(false).setSelectable(false).setVisible(false).persist();
        if (z) {
            editorElement2.addElement(newThumb(editorElement, ThumbRenderer.ControlPoint.CENTER_LEFT));
            editorElement2.addElement(newThumb(editorElement, ThumbRenderer.ControlPoint.CENTER_RIGHT));
            editorElement2.addElement(newThumb(editorElement, ThumbRenderer.ControlPoint.TOP_CENTER));
            editorElement2.addElement(newThumb(editorElement, ThumbRenderer.ControlPoint.BOTTOM_CENTER));
        }
        editorElement2.addElement(newThumb(editorElement, ThumbRenderer.ControlPoint.TOP_LEFT));
        editorElement2.addElement(newThumb(editorElement, ThumbRenderer.ControlPoint.TOP_RIGHT));
        editorElement2.addElement(newThumb(editorElement, ThumbRenderer.ControlPoint.BOTTOM_LEFT));
        editorElement2.addElement(newThumb(editorElement, ThumbRenderer.ControlPoint.BOTTOM_RIGHT));
        return editorElement2;
    }

    public void removeAllSelectionArtifacts() {
        this.selection.deleteAllChildren();
        this.selectedElement = null;
    }

    public void updateSelectionThumbsForElement(EditorElement editorElement, Matrix matrix) {
        if (editorElement == this.selectedElement) {
            setOrUpdateSelectionThumbsForElement(editorElement, matrix);
        }
    }

    public void setOrUpdateSelectionThumbsForElement(EditorElement editorElement, Matrix matrix) {
        if (this.selectedElement != editorElement) {
            removeAllSelectionArtifacts();
            if (editorElement.getRenderer() instanceof SelectableRenderer) {
                this.selectedElement = editorElement;
            } else {
                this.selectedElement = null;
            }
            if (this.selectedElement != null) {
                this.selection.addElement(createSelectionBox());
                this.selection.addElement(createScaleControlThumb(editorElement));
                this.selection.addElement(createRotateControlThumb(editorElement));
            } else {
                return;
            }
        }
        if (matrix != null) {
            Matrix localMatrix = this.selection.getLocalMatrix();
            if (this.selectedElement.getRenderer() instanceof SelectableRenderer) {
                RectF rectF = new RectF();
                ((SelectableRenderer) this.selectedElement.getRenderer()).getSelectionBounds(rectF);
                localMatrix.setRectToRect(Bounds.FULL_BOUNDS, rectF, Matrix.ScaleToFit.FILL);
            }
            localMatrix.postConcat(matrix);
        }
    }

    private static EditorElement createSelectionBox() {
        return new EditorElement(new SelectedElementGuideRenderer());
    }

    private static EditorElement createScaleControlThumb(EditorElement editorElement) {
        ThumbRenderer.ControlPoint controlPoint = ThumbRenderer.ControlPoint.SCALE_ROT_RIGHT;
        EditorElement editorElement2 = new EditorElement(new CropThumbRenderer(controlPoint, editorElement.getId()));
        editorElement2.getLocalMatrix().preTranslate(controlPoint.getX(), controlPoint.getY());
        return editorElement2;
    }

    private static EditorElement createRotateControlThumb(EditorElement editorElement) {
        ThumbRenderer.ControlPoint controlPoint = ThumbRenderer.ControlPoint.SCALE_ROT_LEFT;
        EditorElement editorElement2 = new EditorElement(new CropThumbRenderer(controlPoint, editorElement.getId()));
        editorElement2.getLocalMatrix().preTranslate(controlPoint.getX(), controlPoint.getY());
        return editorElement2;
    }

    private static EditorElement newThumb(EditorElement editorElement, ThumbRenderer.ControlPoint controlPoint) {
        EditorElement editorElement2 = new EditorElement(new CropThumbRenderer(controlPoint, editorElement.getId()));
        editorElement2.getFlags().setSelectable(false).persist();
        editorElement2.getLocalMatrix().preTranslate(controlPoint.getX(), controlPoint.getY());
        return editorElement2;
    }

    public EditorElement getRoot() {
        return this.root;
    }

    public EditorElement getImageRoot() {
        return this.imageRoot;
    }

    public EditorElement getSelection() {
        return this.selection;
    }

    public EditorElement getSelectedElement() {
        return this.selectedElement;
    }

    public EditorElement getTrash() {
        return this.trash;
    }

    public EditorElement getMainImage() {
        if (this.imageRoot.getChildCount() > 0) {
            return this.imageRoot.getChild(0);
        }
        return null;
    }

    public EditorElement getCropEditorElement() {
        return this.cropEditorElement;
    }

    public EditorElement getImageCrop() {
        return this.imageCrop;
    }

    public EditorElement getOverlay() {
        return this.overlay;
    }

    public EditorElement getFlipRotate() {
        return this.flipRotate;
    }

    public void addFade(Runnable runnable) {
        this.fade.getFlags().setVisible(true).persist();
        runnable.run();
    }

    public void removeFade(Runnable runnable) {
        this.fade.getFlags().setVisible(false).persist();
        runnable.run();
    }

    public void startCrop(Runnable runnable, float f) {
        Matrix matrix = new Matrix();
        matrix.postScale(f, f);
        this.root.animateEditorTo(matrix, runnable);
        this.cropEditorElement.getFlags().setVisible(true);
        this.blackout.getFlags().setVisible(false);
        this.thumbs.getFlags().setChildrenVisible(true);
        this.thumbs.forAllInTree(new EditorElement.PerElementFunction() { // from class: org.signal.imageeditor.core.model.EditorElementHierarchy$$ExternalSyntheticLambda0
            @Override // org.signal.imageeditor.core.model.EditorElement.PerElementFunction
            public final void apply(EditorElement editorElement) {
                EditorElementHierarchy.$r8$lambda$lnWTKRHVWhP6_J3VSsOWR3nOPfo(editorElement);
            }
        });
        this.imageRoot.forAllInTree(new EditorElement.PerElementFunction() { // from class: org.signal.imageeditor.core.model.EditorElementHierarchy$$ExternalSyntheticLambda1
            @Override // org.signal.imageeditor.core.model.EditorElement.PerElementFunction
            public final void apply(EditorElement editorElement) {
                EditorElementHierarchy.$r8$lambda$YZrBhAPn23ofygOJbzpeAVxuOQo(editorElement);
            }
        });
        EditorElement mainImage = getMainImage();
        if (mainImage != null) {
            mainImage.getFlags().setSelectable(true);
        }
        runnable.run();
    }

    public static /* synthetic */ void lambda$startCrop$0(EditorElement editorElement) {
        editorElement.getFlags().setSelectable(true);
    }

    public static /* synthetic */ void lambda$startCrop$1(EditorElement editorElement) {
        editorElement.getFlags().setSelectable(false);
    }

    public void doneCrop(RectF rectF, Runnable runnable) {
        updateViewToCrop(rectF, runnable);
        this.root.rollbackEditorMatrix(runnable);
        this.root.forAllInTree(new EditorElement.PerElementFunction() { // from class: org.signal.imageeditor.core.model.EditorElementHierarchy$$ExternalSyntheticLambda2
            @Override // org.signal.imageeditor.core.model.EditorElement.PerElementFunction
            public final void apply(EditorElement editorElement) {
                EditorElementHierarchy.m197$r8$lambda$FtOQb9G4W0WKb8__HMQEb2wVkU(editorElement);
            }
        });
    }

    public static /* synthetic */ void lambda$doneCrop$2(EditorElement editorElement) {
        editorElement.getFlags().reset();
    }

    public void updateViewToCrop(RectF rectF, Runnable runnable) {
        RectF rectF2 = new RectF();
        getCropFinalMatrix().mapRect(rectF2, Bounds.FULL_BOUNDS);
        Matrix matrix = new Matrix();
        matrix.setRectToRect(rectF2, rectF, Matrix.ScaleToFit.CENTER);
        this.view.animateLocalTo(matrix, runnable);
    }

    private Matrix getCropFinalMatrix() {
        Matrix matrix = new Matrix(this.flipRotate.getLocalMatrix());
        matrix.preConcat(this.imageCrop.getLocalMatrix());
        matrix.preConcat(this.cropEditorElement.getLocalMatrix());
        return matrix;
    }

    public Matrix imageMatrixRelativeToCrop() {
        EditorElement mainImage = getMainImage();
        if (mainImage == null) {
            return null;
        }
        Matrix matrix = new Matrix(this.imageCrop.getLocalMatrix());
        matrix.preConcat(this.cropEditorElement.getLocalMatrix());
        matrix.preConcat(this.cropEditorElement.getEditorMatrix());
        Matrix matrix2 = new Matrix(mainImage.getLocalMatrix());
        matrix2.preConcat(mainImage.getEditorMatrix());
        matrix2.preConcat(this.imageCrop.getLocalMatrix());
        Matrix matrix3 = new Matrix();
        matrix2.invert(matrix3);
        matrix3.preConcat(matrix);
        return matrix3;
    }

    public void dragDropRelease(RectF rectF, Runnable runnable) {
        if (this.cropEditorElement.getFlags().isVisible()) {
            updateViewToCrop(rectF, runnable);
        }
    }

    public RectF getCropRect() {
        RectF rectF = new RectF();
        getCropFinalMatrix().mapRect(rectF, Bounds.FULL_BOUNDS);
        return rectF;
    }

    public void flipRotate(float f, int i, int i2, RectF rectF, Runnable runnable) {
        Matrix matrix = new Matrix(this.flipRotate.getLocalMatrix());
        if (f != 0.0f) {
            matrix.postRotate(f);
        }
        matrix.postScale((float) i, (float) i2);
        this.flipRotate.animateLocalTo(matrix, runnable);
        updateViewToCrop(rectF, runnable);
    }

    public Matrix getMainImageFullMatrix() {
        Matrix matrix = new Matrix();
        matrix.preConcat(this.view.getLocalMatrix());
        matrix.preConcat(getMainImageFullMatrixFromFlipRotate());
        return matrix;
    }

    Matrix getMainImageFullMatrixFromFlipRotate() {
        Matrix matrix = new Matrix();
        matrix.preConcat(this.flipRotate.getLocalMatrix());
        matrix.preConcat(this.imageRoot.getLocalMatrix());
        EditorElement mainImage = getMainImage();
        if (mainImage != null) {
            matrix.preConcat(mainImage.getLocalMatrix());
        }
        return matrix;
    }

    public PointF getOutputSize(Point point) {
        Matrix matrix = new Matrix();
        matrix.preConcat(this.flipRotate.getLocalMatrix());
        matrix.preConcat(this.cropEditorElement.getLocalMatrix());
        matrix.preConcat(this.cropEditorElement.getEditorMatrix());
        EditorElement mainImage = getMainImage();
        if (mainImage != null) {
            float xScale = 1.0f / (xScale(mainImage.getLocalMatrix()) * xScale(mainImage.getEditorMatrix()));
            matrix.preScale(xScale, xScale);
        }
        float[] fArr = new float[4];
        matrix.mapPoints(fArr, new float[]{0.0f, 0.0f, (float) point.x, (float) point.y});
        return new PointF(Math.abs(fArr[0] - fArr[2]), Math.abs(fArr[1] - fArr[3]));
    }

    static float xScale(Matrix matrix) {
        float[] fArr = new float[9];
        matrix.getValues(fArr);
        float f = fArr[0];
        float f2 = fArr[3];
        return (float) Math.sqrt((double) ((f * f) + (f2 * f2)));
    }
}
