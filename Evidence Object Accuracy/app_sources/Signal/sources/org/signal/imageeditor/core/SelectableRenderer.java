package org.signal.imageeditor.core;

import android.graphics.RectF;
import kotlin.Metadata;

/* compiled from: SelectableRenderer.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\bH&¨\u0006\t"}, d2 = {"Lorg/signal/imageeditor/core/SelectableRenderer;", "Lorg/signal/imageeditor/core/Renderer;", "getSelectionBounds", "", "bounds", "Landroid/graphics/RectF;", "onSelected", "selected", "", "image-editor_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public interface SelectableRenderer extends Renderer {
    void getSelectionBounds(RectF rectF);

    void onSelected(boolean z);
}
