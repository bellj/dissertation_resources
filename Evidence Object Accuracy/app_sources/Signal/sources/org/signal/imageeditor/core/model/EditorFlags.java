package org.signal.imageeditor.core.model;

/* loaded from: classes3.dex */
public final class EditorFlags {
    private int flags;
    private int markedFlags;
    private int persistedFlags;

    public EditorFlags() {
        this(61);
    }

    public EditorFlags(int i) {
        this.flags = i;
        this.persistedFlags = i;
    }

    public EditorFlags setRotateLocked(boolean z) {
        setFlag(2, z);
        return this;
    }

    public boolean isRotateLocked() {
        return isFlagSet(2);
    }

    public EditorFlags setAspectLocked(boolean z) {
        setFlag(1, z);
        return this;
    }

    public boolean isAspectLocked() {
        return isFlagSet(1);
    }

    public EditorFlags setSelectable(boolean z) {
        setFlag(4, z);
        return this;
    }

    public boolean isSelectable() {
        return isFlagSet(4);
    }

    public EditorFlags setEditable(boolean z) {
        setFlag(32, z);
        return this;
    }

    public boolean isEditable() {
        return isFlagSet(32);
    }

    public EditorFlags setVisible(boolean z) {
        setFlag(8, z);
        return this;
    }

    public boolean isVisible() {
        return isFlagSet(8);
    }

    public EditorFlags setChildrenVisible(boolean z) {
        setFlag(16, z);
        return this;
    }

    public boolean isChildrenVisible() {
        return isFlagSet(16);
    }

    private void setFlag(int i, boolean z) {
        if (z) {
            this.flags = i | this.flags;
            return;
        }
        this.flags = (i ^ -1) & this.flags;
    }

    private boolean isFlagSet(int i) {
        return (i & this.flags) != 0;
    }

    public int asInt() {
        return this.persistedFlags;
    }

    public int getCurrentState() {
        return this.flags;
    }

    public void persist() {
        this.persistedFlags = this.flags;
    }

    public void reset() {
        restoreState(this.persistedFlags);
    }

    public void restoreState(int i) {
        this.flags = i;
    }

    public void mark() {
        this.markedFlags = this.flags;
    }

    public void restore() {
        this.flags = this.markedFlags;
    }

    public void set(EditorFlags editorFlags) {
        this.persistedFlags = editorFlags.persistedFlags;
        this.flags = editorFlags.flags;
    }
}
