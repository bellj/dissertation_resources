package org.signal.imageeditor.core;

import android.graphics.Matrix;
import android.graphics.PointF;
import org.signal.imageeditor.core.model.EditorElement;
import org.signal.imageeditor.core.renderers.BezierDrawingRenderer;

/* access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class DrawingSession extends ElementEditSession {
    private final BezierDrawingRenderer renderer;

    @Override // org.signal.imageeditor.core.EditSession
    public EditSession newPoint(Matrix matrix, PointF pointF, int i) {
        return this;
    }

    @Override // org.signal.imageeditor.core.EditSession
    public EditSession removePoint(Matrix matrix, int i) {
        return this;
    }

    private DrawingSession(EditorElement editorElement, Matrix matrix, BezierDrawingRenderer bezierDrawingRenderer) {
        super(editorElement, matrix);
        this.renderer = bezierDrawingRenderer;
    }

    public static EditSession start(EditorElement editorElement, BezierDrawingRenderer bezierDrawingRenderer, Matrix matrix, PointF pointF) {
        DrawingSession drawingSession = new DrawingSession(editorElement, matrix, bezierDrawingRenderer);
        drawingSession.setScreenStartPoint(0, pointF);
        bezierDrawingRenderer.setFirstPoint(drawingSession.startPointElement[0]);
        return drawingSession;
    }

    @Override // org.signal.imageeditor.core.EditSession
    public void movePoint(int i, PointF pointF) {
        if (i == 0) {
            setScreenEndPoint(i, pointF);
            this.renderer.addNewPoint(this.endPointElement[0]);
        }
    }
}
