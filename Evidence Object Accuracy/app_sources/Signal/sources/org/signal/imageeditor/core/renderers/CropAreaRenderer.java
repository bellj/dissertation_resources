package org.signal.imageeditor.core.renderers;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.core.content.res.ResourcesCompat;
import org.signal.imageeditor.R$color;
import org.signal.imageeditor.R$dimen;
import org.signal.imageeditor.core.Bounds;
import org.signal.imageeditor.core.Renderer;
import org.signal.imageeditor.core.RendererContext;

/* loaded from: classes3.dex */
public final class CropAreaRenderer implements Renderer {
    public static final Parcelable.Creator<CropAreaRenderer> CREATOR = new Parcelable.Creator<CropAreaRenderer>() { // from class: org.signal.imageeditor.core.renderers.CropAreaRenderer.1
        @Override // android.os.Parcelable.Creator
        public CropAreaRenderer createFromParcel(Parcel parcel) {
            int readInt = parcel.readInt();
            boolean z = true;
            if (parcel.readByte() != 1) {
                z = false;
            }
            return new CropAreaRenderer(readInt, z);
        }

        @Override // android.os.Parcelable.Creator
        public CropAreaRenderer[] newArray(int i) {
            return new CropAreaRenderer[i];
        }
    };
    private final int color;
    private final Path cropClipPath;
    private final RectF dst = new RectF();
    private final Paint paint = new Paint();
    private final boolean renderCenterThumbs;
    private final Path screenClipPath;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // org.signal.imageeditor.core.Renderer
    public void render(RendererContext rendererContext) {
        rendererContext.save();
        Canvas canvas = rendererContext.canvas;
        Resources resources = rendererContext.context.getResources();
        canvas.clipPath(this.cropClipPath);
        canvas.drawColor(this.color);
        rendererContext.mapRect(this.dst, Bounds.FULL_BOUNDS);
        int dimensionPixelSize = resources.getDimensionPixelSize(R$dimen.crop_area_renderer_edge_thickness);
        this.paint.setColor(ResourcesCompat.getColor(resources, R$color.crop_area_renderer_edge_color, null));
        rendererContext.canvasMatrix.setToIdentity();
        this.screenClipPath.reset();
        Path path = this.screenClipPath;
        RectF rectF = this.dst;
        path.moveTo(rectF.left, rectF.top);
        Path path2 = this.screenClipPath;
        RectF rectF2 = this.dst;
        path2.lineTo(rectF2.right, rectF2.top);
        Path path3 = this.screenClipPath;
        RectF rectF3 = this.dst;
        path3.lineTo(rectF3.right, rectF3.bottom);
        Path path4 = this.screenClipPath;
        RectF rectF4 = this.dst;
        path4.lineTo(rectF4.left, rectF4.bottom);
        this.screenClipPath.close();
        canvas.clipPath(this.screenClipPath);
        RectF rectF5 = this.dst;
        canvas.translate(rectF5.left, rectF5.top);
        RectF rectF6 = this.dst;
        float min = (float) ((int) Math.min((float) resources.getDimensionPixelSize(R$dimen.crop_area_renderer_edge_size), (Math.min(this.dst.width(), this.dst.height()) / 3.0f) - 10.0f));
        float f = (float) dimensionPixelSize;
        float f2 = (((rectF6.right - rectF6.left) - min) + f) / 2.0f;
        float f3 = (((rectF6.bottom - rectF6.top) - min) + f) / 2.0f;
        float f4 = (float) (-dimensionPixelSize);
        canvas.drawRect(f4, f4, min, min, this.paint);
        canvas.translate(0.0f, f3);
        if (this.renderCenterThumbs) {
            canvas.drawRect(f4, f4, min, min, this.paint);
        }
        canvas.translate(0.0f, f3);
        canvas.drawRect(f4, f4, min, min, this.paint);
        canvas.translate(f2, 0.0f);
        if (this.renderCenterThumbs) {
            canvas.drawRect(f4, f4, min, min, this.paint);
        }
        canvas.translate(f2, 0.0f);
        canvas.drawRect(f4, f4, min, min, this.paint);
        float f5 = -f3;
        canvas.translate(0.0f, f5);
        if (this.renderCenterThumbs) {
            canvas.drawRect(f4, f4, min, min, this.paint);
        }
        canvas.translate(0.0f, f5);
        canvas.drawRect(f4, f4, min, min, this.paint);
        canvas.translate(-f2, 0.0f);
        if (this.renderCenterThumbs) {
            canvas.drawRect(f4, f4, min, min, this.paint);
        }
        rendererContext.restore();
    }

    public CropAreaRenderer(int i, boolean z) {
        Path path = new Path();
        this.cropClipPath = path;
        Path path2 = new Path();
        this.screenClipPath = path2;
        this.color = i;
        this.renderCenterThumbs = z;
        path.toggleInverseFillType();
        path.moveTo(-1000.0f, -1000.0f);
        path.lineTo(1000.0f, -1000.0f);
        path.lineTo(1000.0f, 1000.0f);
        path.lineTo(-1000.0f, 1000.0f);
        path.close();
        path2.toggleInverseFillType();
    }

    @Override // org.signal.imageeditor.core.Renderer
    public boolean hitTest(float f, float f2) {
        return !Bounds.contains(f, f2);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.color);
        parcel.writeByte(this.renderCenterThumbs ? (byte) 1 : 0);
    }
}
