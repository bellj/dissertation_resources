package org.signal.imageeditor.core.model;

import android.graphics.Matrix;
import android.os.Parcel;
import java.util.UUID;

/* loaded from: classes3.dex */
public final class ParcelUtils {
    public static void writeMatrix(Parcel parcel, Matrix matrix) {
        float[] fArr = new float[9];
        matrix.getValues(fArr);
        parcel.writeFloatArray(fArr);
    }

    public static void readMatrix(Matrix matrix, Parcel parcel) {
        float[] fArr = new float[9];
        parcel.readFloatArray(fArr);
        matrix.setValues(fArr);
    }

    public static UUID readUUID(Parcel parcel) {
        return new UUID(parcel.readLong(), parcel.readLong());
    }

    public static void writeUUID(Parcel parcel, UUID uuid) {
        parcel.writeLong(uuid.getMostSignificantBits());
        parcel.writeLong(uuid.getLeastSignificantBits());
    }
}
