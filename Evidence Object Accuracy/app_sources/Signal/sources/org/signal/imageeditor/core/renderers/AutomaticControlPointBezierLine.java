package org.signal.imageeditor.core.renderers;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

/* loaded from: classes3.dex */
public final class AutomaticControlPointBezierLine implements Parcelable {
    public static final Parcelable.Creator<AutomaticControlPointBezierLine> CREATOR = new Parcelable.Creator<AutomaticControlPointBezierLine>() { // from class: org.signal.imageeditor.core.renderers.AutomaticControlPointBezierLine.1
        @Override // android.os.Parcelable.Creator
        public AutomaticControlPointBezierLine createFromParcel(Parcel parcel) {
            float[] createFloatArray = parcel.createFloatArray();
            return new AutomaticControlPointBezierLine(createFloatArray, parcel.createFloatArray(), createFloatArray != null ? createFloatArray.length : 0);
        }

        @Override // android.os.Parcelable.Creator
        public AutomaticControlPointBezierLine[] newArray(int i) {
            return new AutomaticControlPointBezierLine[i];
        }
    };
    private float[] a;
    private float[] b;
    private float[] c;
    private int count;
    private float[] p1x;
    private float[] p1y;
    private float[] p2x;
    private float[] p2y;
    private final Path path;
    private float[] r;
    private float[] x;
    private float[] y;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    private AutomaticControlPointBezierLine(float[] fArr, float[] fArr2, int i) {
        this.path = new Path();
        this.count = i;
        fArr = fArr == null ? new float[256] : fArr;
        this.x = fArr;
        this.y = fArr2 == null ? new float[256] : fArr2;
        allocControlPointsAndWorkingMemory(fArr.length);
        recalculateControlPoints();
    }

    public AutomaticControlPointBezierLine() {
        this(null, null, 0);
    }

    public void reset() {
        this.count = 0;
        this.path.reset();
    }

    public void addPointFiltered(float f, float f2, float f3) {
        int i = this.count;
        if (i > 0) {
            float f4 = this.x[i - 1] - f;
            float f5 = this.y[i - 1] - f2;
            if ((f4 * f4) + (f5 * f5) < f3 * f3) {
                return;
            }
        }
        addPoint(f, f2);
    }

    public void addPoint(float f, float f2) {
        float[] fArr = this.x;
        if (fArr == null || this.count == fArr.length) {
            resize(fArr != null ? fArr.length << 1 : 256);
        }
        float[] fArr2 = this.x;
        int i = this.count;
        fArr2[i] = f;
        this.y[i] = f2;
        this.count = i + 1;
        recalculateControlPoints();
    }

    private void resize(int i) {
        this.x = Arrays.copyOf(this.x, i);
        this.y = Arrays.copyOf(this.y, i);
        allocControlPointsAndWorkingMemory(i - 1);
    }

    private void allocControlPointsAndWorkingMemory(int i) {
        this.p1x = new float[i];
        this.p1y = new float[i];
        this.p2x = new float[i];
        this.p2y = new float[i];
        this.a = new float[i];
        this.b = new float[i];
        this.c = new float[i];
        this.r = new float[i];
    }

    private void recalculateControlPoints() {
        this.path.reset();
        int i = this.count;
        if (i > 2) {
            computeControlPoints(this.x, this.p1x, this.p2x, i);
            computeControlPoints(this.y, this.p1y, this.p2y, this.count);
        }
        this.path.moveTo(this.x[0], this.y[0]);
        int i2 = this.count;
        if (i2 == 1) {
            this.path.lineTo(this.x[0], this.y[0]);
        } else if (i2 != 2) {
            int i3 = 1;
            while (i3 < this.count - 1) {
                Path path = this.path;
                float f = this.p1x[i3];
                float f2 = this.p1y[i3];
                float f3 = this.p2x[i3];
                float f4 = this.p2y[i3];
                i3++;
                path.cubicTo(f, f2, f3, f4, this.x[i3], this.y[i3]);
            }
        } else {
            this.path.lineTo(this.x[1], this.y[1]);
        }
    }

    public void draw(Canvas canvas, Paint paint) {
        canvas.drawPath(this.path, paint);
    }

    private void computeControlPoints(float[] fArr, float[] fArr2, float[] fArr3, int i) {
        int i2;
        int i3 = i - 1;
        int i4 = 0;
        this.a[0] = 0.0f;
        this.b[0] = 2.0f;
        this.c[0] = 1.0f;
        this.r[0] = fArr[0] + (fArr[1] * 2.0f);
        int i5 = 1;
        while (true) {
            i2 = i3 - 1;
            if (i5 >= i2) {
                break;
            }
            this.a[i5] = 1.0f;
            this.b[i5] = 4.0f;
            this.c[i5] = 1.0f;
            int i6 = i5 + 1;
            this.r[i5] = (fArr[i5] * 4.0f) + (fArr[i6] * 2.0f);
            i5 = i6;
        }
        this.a[i2] = 2.0f;
        this.b[i2] = 7.0f;
        this.c[i2] = 0.0f;
        this.r[i2] = (fArr[i2] * 8.0f) + fArr[i3];
        for (int i7 = 1; i7 < i3; i7++) {
            float f = this.a[i7];
            float[] fArr4 = this.b;
            int i8 = i7 - 1;
            float f2 = f / fArr4[i8];
            fArr4[i7] = fArr4[i7] - (this.c[i8] * f2);
            float[] fArr5 = this.r;
            fArr5[i7] = fArr5[i7] - (f2 * fArr5[i8]);
        }
        fArr2[i2] = this.r[i2] / this.b[i2];
        for (int i9 = i3 - 2; i9 >= 0; i9--) {
            fArr2[i9] = (this.r[i9] - (this.c[i9] * fArr2[i9 + 1])) / this.b[i9];
        }
        while (i4 < i2) {
            int i10 = i4 + 1;
            fArr3[i4] = (fArr[i10] * 2.0f) - fArr2[i10];
            i4 = i10;
        }
        fArr3[i2] = (fArr[i3] + fArr2[i2]) * 0.5f;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloatArray(Arrays.copyOfRange(this.x, 0, this.count));
        parcel.writeFloatArray(Arrays.copyOfRange(this.y, 0, this.count));
    }
}
