package org.signal.imageeditor.core.model;

import android.animation.ValueAnimator;
import android.graphics.Matrix;
import android.view.animation.CycleInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import org.signal.imageeditor.core.CanvasMatrix;

/* loaded from: classes3.dex */
public final class AnimationMatrix {
    static AnimationMatrix NULL = new AnimationMatrix();
    private static final float[] iValues;
    private static final Interpolator interpolator = new DecelerateInterpolator();
    private static final Interpolator pulseInterpolator = inverse(new CycleInterpolator(0.5f));
    private float animatedFraction;
    private ValueAnimator animator;
    private final boolean canAnimate;
    private final Runnable invalidate;
    private final Matrix temp;
    private final float[] tempValues;
    private final float[] undoValues;

    static {
        float[] fArr = new float[9];
        iValues = fArr;
        interpolator = new DecelerateInterpolator();
        pulseInterpolator = inverse(new CycleInterpolator(0.5f));
        NULL = new AnimationMatrix();
        new Matrix().getValues(fArr);
    }

    private AnimationMatrix(Matrix matrix, Runnable runnable) {
        float[] fArr = new float[9];
        this.undoValues = fArr;
        this.temp = new Matrix();
        this.tempValues = new float[9];
        this.invalidate = runnable;
        this.canAnimate = true;
        matrix.getValues(fArr);
    }

    private AnimationMatrix() {
        this.undoValues = new float[9];
        this.temp = new Matrix();
        this.tempValues = new float[9];
        this.canAnimate = false;
        this.invalidate = null;
    }

    public static AnimationMatrix animate(Matrix matrix, Matrix matrix2, Runnable runnable) {
        if (runnable == null) {
            return NULL;
        }
        Matrix matrix3 = new Matrix();
        boolean invert = matrix2.invert(matrix3);
        if (invert) {
            matrix3.preConcat(matrix);
        }
        if (!invert || matrix3.isIdentity()) {
            return NULL;
        }
        AnimationMatrix animationMatrix = new AnimationMatrix(matrix3, runnable);
        animationMatrix.start(interpolator);
        return animationMatrix;
    }

    public static AnimationMatrix singlePulse(Matrix matrix, Runnable runnable) {
        if (runnable == null) {
            return NULL;
        }
        AnimationMatrix animationMatrix = new AnimationMatrix(matrix, runnable);
        animationMatrix.start(pulseInterpolator);
        return animationMatrix;
    }

    private void start(Interpolator interpolator2) {
        if (this.canAnimate) {
            ValueAnimator ofFloat = ValueAnimator.ofFloat(1.0f, 0.0f);
            this.animator = ofFloat;
            ofFloat.setDuration(250L);
            this.animator.setInterpolator(interpolator2);
            this.animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.signal.imageeditor.core.model.AnimationMatrix$$ExternalSyntheticLambda1
                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    AnimationMatrix.$r8$lambda$yt6F6VSf2HGZNHXJ_Q44vv7CEgw(AnimationMatrix.this, valueAnimator);
                }
            });
            this.animator.start();
        }
    }

    public /* synthetic */ void lambda$start$0(ValueAnimator valueAnimator) {
        this.animatedFraction = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        this.invalidate.run();
    }

    public void stop() {
        ValueAnimator valueAnimator = this.animator;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
    }

    public void preConcatValueTo(Matrix matrix) {
        if (this.canAnimate) {
            matrix.preConcat(buildTemp());
        }
    }

    public void preConcatValueTo(CanvasMatrix canvasMatrix) {
        if (this.canAnimate) {
            canvasMatrix.concat(buildTemp());
        }
    }

    private Matrix buildTemp() {
        if (!this.canAnimate) {
            this.temp.reset();
            return this.temp;
        }
        float f = 1.0f - this.animatedFraction;
        for (int i = 0; i < 9; i++) {
            this.tempValues[i] = (iValues[i] * f) + (this.animatedFraction * this.undoValues[i]);
        }
        this.temp.setValues(this.tempValues);
        return this.temp;
    }

    private static Interpolator inverse(Interpolator interpolator2) {
        return new Interpolator(interpolator2) { // from class: org.signal.imageeditor.core.model.AnimationMatrix$$ExternalSyntheticLambda0
            public final /* synthetic */ Interpolator f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.animation.TimeInterpolator
            public final float getInterpolation(float f) {
                return AnimationMatrix.$r8$lambda$mQl4mVibG4cdaDmBFguro7XN2CY(this.f$0, f);
            }
        };
    }

    public static /* synthetic */ float lambda$inverse$1(Interpolator interpolator2, float f) {
        return 1.0f - interpolator2.getInterpolation(f);
    }
}
