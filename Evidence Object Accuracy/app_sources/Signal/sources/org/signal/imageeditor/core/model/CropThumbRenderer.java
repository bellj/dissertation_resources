package org.signal.imageeditor.core.model;

import android.graphics.Matrix;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.UUID;
import org.signal.imageeditor.R$dimen;
import org.signal.imageeditor.core.Bounds;
import org.signal.imageeditor.core.Renderer;
import org.signal.imageeditor.core.RendererContext;
import org.signal.imageeditor.core.model.ThumbRenderer;

/* loaded from: classes3.dex */
public class CropThumbRenderer implements Renderer, ThumbRenderer {
    public static final Parcelable.Creator<CropThumbRenderer> CREATOR = new Parcelable.Creator<CropThumbRenderer>() { // from class: org.signal.imageeditor.core.model.CropThumbRenderer.1
        @Override // android.os.Parcelable.Creator
        public CropThumbRenderer createFromParcel(Parcel parcel) {
            return new CropThumbRenderer(ThumbRenderer.ControlPoint.values()[parcel.readInt()], ParcelUtils.readUUID(parcel));
        }

        @Override // android.os.Parcelable.Creator
        public CropThumbRenderer[] newArray(int i) {
            return new CropThumbRenderer[i];
        }
    };
    private final float[] centreOnScreen = new float[2];
    private final ThumbRenderer.ControlPoint controlPoint;
    private final Matrix matrix = new Matrix();
    private int size;
    private final UUID toControl;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public CropThumbRenderer(ThumbRenderer.ControlPoint controlPoint, UUID uuid) {
        this.controlPoint = controlPoint;
        this.toControl = uuid;
    }

    @Override // org.signal.imageeditor.core.model.ThumbRenderer
    public ThumbRenderer.ControlPoint getControlPoint() {
        return this.controlPoint;
    }

    @Override // org.signal.imageeditor.core.model.ThumbRenderer
    public UUID getElementToControl() {
        return this.toControl;
    }

    @Override // org.signal.imageeditor.core.Renderer
    public void render(RendererContext rendererContext) {
        rendererContext.canvasMatrix.mapPoints(this.centreOnScreen, Bounds.CENTRE);
        rendererContext.canvasMatrix.copyTo(this.matrix);
        this.size = rendererContext.context.getResources().getDimensionPixelSize(R$dimen.crop_area_renderer_edge_size);
    }

    @Override // org.signal.imageeditor.core.Renderer
    public boolean hitTest(float f, float f2) {
        float[] fArr = new float[2];
        this.matrix.mapPoints(fArr, new float[]{f, f2});
        float[] fArr2 = this.centreOnScreen;
        float f3 = fArr2[0] - fArr[0];
        float f4 = fArr2[1] - fArr[1];
        float f5 = (f3 * f3) + (f4 * f4);
        int i = this.size;
        if (f5 < ((float) (i * i))) {
            return true;
        }
        return false;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.controlPoint.ordinal());
        ParcelUtils.writeUUID(parcel, this.toControl);
    }
}
