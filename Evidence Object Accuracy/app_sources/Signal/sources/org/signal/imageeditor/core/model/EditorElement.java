package org.signal.imageeditor.core.model;

import android.graphics.Matrix;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.signal.imageeditor.core.MatrixUtils;
import org.signal.imageeditor.core.Renderer;
import org.signal.imageeditor.core.RendererContext;

/* loaded from: classes3.dex */
public final class EditorElement implements Parcelable {
    public static final Parcelable.Creator<EditorElement> CREATOR = new Parcelable.Creator<EditorElement>() { // from class: org.signal.imageeditor.core.model.EditorElement.1
        @Override // android.os.Parcelable.Creator
        public EditorElement createFromParcel(Parcel parcel) {
            return new EditorElement(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public EditorElement[] newArray(int i) {
            return new EditorElement[i];
        }
    };
    private static final Comparator<EditorElement> Z_ORDER_COMPARATOR = new Comparator() { // from class: org.signal.imageeditor.core.model.EditorElement$$ExternalSyntheticLambda0
        @Override // java.util.Comparator
        public final int compare(Object obj, Object obj2) {
            return EditorElement.lambda$static$0((EditorElement) obj, (EditorElement) obj2);
        }
    };
    private AlphaAnimation alphaAnimation;
    private AnimationMatrix animationMatrix;
    private final List<EditorElement> children;
    private final List<EditorElement> deletedChildren;
    private final Matrix editorMatrix;
    private final EditorFlags flags;
    private final UUID id;
    private final Matrix localMatrix;
    private final Renderer renderer;
    private final Matrix temp;
    private final Matrix tempMatrix;
    private final int zOrder;

    /* loaded from: classes3.dex */
    public interface FindElementPredicate {
        boolean test(EditorElement editorElement, Matrix matrix);
    }

    /* loaded from: classes3.dex */
    public interface PerElementFunction {
        void apply(EditorElement editorElement);
    }

    public static /* synthetic */ boolean lambda$findElement$1(EditorElement editorElement, EditorElement editorElement2, Matrix matrix) {
        return editorElement == editorElement2;
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public static /* synthetic */ int lambda$static$0(EditorElement editorElement, EditorElement editorElement2) {
        return Integer.compare(editorElement.zOrder, editorElement2.zOrder);
    }

    public EditorElement(Renderer renderer) {
        this(renderer, 0);
    }

    public EditorElement(Renderer renderer, int i) {
        this.localMatrix = new Matrix();
        this.editorMatrix = new Matrix();
        this.temp = new Matrix();
        this.tempMatrix = new Matrix();
        this.children = new LinkedList();
        this.deletedChildren = new LinkedList();
        this.animationMatrix = AnimationMatrix.NULL;
        this.alphaAnimation = AlphaAnimation.NULL_1;
        this.id = UUID.randomUUID();
        this.flags = new EditorFlags();
        this.renderer = renderer;
        this.zOrder = i;
    }

    private EditorElement(Parcel parcel) {
        Matrix matrix = new Matrix();
        this.localMatrix = matrix;
        this.editorMatrix = new Matrix();
        this.temp = new Matrix();
        this.tempMatrix = new Matrix();
        LinkedList linkedList = new LinkedList();
        this.children = linkedList;
        this.deletedChildren = new LinkedList();
        this.animationMatrix = AnimationMatrix.NULL;
        this.alphaAnimation = AlphaAnimation.NULL_1;
        this.id = ParcelUtils.readUUID(parcel);
        this.flags = new EditorFlags(parcel.readInt());
        ParcelUtils.readMatrix(matrix, parcel);
        this.renderer = (Renderer) parcel.readParcelable(Renderer.class.getClassLoader());
        this.zOrder = parcel.readInt();
        parcel.readTypedList(linkedList, CREATOR);
    }

    public UUID getId() {
        return this.id;
    }

    public Renderer getRenderer() {
        return this.renderer;
    }

    public void draw(RendererContext rendererContext) {
        if (this.flags.isVisible() || this.flags.isChildrenVisible()) {
            rendererContext.save();
            rendererContext.canvasMatrix.concat(this.localMatrix);
            if (rendererContext.isEditing()) {
                rendererContext.canvasMatrix.concat(this.editorMatrix);
                this.animationMatrix.preConcatValueTo(rendererContext.canvasMatrix);
            }
            if (this.flags.isVisible()) {
                float value = this.alphaAnimation.getValue();
                if (value > 0.0f) {
                    rendererContext.setFade(value);
                    rendererContext.setChildren(this.children);
                    drawSelf(rendererContext);
                    rendererContext.setFade(1.0f);
                }
            }
            if (this.flags.isChildrenVisible()) {
                drawChildren(this.children, rendererContext);
                drawChildren(this.deletedChildren, rendererContext);
            }
            rendererContext.restore();
        }
    }

    private void drawSelf(RendererContext rendererContext) {
        Renderer renderer = this.renderer;
        if (renderer != null) {
            renderer.render(rendererContext);
        }
    }

    private static void drawChildren(List<EditorElement> list, RendererContext rendererContext) {
        for (EditorElement editorElement : list) {
            if (editorElement.zOrder >= 0) {
                editorElement.draw(rendererContext);
            }
        }
    }

    public void addElement(EditorElement editorElement) {
        this.children.add(editorElement);
        Collections.sort(this.children, Z_ORDER_COMPARATOR);
    }

    public Matrix getLocalMatrix() {
        return this.localMatrix;
    }

    public Matrix getEditorMatrix() {
        return this.editorMatrix;
    }

    public EditorElement findElement(EditorElement editorElement, Matrix matrix, Matrix matrix2) {
        return findElement(matrix, matrix2, new FindElementPredicate() { // from class: org.signal.imageeditor.core.model.EditorElement$$ExternalSyntheticLambda1
            @Override // org.signal.imageeditor.core.model.EditorElement.FindElementPredicate
            public final boolean test(EditorElement editorElement2, Matrix matrix3) {
                return EditorElement.lambda$findElement$1(EditorElement.this, editorElement2, matrix3);
            }
        });
    }

    public EditorElement findElementAt(float f, float f2, Matrix matrix, Matrix matrix2) {
        return findElement(matrix, matrix2, new FindElementPredicate(new float[2], new float[]{f, f2}) { // from class: org.signal.imageeditor.core.model.EditorElement$$ExternalSyntheticLambda2
            public final /* synthetic */ float[] f$0;
            public final /* synthetic */ float[] f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // org.signal.imageeditor.core.model.EditorElement.FindElementPredicate
            public final boolean test(EditorElement editorElement, Matrix matrix3) {
                return EditorElement.lambda$findElementAt$2(this.f$0, this.f$1, editorElement, matrix3);
            }
        });
    }

    public static /* synthetic */ boolean lambda$findElementAt$2(float[] fArr, float[] fArr2, EditorElement editorElement, Matrix matrix) {
        Renderer renderer = editorElement.renderer;
        if (renderer == null) {
            return false;
        }
        matrix.mapPoints(fArr, fArr2);
        if (!editorElement.flags.isSelectable() || !renderer.hitTest(fArr[0], fArr[1])) {
            return false;
        }
        return true;
    }

    public EditorElement findElement(Matrix matrix, Matrix matrix2, FindElementPredicate findElementPredicate) {
        this.temp.set(matrix);
        this.temp.preConcat(this.localMatrix);
        this.temp.preConcat(this.editorMatrix);
        if (!this.temp.invert(this.tempMatrix)) {
            return null;
        }
        for (int size = this.children.size() - 1; size >= 0; size--) {
            EditorElement findElement = this.children.get(size).findElement(this.temp, matrix2, findElementPredicate);
            if (findElement != null) {
                return findElement;
            }
        }
        if (!findElementPredicate.test(this, this.tempMatrix)) {
            return null;
        }
        matrix2.set(this.tempMatrix);
        return this;
    }

    public EditorFlags getFlags() {
        return this.flags;
    }

    public int getChildCount() {
        return this.children.size();
    }

    public EditorElement getChild(int i) {
        return this.children.get(i);
    }

    public void forAllInTree(PerElementFunction perElementFunction) {
        perElementFunction.apply(this);
        for (EditorElement editorElement : this.children) {
            editorElement.forAllInTree(perElementFunction);
        }
    }

    public EditorElement findParent(EditorElement editorElement) {
        for (EditorElement editorElement2 : this.children) {
            if (editorElement2 == editorElement) {
                return this;
            }
            EditorElement findParent = editorElement2.findParent(editorElement);
            if (findParent != null) {
                return findParent;
            }
        }
        return null;
    }

    public EditorElement findElementWithId(UUID uuid) {
        for (EditorElement editorElement : this.children) {
            if (uuid.equals(editorElement.id)) {
                return editorElement;
            }
            EditorElement findElementWithId = editorElement.findElementWithId(uuid);
            if (findElementWithId != null) {
                return findElementWithId;
            }
        }
        return null;
    }

    public void deleteChild(EditorElement editorElement, Runnable runnable) {
        Iterator<EditorElement> it = this.children.iterator();
        while (it.hasNext()) {
            if (it.next() == editorElement) {
                it.remove();
                addDeletedChildFadingOut(editorElement, runnable);
            }
        }
    }

    public void addDeletedChildFadingOut(EditorElement editorElement, Runnable runnable) {
        this.deletedChildren.add(editorElement);
        editorElement.animateFadeOut(runnable);
    }

    void animateFadeOut(Runnable runnable) {
        this.alphaAnimation = AlphaAnimation.animate(1.0f, 0.0f, runnable);
    }

    public void animateFadeIn(Runnable runnable) {
        this.alphaAnimation = AlphaAnimation.animate(0.0f, 1.0f, runnable);
    }

    public void animatePartialFadeOut(Runnable runnable) {
        this.alphaAnimation = AlphaAnimation.animate(this.alphaAnimation.getValue(), 0.5f, runnable);
    }

    public void animatePartialFadeIn(Runnable runnable) {
        this.alphaAnimation = AlphaAnimation.animate(this.alphaAnimation.getValue(), 1.0f, runnable);
    }

    public EditorElement parentOf(EditorElement editorElement) {
        if (this.children.contains(editorElement)) {
            return this;
        }
        for (EditorElement editorElement2 : this.children) {
            EditorElement parentOf = editorElement2.parentOf(editorElement);
            if (parentOf != null) {
                return parentOf;
            }
        }
        return null;
    }

    public void singleScalePulse(Runnable runnable) {
        Matrix matrix = new Matrix();
        matrix.setScale(1.2f, 1.2f);
        this.animationMatrix = AnimationMatrix.singlePulse(matrix, runnable);
    }

    public int getZOrder() {
        return this.zOrder;
    }

    public void deleteAllChildren() {
        this.children.clear();
    }

    public float getLocalRotationAngle() {
        return MatrixUtils.getRotationAngle(this.localMatrix);
    }

    public float getLocalScaleX() {
        return MatrixUtils.getScaleX(this.localMatrix);
    }

    public void commitEditorMatrix() {
        if (this.flags.isEditable()) {
            this.localMatrix.preConcat(this.editorMatrix);
            this.editorMatrix.reset();
            return;
        }
        rollbackEditorMatrix(null);
    }

    public void rollbackEditorMatrix(Runnable runnable) {
        animateEditorTo(new Matrix(), runnable);
    }

    public void buildMap(Map<UUID, EditorElement> map) {
        map.put(this.id, this);
        for (EditorElement editorElement : this.children) {
            editorElement.buildMap(map);
        }
    }

    public void animateFrom(Matrix matrix, Runnable runnable) {
        Matrix matrix2 = new Matrix(matrix);
        this.animationMatrix.stop();
        this.animationMatrix.preConcatValueTo(matrix2);
        this.animationMatrix = AnimationMatrix.animate(matrix2, this.localMatrix, runnable);
    }

    public void animateEditorTo(Matrix matrix, Runnable runnable) {
        setMatrixWithAnimation(this.editorMatrix, matrix, runnable);
    }

    public void animateLocalTo(Matrix matrix, Runnable runnable) {
        setMatrixWithAnimation(this.localMatrix, matrix, runnable);
    }

    private void setMatrixWithAnimation(Matrix matrix, Matrix matrix2, Runnable runnable) {
        Matrix matrix3 = new Matrix(matrix);
        this.animationMatrix.stop();
        this.animationMatrix.preConcatValueTo(matrix3);
        matrix.set(matrix2);
        this.animationMatrix = AnimationMatrix.animate(matrix3, matrix, runnable);
    }

    public Matrix getLocalMatrixAnimating() {
        Matrix matrix = new Matrix(this.localMatrix);
        this.animationMatrix.preConcatValueTo(matrix);
        return matrix;
    }

    public void stopAnimation() {
        this.animationMatrix.stop();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        ParcelUtils.writeUUID(parcel, this.id);
        parcel.writeInt(this.flags.asInt());
        ParcelUtils.writeMatrix(parcel, this.localMatrix);
        parcel.writeParcelable(this.renderer, i);
        parcel.writeInt(this.zOrder);
        parcel.writeTypedList(this.children);
    }
}
