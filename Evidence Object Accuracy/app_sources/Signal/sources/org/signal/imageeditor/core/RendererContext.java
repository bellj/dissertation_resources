package org.signal.imageeditor.core;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Typeface;
import java.util.Collections;
import java.util.List;
import org.signal.imageeditor.core.model.EditorElement;

/* loaded from: classes3.dex */
public final class RendererContext {
    private boolean blockingLoad;
    public final Canvas canvas;
    public final CanvasMatrix canvasMatrix;
    private List<EditorElement> children = Collections.emptyList();
    public final Context context;
    private float fade = 1.0f;
    public final Invalidate invalidate;
    private boolean isEditing = true;
    private Paint maskPaint;
    public final Ready rendererReady;
    public final TypefaceProvider typefaceProvider;

    /* loaded from: classes3.dex */
    public interface Invalidate {
        public static final Invalidate NULL = new RendererContext$Invalidate$$ExternalSyntheticLambda0();

        /* renamed from: org.signal.imageeditor.core.RendererContext$Invalidate$-CC */
        /* loaded from: classes3.dex */
        public final /* synthetic */ class CC {
            static {
                Invalidate invalidate = Invalidate.NULL;
            }

            public static /* synthetic */ void lambda$static$0(Renderer renderer) {
            }
        }

        void onInvalidate(Renderer renderer);
    }

    /* loaded from: classes3.dex */
    public interface Ready {
        public static final Ready NULL = new RendererContext$Ready$$ExternalSyntheticLambda0();

        /* renamed from: org.signal.imageeditor.core.RendererContext$Ready$-CC */
        /* loaded from: classes3.dex */
        public final /* synthetic */ class CC {
            static {
                Ready ready = Ready.NULL;
            }

            public static /* synthetic */ void lambda$static$0(Renderer renderer, Matrix matrix, Point point) {
            }
        }

        void onReady(Renderer renderer, Matrix matrix, Point point);
    }

    /* loaded from: classes3.dex */
    public interface TypefaceProvider {
        Typeface getSelectedTypeface(Context context, Renderer renderer, Invalidate invalidate);
    }

    public RendererContext(Context context, Canvas canvas, Ready ready, Invalidate invalidate, TypefaceProvider typefaceProvider) {
        this.context = context;
        this.canvas = canvas;
        this.canvasMatrix = new CanvasMatrix(canvas);
        this.rendererReady = ready;
        this.invalidate = invalidate;
        this.typefaceProvider = typefaceProvider;
    }

    public void setBlockingLoad(boolean z) {
        this.blockingLoad = z;
    }

    public boolean isBlockingLoad() {
        return this.blockingLoad;
    }

    public boolean mapRect(RectF rectF, RectF rectF2) {
        return this.canvasMatrix.mapRect(rectF, rectF2);
    }

    public void setIsEditing(boolean z) {
        this.isEditing = z;
    }

    public boolean isEditing() {
        return this.isEditing;
    }

    public void setFade(float f) {
        this.fade = f;
    }

    public int getAlpha(int i) {
        return Math.max(0, Math.min(255, (int) (this.fade * ((float) i))));
    }

    public void save() {
        this.canvasMatrix.save();
    }

    public void restore() {
        this.canvasMatrix.restore();
    }

    public void setChildren(List<EditorElement> list) {
        this.children = list;
    }

    public List<EditorElement> getChildren() {
        return this.children;
    }

    public void setMaskPaint(Paint paint) {
        this.maskPaint = paint;
    }

    public Paint getMaskPaint() {
        return this.maskPaint;
    }
}
