package org.signal.imageeditor.core;

import android.graphics.Matrix;
import android.graphics.PointF;
import org.signal.imageeditor.core.model.EditorElement;

/* loaded from: classes3.dex */
public interface EditSession {
    void commit();

    EditorElement getSelected();

    void movePoint(int i, PointF pointF);

    EditSession newPoint(Matrix matrix, PointF pointF, int i);

    EditSession removePoint(Matrix matrix, int i);
}
