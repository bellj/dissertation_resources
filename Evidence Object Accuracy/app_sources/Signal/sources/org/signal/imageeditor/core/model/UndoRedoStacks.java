package org.signal.imageeditor.core.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

/* loaded from: classes3.dex */
public final class UndoRedoStacks implements Parcelable {
    public static final Parcelable.Creator<UndoRedoStacks> CREATOR = new Parcelable.Creator<UndoRedoStacks>() { // from class: org.signal.imageeditor.core.model.UndoRedoStacks.1
        @Override // android.os.Parcelable.Creator
        public UndoRedoStacks createFromParcel(Parcel parcel) {
            return new UndoRedoStacks((ElementStack) parcel.readParcelable(ElementStack.class.getClassLoader()), (ElementStack) parcel.readParcelable(ElementStack.class.getClassLoader()), parcel.createByteArray());
        }

        @Override // android.os.Parcelable.Creator
        public UndoRedoStacks[] newArray(int i) {
            return new UndoRedoStacks[i];
        }
    };
    private final ElementStack redoStack;
    private byte[] unchangedState;
    private final ElementStack undoStack;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public UndoRedoStacks(int i) {
        this(new ElementStack(i), new ElementStack(i), null);
    }

    private UndoRedoStacks(ElementStack elementStack, ElementStack elementStack2, byte[] bArr) {
        this.undoStack = elementStack;
        this.redoStack = elementStack2;
        this.unchangedState = bArr == null ? new byte[0] : bArr;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.undoStack, i);
        parcel.writeParcelable(this.redoStack, i);
        parcel.writeByteArray(this.unchangedState);
    }

    public ElementStack getUndoStack() {
        return this.undoStack;
    }

    public ElementStack getRedoStack() {
        return this.redoStack;
    }

    public void pushState(EditorElement editorElement) {
        if (this.undoStack.tryPush(editorElement)) {
            this.redoStack.clear();
        }
    }

    public void clear(EditorElement editorElement) {
        this.undoStack.clear();
        this.redoStack.clear();
        this.unchangedState = ElementStack.getBytes(editorElement);
    }

    public boolean isChanged(EditorElement editorElement) {
        return !Arrays.equals(ElementStack.getBytes(editorElement), this.unchangedState);
    }

    public boolean canUndo(EditorElement editorElement) {
        return this.undoStack.stackContainsStateDifferentFrom(editorElement);
    }

    public boolean canRedo(EditorElement editorElement) {
        return this.redoStack.stackContainsStateDifferentFrom(editorElement);
    }
}
