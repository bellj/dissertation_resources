package org.signal.imageeditor.core;

import android.graphics.Matrix;
import android.graphics.PointF;
import org.signal.imageeditor.core.model.EditorElement;
import org.signal.imageeditor.core.model.ThumbRenderer;

/* access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class ThumbDragEditSession extends ElementEditSession {
    private final ThumbRenderer.ControlPoint controlPoint;
    private final PointF oppositeControlPoint = new PointF();
    private final float[] oppositeControlPointOnControlParent = new float[2];
    private final float[] oppositeControlPointOnElement = new float[2];
    private final Matrix thumbContainerRelativeMatrix;

    @Override // org.signal.imageeditor.core.EditSession
    public EditSession newPoint(Matrix matrix, PointF pointF, int i) {
        return null;
    }

    @Override // org.signal.imageeditor.core.EditSession
    public EditSession removePoint(Matrix matrix, int i) {
        return null;
    }

    private ThumbDragEditSession(EditorElement editorElement, ThumbRenderer.ControlPoint controlPoint, Matrix matrix, Matrix matrix2) {
        super(editorElement, matrix);
        this.controlPoint = controlPoint;
        this.thumbContainerRelativeMatrix = matrix2;
    }

    public static EditSession startDrag(EditorElement editorElement, Matrix matrix, Matrix matrix2, ThumbRenderer.ControlPoint controlPoint, PointF pointF) {
        if (!editorElement.getFlags().isEditable()) {
            return null;
        }
        ThumbDragEditSession thumbDragEditSession = new ThumbDragEditSession(editorElement, controlPoint, matrix, matrix2);
        thumbDragEditSession.setScreenStartPoint(0, pointF);
        thumbDragEditSession.setScreenEndPoint(0, pointF);
        return thumbDragEditSession;
    }

    @Override // org.signal.imageeditor.core.EditSession
    public void movePoint(int i, PointF pointF) {
        setScreenEndPoint(i, pointF);
        Matrix editorMatrix = this.selected.getEditorMatrix();
        editorMatrix.reset();
        boolean z = false;
        this.oppositeControlPointOnControlParent[0] = this.controlPoint.opposite().getX();
        this.oppositeControlPointOnControlParent[1] = this.controlPoint.opposite().getY();
        this.thumbContainerRelativeMatrix.mapPoints(this.oppositeControlPointOnElement, this.oppositeControlPointOnControlParent);
        float[] fArr = this.oppositeControlPointOnElement;
        float f = fArr[0];
        float f2 = fArr[1];
        this.oppositeControlPoint.set(f, f2);
        PointF pointF2 = this.endPointElement[0];
        float f3 = pointF2.x;
        PointF pointF3 = this.startPointElement[0];
        float f4 = pointF2.y - pointF3.y;
        float x = this.controlPoint.getX() + (f3 - pointF3.x);
        float y = this.controlPoint.getY() + f4;
        if (this.controlPoint.isScaleAndRotateThumb()) {
            float findScale = findScale(this.oppositeControlPoint, this.startPointElement[0], this.endPointElement[0]);
            PointF pointF4 = this.oppositeControlPoint;
            editorMatrix.postTranslate(-pointF4.x, -pointF4.y);
            editorMatrix.postScale(findScale, findScale);
            rotate(editorMatrix, angle(this.endPointElement[0], this.oppositeControlPoint) - angle(this.startPointElement[0], this.oppositeControlPoint));
            PointF pointF5 = this.oppositeControlPoint;
            editorMatrix.postTranslate(pointF5.x, pointF5.y);
            return;
        }
        if (this.selected.getFlags().isAspectLocked() && !this.controlPoint.isCenter()) {
            z = true;
        }
        float f5 = z ? 2.0f : 1.0f;
        float x2 = this.controlPoint.isVerticalCenter() ? f5 : (x - f) / (this.controlPoint.getX() - f);
        if (!this.controlPoint.isHorizontalCenter()) {
            f5 = (y - f2) / (this.controlPoint.getY() - f2);
        }
        scale(editorMatrix, z, x2, f5, this.controlPoint.opposite());
    }

    private static void scale(Matrix matrix, boolean z, float f, float f2, ThumbRenderer.ControlPoint controlPoint) {
        float x = controlPoint.getX();
        float y = controlPoint.getY();
        matrix.postTranslate(-x, -y);
        if (z) {
            float min = Math.min(f, f2);
            matrix.postScale(min, min);
        } else {
            matrix.postScale(f, f2);
        }
        matrix.postTranslate(x, y);
    }

    private static void rotate(Matrix matrix, double d) {
        matrix.postRotate((float) Math.toDegrees(d));
    }

    private static double angle(PointF pointF, PointF pointF2) {
        return Math.atan2((double) (pointF.y - pointF2.y), (double) (pointF.x - pointF2.x));
    }

    private static float findScale(PointF pointF, PointF pointF2, PointF pointF3) {
        return (float) Math.sqrt((double) (getDistanceSquared(pointF3, pointF) / getDistanceSquared(pointF2, pointF)));
    }

    private static float getDistanceSquared(PointF pointF, PointF pointF2) {
        float f = pointF.x - pointF2.x;
        float f2 = pointF.y - pointF2.y;
        return (f * f) + (f2 * f2);
    }
}
