package org.signal.imageeditor.core;

/* loaded from: classes3.dex */
public interface UndoRedoStackListener {
    void onAvailabilityChanged(boolean z, boolean z2);
}
