package org.signal.imageeditor.core;

import android.graphics.Matrix;

/* loaded from: classes3.dex */
public final class MatrixUtils {
    private static final ThreadLocal<float[]> tempMatrixValues = new ThreadLocal<>();

    protected static float[] getTempMatrixValues() {
        ThreadLocal<float[]> threadLocal = tempMatrixValues;
        float[] fArr = threadLocal.get();
        if (fArr != null) {
            return fArr;
        }
        float[] fArr2 = new float[9];
        threadLocal.set(fArr2);
        return fArr2;
    }

    public static float getRotationAngle(Matrix matrix) {
        float[] tempMatrixValues2 = getTempMatrixValues();
        matrix.getValues(tempMatrixValues2);
        return (float) (-Math.atan2((double) tempMatrixValues2[1], (double) tempMatrixValues2[0]));
    }

    public static float getScaleX(Matrix matrix) {
        float[] tempMatrixValues2 = getTempMatrixValues();
        matrix.getValues(tempMatrixValues2);
        float f = tempMatrixValues2[0];
        float f2 = tempMatrixValues2[1];
        return (float) Math.sqrt((double) ((f * f) + (f2 * f2)));
    }
}
