package org.signal.imageeditor.core;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import androidx.core.view.GestureDetectorCompat;
import java.util.LinkedList;
import java.util.List;
import org.signal.imageeditor.R$styleable;
import org.signal.imageeditor.core.HiddenEditText;
import org.signal.imageeditor.core.RendererContext;
import org.signal.imageeditor.core.model.EditorElement;
import org.signal.imageeditor.core.model.EditorModel;
import org.signal.imageeditor.core.model.ThumbRenderer;
import org.signal.imageeditor.core.renderers.BezierDrawingRenderer;
import org.signal.imageeditor.core.renderers.MultiLineTextRenderer;
import org.signal.imageeditor.core.renderers.TrashRenderer;

/* loaded from: classes3.dex */
public final class ImageEditorView extends FrameLayout {
    private Paint.Cap cap = Paint.Cap.ROUND;
    private int color = -16777216;
    private GestureDetectorCompat doubleTap;
    private DragListener dragListener;
    private DrawingChangedListener drawingChangedListener;
    private EditSession editSession;
    private HiddenEditText editText;
    private Mode mode = Mode.MoveAndResize;
    private EditorModel model;
    private boolean moreThanOnePointerUsedInSession;
    private RendererContext rendererContext;
    private final RendererContext.Invalidate rendererInvalidate = new RendererContext.Invalidate() { // from class: org.signal.imageeditor.core.ImageEditorView$$ExternalSyntheticLambda0
        @Override // org.signal.imageeditor.core.RendererContext.Invalidate
        public final void onInvalidate(Renderer renderer) {
            ImageEditorView.$r8$lambda$107OAvc_40s8ygU3TnFO6pWfU7s(ImageEditorView.this, renderer);
        }
    };
    private final RendererContext.Ready rendererReady = new RendererContext.Ready() { // from class: org.signal.imageeditor.core.ImageEditorView.1
        @Override // org.signal.imageeditor.core.RendererContext.Ready
        public void onReady(Renderer renderer, Matrix matrix, Point point) {
            ImageEditorView.this.model.onReady(renderer, matrix, point);
            ImageEditorView.this.invalidate();
        }
    };
    private final RectF screen = new RectF();
    private SizeChangedListener sizeChangedListener;
    private TapListener tapListener;
    private final List<HiddenEditText.TextFilter> textFilters = new LinkedList();
    private float thickness = 0.02f;
    private RendererContext.TypefaceProvider typefaceProvider;
    private UndoRedoStackListener undoRedoStackListener;
    private final Matrix viewMatrix = new Matrix();
    private final RectF viewPort = Bounds.newFullBounds();
    private final RectF visibleViewPort = Bounds.newFullBounds();

    /* loaded from: classes3.dex */
    public interface DragListener {
        void onDragEnded(EditorElement editorElement, boolean z);

        void onDragMoved(EditorElement editorElement, boolean z);

        void onDragStarted(EditorElement editorElement);
    }

    /* loaded from: classes3.dex */
    public interface DrawingChangedListener {
        void onDrawingChanged(boolean z);
    }

    /* loaded from: classes3.dex */
    public enum Mode {
        MoveAndResize,
        Draw,
        Blur
    }

    /* loaded from: classes3.dex */
    public interface SizeChangedListener {
        void onSizeChanged(int i, int i2);
    }

    /* loaded from: classes3.dex */
    public interface TapListener {
        void onEntityDoubleTap(EditorElement editorElement);

        void onEntityDown(EditorElement editorElement);

        void onEntitySingleTap(EditorElement editorElement);
    }

    public ImageEditorView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(attributeSet);
    }

    private void init(AttributeSet attributeSet) {
        setWillNotDraw(false);
        int i = -16777216;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, R$styleable.ImageEditorView);
            i = obtainStyledAttributes.getColor(R$styleable.ImageEditorView_imageEditorView_blackoutColor, -16777216);
            obtainStyledAttributes.recycle();
        }
        setModel(EditorModel.create(i));
        this.editText = createAHiddenTextEntryField();
        this.doubleTap = new GestureDetectorCompat(getContext(), new DoubleTapGestureListener());
        setOnTouchListener(new View.OnTouchListener() { // from class: org.signal.imageeditor.core.ImageEditorView$$ExternalSyntheticLambda3
            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                return ImageEditorView.m196$r8$lambda$olkq9fq8_YQhh3p55ikwynsumc(ImageEditorView.this, view, motionEvent);
            }
        });
    }

    public /* synthetic */ boolean lambda$init$0(View view, MotionEvent motionEvent) {
        return this.doubleTap.onTouchEvent(motionEvent);
    }

    private HiddenEditText createAHiddenTextEntryField() {
        HiddenEditText hiddenEditText = new HiddenEditText(getContext());
        addView(hiddenEditText);
        hiddenEditText.clearFocus();
        hiddenEditText.setOnEndEdit(new Runnable() { // from class: org.signal.imageeditor.core.ImageEditorView$$ExternalSyntheticLambda4
            @Override // java.lang.Runnable
            public final void run() {
                ImageEditorView.this.doneTextEditing();
            }
        });
        hiddenEditText.setOnEditOrSelectionChange(new HiddenEditText.OnEditOrSelectionChange() { // from class: org.signal.imageeditor.core.ImageEditorView$$ExternalSyntheticLambda5
            @Override // org.signal.imageeditor.core.HiddenEditText.OnEditOrSelectionChange
            public final void onChange(EditorElement editorElement, MultiLineTextRenderer multiLineTextRenderer) {
                ImageEditorView.this.zoomToFitText(editorElement, multiLineTextRenderer);
            }
        });
        hiddenEditText.addTextFilters(this.textFilters);
        return hiddenEditText;
    }

    public void startTextEditing(EditorElement editorElement) {
        getModel().addFade();
        if (editorElement.getRenderer() instanceof MultiLineTextRenderer) {
            getModel().setSelectionVisible(false);
            this.editText.setCurrentTextEditorElement(editorElement);
        }
    }

    public void zoomToFitText(EditorElement editorElement, MultiLineTextRenderer multiLineTextRenderer) {
        getModel().zoomToTextElement(editorElement, multiLineTextRenderer);
    }

    public boolean isTextEditing() {
        return this.editText.getCurrentTextEntity() != null;
    }

    public void doneTextEditing() {
        getModel().zoomOut();
        getModel().removeFade();
        getModel().setSelectionVisible(true);
        if (this.editText.getCurrentTextEntity() != null) {
            getModel().setSelected(null);
            this.editText.setCurrentTextEditorElement(null);
            this.editText.hideKeyboard();
        }
    }

    public void setTypefaceProvider(RendererContext.TypefaceProvider typefaceProvider) {
        this.typefaceProvider = typefaceProvider;
    }

    public void addTextInputFilter(HiddenEditText.TextFilter textFilter) {
        this.textFilters.add(textFilter);
        this.editText = createAHiddenTextEntryField();
    }

    @Override // android.view.View
    protected void onDraw(Canvas canvas) {
        RendererContext rendererContext = this.rendererContext;
        if (!(rendererContext != null && rendererContext.canvas == canvas && rendererContext.typefaceProvider == this.typefaceProvider)) {
            this.rendererContext = new RendererContext(getContext(), canvas, this.rendererReady, this.rendererInvalidate, this.typefaceProvider);
        }
        this.rendererContext.save();
        try {
            this.rendererContext.canvasMatrix.initial(this.viewMatrix);
            this.model.draw(this.rendererContext, this.editText.getCurrentTextEditorElement());
        } finally {
            this.rendererContext.restore();
        }
    }

    public /* synthetic */ void lambda$new$1(Renderer renderer) {
        invalidate();
    }

    @Override // android.view.View
    protected void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        updateViewMatrix();
        SizeChangedListener sizeChangedListener = this.sizeChangedListener;
        if (sizeChangedListener != null) {
            sizeChangedListener.onSizeChanged(i, i2);
        }
    }

    private void updateViewMatrix() {
        this.screen.right = (float) getWidth();
        this.screen.bottom = (float) getHeight();
        this.viewMatrix.setRectToRect(this.viewPort, this.screen, Matrix.ScaleToFit.FILL);
        float[] fArr = new float[9];
        this.viewMatrix.getValues(fArr);
        float f = fArr[0] / fArr[4];
        RectF newFullBounds = Bounds.newFullBounds();
        if (f < 1.0f) {
            newFullBounds.top /= f;
            newFullBounds.bottom /= f;
        } else {
            newFullBounds.left *= f;
            newFullBounds.right *= f;
        }
        this.visibleViewPort.set(newFullBounds);
        this.viewMatrix.setRectToRect(this.visibleViewPort, this.screen, Matrix.ScaleToFit.CENTER);
        this.model.setVisibleViewPort(this.visibleViewPort);
        invalidate();
    }

    public void setModel(EditorModel editorModel) {
        EditorModel editorModel2 = this.model;
        if (editorModel2 != editorModel) {
            if (editorModel2 != null) {
                editorModel2.setInvalidate(null);
                this.model.setUndoRedoStackListener(null);
            }
            this.model = editorModel;
            editorModel.setInvalidate(new ImageEditorView$$ExternalSyntheticLambda1(this));
            this.model.setUndoRedoStackListener(new UndoRedoStackListener() { // from class: org.signal.imageeditor.core.ImageEditorView$$ExternalSyntheticLambda2
                @Override // org.signal.imageeditor.core.UndoRedoStackListener
                public final void onAvailabilityChanged(boolean z, boolean z2) {
                    ImageEditorView.m195$r8$lambda$CB_iaBrRu8QRfMqEYQ1bLoeHGo(ImageEditorView.this, z, z2);
                }
            });
            this.model.setVisibleViewPort(this.visibleViewPort);
            invalidate();
        }
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        boolean z = false;
        z = false;
        z = false;
        if (actionMasked != 0) {
            if (actionMasked == 1) {
                EditSession editSession = this.editSession;
                if (editSession != null) {
                    editSession.commit();
                    dragDropRelease(false);
                    PointF point = getPoint(motionEvent);
                    if (motionEvent.getPointerCount() == 1 && checkTrashIntersect(point) && this.model.findElementAtPoint(point, this.viewMatrix, new Matrix()) == this.editSession.getSelected()) {
                        z = true;
                    }
                    notifyDragEnd(this.editSession.getSelected(), z);
                    this.editSession = null;
                    this.model.postEdit(this.moreThanOnePointerUsedInSession);
                    invalidate();
                    return true;
                }
                this.model.postEdit(this.moreThanOnePointerUsedInSession);
            } else if (actionMasked != 2) {
                if (actionMasked != 5) {
                    if (actionMasked == 6 && this.editSession != null && motionEvent.getActionIndex() < 2) {
                        this.editSession.commit();
                        this.model.pushUndoPoint();
                        dragDropRelease(true);
                        Matrix findElementInverseMatrix = this.model.findElementInverseMatrix(this.editSession.getSelected(), this.viewMatrix);
                        if (findElementInverseMatrix != null) {
                            this.editSession = this.editSession.removePoint(findElementInverseMatrix, motionEvent.getActionIndex());
                        } else {
                            this.editSession = null;
                        }
                        return true;
                    }
                } else if (this.editSession != null && motionEvent.getPointerCount() == 2) {
                    this.moreThanOnePointerUsedInSession = true;
                    this.editSession.commit();
                    this.model.pushUndoPoint();
                    Matrix findElementInverseMatrix2 = this.model.findElementInverseMatrix(this.editSession.getSelected(), this.viewMatrix);
                    if (findElementInverseMatrix2 != null) {
                        this.editSession = this.editSession.newPoint(findElementInverseMatrix2, getPoint(motionEvent, motionEvent.getActionIndex()), motionEvent.getActionIndex());
                    } else {
                        this.editSession = null;
                    }
                    if (this.editSession == null) {
                        dragDropRelease(false);
                    }
                    return true;
                }
            } else if (this.editSession != null) {
                int historySize = motionEvent.getHistorySize();
                int min = Math.min(2, motionEvent.getPointerCount());
                for (int i = 0; i < historySize; i++) {
                    for (int i2 = 0; i2 < min; i2++) {
                        this.editSession.movePoint(i2, getHistoricalPoint(motionEvent, i2, i));
                    }
                }
                for (int i3 = 0; i3 < min; i3++) {
                    this.editSession.movePoint(i3, getPoint(motionEvent, i3));
                }
                this.model.moving(this.editSession.getSelected());
                invalidate();
                notifyDragMove(this.editSession.getSelected(), checkTrashIntersect(getPoint(motionEvent)));
                return true;
            }
            return super.onTouchEvent(motionEvent);
        }
        Matrix matrix = new Matrix();
        PointF point2 = getPoint(motionEvent);
        EditorElement findElementAtPoint = this.model.findElementAtPoint(point2, this.viewMatrix, matrix);
        this.moreThanOnePointerUsedInSession = false;
        this.model.pushUndoPoint();
        EditSession startEdit = startEdit(matrix, point2, findElementAtPoint);
        this.editSession = startEdit;
        if (startEdit != null) {
            checkTrashIntersect(point2);
            notifyDragStart(this.editSession.getSelected());
        }
        if (this.tapListener != null && allowTaps()) {
            EditSession editSession2 = this.editSession;
            if (editSession2 != null) {
                this.tapListener.onEntityDown(editSession2.getSelected());
            } else {
                this.tapListener.onEntityDown(null);
            }
        }
        return true;
    }

    private boolean checkTrashIntersect(PointF pointF) {
        Mode mode = this.mode;
        if (!(mode == Mode.Draw || mode == Mode.Blur)) {
            if (this.model.checkTrashIntersectsPoint(pointF)) {
                if (!(this.model.getTrash().getRenderer() instanceof TrashRenderer)) {
                    return true;
                }
                ((TrashRenderer) this.model.getTrash().getRenderer()).expand();
                return true;
            } else if (this.model.getTrash().getRenderer() instanceof TrashRenderer) {
                ((TrashRenderer) this.model.getTrash().getRenderer()).shrink();
            }
        }
        return false;
    }

    private void notifyDragStart(EditorElement editorElement) {
        DragListener dragListener = this.dragListener;
        if (dragListener != null) {
            dragListener.onDragStarted(editorElement);
        }
    }

    private void notifyDragMove(EditorElement editorElement, boolean z) {
        DragListener dragListener = this.dragListener;
        if (dragListener != null) {
            dragListener.onDragMoved(editorElement, z);
        }
    }

    private void notifyDragEnd(EditorElement editorElement, boolean z) {
        DragListener dragListener = this.dragListener;
        if (dragListener != null) {
            dragListener.onDragEnded(editorElement, z);
        }
    }

    private EditSession startEdit(Matrix matrix, PointF pointF, EditorElement editorElement) {
        Mode mode;
        EditSession startAMoveAndResizeSession = startAMoveAndResizeSession(matrix, pointF, editorElement);
        if (startAMoveAndResizeSession == null && ((mode = this.mode) == Mode.Draw || mode == Mode.Blur)) {
            return startADrawingSession(pointF);
        }
        setMode(Mode.MoveAndResize);
        return startAMoveAndResizeSession;
    }

    private EditSession startADrawingSession(PointF pointF) {
        BezierDrawingRenderer bezierDrawingRenderer = new BezierDrawingRenderer(this.color, this.thickness * Bounds.FULL_BOUNDS.width(), this.cap, this.model.findCropRelativeToRoot());
        EditorElement editorElement = new EditorElement(bezierDrawingRenderer, this.mode == Mode.Blur ? -1 : 0);
        this.model.addElementCentered(editorElement, 1.0f);
        return DrawingSession.start(editorElement, bezierDrawingRenderer, this.model.findElementInverseMatrix(editorElement, this.viewMatrix), pointF);
    }

    private EditSession startAMoveAndResizeSession(Matrix matrix, PointF pointF, EditorElement editorElement) {
        EditorElement findParent;
        Matrix findRelativeMatrix;
        Matrix findElementInverseMatrix;
        if (editorElement == null) {
            return null;
        }
        if (!(editorElement.getRenderer() instanceof ThumbRenderer)) {
            return ElementDragEditSession.startDrag(editorElement, matrix, pointF);
        }
        ThumbRenderer thumbRenderer = (ThumbRenderer) editorElement.getRenderer();
        EditorElement findById = getModel().findById(thumbRenderer.getElementToControl());
        if (findById == null || (findParent = getModel().getRoot().findParent(editorElement)) == null || (findRelativeMatrix = this.model.findRelativeMatrix(findParent, findById)) == null || (findElementInverseMatrix = this.model.findElementInverseMatrix(findById, this.viewMatrix)) == null) {
            return null;
        }
        return ThumbDragEditSession.startDrag(findById, findElementInverseMatrix, findRelativeMatrix, thumbRenderer.getControlPoint(), pointF);
    }

    public Mode getMode() {
        return this.mode;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public void setMainImageEditorMatrixRotation(float f, float f2) {
        this.model.setMainImageEditorMatrixRotation(f, f2);
        invalidate();
    }

    public void startDrawing(float f, Paint.Cap cap, boolean z) {
        this.thickness = f;
        this.cap = cap;
        setMode(z ? Mode.Blur : Mode.Draw);
    }

    public void setDrawingBrushColor(int i) {
        this.color = i;
    }

    private void dragDropRelease(boolean z) {
        this.model.dragDropRelease();
        DrawingChangedListener drawingChangedListener = this.drawingChangedListener;
        if (drawingChangedListener != null) {
            drawingChangedListener.onDrawingChanged(z);
        }
    }

    private static PointF getPoint(MotionEvent motionEvent) {
        return getPoint(motionEvent, 0);
    }

    private static PointF getPoint(MotionEvent motionEvent, int i) {
        return new PointF(motionEvent.getX(i), motionEvent.getY(i));
    }

    private static PointF getHistoricalPoint(MotionEvent motionEvent, int i, int i2) {
        return new PointF(motionEvent.getHistoricalX(i, i2), motionEvent.getHistoricalY(i, i2));
    }

    public EditorModel getModel() {
        return this.model;
    }

    public void setDrawingChangedListener(DrawingChangedListener drawingChangedListener) {
        this.drawingChangedListener = drawingChangedListener;
    }

    public void setSizeChangedListener(SizeChangedListener sizeChangedListener) {
        this.sizeChangedListener = sizeChangedListener;
    }

    public void setUndoRedoStackListener(UndoRedoStackListener undoRedoStackListener) {
        this.undoRedoStackListener = undoRedoStackListener;
    }

    public void setDragListener(DragListener dragListener) {
        this.dragListener = dragListener;
    }

    public void setTapListener(TapListener tapListener) {
        this.tapListener = tapListener;
    }

    public void deleteElement(EditorElement editorElement) {
        if (editorElement != null) {
            this.model.delete(editorElement);
            invalidate();
        }
    }

    public void onUndoRedoAvailabilityChanged(boolean z, boolean z2) {
        UndoRedoStackListener undoRedoStackListener = this.undoRedoStackListener;
        if (undoRedoStackListener != null) {
            undoRedoStackListener.onAvailabilityChanged(z, z2);
        }
    }

    /* loaded from: classes3.dex */
    public final class DoubleTapGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
        public boolean onDown(MotionEvent motionEvent) {
            return false;
        }

        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
        public void onLongPress(MotionEvent motionEvent) {
        }

        private DoubleTapGestureListener() {
            ImageEditorView.this = r1;
        }

        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnDoubleTapListener
        public boolean onDoubleTap(MotionEvent motionEvent) {
            if (ImageEditorView.this.tapListener == null || ImageEditorView.this.editSession == null || !ImageEditorView.this.allowTaps()) {
                return true;
            }
            ImageEditorView.this.tapListener.onEntityDoubleTap(ImageEditorView.this.editSession.getSelected());
            return true;
        }

        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
        public boolean onSingleTapUp(MotionEvent motionEvent) {
            if (ImageEditorView.this.tapListener == null || !ImageEditorView.this.allowTaps()) {
                return false;
            }
            if (ImageEditorView.this.editSession != null) {
                EditorElement selected = ImageEditorView.this.editSession.getSelected();
                ImageEditorView.this.model.indicateSelected(selected);
                ImageEditorView.this.model.setSelected(selected);
                ImageEditorView.this.tapListener.onEntitySingleTap(selected);
                return true;
            }
            ImageEditorView.this.tapListener.onEntitySingleTap(null);
            ImageEditorView.this.model.setSelected(null);
            return true;
        }
    }

    public boolean allowTaps() {
        Mode mode;
        return (this.model.isCropping() || (mode = this.mode) == Mode.Draw || mode == Mode.Blur) ? false : true;
    }
}
