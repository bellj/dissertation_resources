package org.signal.imageeditor.core;

import android.content.Context;
import android.graphics.Rect;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import androidx.appcompat.widget.AppCompatEditText;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import org.signal.imageeditor.core.model.EditorElement;
import org.signal.imageeditor.core.renderers.MultiLineTextRenderer;

/* loaded from: classes3.dex */
public final class HiddenEditText extends AppCompatEditText {
    private EditorElement currentTextEditorElement;
    private MultiLineTextRenderer currentTextEntity;
    private OnEditOrSelectionChange onEditOrSelectionChange;
    private Runnable onEndEdit;
    private List<TextFilter> textFilters = new LinkedList();

    /* loaded from: classes3.dex */
    public interface OnEditOrSelectionChange {
        void onChange(EditorElement editorElement, MultiLineTextRenderer multiLineTextRenderer);
    }

    /* loaded from: classes3.dex */
    public interface TextFilter {
        String filter(String str);
    }

    public HiddenEditText(Context context) {
        super(context);
        setAlpha(0.0f);
        setLayoutParams(new FrameLayout.LayoutParams(1, 1, 8388659));
        setClickable(false);
        setFocusable(true);
        setFocusableInTouchMode(true);
        setBackgroundColor(0);
        setTextSize(2, 1.0f);
        setInputType(131073);
        clearFocus();
    }

    @Override // android.widget.TextView
    protected void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        super.onTextChanged(charSequence, i, i2, i3);
        if (this.currentTextEntity != null) {
            String charSequence2 = charSequence.toString();
            for (TextFilter textFilter : this.textFilters) {
                charSequence2 = textFilter.filter(charSequence2);
            }
            this.currentTextEntity.setText(charSequence2);
            postEditOrSelectionChange();
        }
    }

    @Override // android.widget.TextView
    public void onEditorAction(int i) {
        MultiLineTextRenderer multiLineTextRenderer;
        super.onEditorAction(i);
        if (i == 6 && (multiLineTextRenderer = this.currentTextEntity) != null) {
            multiLineTextRenderer.setFocused(false);
            endEdit();
        }
    }

    @Override // android.widget.TextView, android.view.View
    protected void onFocusChanged(boolean z, int i, Rect rect) {
        super.onFocusChanged(z, i, rect);
        MultiLineTextRenderer multiLineTextRenderer = this.currentTextEntity;
        if (multiLineTextRenderer != null) {
            multiLineTextRenderer.setFocused(z);
            if (!z) {
                endEdit();
            }
        }
    }

    public void addTextFilter(TextFilter textFilter) {
        this.textFilters.add(textFilter);
    }

    public void addTextFilters(Collection<TextFilter> collection) {
        this.textFilters.addAll(collection);
    }

    private void endEdit() {
        Runnable runnable = this.onEndEdit;
        if (runnable != null) {
            runnable.run();
        }
    }

    private void postEditOrSelectionChange() {
        MultiLineTextRenderer multiLineTextRenderer;
        OnEditOrSelectionChange onEditOrSelectionChange;
        EditorElement editorElement = this.currentTextEditorElement;
        if (editorElement != null && (multiLineTextRenderer = this.currentTextEntity) != null && (onEditOrSelectionChange = this.onEditOrSelectionChange) != null) {
            onEditOrSelectionChange.onChange(editorElement, multiLineTextRenderer);
        }
    }

    public MultiLineTextRenderer getCurrentTextEntity() {
        return this.currentTextEntity;
    }

    public EditorElement getCurrentTextEditorElement() {
        return this.currentTextEditorElement;
    }

    public void setCurrentTextEditorElement(EditorElement editorElement) {
        if (editorElement == null || !(editorElement.getRenderer() instanceof MultiLineTextRenderer)) {
            this.currentTextEditorElement = null;
            setCurrentTextEntity(null);
        } else {
            this.currentTextEditorElement = editorElement;
            setCurrentTextEntity((MultiLineTextRenderer) editorElement.getRenderer());
        }
        postEditOrSelectionChange();
    }

    private void setCurrentTextEntity(MultiLineTextRenderer multiLineTextRenderer) {
        MultiLineTextRenderer multiLineTextRenderer2 = this.currentTextEntity;
        if (multiLineTextRenderer2 != multiLineTextRenderer) {
            if (multiLineTextRenderer2 != null) {
                multiLineTextRenderer2.setFocused(false);
            }
            this.currentTextEntity = multiLineTextRenderer;
            if (multiLineTextRenderer != null) {
                String text = multiLineTextRenderer.getText();
                setText(text);
                setSelection(text.length());
                return;
            }
            setText("");
        }
    }

    @Override // android.widget.TextView
    protected void onSelectionChanged(int i, int i2) {
        super.onSelectionChanged(i, i2);
        MultiLineTextRenderer multiLineTextRenderer = this.currentTextEntity;
        if (multiLineTextRenderer != null) {
            multiLineTextRenderer.setSelection(i, i2);
            postEditOrSelectionChange();
        }
    }

    @Override // android.view.View
    public boolean requestFocus(int i, Rect rect) {
        boolean requestFocus = super.requestFocus(i, rect);
        MultiLineTextRenderer multiLineTextRenderer = this.currentTextEntity;
        if (multiLineTextRenderer != null && requestFocus) {
            multiLineTextRenderer.setFocused(true);
            InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService("input_method");
            inputMethodManager.showSoftInput(this, 1);
            if (!inputMethodManager.isAcceptingText()) {
                inputMethodManager.toggleSoftInput(1, 1);
            }
        }
        return requestFocus;
    }

    public void hideKeyboard() {
        ((InputMethodManager) getContext().getSystemService("input_method")).hideSoftInputFromWindow(getWindowToken(), 1);
    }

    public void setIncognitoKeyboardEnabled(boolean z) {
        int i;
        if (z) {
            i = getImeOptions() | 16777216;
        } else {
            i = getImeOptions() & -16777217;
        }
        setImeOptions(i);
    }

    public void setOnEndEdit(Runnable runnable) {
        this.onEndEdit = runnable;
    }

    public void setOnEditOrSelectionChange(OnEditOrSelectionChange onEditOrSelectionChange) {
        this.onEditOrSelectionChange = onEditOrSelectionChange;
    }
}
