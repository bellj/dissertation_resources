package org.signal.imageeditor.core;

import android.os.Parcelable;

/* loaded from: classes3.dex */
public interface Renderer extends Parcelable {
    boolean hitTest(float f, float f2);

    void render(RendererContext rendererContext);
}
