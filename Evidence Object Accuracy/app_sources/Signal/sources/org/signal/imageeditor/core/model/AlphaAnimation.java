package org.signal.imageeditor.core.model;

import android.animation.ValueAnimator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

/* loaded from: classes3.dex */
public final class AlphaAnimation {
    static final AlphaAnimation NULL_1 = new AlphaAnimation(1.0f);
    private static final Interpolator interpolator = new LinearInterpolator();
    private float animatedFraction;
    private final boolean canAnimate;
    private final float from;
    private final Runnable invalidate;
    private final float to;

    private AlphaAnimation(float f, float f2, Runnable runnable) {
        this.from = f;
        this.to = f2;
        this.invalidate = runnable;
        this.canAnimate = runnable != null;
    }

    private AlphaAnimation(float f) {
        this(f, f, null);
    }

    public static AlphaAnimation animate(float f, float f2, Runnable runnable) {
        if (runnable == null) {
            return new AlphaAnimation(f2);
        }
        if (f == f2) {
            return new AlphaAnimation(f2);
        }
        AlphaAnimation alphaAnimation = new AlphaAnimation(f, f2, runnable);
        alphaAnimation.start();
        return alphaAnimation;
    }

    private void start() {
        if (this.canAnimate && this.invalidate != null) {
            ValueAnimator ofFloat = ValueAnimator.ofFloat(this.from, this.to);
            ofFloat.setDuration(200L);
            ofFloat.setInterpolator(interpolator);
            ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.signal.imageeditor.core.model.AlphaAnimation$$ExternalSyntheticLambda0
                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    AlphaAnimation.$r8$lambda$EnEUEzP1hP0S7meoqM5XeKdOuMM(AlphaAnimation.this, valueAnimator);
                }
            });
            ofFloat.start();
        }
    }

    public /* synthetic */ void lambda$start$0(ValueAnimator valueAnimator) {
        this.animatedFraction = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        this.invalidate.run();
    }

    public float getValue() {
        if (!this.canAnimate) {
            return this.to;
        }
        return this.animatedFraction;
    }
}
