package org.signal.imageeditor.core.model;

import android.graphics.Matrix;

/* loaded from: classes3.dex */
public final class Bisect {

    /* loaded from: classes3.dex */
    public interface ModifyElement {
        void applyFactor(Matrix matrix, float f);
    }

    /* loaded from: classes3.dex */
    public interface Predicate {
        boolean test();
    }

    public static boolean bisectToTest(EditorElement editorElement, float f, float f2, Predicate predicate, ModifyElement modifyElement, Runnable runnable) {
        Matrix bisectToTest = bisectToTest(editorElement, f, f2, predicate, modifyElement);
        if (bisectToTest == null) {
            return false;
        }
        editorElement.animateLocalTo(bisectToTest, runnable);
        return true;
    }

    public static Matrix bisectToTest(EditorElement editorElement, float f, float f2, Predicate predicate, ModifyElement modifyElement) {
        Matrix localMatrix = editorElement.getLocalMatrix();
        Matrix matrix = new Matrix(localMatrix);
        Matrix matrix2 = new Matrix();
        float f3 = f2;
        int i = 0;
        boolean z = false;
        float f4 = 0.0f;
        do {
            i++;
            modifyElement.applyFactor(localMatrix, f2);
            try {
                if (predicate.test()) {
                    if (z && Math.abs(f2 - f) >= Math.abs(f4 - f)) {
                        f3 = f2;
                    }
                    matrix2.set(localMatrix);
                    f3 = f2;
                    f4 = f3;
                    z = true;
                } else if (i == 1) {
                    return null;
                } else {
                    f = f2;
                }
                localMatrix.set(matrix);
                f2 = (f3 + f) / 2.0f;
                if (i >= 16) {
                    break;
                }
            } finally {
                localMatrix.set(matrix);
            }
        } while (Math.abs(f3 - f) > 0.001f);
        if (z) {
            return matrix2;
        }
        return null;
    }
}
