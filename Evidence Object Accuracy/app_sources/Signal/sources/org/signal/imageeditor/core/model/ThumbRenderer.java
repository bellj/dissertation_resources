package org.signal.imageeditor.core.model;

import java.util.UUID;
import org.signal.imageeditor.core.Renderer;

/* loaded from: classes3.dex */
public interface ThumbRenderer extends Renderer {
    ControlPoint getControlPoint();

    UUID getElementToControl();

    /* loaded from: classes3.dex */
    public enum ControlPoint {
        CENTER_LEFT(-1000.0f, 0.0f),
        CENTER_RIGHT(1000.0f, 0.0f),
        TOP_CENTER(0.0f, -1000.0f),
        BOTTOM_CENTER(0.0f, 1000.0f),
        TOP_LEFT(-1000.0f, -1000.0f),
        TOP_RIGHT(1000.0f, -1000.0f),
        BOTTOM_LEFT(-1000.0f, 1000.0f),
        BOTTOM_RIGHT(1000.0f, 1000.0f),
        SCALE_ROT_LEFT(-1000.0f, 0.0f),
        SCALE_ROT_RIGHT(1000.0f, 0.0f),
        ORIGIN(0.0f, 0.0f);
        
        private final float x;
        private final float y;

        ControlPoint(float f, float f2) {
            this.x = f;
            this.y = f2;
        }

        public float getX() {
            return this.x;
        }

        public float getY() {
            return this.y;
        }

        public ControlPoint opposite() {
            switch (AnonymousClass1.$SwitchMap$org$signal$imageeditor$core$model$ThumbRenderer$ControlPoint[ordinal()]) {
                case 1:
                    return CENTER_RIGHT;
                case 2:
                    return CENTER_LEFT;
                case 3:
                    return BOTTOM_CENTER;
                case 4:
                    return TOP_CENTER;
                case 5:
                    return BOTTOM_RIGHT;
                case 6:
                    return BOTTOM_LEFT;
                case 7:
                    return TOP_RIGHT;
                case 8:
                    return TOP_LEFT;
                case 9:
                case 10:
                    return ORIGIN;
                default:
                    throw new RuntimeException();
            }
        }

        public boolean isHorizontalCenter() {
            return this == CENTER_LEFT || this == CENTER_RIGHT;
        }

        public boolean isVerticalCenter() {
            return this == TOP_CENTER || this == BOTTOM_CENTER;
        }

        public boolean isCenter() {
            return isHorizontalCenter() || isVerticalCenter();
        }

        public boolean isScaleAndRotateThumb() {
            return this == SCALE_ROT_LEFT || this == SCALE_ROT_RIGHT;
        }
    }

    /* renamed from: org.signal.imageeditor.core.model.ThumbRenderer$1 */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$signal$imageeditor$core$model$ThumbRenderer$ControlPoint;

        static {
            int[] iArr = new int[ControlPoint.values().length];
            $SwitchMap$org$signal$imageeditor$core$model$ThumbRenderer$ControlPoint = iArr;
            try {
                iArr[ControlPoint.CENTER_LEFT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$signal$imageeditor$core$model$ThumbRenderer$ControlPoint[ControlPoint.CENTER_RIGHT.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$signal$imageeditor$core$model$ThumbRenderer$ControlPoint[ControlPoint.TOP_CENTER.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$signal$imageeditor$core$model$ThumbRenderer$ControlPoint[ControlPoint.BOTTOM_CENTER.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$signal$imageeditor$core$model$ThumbRenderer$ControlPoint[ControlPoint.TOP_LEFT.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$signal$imageeditor$core$model$ThumbRenderer$ControlPoint[ControlPoint.TOP_RIGHT.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$signal$imageeditor$core$model$ThumbRenderer$ControlPoint[ControlPoint.BOTTOM_LEFT.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$signal$imageeditor$core$model$ThumbRenderer$ControlPoint[ControlPoint.BOTTOM_RIGHT.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$signal$imageeditor$core$model$ThumbRenderer$ControlPoint[ControlPoint.SCALE_ROT_LEFT.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$signal$imageeditor$core$model$ThumbRenderer$ControlPoint[ControlPoint.SCALE_ROT_RIGHT.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
        }
    }
}
