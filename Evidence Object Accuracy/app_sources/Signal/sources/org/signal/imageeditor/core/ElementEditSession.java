package org.signal.imageeditor.core;

import android.graphics.Matrix;
import android.graphics.PointF;
import org.signal.imageeditor.core.model.EditorElement;

/* access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public abstract class ElementEditSession implements EditSession {
    final PointF[] endPointElement = newTwoPointArray();
    final PointF[] endPointScreen = newTwoPointArray();
    private final Matrix inverseMatrix;
    final EditorElement selected;
    final PointF[] startPointElement = newTwoPointArray();
    final PointF[] startPointScreen = newTwoPointArray();

    public ElementEditSession(EditorElement editorElement, Matrix matrix) {
        this.selected = editorElement;
        this.inverseMatrix = matrix;
    }

    public void setScreenStartPoint(int i, PointF pointF) {
        this.startPointScreen[i] = pointF;
        mapPoint(this.startPointElement[i], this.inverseMatrix, pointF);
    }

    public void setScreenEndPoint(int i, PointF pointF) {
        this.endPointScreen[i] = pointF;
        mapPoint(this.endPointElement[i], this.inverseMatrix, pointF);
    }

    @Override // org.signal.imageeditor.core.EditSession
    public void commit() {
        this.selected.commitEditorMatrix();
    }

    @Override // org.signal.imageeditor.core.EditSession
    public EditorElement getSelected() {
        return this.selected;
    }

    private static PointF[] newTwoPointArray() {
        PointF[] pointFArr = new PointF[2];
        for (int i = 0; i < 2; i++) {
            pointFArr[i] = new PointF();
        }
        return pointFArr;
    }

    private static void mapPoint(PointF pointF, Matrix matrix, PointF pointF2) {
        float[] fArr = {pointF2.x, pointF2.y};
        float[] fArr2 = new float[2];
        matrix.mapPoints(fArr2, fArr);
        pointF.set(fArr2[0], fArr2[1]);
    }
}
