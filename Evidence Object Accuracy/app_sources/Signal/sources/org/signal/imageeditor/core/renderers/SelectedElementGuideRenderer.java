package org.signal.imageeditor.core.renderers;

import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Parcel;
import android.os.Parcelable;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.DimensionUnit;
import org.signal.imageeditor.core.Renderer;
import org.signal.imageeditor.core.RendererContext;
import org.thoughtcrime.securesms.MediaPreviewActivity;

/* compiled from: SelectedElementGuideRenderer.kt */
@Metadata(d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0014\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u001c2\u00020\u0001:\u0001\u001cB\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\r\u001a\u00020\u000eH\u0016J\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\t2\u0006\u0010\u0012\u001a\u00020\tH\u0016J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u0010\u0010\u0017\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u0018\u0010\u0018\u001a\u00020\u00142\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u000eH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u001d"}, d2 = {"Lorg/signal/imageeditor/core/renderers/SelectedElementGuideRenderer;", "Lorg/signal/imageeditor/core/Renderer;", "()V", "allPointsInLocalCords", "", "allPointsOnScreen", "circlePaint", "Landroid/graphics/Paint;", "circleRadius", "", "guidePaint", "path", "Landroid/graphics/Path;", "describeContents", "", "hitTest", "", "x", "y", "performRender", "", "rendererContext", "Lorg/signal/imageeditor/core/RendererContext;", "render", "writeToParcel", "parcel", "Landroid/os/Parcel;", "flags", "CREATOR", "image-editor_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class SelectedElementGuideRenderer implements Renderer {
    public static final CREATOR CREATOR = new CREATOR(null);
    private final float[] allPointsInLocalCords = {-1000.0f, -1000.0f, 1000.0f, -1000.0f, 1000.0f, 1000.0f, -1000.0f, 1000.0f};
    private final float[] allPointsOnScreen = new float[8];
    private final Paint circlePaint;
    private final float circleRadius;
    private final Paint guidePaint;
    private final Path path;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // org.signal.imageeditor.core.Renderer
    public boolean hitTest(float f, float f2) {
        return false;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkNotNullParameter(parcel, "parcel");
    }

    public SelectedElementGuideRenderer() {
        DimensionUnit dimensionUnit = DimensionUnit.DP;
        this.circleRadius = dimensionUnit.toPixels(5.0f);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStrokeWidth(dimensionUnit.toPixels(1.5f));
        paint.setColor(-1);
        paint.setStyle(Paint.Style.STROKE);
        paint.setPathEffect(new DashPathEffect(new float[]{15.0f, 15.0f}, 0.0f));
        this.guidePaint = paint;
        Paint paint2 = new Paint();
        paint2.setAntiAlias(true);
        paint2.setColor(-1);
        paint2.setStyle(Paint.Style.FILL);
        this.circlePaint = paint2;
        this.path = new Path();
    }

    @Override // org.signal.imageeditor.core.Renderer
    public void render(RendererContext rendererContext) {
        Intrinsics.checkNotNullParameter(rendererContext, "rendererContext");
        rendererContext.canvasMatrix.mapPoints(this.allPointsOnScreen, this.allPointsInLocalCords);
        performRender(rendererContext);
    }

    private final void performRender(RendererContext rendererContext) {
        rendererContext.save();
        rendererContext.canvasMatrix.setToIdentity();
        this.path.reset();
        Path path = this.path;
        float[] fArr = this.allPointsOnScreen;
        path.moveTo(fArr[0], fArr[1]);
        Path path2 = this.path;
        float[] fArr2 = this.allPointsOnScreen;
        path2.lineTo(fArr2[2], fArr2[3]);
        Path path3 = this.path;
        float[] fArr3 = this.allPointsOnScreen;
        path3.lineTo(fArr3[4], fArr3[5]);
        Path path4 = this.path;
        float[] fArr4 = this.allPointsOnScreen;
        path4.lineTo(fArr4[6], fArr4[7]);
        this.path.close();
        rendererContext.canvas.drawPath(this.path, this.guidePaint);
        Canvas canvas = rendererContext.canvas;
        float[] fArr5 = this.allPointsOnScreen;
        canvas.drawCircle((fArr5[6] + fArr5[0]) / 2.0f, (fArr5[7] + fArr5[1]) / 2.0f, this.circleRadius, this.circlePaint);
        Canvas canvas2 = rendererContext.canvas;
        float[] fArr6 = this.allPointsOnScreen;
        canvas2.drawCircle((fArr6[4] + fArr6[2]) / 2.0f, (fArr6[5] + fArr6[3]) / 2.0f, this.circleRadius, this.circlePaint);
        rendererContext.restore();
    }

    /* compiled from: SelectedElementGuideRenderer.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u001d\u0010\u0007\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016¢\u0006\u0002\u0010\u000b¨\u0006\f"}, d2 = {"Lorg/signal/imageeditor/core/renderers/SelectedElementGuideRenderer$CREATOR;", "Landroid/os/Parcelable$Creator;", "Lorg/signal/imageeditor/core/renderers/SelectedElementGuideRenderer;", "()V", "createFromParcel", "parcel", "Landroid/os/Parcel;", "newArray", "", MediaPreviewActivity.SIZE_EXTRA, "", "(I)[Lorg/signal/imageeditor/core/renderers/SelectedElementGuideRenderer;", "image-editor_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class CREATOR implements Parcelable.Creator<SelectedElementGuideRenderer> {
        public /* synthetic */ CREATOR(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private CREATOR() {
        }

        @Override // android.os.Parcelable.Creator
        public SelectedElementGuideRenderer createFromParcel(Parcel parcel) {
            Intrinsics.checkNotNullParameter(parcel, "parcel");
            return new SelectedElementGuideRenderer();
        }

        @Override // android.os.Parcelable.Creator
        public SelectedElementGuideRenderer[] newArray(int i) {
            return new SelectedElementGuideRenderer[i];
        }
    }
}
