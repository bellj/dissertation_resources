package org.signal.imageeditor.core.renderers;

import java.lang.ref.WeakReference;
import org.signal.imageeditor.core.Renderer;
import org.signal.imageeditor.core.RendererContext;

/* loaded from: classes3.dex */
public abstract class InvalidateableRenderer implements Renderer {
    private WeakReference<RendererContext.Invalidate> invalidate = new WeakReference<>(null);

    @Override // org.signal.imageeditor.core.Renderer
    public void render(RendererContext rendererContext) {
        setInvalidate(rendererContext.invalidate);
    }

    private void setInvalidate(RendererContext.Invalidate invalidate) {
        if (invalidate != this.invalidate.get()) {
            this.invalidate = new WeakReference<>(invalidate);
        }
    }

    public void invalidate() {
        RendererContext.Invalidate invalidate = this.invalidate.get();
        if (invalidate != null) {
            invalidate.onInvalidate(this);
        }
    }
}
