package org.signal.imageeditor.core;

import android.graphics.Matrix;
import android.graphics.Point;
import org.signal.imageeditor.core.RendererContext;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class RendererContext$Ready$$ExternalSyntheticLambda0 implements RendererContext.Ready {
    @Override // org.signal.imageeditor.core.RendererContext.Ready
    public final void onReady(Renderer renderer, Matrix matrix, Point point) {
        RendererContext.Ready.CC.lambda$static$0(renderer, matrix, point);
    }
}
