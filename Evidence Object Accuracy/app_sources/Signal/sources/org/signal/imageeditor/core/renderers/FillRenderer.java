package org.signal.imageeditor.core.renderers;

import android.graphics.Path;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import org.signal.core.util.DimensionUnit;
import org.signal.imageeditor.core.Bounds;
import org.signal.imageeditor.core.Renderer;
import org.signal.imageeditor.core.RendererContext;

/* loaded from: classes3.dex */
public final class FillRenderer implements Renderer {
    public static final Parcelable.Creator<FillRenderer> CREATOR = new Parcelable.Creator<FillRenderer>() { // from class: org.signal.imageeditor.core.renderers.FillRenderer.1
        @Override // android.os.Parcelable.Creator
        public FillRenderer createFromParcel(Parcel parcel) {
            return new FillRenderer(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public FillRenderer[] newArray(int i) {
            return new FillRenderer[i];
        }
    };
    private final int color;
    private final RectF dst;
    private final Path path;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // org.signal.imageeditor.core.Renderer
    public void render(RendererContext rendererContext) {
        rendererContext.canvas.save();
        rendererContext.mapRect(this.dst, Bounds.FULL_BOUNDS);
        rendererContext.canvasMatrix.setToIdentity();
        this.path.reset();
        Path path = this.path;
        RectF rectF = this.dst;
        DimensionUnit dimensionUnit = DimensionUnit.DP;
        path.addRoundRect(rectF, dimensionUnit.toPixels(18.0f), dimensionUnit.toPixels(18.0f), Path.Direction.CW);
        rendererContext.canvas.clipPath(this.path);
        rendererContext.canvas.drawColor(this.color);
        rendererContext.canvas.restore();
    }

    public FillRenderer(int i) {
        this.dst = new RectF();
        this.path = new Path();
        this.color = i;
    }

    private FillRenderer(Parcel parcel) {
        this(parcel.readInt());
    }

    @Override // org.signal.imageeditor.core.Renderer
    public boolean hitTest(float f, float f2) {
        return !Bounds.contains(f, f2);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.color);
    }
}
