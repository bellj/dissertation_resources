package org.signal.imageeditor.core.renderers;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import org.signal.imageeditor.core.ColorableRenderer;
import org.signal.imageeditor.core.RendererContext;

/* loaded from: classes3.dex */
public final class BezierDrawingRenderer extends InvalidateableRenderer implements ColorableRenderer {
    public static final Parcelable.Creator<BezierDrawingRenderer> CREATOR = new Parcelable.Creator<BezierDrawingRenderer>() { // from class: org.signal.imageeditor.core.renderers.BezierDrawingRenderer.1
        @Override // android.os.Parcelable.Creator
        public BezierDrawingRenderer createFromParcel(Parcel parcel) {
            return new BezierDrawingRenderer(parcel.readInt(), parcel.readFloat(), Paint.Cap.values()[parcel.readInt()], (AutomaticControlPointBezierLine) parcel.readParcelable(AutomaticControlPointBezierLine.class.getClassLoader()), (RectF) parcel.readParcelable(RectF.class.getClassLoader()));
        }

        @Override // android.os.Parcelable.Creator
        public BezierDrawingRenderer[] newArray(int i) {
            return new BezierDrawingRenderer[i];
        }
    };
    private final AutomaticControlPointBezierLine bezierLine;
    private final Paint.Cap cap;
    private final RectF clipRect;
    private int color;
    private final Paint paint;
    private float thickness;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // org.signal.imageeditor.core.Renderer
    public boolean hitTest(float f, float f2) {
        return false;
    }

    private BezierDrawingRenderer(int i, float f, Paint.Cap cap, AutomaticControlPointBezierLine automaticControlPointBezierLine, RectF rectF) {
        this.paint = new Paint();
        this.color = i;
        this.thickness = f;
        this.cap = cap;
        this.clipRect = rectF;
        this.bezierLine = automaticControlPointBezierLine == null ? new AutomaticControlPointBezierLine() : automaticControlPointBezierLine;
        updatePaint();
    }

    public BezierDrawingRenderer(int i, float f, Paint.Cap cap, RectF rectF) {
        this(i, f, cap, null, rectF != null ? new RectF(rectF) : null);
    }

    @Override // org.signal.imageeditor.core.ColorableRenderer
    public int getColor() {
        return this.color;
    }

    @Override // org.signal.imageeditor.core.ColorableRenderer
    public void setColor(int i) {
        if (this.color != i) {
            this.color = i;
            updatePaint();
            invalidate();
        }
    }

    private void updatePaint() {
        this.paint.setColor(this.color);
        this.paint.setStrokeWidth(this.thickness);
        this.paint.setStyle(Paint.Style.STROKE);
        this.paint.setAntiAlias(true);
        this.paint.setStrokeCap(this.cap);
    }

    public void setFirstPoint(PointF pointF) {
        this.bezierLine.reset();
        this.bezierLine.addPoint(pointF.x, pointF.y);
        invalidate();
    }

    public void addNewPoint(PointF pointF) {
        if (this.cap != Paint.Cap.ROUND) {
            this.bezierLine.addPointFiltered(pointF.x, pointF.y, this.thickness * 0.5f);
        } else {
            this.bezierLine.addPoint(pointF.x, pointF.y);
        }
        invalidate();
    }

    @Override // org.signal.imageeditor.core.renderers.InvalidateableRenderer, org.signal.imageeditor.core.Renderer
    public void render(RendererContext rendererContext) {
        super.render(rendererContext);
        Canvas canvas = rendererContext.canvas;
        canvas.save();
        RectF rectF = this.clipRect;
        if (rectF != null) {
            canvas.clipRect(rectF);
        }
        int alpha = this.paint.getAlpha();
        this.paint.setAlpha(rendererContext.getAlpha(alpha));
        this.paint.setXfermode(rendererContext.getMaskPaint() != null ? rendererContext.getMaskPaint().getXfermode() : null);
        this.bezierLine.draw(canvas, this.paint);
        this.paint.setAlpha(alpha);
        rendererContext.canvas.restore();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.color);
        parcel.writeFloat(this.thickness);
        parcel.writeInt(this.cap.ordinal());
        parcel.writeParcelable(this.bezierLine, i);
        parcel.writeParcelable(this.clipRect, i);
    }
}
