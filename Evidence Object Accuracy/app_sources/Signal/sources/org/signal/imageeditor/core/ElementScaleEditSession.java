package org.signal.imageeditor.core;

import android.graphics.Matrix;
import android.graphics.PointF;
import org.signal.imageeditor.core.model.EditorElement;

/* loaded from: classes3.dex */
final class ElementScaleEditSession extends ElementEditSession {
    @Override // org.signal.imageeditor.core.EditSession
    public EditSession newPoint(Matrix matrix, PointF pointF, int i) {
        return this;
    }

    private ElementScaleEditSession(EditorElement editorElement, Matrix matrix) {
        super(editorElement, matrix);
    }

    public static ElementScaleEditSession startScale(ElementDragEditSession elementDragEditSession, Matrix matrix, PointF pointF, int i) {
        elementDragEditSession.commit();
        ElementScaleEditSession elementScaleEditSession = new ElementScaleEditSession(elementDragEditSession.selected, matrix);
        int i2 = 1 - i;
        elementScaleEditSession.setScreenStartPoint(i2, elementDragEditSession.endPointScreen[0]);
        elementScaleEditSession.setScreenEndPoint(i2, elementDragEditSession.endPointScreen[0]);
        elementScaleEditSession.setScreenStartPoint(i, pointF);
        elementScaleEditSession.setScreenEndPoint(i, pointF);
        return elementScaleEditSession;
    }

    @Override // org.signal.imageeditor.core.EditSession
    public void movePoint(int i, PointF pointF) {
        setScreenEndPoint(i, pointF);
        Matrix editorMatrix = this.selected.getEditorMatrix();
        editorMatrix.reset();
        if (this.selected.getFlags().isAspectLocked()) {
            float findScale = (float) findScale(this.startPointElement, this.endPointElement);
            PointF pointF2 = this.startPointElement[0];
            editorMatrix.postTranslate(-pointF2.x, -pointF2.y);
            editorMatrix.postScale(findScale, findScale);
            PointF[] pointFArr = this.endPointElement;
            double angle = angle(pointFArr[0], pointFArr[1]);
            PointF[] pointFArr2 = this.startPointElement;
            double angle2 = angle - angle(pointFArr2[0], pointFArr2[1]);
            if (!this.selected.getFlags().isRotateLocked()) {
                editorMatrix.postRotate((float) Math.toDegrees(angle2));
            }
            PointF pointF3 = this.endPointElement[0];
            editorMatrix.postTranslate(pointF3.x, pointF3.y);
            return;
        }
        PointF pointF4 = this.startPointElement[0];
        editorMatrix.postTranslate(-pointF4.x, -pointF4.y);
        PointF[] pointFArr3 = this.endPointElement;
        PointF pointF5 = pointFArr3[1];
        float f = pointF5.x;
        PointF pointF6 = pointFArr3[0];
        float f2 = f - pointF6.x;
        PointF[] pointFArr4 = this.startPointElement;
        PointF pointF7 = pointFArr4[1];
        float f3 = pointF7.x;
        PointF pointF8 = pointFArr4[0];
        editorMatrix.postScale(f2 / (f3 - pointF8.x), (pointF5.y - pointF6.y) / (pointF7.y - pointF8.y));
        PointF pointF9 = this.endPointElement[0];
        editorMatrix.postTranslate(pointF9.x, pointF9.y);
    }

    @Override // org.signal.imageeditor.core.EditSession
    public EditSession removePoint(Matrix matrix, int i) {
        return convertToDrag(i, matrix);
    }

    private static double angle(PointF pointF, PointF pointF2) {
        return Math.atan2((double) (pointF.y - pointF2.y), (double) (pointF.x - pointF2.x));
    }

    private ElementDragEditSession convertToDrag(int i, Matrix matrix) {
        return ElementDragEditSession.startDrag(this.selected, matrix, this.endPointScreen[1 - i]);
    }

    private static double findScale(PointF[] pointFArr, PointF[] pointFArr2) {
        return Math.sqrt((double) (getDistanceSquared(pointFArr2[0], pointFArr2[1]) / getDistanceSquared(pointFArr[0], pointFArr[1])));
    }

    private static float getDistanceSquared(PointF pointF, PointF pointF2) {
        float f = pointF.x - pointF2.x;
        float f2 = pointF.y - pointF2.y;
        return (f * f) + (f2 * f2);
    }
}
