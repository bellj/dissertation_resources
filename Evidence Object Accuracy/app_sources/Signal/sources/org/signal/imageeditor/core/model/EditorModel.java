package org.signal.imageeditor.core.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.signal.imageeditor.core.Bounds;
import org.signal.imageeditor.core.Renderer;
import org.signal.imageeditor.core.RendererContext;
import org.signal.imageeditor.core.UndoRedoStackListener;
import org.signal.imageeditor.core.model.Bisect;
import org.signal.imageeditor.core.model.EditorElement;
import org.signal.imageeditor.core.renderers.FaceBlurRenderer;
import org.signal.imageeditor.core.renderers.MultiLineTextRenderer;

/* loaded from: classes3.dex */
public final class EditorModel implements Parcelable, RendererContext.Ready {
    public static final Parcelable.Creator<EditorModel> CREATOR = new Parcelable.Creator<EditorModel>() { // from class: org.signal.imageeditor.core.model.EditorModel.1
        @Override // android.os.Parcelable.Creator
        public EditorModel createFromParcel(Parcel parcel) {
            return new EditorModel(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public EditorModel[] newArray(int i) {
            return new EditorModel[i];
        }
    };
    private static final Point MINIMUM_RATIO = new Point(15, 1);
    private static final Runnable NULL_RUNNABLE = new Runnable() { // from class: org.signal.imageeditor.core.model.EditorModel$$ExternalSyntheticLambda5
        @Override // java.lang.Runnable
        public final void run() {
            EditorModel.lambda$static$0();
        }
    };
    private final UndoRedoStacks cropUndoRedoStacks;
    private final EditingPurpose editingPurpose;
    private EditorElementHierarchy editorElementHierarchy;
    private float fixedRatio;
    private final InBoundsMemory inBoundsMemory;
    private Runnable invalidate;
    private final Point size;
    private UndoRedoStackListener undoRedoStackListener;
    private final UndoRedoStacks undoRedoStacks;
    private final RectF visibleViewPort;

    /* loaded from: classes3.dex */
    public enum EditingPurpose {
        IMAGE,
        AVATAR_CAPTURE,
        AVATAR_EDIT,
        WALLPAPER
    }

    public static /* synthetic */ void lambda$static$0() {
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public void setSelected(EditorElement editorElement) {
        if (editorElement == null) {
            this.editorElementHierarchy.removeAllSelectionArtifacts();
            return;
        }
        this.editorElementHierarchy.setOrUpdateSelectionThumbsForElement(editorElement, findRelativeMatrix(editorElement, this.editorElementHierarchy.getOverlay()));
    }

    public void updateSelectionThumbsIfSelected(EditorElement editorElement) {
        this.editorElementHierarchy.updateSelectionThumbsForElement(editorElement, findRelativeMatrix(editorElement, this.editorElementHierarchy.getOverlay()));
    }

    public void setSelectionVisible(boolean z) {
        this.editorElementHierarchy.getSelection().getFlags().setVisible(z).setChildrenVisible(z).persist();
    }

    private EditorModel(Parcel parcel) {
        this.invalidate = NULL_RUNNABLE;
        this.inBoundsMemory = new InBoundsMemory();
        this.visibleViewPort = new RectF();
        ClassLoader classLoader = EditorModel.class.getClassLoader();
        this.editingPurpose = EditingPurpose.values()[parcel.readInt()];
        this.fixedRatio = parcel.readFloat();
        this.size = new Point(parcel.readInt(), parcel.readInt());
        this.editorElementHierarchy = EditorElementHierarchy.create((EditorElement) parcel.readParcelable(classLoader));
        this.undoRedoStacks = (UndoRedoStacks) parcel.readParcelable(classLoader);
        this.cropUndoRedoStacks = (UndoRedoStacks) parcel.readParcelable(classLoader);
    }

    public EditorModel(EditingPurpose editingPurpose, float f, EditorElementHierarchy editorElementHierarchy) {
        this.invalidate = NULL_RUNNABLE;
        this.inBoundsMemory = new InBoundsMemory();
        this.visibleViewPort = new RectF();
        this.editingPurpose = editingPurpose;
        this.fixedRatio = f;
        this.size = new Point(1024, 1024);
        this.editorElementHierarchy = editorElementHierarchy;
        this.undoRedoStacks = new UndoRedoStacks(50);
        this.cropUndoRedoStacks = new UndoRedoStacks(50);
    }

    public static EditorModel create(int i) {
        EditorModel editorModel = new EditorModel(EditingPurpose.IMAGE, 0.0f, EditorElementHierarchy.create(i));
        editorModel.setCropAspectLock(false);
        return editorModel;
    }

    public static EditorModel createForAvatarCapture(int i) {
        EditorModel editorModel = new EditorModel(EditingPurpose.AVATAR_CAPTURE, 1.0f, EditorElementHierarchy.createForCircleEditing(i));
        editorModel.setCropAspectLock(true);
        return editorModel;
    }

    public static EditorModel createForAvatarEdit(int i) {
        EditorModel editorModel = new EditorModel(EditingPurpose.AVATAR_EDIT, 1.0f, EditorElementHierarchy.createForCircleEditing(i));
        editorModel.setCropAspectLock(true);
        return editorModel;
    }

    public static EditorModel createForWallpaperEditing(float f, int i) {
        EditorModel editorModel = new EditorModel(EditingPurpose.WALLPAPER, f, EditorElementHierarchy.createForPinchAndPanCropping(i));
        editorModel.setCropAspectLock(true);
        return editorModel;
    }

    public void setInvalidate(Runnable runnable) {
        if (runnable == null) {
            runnable = NULL_RUNNABLE;
        }
        this.invalidate = runnable;
    }

    public void setUndoRedoStackListener(UndoRedoStackListener undoRedoStackListener) {
        this.undoRedoStackListener = undoRedoStackListener;
        updateUndoRedoAvailableState(getActiveUndoRedoStacks(isCropping()));
    }

    public void setMainImageEditorMatrixRotation(float f, float f2) {
        setEditorMatrixToRotationMatrixAboutParentsOrigin(this.editorElementHierarchy.getMainImage(), f);
        scaleMainImageEditorMatrixToFitInsideCropBounds(f2, 2.0f);
    }

    private void scaleMainImageEditorMatrixToFitInsideCropBounds(float f, float f2) {
        EditorElement mainImage = this.editorElementHierarchy.getMainImage();
        Matrix matrix = new Matrix(mainImage.getLocalMatrix());
        Matrix matrix2 = new Matrix(mainImage.getEditorMatrix());
        mainImage.commitEditorMatrix();
        Matrix matrix3 = new Matrix(mainImage.getLocalMatrix());
        Matrix bisectToTest = Bisect.bisectToTest(mainImage, f, f2, new EditorModel$$ExternalSyntheticLambda0(this), new Bisect.ModifyElement() { // from class: org.signal.imageeditor.core.model.EditorModel$$ExternalSyntheticLambda8
            @Override // org.signal.imageeditor.core.model.Bisect.ModifyElement
            public final void applyFactor(Matrix matrix4, float f3) {
                matrix4.preScale(f3, f3);
            }
        });
        Matrix matrix4 = new Matrix();
        if (bisectToTest != null && matrix3.invert(matrix4)) {
            matrix4.preConcat(bisectToTest);
            matrix2.preConcat(matrix4);
        }
        mainImage.getLocalMatrix().set(matrix);
        mainImage.getEditorMatrix().set(matrix2);
    }

    private void setEditorMatrixToRotationMatrixAboutParentsOrigin(EditorElement editorElement, float f) {
        Matrix localMatrix = editorElement.getLocalMatrix();
        Matrix editorMatrix = editorElement.getEditorMatrix();
        localMatrix.invert(editorMatrix);
        editorMatrix.preRotate(f);
        editorMatrix.preConcat(localMatrix);
    }

    public void draw(RendererContext rendererContext, EditorElement editorElement) {
        EditorElement root = this.editorElementHierarchy.getRoot();
        if (editorElement != null) {
            root.forAllInTree(new EditorElement.PerElementFunction() { // from class: org.signal.imageeditor.core.model.EditorModel$$ExternalSyntheticLambda2
                @Override // org.signal.imageeditor.core.model.EditorElement.PerElementFunction
                public final void apply(EditorElement editorElement2) {
                    EditorModel.lambda$draw$2(editorElement2);
                }
            });
            editorElement.getFlags().setVisible(false);
        }
        root.draw(rendererContext);
        if (editorElement != null) {
            try {
                root.forAllInTree(new EditorElement.PerElementFunction() { // from class: org.signal.imageeditor.core.model.EditorModel$$ExternalSyntheticLambda3
                    @Override // org.signal.imageeditor.core.model.EditorElement.PerElementFunction
                    public final void apply(EditorElement editorElement2) {
                        EditorModel.lambda$draw$3(EditorElement.this, editorElement2);
                    }
                });
                root.draw(rendererContext);
            } finally {
                root.forAllInTree(new EditorElement.PerElementFunction() { // from class: org.signal.imageeditor.core.model.EditorModel$$ExternalSyntheticLambda4
                    @Override // org.signal.imageeditor.core.model.EditorElement.PerElementFunction
                    public final void apply(EditorElement editorElement2) {
                        EditorModel.lambda$draw$4(editorElement2);
                    }
                });
            }
        }
    }

    public static /* synthetic */ void lambda$draw$2(EditorElement editorElement) {
        editorElement.getFlags().mark();
    }

    public static /* synthetic */ void lambda$draw$3(EditorElement editorElement, EditorElement editorElement2) {
        editorElement2.getFlags().setVisible(editorElement == editorElement2);
    }

    public static /* synthetic */ void lambda$draw$4(EditorElement editorElement) {
        editorElement.getFlags().restore();
    }

    public Matrix findElementInverseMatrix(EditorElement editorElement, Matrix matrix) {
        Matrix matrix2 = new Matrix();
        if (findElement(editorElement, matrix, matrix2)) {
            return matrix2;
        }
        return null;
    }

    private Matrix findElementMatrix(EditorElement editorElement, Matrix matrix) {
        Matrix findElementInverseMatrix = findElementInverseMatrix(editorElement, matrix);
        if (findElementInverseMatrix == null) {
            return null;
        }
        Matrix matrix2 = new Matrix();
        findElementInverseMatrix.invert(matrix2);
        return matrix2;
    }

    public EditorElement findElementAtPoint(PointF pointF, Matrix matrix, Matrix matrix2) {
        return this.editorElementHierarchy.getRoot().findElementAt(pointF.x, pointF.y, matrix, matrix2);
    }

    public boolean checkTrashIntersectsPoint(PointF pointF) {
        EditorElement trash = this.editorElementHierarchy.getTrash();
        if (!trash.getFlags().isVisible()) {
            return false;
        }
        boolean z = true;
        trash.getFlags().setSelectable(true).persist();
        if (trash.findElementAt(pointF.x, pointF.y, new Matrix(), new Matrix()) == null) {
            z = false;
        }
        trash.getFlags().setSelectable(false).persist();
        return z;
    }

    private boolean findElement(EditorElement editorElement, Matrix matrix, Matrix matrix2) {
        return this.editorElementHierarchy.getRoot().findElement(editorElement, matrix, matrix2) == editorElement;
    }

    public void pushUndoPoint() {
        boolean isCropping = isCropping();
        if (!isCropping || currentCropIsAcceptable()) {
            getActiveUndoRedoStacks(isCropping).pushState(this.editorElementHierarchy.getRoot());
        }
    }

    public void updateUndoRedoAvailabilityState() {
        updateUndoRedoAvailableState(getActiveUndoRedoStacks(isCropping()));
    }

    public void clearUndoStack() {
        EditorElement pop;
        EditorElement root = this.editorElementHierarchy.getRoot();
        boolean isCropping = isCropping();
        UndoRedoStacks activeUndoRedoStacks = getActiveUndoRedoStacks(isCropping);
        boolean z = false;
        EditorElement editorElement = root;
        while (activeUndoRedoStacks.canUndo(editorElement) && (pop = activeUndoRedoStacks.getUndoStack().pop(editorElement)) != null) {
            z = true;
            this.editorElementHierarchy = EditorElementHierarchy.create(pop);
            activeUndoRedoStacks.getRedoStack().tryPush(editorElement);
            editorElement = this.editorElementHierarchy.getRoot();
        }
        if (z) {
            restoreStateWithAnimations(root, this.editorElementHierarchy.getRoot(), this.invalidate, isCropping);
            this.invalidate.run();
            this.editorElementHierarchy.updateViewToCrop(this.visibleViewPort, this.invalidate);
            this.inBoundsMemory.push(this.editorElementHierarchy.getMainImage(), this.editorElementHierarchy.getCropEditorElement());
        }
        updateUndoRedoAvailableState(activeUndoRedoStacks);
    }

    public void undo() {
        boolean isCropping = isCropping();
        UndoRedoStacks activeUndoRedoStacks = getActiveUndoRedoStacks(isCropping);
        undoRedo(activeUndoRedoStacks.getUndoStack(), activeUndoRedoStacks.getRedoStack(), isCropping);
        updateUndoRedoAvailableState(activeUndoRedoStacks);
    }

    private void undoRedo(ElementStack elementStack, ElementStack elementStack2, boolean z) {
        EditorElement root = this.editorElementHierarchy.getRoot();
        EditorElement pop = elementStack.pop(root);
        if (pop != null) {
            setEditorElementHierarchy(EditorElementHierarchy.create(pop));
            elementStack2.tryPush(root);
            restoreStateWithAnimations(root, this.editorElementHierarchy.getRoot(), this.invalidate, z);
            this.invalidate.run();
            this.editorElementHierarchy.updateViewToCrop(this.visibleViewPort, this.invalidate);
            this.inBoundsMemory.push(this.editorElementHierarchy.getMainImage(), this.editorElementHierarchy.getCropEditorElement());
        }
    }

    private void setEditorElementHierarchy(EditorElementHierarchy editorElementHierarchy) {
        EditorElement selectedElement = this.editorElementHierarchy.getSelectedElement();
        this.editorElementHierarchy = editorElementHierarchy;
        setSelected(selectedElement != null ? findById(selectedElement.getId()) : null);
    }

    private static void restoreStateWithAnimations(EditorElement editorElement, EditorElement editorElement2, Runnable runnable, boolean z) {
        EditorElement editorElement3;
        Map<UUID, EditorElement> elementMap = getElementMap(editorElement);
        Map<UUID, EditorElement> elementMap2 = getElementMap(editorElement2);
        for (EditorElement editorElement4 : elementMap.values()) {
            editorElement4.stopAnimation();
            EditorElement editorElement5 = elementMap2.get(editorElement4.getId());
            if (editorElement5 != null) {
                editorElement5.animateFrom(editorElement4.getLocalMatrixAnimating(), runnable);
                if (z) {
                    editorElement5.getEditorMatrix().set(editorElement4.getEditorMatrix());
                    editorElement5.getFlags().set(editorElement4.getFlags());
                }
            } else {
                EditorElement parentOf = editorElement.parentOf(editorElement4);
                if (!(parentOf == null || (editorElement3 = elementMap2.get(parentOf.getId())) == null)) {
                    editorElement3.addDeletedChildFadingOut(editorElement4, runnable);
                }
            }
        }
        for (EditorElement editorElement6 : elementMap2.values()) {
            if (!elementMap.containsKey(editorElement6.getId())) {
                editorElement6.animateFadeIn(runnable);
            }
        }
    }

    private void updateUndoRedoAvailableState(UndoRedoStacks undoRedoStacks) {
        if (this.undoRedoStackListener != null) {
            EditorElement root = this.editorElementHierarchy.getRoot();
            this.undoRedoStackListener.onAvailabilityChanged(undoRedoStacks.canUndo(root), undoRedoStacks.canRedo(root));
        }
    }

    private static Map<UUID, EditorElement> getElementMap(EditorElement editorElement) {
        HashMap hashMap = new HashMap();
        editorElement.buildMap(hashMap);
        return hashMap;
    }

    public void addFade() {
        this.editorElementHierarchy.addFade(this.invalidate);
    }

    public void removeFade() {
        this.editorElementHierarchy.removeFade(this.invalidate);
    }

    public void startCrop() {
        float f = this.editingPurpose == EditingPurpose.WALLPAPER ? 1.0f : 0.8f;
        pushUndoPoint();
        this.cropUndoRedoStacks.clear(this.editorElementHierarchy.getRoot());
        this.editorElementHierarchy.startCrop(this.invalidate, f);
        this.inBoundsMemory.push(this.editorElementHierarchy.getMainImage(), this.editorElementHierarchy.getCropEditorElement());
        updateUndoRedoAvailableState(this.cropUndoRedoStacks);
    }

    public void doneCrop() {
        this.editorElementHierarchy.doneCrop(this.visibleViewPort, this.invalidate);
        updateUndoRedoAvailableState(this.undoRedoStacks);
    }

    public void setCropAspectLock(boolean z) {
        EditorFlags flags = this.editorElementHierarchy.getCropEditorElement().getFlags();
        int currentState = flags.setAspectLocked(z).getCurrentState();
        flags.reset();
        flags.setAspectLocked(z).persist();
        flags.restoreState(currentState);
    }

    public boolean isCropAspectLocked() {
        return this.editorElementHierarchy.getCropEditorElement().getFlags().isAspectLocked();
    }

    public void postEdit(boolean z) {
        boolean isCropping = isCropping();
        if (isCropping) {
            ensureFitsBounds(z);
        }
        updateUndoRedoAvailableState(getActiveUndoRedoStacks(isCropping));
    }

    private UndoRedoStacks getActiveUndoRedoStacks(boolean z) {
        return z ? this.cropUndoRedoStacks : this.undoRedoStacks;
    }

    private void ensureFitsBounds(boolean z) {
        EditorElement mainImage = this.editorElementHierarchy.getMainImage();
        if (mainImage != null) {
            EditorElement cropEditorElement = this.editorElementHierarchy.getCropEditorElement();
            if (!currentCropIsAcceptable()) {
                if (!z) {
                    tryToFixTranslationOutOfBounds(mainImage, this.inBoundsMemory.getLastKnownGoodMainImageMatrix());
                } else if (!tryToScaleToFit(cropEditorElement, 0.9f)) {
                    tryToScaleToFit(mainImage, 2.0f);
                }
                if (!currentCropIsAcceptable()) {
                    this.inBoundsMemory.restore(mainImage, cropEditorElement, this.invalidate);
                } else {
                    this.inBoundsMemory.push(mainImage, cropEditorElement);
                }
            }
            this.editorElementHierarchy.dragDropRelease(this.visibleViewPort, this.invalidate);
        }
    }

    private boolean tryToScaleToFit(EditorElement editorElement, float f) {
        return Bisect.bisectToTest(editorElement, 1.0f, f, new EditorModel$$ExternalSyntheticLambda0(this), new Bisect.ModifyElement() { // from class: org.signal.imageeditor.core.model.EditorModel$$ExternalSyntheticLambda1
            @Override // org.signal.imageeditor.core.model.Bisect.ModifyElement
            public final void applyFactor(Matrix matrix, float f2) {
                matrix.preScale(f2, f2);
            }
        }, this.invalidate);
    }

    private Matrix tryToTranslateToFit(EditorElement editorElement, float f, float f2) {
        return Bisect.bisectToTest(editorElement, 0.0f, 1.0f, new EditorModel$$ExternalSyntheticLambda0(this), new Bisect.ModifyElement(f, f2) { // from class: org.signal.imageeditor.core.model.EditorModel$$ExternalSyntheticLambda7
            public final /* synthetic */ float f$0;
            public final /* synthetic */ float f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // org.signal.imageeditor.core.model.Bisect.ModifyElement
            public final void applyFactor(Matrix matrix, float f3) {
                EditorModel.lambda$tryToTranslateToFit$6(this.f$0, this.f$1, matrix, f3);
            }
        });
    }

    public static /* synthetic */ void lambda$tryToTranslateToFit$6(float f, float f2, Matrix matrix, float f3) {
        matrix.postTranslate(f * f3, f3 * f2);
    }

    private boolean tryToFixTranslationOutOfBounds(EditorElement editorElement, Matrix matrix) {
        float f;
        float f2;
        float f3;
        Matrix localMatrix = editorElement.getLocalMatrix();
        Matrix matrix2 = new Matrix(localMatrix);
        float[] fArr = new float[9];
        float[] fArr2 = new float[9];
        localMatrix.getValues(fArr);
        matrix.getValues(fArr2);
        float f4 = fArr[2] - fArr2[2];
        float f5 = fArr[5] - fArr2[5];
        if (Math.abs(f4) < 0.001f && Math.abs(f5) < 0.001f) {
            return false;
        }
        float f6 = 0.0f;
        if (Math.abs(f4) < Math.abs(f5)) {
            f6 = -f4;
            f2 = -f5;
            f = 0.0f;
            f3 = 0.0f;
        } else {
            f = -f5;
            f3 = -f4;
            f2 = 0.0f;
        }
        Matrix tryToTranslateToFit = tryToTranslateToFit(editorElement, f6, f);
        if (tryToTranslateToFit != null) {
            editorElement.animateLocalTo(tryToTranslateToFit, this.invalidate);
            return true;
        }
        Matrix tryToTranslateToFit2 = tryToTranslateToFit(editorElement, f3, f2);
        if (tryToTranslateToFit2 != null) {
            editorElement.animateLocalTo(tryToTranslateToFit2, this.invalidate);
            return true;
        }
        localMatrix.postTranslate(f6, f);
        Matrix tryToTranslateToFit3 = tryToTranslateToFit(editorElement, f3, f2);
        localMatrix.set(matrix2);
        if (tryToTranslateToFit3 == null) {
            return false;
        }
        editorElement.animateLocalTo(tryToTranslateToFit3, this.invalidate);
        return true;
    }

    public void dragDropRelease() {
        this.editorElementHierarchy.dragDropRelease(this.visibleViewPort, this.invalidate);
    }

    private boolean currentCropIsAcceptable() {
        Point outputSize = getOutputSize();
        int i = outputSize.x * outputSize.y;
        Point point = this.size;
        int min = Math.min(point.x * point.y, 100);
        Point point2 = MINIMUM_RATIO;
        if (compareRatios(this.size, point2) < 0) {
            point2 = this.size;
        }
        return compareRatios(outputSize, point2) >= 0 && i >= min && cropIsWithinMainImageBounds();
    }

    private static int compareRatios(Point point, Point point2) {
        int min = Math.min(point.x, point.y);
        int max = Math.max(point.x, point.y);
        return Integer.compare(min * Math.max(point2.x, point2.y), Math.min(point2.x, point2.y) * max);
    }

    public boolean cropIsWithinMainImageBounds() {
        return Bounds.boundsRemainInBounds(this.editorElementHierarchy.imageMatrixRelativeToCrop());
    }

    public void moving(EditorElement editorElement) {
        if (!isCropping()) {
            updateSelectionThumbsIfSelected(editorElement);
            return;
        }
        EditorElement mainImage = this.editorElementHierarchy.getMainImage();
        EditorElement cropEditorElement = this.editorElementHierarchy.getCropEditorElement();
        if ((editorElement == mainImage || editorElement == cropEditorElement) && currentCropIsAcceptable()) {
            this.inBoundsMemory.push(mainImage, cropEditorElement);
        }
    }

    public void setVisibleViewPort(RectF rectF) {
        this.visibleViewPort.set(rectF);
        this.editorElementHierarchy.updateViewToCrop(rectF, this.invalidate);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.editingPurpose.ordinal());
        parcel.writeFloat(this.fixedRatio);
        parcel.writeInt(this.size.x);
        parcel.writeInt(this.size.y);
        parcel.writeParcelable(this.editorElementHierarchy.getRoot(), i);
        parcel.writeParcelable(this.undoRedoStacks, i);
        parcel.writeParcelable(this.cropUndoRedoStacks, i);
    }

    public Bitmap render(Context context, RendererContext.TypefaceProvider typefaceProvider) {
        return render(context, null, typefaceProvider);
    }

    public Bitmap render(Context context, Point point, RendererContext.TypefaceProvider typefaceProvider) {
        EditorElement flipRotate = this.editorElementHierarchy.getFlipRotate();
        RectF cropRect = this.editorElementHierarchy.getCropRect();
        if (point == null) {
            point = getOutputSize();
        }
        Bitmap createBitmap = Bitmap.createBitmap(point.x, point.y, Bitmap.Config.ARGB_8888);
        try {
            RendererContext rendererContext = new RendererContext(context, new Canvas(createBitmap), RendererContext.Ready.NULL, RendererContext.Invalidate.NULL, typefaceProvider);
            RectF rectF = new RectF();
            rectF.right = (float) createBitmap.getWidth();
            rectF.bottom = (float) createBitmap.getHeight();
            Matrix matrix = new Matrix();
            matrix.setRectToRect(cropRect, rectF, Matrix.ScaleToFit.FILL);
            rendererContext.setIsEditing(false);
            rendererContext.setBlockingLoad(true);
            EditorElement overlay = this.editorElementHierarchy.getOverlay();
            overlay.getFlags().setVisible(false).setChildrenVisible(false);
            rendererContext.canvasMatrix.initial(matrix);
            flipRotate.draw(rendererContext);
            overlay.getFlags().reset();
            return createBitmap;
        } catch (Exception e) {
            createBitmap.recycle();
            throw e;
        }
    }

    private Point getOutputSize() {
        PointF outputSize = this.editorElementHierarchy.getOutputSize(this.size);
        int max = (int) Math.max(1024.0f, outputSize.x);
        return new Point(max, (int) ((((float) max) * outputSize.y) / outputSize.x));
    }

    public Point getOutputSizeMaxWidth(int i) {
        PointF outputSize = this.editorElementHierarchy.getOutputSize(this.size);
        int min = Math.min(i, (int) Math.max(1024.0f, outputSize.x));
        float f = outputSize.y;
        float f2 = outputSize.x;
        int i2 = (int) ((((float) min) * f) / f2);
        if (i2 > i) {
            min = (int) ((((float) i) * f2) / f);
        } else {
            i = i2;
        }
        return new Point(min, i);
    }

    @Override // org.signal.imageeditor.core.RendererContext.Ready
    public void onReady(Renderer renderer, Matrix matrix, Point point) {
        if (matrix != null && point != null && isRendererOfMainImage(renderer)) {
            boolean isChanged = isChanged();
            Matrix localMatrix = this.editorElementHierarchy.getImageCrop().getLocalMatrix();
            this.size.set(point.x, point.y);
            if (localMatrix.isIdentity()) {
                localMatrix.set(matrix);
                EditingPurpose editingPurpose = this.editingPurpose;
                if (editingPurpose == EditingPurpose.AVATAR_CAPTURE || editingPurpose == EditingPurpose.WALLPAPER || editingPurpose == EditingPurpose.AVATAR_EDIT) {
                    Matrix localMatrix2 = this.editorElementHierarchy.getCropEditorElement().getLocalMatrix();
                    int i = point.x;
                    int i2 = point.y;
                    if (i > i2) {
                        localMatrix2.setScale((this.fixedRatio * ((float) i2)) / ((float) i), 1.0f);
                    } else {
                        localMatrix2.setScale(1.0f, ((float) i) / ((float) i2));
                    }
                }
                this.editorElementHierarchy.doneCrop(this.visibleViewPort, null);
                if (!isChanged) {
                    this.undoRedoStacks.clear(this.editorElementHierarchy.getRoot());
                }
                int i3 = AnonymousClass2.$SwitchMap$org$signal$imageeditor$core$model$EditorModel$EditingPurpose[this.editingPurpose.ordinal()];
                if (i3 == 1) {
                    startCrop();
                } else if (i3 == 2) {
                    setFixedRatio(this.fixedRatio);
                    startCrop();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: org.signal.imageeditor.core.model.EditorModel$2 */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$org$signal$imageeditor$core$model$EditorModel$EditingPurpose;

        static {
            int[] iArr = new int[EditingPurpose.values().length];
            $SwitchMap$org$signal$imageeditor$core$model$EditorModel$EditingPurpose = iArr;
            try {
                iArr[EditingPurpose.AVATAR_CAPTURE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$signal$imageeditor$core$model$EditorModel$EditingPurpose[EditingPurpose.WALLPAPER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    public void setFixedRatio(float f) {
        this.fixedRatio = f;
        Matrix localMatrix = this.editorElementHierarchy.getCropEditorElement().getLocalMatrix();
        Point point = this.size;
        float f2 = ((float) point.x) / ((float) point.y);
        if (f2 > f) {
            localMatrix.setScale(f / f2, 1.0f);
        } else {
            localMatrix.setScale(1.0f, f2 / f);
        }
        this.editorElementHierarchy.doneCrop(this.visibleViewPort, null);
        startCrop();
    }

    private boolean isRendererOfMainImage(Renderer renderer) {
        EditorElement mainImage = this.editorElementHierarchy.getMainImage();
        return (mainImage != null ? mainImage.getRenderer() : null) == renderer;
    }

    public void addElementCentered(EditorElement editorElement, float f) {
        Matrix localMatrix = editorElement.getLocalMatrix();
        this.editorElementHierarchy.getMainImageFullMatrix().invert(localMatrix);
        localMatrix.preScale(f, f);
        addElement(editorElement);
    }

    public void addElement(EditorElement editorElement) {
        pushUndoPoint();
        addElementWithoutPushUndo(editorElement);
    }

    public void addElementWithoutPushUndo(EditorElement editorElement) {
        EditorElement editorElement2;
        EditorElement mainImage = this.editorElementHierarchy.getMainImage();
        if (mainImage != null) {
            editorElement2 = mainImage;
        } else {
            editorElement2 = this.editorElementHierarchy.getImageRoot();
        }
        editorElement2.addElement(editorElement);
        if (editorElement2 != mainImage) {
            this.undoRedoStacks.clear(this.editorElementHierarchy.getRoot());
        }
        updateUndoRedoAvailableState(this.undoRedoStacks);
    }

    public void clearFaceRenderers() {
        EditorElement mainImage = this.editorElementHierarchy.getMainImage();
        if (mainImage != null) {
            boolean z = false;
            for (int childCount = mainImage.getChildCount() - 1; childCount >= 0; childCount--) {
                if (mainImage.getChild(childCount).getRenderer() instanceof FaceBlurRenderer) {
                    if (!z) {
                        pushUndoPoint();
                        z = true;
                    }
                    mainImage.deleteChild(mainImage.getChild(childCount), this.invalidate);
                }
            }
        }
    }

    public boolean hasFaceRenderer() {
        EditorElement mainImage = this.editorElementHierarchy.getMainImage();
        if (mainImage == null) {
            return false;
        }
        for (int childCount = mainImage.getChildCount() - 1; childCount >= 0; childCount--) {
            if (mainImage.getChild(childCount).getRenderer() instanceof FaceBlurRenderer) {
                return true;
            }
        }
        return false;
    }

    public boolean isChanged() {
        return this.undoRedoStacks.isChanged(this.editorElementHierarchy.getRoot());
    }

    public RectF findCropRelativeToRoot() {
        return findCropRelativeTo(this.editorElementHierarchy.getRoot());
    }

    RectF findCropRelativeTo(EditorElement editorElement) {
        return findRelativeBounds(this.editorElementHierarchy.getCropEditorElement(), editorElement);
    }

    RectF findRelativeBounds(EditorElement editorElement, EditorElement editorElement2) {
        Matrix findRelativeMatrix = findRelativeMatrix(editorElement, editorElement2);
        RectF rectF = new RectF(Bounds.FULL_BOUNDS);
        if (findRelativeMatrix != null) {
            findRelativeMatrix.mapRect(rectF, Bounds.FULL_BOUNDS);
        }
        return rectF;
    }

    public Matrix findRelativeMatrix(EditorElement editorElement, EditorElement editorElement2) {
        Matrix findElementInverseMatrix = findElementInverseMatrix(editorElement2, new Matrix());
        Matrix findElementMatrix = findElementMatrix(editorElement, new Matrix());
        if (findElementMatrix == null || findElementInverseMatrix == null) {
            return null;
        }
        findElementInverseMatrix.preConcat(findElementMatrix);
        return findElementInverseMatrix;
    }

    public void rotate90anticlockwise() {
        flipRotate(-90.0f, 1, 1);
    }

    public void flipHorizontal() {
        flipRotate(0.0f, -1, 1);
    }

    private void flipRotate(float f, int i, int i2) {
        pushUndoPoint();
        this.editorElementHierarchy.flipRotate(f, i, i2, this.visibleViewPort, this.invalidate);
        updateUndoRedoAvailableState(getActiveUndoRedoStacks(isCropping()));
    }

    public EditorElement getRoot() {
        return this.editorElementHierarchy.getRoot();
    }

    public EditorElement getTrash() {
        return this.editorElementHierarchy.getTrash();
    }

    public EditorElement getMainImage() {
        return this.editorElementHierarchy.getMainImage();
    }

    public /* synthetic */ void lambda$delete$8(EditorElement editorElement, EditorElement editorElement2) {
        editorElement2.deleteChild(editorElement, this.invalidate);
    }

    public void delete(EditorElement editorElement) {
        this.editorElementHierarchy.getImageRoot().forAllInTree(new EditorElement.PerElementFunction(editorElement) { // from class: org.signal.imageeditor.core.model.EditorModel$$ExternalSyntheticLambda6
            public final /* synthetic */ EditorElement f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.imageeditor.core.model.EditorElement.PerElementFunction
            public final void apply(EditorElement editorElement2) {
                EditorModel.this.lambda$delete$8(this.f$1, editorElement2);
            }
        });
        setSelected(null);
    }

    public EditorElement findById(UUID uuid) {
        return getRoot().findElementWithId(uuid);
    }

    public void zoomToTextElement(EditorElement editorElement, MultiLineTextRenderer multiLineTextRenderer) {
        Matrix findElementInverseMatrix = findElementInverseMatrix(editorElement, new Matrix());
        if (findElementInverseMatrix != null) {
            EditorElement root = this.editorElementHierarchy.getRoot();
            findElementInverseMatrix.preConcat(root.getEditorMatrix());
            multiLineTextRenderer.applyRecommendedEditorMatrix(findElementInverseMatrix);
            root.animateEditorTo(findElementInverseMatrix, this.invalidate);
        }
    }

    public void zoomOut() {
        this.editorElementHierarchy.getRoot().rollbackEditorMatrix(this.invalidate);
    }

    public void indicateSelected(EditorElement editorElement) {
        editorElement.singleScalePulse(this.invalidate);
    }

    public boolean isCropping() {
        return this.editorElementHierarchy.getCropEditorElement().getFlags().isVisible();
    }

    public Matrix getInverseCropPosition() {
        Matrix matrix = new Matrix();
        matrix.set(findRelativeMatrix(this.editorElementHierarchy.getMainImage(), this.editorElementHierarchy.getCropEditorElement()));
        matrix.postConcat(this.editorElementHierarchy.getFlipRotate().getLocalMatrix());
        Matrix matrix2 = new Matrix();
        matrix.invert(matrix2);
        return matrix2;
    }
}
