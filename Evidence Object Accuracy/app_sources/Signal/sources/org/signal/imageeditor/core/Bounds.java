package org.signal.imageeditor.core;

import android.graphics.Matrix;
import android.graphics.RectF;

/* loaded from: classes3.dex */
public final class Bounds {
    public static final float[] CENTRE = {0.0f, 0.0f};
    public static RectF FULL_BOUNDS = newFullBounds();
    private static final float[] POINTS = {-1000.0f, -1000.0f, 1000.0f, -1000.0f, 1000.0f, 1000.0f, -1000.0f, 1000.0f};

    public static RectF newFullBounds() {
        return new RectF(-1000.0f, -1000.0f, 1000.0f, 1000.0f);
    }

    public static boolean contains(float f, float f2) {
        RectF rectF = FULL_BOUNDS;
        return f >= rectF.left && f <= rectF.right && f2 >= rectF.top && f2 <= rectF.bottom;
    }

    public static boolean boundsRemainInBounds(Matrix matrix) {
        if (matrix == null) {
            return true;
        }
        float[] fArr = POINTS;
        float[] fArr2 = new float[fArr.length];
        matrix.mapPoints(fArr2, fArr);
        return allWithinBounds(fArr2);
    }

    private static boolean allWithinBounds(float[] fArr) {
        for (int i = 0; i < fArr.length / 2; i++) {
            int i2 = i * 2;
            if (!contains(fArr[i2], fArr[i2 + 1])) {
                return false;
            }
        }
        return true;
    }
}
