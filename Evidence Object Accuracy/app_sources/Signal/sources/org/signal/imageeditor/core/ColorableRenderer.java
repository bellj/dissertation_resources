package org.signal.imageeditor.core;

/* loaded from: classes3.dex */
public interface ColorableRenderer extends Renderer {
    int getColor();

    void setColor(int i);
}
