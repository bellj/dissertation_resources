package org.signal.imageeditor.core.renderers;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.core.content.ContextCompat;
import org.signal.imageeditor.R$dimen;
import org.signal.imageeditor.core.Bounds;
import org.signal.imageeditor.core.Renderer;
import org.signal.imageeditor.core.RendererContext;

/* loaded from: classes3.dex */
public final class OvalGuideRenderer implements Renderer {
    public static final Parcelable.Creator<OvalGuideRenderer> CREATOR = new Parcelable.Creator<OvalGuideRenderer>() { // from class: org.signal.imageeditor.core.renderers.OvalGuideRenderer.1
        @Override // android.os.Parcelable.Creator
        public OvalGuideRenderer createFromParcel(Parcel parcel) {
            return new OvalGuideRenderer(parcel.readInt());
        }

        @Override // android.os.Parcelable.Creator
        public OvalGuideRenderer[] newArray(int i) {
            return new OvalGuideRenderer[i];
        }
    };
    private final RectF dst = new RectF();
    private final int ovalGuideColor;
    private final Paint paint;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // org.signal.imageeditor.core.Renderer
    public void render(RendererContext rendererContext) {
        rendererContext.save();
        Canvas canvas = rendererContext.canvas;
        Context context = rendererContext.context;
        float dimensionPixelSize = (float) context.getResources().getDimensionPixelSize(R$dimen.oval_guide_stroke_width);
        float f = dimensionPixelSize / 2.0f;
        this.paint.setStrokeWidth(dimensionPixelSize);
        this.paint.setColor(ContextCompat.getColor(context, this.ovalGuideColor));
        rendererContext.mapRect(this.dst, Bounds.FULL_BOUNDS);
        RectF rectF = this.dst;
        rectF.set(rectF.left + f, rectF.top + f, rectF.right - f, rectF.bottom - f);
        rendererContext.canvasMatrix.setToIdentity();
        canvas.drawOval(this.dst, this.paint);
        rendererContext.restore();
    }

    public OvalGuideRenderer(int i) {
        this.ovalGuideColor = i;
        Paint paint = new Paint();
        this.paint = paint;
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);
    }

    @Override // org.signal.imageeditor.core.Renderer
    public boolean hitTest(float f, float f2) {
        return !Bounds.contains(f, f2);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.ovalGuideColor);
    }
}
