package org.signal.imageeditor.core.renderers;

import android.animation.ValueAnimator;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.signal.core.util.DimensionUnit;
import org.signal.imageeditor.core.Bounds;
import org.signal.imageeditor.core.ColorableRenderer;
import org.signal.imageeditor.core.RendererContext;
import org.signal.imageeditor.core.SelectableRenderer;

/* loaded from: classes3.dex */
public final class MultiLineTextRenderer extends InvalidateableRenderer implements ColorableRenderer, SelectableRenderer {
    public static final Parcelable.Creator<MultiLineTextRenderer> CREATOR = new Parcelable.Creator<MultiLineTextRenderer>() { // from class: org.signal.imageeditor.core.renderers.MultiLineTextRenderer.1
        @Override // android.os.Parcelable.Creator
        public MultiLineTextRenderer createFromParcel(Parcel parcel) {
            return new MultiLineTextRenderer(parcel.readString(), parcel.readInt(), Mode.fromCode(parcel.readInt()));
        }

        @Override // android.os.Parcelable.Creator
        public MultiLineTextRenderer[] newArray(int i) {
            return new MultiLineTextRenderer[i];
        }
    };
    private static final float HIGHLIGHT_BOTTOM_PADDING;
    private static final float HIGHLIGHT_CORNER_RADIUS;
    private static final float HIGHLIGHT_HORIZONTAL_PADDING;
    private static final float HIGHLIGHT_TOP_PADDING;
    private static final float HIT_PADDING;
    private int color;
    private float cursorAnimatedValue;
    private ValueAnimator cursorAnimator;
    private boolean hasFocus;
    private List<Line> lines;
    private Mode mode;
    private final Paint modePaint;
    private final Paint paint;
    private final Matrix recommendedEditorMatrix;
    private int selEnd;
    private int selStart;
    private final Paint selectionPaint;
    private String text = "";
    private final RectF textBounds;
    private final float textScale;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // org.signal.imageeditor.core.SelectableRenderer
    public void onSelected(boolean z) {
    }

    static {
        DimensionUnit dimensionUnit = DimensionUnit.DP;
        HIT_PADDING = dimensionUnit.toPixels(30.0f);
        HIGHLIGHT_HORIZONTAL_PADDING = dimensionUnit.toPixels(8.0f);
        HIGHLIGHT_TOP_PADDING = dimensionUnit.toPixels(10.0f);
        HIGHLIGHT_BOTTOM_PADDING = dimensionUnit.toPixels(6.0f);
        HIGHLIGHT_CORNER_RADIUS = dimensionUnit.toPixels(4.0f);
        CREATOR = new Parcelable.Creator<MultiLineTextRenderer>() { // from class: org.signal.imageeditor.core.renderers.MultiLineTextRenderer.1
            @Override // android.os.Parcelable.Creator
            public MultiLineTextRenderer createFromParcel(Parcel parcel) {
                return new MultiLineTextRenderer(parcel.readString(), parcel.readInt(), Mode.fromCode(parcel.readInt()));
            }

            @Override // android.os.Parcelable.Creator
            public MultiLineTextRenderer[] newArray(int i) {
                return new MultiLineTextRenderer[i];
            }
        };
    }

    public MultiLineTextRenderer(String str, int i, Mode mode) {
        Paint paint = new Paint();
        this.paint = paint;
        Paint paint2 = new Paint();
        this.selectionPaint = paint2;
        Paint paint3 = new Paint();
        this.modePaint = paint3;
        this.lines = Collections.emptyList();
        this.recommendedEditorMatrix = new Matrix();
        this.textBounds = new RectF();
        this.mode = mode;
        paint3.setAntiAlias(true);
        paint3.setTextSize(100.0f);
        setColorInternal(i);
        float textSize = paint.getTextSize();
        paint.setAntiAlias(true);
        paint.setTextSize(100.0f);
        this.textScale = paint.getTextSize() / textSize;
        paint2.setAntiAlias(true);
        setText(str == null ? "" : str);
        createLinesForText();
    }

    @Override // org.signal.imageeditor.core.renderers.InvalidateableRenderer, org.signal.imageeditor.core.Renderer
    public void render(RendererContext rendererContext) {
        super.render(rendererContext);
        this.paint.setTypeface(rendererContext.typefaceProvider.getSelectedTypeface(rendererContext.context, this, rendererContext.invalidate));
        this.modePaint.setTypeface(rendererContext.typefaceProvider.getSelectedTypeface(rendererContext.context, this, rendererContext.invalidate));
        float f = 0.0f;
        float f2 = 0.0f;
        for (Line line : this.lines) {
            line.render(rendererContext);
            f2 += (line.heightInBounds - line.ascentInBounds) + line.descentInBounds;
            f = Math.max(line.textBounds.width(), f);
        }
        this.textBounds.set((-f) - 10.0f, -10.0f, f + 10.0f, (f2 / 2.0f) + 10.0f);
    }

    public String getText() {
        return this.text;
    }

    public void setText(String str) {
        if (!this.text.equals(str)) {
            this.text = str;
            createLinesForText();
        }
    }

    public void nextMode() {
        setMode(Mode.fromCode(this.mode.code + 1));
    }

    public void applyRecommendedEditorMatrix(Matrix matrix) {
        this.recommendedEditorMatrix.reset();
        float f = 1.0f;
        for (Line line : this.lines) {
            if (line.scale < f) {
                f = line.scale;
            }
        }
        float f2 = 0.0f;
        for (Line line2 : this.lines) {
            if (line2.containsSelectionEnd()) {
                break;
            }
            f2 -= line2.heightInBounds;
        }
        this.recommendedEditorMatrix.postTranslate(0.0f, f2 - 0.006632487f);
        this.recommendedEditorMatrix.postScale(f, f);
        matrix.postConcat(this.recommendedEditorMatrix);
    }

    private void createLinesForText() {
        String[] split = this.text.split("\n", -1);
        int i = 0;
        if (split.length == this.lines.size()) {
            while (i < split.length) {
                this.lines.get(i).setText(split[i]);
                i++;
            }
        } else {
            this.lines = new ArrayList(split.length);
            int length = split.length;
            while (i < length) {
                this.lines.add(new Line(split[i]));
                i++;
            }
        }
        setSelection(this.selStart, this.selEnd);
    }

    /* loaded from: classes3.dex */
    public class Line {
        private float ascentInBounds;
        private final Matrix ascentMatrix = new Matrix();
        private float descentInBounds;
        private final Matrix descentMatrix = new Matrix();
        private float heightInBounds;
        private final RectF hitBounds = new RectF();
        private final Matrix inverseProjectionMatrix = new Matrix();
        private final RectF modeBounds = new RectF();
        private final Path outlinerPath = new Path();
        private final Matrix projectionMatrix = new Matrix();
        private float scale = 1.0f;
        private int selEnd;
        private int selStart;
        private final RectF selectionBounds = new RectF();
        private String text;
        private final RectF textBounds = new RectF();

        Line(String str) {
            MultiLineTextRenderer.this = r1;
            this.text = str;
            recalculate();
        }

        private void recalculate() {
            RectF rectF = new RectF();
            Rect rect = new Rect();
            String str = this.text;
            getTextBoundsWithoutTrim(str, 0, str.length(), rect);
            this.textBounds.set(rect);
            this.hitBounds.set(this.textBounds);
            this.hitBounds.left -= MultiLineTextRenderer.HIT_PADDING;
            this.hitBounds.right += MultiLineTextRenderer.HIT_PADDING;
            this.hitBounds.top -= MultiLineTextRenderer.HIT_PADDING;
            this.hitBounds.bottom += MultiLineTextRenderer.HIT_PADDING;
            rectF.set(this.textBounds);
            float f = MultiLineTextRenderer.this.textScale * 150.0f;
            this.scale = 1.0f / Math.max(1.0f, rectF.right / f);
            rectF.right = f;
            if (showSelectionOrCursor()) {
                Rect rect2 = new Rect();
                int min = Math.min(this.text.length(), Math.max(0, this.selStart));
                int min2 = Math.min(this.text.length(), Math.max(0, this.selEnd));
                getTextBoundsWithoutTrim(this.text.substring(0, min), 0, min, rect2);
                if (this.selStart != this.selEnd) {
                    getTextBoundsWithoutTrim(this.text, min, min2, rect);
                } else {
                    MultiLineTextRenderer.this.paint.getTextBounds("|", 0, 1, rect);
                    int width = rect.width();
                    rect.left -= width;
                    rect.right -= width;
                }
                int i = rect.left;
                int i2 = rect2.right;
                rect.left = i + i2;
                rect.right += i2;
                this.selectionBounds.set(rect);
            }
            this.projectionMatrix.setRectToRect(new RectF(rectF), Bounds.FULL_BOUNDS, Matrix.ScaleToFit.CENTER);
            removeTranslate(this.projectionMatrix);
            float[] fArr = {0.0f, MultiLineTextRenderer.this.paint.ascent(), 0.0f, MultiLineTextRenderer.this.paint.descent()};
            this.projectionMatrix.mapPoints(fArr);
            float f2 = fArr[1];
            this.ascentInBounds = f2;
            float f3 = fArr[3];
            this.descentInBounds = f3;
            this.heightInBounds = f3 - f2;
            this.projectionMatrix.preTranslate(-this.textBounds.centerX(), 0.0f);
            this.projectionMatrix.invert(this.inverseProjectionMatrix);
            this.ascentMatrix.setTranslate(0.0f, -this.ascentInBounds);
            this.descentMatrix.setTranslate(0.0f, this.descentInBounds + MultiLineTextRenderer.HIGHLIGHT_TOP_PADDING + MultiLineTextRenderer.HIGHLIGHT_BOTTOM_PADDING);
            MultiLineTextRenderer.this.invalidate();
        }

        private void removeTranslate(Matrix matrix) {
            float[] fArr = new float[9];
            matrix.getValues(fArr);
            fArr[2] = 0.0f;
            fArr[5] = 0.0f;
            matrix.setValues(fArr);
        }

        private boolean showSelectionOrCursor() {
            int i = this.selStart;
            return (i >= 0 || this.selEnd >= 0) && (i <= this.text.length() || this.selEnd <= this.text.length());
        }

        public boolean containsSelectionEnd() {
            int i = this.selEnd;
            return i >= 0 && i <= this.text.length();
        }

        private void getTextBoundsWithoutTrim(String str, int i, int i2, Rect rect) {
            Rect rect2 = new Rect();
            Rect rect3 = new Rect();
            String str2 = "x" + str.substring(Math.max(0, i), Math.min(str.length(), i2)) + "x";
            MultiLineTextRenderer.this.paint.getTextBounds(str2, 0, str2.length(), rect2);
            MultiLineTextRenderer.this.paint.getTextBounds("x", 0, 1, rect3);
            rect.set(rect2);
            int width = rect.right - (rect3.width() * 2);
            int i3 = rect.left;
            rect.left = i3 - i3;
            rect.right = width - i3;
        }

        void setText(String str) {
            if (!this.text.equals(str)) {
                this.text = str;
                recalculate();
            }
        }

        public void render(RendererContext rendererContext) {
            rendererContext.canvasMatrix.concat(this.ascentMatrix);
            rendererContext.save();
            rendererContext.canvasMatrix.concat(this.projectionMatrix);
            if (MultiLineTextRenderer.this.mode == Mode.HIGHLIGHT) {
                this.modeBounds.set(this.textBounds.left - MultiLineTextRenderer.HIGHLIGHT_HORIZONTAL_PADDING, this.selectionBounds.top - MultiLineTextRenderer.HIGHLIGHT_TOP_PADDING, this.textBounds.right + MultiLineTextRenderer.HIGHLIGHT_HORIZONTAL_PADDING, this.selectionBounds.bottom + MultiLineTextRenderer.HIGHLIGHT_BOTTOM_PADDING);
                int alpha = MultiLineTextRenderer.this.modePaint.getAlpha();
                MultiLineTextRenderer.this.modePaint.setAlpha(rendererContext.getAlpha(alpha));
                rendererContext.canvas.drawRoundRect(this.modeBounds, MultiLineTextRenderer.HIGHLIGHT_CORNER_RADIUS, MultiLineTextRenderer.HIGHLIGHT_CORNER_RADIUS, MultiLineTextRenderer.this.modePaint);
                MultiLineTextRenderer.this.modePaint.setAlpha(alpha);
            } else if (MultiLineTextRenderer.this.mode == Mode.UNDERLINE) {
                RectF rectF = this.modeBounds;
                RectF rectF2 = this.textBounds;
                float f = rectF2.left;
                RectF rectF3 = this.selectionBounds;
                rectF.set(f, rectF3.top, rectF2.right, rectF3.bottom);
                RectF rectF4 = this.modeBounds;
                DimensionUnit dimensionUnit = DimensionUnit.DP;
                rectF4.inset(-dimensionUnit.toPixels(2.0f), -dimensionUnit.toPixels(2.0f));
                RectF rectF5 = this.modeBounds;
                float f2 = rectF5.left;
                float max = Math.max(rectF5.top, rectF5.bottom - dimensionUnit.toPixels(6.0f));
                RectF rectF6 = this.modeBounds;
                rectF5.set(f2, max, rectF6.right, rectF6.bottom - dimensionUnit.toPixels(2.0f));
                int alpha2 = MultiLineTextRenderer.this.modePaint.getAlpha();
                MultiLineTextRenderer.this.modePaint.setAlpha(rendererContext.getAlpha(alpha2));
                rendererContext.canvas.drawRect(this.modeBounds, MultiLineTextRenderer.this.modePaint);
                MultiLineTextRenderer.this.modePaint.setAlpha(alpha2);
            }
            if (MultiLineTextRenderer.this.hasFocus && showSelectionOrCursor()) {
                if (this.selStart == this.selEnd) {
                    MultiLineTextRenderer.this.selectionPaint.setAlpha((int) (MultiLineTextRenderer.this.cursorAnimatedValue * 128.0f));
                } else {
                    MultiLineTextRenderer.this.selectionPaint.setAlpha(128);
                }
                rendererContext.canvas.drawRect(this.selectionBounds, MultiLineTextRenderer.this.selectionPaint);
            }
            int alpha3 = MultiLineTextRenderer.this.paint.getAlpha();
            MultiLineTextRenderer.this.paint.setAlpha(rendererContext.getAlpha(alpha3));
            rendererContext.canvas.drawText(this.text, 0.0f, 0.0f, MultiLineTextRenderer.this.paint);
            MultiLineTextRenderer.this.paint.setAlpha(alpha3);
            if (MultiLineTextRenderer.this.mode == Mode.OUTLINE) {
                int alpha4 = MultiLineTextRenderer.this.modePaint.getAlpha();
                MultiLineTextRenderer.this.modePaint.setAlpha(rendererContext.getAlpha(alpha3));
                if (Build.VERSION.SDK_INT >= 31) {
                    this.outlinerPath.reset();
                    Paint paint = MultiLineTextRenderer.this.modePaint;
                    String str = this.text;
                    paint.getTextPath(str, 0, str.length(), 0.0f, 0.0f, this.outlinerPath);
                    Path path = this.outlinerPath;
                    path.op(path, Path.Op.INTERSECT);
                    rendererContext.canvas.drawPath(this.outlinerPath, MultiLineTextRenderer.this.modePaint);
                } else {
                    rendererContext.canvas.drawText(this.text, 0.0f, 0.0f, MultiLineTextRenderer.this.modePaint);
                }
                MultiLineTextRenderer.this.modePaint.setAlpha(alpha4);
            }
            rendererContext.restore();
            rendererContext.canvasMatrix.concat(this.descentMatrix);
        }

        void setSelection(int i, int i2) {
            if (i != this.selStart || i2 != this.selEnd) {
                this.selStart = i;
                this.selEnd = i2;
                recalculate();
            }
        }
    }

    @Override // org.signal.imageeditor.core.ColorableRenderer
    public int getColor() {
        return this.color;
    }

    @Override // org.signal.imageeditor.core.ColorableRenderer
    public void setColor(int i) {
        if (this.color != i) {
            setColorInternal(i);
        }
    }

    @Override // org.signal.imageeditor.core.SelectableRenderer
    public void getSelectionBounds(RectF rectF) {
        rectF.set(this.textBounds);
    }

    @Override // org.signal.imageeditor.core.Renderer
    public boolean hitTest(float f, float f2) {
        return this.textBounds.contains(f, f2);
    }

    public void setSelection(int i, int i2) {
        this.selStart = i;
        this.selEnd = i2;
        for (Line line : this.lines) {
            line.setSelection(i, i2);
            int length = line.text.length() + 1;
            i -= length;
            i2 -= length;
        }
    }

    public void setFocused(boolean z) {
        if (this.hasFocus != z) {
            this.hasFocus = z;
            ValueAnimator valueAnimator = this.cursorAnimator;
            if (valueAnimator != null) {
                valueAnimator.cancel();
                this.cursorAnimator = null;
            }
            if (z) {
                ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
                this.cursorAnimator = ofFloat;
                ofFloat.setInterpolator(pulseInterpolator());
                this.cursorAnimator.setRepeatCount(-1);
                this.cursorAnimator.setDuration(1000L);
                this.cursorAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.signal.imageeditor.core.renderers.MultiLineTextRenderer$$ExternalSyntheticLambda0
                    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                    public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                        MultiLineTextRenderer.this.lambda$setFocused$0(valueAnimator2);
                    }
                });
                this.cursorAnimator.start();
                return;
            }
            invalidate();
        }
    }

    public /* synthetic */ void lambda$setFocused$0(ValueAnimator valueAnimator) {
        this.cursorAnimatedValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
        invalidate();
    }

    private void setMode(Mode mode) {
        if (this.mode != mode) {
            this.mode = mode;
            setColorInternal(this.color);
        }
    }

    private void setColorInternal(int i) {
        this.color = i;
        if (this.mode == Mode.REGULAR) {
            this.paint.setColor(i);
            this.selectionPaint.setColor(i);
        } else {
            this.paint.setColor(-1);
            this.selectionPaint.setColor(-1);
        }
        if (this.mode == Mode.OUTLINE) {
            this.modePaint.setStrokeWidth(DimensionUnit.DP.toPixels(15.0f) / 10.0f);
            this.modePaint.setStyle(Paint.Style.STROKE);
        } else {
            this.modePaint.setStyle(Paint.Style.FILL);
        }
        this.modePaint.setColor(i);
        invalidate();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.text);
        parcel.writeInt(this.color);
        parcel.writeInt(this.mode.code);
    }

    private static Interpolator pulseInterpolator() {
        return new Interpolator() { // from class: org.signal.imageeditor.core.renderers.MultiLineTextRenderer$$ExternalSyntheticLambda1
            @Override // android.animation.TimeInterpolator
            public final float getInterpolation(float f) {
                return MultiLineTextRenderer.lambda$pulseInterpolator$1(f);
            }
        };
    }

    public static /* synthetic */ float lambda$pulseInterpolator$1(float f) {
        float f2 = f * 5.0f;
        if (f2 > 1.0f) {
            f2 = 4.0f - f2;
        }
        return Math.max(0.0f, Math.min(1.0f, f2));
    }

    /* loaded from: classes3.dex */
    public enum Mode {
        REGULAR(0),
        HIGHLIGHT(1),
        UNDERLINE(2),
        OUTLINE(3);
        
        private final int code;

        Mode(int i) {
            this.code = i;
        }

        public static Mode fromCode(int i) {
            Mode[] values = values();
            for (Mode mode : values) {
                if (mode.code == i) {
                    return mode;
                }
            }
            return REGULAR;
        }
    }
}
