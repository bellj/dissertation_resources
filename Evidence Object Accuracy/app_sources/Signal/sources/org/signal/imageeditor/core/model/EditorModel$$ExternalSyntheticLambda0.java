package org.signal.imageeditor.core.model;

import org.signal.imageeditor.core.model.Bisect;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class EditorModel$$ExternalSyntheticLambda0 implements Bisect.Predicate {
    public final /* synthetic */ EditorModel f$0;

    public /* synthetic */ EditorModel$$ExternalSyntheticLambda0(EditorModel editorModel) {
        this.f$0 = editorModel;
    }

    @Override // org.signal.imageeditor.core.model.Bisect.Predicate
    public final boolean test() {
        return this.f$0.cropIsWithinMainImageBounds();
    }
}
