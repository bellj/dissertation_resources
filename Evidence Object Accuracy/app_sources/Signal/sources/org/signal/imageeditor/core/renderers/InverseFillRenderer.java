package org.signal.imageeditor.core.renderers;

import android.graphics.Path;
import android.graphics.RectF;
import android.os.Parcel;
import android.os.Parcelable;
import org.signal.core.util.DimensionUnit;
import org.signal.imageeditor.core.Bounds;
import org.signal.imageeditor.core.Renderer;
import org.signal.imageeditor.core.RendererContext;

/* loaded from: classes3.dex */
public final class InverseFillRenderer implements Renderer {
    public static final Parcelable.Creator<InverseFillRenderer> CREATOR = new Parcelable.Creator<InverseFillRenderer>() { // from class: org.signal.imageeditor.core.renderers.InverseFillRenderer.1
        @Override // android.os.Parcelable.Creator
        public InverseFillRenderer createFromParcel(Parcel parcel) {
            return new InverseFillRenderer(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public InverseFillRenderer[] newArray(int i) {
            return new InverseFillRenderer[i];
        }
    };
    private final int color;
    private final RectF dst;
    private final Path path;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // org.signal.imageeditor.core.Renderer
    public void render(RendererContext rendererContext) {
        rendererContext.canvas.save();
        rendererContext.mapRect(this.dst, Bounds.FULL_BOUNDS);
        rendererContext.canvasMatrix.setToIdentity();
        this.path.reset();
        Path path = this.path;
        RectF rectF = this.dst;
        DimensionUnit dimensionUnit = DimensionUnit.DP;
        path.addRoundRect(rectF, dimensionUnit.toPixels(18.0f), dimensionUnit.toPixels(18.0f), Path.Direction.CW);
        rendererContext.canvas.clipPath(this.path);
        rendererContext.canvas.drawColor(this.color);
        rendererContext.canvas.restore();
    }

    public InverseFillRenderer(int i) {
        this.dst = new RectF();
        Path path = new Path();
        this.path = path;
        this.color = i;
        path.toggleInverseFillType();
    }

    private InverseFillRenderer(Parcel parcel) {
        this(parcel.readInt());
    }

    @Override // org.signal.imageeditor.core.Renderer
    public boolean hitTest(float f, float f2) {
        return !Bounds.contains(f, f2);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.color);
    }
}
