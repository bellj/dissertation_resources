package org.signal.imageeditor.core.renderers;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.animation.Interpolator;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.DimensionUnit;
import org.signal.imageeditor.R$drawable;
import org.signal.imageeditor.core.Bounds;
import org.signal.imageeditor.core.RendererContext;

/* compiled from: TrashRenderer.kt */
@Metadata(d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0014\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0000\u0018\u0000 *2\u00020\u00012\u00020\u00022\u00020\u0003:\u0001*B\u0007\b\u0016¢\u0006\u0002\u0010\u0004J\b\u0010\u0018\u001a\u00020\u0017H\u0016J\u0006\u0010\u0019\u001a\u00020\u001aJ\u0010\u0010\u001b\u001a\u00020\n2\u0006\u0010\u001c\u001a\u00020\u0015H\u0002J\u0018\u0010\u001d\u001a\u00020\u000f2\u0006\u0010\u001e\u001a\u00020\n2\u0006\u0010\u001f\u001a\u00020\nH\u0016J\u0010\u0010 \u001a\u00020\n2\u0006\u0010!\u001a\u00020\nH\u0002J\u0010\u0010\"\u001a\u00020\u001a2\u0006\u0010#\u001a\u00020$H\u0016J\u0006\u0010%\u001a\u00020\u001aJ\u0018\u0010&\u001a\u00020\u001a2\u0006\u0010'\u001a\u00020(2\u0006\u0010)\u001a\u00020\u0017H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0011X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0004¢\u0006\u0002\n\u0000¨\u0006+"}, d2 = {"Lorg/signal/imageeditor/core/renderers/TrashRenderer;", "Lorg/signal/imageeditor/core/renderers/InvalidateableRenderer;", "Lorg/signal/imageeditor/core/Renderer;", "Landroid/os/Parcelable;", "()V", "bounds", "Landroid/graphics/RectF;", "buttonCenter", "", "diameterLarge", "", "diameterSmall", "interpolator", "Landroid/view/animation/Interpolator;", "isExpanding", "", "outlinePaint", "Landroid/graphics/Paint;", "padBottom", "shadePaint", "startTime", "", "trashSize", "", "describeContents", "expand", "", "getInterpolatedDiameter", "timeElapsed", "hitTest", "x", "y", "interpolateFromFraction", "fraction", "render", "rendererContext", "Lorg/signal/imageeditor/core/RendererContext;", "shrink", "writeToParcel", "dest", "Landroid/os/Parcel;", "flags", "Companion", "image-editor_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class TrashRenderer extends InvalidateableRenderer implements Parcelable {
    public static final Parcelable.Creator<TrashRenderer> CREATOR = new Parcelable.Creator<TrashRenderer>() { // from class: org.signal.imageeditor.core.renderers.TrashRenderer$Companion$CREATOR$1
        @Override // android.os.Parcelable.Creator
        public TrashRenderer createFromParcel(Parcel parcel) {
            Intrinsics.checkNotNullParameter(parcel, "in");
            return new TrashRenderer();
        }

        @Override // android.os.Parcelable.Creator
        public TrashRenderer[] newArray(int i) {
            return new TrashRenderer[i];
        }
    };
    public static final Companion Companion = new Companion(null);
    private final RectF bounds = new RectF();
    private final float[] buttonCenter;
    private final float diameterLarge;
    private final float diameterSmall;
    private final Interpolator interpolator;
    private boolean isExpanding;
    private final Paint outlinePaint;
    private final float padBottom;
    private final Paint shadePaint;
    private long startTime;
    private final int trashSize;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkNotNullParameter(parcel, "dest");
    }

    public TrashRenderer() {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(-1);
        paint.setStyle(Paint.Style.STROKE);
        DimensionUnit dimensionUnit = DimensionUnit.DP;
        paint.setStrokeWidth(dimensionUnit.toPixels(1.5f));
        this.outlinePaint = paint;
        Paint paint2 = new Paint();
        paint2.setAntiAlias(true);
        paint2.setColor(-1728053248);
        paint2.setStyle(Paint.Style.FILL);
        this.shadePaint = paint2;
        this.diameterSmall = dimensionUnit.toPixels(41.0f);
        this.diameterLarge = dimensionUnit.toPixels(54.0f);
        this.trashSize = (int) dimensionUnit.toPixels(24.0f);
        this.padBottom = dimensionUnit.toPixels(16.0f);
        this.interpolator = new FastOutSlowInInterpolator();
        this.buttonCenter = new float[2];
    }

    @Override // org.signal.imageeditor.core.renderers.InvalidateableRenderer, org.signal.imageeditor.core.Renderer
    public void render(RendererContext rendererContext) {
        Intrinsics.checkNotNullParameter(rendererContext, "rendererContext");
        super.render(rendererContext);
        long currentTimeMillis = System.currentTimeMillis();
        Drawable drawable = AppCompatResources.getDrawable(rendererContext.context, R$drawable.ic_trash_white_24);
        if (drawable != null) {
            int i = this.trashSize;
            drawable.setBounds(0, 0, i, i);
            float interpolatedDiameter = getInterpolatedDiameter(currentTimeMillis - this.startTime);
            rendererContext.canvas.save();
            rendererContext.mapRect(this.bounds, Bounds.FULL_BOUNDS);
            this.buttonCenter[0] = this.bounds.centerX();
            this.buttonCenter[1] = (this.bounds.bottom - (this.diameterLarge / 2.0f)) - this.padBottom;
            rendererContext.canvasMatrix.setToIdentity();
            Canvas canvas = rendererContext.canvas;
            float[] fArr = this.buttonCenter;
            float f = interpolatedDiameter / 2.0f;
            canvas.drawCircle(fArr[0], fArr[1], f, this.shadePaint);
            Canvas canvas2 = rendererContext.canvas;
            float[] fArr2 = this.buttonCenter;
            canvas2.drawCircle(fArr2[0], fArr2[1], f, this.outlinePaint);
            rendererContext.canvas.translate(this.bounds.centerX(), (this.bounds.bottom - (this.diameterLarge / 2.0f)) - this.padBottom);
            Canvas canvas3 = rendererContext.canvas;
            int i2 = this.trashSize;
            canvas3.translate(-(((float) i2) / 2.0f), -(((float) i2) / 2.0f));
            drawable.draw(rendererContext.canvas);
            rendererContext.canvas.restore();
            if (currentTimeMillis - 150 < this.startTime) {
                invalidate();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    private final float getInterpolatedDiameter(long j) {
        if (j < 150) {
            float interpolation = this.interpolator.getInterpolation(((float) j) / 150.0f);
            if (this.isExpanding) {
                return interpolateFromFraction(interpolation);
            }
            return interpolateFromFraction(((float) 1) - interpolation);
        } else if (this.isExpanding) {
            return this.diameterLarge;
        } else {
            return this.diameterSmall;
        }
    }

    private final float interpolateFromFraction(float f) {
        float f2 = this.diameterSmall;
        return f2 + ((this.diameterLarge - f2) * f);
    }

    public final void expand() {
        if (!this.isExpanding) {
            this.isExpanding = true;
            this.startTime = System.currentTimeMillis();
            invalidate();
        }
    }

    public final void shrink() {
        if (this.isExpanding) {
            this.isExpanding = false;
            this.startTime = System.currentTimeMillis();
            invalidate();
        }
    }

    @Override // org.signal.imageeditor.core.Renderer
    public boolean hitTest(float f, float f2) {
        float[] fArr = this.buttonCenter;
        float f3 = f - fArr[0];
        float f4 = f2 - fArr[1];
        float f5 = this.diameterLarge / ((float) 2);
        if ((f3 * f3) + (f4 * f4) < f5 * f5) {
            return true;
        }
        return false;
    }

    /* compiled from: TrashRenderer.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lorg/signal/imageeditor/core/renderers/TrashRenderer$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lorg/signal/imageeditor/core/renderers/TrashRenderer;", "DURATION", "", "image-editor_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
