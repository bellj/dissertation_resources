package org.signal.imageeditor.core;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.RectF;

/* loaded from: classes3.dex */
public final class CanvasMatrix {
    private final Canvas canvas;
    private final Matrix canvasMatrix = new Matrix();
    private final Matrix[] stack = new Matrix[16];
    private int stackHeight;
    private final Matrix temp = new Matrix();

    public CanvasMatrix(Canvas canvas) {
        this.canvas = canvas;
        int i = 0;
        while (true) {
            Matrix[] matrixArr = this.stack;
            if (i < matrixArr.length) {
                matrixArr[i] = new Matrix();
                i++;
            } else {
                return;
            }
        }
    }

    public void concat(Matrix matrix) {
        this.canvas.concat(matrix);
        this.canvasMatrix.preConcat(matrix);
    }

    public void save() {
        this.canvas.save();
        int i = this.stackHeight;
        if (i != 16) {
            Matrix[] matrixArr = this.stack;
            this.stackHeight = i + 1;
            matrixArr[i].set(this.canvasMatrix);
            return;
        }
        throw new AssertionError("Not enough space on stack");
    }

    public void restore() {
        this.canvas.restore();
        Matrix matrix = this.canvasMatrix;
        Matrix[] matrixArr = this.stack;
        int i = this.stackHeight - 1;
        this.stackHeight = i;
        matrix.set(matrixArr[i]);
    }

    public void setToIdentity() {
        if (this.canvasMatrix.invert(this.temp)) {
            concat(this.temp);
        }
    }

    public void initial(Matrix matrix) {
        concat(matrix);
    }

    public boolean mapRect(RectF rectF, RectF rectF2) {
        return this.canvasMatrix.mapRect(rectF, rectF2);
    }

    public void mapPoints(float[] fArr, float[] fArr2) {
        this.canvasMatrix.mapPoints(fArr, fArr2);
    }

    public void copyTo(Matrix matrix) {
        matrix.set(this.canvasMatrix);
    }
}
