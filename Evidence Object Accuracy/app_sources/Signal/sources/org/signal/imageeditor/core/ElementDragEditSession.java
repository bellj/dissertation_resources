package org.signal.imageeditor.core;

import android.graphics.Matrix;
import android.graphics.PointF;
import org.signal.imageeditor.core.model.EditorElement;

/* loaded from: classes3.dex */
public final class ElementDragEditSession extends ElementEditSession {
    @Override // org.signal.imageeditor.core.EditSession
    public EditSession removePoint(Matrix matrix, int i) {
        return this;
    }

    private ElementDragEditSession(EditorElement editorElement, Matrix matrix) {
        super(editorElement, matrix);
    }

    public static ElementDragEditSession startDrag(EditorElement editorElement, Matrix matrix, PointF pointF) {
        if (!editorElement.getFlags().isEditable()) {
            return null;
        }
        ElementDragEditSession elementDragEditSession = new ElementDragEditSession(editorElement, matrix);
        elementDragEditSession.setScreenStartPoint(0, pointF);
        elementDragEditSession.setScreenEndPoint(0, pointF);
        return elementDragEditSession;
    }

    @Override // org.signal.imageeditor.core.EditSession
    public void movePoint(int i, PointF pointF) {
        setScreenEndPoint(i, pointF);
        Matrix editorMatrix = this.selected.getEditorMatrix();
        PointF pointF2 = this.endPointElement[0];
        float f = pointF2.x;
        PointF pointF3 = this.startPointElement[0];
        editorMatrix.setTranslate(f - pointF3.x, pointF2.y - pointF3.y);
    }

    @Override // org.signal.imageeditor.core.EditSession
    public EditSession newPoint(Matrix matrix, PointF pointF, int i) {
        return ElementScaleEditSession.startScale(this, matrix, pointF, i);
    }
}
