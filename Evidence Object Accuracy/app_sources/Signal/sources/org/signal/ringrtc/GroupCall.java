package org.signal.ringrtc;

import android.util.LongSparseArray;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import org.signal.ringrtc.CallManager;
import org.webrtc.AudioSource;
import org.webrtc.AudioTrack;
import org.webrtc.MediaConstraints;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.VideoSink;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;

/* loaded from: classes3.dex */
public final class GroupCall {
    private static final String TAG;
    long clientId;
    private boolean disconnectCalled = false;
    private PeerConnectionFactory factory;
    private boolean handleEndedCalled = false;
    private ArrayList<VideoTrack> incomingVideoTracks;
    private LocalDeviceState localDeviceState = new LocalDeviceState();
    private long nativeCallManager;
    private Observer observer;
    private AudioSource outgoingAudioSource;
    private AudioTrack outgoingAudioTrack;
    private VideoSource outgoingVideoSource;
    private VideoTrack outgoingVideoTrack;
    private PeekInfo peekInfo;
    private LongSparseArray<RemoteDeviceState> remoteDeviceStates;

    /* loaded from: classes3.dex */
    public interface Observer {
        void onAudioLevels(GroupCall groupCall);

        void onEnded(GroupCall groupCall, GroupCallEndReason groupCallEndReason);

        void onLocalDeviceStateChanged(GroupCall groupCall);

        void onPeekChanged(GroupCall groupCall);

        void onRemoteDeviceStatesChanged(GroupCall groupCall);

        void requestGroupMembers(GroupCall groupCall);

        void requestMembershipProof(GroupCall groupCall);
    }

    private native void ringrtcConnect(long j, long j2) throws CallException;

    private native long ringrtcCreateGroupCallClient(long j, byte[] bArr, String str, byte[] bArr2, int i, long j2, long j3, long j4) throws CallException;

    private native void ringrtcDeleteGroupCallClient(long j, long j2) throws CallException;

    private native void ringrtcDisconnect(long j, long j2) throws CallException;

    private native void ringrtcJoin(long j, long j2) throws CallException;

    private native void ringrtcLeave(long j, long j2) throws CallException;

    private native void ringrtcRequestVideo(long j, long j2, List<VideoRequest> list) throws CallException;

    private native void ringrtcResendMediaKeys(long j, long j2) throws CallException;

    private native void ringrtcRing(long j, long j2, byte[] bArr) throws CallException;

    private native void ringrtcSetBandwidthMode(long j, long j2, int i) throws CallException;

    private native void ringrtcSetGroupMembers(long j, long j2, byte[] bArr) throws CallException;

    private native void ringrtcSetMembershipProof(long j, long j2, byte[] bArr) throws CallException;

    private native void ringrtcSetOutgoingAudioMuted(long j, long j2, boolean z) throws CallException;

    private native void ringrtcSetOutgoingVideoMuted(long j, long j2, boolean z) throws CallException;

    public GroupCall(long j, byte[] bArr, String str, byte[] bArr2, Integer num, PeerConnectionFactory peerConnectionFactory, Observer observer) {
        int i;
        Log.i(TAG, "GroupCall():");
        this.nativeCallManager = j;
        this.factory = peerConnectionFactory;
        this.observer = observer;
        AudioSource createAudioSource = peerConnectionFactory.createAudioSource(new MediaConstraints());
        this.outgoingAudioSource = createAudioSource;
        if (createAudioSource != null) {
            AudioTrack createAudioTrack = peerConnectionFactory.createAudioTrack("audio1", createAudioSource);
            this.outgoingAudioTrack = createAudioTrack;
            if (createAudioTrack != null) {
                createAudioTrack.setEnabled(false);
                VideoSource createVideoSource = peerConnectionFactory.createVideoSource(false);
                this.outgoingVideoSource = createVideoSource;
                if (createVideoSource != null) {
                    VideoTrack createVideoTrack = peerConnectionFactory.createVideoTrack("video1", createVideoSource);
                    this.outgoingVideoTrack = createVideoTrack;
                    if (createVideoTrack != null) {
                        createVideoTrack.setEnabled(false);
                        this.outgoingVideoSource.adaptOutputFormat(640, 360, 30);
                        this.incomingVideoTracks = new ArrayList<>();
                        if (num == null) {
                            i = 0;
                        } else {
                            i = num.intValue();
                        }
                        try {
                            this.clientId = ringrtcCreateGroupCallClient(j, bArr, str, bArr2, i, peerConnectionFactory.getNativePeerConnectionFactory(), this.outgoingAudioTrack.getNativeAudioTrack(), this.outgoingVideoTrack.getNativeVideoTrack());
                        } catch (CallException e) {
                            Log.w(TAG, "Unable to create group call client", e);
                            throw new AssertionError("Unable to create group call client");
                        }
                    }
                }
            }
        }
    }

    public void dispose() throws CallException {
        Log.i(TAG, "dispose():");
        long j = this.clientId;
        if (j != 0) {
            ringrtcDeleteGroupCallClient(this.nativeCallManager, j);
            this.clientId = 0;
        }
        AudioTrack audioTrack = this.outgoingAudioTrack;
        if (audioTrack != null) {
            audioTrack.dispose();
            this.outgoingAudioTrack = null;
        }
        VideoTrack videoTrack = this.outgoingVideoTrack;
        if (videoTrack != null) {
            videoTrack.dispose();
            this.outgoingVideoTrack = null;
        }
        Iterator<VideoTrack> it = this.incomingVideoTracks.iterator();
        while (it.hasNext()) {
            it.next().dispose();
        }
    }

    public void connect() throws CallException {
        Log.i(TAG, "connect():");
        ringrtcConnect(this.nativeCallManager, this.clientId);
    }

    public void join() throws CallException {
        Log.i(TAG, "join():");
        ringrtcJoin(this.nativeCallManager, this.clientId);
    }

    public void leave() throws CallException {
        Log.i(TAG, "leave():");
        this.outgoingAudioTrack.setEnabled(false);
        this.outgoingVideoTrack.setEnabled(false);
        ringrtcLeave(this.nativeCallManager, this.clientId);
    }

    public void disconnect() throws CallException {
        Log.i(TAG, "disconnect():");
        if (!this.disconnectCalled) {
            this.disconnectCalled = true;
            if (this.handleEndedCalled) {
                dispose();
                return;
            }
            this.outgoingAudioTrack.setEnabled(false);
            this.outgoingVideoTrack.setEnabled(false);
            ringrtcDisconnect(this.nativeCallManager, this.clientId);
        }
    }

    public LocalDeviceState getLocalDeviceState() {
        Log.i(TAG, "getLocalDevice():");
        return this.localDeviceState;
    }

    public LongSparseArray<RemoteDeviceState> getRemoteDeviceStates() {
        Log.i(TAG, "getRemoteDevices():");
        return this.remoteDeviceStates;
    }

    public PeekInfo getPeekInfo() {
        Log.i(TAG, "getPeekInfo():");
        return this.peekInfo;
    }

    public void setOutgoingAudioMuted(boolean z) throws CallException {
        Log.i(TAG, "setOutgoingAudioMuted():");
        this.localDeviceState.audioMuted = z;
        this.outgoingAudioTrack.setEnabled(!z);
        ringrtcSetOutgoingAudioMuted(this.nativeCallManager, this.clientId, z);
    }

    public void setOutgoingVideoMuted(boolean z) throws CallException {
        Log.i(TAG, "setOutgoingVideoMuted():");
        this.localDeviceState.videoMuted = z;
        this.outgoingVideoTrack.setEnabled(!z);
        ringrtcSetOutgoingVideoMuted(this.nativeCallManager, this.clientId, z);
    }

    public void setOutgoingVideoSource(VideoSink videoSink, CameraControl cameraControl) {
        Log.i(TAG, "setOutgoingVideoSource():");
        if (cameraControl.hasCapturer()) {
            cameraControl.initCapturer(this.outgoingVideoSource.getCapturerObserver());
            this.outgoingVideoTrack.addSink(videoSink);
        }
    }

    public void ringAll() throws CallException {
        Log.i(TAG, "ring():");
        ringrtcRing(this.nativeCallManager, this.clientId, null);
    }

    public void resendMediaKeys() throws CallException {
        Log.i(TAG, "resendMediaKeys():");
        ringrtcResendMediaKeys(this.nativeCallManager, this.clientId);
    }

    public void setBandwidthMode(CallManager.BandwidthMode bandwidthMode) throws CallException {
        Log.i(TAG, "setBandwidthMode():");
        ringrtcSetBandwidthMode(this.nativeCallManager, this.clientId, bandwidthMode.ordinal());
    }

    public void requestVideo(Collection<VideoRequest> collection) throws CallException {
        Log.i(TAG, "requestVideo():");
        ringrtcRequestVideo(this.nativeCallManager, this.clientId, new ArrayList(collection));
    }

    public void setGroupMembers(Collection<GroupMemberInfo> collection) throws CallException {
        Log.i(TAG, "setGroupMembers():");
        ringrtcSetGroupMembers(this.nativeCallManager, this.clientId, Util.serializeFromGroupMemberInfo(collection));
    }

    public void setMembershipProof(byte[] bArr) throws CallException {
        Log.i(TAG, "setMembershipProof():");
        ringrtcSetMembershipProof(this.nativeCallManager, this.clientId, bArr);
    }

    public void requestMembershipProof() {
        Log.i(TAG, "requestMembershipProof():");
        this.observer.requestMembershipProof(this);
    }

    public void requestGroupMembers() {
        Log.i(TAG, "handleGroupMembers():");
        this.observer.requestGroupMembers(this);
    }

    public void handleConnectionStateChanged(ConnectionState connectionState) {
        Log.i(TAG, "handleConnectionStateChanged():");
        LocalDeviceState localDeviceState = new LocalDeviceState(this.localDeviceState);
        localDeviceState.connectionState = connectionState;
        this.localDeviceState = localDeviceState;
        this.observer.onLocalDeviceStateChanged(this);
    }

    public void handleJoinStateChanged(JoinState joinState) {
        Log.i(TAG, "handleJoinStateChanged():");
        LocalDeviceState localDeviceState = new LocalDeviceState(this.localDeviceState);
        localDeviceState.joinState = joinState;
        this.localDeviceState = localDeviceState;
        this.observer.onLocalDeviceStateChanged(this);
    }

    public void handleNetworkRouteChanged(NetworkRoute networkRoute) {
        Log.i(TAG, "handleNetworkRouteChanged():");
        LocalDeviceState localDeviceState = new LocalDeviceState(this.localDeviceState);
        localDeviceState.networkRoute = networkRoute;
        this.localDeviceState = localDeviceState;
        this.observer.onLocalDeviceStateChanged(this);
    }

    public void handleAudioLevels(int i, List<ReceivedAudioLevel> list) {
        Log.d(TAG, "handleAudioLevels():");
        this.localDeviceState.audioLevel = i;
        for (ReceivedAudioLevel receivedAudioLevel : list) {
            RemoteDeviceState remoteDeviceState = this.remoteDeviceStates.get(receivedAudioLevel.demuxId);
            if (remoteDeviceState != null) {
                remoteDeviceState.audioLevel = receivedAudioLevel.level;
            }
        }
        this.observer.onAudioLevels(this);
    }

    public void handleRemoteDevicesChanged(List<RemoteDeviceState> list) {
        RemoteDeviceState remoteDeviceState;
        Log.i(TAG, "handleRemoteDevicesChanged():");
        LongSparseArray<RemoteDeviceState> longSparseArray = new LongSparseArray<>();
        for (RemoteDeviceState remoteDeviceState2 : list) {
            remoteDeviceState2.userId = Util.getUuidFromBytes(remoteDeviceState2.userIdByteArray);
            LongSparseArray<RemoteDeviceState> longSparseArray2 = this.remoteDeviceStates;
            if (!(longSparseArray2 == null || (remoteDeviceState = longSparseArray2.get(remoteDeviceState2.demuxId)) == null)) {
                remoteDeviceState2.videoTrack = remoteDeviceState.videoTrack;
                remoteDeviceState2.audioLevel = remoteDeviceState.audioLevel;
            }
            longSparseArray.put(remoteDeviceState2.demuxId, remoteDeviceState2);
        }
        this.remoteDeviceStates = longSparseArray;
        this.observer.onRemoteDeviceStatesChanged(this);
    }

    public void handleIncomingVideoTrack(long j, long j2) {
        String str = TAG;
        Log.i(str, "handleIncomingVideoTrack():");
        if (j2 == 0) {
            Log.d(str, "nativeVideoTrackOwnedRc is null (0)");
            return;
        }
        RemoteDeviceState remoteDeviceState = this.remoteDeviceStates.get(j);
        if (remoteDeviceState == null) {
            Log.d(str, "No remote device state found for remoteDemuxId");
            return;
        }
        VideoTrack videoTrack = new VideoTrack(j2);
        remoteDeviceState.videoTrack = videoTrack;
        this.incomingVideoTracks.add(videoTrack);
        this.observer.onRemoteDeviceStatesChanged(this);
    }

    public void handlePeekChanged(PeekInfo peekInfo) {
        Log.i(TAG, "handlePeekChanged():");
        this.peekInfo = peekInfo;
        this.observer.onPeekChanged(this);
    }

    public void handleEnded(GroupCallEndReason groupCallEndReason) {
        Log.i(TAG, "handleEnded():");
        if (!this.handleEndedCalled) {
            this.handleEndedCalled = true;
            this.observer.onEnded(this, groupCallEndReason);
            try {
                if (this.disconnectCalled) {
                    dispose();
                }
            } catch (CallException e) {
                String str = TAG;
                Log.w(str, "Unable to delete group call clientId: " + this.clientId, e);
            }
        }
    }

    /* loaded from: classes3.dex */
    public enum ConnectionState {
        NOT_CONNECTED,
        CONNECTING,
        CONNECTED,
        RECONNECTING;

        @CalledByNative
        static ConnectionState fromNativeIndex(int i) {
            return values()[i];
        }
    }

    /* loaded from: classes3.dex */
    public enum JoinState {
        NOT_JOINED,
        JOINING,
        JOINED;

        @CalledByNative
        static JoinState fromNativeIndex(int i) {
            return values()[i];
        }
    }

    /* loaded from: classes3.dex */
    public enum GroupCallEndReason {
        DEVICE_EXPLICITLY_DISCONNECTED,
        SERVER_EXPLICITLY_DISCONNECTED,
        CALL_MANAGER_IS_BUSY,
        SFU_CLIENT_FAILED_TO_JOIN,
        FAILED_TO_CREATE_PEER_CONNECTION_FACTORY,
        FAILED_TO_NEGOTIATE_SRTP_KEYS,
        FAILED_TO_CREATE_PEER_CONNECTION,
        FAILED_TO_START_PEER_CONNECTION,
        FAILED_TO_UPDATE_PEER_CONNECTION,
        FAILED_TO_SET_MAX_SEND_BITRATE,
        ICE_FAILED_WHILE_CONNECTING,
        ICE_FAILED_AFTER_CONNECTED,
        SERVER_CHANGED_DEMUXID,
        HAS_MAX_DEVICES;

        @CalledByNative
        static GroupCallEndReason fromNativeIndex(int i) {
            return values()[i];
        }
    }

    /* loaded from: classes3.dex */
    public class LocalDeviceState {
        int audioLevel;
        boolean audioMuted;
        ConnectionState connectionState;
        JoinState joinState;
        NetworkRoute networkRoute;
        boolean videoMuted;

        public LocalDeviceState() {
            GroupCall.this = r1;
            this.connectionState = ConnectionState.NOT_CONNECTED;
            this.joinState = JoinState.NOT_JOINED;
            this.audioMuted = true;
            this.videoMuted = true;
            this.networkRoute = new NetworkRoute();
            this.audioLevel = 0;
        }

        public LocalDeviceState(LocalDeviceState localDeviceState) {
            GroupCall.this = r1;
            this.connectionState = localDeviceState.connectionState;
            this.joinState = localDeviceState.joinState;
            this.audioMuted = localDeviceState.audioMuted;
            this.videoMuted = localDeviceState.videoMuted;
            this.networkRoute = localDeviceState.networkRoute;
            this.audioLevel = localDeviceState.audioLevel;
        }

        public ConnectionState getConnectionState() {
            return this.connectionState;
        }

        public JoinState getJoinState() {
            return this.joinState;
        }

        public boolean getAudioMuted() {
            return this.audioMuted;
        }

        public boolean getVideoMuted() {
            return this.videoMuted;
        }

        public NetworkRoute getNetworkRoute() {
            return this.networkRoute;
        }

        public int getAudioLevel() {
            return this.audioLevel;
        }
    }

    /* loaded from: classes3.dex */
    public static class RemoteDeviceState {
        long addedTime;
        int audioLevel = 0;
        Boolean audioMuted;
        long demuxId;
        Boolean forwardingVideo;
        boolean mediaKeysReceived;
        Boolean presenting;
        Boolean sharingScreen;
        long speakerTime;
        UUID userId;
        byte[] userIdByteArray;
        Boolean videoMuted;
        VideoTrack videoTrack;

        public RemoteDeviceState(long j, byte[] bArr, boolean z, Boolean bool, Boolean bool2, Boolean bool3, Boolean bool4, long j2, long j3, Boolean bool5) {
            this.demuxId = j;
            this.userIdByteArray = bArr;
            this.mediaKeysReceived = z;
            this.audioMuted = bool;
            this.videoMuted = bool2;
            this.presenting = bool3;
            this.sharingScreen = bool4;
            this.addedTime = j2;
            this.speakerTime = j3;
            this.forwardingVideo = bool5;
        }

        public long getDemuxId() {
            return this.demuxId;
        }

        public UUID getUserId() {
            return this.userId;
        }

        public boolean getMediaKeysReceived() {
            return this.mediaKeysReceived;
        }

        public Boolean getAudioMuted() {
            return this.audioMuted;
        }

        public Boolean getVideoMuted() {
            return this.videoMuted;
        }

        public Boolean getPresenting() {
            return this.presenting;
        }

        public Boolean getSharingScreen() {
            return this.sharingScreen;
        }

        public long getAddedTime() {
            return this.addedTime;
        }

        public long getSpeakerTime() {
            return this.speakerTime;
        }

        public Boolean getForwardingVideo() {
            return this.forwardingVideo;
        }

        public VideoTrack getVideoTrack() {
            return this.videoTrack;
        }

        public int getAudioLevel() {
            return this.audioLevel;
        }
    }

    /* loaded from: classes3.dex */
    public static class ReceivedAudioLevel {
        public long demuxId;
        public int level;

        public ReceivedAudioLevel(long j, int i) {
            this.demuxId = j;
            this.level = i;
        }
    }

    /* loaded from: classes3.dex */
    public static class GroupMemberInfo {
        UUID userId;
        byte[] userIdCipherText;

        public GroupMemberInfo(UUID uuid, byte[] bArr) {
            this.userId = uuid;
            this.userIdCipherText = bArr;
        }
    }

    /* loaded from: classes3.dex */
    public static class VideoRequest {
        long demuxId;
        Integer framerate;
        int height;
        int width;

        public VideoRequest(long j, int i, int i2, Integer num) {
            this.demuxId = j;
            this.width = i;
            this.height = i2;
            this.framerate = num;
        }
    }
}
