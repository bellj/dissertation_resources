package org.signal.ringrtc;

import org.webrtc.Loggable;
import org.webrtc.Logging;

/* loaded from: classes3.dex */
public final class WebRtcLogger implements Loggable {
    private static final String TAG = Log.class.getSimpleName();

    /* renamed from: org.signal.ringrtc.WebRtcLogger$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$webrtc$Logging$Severity;

        static {
            int[] iArr = new int[Logging.Severity.values().length];
            $SwitchMap$org$webrtc$Logging$Severity = iArr;
            try {
                iArr[Logging.Severity.LS_NONE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$webrtc$Logging$Severity[Logging.Severity.LS_ERROR.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$webrtc$Logging$Severity[Logging.Severity.LS_WARNING.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$webrtc$Logging$Severity[Logging.Severity.LS_INFO.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$webrtc$Logging$Severity[Logging.Severity.LS_VERBOSE.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
        }
    }

    @Override // org.webrtc.Loggable
    public void onLogMessage(String str, Logging.Severity severity, String str2) {
        int i = AnonymousClass1.$SwitchMap$org$webrtc$Logging$Severity[severity.ordinal()];
        if (i == 1) {
            return;
        }
        if (i == 2) {
            Log.e(str2, str);
        } else if (i == 3) {
            Log.w(str2, str);
        } else if (i == 4) {
            Log.i(str2, str);
        } else if (i != 5) {
            String str3 = TAG;
            Log.w(str3, "Unknown log level: " + str2 + ", " + str);
        } else {
            Log.d(str2, str);
        }
    }
}
