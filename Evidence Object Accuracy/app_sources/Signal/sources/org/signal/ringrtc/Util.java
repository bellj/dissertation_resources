package org.signal.ringrtc;

import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.UUID;
import org.signal.ringrtc.GroupCall;

/* loaded from: classes3.dex */
public final class Util {
    public static byte[] getBytesFromUuid(UUID uuid) {
        ByteBuffer wrap = ByteBuffer.wrap(new byte[16]);
        wrap.putLong(uuid.getMostSignificantBits());
        wrap.putLong(uuid.getLeastSignificantBits());
        return wrap.array();
    }

    public static UUID getUuidFromBytes(byte[] bArr) {
        ByteBuffer wrap = ByteBuffer.wrap(bArr);
        return new UUID(Long.valueOf(wrap.getLong()).longValue(), Long.valueOf(wrap.getLong()).longValue());
    }

    public static byte[] serializeFromGroupMemberInfo(Collection<GroupCall.GroupMemberInfo> collection) {
        if (collection == null || collection.size() <= 0) {
            return new byte[0];
        }
        byte[] bArr = new byte[collection.size() * 81];
        int i = 0;
        for (GroupCall.GroupMemberInfo groupMemberInfo : collection) {
            System.arraycopy(getBytesFromUuid(groupMemberInfo.userId), 0, bArr, i, 16);
            int i2 = i + 16;
            System.arraycopy(groupMemberInfo.userIdCipherText, 0, bArr, i2, 65);
            i = i2 + 65;
        }
        return bArr;
    }
}
