package org.signal.ringrtc;

/* loaded from: classes3.dex */
public final class CallId implements Comparable<CallId> {
    private final String TAG = CallId.class.getSimpleName();
    private final Long callId;

    public CallId(Long l) {
        this.callId = l;
    }

    public Long longValue() {
        return this.callId;
    }

    public String format(Integer num) {
        return this + "-" + num;
    }

    @Override // java.lang.Object
    public String toString() {
        return "0x" + Long.toHexString(this.callId.longValue());
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj != null && CallId.class == obj.getClass() && compareTo((CallId) obj) == 0) {
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return this.callId.hashCode();
    }

    public int compareTo(CallId callId) {
        return this.callId.compareTo(callId.callId);
    }
}
