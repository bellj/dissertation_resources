package org.signal.ringrtc;

/* loaded from: classes3.dex */
public class CallException extends Exception {
    public CallException() {
    }

    public CallException(String str) {
        super(str);
    }

    public CallException(String str, Throwable th) {
        super(str, th);
    }

    public CallException(Throwable th) {
        super(th);
    }
}
