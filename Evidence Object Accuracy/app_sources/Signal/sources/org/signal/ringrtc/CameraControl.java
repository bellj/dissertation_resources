package org.signal.ringrtc;

import org.webrtc.CapturerObserver;

/* loaded from: classes3.dex */
public interface CameraControl {
    void flip();

    boolean hasCapturer();

    void initCapturer(CapturerObserver capturerObserver);

    void setEnabled(boolean z);

    void setOrientation(Integer num);
}
