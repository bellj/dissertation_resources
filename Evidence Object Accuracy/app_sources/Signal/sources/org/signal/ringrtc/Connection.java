package org.signal.ringrtc;

import org.webrtc.AudioSource;
import org.webrtc.AudioTrack;
import org.webrtc.NativePeerConnectionFactory;
import org.webrtc.PeerConnection;

/* loaded from: classes3.dex */
public class Connection extends PeerConnection {
    private static final String TAG;
    private AudioSource audioSource;
    private AudioTrack audioTrack;
    private final CallId callId;
    private long nativePeerConnection;
    private int remoteDevice;

    public Connection(NativeFactory nativeFactory) {
        super(nativeFactory);
        this.nativePeerConnection = nativeFactory.nativePeerConnection;
        CallId callId = nativeFactory.callId;
        this.callId = callId;
        this.remoteDevice = nativeFactory.remoteDevice;
        String str = TAG;
        Log.i(str, "ctor(): connectionId: " + callId.format(Integer.valueOf(this.remoteDevice)));
    }

    public String toString() {
        return this.callId.format(Integer.valueOf(this.remoteDevice));
    }

    private void checkConnectionExists() {
        if (this.nativePeerConnection == 0) {
            throw new IllegalStateException("Connection has been closed.");
        }
    }

    public void setAudioSource(AudioSource audioSource, AudioTrack audioTrack) {
        checkConnectionExists();
        this.audioSource = audioSource;
        this.audioTrack = audioTrack;
    }

    public void setAudioEnabled(boolean z) {
        String str = TAG;
        Log.i(str, "audioTrack.setEnabled(" + z + ")");
        this.audioTrack.setEnabled(z);
    }

    public void shutdown() {
        checkConnectionExists();
        this.audioSource.dispose();
        try {
            String str = TAG;
            Log.i(str, "Connection.shutdown(): calling super.close()");
            close();
            Log.i(str, "Connection.shutdown(): calling super.dispose()");
            dispose();
            Log.i(str, "Connection.shutdown(): after calling super.dispose()");
        } catch (Exception e) {
            Log.e(TAG, "Problem closing PeerConnection: ", e);
        }
        this.nativePeerConnection = 0;
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes3.dex */
    public static class NativeFactory implements NativePeerConnectionFactory {
        private CallId callId;
        private long nativePeerConnection;
        private int remoteDevice;

        public NativeFactory(long j, CallId callId, int i) {
            this.nativePeerConnection = j;
            this.callId = callId;
            this.remoteDevice = i;
        }

        @Override // org.webrtc.NativePeerConnectionFactory
        public long createNativePeerConnection() {
            return this.nativePeerConnection;
        }
    }
}
