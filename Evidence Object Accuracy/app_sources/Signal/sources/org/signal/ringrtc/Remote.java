package org.signal.ringrtc;

/* loaded from: classes3.dex */
public interface Remote {
    boolean recipientEquals(Remote remote);
}
