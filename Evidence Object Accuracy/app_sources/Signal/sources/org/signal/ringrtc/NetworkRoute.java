package org.signal.ringrtc;

import org.webrtc.PeerConnection;

/* loaded from: classes3.dex */
public class NetworkRoute {
    PeerConnection.AdapterType localAdapterType;

    public NetworkRoute() {
        this.localAdapterType = PeerConnection.AdapterType.UNKNOWN;
    }

    public NetworkRoute(PeerConnection.AdapterType adapterType) {
        this.localAdapterType = adapterType;
    }

    public PeerConnection.AdapterType getLocalAdapterType() {
        return this.localAdapterType;
    }
}
