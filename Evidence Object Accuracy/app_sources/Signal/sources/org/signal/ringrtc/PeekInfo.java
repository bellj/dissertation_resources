package org.signal.ringrtc;

import java.util.Collection;
import java.util.UUID;

/* loaded from: classes3.dex */
public final class PeekInfo {
    private final String TAG = PeekInfo.class.getSimpleName();
    private final UUID creator;
    private final long deviceCount;
    private final String eraId;
    private final Collection<UUID> joinedMembers;
    private final Long maxDevices;

    public PeekInfo(Collection<UUID> collection, UUID uuid, String str, Long l, long j) {
        this.joinedMembers = collection;
        this.creator = uuid;
        this.eraId = str;
        this.maxDevices = l;
        this.deviceCount = j;
    }

    public Collection<UUID> getJoinedMembers() {
        return this.joinedMembers;
    }

    public UUID getCreator() {
        return this.creator;
    }

    public String getEraId() {
        return this.eraId;
    }

    public Long getMaxDevices() {
        return this.maxDevices;
    }

    public long getDeviceCount() {
        return this.deviceCount;
    }
}
