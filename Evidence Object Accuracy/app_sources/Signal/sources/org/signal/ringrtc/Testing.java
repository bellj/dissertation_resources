package org.signal.ringrtc;

import java.io.IOException;
import org.whispersystems.libsignal.IdentityKey;
import org.whispersystems.libsignal.ecc.Curve;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.push.exceptions.UnregisteredUserException;

/* loaded from: classes3.dex */
public class Testing {
    public static IOException fakeIOException() {
        return new IOException("fake network problem");
    }

    public static UnregisteredUserException fakeUnregisteredUserException() {
        return new UnregisteredUserException("+123456", new IOException("fake unregistered user problem"));
    }

    public static UntrustedIdentityException fakeUntrustedIdentityException() {
        return new UntrustedIdentityException("fake identity problem", "+123456", new IdentityKey(Curve.generateKeyPair().getPublicKey()));
    }
}
