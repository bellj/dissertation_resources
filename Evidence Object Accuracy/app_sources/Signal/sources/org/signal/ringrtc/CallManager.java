package org.signal.ringrtc;

import android.content.Context;
import android.os.Build;
import android.util.LongSparseArray;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import org.signal.ringrtc.Connection;
import org.signal.ringrtc.GroupCall;
import org.signal.ringrtc.Log;
import org.webrtc.AudioSource;
import org.webrtc.AudioTrack;
import org.webrtc.ContextUtils;
import org.webrtc.DefaultVideoDecoderFactory;
import org.webrtc.DefaultVideoEncoderFactory;
import org.webrtc.EglBase;
import org.webrtc.Logging;
import org.webrtc.MediaConstraints;
import org.webrtc.MediaStream;
import org.webrtc.NativeLibraryLoader;
import org.webrtc.PeerConnection;
import org.webrtc.PeerConnectionFactory;
import org.webrtc.SoftwareVideoDecoderFactory;
import org.webrtc.SoftwareVideoEncoderFactory;
import org.webrtc.VideoDecoderFactory;
import org.webrtc.VideoEncoderFactory;
import org.webrtc.VideoSink;
import org.webrtc.VideoSource;
import org.webrtc.VideoTrack;
import org.webrtc.audio.JavaAudioDeviceModule;

/* loaded from: classes.dex */
public class CallManager {
    private static final String TAG;
    private static boolean isInitialized;
    private LongSparseArray<GroupCall> groupCallByClientId = new LongSparseArray<>();
    private PeerConnectionFactory groupFactory;
    private long nativeCallManager = 0;
    private Observer observer;
    private Requests<PeekInfo> peekInfoRequests = new Requests<>();

    /* loaded from: classes3.dex */
    public enum AudioProcessingMethod {
        Default,
        ForceHardware,
        ForceSoftwareAec3,
        ForceSoftwareAecM
    }

    /* loaded from: classes3.dex */
    public enum CallMessageUrgency {
        DROPPABLE,
        HANDLE_IMMEDIATELY
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes3.dex */
    public static class NoOpLoader implements NativeLibraryLoader {
        @Override // org.webrtc.NativeLibraryLoader
        public boolean load(String str) {
            return true;
        }
    }

    /* loaded from: classes3.dex */
    public interface Observer {
        void onAudioLevels(Remote remote, int i, int i2);

        void onCallConcluded(Remote remote);

        void onCallEvent(Remote remote, CallEvent callEvent);

        void onGroupCallRingUpdate(byte[] bArr, long j, UUID uuid, RingUpdate ringUpdate);

        void onNetworkRouteChanged(Remote remote, NetworkRoute networkRoute);

        void onSendAnswer(CallId callId, Remote remote, Integer num, Boolean bool, byte[] bArr);

        void onSendBusy(CallId callId, Remote remote, Integer num, Boolean bool);

        void onSendCallMessage(UUID uuid, byte[] bArr, CallMessageUrgency callMessageUrgency);

        void onSendCallMessageToGroup(byte[] bArr, byte[] bArr2, CallMessageUrgency callMessageUrgency);

        void onSendHangup(CallId callId, Remote remote, Integer num, Boolean bool, HangupType hangupType, Integer num2);

        void onSendHttpRequest(long j, String str, HttpMethod httpMethod, List<HttpHeader> list, byte[] bArr);

        void onSendIceCandidates(CallId callId, Remote remote, Integer num, Boolean bool, List<byte[]> list);

        void onSendOffer(CallId callId, Remote remote, Integer num, Boolean bool, byte[] bArr, CallMediaType callMediaType);

        void onStartCall(Remote remote, CallId callId, Boolean bool, CallMediaType callMediaType);
    }

    /* loaded from: classes3.dex */
    public interface ResponseHandler<T> {
        void handleResponse(T t);
    }

    /* loaded from: classes3.dex */
    public enum RingCancelReason {
        DeclinedByUser,
        Busy
    }

    /* loaded from: classes3.dex */
    public enum RingUpdate {
        REQUESTED,
        EXPIRED_REQUEST,
        ACCEPTED_ON_ANOTHER_DEVICE,
        DECLINED_ON_ANOTHER_DEVICE,
        BUSY_LOCALLY,
        BUSY_ON_ANOTHER_DEVICE,
        CANCELLED_BY_RINGER
    }

    private native void ringrtcAcceptCall(long j, long j2) throws CallException;

    private native void ringrtcCall(long j, Remote remote, int i, int i2) throws CallException;

    private native void ringrtcCancelGroupRing(long j, byte[] bArr, long j2, int i) throws CallException;

    private native void ringrtcClose(long j) throws CallException;

    private static native long ringrtcCreateCallManager(CallManager callManager) throws CallException;

    private native long ringrtcCreatePeerConnection(long j, long j2, PeerConnection.RTCConfiguration rTCConfiguration, MediaConstraints mediaConstraints) throws CallException;

    private native void ringrtcDrop(long j, long j2) throws CallException;

    private native CallContext ringrtcGetActiveCallContext(long j) throws CallException;

    private native Connection ringrtcGetActiveConnection(long j) throws CallException;

    private static native BuildInfo ringrtcGetBuildInfo() throws CallException;

    private native void ringrtcHangup(long j) throws CallException;

    private native void ringrtcHttpRequestFailed(long j, long j2) throws CallException;

    private static native void ringrtcInitialize() throws CallException;

    private native void ringrtcMessageSendFailure(long j, long j2) throws CallException;

    private native void ringrtcMessageSent(long j, long j2) throws CallException;

    private native void ringrtcPeekGroupCall(long j, long j2, String str, byte[] bArr, byte[] bArr2) throws CallException;

    private native void ringrtcProceed(long j, long j2, CallContext callContext, int i, int i2) throws CallException;

    private native void ringrtcReceivedAnswer(long j, long j2, int i, byte[] bArr, byte[] bArr2, byte[] bArr3) throws CallException;

    private native void ringrtcReceivedBusy(long j, long j2, int i) throws CallException;

    private native void ringrtcReceivedCallMessage(long j, byte[] bArr, int i, int i2, byte[] bArr2, long j2) throws CallException;

    private native void ringrtcReceivedHangup(long j, long j2, int i, int i2, int i3) throws CallException;

    private native void ringrtcReceivedHttpResponse(long j, long j2, int i, byte[] bArr) throws CallException;

    private native void ringrtcReceivedIceCandidates(long j, long j2, int i, List<byte[]> list) throws CallException;

    private native void ringrtcReceivedOffer(long j, long j2, Remote remote, int i, byte[] bArr, long j3, int i2, int i3, boolean z, byte[] bArr2, byte[] bArr3) throws CallException;

    private native void ringrtcReset(long j) throws CallException;

    private native void ringrtcSetSelfUuid(long j, byte[] bArr) throws CallException;

    private native void ringrtcSetVideoEnable(long j, boolean z) throws CallException;

    private native void ringrtcUpdateBandwidthMode(long j, int i) throws CallException;

    static {
        String simpleName = CallManager.class.getSimpleName();
        int i = Build.VERSION.SDK_INT;
        if (i < 21) {
            Log.i(simpleName, "Preloading ringrtc_rffi library for SDK: " + i);
            System.loadLibrary("ringrtc_rffi");
        }
        Log.d(simpleName, "Loading ringrtc library");
        System.loadLibrary("ringrtc");
    }

    public static void initialize(Context context, Log.Logger logger) {
        try {
            Log.initialize(logger);
            PeerConnectionFactory.InitializationOptions.Builder nativeLibraryLoader = PeerConnectionFactory.InitializationOptions.builder(context).setNativeLibraryLoader(new NoOpLoader());
            BuildInfo ringrtcGetBuildInfo = ringrtcGetBuildInfo();
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("CallManager.initialize(): (");
            sb.append(ringrtcGetBuildInfo.debug ? "debug" : "release");
            sb.append(" build)");
            Log.i(str, sb.toString());
            if (ringrtcGetBuildInfo.debug) {
                nativeLibraryLoader.setInjectableLogger(new WebRtcLogger(), Logging.Severity.LS_INFO);
            } else {
                nativeLibraryLoader.setInjectableLogger(new WebRtcLogger(), Logging.Severity.LS_WARNING);
            }
            PeerConnectionFactory.initialize(nativeLibraryLoader.createInitializationOptions());
            ringrtcInitialize();
            isInitialized = true;
            Log.i(str, "CallManager.initialize() returned");
        } catch (UnsatisfiedLinkError e) {
            Log.w(TAG, "Unable to load ringrtc library", e);
            throw new AssertionError("Unable to load ringrtc library");
        } catch (CallException e2) {
            Log.w(TAG, "Unable to initialize ringrtc library", e2);
            throw new AssertionError("Unable to initialize ringrtc library");
        }
    }

    private static void checkInitializeHasBeenCalled() {
        if (!isInitialized) {
            throw new IllegalStateException("CallManager.initialize has not been called");
        }
    }

    /* loaded from: classes3.dex */
    public class PeerConnectionFactoryOptions extends PeerConnectionFactory.Options {
        public PeerConnectionFactoryOptions() {
            CallManager.this = r1;
            this.networkIgnoreMask = 16;
        }
    }

    private PeerConnectionFactory createPeerConnectionFactory(EglBase eglBase, AudioProcessingMethod audioProcessingMethod) {
        VideoEncoderFactory videoEncoderFactory;
        VideoDecoderFactory videoDecoderFactory;
        AnonymousClass1 r0 = new HashSet<String>() { // from class: org.signal.ringrtc.CallManager.1
            {
                add("SM-G920F");
                add("SM-G920FD");
                add("SM-G920FQ");
                add("SM-G920I");
                add("SM-G920A");
                add("SM-G920T");
                add("SM-G930F");
                add("SM-G930FD");
                add("SM-G930W8");
                add("SM-G930S");
                add("SM-G930K");
                add("SM-G930L");
                add("SM-G935F");
                add("SM-G935FD");
                add("SM-G935W8");
                add("SM-G935S");
                add("SM-G935K");
                add("SM-G935L");
                add("SM-A320F");
                add("SM-A320FL");
                add("SM-A320F/DS");
                add("SM-A320Y/DS");
                add("SM-A320Y");
            }
        };
        if (eglBase == null || r0.contains(Build.MODEL)) {
            videoEncoderFactory = new SoftwareVideoEncoderFactory();
        } else {
            videoEncoderFactory = new DefaultVideoEncoderFactory(eglBase.getEglBaseContext(), true, true);
        }
        if (eglBase == null) {
            videoDecoderFactory = new SoftwareVideoDecoderFactory();
        } else {
            videoDecoderFactory = new DefaultVideoDecoderFactory(eglBase.getEglBaseContext());
        }
        JavaAudioDeviceModule createAudioDeviceModule = createAudioDeviceModule(audioProcessingMethod);
        PeerConnectionFactory createPeerConnectionFactory = PeerConnectionFactory.builder().setOptions(new PeerConnectionFactoryOptions()).setAudioDeviceModule(createAudioDeviceModule).setVideoEncoderFactory(videoEncoderFactory).setVideoDecoderFactory(videoDecoderFactory).createPeerConnectionFactory();
        createAudioDeviceModule.release();
        return createPeerConnectionFactory;
    }

    /* renamed from: org.signal.ringrtc.CallManager$2 */
    /* loaded from: classes3.dex */
    public static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$org$signal$ringrtc$CallManager$AudioProcessingMethod;

        static {
            int[] iArr = new int[AudioProcessingMethod.values().length];
            $SwitchMap$org$signal$ringrtc$CallManager$AudioProcessingMethod = iArr;
            try {
                iArr[AudioProcessingMethod.ForceSoftwareAecM.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$AudioProcessingMethod[AudioProcessingMethod.ForceSoftwareAec3.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    static JavaAudioDeviceModule createAudioDeviceModule(AudioProcessingMethod audioProcessingMethod) {
        int i = AnonymousClass2.$SwitchMap$org$signal$ringrtc$CallManager$AudioProcessingMethod[audioProcessingMethod.ordinal()];
        boolean z = true;
        boolean z2 = false;
        if (i == 1) {
            z = false;
            z2 = true;
        } else if (i == 2) {
            z = false;
        }
        String str = TAG;
        Log.i(str, "createAudioDeviceModule(): useHardware: " + z + " useAecM: " + z2);
        return JavaAudioDeviceModule.builder(ContextUtils.getApplicationContext()).setUseHardwareAcousticEchoCanceler(z).setUseHardwareNoiseSuppressor(z).setUseAecm(z2).createAudioDeviceModule();
    }

    private void checkCallManagerExists() {
        if (this.nativeCallManager == 0) {
            throw new IllegalStateException("CallManager has been disposed");
        }
    }

    CallManager(Observer observer) {
        Log.i(TAG, "CallManager():");
        this.observer = observer;
    }

    public static CallManager createCallManager(Observer observer) throws CallException {
        String str = TAG;
        Log.i(str, "createCallManager():");
        checkInitializeHasBeenCalled();
        CallManager callManager = new CallManager(observer);
        long ringrtcCreateCallManager = ringrtcCreateCallManager(callManager);
        if (ringrtcCreateCallManager != 0) {
            callManager.nativeCallManager = ringrtcCreateCallManager;
            return callManager;
        }
        Log.w(str, "Unable to create Call Manager");
        return null;
    }

    public void close() throws CallException {
        checkCallManagerExists();
        String str = TAG;
        Log.i(str, "close():");
        LongSparseArray<GroupCall> longSparseArray = this.groupCallByClientId;
        if (longSparseArray != null && longSparseArray.size() > 0) {
            Log.w(str, "Closing CallManager but groupCallByClientId still has objects");
        }
        PeerConnectionFactory peerConnectionFactory = this.groupFactory;
        if (peerConnectionFactory != null) {
            peerConnectionFactory.dispose();
        }
        ringrtcClose(this.nativeCallManager);
        this.nativeCallManager = 0;
    }

    public void setSelfUuid(UUID uuid) throws CallException {
        checkCallManagerExists();
        Log.i(TAG, "setSelfUuid():");
        ringrtcSetSelfUuid(this.nativeCallManager, Util.getBytesFromUuid(uuid));
    }

    public void call(Remote remote, CallMediaType callMediaType, Integer num) throws CallException {
        checkCallManagerExists();
        Log.i(TAG, "call(): creating new call:");
        ringrtcCall(this.nativeCallManager, remote, callMediaType.ordinal(), num.intValue());
    }

    public void proceed(CallId callId, Context context, EglBase eglBase, AudioProcessingMethod audioProcessingMethod, VideoSink videoSink, VideoSink videoSink2, CameraControl cameraControl, List<PeerConnection.IceServer> list, boolean z, BandwidthMode bandwidthMode, Integer num, boolean z2) throws CallException {
        int i;
        checkCallManagerExists();
        String str = TAG;
        Log.i(str, "proceed(): callId: " + callId + ", hideIp: " + z);
        for (PeerConnection.IceServer iceServer : list) {
            Iterator<String> it = iceServer.urls.iterator();
            while (it.hasNext()) {
                String str2 = TAG;
                Log.i(str2, "  server: " + it.next());
            }
        }
        CallContext callContext = new CallContext(callId, context, createPeerConnectionFactory(eglBase, audioProcessingMethod), videoSink, videoSink2, cameraControl, list, z);
        callContext.setVideoEnabled(z2);
        if (num == null) {
            i = 0;
        } else {
            i = num.intValue();
        }
        ringrtcProceed(this.nativeCallManager, callId.longValue().longValue(), callContext, bandwidthMode.ordinal(), i);
    }

    public void drop(CallId callId) throws CallException {
        checkCallManagerExists();
        String str = TAG;
        Log.i(str, "drop(): " + callId);
        ringrtcDrop(this.nativeCallManager, callId.longValue().longValue());
    }

    public void reset() throws CallException {
        checkCallManagerExists();
        Log.i(TAG, "reset():");
        ringrtcReset(this.nativeCallManager);
    }

    public void messageSent(CallId callId) throws CallException {
        checkCallManagerExists();
        String str = TAG;
        Log.i(str, "messageSent(): " + callId);
        ringrtcMessageSent(this.nativeCallManager, callId.longValue().longValue());
    }

    public void messageSendFailure(CallId callId) throws CallException {
        checkCallManagerExists();
        String str = TAG;
        Log.i(str, "messageSendFailure(): " + callId);
        ringrtcMessageSendFailure(this.nativeCallManager, callId.longValue().longValue());
    }

    public void receivedOffer(CallId callId, Remote remote, Integer num, byte[] bArr, Long l, CallMediaType callMediaType, Integer num2, boolean z, byte[] bArr2, byte[] bArr3) throws CallException {
        checkCallManagerExists();
        String str = TAG;
        Log.i(str, "receivedOffer(): id: " + callId.format(num));
        ringrtcReceivedOffer(this.nativeCallManager, callId.longValue().longValue(), remote, num.intValue(), bArr, l.longValue(), callMediaType.ordinal(), num2.intValue(), z, bArr2, bArr3);
    }

    public void receivedAnswer(CallId callId, Integer num, byte[] bArr, byte[] bArr2, byte[] bArr3) throws CallException {
        checkCallManagerExists();
        String str = TAG;
        Log.i(str, "receivedAnswer(): id: " + callId.format(num));
        ringrtcReceivedAnswer(this.nativeCallManager, callId.longValue().longValue(), num.intValue(), bArr, bArr2, bArr3);
    }

    public void receivedIceCandidates(CallId callId, Integer num, List<byte[]> list) throws CallException {
        checkCallManagerExists();
        String str = TAG;
        Log.i(str, "receivedIceCandidates(): id: " + callId.format(num) + ", count: " + list.size());
        ringrtcReceivedIceCandidates(this.nativeCallManager, callId.longValue().longValue(), num.intValue(), list);
    }

    public void receivedHangup(CallId callId, Integer num, HangupType hangupType, Integer num2) throws CallException {
        checkCallManagerExists();
        String str = TAG;
        Log.i(str, "receivedHangup(): id: " + callId.format(num));
        ringrtcReceivedHangup(this.nativeCallManager, callId.longValue().longValue(), num.intValue(), hangupType.ordinal(), num2.intValue());
    }

    public void receivedBusy(CallId callId, Integer num) throws CallException {
        checkCallManagerExists();
        String str = TAG;
        Log.i(str, "receivedBusy(): id: " + callId.format(num));
        ringrtcReceivedBusy(this.nativeCallManager, callId.longValue().longValue(), num.intValue());
    }

    public void receivedCallMessage(UUID uuid, Integer num, Integer num2, byte[] bArr, Long l) throws CallException {
        checkCallManagerExists();
        Log.i(TAG, "receivedCallMessage():");
        ringrtcReceivedCallMessage(this.nativeCallManager, Util.getBytesFromUuid(uuid), num.intValue(), num2.intValue(), bArr, l.longValue());
    }

    public void receivedHttpResponse(long j, int i, byte[] bArr) throws CallException {
        checkCallManagerExists();
        String str = TAG;
        Log.i(str, "receivedHttpResponse(): requestId: " + j);
        ringrtcReceivedHttpResponse(this.nativeCallManager, j, i, bArr);
    }

    public void httpRequestFailed(long j) throws CallException {
        checkCallManagerExists();
        String str = TAG;
        Log.i(str, "httpRequestFailed(): requestId: " + j);
        ringrtcHttpRequestFailed(this.nativeCallManager, j);
    }

    public void acceptCall(CallId callId) throws CallException {
        checkCallManagerExists();
        String str = TAG;
        Log.i(str, "accept(): " + callId);
        ringrtcAcceptCall(this.nativeCallManager, callId.longValue().longValue());
    }

    public void setAudioEnable(boolean z) throws CallException {
        checkCallManagerExists();
        ringrtcGetActiveConnection(this.nativeCallManager).setAudioEnabled(z);
    }

    public void setVideoEnable(boolean z) throws CallException {
        checkCallManagerExists();
        ringrtcGetActiveCallContext(this.nativeCallManager).setVideoEnabled(z);
        ringrtcSetVideoEnable(this.nativeCallManager, z);
    }

    public void updateBandwidthMode(BandwidthMode bandwidthMode) throws CallException {
        checkCallManagerExists();
        ringrtcUpdateBandwidthMode(this.nativeCallManager, bandwidthMode.ordinal());
    }

    public void hangup() throws CallException {
        checkCallManagerExists();
        Log.i(TAG, "hangup():");
        ringrtcHangup(this.nativeCallManager);
    }

    public void cancelGroupRing(byte[] bArr, long j, RingCancelReason ringCancelReason) throws CallException {
        checkCallManagerExists();
        Log.i(TAG, "cancelGroupRing():");
        ringrtcCancelGroupRing(this.nativeCallManager, bArr, j, ringCancelReason != null ? ringCancelReason.ordinal() : -1);
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes3.dex */
    public static class Requests<T> {
        private LongSparseArray<ResponseHandler<T>> handlerById = new LongSparseArray<>();
        private long nextId = 1;

        Requests() {
        }

        long add(ResponseHandler<T> responseHandler) {
            long j = this.nextId;
            this.nextId = 1 + j;
            this.handlerById.put(j, responseHandler);
            return j;
        }

        boolean resolve(long j, T t) {
            ResponseHandler<T> responseHandler = this.handlerById.get(j);
            if (responseHandler == null) {
                return false;
            }
            responseHandler.handleResponse(t);
            this.handlerById.delete(j);
            return true;
        }
    }

    public void peekGroupCall(String str, byte[] bArr, Collection<GroupCall.GroupMemberInfo> collection, ResponseHandler<PeekInfo> responseHandler) throws CallException {
        checkCallManagerExists();
        Log.i(TAG, "peekGroupCall():");
        ringrtcPeekGroupCall(this.nativeCallManager, this.peekInfoRequests.add(responseHandler), str, bArr, Util.serializeFromGroupMemberInfo(collection));
    }

    public GroupCall createGroupCall(byte[] bArr, String str, byte[] bArr2, Integer num, AudioProcessingMethod audioProcessingMethod, GroupCall.Observer observer) {
        checkCallManagerExists();
        if (this.groupFactory == null) {
            PeerConnectionFactory createPeerConnectionFactory = createPeerConnectionFactory(null, audioProcessingMethod);
            this.groupFactory = createPeerConnectionFactory;
            if (createPeerConnectionFactory == null) {
                Log.e(TAG, "createPeerConnectionFactory failed");
                return null;
            }
        }
        GroupCall groupCall = new GroupCall(this.nativeCallManager, bArr, str, bArr2, num, this.groupFactory, observer);
        long j = groupCall.clientId;
        if (j != 0) {
            this.groupCallByClientId.append(j, groupCall);
            return groupCall;
        }
        try {
            groupCall.dispose();
        } catch (CallException e) {
            Log.e(TAG, "Unable to properly dispose of GroupCall", e);
        }
        return null;
    }

    @CalledByNative
    private Connection createConnection(long j, long j2, int i, CallContext callContext) {
        CallId callId = new CallId(Long.valueOf(j2));
        String str = TAG;
        Log.i(str, "createConnection(): connectionId: " + callId.format(Integer.valueOf(i)));
        MediaConstraints mediaConstraints = new MediaConstraints();
        PeerConnection.RTCConfiguration rTCConfiguration = new PeerConnection.RTCConfiguration(callContext.iceServers);
        rTCConfiguration.sdpSemantics = PeerConnection.SdpSemantics.PLAN_B;
        rTCConfiguration.bundlePolicy = PeerConnection.BundlePolicy.MAXBUNDLE;
        rTCConfiguration.rtcpMuxPolicy = PeerConnection.RtcpMuxPolicy.REQUIRE;
        rTCConfiguration.tcpCandidatePolicy = PeerConnection.TcpCandidatePolicy.DISABLED;
        rTCConfiguration.continualGatheringPolicy = PeerConnection.ContinualGatheringPolicy.GATHER_CONTINUALLY;
        if (callContext.hideIp) {
            rTCConfiguration.iceTransportsType = PeerConnection.IceTransportsType.RELAY;
        }
        PeerConnectionFactory peerConnectionFactory = callContext.factory;
        try {
            long ringrtcCreatePeerConnection = ringrtcCreatePeerConnection(peerConnectionFactory.getNativeOwnedFactoryAndThreads(), j, rTCConfiguration, mediaConstraints);
            if (ringrtcCreatePeerConnection == 0) {
                Log.w(str, "Unable to create native PeerConnection.");
                return null;
            }
            Connection connection = new Connection(new Connection.NativeFactory(ringrtcCreatePeerConnection, callId, i));
            connection.setAudioPlayout(false);
            connection.setAudioRecording(false);
            MediaStream createLocalMediaStream = peerConnectionFactory.createLocalMediaStream("ARDAMS");
            AudioSource createAudioSource = peerConnectionFactory.createAudioSource(new MediaConstraints());
            AudioTrack createAudioTrack = peerConnectionFactory.createAudioTrack("audio1", createAudioSource);
            createAudioTrack.setEnabled(false);
            createLocalMediaStream.addTrack(createAudioTrack);
            VideoTrack videoTrack = callContext.videoTrack;
            if (videoTrack != null) {
                createLocalMediaStream.addPreservedTrack(videoTrack);
            }
            connection.addStream(createLocalMediaStream);
            connection.setAudioSource(createAudioSource, createAudioTrack);
            return connection;
        } catch (CallException e) {
            Log.w(TAG, "Unable to create Peer Connection with native call", e);
            return null;
        }
    }

    @CalledByNative
    private void onConnectMedia(CallContext callContext, MediaStream mediaStream) {
        String str = TAG;
        Log.i(str, "onConnectMedia(): mediaStream: " + mediaStream);
        if (mediaStream == null) {
            Log.w(str, "Remote media stream unavailable");
            return;
        }
        List<AudioTrack> list = mediaStream.audioTracks;
        if (list == null) {
            Log.w(str, "Remote media stream contains no audio tracks");
            return;
        }
        for (AudioTrack audioTrack : list) {
            Log.i(TAG, "onConnectMedia(): enabling audioTrack");
            audioTrack.setEnabled(true);
        }
        List<VideoTrack> list2 = mediaStream.videoTracks;
        if (list2 == null) {
            Log.w(TAG, "Remote media stream contains no video tracks");
        } else if (list2.size() == 1) {
            Log.i(TAG, "onConnectMedia(): enabling videoTrack(0)");
            VideoTrack videoTrack = mediaStream.videoTracks.get(0);
            videoTrack.setEnabled(true);
            videoTrack.addSink(callContext.remoteSink);
        } else {
            String str2 = TAG;
            Log.w(str2, "onConnectMedia(): Media stream contains unexpected number of video tracks: " + mediaStream.videoTracks.size());
        }
    }

    @CalledByNative
    private void onCloseMedia(CallContext callContext) {
        Log.i(TAG, "onCloseMedia():");
        callContext.setVideoEnabled(false);
    }

    @CalledByNative
    private void closeConnection(Connection connection) {
        String str = TAG;
        Log.i(str, "closeConnection(): " + connection);
        connection.shutdown();
    }

    @CalledByNative
    private void closeCall(CallContext callContext) {
        Log.i(TAG, "closeCall():");
        callContext.dispose();
    }

    @CalledByNative
    private void onStartCall(Remote remote, long j, boolean z, CallMediaType callMediaType) {
        Log.i(TAG, "onStartCall():");
        this.observer.onStartCall(remote, new CallId(Long.valueOf(j)), Boolean.valueOf(z), callMediaType);
    }

    @CalledByNative
    private void onEvent(Remote remote, CallEvent callEvent) {
        Log.i(TAG, "onEvent():");
        this.observer.onCallEvent(remote, callEvent);
    }

    @CalledByNative
    private void onNetworkRouteChanged(Remote remote, int i) {
        Log.i(TAG, "onNetworkRouteChange():");
        this.observer.onNetworkRouteChanged(remote, new NetworkRoute(NetworkAdapterTypeFromRawValue(i)));
    }

    @CalledByNative
    private void onAudioLevels(Remote remote, int i, int i2) {
        this.observer.onAudioLevels(remote, i, i2);
    }

    private PeerConnection.AdapterType NetworkAdapterTypeFromRawValue(int i) {
        if (i == 0) {
            return PeerConnection.AdapterType.UNKNOWN;
        }
        if (i == 1) {
            return PeerConnection.AdapterType.ETHERNET;
        }
        if (i == 2) {
            return PeerConnection.AdapterType.WIFI;
        }
        if (i == 4) {
            return PeerConnection.AdapterType.CELLULAR;
        }
        if (i == 8) {
            return PeerConnection.AdapterType.VPN;
        }
        if (i == 16) {
            return PeerConnection.AdapterType.LOOPBACK;
        }
        if (i == 32) {
            return PeerConnection.AdapterType.ADAPTER_TYPE_ANY;
        }
        if (i == 64) {
            return PeerConnection.AdapterType.CELLULAR_2G;
        }
        if (i == 128) {
            return PeerConnection.AdapterType.CELLULAR_3G;
        }
        if (i == 256) {
            return PeerConnection.AdapterType.CELLULAR_4G;
        }
        if (i != 512) {
            return PeerConnection.AdapterType.UNKNOWN;
        }
        return PeerConnection.AdapterType.CELLULAR_5G;
    }

    @CalledByNative
    private void onCallConcluded(Remote remote) {
        Log.i(TAG, "onCallConcluded():");
        this.observer.onCallConcluded(remote);
    }

    @CalledByNative
    private void onSendOffer(long j, Remote remote, int i, boolean z, byte[] bArr, CallMediaType callMediaType) {
        Log.i(TAG, "onSendOffer():");
        this.observer.onSendOffer(new CallId(Long.valueOf(j)), remote, Integer.valueOf(i), Boolean.valueOf(z), bArr, callMediaType);
    }

    @CalledByNative
    private void onSendAnswer(long j, Remote remote, int i, boolean z, byte[] bArr) {
        Log.i(TAG, "onSendAnswer():");
        this.observer.onSendAnswer(new CallId(Long.valueOf(j)), remote, Integer.valueOf(i), Boolean.valueOf(z), bArr);
    }

    @CalledByNative
    private void onSendIceCandidates(long j, Remote remote, int i, boolean z, List<byte[]> list) {
        Log.i(TAG, "onSendIceCandidates():");
        this.observer.onSendIceCandidates(new CallId(Long.valueOf(j)), remote, Integer.valueOf(i), Boolean.valueOf(z), list);
    }

    @CalledByNative
    private void onSendHangup(long j, Remote remote, int i, boolean z, HangupType hangupType, int i2) {
        Log.i(TAG, "onSendHangup():");
        this.observer.onSendHangup(new CallId(Long.valueOf(j)), remote, Integer.valueOf(i), Boolean.valueOf(z), hangupType, Integer.valueOf(i2));
    }

    @CalledByNative
    private void onSendBusy(long j, Remote remote, int i, boolean z) {
        Log.i(TAG, "onSendBusy():");
        this.observer.onSendBusy(new CallId(Long.valueOf(j)), remote, Integer.valueOf(i), Boolean.valueOf(z));
    }

    @CalledByNative
    private void sendCallMessage(byte[] bArr, byte[] bArr2, int i) {
        Log.i(TAG, "sendCallMessage():");
        this.observer.onSendCallMessage(Util.getUuidFromBytes(bArr), bArr2, CallMessageUrgency.values()[i]);
    }

    @CalledByNative
    private void sendCallMessageToGroup(byte[] bArr, byte[] bArr2, int i) {
        Log.i(TAG, "sendCallMessageToGroup():");
        this.observer.onSendCallMessageToGroup(bArr, bArr2, CallMessageUrgency.values()[i]);
    }

    @CalledByNative
    private void sendHttpRequest(long j, String str, HttpMethod httpMethod, List<HttpHeader> list, byte[] bArr) {
        Log.i(TAG, "sendHttpRequest():");
        this.observer.onSendHttpRequest(j, str, httpMethod, list, bArr);
    }

    @CalledByNative
    private boolean compareRemotes(Remote remote, Remote remote2) {
        Log.i(TAG, "compareRemotes():");
        if (remote != null) {
            return remote.recipientEquals(remote2);
        }
        return false;
    }

    @CalledByNative
    private void groupCallRingUpdate(byte[] bArr, long j, byte[] bArr2, int i) {
        Log.i(TAG, "groupCallRingUpdate():");
        this.observer.onGroupCallRingUpdate(bArr, j, Util.getUuidFromBytes(bArr2), RingUpdate.values()[i]);
    }

    @CalledByNative
    private void handlePeekResponse(long j, List<byte[]> list, byte[] bArr, String str, Long l, long j2) {
        if (list != null) {
            String str2 = TAG;
            Log.i(str2, "handlePeekResponse(): joinedMembers.size = " + list.size());
        } else {
            Log.i(TAG, "handlePeekResponse(): joinedMembers is null");
        }
        ArrayList arrayList = new ArrayList();
        for (byte[] bArr2 : list) {
            arrayList.add(Util.getUuidFromBytes(bArr2));
        }
        if (!this.peekInfoRequests.resolve(j, new PeekInfo(arrayList, bArr == null ? null : Util.getUuidFromBytes(bArr), str, l, j2))) {
            String str3 = TAG;
            Log.w(str3, "Invalid requestId for handlePeekResponse: " + j);
        }
    }

    @CalledByNative
    private void requestMembershipProof(long j) {
        String str = TAG;
        Log.i(str, "requestMembershipProof():");
        GroupCall groupCall = this.groupCallByClientId.get(j);
        if (groupCall == null) {
            Log.w(str, "groupCall not found by clientId: " + j);
            return;
        }
        groupCall.requestMembershipProof();
    }

    @CalledByNative
    private void requestGroupMembers(long j) {
        String str = TAG;
        Log.i(str, "requestGroupMembers():");
        GroupCall groupCall = this.groupCallByClientId.get(j);
        if (groupCall == null) {
            Log.w(str, "groupCall not found by clientId: " + j);
            return;
        }
        groupCall.requestGroupMembers();
    }

    @CalledByNative
    private void handleConnectionStateChanged(long j, GroupCall.ConnectionState connectionState) {
        String str = TAG;
        Log.i(str, "handleConnectionStateChanged():");
        GroupCall groupCall = this.groupCallByClientId.get(j);
        if (groupCall == null) {
            Log.w(str, "groupCall not found by clientId: " + j);
            return;
        }
        groupCall.handleConnectionStateChanged(connectionState);
    }

    @CalledByNative
    private void handleNetworkRouteChanged(long j, int i) {
        String str = TAG;
        Log.i(str, "handleNetworkRouteChanged():");
        GroupCall groupCall = this.groupCallByClientId.get(j);
        if (groupCall == null) {
            Log.w(str, "groupCall not found by clientId: " + j);
            return;
        }
        groupCall.handleNetworkRouteChanged(new NetworkRoute(NetworkAdapterTypeFromRawValue(i)));
    }

    @CalledByNative
    private void handleAudioLevels(long j, int i, List<GroupCall.ReceivedAudioLevel> list) {
        String str = TAG;
        Log.d(str, "handleAudioLevels():");
        GroupCall groupCall = this.groupCallByClientId.get(j);
        if (groupCall == null) {
            Log.w(str, "groupCall not found by clientId: " + j);
            return;
        }
        groupCall.handleAudioLevels(i, list);
    }

    @CalledByNative
    private void handleJoinStateChanged(long j, GroupCall.JoinState joinState) {
        String str = TAG;
        Log.i(str, "handleJoinStateChanged():");
        GroupCall groupCall = this.groupCallByClientId.get(j);
        if (groupCall == null) {
            Log.w(str, "groupCall not found by clientId: " + j);
            return;
        }
        groupCall.handleJoinStateChanged(joinState);
    }

    @CalledByNative
    private void handleRemoteDevicesChanged(long j, List<GroupCall.RemoteDeviceState> list) {
        if (list != null) {
            String str = TAG;
            Log.i(str, "handleRemoteDevicesChanged(): remoteDeviceStates.size = " + list.size());
        } else {
            Log.i(TAG, "handleRemoteDevicesChanged(): remoteDeviceStates is null!");
        }
        GroupCall groupCall = this.groupCallByClientId.get(j);
        if (groupCall == null) {
            String str2 = TAG;
            Log.w(str2, "groupCall not found by clientId: " + j);
            return;
        }
        groupCall.handleRemoteDevicesChanged(list);
    }

    @CalledByNative
    private void handleIncomingVideoTrack(long j, long j2, long j3) {
        String str = TAG;
        Log.i(str, "handleIncomingVideoTrack():");
        GroupCall groupCall = this.groupCallByClientId.get(j);
        if (groupCall == null) {
            Log.w(str, "groupCall not found by clientId: " + j);
            return;
        }
        groupCall.handleIncomingVideoTrack(j2, j3);
    }

    @CalledByNative
    private void handlePeekChanged(long j, List<byte[]> list, byte[] bArr, String str, Long l, long j2) {
        if (list != null) {
            String str2 = TAG;
            Log.i(str2, "handlePeekChanged(): joinedMembers.size = " + list.size());
        } else {
            Log.i(TAG, "handlePeekChanged(): joinedMembers is null");
        }
        GroupCall groupCall = this.groupCallByClientId.get(j);
        if (groupCall == null) {
            String str3 = TAG;
            Log.w(str3, "groupCall not found by clientId: " + j);
            return;
        }
        ArrayList arrayList = new ArrayList();
        for (byte[] bArr2 : list) {
            arrayList.add(Util.getUuidFromBytes(bArr2));
        }
        groupCall.handlePeekChanged(new PeekInfo(arrayList, bArr == null ? null : Util.getUuidFromBytes(bArr), str, l, j2));
    }

    @CalledByNative
    private void handleEnded(long j, GroupCall.GroupCallEndReason groupCallEndReason) {
        String str = TAG;
        Log.i(str, "handleEnded():");
        GroupCall groupCall = this.groupCallByClientId.get(j);
        if (groupCall == null) {
            Log.w(str, "groupCall not found by clientId: " + j);
            return;
        }
        this.groupCallByClientId.delete(j);
        groupCall.handleEnded(groupCallEndReason);
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes3.dex */
    public static class CallContext {
        private final String TAG;
        public final CallId callId;
        public final CameraControl cameraControl;
        public final PeerConnectionFactory factory;
        public final boolean hideIp;
        public final List<PeerConnection.IceServer> iceServers;
        public final VideoSink remoteSink;
        public final VideoSource videoSource;
        public final VideoTrack videoTrack;

        public CallContext(CallId callId, Context context, PeerConnectionFactory peerConnectionFactory, VideoSink videoSink, VideoSink videoSink2, CameraControl cameraControl, List<PeerConnection.IceServer> list, boolean z) {
            String simpleName = CallContext.class.getSimpleName();
            this.TAG = simpleName;
            Log.i(simpleName, "ctor(): " + callId);
            this.callId = callId;
            this.factory = peerConnectionFactory;
            this.remoteSink = videoSink2;
            this.cameraControl = cameraControl;
            this.iceServers = list;
            this.hideIp = z;
            if (cameraControl.hasCapturer()) {
                VideoSource createVideoSource = peerConnectionFactory.createVideoSource(false);
                this.videoSource = createVideoSource;
                VideoTrack createVideoTrack = peerConnectionFactory.createVideoTrack("video1", createVideoSource);
                this.videoTrack = createVideoTrack;
                createVideoTrack.setEnabled(false);
                cameraControl.initCapturer(createVideoSource.getCapturerObserver());
                createVideoTrack.addSink(videoSink);
                return;
            }
            this.videoSource = null;
            this.videoTrack = null;
        }

        void setVideoEnabled(boolean z) {
            String str = this.TAG;
            Log.i(str, "setVideoEnabled(): " + this.callId);
            VideoTrack videoTrack = this.videoTrack;
            if (videoTrack != null) {
                videoTrack.setEnabled(z);
                this.cameraControl.setEnabled(z);
            }
        }

        void dispose() {
            String str = this.TAG;
            Log.i(str, "dispose(): " + this.callId);
            CameraControl cameraControl = this.cameraControl;
            if (cameraControl != null) {
                cameraControl.setEnabled(false);
            }
            VideoSource videoSource = this.videoSource;
            if (videoSource != null) {
                videoSource.dispose();
            }
            VideoTrack videoTrack = this.videoTrack;
            if (videoTrack != null) {
                videoTrack.dispose();
            }
            this.factory.dispose();
        }
    }

    /* loaded from: classes3.dex */
    public enum CallEvent {
        LOCAL_RINGING,
        REMOTE_RINGING,
        LOCAL_CONNECTED,
        REMOTE_CONNECTED,
        ENDED_LOCAL_HANGUP,
        ENDED_REMOTE_HANGUP,
        ENDED_REMOTE_HANGUP_NEED_PERMISSION,
        ENDED_REMOTE_HANGUP_ACCEPTED,
        ENDED_REMOTE_HANGUP_DECLINED,
        ENDED_REMOTE_HANGUP_BUSY,
        ENDED_REMOTE_BUSY,
        ENDED_REMOTE_GLARE,
        ENDED_REMOTE_RECALL,
        ENDED_TIMEOUT,
        ENDED_INTERNAL_FAILURE,
        ENDED_SIGNALING_FAILURE,
        ENDED_GLARE_HANDLING_FAILURE,
        ENDED_CONNECTION_FAILURE,
        ENDED_APP_DROPPED_CALL,
        REMOTE_VIDEO_ENABLE,
        REMOTE_VIDEO_DISABLE,
        REMOTE_SHARING_SCREEN_ENABLE,
        REMOTE_SHARING_SCREEN_DISABLE,
        RECONNECTING,
        RECONNECTED,
        RECEIVED_OFFER_EXPIRED,
        RECEIVED_OFFER_WHILE_ACTIVE,
        RECEIVED_OFFER_WITH_GLARE;

        @CalledByNative
        static CallEvent fromNativeIndex(int i) {
            return values()[i];
        }
    }

    /* loaded from: classes3.dex */
    public enum CallMediaType {
        AUDIO_CALL,
        VIDEO_CALL;

        @CalledByNative
        static CallMediaType fromNativeIndex(int i) {
            return values()[i];
        }
    }

    /* loaded from: classes3.dex */
    public enum HangupType {
        NORMAL,
        ACCEPTED,
        DECLINED,
        BUSY,
        NEED_PERMISSION;

        @CalledByNative
        static HangupType fromNativeIndex(int i) {
            return values()[i];
        }
    }

    /* loaded from: classes3.dex */
    public enum BandwidthMode {
        VERY_LOW,
        LOW,
        NORMAL;

        @CalledByNative
        static BandwidthMode fromNativeIndex(int i) {
            return values()[i];
        }
    }

    /* loaded from: classes3.dex */
    public enum HttpMethod {
        GET,
        PUT,
        POST,
        DELETE;

        @CalledByNative
        static HttpMethod fromNativeIndex(int i) {
            return values()[i];
        }
    }
}
