package org.signal.ringrtc;

/* loaded from: classes3.dex */
public class Log {
    private static final int LL_DEBUG;
    private static final int LL_ERROR;
    private static final int LL_INFO;
    private static final int LL_TRACE;
    private static final int LL_WARN;
    private static final String TAG;
    private static Logger logger;

    /* loaded from: classes.dex */
    public interface Logger {
        void d(String str, String str2, Throwable th);

        void e(String str, String str2, Throwable th);

        void i(String str, String str2, Throwable th);

        void v(String str, String str2, Throwable th);

        void w(String str, String str2, Throwable th);
    }

    public static void initialize(Logger logger2) {
        logger = logger2;
    }

    public static void log(int i, String str, String str2) {
        if (i == 1) {
            e(str, str2);
        } else if (i == 2) {
            w(str, str2);
        } else if (i == 3) {
            i(str, str2);
        } else if (i == 4) {
            d(str, str2);
        } else if (i != 5) {
            String str3 = TAG;
            w(str3, "Unknown log level: " + str + ", " + str2);
        } else {
            v(str, str2);
        }
    }

    public static void v(String str, String str2) {
        v(str, str2, null);
    }

    public static void d(String str, String str2) {
        d(str, str2, null);
    }

    public static void i(String str, String str2) {
        i(str, str2, null);
    }

    public static void w(String str, String str2) {
        w(str, str2, null);
    }

    public static void e(String str, String str2) {
        e(str, str2, null);
    }

    public static void v(String str, Throwable th) {
        v(str, null, th);
    }

    public static void d(String str, Throwable th) {
        d(str, null, th);
    }

    public static void i(String str, Throwable th) {
        i(str, null, th);
    }

    public static void w(String str, Throwable th) {
        w(str, null, th);
    }

    public static void e(String str, Throwable th) {
        e(str, null, th);
    }

    public static void v(String str, String str2, Throwable th) {
        Logger logger2 = logger;
        if (logger2 != null) {
            logger2.v(str, str2, th);
        } else {
            android.util.Log.v(str, str2, th);
        }
    }

    public static void d(String str, String str2, Throwable th) {
        Logger logger2 = logger;
        if (logger2 != null) {
            logger2.d(str, str2, th);
        } else {
            android.util.Log.d(str, str2, th);
        }
    }

    public static void i(String str, String str2, Throwable th) {
        Logger logger2 = logger;
        if (logger2 != null) {
            logger2.i(str, str2, th);
        } else {
            android.util.Log.i(str, str2, th);
        }
    }

    public static void w(String str, String str2, Throwable th) {
        Logger logger2 = logger;
        if (logger2 != null) {
            logger2.w(str, str2, th);
        } else {
            android.util.Log.w(str, str2, th);
        }
    }

    public static void e(String str, String str2, Throwable th) {
        Logger logger2 = logger;
        if (logger2 != null) {
            logger2.e(str, str2, th);
        } else {
            android.util.Log.e(str, str2, th);
        }
    }
}
