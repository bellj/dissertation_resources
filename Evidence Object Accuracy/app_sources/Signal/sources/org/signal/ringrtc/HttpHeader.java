package org.signal.ringrtc;

/* loaded from: classes3.dex */
public final class HttpHeader {
    private final String name;
    private final String value;

    public HttpHeader(String str, String str2) {
        this.name = str;
        this.value = str2;
    }

    public String getName() {
        return this.name;
    }

    public String getValue() {
        return this.value;
    }
}
