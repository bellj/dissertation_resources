package org.signal.qr;

import androidx.camera.core.ImageProxy;
import com.google.zxing.LuminanceSource;
import java.nio.ByteBuffer;
import kotlin.Metadata;
import kotlin.collections.ArraysKt___ArraysJvmKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* compiled from: ImageProxyLuminanceSource.kt */
@Metadata(bv = {}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0012\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u000e\u0010\u000fJ\u001a\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004H\u0016J\b\u0010\u0007\u001a\u00020\u0004H\u0016R\u0017\u0010\b\u001a\u00020\u00048\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lorg/signal/qr/ImageProxyLuminanceSource;", "Lcom/google/zxing/LuminanceSource;", "", "y", "", "row", "getRow", "getMatrix", "yData", "[B", "getYData", "()[B", "Landroidx/camera/core/ImageProxy;", DraftDatabase.Draft.IMAGE, "<init>", "(Landroidx/camera/core/ImageProxy;)V", "qr_release"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes3.dex */
public final class ImageProxyLuminanceSource extends LuminanceSource {
    private final byte[] yData;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ImageProxyLuminanceSource(ImageProxy imageProxy) {
        super(imageProxy.getWidth(), imageProxy.getHeight());
        Intrinsics.checkNotNullParameter(imageProxy, DraftDatabase.Draft.IMAGE);
        if (imageProxy.getFormat() == 35) {
            this.yData = new byte[imageProxy.getWidth() * imageProxy.getHeight()];
            ByteBuffer buffer = imageProxy.getPlanes()[0].getBuffer();
            Intrinsics.checkNotNullExpressionValue(buffer, "image.planes[0].buffer");
            buffer.position(0);
            int rowStride = imageProxy.getPlanes()[0].getRowStride();
            int height = imageProxy.getHeight();
            for (int i = 0; i < height; i++) {
                buffer.position(i * rowStride);
                buffer.get(this.yData, imageProxy.getWidth() * i, imageProxy.getWidth());
            }
            return;
        }
        throw new IllegalArgumentException("Invalid image format".toString());
    }

    @Override // com.google.zxing.LuminanceSource
    public byte[] getRow(int i, byte[] bArr) {
        if (i >= 0 && i < getHeight()) {
            if (bArr == null || bArr.length < getWidth()) {
                bArr = new byte[getWidth()];
            }
            int width = i * getWidth();
            byte[] unused = ArraysKt___ArraysJvmKt.copyInto(this.yData, bArr, 0, width, getWidth() + width);
            return bArr;
        }
        throw new IllegalArgumentException(("Requested row is outside the image: " + i).toString());
    }

    @Override // com.google.zxing.LuminanceSource
    public byte[] getMatrix() {
        return this.yData;
    }
}
