package org.signal.qr.kitkat;

import android.hardware.Camera;
import org.signal.qr.kitkat.QrCameraView;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class QrCameraView$3$$ExternalSyntheticLambda1 implements Camera.PreviewCallback {
    public final /* synthetic */ QrCameraView.AnonymousClass3 f$0;
    public final /* synthetic */ QrCameraView.PreviewCallback f$1;

    public /* synthetic */ QrCameraView$3$$ExternalSyntheticLambda1(QrCameraView.AnonymousClass3 r1, QrCameraView.PreviewCallback previewCallback) {
        this.f$0 = r1;
        this.f$1 = previewCallback;
    }

    @Override // android.hardware.Camera.PreviewCallback
    public final void onPreviewFrame(byte[] bArr, Camera camera) {
        this.f$0.lambda$onPostMain$0(this.f$1, bArr, camera);
    }
}
