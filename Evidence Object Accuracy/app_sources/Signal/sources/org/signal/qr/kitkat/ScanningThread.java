package org.signal.qr.kitkat;

import com.google.zxing.DecodeHintType;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import org.signal.core.util.logging.Log;
import org.signal.qr.QrProcessor;
import org.signal.qr.kitkat.QrCameraView;

/* loaded from: classes3.dex */
public class ScanningThread extends Thread implements QrCameraView.PreviewCallback {
    private static final String TAG = Log.tag(ScanningThread.class);
    private final Map<DecodeHintType, String> hints = new HashMap();
    private QrCameraView.PreviewFrame previewFrame;
    private final QrProcessor processor = new QrProcessor();
    private final AtomicReference<ScanListener> scanListener = new AtomicReference<>();
    private boolean scanning = true;

    public void setCharacterSet(String str) {
        this.hints.put(DecodeHintType.CHARACTER_SET, str);
    }

    public void setScanListener(ScanListener scanListener) {
        this.scanListener.set(scanListener);
    }

    @Override // org.signal.qr.kitkat.QrCameraView.PreviewCallback
    public void onPreviewFrame(QrCameraView.PreviewFrame previewFrame) {
        try {
            synchronized (this) {
                this.previewFrame = previewFrame;
                notify();
            }
        } catch (RuntimeException e) {
            Log.w(TAG, e);
        }
    }

    @Override // java.lang.Thread, java.lang.Runnable
    public void run() {
        boolean z;
        while (true) {
            synchronized (this) {
                while (true) {
                    z = this.scanning;
                    if (!z || this.previewFrame != null) {
                        break;
                    }
                    waitFor();
                }
                if (z) {
                    QrCameraView.PreviewFrame previewFrame = this.previewFrame;
                    this.previewFrame = null;
                    String scannedData = this.processor.getScannedData(previewFrame.getData(), previewFrame.getWidth(), previewFrame.getHeight());
                    ScanListener scanListener = this.scanListener.get();
                    if (scannedData != null && scanListener != null) {
                        scanListener.onQrDataFound(scannedData);
                        return;
                    }
                } else {
                    return;
                }
            }
        }
    }

    public void stopScanning() {
        synchronized (this) {
            this.scanning = false;
            notify();
        }
    }

    private void waitFor() {
        try {
            wait(0);
        } catch (InterruptedException e) {
            throw new AssertionError(e);
        }
    }
}
