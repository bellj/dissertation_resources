package org.signal.qr.kitkat;

import android.hardware.Camera;
import j$.util.function.Consumer;
import org.signal.qr.kitkat.QrCameraView;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class QrCameraView$3$$ExternalSyntheticLambda0 implements Consumer {
    public final /* synthetic */ QrCameraView.AnonymousClass3 f$0;
    public final /* synthetic */ QrCameraView.PreviewCallback f$1;

    public /* synthetic */ QrCameraView$3$$ExternalSyntheticLambda0(QrCameraView.AnonymousClass3 r1, QrCameraView.PreviewCallback previewCallback) {
        this.f$0 = r1;
        this.f$1 = previewCallback;
    }

    @Override // j$.util.function.Consumer
    public final void accept(Object obj) {
        this.f$0.lambda$onPostMain$1(this.f$1, (Camera) obj);
    }

    @Override // j$.util.function.Consumer
    public /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }
}
