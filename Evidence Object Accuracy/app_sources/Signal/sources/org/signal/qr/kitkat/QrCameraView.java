package org.signal.qr.kitkat;

import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.OrientationEventListener;
import android.view.ViewGroup;
import j$.util.Optional;
import j$.util.function.Consumer;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;

/* loaded from: classes3.dex */
public class QrCameraView extends ViewGroup {
    private static final String TAG = Log.tag(QrCameraView.class);
    private volatile Optional<Camera> camera;
    private final int cameraId;
    private volatile int displayOrientation;
    private final List<CameraViewListener> listeners;
    private final OnOrientationChange onOrientationChange;
    private int outputOrientation;
    private Camera.Size previewSize;
    private State state;
    private final CameraSurfaceView surface;

    /* loaded from: classes3.dex */
    public interface CameraViewListener {
        void onCameraFail();

        void onCameraStart();

        void onCameraStop();
    }

    /* loaded from: classes3.dex */
    public interface PreviewCallback {
        void onPreviewFrame(PreviewFrame previewFrame);
    }

    /* loaded from: classes3.dex */
    public enum State {
        PAUSED,
        RESUMED,
        ACTIVE
    }

    public QrCameraView(Context context) {
        this(context, null);
    }

    public QrCameraView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public QrCameraView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.camera = Optional.empty();
        this.cameraId = 0;
        this.displayOrientation = -1;
        this.state = State.PAUSED;
        this.listeners = Collections.synchronizedList(new LinkedList());
        this.outputOrientation = -1;
        setBackgroundColor(-16777216);
        CameraSurfaceView cameraSurfaceView = new CameraSurfaceView(getContext());
        this.surface = cameraSurfaceView;
        this.onOrientationChange = new OnOrientationChange(context.getApplicationContext());
        addView(cameraSurfaceView);
    }

    public void onResume() {
        if (this.state == State.PAUSED) {
            this.state = State.RESUMED;
            Log.i(TAG, "onResume() queued");
            enqueueTask(new SerialAsyncTask<Void>() { // from class: org.signal.qr.kitkat.QrCameraView.1
                @Override // org.signal.qr.kitkat.QrCameraView.SerialAsyncTask
                public Void onRunBackground() {
                    try {
                        long currentTimeMillis = System.currentTimeMillis();
                        QrCameraView.this.camera = Optional.ofNullable(Camera.open(0));
                        String str = QrCameraView.TAG;
                        Log.i(str, "camera.open() -> " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
                        synchronized (QrCameraView.this) {
                            QrCameraView.this.notifyAll();
                        }
                        QrCameraView.this.camera.ifPresent(new QrCameraView$1$$ExternalSyntheticLambda0(this));
                        return null;
                    } catch (Exception e) {
                        Log.w(QrCameraView.TAG, e);
                        return null;
                    }
                }

                public /* synthetic */ void lambda$onRunBackground$0(Camera camera) {
                    QrCameraView.this.onCameraReady(camera);
                }

                public void onPostMain(Void r2) {
                    if (!QrCameraView.this.camera.isPresent()) {
                        Log.w(QrCameraView.TAG, "tried to open camera but got null");
                        for (CameraViewListener cameraViewListener : QrCameraView.this.listeners) {
                            cameraViewListener.onCameraFail();
                        }
                        return;
                    }
                    if (QrCameraView.this.getActivity().getRequestedOrientation() != -1) {
                        QrCameraView.this.onOrientationChange.enable();
                    }
                    Log.i(QrCameraView.TAG, "onResume() completed");
                }
            });
        }
    }

    public void onPause() {
        State state = this.state;
        State state2 = State.PAUSED;
        if (state != state2) {
            this.state = state2;
            Log.i(TAG, "onPause() queued");
            enqueueTask(new SerialAsyncTask<Void>() { // from class: org.signal.qr.kitkat.QrCameraView.2
                private Optional<Camera> cameraToDestroy;

                /* access modifiers changed from: protected */
                @Override // org.signal.qr.kitkat.QrCameraView.SerialAsyncTask
                public void onPreMain() {
                    this.cameraToDestroy = QrCameraView.this.camera;
                    QrCameraView.this.camera = Optional.empty();
                }

                @Override // org.signal.qr.kitkat.QrCameraView.SerialAsyncTask
                public Void onRunBackground() {
                    if (this.cameraToDestroy.isPresent()) {
                        try {
                            QrCameraView.this.stopPreview();
                            this.cameraToDestroy.get().setPreviewCallback(null);
                            this.cameraToDestroy.get().release();
                            Log.w(QrCameraView.TAG, "released old camera instance");
                        } catch (Exception e) {
                            Log.w(QrCameraView.TAG, e);
                        }
                    }
                    return null;
                }

                public void onPostMain(Void r2) {
                    QrCameraView.this.onOrientationChange.disable();
                    QrCameraView.this.displayOrientation = -1;
                    QrCameraView.this.outputOrientation = -1;
                    QrCameraView qrCameraView = QrCameraView.this;
                    qrCameraView.removeView(qrCameraView.surface);
                    QrCameraView qrCameraView2 = QrCameraView.this;
                    qrCameraView2.addView(qrCameraView2.surface);
                    Log.i(QrCameraView.TAG, "onPause() completed");
                }
            });
            for (CameraViewListener cameraViewListener : this.listeners) {
                cameraViewListener.onCameraStop();
            }
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    protected void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7 = i3 - i;
        int i8 = i4 - i2;
        if (!this.camera.isPresent() || this.previewSize == null) {
            i5 = i7;
            i6 = i8;
        } else if (this.displayOrientation == 90 || this.displayOrientation == 270) {
            Camera.Size size = this.previewSize;
            i5 = size.height;
            i6 = size.width;
        } else {
            Camera.Size size2 = this.previewSize;
            i5 = size2.width;
            i6 = size2.height;
        }
        if (i6 == 0 || i5 == 0) {
            Log.w(TAG, "skipping layout due to zero-width/height preview size");
            return;
        }
        int i9 = i7 * i6;
        int i10 = i8 * i5;
        if (i9 > i10) {
            int i11 = i9 / i5;
            this.surface.layout(0, (i8 - i11) / 2, i7, (i8 + i11) / 2);
            return;
        }
        int i12 = i10 / i6;
        this.surface.layout((i7 - i12) / 2, 0, (i7 + i12) / 2, i8);
    }

    @Override // android.view.View
    protected void onSizeChanged(int i, int i2, int i3, int i4) {
        String str = TAG;
        Log.i(str, "onSizeChanged(" + i3 + "x" + i4 + " -> " + i + "x" + i2 + ")");
        super.onSizeChanged(i, i2, i3, i4);
        this.camera.ifPresent(new Consumer() { // from class: org.signal.qr.kitkat.QrCameraView$$ExternalSyntheticLambda0
            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                QrCameraView.this.lambda$onSizeChanged$0((Camera) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ Consumer andThen(Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
    }

    public /* synthetic */ void lambda$onSizeChanged$0(Camera camera) {
        startPreview(camera.getParameters());
    }

    public void setPreviewCallback(final PreviewCallback previewCallback) {
        enqueueTask(new PostInitializationTask<Void>() { // from class: org.signal.qr.kitkat.QrCameraView.3
            public /* synthetic */ void lambda$onPostMain$1(PreviewCallback previewCallback2, Camera camera) {
                camera.setPreviewCallback(new QrCameraView$3$$ExternalSyntheticLambda1(this, previewCallback2));
            }

            public void onPostMain(Void r3) {
                QrCameraView.this.camera.ifPresent(new QrCameraView$3$$ExternalSyntheticLambda0(this, previewCallback));
            }

            public /* synthetic */ void lambda$onPostMain$0(PreviewCallback previewCallback2, byte[] bArr, Camera camera) {
                if (QrCameraView.this.camera.isPresent()) {
                    int cameraPictureOrientation = QrCameraView.this.getCameraPictureOrientation();
                    Camera.Size previewSize = camera.getParameters().getPreviewSize();
                    if (bArr != null) {
                        previewCallback2.onPreviewFrame(new PreviewFrame(bArr, previewSize.width, previewSize.height, cameraPictureOrientation));
                    }
                }
            }
        });
    }

    public void onCameraReady(final Camera camera) {
        final Camera.Parameters parameters = camera.getParameters();
        parameters.setRecordingHint(true);
        List<String> supportedFocusModes = parameters.getSupportedFocusModes();
        if (supportedFocusModes.contains("continuous-picture")) {
            parameters.setFocusMode("continuous-picture");
        } else if (supportedFocusModes.contains("continuous-video")) {
            parameters.setFocusMode("continuous-video");
        }
        this.displayOrientation = CameraUtils.getCameraDisplayOrientation(getActivity(), getCameraInfo());
        camera.setDisplayOrientation(this.displayOrientation);
        camera.setParameters(parameters);
        enqueueTask(new PostInitializationTask<Void>() { // from class: org.signal.qr.kitkat.QrCameraView.4
            @Override // org.signal.qr.kitkat.QrCameraView.SerialAsyncTask
            public Void onRunBackground() {
                try {
                    camera.setPreviewDisplay(QrCameraView.this.surface.getHolder());
                    QrCameraView.this.startPreview(parameters);
                    return null;
                } catch (Exception e) {
                    Log.w(QrCameraView.TAG, "couldn't set preview display", e);
                    return null;
                }
            }
        });
    }

    public void startPreview(Camera.Parameters parameters) {
        this.camera.ifPresent(new Consumer(parameters) { // from class: org.signal.qr.kitkat.QrCameraView$$ExternalSyntheticLambda1
            public final /* synthetic */ Camera.Parameters f$1;

            {
                this.f$1 = r2;
            }

            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                QrCameraView.this.lambda$startPreview$2(this.f$1, (Camera) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ Consumer andThen(Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
    }

    public /* synthetic */ void lambda$startPreview$2(Camera.Parameters parameters, Camera camera) {
        try {
            Camera.Size preferredPreviewSize = getPreferredPreviewSize(parameters);
            if (preferredPreviewSize == null || parameters.getPreviewSize().equals(preferredPreviewSize)) {
                this.previewSize = parameters.getPreviewSize();
            } else {
                String str = TAG;
                Log.i(str, "starting preview with size " + preferredPreviewSize.width + "x" + preferredPreviewSize.height);
                if (this.state == State.ACTIVE) {
                    stopPreview();
                }
                this.previewSize = preferredPreviewSize;
                parameters.setPreviewSize(preferredPreviewSize.width, preferredPreviewSize.height);
                camera.setParameters(parameters);
            }
            long currentTimeMillis = System.currentTimeMillis();
            camera.startPreview();
            String str2 = TAG;
            Log.i(str2, "camera.startPreview() -> " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
            this.state = State.ACTIVE;
            ThreadUtil.runOnMain(new Runnable() { // from class: org.signal.qr.kitkat.QrCameraView$$ExternalSyntheticLambda3
                @Override // java.lang.Runnable
                public final void run() {
                    QrCameraView.this.lambda$startPreview$1();
                }
            });
        } catch (Exception e) {
            Log.w(TAG, e);
        }
    }

    public /* synthetic */ void lambda$startPreview$1() {
        requestLayout();
        for (CameraViewListener cameraViewListener : this.listeners) {
            cameraViewListener.onCameraStart();
        }
    }

    public void stopPreview() {
        this.camera.ifPresent(new Consumer() { // from class: org.signal.qr.kitkat.QrCameraView$$ExternalSyntheticLambda2
            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                QrCameraView.this.lambda$stopPreview$3((Camera) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ Consumer andThen(Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
    }

    public /* synthetic */ void lambda$stopPreview$3(Camera camera) {
        try {
            camera.stopPreview();
            this.state = State.RESUMED;
        } catch (Exception e) {
            Log.w(TAG, e);
        }
    }

    private Camera.Size getPreferredPreviewSize(Camera.Parameters parameters) {
        return CameraUtils.getPreferredPreviewSize(this.displayOrientation, getMeasuredWidth(), getMeasuredHeight(), parameters);
    }

    public int getCameraPictureOrientation() {
        if (getActivity().getRequestedOrientation() != -1) {
            this.outputOrientation = getCameraPictureRotation(getActivity().getWindowManager().getDefaultDisplay().getOrientation());
        } else if (getCameraInfo().facing == 1) {
            this.outputOrientation = (360 - this.displayOrientation) % 360;
        } else {
            this.outputOrientation = this.displayOrientation;
        }
        return this.outputOrientation;
    }

    private Camera.CameraInfo getCameraInfo() {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(0, cameraInfo);
        return cameraInfo;
    }

    public Activity getActivity() {
        return (Activity) getContext();
    }

    public int getCameraPictureRotation(int i) {
        Camera.CameraInfo cameraInfo = getCameraInfo();
        int i2 = ((i + 45) / 90) * 90;
        if (cameraInfo.facing == 1) {
            return ((cameraInfo.orientation - i2) + 360) % 360;
        }
        return (cameraInfo.orientation + i2) % 360;
    }

    /* loaded from: classes3.dex */
    public class OnOrientationChange extends OrientationEventListener {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public OnOrientationChange(Context context) {
            super(context);
            QrCameraView.this = r1;
            disable();
        }

        @Override // android.view.OrientationEventListener
        public void onOrientationChanged(int i) {
            QrCameraView.this.camera.ifPresent(new QrCameraView$OnOrientationChange$$ExternalSyntheticLambda0(this, i));
        }

        public /* synthetic */ void lambda$onOrientationChanged$0(int i, Camera camera) {
            int cameraPictureRotation;
            if (i != -1 && (cameraPictureRotation = QrCameraView.this.getCameraPictureRotation(i)) != QrCameraView.this.outputOrientation) {
                QrCameraView.this.outputOrientation = cameraPictureRotation;
                Camera.Parameters parameters = camera.getParameters();
                parameters.setRotation(QrCameraView.this.outputOrientation);
                try {
                    camera.setParameters(parameters);
                } catch (Exception e) {
                    Log.e(QrCameraView.TAG, "Exception updating camera parameters in orientation change", e);
                }
            }
        }
    }

    private void enqueueTask(SerialAsyncTask<?> serialAsyncTask) {
        AsyncTask.SERIAL_EXECUTOR.execute(serialAsyncTask);
    }

    /* loaded from: classes3.dex */
    public static abstract class SerialAsyncTask<Result> implements Runnable {
        /* renamed from: onPostMain */
        public void lambda$run$0(Result result) {
        }

        public void onPreMain() {
        }

        protected Result onRunBackground() {
            return null;
        }

        protected boolean onWait() {
            return true;
        }

        @Override // java.lang.Runnable
        public final void run() {
            if (!onWait()) {
                Log.w(QrCameraView.TAG, "skipping task, preconditions not met in onWait()");
                return;
            }
            ThreadUtil.runOnMainSync(new QrCameraView$SerialAsyncTask$$ExternalSyntheticLambda0(this));
            ThreadUtil.runOnMainSync(new QrCameraView$SerialAsyncTask$$ExternalSyntheticLambda1(this, onRunBackground()));
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes3.dex */
    public abstract class PostInitializationTask<Result> extends SerialAsyncTask<Result> {
        private PostInitializationTask() {
            QrCameraView.this = r1;
        }

        @Override // org.signal.qr.kitkat.QrCameraView.SerialAsyncTask
        protected boolean onWait() {
            synchronized (QrCameraView.this) {
                if (!QrCameraView.this.camera.isPresent()) {
                    return false;
                }
                while (true) {
                    if (QrCameraView.this.getMeasuredHeight() > 0 && QrCameraView.this.getMeasuredWidth() > 0 && QrCameraView.this.surface.isReady()) {
                        return true;
                    }
                    Log.i(QrCameraView.TAG, String.format("waiting. surface ready? %s", Boolean.valueOf(QrCameraView.this.surface.isReady())));
                    QrCameraView.this.waitFor();
                }
            }
        }
    }

    public void waitFor() {
        try {
            wait(0);
        } catch (InterruptedException e) {
            throw new AssertionError(e);
        }
    }

    /* loaded from: classes3.dex */
    public static class PreviewFrame {
        private final byte[] data;
        private final int height;
        private final int orientation;
        private final int width;

        public PreviewFrame(byte[] bArr, int i, int i2, int i3) {
            this.data = bArr;
            this.width = i;
            this.height = i2;
            this.orientation = i3;
        }

        public byte[] getData() {
            return this.data;
        }

        public int getWidth() {
            return this.width;
        }

        public int getHeight() {
            return this.height;
        }
    }
}
