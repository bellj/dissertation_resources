package org.signal.qr.kitkat;

import org.signal.qr.kitkat.QrCameraView;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class QrCameraView$SerialAsyncTask$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ QrCameraView.SerialAsyncTask f$0;
    public final /* synthetic */ Object f$1;

    public /* synthetic */ QrCameraView$SerialAsyncTask$$ExternalSyntheticLambda1(QrCameraView.SerialAsyncTask serialAsyncTask, Object obj) {
        this.f$0 = serialAsyncTask;
        this.f$1 = obj;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$run$0(this.f$1);
    }
}
