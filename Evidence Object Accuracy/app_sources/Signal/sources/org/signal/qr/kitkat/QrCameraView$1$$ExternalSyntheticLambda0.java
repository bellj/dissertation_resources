package org.signal.qr.kitkat;

import android.hardware.Camera;
import j$.util.function.Consumer;
import org.signal.qr.kitkat.QrCameraView;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class QrCameraView$1$$ExternalSyntheticLambda0 implements Consumer {
    public final /* synthetic */ QrCameraView.AnonymousClass1 f$0;

    public /* synthetic */ QrCameraView$1$$ExternalSyntheticLambda0(QrCameraView.AnonymousClass1 r1) {
        this.f$0 = r1;
    }

    @Override // j$.util.function.Consumer
    public final void accept(Object obj) {
        this.f$0.lambda$onRunBackground$0((Camera) obj);
    }

    @Override // j$.util.function.Consumer
    public /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }
}
