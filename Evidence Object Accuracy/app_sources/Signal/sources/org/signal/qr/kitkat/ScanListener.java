package org.signal.qr.kitkat;

/* loaded from: classes3.dex */
public interface ScanListener {
    void onQrDataFound(String str);
}
