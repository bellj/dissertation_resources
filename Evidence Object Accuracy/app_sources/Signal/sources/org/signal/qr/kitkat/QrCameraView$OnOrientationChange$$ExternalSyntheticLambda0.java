package org.signal.qr.kitkat;

import android.hardware.Camera;
import j$.util.function.Consumer;
import org.signal.qr.kitkat.QrCameraView;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class QrCameraView$OnOrientationChange$$ExternalSyntheticLambda0 implements Consumer {
    public final /* synthetic */ QrCameraView.OnOrientationChange f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ QrCameraView$OnOrientationChange$$ExternalSyntheticLambda0(QrCameraView.OnOrientationChange onOrientationChange, int i) {
        this.f$0 = onOrientationChange;
        this.f$1 = i;
    }

    @Override // j$.util.function.Consumer
    public final void accept(Object obj) {
        this.f$0.lambda$onOrientationChanged$0(this.f$1, (Camera) obj);
    }

    @Override // j$.util.function.Consumer
    public /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }
}
