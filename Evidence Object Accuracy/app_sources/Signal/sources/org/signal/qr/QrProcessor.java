package org.signal.qr;

import androidx.camera.core.ImageProxy;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.NotFoundException;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.PushDatabase;

/* compiled from: QrProcessor.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0002\b\u0004\u0018\u0000 \u00122\u00020\u0001:\u0001\u0012B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\b\u001a\u0004\u0018\u00010\t2\u0006\u0010\n\u001a\u00020\u000bJ\u0012\u0010\b\u001a\u0004\u0018\u00010\t2\u0006\u0010\f\u001a\u00020\rH\u0002J \u0010\b\u001a\u0004\u0018\u00010\t2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u0004R\u000e\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013"}, d2 = {"Lorg/signal/qr/QrProcessor;", "", "()V", "previousHeight", "", "previousWidth", "reader", "Lcom/google/zxing/qrcode/QRCodeReader;", "getScannedData", "", "proxy", "Landroidx/camera/core/ImageProxy;", PushDatabase.SOURCE_E164, "Lcom/google/zxing/LuminanceSource;", "data", "", "width", "height", "Companion", "qr_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class QrProcessor {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(QrProcessor.class);
    private int previousHeight;
    private int previousWidth;
    private final QRCodeReader reader = new QRCodeReader();

    public final String getScannedData(ImageProxy imageProxy) {
        Intrinsics.checkNotNullParameter(imageProxy, "proxy");
        return getScannedData(new ImageProxyLuminanceSource(imageProxy));
    }

    public final String getScannedData(byte[] bArr, int i, int i2) {
        Intrinsics.checkNotNullParameter(bArr, "data");
        return getScannedData(new PlanarYUVLuminanceSource(bArr, i, i2, 0, 0, i, i2, false));
    }

    private final String getScannedData(LuminanceSource luminanceSource) {
        try {
            if (!(luminanceSource.getWidth() == this.previousWidth && luminanceSource.getHeight() == this.previousHeight)) {
                String str = TAG;
                Log.i(str, "Processing " + luminanceSource.getWidth() + " x " + luminanceSource.getHeight() + " image");
                this.previousWidth = luminanceSource.getWidth();
                this.previousHeight = luminanceSource.getHeight();
            }
            Result decode = this.reader.decode(new BinaryBitmap(new HybridBinarizer(luminanceSource)), MapsKt__MapsKt.mapOf(TuplesKt.to(DecodeHintType.TRY_HARDER, Boolean.TRUE), TuplesKt.to(DecodeHintType.CHARACTER_SET, "ISO-8859-1")));
            if (decode != null) {
                return decode.getText();
            }
            return null;
        } catch (ChecksumException e) {
            Log.w(TAG, "QR code read and decoded, but checksum failed", e);
            return null;
        } catch (FormatException e2) {
            Log.w(TAG, "Thrown when a barcode was successfully detected, but some aspect of the content did not conform to the barcodes format rules.", e2);
            return null;
        } catch (NotFoundException unused) {
            return null;
        } catch (NullPointerException e3) {
            Log.w(TAG, "Random null", e3);
            return null;
        }
    }

    /* compiled from: QrProcessor.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/signal/qr/QrProcessor$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "qr_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
