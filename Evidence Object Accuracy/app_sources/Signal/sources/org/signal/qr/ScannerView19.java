package org.signal.qr;

import android.content.Context;
import android.widget.FrameLayout;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.qr.kitkat.QrCameraView;
import org.signal.qr.kitkat.ScanListener;
import org.signal.qr.kitkat.ScanningThread;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;

/* compiled from: ScannerView19.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0001\u0018\u00002\u00020\u00012\u00020\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u000e¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lorg/signal/qr/ScannerView19;", "Landroid/widget/FrameLayout;", "Lorg/signal/qr/ScannerView;", "context", "Landroid/content/Context;", "scanListener", "Lorg/signal/qr/kitkat/ScanListener;", "(Landroid/content/Context;Lorg/signal/qr/kitkat/ScanListener;)V", "cameraView", "Lorg/signal/qr/kitkat/QrCameraView;", "scanningThread", "Lorg/signal/qr/kitkat/ScanningThread;", NotificationProfileDatabase.NotificationProfileScheduleTable.START, "", "lifecycleOwner", "Landroidx/lifecycle/LifecycleOwner;", "qr_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ScannerView19 extends FrameLayout implements ScannerView {
    private final QrCameraView cameraView;
    private final ScanListener scanListener;
    private ScanningThread scanningThread;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ScannerView19(Context context, ScanListener scanListener) {
        super(context);
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(scanListener, "scanListener");
        this.scanListener = scanListener;
        QrCameraView qrCameraView = new QrCameraView(context);
        this.cameraView = qrCameraView;
        qrCameraView.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        addView(qrCameraView);
    }

    @Override // org.signal.qr.ScannerView
    public void start(LifecycleOwner lifecycleOwner) {
        Intrinsics.checkNotNullParameter(lifecycleOwner, "lifecycleOwner");
        lifecycleOwner.getLifecycle().addObserver(new DefaultLifecycleObserver(this) { // from class: org.signal.qr.ScannerView19$start$1
            final /* synthetic */ ScannerView19 this$0;

            @Override // androidx.lifecycle.FullLifecycleObserver
            public /* synthetic */ void onCreate(LifecycleOwner lifecycleOwner2) {
                DefaultLifecycleObserver.CC.$default$onCreate(this, lifecycleOwner2);
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public /* synthetic */ void onDestroy(LifecycleOwner lifecycleOwner2) {
                DefaultLifecycleObserver.CC.$default$onDestroy(this, lifecycleOwner2);
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public /* synthetic */ void onStart(LifecycleOwner lifecycleOwner2) {
                DefaultLifecycleObserver.CC.$default$onStart(this, lifecycleOwner2);
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public /* synthetic */ void onStop(LifecycleOwner lifecycleOwner2) {
                DefaultLifecycleObserver.CC.$default$onStop(this, lifecycleOwner2);
            }

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public void onResume(LifecycleOwner lifecycleOwner2) {
                Intrinsics.checkNotNullParameter(lifecycleOwner2, "owner");
                ScanningThread scanningThread = new ScanningThread();
                scanningThread.setScanListener(this.this$0.scanListener);
                this.this$0.cameraView.onResume();
                this.this$0.cameraView.setPreviewCallback(scanningThread);
                scanningThread.start();
                this.this$0.scanningThread = scanningThread;
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public void onPause(LifecycleOwner lifecycleOwner2) {
                Intrinsics.checkNotNullParameter(lifecycleOwner2, "owner");
                this.this$0.cameraView.onPause();
                ScanningThread scanningThread = this.this$0.scanningThread;
                if (scanningThread != null) {
                    scanningThread.stopScanning();
                }
                this.this$0.scanningThread = null;
            }
        });
    }
}
