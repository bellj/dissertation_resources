package org.signal.qr;

import android.content.Context;
import android.util.Size;
import android.widget.FrameLayout;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import com.google.common.util.concurrent.ListenableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.signal.qr.kitkat.ScanListener;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;

/* compiled from: ScannerView21.kt */
@Metadata(d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0001\u0018\u0000 \u00192\u00020\u00012\u00020\u0002:\u0001\u0019B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u001a\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\b\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0002J\u0010\u0010\u0017\u001a\u00020\u00142\u0006\u0010\u0018\u001a\u00020\u0016H\u0016R\u0016\u0010\b\u001a\n \n*\u0004\u0018\u00010\t0\tX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001a"}, d2 = {"Lorg/signal/qr/ScannerView21;", "Landroid/widget/FrameLayout;", "Lorg/signal/qr/ScannerView;", "context", "Landroid/content/Context;", "listener", "Lorg/signal/qr/kitkat/ScanListener;", "(Landroid/content/Context;Lorg/signal/qr/kitkat/ScanListener;)V", "analyzerExecutor", "Ljava/util/concurrent/ExecutorService;", "kotlin.jvm.PlatformType", "camera", "Landroidx/camera/core/Camera;", "cameraProvider", "Landroidx/camera/lifecycle/ProcessCameraProvider;", "previewView", "Landroidx/camera/view/PreviewView;", "qrProcessor", "Lorg/signal/qr/QrProcessor;", "onCameraProvider", "", "lifecycle", "Landroidx/lifecycle/LifecycleOwner;", NotificationProfileDatabase.NotificationProfileScheduleTable.START, "lifecycleOwner", "Companion", "qr_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class ScannerView21 extends FrameLayout implements ScannerView {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(ScannerView21.class);
    private final ExecutorService analyzerExecutor = Executors.newSingleThreadExecutor();
    private Camera camera;
    private ProcessCameraProvider cameraProvider;
    private final ScanListener listener;
    private PreviewView previewView;
    private final QrProcessor qrProcessor = new QrProcessor();

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ScannerView21(Context context, ScanListener scanListener) {
        super(context);
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(scanListener, "listener");
        this.listener = scanListener;
        PreviewView previewView = new PreviewView(context);
        this.previewView = previewView;
        previewView.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        addView(this.previewView);
    }

    @Override // org.signal.qr.ScannerView
    public void start(LifecycleOwner lifecycleOwner) {
        Intrinsics.checkNotNullParameter(lifecycleOwner, "lifecycleOwner");
        this.previewView.post(new Runnable(lifecycleOwner) { // from class: org.signal.qr.ScannerView21$$ExternalSyntheticLambda0
            public final /* synthetic */ LifecycleOwner f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ScannerView21.m209start$lambda2(ScannerView21.this, this.f$1);
            }
        });
        lifecycleOwner.getLifecycle().addObserver(new DefaultLifecycleObserver(this) { // from class: org.signal.qr.ScannerView21$start$2
            final /* synthetic */ ScannerView21 this$0;

            @Override // androidx.lifecycle.FullLifecycleObserver
            public /* synthetic */ void onCreate(LifecycleOwner lifecycleOwner2) {
                DefaultLifecycleObserver.CC.$default$onCreate(this, lifecycleOwner2);
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public /* synthetic */ void onPause(LifecycleOwner lifecycleOwner2) {
                DefaultLifecycleObserver.CC.$default$onPause(this, lifecycleOwner2);
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public /* synthetic */ void onResume(LifecycleOwner lifecycleOwner2) {
                DefaultLifecycleObserver.CC.$default$onResume(this, lifecycleOwner2);
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public /* synthetic */ void onStart(LifecycleOwner lifecycleOwner2) {
                DefaultLifecycleObserver.CC.$default$onStart(this, lifecycleOwner2);
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public /* synthetic */ void onStop(LifecycleOwner lifecycleOwner2) {
                DefaultLifecycleObserver.CC.$default$onStop(this, lifecycleOwner2);
            }

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public void onDestroy(LifecycleOwner lifecycleOwner2) {
                Intrinsics.checkNotNullParameter(lifecycleOwner2, "owner");
                this.this$0.cameraProvider = null;
                this.this$0.camera = null;
                this.this$0.analyzerExecutor.shutdown();
            }
        });
    }

    /* renamed from: start$lambda-2 */
    public static final void m209start$lambda2(ScannerView21 scannerView21, LifecycleOwner lifecycleOwner) {
        Intrinsics.checkNotNullParameter(scannerView21, "this$0");
        Intrinsics.checkNotNullParameter(lifecycleOwner, "$lifecycleOwner");
        Log.i(TAG, "Starting");
        ListenableFuture<ProcessCameraProvider> instance = ProcessCameraProvider.getInstance(scannerView21.getContext());
        instance.addListener(new Runnable(lifecycleOwner, instance) { // from class: org.signal.qr.ScannerView21$$ExternalSyntheticLambda1
            public final /* synthetic */ LifecycleOwner f$1;
            public final /* synthetic */ ListenableFuture f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ScannerView21.m210start$lambda2$lambda1$lambda0(ScannerView21.this, this.f$1, this.f$2);
            }
        }, ContextCompat.getMainExecutor(scannerView21.getContext()));
    }

    /* renamed from: start$lambda-2$lambda-1$lambda-0 */
    public static final void m210start$lambda2$lambda1$lambda0(ScannerView21 scannerView21, LifecycleOwner lifecycleOwner, ListenableFuture listenableFuture) {
        Intrinsics.checkNotNullParameter(scannerView21, "this$0");
        Intrinsics.checkNotNullParameter(lifecycleOwner, "$lifecycleOwner");
        Intrinsics.checkNotNullParameter(listenableFuture, "$this_apply");
        try {
            scannerView21.onCameraProvider(lifecycleOwner, (ProcessCameraProvider) listenableFuture.get());
        } catch (Exception e) {
            Log.w(TAG, e);
        }
    }

    private final void onCameraProvider(LifecycleOwner lifecycleOwner, ProcessCameraProvider processCameraProvider) {
        if (processCameraProvider == null) {
            Log.w(TAG, "Camera provider is null");
            return;
        }
        Log.i(TAG, "Initializing use cases");
        Preview build = new Preview.Builder().build();
        Intrinsics.checkNotNullExpressionValue(build, "Builder().build()");
        ImageAnalysis build2 = new ImageAnalysis.Builder().setTargetResolution(new Size(1920, 1080)).setBackpressureStrategy(0).build();
        Intrinsics.checkNotNullExpressionValue(build2, "Builder()\n      .setTarg…LY_LATEST)\n      .build()");
        build2.setAnalyzer(this.analyzerExecutor, new ImageAnalysis.Analyzer() { // from class: org.signal.qr.ScannerView21$$ExternalSyntheticLambda2
            @Override // androidx.camera.core.ImageAnalysis.Analyzer
            public final void analyze(ImageProxy imageProxy) {
                ScannerView21.m208onCameraProvider$lambda4(ScannerView21.this, imageProxy);
            }
        });
        processCameraProvider.unbindAll();
        this.camera = processCameraProvider.bindToLifecycle(lifecycleOwner, CameraSelector.DEFAULT_BACK_CAMERA, build, build2);
        build.setSurfaceProvider(this.previewView.getSurfaceProvider());
        this.cameraProvider = processCameraProvider;
    }

    /* renamed from: onCameraProvider$lambda-4 */
    public static final void m208onCameraProvider$lambda4(ScannerView21 scannerView21, ImageProxy imageProxy) {
        Intrinsics.checkNotNullParameter(scannerView21, "this$0");
        Intrinsics.checkNotNullParameter(imageProxy, "proxy");
        try {
            String scannedData = scannerView21.qrProcessor.getScannedData(imageProxy);
            if (scannedData != null) {
                scannerView21.listener.onQrDataFound(scannedData);
            }
            Unit unit = Unit.INSTANCE;
            th = null;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    /* compiled from: ScannerView21.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/signal/qr/ScannerView21$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "qr_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
