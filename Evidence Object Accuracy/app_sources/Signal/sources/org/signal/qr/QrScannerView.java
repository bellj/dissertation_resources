package org.signal.qr;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.PublishSubject;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.signal.qr.kitkat.ScanListener;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;

/* compiled from: QrScannerView.kt */
@Metadata(d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0018\u0010\u0016\u001a\u00020\u00132\u0006\u0010\u0017\u001a\u00020\u00182\b\b\u0002\u0010\u0014\u001a\u00020\u0015R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000b0\u000fX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u000e¢\u0006\u0002\n\u0000¨\u0006\u001a"}, d2 = {"Lorg/signal/qr/QrScannerView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "qrData", "Lio/reactivex/rxjava3/core/Observable;", "", "getQrData", "()Lio/reactivex/rxjava3/core/Observable;", "qrDataPublish", "Lio/reactivex/rxjava3/subjects/PublishSubject;", "scannerView", "Lorg/signal/qr/ScannerView;", "initScannerView", "", "forceLegacy", "", NotificationProfileDatabase.NotificationProfileScheduleTable.START, "lifecycleOwner", "Landroidx/lifecycle/LifecycleOwner;", "Companion", "qr_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class QrScannerView extends FrameLayout {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(QrScannerView.class);
    private final Observable<String> qrData;
    private final PublishSubject<String> qrDataPublish;
    private ScannerView scannerView;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public QrScannerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ QrScannerView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public QrScannerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkNotNullParameter(context, "context");
        PublishSubject<String> create = PublishSubject.create();
        Intrinsics.checkNotNullExpressionValue(create, "create()");
        this.qrDataPublish = create;
        this.qrData = create;
    }

    public final Observable<String> getQrData() {
        return this.qrData;
    }

    private final void initScannerView(boolean z) {
        FrameLayout frameLayout;
        if (Build.VERSION.SDK_INT < 21 || z) {
            Context context = getContext();
            Intrinsics.checkNotNullExpressionValue(context, "context");
            frameLayout = new ScannerView19(context, new ScanListener() { // from class: org.signal.qr.QrScannerView$$ExternalSyntheticLambda1
                @Override // org.signal.qr.kitkat.ScanListener
                public final void onQrDataFound(String str) {
                    QrScannerView.m202$r8$lambda$SNpjrXqaTD3cvy4YWKhnn464t8(QrScannerView.this, str);
                }
            });
        } else {
            Context context2 = getContext();
            Intrinsics.checkNotNullExpressionValue(context2, "context");
            frameLayout = new ScannerView21(context2, new ScanListener() { // from class: org.signal.qr.QrScannerView$$ExternalSyntheticLambda0
                @Override // org.signal.qr.kitkat.ScanListener
                public final void onQrDataFound(String str) {
                    QrScannerView.m203$r8$lambda$c3WDeRKxVYqRflhwwS_oIOjhwE(QrScannerView.this, str);
                }
            });
        }
        frameLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        addView(frameLayout);
        this.scannerView = (ScannerView) frameLayout;
    }

    /* renamed from: initScannerView$lambda-0 */
    public static final void m204initScannerView$lambda0(QrScannerView qrScannerView, String str) {
        Intrinsics.checkNotNullParameter(qrScannerView, "this$0");
        Intrinsics.checkNotNullParameter(str, "it");
        qrScannerView.qrDataPublish.onNext(str);
    }

    /* renamed from: initScannerView$lambda-1 */
    public static final void m205initScannerView$lambda1(QrScannerView qrScannerView, String str) {
        Intrinsics.checkNotNullParameter(qrScannerView, "this$0");
        Intrinsics.checkNotNullParameter(str, "it");
        qrScannerView.qrDataPublish.onNext(str);
    }

    public final void start(LifecycleOwner lifecycleOwner, boolean z) {
        Intrinsics.checkNotNullParameter(lifecycleOwner, "lifecycleOwner");
        if (this.scannerView != null) {
            Log.w(TAG, "Attempt to start scanning that has already started");
            return;
        }
        initScannerView(z);
        ScannerView scannerView = this.scannerView;
        if (scannerView != null) {
            scannerView.start(lifecycleOwner);
        }
        lifecycleOwner.getLifecycle().addObserver(new DefaultLifecycleObserver(this) { // from class: org.signal.qr.QrScannerView$start$1
            final /* synthetic */ QrScannerView this$0;

            @Override // androidx.lifecycle.FullLifecycleObserver
            public /* synthetic */ void onCreate(LifecycleOwner lifecycleOwner2) {
                DefaultLifecycleObserver.CC.$default$onCreate(this, lifecycleOwner2);
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public /* synthetic */ void onPause(LifecycleOwner lifecycleOwner2) {
                DefaultLifecycleObserver.CC.$default$onPause(this, lifecycleOwner2);
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public /* synthetic */ void onResume(LifecycleOwner lifecycleOwner2) {
                DefaultLifecycleObserver.CC.$default$onResume(this, lifecycleOwner2);
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public /* synthetic */ void onStart(LifecycleOwner lifecycleOwner2) {
                DefaultLifecycleObserver.CC.$default$onStart(this, lifecycleOwner2);
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public /* synthetic */ void onStop(LifecycleOwner lifecycleOwner2) {
                DefaultLifecycleObserver.CC.$default$onStop(this, lifecycleOwner2);
            }

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public void onDestroy(LifecycleOwner lifecycleOwner2) {
                Intrinsics.checkNotNullParameter(lifecycleOwner2, "owner");
                QrScannerView.access$getQrDataPublish$p(this.this$0).onComplete();
            }
        });
    }

    /* compiled from: QrScannerView.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/signal/qr/QrScannerView$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "qr_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
