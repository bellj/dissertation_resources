package org.w3c.dom.smil;

/* loaded from: classes5.dex */
public interface ElementLayout {
    int getHeight();

    int getWidth();
}
