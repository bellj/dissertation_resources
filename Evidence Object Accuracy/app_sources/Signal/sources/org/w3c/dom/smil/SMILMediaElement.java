package org.w3c.dom.smil;

import org.w3c.dom.DOMException;

/* loaded from: classes5.dex */
public interface SMILMediaElement extends ElementTime, SMILElement {
    void setSrc(String str) throws DOMException;
}
