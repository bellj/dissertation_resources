package org.w3c.dom.smil;

import org.w3c.dom.Document;

/* loaded from: classes5.dex */
public interface SMILDocument extends Document, ElementSequentialTimeContainer {
    SMILElement getBody();

    SMILLayoutElement getLayout();
}
