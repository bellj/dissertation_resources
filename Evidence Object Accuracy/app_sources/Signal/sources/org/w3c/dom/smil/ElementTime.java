package org.w3c.dom.smil;

import org.w3c.dom.DOMException;

/* loaded from: classes5.dex */
public interface ElementTime {
    void setDur(float f) throws DOMException;
}
