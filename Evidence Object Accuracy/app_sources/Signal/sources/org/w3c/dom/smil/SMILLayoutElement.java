package org.w3c.dom.smil;

/* loaded from: classes5.dex */
public interface SMILLayoutElement extends SMILElement {
    SMILRootLayoutElement getRootLayout();
}
