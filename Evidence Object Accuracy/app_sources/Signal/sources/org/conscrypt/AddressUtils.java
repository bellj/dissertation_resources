package org.conscrypt;

import java.util.regex.Pattern;

/* loaded from: classes3.dex */
public final class AddressUtils {
    private static final String IP_PATTERN;
    private static Pattern ipPattern;

    private AddressUtils() {
    }

    public static boolean isValidSniHostname(String str) {
        if (str == null) {
            return false;
        }
        if ((str.equalsIgnoreCase("localhost") || str.indexOf(46) != -1) && !isLiteralIpAddress(str) && !str.endsWith(".") && str.indexOf(0) == -1) {
            return true;
        }
        return false;
    }

    static boolean isLiteralIpAddress(String str) {
        Pattern pattern = ipPattern;
        if (pattern == null) {
            pattern = Pattern.compile(IP_PATTERN);
            ipPattern = pattern;
        }
        return pattern.matcher(str).matches();
    }
}
