package org.conscrypt.ct;

/* loaded from: classes3.dex */
public class CTConstants {
    public static final int CERTIFICATE_LENGTH_BYTES;
    public static final int EXTENSIONS_LENGTH_BYTES;
    public static final int HASH_ALGORITHM_LENGTH;
    public static final int ISSUER_KEY_HASH_LENGTH;
    public static final int LOGID_LENGTH;
    public static final int LOG_ENTRY_TYPE_LENGTH;
    public static final String OCSP_SCT_LIST_OID;
    public static final int SCT_LIST_LENGTH_BYTES;
    public static final int SERIALIZED_SCT_LENGTH_BYTES;
    public static final int SIGNATURE_ALGORITHM_LENGTH;
    public static final int SIGNATURE_LENGTH_BYTES;
    public static final int SIGNATURE_TYPE_LENGTH;
    public static final int TIMESTAMP_LENGTH;
    public static final int VERSION_LENGTH;
    public static final String X509_SCT_LIST_OID;
}
