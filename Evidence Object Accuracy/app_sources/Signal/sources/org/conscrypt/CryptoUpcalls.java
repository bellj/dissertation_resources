package org.conscrypt;

import java.security.PrivateKey;
import java.security.Provider;
import java.security.Security;
import java.util.ArrayList;
import java.util.logging.Logger;

/* loaded from: classes3.dex */
final class CryptoUpcalls {
    private static final Logger logger = Logger.getLogger(CryptoUpcalls.class.getName());

    private CryptoUpcalls() {
    }

    private static ArrayList<Provider> getExternalProviders(String str) {
        ArrayList<Provider> arrayList = new ArrayList<>(1);
        Provider[] providers = Security.getProviders(str);
        for (Provider provider : providers) {
            if (!Conscrypt.isConscrypt(provider)) {
                arrayList.add(provider);
            }
        }
        if (arrayList.isEmpty()) {
            logger.warning("Could not find external provider for algorithm: " + str);
        }
        return arrayList;
    }

    static byte[] ecSignDigestWithPrivateKey(PrivateKey privateKey, byte[] bArr) {
        if ("EC".equals(privateKey.getAlgorithm())) {
            return signDigestWithPrivateKey(privateKey, bArr, "NONEwithECDSA");
        }
        throw new RuntimeException("Unexpected key type: " + privateKey.toString());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0010, code lost:
        if (org.conscrypt.Conscrypt.isConscrypt(r1.getProvider()) != false) goto L_0x001e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0053  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] signDigestWithPrivateKey(java.security.PrivateKey r4, byte[] r5, java.lang.String r6) {
        /*
            r0 = 0
            java.security.Signature r1 = java.security.Signature.getInstance(r6)     // Catch: NoSuchAlgorithmException -> 0x0095, InvalidKeyException -> 0x0013
            r1.initSign(r4)     // Catch: NoSuchAlgorithmException -> 0x0095, InvalidKeyException -> 0x0013
            java.security.Provider r2 = r1.getProvider()     // Catch: NoSuchAlgorithmException -> 0x0095, InvalidKeyException -> 0x0013
            boolean r2 = org.conscrypt.Conscrypt.isConscrypt(r2)     // Catch: NoSuchAlgorithmException -> 0x0095, InvalidKeyException -> 0x0013
            if (r2 == 0) goto L_0x001f
            goto L_0x001e
        L_0x0013:
            r1 = move-exception
            java.util.logging.Logger r2 = org.conscrypt.CryptoUpcalls.logger
            java.lang.String r3 = "Preferred provider doesn't support key:"
            r2.warning(r3)
            r1.printStackTrace()
        L_0x001e:
            r1 = r0
        L_0x001f:
            if (r1 != 0) goto L_0x006a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Signature."
            r2.append(r3)
            r2.append(r6)
            java.lang.String r2 = r2.toString()
            java.util.ArrayList r2 = getExternalProviders(r2)
            java.util.Iterator r2 = r2.iterator()
        L_0x003a:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x0051
            java.lang.Object r1 = r2.next()
            java.security.Provider r1 = (java.security.Provider) r1
            java.security.Signature r1 = java.security.Signature.getInstance(r6, r1)     // Catch: NoSuchAlgorithmException | InvalidKeyException -> 0x004e
            r1.initSign(r4)     // Catch: NoSuchAlgorithmException | InvalidKeyException -> 0x004e
            goto L_0x0051
        L_0x004e:
            r1 = r0
            goto L_0x003a
        L_0x0051:
            if (r1 != 0) goto L_0x006a
            java.util.logging.Logger r4 = org.conscrypt.CryptoUpcalls.logger
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r1 = "Could not find provider for algorithm: "
            r5.append(r1)
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            r4.warning(r5)
            return r0
        L_0x006a:
            r1.update(r5)     // Catch: Exception -> 0x0072
            byte[] r4 = r1.sign()     // Catch: Exception -> 0x0072
            return r4
        L_0x0072:
            r5 = move-exception
            java.util.logging.Logger r6 = org.conscrypt.CryptoUpcalls.logger
            java.util.logging.Level r1 = java.util.logging.Level.WARNING
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Exception while signing message with "
            r2.append(r3)
            java.lang.String r4 = r4.getAlgorithm()
            r2.append(r4)
            java.lang.String r4 = " private key:"
            r2.append(r4)
            java.lang.String r4 = r2.toString()
            r6.log(r1, r4, r5)
            return r0
        L_0x0095:
            java.util.logging.Logger r4 = org.conscrypt.CryptoUpcalls.logger
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r1 = "Unsupported signature algorithm: "
            r5.append(r1)
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            r4.warning(r5)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.conscrypt.CryptoUpcalls.signDigestWithPrivateKey(java.security.PrivateKey, byte[], java.lang.String):byte[]");
    }

    static byte[] rsaSignDigestWithPrivateKey(PrivateKey privateKey, int i, byte[] bArr) {
        return rsaOpWithPrivateKey(privateKey, i, 1, bArr);
    }

    static byte[] rsaDecryptWithPrivateKey(PrivateKey privateKey, int i, byte[] bArr) {
        return rsaOpWithPrivateKey(privateKey, i, 2, bArr);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x006e, code lost:
        if (org.conscrypt.Conscrypt.isConscrypt(r1.getProvider()) != false) goto L_0x007b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00b0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] rsaOpWithPrivateKey(java.security.PrivateKey r5, int r6, int r7, byte[] r8) {
        /*
        // Method dump skipped, instructions count: 289
        */
        throw new UnsupportedOperationException("Method not decompiled: org.conscrypt.CryptoUpcalls.rsaOpWithPrivateKey(java.security.PrivateKey, int, int, byte[]):byte[]");
    }
}
