package org.conscrypt;

import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.InvalidParameterException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Locale;
import javax.crypto.BadPaddingException;
import javax.crypto.CipherSpi;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.conscrypt.NativeRef;

/* loaded from: classes3.dex */
public abstract class OpenSSLCipher extends CipherSpi {
    private int blockSize;
    byte[] encodedKey;
    private boolean encrypting;
    byte[] iv;
    Mode mode;
    private Padding padding;

    /* access modifiers changed from: package-private */
    /* loaded from: classes3.dex */
    public enum Mode {
        NONE,
        CBC,
        CTR,
        ECB,
        GCM,
        POLY1305
    }

    abstract void checkSupportedKeySize(int i) throws InvalidKeyException;

    abstract void checkSupportedMode(Mode mode) throws NoSuchAlgorithmException;

    abstract void checkSupportedPadding(Padding padding) throws NoSuchPaddingException;

    abstract int doFinalInternal(byte[] bArr, int i, int i2) throws IllegalBlockSizeException, BadPaddingException, ShortBufferException;

    abstract void engineInitInternal(byte[] bArr, AlgorithmParameterSpec algorithmParameterSpec, SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException;

    abstract String getBaseCipherName();

    abstract int getCipherBlockSize();

    abstract int getOutputSizeForFinal(int i);

    abstract int getOutputSizeForUpdate(int i);

    boolean supportsVariableSizeIv() {
        return false;
    }

    boolean supportsVariableSizeKey() {
        return false;
    }

    abstract int updateInternal(byte[] bArr, int i, int i2, byte[] bArr2, int i3, int i4) throws ShortBufferException;

    /* loaded from: classes3.dex */
    public enum Padding {
        NOPADDING,
        PKCS5PADDING,
        PKCS7PADDING;

        public static Padding getNormalized(String str) {
            Padding valueOf = valueOf(str);
            return valueOf == PKCS7PADDING ? PKCS5PADDING : valueOf;
        }
    }

    public OpenSSLCipher() {
        this.mode = Mode.ECB;
        this.padding = Padding.PKCS5PADDING;
    }

    OpenSSLCipher(Mode mode, Padding padding) {
        this.mode = Mode.ECB;
        Padding padding2 = Padding.NOPADDING;
        this.mode = mode;
        this.padding = padding;
        this.blockSize = getCipherBlockSize();
    }

    @Override // javax.crypto.CipherSpi
    protected void engineSetMode(String str) throws NoSuchAlgorithmException {
        try {
            Mode valueOf = Mode.valueOf(str.toUpperCase(Locale.US));
            checkSupportedMode(valueOf);
            this.mode = valueOf;
        } catch (IllegalArgumentException e) {
            NoSuchAlgorithmException noSuchAlgorithmException = new NoSuchAlgorithmException("No such mode: " + str);
            noSuchAlgorithmException.initCause(e);
            throw noSuchAlgorithmException;
        }
    }

    @Override // javax.crypto.CipherSpi
    protected void engineSetPadding(String str) throws NoSuchPaddingException {
        try {
            Padding normalized = Padding.getNormalized(str.toUpperCase(Locale.US));
            checkSupportedPadding(normalized);
            this.padding = normalized;
        } catch (IllegalArgumentException e) {
            NoSuchPaddingException noSuchPaddingException = new NoSuchPaddingException("No such padding: " + str);
            noSuchPaddingException.initCause(e);
            throw noSuchPaddingException;
        }
    }

    Padding getPadding() {
        return this.padding;
    }

    @Override // javax.crypto.CipherSpi
    protected int engineGetBlockSize() {
        return this.blockSize;
    }

    @Override // javax.crypto.CipherSpi
    protected int engineGetOutputSize(int i) {
        return Math.max(getOutputSizeForUpdate(i), getOutputSizeForFinal(i));
    }

    @Override // javax.crypto.CipherSpi
    protected byte[] engineGetIV() {
        return this.iv;
    }

    @Override // javax.crypto.CipherSpi
    protected AlgorithmParameters engineGetParameters() {
        byte[] bArr = this.iv;
        if (bArr != null && bArr.length > 0) {
            try {
                AlgorithmParameters instance = AlgorithmParameters.getInstance(getBaseCipherName());
                instance.init(new IvParameterSpec(this.iv));
                return instance;
            } catch (NoSuchAlgorithmException | InvalidParameterSpecException unused) {
            }
        }
        return null;
    }

    protected AlgorithmParameterSpec getParameterSpec(AlgorithmParameters algorithmParameters) throws InvalidAlgorithmParameterException {
        if (algorithmParameters == null) {
            return null;
        }
        try {
            return algorithmParameters.getParameterSpec(IvParameterSpec.class);
        } catch (InvalidParameterSpecException e) {
            throw new InvalidAlgorithmParameterException("Params must be convertible to IvParameterSpec", e);
        }
    }

    @Override // javax.crypto.CipherSpi
    protected void engineInit(int i, Key key, SecureRandom secureRandom) throws InvalidKeyException {
        checkAndSetEncodedKey(i, key);
        try {
            engineInitInternal(this.encodedKey, null, secureRandom);
        } catch (InvalidAlgorithmParameterException e) {
            throw new RuntimeException(e);
        }
    }

    @Override // javax.crypto.CipherSpi
    protected void engineInit(int i, Key key, AlgorithmParameterSpec algorithmParameterSpec, SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
        checkAndSetEncodedKey(i, key);
        engineInitInternal(this.encodedKey, algorithmParameterSpec, secureRandom);
    }

    @Override // javax.crypto.CipherSpi
    protected void engineInit(int i, Key key, AlgorithmParameters algorithmParameters, SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
        engineInit(i, key, getParameterSpec(algorithmParameters), secureRandom);
    }

    @Override // javax.crypto.CipherSpi
    protected byte[] engineUpdate(byte[] bArr, int i, int i2) {
        byte[] bArr2;
        int outputSizeForUpdate = getOutputSizeForUpdate(i2);
        if (outputSizeForUpdate > 0) {
            bArr2 = new byte[outputSizeForUpdate];
        } else {
            bArr2 = EmptyArray.BYTE;
        }
        try {
            int updateInternal = updateInternal(bArr, i, i2, bArr2, 0, outputSizeForUpdate);
            if (bArr2.length == updateInternal) {
                return bArr2;
            }
            if (updateInternal == 0) {
                return EmptyArray.BYTE;
            }
            return Arrays.copyOfRange(bArr2, 0, updateInternal);
        } catch (ShortBufferException unused) {
            throw new RuntimeException("calculated buffer size was wrong: " + outputSizeForUpdate);
        }
    }

    @Override // javax.crypto.CipherSpi
    protected int engineUpdate(byte[] bArr, int i, int i2, byte[] bArr2, int i3) throws ShortBufferException {
        return updateInternal(bArr, i, i2, bArr2, i3, getOutputSizeForUpdate(i2));
    }

    @Override // javax.crypto.CipherSpi
    protected byte[] engineDoFinal(byte[] bArr, int i, int i2) throws IllegalBlockSizeException, BadPaddingException {
        int i3;
        int outputSizeForFinal = getOutputSizeForFinal(i2);
        byte[] bArr2 = new byte[outputSizeForFinal];
        if (i2 > 0) {
            try {
                i3 = updateInternal(bArr, i, i2, bArr2, 0, outputSizeForFinal);
            } catch (ShortBufferException e) {
                throw new RuntimeException("our calculated buffer was too small", e);
            }
        } else {
            i3 = 0;
        }
        try {
            int doFinalInternal = i3 + doFinalInternal(bArr2, i3, outputSizeForFinal - i3);
            if (doFinalInternal == outputSizeForFinal) {
                return bArr2;
            }
            if (doFinalInternal == 0) {
                return EmptyArray.BYTE;
            }
            return Arrays.copyOfRange(bArr2, 0, doFinalInternal);
        } catch (ShortBufferException e2) {
            throw new RuntimeException("our calculated buffer was too small", e2);
        }
    }

    @Override // javax.crypto.CipherSpi
    protected int engineDoFinal(byte[] bArr, int i, int i2, byte[] bArr2, int i3) throws ShortBufferException, IllegalBlockSizeException, BadPaddingException {
        int i4;
        if (bArr2 != null) {
            int outputSizeForFinal = getOutputSizeForFinal(i2);
            if (i2 > 0) {
                i4 = updateInternal(bArr, i, i2, bArr2, i3, outputSizeForFinal);
                i3 += i4;
                outputSizeForFinal -= i4;
            } else {
                i4 = 0;
            }
            return i4 + doFinalInternal(bArr2, i3, outputSizeForFinal);
        }
        throw new NullPointerException("output == null");
    }

    @Override // javax.crypto.CipherSpi
    protected byte[] engineWrap(Key key) throws IllegalBlockSizeException, InvalidKeyException {
        try {
            byte[] encoded = key.getEncoded();
            return engineDoFinal(encoded, 0, encoded.length);
        } catch (BadPaddingException e) {
            IllegalBlockSizeException illegalBlockSizeException = new IllegalBlockSizeException();
            illegalBlockSizeException.initCause(e);
            throw illegalBlockSizeException;
        }
    }

    @Override // javax.crypto.CipherSpi
    protected Key engineUnwrap(byte[] bArr, String str, int i) throws InvalidKeyException, NoSuchAlgorithmException {
        try {
            byte[] engineDoFinal = engineDoFinal(bArr, 0, bArr.length);
            if (i == 1) {
                return KeyFactory.getInstance(str).generatePublic(new X509EncodedKeySpec(engineDoFinal));
            }
            if (i == 2) {
                return KeyFactory.getInstance(str).generatePrivate(new PKCS8EncodedKeySpec(engineDoFinal));
            }
            if (i == 3) {
                return new SecretKeySpec(engineDoFinal, str);
            }
            throw new UnsupportedOperationException("wrappedKeyType == " + i);
        } catch (InvalidKeySpecException e) {
            throw new InvalidKeyException(e);
        } catch (BadPaddingException e2) {
            throw new InvalidKeyException(e2);
        } catch (IllegalBlockSizeException e3) {
            throw new InvalidKeyException(e3);
        }
    }

    @Override // javax.crypto.CipherSpi
    protected int engineGetKeySize(Key key) throws InvalidKeyException {
        if (key instanceof SecretKey) {
            byte[] encoded = key.getEncoded();
            if (encoded != null) {
                checkSupportedKeySize(encoded.length);
                return encoded.length * 8;
            }
            throw new InvalidKeyException("key.getEncoded() == null");
        }
        throw new InvalidKeyException("Only SecretKey is supported");
    }

    private byte[] checkAndSetEncodedKey(int i, Key key) throws InvalidKeyException {
        if (i == 1 || i == 3) {
            this.encrypting = true;
        } else if (i == 2 || i == 4) {
            this.encrypting = false;
        } else {
            throw new InvalidParameterException("Unsupported opmode " + i);
        }
        if (key instanceof SecretKey) {
            byte[] encoded = key.getEncoded();
            if (encoded != null) {
                checkSupportedKeySize(encoded.length);
                this.encodedKey = encoded;
                return encoded;
            }
            throw new InvalidKeyException("key.getEncoded() == null");
        }
        throw new InvalidKeyException("Only SecretKey is supported");
    }

    public boolean isEncrypting() {
        return this.encrypting;
    }

    /* loaded from: classes3.dex */
    public static abstract class EVP_CIPHER extends OpenSSLCipher {
        boolean calledUpdate;
        private final NativeRef.EVP_CIPHER_CTX cipherCtx = new NativeRef.EVP_CIPHER_CTX(NativeCrypto.EVP_CIPHER_CTX_new());
        private int modeBlockSize;

        abstract String getCipherName(int i, Mode mode);

        public EVP_CIPHER(Mode mode, Padding padding) {
            super(mode, padding);
        }

        @Override // org.conscrypt.OpenSSLCipher
        void engineInitInternal(byte[] bArr, AlgorithmParameterSpec algorithmParameterSpec, SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
            byte[] iv = algorithmParameterSpec instanceof IvParameterSpec ? ((IvParameterSpec) algorithmParameterSpec).getIV() : null;
            long EVP_get_cipherbyname = NativeCrypto.EVP_get_cipherbyname(getCipherName(bArr.length, this.mode));
            if (EVP_get_cipherbyname != 0) {
                boolean isEncrypting = isEncrypting();
                int EVP_CIPHER_iv_length = NativeCrypto.EVP_CIPHER_iv_length(EVP_get_cipherbyname);
                if (iv != null || EVP_CIPHER_iv_length == 0) {
                    if (EVP_CIPHER_iv_length == 0 && iv != null) {
                        throw new InvalidAlgorithmParameterException("IV not used in " + this.mode + " mode");
                    } else if (!(iv == null || iv.length == EVP_CIPHER_iv_length)) {
                        throw new InvalidAlgorithmParameterException("expected IV length of " + EVP_CIPHER_iv_length + " but was " + iv.length);
                    }
                } else if (isEncrypting) {
                    iv = new byte[EVP_CIPHER_iv_length];
                    if (secureRandom != null) {
                        secureRandom.nextBytes(iv);
                    } else {
                        NativeCrypto.RAND_bytes(iv);
                    }
                } else {
                    throw new InvalidAlgorithmParameterException("IV must be specified in " + this.mode + " mode");
                }
                this.iv = iv;
                if (supportsVariableSizeKey()) {
                    NativeCrypto.EVP_CipherInit_ex(this.cipherCtx, EVP_get_cipherbyname, null, null, isEncrypting);
                    NativeCrypto.EVP_CIPHER_CTX_set_key_length(this.cipherCtx, bArr.length);
                    NativeCrypto.EVP_CipherInit_ex(this.cipherCtx, 0, bArr, iv, isEncrypting());
                } else {
                    NativeCrypto.EVP_CipherInit_ex(this.cipherCtx, EVP_get_cipherbyname, bArr, iv, isEncrypting);
                }
                NativeCrypto.EVP_CIPHER_CTX_set_padding(this.cipherCtx, getPadding() == Padding.PKCS5PADDING);
                this.modeBlockSize = NativeCrypto.EVP_CIPHER_CTX_block_size(this.cipherCtx);
                this.calledUpdate = false;
                return;
            }
            throw new InvalidAlgorithmParameterException("Cannot find name for key length = " + (bArr.length * 8) + " and mode = " + this.mode);
        }

        @Override // org.conscrypt.OpenSSLCipher
        int updateInternal(byte[] bArr, int i, int i2, byte[] bArr2, int i3, int i4) throws ShortBufferException {
            int length = bArr2.length - i3;
            if (length >= i4) {
                this.calledUpdate = true;
                return (NativeCrypto.EVP_CipherUpdate(this.cipherCtx, bArr2, i3, bArr, i, i2) + i3) - i3;
            }
            throw new ShortBufferException("output buffer too small during update: " + length + " < " + i4);
        }

        @Override // org.conscrypt.OpenSSLCipher
        int doFinalInternal(byte[] bArr, int i, int i2) throws IllegalBlockSizeException, BadPaddingException, ShortBufferException {
            int i3;
            if (!isEncrypting() && !this.calledUpdate) {
                return 0;
            }
            int length = bArr.length - i;
            if (length >= i2) {
                i3 = NativeCrypto.EVP_CipherFinal_ex(this.cipherCtx, bArr, i);
            } else {
                byte[] bArr2 = new byte[i2];
                int EVP_CipherFinal_ex = NativeCrypto.EVP_CipherFinal_ex(this.cipherCtx, bArr2, 0);
                if (EVP_CipherFinal_ex <= length) {
                    if (EVP_CipherFinal_ex > 0) {
                        System.arraycopy(bArr2, 0, bArr, i, EVP_CipherFinal_ex);
                    }
                    i3 = EVP_CipherFinal_ex;
                } else {
                    throw new ShortBufferException("buffer is too short: " + EVP_CipherFinal_ex + " > " + length);
                }
            }
            reset();
            return (i3 + i) - i;
        }

        @Override // org.conscrypt.OpenSSLCipher
        int getOutputSizeForFinal(int i) {
            if (this.modeBlockSize == 1) {
                return i;
            }
            int i2 = NativeCrypto.get_EVP_CIPHER_CTX_buf_len(this.cipherCtx);
            if (getPadding() == Padding.NOPADDING) {
                return i2 + i;
            }
            int i3 = i + i2;
            int i4 = 0;
            int i5 = i3 + (NativeCrypto.get_EVP_CIPHER_CTX_final_used(this.cipherCtx) ? this.modeBlockSize : 0);
            if (i5 % this.modeBlockSize != 0 || isEncrypting()) {
                i4 = this.modeBlockSize;
            }
            int i6 = i5 + i4;
            return i6 - (i6 % this.modeBlockSize);
        }

        @Override // org.conscrypt.OpenSSLCipher
        int getOutputSizeForUpdate(int i) {
            return getOutputSizeForFinal(i);
        }

        private void reset() {
            NativeCrypto.EVP_CipherInit_ex(this.cipherCtx, 0, this.encodedKey, this.iv, isEncrypting());
            this.calledUpdate = false;
        }

        /* access modifiers changed from: package-private */
        /* loaded from: classes3.dex */
        public static abstract class AES_BASE extends EVP_CIPHER {
            private static final int AES_BLOCK_SIZE;

            @Override // org.conscrypt.OpenSSLCipher
            String getBaseCipherName() {
                return "AES";
            }

            @Override // org.conscrypt.OpenSSLCipher
            int getCipherBlockSize() {
                return 16;
            }

            AES_BASE(Mode mode, Padding padding) {
                super(mode, padding);
            }

            @Override // org.conscrypt.OpenSSLCipher
            void checkSupportedMode(Mode mode) throws NoSuchAlgorithmException {
                int i = AnonymousClass1.$SwitchMap$org$conscrypt$OpenSSLCipher$Mode[mode.ordinal()];
                if (i != 1 && i != 2 && i != 3) {
                    throw new NoSuchAlgorithmException("Unsupported mode " + mode.toString());
                }
            }

            @Override // org.conscrypt.OpenSSLCipher
            void checkSupportedPadding(Padding padding) throws NoSuchPaddingException {
                int i = AnonymousClass1.$SwitchMap$org$conscrypt$OpenSSLCipher$Padding[padding.ordinal()];
                if (i != 1 && i != 2) {
                    throw new NoSuchPaddingException("Unsupported padding " + padding.toString());
                }
            }

            @Override // org.conscrypt.OpenSSLCipher.EVP_CIPHER
            String getCipherName(int i, Mode mode) {
                return "aes-" + (i * 8) + "-" + mode.toString().toLowerCase(Locale.US);
            }
        }

        /* loaded from: classes3.dex */
        public static class AES extends AES_BASE {
            AES(Mode mode, Padding padding) {
                super(mode, padding);
            }

            /* loaded from: classes3.dex */
            public static class CBC extends AES {
                public CBC(Padding padding) {
                    super(Mode.CBC, padding);
                }

                /* loaded from: classes3.dex */
                public static class NoPadding extends CBC {
                    public NoPadding() {
                        super(Padding.NOPADDING);
                    }
                }

                /* loaded from: classes3.dex */
                public static class PKCS5Padding extends CBC {
                    public PKCS5Padding() {
                        super(Padding.PKCS5PADDING);
                    }
                }
            }

            /* loaded from: classes3.dex */
            public static class CTR extends AES {
                public CTR() {
                    super(Mode.CTR, Padding.NOPADDING);
                }
            }

            /* loaded from: classes3.dex */
            public static class ECB extends AES {
                public ECB(Padding padding) {
                    super(Mode.ECB, padding);
                }

                /* loaded from: classes3.dex */
                public static class NoPadding extends ECB {
                    public NoPadding() {
                        super(Padding.NOPADDING);
                    }
                }

                /* loaded from: classes3.dex */
                public static class PKCS5Padding extends ECB {
                    public PKCS5Padding() {
                        super(Padding.PKCS5PADDING);
                    }
                }
            }

            @Override // org.conscrypt.OpenSSLCipher
            void checkSupportedKeySize(int i) throws InvalidKeyException {
                if (i != 16 && i != 24 && i != 32) {
                    throw new InvalidKeyException("Unsupported key size: " + i + " bytes");
                }
            }
        }

        /* loaded from: classes3.dex */
        public static class AES_128 extends AES_BASE {
            AES_128(Mode mode, Padding padding) {
                super(mode, padding);
            }

            /* loaded from: classes3.dex */
            public static class CBC extends AES_128 {
                public CBC(Padding padding) {
                    super(Mode.CBC, padding);
                }

                /* loaded from: classes3.dex */
                public static class NoPadding extends CBC {
                    public NoPadding() {
                        super(Padding.NOPADDING);
                    }
                }

                /* loaded from: classes3.dex */
                public static class PKCS5Padding extends CBC {
                    public PKCS5Padding() {
                        super(Padding.PKCS5PADDING);
                    }
                }
            }

            /* loaded from: classes3.dex */
            public static class CTR extends AES_128 {
                public CTR() {
                    super(Mode.CTR, Padding.NOPADDING);
                }
            }

            /* loaded from: classes3.dex */
            public static class ECB extends AES_128 {
                public ECB(Padding padding) {
                    super(Mode.ECB, padding);
                }

                /* loaded from: classes3.dex */
                public static class NoPadding extends ECB {
                    public NoPadding() {
                        super(Padding.NOPADDING);
                    }
                }

                /* loaded from: classes3.dex */
                public static class PKCS5Padding extends ECB {
                    public PKCS5Padding() {
                        super(Padding.PKCS5PADDING);
                    }
                }
            }

            @Override // org.conscrypt.OpenSSLCipher
            void checkSupportedKeySize(int i) throws InvalidKeyException {
                if (i != 16) {
                    throw new InvalidKeyException("Unsupported key size: " + i + " bytes");
                }
            }
        }

        /* loaded from: classes3.dex */
        public static class AES_256 extends AES_BASE {
            AES_256(Mode mode, Padding padding) {
                super(mode, padding);
            }

            /* loaded from: classes3.dex */
            public static class CBC extends AES_256 {
                public CBC(Padding padding) {
                    super(Mode.CBC, padding);
                }

                /* loaded from: classes3.dex */
                public static class NoPadding extends CBC {
                    public NoPadding() {
                        super(Padding.NOPADDING);
                    }
                }

                /* loaded from: classes3.dex */
                public static class PKCS5Padding extends CBC {
                    public PKCS5Padding() {
                        super(Padding.PKCS5PADDING);
                    }
                }
            }

            /* loaded from: classes3.dex */
            public static class CTR extends AES_256 {
                public CTR() {
                    super(Mode.CTR, Padding.NOPADDING);
                }
            }

            /* loaded from: classes3.dex */
            public static class ECB extends AES_256 {
                public ECB(Padding padding) {
                    super(Mode.ECB, padding);
                }

                /* loaded from: classes3.dex */
                public static class NoPadding extends ECB {
                    public NoPadding() {
                        super(Padding.NOPADDING);
                    }
                }

                /* loaded from: classes3.dex */
                public static class PKCS5Padding extends ECB {
                    public PKCS5Padding() {
                        super(Padding.PKCS5PADDING);
                    }
                }
            }

            @Override // org.conscrypt.OpenSSLCipher
            void checkSupportedKeySize(int i) throws InvalidKeyException {
                if (i != 32) {
                    throw new InvalidKeyException("Unsupported key size: " + i + " bytes");
                }
            }
        }

        /* loaded from: classes3.dex */
        public static class DESEDE extends EVP_CIPHER {
            private static final int DES_BLOCK_SIZE;

            @Override // org.conscrypt.OpenSSLCipher
            String getBaseCipherName() {
                return "DESede";
            }

            @Override // org.conscrypt.OpenSSLCipher
            int getCipherBlockSize() {
                return 8;
            }

            public DESEDE(Mode mode, Padding padding) {
                super(mode, padding);
            }

            /* loaded from: classes3.dex */
            public static class CBC extends DESEDE {
                public CBC(Padding padding) {
                    super(Mode.CBC, padding);
                }

                /* loaded from: classes3.dex */
                public static class NoPadding extends CBC {
                    public NoPadding() {
                        super(Padding.NOPADDING);
                    }
                }

                /* loaded from: classes3.dex */
                public static class PKCS5Padding extends CBC {
                    public PKCS5Padding() {
                        super(Padding.PKCS5PADDING);
                    }
                }
            }

            @Override // org.conscrypt.OpenSSLCipher.EVP_CIPHER
            String getCipherName(int i, Mode mode) {
                String str = i == 16 ? "des-ede" : "des-ede3";
                return str + "-" + mode.toString().toLowerCase(Locale.US);
            }

            @Override // org.conscrypt.OpenSSLCipher
            void checkSupportedKeySize(int i) throws InvalidKeyException {
                if (i != 16 && i != 24) {
                    throw new InvalidKeyException("key size must be 128 or 192 bits");
                }
            }

            @Override // org.conscrypt.OpenSSLCipher
            void checkSupportedMode(Mode mode) throws NoSuchAlgorithmException {
                if (mode != Mode.CBC) {
                    throw new NoSuchAlgorithmException("Unsupported mode " + mode.toString());
                }
            }

            @Override // org.conscrypt.OpenSSLCipher
            void checkSupportedPadding(Padding padding) throws NoSuchPaddingException {
                int i = AnonymousClass1.$SwitchMap$org$conscrypt$OpenSSLCipher$Padding[padding.ordinal()];
                if (i != 1 && i != 2) {
                    throw new NoSuchPaddingException("Unsupported padding " + padding.toString());
                }
            }
        }

        /* loaded from: classes3.dex */
        public static class ARC4 extends EVP_CIPHER {
            @Override // org.conscrypt.OpenSSLCipher
            void checkSupportedKeySize(int i) throws InvalidKeyException {
            }

            @Override // org.conscrypt.OpenSSLCipher
            String getBaseCipherName() {
                return "ARCFOUR";
            }

            @Override // org.conscrypt.OpenSSLCipher
            int getCipherBlockSize() {
                return 0;
            }

            @Override // org.conscrypt.OpenSSLCipher.EVP_CIPHER
            String getCipherName(int i, Mode mode) {
                return "rc4";
            }

            @Override // org.conscrypt.OpenSSLCipher
            boolean supportsVariableSizeKey() {
                return true;
            }

            public ARC4() {
                super(Mode.ECB, Padding.NOPADDING);
            }

            @Override // org.conscrypt.OpenSSLCipher
            void checkSupportedMode(Mode mode) throws NoSuchAlgorithmException {
                if (mode != Mode.NONE && mode != Mode.ECB) {
                    throw new NoSuchAlgorithmException("Unsupported mode " + mode.toString());
                }
            }

            @Override // org.conscrypt.OpenSSLCipher
            void checkSupportedPadding(Padding padding) throws NoSuchPaddingException {
                if (padding != Padding.NOPADDING) {
                    throw new NoSuchPaddingException("Unsupported padding " + padding.toString());
                }
            }
        }
    }

    /* renamed from: org.conscrypt.OpenSSLCipher$1 */
    /* loaded from: classes3.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$conscrypt$OpenSSLCipher$Mode;
        static final /* synthetic */ int[] $SwitchMap$org$conscrypt$OpenSSLCipher$Padding;

        static {
            int[] iArr = new int[Padding.values().length];
            $SwitchMap$org$conscrypt$OpenSSLCipher$Padding = iArr;
            try {
                iArr[Padding.NOPADDING.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$conscrypt$OpenSSLCipher$Padding[Padding.PKCS5PADDING.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            int[] iArr2 = new int[Mode.values().length];
            $SwitchMap$org$conscrypt$OpenSSLCipher$Mode = iArr2;
            try {
                iArr2[Mode.CBC.ordinal()] = 1;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$conscrypt$OpenSSLCipher$Mode[Mode.CTR.ordinal()] = 2;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$conscrypt$OpenSSLCipher$Mode[Mode.ECB.ordinal()] = 3;
            } catch (NoSuchFieldError unused5) {
            }
        }
    }

    /* loaded from: classes3.dex */
    public static abstract class EVP_AEAD extends OpenSSLCipher {
        private static final int DEFAULT_TAG_SIZE_BITS;
        private static int lastGlobalMessageSize;
        private byte[] aad;
        byte[] buf;
        int bufCount;
        long evpAead;
        private boolean mustInitialize;
        private byte[] previousIv;
        private byte[] previousKey;
        int tagLengthInBytes;

        abstract long getEVP_AEAD(int i) throws InvalidKeyException;

        @Override // org.conscrypt.OpenSSLCipher
        int getOutputSizeForUpdate(int i) {
            return 0;
        }

        public EVP_AEAD(Mode mode) {
            super(mode, Padding.NOPADDING);
        }

        private void checkInitialization() {
            if (this.mustInitialize) {
                throw new IllegalStateException("Cannot re-use same key and IV for multiple encryptions");
            }
        }

        private boolean arraysAreEqual(byte[] bArr, byte[] bArr2) {
            if (bArr.length != bArr2.length) {
                return false;
            }
            int i = 0;
            for (int i2 = 0; i2 < bArr.length; i2++) {
                i |= bArr[i2] ^ bArr2[i2];
            }
            if (i == 0) {
                return true;
            }
            return false;
        }

        private void expand(int i) {
            int i2 = this.bufCount;
            int i3 = i2 + i;
            byte[] bArr = this.buf;
            if (i3 > bArr.length) {
                byte[] bArr2 = new byte[(i + i2) * 2];
                System.arraycopy(bArr, 0, bArr2, 0, i2);
                this.buf = bArr2;
            }
        }

        private void reset() {
            this.aad = null;
            int i = lastGlobalMessageSize;
            byte[] bArr = this.buf;
            if (bArr == null) {
                this.buf = new byte[i];
            } else {
                int i2 = this.bufCount;
                if (i2 > 0 && i2 != i) {
                    lastGlobalMessageSize = i2;
                    if (bArr.length != i2) {
                        this.buf = new byte[i2];
                    }
                }
            }
            this.bufCount = 0;
        }

        @Override // org.conscrypt.OpenSSLCipher
        void engineInitInternal(byte[] bArr, AlgorithmParameterSpec algorithmParameterSpec, SecureRandom secureRandom) throws InvalidKeyException, InvalidAlgorithmParameterException {
            byte[] bArr2 = null;
            int i = 128;
            if (algorithmParameterSpec != null) {
                GCMParameters fromGCMParameterSpec = Platform.fromGCMParameterSpec(algorithmParameterSpec);
                if (fromGCMParameterSpec != null) {
                    bArr2 = fromGCMParameterSpec.getIV();
                    i = fromGCMParameterSpec.getTLen();
                } else if (algorithmParameterSpec instanceof IvParameterSpec) {
                    bArr2 = ((IvParameterSpec) algorithmParameterSpec).getIV();
                }
            }
            if (i % 8 == 0) {
                this.tagLengthInBytes = i / 8;
                boolean isEncrypting = isEncrypting();
                long evp_aead = getEVP_AEAD(bArr.length);
                this.evpAead = evp_aead;
                int EVP_AEAD_nonce_length = NativeCrypto.EVP_AEAD_nonce_length(evp_aead);
                if (bArr2 != null || EVP_AEAD_nonce_length == 0) {
                    if (EVP_AEAD_nonce_length == 0 && bArr2 != null) {
                        throw new InvalidAlgorithmParameterException("IV not used in " + this.mode + " mode");
                    } else if (!(bArr2 == null || bArr2.length == EVP_AEAD_nonce_length)) {
                        throw new InvalidAlgorithmParameterException("Expected IV length of " + EVP_AEAD_nonce_length + " but was " + bArr2.length);
                    }
                } else if (isEncrypting) {
                    bArr2 = new byte[EVP_AEAD_nonce_length];
                    if (secureRandom != null) {
                        secureRandom.nextBytes(bArr2);
                    } else {
                        NativeCrypto.RAND_bytes(bArr2);
                    }
                } else {
                    throw new InvalidAlgorithmParameterException("IV must be specified in " + this.mode + " mode");
                }
                if (isEncrypting() && bArr2 != null) {
                    byte[] bArr3 = this.previousKey;
                    if (bArr3 == null || this.previousIv == null || !arraysAreEqual(bArr3, bArr) || !arraysAreEqual(this.previousIv, bArr2)) {
                        this.previousKey = bArr;
                        this.previousIv = bArr2;
                    } else {
                        this.mustInitialize = true;
                        throw new InvalidAlgorithmParameterException("When using AEAD key and IV must not be re-used");
                    }
                }
                this.mustInitialize = false;
                this.iv = bArr2;
                reset();
                return;
            }
            throw new InvalidAlgorithmParameterException("Tag length must be a multiple of 8; was " + this.tagLengthInBytes);
        }

        @Override // org.conscrypt.OpenSSLCipher, javax.crypto.CipherSpi
        protected int engineDoFinal(byte[] bArr, int i, int i2, byte[] bArr2, int i3) throws ShortBufferException, IllegalBlockSizeException, BadPaddingException {
            if (bArr2 == null || getOutputSizeForFinal(i2) <= bArr2.length - i3) {
                return OpenSSLCipher.super.engineDoFinal(bArr, i, i2, bArr2, i3);
            }
            throw new ShortBufferException("Insufficient output space");
        }

        @Override // org.conscrypt.OpenSSLCipher
        int updateInternal(byte[] bArr, int i, int i2, byte[] bArr2, int i3, int i4) throws ShortBufferException {
            checkInitialization();
            if (this.buf != null) {
                ArrayUtils.checkOffsetAndCount(bArr.length, i, i2);
                if (i2 <= 0) {
                    return 0;
                }
                expand(i2);
                System.arraycopy(bArr, i, this.buf, this.bufCount, i2);
                this.bufCount += i2;
                return 0;
            }
            throw new IllegalStateException("Cipher not initialized");
        }

        /* JADX WARNING: Removed duplicated region for block: B:13:0x0039 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x003a  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void throwAEADBadTagExceptionIfAvailable(java.lang.String r6, java.lang.Throwable r7) throws javax.crypto.BadPaddingException {
            /*
                r5 = this;
                java.lang.String r0 = "javax.crypto.AEADBadTagException"
                java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch: Exception -> 0x003b
                r1 = 1
                java.lang.Class[] r2 = new java.lang.Class[r1]     // Catch: Exception -> 0x003b
                java.lang.Class<java.lang.String> r3 = java.lang.String.class
                r4 = 0
                r2[r4] = r3     // Catch: Exception -> 0x003b
                java.lang.reflect.Constructor r0 = r0.getConstructor(r2)     // Catch: Exception -> 0x003b
                r2 = 0
                java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch: IllegalAccessException | InstantiationException -> 0x0035, InvocationTargetException -> 0x0024
                r1[r4] = r6     // Catch: IllegalAccessException | InstantiationException -> 0x0035, InvocationTargetException -> 0x0024
                java.lang.Object r6 = r0.newInstance(r1)     // Catch: IllegalAccessException | InstantiationException -> 0x0035, InvocationTargetException -> 0x0024
                javax.crypto.BadPaddingException r6 = (javax.crypto.BadPaddingException) r6     // Catch: IllegalAccessException | InstantiationException -> 0x0035, InvocationTargetException -> 0x0024
                r6.initCause(r7)     // Catch: IllegalAccessException | InstantiationException -> 0x0021, InvocationTargetException -> 0x0024
                goto L_0x0037
            L_0x0021:
                r2 = r6
                goto L_0x0036
            L_0x0024:
                r6 = move-exception
                javax.crypto.BadPaddingException r7 = new javax.crypto.BadPaddingException
                r7.<init>()
                java.lang.Throwable r6 = r6.getTargetException()
                java.lang.Throwable r6 = r7.initCause(r6)
                javax.crypto.BadPaddingException r6 = (javax.crypto.BadPaddingException) r6
                throw r6
            L_0x0035:
            L_0x0036:
                r6 = r2
            L_0x0037:
                if (r6 != 0) goto L_0x003a
                return
            L_0x003a:
                throw r6
            L_0x003b:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.conscrypt.OpenSSLCipher.EVP_AEAD.throwAEADBadTagExceptionIfAvailable(java.lang.String, java.lang.Throwable):void");
        }

        @Override // org.conscrypt.OpenSSLCipher
        int doFinalInternal(byte[] bArr, int i, int i2) throws ShortBufferException, IllegalBlockSizeException, BadPaddingException {
            int i3;
            checkInitialization();
            try {
                if (isEncrypting()) {
                    i3 = NativeCrypto.EVP_AEAD_CTX_seal(this.evpAead, this.encodedKey, this.tagLengthInBytes, bArr, i, this.iv, this.buf, 0, this.bufCount, this.aad);
                } else {
                    i3 = NativeCrypto.EVP_AEAD_CTX_open(this.evpAead, this.encodedKey, this.tagLengthInBytes, bArr, i, this.iv, this.buf, 0, this.bufCount, this.aad);
                }
                if (isEncrypting()) {
                    this.mustInitialize = true;
                }
                reset();
                return i3;
            } catch (BadPaddingException e) {
                throwAEADBadTagExceptionIfAvailable(e.getMessage(), e.getCause());
                throw e;
            }
        }

        @Override // org.conscrypt.OpenSSLCipher
        void checkSupportedPadding(Padding padding) throws NoSuchPaddingException {
            if (padding != Padding.NOPADDING) {
                throw new NoSuchPaddingException("Must be NoPadding for AEAD ciphers");
            }
        }

        @Override // org.conscrypt.OpenSSLCipher
        int getOutputSizeForFinal(int i) {
            return this.bufCount + i + (isEncrypting() ? NativeCrypto.EVP_AEAD_max_overhead(this.evpAead) : 0);
        }

        @Override // javax.crypto.CipherSpi
        protected void engineUpdateAAD(byte[] bArr, int i, int i2) {
            checkInitialization();
            byte[] bArr2 = this.aad;
            if (bArr2 == null) {
                this.aad = Arrays.copyOfRange(bArr, i, i2 + i);
                return;
            }
            byte[] bArr3 = new byte[bArr2.length + i2];
            System.arraycopy(bArr2, 0, bArr3, 0, bArr2.length);
            System.arraycopy(bArr, i, bArr3, this.aad.length, i2);
            this.aad = bArr3;
        }

        /* loaded from: classes3.dex */
        public static abstract class AES extends EVP_AEAD {
            private static final int AES_BLOCK_SIZE;

            @Override // org.conscrypt.OpenSSLCipher
            String getBaseCipherName() {
                return "AES";
            }

            @Override // org.conscrypt.OpenSSLCipher
            int getCipherBlockSize() {
                return 16;
            }

            AES(Mode mode) {
                super(mode);
            }

            @Override // org.conscrypt.OpenSSLCipher
            void checkSupportedKeySize(int i) throws InvalidKeyException {
                if (i != 16 && i != 32) {
                    throw new InvalidKeyException("Unsupported key size: " + i + " bytes (must be 16 or 32)");
                }
            }

            /* loaded from: classes3.dex */
            public static class GCM extends AES {
                public GCM() {
                    super(Mode.GCM);
                }

                @Override // org.conscrypt.OpenSSLCipher
                void checkSupportedMode(Mode mode) throws NoSuchAlgorithmException {
                    if (mode != Mode.GCM) {
                        throw new NoSuchAlgorithmException("Mode must be GCM");
                    }
                }

                @Override // org.conscrypt.OpenSSLCipher, javax.crypto.CipherSpi
                protected AlgorithmParameters engineGetParameters() {
                    byte[] bArr = this.iv;
                    if (bArr == null) {
                        return null;
                    }
                    AlgorithmParameterSpec gCMParameterSpec = Platform.toGCMParameterSpec(this.tagLengthInBytes * 8, bArr);
                    if (gCMParameterSpec == null) {
                        return OpenSSLCipher.super.engineGetParameters();
                    }
                    try {
                        AlgorithmParameters instance = AlgorithmParameters.getInstance("GCM");
                        instance.init(gCMParameterSpec);
                        return instance;
                    } catch (NoSuchAlgorithmException e) {
                        throw ((Error) new AssertionError("GCM not supported").initCause(e));
                    } catch (InvalidParameterSpecException unused) {
                        return null;
                    }
                }

                @Override // org.conscrypt.OpenSSLCipher
                protected AlgorithmParameterSpec getParameterSpec(AlgorithmParameters algorithmParameters) throws InvalidAlgorithmParameterException {
                    if (algorithmParameters == null) {
                        return null;
                    }
                    AlgorithmParameterSpec fromGCMParameters = Platform.fromGCMParameters(algorithmParameters);
                    if (fromGCMParameters != null) {
                        return fromGCMParameters;
                    }
                    return OpenSSLCipher.super.getParameterSpec(algorithmParameters);
                }

                @Override // org.conscrypt.OpenSSLCipher.EVP_AEAD
                long getEVP_AEAD(int i) throws InvalidKeyException {
                    if (i == 16) {
                        return NativeCrypto.EVP_aead_aes_128_gcm();
                    }
                    if (i == 32) {
                        return NativeCrypto.EVP_aead_aes_256_gcm();
                    }
                    throw new RuntimeException("Unexpected key length: " + i);
                }

                @Override // org.conscrypt.OpenSSLCipher.EVP_AEAD, org.conscrypt.OpenSSLCipher
                int getOutputSizeForFinal(int i) {
                    if (isEncrypting()) {
                        return this.bufCount + i + this.tagLengthInBytes;
                    }
                    return Math.max(0, (this.bufCount + i) - this.tagLengthInBytes);
                }

                /* loaded from: classes3.dex */
                public static class AES_128 extends GCM {
                    @Override // org.conscrypt.OpenSSLCipher.EVP_AEAD.AES, org.conscrypt.OpenSSLCipher
                    void checkSupportedKeySize(int i) throws InvalidKeyException {
                        if (i != 16) {
                            throw new InvalidKeyException("Unsupported key size: " + i + " bytes (must be 16)");
                        }
                    }
                }

                /* loaded from: classes3.dex */
                public static class AES_256 extends GCM {
                    @Override // org.conscrypt.OpenSSLCipher.EVP_AEAD.AES, org.conscrypt.OpenSSLCipher
                    void checkSupportedKeySize(int i) throws InvalidKeyException {
                        if (i != 32) {
                            throw new InvalidKeyException("Unsupported key size: " + i + " bytes (must be 32)");
                        }
                    }
                }
            }
        }

        /* loaded from: classes3.dex */
        public static class ChaCha20 extends EVP_AEAD {
            @Override // org.conscrypt.OpenSSLCipher
            String getBaseCipherName() {
                return "ChaCha20";
            }

            @Override // org.conscrypt.OpenSSLCipher
            int getCipherBlockSize() {
                return 0;
            }

            public ChaCha20() {
                super(Mode.POLY1305);
            }

            @Override // org.conscrypt.OpenSSLCipher
            void checkSupportedKeySize(int i) throws InvalidKeyException {
                if (i != 32) {
                    throw new InvalidKeyException("Unsupported key size: " + i + " bytes (must be 32)");
                }
            }

            @Override // org.conscrypt.OpenSSLCipher
            void checkSupportedMode(Mode mode) throws NoSuchAlgorithmException {
                if (mode != Mode.POLY1305) {
                    throw new NoSuchAlgorithmException("Mode must be Poly1305");
                }
            }

            @Override // org.conscrypt.OpenSSLCipher.EVP_AEAD
            long getEVP_AEAD(int i) throws InvalidKeyException {
                if (i == 32) {
                    return NativeCrypto.EVP_aead_chacha20_poly1305();
                }
                throw new RuntimeException("Unexpected key length: " + i);
            }

            @Override // org.conscrypt.OpenSSLCipher.EVP_AEAD, org.conscrypt.OpenSSLCipher
            int getOutputSizeForFinal(int i) {
                if (isEncrypting()) {
                    return this.bufCount + i + 16;
                }
                return Math.max(0, (this.bufCount + i) - 16);
            }
        }
    }
}
