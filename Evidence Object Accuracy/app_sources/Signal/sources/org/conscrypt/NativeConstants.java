package org.conscrypt;

/* loaded from: classes3.dex */
final class NativeConstants {
    static final int EVP_PKEY_EC;
    static final int EVP_PKEY_RSA;
    static final int EXFLAG_CA;
    static final int EXFLAG_CRITICAL;
    static final int RSA_NO_PADDING;
    static final int RSA_PKCS1_OAEP_PADDING;
    static final int RSA_PKCS1_PADDING;
    static final int RSA_PKCS1_PSS_PADDING;
    static final int SSL3_RT_ALERT;
    static final int SSL3_RT_APPLICATION_DATA;
    static final int SSL3_RT_CHANGE_CIPHER_SPEC;
    static final int SSL3_RT_HANDSHAKE;
    static final int SSL3_RT_HEADER_LENGTH;
    static final int SSL3_RT_MAX_PACKET_SIZE;
    static final int SSL3_RT_MAX_PLAIN_LENGTH;
    static final int SSL_CB_HANDSHAKE_DONE;
    static final int SSL_CB_HANDSHAKE_START;
    static final int SSL_ERROR_NONE;
    static final int SSL_ERROR_WANT_READ;
    static final int SSL_ERROR_WANT_WRITE;
    static final int SSL_ERROR_ZERO_RETURN;
    static final int SSL_MODE_CBC_RECORD_SPLITTING;
    static final int SSL_MODE_ENABLE_FALSE_START;
    static final int SSL_MODE_SEND_FALLBACK_SCSV;
    static final int SSL_OP_CIPHER_SERVER_PREFERENCE;
    static final int SSL_OP_NO_TICKET;
    static final int SSL_RECEIVED_SHUTDOWN;
    static final int SSL_SENT_SHUTDOWN;
    static final int SSL_SIGN_ECDSA_SECP256R1_SHA256;
    static final int SSL_SIGN_ECDSA_SECP384R1_SHA384;
    static final int SSL_SIGN_ECDSA_SECP521R1_SHA512;
    static final int SSL_SIGN_ECDSA_SHA1;
    static final int SSL_SIGN_ED25519;
    static final int SSL_SIGN_RSA_PKCS1_SHA1;
    static final int SSL_SIGN_RSA_PKCS1_SHA256;
    static final int SSL_SIGN_RSA_PKCS1_SHA384;
    static final int SSL_SIGN_RSA_PKCS1_SHA512;
    static final int SSL_SIGN_RSA_PSS_RSAE_SHA256;
    static final int SSL_SIGN_RSA_PSS_RSAE_SHA384;
    static final int SSL_SIGN_RSA_PSS_RSAE_SHA512;
    static final int SSL_VERIFY_FAIL_IF_NO_PEER_CERT;
    static final int SSL_VERIFY_NONE;
    static final int SSL_VERIFY_PEER;
    static final int TLS1_1_VERSION;
    static final int TLS1_2_VERSION;
    static final int TLS1_3_VERSION;
    static final int TLS1_VERSION;
    static final int TLS_CT_ECDSA_SIGN;
    static final int TLS_CT_RSA_SIGN;

    NativeConstants() {
    }
}
