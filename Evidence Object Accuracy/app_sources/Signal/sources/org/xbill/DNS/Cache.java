package org.xbill.DNS;

import java.io.PrintStream;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* loaded from: classes5.dex */
public class Cache {
    private CacheMap data;
    private int dclass;
    private int maxcache;
    private int maxncache;

    /* loaded from: classes5.dex */
    public interface Element {
        int compareCredibility(int i);

        boolean expired();

        int getType();
    }

    public static int limitExpire(long j, long j2) {
        if (j2 >= 0 && j2 < j) {
            j = j2;
        }
        long currentTimeMillis = (System.currentTimeMillis() / 1000) + j;
        if (currentTimeMillis < 0 || currentTimeMillis > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        return (int) currentTimeMillis;
    }

    /* loaded from: classes5.dex */
    public static class CacheRRset extends RRset implements Element {
        int credibility;
        int expire;

        public CacheRRset(RRset rRset, int i, long j) {
            super(rRset);
            this.credibility = i;
            this.expire = Cache.limitExpire(rRset.getTTL(), j);
        }

        @Override // org.xbill.DNS.Cache.Element
        public final boolean expired() {
            return ((int) (System.currentTimeMillis() / 1000)) >= this.expire;
        }

        @Override // org.xbill.DNS.Cache.Element
        public final int compareCredibility(int i) {
            return this.credibility - i;
        }

        @Override // org.xbill.DNS.RRset, java.lang.Object
        public String toString() {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(super.toString());
            stringBuffer.append(" cl = ");
            stringBuffer.append(this.credibility);
            return stringBuffer.toString();
        }
    }

    /* loaded from: classes5.dex */
    public static class NegativeElement implements Element {
        int credibility;
        int expire;
        Name name;
        int type;

        public NegativeElement(Name name, int i, SOARecord sOARecord, int i2, long j) {
            this.name = name;
            this.type = i;
            long minimum = sOARecord != null ? sOARecord.getMinimum() : 0;
            this.credibility = i2;
            this.expire = Cache.limitExpire(minimum, j);
        }

        @Override // org.xbill.DNS.Cache.Element
        public int getType() {
            return this.type;
        }

        @Override // org.xbill.DNS.Cache.Element
        public final boolean expired() {
            return ((int) (System.currentTimeMillis() / 1000)) >= this.expire;
        }

        @Override // org.xbill.DNS.Cache.Element
        public final int compareCredibility(int i) {
            return this.credibility - i;
        }

        public String toString() {
            StringBuffer stringBuffer = new StringBuffer();
            if (this.type == 0) {
                StringBuffer stringBuffer2 = new StringBuffer();
                stringBuffer2.append("NXDOMAIN ");
                stringBuffer2.append(this.name);
                stringBuffer.append(stringBuffer2.toString());
            } else {
                StringBuffer stringBuffer3 = new StringBuffer();
                stringBuffer3.append("NXRRSET ");
                stringBuffer3.append(this.name);
                stringBuffer3.append(" ");
                stringBuffer3.append(Type.string(this.type));
                stringBuffer.append(stringBuffer3.toString());
            }
            stringBuffer.append(" cl = ");
            stringBuffer.append(this.credibility);
            return stringBuffer.toString();
        }
    }

    /* loaded from: classes5.dex */
    public static class CacheMap extends LinkedHashMap {
        private int maxsize;

        CacheMap(int i) {
            super(16, 0.75f, true);
            this.maxsize = i;
        }

        @Override // java.util.LinkedHashMap
        protected boolean removeEldestEntry(Map.Entry entry) {
            return this.maxsize >= 0 && size() > this.maxsize;
        }
    }

    public Cache(int i) {
        this.maxncache = -1;
        this.maxcache = -1;
        this.dclass = i;
        this.data = new CacheMap(50000);
    }

    public Cache() {
        this(1);
    }

    private synchronized Object exactName(Name name) {
        return this.data.get(name);
    }

    private synchronized Element[] allElements(Object obj) {
        if (obj instanceof List) {
            List list = (List) obj;
            return (Element[]) list.toArray(new Element[list.size()]);
        }
        return new Element[]{(Element) obj};
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002a, code lost:
        if (r2.getType() == r7) goto L_0x002e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized org.xbill.DNS.Cache.Element oneElement(org.xbill.DNS.Name r5, java.lang.Object r6, int r7, int r8) {
        /*
            r4 = this;
            monitor-enter(r4)
            r0 = 255(0xff, float:3.57E-43)
            if (r7 == r0) goto L_0x0049
            boolean r0 = r6 instanceof java.util.List     // Catch: all -> 0x0047
            r1 = 0
            if (r0 == 0) goto L_0x0023
            java.util.List r6 = (java.util.List) r6     // Catch: all -> 0x0047
            r0 = 0
        L_0x000d:
            int r2 = r6.size()     // Catch: all -> 0x0047
            if (r0 >= r2) goto L_0x002d
            java.lang.Object r2 = r6.get(r0)     // Catch: all -> 0x0047
            org.xbill.DNS.Cache$Element r2 = (org.xbill.DNS.Cache.Element) r2     // Catch: all -> 0x0047
            int r3 = r2.getType()     // Catch: all -> 0x0047
            if (r3 != r7) goto L_0x0020
            goto L_0x002e
        L_0x0020:
            int r0 = r0 + 1
            goto L_0x000d
        L_0x0023:
            r2 = r6
            org.xbill.DNS.Cache$Element r2 = (org.xbill.DNS.Cache.Element) r2     // Catch: all -> 0x0047
            int r6 = r2.getType()     // Catch: all -> 0x0047
            if (r6 != r7) goto L_0x002d
            goto L_0x002e
        L_0x002d:
            r2 = r1
        L_0x002e:
            if (r2 != 0) goto L_0x0032
            monitor-exit(r4)
            return r1
        L_0x0032:
            boolean r6 = r2.expired()     // Catch: all -> 0x0047
            if (r6 == 0) goto L_0x003d
            r4.removeElement(r5, r7)     // Catch: all -> 0x0047
            monitor-exit(r4)
            return r1
        L_0x003d:
            int r5 = r2.compareCredibility(r8)     // Catch: all -> 0x0047
            if (r5 >= 0) goto L_0x0045
            monitor-exit(r4)
            return r1
        L_0x0045:
            monitor-exit(r4)
            return r2
        L_0x0047:
            r5 = move-exception
            goto L_0x0051
        L_0x0049:
            java.lang.IllegalArgumentException r5 = new java.lang.IllegalArgumentException     // Catch: all -> 0x0047
            java.lang.String r6 = "oneElement(ANY)"
            r5.<init>(r6)     // Catch: all -> 0x0047
            throw r5     // Catch: all -> 0x0047
        L_0x0051:
            monitor-exit(r4)
            goto L_0x0054
        L_0x0053:
            throw r5
        L_0x0054:
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: org.xbill.DNS.Cache.oneElement(org.xbill.DNS.Name, java.lang.Object, int, int):org.xbill.DNS.Cache$Element");
    }

    private synchronized Element findElement(Name name, int i, int i2) {
        Object exactName = exactName(name);
        if (exactName == null) {
            return null;
        }
        return oneElement(name, exactName, i, i2);
    }

    private synchronized void addElement(Name name, Element element) {
        Object obj = this.data.get(name);
        if (obj == null) {
            this.data.put(name, element);
            return;
        }
        int type = element.getType();
        if (obj instanceof List) {
            List list = (List) obj;
            for (int i = 0; i < list.size(); i++) {
                if (((Element) list.get(i)).getType() == type) {
                    list.set(i, element);
                    return;
                }
            }
            list.add(element);
        } else {
            Element element2 = (Element) obj;
            if (element2.getType() == type) {
                this.data.put(name, element);
            } else {
                LinkedList linkedList = new LinkedList();
                linkedList.add(element2);
                linkedList.add(element);
                this.data.put(name, linkedList);
            }
        }
    }

    private synchronized void removeElement(Name name, int i) {
        Object obj = this.data.get(name);
        if (obj != null) {
            if (obj instanceof List) {
                List list = (List) obj;
                for (int i2 = 0; i2 < list.size(); i2++) {
                    if (((Element) list.get(i2)).getType() == i) {
                        list.remove(i2);
                        if (list.size() == 0) {
                            this.data.remove(name);
                        }
                        return;
                    }
                }
            } else if (((Element) obj).getType() == i) {
                this.data.remove(name);
            }
        }
    }

    public synchronized void clearCache() {
        this.data.clear();
    }

    public synchronized void addRRset(RRset rRset, int i) {
        CacheRRset cacheRRset;
        long ttl = rRset.getTTL();
        Name name = rRset.getName();
        int type = rRset.getType();
        Element findElement = findElement(name, type, 0);
        if (ttl != 0) {
            if (findElement != null && findElement.compareCredibility(i) <= 0) {
                findElement = null;
            }
            if (findElement == null) {
                if (rRset instanceof CacheRRset) {
                    cacheRRset = (CacheRRset) rRset;
                } else {
                    cacheRRset = new CacheRRset(rRset, i, (long) this.maxcache);
                }
                addElement(name, cacheRRset);
            }
        } else if (findElement != null && findElement.compareCredibility(i) <= 0) {
            removeElement(name, type);
        }
    }

    public synchronized void addNegative(Name name, int i, SOARecord sOARecord, int i2) {
        long ttl = sOARecord != null ? sOARecord.getTTL() : 0;
        Element findElement = findElement(name, i, 0);
        if (ttl != 0) {
            if (findElement != null && findElement.compareCredibility(i2) <= 0) {
                findElement = null;
            }
            if (findElement == null) {
                addElement(name, new NegativeElement(name, i, sOARecord, i2, (long) this.maxncache));
            }
        } else if (findElement != null && findElement.compareCredibility(i2) <= 0) {
            removeElement(name, i);
        }
    }

    protected synchronized SetResponse lookup(Name name, int i, int i2) {
        Name name2;
        int labels = name.labels();
        int i3 = labels;
        while (i3 >= 1) {
            boolean z = i3 == 1;
            boolean z2 = i3 == labels;
            if (z) {
                name2 = Name.root;
            } else {
                name2 = z2 ? name : new Name(name, labels - i3);
            }
            Object obj = this.data.get(name2);
            if (obj != null) {
                if (z2 && i == 255) {
                    SetResponse setResponse = new SetResponse(6);
                    Element[] allElements = allElements(obj);
                    int i4 = 0;
                    for (Element element : allElements) {
                        if (element.expired()) {
                            removeElement(name2, element.getType());
                        } else if ((element instanceof CacheRRset) && element.compareCredibility(i2) >= 0) {
                            setResponse.addRRset((CacheRRset) element);
                            i4++;
                        }
                    }
                    if (i4 > 0) {
                        return setResponse;
                    }
                } else if (z2) {
                    Element oneElement = oneElement(name2, obj, i, i2);
                    if (oneElement != null && (oneElement instanceof CacheRRset)) {
                        SetResponse setResponse2 = new SetResponse(6);
                        setResponse2.addRRset((CacheRRset) oneElement);
                        return setResponse2;
                    } else if (oneElement != null) {
                        return new SetResponse(2);
                    } else {
                        Element oneElement2 = oneElement(name2, obj, 5, i2);
                        if (oneElement2 != null && (oneElement2 instanceof CacheRRset)) {
                            return new SetResponse(4, (CacheRRset) oneElement2);
                        }
                    }
                } else {
                    Element oneElement3 = oneElement(name2, obj, 39, i2);
                    if (oneElement3 != null && (oneElement3 instanceof CacheRRset)) {
                        return new SetResponse(5, (CacheRRset) oneElement3);
                    }
                }
                Element oneElement4 = oneElement(name2, obj, 2, i2);
                if (oneElement4 != null && (oneElement4 instanceof CacheRRset)) {
                    return new SetResponse(3, (CacheRRset) oneElement4);
                } else if (z2 && oneElement(name2, obj, 0, i2) != null) {
                    return SetResponse.ofType(1);
                }
            }
            i3--;
        }
        return SetResponse.ofType(0);
    }

    public SetResponse lookupRecords(Name name, int i, int i2) {
        return lookup(name, i, i2);
    }

    private final int getCred(int i, boolean z) {
        if (i == 1) {
            return z ? 4 : 3;
        }
        if (i == 2) {
            return z ? 4 : 3;
        }
        if (i == 3) {
            return 1;
        }
        throw new IllegalArgumentException("getCred: invalid section");
    }

    private static void markAdditional(RRset rRset, Set set) {
        if (rRset.first().getAdditionalName() != null) {
            Iterator rrs = rRset.rrs();
            while (rrs.hasNext()) {
                Name additionalName = ((Record) rrs.next()).getAdditionalName();
                if (additionalName != null) {
                    set.add(additionalName);
                }
            }
        }
    }

    public SetResponse addMessage(Message message) {
        boolean flag = message.getHeader().getFlag(5);
        Record question = message.getQuestion();
        int rcode = message.getHeader().getRcode();
        boolean check = Options.check("verbosecache");
        if (!(rcode == 0 || rcode == 3) || question == null) {
            return null;
        }
        Name name = question.getName();
        int type = question.getType();
        int dClass = question.getDClass();
        HashSet hashSet = new HashSet();
        int i = 1;
        RRset[] sectionRRsets = message.getSectionRRsets(1);
        SetResponse setResponse = null;
        Name name2 = name;
        int i2 = 0;
        boolean z = false;
        while (i2 < sectionRRsets.length) {
            if (sectionRRsets[i2].getDClass() == dClass) {
                int type2 = sectionRRsets[i2].getType();
                Name name3 = sectionRRsets[i2].getName();
                int cred = getCred(i, flag);
                if ((type2 == type || type == 255) && name3.equals(name2)) {
                    addRRset(sectionRRsets[i2], cred);
                    if (name2 == name) {
                        SetResponse setResponse2 = setResponse == null ? new SetResponse(6) : setResponse;
                        setResponse2.addRRset(sectionRRsets[i2]);
                        setResponse = setResponse2;
                    }
                    markAdditional(sectionRRsets[i2], hashSet);
                    z = true;
                } else if (type2 == 5 && name3.equals(name2)) {
                    addRRset(sectionRRsets[i2], cred);
                    if (name2 == name) {
                        setResponse = new SetResponse(4, sectionRRsets[i2]);
                    }
                    name2 = ((CNAMERecord) sectionRRsets[i2].first()).getTarget();
                } else if (type2 == 39 && name2.subdomain(name3)) {
                    addRRset(sectionRRsets[i2], cred);
                    if (name2 == name) {
                        setResponse = new SetResponse(5, sectionRRsets[i2]);
                    }
                    try {
                        name2 = name2.fromDNAME((DNAMERecord) sectionRRsets[i2].first());
                    } catch (NameTooLongException unused) {
                    }
                }
                i2++;
                i = 1;
            }
            i2++;
            i = 1;
        }
        int i3 = 2;
        RRset[] sectionRRsets2 = message.getSectionRRsets(2);
        RRset rRset = null;
        RRset rRset2 = null;
        for (int i4 = 0; i4 < sectionRRsets2.length; i4++) {
            if (sectionRRsets2[i4].getType() == 6 && name2.subdomain(sectionRRsets2[i4].getName())) {
                rRset2 = sectionRRsets2[i4];
            } else if (sectionRRsets2[i4].getType() == 2 && name2.subdomain(sectionRRsets2[i4].getName())) {
                rRset = sectionRRsets2[i4];
            }
        }
        if (!z) {
            if (rcode == 3) {
                type = 0;
            }
            if (rcode == 3 || rRset2 != null || rRset == null) {
                addNegative(name2, type, rRset2 != null ? (SOARecord) rRset2.first() : null, getCred(2, flag));
                if (setResponse == null) {
                    if (rcode == 3) {
                        i3 = 1;
                    }
                    setResponse = SetResponse.ofType(i3);
                }
            } else {
                addRRset(rRset, getCred(2, flag));
                markAdditional(rRset, hashSet);
                if (setResponse == null) {
                    setResponse = new SetResponse(3, rRset);
                }
            }
        } else if (rcode == 0 && rRset != null) {
            addRRset(rRset, getCred(2, flag));
            markAdditional(rRset, hashSet);
        }
        RRset[] sectionRRsets3 = message.getSectionRRsets(3);
        for (int i5 = 0; i5 < sectionRRsets3.length; i5++) {
            int type3 = sectionRRsets3[i5].getType();
            if ((type3 == 1 || type3 == 28 || type3 == 38) && hashSet.contains(sectionRRsets3[i5].getName())) {
                addRRset(sectionRRsets3[i5], getCred(3, flag));
            }
        }
        if (check) {
            PrintStream printStream = System.out;
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("addMessage: ");
            stringBuffer.append(setResponse);
            printStream.println(stringBuffer.toString());
        }
        return setResponse;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        synchronized (this) {
            for (Object obj : this.data.values()) {
                for (Element element : allElements(obj)) {
                    stringBuffer.append(element);
                    stringBuffer.append("\n");
                }
            }
        }
        return stringBuffer.toString();
    }
}
