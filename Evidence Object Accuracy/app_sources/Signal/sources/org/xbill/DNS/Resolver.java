package org.xbill.DNS;

import java.io.IOException;

/* loaded from: classes5.dex */
public interface Resolver {
    Message send(Message message) throws IOException;

    Object sendAsync(Message message, ResolverListener resolverListener);

    void setTimeout(int i);

    void setTimeout(int i, int i2);
}
