package org.xbill.DNS;

/* loaded from: classes5.dex */
public class CDNSKEYRecord extends DNSKEYRecord {
    @Override // org.xbill.DNS.DNSKEYRecord, org.xbill.DNS.Record
    Record getObject() {
        return new CDNSKEYRecord();
    }
}
