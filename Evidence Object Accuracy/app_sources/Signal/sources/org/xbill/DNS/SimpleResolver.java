package org.xbill.DNS;

import java.io.IOException;
import java.io.PrintStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.List;

/* loaded from: classes5.dex */
public class SimpleResolver implements Resolver {
    private static String defaultResolver;
    private static int uniqueID;
    private InetSocketAddress address;
    private boolean ignoreTruncation;
    private InetSocketAddress localAddress;
    private OPTRecord queryOPT;
    private long timeoutValue;
    private boolean useTCP;

    private void verifyTSIG(Message message, Message message2, byte[] bArr, TSIG tsig) {
    }

    public SimpleResolver(String str) throws UnknownHostException {
        InetAddress inetAddress;
        this.timeoutValue = 10000;
        if (str == null && (str = ResolverConfig.getCurrentConfig().server()) == null) {
            str = defaultResolver;
        }
        if (str.equals("0")) {
            inetAddress = InetAddress.getLocalHost();
        } else {
            inetAddress = InetAddress.getByName(str);
        }
        this.address = new InetSocketAddress(inetAddress, 53);
    }

    public SimpleResolver() throws UnknownHostException {
        this(null);
    }

    @Override // org.xbill.DNS.Resolver
    public void setTimeout(int i, int i2) {
        this.timeoutValue = (((long) i) * 1000) + ((long) i2);
    }

    @Override // org.xbill.DNS.Resolver
    public void setTimeout(int i) {
        setTimeout(i, 0);
    }

    long getTimeout() {
        return this.timeoutValue;
    }

    private Message parseMessage(byte[] bArr) throws WireParseException {
        try {
            return new Message(bArr);
        } catch (IOException e) {
            e = e;
            if (Options.check("verbose")) {
                e.printStackTrace();
            }
            if (!(e instanceof WireParseException)) {
                e = new WireParseException("Error parsing message");
            }
            throw ((WireParseException) e);
        }
    }

    private void applyEDNS(Message message) {
        if (this.queryOPT != null && message.getOPT() == null) {
            message.addRecord(this.queryOPT, 3);
        }
    }

    private int maxUDPSize(Message message) {
        OPTRecord opt = message.getOPT();
        if (opt == null) {
            return 512;
        }
        return opt.getPayloadSize();
    }

    @Override // org.xbill.DNS.Resolver
    public Message send(Message message) throws IOException {
        byte[] bArr;
        Message parseMessage;
        Record question;
        if (Options.check("verbose")) {
            PrintStream printStream = System.err;
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Sending to ");
            stringBuffer.append(this.address.getAddress().getHostAddress());
            stringBuffer.append(":");
            stringBuffer.append(this.address.getPort());
            printStream.println(stringBuffer.toString());
        }
        if (message.getHeader().getOpcode() == 0 && (question = message.getQuestion()) != null && question.getType() == 252) {
            return sendAXFR(message);
        }
        Message message2 = (Message) message.clone();
        applyEDNS(message2);
        byte[] wire = message2.toWire(65535);
        int maxUDPSize = maxUDPSize(message2);
        long currentTimeMillis = System.currentTimeMillis() + this.timeoutValue;
        boolean z = false;
        while (true) {
            boolean z2 = (this.useTCP || wire.length > maxUDPSize) ? true : z;
            if (z2) {
                bArr = TCPClient.sendrecv(this.localAddress, this.address, wire, currentTimeMillis);
            } else {
                bArr = UDPClient.sendrecv(this.localAddress, this.address, wire, maxUDPSize, currentTimeMillis);
            }
            if (bArr.length >= 12) {
                int i = ((bArr[0] & 255) << 8) + (bArr[1] & 255);
                int id = message2.getHeader().getID();
                if (i != id) {
                    StringBuffer stringBuffer2 = new StringBuffer();
                    stringBuffer2.append("invalid message id: expected ");
                    stringBuffer2.append(id);
                    stringBuffer2.append("; got id ");
                    stringBuffer2.append(i);
                    String stringBuffer3 = stringBuffer2.toString();
                    if (!z2) {
                        if (Options.check("verbose")) {
                            System.err.println(stringBuffer3);
                        }
                        z = z2;
                    } else {
                        throw new WireParseException(stringBuffer3);
                    }
                } else {
                    parseMessage = parseMessage(bArr);
                    verifyTSIG(message2, parseMessage, bArr, null);
                    if (z2 || this.ignoreTruncation || !parseMessage.getHeader().getFlag(6)) {
                        break;
                    }
                    z = true;
                }
            } else {
                throw new WireParseException("invalid DNS header - too short");
            }
        }
        return parseMessage;
    }

    @Override // org.xbill.DNS.Resolver
    public Object sendAsync(Message message, ResolverListener resolverListener) {
        Integer num;
        synchronized (this) {
            int i = uniqueID;
            uniqueID = i + 1;
            num = new Integer(i);
        }
        Record question = message.getQuestion();
        String name = question != null ? question.getName().toString() : "(none)";
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(getClass());
        stringBuffer.append(": ");
        stringBuffer.append(name);
        String stringBuffer2 = stringBuffer.toString();
        ResolveThread resolveThread = new ResolveThread(this, message, num, resolverListener);
        resolveThread.setName(stringBuffer2);
        resolveThread.setDaemon(true);
        resolveThread.start();
        return num;
    }

    private Message sendAXFR(Message message) throws IOException {
        ZoneTransferIn newAXFR = ZoneTransferIn.newAXFR(message.getQuestion().getName(), this.address, null);
        newAXFR.setTimeout((int) (getTimeout() / 1000));
        newAXFR.setLocalAddress(this.localAddress);
        try {
            newAXFR.run();
            List<Record> axfr = newAXFR.getAXFR();
            Message message2 = new Message(message.getHeader().getID());
            message2.getHeader().setFlag(5);
            message2.getHeader().setFlag(0);
            message2.addRecord(message.getQuestion(), 0);
            for (Record record : axfr) {
                message2.addRecord(record, 1);
            }
            return message2;
        } catch (ZoneTransferException e) {
            throw new WireParseException(e.getMessage());
        }
    }
}
