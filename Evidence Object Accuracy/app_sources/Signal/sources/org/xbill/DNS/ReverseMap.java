package org.xbill.DNS;

import java.net.InetAddress;

/* loaded from: classes5.dex */
public final class ReverseMap {
    private static Name inaddr4 = Name.fromConstantString("in-addr.arpa.");
    private static Name inaddr6 = Name.fromConstantString("ip6.arpa.");

    public static Name fromAddress(byte[] bArr) {
        if (bArr.length == 4 || bArr.length == 16) {
            StringBuffer stringBuffer = new StringBuffer();
            if (bArr.length == 4) {
                for (int length = bArr.length - 1; length >= 0; length--) {
                    stringBuffer.append(bArr[length] & 255);
                    if (length > 0) {
                        stringBuffer.append(".");
                    }
                }
            } else {
                int[] iArr = new int[2];
                for (int length2 = bArr.length - 1; length2 >= 0; length2--) {
                    byte b = bArr[length2];
                    iArr[0] = (b & 255) >> 4;
                    iArr[1] = b & 255 & 15;
                    for (int i = 1; i >= 0; i--) {
                        stringBuffer.append(Integer.toHexString(iArr[i]));
                        if (length2 > 0 || i > 0) {
                            stringBuffer.append(".");
                        }
                    }
                }
            }
            try {
                if (bArr.length == 4) {
                    return Name.fromString(stringBuffer.toString(), inaddr4);
                }
                return Name.fromString(stringBuffer.toString(), inaddr6);
            } catch (TextParseException unused) {
                throw new IllegalStateException("name cannot be invalid");
            }
        } else {
            throw new IllegalArgumentException("array must contain 4 or 16 elements");
        }
    }

    public static Name fromAddress(InetAddress inetAddress) {
        return fromAddress(inetAddress.getAddress());
    }
}
