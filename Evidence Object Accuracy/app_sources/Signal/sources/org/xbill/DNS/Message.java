package org.xbill.DNS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

/* loaded from: classes5.dex */
public class Message implements Cloneable {
    private static RRset[] emptyRRsetArray = new RRset[0];
    private static Record[] emptyRecordArray = new Record[0];
    private Header header;
    private List[] sections;
    int sig0start;
    private int size;
    int tsigState;
    int tsigstart;

    private Message(Header header) {
        this.sections = new List[4];
        this.header = header;
    }

    public Message(int i) {
        this(new Header(i));
    }

    public Message() {
        this(new Header());
    }

    public static Message newQuery(Record record) {
        Message message = new Message();
        message.header.setOpcode(0);
        message.header.setFlag(7);
        message.addRecord(record, 0);
        return message;
    }

    Message(DNSInput dNSInput) throws IOException {
        this(new Header(dNSInput));
        boolean z = this.header.getOpcode() == 5;
        boolean flag = this.header.getFlag(6);
        for (int i = 0; i < 4; i++) {
            try {
                int count = this.header.getCount(i);
                if (count > 0) {
                    this.sections[i] = new ArrayList(count);
                }
                for (int i2 = 0; i2 < count; i2++) {
                    int current = dNSInput.current();
                    Record fromWire = Record.fromWire(dNSInput, i, z);
                    this.sections[i].add(fromWire);
                    if (i == 3) {
                        if (fromWire.getType() == 250) {
                            this.tsigstart = current;
                        }
                        if (fromWire.getType() == 24 && ((SIGRecord) fromWire).getTypeCovered() == 0) {
                            this.sig0start = current;
                        }
                    }
                }
            } catch (WireParseException e) {
                if (!flag) {
                    throw e;
                }
            }
        }
        this.size = dNSInput.current();
    }

    public Message(byte[] bArr) throws IOException {
        this(new DNSInput(bArr));
    }

    public Header getHeader() {
        return this.header;
    }

    public void addRecord(Record record, int i) {
        List[] listArr = this.sections;
        if (listArr[i] == null) {
            listArr[i] = new LinkedList();
        }
        this.header.incCount(i);
        this.sections[i].add(record);
    }

    public Record getQuestion() {
        List list = this.sections[0];
        if (list == null || list.size() == 0) {
            return null;
        }
        return (Record) list.get(0);
    }

    public boolean isSigned() {
        int i = this.tsigState;
        return i == 3 || i == 1 || i == 4;
    }

    public boolean isVerified() {
        return this.tsigState == 1;
    }

    public OPTRecord getOPT() {
        Record[] sectionArray = getSectionArray(3);
        for (Record record : sectionArray) {
            if (record instanceof OPTRecord) {
                return (OPTRecord) record;
            }
        }
        return null;
    }

    public int getRcode() {
        int rcode = this.header.getRcode();
        OPTRecord opt = getOPT();
        return opt != null ? rcode + (opt.getExtendedRcode() << 4) : rcode;
    }

    public Record[] getSectionArray(int i) {
        List list = this.sections[i];
        if (list == null) {
            return emptyRecordArray;
        }
        return (Record[]) list.toArray(new Record[list.size()]);
    }

    private static boolean sameSet(Record record, Record record2) {
        return record.getRRsetType() == record2.getRRsetType() && record.getDClass() == record2.getDClass() && record.getName().equals(record2.getName());
    }

    public RRset[] getSectionRRsets(int i) {
        if (this.sections[i] == null) {
            return emptyRRsetArray;
        }
        LinkedList linkedList = new LinkedList();
        Record[] sectionArray = getSectionArray(i);
        HashSet hashSet = new HashSet();
        for (int i2 = 0; i2 < sectionArray.length; i2++) {
            Name name = sectionArray[i2].getName();
            boolean z = true;
            if (hashSet.contains(name)) {
                int size = linkedList.size() - 1;
                while (true) {
                    if (size < 0) {
                        break;
                    }
                    RRset rRset = (RRset) linkedList.get(size);
                    if (rRset.getType() == sectionArray[i2].getRRsetType() && rRset.getDClass() == sectionArray[i2].getDClass() && rRset.getName().equals(name)) {
                        rRset.addRR(sectionArray[i2]);
                        z = false;
                        break;
                    }
                    size--;
                }
            }
            if (z) {
                linkedList.add(new RRset(sectionArray[i2]));
                hashSet.add(name);
            }
        }
        return (RRset[]) linkedList.toArray(new RRset[linkedList.size()]);
    }

    private int sectionToWire(DNSOutput dNSOutput, int i, Compression compression, int i2) {
        int size = this.sections[i].size();
        int current = dNSOutput.current();
        Record record = null;
        int i3 = 0;
        int i4 = 0;
        for (int i5 = 0; i5 < size; i5++) {
            Record record2 = (Record) this.sections[i].get(i5);
            if (i != 3 || !(record2 instanceof OPTRecord)) {
                if (record != null && !sameSet(record2, record)) {
                    current = dNSOutput.current();
                    i4 = i3;
                }
                record2.toWire(dNSOutput, i, compression);
                if (dNSOutput.current() > i2) {
                    dNSOutput.jump(current);
                    return size - i4;
                }
                i3++;
                record = record2;
            }
        }
        return size - i3;
    }

    private boolean toWire(DNSOutput dNSOutput, int i) {
        if (i < 12) {
            return false;
        }
        OPTRecord opt = getOPT();
        byte[] bArr = null;
        if (opt != null) {
            bArr = opt.toWire(3);
            i -= bArr.length;
        }
        int current = dNSOutput.current();
        this.header.toWire(dNSOutput);
        Compression compression = new Compression();
        int flagsByte = this.header.getFlagsByte();
        int i2 = 0;
        int i3 = 0;
        while (true) {
            if (i2 >= 4) {
                break;
            }
            if (this.sections[i2] != null) {
                int sectionToWire = sectionToWire(dNSOutput, i2, compression, i);
                if (sectionToWire != 0 && i2 != 3) {
                    flagsByte = Header.setFlag(flagsByte, 6, true);
                    int i4 = current + 4;
                    dNSOutput.writeU16At(this.header.getCount(i2) - sectionToWire, (i2 * 2) + i4);
                    for (int i5 = i2 + 1; i5 < 3; i5++) {
                        dNSOutput.writeU16At(0, (i5 * 2) + i4);
                    }
                } else if (i2 == 3) {
                    i3 = this.header.getCount(i2) - sectionToWire;
                }
            }
            i2++;
        }
        if (bArr != null) {
            dNSOutput.writeByteArray(bArr);
            i3++;
        }
        if (flagsByte != this.header.getFlagsByte()) {
            dNSOutput.writeU16At(flagsByte, current + 2);
        }
        if (i3 != this.header.getCount(3)) {
            dNSOutput.writeU16At(i3, current + 10);
        }
        return true;
    }

    public byte[] toWire(int i) {
        DNSOutput dNSOutput = new DNSOutput();
        toWire(dNSOutput, i);
        this.size = dNSOutput.current();
        return dNSOutput.toByteArray();
    }

    public int numBytes() {
        return this.size;
    }

    public String sectionToString(int i) {
        if (i > 3) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        Record[] sectionArray = getSectionArray(i);
        for (Record record : sectionArray) {
            if (i == 0) {
                StringBuffer stringBuffer2 = new StringBuffer();
                stringBuffer2.append(";;\t");
                stringBuffer2.append(record.name);
                stringBuffer.append(stringBuffer2.toString());
                StringBuffer stringBuffer3 = new StringBuffer();
                stringBuffer3.append(", type = ");
                stringBuffer3.append(Type.string(record.type));
                stringBuffer.append(stringBuffer3.toString());
                StringBuffer stringBuffer4 = new StringBuffer();
                stringBuffer4.append(", class = ");
                stringBuffer4.append(DClass.string(record.dclass));
                stringBuffer.append(stringBuffer4.toString());
            } else {
                stringBuffer.append(record);
            }
            stringBuffer.append("\n");
        }
        return stringBuffer.toString();
    }

    @Override // java.lang.Object
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        if (getOPT() != null) {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append(this.header.toStringWithRcode(getRcode()));
            stringBuffer2.append("\n");
            stringBuffer.append(stringBuffer2.toString());
        } else {
            StringBuffer stringBuffer3 = new StringBuffer();
            stringBuffer3.append(this.header);
            stringBuffer3.append("\n");
            stringBuffer.append(stringBuffer3.toString());
        }
        if (isSigned()) {
            stringBuffer.append(";; TSIG ");
            if (isVerified()) {
                stringBuffer.append("ok");
            } else {
                stringBuffer.append("invalid");
            }
            stringBuffer.append('\n');
        }
        for (int i = 0; i < 4; i++) {
            if (this.header.getOpcode() != 5) {
                StringBuffer stringBuffer4 = new StringBuffer();
                stringBuffer4.append(";; ");
                stringBuffer4.append(Section.longString(i));
                stringBuffer4.append(":\n");
                stringBuffer.append(stringBuffer4.toString());
            } else {
                StringBuffer stringBuffer5 = new StringBuffer();
                stringBuffer5.append(";; ");
                stringBuffer5.append(Section.updString(i));
                stringBuffer5.append(":\n");
                stringBuffer.append(stringBuffer5.toString());
            }
            StringBuffer stringBuffer6 = new StringBuffer();
            stringBuffer6.append(sectionToString(i));
            stringBuffer6.append("\n");
            stringBuffer.append(stringBuffer6.toString());
        }
        StringBuffer stringBuffer7 = new StringBuffer();
        stringBuffer7.append(";; Message size: ");
        stringBuffer7.append(numBytes());
        stringBuffer7.append(" bytes");
        stringBuffer.append(stringBuffer7.toString());
        return stringBuffer.toString();
    }

    @Override // java.lang.Object
    public Object clone() {
        Message message = new Message();
        int i = 0;
        while (true) {
            List[] listArr = this.sections;
            if (i < listArr.length) {
                if (listArr[i] != null) {
                    message.sections[i] = new LinkedList(this.sections[i]);
                }
                i++;
            } else {
                message.header = (Header) this.header.clone();
                message.size = this.size;
                return message;
            }
        }
    }
}
