package org.xbill.DNS.spi;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.StringTokenizer;
import org.xbill.DNS.AAAARecord;
import org.xbill.DNS.ARecord;
import org.xbill.DNS.ExtendedResolver;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.Name;
import org.xbill.DNS.PTRRecord;
import org.xbill.DNS.Record;
import org.xbill.DNS.ReverseMap;
import org.xbill.DNS.TextParseException;

/* loaded from: classes5.dex */
public class DNSJavaNameService implements InvocationHandler {
    static /* synthetic */ Class array$$B;
    static /* synthetic */ Class array$Ljava$net$InetAddress;
    private boolean preferV6 = false;

    public DNSJavaNameService() {
        String property = System.getProperty("sun.net.spi.nameservice.nameservers");
        String property2 = System.getProperty("sun.net.spi.nameservice.domain");
        String property3 = System.getProperty("java.net.preferIPv6Addresses");
        if (property != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(property, ",");
            String[] strArr = new String[stringTokenizer.countTokens()];
            int i = 0;
            while (stringTokenizer.hasMoreTokens()) {
                strArr[i] = stringTokenizer.nextToken();
                i++;
            }
            try {
                Lookup.setDefaultResolver(new ExtendedResolver(strArr));
            } catch (UnknownHostException unused) {
                System.err.println("DNSJavaNameService: invalid sun.net.spi.nameservice.nameservers");
            }
        }
        if (property2 != null) {
            try {
                Lookup.setDefaultSearchPath(new String[]{property2});
            } catch (TextParseException unused2) {
                System.err.println("DNSJavaNameService: invalid sun.net.spi.nameservice.domain");
            }
        }
        if (property3 != null && property3.equalsIgnoreCase("true")) {
            this.preferV6 = true;
        }
    }

    @Override // java.lang.reflect.InvocationHandler
    public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
        try {
            if (method.getName().equals("getHostByAddr")) {
                return getHostByAddr((byte[]) objArr[0]);
            }
            if (method.getName().equals("lookupAllHostAddr")) {
                InetAddress[] lookupAllHostAddr = lookupAllHostAddr((String) objArr[0]);
                Class<?> returnType = method.getReturnType();
                Class cls = array$Ljava$net$InetAddress;
                if (cls == null) {
                    cls = class$("[Ljava.net.InetAddress;");
                    array$Ljava$net$InetAddress = cls;
                }
                if (returnType.equals(cls)) {
                    return lookupAllHostAddr;
                }
                Class cls2 = array$$B;
                if (cls2 == null) {
                    cls2 = class$("[[B");
                    array$$B = cls2;
                }
                if (returnType.equals(cls2)) {
                    int length = lookupAllHostAddr.length;
                    byte[][] bArr = new byte[length];
                    for (int i = 0; i < length; i++) {
                        bArr[i] = lookupAllHostAddr[i].getAddress();
                    }
                    return bArr;
                }
            }
            throw new IllegalArgumentException("Unknown function name or arguments.");
        } catch (Throwable th) {
            System.err.println("DNSJavaNameService: Unexpected error.");
            th.printStackTrace();
            throw th;
        }
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public InetAddress[] lookupAllHostAddr(String str) throws UnknownHostException {
        try {
            Name name = new Name(str);
            Record[] recordArr = null;
            if (this.preferV6) {
                recordArr = new Lookup(name, 28).run();
            }
            if (recordArr == null) {
                recordArr = new Lookup(name, 1).run();
            }
            if (recordArr == null && !this.preferV6) {
                recordArr = new Lookup(name, 28).run();
            }
            if (recordArr != null) {
                InetAddress[] inetAddressArr = new InetAddress[recordArr.length];
                for (int i = 0; i < recordArr.length; i++) {
                    Record record = recordArr[i];
                    if (record instanceof ARecord) {
                        inetAddressArr[i] = ((ARecord) record).getAddress();
                    } else {
                        inetAddressArr[i] = ((AAAARecord) record).getAddress();
                    }
                }
                return inetAddressArr;
            }
            throw new UnknownHostException(str);
        } catch (TextParseException unused) {
            throw new UnknownHostException(str);
        }
    }

    public String getHostByAddr(byte[] bArr) throws UnknownHostException {
        Record[] run = new Lookup(ReverseMap.fromAddress(InetAddress.getByAddress(bArr)), 12).run();
        if (run != null) {
            return ((PTRRecord) run[0]).getTarget().toString();
        }
        throw new UnknownHostException();
    }
}
