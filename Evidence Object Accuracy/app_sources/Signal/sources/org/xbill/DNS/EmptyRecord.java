package org.xbill.DNS;

import java.io.IOException;

/* loaded from: classes5.dex */
public class EmptyRecord extends Record {
    @Override // org.xbill.DNS.Record
    void rrFromWire(DNSInput dNSInput) throws IOException {
    }

    @Override // org.xbill.DNS.Record
    String rrToString() {
        return "";
    }

    @Override // org.xbill.DNS.Record
    void rrToWire(DNSOutput dNSOutput, Compression compression, boolean z) {
    }

    @Override // org.xbill.DNS.Record
    Record getObject() {
        return new EmptyRecord();
    }
}
