package org.xbill.DNS;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

/* loaded from: classes5.dex */
public class ResolverConfig {
    static /* synthetic */ Class class$java$lang$String;
    static /* synthetic */ Class class$org$xbill$DNS$ResolverConfig;
    private static ResolverConfig currentConfig;
    private int ndots = -1;
    private Name[] searchlist = null;
    private String[] servers = null;

    static {
        refresh();
    }

    public ResolverConfig() {
        if (findProperty() || findSunJVM()) {
            return;
        }
        if (this.servers == null || this.searchlist == null) {
            String property = System.getProperty("os.name");
            String property2 = System.getProperty("java.vendor");
            if (property.indexOf("Windows") != -1) {
                if (property.indexOf("95") == -1 && property.indexOf("98") == -1 && property.indexOf("ME") == -1) {
                    findNT();
                } else {
                    find95();
                }
            } else if (property.indexOf("NetWare") != -1) {
                findNetware();
            } else if (property2.indexOf("Android") != -1) {
                findAndroid();
            } else {
                findUnix();
            }
        }
    }

    private void addServer(String str, List list) {
        if (!list.contains(str)) {
            if (Options.check("verbose")) {
                PrintStream printStream = System.out;
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("adding server ");
                stringBuffer.append(str);
                printStream.println(stringBuffer.toString());
            }
            list.add(str);
        }
    }

    private void addSearch(String str, List list) {
        if (Options.check("verbose")) {
            PrintStream printStream = System.out;
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("adding search ");
            stringBuffer.append(str);
            printStream.println(stringBuffer.toString());
        }
        try {
            Name fromString = Name.fromString(str, Name.root);
            if (!list.contains(fromString)) {
                list.add(fromString);
            }
        } catch (TextParseException unused) {
        }
    }

    private int parseNdots(String str) {
        String substring = str.substring(6);
        try {
            int parseInt = Integer.parseInt(substring);
            if (parseInt < 0) {
                return -1;
            }
            if (Options.check("verbose")) {
                PrintStream printStream = System.out;
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("setting ndots ");
                stringBuffer.append(substring);
                printStream.println(stringBuffer.toString());
            }
            return parseInt;
        } catch (NumberFormatException unused) {
            return -1;
        }
    }

    private void configureFromLists(List list, List list2) {
        if (this.servers == null && list.size() > 0) {
            this.servers = (String[]) list.toArray(new String[0]);
        }
        if (this.searchlist == null && list2.size() > 0) {
            this.searchlist = (Name[]) list2.toArray(new Name[0]);
        }
    }

    private void configureNdots(int i) {
        if (this.ndots < 0 && i > 0) {
            this.ndots = i;
        }
    }

    private boolean findProperty() {
        ArrayList arrayList = new ArrayList(0);
        ArrayList arrayList2 = new ArrayList(0);
        String property = System.getProperty("dns.server");
        if (property != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(property, ",");
            while (stringTokenizer.hasMoreTokens()) {
                addServer(stringTokenizer.nextToken(), arrayList);
            }
        }
        String property2 = System.getProperty("dns.search");
        if (property2 != null) {
            StringTokenizer stringTokenizer2 = new StringTokenizer(property2, ",");
            while (stringTokenizer2.hasMoreTokens()) {
                addSearch(stringTokenizer2.nextToken(), arrayList2);
            }
        }
        configureFromLists(arrayList, arrayList2);
        if (this.servers == null || this.searchlist == null) {
            return false;
        }
        return true;
    }

    private boolean findSunJVM() {
        ArrayList arrayList = new ArrayList(0);
        ArrayList arrayList2 = new ArrayList(0);
        try {
            Class<?>[] clsArr = new Class[0];
            Object[] objArr = new Object[0];
            Class<?> cls = Class.forName("sun.net.dns.ResolverConfiguration");
            Object invoke = cls.getDeclaredMethod("open", clsArr).invoke(null, objArr);
            List<String> list = (List) cls.getMethod("nameservers", clsArr).invoke(invoke, objArr);
            List<String> list2 = (List) cls.getMethod("searchlist", clsArr).invoke(invoke, objArr);
            if (list.size() == 0) {
                return false;
            }
            if (list.size() > 0) {
                for (String str : list) {
                    addServer(str, arrayList);
                }
            }
            if (list2.size() > 0) {
                for (String str2 : list2) {
                    addSearch(str2, arrayList2);
                }
            }
            configureFromLists(arrayList, arrayList2);
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    private void findResolvConf(String str) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(str)));
            ArrayList arrayList = new ArrayList(0);
            ArrayList arrayList2 = new ArrayList(0);
            int i = -1;
            while (true) {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    } else if (readLine.startsWith("nameserver")) {
                        StringTokenizer stringTokenizer = new StringTokenizer(readLine);
                        stringTokenizer.nextToken();
                        addServer(stringTokenizer.nextToken(), arrayList);
                    } else if (readLine.startsWith("domain")) {
                        StringTokenizer stringTokenizer2 = new StringTokenizer(readLine);
                        stringTokenizer2.nextToken();
                        if (stringTokenizer2.hasMoreTokens() && arrayList2.isEmpty()) {
                            addSearch(stringTokenizer2.nextToken(), arrayList2);
                        }
                    } else if (readLine.startsWith("search")) {
                        if (!arrayList2.isEmpty()) {
                            arrayList2.clear();
                        }
                        StringTokenizer stringTokenizer3 = new StringTokenizer(readLine);
                        stringTokenizer3.nextToken();
                        while (stringTokenizer3.hasMoreTokens()) {
                            addSearch(stringTokenizer3.nextToken(), arrayList2);
                        }
                    } else if (readLine.startsWith("options")) {
                        StringTokenizer stringTokenizer4 = new StringTokenizer(readLine);
                        stringTokenizer4.nextToken();
                        while (stringTokenizer4.hasMoreTokens()) {
                            String nextToken = stringTokenizer4.nextToken();
                            if (nextToken.startsWith("ndots:")) {
                                i = parseNdots(nextToken);
                            }
                        }
                    }
                } catch (IOException unused) {
                }
            }
            bufferedReader.close();
            configureFromLists(arrayList, arrayList2);
            configureNdots(i);
        } catch (FileNotFoundException unused2) {
        }
    }

    private void findUnix() {
        findResolvConf("/etc/resolv.conf");
    }

    private void findNetware() {
        findResolvConf("sys:/etc/resolv.cfg");
    }

    static /* synthetic */ Class class$(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    private void findWin(InputStream inputStream, Locale locale) {
        ResourceBundle resourceBundle;
        Class cls = class$org$xbill$DNS$ResolverConfig;
        if (cls == null) {
            cls = class$("org.xbill.DNS.ResolverConfig");
            class$org$xbill$DNS$ResolverConfig = cls;
        }
        String name = cls.getPackage().getName();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(name);
        stringBuffer.append(".windows.DNSServer");
        String stringBuffer2 = stringBuffer.toString();
        if (locale != null) {
            resourceBundle = ResourceBundle.getBundle(stringBuffer2, locale);
        } else {
            resourceBundle = ResourceBundle.getBundle(stringBuffer2);
        }
        String string = resourceBundle.getString("host_name");
        String string2 = resourceBundle.getString("primary_dns_suffix");
        String string3 = resourceBundle.getString("dns_suffix");
        String string4 = resourceBundle.getString("dns_servers");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        try {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            while (true) {
                boolean z = false;
                boolean z2 = false;
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine != null) {
                        StringTokenizer stringTokenizer = new StringTokenizer(readLine);
                        if (!stringTokenizer.hasMoreTokens()) {
                            break;
                        }
                        String nextToken = stringTokenizer.nextToken();
                        if (readLine.indexOf(":") != -1) {
                            z = false;
                            z2 = false;
                        }
                        if (readLine.indexOf(string) != -1) {
                            while (stringTokenizer.hasMoreTokens()) {
                                nextToken = stringTokenizer.nextToken();
                            }
                            try {
                                if (Name.fromString(nextToken, null).labels() != 1) {
                                    addSearch(nextToken, arrayList2);
                                }
                            } catch (TextParseException unused) {
                            }
                        } else {
                            if (readLine.indexOf(string2) != -1) {
                                while (stringTokenizer.hasMoreTokens()) {
                                    nextToken = stringTokenizer.nextToken();
                                }
                                if (!nextToken.equals(":")) {
                                    addSearch(nextToken, arrayList2);
                                }
                            } else {
                                if (!z && readLine.indexOf(string3) == -1) {
                                    if (z2 || readLine.indexOf(string4) != -1) {
                                        while (stringTokenizer.hasMoreTokens()) {
                                            nextToken = stringTokenizer.nextToken();
                                        }
                                        if (!nextToken.equals(":")) {
                                            addServer(nextToken, arrayList);
                                            z2 = true;
                                        }
                                    }
                                }
                                while (stringTokenizer.hasMoreTokens()) {
                                    nextToken = stringTokenizer.nextToken();
                                }
                                if (!nextToken.equals(":")) {
                                    addSearch(nextToken, arrayList2);
                                }
                            }
                            z = true;
                        }
                        string = string;
                    } else {
                        configureFromLists(arrayList, arrayList2);
                        return;
                    }
                }
            }
        } catch (IOException unused2) {
        }
    }

    private void findWin(InputStream inputStream) {
        int intValue = Integer.getInteger("org.xbill.DNS.windows.parse.buffer", 8192).intValue();
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, intValue);
        bufferedInputStream.mark(intValue);
        findWin(bufferedInputStream, null);
        if (this.servers == null) {
            try {
                bufferedInputStream.reset();
                findWin(bufferedInputStream, new Locale("", ""));
            } catch (IOException unused) {
            }
        }
    }

    private void find95() {
        try {
            Runtime runtime = Runtime.getRuntime();
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("winipcfg /all /batch ");
            stringBuffer.append("winipcfg.out");
            runtime.exec(stringBuffer.toString()).waitFor();
            findWin(new FileInputStream(new File("winipcfg.out")));
            new File("winipcfg.out").delete();
        } catch (Exception unused) {
        }
    }

    private void findNT() {
        try {
            Process exec = Runtime.getRuntime().exec("ipconfig /all");
            findWin(exec.getInputStream());
            exec.destroy();
        } catch (Exception unused) {
        }
    }

    private void findAndroid() {
        ArrayList arrayList = new ArrayList();
        List arrayList2 = new ArrayList();
        try {
            Class<?> cls = Class.forName("android.os.SystemProperties");
            Class<?>[] clsArr = new Class[1];
            Class<?> cls2 = class$java$lang$String;
            if (cls2 == null) {
                cls2 = class$("java.lang.String");
                class$java$lang$String = cls2;
            }
            clsArr[0] = cls2;
            Method method = cls.getMethod("get", clsArr);
            String[] strArr = {"net.dns1", "net.dns2", "net.dns3", "net.dns4"};
            for (int i = 0; i < 4; i++) {
                String str = (String) method.invoke(null, strArr[i]);
                if (str != null && ((str.matches("^\\d+(\\.\\d+){3}$") || str.matches("^[0-9a-f]+(:[0-9a-f]*)+:[0-9a-f]+$")) && !arrayList.contains(str))) {
                    arrayList.add(str);
                }
            }
        } catch (Exception unused) {
        }
        configureFromLists(arrayList, arrayList2);
    }

    public String[] servers() {
        return this.servers;
    }

    public String server() {
        String[] strArr = this.servers;
        if (strArr == null) {
            return null;
        }
        return strArr[0];
    }

    public Name[] searchPath() {
        return this.searchlist;
    }

    public int ndots() {
        int i = this.ndots;
        if (i < 0) {
            return 1;
        }
        return i;
    }

    public static synchronized ResolverConfig getCurrentConfig() {
        ResolverConfig resolverConfig;
        synchronized (ResolverConfig.class) {
            resolverConfig = currentConfig;
        }
        return resolverConfig;
    }

    public static void refresh() {
        ResolverConfig resolverConfig = new ResolverConfig();
        Class cls = class$org$xbill$DNS$ResolverConfig;
        if (cls == null) {
            cls = class$("org.xbill.DNS.ResolverConfig");
            class$org$xbill$DNS$ResolverConfig = cls;
        }
        synchronized (cls) {
            currentConfig = resolverConfig;
        }
    }
}
