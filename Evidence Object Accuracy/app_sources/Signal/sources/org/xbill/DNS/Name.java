package org.xbill.DNS;

import java.io.PrintStream;
import java.io.Serializable;
import java.text.DecimalFormat;

/* loaded from: classes5.dex */
public class Name implements Comparable, Serializable {
    private static final DecimalFormat byteFormat;
    public static final Name empty;
    private static final byte[] emptyLabel = {0};
    private static final byte[] lowercase = new byte[256];
    public static final Name root;
    private static final Name wild;
    private static final byte[] wildLabel = {1, 42};
    private int hashcode;
    private byte[] name;
    private long offsets;

    static {
        emptyLabel = new byte[]{0};
        wildLabel = new byte[]{1, 42};
        DecimalFormat decimalFormat = new DecimalFormat();
        byteFormat = decimalFormat;
        lowercase = new byte[256];
        decimalFormat.setMinimumIntegerDigits(3);
        int i = 0;
        while (true) {
            byte[] bArr = lowercase;
            if (i < bArr.length) {
                if (i < 65 || i > 90) {
                    bArr[i] = (byte) i;
                } else {
                    bArr[i] = (byte) ((i - 65) + 97);
                }
                i++;
            } else {
                Name name = new Name();
                root = name;
                name.appendSafe(emptyLabel, 0, 1);
                Name name2 = new Name();
                empty = name2;
                name2.name = new byte[0];
                Name name3 = new Name();
                wild = name3;
                name3.appendSafe(wildLabel, 0, 1);
                return;
            }
        }
    }

    private Name() {
    }

    private final void setoffset(int i, int i2) {
        if (i < 7) {
            int i3 = (7 - i) * 8;
            this.offsets = (((long) i2) << i3) | (this.offsets & ((255 << i3) ^ -1));
        }
    }

    private final int offset(int i) {
        if (i == 0 && getlabels() == 0) {
            return 0;
        }
        if (i < 0 || i >= getlabels()) {
            throw new IllegalArgumentException("label out of range");
        } else if (i < 7) {
            return ((int) (this.offsets >>> ((7 - i) * 8))) & 255;
        } else {
            int offset = offset(6);
            for (int i2 = 6; i2 < i; i2++) {
                offset += this.name[offset] + 1;
            }
            return offset;
        }
    }

    private final void setlabels(int i) {
        this.offsets = (this.offsets & -256) | ((long) i);
    }

    private final int getlabels() {
        return (int) (this.offsets & 255);
    }

    private static final void copy(Name name, Name name2) {
        int i = 0;
        if (name.offset(0) == 0) {
            name2.name = name.name;
            name2.offsets = name.offsets;
            return;
        }
        int offset = name.offset(0);
        int length = name.name.length - offset;
        int labels = name.labels();
        byte[] bArr = new byte[length];
        name2.name = bArr;
        System.arraycopy(name.name, offset, bArr, 0, length);
        while (i < labels && i < 7) {
            name2.setoffset(i, name.offset(i) - offset);
            i++;
        }
        name2.setlabels(labels);
    }

    private final void append(byte[] bArr, int i, int i2) throws NameTooLongException {
        byte[] bArr2 = this.name;
        int length = bArr2 == null ? 0 : bArr2.length - offset(0);
        int i3 = i;
        int i4 = 0;
        for (int i5 = 0; i5 < i2; i5++) {
            byte b = bArr[i3];
            if (b <= 63) {
                int i6 = b + 1;
                i3 += i6;
                i4 += i6;
            } else {
                throw new IllegalStateException("invalid label");
            }
        }
        int i7 = length + i4;
        if (i7 <= 255) {
            int i8 = getlabels();
            int i9 = i8 + i2;
            if (i9 <= 128) {
                byte[] bArr3 = new byte[i7];
                if (length != 0) {
                    System.arraycopy(this.name, offset(0), bArr3, 0, length);
                }
                System.arraycopy(bArr, i, bArr3, length, i4);
                this.name = bArr3;
                for (int i10 = 0; i10 < i2; i10++) {
                    setoffset(i8 + i10, length);
                    length += bArr3[length] + 1;
                }
                setlabels(i9);
                return;
            }
            throw new IllegalStateException("too many labels");
        }
        throw new NameTooLongException();
    }

    private static TextParseException parseException(String str, String str2) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("'");
        stringBuffer.append(str);
        stringBuffer.append("': ");
        stringBuffer.append(str2);
        return new TextParseException(stringBuffer.toString());
    }

    private final void appendFromString(String str, byte[] bArr, int i, int i2) throws TextParseException {
        try {
            append(bArr, i, i2);
        } catch (NameTooLongException unused) {
            throw parseException(str, "Name too long");
        }
    }

    private final void appendSafe(byte[] bArr, int i, int i2) {
        try {
            append(bArr, i, i2);
        } catch (NameTooLongException unused) {
        }
    }

    public Name(String str, Name name) throws TextParseException {
        int i;
        boolean z;
        int i2;
        if (str.equals("")) {
            throw parseException(str, "empty name");
        } else if (str.equals("@")) {
            if (name == null) {
                copy(empty, this);
            } else {
                copy(name, this);
            }
        } else if (str.equals(".")) {
            copy(root, this);
        } else {
            byte[] bArr = new byte[64];
            int i3 = 0;
            boolean z2 = false;
            int i4 = -1;
            int i5 = 1;
            int i6 = 0;
            for (int i7 = 0; i7 < str.length(); i7++) {
                byte charAt = (byte) str.charAt(i7);
                if (z2) {
                    if (charAt >= 48 && charAt <= 57 && i3 < 3) {
                        i3++;
                        i6 = (i6 * 10) + (charAt - 48);
                        if (i6 > 255) {
                            throw parseException(str, "bad escape");
                        } else if (i3 < 3) {
                            continue;
                        } else {
                            charAt = (byte) i6;
                        }
                    } else if (i3 > 0 && i3 < 3) {
                        throw parseException(str, "bad escape");
                    }
                    if (i5 <= 63) {
                        i2 = i5 + 1;
                        bArr[i5] = charAt;
                        i4 = i5;
                        z2 = false;
                        i5 = i2;
                    } else {
                        throw parseException(str, "label too long");
                    }
                } else {
                    if (charAt == 92) {
                        i3 = 0;
                        z2 = true;
                        i6 = 0;
                    } else if (charAt != 46) {
                        i4 = i4 == -1 ? i7 : i4;
                        if (i5 <= 63) {
                            i2 = i5 + 1;
                            bArr[i5] = charAt;
                            i5 = i2;
                        } else {
                            throw parseException(str, "label too long");
                        }
                    } else if (i4 != -1) {
                        bArr[0] = (byte) (i5 - 1);
                        appendFromString(str, bArr, 0, 1);
                        i4 = -1;
                        i5 = 1;
                    } else {
                        throw parseException(str, "invalid empty label");
                    }
                }
            }
            if (i3 > 0 && i3 < 3) {
                throw parseException(str, "bad escape");
            } else if (!z2) {
                if (i4 == -1) {
                    z = true;
                    i = 0;
                    appendFromString(str, emptyLabel, 0, 1);
                } else {
                    i = 0;
                    bArr[0] = (byte) (i5 - 1);
                    appendFromString(str, bArr, 0, 1);
                    z = false;
                }
                if (!(name == null || z)) {
                    appendFromString(str, name.name, name.offset(i), name.getlabels());
                }
            } else {
                throw parseException(str, "bad escape");
            }
        }
    }

    public Name(String str) throws TextParseException {
        this(str, (Name) null);
    }

    public static Name fromString(String str, Name name) throws TextParseException {
        if (str.equals("@") && name != null) {
            return name;
        }
        if (str.equals(".")) {
            return root;
        }
        return new Name(str, name);
    }

    public static Name fromString(String str) throws TextParseException {
        return fromString(str, null);
    }

    public static Name fromConstantString(String str) {
        try {
            return fromString(str, null);
        } catch (TextParseException unused) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("Invalid name '");
            stringBuffer.append(str);
            stringBuffer.append("'");
            throw new IllegalArgumentException(stringBuffer.toString());
        }
    }

    public Name(DNSInput dNSInput) throws WireParseException {
        byte[] bArr = new byte[64];
        boolean z = false;
        boolean z2 = false;
        while (!z) {
            int readU8 = dNSInput.readU8();
            int i = readU8 & 192;
            if (i != 0) {
                if (i == 192) {
                    int readU82 = dNSInput.readU8() + ((readU8 & -193) << 8);
                    if (Options.check("verbosecompression")) {
                        PrintStream printStream = System.err;
                        StringBuffer stringBuffer = new StringBuffer();
                        stringBuffer.append("currently ");
                        stringBuffer.append(dNSInput.current());
                        stringBuffer.append(", pointer to ");
                        stringBuffer.append(readU82);
                        printStream.println(stringBuffer.toString());
                    }
                    if (readU82 < dNSInput.current() - 2) {
                        if (!z2) {
                            dNSInput.save();
                            z2 = true;
                        }
                        dNSInput.jump(readU82);
                        if (Options.check("verbosecompression")) {
                            PrintStream printStream2 = System.err;
                            StringBuffer stringBuffer2 = new StringBuffer();
                            stringBuffer2.append("current name '");
                            stringBuffer2.append(this);
                            stringBuffer2.append("', seeking to ");
                            stringBuffer2.append(readU82);
                            printStream2.println(stringBuffer2.toString());
                        }
                    } else {
                        throw new WireParseException("bad compression");
                    }
                } else {
                    throw new WireParseException("bad label type");
                }
            } else if (getlabels() >= 128) {
                throw new WireParseException("too many labels");
            } else if (readU8 == 0) {
                append(emptyLabel, 0, 1);
                z = true;
            } else {
                bArr[0] = (byte) readU8;
                dNSInput.readByteArray(bArr, 1, readU8);
                append(bArr, 0, 1);
            }
        }
        if (z2) {
            dNSInput.restore();
        }
    }

    public Name(Name name, int i) {
        int labels = name.labels();
        if (i <= labels) {
            this.name = name.name;
            int i2 = labels - i;
            setlabels(i2);
            int i3 = 0;
            while (i3 < 7 && i3 < i2) {
                setoffset(i3, name.offset(i3 + i));
                i3++;
            }
            return;
        }
        throw new IllegalArgumentException("attempted to remove too many labels");
    }

    public static Name concatenate(Name name, Name name2) throws NameTooLongException {
        if (name.isAbsolute()) {
            return name;
        }
        Name name3 = new Name();
        copy(name, name3);
        name3.append(name2.name, name2.offset(0), name2.getlabels());
        return name3;
    }

    public Name fromDNAME(DNAMERecord dNAMERecord) throws NameTooLongException {
        Name name = dNAMERecord.getName();
        Name target = dNAMERecord.getTarget();
        if (!subdomain(name)) {
            return null;
        }
        int labels = labels() - name.labels();
        int length = length() - name.length();
        int i = 0;
        int offset = offset(0);
        int labels2 = target.labels();
        short length2 = target.length();
        int i2 = length + length2;
        if (i2 <= 255) {
            Name name2 = new Name();
            int i3 = labels + labels2;
            name2.setlabels(i3);
            byte[] bArr = new byte[i2];
            name2.name = bArr;
            System.arraycopy(this.name, offset, bArr, 0, length);
            System.arraycopy(target.name, 0, name2.name, length, length2);
            int i4 = 0;
            while (i < 7 && i < i3) {
                name2.setoffset(i, i4);
                i4 += name2.name[i4] + 1;
                i++;
            }
            return name2;
        }
        throw new NameTooLongException();
    }

    public boolean isAbsolute() {
        int labels = labels();
        if (labels != 0 && this.name[offset(labels - 1)] == 0) {
            return true;
        }
        return false;
    }

    public short length() {
        if (getlabels() == 0) {
            return 0;
        }
        return (short) (this.name.length - offset(0));
    }

    public int labels() {
        return getlabels();
    }

    public boolean subdomain(Name name) {
        int labels = labels();
        int labels2 = name.labels();
        if (labels2 > labels) {
            return false;
        }
        if (labels2 == labels) {
            return equals(name);
        }
        return name.equals(this.name, offset(labels - labels2));
    }

    private String byteString(byte[] bArr, int i) {
        StringBuffer stringBuffer = new StringBuffer();
        int i2 = i + 1;
        byte b = bArr[i];
        for (int i3 = i2; i3 < i2 + b; i3++) {
            int i4 = bArr[i3] & 255;
            if (i4 <= 32 || i4 >= 127) {
                stringBuffer.append('\\');
                stringBuffer.append(byteFormat.format((long) i4));
            } else if (i4 == 34 || i4 == 40 || i4 == 41 || i4 == 46 || i4 == 59 || i4 == 92 || i4 == 64 || i4 == 36) {
                stringBuffer.append('\\');
                stringBuffer.append((char) i4);
            } else {
                stringBuffer.append((char) i4);
            }
        }
        return stringBuffer.toString();
    }

    public String toString(boolean z) {
        int labels = labels();
        if (labels == 0) {
            return "@";
        }
        int i = 0;
        if (labels == 1 && this.name[offset(0)] == 0) {
            return ".";
        }
        StringBuffer stringBuffer = new StringBuffer();
        int offset = offset(0);
        while (true) {
            if (i >= labels) {
                break;
            }
            byte b = this.name[offset];
            if (b > 63) {
                throw new IllegalStateException("invalid label");
            } else if (b != 0) {
                if (i > 0) {
                    stringBuffer.append('.');
                }
                stringBuffer.append(byteString(this.name, offset));
                offset += b + 1;
                i++;
            } else if (!z) {
                stringBuffer.append('.');
            }
        }
        return stringBuffer.toString();
    }

    @Override // java.lang.Object
    public String toString() {
        return toString(false);
    }

    public void toWire(DNSOutput dNSOutput, Compression compression) {
        Name name;
        if (isAbsolute()) {
            int labels = labels();
            for (int i = 0; i < labels - 1; i++) {
                if (i == 0) {
                    name = this;
                } else {
                    name = new Name(this, i);
                }
                int i2 = -1;
                if (compression != null) {
                    i2 = compression.get(name);
                }
                if (i2 >= 0) {
                    dNSOutput.writeU16(49152 | i2);
                    return;
                }
                if (compression != null) {
                    compression.add(dNSOutput.current(), name);
                }
                int offset = offset(i);
                byte[] bArr = this.name;
                dNSOutput.writeByteArray(bArr, offset, bArr[offset] + 1);
            }
            dNSOutput.writeU8(0);
            return;
        }
        throw new IllegalArgumentException("toWire() called on non-absolute name");
    }

    public void toWireCanonical(DNSOutput dNSOutput) {
        dNSOutput.writeByteArray(toWireCanonical());
    }

    public byte[] toWireCanonical() {
        int labels = labels();
        if (labels == 0) {
            return new byte[0];
        }
        byte[] bArr = new byte[this.name.length - offset(0)];
        int offset = offset(0);
        int i = 0;
        for (int i2 = 0; i2 < labels; i2++) {
            byte b = this.name[offset];
            if (b <= 63) {
                offset++;
                bArr[i] = b;
                i++;
                int i3 = 0;
                while (i3 < b) {
                    offset++;
                    bArr[i] = lowercase[this.name[offset] & 255];
                    i3++;
                    i++;
                }
            } else {
                throw new IllegalStateException("invalid label");
            }
        }
        return bArr;
    }

    public void toWire(DNSOutput dNSOutput, Compression compression, boolean z) {
        if (z) {
            toWireCanonical(dNSOutput);
        } else {
            toWire(dNSOutput, compression);
        }
    }

    private final boolean equals(byte[] bArr, int i) {
        int labels = labels();
        int offset = offset(0);
        for (int i2 = 0; i2 < labels; i2++) {
            byte b = this.name[offset];
            if (b != bArr[i]) {
                return false;
            }
            offset++;
            i++;
            if (b <= 63) {
                for (int i3 = 0; i3 < b; i3++) {
                    byte[] bArr2 = lowercase;
                    offset++;
                    i++;
                    if (bArr2[this.name[offset] & 255] != bArr2[bArr[i] & 255]) {
                        return false;
                    }
                }
            } else {
                throw new IllegalStateException("invalid label");
            }
        }
        return true;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || !(obj instanceof Name)) {
            return false;
        }
        Name name = (Name) obj;
        if (name.hashcode == 0) {
            name.hashCode();
        }
        if (this.hashcode == 0) {
            hashCode();
        }
        if (name.hashcode == this.hashcode && name.labels() == labels()) {
            return equals(name.name, name.offset(0));
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        int i = this.hashcode;
        if (i != 0) {
            return i;
        }
        int i2 = 0;
        int offset = offset(0);
        while (true) {
            byte[] bArr = this.name;
            if (offset < bArr.length) {
                i2 += (i2 << 3) + lowercase[bArr[offset] & 255];
                offset++;
            } else {
                this.hashcode = i2;
                return i2;
            }
        }
    }

    @Override // java.lang.Comparable
    public int compareTo(Object obj) {
        Name name = (Name) obj;
        if (this == name) {
            return 0;
        }
        int labels = labels();
        int labels2 = name.labels();
        int i = labels > labels2 ? labels2 : labels;
        for (int i2 = 1; i2 <= i; i2++) {
            int offset = offset(labels - i2);
            int offset2 = name.offset(labels2 - i2);
            byte b = this.name[offset];
            byte b2 = name.name[offset2];
            int i3 = 0;
            while (i3 < b && i3 < b2) {
                byte[] bArr = lowercase;
                int i4 = bArr[this.name[(i3 + offset) + 1] & 255] - bArr[name.name[(i3 + offset2) + 1] & 255];
                if (i4 != 0) {
                    return i4;
                }
                i3++;
            }
            if (b != b2) {
                return b - b2;
            }
        }
        return labels - labels2;
    }
}
