package org.xbill.DNS;

import java.io.IOException;

/* loaded from: classes5.dex */
public abstract class SingleNameBase extends Record {
    protected Name singleName;

    @Override // org.xbill.DNS.Record
    void rrFromWire(DNSInput dNSInput) throws IOException {
        this.singleName = new Name(dNSInput);
    }

    @Override // org.xbill.DNS.Record
    String rrToString() {
        return this.singleName.toString();
    }

    public Name getSingleName() {
        return this.singleName;
    }

    @Override // org.xbill.DNS.Record
    void rrToWire(DNSOutput dNSOutput, Compression compression, boolean z) {
        this.singleName.toWire(dNSOutput, null, z);
    }
}
