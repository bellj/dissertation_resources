package org.xbill.DNS;

/* loaded from: classes5.dex */
public class ZoneTransferException extends Exception {
    public ZoneTransferException(String str) {
        super(str);
    }
}
