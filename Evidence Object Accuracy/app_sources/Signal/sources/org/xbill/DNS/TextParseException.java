package org.xbill.DNS;

import java.io.IOException;

/* loaded from: classes5.dex */
public class TextParseException extends IOException {
    public TextParseException(String str) {
        super(str);
    }
}
