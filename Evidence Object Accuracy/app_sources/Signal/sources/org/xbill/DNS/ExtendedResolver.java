package org.xbill.DNS;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.PrintStream;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes5.dex */
public class ExtendedResolver implements Resolver {
    private int lbStart = 0;
    private boolean loadBalance = false;
    private List resolvers;
    private int retries = 3;

    static /* synthetic */ int access$208(ExtendedResolver extendedResolver) {
        int i = extendedResolver.lbStart;
        extendedResolver.lbStart = i + 1;
        return i;
    }

    /* loaded from: classes5.dex */
    private static class Resolution implements ResolverListener {
        boolean done;
        Object[] inprogress;
        ResolverListener listener;
        int outstanding;
        Message query;
        Resolver[] resolvers;
        Message response;
        int retries;
        int[] sent;
        Throwable thrown;

        public Resolution(ExtendedResolver extendedResolver, Message message) {
            List list = extendedResolver.resolvers;
            this.resolvers = (Resolver[]) list.toArray(new Resolver[list.size()]);
            if (extendedResolver.loadBalance) {
                int length = this.resolvers.length;
                int access$208 = ExtendedResolver.access$208(extendedResolver) % length;
                if (extendedResolver.lbStart > length) {
                    extendedResolver.lbStart %= length;
                }
                if (access$208 > 0) {
                    Resolver[] resolverArr = new Resolver[length];
                    for (int i = 0; i < length; i++) {
                        resolverArr[i] = this.resolvers[(i + access$208) % length];
                    }
                    this.resolvers = resolverArr;
                }
            }
            Resolver[] resolverArr2 = this.resolvers;
            this.sent = new int[resolverArr2.length];
            this.inprogress = new Object[resolverArr2.length];
            this.retries = extendedResolver.retries;
            this.query = message;
        }

        public void send(int i) {
            int[] iArr = this.sent;
            iArr[i] = iArr[i] + 1;
            this.outstanding++;
            try {
                this.inprogress[i] = this.resolvers[i].sendAsync(this.query, this);
            } catch (Throwable th) {
                synchronized (this) {
                    try {
                        this.thrown = th;
                        this.done = true;
                        if (this.listener == null) {
                            notifyAll();
                        }
                    } catch (Throwable th2) {
                        throw th2;
                    }
                }
            }
        }

        public Message start() throws IOException {
            try {
                int[] iArr = this.sent;
                iArr[0] = iArr[0] + 1;
                this.outstanding++;
                this.inprogress[0] = new Object();
                return this.resolvers[0].send(this.query);
            } catch (Exception e) {
                handleException(this.inprogress[0], e);
                synchronized (this) {
                    while (!this.done) {
                        try {
                            wait();
                        } catch (InterruptedException unused) {
                        }
                    }
                    Message message = this.response;
                    if (message != null) {
                        return message;
                    }
                    Throwable th = this.thrown;
                    if (th instanceof IOException) {
                        throw ((IOException) th);
                    } else if (th instanceof RuntimeException) {
                        throw ((RuntimeException) th);
                    } else if (th instanceof Error) {
                        throw ((Error) th);
                    } else {
                        throw new IllegalStateException("ExtendedResolver failure");
                    }
                }
            }
        }

        public void startAsync(ResolverListener resolverListener) {
            this.listener = resolverListener;
            send(0);
        }

        @Override // org.xbill.DNS.ResolverListener
        public void receiveMessage(Object obj, Message message) {
            if (Options.check("verbose")) {
                System.err.println("ExtendedResolver: received message");
            }
            synchronized (this) {
                if (!this.done) {
                    this.response = message;
                    this.done = true;
                    ResolverListener resolverListener = this.listener;
                    if (resolverListener == null) {
                        notifyAll();
                    } else {
                        resolverListener.receiveMessage(this, message);
                    }
                }
            }
        }

        @Override // org.xbill.DNS.ResolverListener
        public void handleException(Object obj, Exception exc) {
            Object[] objArr;
            if (Options.check("verbose")) {
                PrintStream printStream = System.err;
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("ExtendedResolver: got ");
                stringBuffer.append(exc);
                printStream.println(stringBuffer.toString());
            }
            synchronized (this) {
                this.outstanding--;
                if (!this.done) {
                    boolean z = false;
                    int i = 0;
                    while (true) {
                        objArr = this.inprogress;
                        if (i >= objArr.length || objArr[i] == obj) {
                            break;
                        }
                        i++;
                    }
                    if (i != objArr.length) {
                        int i2 = this.sent[i];
                        if (i2 == 1 && i < this.resolvers.length - 1) {
                            z = true;
                        }
                        if (exc instanceof InterruptedIOException) {
                            if (i2 < this.retries) {
                                send(i);
                            }
                            if (this.thrown == null) {
                                this.thrown = exc;
                            }
                        } else if (exc instanceof SocketException) {
                            Throwable th = this.thrown;
                            if (th == null || (th instanceof InterruptedIOException)) {
                                this.thrown = exc;
                            }
                        } else {
                            this.thrown = exc;
                        }
                        if (!this.done) {
                            if (z) {
                                send(i + 1);
                            }
                            if (!this.done) {
                                if (this.outstanding == 0) {
                                    this.done = true;
                                    if (this.listener == null) {
                                        notifyAll();
                                        return;
                                    }
                                }
                                if (this.done) {
                                    if (!(this.thrown instanceof Exception)) {
                                        this.thrown = new RuntimeException(this.thrown.getMessage());
                                    }
                                    this.listener.handleException(this, (Exception) this.thrown);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void init() {
        this.resolvers = new ArrayList();
    }

    public ExtendedResolver() throws UnknownHostException {
        init();
        String[] servers = ResolverConfig.getCurrentConfig().servers();
        if (servers != null) {
            for (String str : servers) {
                SimpleResolver simpleResolver = new SimpleResolver(str);
                simpleResolver.setTimeout(5);
                this.resolvers.add(simpleResolver);
            }
            return;
        }
        this.resolvers.add(new SimpleResolver());
    }

    public ExtendedResolver(String[] strArr) throws UnknownHostException {
        init();
        for (String str : strArr) {
            SimpleResolver simpleResolver = new SimpleResolver(str);
            simpleResolver.setTimeout(5);
            this.resolvers.add(simpleResolver);
        }
    }

    @Override // org.xbill.DNS.Resolver
    public void setTimeout(int i, int i2) {
        for (int i3 = 0; i3 < this.resolvers.size(); i3++) {
            ((Resolver) this.resolvers.get(i3)).setTimeout(i, i2);
        }
    }

    @Override // org.xbill.DNS.Resolver
    public void setTimeout(int i) {
        setTimeout(i, 0);
    }

    @Override // org.xbill.DNS.Resolver
    public Message send(Message message) throws IOException {
        return new Resolution(this, message).start();
    }

    @Override // org.xbill.DNS.Resolver
    public Object sendAsync(Message message, ResolverListener resolverListener) {
        Resolution resolution = new Resolution(this, message);
        resolution.startAsync(resolverListener);
        return resolution;
    }
}
