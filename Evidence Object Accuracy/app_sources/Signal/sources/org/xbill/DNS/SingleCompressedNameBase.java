package org.xbill.DNS;

/* access modifiers changed from: package-private */
/* loaded from: classes5.dex */
public abstract class SingleCompressedNameBase extends SingleNameBase {
    @Override // org.xbill.DNS.SingleNameBase, org.xbill.DNS.Record
    void rrToWire(DNSOutput dNSOutput, Compression compression, boolean z) {
        this.singleName.toWire(dNSOutput, compression, z);
    }
}
