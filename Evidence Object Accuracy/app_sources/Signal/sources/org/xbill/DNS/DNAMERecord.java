package org.xbill.DNS;

/* loaded from: classes5.dex */
public class DNAMERecord extends SingleNameBase {
    @Override // org.xbill.DNS.Record
    Record getObject() {
        return new DNAMERecord();
    }

    public Name getTarget() {
        return getSingleName();
    }
}
