package org.xbill.DNS;

/* loaded from: classes5.dex */
public class PTRRecord extends SingleCompressedNameBase {
    @Override // org.xbill.DNS.Record
    Record getObject() {
        return new PTRRecord();
    }

    public Name getTarget() {
        return getSingleName();
    }
}
