package org.xbill.DNS;

/* loaded from: classes5.dex */
public class CNAMERecord extends SingleCompressedNameBase {
    @Override // org.xbill.DNS.Record
    Record getObject() {
        return new CNAMERecord();
    }

    public Name getTarget() {
        return getSingleName();
    }
}
