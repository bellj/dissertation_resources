package org.xbill.DNS;

import java.util.ArrayList;
import java.util.List;

/* loaded from: classes5.dex */
public class SetResponse {
    private static final SetResponse nxdomain = new SetResponse(1);
    private static final SetResponse nxrrset = new SetResponse(2);
    private static final SetResponse unknown = new SetResponse(0);
    private Object data;
    private int type;

    private SetResponse() {
    }

    public SetResponse(int i, RRset rRset) {
        if (i < 0 || i > 6) {
            throw new IllegalArgumentException("invalid type");
        }
        this.type = i;
        this.data = rRset;
    }

    public SetResponse(int i) {
        if (i < 0 || i > 6) {
            throw new IllegalArgumentException("invalid type");
        }
        this.type = i;
        this.data = null;
    }

    public static SetResponse ofType(int i) {
        switch (i) {
            case 0:
                return unknown;
            case 1:
                return nxdomain;
            case 2:
                return nxrrset;
            case 3:
            case 4:
            case 5:
            case 6:
                SetResponse setResponse = new SetResponse();
                setResponse.type = i;
                setResponse.data = null;
                return setResponse;
            default:
                throw new IllegalArgumentException("invalid type");
        }
    }

    public void addRRset(RRset rRset) {
        if (this.data == null) {
            this.data = new ArrayList();
        }
        ((List) this.data).add(rRset);
    }

    public boolean isNXDOMAIN() {
        return this.type == 1;
    }

    public boolean isNXRRSET() {
        return this.type == 2;
    }

    public boolean isDelegation() {
        return this.type == 3;
    }

    public boolean isCNAME() {
        return this.type == 4;
    }

    public boolean isDNAME() {
        return this.type == 5;
    }

    public boolean isSuccessful() {
        return this.type == 6;
    }

    public RRset[] answers() {
        if (this.type != 6) {
            return null;
        }
        List list = (List) this.data;
        return (RRset[]) list.toArray(new RRset[list.size()]);
    }

    public CNAMERecord getCNAME() {
        return (CNAMERecord) ((RRset) this.data).first();
    }

    public DNAMERecord getDNAME() {
        return (DNAMERecord) ((RRset) this.data).first();
    }

    public String toString() {
        switch (this.type) {
            case 0:
                return "unknown";
            case 1:
                return "NXDOMAIN";
            case 2:
                return "NXRRSET";
            case 3:
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append("delegation: ");
                stringBuffer.append(this.data);
                return stringBuffer.toString();
            case 4:
                StringBuffer stringBuffer2 = new StringBuffer();
                stringBuffer2.append("CNAME: ");
                stringBuffer2.append(this.data);
                return stringBuffer2.toString();
            case 5:
                StringBuffer stringBuffer3 = new StringBuffer();
                stringBuffer3.append("DNAME: ");
                stringBuffer3.append(this.data);
                return stringBuffer3.toString();
            case 6:
                return "successful";
            default:
                throw new IllegalStateException();
        }
    }
}
