package org.xbill.DNS;

import org.thoughtcrime.securesms.database.MmsSmsColumns;

/* loaded from: classes5.dex */
public final class Serial {
    public static int compare(long j, long j2) {
        if (j < 0 || j > 4294967295L) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(j);
            stringBuffer.append(" out of range");
            throw new IllegalArgumentException(stringBuffer.toString());
        } else if (j2 < 0 || j2 > 4294967295L) {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append(j2);
            stringBuffer2.append(" out of range");
            throw new IllegalArgumentException(stringBuffer2.toString());
        } else {
            long j3 = j - j2;
            if (j3 >= 4294967295L) {
                j3 -= MmsSmsColumns.Types.SPECIAL_TYPE_STORY_REACTION;
            } else if (j3 < -4294967295L) {
                j3 += MmsSmsColumns.Types.SPECIAL_TYPE_STORY_REACTION;
            }
            return (int) j3;
        }
    }
}
