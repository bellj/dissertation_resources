package org.slf4j;

/* loaded from: classes3.dex */
public interface Logger {
    void debug(String str, Object obj);

    void error(String str, Throwable th);

    String getName();

    boolean isWarnEnabled();

    void trace(String str, Object... objArr);

    void warn(String str);

    void warn(String str, Throwable th);

    void warn(String str, Object... objArr);
}
