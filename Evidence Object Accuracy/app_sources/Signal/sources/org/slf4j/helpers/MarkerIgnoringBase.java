package org.slf4j.helpers;

/* loaded from: classes3.dex */
public abstract class MarkerIgnoringBase extends NamedLoggerBase {
    @Override // org.slf4j.Logger
    public abstract /* bridge */ /* synthetic */ String getName();

    @Override // java.lang.Object
    public String toString() {
        return getClass().getName() + "(" + getName() + ")";
    }
}
