package org.slf4j.helpers;

import java.io.Serializable;
import org.slf4j.Logger;

/* access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public abstract class NamedLoggerBase implements Logger, Serializable {
}
