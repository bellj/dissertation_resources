package org.thoughtcrime.securesms.invites;

import android.content.Context;
import org.thoughtcrime.securesms.database.MmsSmsDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.invites.InviteReminderModel;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public final class InviteReminderRepository implements InviteReminderModel.Repository {
    private final Context context;

    public InviteReminderRepository(Context context) {
        this.context = context;
    }

    @Override // org.thoughtcrime.securesms.invites.InviteReminderModel.Repository
    public void setHasSeenFirstInviteReminder(Recipient recipient) {
        SignalDatabase.recipients().setSeenFirstInviteReminder(recipient.getId());
    }

    @Override // org.thoughtcrime.securesms.invites.InviteReminderModel.Repository
    public void setHasSeenSecondInviteReminder(Recipient recipient) {
        SignalDatabase.recipients().setSeenSecondInviteReminder(recipient.getId());
    }

    @Override // org.thoughtcrime.securesms.invites.InviteReminderModel.Repository
    public int getPercentOfInsecureMessages(int i) {
        MmsSmsDatabase mmsSms = SignalDatabase.mmsSms();
        int insecureMessageCountForInsights = mmsSms.getInsecureMessageCountForInsights() + mmsSms.getSecureMessageCountForInsights();
        if (insecureMessageCountForInsights == 0) {
            return 0;
        }
        return Math.round((((float) i) / ((float) insecureMessageCountForInsights)) * 100.0f);
    }
}
