package org.thoughtcrime.securesms.invites;

import android.content.Context;
import j$.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.components.reminder.FirstInviteReminder;
import org.thoughtcrime.securesms.components.reminder.Reminder;
import org.thoughtcrime.securesms.components.reminder.SecondInviteReminder;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.invites.InviteReminderModel;
import org.thoughtcrime.securesms.recipients.LiveRecipient;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public final class InviteReminderModel {
    private static final int FIRST_INVITE_REMINDER_MESSAGE_THRESHOLD;
    private static final int SECOND_INVITE_REMINDER_MESSAGE_THRESHOLD;
    private final Context context;
    private final AtomicReference<ReminderInfo> reminderInfo = new AtomicReference<>();
    private final Repository repository;

    /* loaded from: classes4.dex */
    public interface Repository {
        int getPercentOfInsecureMessages(int i);

        void setHasSeenFirstInviteReminder(Recipient recipient);

        void setHasSeenSecondInviteReminder(Recipient recipient);
    }

    public static /* synthetic */ void lambda$dismissReminder$3(Object obj) {
    }

    public InviteReminderModel(Context context, Repository repository) {
        this.context = context;
        this.repository = repository;
    }

    public /* synthetic */ ReminderInfo lambda$loadReminder$0(LiveRecipient liveRecipient) {
        return createReminderInfo(liveRecipient.resolve());
    }

    public void loadReminder(LiveRecipient liveRecipient, Runnable runnable) {
        SimpleTask.run(new SimpleTask.BackgroundTask(liveRecipient) { // from class: org.thoughtcrime.securesms.invites.InviteReminderModel$$ExternalSyntheticLambda0
            public final /* synthetic */ LiveRecipient f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return InviteReminderModel.this.lambda$loadReminder$0(this.f$1);
            }
        }, new SimpleTask.ForegroundTask(runnable) { // from class: org.thoughtcrime.securesms.invites.InviteReminderModel$$ExternalSyntheticLambda1
            public final /* synthetic */ Runnable f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                InviteReminderModel.this.lambda$loadReminder$1(this.f$1, (InviteReminderModel.ReminderInfo) obj);
            }
        });
    }

    public /* synthetic */ void lambda$loadReminder$1(Runnable runnable, ReminderInfo reminderInfo) {
        this.reminderInfo.set(reminderInfo);
        runnable.run();
    }

    private ReminderInfo createReminderInfo(Recipient recipient) {
        Recipient resolve = recipient.resolve();
        if (resolve.isRegistered() || resolve.isGroup() || resolve.hasSeenSecondInviteReminder()) {
            return new NoReminderInfo();
        }
        Long threadIdFor = SignalDatabase.threads().getThreadIdFor(recipient.getId());
        if (threadIdFor != null) {
            int insecureSentCount = SignalDatabase.mmsSms().getInsecureSentCount(threadIdFor.longValue());
            if (insecureSentCount >= SECOND_INVITE_REMINDER_MESSAGE_THRESHOLD && !resolve.hasSeenSecondInviteReminder()) {
                Context context = this.context;
                Repository repository = this.repository;
                return new SecondInviteReminderInfo(context, resolve, repository, repository.getPercentOfInsecureMessages(insecureSentCount));
            } else if (insecureSentCount >= 10 && !resolve.hasSeenFirstInviteReminder()) {
                Context context2 = this.context;
                Repository repository2 = this.repository;
                return new FirstInviteReminderInfo(context2, resolve, repository2, repository2.getPercentOfInsecureMessages(insecureSentCount));
            }
        }
        return new NoReminderInfo();
    }

    public Optional<Reminder> getReminder() {
        ReminderInfo reminderInfo = this.reminderInfo.get();
        if (reminderInfo == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(reminderInfo.reminder);
    }

    public void dismissReminder() {
        SimpleTask.run(new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.invites.InviteReminderModel$$ExternalSyntheticLambda2
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return InviteReminderModel.ReminderInfo.this.dismiss();
            }
        }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.invites.InviteReminderModel$$ExternalSyntheticLambda3
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                InviteReminderModel.lambda$dismissReminder$3(obj);
            }
        });
    }

    /* loaded from: classes4.dex */
    public static abstract class ReminderInfo {
        private final Reminder reminder;

        public void dismiss() {
        }

        ReminderInfo(Reminder reminder) {
            this.reminder = reminder;
        }
    }

    /* loaded from: classes4.dex */
    public static class NoReminderInfo extends ReminderInfo {
        private NoReminderInfo() {
            super(null);
        }
    }

    /* loaded from: classes4.dex */
    public class FirstInviteReminderInfo extends ReminderInfo {
        private final Recipient recipient;
        private final Repository repository;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        private FirstInviteReminderInfo(Context context, Recipient recipient, Repository repository, int i) {
            super(new FirstInviteReminder(context, recipient, i));
            InviteReminderModel.this = r1;
            this.recipient = recipient;
            this.repository = repository;
        }

        @Override // org.thoughtcrime.securesms.invites.InviteReminderModel.ReminderInfo
        public void dismiss() {
            this.repository.setHasSeenFirstInviteReminder(this.recipient);
        }
    }

    /* loaded from: classes4.dex */
    public static class SecondInviteReminderInfo extends ReminderInfo {
        private final Recipient recipient;
        private final Repository repository;

        private SecondInviteReminderInfo(Context context, Recipient recipient, Repository repository, int i) {
            super(new SecondInviteReminder(context, recipient, i));
            this.repository = repository;
            this.recipient = recipient;
        }

        @Override // org.thoughtcrime.securesms.invites.InviteReminderModel.ReminderInfo
        public void dismiss() {
            this.repository.setHasSeenSecondInviteReminder(this.recipient);
        }
    }
}
