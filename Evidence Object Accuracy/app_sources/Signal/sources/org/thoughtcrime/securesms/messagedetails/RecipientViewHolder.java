package org.thoughtcrime.securesms.messagedetails;

import android.view.View;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Locale;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.components.FromTextView;
import org.thoughtcrime.securesms.messagedetails.MessageDetailsAdapter;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public final class RecipientViewHolder extends RecyclerView.ViewHolder {
    private final AvatarImageView avatar;
    private final BadgeImageView badge;
    private MessageDetailsAdapter.Callbacks callbacks;
    private final View conflictButton;
    private final TextView error;
    private final FromTextView fromView;
    private final TextView timestamp;
    private final View unidentifiedDeliveryIcon;

    public RecipientViewHolder(View view, MessageDetailsAdapter.Callbacks callbacks) {
        super(view);
        this.callbacks = callbacks;
        this.fromView = (FromTextView) view.findViewById(R.id.message_details_recipient_name);
        this.avatar = (AvatarImageView) view.findViewById(R.id.message_details_recipient_avatar);
        this.timestamp = (TextView) view.findViewById(R.id.message_details_recipient_timestamp);
        this.error = (TextView) view.findViewById(R.id.message_details_recipient_error_description);
        this.conflictButton = view.findViewById(R.id.message_details_recipient_conflict_button);
        this.unidentifiedDeliveryIcon = view.findViewById(R.id.message_details_recipient_ud_indicator);
        this.badge = (BadgeImageView) view.findViewById(R.id.message_details_recipient_badge);
    }

    public void bind(RecipientDeliveryStatus recipientDeliveryStatus) {
        this.unidentifiedDeliveryIcon.setVisibility((!TextSecurePreferences.isShowUnidentifiedDeliveryIndicatorsEnabled(this.itemView.getContext()) || !recipientDeliveryStatus.isUnidentified()) ? 8 : 0);
        this.fromView.setText(recipientDeliveryStatus.getRecipient());
        this.avatar.setRecipient(recipientDeliveryStatus.getRecipient());
        this.badge.setBadgeFromRecipient(recipientDeliveryStatus.getRecipient());
        if (recipientDeliveryStatus.getKeyMismatchFailure() != null) {
            this.timestamp.setVisibility(8);
            this.error.setVisibility(0);
            this.conflictButton.setVisibility(0);
            this.error.setText(this.itemView.getContext().getString(R.string.message_details_recipient__new_safety_number));
            this.conflictButton.setOnClickListener(new View.OnClickListener(recipientDeliveryStatus) { // from class: org.thoughtcrime.securesms.messagedetails.RecipientViewHolder$$ExternalSyntheticLambda0
                public final /* synthetic */ RecipientDeliveryStatus f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    RecipientViewHolder.this.lambda$bind$0(this.f$1, view);
                }
            });
        } else if ((recipientDeliveryStatus.getNetworkFailure() == null || recipientDeliveryStatus.getMessageRecord().isPending()) && (recipientDeliveryStatus.getMessageRecord().getRecipient().isPushGroup() || !recipientDeliveryStatus.getMessageRecord().isFailed())) {
            this.timestamp.setVisibility(0);
            this.error.setVisibility(8);
            this.conflictButton.setVisibility(8);
            if (recipientDeliveryStatus.getTimestamp() > 0) {
                this.timestamp.setText(DateUtils.getTimeString(this.itemView.getContext(), Locale.getDefault(), recipientDeliveryStatus.getTimestamp()));
            } else {
                this.timestamp.setText("");
            }
        } else {
            this.timestamp.setVisibility(8);
            this.error.setVisibility(0);
            this.conflictButton.setVisibility(8);
            this.error.setText(this.itemView.getContext().getString(R.string.message_details_recipient__failed_to_send));
        }
    }

    public /* synthetic */ void lambda$bind$0(RecipientDeliveryStatus recipientDeliveryStatus, View view) {
        this.callbacks.onErrorClicked(recipientDeliveryStatus.getMessageRecord());
    }
}
