package org.thoughtcrime.securesms.messagedetails;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.FullScreenDialogFragment;
import org.thoughtcrime.securesms.conversation.colors.Colorizer;
import org.thoughtcrime.securesms.conversation.colors.RecyclerViewColorizer;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4PlaybackController;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4ProjectionPlayerHolder;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4ProjectionRecycler;
import org.thoughtcrime.securesms.messagedetails.MessageDetailsAdapter;
import org.thoughtcrime.securesms.messagedetails.MessageDetailsViewModel;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet;
import org.thoughtcrime.securesms.util.Material3OnScrollHelper;

/* loaded from: classes4.dex */
public final class MessageDetailsFragment extends FullScreenDialogFragment {
    private static final String MESSAGE_ID_EXTRA;
    private static final String RECIPIENT_EXTRA;
    private static final String TYPE_EXTRA;
    private MessageDetailsAdapter adapter;
    private Colorizer colorizer;
    private GlideRequests glideRequests;
    private RecyclerViewColorizer recyclerViewColorizer;
    private MessageDetailsViewModel viewModel;

    /* loaded from: classes4.dex */
    public interface Callback {
        void onMessageDetailsFragmentDismissed();
    }

    @Override // org.thoughtcrime.securesms.components.FullScreenDialogFragment
    protected int getDialogLayoutResource() {
        return R.layout.message_details_fragment;
    }

    @Override // org.thoughtcrime.securesms.components.FullScreenDialogFragment
    protected int getTitle() {
        return R.string.AndroidManifest__message_details;
    }

    public static DialogFragment create(MessageRecord messageRecord, RecipientId recipientId) {
        MessageDetailsFragment messageDetailsFragment = new MessageDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putLong("message_id", messageRecord.getId());
        bundle.putString("type", messageRecord.isMms() ? "mms" : "sms");
        bundle.putParcelable("recipient_id", recipientId);
        messageDetailsFragment.setArguments(bundle);
        return messageDetailsFragment;
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.glideRequests = GlideApp.with(this);
        initializeList(view);
        initializeViewModel();
        initializeVideoPlayer(view);
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        if (getActivity() instanceof Callback) {
            ((Callback) getActivity()).onMessageDetailsFragmentDismissed();
        } else if (getParentFragment() instanceof Callback) {
            ((Callback) getParentFragment()).onMessageDetailsFragmentDismissed();
        }
    }

    private void initializeList(View view) {
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.message_details_list);
        View findViewById = view.findViewById(R.id.toolbar_shadow);
        this.colorizer = new Colorizer();
        this.adapter = new MessageDetailsAdapter(getViewLifecycleOwner(), this.glideRequests, this.colorizer, new MessageDetailsAdapter.Callbacks() { // from class: org.thoughtcrime.securesms.messagedetails.MessageDetailsFragment$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.messagedetails.MessageDetailsAdapter.Callbacks
            public final void onErrorClicked(MessageRecord messageRecord) {
                MessageDetailsFragment.this.onErrorClicked(messageRecord);
            }
        });
        this.recyclerViewColorizer = new RecyclerViewColorizer(recyclerView);
        recyclerView.setAdapter(this.adapter);
        recyclerView.setItemAnimator(null);
        new Material3OnScrollHelper(requireActivity(), findViewById).attach(recyclerView);
    }

    private void initializeViewModel() {
        MessageDetailsViewModel messageDetailsViewModel = (MessageDetailsViewModel) ViewModelProviders.of(this, new MessageDetailsViewModel.Factory((RecipientId) requireArguments().getParcelable("recipient_id"), requireArguments().getString("type"), Long.valueOf(requireArguments().getLong("message_id", -1)))).get(MessageDetailsViewModel.class);
        this.viewModel = messageDetailsViewModel;
        messageDetailsViewModel.getMessageDetails().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.messagedetails.MessageDetailsFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                MessageDetailsFragment.this.lambda$initializeViewModel$0((MessageDetails) obj);
            }
        });
        this.viewModel.getRecipient().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.messagedetails.MessageDetailsFragment$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                MessageDetailsFragment.this.lambda$initializeViewModel$1((Recipient) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializeViewModel$0(MessageDetails messageDetails) {
        if (messageDetails == null) {
            dismissAllowingStateLoss();
        } else {
            this.adapter.submitList(convertToRows(messageDetails));
        }
    }

    public /* synthetic */ void lambda$initializeViewModel$1(Recipient recipient) {
        this.recyclerViewColorizer.setChatColors(recipient.getChatColors());
    }

    private void initializeVideoPlayer(View view) {
        GiphyMp4PlaybackController.attach((RecyclerView) view.findViewById(R.id.message_details_list), new GiphyMp4ProjectionRecycler(GiphyMp4ProjectionPlayerHolder.injectVideoViews(requireContext(), getLifecycle(), (FrameLayout) view.findViewById(R.id.video_container), 1)), 1);
    }

    private List<MessageDetailsAdapter.MessageDetailsViewState<?>> convertToRows(MessageDetails messageDetails) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new MessageDetailsAdapter.MessageDetailsViewState<>(messageDetails.getConversationMessage(), 0));
        if (messageDetails.getConversationMessage().getMessageRecord().isOutgoing()) {
            addRecipients(arrayList, RecipientHeader.NOT_SENT, messageDetails.getNotSent());
            addRecipients(arrayList, RecipientHeader.VIEWED, messageDetails.getViewed());
            addRecipients(arrayList, RecipientHeader.READ, messageDetails.getRead());
            addRecipients(arrayList, RecipientHeader.DELIVERED, messageDetails.getDelivered());
            addRecipients(arrayList, RecipientHeader.SENT_TO, messageDetails.getSent());
            addRecipients(arrayList, RecipientHeader.PENDING, messageDetails.getPending());
            addRecipients(arrayList, RecipientHeader.SKIPPED, messageDetails.getSkipped());
        } else {
            addRecipients(arrayList, RecipientHeader.SENT_FROM, messageDetails.getSent());
        }
        return arrayList;
    }

    private boolean addRecipients(List<MessageDetailsAdapter.MessageDetailsViewState<?>> list, RecipientHeader recipientHeader, Collection<RecipientDeliveryStatus> collection) {
        if (collection.isEmpty()) {
            return false;
        }
        list.add(new MessageDetailsAdapter.MessageDetailsViewState<>(recipientHeader, 1));
        for (RecipientDeliveryStatus recipientDeliveryStatus : collection) {
            list.add(new MessageDetailsAdapter.MessageDetailsViewState<>(recipientDeliveryStatus, 2));
        }
        return true;
    }

    public void onErrorClicked(MessageRecord messageRecord) {
        SafetyNumberBottomSheet.forMessageRecord(requireContext(), messageRecord).show(getChildFragmentManager());
    }
}
