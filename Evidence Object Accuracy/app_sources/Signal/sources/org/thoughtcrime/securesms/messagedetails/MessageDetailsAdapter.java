package org.thoughtcrime.securesms.messagedetails;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.conversation.colors.Colorizer;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.mms.GlideRequests;

/* loaded from: classes4.dex */
public final class MessageDetailsAdapter extends ListAdapter<MessageDetailsViewState<?>, RecyclerView.ViewHolder> {
    private final Callbacks callbacks;
    private final Colorizer colorizer;
    private final GlideRequests glideRequests;
    private final LifecycleOwner lifecycleOwner;

    /* loaded from: classes4.dex */
    public interface Callbacks {
        void onErrorClicked(MessageRecord messageRecord);
    }

    public MessageDetailsAdapter(LifecycleOwner lifecycleOwner, GlideRequests glideRequests, Colorizer colorizer, Callbacks callbacks) {
        super(new MessageDetailsDiffer());
        this.lifecycleOwner = lifecycleOwner;
        this.glideRequests = glideRequests;
        this.colorizer = colorizer;
        this.callbacks = callbacks;
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == 0) {
            return new MessageHeaderViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.message_details_header, viewGroup, false), this.glideRequests, this.colorizer);
        }
        if (i == 1) {
            return new RecipientHeaderViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.message_details_recipient_header, viewGroup, false));
        }
        if (i == 2) {
            return new RecipientViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.message_details_recipient, viewGroup, false), this.callbacks);
        }
        throw new AssertionError("unknown view type");
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof MessageHeaderViewHolder) {
            ((MessageHeaderViewHolder) viewHolder).bind(this.lifecycleOwner, (ConversationMessage) ((MessageDetailsViewState) getItem(i)).data);
        } else if (viewHolder instanceof RecipientHeaderViewHolder) {
            ((RecipientHeaderViewHolder) viewHolder).bind((RecipientHeader) ((MessageDetailsViewState) getItem(i)).data);
        } else if (viewHolder instanceof RecipientViewHolder) {
            ((RecipientViewHolder) viewHolder).bind((RecipientDeliveryStatus) ((MessageDetailsViewState) getItem(i)).data);
        } else {
            throw new AssertionError("unknown view holder");
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        return ((MessageDetailsViewState) getItem(i)).itemType;
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public static class MessageDetailsDiffer extends DiffUtil.ItemCallback<MessageDetailsViewState<?>> {
        private MessageDetailsDiffer() {
        }

        public boolean areItemsTheSame(MessageDetailsViewState<?> messageDetailsViewState, MessageDetailsViewState<?> messageDetailsViewState2) {
            Object obj = ((MessageDetailsViewState) messageDetailsViewState).data;
            Object obj2 = ((MessageDetailsViewState) messageDetailsViewState2).data;
            if (obj.getClass() == obj2.getClass() && ((MessageDetailsViewState) messageDetailsViewState).itemType == ((MessageDetailsViewState) messageDetailsViewState2).itemType) {
                int i = ((MessageDetailsViewState) messageDetailsViewState).itemType;
                if (i == 0) {
                    return true;
                }
                if (i != 1) {
                    if (i == 2) {
                        return ((RecipientDeliveryStatus) obj).getRecipient().getId().equals(((RecipientDeliveryStatus) obj2).getRecipient().getId());
                    }
                } else if (obj == obj2) {
                    return true;
                } else {
                    return false;
                }
            }
            return false;
        }

        public boolean areContentsTheSame(MessageDetailsViewState<?> messageDetailsViewState, MessageDetailsViewState<?> messageDetailsViewState2) {
            Object obj = ((MessageDetailsViewState) messageDetailsViewState).data;
            Object obj2 = ((MessageDetailsViewState) messageDetailsViewState2).data;
            if (obj.getClass() == obj2.getClass() && ((MessageDetailsViewState) messageDetailsViewState).itemType == ((MessageDetailsViewState) messageDetailsViewState2).itemType) {
                int i = ((MessageDetailsViewState) messageDetailsViewState).itemType;
                if (i == 1) {
                    return true;
                }
                if (i == 2 && ((RecipientDeliveryStatus) obj).getDeliveryStatus() == ((RecipientDeliveryStatus) obj2).getDeliveryStatus()) {
                    return true;
                }
                return false;
            }
            return false;
        }
    }

    /* loaded from: classes4.dex */
    public static final class MessageDetailsViewState<T> {
        public static final int MESSAGE_HEADER;
        public static final int RECIPIENT;
        public static final int RECIPIENT_HEADER;
        private final T data;
        private int itemType;

        public MessageDetailsViewState(T t, int i) {
            this.data = t;
            this.itemType = i;
        }
    }
}
