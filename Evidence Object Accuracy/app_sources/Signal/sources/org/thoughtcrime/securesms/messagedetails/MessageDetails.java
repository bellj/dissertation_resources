package org.thoughtcrime.securesms.messagedetails;

import com.annimon.stream.ComparatorCompat;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.messagedetails.RecipientDeliveryStatus;

/* loaded from: classes4.dex */
public final class MessageDetails {
    private static final Comparator<RecipientDeliveryStatus> ALPHABETICAL;
    private static final Comparator<RecipientDeliveryStatus> HAS_DISPLAY_NAME;
    private static final Comparator<RecipientDeliveryStatus> RECIPIENT_COMPARATOR;
    private final ConversationMessage conversationMessage;
    private final Collection<RecipientDeliveryStatus> delivered;
    private final Collection<RecipientDeliveryStatus> notSent;
    private final Collection<RecipientDeliveryStatus> pending;
    private final Collection<RecipientDeliveryStatus> read;
    private final Collection<RecipientDeliveryStatus> sent;
    private final Collection<RecipientDeliveryStatus> skipped;
    private final Collection<RecipientDeliveryStatus> viewed;

    static {
        MessageDetails$$ExternalSyntheticLambda0 messageDetails$$ExternalSyntheticLambda0 = new Comparator() { // from class: org.thoughtcrime.securesms.messagedetails.MessageDetails$$ExternalSyntheticLambda0
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return MessageDetails.m2321$r8$lambda$N0HvLprR2zt3ZLWskfFTpGhVzg((RecipientDeliveryStatus) obj, (RecipientDeliveryStatus) obj2);
            }
        };
        HAS_DISPLAY_NAME = messageDetails$$ExternalSyntheticLambda0;
        MessageDetails$$ExternalSyntheticLambda1 messageDetails$$ExternalSyntheticLambda1 = new Comparator() { // from class: org.thoughtcrime.securesms.messagedetails.MessageDetails$$ExternalSyntheticLambda1
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return MessageDetails.$r8$lambda$q16RCtyGCvZmTrSNss8zm2DD8Do((RecipientDeliveryStatus) obj, (RecipientDeliveryStatus) obj2);
            }
        };
        ALPHABETICAL = messageDetails$$ExternalSyntheticLambda1;
        RECIPIENT_COMPARATOR = ComparatorCompat.chain(messageDetails$$ExternalSyntheticLambda0).thenComparing((Comparator) messageDetails$$ExternalSyntheticLambda1);
    }

    public static /* synthetic */ int lambda$static$0(RecipientDeliveryStatus recipientDeliveryStatus, RecipientDeliveryStatus recipientDeliveryStatus2) {
        return Boolean.compare(recipientDeliveryStatus2.getRecipient().hasAUserSetDisplayName(ApplicationDependencies.getApplication()), recipientDeliveryStatus.getRecipient().hasAUserSetDisplayName(ApplicationDependencies.getApplication()));
    }

    public static /* synthetic */ int lambda$static$1(RecipientDeliveryStatus recipientDeliveryStatus, RecipientDeliveryStatus recipientDeliveryStatus2) {
        return recipientDeliveryStatus.getRecipient().getDisplayName(ApplicationDependencies.getApplication()).compareToIgnoreCase(recipientDeliveryStatus2.getRecipient().getDisplayName(ApplicationDependencies.getApplication()));
    }

    public MessageDetails(ConversationMessage conversationMessage, List<RecipientDeliveryStatus> list) {
        this.conversationMessage = conversationMessage;
        Comparator<RecipientDeliveryStatus> comparator = RECIPIENT_COMPARATOR;
        this.pending = new TreeSet(comparator);
        TreeSet treeSet = new TreeSet(comparator);
        this.sent = treeSet;
        this.delivered = new TreeSet(comparator);
        TreeSet treeSet2 = new TreeSet(comparator);
        this.read = treeSet2;
        this.notSent = new TreeSet(comparator);
        this.viewed = new TreeSet(comparator);
        this.skipped = new TreeSet(comparator);
        if (conversationMessage.getMessageRecord().getRecipient().isSelf()) {
            treeSet2.addAll(list);
        } else if (conversationMessage.getMessageRecord().isOutgoing()) {
            for (RecipientDeliveryStatus recipientDeliveryStatus : list) {
                switch (AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$messagedetails$RecipientDeliveryStatus$Status[recipientDeliveryStatus.getDeliveryStatus().ordinal()]) {
                    case 1:
                        this.notSent.add(recipientDeliveryStatus);
                        break;
                    case 2:
                        this.pending.add(recipientDeliveryStatus);
                        break;
                    case 3:
                        this.sent.add(recipientDeliveryStatus);
                        break;
                    case 4:
                        this.delivered.add(recipientDeliveryStatus);
                        break;
                    case 5:
                        this.read.add(recipientDeliveryStatus);
                        break;
                    case 6:
                        this.viewed.add(recipientDeliveryStatus);
                        break;
                    case 7:
                        this.skipped.add(recipientDeliveryStatus);
                        break;
                }
            }
        } else {
            treeSet.addAll(list);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: org.thoughtcrime.securesms.messagedetails.MessageDetails$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$messagedetails$RecipientDeliveryStatus$Status;

        static {
            int[] iArr = new int[RecipientDeliveryStatus.Status.values().length];
            $SwitchMap$org$thoughtcrime$securesms$messagedetails$RecipientDeliveryStatus$Status = iArr;
            try {
                iArr[RecipientDeliveryStatus.Status.UNKNOWN.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagedetails$RecipientDeliveryStatus$Status[RecipientDeliveryStatus.Status.PENDING.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagedetails$RecipientDeliveryStatus$Status[RecipientDeliveryStatus.Status.SENT.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagedetails$RecipientDeliveryStatus$Status[RecipientDeliveryStatus.Status.DELIVERED.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagedetails$RecipientDeliveryStatus$Status[RecipientDeliveryStatus.Status.READ.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagedetails$RecipientDeliveryStatus$Status[RecipientDeliveryStatus.Status.VIEWED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagedetails$RecipientDeliveryStatus$Status[RecipientDeliveryStatus.Status.SKIPPED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    public ConversationMessage getConversationMessage() {
        return this.conversationMessage;
    }

    public Collection<RecipientDeliveryStatus> getPending() {
        return this.pending;
    }

    public Collection<RecipientDeliveryStatus> getSent() {
        return this.sent;
    }

    public Collection<RecipientDeliveryStatus> getSkipped() {
        return this.skipped;
    }

    public Collection<RecipientDeliveryStatus> getDelivered() {
        return this.delivered;
    }

    public Collection<RecipientDeliveryStatus> getRead() {
        return this.read;
    }

    public Collection<RecipientDeliveryStatus> getNotSent() {
        return this.notSent;
    }

    public Collection<RecipientDeliveryStatus> getViewed() {
        return this.viewed;
    }
}
