package org.thoughtcrime.securesms.messagedetails;

import android.view.View;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.DeliveryStatusView;

/* loaded from: classes4.dex */
final class RecipientHeaderViewHolder extends RecyclerView.ViewHolder {
    private final DeliveryStatusView deliveryStatus;
    private final TextView header;

    public RecipientHeaderViewHolder(View view) {
        super(view);
        this.header = (TextView) view.findViewById(R.id.recipient_header_text);
        this.deliveryStatus = (DeliveryStatusView) view.findViewById(R.id.recipient_header_delivery_status);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: org.thoughtcrime.securesms.messagedetails.RecipientHeaderViewHolder$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$messagedetails$RecipientHeader;

        static {
            int[] iArr = new int[RecipientHeader.values().length];
            $SwitchMap$org$thoughtcrime$securesms$messagedetails$RecipientHeader = iArr;
            try {
                iArr[RecipientHeader.PENDING.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagedetails$RecipientHeader[RecipientHeader.SENT_TO.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagedetails$RecipientHeader[RecipientHeader.DELIVERED.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagedetails$RecipientHeader[RecipientHeader.READ.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    public void bind(RecipientHeader recipientHeader) {
        this.header.setText(recipientHeader.getHeaderText());
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$messagedetails$RecipientHeader[recipientHeader.ordinal()];
        if (i == 1) {
            this.deliveryStatus.setPending();
        } else if (i == 2) {
            this.deliveryStatus.setSent();
        } else if (i == 3) {
            this.deliveryStatus.setDelivered();
        } else if (i != 4) {
            this.deliveryStatus.setNone();
        } else {
            this.deliveryStatus.setRead();
        }
    }
}
