package org.thoughtcrime.securesms.messagedetails;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.CountDownTimer;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.TextView;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.exoplayer2.MediaItem;
import j$.util.Optional;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.ConversationItem;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.conversation.colors.Colorizable;
import org.thoughtcrime.securesms.conversation.colors.Colorizer;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4PlaybackPolicyEnforcer;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.ExpirationUtil;
import org.thoughtcrime.securesms.util.Projection;
import org.thoughtcrime.securesms.util.ProjectionList;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public final class MessageHeaderViewHolder extends RecyclerView.ViewHolder implements GiphyMp4Playable, Colorizable {
    private final Colorizer colorizer;
    private ConversationItem conversationItem;
    private final TextView errorText;
    private final TextView expiresIn;
    private CountDownTimer expiresUpdater;
    private final GlideRequests glideRequests;
    private final View messageMetadata;
    private final TextView receivedDate;
    private final ViewStub receivedStub;
    private final View resendButton;
    private final TextView sentDate;
    private final ViewStub sentStub;
    private final TextView transport;
    private final ViewStub updateStub;

    public MessageHeaderViewHolder(View view, GlideRequests glideRequests, Colorizer colorizer) {
        super(view);
        this.glideRequests = glideRequests;
        this.colorizer = colorizer;
        this.sentDate = (TextView) view.findViewById(R.id.message_details_header_sent_time);
        this.receivedDate = (TextView) view.findViewById(R.id.message_details_header_received_time);
        this.expiresIn = (TextView) view.findViewById(R.id.message_details_header_expires_in);
        this.transport = (TextView) view.findViewById(R.id.message_details_header_transport);
        this.errorText = (TextView) view.findViewById(R.id.message_details_header_error_text);
        this.resendButton = view.findViewById(R.id.message_details_header_resend_button);
        this.messageMetadata = view.findViewById(R.id.message_details_header_message_metadata);
        this.updateStub = (ViewStub) view.findViewById(R.id.message_details_header_message_view_update);
        this.sentStub = (ViewStub) view.findViewById(R.id.message_details_header_message_view_sent_multimedia);
        this.receivedStub = (ViewStub) view.findViewById(R.id.message_details_header_message_view_received_multimedia);
    }

    public void bind(LifecycleOwner lifecycleOwner, ConversationMessage conversationMessage) {
        MessageRecord messageRecord = conversationMessage.getMessageRecord();
        bindMessageView(lifecycleOwner, conversationMessage);
        bindErrorState(messageRecord);
        bindSentReceivedDates(messageRecord);
        bindExpirationTime(lifecycleOwner, messageRecord);
        bindTransport(messageRecord);
    }

    private void bindMessageView(LifecycleOwner lifecycleOwner, ConversationMessage conversationMessage) {
        if (this.conversationItem == null) {
            if (conversationMessage.getMessageRecord().isGroupAction()) {
                this.conversationItem = (ConversationItem) this.updateStub.inflate();
            } else if (conversationMessage.getMessageRecord().isOutgoing()) {
                this.conversationItem = (ConversationItem) this.sentStub.inflate();
            } else {
                this.conversationItem = (ConversationItem) this.receivedStub.inflate();
            }
        }
        this.conversationItem.bind(lifecycleOwner, conversationMessage, Optional.empty(), Optional.empty(), this.glideRequests, Locale.getDefault(), new HashSet(), conversationMessage.getMessageRecord().getRecipient(), null, false, false, false, true, this.colorizer, false);
    }

    private void bindErrorState(MessageRecord messageRecord) {
        if (messageRecord.hasFailedWithNetworkFailures()) {
            this.errorText.setVisibility(0);
            this.resendButton.setVisibility(0);
            this.resendButton.setOnClickListener(new View.OnClickListener(messageRecord) { // from class: org.thoughtcrime.securesms.messagedetails.MessageHeaderViewHolder$$ExternalSyntheticLambda2
                public final /* synthetic */ MessageRecord f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    MessageHeaderViewHolder.this.lambda$bindErrorState$1(this.f$1, view);
                }
            });
            this.messageMetadata.setVisibility(8);
        } else if (messageRecord.isFailed()) {
            this.errorText.setVisibility(0);
            this.resendButton.setVisibility(8);
            this.resendButton.setOnClickListener(null);
            this.messageMetadata.setVisibility(8);
        } else {
            this.errorText.setVisibility(8);
            this.resendButton.setVisibility(8);
            this.resendButton.setOnClickListener(null);
            this.messageMetadata.setVisibility(0);
        }
    }

    public /* synthetic */ void lambda$bindErrorState$1(MessageRecord messageRecord, View view) {
        this.resendButton.setOnClickListener(null);
        SignalExecutors.BOUNDED.execute(new Runnable(messageRecord) { // from class: org.thoughtcrime.securesms.messagedetails.MessageHeaderViewHolder$$ExternalSyntheticLambda3
            public final /* synthetic */ MessageRecord f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MessageHeaderViewHolder.this.lambda$bindErrorState$0(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$bindErrorState$0(MessageRecord messageRecord) {
        MessageSender.resend(this.itemView.getContext().getApplicationContext(), messageRecord);
    }

    private void bindSentReceivedDates(MessageRecord messageRecord) {
        this.sentDate.setOnLongClickListener(null);
        this.receivedDate.setOnLongClickListener(null);
        if (messageRecord.isPending() || messageRecord.isFailed()) {
            this.sentDate.setText(formatBoldString(R.string.message_details_header_sent, "-"));
            this.receivedDate.setVisibility(8);
            return;
        }
        SimpleDateFormat detailedDateFormatter = DateUtils.getDetailedDateFormatter(this.itemView.getContext(), Locale.getDefault());
        this.sentDate.setText(formatBoldString(R.string.message_details_header_sent, detailedDateFormatter.format((Date) new java.sql.Date(messageRecord.getDateSent()))));
        this.sentDate.setOnLongClickListener(new View.OnLongClickListener(messageRecord) { // from class: org.thoughtcrime.securesms.messagedetails.MessageHeaderViewHolder$$ExternalSyntheticLambda0
            public final /* synthetic */ MessageRecord f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view) {
                return MessageHeaderViewHolder.this.lambda$bindSentReceivedDates$2(this.f$1, view);
            }
        });
        if (messageRecord.getDateReceived() == messageRecord.getDateSent() || messageRecord.isOutgoing()) {
            this.receivedDate.setVisibility(8);
            return;
        }
        this.receivedDate.setText(formatBoldString(R.string.message_details_header_received, detailedDateFormatter.format((Date) new java.sql.Date(messageRecord.getDateReceived()))));
        this.receivedDate.setOnLongClickListener(new View.OnLongClickListener(messageRecord) { // from class: org.thoughtcrime.securesms.messagedetails.MessageHeaderViewHolder$$ExternalSyntheticLambda1
            public final /* synthetic */ MessageRecord f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view) {
                return MessageHeaderViewHolder.this.lambda$bindSentReceivedDates$3(this.f$1, view);
            }
        });
        this.receivedDate.setVisibility(0);
    }

    public /* synthetic */ boolean lambda$bindSentReceivedDates$2(MessageRecord messageRecord, View view) {
        copyToClipboard(String.valueOf(messageRecord.getDateSent()));
        return true;
    }

    public /* synthetic */ boolean lambda$bindSentReceivedDates$3(MessageRecord messageRecord, View view) {
        copyToClipboard(String.valueOf(messageRecord.getDateReceived()));
        return true;
    }

    private void bindExpirationTime(LifecycleOwner lifecycleOwner, final MessageRecord messageRecord) {
        CountDownTimer countDownTimer = this.expiresUpdater;
        if (countDownTimer != null) {
            countDownTimer.cancel();
            this.expiresUpdater = null;
        }
        if (messageRecord.getExpiresIn() <= 0 || messageRecord.getExpireStarted() <= 0) {
            this.expiresIn.setVisibility(8);
            return;
        }
        this.expiresIn.setVisibility(0);
        lifecycleOwner.getLifecycle().addObserver(new DefaultLifecycleObserver() { // from class: org.thoughtcrime.securesms.messagedetails.MessageHeaderViewHolder.1
            @Override // androidx.lifecycle.FullLifecycleObserver
            public /* bridge */ /* synthetic */ void onCreate(LifecycleOwner lifecycleOwner2) {
                DefaultLifecycleObserver.CC.$default$onCreate(this, lifecycleOwner2);
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public /* bridge */ /* synthetic */ void onDestroy(LifecycleOwner lifecycleOwner2) {
                DefaultLifecycleObserver.CC.$default$onDestroy(this, lifecycleOwner2);
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public /* bridge */ /* synthetic */ void onStart(LifecycleOwner lifecycleOwner2) {
                DefaultLifecycleObserver.CC.$default$onStart(this, lifecycleOwner2);
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public /* bridge */ /* synthetic */ void onStop(LifecycleOwner lifecycleOwner2) {
                DefaultLifecycleObserver.CC.$default$onStop(this, lifecycleOwner2);
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public void onResume(LifecycleOwner lifecycleOwner2) {
                if (MessageHeaderViewHolder.this.expiresUpdater != null) {
                    MessageHeaderViewHolder.this.expiresUpdater.cancel();
                }
                long expiresIn = messageRecord.getExpiresIn() - (System.currentTimeMillis() - messageRecord.getExpireStarted());
                MessageHeaderViewHolder.this.expiresUpdater = new CountDownTimer(expiresIn, (expiresIn < TimeUnit.HOURS.toMillis(1) ? TimeUnit.SECONDS : TimeUnit.MINUTES).toMillis(1)) { // from class: org.thoughtcrime.securesms.messagedetails.MessageHeaderViewHolder.1.1
                    @Override // android.os.CountDownTimer
                    public void onFinish() {
                    }

                    @Override // android.os.CountDownTimer
                    public void onTick(long j) {
                        MessageHeaderViewHolder.this.expiresIn.setText(MessageHeaderViewHolder.this.formatBoldString(R.string.message_details_header_disappears, ExpirationUtil.getExpirationDisplayValue(MessageHeaderViewHolder.this.itemView.getContext(), Math.max((int) TimeUnit.MILLISECONDS.toSeconds(j), 1))));
                    }
                };
                MessageHeaderViewHolder.this.expiresUpdater.start();
            }

            @Override // androidx.lifecycle.FullLifecycleObserver
            public void onPause(LifecycleOwner lifecycleOwner2) {
                if (MessageHeaderViewHolder.this.expiresUpdater != null) {
                    MessageHeaderViewHolder.this.expiresUpdater.cancel();
                    MessageHeaderViewHolder.this.expiresUpdater = null;
                }
            }
        });
    }

    private void bindTransport(MessageRecord messageRecord) {
        String str;
        if (messageRecord.isOutgoing() && messageRecord.isFailed()) {
            str = "-";
        } else if (messageRecord.isPending()) {
            str = this.itemView.getContext().getString(R.string.ConversationFragment_pending);
        } else if (messageRecord.isPush()) {
            str = this.itemView.getContext().getString(R.string.ConversationFragment_push);
        } else if (messageRecord.isMms()) {
            str = this.itemView.getContext().getString(R.string.ConversationFragment_mms);
        } else {
            str = this.itemView.getContext().getString(R.string.ConversationFragment_sms);
        }
        this.transport.setText(formatBoldString(R.string.message_details_header_via, str));
    }

    public CharSequence formatBoldString(int i, CharSequence charSequence) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        StyleSpan styleSpan = new StyleSpan(1);
        String string = this.itemView.getContext().getString(i);
        spannableStringBuilder.append((CharSequence) string).append((CharSequence) " ").append(charSequence);
        spannableStringBuilder.setSpan(styleSpan, 0, string.length(), 17);
        return spannableStringBuilder;
    }

    private void copyToClipboard(String str) {
        ((ClipboardManager) this.itemView.getContext().getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText(DraftDatabase.Draft.TEXT, str));
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public void showProjectionArea() {
        this.conversationItem.showProjectionArea();
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public void hideProjectionArea() {
        this.conversationItem.hideProjectionArea();
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public MediaItem getMediaItem() {
        return this.conversationItem.getMediaItem();
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public GiphyMp4PlaybackPolicyEnforcer getPlaybackPolicyEnforcer() {
        return this.conversationItem.getPlaybackPolicyEnforcer();
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public Projection getGiphyMp4PlayableProjection(ViewGroup viewGroup) {
        return this.conversationItem.getGiphyMp4PlayableProjection(viewGroup);
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public boolean canPlayContent() {
        return this.conversationItem.canPlayContent();
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public boolean shouldProjectContent() {
        return this.conversationItem.shouldProjectContent();
    }

    @Override // org.thoughtcrime.securesms.conversation.colors.Colorizable
    public ProjectionList getColorizerProjections(ViewGroup viewGroup) {
        return this.conversationItem.getColorizerProjections(viewGroup);
    }
}
