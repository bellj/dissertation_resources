package org.thoughtcrime.securesms.messagedetails;

import android.content.Context;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import java.util.LinkedList;
import java.util.List;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.GroupReceiptDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.documents.IdentityKeyMismatch;
import org.thoughtcrime.securesms.database.documents.NetworkFailure;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.messagedetails.RecipientDeliveryStatus;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public final class MessageDetailsRepository {
    private final Context context = ApplicationDependencies.getApplication();

    public LiveData<MessageRecord> getMessageRecord(String str, Long l) {
        return new MessageRecordLiveData(new MessageId(l.longValue(), str.equals("mms")));
    }

    public LiveData<MessageDetails> getMessageDetails(MessageRecord messageRecord) {
        MutableLiveData mutableLiveData = new MutableLiveData();
        if (messageRecord != null) {
            SignalExecutors.BOUNDED.execute(new Runnable(mutableLiveData, messageRecord) { // from class: org.thoughtcrime.securesms.messagedetails.MessageDetailsRepository$$ExternalSyntheticLambda0
                public final /* synthetic */ MutableLiveData f$1;
                public final /* synthetic */ MessageRecord f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    MessageDetailsRepository.$r8$lambda$07ztwGzKkNRkt3S_XYAZRmC5yes(MessageDetailsRepository.this, this.f$1, this.f$2);
                }
            });
        } else {
            mutableLiveData.setValue(null);
        }
        return mutableLiveData;
    }

    public /* synthetic */ void lambda$getMessageDetails$0(MutableLiveData mutableLiveData, MessageRecord messageRecord) {
        mutableLiveData.postValue(getRecipientDeliveryStatusesInternal(messageRecord));
    }

    private MessageDetails getRecipientDeliveryStatusesInternal(MessageRecord messageRecord) {
        LinkedList linkedList = new LinkedList();
        if (!messageRecord.getRecipient().isGroup()) {
            linkedList.add(new RecipientDeliveryStatus(messageRecord, messageRecord.getRecipient(), getStatusFor(messageRecord), messageRecord.isUnidentified(), messageRecord.getReceiptTimestamp(), getNetworkFailure(messageRecord, messageRecord.getRecipient()), getKeyMismatchFailure(messageRecord, messageRecord.getRecipient())));
        } else {
            List<GroupReceiptDatabase.GroupReceiptInfo> groupReceiptInfo = SignalDatabase.groupReceipts().getGroupReceiptInfo(messageRecord.getId());
            if (groupReceiptInfo.isEmpty()) {
                for (Recipient recipient : SignalDatabase.groups().getGroupMembers(messageRecord.getRecipient().requireGroupId(), GroupDatabase.MemberSet.FULL_MEMBERS_EXCLUDING_SELF)) {
                    linkedList.add(new RecipientDeliveryStatus(messageRecord, recipient, RecipientDeliveryStatus.Status.UNKNOWN, false, messageRecord.getReceiptTimestamp(), getNetworkFailure(messageRecord, recipient), getKeyMismatchFailure(messageRecord, recipient)));
                }
            } else {
                for (GroupReceiptDatabase.GroupReceiptInfo groupReceiptInfo2 : groupReceiptInfo) {
                    Recipient resolved = Recipient.resolved(groupReceiptInfo2.getRecipientId());
                    NetworkFailure networkFailure = getNetworkFailure(messageRecord, resolved);
                    IdentityKeyMismatch keyMismatchFailure = getKeyMismatchFailure(messageRecord, resolved);
                    linkedList.add(new RecipientDeliveryStatus(messageRecord, resolved, getStatusFor(groupReceiptInfo2.getStatus(), messageRecord.isPending(), (networkFailure == null && keyMismatchFailure == null) ? false : true), groupReceiptInfo2.isUnidentified(), groupReceiptInfo2.getTimestamp(), networkFailure, keyMismatchFailure));
                }
            }
        }
        return new MessageDetails(ConversationMessage.ConversationMessageFactory.createWithUnresolvedData(this.context, messageRecord), linkedList);
    }

    private NetworkFailure getNetworkFailure(MessageRecord messageRecord, Recipient recipient) {
        if (!messageRecord.hasNetworkFailures()) {
            return null;
        }
        for (NetworkFailure networkFailure : messageRecord.getNetworkFailures()) {
            if (networkFailure.getRecipientId(this.context).equals(recipient.getId())) {
                return networkFailure;
            }
        }
        return null;
    }

    private IdentityKeyMismatch getKeyMismatchFailure(MessageRecord messageRecord, Recipient recipient) {
        if (!messageRecord.isIdentityMismatchFailure()) {
            return null;
        }
        for (IdentityKeyMismatch identityKeyMismatch : messageRecord.getIdentityKeyMismatches()) {
            if (identityKeyMismatch.getRecipientId(this.context).equals(recipient.getId())) {
                return identityKeyMismatch;
            }
        }
        return null;
    }

    private RecipientDeliveryStatus.Status getStatusFor(MessageRecord messageRecord) {
        if (messageRecord.isRemoteViewed()) {
            return RecipientDeliveryStatus.Status.VIEWED;
        }
        if (messageRecord.isRemoteRead()) {
            return RecipientDeliveryStatus.Status.READ;
        }
        if (messageRecord.isDelivered()) {
            return RecipientDeliveryStatus.Status.DELIVERED;
        }
        if (messageRecord.isSent()) {
            return RecipientDeliveryStatus.Status.SENT;
        }
        if (messageRecord.isPending()) {
            return RecipientDeliveryStatus.Status.PENDING;
        }
        return RecipientDeliveryStatus.Status.UNKNOWN;
    }

    private RecipientDeliveryStatus.Status getStatusFor(int i, boolean z, boolean z2) {
        if (i == 2) {
            return RecipientDeliveryStatus.Status.READ;
        }
        if (i == 1) {
            return RecipientDeliveryStatus.Status.DELIVERED;
        }
        if (i == 0 && z2) {
            return RecipientDeliveryStatus.Status.UNKNOWN;
        }
        if (i == 0 && !z) {
            return RecipientDeliveryStatus.Status.SENT;
        }
        if (i == 0) {
            return RecipientDeliveryStatus.Status.PENDING;
        }
        if (i == -1) {
            return RecipientDeliveryStatus.Status.UNKNOWN;
        }
        if (i == 3) {
            return RecipientDeliveryStatus.Status.VIEWED;
        }
        if (i == 4) {
            return RecipientDeliveryStatus.Status.SKIPPED;
        }
        throw new AssertionError();
    }
}
