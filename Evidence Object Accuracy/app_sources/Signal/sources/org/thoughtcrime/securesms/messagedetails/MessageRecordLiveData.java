package org.thoughtcrime.securesms.messagedetails;

import androidx.lifecycle.LiveData;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.NoSuchMessageException;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;

/* loaded from: classes4.dex */
public final class MessageRecordLiveData extends LiveData<MessageRecord> {
    private final MessageId messageId;
    private final DatabaseObserver.Observer observer = new DatabaseObserver.Observer() { // from class: org.thoughtcrime.securesms.messagedetails.MessageRecordLiveData$$ExternalSyntheticLambda0
        @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
        public final void onChanged() {
            MessageRecordLiveData.this.retrieveMessageRecordActual();
        }
    };

    public MessageRecordLiveData(MessageId messageId) {
        this.messageId = messageId;
    }

    @Override // androidx.lifecycle.LiveData
    public void onActive() {
        SignalExecutors.BOUNDED_IO.execute(new Runnable() { // from class: org.thoughtcrime.securesms.messagedetails.MessageRecordLiveData$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                MessageRecordLiveData.this.retrieveMessageRecordActual();
            }
        });
    }

    @Override // androidx.lifecycle.LiveData
    public void onInactive() {
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(this.observer);
    }

    public synchronized void retrieveMessageRecordActual() {
        retrieve(this.messageId.isMms() ? SignalDatabase.mms() : SignalDatabase.sms());
    }

    private synchronized void retrieve(MessageDatabase messageDatabase) {
        try {
            MessageRecord messageRecord = messageDatabase.getMessageRecord(this.messageId.getId());
            postValue(messageRecord);
            ApplicationDependencies.getDatabaseObserver().registerVerboseConversationObserver(messageRecord.getThreadId(), this.observer);
        } catch (NoSuchMessageException unused) {
            postValue(null);
        }
    }
}
