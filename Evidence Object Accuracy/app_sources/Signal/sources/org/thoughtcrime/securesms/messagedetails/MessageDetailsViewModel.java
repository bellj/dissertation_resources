package org.thoughtcrime.securesms.messagedetails;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import java.util.Objects;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public final class MessageDetailsViewModel extends ViewModel {
    private final LiveData<MessageDetails> messageDetails;
    private final LiveData<Recipient> recipient;

    private MessageDetailsViewModel(RecipientId recipientId, String str, Long l) {
        this.recipient = Recipient.live(recipientId).getLiveData();
        MessageDetailsRepository messageDetailsRepository = new MessageDetailsRepository();
        this.messageDetails = Transformations.switchMap(messageDetailsRepository.getMessageRecord(str, l), new Function() { // from class: org.thoughtcrime.securesms.messagedetails.MessageDetailsViewModel$$ExternalSyntheticLambda0
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return MessageDetailsRepository.this.getMessageDetails((MessageRecord) obj);
            }
        });
    }

    public LiveData<MessageDetails> getMessageDetails() {
        return this.messageDetails;
    }

    public LiveData<Recipient> getRecipient() {
        return this.recipient;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final Long messageId;
        private final RecipientId recipientId;
        private final String type;

        public Factory(RecipientId recipientId, String str, Long l) {
            this.recipientId = recipientId;
            this.type = str;
            this.messageId = l;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            T cast = cls.cast(new MessageDetailsViewModel(this.recipientId, this.type, this.messageId));
            Objects.requireNonNull(cast);
            return cast;
        }
    }
}
