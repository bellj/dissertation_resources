package org.thoughtcrime.securesms.messagedetails;

import org.thoughtcrime.securesms.database.documents.IdentityKeyMismatch;
import org.thoughtcrime.securesms.database.documents.NetworkFailure;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public final class RecipientDeliveryStatus {
    private final Status deliveryStatus;
    private final boolean isUnidentified;
    private final IdentityKeyMismatch keyMismatchFailure;
    private final MessageRecord messageRecord;
    private final NetworkFailure networkFailure;
    private final Recipient recipient;
    private final long timestamp;

    /* access modifiers changed from: package-private */
    /* loaded from: classes4.dex */
    public enum Status {
        UNKNOWN,
        PENDING,
        SENT,
        DELIVERED,
        READ,
        VIEWED,
        SKIPPED
    }

    public RecipientDeliveryStatus(MessageRecord messageRecord, Recipient recipient, Status status, boolean z, long j, NetworkFailure networkFailure, IdentityKeyMismatch identityKeyMismatch) {
        this.messageRecord = messageRecord;
        this.recipient = recipient;
        this.deliveryStatus = status;
        this.isUnidentified = z;
        this.timestamp = j;
        this.networkFailure = networkFailure;
        this.keyMismatchFailure = identityKeyMismatch;
    }

    public MessageRecord getMessageRecord() {
        return this.messageRecord;
    }

    public Status getDeliveryStatus() {
        return this.deliveryStatus;
    }

    public boolean isUnidentified() {
        return this.isUnidentified;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public Recipient getRecipient() {
        return this.recipient;
    }

    public NetworkFailure getNetworkFailure() {
        return this.networkFailure;
    }

    public IdentityKeyMismatch getKeyMismatchFailure() {
        return this.keyMismatchFailure;
    }
}
