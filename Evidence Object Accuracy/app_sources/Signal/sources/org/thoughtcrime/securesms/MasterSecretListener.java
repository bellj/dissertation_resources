package org.thoughtcrime.securesms;

/* loaded from: classes.dex */
public interface MasterSecretListener {
    void onMasterSecretCleared();
}
