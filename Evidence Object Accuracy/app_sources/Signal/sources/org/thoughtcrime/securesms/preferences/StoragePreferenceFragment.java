package org.thoughtcrime.securesms.preferences;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.preference.Preference;
import com.annimon.stream.Stream;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.text.NumberFormat;
import org.signal.core.util.StringUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.BaseSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.BaseSettingsFragment;
import org.thoughtcrime.securesms.components.settings.CustomizableSingleSelectSetting;
import org.thoughtcrime.securesms.components.settings.SingleSelectSetting;
import org.thoughtcrime.securesms.components.settings.app.wrapped.SettingsWrapperFragment;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.KeepMessagesDuration;
import org.thoughtcrime.securesms.keyvalue.SettingsValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mediaoverview.MediaOverviewActivity;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.preferences.widgets.StorageGraphView;
import org.thoughtcrime.securesms.preferences.widgets.StoragePreferenceCategory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;

/* loaded from: classes4.dex */
public class StoragePreferenceFragment extends ListSummaryPreferenceFragment {
    private Preference keepMessages;
    private Preference trimLength;

    @Override // org.thoughtcrime.securesms.preferences.CorrectedPreferenceFragment, androidx.preference.PreferenceFragmentCompat, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        findPreference("pref_storage_clear_message_history").setOnPreferenceClickListener(new ClearMessageHistoryClickListener());
        Preference findPreference = findPreference(SettingsValues.THREAD_TRIM_LENGTH);
        this.trimLength = findPreference;
        findPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() { // from class: org.thoughtcrime.securesms.preferences.StoragePreferenceFragment$$ExternalSyntheticLambda0
            @Override // androidx.preference.Preference.OnPreferenceClickListener
            public final boolean onPreferenceClick(Preference preference) {
                return StoragePreferenceFragment.this.lambda$onCreate$0(preference);
            }
        });
        Preference findPreference2 = findPreference(SettingsValues.KEEP_MESSAGES_DURATION);
        this.keepMessages = findPreference2;
        findPreference2.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() { // from class: org.thoughtcrime.securesms.preferences.StoragePreferenceFragment$$ExternalSyntheticLambda1
            @Override // androidx.preference.Preference.OnPreferenceClickListener
            public final boolean onPreferenceClick(Preference preference) {
                return StoragePreferenceFragment.this.lambda$onCreate$1(preference);
            }
        });
        StoragePreferenceCategory storagePreferenceCategory = (StoragePreferenceCategory) findPreference("pref_storage_category");
        FragmentActivity requireActivity = requireActivity();
        ApplicationPreferencesViewModel applicationPreferencesViewModel = ApplicationPreferencesViewModel.getApplicationPreferencesViewModel(requireActivity);
        storagePreferenceCategory.setOnFreeUpSpace(new Runnable() { // from class: org.thoughtcrime.securesms.preferences.StoragePreferenceFragment$$ExternalSyntheticLambda2
            @Override // java.lang.Runnable
            public final void run() {
                StoragePreferenceFragment.lambda$onCreate$2(FragmentActivity.this);
            }
        });
        applicationPreferencesViewModel.getStorageBreakdown().observe(requireActivity, new Observer() { // from class: org.thoughtcrime.securesms.preferences.StoragePreferenceFragment$$ExternalSyntheticLambda3
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                StoragePreferenceCategory.this.setStorage((StorageGraphView.StorageBreakdown) obj);
            }
        });
    }

    public /* synthetic */ boolean lambda$onCreate$0(Preference preference) {
        updateToolbarTitle(R.string.preferences__conversation_length_limit);
        pushFragment(BaseSettingsFragment.create(new ConversationLengthLimitConfiguration()));
        return true;
    }

    public /* synthetic */ boolean lambda$onCreate$1(Preference preference) {
        updateToolbarTitle(R.string.preferences__keep_messages);
        pushFragment(BaseSettingsFragment.create(new KeepMessagesConfiguration()));
        return true;
    }

    public static /* synthetic */ void lambda$onCreate$2(FragmentActivity fragmentActivity) {
        fragmentActivity.startActivity(MediaOverviewActivity.forAll(fragmentActivity));
    }

    @Override // androidx.preference.PreferenceFragmentCompat
    public void onCreatePreferences(Bundle bundle, String str) {
        addPreferencesFromResource(R.xml.preferences_storage);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        String str;
        super.onResume();
        updateToolbarTitle(R.string.preferences__storage);
        FragmentActivity requireActivity = requireActivity();
        ApplicationPreferencesViewModel.getApplicationPreferencesViewModel(requireActivity).refreshStorageBreakdown(requireActivity.getApplicationContext());
        this.keepMessages.setSummary(SignalStore.settings().getKeepMessagesDuration().getStringResource());
        Preference preference = this.trimLength;
        if (SignalStore.settings().isTrimByLengthEnabled()) {
            str = getString(R.string.preferences_storage__s_messages, NumberFormat.getInstance().format((long) SignalStore.settings().getThreadTrimLength()));
        } else {
            str = getString(R.string.preferences_storage__none);
        }
        preference.setSummary(str);
    }

    @Override // androidx.fragment.app.Fragment
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        Permissions.onRequestPermissionsResult(this, i, strArr, iArr);
    }

    private void updateToolbarTitle(int i) {
        if (getParentFragment() instanceof SettingsWrapperFragment) {
            ((SettingsWrapperFragment) getParentFragment()).setTitle(i);
        }
    }

    private void pushFragment(Fragment fragment) {
        getParentFragmentManager().beginTransaction().replace(R.id.wrapped_fragment, fragment).addToBackStack(null).commit();
    }

    /* loaded from: classes4.dex */
    public class ClearMessageHistoryClickListener implements Preference.OnPreferenceClickListener {
        private ClearMessageHistoryClickListener() {
            StoragePreferenceFragment.this = r1;
        }

        @Override // androidx.preference.Preference.OnPreferenceClickListener
        public boolean onPreferenceClick(Preference preference) {
            new MaterialAlertDialogBuilder(StoragePreferenceFragment.this.requireActivity()).setTitle(R.string.preferences_storage__clear_message_history).setMessage(R.string.preferences_storage__this_will_delete_all_message_history_and_media_from_your_device).setPositiveButton(R.string.delete, (DialogInterface.OnClickListener) new StoragePreferenceFragment$ClearMessageHistoryClickListener$$ExternalSyntheticLambda2(this)).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).show();
            return true;
        }

        public /* synthetic */ void lambda$onPreferenceClick$0(DialogInterface dialogInterface, int i) {
            showAreYouReallySure();
        }

        private void showAreYouReallySure() {
            new MaterialAlertDialogBuilder(StoragePreferenceFragment.this.requireActivity()).setTitle(R.string.preferences_storage__are_you_sure_you_want_to_delete_all_message_history).setMessage(R.string.preferences_storage__all_message_history_will_be_permanently_removed_this_action_cannot_be_undone).setPositiveButton(R.string.preferences_storage__delete_all_now, (DialogInterface.OnClickListener) new StoragePreferenceFragment$ClearMessageHistoryClickListener$$ExternalSyntheticLambda0()).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).show();
        }

        public static /* synthetic */ void lambda$showAreYouReallySure$1() {
            SignalDatabase.threads().deleteAllConversations();
        }

        public static /* synthetic */ void lambda$showAreYouReallySure$2(DialogInterface dialogInterface, int i) {
            SignalExecutors.BOUNDED.execute(new StoragePreferenceFragment$ClearMessageHistoryClickListener$$ExternalSyntheticLambda1());
        }
    }

    /* loaded from: classes4.dex */
    public static class KeepMessagesConfiguration extends BaseSettingsFragment.Configuration implements SingleSelectSetting.SingleSelectSelectionChangedListener {
        @Override // org.thoughtcrime.securesms.components.settings.BaseSettingsFragment.Configuration
        public void configureAdapter(BaseSettingsAdapter baseSettingsAdapter) {
            baseSettingsAdapter.configureSingleSelect(this);
        }

        @Override // org.thoughtcrime.securesms.components.settings.BaseSettingsFragment.Configuration
        public MappingModelList getSettings() {
            return (MappingModelList) Stream.of(KeepMessagesDuration.values()).map(new StoragePreferenceFragment$KeepMessagesConfiguration$$ExternalSyntheticLambda1(this, SignalStore.settings().getKeepMessagesDuration())).collect(MappingModelList.toMappingModelList());
        }

        public /* synthetic */ SingleSelectSetting.Item lambda$getSettings$0(KeepMessagesDuration keepMessagesDuration, KeepMessagesDuration keepMessagesDuration2) {
            return new SingleSelectSetting.Item(keepMessagesDuration2, this.activity.getString(keepMessagesDuration2.getStringResource()), null, keepMessagesDuration2.equals(keepMessagesDuration));
        }

        @Override // org.thoughtcrime.securesms.components.settings.SingleSelectSetting.SingleSelectSelectionChangedListener
        public void onSelectionChanged(Object obj) {
            KeepMessagesDuration keepMessagesDuration = (KeepMessagesDuration) obj;
            if (keepMessagesDuration.ordinal() > SignalStore.settings().getKeepMessagesDuration().ordinal()) {
                MaterialAlertDialogBuilder title = new MaterialAlertDialogBuilder(this.activity).setTitle(R.string.preferences_storage__delete_older_messages);
                FragmentActivity fragmentActivity = this.activity;
                title.setMessage((CharSequence) fragmentActivity.getString(R.string.preferences_storage__this_will_permanently_delete_all_message_history_and_media, new Object[]{fragmentActivity.getString(keepMessagesDuration.getStringResource())})).setPositiveButton(R.string.delete, (DialogInterface.OnClickListener) new StoragePreferenceFragment$KeepMessagesConfiguration$$ExternalSyntheticLambda0(this, keepMessagesDuration)).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).show();
                return;
            }
            updateTrimByTime(keepMessagesDuration);
        }

        public /* synthetic */ void lambda$onSelectionChanged$1(KeepMessagesDuration keepMessagesDuration, DialogInterface dialogInterface, int i) {
            updateTrimByTime(keepMessagesDuration);
        }

        private void updateTrimByTime(KeepMessagesDuration keepMessagesDuration) {
            SignalStore.settings().setKeepMessagesForDuration(keepMessagesDuration);
            updateSettingsList();
            ApplicationDependencies.getTrimThreadsByDateManager().scheduleIfNecessary();
        }
    }

    /* loaded from: classes4.dex */
    public static class ConversationLengthLimitConfiguration extends BaseSettingsFragment.Configuration implements CustomizableSingleSelectSetting.CustomizableSingleSelectionListener {
        private static final int CUSTOM_LENGTH;

        @Override // org.thoughtcrime.securesms.components.settings.BaseSettingsFragment.Configuration
        public void configureAdapter(BaseSettingsAdapter baseSettingsAdapter) {
            baseSettingsAdapter.configureSingleSelect(this);
            baseSettingsAdapter.configureCustomizableSingleSelect(this);
        }

        @Override // org.thoughtcrime.securesms.components.settings.BaseSettingsFragment.Configuration
        public MappingModelList getSettings() {
            String str;
            int threadTrimLength = SignalStore.settings().isTrimByLengthEnabled() ? SignalStore.settings().getThreadTrimLength() : 0;
            int[] intArray = this.activity.getResources().getIntArray(R.array.conversation_length_limit);
            MappingModelList mappingModelList = new MappingModelList();
            int length = intArray.length;
            boolean z = false;
            for (int i = 0; i < length; i++) {
                int i2 = intArray[i];
                boolean z2 = i2 == threadTrimLength;
                if (i2 == 0) {
                    str = this.activity.getString(R.string.preferences_storage__none);
                } else {
                    str = this.activity.getString(R.string.preferences_storage__s_messages, new Object[]{NumberFormat.getInstance().format((long) i2)});
                }
                mappingModelList.add(new SingleSelectSetting.Item(Integer.valueOf(i2), str, null, z2));
                z = z || z2;
            }
            int threadTrimLength2 = SignalStore.settings().getThreadTrimLength();
            mappingModelList.add(new CustomizableSingleSelectSetting.Item(-1, this.activity.getString(R.string.preferences_storage__custom), !z, Integer.valueOf(threadTrimLength2), this.activity.getString(R.string.preferences_storage__s_messages, new Object[]{NumberFormat.getInstance().format((long) threadTrimLength2)})));
            return mappingModelList;
        }

        @Override // org.thoughtcrime.securesms.components.settings.CustomizableSingleSelectSetting.CustomizableSingleSelectionListener
        public void onCustomizeClicked(CustomizableSingleSelectSetting.Item item) {
            int threadTrimLength = SignalStore.settings().isTrimByLengthEnabled() ? SignalStore.settings().getThreadTrimLength() : 0;
            View inflate = LayoutInflater.from(this.activity).inflate(R.layout.customizable_setting_edit_text, (ViewGroup) null, false);
            EditText editText = (EditText) inflate.findViewById(R.id.customizable_setting_edit_text);
            if (threadTrimLength > 0) {
                editText.setText(String.valueOf(threadTrimLength));
            }
            AlertDialog create = new MaterialAlertDialogBuilder(this.activity).setTitle(R.string.preferences__conversation_length_limit).setView(inflate).setPositiveButton(17039370, (DialogInterface.OnClickListener) new StoragePreferenceFragment$ConversationLengthLimitConfiguration$$ExternalSyntheticLambda0(this, editText)).setNegativeButton(17039360, (DialogInterface.OnClickListener) new StoragePreferenceFragment$ConversationLengthLimitConfiguration$$ExternalSyntheticLambda1(this)).create();
            create.setOnShowListener(new StoragePreferenceFragment$ConversationLengthLimitConfiguration$$ExternalSyntheticLambda2(this, create, editText));
            create.show();
        }

        public /* synthetic */ void lambda$onCustomizeClicked$0(EditText editText, DialogInterface dialogInterface, int i) {
            onSelectionChanged(Integer.valueOf(Integer.parseInt(editText.getText().toString())));
        }

        public /* synthetic */ void lambda$onCustomizeClicked$1(DialogInterface dialogInterface, int i) {
            updateSettingsList();
        }

        public /* synthetic */ void lambda$onCustomizeClicked$2(final AlertDialog alertDialog, EditText editText, DialogInterface dialogInterface) {
            alertDialog.getButton(-1).setEnabled(!TextUtils.isEmpty(editText.getText()));
            editText.requestFocus();
            editText.addTextChangedListener(new TextWatcher() { // from class: org.thoughtcrime.securesms.preferences.StoragePreferenceFragment.ConversationLengthLimitConfiguration.1
                @Override // android.text.TextWatcher
                public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                }

                @Override // android.text.TextWatcher
                public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                }

                @Override // android.text.TextWatcher
                public void afterTextChanged(Editable editable) {
                    CharSequence trimSequence = StringUtil.trimSequence(editable);
                    if (TextUtils.isEmpty(trimSequence)) {
                        editable.replace(0, editable.length(), "");
                    } else {
                        try {
                            Integer.parseInt(trimSequence.toString());
                            alertDialog.getButton(-1).setEnabled(true);
                            return;
                        } catch (NumberFormatException unused) {
                            String replaceAll = trimSequence.toString().replaceAll("[^\\d]", "");
                            if (!replaceAll.equals(trimSequence.toString())) {
                                editable.replace(0, editable.length(), replaceAll);
                            }
                        }
                    }
                    alertDialog.getButton(-1).setEnabled(false);
                }
            });
        }

        @Override // org.thoughtcrime.securesms.components.settings.SingleSelectSetting.SingleSelectSelectionChangedListener
        public void onSelectionChanged(Object obj) {
            boolean isTrimByLengthEnabled = SignalStore.settings().isTrimByLengthEnabled();
            int threadTrimLength = isTrimByLengthEnabled ? SignalStore.settings().getThreadTrimLength() : 0;
            int intValue = ((Integer) obj).intValue();
            if (intValue > 0 && (!isTrimByLengthEnabled || intValue < threadTrimLength)) {
                new MaterialAlertDialogBuilder(this.activity).setTitle(R.string.preferences_storage__delete_older_messages).setMessage((CharSequence) this.activity.getString(R.string.preferences_storage__this_will_permanently_trim_all_conversations_to_the_d_most_recent_messages, new Object[]{NumberFormat.getInstance().format((long) intValue)})).setPositiveButton(R.string.delete, (DialogInterface.OnClickListener) new StoragePreferenceFragment$ConversationLengthLimitConfiguration$$ExternalSyntheticLambda4(this, intValue)).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).show();
            } else if (intValue == -1) {
                onCustomizeClicked(null);
            } else {
                updateTrimByLength(intValue);
            }
        }

        public /* synthetic */ void lambda$onSelectionChanged$3(int i, DialogInterface dialogInterface, int i2) {
            updateTrimByLength(i);
        }

        private void updateTrimByLength(int i) {
            boolean z = false;
            boolean z2 = !SignalStore.settings().isTrimByLengthEnabled() || i < SignalStore.settings().getThreadTrimLength();
            SettingsValues settingsValues = SignalStore.settings();
            if (i > 0) {
                z = true;
            }
            settingsValues.setThreadTrimByLengthEnabled(z);
            SignalStore.settings().setThreadTrimLength(i);
            updateSettingsList();
            if (SignalStore.settings().isTrimByLengthEnabled() && z2) {
                KeepMessagesDuration keepMessagesDuration = SignalStore.settings().getKeepMessagesDuration();
                SignalExecutors.BOUNDED.execute(new StoragePreferenceFragment$ConversationLengthLimitConfiguration$$ExternalSyntheticLambda3(i, keepMessagesDuration != KeepMessagesDuration.FOREVER ? System.currentTimeMillis() - keepMessagesDuration.getDuration() : 0));
            }
        }

        public static /* synthetic */ void lambda$updateTrimByLength$4(int i, long j) {
            SignalDatabase.threads().trimAllThreads(i, j);
        }
    }
}
