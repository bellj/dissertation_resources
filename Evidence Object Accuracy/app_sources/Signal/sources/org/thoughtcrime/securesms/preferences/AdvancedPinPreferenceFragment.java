package org.thoughtcrime.securesms.preferences;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.preference.Preference;
import com.google.android.material.snackbar.Snackbar;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.lock.v2.CreateKbsPinActivity;
import org.thoughtcrime.securesms.payments.backup.PaymentsRecoveryStartFragmentArgs;
import org.thoughtcrime.securesms.payments.preferences.PaymentsActivity;
import org.thoughtcrime.securesms.pin.PinOptOutDialog;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class AdvancedPinPreferenceFragment extends ListSummaryPreferenceFragment {
    private static final String PREF_DISABLE;
    private static final String PREF_ENABLE;

    @Override // org.thoughtcrime.securesms.preferences.CorrectedPreferenceFragment, androidx.preference.PreferenceFragmentCompat, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override // androidx.preference.PreferenceFragmentCompat
    public void onCreatePreferences(Bundle bundle, String str) {
        addPreferencesFromResource(R.xml.preferences_advanced_pin);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        updatePreferenceState();
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 27698 && i2 == -1) {
            Snackbar.make(requireView(), (int) R.string.ApplicationPreferencesActivity_pin_created, 0).show();
        }
    }

    private void updatePreferenceState() {
        Preference findPreference = findPreference(PREF_ENABLE);
        Preference findPreference2 = findPreference(PREF_DISABLE);
        if (SignalStore.kbsValues().hasOptedOut()) {
            findPreference.setVisible(true);
            findPreference2.setVisible(false);
            findPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() { // from class: org.thoughtcrime.securesms.preferences.AdvancedPinPreferenceFragment$$ExternalSyntheticLambda4
                @Override // androidx.preference.Preference.OnPreferenceClickListener
                public final boolean onPreferenceClick(Preference preference) {
                    return AdvancedPinPreferenceFragment.this.lambda$updatePreferenceState$0(preference);
                }
            });
            return;
        }
        findPreference.setVisible(false);
        findPreference2.setVisible(true);
        findPreference2.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() { // from class: org.thoughtcrime.securesms.preferences.AdvancedPinPreferenceFragment$$ExternalSyntheticLambda5
            @Override // androidx.preference.Preference.OnPreferenceClickListener
            public final boolean onPreferenceClick(Preference preference) {
                return AdvancedPinPreferenceFragment.this.lambda$updatePreferenceState$1(preference);
            }
        });
    }

    public /* synthetic */ boolean lambda$updatePreferenceState$0(Preference preference) {
        onPreferenceChanged(true);
        return true;
    }

    public /* synthetic */ boolean lambda$updatePreferenceState$1(Preference preference) {
        onPreferenceChanged(false);
        return true;
    }

    private void onPreferenceChanged(boolean z) {
        boolean z2 = TextSecurePreferences.isV1RegistrationLockEnabled(requireContext()) || SignalStore.kbsValues().isV2RegistrationLockEnabled();
        if (!z && z2) {
            new AlertDialog.Builder(requireContext()).setMessage(R.string.ApplicationPreferencesActivity_pins_are_required_for_registration_lock).setCancelable(true).setPositiveButton(17039370, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.preferences.AdvancedPinPreferenceFragment$$ExternalSyntheticLambda0
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).show();
        } else if (!z && SignalStore.paymentsValues().mobileCoinPaymentsEnabled() && !SignalStore.paymentsValues().userConfirmedMnemonic()) {
            new AlertDialog.Builder(requireContext()).setTitle(R.string.ApplicationPreferencesActivity_record_payments_recovery_phrase).setMessage(R.string.ApplicationPreferencesActivity_before_you_can_disable_your_pin).setPositiveButton(R.string.ApplicationPreferencesActivity_record_phrase, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.preferences.AdvancedPinPreferenceFragment$$ExternalSyntheticLambda1
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    AdvancedPinPreferenceFragment.this.lambda$onPreferenceChanged$3(dialogInterface, i);
                }
            }).setNegativeButton(17039360, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.preferences.AdvancedPinPreferenceFragment$$ExternalSyntheticLambda2
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).setCancelable(true).show();
        } else if (!z) {
            PinOptOutDialog.show(requireContext(), new Runnable() { // from class: org.thoughtcrime.securesms.preferences.AdvancedPinPreferenceFragment$$ExternalSyntheticLambda3
                @Override // java.lang.Runnable
                public final void run() {
                    AdvancedPinPreferenceFragment.this.lambda$onPreferenceChanged$5();
                }
            });
        } else {
            startActivityForResult(CreateKbsPinActivity.getIntentForPinCreate(requireContext()), 27698);
        }
    }

    public /* synthetic */ void lambda$onPreferenceChanged$3(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent(requireContext(), PaymentsActivity.class);
        intent.putExtra(PaymentsActivity.EXTRA_PAYMENTS_STARTING_ACTION, R.id.action_directly_to_paymentsBackup);
        intent.putExtra(PaymentsActivity.EXTRA_STARTING_ARGUMENTS, new PaymentsRecoveryStartFragmentArgs.Builder().setFinishOnConfirm(true).build().toBundle());
        startActivity(intent);
        dialogInterface.dismiss();
    }

    public /* synthetic */ void lambda$onPreferenceChanged$5() {
        updatePreferenceState();
        Snackbar.make(requireView(), (int) R.string.ApplicationPreferencesActivity_pin_disabled, -1).show();
    }
}
