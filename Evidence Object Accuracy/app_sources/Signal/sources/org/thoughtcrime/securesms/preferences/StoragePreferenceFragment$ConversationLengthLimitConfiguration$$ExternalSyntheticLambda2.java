package org.thoughtcrime.securesms.preferences;

import android.content.DialogInterface;
import android.widget.EditText;
import androidx.appcompat.app.AlertDialog;
import org.thoughtcrime.securesms.preferences.StoragePreferenceFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class StoragePreferenceFragment$ConversationLengthLimitConfiguration$$ExternalSyntheticLambda2 implements DialogInterface.OnShowListener {
    public final /* synthetic */ StoragePreferenceFragment.ConversationLengthLimitConfiguration f$0;
    public final /* synthetic */ AlertDialog f$1;
    public final /* synthetic */ EditText f$2;

    public /* synthetic */ StoragePreferenceFragment$ConversationLengthLimitConfiguration$$ExternalSyntheticLambda2(StoragePreferenceFragment.ConversationLengthLimitConfiguration conversationLengthLimitConfiguration, AlertDialog alertDialog, EditText editText) {
        this.f$0 = conversationLengthLimitConfiguration;
        this.f$1 = alertDialog;
        this.f$2 = editText;
    }

    @Override // android.content.DialogInterface.OnShowListener
    public final void onShow(DialogInterface dialogInterface) {
        this.f$0.lambda$onCustomizeClicked$2(this.f$1, this.f$2, dialogInterface);
    }
}
