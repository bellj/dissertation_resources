package org.thoughtcrime.securesms.preferences.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import androidx.preference.PreferenceCategory;
import androidx.preference.PreferenceViewHolder;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.preferences.widgets.StorageGraphView;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public final class StoragePreferenceCategory extends PreferenceCategory {
    private Runnable onFreeUpSpace;
    private StorageGraphView.StorageBreakdown storage;
    private StorageGraphView storageGraphView;
    private TextView totalSize;

    public StoragePreferenceCategory(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initialize();
    }

    public StoragePreferenceCategory(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initialize();
    }

    public StoragePreferenceCategory(Context context) {
        super(context);
        initialize();
    }

    private void initialize() {
        setLayoutResource(R.layout.preference_storage_category);
    }

    @Override // androidx.preference.PreferenceCategory, androidx.preference.Preference
    public void onBindViewHolder(PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        this.totalSize = (TextView) preferenceViewHolder.findViewById(R.id.total_size);
        this.storageGraphView = (StorageGraphView) preferenceViewHolder.findViewById(R.id.storageGraphView);
        preferenceViewHolder.findViewById(R.id.free_up_space).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.preferences.widgets.StoragePreferenceCategory$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StoragePreferenceCategory.this.lambda$onBindViewHolder$0(view);
            }
        });
        this.totalSize.setText(Util.getPrettyFileSize(0));
        StorageGraphView.StorageBreakdown storageBreakdown = this.storage;
        if (storageBreakdown != null) {
            setStorage(storageBreakdown);
        }
    }

    public /* synthetic */ void lambda$onBindViewHolder$0(View view) {
        Runnable runnable = this.onFreeUpSpace;
        if (runnable != null) {
            runnable.run();
        }
    }

    public void setOnFreeUpSpace(Runnable runnable) {
        this.onFreeUpSpace = runnable;
    }

    public void setStorage(StorageGraphView.StorageBreakdown storageBreakdown) {
        this.storage = storageBreakdown;
        TextView textView = this.totalSize;
        if (textView != null) {
            textView.setText(Util.getPrettyFileSize(storageBreakdown.getTotalSize()));
        }
        StorageGraphView storageGraphView = this.storageGraphView;
        if (storageGraphView != null) {
            storageGraphView.setStorageBreakdown(storageBreakdown);
        }
    }
}
