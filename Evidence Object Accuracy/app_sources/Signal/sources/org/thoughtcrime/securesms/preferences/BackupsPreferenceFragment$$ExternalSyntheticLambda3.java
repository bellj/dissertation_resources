package org.thoughtcrime.securesms.preferences;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class BackupsPreferenceFragment$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ BackupsPreferenceFragment f$0;

    public /* synthetic */ BackupsPreferenceFragment$$ExternalSyntheticLambda3(BackupsPreferenceFragment backupsPreferenceFragment) {
        this.f$0 = backupsPreferenceFragment;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.setBackupsDisabled();
    }
}
