package org.thoughtcrime.securesms.preferences.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import androidx.core.content.ContextCompat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public final class StorageGraphView extends View {
    private StorageBreakdown emptyBreakdown;
    private final Paint paint = new Paint();
    private final Path path = new Path();
    private final RectF rect = new RectF();
    private StorageBreakdown storageBreakdown;

    public StorageGraphView(Context context) {
        super(context);
        initialize();
    }

    public StorageGraphView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initialize();
    }

    public StorageGraphView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initialize();
    }

    private void initialize() {
        setWillNotDraw(false);
        this.paint.setStyle(Paint.Style.FILL);
        StorageBreakdown storageBreakdown = new StorageBreakdown(Collections.singletonList(new Entry(ContextCompat.getColor(getContext(), R.color.storage_color_empty), 1)));
        this.emptyBreakdown = storageBreakdown;
        setStorageBreakdown(storageBreakdown);
    }

    public void setStorageBreakdown(StorageBreakdown storageBreakdown) {
        if (storageBreakdown.totalSize == 0) {
            storageBreakdown = this.emptyBreakdown;
        }
        this.storageBreakdown = storageBreakdown;
        invalidate();
    }

    @Override // android.view.View
    protected void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.rect.set(0.0f, 0.0f, (float) i, (float) i2);
        this.path.reset();
        float height = (float) (getHeight() / 2);
        this.path.addRoundRect(this.rect, height, height, Path.Direction.CW);
    }

    @Override // android.view.View
    protected void onDraw(Canvas canvas) {
        if (this.storageBreakdown.totalSize != 0) {
            canvas.clipPath(this.path);
            int size = this.storageBreakdown.entries.size();
            int width = getWidth();
            int i = 0;
            int i2 = 0;
            while (i < size) {
                Entry entry = (Entry) this.storageBreakdown.entries.get(i);
                int i3 = i < size + -1 ? ((int) ((((long) width) * entry.size) / this.storageBreakdown.totalSize)) + i2 : width;
                RectF rectF = this.rect;
                rectF.left = (float) i2;
                rectF.right = (float) i3;
                this.paint.setColor(entry.color);
                canvas.drawRect(this.rect, this.paint);
                i++;
                i2 = i3;
            }
        }
    }

    /* loaded from: classes4.dex */
    public static class StorageBreakdown {
        private final List<Entry> entries;
        private final long totalSize;

        public StorageBreakdown(List<Entry> list) {
            this.entries = new ArrayList(list);
            long j = 0;
            for (Entry entry : list) {
                j += entry.size;
            }
            this.totalSize = j;
        }

        public long getTotalSize() {
            return this.totalSize;
        }
    }

    /* loaded from: classes4.dex */
    public static class Entry {
        private final int color;
        private final long size;

        public Entry(int i, long j) {
            this.color = i;
            this.size = j;
        }
    }
}
