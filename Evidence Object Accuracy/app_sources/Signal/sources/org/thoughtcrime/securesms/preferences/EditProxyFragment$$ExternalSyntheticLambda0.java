package org.thoughtcrime.securesms.preferences;

import j$.util.function.Function;
import org.whispersystems.signalservice.internal.configuration.SignalProxy;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class EditProxyFragment$$ExternalSyntheticLambda0 implements Function {
    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return ((SignalProxy) obj).getHost();
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
