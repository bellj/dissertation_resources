package org.thoughtcrime.securesms.preferences;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;
import java.util.Arrays;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.MediaDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.preferences.widgets.StorageGraphView;

/* loaded from: classes4.dex */
public class ApplicationPreferencesViewModel extends ViewModel {
    private final MutableLiveData<StorageGraphView.StorageBreakdown> storageBreakdown = new MutableLiveData<>();

    public LiveData<StorageGraphView.StorageBreakdown> getStorageBreakdown() {
        return this.storageBreakdown;
    }

    public static ApplicationPreferencesViewModel getApplicationPreferencesViewModel(FragmentActivity fragmentActivity) {
        return (ApplicationPreferencesViewModel) ViewModelProviders.of(fragmentActivity).get(ApplicationPreferencesViewModel.class);
    }

    public void refreshStorageBreakdown(Context context) {
        SignalExecutors.BOUNDED.execute(new Runnable(context) { // from class: org.thoughtcrime.securesms.preferences.ApplicationPreferencesViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ApplicationPreferencesViewModel.m2453$r8$lambda$zmLKPCHxzwSataI3pVHz_c9LT0(ApplicationPreferencesViewModel.this, this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$refreshStorageBreakdown$0(Context context) {
        MediaDatabase.StorageBreakdown storageBreakdown = SignalDatabase.media().getStorageBreakdown();
        this.storageBreakdown.postValue(new StorageGraphView.StorageBreakdown(Arrays.asList(new StorageGraphView.Entry(ContextCompat.getColor(context, R.color.storage_color_photos), storageBreakdown.getPhotoSize()), new StorageGraphView.Entry(ContextCompat.getColor(context, R.color.storage_color_videos), storageBreakdown.getVideoSize()), new StorageGraphView.Entry(ContextCompat.getColor(context, R.color.storage_color_files), storageBreakdown.getDocumentSize()), new StorageGraphView.Entry(ContextCompat.getColor(context, R.color.storage_color_audio), storageBreakdown.getAudioSize()))));
    }
}
