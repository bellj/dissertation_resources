package org.thoughtcrime.securesms.preferences;

import android.content.DialogInterface;
import org.thoughtcrime.securesms.preferences.StoragePreferenceFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class StoragePreferenceFragment$ConversationLengthLimitConfiguration$$ExternalSyntheticLambda4 implements DialogInterface.OnClickListener {
    public final /* synthetic */ StoragePreferenceFragment.ConversationLengthLimitConfiguration f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ StoragePreferenceFragment$ConversationLengthLimitConfiguration$$ExternalSyntheticLambda4(StoragePreferenceFragment.ConversationLengthLimitConfiguration conversationLengthLimitConfiguration, int i) {
        this.f$0 = conversationLengthLimitConfiguration;
        this.f$1 = i;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onSelectionChanged$3(this.f$1, dialogInterface, i);
    }
}
