package org.thoughtcrime.securesms.preferences;

import android.os.AsyncTask;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.CustomDefaultPreference;
import org.thoughtcrime.securesms.database.ApnDatabase;
import org.thoughtcrime.securesms.mms.LegacyMmsConnection;
import org.thoughtcrime.securesms.util.TelephonyUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class MmsPreferencesFragment extends CorrectedPreferenceFragment {
    private static final String TAG = Log.tag(MmsPreferencesFragment.class);

    @Override // androidx.preference.PreferenceFragmentCompat
    public void onCreatePreferences(Bundle bundle, String str) {
        addPreferencesFromResource(R.xml.preferences_manual_mms);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        new LoadApnDefaultsTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
    }

    /* loaded from: classes4.dex */
    private class LoadApnDefaultsTask extends AsyncTask<Void, Void, LegacyMmsConnection.Apn> {
        private LoadApnDefaultsTask() {
            MmsPreferencesFragment.this = r1;
        }

        public LegacyMmsConnection.Apn doInBackground(Void... voidArr) {
            try {
                FragmentActivity activity = MmsPreferencesFragment.this.getActivity();
                if (activity != null) {
                    return ApnDatabase.getInstance(activity).getDefaultApnParameters(TelephonyUtil.getMccMnc(activity), TelephonyUtil.getApn(activity));
                }
                return null;
            } catch (IOException e) {
                Log.w(MmsPreferencesFragment.TAG, e);
                return null;
            }
        }

        public void onPostExecute(LegacyMmsConnection.Apn apn) {
            ((CustomDefaultPreference) MmsPreferencesFragment.this.findPreference(TextSecurePreferences.MMSC_HOST_PREF)).setValidator(new CustomDefaultPreference.CustomDefaultPreferenceDialogFragmentCompat.UriValidator()).setDefaultValue(apn.getMmsc());
            ((CustomDefaultPreference) MmsPreferencesFragment.this.findPreference(TextSecurePreferences.MMSC_PROXY_HOST_PREF)).setValidator(new CustomDefaultPreference.CustomDefaultPreferenceDialogFragmentCompat.HostnameValidator()).setDefaultValue(apn.getProxy());
            ((CustomDefaultPreference) MmsPreferencesFragment.this.findPreference(TextSecurePreferences.MMSC_PROXY_PORT_PREF)).setValidator(new CustomDefaultPreference.CustomDefaultPreferenceDialogFragmentCompat.PortValidator()).setDefaultValue(Integer.valueOf(apn.getPort()));
            ((CustomDefaultPreference) MmsPreferencesFragment.this.findPreference(TextSecurePreferences.MMSC_USERNAME_PREF)).setDefaultValue(Integer.valueOf(apn.getPort()));
            ((CustomDefaultPreference) MmsPreferencesFragment.this.findPreference(TextSecurePreferences.MMSC_PASSWORD_PREF)).setDefaultValue(apn.getPassword());
            ((CustomDefaultPreference) MmsPreferencesFragment.this.findPreference(TextSecurePreferences.MMS_USER_AGENT)).setDefaultValue(LegacyMmsConnection.USER_AGENT);
        }
    }
}
