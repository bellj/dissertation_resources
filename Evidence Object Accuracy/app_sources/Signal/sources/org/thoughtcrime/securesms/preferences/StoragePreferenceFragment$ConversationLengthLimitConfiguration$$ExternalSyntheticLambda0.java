package org.thoughtcrime.securesms.preferences;

import android.content.DialogInterface;
import android.widget.EditText;
import org.thoughtcrime.securesms.preferences.StoragePreferenceFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class StoragePreferenceFragment$ConversationLengthLimitConfiguration$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ StoragePreferenceFragment.ConversationLengthLimitConfiguration f$0;
    public final /* synthetic */ EditText f$1;

    public /* synthetic */ StoragePreferenceFragment$ConversationLengthLimitConfiguration$$ExternalSyntheticLambda0(StoragePreferenceFragment.ConversationLengthLimitConfiguration conversationLengthLimitConfiguration, EditText editText) {
        this.f$0 = conversationLengthLimitConfiguration;
        this.f$1 = editText;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onCustomizeClicked$0(this.f$1, dialogInterface, i);
    }
}
