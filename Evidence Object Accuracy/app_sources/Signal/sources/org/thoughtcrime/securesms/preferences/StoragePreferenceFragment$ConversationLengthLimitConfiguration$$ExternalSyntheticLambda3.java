package org.thoughtcrime.securesms.preferences;

import org.thoughtcrime.securesms.preferences.StoragePreferenceFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class StoragePreferenceFragment$ConversationLengthLimitConfiguration$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ int f$0;
    public final /* synthetic */ long f$1;

    public /* synthetic */ StoragePreferenceFragment$ConversationLengthLimitConfiguration$$ExternalSyntheticLambda3(int i, long j) {
        this.f$0 = i;
        this.f$1 = j;
    }

    @Override // java.lang.Runnable
    public final void run() {
        StoragePreferenceFragment.ConversationLengthLimitConfiguration.lambda$updateTrimByLength$4(this.f$0, this.f$1);
    }
}
