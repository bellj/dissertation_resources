package org.thoughtcrime.securesms.preferences;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.ShareCompat$IntentBuilder;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import j$.util.Optional;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contactshare.SimpleTextWatcher;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.preferences.EditProxyViewModel;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.SignalProxyUtil;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;
import org.thoughtcrime.securesms.util.views.LearnMoreTextView;
import org.whispersystems.signalservice.api.websocket.WebSocketConnectionState;

/* loaded from: classes4.dex */
public class EditProxyFragment extends Fragment {
    private TextView proxyStatus;
    private SwitchCompat proxySwitch;
    private EditText proxyText;
    private TextView proxyTitle;
    private CircularProgressMaterialButton saveButton;
    private View shareButton;
    private EditProxyViewModel viewModel;

    public static EditProxyFragment newInstance() {
        return new EditProxyFragment();
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.edit_proxy_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.proxySwitch = (SwitchCompat) view.findViewById(R.id.edit_proxy_switch);
        this.proxyTitle = (TextView) view.findViewById(R.id.edit_proxy_address_title);
        this.proxyText = (EditText) view.findViewById(R.id.edit_proxy_host);
        this.proxyStatus = (TextView) view.findViewById(R.id.edit_proxy_status);
        this.saveButton = (CircularProgressMaterialButton) view.findViewById(R.id.edit_proxy_save);
        this.shareButton = view.findViewById(R.id.edit_proxy_share);
        this.proxyText.addTextChangedListener(new SimpleTextWatcher() { // from class: org.thoughtcrime.securesms.preferences.EditProxyFragment.1
            @Override // org.thoughtcrime.securesms.contactshare.SimpleTextWatcher
            public void onTextChanged(String str) {
                EditProxyFragment.this.onProxyTextChanged(str);
            }
        });
        this.proxyText.setText((CharSequence) Optional.ofNullable(SignalStore.proxy().getProxy()).map(new EditProxyFragment$$ExternalSyntheticLambda0()).orElse(""));
        this.proxySwitch.setChecked(SignalStore.proxy().isProxyEnabled());
        initViewModel();
        this.saveButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.preferences.EditProxyFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditProxyFragment.this.lambda$onViewCreated$0(view2);
            }
        });
        this.shareButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.preferences.EditProxyFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditProxyFragment.this.lambda$onViewCreated$1(view2);
            }
        });
        this.proxySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: org.thoughtcrime.securesms.preferences.EditProxyFragment$$ExternalSyntheticLambda3
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                EditProxyFragment.this.lambda$onViewCreated$2(compoundButton, z);
            }
        });
        LearnMoreTextView learnMoreTextView = (LearnMoreTextView) view.findViewById(R.id.edit_proxy_switch_title_description);
        learnMoreTextView.setLearnMoreVisible(true);
        learnMoreTextView.setOnLinkClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.preferences.EditProxyFragment$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditProxyFragment.this.lambda$onViewCreated$3(view2);
            }
        });
        requireActivity().getWindow().setSoftInputMode(16);
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        onSaveClicked();
    }

    public /* synthetic */ void lambda$onViewCreated$1(View view) {
        onShareClicked();
    }

    public /* synthetic */ void lambda$onViewCreated$2(CompoundButton compoundButton, boolean z) {
        this.viewModel.onToggleProxy(z);
    }

    public /* synthetic */ void lambda$onViewCreated$3(View view) {
        CommunicationActions.openBrowserLink(requireContext(), "https://support.signal.org/hc/articles/360056052052");
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        SignalProxyUtil.startListeningToWebsocket();
    }

    private void initViewModel() {
        EditProxyViewModel editProxyViewModel = (EditProxyViewModel) ViewModelProviders.of(this).get(EditProxyViewModel.class);
        this.viewModel = editProxyViewModel;
        editProxyViewModel.getUiState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.preferences.EditProxyFragment$$ExternalSyntheticLambda5
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                EditProxyFragment.this.presentUiState((EditProxyViewModel.UiState) obj);
            }
        });
        this.viewModel.getProxyState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.preferences.EditProxyFragment$$ExternalSyntheticLambda6
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                EditProxyFragment.this.presentProxyState((WebSocketConnectionState) obj);
            }
        });
        this.viewModel.getEvents().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.preferences.EditProxyFragment$$ExternalSyntheticLambda7
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                EditProxyFragment.this.presentEvent((EditProxyViewModel.Event) obj);
            }
        });
        this.viewModel.getSaveState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.preferences.EditProxyFragment$$ExternalSyntheticLambda8
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                EditProxyFragment.this.presentSaveState((EditProxyViewModel.SaveState) obj);
            }
        });
    }

    public void presentUiState(EditProxyViewModel.UiState uiState) {
        int i = AnonymousClass2.$SwitchMap$org$thoughtcrime$securesms$preferences$EditProxyViewModel$UiState[uiState.ordinal()];
        if (i == 1) {
            this.proxyText.setEnabled(true);
            this.proxyText.setAlpha(1.0f);
            this.proxyTitle.setAlpha(1.0f);
            onProxyTextChanged(this.proxyText.getText().toString());
        } else if (i == 2) {
            this.proxyText.setEnabled(false);
            this.proxyText.setAlpha(0.5f);
            this.saveButton.setEnabled(false);
            this.saveButton.setAlpha(0.5f);
            this.shareButton.setEnabled(false);
            this.shareButton.setAlpha(0.5f);
            this.proxyTitle.setAlpha(0.5f);
            this.proxyStatus.setVisibility(4);
        }
    }

    public void presentProxyState(WebSocketConnectionState webSocketConnectionState) {
        if (SignalStore.proxy().getProxy() != null) {
            switch (AnonymousClass2.$SwitchMap$org$whispersystems$signalservice$api$websocket$WebSocketConnectionState[webSocketConnectionState.ordinal()]) {
                case 1:
                case 2:
                case 3:
                    this.proxyStatus.setText(R.string.preferences_connecting_to_proxy);
                    this.proxyStatus.setTextColor(getResources().getColor(R.color.signal_text_secondary));
                    return;
                case 4:
                    this.proxyStatus.setText(R.string.preferences_connected_to_proxy);
                    this.proxyStatus.setTextColor(getResources().getColor(R.color.signal_accent_green));
                    return;
                case 5:
                case 6:
                    this.proxyStatus.setText(R.string.preferences_connection_failed);
                    this.proxyStatus.setTextColor(getResources().getColor(R.color.signal_alert_primary));
                    return;
                default:
                    return;
            }
        } else {
            this.proxyStatus.setText("");
        }
    }

    public void presentEvent(EditProxyViewModel.Event event) {
        int i = AnonymousClass2.$SwitchMap$org$thoughtcrime$securesms$preferences$EditProxyViewModel$Event[event.ordinal()];
        if (i == 1) {
            this.proxyStatus.setVisibility(0);
            this.proxyText.setText((CharSequence) Optional.ofNullable(SignalStore.proxy().getProxy()).map(new EditProxyFragment$$ExternalSyntheticLambda0()).orElse(""));
            new AlertDialog.Builder(requireContext()).setTitle(R.string.preferences_success).setMessage(R.string.preferences_you_are_connected_to_the_proxy).setPositiveButton(17039370, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.preferences.EditProxyFragment$$ExternalSyntheticLambda9
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    EditProxyFragment.this.lambda$presentEvent$4(dialogInterface, i2);
                }
            }).show();
        } else if (i == 2) {
            this.proxyStatus.setVisibility(4);
            this.proxyText.setText((CharSequence) Optional.ofNullable(SignalStore.proxy().getProxy()).map(new EditProxyFragment$$ExternalSyntheticLambda0()).orElse(""));
            ViewUtil.focusAndMoveCursorToEndAndOpenKeyboard(this.proxyText);
            new AlertDialog.Builder(requireContext()).setTitle(R.string.preferences_failed_to_connect).setMessage(R.string.preferences_couldnt_connect_to_the_proxy).setPositiveButton(17039370, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.preferences.EditProxyFragment$$ExternalSyntheticLambda10
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    dialogInterface.dismiss();
                }
            }).show();
        }
    }

    public /* synthetic */ void lambda$presentEvent$4(DialogInterface dialogInterface, int i) {
        requireActivity().onBackPressed();
        dialogInterface.dismiss();
    }

    /* renamed from: org.thoughtcrime.securesms.preferences.EditProxyFragment$2 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$preferences$EditProxyViewModel$Event;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$preferences$EditProxyViewModel$SaveState;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$preferences$EditProxyViewModel$UiState;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$api$websocket$WebSocketConnectionState;

        static {
            int[] iArr = new int[EditProxyViewModel.SaveState.values().length];
            $SwitchMap$org$thoughtcrime$securesms$preferences$EditProxyViewModel$SaveState = iArr;
            try {
                iArr[EditProxyViewModel.SaveState.IDLE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$preferences$EditProxyViewModel$SaveState[EditProxyViewModel.SaveState.IN_PROGRESS.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            int[] iArr2 = new int[EditProxyViewModel.Event.values().length];
            $SwitchMap$org$thoughtcrime$securesms$preferences$EditProxyViewModel$Event = iArr2;
            try {
                iArr2[EditProxyViewModel.Event.PROXY_SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$preferences$EditProxyViewModel$Event[EditProxyViewModel.Event.PROXY_FAILURE.ordinal()] = 2;
            } catch (NoSuchFieldError unused4) {
            }
            int[] iArr3 = new int[WebSocketConnectionState.values().length];
            $SwitchMap$org$whispersystems$signalservice$api$websocket$WebSocketConnectionState = iArr3;
            try {
                iArr3[WebSocketConnectionState.DISCONNECTED.ordinal()] = 1;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$websocket$WebSocketConnectionState[WebSocketConnectionState.DISCONNECTING.ordinal()] = 2;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$websocket$WebSocketConnectionState[WebSocketConnectionState.CONNECTING.ordinal()] = 3;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$websocket$WebSocketConnectionState[WebSocketConnectionState.CONNECTED.ordinal()] = 4;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$websocket$WebSocketConnectionState[WebSocketConnectionState.AUTHENTICATION_FAILED.ordinal()] = 5;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$websocket$WebSocketConnectionState[WebSocketConnectionState.FAILED.ordinal()] = 6;
            } catch (NoSuchFieldError unused10) {
            }
            int[] iArr4 = new int[EditProxyViewModel.UiState.values().length];
            $SwitchMap$org$thoughtcrime$securesms$preferences$EditProxyViewModel$UiState = iArr4;
            try {
                iArr4[EditProxyViewModel.UiState.ALL_ENABLED.ordinal()] = 1;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$preferences$EditProxyViewModel$UiState[EditProxyViewModel.UiState.ALL_DISABLED.ordinal()] = 2;
            } catch (NoSuchFieldError unused12) {
            }
        }
    }

    public void presentSaveState(EditProxyViewModel.SaveState saveState) {
        int i = AnonymousClass2.$SwitchMap$org$thoughtcrime$securesms$preferences$EditProxyViewModel$SaveState[saveState.ordinal()];
        if (i == 1) {
            this.saveButton.cancelSpinning();
        } else if (i == 2) {
            this.saveButton.setSpinning();
        }
    }

    private void onSaveClicked() {
        this.viewModel.onSaveClicked(this.proxyText.getText().toString());
    }

    private void onShareClicked() {
        ShareCompat$IntentBuilder.from(requireActivity()).setText(SignalProxyUtil.generateProxyUrl(this.proxyText.getText().toString())).setType("text/plain").startChooser();
    }

    public void onProxyTextChanged(String str) {
        if (Util.isEmpty(str)) {
            this.saveButton.setEnabled(false);
            this.saveButton.setAlpha(0.5f);
            this.shareButton.setEnabled(false);
            this.shareButton.setAlpha(0.5f);
            this.proxyStatus.setVisibility(4);
            return;
        }
        this.saveButton.setEnabled(true);
        this.saveButton.setAlpha(1.0f);
        this.shareButton.setEnabled(true);
        this.shareButton.setAlpha(1.0f);
        String convertUserEnteredAddressToHost = SignalProxyUtil.convertUserEnteredAddressToHost(this.proxyText.getText().toString());
        if (!SignalStore.proxy().isProxyEnabled() || !convertUserEnteredAddressToHost.equals(SignalStore.proxy().getProxyHost())) {
            this.proxyStatus.setVisibility(4);
        } else {
            this.proxyStatus.setVisibility(0);
        }
    }
}
