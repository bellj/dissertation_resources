package org.thoughtcrime.securesms.preferences;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.DialogFragment;
import androidx.preference.Preference;
import androidx.preference.PreferenceCategory;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceGroupAdapter;
import androidx.preference.PreferenceScreen;
import androidx.preference.PreferenceViewHolder;
import androidx.recyclerview.widget.RecyclerView;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.CustomDefaultPreference;
import org.thoughtcrime.securesms.preferences.widgets.ColorPickerPreference;
import org.thoughtcrime.securesms.preferences.widgets.ColorPickerPreferenceDialogFragmentCompat;

/* loaded from: classes4.dex */
public abstract class CorrectedPreferenceFragment extends PreferenceFragmentCompat {
    @Override // androidx.preference.PreferenceFragmentCompat, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        View findViewById = getView().findViewById(16908298);
        if (findViewById != null) {
            findViewById.setPadding(0, 0, 0, 0);
        }
    }

    @Override // androidx.preference.PreferenceFragmentCompat, androidx.preference.PreferenceManager.OnDisplayPreferenceDialogListener
    public void onDisplayPreferenceDialog(Preference preference) {
        DialogFragment dialogFragment;
        if (preference instanceof ColorPickerPreference) {
            dialogFragment = ColorPickerPreferenceDialogFragmentCompat.newInstance(preference.getKey());
        } else {
            dialogFragment = preference instanceof CustomDefaultPreference ? CustomDefaultPreference.CustomDefaultPreferenceDialogFragmentCompat.newInstance(preference.getKey()) : null;
        }
        if (dialogFragment != null) {
            dialogFragment.setTargetFragment(this, 0);
            dialogFragment.show(getFragmentManager(), "android.support.v7.preference.PreferenceFragment.DIALOG");
            return;
        }
        super.onDisplayPreferenceDialog(preference);
    }

    @Override // androidx.preference.PreferenceFragmentCompat
    protected RecyclerView.Adapter onCreateAdapter(PreferenceScreen preferenceScreen) {
        return new PreferenceGroupAdapter(preferenceScreen) { // from class: org.thoughtcrime.securesms.preferences.CorrectedPreferenceFragment.1
            @Override // androidx.preference.PreferenceGroupAdapter
            public void onBindViewHolder(PreferenceViewHolder preferenceViewHolder, int i) {
                super.onBindViewHolder(preferenceViewHolder, i);
                Preference item = getItem(i);
                if (item instanceof PreferenceCategory) {
                    CorrectedPreferenceFragment.this.setZeroPaddingToLayoutChildren(preferenceViewHolder.itemView);
                    return;
                }
                View findViewById = preferenceViewHolder.itemView.findViewById(R.id.icon_frame);
                if (findViewById != null) {
                    findViewById.setVisibility(item.getIcon() == null ? 8 : 0);
                }
            }
        };
    }

    public void setZeroPaddingToLayoutChildren(View view) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                setZeroPaddingToLayoutChildren(viewGroup.getChildAt(i));
                ViewCompat.setPaddingRelative(viewGroup, 0, viewGroup.getPaddingTop(), ViewCompat.getPaddingEnd(viewGroup), viewGroup.getPaddingBottom());
            }
        }
    }
}
