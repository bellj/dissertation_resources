package org.thoughtcrime.securesms.preferences;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.rxjava3.core.BackpressureStrategy;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.SignalProxyUtil;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.websocket.WebSocketConnectionState;
import org.whispersystems.signalservice.internal.configuration.SignalProxy;

/* loaded from: classes4.dex */
public class EditProxyViewModel extends ViewModel {
    private final SingleLiveEvent<Event> events = new SingleLiveEvent<>();
    private final LiveData<WebSocketConnectionState> pipeState;
    private final MutableLiveData<SaveState> saveState;
    private final MutableLiveData<UiState> uiState;

    /* loaded from: classes4.dex */
    public enum Event {
        PROXY_SUCCESS,
        PROXY_FAILURE
    }

    /* loaded from: classes4.dex */
    public enum SaveState {
        IDLE,
        IN_PROGRESS
    }

    /* loaded from: classes4.dex */
    public enum UiState {
        ALL_DISABLED,
        ALL_ENABLED
    }

    public EditProxyViewModel() {
        LiveData<WebSocketConnectionState> liveData;
        MutableLiveData<UiState> mutableLiveData = new MutableLiveData<>();
        this.uiState = mutableLiveData;
        this.saveState = new MutableLiveData<>(SaveState.IDLE);
        if (SignalStore.account().getE164() == null) {
            liveData = new MutableLiveData<>();
        } else {
            liveData = LiveDataReactiveStreams.fromPublisher(ApplicationDependencies.getSignalWebSocket().getWebSocketState().toFlowable(BackpressureStrategy.LATEST));
        }
        this.pipeState = liveData;
        if (SignalStore.proxy().isProxyEnabled()) {
            mutableLiveData.setValue(UiState.ALL_ENABLED);
        } else {
            mutableLiveData.setValue(UiState.ALL_DISABLED);
        }
    }

    public void onToggleProxy(boolean z) {
        if (z) {
            SignalProxy proxy = SignalStore.proxy().getProxy();
            if (proxy != null && !Util.isEmpty(proxy.getHost())) {
                SignalProxyUtil.enableProxy(proxy);
            }
            this.uiState.postValue(UiState.ALL_ENABLED);
            return;
        }
        SignalProxyUtil.disableProxy();
        this.uiState.postValue(UiState.ALL_DISABLED);
    }

    public void onSaveClicked(String str) {
        String convertUserEnteredAddressToHost = SignalProxyUtil.convertUserEnteredAddressToHost(str);
        this.saveState.postValue(SaveState.IN_PROGRESS);
        SignalExecutors.BOUNDED.execute(new Runnable(convertUserEnteredAddressToHost) { // from class: org.thoughtcrime.securesms.preferences.EditProxyViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                EditProxyViewModel.this.lambda$onSaveClicked$0(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$onSaveClicked$0(String str) {
        SignalProxyUtil.enableProxy(new SignalProxy(str, BuildConfig.CONTENT_PROXY_PORT));
        if (SignalProxyUtil.testWebsocketConnection(TimeUnit.SECONDS.toMillis(10))) {
            this.events.postValue(Event.PROXY_SUCCESS);
        } else {
            SignalProxyUtil.disableProxy();
            this.events.postValue(Event.PROXY_FAILURE);
        }
        this.saveState.postValue(SaveState.IDLE);
    }

    public LiveData<UiState> getUiState() {
        return this.uiState;
    }

    public LiveData<Event> getEvents() {
        return this.events;
    }

    public LiveData<WebSocketConnectionState> getProxyState() {
        return this.pipeState;
    }

    public LiveData<SaveState> getSaveState() {
        return this.saveState;
    }
}
