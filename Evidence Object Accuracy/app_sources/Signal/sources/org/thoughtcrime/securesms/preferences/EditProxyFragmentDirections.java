package org.thoughtcrime.securesms.preferences;

import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.SignupDirections;

/* loaded from: classes4.dex */
public class EditProxyFragmentDirections {
    private EditProxyFragmentDirections() {
    }

    public static NavDirections actionRestartToWelcomeFragment() {
        return SignupDirections.actionRestartToWelcomeFragment();
    }
}
