package org.thoughtcrime.securesms.preferences;

import androidx.preference.ListPreference;
import androidx.preference.Preference;
import java.util.Arrays;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public abstract class ListSummaryPreferenceFragment extends CorrectedPreferenceFragment {

    /* loaded from: classes4.dex */
    protected class ListSummaryListener implements Preference.OnPreferenceChangeListener {
        protected ListSummaryListener() {
            ListSummaryPreferenceFragment.this = r1;
        }

        @Override // androidx.preference.Preference.OnPreferenceChangeListener
        public boolean onPreferenceChange(Preference preference, Object obj) {
            String str;
            ListPreference listPreference = (ListPreference) preference;
            int indexOf = Arrays.asList(listPreference.getEntryValues()).indexOf(obj);
            if (indexOf < 0 || indexOf >= listPreference.getEntries().length) {
                str = ListSummaryPreferenceFragment.this.getString(R.string.preferences__led_color_unknown);
            } else {
                str = listPreference.getEntries()[indexOf];
            }
            listPreference.setSummary(str);
            return true;
        }
    }

    protected void initializeListSummary(ListPreference listPreference) {
        listPreference.setSummary(listPreference.getEntry());
    }
}
