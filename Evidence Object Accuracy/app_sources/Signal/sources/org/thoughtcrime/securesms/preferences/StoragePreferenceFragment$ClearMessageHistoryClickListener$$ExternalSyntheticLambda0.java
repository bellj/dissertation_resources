package org.thoughtcrime.securesms.preferences;

import android.content.DialogInterface;
import org.thoughtcrime.securesms.preferences.StoragePreferenceFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class StoragePreferenceFragment$ClearMessageHistoryClickListener$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        StoragePreferenceFragment.ClearMessageHistoryClickListener.lambda$showAreYouReallySure$2(dialogInterface, i);
    }
}
