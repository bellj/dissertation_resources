package org.thoughtcrime.securesms.preferences;

import android.content.DialogInterface;
import org.thoughtcrime.securesms.preferences.StoragePreferenceFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class StoragePreferenceFragment$ClearMessageHistoryClickListener$$ExternalSyntheticLambda2 implements DialogInterface.OnClickListener {
    public final /* synthetic */ StoragePreferenceFragment.ClearMessageHistoryClickListener f$0;

    public /* synthetic */ StoragePreferenceFragment$ClearMessageHistoryClickListener$$ExternalSyntheticLambda2(StoragePreferenceFragment.ClearMessageHistoryClickListener clearMessageHistoryClickListener) {
        this.f$0 = clearMessageHistoryClickListener;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onPreferenceClick$0(dialogInterface, i);
    }
}
