package org.thoughtcrime.securesms.preferences;

import android.os.Bundle;
import android.view.MenuItem;
import androidx.fragment.app.FragmentTransaction;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.util.DynamicLanguage;
import org.thoughtcrime.securesms.util.DynamicTheme;

/* loaded from: classes4.dex */
public class MmsPreferencesActivity extends PassphraseRequiredActivity {
    static final /* synthetic */ boolean $assertionsDisabled;
    private final DynamicLanguage dynamicLanguage = new DynamicLanguage();
    private final DynamicTheme dynamicTheme = new DynamicTheme();

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onPreCreate() {
        this.dynamicTheme.onCreate(this);
        this.dynamicLanguage.onCreate(this);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        MmsPreferencesFragment mmsPreferencesFragment = new MmsPreferencesFragment();
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        beginTransaction.replace(16908290, mmsPreferencesFragment);
        beginTransaction.commit();
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
        this.dynamicLanguage.onResume(this);
    }

    @Override // android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        finish();
        return true;
    }

    @Override // androidx.activity.ComponentActivity, android.app.Activity
    public void onBackPressed() {
        super.onBackPressed();
    }
}
