package org.thoughtcrime.securesms.preferences;

import android.content.DialogInterface;
import org.thoughtcrime.securesms.preferences.StoragePreferenceFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class StoragePreferenceFragment$ConversationLengthLimitConfiguration$$ExternalSyntheticLambda1 implements DialogInterface.OnClickListener {
    public final /* synthetic */ StoragePreferenceFragment.ConversationLengthLimitConfiguration f$0;

    public /* synthetic */ StoragePreferenceFragment$ConversationLengthLimitConfiguration$$ExternalSyntheticLambda1(StoragePreferenceFragment.ConversationLengthLimitConfiguration conversationLengthLimitConfiguration) {
        this.f$0 = conversationLengthLimitConfiguration;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onCustomizeClicked$1(dialogInterface, i);
    }
}
