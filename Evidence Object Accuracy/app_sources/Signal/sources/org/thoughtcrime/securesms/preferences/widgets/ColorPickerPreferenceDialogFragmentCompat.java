package org.thoughtcrime.securesms.preferences.widgets;

import android.app.Dialog;
import android.os.Bundle;
import androidx.preference.PreferenceDialogFragmentCompat;
import com.takisoft.colorpicker.ColorPickerDialog;
import com.takisoft.colorpicker.OnColorSelectedListener;

/* loaded from: classes4.dex */
public class ColorPickerPreferenceDialogFragmentCompat extends PreferenceDialogFragmentCompat implements OnColorSelectedListener {
    private int pickedColor;

    public static ColorPickerPreferenceDialogFragmentCompat newInstance(String str) {
        ColorPickerPreferenceDialogFragmentCompat colorPickerPreferenceDialogFragmentCompat = new ColorPickerPreferenceDialogFragmentCompat();
        Bundle bundle = new Bundle(1);
        bundle.putString("key", str);
        colorPickerPreferenceDialogFragmentCompat.setArguments(bundle);
        return colorPickerPreferenceDialogFragmentCompat;
    }

    @Override // androidx.preference.PreferenceDialogFragmentCompat, androidx.fragment.app.DialogFragment
    public Dialog onCreateDialog(Bundle bundle) {
        ColorPickerPreference colorPickerPreference = getColorPickerPreference();
        ColorPickerDialog colorPickerDialog = new ColorPickerDialog(getActivity(), this, new ColorPickerDialog.Params.Builder(getContext()).setSelectedColor(colorPickerPreference.getColor()).setColors(colorPickerPreference.getColors()).setColorContentDescriptions(colorPickerPreference.getColorDescriptions()).setSize(colorPickerPreference.getSize()).setSortColors(colorPickerPreference.isSortColors()).setColumns(colorPickerPreference.getColumns()).build());
        colorPickerDialog.setTitle(colorPickerPreference.getDialogTitle());
        return colorPickerDialog;
    }

    @Override // androidx.preference.PreferenceDialogFragmentCompat
    public void onDialogClosed(boolean z) {
        ColorPickerPreference colorPickerPreference = getColorPickerPreference();
        if (z) {
            colorPickerPreference.setColor(this.pickedColor);
        }
    }

    @Override // com.takisoft.colorpicker.OnColorSelectedListener
    public void onColorSelected(int i) {
        this.pickedColor = i;
        super.onClick(getDialog(), -1);
    }

    ColorPickerPreference getColorPickerPreference() {
        return (ColorPickerPreference) getPreference();
    }
}
