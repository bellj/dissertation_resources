package org.thoughtcrime.securesms.preferences;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.keyvalue.KeepMessagesDuration;
import org.thoughtcrime.securesms.preferences.StoragePreferenceFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class StoragePreferenceFragment$KeepMessagesConfiguration$$ExternalSyntheticLambda1 implements Function {
    public final /* synthetic */ StoragePreferenceFragment.KeepMessagesConfiguration f$0;
    public final /* synthetic */ KeepMessagesDuration f$1;

    public /* synthetic */ StoragePreferenceFragment$KeepMessagesConfiguration$$ExternalSyntheticLambda1(StoragePreferenceFragment.KeepMessagesConfiguration keepMessagesConfiguration, KeepMessagesDuration keepMessagesDuration) {
        this.f$0 = keepMessagesConfiguration;
        this.f$1 = keepMessagesDuration;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return this.f$0.lambda$getSettings$0(this.f$1, (KeepMessagesDuration) obj);
    }
}
