package org.thoughtcrime.securesms.preferences.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.TypedArrayUtils;
import androidx.preference.DialogPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceViewHolder;
import com.takisoft.colorpicker.ColorStateDrawable;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class ColorPickerPreference extends DialogPreference {
    private static final String TAG = Log.tag(ColorPickerPreference.class);
    private int color;
    private CharSequence[] colorDescriptions;
    private ImageView colorWidget;
    private int[] colors;
    private int columns;
    private Preference.OnPreferenceChangeListener listener;
    private int size;
    private boolean sortColors;

    public ColorPickerPreference(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.ColorPickerPreference, i, 0);
        int resourceId = obtainStyledAttributes.getResourceId(2, R.array.color_picker_default_colors);
        if (resourceId != 0) {
            this.colors = context.getResources().getIntArray(resourceId);
        }
        this.colorDescriptions = obtainStyledAttributes.getTextArray(0);
        this.color = obtainStyledAttributes.getColor(4, 0);
        this.columns = obtainStyledAttributes.getInt(3, 3);
        this.size = obtainStyledAttributes.getInt(1, 2);
        this.sortColors = obtainStyledAttributes.getBoolean(5, false);
        obtainStyledAttributes.recycle();
        setWidgetLayoutResource(R.layout.preference_widget_color_swatch);
    }

    public ColorPickerPreference(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet, i, 0);
    }

    public ColorPickerPreference(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, TypedArrayUtils.getAttr(context, R.attr.dialogPreferenceStyle, 16842897));
    }

    public ColorPickerPreference(Context context) {
        this(context, null);
    }

    @Override // androidx.preference.Preference
    public void setOnPreferenceChangeListener(Preference.OnPreferenceChangeListener onPreferenceChangeListener) {
        super.setOnPreferenceChangeListener(onPreferenceChangeListener);
        this.listener = onPreferenceChangeListener;
    }

    @Override // androidx.preference.Preference
    public void onBindViewHolder(PreferenceViewHolder preferenceViewHolder) {
        super.onBindViewHolder(preferenceViewHolder);
        this.colorWidget = (ImageView) preferenceViewHolder.findViewById(R.id.color_picker_widget);
        setColorOnWidget(this.color);
    }

    private void setColorOnWidget(int i) {
        if (this.colorWidget != null) {
            this.colorWidget.setImageDrawable(new ColorStateDrawable(new Drawable[]{ContextCompat.getDrawable(getContext(), R.drawable.colorpickerpreference_pref_swatch)}, i));
        }
    }

    public int getColor() {
        return this.color;
    }

    public void setColor(int i) {
        setInternalColor(i, false);
    }

    public int[] getColors() {
        return this.colors;
    }

    public void setColors(int[] iArr) {
        this.colors = iArr;
    }

    public boolean isSortColors() {
        return this.sortColors;
    }

    public void setSortColors(boolean z) {
        this.sortColors = z;
    }

    public CharSequence[] getColorDescriptions() {
        return this.colorDescriptions;
    }

    public void setColorDescriptions(CharSequence[] charSequenceArr) {
        this.colorDescriptions = charSequenceArr;
    }

    public int getColumns() {
        return this.columns;
    }

    public void setColumns(int i) {
        this.columns = i;
    }

    public int getSize() {
        return this.size;
    }

    public void setSize(int i) {
        this.size = i;
    }

    private void setInternalColor(int i, boolean z) {
        boolean z2 = false;
        if (getPersistedInt(0) != i) {
            z2 = true;
        }
        if (z2 || z) {
            this.color = i;
            persistInt(i);
            setColorOnWidget(i);
            Preference.OnPreferenceChangeListener onPreferenceChangeListener = this.listener;
            if (onPreferenceChangeListener != null) {
                onPreferenceChangeListener.onPreferenceChange(this, Integer.valueOf(i));
            }
            notifyChanged();
        }
    }

    @Override // androidx.preference.Preference
    protected Object onGetDefaultValue(TypedArray typedArray, int i) {
        return typedArray.getString(i);
    }

    @Override // androidx.preference.Preference
    protected void onSetInitialValue(boolean z, Object obj) {
        String str = (String) obj;
        int i = 0;
        if (z) {
            i = getPersistedInt(0);
        } else if (!TextUtils.isEmpty(str)) {
            i = Color.parseColor(str);
        }
        setInternalColor(i, true);
    }
}
