package org.thoughtcrime.securesms.preferences;

import android.content.DialogInterface;
import org.thoughtcrime.securesms.keyvalue.KeepMessagesDuration;
import org.thoughtcrime.securesms.preferences.StoragePreferenceFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class StoragePreferenceFragment$KeepMessagesConfiguration$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ StoragePreferenceFragment.KeepMessagesConfiguration f$0;
    public final /* synthetic */ KeepMessagesDuration f$1;

    public /* synthetic */ StoragePreferenceFragment$KeepMessagesConfiguration$$ExternalSyntheticLambda0(StoragePreferenceFragment.KeepMessagesConfiguration keepMessagesConfiguration, KeepMessagesDuration keepMessagesDuration) {
        this.f$0 = keepMessagesConfiguration;
        this.f$1 = keepMessagesDuration;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onSelectionChanged$1(this.f$1, dialogInterface, i);
    }
}
