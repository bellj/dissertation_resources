package org.thoughtcrime.securesms.preferences.widgets;

import java.util.Objects;

/* loaded from: classes.dex */
public final class NotificationPrivacyPreference {
    private final String preference;

    public NotificationPrivacyPreference(String str) {
        this.preference = str;
    }

    public boolean isDisplayContact() {
        return "all".equals(this.preference) || "contact".equals(this.preference);
    }

    public boolean isDisplayMessage() {
        return "all".equals(this.preference);
    }

    public boolean isDisplayNothing() {
        return !isDisplayContact();
    }

    public String toString() {
        return this.preference;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || NotificationPrivacyPreference.class != obj.getClass()) {
            return false;
        }
        return Objects.equals(this.preference, ((NotificationPrivacyPreference) obj).preference);
    }

    public int hashCode() {
        return Objects.hash(this.preference);
    }
}
