package org.thoughtcrime.securesms.preferences;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.core.text.HtmlCompat;
import androidx.fragment.app.Fragment;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Objects;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.backup.BackupDialog;
import org.thoughtcrime.securesms.backup.FullBackupBase;
import org.thoughtcrime.securesms.database.NoExternalStorageException;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.LocalBackupJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.util.BackupUtil;
import org.thoughtcrime.securesms.util.StorageUtil;

/* loaded from: classes.dex */
public class BackupsPreferenceFragment extends Fragment {
    private static final short CHOOSE_BACKUPS_LOCATION_REQUEST_CODE;
    private static final String TAG = Log.tag(BackupsPreferenceFragment.class);
    private View create;
    private View folder;
    private TextView folderName;
    private final NumberFormat formatter = NumberFormat.getInstance();
    private TextView info;
    private ProgressBar progress;
    private TextView progressSummary;
    private TextView summary;
    private TextView toggle;
    private View verify;

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_backups, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.create = view.findViewById(R.id.fragment_backup_create);
        this.folder = view.findViewById(R.id.fragment_backup_folder);
        this.verify = view.findViewById(R.id.fragment_backup_verify);
        this.toggle = (TextView) view.findViewById(R.id.fragment_backup_toggle);
        this.info = (TextView) view.findViewById(R.id.fragment_backup_info);
        this.summary = (TextView) view.findViewById(R.id.fragment_backup_create_summary);
        this.folderName = (TextView) view.findViewById(R.id.fragment_backup_folder_name);
        this.progress = (ProgressBar) view.findViewById(R.id.fragment_backup_progress);
        this.progressSummary = (TextView) view.findViewById(R.id.fragment_backup_progress_summary);
        this.toggle.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.preferences.BackupsPreferenceFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                BackupsPreferenceFragment.this.lambda$onViewCreated$0(view2);
            }
        });
        this.create.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.preferences.BackupsPreferenceFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                BackupsPreferenceFragment.this.lambda$onViewCreated$1(view2);
            }
        });
        this.verify.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.preferences.BackupsPreferenceFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                BackupsPreferenceFragment.this.lambda$onViewCreated$2(view2);
            }
        });
        this.formatter.setMinimumFractionDigits(1);
        this.formatter.setMaximumFractionDigits(1);
        EventBus.getDefault().register(this);
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        onToggleClicked();
    }

    public /* synthetic */ void lambda$onViewCreated$1(View view) {
        onCreateClicked();
    }

    public /* synthetic */ void lambda$onViewCreated$2(View view) {
        BackupDialog.showVerifyBackupPassphraseDialog(requireContext());
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        setBackupStatus();
        setBackupSummary();
        setInfo();
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Override // androidx.fragment.app.Fragment
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        Permissions.onRequestPermissionsResult(this, i, strArr, iArr);
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        if (Build.VERSION.SDK_INT >= 29 && i == 26212 && i2 == -1 && intent != null && intent.getData() != null) {
            BackupDialog.showEnableBackupDialog(requireContext(), intent, StorageUtil.getDisplayPath(requireContext(), intent.getData()), new BackupsPreferenceFragment$$ExternalSyntheticLambda6(this));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(FullBackupBase.BackupEvent backupEvent) {
        int i = 8;
        if (backupEvent.getType() == FullBackupBase.BackupEvent.Type.PROGRESS) {
            this.create.setEnabled(false);
            this.summary.setText(getString(R.string.BackupsPreferenceFragment__in_progress));
            this.progress.setVisibility(0);
            TextView textView = this.progressSummary;
            if (backupEvent.getCount() > 0) {
                i = 0;
            }
            textView.setVisibility(i);
            if (backupEvent.getEstimatedTotalCount() == 0) {
                this.progress.setIndeterminate(true);
                this.progressSummary.setText(getString(R.string.BackupsPreferenceFragment__d_so_far, Long.valueOf(backupEvent.getCount())));
                return;
            }
            double completionPercentage = backupEvent.getCompletionPercentage();
            this.progress.setIndeterminate(false);
            this.progress.setMax(100);
            this.progress.setProgress((int) completionPercentage);
            this.progressSummary.setText(getString(R.string.BackupsPreferenceFragment__s_so_far, this.formatter.format(completionPercentage)));
        } else if (backupEvent.getType() == FullBackupBase.BackupEvent.Type.FINISHED) {
            this.create.setEnabled(true);
            this.progress.setVisibility(8);
            this.progressSummary.setVisibility(8);
            setBackupSummary();
        }
    }

    private void setBackupStatus() {
        if (!SignalStore.settings().isBackupEnabled()) {
            setBackupsDisabled();
        } else if (BackupUtil.canUserAccessBackupDirectory(requireContext())) {
            setBackupsEnabled();
        } else {
            Log.w(TAG, "Cannot access backup directory. Disabling backups.");
            BackupUtil.disableBackups(requireContext());
            setBackupsDisabled();
        }
    }

    private void setBackupSummary() {
        this.summary.setText(getString(R.string.BackupsPreferenceFragment__last_backup, BackupUtil.getLastBackupTime(requireContext(), Locale.getDefault())));
    }

    private void setBackupFolderName() {
        this.folder.setVisibility(8);
        if (!BackupUtil.canUserAccessBackupDirectory(requireContext())) {
            return;
        }
        if (BackupUtil.isUserSelectionRequired(requireContext()) && BackupUtil.canUserAccessBackupDirectory(requireContext())) {
            Uri signalBackupDirectory = SignalStore.settings().getSignalBackupDirectory();
            Objects.requireNonNull(signalBackupDirectory);
            this.folder.setVisibility(0);
            this.folderName.setText(StorageUtil.getDisplayPath(requireContext(), signalBackupDirectory));
        } else if (StorageUtil.canWriteInSignalStorageDir()) {
            try {
                this.folder.setVisibility(0);
                this.folderName.setText(StorageUtil.getOrCreateBackupDirectory().getPath());
            } catch (NoExternalStorageException e) {
                Log.w(TAG, "Could not display folder name.", e);
            }
        }
    }

    private void setInfo() {
        this.info.setText(HtmlCompat.fromHtml(getString(R.string.BackupsPreferenceFragment__to_restore_a_backup, String.format("<a href=\"%s\">%s</a>", getString(R.string.backup_support_url), getString(R.string.BackupsPreferenceFragment__learn_more))), 0));
        this.info.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void onToggleClicked() {
        if (BackupUtil.isUserSelectionRequired(requireContext())) {
            onToggleClickedApi29();
        } else {
            onToggleClickedLegacy();
        }
    }

    private void onToggleClickedApi29() {
        if (!SignalStore.settings().isBackupEnabled()) {
            BackupDialog.showChooseBackupLocationDialog(this, 26212);
        } else {
            BackupDialog.showDisableBackupDialog(requireContext(), new BackupsPreferenceFragment$$ExternalSyntheticLambda3(this));
        }
    }

    private void onToggleClickedLegacy() {
        Permissions.with(this).request("android.permission.WRITE_EXTERNAL_STORAGE").ifNecessary().onAllGranted(new Runnable() { // from class: org.thoughtcrime.securesms.preferences.BackupsPreferenceFragment$$ExternalSyntheticLambda5
            @Override // java.lang.Runnable
            public final void run() {
                BackupsPreferenceFragment.this.lambda$onToggleClickedLegacy$3();
            }
        }).withPermanentDenialDialog(getString(R.string.BackupsPreferenceFragment_signal_requires_external_storage_permission_in_order_to_create_backups)).execute();
    }

    public /* synthetic */ void lambda$onToggleClickedLegacy$3() {
        if (!SignalStore.settings().isBackupEnabled()) {
            BackupDialog.showEnableBackupDialog(requireContext(), null, null, new BackupsPreferenceFragment$$ExternalSyntheticLambda6(this));
        } else {
            BackupDialog.showDisableBackupDialog(requireContext(), new BackupsPreferenceFragment$$ExternalSyntheticLambda3(this));
        }
    }

    private void onCreateClicked() {
        if (BackupUtil.isUserSelectionRequired(requireContext())) {
            onCreateClickedApi29();
        } else {
            onCreateClickedLegacy();
        }
    }

    private void onCreateClickedApi29() {
        Log.i(TAG, "Queing backup...");
        LocalBackupJob.enqueue(true);
    }

    private void onCreateClickedLegacy() {
        Permissions.with(this).request("android.permission.WRITE_EXTERNAL_STORAGE").ifNecessary().onAllGranted(new Runnable() { // from class: org.thoughtcrime.securesms.preferences.BackupsPreferenceFragment$$ExternalSyntheticLambda4
            @Override // java.lang.Runnable
            public final void run() {
                BackupsPreferenceFragment.lambda$onCreateClickedLegacy$4();
            }
        }).withPermanentDenialDialog(getString(R.string.BackupsPreferenceFragment_signal_requires_external_storage_permission_in_order_to_create_backups)).execute();
    }

    public static /* synthetic */ void lambda$onCreateClickedLegacy$4() {
        Log.i(TAG, "Queuing backup...");
        LocalBackupJob.enqueue(true);
    }

    public void setBackupsEnabled() {
        this.toggle.setText(R.string.BackupsPreferenceFragment__turn_off);
        this.create.setVisibility(0);
        this.verify.setVisibility(0);
        setBackupFolderName();
    }

    public void setBackupsDisabled() {
        this.toggle.setText(R.string.BackupsPreferenceFragment__turn_on);
        this.create.setVisibility(8);
        this.folder.setVisibility(8);
        this.verify.setVisibility(8);
        ApplicationDependencies.getJobManager().cancelAllInQueue(LocalBackupJob.QUEUE);
    }
}
