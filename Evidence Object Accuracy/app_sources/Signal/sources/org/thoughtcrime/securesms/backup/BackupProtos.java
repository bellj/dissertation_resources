package org.thoughtcrime.securesms.backup;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;

/* loaded from: classes.dex */
public final class BackupProtos {

    /* loaded from: classes.dex */
    public interface AttachmentOrBuilder extends MessageLiteOrBuilder {
        long getAttachmentId();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        int getLength();

        long getRowId();

        boolean hasAttachmentId();

        boolean hasLength();

        boolean hasRowId();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes.dex */
    public interface AvatarOrBuilder extends MessageLiteOrBuilder {
        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        int getLength();

        String getName();

        ByteString getNameBytes();

        String getRecipientId();

        ByteString getRecipientIdBytes();

        boolean hasLength();

        boolean hasName();

        boolean hasRecipientId();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes.dex */
    public interface BackupFrameOrBuilder extends MessageLiteOrBuilder {
        Attachment getAttachment();

        Avatar getAvatar();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        boolean getEnd();

        Header getHeader();

        KeyValue getKeyValue();

        SharedPreference getPreference();

        SqlStatement getStatement();

        Sticker getSticker();

        DatabaseVersion getVersion();

        boolean hasAttachment();

        boolean hasAvatar();

        boolean hasEnd();

        boolean hasHeader();

        boolean hasKeyValue();

        boolean hasPreference();

        boolean hasStatement();

        boolean hasSticker();

        boolean hasVersion();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes.dex */
    public interface DatabaseVersionOrBuilder extends MessageLiteOrBuilder {
        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        int getVersion();

        boolean hasVersion();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes.dex */
    public interface HeaderOrBuilder extends MessageLiteOrBuilder {
        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        ByteString getIv();

        ByteString getSalt();

        boolean hasIv();

        boolean hasSalt();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes.dex */
    public interface KeyValueOrBuilder extends MessageLiteOrBuilder {
        ByteString getBlobValue();

        boolean getBooleanValue();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        float getFloatValue();

        int getIntegerValue();

        String getKey();

        ByteString getKeyBytes();

        long getLongValue();

        String getStringValue();

        ByteString getStringValueBytes();

        boolean hasBlobValue();

        boolean hasBooleanValue();

        boolean hasFloatValue();

        boolean hasIntegerValue();

        boolean hasKey();

        boolean hasLongValue();

        boolean hasStringValue();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes.dex */
    public interface SharedPreferenceOrBuilder extends MessageLiteOrBuilder {
        boolean getBooleanValue();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        String getFile();

        ByteString getFileBytes();

        boolean getIsStringSetValue();

        String getKey();

        ByteString getKeyBytes();

        String getStringSetValue(int i);

        ByteString getStringSetValueBytes(int i);

        int getStringSetValueCount();

        List<String> getStringSetValueList();

        String getValue();

        ByteString getValueBytes();

        boolean hasBooleanValue();

        boolean hasFile();

        boolean hasIsStringSetValue();

        boolean hasKey();

        boolean hasValue();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes.dex */
    public interface SqlStatementOrBuilder extends MessageLiteOrBuilder {
        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        SqlStatement.SqlParameter getParameters(int i);

        int getParametersCount();

        List<SqlStatement.SqlParameter> getParametersList();

        String getStatement();

        ByteString getStatementBytes();

        boolean hasStatement();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    /* loaded from: classes.dex */
    public interface StickerOrBuilder extends MessageLiteOrBuilder {
        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        int getLength();

        long getRowId();

        boolean hasLength();

        boolean hasRowId();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    public static void registerAllExtensions(ExtensionRegistryLite extensionRegistryLite) {
    }

    private BackupProtos() {
    }

    /* loaded from: classes.dex */
    public static final class SqlStatement extends GeneratedMessageLite<SqlStatement, Builder> implements SqlStatementOrBuilder {
        private static final SqlStatement DEFAULT_INSTANCE;
        public static final int PARAMETERS_FIELD_NUMBER;
        private static volatile Parser<SqlStatement> PARSER;
        public static final int STATEMENT_FIELD_NUMBER;
        private int bitField0_;
        private Internal.ProtobufList<SqlParameter> parameters_ = GeneratedMessageLite.emptyProtobufList();
        private String statement_ = "";

        /* loaded from: classes.dex */
        public interface SqlParameterOrBuilder extends MessageLiteOrBuilder {
            ByteString getBlobParameter();

            @Override // com.google.protobuf.MessageLiteOrBuilder
            /* synthetic */ MessageLite getDefaultInstanceForType();

            double getDoubleParameter();

            long getIntegerParameter();

            boolean getNullparameter();

            String getStringParamter();

            ByteString getStringParamterBytes();

            boolean hasBlobParameter();

            boolean hasDoubleParameter();

            boolean hasIntegerParameter();

            boolean hasNullparameter();

            boolean hasStringParamter();

            @Override // com.google.protobuf.MessageLiteOrBuilder
            /* synthetic */ boolean isInitialized();
        }

        private SqlStatement() {
        }

        /* loaded from: classes.dex */
        public static final class SqlParameter extends GeneratedMessageLite<SqlParameter, Builder> implements SqlParameterOrBuilder {
            public static final int BLOBPARAMETER_FIELD_NUMBER;
            private static final SqlParameter DEFAULT_INSTANCE;
            public static final int DOUBLEPARAMETER_FIELD_NUMBER;
            public static final int INTEGERPARAMETER_FIELD_NUMBER;
            public static final int NULLPARAMETER_FIELD_NUMBER;
            private static volatile Parser<SqlParameter> PARSER;
            public static final int STRINGPARAMTER_FIELD_NUMBER;
            private int bitField0_;
            private ByteString blobParameter_ = ByteString.EMPTY;
            private double doubleParameter_;
            private long integerParameter_;
            private boolean nullparameter_;
            private String stringParamter_ = "";

            private SqlParameter() {
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatement.SqlParameterOrBuilder
            public boolean hasStringParamter() {
                return (this.bitField0_ & 1) != 0;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatement.SqlParameterOrBuilder
            public String getStringParamter() {
                return this.stringParamter_;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatement.SqlParameterOrBuilder
            public ByteString getStringParamterBytes() {
                return ByteString.copyFromUtf8(this.stringParamter_);
            }

            public void setStringParamter(String str) {
                str.getClass();
                this.bitField0_ |= 1;
                this.stringParamter_ = str;
            }

            public void clearStringParamter() {
                this.bitField0_ &= -2;
                this.stringParamter_ = getDefaultInstance().getStringParamter();
            }

            public void setStringParamterBytes(ByteString byteString) {
                this.stringParamter_ = byteString.toStringUtf8();
                this.bitField0_ |= 1;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatement.SqlParameterOrBuilder
            public boolean hasIntegerParameter() {
                return (this.bitField0_ & 2) != 0;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatement.SqlParameterOrBuilder
            public long getIntegerParameter() {
                return this.integerParameter_;
            }

            public void setIntegerParameter(long j) {
                this.bitField0_ |= 2;
                this.integerParameter_ = j;
            }

            public void clearIntegerParameter() {
                this.bitField0_ &= -3;
                this.integerParameter_ = 0;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatement.SqlParameterOrBuilder
            public boolean hasDoubleParameter() {
                return (this.bitField0_ & 4) != 0;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatement.SqlParameterOrBuilder
            public double getDoubleParameter() {
                return this.doubleParameter_;
            }

            public void setDoubleParameter(double d) {
                this.bitField0_ |= 4;
                this.doubleParameter_ = d;
            }

            public void clearDoubleParameter() {
                this.bitField0_ &= -5;
                this.doubleParameter_ = 0.0d;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatement.SqlParameterOrBuilder
            public boolean hasBlobParameter() {
                return (this.bitField0_ & 8) != 0;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatement.SqlParameterOrBuilder
            public ByteString getBlobParameter() {
                return this.blobParameter_;
            }

            public void setBlobParameter(ByteString byteString) {
                byteString.getClass();
                this.bitField0_ |= 8;
                this.blobParameter_ = byteString;
            }

            public void clearBlobParameter() {
                this.bitField0_ &= -9;
                this.blobParameter_ = getDefaultInstance().getBlobParameter();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatement.SqlParameterOrBuilder
            public boolean hasNullparameter() {
                return (this.bitField0_ & 16) != 0;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatement.SqlParameterOrBuilder
            public boolean getNullparameter() {
                return this.nullparameter_;
            }

            public void setNullparameter(boolean z) {
                this.bitField0_ |= 16;
                this.nullparameter_ = z;
            }

            public void clearNullparameter() {
                this.bitField0_ &= -17;
                this.nullparameter_ = false;
            }

            public static SqlParameter parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
                return (SqlParameter) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
            }

            public static SqlParameter parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (SqlParameter) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
            }

            public static SqlParameter parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
                return (SqlParameter) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
            }

            public static SqlParameter parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (SqlParameter) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
            }

            public static SqlParameter parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
                return (SqlParameter) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
            }

            public static SqlParameter parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
                return (SqlParameter) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
            }

            public static SqlParameter parseFrom(InputStream inputStream) throws IOException {
                return (SqlParameter) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static SqlParameter parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (SqlParameter) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static SqlParameter parseDelimitedFrom(InputStream inputStream) throws IOException {
                return (SqlParameter) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
            }

            public static SqlParameter parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (SqlParameter) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
            }

            public static SqlParameter parseFrom(CodedInputStream codedInputStream) throws IOException {
                return (SqlParameter) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
            }

            public static SqlParameter parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
                return (SqlParameter) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
            }

            public static Builder newBuilder() {
                return DEFAULT_INSTANCE.createBuilder();
            }

            public static Builder newBuilder(SqlParameter sqlParameter) {
                return DEFAULT_INSTANCE.createBuilder(sqlParameter);
            }

            /* loaded from: classes.dex */
            public static final class Builder extends GeneratedMessageLite.Builder<SqlParameter, Builder> implements SqlParameterOrBuilder {
                /* synthetic */ Builder(AnonymousClass1 r1) {
                    this();
                }

                private Builder() {
                    super(SqlParameter.DEFAULT_INSTANCE);
                }

                @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatement.SqlParameterOrBuilder
                public boolean hasStringParamter() {
                    return ((SqlParameter) this.instance).hasStringParamter();
                }

                @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatement.SqlParameterOrBuilder
                public String getStringParamter() {
                    return ((SqlParameter) this.instance).getStringParamter();
                }

                @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatement.SqlParameterOrBuilder
                public ByteString getStringParamterBytes() {
                    return ((SqlParameter) this.instance).getStringParamterBytes();
                }

                public Builder setStringParamter(String str) {
                    copyOnWrite();
                    ((SqlParameter) this.instance).setStringParamter(str);
                    return this;
                }

                public Builder clearStringParamter() {
                    copyOnWrite();
                    ((SqlParameter) this.instance).clearStringParamter();
                    return this;
                }

                public Builder setStringParamterBytes(ByteString byteString) {
                    copyOnWrite();
                    ((SqlParameter) this.instance).setStringParamterBytes(byteString);
                    return this;
                }

                @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatement.SqlParameterOrBuilder
                public boolean hasIntegerParameter() {
                    return ((SqlParameter) this.instance).hasIntegerParameter();
                }

                @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatement.SqlParameterOrBuilder
                public long getIntegerParameter() {
                    return ((SqlParameter) this.instance).getIntegerParameter();
                }

                public Builder setIntegerParameter(long j) {
                    copyOnWrite();
                    ((SqlParameter) this.instance).setIntegerParameter(j);
                    return this;
                }

                public Builder clearIntegerParameter() {
                    copyOnWrite();
                    ((SqlParameter) this.instance).clearIntegerParameter();
                    return this;
                }

                @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatement.SqlParameterOrBuilder
                public boolean hasDoubleParameter() {
                    return ((SqlParameter) this.instance).hasDoubleParameter();
                }

                @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatement.SqlParameterOrBuilder
                public double getDoubleParameter() {
                    return ((SqlParameter) this.instance).getDoubleParameter();
                }

                public Builder setDoubleParameter(double d) {
                    copyOnWrite();
                    ((SqlParameter) this.instance).setDoubleParameter(d);
                    return this;
                }

                public Builder clearDoubleParameter() {
                    copyOnWrite();
                    ((SqlParameter) this.instance).clearDoubleParameter();
                    return this;
                }

                @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatement.SqlParameterOrBuilder
                public boolean hasBlobParameter() {
                    return ((SqlParameter) this.instance).hasBlobParameter();
                }

                @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatement.SqlParameterOrBuilder
                public ByteString getBlobParameter() {
                    return ((SqlParameter) this.instance).getBlobParameter();
                }

                public Builder setBlobParameter(ByteString byteString) {
                    copyOnWrite();
                    ((SqlParameter) this.instance).setBlobParameter(byteString);
                    return this;
                }

                public Builder clearBlobParameter() {
                    copyOnWrite();
                    ((SqlParameter) this.instance).clearBlobParameter();
                    return this;
                }

                @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatement.SqlParameterOrBuilder
                public boolean hasNullparameter() {
                    return ((SqlParameter) this.instance).hasNullparameter();
                }

                @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatement.SqlParameterOrBuilder
                public boolean getNullparameter() {
                    return ((SqlParameter) this.instance).getNullparameter();
                }

                public Builder setNullparameter(boolean z) {
                    copyOnWrite();
                    ((SqlParameter) this.instance).setNullparameter(z);
                    return this;
                }

                public Builder clearNullparameter() {
                    copyOnWrite();
                    ((SqlParameter) this.instance).clearNullparameter();
                    return this;
                }
            }

            @Override // com.google.protobuf.GeneratedMessageLite
            protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
                switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                    case 1:
                        return new SqlParameter();
                    case 2:
                        return new Builder(null);
                    case 3:
                        return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001ဈ\u0000\u0002ဃ\u0001\u0003က\u0002\u0004ည\u0003\u0005ဇ\u0004", new Object[]{"bitField0_", "stringParamter_", "integerParameter_", "doubleParameter_", "blobParameter_", "nullparameter_"});
                    case 4:
                        return DEFAULT_INSTANCE;
                    case 5:
                        Parser<SqlParameter> parser = PARSER;
                        if (parser == null) {
                            synchronized (SqlParameter.class) {
                                parser = PARSER;
                                if (parser == null) {
                                    parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                    PARSER = parser;
                                }
                            }
                        }
                        return parser;
                    case 6:
                        return (byte) 1;
                    case 7:
                        return null;
                    default:
                        throw new UnsupportedOperationException();
                }
            }

            static {
                SqlParameter sqlParameter = new SqlParameter();
                DEFAULT_INSTANCE = sqlParameter;
                GeneratedMessageLite.registerDefaultInstance(SqlParameter.class, sqlParameter);
            }

            public static SqlParameter getDefaultInstance() {
                return DEFAULT_INSTANCE;
            }

            public static Parser<SqlParameter> parser() {
                return DEFAULT_INSTANCE.getParserForType();
            }
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatementOrBuilder
        public boolean hasStatement() {
            return (this.bitField0_ & 1) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatementOrBuilder
        public String getStatement() {
            return this.statement_;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatementOrBuilder
        public ByteString getStatementBytes() {
            return ByteString.copyFromUtf8(this.statement_);
        }

        public void setStatement(String str) {
            str.getClass();
            this.bitField0_ |= 1;
            this.statement_ = str;
        }

        public void clearStatement() {
            this.bitField0_ &= -2;
            this.statement_ = getDefaultInstance().getStatement();
        }

        public void setStatementBytes(ByteString byteString) {
            this.statement_ = byteString.toStringUtf8();
            this.bitField0_ |= 1;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatementOrBuilder
        public List<SqlParameter> getParametersList() {
            return this.parameters_;
        }

        public List<? extends SqlParameterOrBuilder> getParametersOrBuilderList() {
            return this.parameters_;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatementOrBuilder
        public int getParametersCount() {
            return this.parameters_.size();
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatementOrBuilder
        public SqlParameter getParameters(int i) {
            return this.parameters_.get(i);
        }

        public SqlParameterOrBuilder getParametersOrBuilder(int i) {
            return this.parameters_.get(i);
        }

        private void ensureParametersIsMutable() {
            Internal.ProtobufList<SqlParameter> protobufList = this.parameters_;
            if (!protobufList.isModifiable()) {
                this.parameters_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setParameters(int i, SqlParameter sqlParameter) {
            sqlParameter.getClass();
            ensureParametersIsMutable();
            this.parameters_.set(i, sqlParameter);
        }

        public void addParameters(SqlParameter sqlParameter) {
            sqlParameter.getClass();
            ensureParametersIsMutable();
            this.parameters_.add(sqlParameter);
        }

        public void addParameters(int i, SqlParameter sqlParameter) {
            sqlParameter.getClass();
            ensureParametersIsMutable();
            this.parameters_.add(i, sqlParameter);
        }

        public void addAllParameters(Iterable<? extends SqlParameter> iterable) {
            ensureParametersIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.parameters_);
        }

        public void clearParameters() {
            this.parameters_ = GeneratedMessageLite.emptyProtobufList();
        }

        public void removeParameters(int i) {
            ensureParametersIsMutable();
            this.parameters_.remove(i);
        }

        public static SqlStatement parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (SqlStatement) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static SqlStatement parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (SqlStatement) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static SqlStatement parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (SqlStatement) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static SqlStatement parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (SqlStatement) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static SqlStatement parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (SqlStatement) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static SqlStatement parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (SqlStatement) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static SqlStatement parseFrom(InputStream inputStream) throws IOException {
            return (SqlStatement) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static SqlStatement parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (SqlStatement) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static SqlStatement parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SqlStatement) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static SqlStatement parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (SqlStatement) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static SqlStatement parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (SqlStatement) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static SqlStatement parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (SqlStatement) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(SqlStatement sqlStatement) {
            return DEFAULT_INSTANCE.createBuilder(sqlStatement);
        }

        /* loaded from: classes.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<SqlStatement, Builder> implements SqlStatementOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(SqlStatement.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatementOrBuilder
            public boolean hasStatement() {
                return ((SqlStatement) this.instance).hasStatement();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatementOrBuilder
            public String getStatement() {
                return ((SqlStatement) this.instance).getStatement();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatementOrBuilder
            public ByteString getStatementBytes() {
                return ((SqlStatement) this.instance).getStatementBytes();
            }

            public Builder setStatement(String str) {
                copyOnWrite();
                ((SqlStatement) this.instance).setStatement(str);
                return this;
            }

            public Builder clearStatement() {
                copyOnWrite();
                ((SqlStatement) this.instance).clearStatement();
                return this;
            }

            public Builder setStatementBytes(ByteString byteString) {
                copyOnWrite();
                ((SqlStatement) this.instance).setStatementBytes(byteString);
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatementOrBuilder
            public List<SqlParameter> getParametersList() {
                return Collections.unmodifiableList(((SqlStatement) this.instance).getParametersList());
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatementOrBuilder
            public int getParametersCount() {
                return ((SqlStatement) this.instance).getParametersCount();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SqlStatementOrBuilder
            public SqlParameter getParameters(int i) {
                return ((SqlStatement) this.instance).getParameters(i);
            }

            public Builder setParameters(int i, SqlParameter sqlParameter) {
                copyOnWrite();
                ((SqlStatement) this.instance).setParameters(i, sqlParameter);
                return this;
            }

            public Builder setParameters(int i, SqlParameter.Builder builder) {
                copyOnWrite();
                ((SqlStatement) this.instance).setParameters(i, builder.build());
                return this;
            }

            public Builder addParameters(SqlParameter sqlParameter) {
                copyOnWrite();
                ((SqlStatement) this.instance).addParameters(sqlParameter);
                return this;
            }

            public Builder addParameters(int i, SqlParameter sqlParameter) {
                copyOnWrite();
                ((SqlStatement) this.instance).addParameters(i, sqlParameter);
                return this;
            }

            public Builder addParameters(SqlParameter.Builder builder) {
                copyOnWrite();
                ((SqlStatement) this.instance).addParameters(builder.build());
                return this;
            }

            public Builder addParameters(int i, SqlParameter.Builder builder) {
                copyOnWrite();
                ((SqlStatement) this.instance).addParameters(i, builder.build());
                return this;
            }

            public Builder addAllParameters(Iterable<? extends SqlParameter> iterable) {
                copyOnWrite();
                ((SqlStatement) this.instance).addAllParameters(iterable);
                return this;
            }

            public Builder clearParameters() {
                copyOnWrite();
                ((SqlStatement) this.instance).clearParameters();
                return this;
            }

            public Builder removeParameters(int i) {
                copyOnWrite();
                ((SqlStatement) this.instance).removeParameters(i);
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new SqlStatement();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001ဈ\u0000\u0002\u001b", new Object[]{"bitField0_", "statement_", "parameters_", SqlParameter.class});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<SqlStatement> parser = PARSER;
                    if (parser == null) {
                        synchronized (SqlStatement.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            SqlStatement sqlStatement = new SqlStatement();
            DEFAULT_INSTANCE = sqlStatement;
            GeneratedMessageLite.registerDefaultInstance(SqlStatement.class, sqlStatement);
        }

        public static SqlStatement getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<SqlStatement> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* renamed from: org.thoughtcrime.securesms.backup.BackupProtos$1 */
    /* loaded from: classes.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    /* loaded from: classes.dex */
    public static final class SharedPreference extends GeneratedMessageLite<SharedPreference, Builder> implements SharedPreferenceOrBuilder {
        public static final int BOOLEANVALUE_FIELD_NUMBER;
        private static final SharedPreference DEFAULT_INSTANCE;
        public static final int FILE_FIELD_NUMBER;
        public static final int ISSTRINGSETVALUE_FIELD_NUMBER;
        public static final int KEY_FIELD_NUMBER;
        private static volatile Parser<SharedPreference> PARSER;
        public static final int STRINGSETVALUE_FIELD_NUMBER;
        public static final int VALUE_FIELD_NUMBER;
        private int bitField0_;
        private boolean booleanValue_;
        private String file_ = "";
        private boolean isStringSetValue_;
        private String key_ = "";
        private Internal.ProtobufList<String> stringSetValue_ = GeneratedMessageLite.emptyProtobufList();
        private String value_ = "";

        private SharedPreference() {
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
        public boolean hasFile() {
            return (this.bitField0_ & 1) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
        public String getFile() {
            return this.file_;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
        public ByteString getFileBytes() {
            return ByteString.copyFromUtf8(this.file_);
        }

        public void setFile(String str) {
            str.getClass();
            this.bitField0_ |= 1;
            this.file_ = str;
        }

        public void clearFile() {
            this.bitField0_ &= -2;
            this.file_ = getDefaultInstance().getFile();
        }

        public void setFileBytes(ByteString byteString) {
            this.file_ = byteString.toStringUtf8();
            this.bitField0_ |= 1;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
        public boolean hasKey() {
            return (this.bitField0_ & 2) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
        public String getKey() {
            return this.key_;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
        public ByteString getKeyBytes() {
            return ByteString.copyFromUtf8(this.key_);
        }

        public void setKey(String str) {
            str.getClass();
            this.bitField0_ |= 2;
            this.key_ = str;
        }

        public void clearKey() {
            this.bitField0_ &= -3;
            this.key_ = getDefaultInstance().getKey();
        }

        public void setKeyBytes(ByteString byteString) {
            this.key_ = byteString.toStringUtf8();
            this.bitField0_ |= 2;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
        public boolean hasValue() {
            return (this.bitField0_ & 4) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
        public String getValue() {
            return this.value_;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
        public ByteString getValueBytes() {
            return ByteString.copyFromUtf8(this.value_);
        }

        public void setValue(String str) {
            str.getClass();
            this.bitField0_ |= 4;
            this.value_ = str;
        }

        public void clearValue() {
            this.bitField0_ &= -5;
            this.value_ = getDefaultInstance().getValue();
        }

        public void setValueBytes(ByteString byteString) {
            this.value_ = byteString.toStringUtf8();
            this.bitField0_ |= 4;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
        public boolean hasBooleanValue() {
            return (this.bitField0_ & 8) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
        public boolean getBooleanValue() {
            return this.booleanValue_;
        }

        public void setBooleanValue(boolean z) {
            this.bitField0_ |= 8;
            this.booleanValue_ = z;
        }

        public void clearBooleanValue() {
            this.bitField0_ &= -9;
            this.booleanValue_ = false;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
        public List<String> getStringSetValueList() {
            return this.stringSetValue_;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
        public int getStringSetValueCount() {
            return this.stringSetValue_.size();
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
        public String getStringSetValue(int i) {
            return this.stringSetValue_.get(i);
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
        public ByteString getStringSetValueBytes(int i) {
            return ByteString.copyFromUtf8(this.stringSetValue_.get(i));
        }

        private void ensureStringSetValueIsMutable() {
            Internal.ProtobufList<String> protobufList = this.stringSetValue_;
            if (!protobufList.isModifiable()) {
                this.stringSetValue_ = GeneratedMessageLite.mutableCopy(protobufList);
            }
        }

        public void setStringSetValue(int i, String str) {
            str.getClass();
            ensureStringSetValueIsMutable();
            this.stringSetValue_.set(i, str);
        }

        public void addStringSetValue(String str) {
            str.getClass();
            ensureStringSetValueIsMutable();
            this.stringSetValue_.add(str);
        }

        public void addAllStringSetValue(Iterable<String> iterable) {
            ensureStringSetValueIsMutable();
            AbstractMessageLite.addAll((Iterable) iterable, (List) this.stringSetValue_);
        }

        public void clearStringSetValue() {
            this.stringSetValue_ = GeneratedMessageLite.emptyProtobufList();
        }

        public void addStringSetValueBytes(ByteString byteString) {
            ensureStringSetValueIsMutable();
            this.stringSetValue_.add(byteString.toStringUtf8());
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
        public boolean hasIsStringSetValue() {
            return (this.bitField0_ & 16) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
        public boolean getIsStringSetValue() {
            return this.isStringSetValue_;
        }

        public void setIsStringSetValue(boolean z) {
            this.bitField0_ |= 16;
            this.isStringSetValue_ = z;
        }

        public void clearIsStringSetValue() {
            this.bitField0_ &= -17;
            this.isStringSetValue_ = false;
        }

        public static SharedPreference parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (SharedPreference) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static SharedPreference parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (SharedPreference) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static SharedPreference parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (SharedPreference) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static SharedPreference parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (SharedPreference) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static SharedPreference parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (SharedPreference) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static SharedPreference parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (SharedPreference) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static SharedPreference parseFrom(InputStream inputStream) throws IOException {
            return (SharedPreference) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static SharedPreference parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (SharedPreference) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static SharedPreference parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (SharedPreference) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static SharedPreference parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (SharedPreference) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static SharedPreference parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (SharedPreference) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static SharedPreference parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (SharedPreference) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(SharedPreference sharedPreference) {
            return DEFAULT_INSTANCE.createBuilder(sharedPreference);
        }

        /* loaded from: classes.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<SharedPreference, Builder> implements SharedPreferenceOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(SharedPreference.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
            public boolean hasFile() {
                return ((SharedPreference) this.instance).hasFile();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
            public String getFile() {
                return ((SharedPreference) this.instance).getFile();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
            public ByteString getFileBytes() {
                return ((SharedPreference) this.instance).getFileBytes();
            }

            public Builder setFile(String str) {
                copyOnWrite();
                ((SharedPreference) this.instance).setFile(str);
                return this;
            }

            public Builder clearFile() {
                copyOnWrite();
                ((SharedPreference) this.instance).clearFile();
                return this;
            }

            public Builder setFileBytes(ByteString byteString) {
                copyOnWrite();
                ((SharedPreference) this.instance).setFileBytes(byteString);
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
            public boolean hasKey() {
                return ((SharedPreference) this.instance).hasKey();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
            public String getKey() {
                return ((SharedPreference) this.instance).getKey();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
            public ByteString getKeyBytes() {
                return ((SharedPreference) this.instance).getKeyBytes();
            }

            public Builder setKey(String str) {
                copyOnWrite();
                ((SharedPreference) this.instance).setKey(str);
                return this;
            }

            public Builder clearKey() {
                copyOnWrite();
                ((SharedPreference) this.instance).clearKey();
                return this;
            }

            public Builder setKeyBytes(ByteString byteString) {
                copyOnWrite();
                ((SharedPreference) this.instance).setKeyBytes(byteString);
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
            public boolean hasValue() {
                return ((SharedPreference) this.instance).hasValue();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
            public String getValue() {
                return ((SharedPreference) this.instance).getValue();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
            public ByteString getValueBytes() {
                return ((SharedPreference) this.instance).getValueBytes();
            }

            public Builder setValue(String str) {
                copyOnWrite();
                ((SharedPreference) this.instance).setValue(str);
                return this;
            }

            public Builder clearValue() {
                copyOnWrite();
                ((SharedPreference) this.instance).clearValue();
                return this;
            }

            public Builder setValueBytes(ByteString byteString) {
                copyOnWrite();
                ((SharedPreference) this.instance).setValueBytes(byteString);
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
            public boolean hasBooleanValue() {
                return ((SharedPreference) this.instance).hasBooleanValue();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
            public boolean getBooleanValue() {
                return ((SharedPreference) this.instance).getBooleanValue();
            }

            public Builder setBooleanValue(boolean z) {
                copyOnWrite();
                ((SharedPreference) this.instance).setBooleanValue(z);
                return this;
            }

            public Builder clearBooleanValue() {
                copyOnWrite();
                ((SharedPreference) this.instance).clearBooleanValue();
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
            public List<String> getStringSetValueList() {
                return Collections.unmodifiableList(((SharedPreference) this.instance).getStringSetValueList());
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
            public int getStringSetValueCount() {
                return ((SharedPreference) this.instance).getStringSetValueCount();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
            public String getStringSetValue(int i) {
                return ((SharedPreference) this.instance).getStringSetValue(i);
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
            public ByteString getStringSetValueBytes(int i) {
                return ((SharedPreference) this.instance).getStringSetValueBytes(i);
            }

            public Builder setStringSetValue(int i, String str) {
                copyOnWrite();
                ((SharedPreference) this.instance).setStringSetValue(i, str);
                return this;
            }

            public Builder addStringSetValue(String str) {
                copyOnWrite();
                ((SharedPreference) this.instance).addStringSetValue(str);
                return this;
            }

            public Builder addAllStringSetValue(Iterable<String> iterable) {
                copyOnWrite();
                ((SharedPreference) this.instance).addAllStringSetValue(iterable);
                return this;
            }

            public Builder clearStringSetValue() {
                copyOnWrite();
                ((SharedPreference) this.instance).clearStringSetValue();
                return this;
            }

            public Builder addStringSetValueBytes(ByteString byteString) {
                copyOnWrite();
                ((SharedPreference) this.instance).addStringSetValueBytes(byteString);
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
            public boolean hasIsStringSetValue() {
                return ((SharedPreference) this.instance).hasIsStringSetValue();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.SharedPreferenceOrBuilder
            public boolean getIsStringSetValue() {
                return ((SharedPreference) this.instance).getIsStringSetValue();
            }

            public Builder setIsStringSetValue(boolean z) {
                copyOnWrite();
                ((SharedPreference) this.instance).setIsStringSetValue(z);
                return this;
            }

            public Builder clearIsStringSetValue() {
                copyOnWrite();
                ((SharedPreference) this.instance).clearIsStringSetValue();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new SharedPreference();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0001\u0000\u0001ဈ\u0000\u0002ဈ\u0001\u0003ဈ\u0002\u0004ဇ\u0003\u0005\u001a\u0006ဇ\u0004", new Object[]{"bitField0_", "file_", "key_", "value_", "booleanValue_", "stringSetValue_", "isStringSetValue_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<SharedPreference> parser = PARSER;
                    if (parser == null) {
                        synchronized (SharedPreference.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            SharedPreference sharedPreference = new SharedPreference();
            DEFAULT_INSTANCE = sharedPreference;
            GeneratedMessageLite.registerDefaultInstance(SharedPreference.class, sharedPreference);
        }

        public static SharedPreference getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<SharedPreference> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* loaded from: classes.dex */
    public static final class Attachment extends GeneratedMessageLite<Attachment, Builder> implements AttachmentOrBuilder {
        public static final int ATTACHMENTID_FIELD_NUMBER;
        private static final Attachment DEFAULT_INSTANCE;
        public static final int LENGTH_FIELD_NUMBER;
        private static volatile Parser<Attachment> PARSER;
        public static final int ROWID_FIELD_NUMBER;
        private long attachmentId_;
        private int bitField0_;
        private int length_;
        private long rowId_;

        private Attachment() {
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.AttachmentOrBuilder
        public boolean hasRowId() {
            return (this.bitField0_ & 1) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.AttachmentOrBuilder
        public long getRowId() {
            return this.rowId_;
        }

        public void setRowId(long j) {
            this.bitField0_ |= 1;
            this.rowId_ = j;
        }

        public void clearRowId() {
            this.bitField0_ &= -2;
            this.rowId_ = 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.AttachmentOrBuilder
        public boolean hasAttachmentId() {
            return (this.bitField0_ & 2) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.AttachmentOrBuilder
        public long getAttachmentId() {
            return this.attachmentId_;
        }

        public void setAttachmentId(long j) {
            this.bitField0_ |= 2;
            this.attachmentId_ = j;
        }

        public void clearAttachmentId() {
            this.bitField0_ &= -3;
            this.attachmentId_ = 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.AttachmentOrBuilder
        public boolean hasLength() {
            return (this.bitField0_ & 4) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.AttachmentOrBuilder
        public int getLength() {
            return this.length_;
        }

        public void setLength(int i) {
            this.bitField0_ |= 4;
            this.length_ = i;
        }

        public void clearLength() {
            this.bitField0_ &= -5;
            this.length_ = 0;
        }

        public static Attachment parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (Attachment) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static Attachment parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Attachment) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static Attachment parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (Attachment) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static Attachment parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Attachment) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static Attachment parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (Attachment) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static Attachment parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Attachment) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static Attachment parseFrom(InputStream inputStream) throws IOException {
            return (Attachment) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Attachment parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Attachment) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Attachment parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Attachment) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Attachment parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Attachment) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Attachment parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (Attachment) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static Attachment parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Attachment) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(Attachment attachment) {
            return DEFAULT_INSTANCE.createBuilder(attachment);
        }

        /* loaded from: classes.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<Attachment, Builder> implements AttachmentOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(Attachment.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.AttachmentOrBuilder
            public boolean hasRowId() {
                return ((Attachment) this.instance).hasRowId();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.AttachmentOrBuilder
            public long getRowId() {
                return ((Attachment) this.instance).getRowId();
            }

            public Builder setRowId(long j) {
                copyOnWrite();
                ((Attachment) this.instance).setRowId(j);
                return this;
            }

            public Builder clearRowId() {
                copyOnWrite();
                ((Attachment) this.instance).clearRowId();
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.AttachmentOrBuilder
            public boolean hasAttachmentId() {
                return ((Attachment) this.instance).hasAttachmentId();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.AttachmentOrBuilder
            public long getAttachmentId() {
                return ((Attachment) this.instance).getAttachmentId();
            }

            public Builder setAttachmentId(long j) {
                copyOnWrite();
                ((Attachment) this.instance).setAttachmentId(j);
                return this;
            }

            public Builder clearAttachmentId() {
                copyOnWrite();
                ((Attachment) this.instance).clearAttachmentId();
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.AttachmentOrBuilder
            public boolean hasLength() {
                return ((Attachment) this.instance).hasLength();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.AttachmentOrBuilder
            public int getLength() {
                return ((Attachment) this.instance).getLength();
            }

            public Builder setLength(int i) {
                copyOnWrite();
                ((Attachment) this.instance).setLength(i);
                return this;
            }

            public Builder clearLength() {
                copyOnWrite();
                ((Attachment) this.instance).clearLength();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new Attachment();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဃ\u0000\u0002ဃ\u0001\u0003ဋ\u0002", new Object[]{"bitField0_", "rowId_", "attachmentId_", "length_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<Attachment> parser = PARSER;
                    if (parser == null) {
                        synchronized (Attachment.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            Attachment attachment = new Attachment();
            DEFAULT_INSTANCE = attachment;
            GeneratedMessageLite.registerDefaultInstance(Attachment.class, attachment);
        }

        public static Attachment getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<Attachment> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* loaded from: classes.dex */
    public static final class Sticker extends GeneratedMessageLite<Sticker, Builder> implements StickerOrBuilder {
        private static final Sticker DEFAULT_INSTANCE;
        public static final int LENGTH_FIELD_NUMBER;
        private static volatile Parser<Sticker> PARSER;
        public static final int ROWID_FIELD_NUMBER;
        private int bitField0_;
        private int length_;
        private long rowId_;

        private Sticker() {
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.StickerOrBuilder
        public boolean hasRowId() {
            return (this.bitField0_ & 1) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.StickerOrBuilder
        public long getRowId() {
            return this.rowId_;
        }

        public void setRowId(long j) {
            this.bitField0_ |= 1;
            this.rowId_ = j;
        }

        public void clearRowId() {
            this.bitField0_ &= -2;
            this.rowId_ = 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.StickerOrBuilder
        public boolean hasLength() {
            return (this.bitField0_ & 2) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.StickerOrBuilder
        public int getLength() {
            return this.length_;
        }

        public void setLength(int i) {
            this.bitField0_ |= 2;
            this.length_ = i;
        }

        public void clearLength() {
            this.bitField0_ &= -3;
            this.length_ = 0;
        }

        public static Sticker parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (Sticker) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static Sticker parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Sticker) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static Sticker parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (Sticker) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static Sticker parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Sticker) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static Sticker parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (Sticker) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static Sticker parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Sticker) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static Sticker parseFrom(InputStream inputStream) throws IOException {
            return (Sticker) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Sticker parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Sticker) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Sticker parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Sticker) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Sticker parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Sticker) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Sticker parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (Sticker) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static Sticker parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Sticker) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(Sticker sticker) {
            return DEFAULT_INSTANCE.createBuilder(sticker);
        }

        /* loaded from: classes.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<Sticker, Builder> implements StickerOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(Sticker.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.StickerOrBuilder
            public boolean hasRowId() {
                return ((Sticker) this.instance).hasRowId();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.StickerOrBuilder
            public long getRowId() {
                return ((Sticker) this.instance).getRowId();
            }

            public Builder setRowId(long j) {
                copyOnWrite();
                ((Sticker) this.instance).setRowId(j);
                return this;
            }

            public Builder clearRowId() {
                copyOnWrite();
                ((Sticker) this.instance).clearRowId();
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.StickerOrBuilder
            public boolean hasLength() {
                return ((Sticker) this.instance).hasLength();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.StickerOrBuilder
            public int getLength() {
                return ((Sticker) this.instance).getLength();
            }

            public Builder setLength(int i) {
                copyOnWrite();
                ((Sticker) this.instance).setLength(i);
                return this;
            }

            public Builder clearLength() {
                copyOnWrite();
                ((Sticker) this.instance).clearLength();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new Sticker();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ဃ\u0000\u0002ဋ\u0001", new Object[]{"bitField0_", "rowId_", "length_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<Sticker> parser = PARSER;
                    if (parser == null) {
                        synchronized (Sticker.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            Sticker sticker = new Sticker();
            DEFAULT_INSTANCE = sticker;
            GeneratedMessageLite.registerDefaultInstance(Sticker.class, sticker);
        }

        public static Sticker getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<Sticker> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* loaded from: classes.dex */
    public static final class Avatar extends GeneratedMessageLite<Avatar, Builder> implements AvatarOrBuilder {
        private static final Avatar DEFAULT_INSTANCE;
        public static final int LENGTH_FIELD_NUMBER;
        public static final int NAME_FIELD_NUMBER;
        private static volatile Parser<Avatar> PARSER;
        public static final int RECIPIENTID_FIELD_NUMBER;
        private int bitField0_;
        private int length_;
        private String name_ = "";
        private String recipientId_ = "";

        private Avatar() {
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.AvatarOrBuilder
        public boolean hasName() {
            return (this.bitField0_ & 1) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.AvatarOrBuilder
        public String getName() {
            return this.name_;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.AvatarOrBuilder
        public ByteString getNameBytes() {
            return ByteString.copyFromUtf8(this.name_);
        }

        public void setName(String str) {
            str.getClass();
            this.bitField0_ |= 1;
            this.name_ = str;
        }

        public void clearName() {
            this.bitField0_ &= -2;
            this.name_ = getDefaultInstance().getName();
        }

        public void setNameBytes(ByteString byteString) {
            this.name_ = byteString.toStringUtf8();
            this.bitField0_ |= 1;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.AvatarOrBuilder
        public boolean hasRecipientId() {
            return (this.bitField0_ & 2) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.AvatarOrBuilder
        public String getRecipientId() {
            return this.recipientId_;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.AvatarOrBuilder
        public ByteString getRecipientIdBytes() {
            return ByteString.copyFromUtf8(this.recipientId_);
        }

        public void setRecipientId(String str) {
            str.getClass();
            this.bitField0_ |= 2;
            this.recipientId_ = str;
        }

        public void clearRecipientId() {
            this.bitField0_ &= -3;
            this.recipientId_ = getDefaultInstance().getRecipientId();
        }

        public void setRecipientIdBytes(ByteString byteString) {
            this.recipientId_ = byteString.toStringUtf8();
            this.bitField0_ |= 2;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.AvatarOrBuilder
        public boolean hasLength() {
            return (this.bitField0_ & 4) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.AvatarOrBuilder
        public int getLength() {
            return this.length_;
        }

        public void setLength(int i) {
            this.bitField0_ |= 4;
            this.length_ = i;
        }

        public void clearLength() {
            this.bitField0_ &= -5;
            this.length_ = 0;
        }

        public static Avatar parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (Avatar) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static Avatar parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Avatar) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static Avatar parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (Avatar) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static Avatar parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Avatar) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static Avatar parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (Avatar) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static Avatar parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Avatar) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static Avatar parseFrom(InputStream inputStream) throws IOException {
            return (Avatar) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Avatar parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Avatar) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Avatar parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Avatar) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Avatar parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Avatar) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Avatar parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (Avatar) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static Avatar parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Avatar) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(Avatar avatar) {
            return DEFAULT_INSTANCE.createBuilder(avatar);
        }

        /* loaded from: classes.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<Avatar, Builder> implements AvatarOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(Avatar.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.AvatarOrBuilder
            public boolean hasName() {
                return ((Avatar) this.instance).hasName();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.AvatarOrBuilder
            public String getName() {
                return ((Avatar) this.instance).getName();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.AvatarOrBuilder
            public ByteString getNameBytes() {
                return ((Avatar) this.instance).getNameBytes();
            }

            public Builder setName(String str) {
                copyOnWrite();
                ((Avatar) this.instance).setName(str);
                return this;
            }

            public Builder clearName() {
                copyOnWrite();
                ((Avatar) this.instance).clearName();
                return this;
            }

            public Builder setNameBytes(ByteString byteString) {
                copyOnWrite();
                ((Avatar) this.instance).setNameBytes(byteString);
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.AvatarOrBuilder
            public boolean hasRecipientId() {
                return ((Avatar) this.instance).hasRecipientId();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.AvatarOrBuilder
            public String getRecipientId() {
                return ((Avatar) this.instance).getRecipientId();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.AvatarOrBuilder
            public ByteString getRecipientIdBytes() {
                return ((Avatar) this.instance).getRecipientIdBytes();
            }

            public Builder setRecipientId(String str) {
                copyOnWrite();
                ((Avatar) this.instance).setRecipientId(str);
                return this;
            }

            public Builder clearRecipientId() {
                copyOnWrite();
                ((Avatar) this.instance).clearRecipientId();
                return this;
            }

            public Builder setRecipientIdBytes(ByteString byteString) {
                copyOnWrite();
                ((Avatar) this.instance).setRecipientIdBytes(byteString);
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.AvatarOrBuilder
            public boolean hasLength() {
                return ((Avatar) this.instance).hasLength();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.AvatarOrBuilder
            public int getLength() {
                return ((Avatar) this.instance).getLength();
            }

            public Builder setLength(int i) {
                copyOnWrite();
                ((Avatar) this.instance).setLength(i);
                return this;
            }

            public Builder clearLength() {
                copyOnWrite();
                ((Avatar) this.instance).clearLength();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new Avatar();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ဈ\u0000\u0002ဋ\u0002\u0003ဈ\u0001", new Object[]{"bitField0_", "name_", "length_", "recipientId_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<Avatar> parser = PARSER;
                    if (parser == null) {
                        synchronized (Avatar.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            Avatar avatar = new Avatar();
            DEFAULT_INSTANCE = avatar;
            GeneratedMessageLite.registerDefaultInstance(Avatar.class, avatar);
        }

        public static Avatar getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<Avatar> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* loaded from: classes.dex */
    public static final class DatabaseVersion extends GeneratedMessageLite<DatabaseVersion, Builder> implements DatabaseVersionOrBuilder {
        private static final DatabaseVersion DEFAULT_INSTANCE;
        private static volatile Parser<DatabaseVersion> PARSER;
        public static final int VERSION_FIELD_NUMBER;
        private int bitField0_;
        private int version_;

        private DatabaseVersion() {
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.DatabaseVersionOrBuilder
        public boolean hasVersion() {
            return (this.bitField0_ & 1) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.DatabaseVersionOrBuilder
        public int getVersion() {
            return this.version_;
        }

        public void setVersion(int i) {
            this.bitField0_ |= 1;
            this.version_ = i;
        }

        public void clearVersion() {
            this.bitField0_ &= -2;
            this.version_ = 0;
        }

        public static DatabaseVersion parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (DatabaseVersion) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static DatabaseVersion parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (DatabaseVersion) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static DatabaseVersion parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (DatabaseVersion) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static DatabaseVersion parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (DatabaseVersion) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static DatabaseVersion parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (DatabaseVersion) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static DatabaseVersion parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (DatabaseVersion) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static DatabaseVersion parseFrom(InputStream inputStream) throws IOException {
            return (DatabaseVersion) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static DatabaseVersion parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (DatabaseVersion) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static DatabaseVersion parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (DatabaseVersion) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static DatabaseVersion parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (DatabaseVersion) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static DatabaseVersion parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (DatabaseVersion) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static DatabaseVersion parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (DatabaseVersion) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(DatabaseVersion databaseVersion) {
            return DEFAULT_INSTANCE.createBuilder(databaseVersion);
        }

        /* loaded from: classes.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<DatabaseVersion, Builder> implements DatabaseVersionOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(DatabaseVersion.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.DatabaseVersionOrBuilder
            public boolean hasVersion() {
                return ((DatabaseVersion) this.instance).hasVersion();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.DatabaseVersionOrBuilder
            public int getVersion() {
                return ((DatabaseVersion) this.instance).getVersion();
            }

            public Builder setVersion(int i) {
                copyOnWrite();
                ((DatabaseVersion) this.instance).setVersion(i);
                return this;
            }

            public Builder clearVersion() {
                copyOnWrite();
                ((DatabaseVersion) this.instance).clearVersion();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new DatabaseVersion();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0001\u0000\u0001\u0001\u0001\u0001\u0000\u0000\u0000\u0001ဋ\u0000", new Object[]{"bitField0_", "version_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<DatabaseVersion> parser = PARSER;
                    if (parser == null) {
                        synchronized (DatabaseVersion.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            DatabaseVersion databaseVersion = new DatabaseVersion();
            DEFAULT_INSTANCE = databaseVersion;
            GeneratedMessageLite.registerDefaultInstance(DatabaseVersion.class, databaseVersion);
        }

        public static DatabaseVersion getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<DatabaseVersion> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* loaded from: classes.dex */
    public static final class Header extends GeneratedMessageLite<Header, Builder> implements HeaderOrBuilder {
        private static final Header DEFAULT_INSTANCE;
        public static final int IV_FIELD_NUMBER;
        private static volatile Parser<Header> PARSER;
        public static final int SALT_FIELD_NUMBER;
        private int bitField0_;
        private ByteString iv_;
        private ByteString salt_;

        private Header() {
            ByteString byteString = ByteString.EMPTY;
            this.iv_ = byteString;
            this.salt_ = byteString;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.HeaderOrBuilder
        public boolean hasIv() {
            return (this.bitField0_ & 1) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.HeaderOrBuilder
        public ByteString getIv() {
            return this.iv_;
        }

        public void setIv(ByteString byteString) {
            byteString.getClass();
            this.bitField0_ |= 1;
            this.iv_ = byteString;
        }

        public void clearIv() {
            this.bitField0_ &= -2;
            this.iv_ = getDefaultInstance().getIv();
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.HeaderOrBuilder
        public boolean hasSalt() {
            return (this.bitField0_ & 2) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.HeaderOrBuilder
        public ByteString getSalt() {
            return this.salt_;
        }

        public void setSalt(ByteString byteString) {
            byteString.getClass();
            this.bitField0_ |= 2;
            this.salt_ = byteString;
        }

        public void clearSalt() {
            this.bitField0_ &= -3;
            this.salt_ = getDefaultInstance().getSalt();
        }

        public static Header parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (Header) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static Header parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Header) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static Header parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (Header) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static Header parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Header) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static Header parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (Header) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static Header parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (Header) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static Header parseFrom(InputStream inputStream) throws IOException {
            return (Header) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Header parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Header) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Header parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (Header) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static Header parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Header) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static Header parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (Header) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static Header parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (Header) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(Header header) {
            return DEFAULT_INSTANCE.createBuilder(header);
        }

        /* loaded from: classes.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<Header, Builder> implements HeaderOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(Header.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.HeaderOrBuilder
            public boolean hasIv() {
                return ((Header) this.instance).hasIv();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.HeaderOrBuilder
            public ByteString getIv() {
                return ((Header) this.instance).getIv();
            }

            public Builder setIv(ByteString byteString) {
                copyOnWrite();
                ((Header) this.instance).setIv(byteString);
                return this;
            }

            public Builder clearIv() {
                copyOnWrite();
                ((Header) this.instance).clearIv();
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.HeaderOrBuilder
            public boolean hasSalt() {
                return ((Header) this.instance).hasSalt();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.HeaderOrBuilder
            public ByteString getSalt() {
                return ((Header) this.instance).getSalt();
            }

            public Builder setSalt(ByteString byteString) {
                copyOnWrite();
                ((Header) this.instance).setSalt(byteString);
                return this;
            }

            public Builder clearSalt() {
                copyOnWrite();
                ((Header) this.instance).clearSalt();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new Header();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001ည\u0000\u0002ည\u0001", new Object[]{"bitField0_", "iv_", "salt_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<Header> parser = PARSER;
                    if (parser == null) {
                        synchronized (Header.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            Header header = new Header();
            DEFAULT_INSTANCE = header;
            GeneratedMessageLite.registerDefaultInstance(Header.class, header);
        }

        public static Header getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<Header> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* loaded from: classes.dex */
    public static final class KeyValue extends GeneratedMessageLite<KeyValue, Builder> implements KeyValueOrBuilder {
        public static final int BLOBVALUE_FIELD_NUMBER;
        public static final int BOOLEANVALUE_FIELD_NUMBER;
        private static final KeyValue DEFAULT_INSTANCE;
        public static final int FLOATVALUE_FIELD_NUMBER;
        public static final int INTEGERVALUE_FIELD_NUMBER;
        public static final int KEY_FIELD_NUMBER;
        public static final int LONGVALUE_FIELD_NUMBER;
        private static volatile Parser<KeyValue> PARSER;
        public static final int STRINGVALUE_FIELD_NUMBER;
        private int bitField0_;
        private ByteString blobValue_ = ByteString.EMPTY;
        private boolean booleanValue_;
        private float floatValue_;
        private int integerValue_;
        private String key_ = "";
        private long longValue_;
        private String stringValue_ = "";

        private KeyValue() {
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
        public boolean hasKey() {
            return (this.bitField0_ & 1) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
        public String getKey() {
            return this.key_;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
        public ByteString getKeyBytes() {
            return ByteString.copyFromUtf8(this.key_);
        }

        public void setKey(String str) {
            str.getClass();
            this.bitField0_ |= 1;
            this.key_ = str;
        }

        public void clearKey() {
            this.bitField0_ &= -2;
            this.key_ = getDefaultInstance().getKey();
        }

        public void setKeyBytes(ByteString byteString) {
            this.key_ = byteString.toStringUtf8();
            this.bitField0_ |= 1;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
        public boolean hasBlobValue() {
            return (this.bitField0_ & 2) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
        public ByteString getBlobValue() {
            return this.blobValue_;
        }

        public void setBlobValue(ByteString byteString) {
            byteString.getClass();
            this.bitField0_ |= 2;
            this.blobValue_ = byteString;
        }

        public void clearBlobValue() {
            this.bitField0_ &= -3;
            this.blobValue_ = getDefaultInstance().getBlobValue();
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
        public boolean hasBooleanValue() {
            return (this.bitField0_ & 4) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
        public boolean getBooleanValue() {
            return this.booleanValue_;
        }

        public void setBooleanValue(boolean z) {
            this.bitField0_ |= 4;
            this.booleanValue_ = z;
        }

        public void clearBooleanValue() {
            this.bitField0_ &= -5;
            this.booleanValue_ = false;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
        public boolean hasFloatValue() {
            return (this.bitField0_ & 8) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
        public float getFloatValue() {
            return this.floatValue_;
        }

        public void setFloatValue(float f) {
            this.bitField0_ |= 8;
            this.floatValue_ = f;
        }

        public void clearFloatValue() {
            this.bitField0_ &= -9;
            this.floatValue_ = 0.0f;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
        public boolean hasIntegerValue() {
            return (this.bitField0_ & 16) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
        public int getIntegerValue() {
            return this.integerValue_;
        }

        public void setIntegerValue(int i) {
            this.bitField0_ |= 16;
            this.integerValue_ = i;
        }

        public void clearIntegerValue() {
            this.bitField0_ &= -17;
            this.integerValue_ = 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
        public boolean hasLongValue() {
            return (this.bitField0_ & 32) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
        public long getLongValue() {
            return this.longValue_;
        }

        public void setLongValue(long j) {
            this.bitField0_ |= 32;
            this.longValue_ = j;
        }

        public void clearLongValue() {
            this.bitField0_ &= -33;
            this.longValue_ = 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
        public boolean hasStringValue() {
            return (this.bitField0_ & 64) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
        public String getStringValue() {
            return this.stringValue_;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
        public ByteString getStringValueBytes() {
            return ByteString.copyFromUtf8(this.stringValue_);
        }

        public void setStringValue(String str) {
            str.getClass();
            this.bitField0_ |= 64;
            this.stringValue_ = str;
        }

        public void clearStringValue() {
            this.bitField0_ &= -65;
            this.stringValue_ = getDefaultInstance().getStringValue();
        }

        public void setStringValueBytes(ByteString byteString) {
            this.stringValue_ = byteString.toStringUtf8();
            this.bitField0_ |= 64;
        }

        public static KeyValue parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (KeyValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static KeyValue parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (KeyValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static KeyValue parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (KeyValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static KeyValue parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (KeyValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static KeyValue parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (KeyValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static KeyValue parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (KeyValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static KeyValue parseFrom(InputStream inputStream) throws IOException {
            return (KeyValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static KeyValue parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (KeyValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static KeyValue parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (KeyValue) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static KeyValue parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (KeyValue) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static KeyValue parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (KeyValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static KeyValue parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (KeyValue) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(KeyValue keyValue) {
            return DEFAULT_INSTANCE.createBuilder(keyValue);
        }

        /* loaded from: classes.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<KeyValue, Builder> implements KeyValueOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(KeyValue.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
            public boolean hasKey() {
                return ((KeyValue) this.instance).hasKey();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
            public String getKey() {
                return ((KeyValue) this.instance).getKey();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
            public ByteString getKeyBytes() {
                return ((KeyValue) this.instance).getKeyBytes();
            }

            public Builder setKey(String str) {
                copyOnWrite();
                ((KeyValue) this.instance).setKey(str);
                return this;
            }

            public Builder clearKey() {
                copyOnWrite();
                ((KeyValue) this.instance).clearKey();
                return this;
            }

            public Builder setKeyBytes(ByteString byteString) {
                copyOnWrite();
                ((KeyValue) this.instance).setKeyBytes(byteString);
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
            public boolean hasBlobValue() {
                return ((KeyValue) this.instance).hasBlobValue();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
            public ByteString getBlobValue() {
                return ((KeyValue) this.instance).getBlobValue();
            }

            public Builder setBlobValue(ByteString byteString) {
                copyOnWrite();
                ((KeyValue) this.instance).setBlobValue(byteString);
                return this;
            }

            public Builder clearBlobValue() {
                copyOnWrite();
                ((KeyValue) this.instance).clearBlobValue();
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
            public boolean hasBooleanValue() {
                return ((KeyValue) this.instance).hasBooleanValue();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
            public boolean getBooleanValue() {
                return ((KeyValue) this.instance).getBooleanValue();
            }

            public Builder setBooleanValue(boolean z) {
                copyOnWrite();
                ((KeyValue) this.instance).setBooleanValue(z);
                return this;
            }

            public Builder clearBooleanValue() {
                copyOnWrite();
                ((KeyValue) this.instance).clearBooleanValue();
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
            public boolean hasFloatValue() {
                return ((KeyValue) this.instance).hasFloatValue();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
            public float getFloatValue() {
                return ((KeyValue) this.instance).getFloatValue();
            }

            public Builder setFloatValue(float f) {
                copyOnWrite();
                ((KeyValue) this.instance).setFloatValue(f);
                return this;
            }

            public Builder clearFloatValue() {
                copyOnWrite();
                ((KeyValue) this.instance).clearFloatValue();
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
            public boolean hasIntegerValue() {
                return ((KeyValue) this.instance).hasIntegerValue();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
            public int getIntegerValue() {
                return ((KeyValue) this.instance).getIntegerValue();
            }

            public Builder setIntegerValue(int i) {
                copyOnWrite();
                ((KeyValue) this.instance).setIntegerValue(i);
                return this;
            }

            public Builder clearIntegerValue() {
                copyOnWrite();
                ((KeyValue) this.instance).clearIntegerValue();
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
            public boolean hasLongValue() {
                return ((KeyValue) this.instance).hasLongValue();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
            public long getLongValue() {
                return ((KeyValue) this.instance).getLongValue();
            }

            public Builder setLongValue(long j) {
                copyOnWrite();
                ((KeyValue) this.instance).setLongValue(j);
                return this;
            }

            public Builder clearLongValue() {
                copyOnWrite();
                ((KeyValue) this.instance).clearLongValue();
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
            public boolean hasStringValue() {
                return ((KeyValue) this.instance).hasStringValue();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
            public String getStringValue() {
                return ((KeyValue) this.instance).getStringValue();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.KeyValueOrBuilder
            public ByteString getStringValueBytes() {
                return ((KeyValue) this.instance).getStringValueBytes();
            }

            public Builder setStringValue(String str) {
                copyOnWrite();
                ((KeyValue) this.instance).setStringValue(str);
                return this;
            }

            public Builder clearStringValue() {
                copyOnWrite();
                ((KeyValue) this.instance).clearStringValue();
                return this;
            }

            public Builder setStringValueBytes(ByteString byteString) {
                copyOnWrite();
                ((KeyValue) this.instance).setStringValueBytes(byteString);
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new KeyValue();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0007\u0000\u0001\u0001\u0007\u0007\u0000\u0000\u0000\u0001ဈ\u0000\u0002ည\u0001\u0003ဇ\u0002\u0004ခ\u0003\u0005င\u0004\u0006ဂ\u0005\u0007ဈ\u0006", new Object[]{"bitField0_", "key_", "blobValue_", "booleanValue_", "floatValue_", "integerValue_", "longValue_", "stringValue_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<KeyValue> parser = PARSER;
                    if (parser == null) {
                        synchronized (KeyValue.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            KeyValue keyValue = new KeyValue();
            DEFAULT_INSTANCE = keyValue;
            GeneratedMessageLite.registerDefaultInstance(KeyValue.class, keyValue);
        }

        public static KeyValue getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<KeyValue> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* loaded from: classes.dex */
    public static final class BackupFrame extends GeneratedMessageLite<BackupFrame, Builder> implements BackupFrameOrBuilder {
        public static final int ATTACHMENT_FIELD_NUMBER;
        public static final int AVATAR_FIELD_NUMBER;
        private static final BackupFrame DEFAULT_INSTANCE;
        public static final int END_FIELD_NUMBER;
        public static final int HEADER_FIELD_NUMBER;
        public static final int KEYVALUE_FIELD_NUMBER;
        private static volatile Parser<BackupFrame> PARSER;
        public static final int PREFERENCE_FIELD_NUMBER;
        public static final int STATEMENT_FIELD_NUMBER;
        public static final int STICKER_FIELD_NUMBER;
        public static final int VERSION_FIELD_NUMBER;
        private Attachment attachment_;
        private Avatar avatar_;
        private int bitField0_;
        private boolean end_;
        private Header header_;
        private KeyValue keyValue_;
        private SharedPreference preference_;
        private SqlStatement statement_;
        private Sticker sticker_;
        private DatabaseVersion version_;

        private BackupFrame() {
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
        public boolean hasHeader() {
            return (this.bitField0_ & 1) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
        public Header getHeader() {
            Header header = this.header_;
            return header == null ? Header.getDefaultInstance() : header;
        }

        public void setHeader(Header header) {
            header.getClass();
            this.header_ = header;
            this.bitField0_ |= 1;
        }

        public void mergeHeader(Header header) {
            header.getClass();
            Header header2 = this.header_;
            if (header2 == null || header2 == Header.getDefaultInstance()) {
                this.header_ = header;
            } else {
                this.header_ = Header.newBuilder(this.header_).mergeFrom((Header.Builder) header).buildPartial();
            }
            this.bitField0_ |= 1;
        }

        public void clearHeader() {
            this.header_ = null;
            this.bitField0_ &= -2;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
        public boolean hasStatement() {
            return (this.bitField0_ & 2) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
        public SqlStatement getStatement() {
            SqlStatement sqlStatement = this.statement_;
            return sqlStatement == null ? SqlStatement.getDefaultInstance() : sqlStatement;
        }

        public void setStatement(SqlStatement sqlStatement) {
            sqlStatement.getClass();
            this.statement_ = sqlStatement;
            this.bitField0_ |= 2;
        }

        public void mergeStatement(SqlStatement sqlStatement) {
            sqlStatement.getClass();
            SqlStatement sqlStatement2 = this.statement_;
            if (sqlStatement2 == null || sqlStatement2 == SqlStatement.getDefaultInstance()) {
                this.statement_ = sqlStatement;
            } else {
                this.statement_ = SqlStatement.newBuilder(this.statement_).mergeFrom((SqlStatement.Builder) sqlStatement).buildPartial();
            }
            this.bitField0_ |= 2;
        }

        public void clearStatement() {
            this.statement_ = null;
            this.bitField0_ &= -3;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
        public boolean hasPreference() {
            return (this.bitField0_ & 4) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
        public SharedPreference getPreference() {
            SharedPreference sharedPreference = this.preference_;
            return sharedPreference == null ? SharedPreference.getDefaultInstance() : sharedPreference;
        }

        public void setPreference(SharedPreference sharedPreference) {
            sharedPreference.getClass();
            this.preference_ = sharedPreference;
            this.bitField0_ |= 4;
        }

        public void mergePreference(SharedPreference sharedPreference) {
            sharedPreference.getClass();
            SharedPreference sharedPreference2 = this.preference_;
            if (sharedPreference2 == null || sharedPreference2 == SharedPreference.getDefaultInstance()) {
                this.preference_ = sharedPreference;
            } else {
                this.preference_ = SharedPreference.newBuilder(this.preference_).mergeFrom((SharedPreference.Builder) sharedPreference).buildPartial();
            }
            this.bitField0_ |= 4;
        }

        public void clearPreference() {
            this.preference_ = null;
            this.bitField0_ &= -5;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
        public boolean hasAttachment() {
            return (this.bitField0_ & 8) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
        public Attachment getAttachment() {
            Attachment attachment = this.attachment_;
            return attachment == null ? Attachment.getDefaultInstance() : attachment;
        }

        public void setAttachment(Attachment attachment) {
            attachment.getClass();
            this.attachment_ = attachment;
            this.bitField0_ |= 8;
        }

        public void mergeAttachment(Attachment attachment) {
            attachment.getClass();
            Attachment attachment2 = this.attachment_;
            if (attachment2 == null || attachment2 == Attachment.getDefaultInstance()) {
                this.attachment_ = attachment;
            } else {
                this.attachment_ = Attachment.newBuilder(this.attachment_).mergeFrom((Attachment.Builder) attachment).buildPartial();
            }
            this.bitField0_ |= 8;
        }

        public void clearAttachment() {
            this.attachment_ = null;
            this.bitField0_ &= -9;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
        public boolean hasVersion() {
            return (this.bitField0_ & 16) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
        public DatabaseVersion getVersion() {
            DatabaseVersion databaseVersion = this.version_;
            return databaseVersion == null ? DatabaseVersion.getDefaultInstance() : databaseVersion;
        }

        public void setVersion(DatabaseVersion databaseVersion) {
            databaseVersion.getClass();
            this.version_ = databaseVersion;
            this.bitField0_ |= 16;
        }

        public void mergeVersion(DatabaseVersion databaseVersion) {
            databaseVersion.getClass();
            DatabaseVersion databaseVersion2 = this.version_;
            if (databaseVersion2 == null || databaseVersion2 == DatabaseVersion.getDefaultInstance()) {
                this.version_ = databaseVersion;
            } else {
                this.version_ = DatabaseVersion.newBuilder(this.version_).mergeFrom((DatabaseVersion.Builder) databaseVersion).buildPartial();
            }
            this.bitField0_ |= 16;
        }

        public void clearVersion() {
            this.version_ = null;
            this.bitField0_ &= -17;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
        public boolean hasEnd() {
            return (this.bitField0_ & 32) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
        public boolean getEnd() {
            return this.end_;
        }

        public void setEnd(boolean z) {
            this.bitField0_ |= 32;
            this.end_ = z;
        }

        public void clearEnd() {
            this.bitField0_ &= -33;
            this.end_ = false;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
        public boolean hasAvatar() {
            return (this.bitField0_ & 64) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
        public Avatar getAvatar() {
            Avatar avatar = this.avatar_;
            return avatar == null ? Avatar.getDefaultInstance() : avatar;
        }

        public void setAvatar(Avatar avatar) {
            avatar.getClass();
            this.avatar_ = avatar;
            this.bitField0_ |= 64;
        }

        public void mergeAvatar(Avatar avatar) {
            avatar.getClass();
            Avatar avatar2 = this.avatar_;
            if (avatar2 == null || avatar2 == Avatar.getDefaultInstance()) {
                this.avatar_ = avatar;
            } else {
                this.avatar_ = Avatar.newBuilder(this.avatar_).mergeFrom((Avatar.Builder) avatar).buildPartial();
            }
            this.bitField0_ |= 64;
        }

        public void clearAvatar() {
            this.avatar_ = null;
            this.bitField0_ &= -65;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
        public boolean hasSticker() {
            return (this.bitField0_ & 128) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
        public Sticker getSticker() {
            Sticker sticker = this.sticker_;
            return sticker == null ? Sticker.getDefaultInstance() : sticker;
        }

        public void setSticker(Sticker sticker) {
            sticker.getClass();
            this.sticker_ = sticker;
            this.bitField0_ |= 128;
        }

        public void mergeSticker(Sticker sticker) {
            sticker.getClass();
            Sticker sticker2 = this.sticker_;
            if (sticker2 == null || sticker2 == Sticker.getDefaultInstance()) {
                this.sticker_ = sticker;
            } else {
                this.sticker_ = Sticker.newBuilder(this.sticker_).mergeFrom((Sticker.Builder) sticker).buildPartial();
            }
            this.bitField0_ |= 128;
        }

        public void clearSticker() {
            this.sticker_ = null;
            this.bitField0_ &= -129;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
        public boolean hasKeyValue() {
            return (this.bitField0_ & 256) != 0;
        }

        @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
        public KeyValue getKeyValue() {
            KeyValue keyValue = this.keyValue_;
            return keyValue == null ? KeyValue.getDefaultInstance() : keyValue;
        }

        public void setKeyValue(KeyValue keyValue) {
            keyValue.getClass();
            this.keyValue_ = keyValue;
            this.bitField0_ |= 256;
        }

        public void mergeKeyValue(KeyValue keyValue) {
            keyValue.getClass();
            KeyValue keyValue2 = this.keyValue_;
            if (keyValue2 == null || keyValue2 == KeyValue.getDefaultInstance()) {
                this.keyValue_ = keyValue;
            } else {
                this.keyValue_ = KeyValue.newBuilder(this.keyValue_).mergeFrom((KeyValue.Builder) keyValue).buildPartial();
            }
            this.bitField0_ |= 256;
        }

        public void clearKeyValue() {
            this.keyValue_ = null;
            this.bitField0_ &= -257;
        }

        public static BackupFrame parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (BackupFrame) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static BackupFrame parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (BackupFrame) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static BackupFrame parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (BackupFrame) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static BackupFrame parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (BackupFrame) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static BackupFrame parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (BackupFrame) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static BackupFrame parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (BackupFrame) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static BackupFrame parseFrom(InputStream inputStream) throws IOException {
            return (BackupFrame) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static BackupFrame parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (BackupFrame) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static BackupFrame parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (BackupFrame) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static BackupFrame parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (BackupFrame) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static BackupFrame parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (BackupFrame) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static BackupFrame parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (BackupFrame) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(BackupFrame backupFrame) {
            return DEFAULT_INSTANCE.createBuilder(backupFrame);
        }

        /* loaded from: classes.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<BackupFrame, Builder> implements BackupFrameOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(BackupFrame.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
            public boolean hasHeader() {
                return ((BackupFrame) this.instance).hasHeader();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
            public Header getHeader() {
                return ((BackupFrame) this.instance).getHeader();
            }

            public Builder setHeader(Header header) {
                copyOnWrite();
                ((BackupFrame) this.instance).setHeader(header);
                return this;
            }

            public Builder setHeader(Header.Builder builder) {
                copyOnWrite();
                ((BackupFrame) this.instance).setHeader(builder.build());
                return this;
            }

            public Builder mergeHeader(Header header) {
                copyOnWrite();
                ((BackupFrame) this.instance).mergeHeader(header);
                return this;
            }

            public Builder clearHeader() {
                copyOnWrite();
                ((BackupFrame) this.instance).clearHeader();
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
            public boolean hasStatement() {
                return ((BackupFrame) this.instance).hasStatement();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
            public SqlStatement getStatement() {
                return ((BackupFrame) this.instance).getStatement();
            }

            public Builder setStatement(SqlStatement sqlStatement) {
                copyOnWrite();
                ((BackupFrame) this.instance).setStatement(sqlStatement);
                return this;
            }

            public Builder setStatement(SqlStatement.Builder builder) {
                copyOnWrite();
                ((BackupFrame) this.instance).setStatement(builder.build());
                return this;
            }

            public Builder mergeStatement(SqlStatement sqlStatement) {
                copyOnWrite();
                ((BackupFrame) this.instance).mergeStatement(sqlStatement);
                return this;
            }

            public Builder clearStatement() {
                copyOnWrite();
                ((BackupFrame) this.instance).clearStatement();
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
            public boolean hasPreference() {
                return ((BackupFrame) this.instance).hasPreference();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
            public SharedPreference getPreference() {
                return ((BackupFrame) this.instance).getPreference();
            }

            public Builder setPreference(SharedPreference sharedPreference) {
                copyOnWrite();
                ((BackupFrame) this.instance).setPreference(sharedPreference);
                return this;
            }

            public Builder setPreference(SharedPreference.Builder builder) {
                copyOnWrite();
                ((BackupFrame) this.instance).setPreference(builder.build());
                return this;
            }

            public Builder mergePreference(SharedPreference sharedPreference) {
                copyOnWrite();
                ((BackupFrame) this.instance).mergePreference(sharedPreference);
                return this;
            }

            public Builder clearPreference() {
                copyOnWrite();
                ((BackupFrame) this.instance).clearPreference();
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
            public boolean hasAttachment() {
                return ((BackupFrame) this.instance).hasAttachment();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
            public Attachment getAttachment() {
                return ((BackupFrame) this.instance).getAttachment();
            }

            public Builder setAttachment(Attachment attachment) {
                copyOnWrite();
                ((BackupFrame) this.instance).setAttachment(attachment);
                return this;
            }

            public Builder setAttachment(Attachment.Builder builder) {
                copyOnWrite();
                ((BackupFrame) this.instance).setAttachment(builder.build());
                return this;
            }

            public Builder mergeAttachment(Attachment attachment) {
                copyOnWrite();
                ((BackupFrame) this.instance).mergeAttachment(attachment);
                return this;
            }

            public Builder clearAttachment() {
                copyOnWrite();
                ((BackupFrame) this.instance).clearAttachment();
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
            public boolean hasVersion() {
                return ((BackupFrame) this.instance).hasVersion();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
            public DatabaseVersion getVersion() {
                return ((BackupFrame) this.instance).getVersion();
            }

            public Builder setVersion(DatabaseVersion databaseVersion) {
                copyOnWrite();
                ((BackupFrame) this.instance).setVersion(databaseVersion);
                return this;
            }

            public Builder setVersion(DatabaseVersion.Builder builder) {
                copyOnWrite();
                ((BackupFrame) this.instance).setVersion(builder.build());
                return this;
            }

            public Builder mergeVersion(DatabaseVersion databaseVersion) {
                copyOnWrite();
                ((BackupFrame) this.instance).mergeVersion(databaseVersion);
                return this;
            }

            public Builder clearVersion() {
                copyOnWrite();
                ((BackupFrame) this.instance).clearVersion();
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
            public boolean hasEnd() {
                return ((BackupFrame) this.instance).hasEnd();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
            public boolean getEnd() {
                return ((BackupFrame) this.instance).getEnd();
            }

            public Builder setEnd(boolean z) {
                copyOnWrite();
                ((BackupFrame) this.instance).setEnd(z);
                return this;
            }

            public Builder clearEnd() {
                copyOnWrite();
                ((BackupFrame) this.instance).clearEnd();
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
            public boolean hasAvatar() {
                return ((BackupFrame) this.instance).hasAvatar();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
            public Avatar getAvatar() {
                return ((BackupFrame) this.instance).getAvatar();
            }

            public Builder setAvatar(Avatar avatar) {
                copyOnWrite();
                ((BackupFrame) this.instance).setAvatar(avatar);
                return this;
            }

            public Builder setAvatar(Avatar.Builder builder) {
                copyOnWrite();
                ((BackupFrame) this.instance).setAvatar(builder.build());
                return this;
            }

            public Builder mergeAvatar(Avatar avatar) {
                copyOnWrite();
                ((BackupFrame) this.instance).mergeAvatar(avatar);
                return this;
            }

            public Builder clearAvatar() {
                copyOnWrite();
                ((BackupFrame) this.instance).clearAvatar();
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
            public boolean hasSticker() {
                return ((BackupFrame) this.instance).hasSticker();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
            public Sticker getSticker() {
                return ((BackupFrame) this.instance).getSticker();
            }

            public Builder setSticker(Sticker sticker) {
                copyOnWrite();
                ((BackupFrame) this.instance).setSticker(sticker);
                return this;
            }

            public Builder setSticker(Sticker.Builder builder) {
                copyOnWrite();
                ((BackupFrame) this.instance).setSticker(builder.build());
                return this;
            }

            public Builder mergeSticker(Sticker sticker) {
                copyOnWrite();
                ((BackupFrame) this.instance).mergeSticker(sticker);
                return this;
            }

            public Builder clearSticker() {
                copyOnWrite();
                ((BackupFrame) this.instance).clearSticker();
                return this;
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
            public boolean hasKeyValue() {
                return ((BackupFrame) this.instance).hasKeyValue();
            }

            @Override // org.thoughtcrime.securesms.backup.BackupProtos.BackupFrameOrBuilder
            public KeyValue getKeyValue() {
                return ((BackupFrame) this.instance).getKeyValue();
            }

            public Builder setKeyValue(KeyValue keyValue) {
                copyOnWrite();
                ((BackupFrame) this.instance).setKeyValue(keyValue);
                return this;
            }

            public Builder setKeyValue(KeyValue.Builder builder) {
                copyOnWrite();
                ((BackupFrame) this.instance).setKeyValue(builder.build());
                return this;
            }

            public Builder mergeKeyValue(KeyValue keyValue) {
                copyOnWrite();
                ((BackupFrame) this.instance).mergeKeyValue(keyValue);
                return this;
            }

            public Builder clearKeyValue() {
                copyOnWrite();
                ((BackupFrame) this.instance).clearKeyValue();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new BackupFrame();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\t\u0000\u0001\u0001\t\t\u0000\u0000\u0000\u0001ဉ\u0000\u0002ဉ\u0001\u0003ဉ\u0002\u0004ဉ\u0003\u0005ဉ\u0004\u0006ဇ\u0005\u0007ဉ\u0006\bဉ\u0007\tဉ\b", new Object[]{"bitField0_", "header_", "statement_", "preference_", "attachment_", "version_", "end_", "avatar_", "sticker_", "keyValue_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<BackupFrame> parser = PARSER;
                    if (parser == null) {
                        synchronized (BackupFrame.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            BackupFrame backupFrame = new BackupFrame();
            DEFAULT_INSTANCE = backupFrame;
            GeneratedMessageLite.registerDefaultInstance(BackupFrame.class, backupFrame);
        }

        public static BackupFrame getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<BackupFrame> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }
}
