package org.thoughtcrime.securesms.backup;

import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.core.util.Consumer;
import androidx.fragment.app.Fragment;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.registration.fragments.RestoreBackupFragment;
import org.thoughtcrime.securesms.service.LocalBackupListener;
import org.thoughtcrime.securesms.util.BackupUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.text.AfterTextChanged;

/* loaded from: classes.dex */
public class BackupDialog {
    private static final String TAG = Log.tag(BackupDialog.class);

    public static void showEnableBackupDialog(Context context, Intent intent, String str, Runnable runnable) {
        String[] generateBackupPassphrase = BackupUtil.generateBackupPassphrase();
        AlertDialog create = new AlertDialog.Builder(context).setTitle(R.string.BackupDialog_enable_local_backups).setView(intent != null ? R.layout.backup_enable_dialog_v29 : R.layout.backup_enable_dialog).setPositiveButton(R.string.BackupDialog_enable_backups, (DialogInterface.OnClickListener) null).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).create();
        create.setOnShowListener(new DialogInterface.OnShowListener(str, create, intent, context, generateBackupPassphrase, runnable) { // from class: org.thoughtcrime.securesms.backup.BackupDialog$$ExternalSyntheticLambda3
            public final /* synthetic */ String f$0;
            public final /* synthetic */ AlertDialog f$1;
            public final /* synthetic */ Intent f$2;
            public final /* synthetic */ Context f$3;
            public final /* synthetic */ String[] f$4;
            public final /* synthetic */ Runnable f$5;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
            }

            @Override // android.content.DialogInterface.OnShowListener
            public final void onShow(DialogInterface dialogInterface) {
                BackupDialog.$r8$lambda$KqjyPS7TEavOZcflA6EJPuVjchk(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, dialogInterface);
            }
        });
        create.show();
        ((TextView) create.findViewById(R.id.code_first)).setText(generateBackupPassphrase[0]);
        ((TextView) create.findViewById(R.id.code_second)).setText(generateBackupPassphrase[1]);
        ((TextView) create.findViewById(R.id.code_third)).setText(generateBackupPassphrase[2]);
        ((TextView) create.findViewById(R.id.code_fourth)).setText(generateBackupPassphrase[3]);
        ((TextView) create.findViewById(R.id.code_fifth)).setText(generateBackupPassphrase[4]);
        ((TextView) create.findViewById(R.id.code_sixth)).setText(generateBackupPassphrase[5]);
        ((TextView) create.findViewById(R.id.confirmation_text)).setOnClickListener(new View.OnClickListener((CheckBox) create.findViewById(R.id.confirmation_check)) { // from class: org.thoughtcrime.securesms.backup.BackupDialog$$ExternalSyntheticLambda4
            public final /* synthetic */ CheckBox f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                BackupDialog.m361$r8$lambda$Pkpga6YxjLvK1bJptjk85Vtbqc(this.f$0, view);
            }
        });
        create.findViewById(R.id.number_table).setOnClickListener(new View.OnClickListener(context, generateBackupPassphrase) { // from class: org.thoughtcrime.securesms.backup.BackupDialog$$ExternalSyntheticLambda5
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ String[] f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                BackupDialog.$r8$lambda$LTq5uUbwV9ngt3ckxHr_LphOlnY(this.f$0, this.f$1, view);
            }
        });
    }

    public static /* synthetic */ void lambda$showEnableBackupDialog$1(String str, AlertDialog alertDialog, Intent intent, Context context, String[] strArr, Runnable runnable, DialogInterface dialogInterface) {
        TextView textView;
        if (!(str == null || (textView = (TextView) alertDialog.findViewById(R.id.backup_enable_dialog_folder_name)) == null)) {
            textView.setText(str);
        }
        ((AlertDialog) dialogInterface).getButton(-1).setOnClickListener(new View.OnClickListener(intent, context, strArr, runnable, dialogInterface) { // from class: org.thoughtcrime.securesms.backup.BackupDialog$$ExternalSyntheticLambda8
            public final /* synthetic */ Intent f$1;
            public final /* synthetic */ Context f$2;
            public final /* synthetic */ String[] f$3;
            public final /* synthetic */ Runnable f$4;
            public final /* synthetic */ DialogInterface f$5;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                BackupDialog.$r8$lambda$013SoXieFkN2qBvYu_rPI0PeQ5U(AlertDialog.this, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, view);
            }
        });
    }

    public static /* synthetic */ void lambda$showEnableBackupDialog$0(AlertDialog alertDialog, Intent intent, Context context, String[] strArr, Runnable runnable, DialogInterface dialogInterface, View view) {
        if (((CheckBox) alertDialog.findViewById(R.id.confirmation_check)).isChecked()) {
            if (!(intent == null || intent.getData() == null)) {
                Uri data = intent.getData();
                SignalStore.settings().setSignalBackupDirectory(data);
                context.getContentResolver().takePersistableUriPermission(data, 3);
            }
            BackupPassphrase.set(context, Util.join(strArr, " "));
            TextSecurePreferences.setNextBackupTime(context, 0);
            SignalStore.settings().setBackupEnabled(true);
            LocalBackupListener.schedule(context);
            runnable.run();
            dialogInterface.dismiss();
            return;
        }
        Toast.makeText(context, (int) R.string.BackupDialog_please_acknowledge_your_understanding_by_marking_the_confirmation_check_box, 1).show();
    }

    public static /* synthetic */ void lambda$showEnableBackupDialog$3(Context context, String[] strArr, View view) {
        ((ClipboardManager) context.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText(DraftDatabase.Draft.TEXT, Util.join(strArr, " ")));
        Toast.makeText(context, (int) R.string.BackupDialog_copied_to_clipboard, 1).show();
    }

    public static void showChooseBackupLocationDialog(Fragment fragment, int i) {
        new MaterialAlertDialogBuilder(fragment.requireContext()).setView(R.layout.backup_choose_location_dialog).setCancelable(true).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.backup.BackupDialog$$ExternalSyntheticLambda6
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                BackupDialog.$r8$lambda$nw7cjxSh1cBudNZ4srtvGmf7dIc(dialogInterface, i2);
            }
        }).setPositiveButton(R.string.BackupDialog_choose_folder, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(i) { // from class: org.thoughtcrime.securesms.backup.BackupDialog$$ExternalSyntheticLambda7
            public final /* synthetic */ int f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                BackupDialog.m362$r8$lambda$Q7EezTwyVjjHQAJjrXu7jAug(Fragment.this, this.f$1, dialogInterface, i2);
            }
        }).create().show();
    }

    public static /* synthetic */ void lambda$showChooseBackupLocationDialog$5(Fragment fragment, int i, DialogInterface dialogInterface, int i2) {
        Intent intent = new Intent("android.intent.action.OPEN_DOCUMENT_TREE");
        if (Build.VERSION.SDK_INT >= 26) {
            intent.putExtra("android.provider.extra.INITIAL_URI", SignalStore.settings().getLatestSignalBackupDirectory());
        }
        intent.addFlags(67);
        try {
            fragment.startActivityForResult(intent, i);
        } catch (ActivityNotFoundException unused) {
            Toast.makeText(fragment.requireContext(), (int) R.string.BackupDialog_no_file_picker_available, 1).show();
        }
        dialogInterface.dismiss();
    }

    public static void showDisableBackupDialog(Context context, Runnable runnable) {
        new MaterialAlertDialogBuilder(context).setTitle(R.string.BackupDialog_delete_backups).setMessage(R.string.BackupDialog_disable_and_delete_all_local_backups).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).setPositiveButton(R.string.BackupDialog_delete_backups_statement, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(context, runnable) { // from class: org.thoughtcrime.securesms.backup.BackupDialog$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ Runnable f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                BackupDialog.$r8$lambda$q5yMOKQC_I7y3QnH_AXn8xfd7CA(this.f$0, this.f$1, dialogInterface, i);
            }
        }).create().show();
    }

    public static /* synthetic */ void lambda$showDisableBackupDialog$6(Context context, Runnable runnable, DialogInterface dialogInterface, int i) {
        BackupUtil.disableBackups(context);
        runnable.run();
    }

    public static void showVerifyBackupPassphraseDialog(Context context) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.enter_backup_passphrase_dialog, (ViewGroup) null);
        EditText editText = (EditText) inflate.findViewById(R.id.restore_passphrase_input);
        AlertDialog show = new MaterialAlertDialogBuilder(context).setTitle(R.string.BackupDialog_enter_backup_passphrase_to_verify).setView(inflate).setPositiveButton(R.string.BackupDialog_verify, (DialogInterface.OnClickListener) null).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).show();
        Button button = show.getButton(-1);
        button.setEnabled(false);
        editText.addTextChangedListener(new AfterTextChanged(new Consumer(button) { // from class: org.thoughtcrime.securesms.backup.BackupDialog$$ExternalSyntheticLambda1
            public final /* synthetic */ Button f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                BackupDialog.m363$r8$lambda$y3qKAfaiSEoXhBQirY0yKKWDSM(RestoreBackupFragment.PassphraseAsYouTypeFormatter.this, this.f$1, (Editable) obj);
            }
        }));
        button.setOnClickListener(new View.OnClickListener(editText, context, show) { // from class: org.thoughtcrime.securesms.backup.BackupDialog$$ExternalSyntheticLambda2
            public final /* synthetic */ EditText f$0;
            public final /* synthetic */ Context f$1;
            public final /* synthetic */ AlertDialog f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                BackupDialog.$r8$lambda$pCSnZg31Z18DMD7rh8R8IqyFhGs(this.f$0, this.f$1, this.f$2, view);
            }
        });
    }

    public static /* synthetic */ void lambda$showVerifyBackupPassphraseDialog$7(RestoreBackupFragment.PassphraseAsYouTypeFormatter passphraseAsYouTypeFormatter, Button button, Editable editable) {
        passphraseAsYouTypeFormatter.afterTextChanged(editable);
        button.setEnabled(editable.length() == 30);
    }

    public static /* synthetic */ void lambda$showVerifyBackupPassphraseDialog$8(EditText editText, Context context, AlertDialog alertDialog, View view) {
        if (editText.getText().toString().equals(BackupPassphrase.get(context))) {
            Toast.makeText(context, (int) R.string.BackupDialog_you_successfully_entered_your_backup_passphrase, 0).show();
            alertDialog.dismiss();
            return;
        }
        Toast.makeText(context, (int) R.string.BackupDialog_passphrase_was_not_correct, 0).show();
    }
}
