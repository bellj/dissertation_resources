package org.thoughtcrime.securesms.backup;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.util.Pair;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Objects;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.greenrobot.eventbus.EventBus;
import org.signal.core.util.Conversions;
import org.signal.core.util.SqlUtil;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.kdf.HKDF;
import org.signal.libsignal.protocol.kdf.HKDFv3;
import org.signal.libsignal.protocol.util.ByteUtil;
import org.thoughtcrime.securesms.backup.BackupProtos;
import org.thoughtcrime.securesms.backup.FullBackupBase;
import org.thoughtcrime.securesms.crypto.AttachmentSecret;
import org.thoughtcrime.securesms.crypto.MasterSecretUtil;
import org.thoughtcrime.securesms.crypto.ModernEncryptingPartOutputStream;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.DistributionListDatabase;
import org.thoughtcrime.securesms.database.KeyValueDatabase;
import org.thoughtcrime.securesms.database.MessageSendLogDatabase;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.database.ReactionDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.StickerDatabase;
import org.thoughtcrime.securesms.database.StorySendsDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.KeyValueDataSet;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.BackupUtil;

/* loaded from: classes.dex */
public class FullBackupImporter extends FullBackupBase {
    private static final String[] TABLES_TO_DROP_FIRST = {DistributionListDatabase.MembershipTable.TABLE_NAME, "distribution_list", "message_send_log_recipients", MessageSendLogDatabase.RecipientTable.TABLE_NAME, MessageSendLogDatabase.MessageTable.TABLE_NAME, ReactionDatabase.TABLE_NAME, NotificationProfileDatabase.NotificationProfileScheduleTable.TABLE_NAME, NotificationProfileDatabase.NotificationProfileAllowedMembersTable.TABLE_NAME, StorySendsDatabase.TABLE_NAME};
    private static final String TAG = Log.tag(FullBackupImporter.class);

    public static void importFile(Context context, AttachmentSecret attachmentSecret, SQLiteDatabase sQLiteDatabase, Uri uri, String str) throws IOException {
        InputStream inputStream = getInputStream(context, uri);
        try {
            importFile(context, attachmentSecret, sQLiteDatabase, inputStream, str);
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public static void importFile(Context context, AttachmentSecret attachmentSecret, SQLiteDatabase sQLiteDatabase, InputStream inputStream, String str) throws IOException {
        SQLiteDatabase sqlCipherDatabase = KeyValueDatabase.getInstance(ApplicationDependencies.getApplication()).getSqlCipherDatabase();
        sQLiteDatabase.beginTransaction();
        sqlCipherDatabase.beginTransaction();
        try {
            BackupRecordInputStream backupRecordInputStream = new BackupRecordInputStream(inputStream, str);
            dropAllTables(sQLiteDatabase);
            int i = 0;
            while (true) {
                BackupProtos.BackupFrame readFrame = backupRecordInputStream.readFrame();
                if (!readFrame.getEnd()) {
                    if (i % 100 == 0) {
                        EventBus.getDefault().post(new FullBackupBase.BackupEvent(FullBackupBase.BackupEvent.Type.PROGRESS, (long) i, 0));
                    }
                    i++;
                    if (readFrame.hasVersion()) {
                        processVersion(sQLiteDatabase, readFrame.getVersion());
                    } else if (readFrame.hasStatement()) {
                        processStatement(sQLiteDatabase, readFrame.getStatement());
                    } else if (readFrame.hasPreference()) {
                        processPreference(context, readFrame.getPreference());
                    } else if (readFrame.hasAttachment()) {
                        processAttachment(context, attachmentSecret, sQLiteDatabase, readFrame.getAttachment(), backupRecordInputStream);
                    } else if (readFrame.hasSticker()) {
                        processSticker(context, attachmentSecret, sQLiteDatabase, readFrame.getSticker(), backupRecordInputStream);
                    } else if (readFrame.hasAvatar()) {
                        processAvatar(context, sQLiteDatabase, readFrame.getAvatar(), backupRecordInputStream);
                    } else if (readFrame.hasKeyValue()) {
                        processKeyValue(readFrame.getKeyValue());
                    } else {
                        i--;
                    }
                } else {
                    sQLiteDatabase.setTransactionSuccessful();
                    sqlCipherDatabase.setTransactionSuccessful();
                    sQLiteDatabase.endTransaction();
                    sqlCipherDatabase.endTransaction();
                    EventBus.getDefault().post(new FullBackupBase.BackupEvent(FullBackupBase.BackupEvent.Type.FINISHED, (long) i, 0));
                    return;
                }
            }
        } catch (Throwable th) {
            sQLiteDatabase.endTransaction();
            sqlCipherDatabase.endTransaction();
            throw th;
        }
    }

    private static InputStream getInputStream(Context context, Uri uri) throws IOException {
        if (BackupUtil.isUserSelectionRequired(context) || uri.getScheme().equals("content")) {
            InputStream openInputStream = context.getContentResolver().openInputStream(uri);
            Objects.requireNonNull(openInputStream);
            return openInputStream;
        }
        String path = uri.getPath();
        Objects.requireNonNull(path);
        return new FileInputStream(new File(path));
    }

    private static void processVersion(SQLiteDatabase sQLiteDatabase, BackupProtos.DatabaseVersion databaseVersion) throws IOException {
        if (databaseVersion.getVersion() <= sQLiteDatabase.getVersion()) {
            sQLiteDatabase.setVersion(databaseVersion.getVersion());
            return;
        }
        throw new DatabaseDowngradeException(sQLiteDatabase.getVersion(), databaseVersion.getVersion());
    }

    private static void processStatement(SQLiteDatabase sQLiteDatabase, BackupProtos.SqlStatement sqlStatement) {
        boolean contains = sqlStatement.getStatement().contains("sms_fts_");
        boolean contains2 = sqlStatement.getStatement().contains("mms_fts_");
        boolean contains3 = sqlStatement.getStatement().contains("emoji_search_");
        boolean startsWith = sqlStatement.getStatement().toLowerCase().startsWith("create table sqlite_");
        if (contains || contains2 || contains3 || startsWith) {
            String str = TAG;
            Log.i(str, "Ignoring import for statement: " + sqlStatement.getStatement());
            return;
        }
        LinkedList linkedList = new LinkedList();
        for (BackupProtos.SqlStatement.SqlParameter sqlParameter : sqlStatement.getParametersList()) {
            if (sqlParameter.hasStringParamter()) {
                linkedList.add(sqlParameter.getStringParamter());
            } else if (sqlParameter.hasDoubleParameter()) {
                linkedList.add(Double.valueOf(sqlParameter.getDoubleParameter()));
            } else if (sqlParameter.hasIntegerParameter()) {
                linkedList.add(Long.valueOf(sqlParameter.getIntegerParameter()));
            } else if (sqlParameter.hasBlobParameter()) {
                linkedList.add(sqlParameter.getBlobParameter().toByteArray());
            } else if (sqlParameter.hasNullparameter()) {
                linkedList.add(null);
            }
        }
        if (linkedList.size() > 0) {
            sQLiteDatabase.execSQL(sqlStatement.getStatement(), linkedList.toArray());
        } else {
            sQLiteDatabase.execSQL(sqlStatement.getStatement());
        }
    }

    private static void processAttachment(Context context, AttachmentSecret attachmentSecret, SQLiteDatabase sQLiteDatabase, BackupProtos.Attachment attachment, BackupRecordInputStream backupRecordInputStream) throws IOException {
        File newFile = AttachmentDatabase.newFile(context);
        Pair<byte[], OutputStream> createFor = ModernEncryptingPartOutputStream.createFor(attachmentSecret, newFile, false);
        ContentValues contentValues = new ContentValues();
        try {
            backupRecordInputStream.readAttachmentTo((OutputStream) createFor.second, attachment.getLength());
            contentValues.put(AttachmentDatabase.DATA, newFile.getAbsolutePath());
            contentValues.put(AttachmentDatabase.DATA_RANDOM, (byte[]) createFor.first);
        } catch (BadMacException e) {
            String str = TAG;
            Log.w(str, "Bad MAC for attachment " + attachment.getAttachmentId() + "! Can't restore it.", e);
            newFile.delete();
            contentValues.put(AttachmentDatabase.DATA, (String) null);
            contentValues.put(AttachmentDatabase.DATA_RANDOM, (String) null);
        }
        sQLiteDatabase.update(AttachmentDatabase.TABLE_NAME, contentValues, "_id = ? AND unique_id = ?", new String[]{String.valueOf(attachment.getRowId()), String.valueOf(attachment.getAttachmentId())});
    }

    private static void processSticker(Context context, AttachmentSecret attachmentSecret, SQLiteDatabase sQLiteDatabase, BackupProtos.Sticker sticker, BackupRecordInputStream backupRecordInputStream) throws IOException {
        File createTempFile = File.createTempFile(StickerDatabase.TABLE_NAME, ".mms", context.getDir(StickerDatabase.DIRECTORY, 0));
        Pair<byte[], OutputStream> createFor = ModernEncryptingPartOutputStream.createFor(attachmentSecret, createTempFile, false);
        backupRecordInputStream.readAttachmentTo((OutputStream) createFor.second, sticker.getLength());
        ContentValues contentValues = new ContentValues();
        contentValues.put(StickerDatabase.FILE_PATH, createTempFile.getAbsolutePath());
        contentValues.put(StickerDatabase.FILE_LENGTH, Integer.valueOf(sticker.getLength()));
        contentValues.put(StickerDatabase.FILE_RANDOM, (byte[]) createFor.first);
        sQLiteDatabase.update(StickerDatabase.TABLE_NAME, contentValues, "_id = ?", new String[]{String.valueOf(sticker.getRowId())});
    }

    private static void processAvatar(Context context, SQLiteDatabase sQLiteDatabase, BackupProtos.Avatar avatar, BackupRecordInputStream backupRecordInputStream) throws IOException {
        if (avatar.hasRecipientId()) {
            backupRecordInputStream.readAttachmentTo(AvatarHelper.getOutputStream(context, RecipientId.from(avatar.getRecipientId()), false), avatar.getLength());
            return;
        }
        if (avatar.hasName() && SqlUtil.tableExists(sQLiteDatabase, "recipient_preferences")) {
            Log.w(TAG, "Avatar is missing a recipientId. Clearing signal_profile_avatar (legacy) so it can be fetched later.");
            sQLiteDatabase.execSQL("UPDATE recipient_preferences SET signal_profile_avatar = NULL WHERE recipient_ids = ?", new String[]{avatar.getName()});
        } else if (!avatar.hasName() || !SqlUtil.tableExists(sQLiteDatabase, RecipientDatabase.TABLE_NAME)) {
            Log.w(TAG, "Avatar is missing a recipientId. Skipping avatar restore.");
        } else {
            Log.w(TAG, "Avatar is missing a recipientId. Clearing signal_profile_avatar so it can be fetched later.");
            sQLiteDatabase.execSQL("UPDATE recipient SET signal_profile_avatar = NULL WHERE phone = ?", new String[]{avatar.getName()});
        }
        backupRecordInputStream.readAttachmentTo(new ByteArrayOutputStream(), avatar.getLength());
    }

    private static void processKeyValue(BackupProtos.KeyValue keyValue) {
        KeyValueDataSet keyValueDataSet = new KeyValueDataSet();
        if (keyValue.hasBlobValue()) {
            keyValueDataSet.putBlob(keyValue.getKey(), keyValue.getBlobValue().toByteArray());
        } else if (keyValue.hasBooleanValue()) {
            keyValueDataSet.putBoolean(keyValue.getKey(), keyValue.getBooleanValue());
        } else if (keyValue.hasFloatValue()) {
            keyValueDataSet.putFloat(keyValue.getKey(), keyValue.getFloatValue());
        } else if (keyValue.hasIntegerValue()) {
            keyValueDataSet.putInteger(keyValue.getKey(), keyValue.getIntegerValue());
        } else if (keyValue.hasLongValue()) {
            keyValueDataSet.putLong(keyValue.getKey(), keyValue.getLongValue());
        } else if (keyValue.hasStringValue()) {
            keyValueDataSet.putString(keyValue.getKey(), keyValue.getStringValue());
        } else {
            Log.i(TAG, "Unknown KeyValue backup value, skipping");
            return;
        }
        KeyValueDatabase.getInstance(ApplicationDependencies.getApplication()).writeDataSet(keyValueDataSet, Collections.emptyList());
    }

    private static void processPreference(Context context, BackupProtos.SharedPreference sharedPreference) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(sharedPreference.getFile(), 0);
        if (MasterSecretUtil.PREFERENCES_NAME.equals(sharedPreference.getFile())) {
            if ("pref_identity_public_v3".equals(sharedPreference.getKey()) && sharedPreference.hasValue()) {
                SignalStore.account().restoreLegacyIdentityPublicKeyFromBackup(sharedPreference.getValue());
            } else if ("pref_identity_private_v3".equals(sharedPreference.getKey()) && sharedPreference.hasValue()) {
                SignalStore.account().restoreLegacyIdentityPrivateKeyFromBackup(sharedPreference.getValue());
            }
        } else if (sharedPreference.hasValue()) {
            sharedPreferences.edit().putString(sharedPreference.getKey(), sharedPreference.getValue()).commit();
        } else if (sharedPreference.hasBooleanValue()) {
            sharedPreferences.edit().putBoolean(sharedPreference.getKey(), sharedPreference.getBooleanValue()).commit();
        } else if (sharedPreference.hasIsStringSetValue() && sharedPreference.getIsStringSetValue()) {
            sharedPreferences.edit().putStringSet(sharedPreference.getKey(), new HashSet(sharedPreference.getStringSetValueList())).commit();
        }
    }

    private static void dropAllTables(SQLiteDatabase sQLiteDatabase) {
        String[] strArr = TABLES_TO_DROP_FIRST;
        for (String str : strArr) {
            sQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + str);
        }
        Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT name, type FROM sqlite_master", (String[]) null);
        while (rawQuery != null) {
            try {
                if (!rawQuery.moveToNext()) {
                    break;
                }
                String string = rawQuery.getString(0);
                if ("table".equals(rawQuery.getString(1)) && !string.startsWith("sqlite_")) {
                    Log.i(TAG, "Dropping table: " + string);
                    sQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + string);
                }
            } catch (Throwable th) {
                try {
                    rawQuery.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (rawQuery != null) {
            rawQuery.close();
        }
    }

    /* loaded from: classes.dex */
    public static class BackupRecordInputStream extends FullBackupBase.BackupStream {
        private final Cipher cipher;
        private final byte[] cipherKey;
        private int counter;
        private final InputStream in;
        private byte[] iv;
        private final Mac mac;
        private final byte[] macKey;

        private BackupRecordInputStream(InputStream inputStream, String str) throws IOException {
            try {
                this.in = inputStream;
                byte[] bArr = new byte[4];
                StreamUtil.readFully(inputStream, bArr);
                byte[] bArr2 = new byte[Conversions.byteArrayToInt(bArr)];
                StreamUtil.readFully(inputStream, bArr2);
                BackupProtos.BackupFrame parseFrom = BackupProtos.BackupFrame.parseFrom(bArr2);
                if (parseFrom.hasHeader()) {
                    BackupProtos.Header header = parseFrom.getHeader();
                    byte[] byteArray = header.getIv().toByteArray();
                    this.iv = byteArray;
                    if (byteArray.length == 16) {
                        byte[] backupKey = FullBackupBase.BackupStream.getBackupKey(str, header.hasSalt() ? header.getSalt().toByteArray() : null);
                        new HKDFv3();
                        byte[][] split = ByteUtil.split(HKDF.deriveSecrets(backupKey, "Backup Export".getBytes(), 64), 32, 32);
                        this.cipherKey = split[0];
                        byte[] bArr3 = split[1];
                        this.macKey = bArr3;
                        this.cipher = Cipher.getInstance("AES/CTR/NoPadding");
                        Mac instance = Mac.getInstance("HmacSHA256");
                        this.mac = instance;
                        instance.init(new SecretKeySpec(bArr3, "HmacSHA256"));
                        this.counter = Conversions.byteArrayToInt(this.iv);
                        return;
                    }
                    throw new IOException("Invalid IV length!");
                }
                throw new IOException("Backup stream does not start with header!");
            } catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {
                throw new AssertionError(e);
            }
        }

        BackupProtos.BackupFrame readFrame() throws IOException {
            return readFrame(this.in);
        }

        void readAttachmentTo(OutputStream outputStream, int i) throws IOException {
            try {
                byte[] bArr = this.iv;
                int i2 = this.counter;
                this.counter = i2 + 1;
                Conversions.intToByteArray(bArr, 0, i2);
                this.cipher.init(2, new SecretKeySpec(this.cipherKey, "AES"), new IvParameterSpec(this.iv));
                this.mac.update(this.iv);
                byte[] bArr2 = new byte[8192];
                while (i > 0) {
                    int read = this.in.read(bArr2, 0, Math.min(8192, i));
                    if (read != -1) {
                        this.mac.update(bArr2, 0, read);
                        byte[] update = this.cipher.update(bArr2, 0, read);
                        if (update != null) {
                            outputStream.write(update, 0, update.length);
                        }
                        i -= read;
                    } else {
                        throw new IOException("File ended early!");
                    }
                }
                byte[] doFinal = this.cipher.doFinal();
                if (doFinal != null) {
                    outputStream.write(doFinal, 0, doFinal.length);
                }
                outputStream.close();
                byte[] trim = ByteUtil.trim(this.mac.doFinal(), 10);
                byte[] bArr3 = new byte[10];
                try {
                    StreamUtil.readFully(this.in, bArr3);
                    if (!MessageDigest.isEqual(trim, bArr3)) {
                        throw new BadMacException();
                    }
                } catch (IOException e) {
                    throw new IOException(e);
                }
            } catch (InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e2) {
                throw new AssertionError(e2);
            }
        }

        private BackupProtos.BackupFrame readFrame(InputStream inputStream) throws IOException {
            try {
                byte[] bArr = new byte[4];
                StreamUtil.readFully(inputStream, bArr);
                int byteArrayToInt = Conversions.byteArrayToInt(bArr);
                byte[] bArr2 = new byte[byteArrayToInt];
                StreamUtil.readFully(inputStream, bArr2);
                byte[] bArr3 = new byte[10];
                int i = byteArrayToInt - 10;
                System.arraycopy(bArr2, i, bArr3, 0, 10);
                this.mac.update(bArr2, 0, i);
                if (MessageDigest.isEqual(ByteUtil.trim(this.mac.doFinal(), 10), bArr3)) {
                    byte[] bArr4 = this.iv;
                    int i2 = this.counter;
                    this.counter = i2 + 1;
                    Conversions.intToByteArray(bArr4, 0, i2);
                    this.cipher.init(2, new SecretKeySpec(this.cipherKey, "AES"), new IvParameterSpec(this.iv));
                    return BackupProtos.BackupFrame.parseFrom(this.cipher.doFinal(bArr2, 0, i));
                }
                throw new IOException("Bad MAC");
            } catch (InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
                throw new AssertionError(e);
            }
        }
    }

    /* loaded from: classes.dex */
    public static class BadMacException extends IOException {
        private BadMacException() {
        }
    }

    /* loaded from: classes.dex */
    public static class DatabaseDowngradeException extends IOException {
        DatabaseDowngradeException(int i, int i2) {
            super("Tried to import a backup with version " + i2 + " into a database with version " + i);
        }
    }
}
