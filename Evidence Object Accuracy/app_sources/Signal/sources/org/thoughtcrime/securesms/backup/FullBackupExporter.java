package org.thoughtcrime.securesms.backup;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import androidx.documentfile.provider.DocumentFile;
import com.annimon.stream.function.Predicate;
import com.google.protobuf.ByteString;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.greenrobot.eventbus.EventBus;
import org.signal.core.util.Conversions;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.SetUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.kdf.HKDF;
import org.signal.libsignal.protocol.kdf.HKDFv3;
import org.signal.libsignal.protocol.util.ByteUtil;
import org.thoughtcrime.securesms.attachments.AttachmentId;
import org.thoughtcrime.securesms.backup.BackupProtos;
import org.thoughtcrime.securesms.backup.FullBackupBase;
import org.thoughtcrime.securesms.crypto.AttachmentSecret;
import org.thoughtcrime.securesms.crypto.ClassicDecryptingPartInputStream;
import org.thoughtcrime.securesms.crypto.ModernDecryptingPartInputStream;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.EmojiSearchDatabase;
import org.thoughtcrime.securesms.database.GroupReceiptDatabase;
import org.thoughtcrime.securesms.database.KeyValueDatabase;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.OneTimePreKeyDatabase;
import org.thoughtcrime.securesms.database.PendingRetryReceiptDatabase;
import org.thoughtcrime.securesms.database.SearchDatabase;
import org.thoughtcrime.securesms.database.SenderKeyDatabase;
import org.thoughtcrime.securesms.database.SenderKeySharedDatabase;
import org.thoughtcrime.securesms.database.SessionDatabase;
import org.thoughtcrime.securesms.database.SignedPreKeyDatabase;
import org.thoughtcrime.securesms.database.StickerDatabase;
import org.thoughtcrime.securesms.database.model.AvatarPickerDatabase;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.KeyValueDataSet;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes.dex */
public class FullBackupExporter extends FullBackupBase {
    private static final Set<String> BLACKLISTED_TABLES = SetUtil.newHashSet(SignedPreKeyDatabase.TABLE_NAME, OneTimePreKeyDatabase.TABLE_NAME, SessionDatabase.TABLE_NAME, SearchDatabase.SMS_FTS_TABLE_NAME, SearchDatabase.MMS_FTS_TABLE_NAME, EmojiSearchDatabase.TABLE_NAME, SenderKeyDatabase.TABLE_NAME, SenderKeySharedDatabase.TABLE_NAME, PendingRetryReceiptDatabase.TABLE_NAME, AvatarPickerDatabase.TABLE_NAME);
    private static final long DATABASE_VERSION_RECORD_COUNT;
    private static final long FINAL_MESSAGE_COUNT;
    private static final long IDENTITY_KEY_BACKUP_RECORD_COUNT;
    private static final long TABLE_RECORD_COUNT_MULTIPLIER;
    private static final String TAG = Log.tag(FullBackupExporter.class);

    /* loaded from: classes.dex */
    public static final class BackupCanceledException extends IOException {
    }

    /* loaded from: classes.dex */
    public interface BackupCancellationSignal {
        boolean isCanceled();
    }

    /* loaded from: classes.dex */
    public interface PostProcessor {
        int postProcess(Cursor cursor, int i);
    }

    public static /* synthetic */ boolean lambda$internalExport$8(Cursor cursor) {
        return true;
    }

    public static /* synthetic */ boolean lambda$transfer$0() {
        return false;
    }

    public static void export(Context context, AttachmentSecret attachmentSecret, SQLiteDatabase sQLiteDatabase, File file, String str, BackupCancellationSignal backupCancellationSignal) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        try {
            internalExport(context, attachmentSecret, sQLiteDatabase, fileOutputStream, str, true, backupCancellationSignal);
            fileOutputStream.close();
        } catch (Throwable th) {
            try {
                fileOutputStream.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public static void export(Context context, AttachmentSecret attachmentSecret, SQLiteDatabase sQLiteDatabase, DocumentFile documentFile, String str, BackupCancellationSignal backupCancellationSignal) throws IOException {
        OutputStream openOutputStream = context.getContentResolver().openOutputStream(documentFile.getUri());
        Objects.requireNonNull(openOutputStream);
        try {
            internalExport(context, attachmentSecret, sQLiteDatabase, openOutputStream, str, true, backupCancellationSignal);
            openOutputStream.close();
        } catch (Throwable th) {
            try {
                openOutputStream.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }

    public static void transfer(Context context, AttachmentSecret attachmentSecret, SQLiteDatabase sQLiteDatabase, OutputStream outputStream, String str) throws IOException {
        internalExport(context, attachmentSecret, sQLiteDatabase, outputStream, str, false, new BackupCancellationSignal() { // from class: org.thoughtcrime.securesms.backup.FullBackupExporter$$ExternalSyntheticLambda9
            @Override // org.thoughtcrime.securesms.backup.FullBackupExporter.BackupCancellationSignal
            public final boolean isCanceled() {
                return FullBackupExporter.m364$r8$lambda$Jk1LnTvJ8uTA2RWqvoqNi46Hng();
            }
        });
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:(6:95|11|(1:13)(2:15|(1:17)(2:18|(1:20)(2:21|(1:23)(2:24|(1:26)(2:27|(1:29)(2:30|(1:32)(4:33|(1:37)|38|39)))))))|14|38|39)|93|9) */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0242, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0243, code lost:
        r8 = r14;
     */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x0259  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void internalExport(android.content.Context r20, org.thoughtcrime.securesms.crypto.AttachmentSecret r21, net.zetetic.database.sqlcipher.SQLiteDatabase r22, java.io.OutputStream r23, java.lang.String r24, boolean r25, org.thoughtcrime.securesms.backup.FullBackupExporter.BackupCancellationSignal r26) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 625
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.backup.FullBackupExporter.internalExport(android.content.Context, org.thoughtcrime.securesms.crypto.AttachmentSecret, net.zetetic.database.sqlcipher.SQLiteDatabase, java.io.OutputStream, java.lang.String, boolean, org.thoughtcrime.securesms.backup.FullBackupExporter$BackupCancellationSignal):void");
    }

    public static /* synthetic */ boolean lambda$internalExport$1(Cursor cursor) {
        return isNonExpiringMmsMessage(cursor) && isNotReleaseChannel(cursor);
    }

    public static /* synthetic */ boolean lambda$internalExport$2(Cursor cursor) {
        return isNonExpiringSmsMessage(cursor) && isNotReleaseChannel(cursor);
    }

    public static /* synthetic */ boolean lambda$internalExport$3(SQLiteDatabase sQLiteDatabase, Cursor cursor) {
        return isForNonExpiringMessage(sQLiteDatabase, new MessageId(CursorUtil.requireLong(cursor, "message_id"), CursorUtil.requireBoolean(cursor, "is_mms")));
    }

    public static /* synthetic */ boolean lambda$internalExport$4(SQLiteDatabase sQLiteDatabase, Cursor cursor) {
        return isForNonExpiringMmsMessageAndNotReleaseChannel(sQLiteDatabase, CursorUtil.requireLong(cursor, "message_id"));
    }

    public static /* synthetic */ boolean lambda$internalExport$5(SQLiteDatabase sQLiteDatabase, Cursor cursor) {
        return isForNonExpiringMmsMessageAndNotReleaseChannel(sQLiteDatabase, cursor.getLong(cursor.getColumnIndexOrThrow(GroupReceiptDatabase.MMS_ID)));
    }

    public static /* synthetic */ boolean lambda$internalExport$6(SQLiteDatabase sQLiteDatabase, Cursor cursor) {
        return isForNonExpiringMmsMessageAndNotReleaseChannel(sQLiteDatabase, cursor.getLong(cursor.getColumnIndexOrThrow(AttachmentDatabase.MMS_ID)));
    }

    private static long calculateCount(Context context, SQLiteDatabase sQLiteDatabase, List<String> list) {
        long count;
        long size = (((long) list.size()) * TABLE_RECORD_COUNT_MULTIPLIER) + 1;
        for (String str : list) {
            if (str.equals("mms")) {
                count = getCount(sQLiteDatabase, BackupCountQueries.mmsCount);
            } else if (str.equals("sms")) {
                count = getCount(sQLiteDatabase, BackupCountQueries.smsCount);
            } else if (str.equals(GroupReceiptDatabase.TABLE_NAME)) {
                count = getCount(sQLiteDatabase, BackupCountQueries.getGroupReceiptCount());
            } else if (str.equals(AttachmentDatabase.TABLE_NAME)) {
                count = getCount(sQLiteDatabase, BackupCountQueries.getAttachmentCount());
            } else if (str.equals(StickerDatabase.TABLE_NAME)) {
                count = getCount(sQLiteDatabase, "SELECT COUNT(*) FROM " + str);
            } else if (!BLACKLISTED_TABLES.contains(str) && !str.startsWith("sqlite_")) {
                count = getCount(sQLiteDatabase, "SELECT COUNT(*) FROM " + str);
            }
            size += count;
        }
        long preferencesToSaveToBackupCount = size + IDENTITY_KEY_BACKUP_RECORD_COUNT + TextSecurePreferences.getPreferencesToSaveToBackupCount(context);
        KeyValueDataSet dataSet = KeyValueDatabase.getInstance(ApplicationDependencies.getApplication()).getDataSet();
        for (String str2 : SignalStore.getKeysToIncludeInBackup()) {
            if (dataSet.containsKey(str2)) {
                preferencesToSaveToBackupCount++;
            }
        }
        return preferencesToSaveToBackupCount + AvatarHelper.getAvatarCount(context) + 1;
    }

    private static long getCount(SQLiteDatabase sQLiteDatabase, String str) {
        Cursor rawQuery = sQLiteDatabase.rawQuery(str, new Object[0]);
        try {
            long j = rawQuery.moveToFirst() ? rawQuery.getLong(0) : 0;
            rawQuery.close();
            return j;
        } catch (Throwable th) {
            if (rawQuery != null) {
                try {
                    rawQuery.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    private static void throwIfCanceled(BackupCancellationSignal backupCancellationSignal) throws BackupCanceledException {
        if (backupCancellationSignal.isCanceled()) {
            throw new BackupCanceledException();
        }
    }

    private static List<String> exportSchema(SQLiteDatabase sQLiteDatabase, BackupFrameOutputStream backupFrameOutputStream) throws IOException {
        LinkedList linkedList = new LinkedList();
        Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT sql, name, type FROM sqlite_master", (String[]) null);
        while (rawQuery != null) {
            try {
                if (!rawQuery.moveToNext()) {
                    break;
                }
                String string = rawQuery.getString(0);
                boolean z = true;
                String string2 = rawQuery.getString(1);
                String string3 = rawQuery.getString(2);
                if (string != null) {
                    boolean z2 = string2 != null && !string2.equals(SearchDatabase.SMS_FTS_TABLE_NAME) && string2.startsWith(SearchDatabase.SMS_FTS_TABLE_NAME);
                    boolean z3 = string2 != null && !string2.equals(SearchDatabase.MMS_FTS_TABLE_NAME) && string2.startsWith(SearchDatabase.MMS_FTS_TABLE_NAME);
                    if (string2 == null || string2.equals(EmojiSearchDatabase.TABLE_NAME) || !string2.startsWith(EmojiSearchDatabase.TABLE_NAME)) {
                        z = false;
                    }
                    if (!z2 && !z3 && !z) {
                        if ("table".equals(string3)) {
                            linkedList.add(string2);
                        }
                        backupFrameOutputStream.write(BackupProtos.SqlStatement.newBuilder().setStatement(rawQuery.getString(0)).build());
                    }
                }
            } catch (Throwable th) {
                try {
                    rawQuery.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (rawQuery != null) {
            rawQuery.close();
        }
        return linkedList;
    }

    private static int exportTable(String str, SQLiteDatabase sQLiteDatabase, BackupFrameOutputStream backupFrameOutputStream, Predicate<Cursor> predicate, PostProcessor postProcessor, int i, long j, BackupCancellationSignal backupCancellationSignal) throws IOException {
        String str2 = "INSERT INTO " + str + " VALUES ";
        Cursor rawQuery = sQLiteDatabase.rawQuery("SELECT * FROM " + str, (String[]) null);
        int i2 = i;
        while (rawQuery != null) {
            try {
                if (!rawQuery.moveToNext()) {
                    break;
                }
                throwIfCanceled(backupCancellationSignal);
                if (predicate == null || predicate.test(rawQuery)) {
                    StringBuilder sb = new StringBuilder(str2);
                    BackupProtos.SqlStatement.Builder newBuilder = BackupProtos.SqlStatement.newBuilder();
                    sb.append('(');
                    for (int i3 = 0; i3 < rawQuery.getColumnCount(); i3++) {
                        sb.append('?');
                        if (rawQuery.getType(i3) == 3) {
                            newBuilder.addParameters(BackupProtos.SqlStatement.SqlParameter.newBuilder().setStringParamter(rawQuery.getString(i3)));
                        } else if (rawQuery.getType(i3) == 2) {
                            newBuilder.addParameters(BackupProtos.SqlStatement.SqlParameter.newBuilder().setDoubleParameter(rawQuery.getDouble(i3)));
                        } else if (rawQuery.getType(i3) == 1) {
                            newBuilder.addParameters(BackupProtos.SqlStatement.SqlParameter.newBuilder().setIntegerParameter(rawQuery.getLong(i3)));
                        } else if (rawQuery.getType(i3) == 4) {
                            newBuilder.addParameters(BackupProtos.SqlStatement.SqlParameter.newBuilder().setBlobParameter(ByteString.copyFrom(rawQuery.getBlob(i3))));
                        } else if (rawQuery.getType(i3) == 0) {
                            newBuilder.addParameters(BackupProtos.SqlStatement.SqlParameter.newBuilder().setNullparameter(true));
                        } else {
                            throw new AssertionError("unknown type?" + rawQuery.getType(i3));
                        }
                        if (i3 < rawQuery.getColumnCount() - 1) {
                            sb.append(',');
                        }
                    }
                    sb.append(')');
                    i2++;
                    EventBus.getDefault().post(new FullBackupBase.BackupEvent(FullBackupBase.BackupEvent.Type.PROGRESS, (long) i2, j));
                    backupFrameOutputStream.write(newBuilder.setStatement(sb.toString()).build());
                    if (postProcessor != null) {
                        i2 = postProcessor.postProcess(rawQuery, i2);
                    }
                }
            } catch (Throwable th) {
                try {
                    rawQuery.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (rawQuery != null) {
            rawQuery.close();
        }
        return i2;
    }

    public static int exportAttachment(AttachmentSecret attachmentSecret, Cursor cursor, BackupFrameOutputStream backupFrameOutputStream, int i, long j) {
        int i2;
        IOException e;
        InputStream inputStream;
        try {
            long j2 = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
            long j3 = cursor.getLong(cursor.getColumnIndexOrThrow(AttachmentDatabase.UNIQUE_ID));
            long j4 = cursor.getLong(cursor.getColumnIndexOrThrow(AttachmentDatabase.SIZE));
            String string = cursor.getString(cursor.getColumnIndexOrThrow(AttachmentDatabase.DATA));
            byte[] blob = cursor.getBlob(cursor.getColumnIndexOrThrow(AttachmentDatabase.DATA_RANDOM));
            if (!TextUtils.isEmpty(string)) {
                long length = new File(string).length();
                if (j4 <= 0 || length != j4) {
                    long calculateVeryOldStreamLength = calculateVeryOldStreamLength(attachmentSecret, blob, string);
                    String str = TAG;
                    Log.w(str, "Needed size calculation! Manual: " + calculateVeryOldStreamLength + " File: " + length + "  DB: " + j4 + " ID: " + new AttachmentId(j2, j3));
                    j4 = calculateVeryOldStreamLength;
                }
            }
            if (TextUtils.isEmpty(string) || j4 <= 0) {
                return i;
            }
            if (blob == null || blob.length != 32) {
                inputStream = ClassicDecryptingPartInputStream.createFor(attachmentSecret, new File(string));
            } else {
                inputStream = ModernDecryptingPartInputStream.createFor(attachmentSecret, blob, new File(string), 0);
            }
            i2 = i + 1;
            try {
                EventBus.getDefault().post(new FullBackupBase.BackupEvent(FullBackupBase.BackupEvent.Type.PROGRESS, (long) i2, j));
                backupFrameOutputStream.write(new AttachmentId(j2, j3), inputStream, j4);
                inputStream.close();
                return i2;
            } catch (IOException e2) {
                e = e2;
                Log.w(TAG, e);
                return i2;
            }
        } catch (IOException e3) {
            e = e3;
            i2 = i;
        }
    }

    public static int exportSticker(AttachmentSecret attachmentSecret, Cursor cursor, BackupFrameOutputStream backupFrameOutputStream, int i, long j) {
        int i2;
        IOException e;
        try {
            long j2 = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
            long j3 = cursor.getLong(cursor.getColumnIndexOrThrow(StickerDatabase.FILE_LENGTH));
            String string = cursor.getString(cursor.getColumnIndexOrThrow(StickerDatabase.FILE_PATH));
            byte[] blob = cursor.getBlob(cursor.getColumnIndexOrThrow(StickerDatabase.FILE_RANDOM));
            if (TextUtils.isEmpty(string) || j3 <= 0) {
                return i;
            }
            int i3 = i + 1;
            i2 = i3;
            try {
                EventBus.getDefault().post(new FullBackupBase.BackupEvent(FullBackupBase.BackupEvent.Type.PROGRESS, (long) i3, j));
                InputStream createFor = ModernDecryptingPartInputStream.createFor(attachmentSecret, blob, new File(string), 0);
                backupFrameOutputStream.writeSticker(j2, createFor, j3);
                if (createFor == null) {
                    return i2;
                }
                createFor.close();
                return i2;
            } catch (IOException e2) {
                e = e2;
                Log.w(TAG, e);
                return i2;
            }
        } catch (IOException e3) {
            e = e3;
            i2 = i;
        }
    }

    private static long calculateVeryOldStreamLength(AttachmentSecret attachmentSecret, byte[] bArr, String str) throws IOException {
        InputStream inputStream;
        long j = 0;
        if (bArr == null || bArr.length != 32) {
            inputStream = ClassicDecryptingPartInputStream.createFor(attachmentSecret, new File(str));
        } else {
            inputStream = ModernDecryptingPartInputStream.createFor(attachmentSecret, bArr, new File(str), 0);
        }
        byte[] bArr2 = new byte[8192];
        while (true) {
            int read = inputStream.read(bArr2, 0, 8192);
            if (read == -1) {
                return j;
            }
            j += (long) read;
        }
    }

    private static int exportKeyValues(BackupFrameOutputStream backupFrameOutputStream, List<String> list, int i, long j, BackupCancellationSignal backupCancellationSignal) throws IOException {
        KeyValueDataSet dataSet = KeyValueDatabase.getInstance(ApplicationDependencies.getApplication()).getDataSet();
        for (String str : list) {
            throwIfCanceled(backupCancellationSignal);
            if (dataSet.containsKey(str)) {
                BackupProtos.KeyValue.Builder key = BackupProtos.KeyValue.newBuilder().setKey(str);
                Class type = dataSet.getType(str);
                if (type == byte[].class) {
                    if (dataSet.getBlob(str, null) != null) {
                        key.setBlobValue(ByteString.copyFrom(dataSet.getBlob(str, null)));
                    } else {
                        String str2 = TAG;
                        Log.w(str2, "Skipping storing null blob for key: " + str);
                    }
                } else if (type == Boolean.class) {
                    key.setBooleanValue(dataSet.getBoolean(str, false));
                } else if (type == Float.class) {
                    key.setFloatValue(dataSet.getFloat(str, 0.0f));
                } else if (type == Integer.class) {
                    key.setIntegerValue(dataSet.getInteger(str, 0));
                } else if (type == Long.class) {
                    key.setLongValue(dataSet.getLong(str, 0));
                } else if (type != String.class) {
                    throw new AssertionError("Unknown type: " + type);
                } else if (dataSet.getString(str, null) != null) {
                    key.setStringValue(dataSet.getString(str, null));
                } else {
                    String str3 = TAG;
                    Log.w(str3, "Skipping storing null string for key: " + str);
                }
                i++;
                EventBus.getDefault().post(new FullBackupBase.BackupEvent(FullBackupBase.BackupEvent.Type.PROGRESS, (long) i, j));
                backupFrameOutputStream.write(key.build());
            }
        }
        return i;
    }

    private static boolean isNonExpiringMmsMessage(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow("expires_in")) <= 0 && cursor.getLong(cursor.getColumnIndexOrThrow(MmsDatabase.VIEW_ONCE)) <= 0;
    }

    private static boolean isNonExpiringSmsMessage(Cursor cursor) {
        return cursor.getLong(cursor.getColumnIndexOrThrow("expires_in")) <= 0;
    }

    private static boolean isForNonExpiringMessage(SQLiteDatabase sQLiteDatabase, MessageId messageId) {
        if (messageId.isMms()) {
            return isForNonExpiringMmsMessageAndNotReleaseChannel(sQLiteDatabase, messageId.getId());
        }
        return isForNonExpiringSmsMessage(sQLiteDatabase, messageId.getId());
    }

    private static boolean isForNonExpiringSmsMessage(SQLiteDatabase sQLiteDatabase, long j) {
        Cursor query = sQLiteDatabase.query("sms", new String[]{"expires_in"}, "_id = ?", new String[]{String.valueOf(j)}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    boolean isNonExpiringSmsMessage = isNonExpiringSmsMessage(query);
                    query.close();
                    return isNonExpiringSmsMessage;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0034, code lost:
        if (isNotReleaseChannel(r11) != false) goto L_0x0038;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean isForNonExpiringMmsMessageAndNotReleaseChannel(net.zetetic.database.sqlcipher.SQLiteDatabase r11, long r12) {
        /*
            java.lang.String r0 = "address"
            java.lang.String r1 = "expires_in"
            java.lang.String r2 = "reveal_duration"
            java.lang.String[] r5 = new java.lang.String[]{r0, r1, r2}
            r0 = 1
            java.lang.String[] r7 = new java.lang.String[r0]
            java.lang.String r12 = java.lang.String.valueOf(r12)
            r13 = 0
            r7[r13] = r12
            java.lang.String r4 = "mms"
            java.lang.String r6 = "_id = ?"
            r8 = 0
            r9 = 0
            r10 = 0
            r3 = r11
            android.database.Cursor r11 = r3.query(r4, r5, r6, r7, r8, r9, r10)
            if (r11 == 0) goto L_0x0046
            boolean r12 = r11.moveToFirst()     // Catch: all -> 0x003c
            if (r12 == 0) goto L_0x0046
            boolean r12 = isNonExpiringMmsMessage(r11)     // Catch: all -> 0x003c
            if (r12 == 0) goto L_0x0037
            boolean r12 = isNotReleaseChannel(r11)     // Catch: all -> 0x003c
            if (r12 == 0) goto L_0x0037
            goto L_0x0038
        L_0x0037:
            r0 = 0
        L_0x0038:
            r11.close()
            return r0
        L_0x003c:
            r12 = move-exception
            r11.close()     // Catch: all -> 0x0041
            goto L_0x0045
        L_0x0041:
            r11 = move-exception
            r12.addSuppressed(r11)
        L_0x0045:
            throw r12
        L_0x0046:
            if (r11 == 0) goto L_0x004b
            r11.close()
        L_0x004b:
            return r13
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.backup.FullBackupExporter.isForNonExpiringMmsMessageAndNotReleaseChannel(net.zetetic.database.sqlcipher.SQLiteDatabase, long):boolean");
    }

    private static boolean isNotReleaseChannel(Cursor cursor) {
        RecipientId releaseChannelRecipientId = SignalStore.releaseChannelValues().getReleaseChannelRecipientId();
        return releaseChannelRecipientId == null || cursor.getLong(cursor.getColumnIndexOrThrow("address")) != releaseChannelRecipientId.toLong();
    }

    /* loaded from: classes.dex */
    public static class BackupFrameOutputStream extends FullBackupBase.BackupStream {
        private final Cipher cipher;
        private final byte[] cipherKey;
        private int counter;
        private byte[] iv;
        private final Mac mac;
        private final byte[] macKey;
        private final OutputStream outputStream;

        private BackupFrameOutputStream(OutputStream outputStream, String str) throws IOException {
            try {
                byte[] secretBytes = Util.getSecretBytes(32);
                byte[] backupKey = FullBackupBase.BackupStream.getBackupKey(str, secretBytes);
                new HKDFv3();
                byte[][] split = ByteUtil.split(HKDF.deriveSecrets(backupKey, "Backup Export".getBytes(), 64), 32, 32);
                this.cipherKey = split[0];
                byte[] bArr = split[1];
                this.macKey = bArr;
                this.cipher = Cipher.getInstance("AES/CTR/NoPadding");
                Mac instance = Mac.getInstance("HmacSHA256");
                this.mac = instance;
                this.outputStream = outputStream;
                byte[] secretBytes2 = Util.getSecretBytes(16);
                this.iv = secretBytes2;
                this.counter = Conversions.byteArrayToInt(secretBytes2);
                instance.init(new SecretKeySpec(bArr, "HmacSHA256"));
                byte[] byteArray = BackupProtos.BackupFrame.newBuilder().setHeader(BackupProtos.Header.newBuilder().setIv(ByteString.copyFrom(this.iv)).setSalt(ByteString.copyFrom(secretBytes))).build().toByteArray();
                outputStream.write(Conversions.intToByteArray(byteArray.length));
                outputStream.write(byteArray);
            } catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {
                throw new AssertionError(e);
            }
        }

        public void write(BackupProtos.SharedPreference sharedPreference) throws IOException {
            write(this.outputStream, BackupProtos.BackupFrame.newBuilder().setPreference(sharedPreference).build());
        }

        public void write(BackupProtos.KeyValue keyValue) throws IOException {
            write(this.outputStream, BackupProtos.BackupFrame.newBuilder().setKeyValue(keyValue).build());
        }

        public void write(BackupProtos.SqlStatement sqlStatement) throws IOException {
            write(this.outputStream, BackupProtos.BackupFrame.newBuilder().setStatement(sqlStatement).build());
        }

        public void write(String str, InputStream inputStream, long j) throws IOException {
            write(this.outputStream, BackupProtos.BackupFrame.newBuilder().setAvatar(BackupProtos.Avatar.newBuilder().setRecipientId(str).setLength(Util.toIntExact(j)).build()).build());
            if (writeStream(inputStream) != j) {
                throw new IOException("Size mismatch!");
            }
        }

        public void write(AttachmentId attachmentId, InputStream inputStream, long j) throws IOException {
            write(this.outputStream, BackupProtos.BackupFrame.newBuilder().setAttachment(BackupProtos.Attachment.newBuilder().setRowId(attachmentId.getRowId()).setAttachmentId(attachmentId.getUniqueId()).setLength(Util.toIntExact(j)).build()).build());
            if (writeStream(inputStream) != j) {
                throw new IOException("Size mismatch!");
            }
        }

        public void writeSticker(long j, InputStream inputStream, long j2) throws IOException {
            write(this.outputStream, BackupProtos.BackupFrame.newBuilder().setSticker(BackupProtos.Sticker.newBuilder().setRowId(j).setLength(Util.toIntExact(j2)).build()).build());
            if (writeStream(inputStream) != j2) {
                throw new IOException("Size mismatch!");
            }
        }

        void writeDatabaseVersion(int i) throws IOException {
            write(this.outputStream, BackupProtos.BackupFrame.newBuilder().setVersion(BackupProtos.DatabaseVersion.newBuilder().setVersion(i)).build());
        }

        void writeEnd() throws IOException {
            write(this.outputStream, BackupProtos.BackupFrame.newBuilder().setEnd(true).build());
        }

        private long writeStream(InputStream inputStream) throws IOException {
            try {
                byte[] bArr = this.iv;
                int i = this.counter;
                this.counter = i + 1;
                Conversions.intToByteArray(bArr, 0, i);
                this.cipher.init(1, new SecretKeySpec(this.cipherKey, "AES"), new IvParameterSpec(this.iv));
                this.mac.update(this.iv);
                byte[] bArr2 = new byte[8192];
                long j = 0;
                while (true) {
                    int read = inputStream.read(bArr2);
                    if (read != -1) {
                        byte[] update = this.cipher.update(bArr2, 0, read);
                        if (update != null) {
                            this.outputStream.write(update);
                            this.mac.update(update);
                        }
                        j += (long) read;
                    } else {
                        byte[] doFinal = this.cipher.doFinal();
                        this.outputStream.write(doFinal);
                        this.mac.update(doFinal);
                        this.outputStream.write(this.mac.doFinal(), 0, 10);
                        return j;
                    }
                }
            } catch (InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
                throw new AssertionError(e);
            }
        }

        private void write(OutputStream outputStream, BackupProtos.BackupFrame backupFrame) throws IOException {
            try {
                byte[] bArr = this.iv;
                int i = this.counter;
                this.counter = i + 1;
                Conversions.intToByteArray(bArr, 0, i);
                this.cipher.init(1, new SecretKeySpec(this.cipherKey, "AES"), new IvParameterSpec(this.iv));
                byte[] doFinal = this.cipher.doFinal(backupFrame.toByteArray());
                byte[] doFinal2 = this.mac.doFinal(doFinal);
                outputStream.write(Conversions.intToByteArray(doFinal.length + 10));
                outputStream.write(doFinal);
                outputStream.write(doFinal2, 0, 10);
            } catch (InvalidAlgorithmParameterException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
                throw new AssertionError(e);
            }
        }

        public void close() throws IOException {
            this.outputStream.close();
        }
    }
}
