package org.thoughtcrime.securesms.backup;

import android.app.PendingIntent;
import android.content.Context;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import java.io.IOException;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsActivity;
import org.thoughtcrime.securesms.notifications.NotificationCancellationHelper;
import org.thoughtcrime.securesms.notifications.NotificationChannels;

/* loaded from: classes.dex */
public enum BackupFileIOError {
    ACCESS_ERROR(R.string.LocalBackupJobApi29_backup_failed, R.string.LocalBackupJobApi29_your_backup_directory_has_been_deleted_or_moved),
    FILE_TOO_LARGE(R.string.LocalBackupJobApi29_backup_failed, R.string.LocalBackupJobApi29_your_backup_file_is_too_large),
    NOT_ENOUGH_SPACE(R.string.LocalBackupJobApi29_backup_failed, R.string.LocalBackupJobApi29_there_is_not_enough_space),
    UNKNOWN(R.string.LocalBackupJobApi29_backup_failed, R.string.LocalBackupJobApi29_tap_to_manage_backups);
    
    private static final short BACKUP_FAILED_ID;
    private final int messageId;
    private final int titleId;

    BackupFileIOError(int i, int i2) {
        this.titleId = i;
        this.messageId = i2;
    }

    public static void clearNotification(Context context) {
        NotificationCancellationHelper.cancelLegacy(context, 31321);
    }

    public void postNotification(Context context) {
        NotificationManagerCompat.from(context).notify(31321, new NotificationCompat.Builder(context, NotificationChannels.FAILURES).setSmallIcon(R.drawable.ic_signal_backup).setContentTitle(context.getString(this.titleId)).setContentText(context.getString(this.messageId)).setContentIntent(PendingIntent.getActivity(context, -1, AppSettingsActivity.backups(context), 0)).build());
    }

    public static void postNotificationForException(Context context, IOException iOException, int i) {
        BackupFileIOError fromException = getFromException(iOException);
        if (fromException != null) {
            fromException.postNotification(context);
        }
        if (fromException == null && i > 0) {
            UNKNOWN.postNotification(context);
        }
    }

    private static BackupFileIOError getFromException(IOException iOException) {
        if (iOException.getMessage() == null) {
            return null;
        }
        if (iOException.getMessage().contains("EFBIG")) {
            return FILE_TOO_LARGE;
        }
        if (iOException.getMessage().contains("ENOSPC")) {
            return NOT_ENOUGH_SPACE;
        }
        return null;
    }
}
