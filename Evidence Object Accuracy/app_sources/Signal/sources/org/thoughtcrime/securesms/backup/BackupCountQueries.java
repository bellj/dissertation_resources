package org.thoughtcrime.securesms.backup;

import kotlin.Metadata;
import kotlin.jvm.JvmStatic;

/* compiled from: BackupCountQueries.kt */
@Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0013\u0010\u0003\u001a\u00020\u00048G¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0013\u0010\u0007\u001a\u00020\u00048G¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0006R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/backup/BackupCountQueries;", "", "()V", "attachmentCount", "", "getAttachmentCount", "()Ljava/lang/String;", "groupReceiptCount", "getGroupReceiptCount", "mmsCount", "smsCount", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class BackupCountQueries {
    public static final BackupCountQueries INSTANCE = new BackupCountQueries();
    private static final String attachmentCount = "SELECT COUNT(*) FROM part \nINNER JOIN mms ON part.mid = mms._id \nWHERE mms.expires_in <= 0 AND mms.reveal_duration <= 0";
    private static final String groupReceiptCount = "SELECT COUNT(*) FROM group_receipts \nINNER JOIN mms ON group_receipts.mms_id = mms._id \nWHERE mms.expires_in <= 0 AND mms.reveal_duration <= 0";
    public static final String mmsCount;
    public static final String smsCount;

    private BackupCountQueries() {
    }

    @JvmStatic
    public static final String getGroupReceiptCount() {
        return groupReceiptCount;
    }

    @JvmStatic
    public static final String getAttachmentCount() {
        return attachmentCount;
    }
}
