package org.thoughtcrime.securesms.backup;

import android.content.Context;
import android.os.Build;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.KeyStoreHelper;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes.dex */
public final class BackupPassphrase {
    private static final String TAG = Log.tag(BackupPassphrase.class);

    private BackupPassphrase() {
    }

    public static String get(Context context) {
        String backupPassphrase = TextSecurePreferences.getBackupPassphrase(context);
        String encryptedBackupPassphrase = TextSecurePreferences.getEncryptedBackupPassphrase(context);
        if (Build.VERSION.SDK_INT < 23 || (backupPassphrase == null && encryptedBackupPassphrase == null)) {
            return stripSpaces(backupPassphrase);
        }
        if (encryptedBackupPassphrase == null) {
            Log.i(TAG, "Migrating to encrypted passphrase.");
            set(context, backupPassphrase);
            encryptedBackupPassphrase = TextSecurePreferences.getEncryptedBackupPassphrase(context);
            if (encryptedBackupPassphrase == null) {
                throw new AssertionError("Passphrase migration failed");
            }
        }
        return stripSpaces(new String(KeyStoreHelper.unseal(KeyStoreHelper.SealedData.fromString(encryptedBackupPassphrase))));
    }

    public static void set(Context context, String str) {
        if (str == null || Build.VERSION.SDK_INT < 23) {
            TextSecurePreferences.setBackupPassphrase(context, str);
            TextSecurePreferences.setEncryptedBackupPassphrase(context, null);
            return;
        }
        TextSecurePreferences.setEncryptedBackupPassphrase(context, KeyStoreHelper.seal(str.getBytes()).serialize());
        TextSecurePreferences.setBackupPassphrase(context, null);
    }

    private static String stripSpaces(String str) {
        if (str != null) {
            return str.replace(" ", "");
        }
        return null;
    }
}
