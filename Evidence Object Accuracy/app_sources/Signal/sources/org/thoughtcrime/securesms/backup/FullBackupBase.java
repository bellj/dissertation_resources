package org.thoughtcrime.securesms.backup;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.greenrobot.eventbus.EventBus;
import org.signal.libsignal.protocol.util.ByteUtil;
import org.thoughtcrime.securesms.util.MessageRecordUtil;

/* loaded from: classes.dex */
public abstract class FullBackupBase {
    private static final int DIGEST_ROUNDS;

    /* access modifiers changed from: package-private */
    /* loaded from: classes.dex */
    public static class BackupStream {
        public static byte[] getBackupKey(String str, byte[] bArr) {
            try {
                EventBus.getDefault().post(new BackupEvent(BackupEvent.Type.PROGRESS, 0, 0));
                MessageDigest instance = MessageDigest.getInstance("SHA-512");
                byte[] bytes = str.replace(" ", "").getBytes();
                if (bArr != null) {
                    instance.update(bArr);
                }
                byte[] bArr2 = bytes;
                for (int i = 0; i < FullBackupBase.DIGEST_ROUNDS; i++) {
                    if (i % MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH == 0) {
                        EventBus.getDefault().post(new BackupEvent(BackupEvent.Type.PROGRESS, 0, 0));
                    }
                    instance.update(bArr2);
                    bArr2 = instance.digest(bytes);
                }
                return ByteUtil.trim(bArr2, 32);
            } catch (NoSuchAlgorithmException e) {
                throw new AssertionError(e);
            }
        }
    }

    /* loaded from: classes.dex */
    public static class BackupEvent {
        private final long count;
        private final long estimatedTotalCount;
        private final Type type;

        /* loaded from: classes.dex */
        public enum Type {
            PROGRESS,
            FINISHED
        }

        public BackupEvent(Type type, long j, long j2) {
            this.type = type;
            this.count = j;
            this.estimatedTotalCount = j2;
        }

        public Type getType() {
            return this.type;
        }

        public long getCount() {
            return this.count;
        }

        public long getEstimatedTotalCount() {
            return this.estimatedTotalCount;
        }

        public double getCompletionPercentage() {
            long j = this.estimatedTotalCount;
            if (j == 0) {
                return 0.0d;
            }
            double d = (double) this.count;
            Double.isNaN(d);
            double d2 = (double) j;
            Double.isNaN(d2);
            return Math.min(99.9000015258789d, (d * 100.0d) / d2);
        }
    }
}
