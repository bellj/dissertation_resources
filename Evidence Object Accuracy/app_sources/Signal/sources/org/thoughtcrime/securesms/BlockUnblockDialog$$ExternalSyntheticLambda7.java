package org.thoughtcrime.securesms;

import androidx.appcompat.app.AlertDialog;
import org.signal.core.util.concurrent.SimpleTask;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class BlockUnblockDialog$$ExternalSyntheticLambda7 implements SimpleTask.ForegroundTask {
    @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
    public final void run(Object obj) {
        ((AlertDialog.Builder) obj).show();
    }
}
