package org.thoughtcrime.securesms.pin;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import j$.util.Optional;
import j$.util.function.Consumer;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.lock.v2.PinKeyboardType;
import org.thoughtcrime.securesms.pin.PinRestoreRepository;
import org.thoughtcrime.securesms.util.DefaultValueLiveData;
import org.thoughtcrime.securesms.util.SingleLiveEvent;

/* loaded from: classes4.dex */
public class PinRestoreViewModel extends ViewModel {
    private final SingleLiveEvent<Event> event;
    private final KbsRepository kbsRepository;
    private final PinRestoreRepository repo = new PinRestoreRepository();
    private volatile TokenData tokenData;
    private final DefaultValueLiveData<TriesRemaining> triesRemaining;

    /* loaded from: classes4.dex */
    public enum Event {
        SUCCESS,
        EMPTY_PIN,
        PIN_TOO_SHORT,
        PIN_INCORRECT,
        PIN_LOCKED,
        NETWORK_ERROR
    }

    public PinRestoreViewModel() {
        KbsRepository kbsRepository = new KbsRepository();
        this.kbsRepository = kbsRepository;
        this.triesRemaining = new DefaultValueLiveData<>(new TriesRemaining(10, false));
        this.event = new SingleLiveEvent<>();
        kbsRepository.getToken(new Consumer() { // from class: org.thoughtcrime.securesms.pin.PinRestoreViewModel$$ExternalSyntheticLambda2
            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                PinRestoreViewModel.this.lambda$new$0((Optional) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ Consumer andThen(Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
    }

    public /* synthetic */ void lambda$new$0(Optional optional) {
        if (optional.isPresent()) {
            updateTokenData((TokenData) optional.get(), false);
        } else {
            this.event.postValue(Event.NETWORK_ERROR);
        }
    }

    public void onPinSubmitted(String str, PinKeyboardType pinKeyboardType) {
        int length = str.replace(" ", "").length();
        if (length == 0) {
            this.event.postValue(Event.EMPTY_PIN);
        } else if (length < 4) {
            this.event.postValue(Event.PIN_TOO_SHORT);
        } else if (this.tokenData != null) {
            this.repo.submitPin(str, this.tokenData, new PinRestoreRepository.Callback(pinKeyboardType) { // from class: org.thoughtcrime.securesms.pin.PinRestoreViewModel$$ExternalSyntheticLambda0
                public final /* synthetic */ PinKeyboardType f$1;

                {
                    this.f$1 = r2;
                }

                @Override // org.thoughtcrime.securesms.pin.PinRestoreRepository.Callback
                public final void onComplete(Object obj) {
                    PinRestoreViewModel.this.lambda$onPinSubmitted$1(this.f$1, (PinRestoreRepository.PinResultData) obj);
                }
            });
        } else {
            this.kbsRepository.getToken(new Consumer(str, pinKeyboardType) { // from class: org.thoughtcrime.securesms.pin.PinRestoreViewModel$$ExternalSyntheticLambda1
                public final /* synthetic */ String f$1;
                public final /* synthetic */ PinKeyboardType f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // j$.util.function.Consumer
                public final void accept(Object obj) {
                    PinRestoreViewModel.this.lambda$onPinSubmitted$2(this.f$1, this.f$2, (Optional) obj);
                }

                @Override // j$.util.function.Consumer
                public /* synthetic */ Consumer andThen(Consumer consumer) {
                    return Consumer.CC.$default$andThen(this, consumer);
                }
            });
        }
    }

    /* renamed from: org.thoughtcrime.securesms.pin.PinRestoreViewModel$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$pin$PinRestoreRepository$PinResult;

        static {
            int[] iArr = new int[PinRestoreRepository.PinResult.values().length];
            $SwitchMap$org$thoughtcrime$securesms$pin$PinRestoreRepository$PinResult = iArr;
            try {
                iArr[PinRestoreRepository.PinResult.SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$pin$PinRestoreRepository$PinResult[PinRestoreRepository.PinResult.LOCKED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$pin$PinRestoreRepository$PinResult[PinRestoreRepository.PinResult.INCORRECT.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$pin$PinRestoreRepository$PinResult[PinRestoreRepository.PinResult.NETWORK_ERROR.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    public /* synthetic */ void lambda$onPinSubmitted$1(PinKeyboardType pinKeyboardType, PinRestoreRepository.PinResultData pinResultData) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$pin$PinRestoreRepository$PinResult[pinResultData.getResult().ordinal()];
        if (i == 1) {
            SignalStore.pinValues().setKeyboardType(pinKeyboardType);
            SignalStore.storageService().setNeedsAccountRestore(false);
            this.event.postValue(Event.SUCCESS);
        } else if (i == 2) {
            this.event.postValue(Event.PIN_LOCKED);
        } else if (i == 3) {
            this.event.postValue(Event.PIN_INCORRECT);
            updateTokenData(pinResultData.getTokenData(), true);
        } else if (i == 4) {
            this.event.postValue(Event.NETWORK_ERROR);
        }
    }

    public /* synthetic */ void lambda$onPinSubmitted$2(String str, PinKeyboardType pinKeyboardType, Optional optional) {
        if (optional.isPresent()) {
            updateTokenData((TokenData) optional.get(), false);
            onPinSubmitted(str, pinKeyboardType);
            return;
        }
        this.event.postValue(Event.NETWORK_ERROR);
    }

    public DefaultValueLiveData<TriesRemaining> getTriesRemaining() {
        return this.triesRemaining;
    }

    public LiveData<Event> getEvent() {
        return this.event;
    }

    private void updateTokenData(TokenData tokenData, boolean z) {
        this.tokenData = tokenData;
        this.triesRemaining.postValue(new TriesRemaining(tokenData.getTriesRemaining(), z));
    }

    /* loaded from: classes4.dex */
    public static class TriesRemaining {
        private final boolean hasIncorrectGuess;
        private final int triesRemaining;

        TriesRemaining(int i, boolean z) {
            this.triesRemaining = i;
            this.hasIncorrectGuess = z;
        }

        public int getCount() {
            return this.triesRemaining;
        }

        public boolean hasIncorrectGuess() {
            return this.hasIncorrectGuess;
        }
    }
}
