package org.thoughtcrime.securesms.pin;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import j$.util.Optional;
import j$.util.function.Consumer;
import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.Callable;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.thoughtcrime.securesms.KbsEnclave;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.lock.PinHashing;
import org.whispersystems.signalservice.api.KbsPinData;
import org.whispersystems.signalservice.api.KeyBackupService;
import org.whispersystems.signalservice.api.KeyBackupServicePinException;
import org.whispersystems.signalservice.api.KeyBackupSystemNoDataException;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.contacts.crypto.UnauthenticatedResponseException;
import org.whispersystems.signalservice.internal.contacts.entities.TokenResponse;

/* loaded from: classes4.dex */
public final class KbsRepository {
    private static final String TAG = Log.tag(KbsRepository.class);

    public void getToken(Consumer<Optional<TokenData>> consumer) {
        SignalExecutors.UNBOUNDED.execute(new Runnable(consumer) { // from class: org.thoughtcrime.securesms.pin.KbsRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ Consumer f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                KbsRepository.this.lambda$getToken$0(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$getToken$0(Consumer consumer) {
        try {
            consumer.accept(Optional.ofNullable(getTokenSync(null)));
        } catch (IOException unused) {
            consumer.accept(Optional.empty());
        }
    }

    public Single<ServiceResponse<TokenData>> getToken(String str) {
        return Single.fromCallable(new Callable(str) { // from class: org.thoughtcrime.securesms.pin.KbsRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return KbsRepository.this.lambda$getToken$1(this.f$1);
            }
        }).subscribeOn(Schedulers.io());
    }

    public /* synthetic */ ServiceResponse lambda$getToken$1(String str) throws Exception {
        try {
            return ServiceResponse.forResult(getTokenSync(str), 200, null);
        } catch (IOException e) {
            return ServiceResponse.forUnknownError(e);
        }
    }

    private TokenData getTokenSync(String str) throws IOException {
        TokenData tokenData = null;
        for (KbsEnclave kbsEnclave : KbsEnclaves.all()) {
            KeyBackupService keyBackupService = ApplicationDependencies.getKeyBackupService(kbsEnclave);
            if (str == null) {
                str = keyBackupService.getAuthorization();
            }
            TokenData tokenData2 = new TokenData(kbsEnclave, str, keyBackupService.getToken(str));
            if (tokenData2.getTriesRemaining() > 0) {
                String str2 = TAG;
                Log.i(str2, "Found data! " + kbsEnclave.getEnclaveName());
                return tokenData2;
            } else if (tokenData == null) {
                String str3 = TAG;
                Log.i(str3, "No data, but storing as the first response. " + kbsEnclave.getEnclaveName());
                tokenData = tokenData2;
            } else {
                String str4 = TAG;
                Log.i(str4, "No data, and we already have a 'first response'. " + kbsEnclave.getEnclaveName());
            }
        }
        Objects.requireNonNull(tokenData);
        return tokenData;
    }

    public static synchronized KbsPinData restoreMasterKey(String str, KbsEnclave kbsEnclave, String str2, TokenResponse tokenResponse) throws IOException, KeyBackupSystemWrongPinException, KeyBackupSystemNoDataException {
        synchronized (KbsRepository.class) {
            String str3 = TAG;
            Log.i(str3, "restoreMasterKey()");
            if (str == null) {
                return null;
            }
            if (str2 != null) {
                Log.i(str3, "Preparing to restore from " + kbsEnclave.getEnclaveName());
                return restoreMasterKeyFromEnclave(kbsEnclave, str, str2, tokenResponse);
            }
            throw new AssertionError("Cannot restore KBS key, no storage credentials supplied. Enclave: " + kbsEnclave.getEnclaveName());
        }
    }

    private static KbsPinData restoreMasterKeyFromEnclave(KbsEnclave kbsEnclave, String str, String str2, TokenResponse tokenResponse) throws IOException, KeyBackupSystemWrongPinException, KeyBackupSystemNoDataException {
        Throwable e;
        KeyBackupService.RestoreSession newRegistrationSession = ApplicationDependencies.getKeyBackupService(kbsEnclave).newRegistrationSession(str2, tokenResponse);
        try {
            String str3 = TAG;
            Log.i(str3, "Restoring pin from KBS");
            KbsPinData restorePin = newRegistrationSession.restorePin(PinHashing.hashPin(str, newRegistrationSession));
            if (restorePin != null) {
                Log.i(str3, "Found registration lock token on KBS.");
                return restorePin;
            }
            throw new AssertionError("Null not expected");
        } catch (InvalidKeyException e2) {
            e = e2;
            Log.w(TAG, "Failed to restore key", e);
            throw new IOException(e);
        } catch (KeyBackupServicePinException e3) {
            Log.w(TAG, "Incorrect pin", e3);
            throw new KeyBackupSystemWrongPinException(e3.getToken());
        } catch (UnauthenticatedResponseException e4) {
            e = e4;
            Log.w(TAG, "Failed to restore key", e);
            throw new IOException(e);
        }
    }
}
