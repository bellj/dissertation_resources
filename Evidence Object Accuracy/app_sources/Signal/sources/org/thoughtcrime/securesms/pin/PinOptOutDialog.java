package org.thoughtcrime.securesms.pin;

import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;

/* loaded from: classes4.dex */
public final class PinOptOutDialog {
    private static final String TAG = Log.tag(PinOptOutDialog.class);

    public static void show(Context context, Runnable runnable) {
        Log.i(TAG, "show()");
        AlertDialog create = new AlertDialog.Builder(context).setTitle(R.string.PinOptOutDialog_warning).setMessage(R.string.PinOptOutDialog_if_you_disable_the_pin_you_will_lose_all_data).setCancelable(true).setPositiveButton(R.string.PinOptOutDialog_disable_pin, new DialogInterface.OnClickListener(context, runnable) { // from class: org.thoughtcrime.securesms.pin.PinOptOutDialog$$ExternalSyntheticLambda2
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ Runnable f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                PinOptOutDialog.lambda$show$2(this.f$0, this.f$1, dialogInterface, i);
            }
        }).setNegativeButton(17039360, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.pin.PinOptOutDialog$$ExternalSyntheticLambda3
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                PinOptOutDialog.lambda$show$3(dialogInterface, i);
            }
        }).create();
        create.setOnShowListener(new DialogInterface.OnShowListener(context) { // from class: org.thoughtcrime.securesms.pin.PinOptOutDialog$$ExternalSyntheticLambda4
            public final /* synthetic */ Context f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnShowListener
            public final void onShow(DialogInterface dialogInterface) {
                PinOptOutDialog.lambda$show$4(AlertDialog.this, this.f$1, dialogInterface);
            }
        });
        create.show();
    }

    public static /* synthetic */ void lambda$show$2(Context context, Runnable runnable, DialogInterface dialogInterface, int i) {
        Log.i(TAG, "Disable clicked.");
        dialogInterface.dismiss();
        SimpleTask.run(new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.pin.PinOptOutDialog$$ExternalSyntheticLambda0
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return PinState.onPinOptOut();
            }
        }, new SimpleTask.ForegroundTask(runnable, SimpleProgressDialog.show(context)) { // from class: org.thoughtcrime.securesms.pin.PinOptOutDialog$$ExternalSyntheticLambda1
            public final /* synthetic */ Runnable f$0;
            public final /* synthetic */ AlertDialog f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                PinOptOutDialog.lambda$show$1(this.f$0, this.f$1, obj);
            }
        });
    }

    public static /* synthetic */ void lambda$show$1(Runnable runnable, AlertDialog alertDialog, Object obj) {
        Log.i(TAG, "Disable operation finished.");
        runnable.run();
        alertDialog.dismiss();
    }

    public static /* synthetic */ void lambda$show$3(DialogInterface dialogInterface, int i) {
        Log.i(TAG, "Cancel clicked.");
        dialogInterface.dismiss();
    }

    public static /* synthetic */ void lambda$show$4(AlertDialog alertDialog, Context context, DialogInterface dialogInterface) {
        alertDialog.getButton(-1).setTextColor(ContextCompat.getColor(context, R.color.signal_alert_primary));
    }
}
