package org.thoughtcrime.securesms.pin;

import android.os.Parcel;
import android.os.Parcelable;
import org.thoughtcrime.securesms.KbsEnclave;
import org.whispersystems.signalservice.internal.contacts.entities.TokenResponse;

/* loaded from: classes4.dex */
public class TokenData implements Parcelable {
    public static final Parcelable.Creator<TokenData> CREATOR = new Parcelable.Creator<TokenData>() { // from class: org.thoughtcrime.securesms.pin.TokenData.1
        @Override // android.os.Parcelable.Creator
        public TokenData createFromParcel(Parcel parcel) {
            return new TokenData(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public TokenData[] newArray(int i) {
            return new TokenData[i];
        }
    };
    private final String basicAuth;
    private final KbsEnclave enclave;
    private final TokenResponse tokenResponse;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public TokenData(KbsEnclave kbsEnclave, String str, TokenResponse tokenResponse) {
        this.enclave = kbsEnclave;
        this.basicAuth = str;
        this.tokenResponse = tokenResponse;
    }

    private TokenData(Parcel parcel) {
        this.enclave = new KbsEnclave(parcel.readString(), parcel.readString(), parcel.readString());
        this.basicAuth = parcel.readString();
        this.tokenResponse = new TokenResponse(parcel.createByteArray(), parcel.createByteArray(), parcel.readInt());
    }

    public static TokenData withResponse(TokenData tokenData, TokenResponse tokenResponse) {
        return new TokenData(tokenData.getEnclave(), tokenData.getBasicAuth(), tokenResponse);
    }

    public int getTriesRemaining() {
        return this.tokenResponse.getTries();
    }

    public String getBasicAuth() {
        return this.basicAuth;
    }

    public TokenResponse getTokenResponse() {
        return this.tokenResponse;
    }

    public KbsEnclave getEnclave() {
        return this.enclave;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.enclave.getEnclaveName());
        parcel.writeString(this.enclave.getServiceId());
        parcel.writeString(this.enclave.getMrEnclave());
        parcel.writeString(this.basicAuth);
        parcel.writeByteArray(this.tokenResponse.getBackupId());
        parcel.writeByteArray(this.tokenResponse.getToken());
        parcel.writeInt(this.tokenResponse.getTries());
    }
}
