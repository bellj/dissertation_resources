package org.thoughtcrime.securesms.pin;

import org.whispersystems.signalservice.internal.contacts.entities.TokenResponse;

/* loaded from: classes4.dex */
public final class KeyBackupSystemWrongPinException extends Exception {
    private final TokenResponse tokenResponse;

    public KeyBackupSystemWrongPinException(TokenResponse tokenResponse) {
        this.tokenResponse = tokenResponse;
    }

    public TokenResponse getTokenResponse() {
        return this.tokenResponse;
    }
}
