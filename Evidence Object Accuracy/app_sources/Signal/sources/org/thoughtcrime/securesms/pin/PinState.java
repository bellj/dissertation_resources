package org.thoughtcrime.securesms.pin;

import android.app.Application;
import android.content.Context;
import j$.util.Optional;
import java.io.IOException;
import java.util.Arrays;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.thoughtcrime.securesms.KbsEnclave;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.JobTracker;
import org.thoughtcrime.securesms.jobs.ClearFallbackKbsEnclaveJob;
import org.thoughtcrime.securesms.jobs.RefreshAttributesJob;
import org.thoughtcrime.securesms.jobs.StorageForcePushJob;
import org.thoughtcrime.securesms.keyvalue.KbsValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.lock.PinHashing;
import org.thoughtcrime.securesms.lock.RegistrationLockReminders;
import org.thoughtcrime.securesms.megaphone.Megaphones;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.KbsPinData;
import org.whispersystems.signalservice.api.KeyBackupService;
import org.whispersystems.signalservice.api.kbs.MasterKey;
import org.whispersystems.signalservice.internal.contacts.crypto.UnauthenticatedResponseException;

/* loaded from: classes4.dex */
public final class PinState {
    private static final String TAG = Log.tag(PinState.class);

    public static synchronized void onRegistration(Context context, KbsPinData kbsPinData, String str, boolean z) {
        synchronized (PinState.class) {
            String str2 = TAG;
            Log.i(str2, "onRegistration()");
            TextSecurePreferences.setV1RegistrationLockPin(context, str);
            if (kbsPinData == null && str != null) {
                Log.i(str2, "Registration Lock V1");
                SignalStore.kbsValues().clearRegistrationLockAndPin();
                TextSecurePreferences.setV1RegistrationLockEnabled(context, true);
                TextSecurePreferences.setRegistrationLockLastReminderTime(context, System.currentTimeMillis());
                TextSecurePreferences.setRegistrationLockNextReminderInterval(context, RegistrationLockReminders.INITIAL_INTERVAL);
            } else if (kbsPinData != null && str != null) {
                Log.i(str2, "Registration Lock V2");
                TextSecurePreferences.setV1RegistrationLockEnabled(context, false);
                SignalStore.kbsValues().setV2RegistrationLockEnabled(true);
                SignalStore.kbsValues().setKbsMasterKey(kbsPinData, str);
                SignalStore.pinValues().resetPinReminders();
                resetPinRetryCount(context, str);
                ClearFallbackKbsEnclaveJob.clearAll();
            } else if (z) {
                Log.i(str2, "Has a PIN to restore.");
                SignalStore.kbsValues().clearRegistrationLockAndPin();
                TextSecurePreferences.setV1RegistrationLockEnabled(context, false);
                SignalStore.storageService().setNeedsAccountRestore(true);
            } else {
                Log.i(str2, "No registration lock or PIN at all.");
                SignalStore.kbsValues().clearRegistrationLockAndPin();
                TextSecurePreferences.setV1RegistrationLockEnabled(context, false);
            }
            updateState(buildInferredStateFromOtherFields());
        }
    }

    public static synchronized void onSignalPinRestore(Context context, KbsPinData kbsPinData, String str) {
        synchronized (PinState.class) {
            Log.i(TAG, "onSignalPinRestore()");
            SignalStore.kbsValues().setKbsMasterKey(kbsPinData, str);
            SignalStore.kbsValues().setV2RegistrationLockEnabled(false);
            SignalStore.pinValues().resetPinReminders();
            SignalStore.kbsValues().setPinForgottenOrSkipped(false);
            SignalStore.storageService().setNeedsAccountRestore(false);
            resetPinRetryCount(context, str);
            ClearFallbackKbsEnclaveJob.clearAll();
            updateState(buildInferredStateFromOtherFields());
        }
    }

    public static synchronized void onPinRestoreForgottenOrSkipped() {
        synchronized (PinState.class) {
            SignalStore.kbsValues().clearRegistrationLockAndPin();
            SignalStore.storageService().setNeedsAccountRestore(false);
            SignalStore.kbsValues().setPinForgottenOrSkipped(true);
            updateState(buildInferredStateFromOtherFields());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0059 A[Catch: all -> 0x0096, TryCatch #0 {, blocks: (B:4:0x0003, B:6:0x0019, B:11:0x0023, B:13:0x0059, B:14:0x0075, B:15:0x008d), top: B:20:0x0003 }] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0075 A[Catch: all -> 0x0096, TryCatch #0 {, blocks: (B:4:0x0003, B:6:0x0019, B:11:0x0023, B:13:0x0059, B:14:0x0075, B:15:0x008d), top: B:20:0x0003 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void onPinChangedOrCreated(android.content.Context r9, java.lang.String r10, org.thoughtcrime.securesms.lock.v2.PinKeyboardType r11) throws java.io.IOException, org.whispersystems.signalservice.internal.contacts.crypto.UnauthenticatedResponseException, org.signal.libsignal.protocol.InvalidKeyException {
        /*
            java.lang.Class<org.thoughtcrime.securesms.pin.PinState> r0 = org.thoughtcrime.securesms.pin.PinState.class
            monitor-enter(r0)
            java.lang.String r1 = org.thoughtcrime.securesms.pin.PinState.TAG     // Catch: all -> 0x0096
            java.lang.String r2 = "onPinChangedOrCreated()"
            org.signal.core.util.logging.Log.i(r1, r2)     // Catch: all -> 0x0096
            org.thoughtcrime.securesms.KbsEnclave r2 = org.thoughtcrime.securesms.pin.KbsEnclaves.current()     // Catch: all -> 0x0096
            org.thoughtcrime.securesms.keyvalue.KbsValues r3 = org.thoughtcrime.securesms.keyvalue.SignalStore.kbsValues()     // Catch: all -> 0x0096
            boolean r4 = r3.hasPin()     // Catch: all -> 0x0096
            r5 = 0
            if (r4 == 0) goto L_0x0022
            boolean r4 = r3.hasOptedOut()     // Catch: all -> 0x0096
            if (r4 == 0) goto L_0x0020
            goto L_0x0022
        L_0x0020:
            r4 = 0
            goto L_0x0023
        L_0x0022:
            r4 = 1
        L_0x0023:
            org.whispersystems.signalservice.api.kbs.MasterKey r6 = r3.getOrCreateMasterKey()     // Catch: all -> 0x0096
            org.whispersystems.signalservice.api.KeyBackupService r7 = org.thoughtcrime.securesms.dependencies.ApplicationDependencies.getKeyBackupService(r2)     // Catch: all -> 0x0096
            org.whispersystems.signalservice.api.KeyBackupService$PinChangeSession r7 = r7.newPinChangeSession()     // Catch: all -> 0x0096
            org.whispersystems.signalservice.api.kbs.HashedPin r8 = org.thoughtcrime.securesms.lock.PinHashing.hashPin(r10, r7)     // Catch: all -> 0x0096
            org.whispersystems.signalservice.api.KbsPinData r6 = r7.setPin(r8, r6)     // Catch: all -> 0x0096
            r3.setKbsMasterKey(r6, r10)     // Catch: all -> 0x0096
            r3.setPinForgottenOrSkipped(r5)     // Catch: all -> 0x0096
            org.thoughtcrime.securesms.util.TextSecurePreferences.clearRegistrationLockV1(r9)     // Catch: all -> 0x0096
            org.thoughtcrime.securesms.keyvalue.PinValues r9 = org.thoughtcrime.securesms.keyvalue.SignalStore.pinValues()     // Catch: all -> 0x0096
            r9.setKeyboardType(r11)     // Catch: all -> 0x0096
            org.thoughtcrime.securesms.keyvalue.PinValues r9 = org.thoughtcrime.securesms.keyvalue.SignalStore.pinValues()     // Catch: all -> 0x0096
            r9.resetPinReminders()     // Catch: all -> 0x0096
            org.thoughtcrime.securesms.megaphone.MegaphoneRepository r9 = org.thoughtcrime.securesms.dependencies.ApplicationDependencies.getMegaphoneRepository()     // Catch: all -> 0x0096
            org.thoughtcrime.securesms.megaphone.Megaphones$Event r10 = org.thoughtcrime.securesms.megaphone.Megaphones.Event.PINS_FOR_ALL     // Catch: all -> 0x0096
            r9.markFinished(r10)     // Catch: all -> 0x0096
            if (r4 == 0) goto L_0x0075
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch: all -> 0x0096
            r9.<init>()     // Catch: all -> 0x0096
            java.lang.String r10 = "First time setting a PIN. Refreshing attributes to set the 'storage' capability. Enclave: "
            r9.append(r10)     // Catch: all -> 0x0096
            java.lang.String r10 = r2.getEnclaveName()     // Catch: all -> 0x0096
            r9.append(r10)     // Catch: all -> 0x0096
            java.lang.String r9 = r9.toString()     // Catch: all -> 0x0096
            org.signal.core.util.logging.Log.i(r1, r9)     // Catch: all -> 0x0096
            bestEffortRefreshAttributes()     // Catch: all -> 0x0096
            goto L_0x008d
        L_0x0075:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch: all -> 0x0096
            r9.<init>()     // Catch: all -> 0x0096
            java.lang.String r10 = "Not the first time setting a PIN. Enclave: "
            r9.append(r10)     // Catch: all -> 0x0096
            java.lang.String r10 = r2.getEnclaveName()     // Catch: all -> 0x0096
            r9.append(r10)     // Catch: all -> 0x0096
            java.lang.String r9 = r9.toString()     // Catch: all -> 0x0096
            org.signal.core.util.logging.Log.i(r1, r9)     // Catch: all -> 0x0096
        L_0x008d:
            org.thoughtcrime.securesms.pin.PinState$State r9 = buildInferredStateFromOtherFields()     // Catch: all -> 0x0096
            updateState(r9)     // Catch: all -> 0x0096
            monitor-exit(r0)
            return
        L_0x0096:
            r9 = move-exception
            monitor-exit(r0)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.pin.PinState.onPinChangedOrCreated(android.content.Context, java.lang.String, org.thoughtcrime.securesms.lock.v2.PinKeyboardType):void");
    }

    public static synchronized void onPinCreateFailure() {
        synchronized (PinState.class) {
            Log.i(TAG, "onPinCreateFailure()");
            if (getState() == State.NO_REGISTRATION_LOCK) {
                SignalStore.kbsValues().onPinCreateFailure();
            }
        }
    }

    public static synchronized void onPinOptOut() {
        synchronized (PinState.class) {
            Log.i(TAG, "onPinOptOutEnabled()");
            assertState(State.PIN_WITH_REGISTRATION_LOCK_DISABLED, State.NO_REGISTRATION_LOCK);
            optOutOfPin();
            updateState(buildInferredStateFromOtherFields());
        }
    }

    public static synchronized void onEnableRegistrationLockForUserWithPin() throws IOException {
        synchronized (PinState.class) {
            String str = TAG;
            Log.i(str, "onEnableRegistrationLockForUserWithPin()");
            State state = getState();
            State state2 = State.PIN_WITH_REGISTRATION_LOCK_ENABLED;
            if (state == state2) {
                Log.i(str, "Registration lock already enabled. Skipping.");
                return;
            }
            assertState(State.PIN_WITH_REGISTRATION_LOCK_DISABLED);
            KbsEnclave current = KbsEnclaves.current();
            Log.i(str, "Enclave: " + current.getEnclaveName());
            SignalStore.kbsValues().setV2RegistrationLockEnabled(false);
            ApplicationDependencies.getKeyBackupService(current).newPinChangeSession(SignalStore.kbsValues().getRegistrationLockTokenResponse()).enableRegistrationLock(SignalStore.kbsValues().getOrCreateMasterKey());
            SignalStore.kbsValues().setV2RegistrationLockEnabled(true);
            updateState(state2);
        }
    }

    public static synchronized void onDisableRegistrationLockForUserWithPin() throws IOException {
        synchronized (PinState.class) {
            String str = TAG;
            Log.i(str, "onDisableRegistrationLockForUserWithPin()");
            State state = getState();
            State state2 = State.PIN_WITH_REGISTRATION_LOCK_DISABLED;
            if (state == state2) {
                Log.i(str, "Registration lock already disabled. Skipping.");
                return;
            }
            assertState(State.PIN_WITH_REGISTRATION_LOCK_ENABLED);
            SignalStore.kbsValues().setV2RegistrationLockEnabled(true);
            ApplicationDependencies.getKeyBackupService(KbsEnclaves.current()).newPinChangeSession(SignalStore.kbsValues().getRegistrationLockTokenResponse()).disableRegistrationLock();
            SignalStore.kbsValues().setV2RegistrationLockEnabled(false);
            updateState(state2);
        }
    }

    public static synchronized void onMigrateToRegistrationLockV2(Context context, String str) throws IOException, UnauthenticatedResponseException, InvalidKeyException {
        synchronized (PinState.class) {
            String str2 = TAG;
            Log.i(str2, "onMigrateToRegistrationLockV2()");
            KbsEnclave current = KbsEnclaves.current();
            Log.i(str2, "Enclave: " + current.getEnclaveName());
            KbsValues kbsValues = SignalStore.kbsValues();
            MasterKey orCreateMasterKey = kbsValues.getOrCreateMasterKey();
            KeyBackupService.PinChangeSession newPinChangeSession = ApplicationDependencies.getKeyBackupService(current).newPinChangeSession();
            KbsPinData pin = newPinChangeSession.setPin(PinHashing.hashPin(str, newPinChangeSession), orCreateMasterKey);
            newPinChangeSession.enableRegistrationLock(orCreateMasterKey);
            kbsValues.setKbsMasterKey(pin, str);
            TextSecurePreferences.clearRegistrationLockV1(context);
            updateState(buildInferredStateFromOtherFields());
        }
    }

    public static synchronized void onMigrateToNewEnclave(String str) throws IOException, UnauthenticatedResponseException {
        synchronized (PinState.class) {
            String str2 = TAG;
            Log.i(str2, "onMigrateToNewEnclave()");
            assertState(State.PIN_WITH_REGISTRATION_LOCK_DISABLED, State.PIN_WITH_REGISTRATION_LOCK_ENABLED);
            Log.i(str2, "Migrating to enclave " + KbsEnclaves.current().getEnclaveName());
            setPinOnEnclave(KbsEnclaves.current(), str, SignalStore.kbsValues().getOrCreateMasterKey());
            ClearFallbackKbsEnclaveJob.clearAll();
        }
    }

    private static void bestEffortRefreshAttributes() {
        Optional<JobTracker.JobState> runSynchronously = ApplicationDependencies.getJobManager().runSynchronously(new RefreshAttributesJob(), TimeUnit.SECONDS.toMillis(10));
        if (runSynchronously.isPresent() && runSynchronously.get() == JobTracker.JobState.SUCCESS) {
            Log.i(TAG, "Attributes were refreshed successfully.");
        } else if (runSynchronously.isPresent()) {
            String str = TAG;
            Log.w(str, "Attribute refresh finished, but was not successful. Enqueuing one for later. (Result: " + runSynchronously.get() + ")");
            ApplicationDependencies.getJobManager().add(new RefreshAttributesJob());
        } else {
            Log.w(TAG, "Job did not finish in the allotted time. It'll finish later.");
        }
    }

    private static void bestEffortForcePushStorage() {
        Optional<JobTracker.JobState> runSynchronously = ApplicationDependencies.getJobManager().runSynchronously(new StorageForcePushJob(), TimeUnit.SECONDS.toMillis(10));
        if (runSynchronously.isPresent() && runSynchronously.get() == JobTracker.JobState.SUCCESS) {
            Log.i(TAG, "Storage was force-pushed successfully.");
        } else if (runSynchronously.isPresent()) {
            String str = TAG;
            Log.w(str, "Storage force-pushed finished, but was not successful. Enqueuing one for later. (Result: " + runSynchronously.get() + ")");
            ApplicationDependencies.getJobManager().add(new RefreshAttributesJob());
        } else {
            Log.w(TAG, "Storage fore push did not finish in the allotted time. It'll finish later.");
        }
    }

    private static void resetPinRetryCount(Context context, String str) {
        if (str != null) {
            try {
                setPinOnEnclave(KbsEnclaves.current(), str, SignalStore.kbsValues().getOrCreateMasterKey());
                TextSecurePreferences.clearRegistrationLockV1(context);
                Log.i(TAG, "Pin set/attempts reset on KBS");
            } catch (IOException e) {
                Log.w(TAG, "May have failed to reset pin attempts!", e);
            } catch (UnauthenticatedResponseException e2) {
                Log.w(TAG, "Failed to reset pin attempts", e2);
            }
        }
    }

    private static KbsPinData setPinOnEnclave(KbsEnclave kbsEnclave, String str, MasterKey masterKey) throws IOException, UnauthenticatedResponseException {
        String str2 = TAG;
        Log.i(str2, "Setting PIN on enclave: " + kbsEnclave.getEnclaveName());
        KeyBackupService.PinChangeSession newPinChangeSession = ApplicationDependencies.getKeyBackupService(kbsEnclave).newPinChangeSession();
        KbsPinData pin = newPinChangeSession.setPin(PinHashing.hashPin(str, newPinChangeSession), masterKey);
        SignalStore.kbsValues().setKbsMasterKey(pin, str);
        return pin;
    }

    private static void optOutOfPin() {
        SignalStore.kbsValues().optOut();
        ApplicationDependencies.getMegaphoneRepository().markFinished(Megaphones.Event.PINS_FOR_ALL);
        bestEffortRefreshAttributes();
        bestEffortForcePushStorage();
    }

    private static State assertState(State... stateArr) {
        State state = getState();
        for (State state2 : stateArr) {
            if (state == state2) {
                return state;
            }
        }
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$pin$PinState$State[state.ordinal()];
        if (i == 1) {
            throw new InvalidState_NoRegistrationLock(null);
        } else if (i == 2) {
            throw new InvalidState_RegistrationLockV1(null);
        } else if (i == 3) {
            throw new InvalidState_PinWithRegistrationLockEnabled(null);
        } else if (i == 4) {
            throw new InvalidState_PinWithRegistrationLockDisabled(null);
        } else if (i != 5) {
            throw new IllegalStateException("Expected: " + Arrays.toString(stateArr) + ", Actual: " + state);
        } else {
            throw new InvalidState_PinOptOut(null);
        }
    }

    /* renamed from: org.thoughtcrime.securesms.pin.PinState$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$pin$PinState$State;

        static {
            int[] iArr = new int[State.values().length];
            $SwitchMap$org$thoughtcrime$securesms$pin$PinState$State = iArr;
            try {
                iArr[State.NO_REGISTRATION_LOCK.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$pin$PinState$State[State.REGISTRATION_LOCK_V1.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$pin$PinState$State[State.PIN_WITH_REGISTRATION_LOCK_ENABLED.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$pin$PinState$State[State.PIN_WITH_REGISTRATION_LOCK_DISABLED.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$pin$PinState$State[State.PIN_OPT_OUT.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
        }
    }

    private static State getState() {
        String pinState = SignalStore.pinValues().getPinState();
        if (pinState != null) {
            return State.deserialize(pinState);
        }
        State buildInferredStateFromOtherFields = buildInferredStateFromOtherFields();
        SignalStore.pinValues().setPinState(buildInferredStateFromOtherFields.serialize());
        return buildInferredStateFromOtherFields;
    }

    private static void updateState(State state) {
        String str = TAG;
        Log.i(str, "Updating state to: " + state);
        SignalStore.pinValues().setPinState(state.serialize());
    }

    private static State buildInferredStateFromOtherFields() {
        Application application = ApplicationDependencies.getApplication();
        KbsValues kbsValues = SignalStore.kbsValues();
        boolean isV1RegistrationLockEnabled = TextSecurePreferences.isV1RegistrationLockEnabled(application);
        boolean isV2RegistrationLockEnabled = kbsValues.isV2RegistrationLockEnabled();
        boolean hasPin = kbsValues.hasPin();
        if (kbsValues.hasOptedOut() && !isV2RegistrationLockEnabled && !isV1RegistrationLockEnabled) {
            return State.PIN_OPT_OUT;
        }
        if (!isV1RegistrationLockEnabled && !isV2RegistrationLockEnabled && !hasPin) {
            return State.NO_REGISTRATION_LOCK;
        }
        if (isV1RegistrationLockEnabled && !isV2RegistrationLockEnabled && !hasPin) {
            return State.REGISTRATION_LOCK_V1;
        }
        if (isV2RegistrationLockEnabled && hasPin) {
            TextSecurePreferences.setV1RegistrationLockEnabled(application, false);
            return State.PIN_WITH_REGISTRATION_LOCK_ENABLED;
        } else if (isV2RegistrationLockEnabled || !hasPin) {
            throw new InvalidInferredStateError(String.format(Locale.ENGLISH, "Invalid state! v1: %b, v2: %b, pin: %b", Boolean.valueOf(isV1RegistrationLockEnabled), Boolean.valueOf(isV2RegistrationLockEnabled), Boolean.valueOf(hasPin)));
        } else {
            TextSecurePreferences.setV1RegistrationLockEnabled(application, false);
            return State.PIN_WITH_REGISTRATION_LOCK_DISABLED;
        }
    }

    /* loaded from: classes4.dex */
    public enum State {
        NO_REGISTRATION_LOCK("no_registration_lock"),
        REGISTRATION_LOCK_V1("registration_lock_v1"),
        PIN_WITH_REGISTRATION_LOCK_ENABLED("pin_with_registration_lock_enabled"),
        PIN_WITH_REGISTRATION_LOCK_DISABLED("pin_with_registration_lock_disabled"),
        PIN_OPT_OUT("pin_opt_out");
        
        private final String key;

        State(String str) {
            this.key = str;
        }

        public String serialize() {
            return this.key;
        }

        public static State deserialize(String str) {
            State[] values = values();
            for (State state : values) {
                if (state.key.equals(str)) {
                    return state;
                }
            }
            throw new IllegalArgumentException("No state for value: " + str);
        }
    }

    /* loaded from: classes4.dex */
    public static class InvalidInferredStateError extends Error {
        InvalidInferredStateError(String str) {
            super(str);
        }
    }

    /* loaded from: classes4.dex */
    public static class InvalidState_NoRegistrationLock extends IllegalStateException {
        private InvalidState_NoRegistrationLock() {
        }

        /* synthetic */ InvalidState_NoRegistrationLock(AnonymousClass1 r1) {
            this();
        }
    }

    /* loaded from: classes4.dex */
    public static class InvalidState_RegistrationLockV1 extends IllegalStateException {
        private InvalidState_RegistrationLockV1() {
        }

        /* synthetic */ InvalidState_RegistrationLockV1(AnonymousClass1 r1) {
            this();
        }
    }

    /* loaded from: classes4.dex */
    public static class InvalidState_PinWithRegistrationLockEnabled extends IllegalStateException {
        private InvalidState_PinWithRegistrationLockEnabled() {
        }

        /* synthetic */ InvalidState_PinWithRegistrationLockEnabled(AnonymousClass1 r1) {
            this();
        }
    }

    /* loaded from: classes4.dex */
    public static class InvalidState_PinWithRegistrationLockDisabled extends IllegalStateException {
        private InvalidState_PinWithRegistrationLockDisabled() {
        }

        /* synthetic */ InvalidState_PinWithRegistrationLockDisabled(AnonymousClass1 r1) {
            this();
        }
    }

    /* loaded from: classes4.dex */
    public static class InvalidState_PinOptOut extends IllegalStateException {
        private InvalidState_PinOptOut() {
        }

        /* synthetic */ InvalidState_PinOptOut(AnonymousClass1 r1) {
            this();
        }
    }
}
