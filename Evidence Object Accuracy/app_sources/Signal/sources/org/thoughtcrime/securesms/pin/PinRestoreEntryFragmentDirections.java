package org.thoughtcrime.securesms.pin;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class PinRestoreEntryFragmentDirections {
    private PinRestoreEntryFragmentDirections() {
    }

    public static NavDirections actionAccountLocked() {
        return new ActionOnlyNavDirections(R.id.action_accountLocked);
    }
}
