package org.thoughtcrime.securesms.pin;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.CommunicationActions;

/* loaded from: classes4.dex */
public class PinRestoreLockedFragment extends LoggingFragment {
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.pin_restore_locked_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        View findViewById = view.findViewById(R.id.pin_locked_next);
        View findViewById2 = view.findViewById(R.id.pin_locked_learn_more);
        findViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.pin.PinRestoreLockedFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PinRestoreLockedFragment.$r8$lambda$RU_FvNxeM7Eno6DNtnBSqsR8C74(PinRestoreLockedFragment.this, view2);
            }
        });
        findViewById2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.pin.PinRestoreLockedFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PinRestoreLockedFragment.$r8$lambda$kKYRmfEzrdngykUBgjLlnAxTvwg(PinRestoreLockedFragment.this, view2);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        PinState.onPinRestoreForgottenOrSkipped();
        ((PinRestoreActivity) requireActivity()).navigateToPinCreation();
    }

    public /* synthetic */ void lambda$onViewCreated$1(View view) {
        CommunicationActions.openBrowserLink(requireContext(), getString(R.string.PinRestoreLockedFragment_learn_more_url));
    }
}
