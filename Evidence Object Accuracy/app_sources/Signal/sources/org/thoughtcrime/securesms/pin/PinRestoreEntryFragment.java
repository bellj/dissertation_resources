package org.thoughtcrime.securesms.pin;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.MainActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.ProfileUploadJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.lock.v2.PinKeyboardType;
import org.thoughtcrime.securesms.pin.PinRestoreViewModel;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.profiles.edit.EditProfileActivity;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.registration.RegistrationUtil;
import org.thoughtcrime.securesms.registration.fragments.RegistrationViewDelegate;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.SupportEmailUtil;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;

/* loaded from: classes4.dex */
public class PinRestoreEntryFragment extends LoggingFragment {
    private static final int MINIMUM_PIN_LENGTH;
    private static final String TAG = Log.tag(PinRestoreActivity.class);
    private TextView errorLabel;
    private View helpButton;
    private TextView keyboardToggle;
    private CircularProgressMaterialButton pinButton;
    private EditText pinEntry;
    private View skipButton;
    private PinRestoreViewModel viewModel;

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.pin_restore_entry_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        initViews(view);
        initViewModel();
    }

    private void initViews(View view) {
        RegistrationViewDelegate.setDebugLogSubmitMultiTapView(view.findViewById(R.id.pin_restore_pin_title));
        this.pinEntry = (EditText) view.findViewById(R.id.pin_restore_pin_input);
        this.pinButton = (CircularProgressMaterialButton) view.findViewById(R.id.pin_restore_pin_confirm);
        this.errorLabel = (TextView) view.findViewById(R.id.pin_restore_pin_input_label);
        this.keyboardToggle = (TextView) view.findViewById(R.id.pin_restore_keyboard_toggle);
        this.helpButton = view.findViewById(R.id.pin_restore_forgot_pin);
        this.skipButton = view.findViewById(R.id.pin_restore_skip_button);
        this.helpButton.setVisibility(8);
        this.helpButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.pin.PinRestoreEntryFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PinRestoreEntryFragment.$r8$lambda$oBa6mRI7iND6MyNhzUSWkJxpWEk(PinRestoreEntryFragment.this, view2);
            }
        });
        this.skipButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.pin.PinRestoreEntryFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PinRestoreEntryFragment.m2440$r8$lambda$HvXFoIYQP3bFDhGYdITtUKm0Q0(PinRestoreEntryFragment.this, view2);
            }
        });
        this.pinEntry.setImeOptions(6);
        this.pinEntry.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: org.thoughtcrime.securesms.pin.PinRestoreEntryFragment$$ExternalSyntheticLambda2
            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                return PinRestoreEntryFragment.m2444$r8$lambda$tN1avQeFG3fsyC7GH07UlPUKY(PinRestoreEntryFragment.this, textView, i, keyEvent);
            }
        });
        ViewCompat.setAutofillHints(this.pinEntry, "password");
        enableAndFocusPinEntry();
        this.pinButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.pin.PinRestoreEntryFragment$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PinRestoreEntryFragment.$r8$lambda$PyBMmSoURe8BZwxa2OVNkq1np5I(PinRestoreEntryFragment.this, view2);
            }
        });
        this.keyboardToggle.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.pin.PinRestoreEntryFragment$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                PinRestoreEntryFragment.m2439$r8$lambda$5WPTcu3TBVTZXhJ_sZLKiaxKUo(PinRestoreEntryFragment.this, view2);
            }
        });
        this.keyboardToggle.setText(resolveKeyboardToggleText(getPinEntryKeyboardType().getOther()));
    }

    public /* synthetic */ void lambda$initViews$0(View view) {
        onNeedHelpClicked();
    }

    public /* synthetic */ void lambda$initViews$1(View view) {
        onSkipClicked();
    }

    public /* synthetic */ boolean lambda$initViews$2(TextView textView, int i, KeyEvent keyEvent) {
        if (i != 6) {
            return false;
        }
        ViewUtil.hideKeyboard(requireContext(), textView);
        onPinSubmitted();
        return true;
    }

    public /* synthetic */ void lambda$initViews$3(View view) {
        ViewUtil.hideKeyboard(requireContext(), this.pinEntry);
        onPinSubmitted();
    }

    public /* synthetic */ void lambda$initViews$4(View view) {
        PinKeyboardType pinEntryKeyboardType = getPinEntryKeyboardType();
        updateKeyboard(pinEntryKeyboardType.getOther());
        this.keyboardToggle.setText(resolveKeyboardToggleText(pinEntryKeyboardType));
    }

    private void initViewModel() {
        PinRestoreViewModel pinRestoreViewModel = (PinRestoreViewModel) ViewModelProviders.of(this).get(PinRestoreViewModel.class);
        this.viewModel = pinRestoreViewModel;
        pinRestoreViewModel.getTriesRemaining().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.pin.PinRestoreEntryFragment$$ExternalSyntheticLambda5
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PinRestoreEntryFragment.m2442$r8$lambda$iG1wXu2Hq9eO39hVyc9QfGnAtE(PinRestoreEntryFragment.this, (PinRestoreViewModel.TriesRemaining) obj);
            }
        });
        this.viewModel.getEvent().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.pin.PinRestoreEntryFragment$$ExternalSyntheticLambda6
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PinRestoreEntryFragment.m2443$r8$lambda$rz7VkXZlW4fvlRlv34M4qYYew(PinRestoreEntryFragment.this, (PinRestoreViewModel.Event) obj);
            }
        });
    }

    public void presentTriesRemaining(PinRestoreViewModel.TriesRemaining triesRemaining) {
        if (triesRemaining.hasIncorrectGuess()) {
            if (triesRemaining.getCount() == 1) {
                new AlertDialog.Builder(requireContext()).setTitle(R.string.PinRestoreEntryFragment_incorrect_pin).setMessage(getResources().getQuantityString(R.plurals.PinRestoreEntryFragment_you_have_d_attempt_remaining, triesRemaining.getCount(), Integer.valueOf(triesRemaining.getCount()))).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).show();
            }
            this.errorLabel.setText(R.string.PinRestoreEntryFragment_incorrect_pin);
            this.helpButton.setVisibility(0);
        } else if (triesRemaining.getCount() == 1) {
            this.helpButton.setVisibility(0);
            new AlertDialog.Builder(requireContext()).setMessage(getResources().getQuantityString(R.plurals.PinRestoreEntryFragment_you_have_d_attempt_remaining, triesRemaining.getCount(), Integer.valueOf(triesRemaining.getCount()))).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).show();
        }
        if (triesRemaining.getCount() == 0) {
            Log.w(TAG, "Account locked. User out of attempts on KBS.");
            onAccountLocked();
        }
    }

    /* renamed from: org.thoughtcrime.securesms.pin.PinRestoreEntryFragment$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$pin$PinRestoreViewModel$Event;

        static {
            int[] iArr = new int[PinRestoreViewModel.Event.values().length];
            $SwitchMap$org$thoughtcrime$securesms$pin$PinRestoreViewModel$Event = iArr;
            try {
                iArr[PinRestoreViewModel.Event.SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$pin$PinRestoreViewModel$Event[PinRestoreViewModel.Event.EMPTY_PIN.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$pin$PinRestoreViewModel$Event[PinRestoreViewModel.Event.PIN_TOO_SHORT.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$pin$PinRestoreViewModel$Event[PinRestoreViewModel.Event.PIN_INCORRECT.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$pin$PinRestoreViewModel$Event[PinRestoreViewModel.Event.PIN_LOCKED.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$pin$PinRestoreViewModel$Event[PinRestoreViewModel.Event.NETWORK_ERROR.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
        }
    }

    public void presentEvent(PinRestoreViewModel.Event event) {
        switch (AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$pin$PinRestoreViewModel$Event[event.ordinal()]) {
            case 1:
                handleSuccess();
                return;
            case 2:
                Toast.makeText(requireContext(), (int) R.string.RegistrationActivity_you_must_enter_your_registration_lock_PIN, 1).show();
                this.pinButton.cancelSpinning();
                this.pinEntry.getText().clear();
                enableAndFocusPinEntry();
                return;
            case 3:
                Toast.makeText(requireContext(), getString(R.string.RegistrationActivity_your_pin_has_at_least_d_digits_or_characters, 4), 1).show();
                this.pinButton.cancelSpinning();
                this.pinEntry.getText().clear();
                enableAndFocusPinEntry();
                return;
            case 4:
                this.pinButton.cancelSpinning();
                this.pinEntry.getText().clear();
                enableAndFocusPinEntry();
                return;
            case 5:
                onAccountLocked();
                return;
            case 6:
                Toast.makeText(requireContext(), (int) R.string.RegistrationActivity_error_connecting_to_service, 1).show();
                this.pinButton.cancelSpinning();
                this.pinEntry.setEnabled(true);
                enableAndFocusPinEntry();
                return;
            default:
                return;
        }
    }

    private PinKeyboardType getPinEntryKeyboardType() {
        return (this.pinEntry.getInputType() & 15) == 2 ? PinKeyboardType.NUMERIC : PinKeyboardType.ALPHA_NUMERIC;
    }

    private void onPinSubmitted() {
        this.pinEntry.setEnabled(false);
        this.viewModel.onPinSubmitted(this.pinEntry.getText().toString(), getPinEntryKeyboardType());
        this.pinButton.setSpinning();
    }

    private void onNeedHelpClicked() {
        new AlertDialog.Builder(requireContext()).setTitle(R.string.PinRestoreEntryFragment_need_help).setMessage(getString(R.string.PinRestoreEntryFragment_your_pin_is_a_d_digit_code, 4)).setPositiveButton(R.string.PinRestoreEntryFragment_create_new_pin, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.pin.PinRestoreEntryFragment$$ExternalSyntheticLambda7
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                PinRestoreEntryFragment.$r8$lambda$L6laZTHAThJBzuFtCdaOATtpA_0(PinRestoreEntryFragment.this, dialogInterface, i);
            }
        }).setNeutralButton(R.string.PinRestoreEntryFragment_contact_support, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.pin.PinRestoreEntryFragment$$ExternalSyntheticLambda8
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                PinRestoreEntryFragment.m2441$r8$lambda$Z5pxI6ySO367v6CKvgkJ5gZzsw(PinRestoreEntryFragment.this, dialogInterface, i);
            }
        }).setNegativeButton(R.string.PinRestoreEntryFragment_cancel, (DialogInterface.OnClickListener) null).show();
    }

    public /* synthetic */ void lambda$onNeedHelpClicked$5(DialogInterface dialogInterface, int i) {
        PinState.onPinRestoreForgottenOrSkipped();
        ((PinRestoreActivity) requireActivity()).navigateToPinCreation();
    }

    public /* synthetic */ void lambda$onNeedHelpClicked$6(DialogInterface dialogInterface, int i) {
        CommunicationActions.openEmail(requireContext(), SupportEmailUtil.getSupportEmailAddress(requireContext()), getString(R.string.PinRestoreEntryFragment_signal_registration_need_help_with_pin), SupportEmailUtil.generateSupportEmailBody(requireContext(), R.string.PinRestoreEntryFragment_signal_registration_need_help_with_pin, null, null));
    }

    private void onSkipClicked() {
        new AlertDialog.Builder(requireContext()).setTitle(R.string.PinRestoreEntryFragment_skip_pin_entry).setMessage(R.string.PinRestoreEntryFragment_if_you_cant_remember_your_pin).setPositiveButton(R.string.PinRestoreEntryFragment_create_new_pin, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.pin.PinRestoreEntryFragment$$ExternalSyntheticLambda9
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                PinRestoreEntryFragment.$r8$lambda$GC5a3q4N5Ufrqdb32MB5vB3z0N8(PinRestoreEntryFragment.this, dialogInterface, i);
            }
        }).setNegativeButton(R.string.PinRestoreEntryFragment_cancel, (DialogInterface.OnClickListener) null).show();
    }

    public /* synthetic */ void lambda$onSkipClicked$7(DialogInterface dialogInterface, int i) {
        PinState.onPinRestoreForgottenOrSkipped();
        ((PinRestoreActivity) requireActivity()).navigateToPinCreation();
    }

    private void onAccountLocked() {
        PinState.onPinRestoreForgottenOrSkipped();
        SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), PinRestoreEntryFragmentDirections.actionAccountLocked());
    }

    private void handleSuccess() {
        this.pinButton.cancelSpinning();
        SignalStore.onboarding().clearAll();
        FragmentActivity requireActivity = requireActivity();
        if (Recipient.self().getProfileName().isEmpty() || !AvatarHelper.hasAvatar(requireActivity, Recipient.self().getId())) {
            Intent clearTop = MainActivity.clearTop(requireActivity);
            Intent intentForUserProfile = EditProfileActivity.getIntentForUserProfile(requireActivity);
            intentForUserProfile.putExtra("next_intent", clearTop);
            startActivity(intentForUserProfile);
        } else {
            RegistrationUtil.maybeMarkRegistrationComplete(requireContext());
            ApplicationDependencies.getJobManager().add(new ProfileUploadJob());
            startActivity(MainActivity.clearTop(requireActivity));
        }
        requireActivity.finish();
    }

    private void updateKeyboard(PinKeyboardType pinKeyboardType) {
        this.pinEntry.setInputType(pinKeyboardType == PinKeyboardType.ALPHA_NUMERIC ? 129 : 18);
        this.pinEntry.getText().clear();
    }

    private static int resolveKeyboardToggleText(PinKeyboardType pinKeyboardType) {
        return pinKeyboardType == PinKeyboardType.ALPHA_NUMERIC ? R.string.PinRestoreEntryFragment_enter_alphanumeric_pin : R.string.PinRestoreEntryFragment_enter_numeric_pin;
    }

    private void enableAndFocusPinEntry() {
        this.pinEntry.setEnabled(true);
        this.pinEntry.setFocusable(true);
        if (this.pinEntry.requestFocus()) {
            ServiceUtil.getInputMethodManager(this.pinEntry.getContext()).showSoftInput(this.pinEntry, 0);
        }
    }
}
