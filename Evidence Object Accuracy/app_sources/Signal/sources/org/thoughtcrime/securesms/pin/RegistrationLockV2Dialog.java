package org.thoughtcrime.securesms.pin;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.io.IOException;
import java.util.Objects;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public final class RegistrationLockV2Dialog {
    private static final String TAG = Log.tag(RegistrationLockV2Dialog.class);

    private RegistrationLockV2Dialog() {
    }

    public static void showEnableDialog(Context context, Runnable runnable) {
        AlertDialog create = new MaterialAlertDialogBuilder(context).setTitle(R.string.RegistrationLockV2Dialog_turn_on_registration_lock).setView(R.layout.registration_lock_v2_dialog).setMessage(R.string.RegistrationLockV2Dialog_if_you_forget_your_signal_pin_when_registering_again).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).setPositiveButton(R.string.RegistrationLockV2Dialog_turn_on, (DialogInterface.OnClickListener) null).create();
        create.setOnShowListener(new DialogInterface.OnShowListener(context, runnable) { // from class: org.thoughtcrime.securesms.pin.RegistrationLockV2Dialog$$ExternalSyntheticLambda6
            public final /* synthetic */ Context f$1;
            public final /* synthetic */ Runnable f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // android.content.DialogInterface.OnShowListener
            public final void onShow(DialogInterface dialogInterface) {
                RegistrationLockV2Dialog.lambda$showEnableDialog$3(AlertDialog.this, this.f$1, this.f$2, dialogInterface);
            }
        });
        create.show();
    }

    public static /* synthetic */ void lambda$showEnableDialog$3(AlertDialog alertDialog, Context context, Runnable runnable, DialogInterface dialogInterface) {
        ProgressBar progressBar = (ProgressBar) alertDialog.findViewById(R.id.reglockv2_dialog_progress);
        Objects.requireNonNull(progressBar);
        alertDialog.getButton(-1).setOnClickListener(new View.OnClickListener(progressBar, context, runnable, alertDialog) { // from class: org.thoughtcrime.securesms.pin.RegistrationLockV2Dialog$$ExternalSyntheticLambda7
            public final /* synthetic */ ProgressBar f$0;
            public final /* synthetic */ Context f$1;
            public final /* synthetic */ Runnable f$2;
            public final /* synthetic */ AlertDialog f$3;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                RegistrationLockV2Dialog.lambda$showEnableDialog$2(this.f$0, this.f$1, this.f$2, this.f$3, view);
            }
        });
    }

    public static /* synthetic */ void lambda$showEnableDialog$2(ProgressBar progressBar, Context context, Runnable runnable, AlertDialog alertDialog, View view) {
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(0);
        SimpleTask.run(SignalExecutors.UNBOUNDED, new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.pin.RegistrationLockV2Dialog$$ExternalSyntheticLambda0
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return RegistrationLockV2Dialog.lambda$showEnableDialog$0();
            }
        }, new SimpleTask.ForegroundTask(progressBar, context, runnable, alertDialog) { // from class: org.thoughtcrime.securesms.pin.RegistrationLockV2Dialog$$ExternalSyntheticLambda1
            public final /* synthetic */ ProgressBar f$0;
            public final /* synthetic */ Context f$1;
            public final /* synthetic */ Runnable f$2;
            public final /* synthetic */ AlertDialog f$3;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                RegistrationLockV2Dialog.lambda$showEnableDialog$1(this.f$0, this.f$1, this.f$2, this.f$3, (Boolean) obj);
            }
        });
    }

    public static /* synthetic */ Boolean lambda$showEnableDialog$0() {
        try {
            PinState.onEnableRegistrationLockForUserWithPin();
            Log.i(TAG, "Successfully enabled registration lock.");
            return Boolean.TRUE;
        } catch (IOException e) {
            Log.w(TAG, "Failed to enable registration lock setting.", e);
            return Boolean.FALSE;
        }
    }

    public static /* synthetic */ void lambda$showEnableDialog$1(ProgressBar progressBar, Context context, Runnable runnable, AlertDialog alertDialog, Boolean bool) {
        progressBar.setVisibility(8);
        if (!bool.booleanValue()) {
            Toast.makeText(context, (int) R.string.preferences_app_protection__failed_to_enable_registration_lock, 1).show();
        } else {
            runnable.run();
        }
        alertDialog.dismiss();
    }

    public static void showDisableDialog(Context context, Runnable runnable) {
        AlertDialog create = new MaterialAlertDialogBuilder(context).setTitle(R.string.RegistrationLockV2Dialog_turn_off_registration_lock).setView(R.layout.registration_lock_v2_dialog).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).setPositiveButton(R.string.RegistrationLockV2Dialog_turn_off, (DialogInterface.OnClickListener) null).create();
        create.setOnShowListener(new DialogInterface.OnShowListener(context, runnable) { // from class: org.thoughtcrime.securesms.pin.RegistrationLockV2Dialog$$ExternalSyntheticLambda4
            public final /* synthetic */ Context f$1;
            public final /* synthetic */ Runnable f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // android.content.DialogInterface.OnShowListener
            public final void onShow(DialogInterface dialogInterface) {
                RegistrationLockV2Dialog.lambda$showDisableDialog$7(AlertDialog.this, this.f$1, this.f$2, dialogInterface);
            }
        });
        create.show();
    }

    public static /* synthetic */ void lambda$showDisableDialog$7(AlertDialog alertDialog, Context context, Runnable runnable, DialogInterface dialogInterface) {
        ProgressBar progressBar = (ProgressBar) alertDialog.findViewById(R.id.reglockv2_dialog_progress);
        Objects.requireNonNull(progressBar);
        alertDialog.getButton(-1).setOnClickListener(new View.OnClickListener(progressBar, context, runnable, alertDialog) { // from class: org.thoughtcrime.securesms.pin.RegistrationLockV2Dialog$$ExternalSyntheticLambda5
            public final /* synthetic */ ProgressBar f$0;
            public final /* synthetic */ Context f$1;
            public final /* synthetic */ Runnable f$2;
            public final /* synthetic */ AlertDialog f$3;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                RegistrationLockV2Dialog.lambda$showDisableDialog$6(this.f$0, this.f$1, this.f$2, this.f$3, view);
            }
        });
    }

    public static /* synthetic */ void lambda$showDisableDialog$6(ProgressBar progressBar, Context context, Runnable runnable, AlertDialog alertDialog, View view) {
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(0);
        SimpleTask.run(SignalExecutors.UNBOUNDED, new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.pin.RegistrationLockV2Dialog$$ExternalSyntheticLambda2
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return RegistrationLockV2Dialog.lambda$showDisableDialog$4();
            }
        }, new SimpleTask.ForegroundTask(progressBar, context, runnable, alertDialog) { // from class: org.thoughtcrime.securesms.pin.RegistrationLockV2Dialog$$ExternalSyntheticLambda3
            public final /* synthetic */ ProgressBar f$0;
            public final /* synthetic */ Context f$1;
            public final /* synthetic */ Runnable f$2;
            public final /* synthetic */ AlertDialog f$3;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                RegistrationLockV2Dialog.lambda$showDisableDialog$5(this.f$0, this.f$1, this.f$2, this.f$3, (Boolean) obj);
            }
        });
    }

    public static /* synthetic */ Boolean lambda$showDisableDialog$4() {
        try {
            PinState.onDisableRegistrationLockForUserWithPin();
            Log.i(TAG, "Successfully disabled registration lock.");
            return Boolean.TRUE;
        } catch (IOException e) {
            Log.w(TAG, "Failed to disable registration lock.", e);
            return Boolean.FALSE;
        }
    }

    public static /* synthetic */ void lambda$showDisableDialog$5(ProgressBar progressBar, Context context, Runnable runnable, AlertDialog alertDialog, Boolean bool) {
        progressBar.setVisibility(8);
        if (!bool.booleanValue()) {
            Toast.makeText(context, (int) R.string.preferences_app_protection__failed_to_disable_registration_lock, 1).show();
        } else {
            runnable.run();
        }
        alertDialog.dismiss();
    }
}
