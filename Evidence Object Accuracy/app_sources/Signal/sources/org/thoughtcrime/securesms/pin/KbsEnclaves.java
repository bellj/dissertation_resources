package org.thoughtcrime.securesms.pin;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.KbsEnclave;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public final class KbsEnclaves {
    public static KbsEnclave current() {
        return BuildConfig.KBS_ENCLAVE;
    }

    public static List<KbsEnclave> all() {
        return Util.join(Collections.singletonList(BuildConfig.KBS_ENCLAVE), fallbacks());
    }

    public static List<KbsEnclave> fallbacks() {
        return Arrays.asList(BuildConfig.KBS_FALLBACKS);
    }
}
