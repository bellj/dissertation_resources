package org.thoughtcrime.securesms.pin;

import android.app.Application;
import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.StorageAccountRestoreJob;
import org.thoughtcrime.securesms.jobs.StorageSyncJob;
import org.thoughtcrime.securesms.pin.PinRestoreRepository;
import org.thoughtcrime.securesms.util.Stopwatch;
import org.whispersystems.signalservice.api.KbsPinData;
import org.whispersystems.signalservice.api.KeyBackupSystemNoDataException;

/* loaded from: classes4.dex */
public class PinRestoreRepository {
    private static final String TAG = Log.tag(PinRestoreRepository.class);
    private final Executor executor = SignalExecutors.UNBOUNDED;

    /* loaded from: classes4.dex */
    public interface Callback<T> {
        void onComplete(T t);
    }

    /* loaded from: classes4.dex */
    public enum PinResult {
        SUCCESS,
        INCORRECT,
        LOCKED,
        NETWORK_ERROR
    }

    public void submitPin(String str, TokenData tokenData, Callback<PinResultData> callback) {
        this.executor.execute(new Runnable(str, tokenData, callback) { // from class: org.thoughtcrime.securesms.pin.PinRestoreRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$0;
            public final /* synthetic */ TokenData f$1;
            public final /* synthetic */ PinRestoreRepository.Callback f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                PinRestoreRepository.m2445$r8$lambda$VJlnccMCsMPh_tFnD8nFcZuk2w(this.f$0, this.f$1, this.f$2);
            }
        });
    }

    public static /* synthetic */ void lambda$submitPin$0(String str, TokenData tokenData, Callback callback) {
        try {
            Stopwatch stopwatch = new Stopwatch("PinSubmission");
            KbsPinData restoreMasterKey = KbsRepository.restoreMasterKey(str, tokenData.getEnclave(), tokenData.getBasicAuth(), tokenData.getTokenResponse());
            Application application = ApplicationDependencies.getApplication();
            Objects.requireNonNull(restoreMasterKey);
            PinState.onSignalPinRestore(application, restoreMasterKey, str);
            stopwatch.split("MasterKey");
            ApplicationDependencies.getJobManager().runSynchronously(new StorageAccountRestoreJob(), StorageAccountRestoreJob.LIFESPAN);
            stopwatch.split("AccountRestore");
            ApplicationDependencies.getJobManager().runSynchronously(new StorageSyncJob(), TimeUnit.SECONDS.toMillis(10));
            stopwatch.split("ContactRestore");
            stopwatch.stop(TAG);
            callback.onComplete(new PinResultData(PinResult.SUCCESS, tokenData));
        } catch (IOException unused) {
            callback.onComplete(new PinResultData(PinResult.NETWORK_ERROR, tokenData));
        } catch (KeyBackupSystemWrongPinException e) {
            callback.onComplete(new PinResultData(PinResult.INCORRECT, TokenData.withResponse(tokenData, e.getTokenResponse())));
        } catch (KeyBackupSystemNoDataException unused2) {
            callback.onComplete(new PinResultData(PinResult.LOCKED, tokenData));
        }
    }

    /* loaded from: classes4.dex */
    public static class PinResultData {
        private final PinResult result;
        private final TokenData tokenData;

        PinResultData(PinResult pinResult, TokenData tokenData) {
            this.result = pinResult;
            this.tokenData = tokenData;
        }

        public PinResult getResult() {
            return this.result;
        }

        public TokenData getTokenData() {
            return this.tokenData;
        }
    }
}
