package org.thoughtcrime.securesms;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.transition.AutoTransition;
import androidx.transition.TransitionManager;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.pnikosis.materialishprogress.ProgressWheel;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Consumer;
import j$.util.function.Function;
import j$.util.function.Predicate;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.ContactSelectionListFragment;
import org.thoughtcrime.securesms.components.RecyclerViewFastScroller;
import org.thoughtcrime.securesms.contacts.AbstractContactsCursorLoader;
import org.thoughtcrime.securesms.contacts.ContactChipViewModel;
import org.thoughtcrime.securesms.contacts.ContactSelectionListAdapter;
import org.thoughtcrime.securesms.contacts.ContactSelectionListItem;
import org.thoughtcrime.securesms.contacts.ContactsCursorLoader;
import org.thoughtcrime.securesms.contacts.HeaderAction;
import org.thoughtcrime.securesms.contacts.LetterHeaderDecoration;
import org.thoughtcrime.securesms.contacts.SelectedContact;
import org.thoughtcrime.securesms.contacts.SelectedContacts;
import org.thoughtcrime.securesms.contacts.selection.ContactSelectionArguments;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscovery;
import org.thoughtcrime.securesms.groups.SelectionLimits;
import org.thoughtcrime.securesms.groups.ui.GroupLimitDialog;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.sharing.ShareContact;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.UsernameUtil;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.adapter.FixedViewsAdapter;
import org.thoughtcrime.securesms.util.adapter.RecyclerViewConcatenateAdapterStickyHeader;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes.dex */
public final class ContactSelectionListFragment extends LoggingFragment implements LoaderManager.LoaderCallbacks<Cursor> {
    public static final String CAN_SELECT_SELF;
    private static final int CHIP_GROUP_EMPTY_CHILD_COUNT;
    private static final int CHIP_GROUP_REVEAL_DURATION_MS;
    public static final String CURRENT_SELECTION;
    public static final String DISPLAY_CHIPS;
    public static final String DISPLAY_MODE;
    public static final String HIDE_COUNT;
    public static final int NO_LIMIT;
    public static final String RECENTS;
    public static final String REFRESHABLE;
    public static final String RV_CLIP;
    public static final String RV_PADDING_BOTTOM;
    public static final String SELECTION_LIMITS;
    private static final String TAG = Log.tag(ContactSelectionListFragment.class);
    private boolean canSelectSelf;
    private RecyclerView chipRecycler;
    private ConstraintLayout constraintLayout;
    private MappingAdapter contactChipAdapter;
    private ContactChipViewModel contactChipViewModel;
    private Set<RecipientId> currentSelection;
    private AbstractContactsCursorLoaderFactoryProvider cursorFactoryProvider;
    private String cursorFilter;
    private ContactSelectionListAdapter cursorRecyclerViewAdapter;
    private TextView emptyText;
    private RecyclerViewFastScroller fastScroller;
    private FixedViewsAdapter footerAdapter;
    private GlideRequests glideRequests;
    private HeaderActionProvider headerActionProvider;
    private TextView headerActionView;
    private FixedViewsAdapter headerAdapter;
    private boolean hideCount;
    private boolean isMulti;
    private LifecycleDisposable lifecycleDisposable;
    private ListCallback listCallback;
    private OnContactSelectedListener onContactSelectedListener;
    private OnSelectionLimitReachedListener onSelectionLimitReachedListener;
    private RecyclerView recyclerView;
    private ScrollCallback scrollCallback;
    private SelectionLimits selectionLimit = SelectionLimits.NO_LIMITS;
    private Button showContactsButton;
    private TextView showContactsDescription;
    private View showContactsLayout;
    private ProgressWheel showContactsProgress;
    private SwipeRefreshLayout swipeRefresh;

    /* loaded from: classes.dex */
    public interface AbstractContactsCursorLoaderFactoryProvider {
        AbstractContactsCursorLoader.Factory get();
    }

    /* loaded from: classes.dex */
    public interface HeaderActionProvider {
        HeaderAction getHeaderAction();
    }

    /* loaded from: classes.dex */
    public interface ListCallback {
        void onInvite();

        void onNewGroup(boolean z);
    }

    /* loaded from: classes.dex */
    public interface OnContactSelectedListener {
        void onBeforeContactSelected(Optional<RecipientId> optional, String str, Consumer<Boolean> consumer);

        void onContactDeselected(Optional<RecipientId> optional, String str);

        void onSelectionChanged();
    }

    /* loaded from: classes.dex */
    public interface OnSelectionLimitReachedListener {
        void onHardLimitReached(int i);

        void onSuggestedLimitReached(int i);
    }

    /* loaded from: classes.dex */
    public interface ScrollCallback {
        void onBeginScroll();
    }

    @Override // androidx.loader.app.LoaderManager.LoaderCallbacks
    public /* bridge */ /* synthetic */ void onLoadFinished(Loader loader, Object obj) {
        onLoadFinished((Loader<Cursor>) loader, (Cursor) obj);
    }

    @Override // androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ListCallback) {
            this.listCallback = (ListCallback) context;
        }
        if (getParentFragment() instanceof ScrollCallback) {
            this.scrollCallback = (ScrollCallback) getParentFragment();
        }
        if (context instanceof ScrollCallback) {
            this.scrollCallback = (ScrollCallback) context;
        }
        if (getParentFragment() instanceof OnContactSelectedListener) {
            this.onContactSelectedListener = (OnContactSelectedListener) getParentFragment();
        }
        if (context instanceof OnContactSelectedListener) {
            this.onContactSelectedListener = (OnContactSelectedListener) context;
        }
        if (context instanceof OnSelectionLimitReachedListener) {
            this.onSelectionLimitReachedListener = (OnSelectionLimitReachedListener) context;
        }
        if (getParentFragment() instanceof OnSelectionLimitReachedListener) {
            this.onSelectionLimitReachedListener = (OnSelectionLimitReachedListener) getParentFragment();
        }
        if (context instanceof AbstractContactsCursorLoaderFactoryProvider) {
            this.cursorFactoryProvider = (AbstractContactsCursorLoaderFactoryProvider) context;
        }
        if (getParentFragment() instanceof AbstractContactsCursorLoaderFactoryProvider) {
            this.cursorFactoryProvider = (AbstractContactsCursorLoaderFactoryProvider) getParentFragment();
        }
        if (context instanceof HeaderActionProvider) {
            this.headerActionProvider = (HeaderActionProvider) context;
        }
        if (getParentFragment() instanceof HeaderActionProvider) {
            this.headerActionProvider = (HeaderActionProvider) getParentFragment();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        initializeCursor();
    }

    public void onStart() {
        super.onStart();
        Permissions.with(this).request("android.permission.WRITE_CONTACTS", "android.permission.READ_CONTACTS").ifNecessary().onAllGranted(new Runnable() { // from class: org.thoughtcrime.securesms.ContactSelectionListFragment$$ExternalSyntheticLambda8
            @Override // java.lang.Runnable
            public final void run() {
                ContactSelectionListFragment.this.lambda$onStart$0();
            }
        }).onAnyDenied(new Runnable() { // from class: org.thoughtcrime.securesms.ContactSelectionListFragment$$ExternalSyntheticLambda9
            @Override // java.lang.Runnable
            public final void run() {
                ContactSelectionListFragment.this.lambda$onStart$1();
            }
        }).execute();
    }

    public /* synthetic */ void lambda$onStart$0() {
        if (!TextSecurePreferences.hasSuccessfullyRetrievedDirectory(getActivity())) {
            handleContactPermissionGranted();
        } else {
            LoaderManager.getInstance(this).initLoader(0, null, this);
        }
    }

    public /* synthetic */ void lambda$onStart$1() {
        FragmentActivity requireActivity = requireActivity();
        requireActivity.getWindow().setSoftInputMode(3);
        if (safeArguments().getBoolean("recents", requireActivity.getIntent().getBooleanExtra("recents", false))) {
            LoaderManager.getInstance(this).initLoader(0, null, this);
        } else {
            initializeNoContactsPermission();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.contact_selection_list_fragment, viewGroup, false);
        this.emptyText = (TextView) inflate.findViewById(16908292);
        this.recyclerView = (RecyclerView) inflate.findViewById(R.id.recycler_view);
        this.swipeRefresh = (SwipeRefreshLayout) inflate.findViewById(R.id.swipe_refresh);
        this.fastScroller = (RecyclerViewFastScroller) inflate.findViewById(R.id.fast_scroller);
        this.showContactsLayout = inflate.findViewById(R.id.show_contacts_container);
        this.showContactsButton = (Button) inflate.findViewById(R.id.show_contacts_button);
        this.showContactsDescription = (TextView) inflate.findViewById(R.id.show_contacts_description);
        this.showContactsProgress = (ProgressWheel) inflate.findViewById(R.id.progress);
        this.chipRecycler = (RecyclerView) inflate.findViewById(R.id.chipRecycler);
        this.constraintLayout = (ConstraintLayout) inflate.findViewById(R.id.container);
        this.headerActionView = (TextView) inflate.findViewById(R.id.header_action);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(requireContext());
        this.recyclerView.setLayoutManager(linearLayoutManager);
        this.recyclerView.setItemAnimator(new DefaultItemAnimator() { // from class: org.thoughtcrime.securesms.ContactSelectionListFragment.1
            public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder viewHolder) {
                return true;
            }
        });
        this.contactChipViewModel = (ContactChipViewModel) new ViewModelProvider(this).get(ContactChipViewModel.class);
        this.contactChipAdapter = new MappingAdapter();
        LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
        this.lifecycleDisposable = lifecycleDisposable;
        lifecycleDisposable.bindTo(getViewLifecycleOwner());
        SelectedContacts.register(this.contactChipAdapter, new Function1() { // from class: org.thoughtcrime.securesms.ContactSelectionListFragment$$ExternalSyntheticLambda4
            @Override // kotlin.jvm.functions.Function1
            public final Object invoke(Object obj) {
                return ContactSelectionListFragment.this.onChipCloseIconClicked((SelectedContacts.Model) obj);
            }
        });
        this.chipRecycler.setAdapter(this.contactChipAdapter);
        this.lifecycleDisposable.add(this.contactChipViewModel.getState().subscribe(new io.reactivex.rxjava3.functions.Consumer() { // from class: org.thoughtcrime.securesms.ContactSelectionListFragment$$ExternalSyntheticLambda5
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ContactSelectionListFragment.this.handleSelectedContactsChanged((List) obj);
            }
        }));
        Intent intent = requireActivity().getIntent();
        Bundle safeArguments = safeArguments();
        int i = safeArguments.getInt("recycler_view_padding_bottom", intent.getIntExtra("recycler_view_padding_bottom", -1));
        boolean z = safeArguments.getBoolean("recycler_view_clipping", intent.getBooleanExtra("recycler_view_clipping", true));
        if (i != -1) {
            ViewUtil.setPaddingBottom(this.recyclerView, i);
        }
        this.recyclerView.setClipToPadding(z);
        boolean z2 = safeArguments.getBoolean("refreshable", intent.getBooleanExtra("refreshable", true));
        this.swipeRefresh.setNestedScrollingEnabled(z2);
        this.swipeRefresh.setEnabled(z2);
        this.hideCount = safeArguments.getBoolean("hide_count", intent.getBooleanExtra("hide_count", false));
        SelectionLimits selectionLimits = (SelectionLimits) safeArguments.getParcelable("selection_limits");
        this.selectionLimit = selectionLimits;
        if (selectionLimits == null) {
            this.selectionLimit = (SelectionLimits) intent.getParcelableExtra("selection_limits");
        }
        boolean z3 = this.selectionLimit != null;
        this.isMulti = z3;
        this.canSelectSelf = safeArguments.getBoolean("can_select_self", intent.getBooleanExtra("can_select_self", !z3));
        if (!this.isMulti) {
            this.selectionLimit = SelectionLimits.NO_LIMITS;
        }
        this.currentSelection = getCurrentSelection();
        HeaderActionProvider headerActionProvider = this.headerActionProvider;
        if (headerActionProvider != null) {
            HeaderAction headerAction = headerActionProvider.getHeaderAction();
            this.headerActionView.setEnabled(true);
            this.headerActionView.setText(headerAction.getLabel());
            this.headerActionView.setCompoundDrawablesRelativeWithIntrinsicBounds(headerAction.getIcon(), 0, 0, 0);
            this.headerActionView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.ContactSelectionListFragment$$ExternalSyntheticLambda6
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ContactSelectionListFragment.lambda$onCreateView$2(HeaderAction.this, view);
                }
            });
            this.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() { // from class: org.thoughtcrime.securesms.ContactSelectionListFragment.2
                private final Rect bounds = new Rect();

                public void onScrollStateChanged(RecyclerView recyclerView, int i2) {
                }

                public void onScrolled(RecyclerView recyclerView, int i2, int i3) {
                    if (!ContactSelectionListFragment.this.hideLetterHeaders() && linearLayoutManager.findFirstVisibleItemPosition() == 0) {
                        recyclerView.getDecoratedBoundsWithMargins(recyclerView.getChildAt(0), this.bounds);
                        ContactSelectionListFragment.this.headerActionView.setTranslationY((float) this.bounds.top);
                    }
                }
            });
        } else {
            this.headerActionView.setEnabled(false);
        }
        return inflate;
    }

    public static /* synthetic */ void lambda$onCreateView$2(HeaderAction headerAction, View view) {
        headerAction.getAction().run();
    }

    private Bundle safeArguments() {
        return getArguments() != null ? getArguments() : new Bundle();
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        Permissions.onRequestPermissionsResult(this, i, strArr, iArr);
    }

    public List<SelectedContact> getSelectedContacts() {
        ContactSelectionListAdapter contactSelectionListAdapter = this.cursorRecyclerViewAdapter;
        if (contactSelectionListAdapter == null) {
            return Collections.emptyList();
        }
        return contactSelectionListAdapter.getSelectedContacts();
    }

    public int getSelectedContactsCount() {
        ContactSelectionListAdapter contactSelectionListAdapter = this.cursorRecyclerViewAdapter;
        if (contactSelectionListAdapter == null) {
            return 0;
        }
        return contactSelectionListAdapter.getSelectedContactsCount();
    }

    public int getTotalMemberCount() {
        ContactSelectionListAdapter contactSelectionListAdapter = this.cursorRecyclerViewAdapter;
        if (contactSelectionListAdapter == null) {
            return 0;
        }
        return contactSelectionListAdapter.getSelectedContactsCount() + this.cursorRecyclerViewAdapter.getCurrentContactsCount();
    }

    private Set<RecipientId> getCurrentSelection() {
        ArrayList parcelableArrayList = safeArguments().getParcelableArrayList("current_selection");
        if (parcelableArrayList == null) {
            parcelableArrayList = requireActivity().getIntent().getParcelableArrayListExtra("current_selection");
        }
        if (parcelableArrayList == null) {
            return Collections.emptySet();
        }
        return Collections.unmodifiableSet((Set) Stream.of(parcelableArrayList).collect(Collectors.toSet()));
    }

    public boolean isMulti() {
        return this.isMulti;
    }

    private void initializeCursor() {
        this.glideRequests = GlideApp.with(this);
        this.cursorRecyclerViewAdapter = new ContactSelectionListAdapter(requireContext(), this.glideRequests, null, new ListClickListener(), this.isMulti, this.currentSelection, safeArguments().getInt(ContactSelectionArguments.CHECKBOX_RESOURCE, R.drawable.contact_selection_checkbox));
        RecyclerViewConcatenateAdapterStickyHeader recyclerViewConcatenateAdapterStickyHeader = new RecyclerViewConcatenateAdapterStickyHeader();
        ListCallback listCallback = this.listCallback;
        if (listCallback != null) {
            FixedViewsAdapter fixedViewsAdapter = new FixedViewsAdapter(createNewGroupItem(listCallback));
            this.headerAdapter = fixedViewsAdapter;
            fixedViewsAdapter.hide();
            recyclerViewConcatenateAdapterStickyHeader.addAdapter(this.headerAdapter);
        }
        recyclerViewConcatenateAdapterStickyHeader.addAdapter(this.cursorRecyclerViewAdapter);
        ListCallback listCallback2 = this.listCallback;
        if (listCallback2 != null) {
            FixedViewsAdapter fixedViewsAdapter2 = new FixedViewsAdapter(createInviteActionView(listCallback2));
            this.footerAdapter = fixedViewsAdapter2;
            fixedViewsAdapter2.hide();
            recyclerViewConcatenateAdapterStickyHeader.addAdapter(this.footerAdapter);
        }
        this.recyclerView.addItemDecoration(new LetterHeaderDecoration(requireContext(), new Function0() { // from class: org.thoughtcrime.securesms.ContactSelectionListFragment$$ExternalSyntheticLambda1
            @Override // kotlin.jvm.functions.Function0
            public final Object invoke() {
                return Boolean.valueOf(ContactSelectionListFragment.this.hideLetterHeaders());
            }
        }));
        this.recyclerView.setAdapter(recyclerViewConcatenateAdapterStickyHeader);
        this.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() { // from class: org.thoughtcrime.securesms.ContactSelectionListFragment.3
            public void onScrollStateChanged(RecyclerView recyclerView, int i) {
                if (i == 1 && ContactSelectionListFragment.this.scrollCallback != null) {
                    ContactSelectionListFragment.this.scrollCallback.onBeginScroll();
                }
            }
        });
        OnContactSelectedListener onContactSelectedListener = this.onContactSelectedListener;
        if (onContactSelectedListener != null) {
            onContactSelectedListener.onSelectionChanged();
        }
    }

    public boolean hideLetterHeaders() {
        return hasQueryFilter() || shouldDisplayRecents();
    }

    private View createInviteActionView(ListCallback listCallback) {
        View inflate = LayoutInflater.from(requireContext()).inflate(R.layout.contact_selection_invite_action_item, (ViewGroup) requireView(), false);
        inflate.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.ContactSelectionListFragment$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ContactSelectionListFragment.ListCallback.this.onInvite();
            }
        });
        return inflate;
    }

    private View createNewGroupItem(ListCallback listCallback) {
        View inflate = LayoutInflater.from(requireContext()).inflate(R.layout.contact_selection_new_group_item, (ViewGroup) requireView(), false);
        inflate.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.ContactSelectionListFragment$$ExternalSyntheticLambda14
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ContactSelectionListFragment.ListCallback.this.onNewGroup(false);
            }
        });
        return inflate;
    }

    public void initializeNoContactsPermission() {
        this.swipeRefresh.setVisibility(8);
        this.showContactsLayout.setVisibility(0);
        this.showContactsProgress.setVisibility(4);
        this.showContactsDescription.setText(R.string.contact_selection_list_fragment__signal_needs_access_to_your_contacts_in_order_to_display_them);
        this.showContactsButton.setVisibility(0);
        this.showContactsButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.ContactSelectionListFragment$$ExternalSyntheticLambda7
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ContactSelectionListFragment.this.lambda$initializeNoContactsPermission$6(view);
            }
        });
    }

    public /* synthetic */ void lambda$initializeNoContactsPermission$6(View view) {
        Permissions.with(this).request("android.permission.WRITE_CONTACTS", "android.permission.READ_CONTACTS").ifNecessary().withPermanentDenialDialog(getString(R.string.ContactSelectionListFragment_signal_requires_the_contacts_permission_in_order_to_display_your_contacts)).onSomeGranted(new com.annimon.stream.function.Consumer() { // from class: org.thoughtcrime.securesms.ContactSelectionListFragment$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Consumer
            public final void accept(Object obj) {
                ContactSelectionListFragment.this.lambda$initializeNoContactsPermission$5((List) obj);
            }
        }).execute();
    }

    public /* synthetic */ void lambda$initializeNoContactsPermission$5(List list) {
        if (list.contains("android.permission.WRITE_CONTACTS")) {
            handleContactPermissionGranted();
        }
    }

    public void setQueryFilter(String str) {
        this.cursorFilter = str;
        LoaderManager.getInstance(this).restartLoader(0, null, this);
    }

    public void resetQueryFilter() {
        setQueryFilter(null);
        this.swipeRefresh.setRefreshing(false);
    }

    public boolean hasQueryFilter() {
        return !TextUtils.isEmpty(this.cursorFilter);
    }

    public void setRefreshing(boolean z) {
        this.swipeRefresh.setRefreshing(z);
    }

    public void reset() {
        this.cursorRecyclerViewAdapter.clearSelectedContacts();
        if (!isDetached() && !isRemoving() && getActivity() != null && !getActivity().isFinishing()) {
            LoaderManager.getInstance(this).restartLoader(0, null, this);
        }
    }

    public void setRecyclerViewPaddingBottom(int i) {
        ViewUtil.setPaddingBottom(this.recyclerView, i);
    }

    @Override // androidx.loader.app.LoaderManager.LoaderCallbacks
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        FragmentActivity requireActivity = requireActivity();
        int i2 = safeArguments().getInt("display_mode", requireActivity.getIntent().getIntExtra("display_mode", 31));
        boolean shouldDisplayRecents = shouldDisplayRecents();
        AbstractContactsCursorLoaderFactoryProvider abstractContactsCursorLoaderFactoryProvider = this.cursorFactoryProvider;
        if (abstractContactsCursorLoaderFactoryProvider != null) {
            return abstractContactsCursorLoaderFactoryProvider.get().create();
        }
        return new ContactsCursorLoader.Factory(requireActivity, i2, this.cursorFilter, shouldDisplayRecents).create();
    }

    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        this.swipeRefresh.setVisibility(0);
        this.showContactsLayout.setVisibility(8);
        this.cursorRecyclerViewAdapter.changeCursor(cursor);
        FixedViewsAdapter fixedViewsAdapter = this.footerAdapter;
        if (fixedViewsAdapter != null) {
            fixedViewsAdapter.show();
        }
        if (this.headerAdapter != null) {
            if (TextUtils.isEmpty(this.cursorFilter)) {
                this.headerAdapter.show();
            } else {
                this.headerAdapter.hide();
            }
        }
        this.emptyText.setText(R.string.contact_selection_group_activity__no_contacts);
        boolean z = cursor != null && cursor.getCount() > 20;
        this.recyclerView.setVerticalScrollBarEnabled(!z);
        if (z) {
            this.fastScroller.setVisibility(0);
            this.fastScroller.setRecyclerView(this.recyclerView);
        } else {
            this.fastScroller.setRecyclerView(null);
            this.fastScroller.setVisibility(8);
        }
        if (!this.headerActionView.isEnabled() || hasQueryFilter()) {
            this.headerActionView.setVisibility(8);
        } else {
            this.headerActionView.setVisibility(0);
        }
    }

    public void onLoaderReset(Loader<Cursor> loader) {
        this.cursorRecyclerViewAdapter.changeCursor(null);
        this.fastScroller.setVisibility(8);
        this.headerActionView.setVisibility(8);
    }

    private boolean shouldDisplayRecents() {
        return safeArguments().getBoolean("recents", requireActivity().getIntent().getBooleanExtra("recents", false));
    }

    private void handleContactPermissionGranted() {
        final Context requireContext = requireContext();
        new AsyncTask<Void, Void, Boolean>() { // from class: org.thoughtcrime.securesms.ContactSelectionListFragment.4
            protected void onPreExecute() {
                ContactSelectionListFragment.this.swipeRefresh.setVisibility(8);
                ContactSelectionListFragment.this.showContactsLayout.setVisibility(0);
                ContactSelectionListFragment.this.showContactsButton.setVisibility(4);
                ContactSelectionListFragment.this.showContactsDescription.setText(R.string.ConversationListFragment_loading);
                ContactSelectionListFragment.this.showContactsProgress.setVisibility(0);
                ContactSelectionListFragment.this.showContactsProgress.spin();
            }

            public Boolean doInBackground(Void... voidArr) {
                try {
                    ContactDiscovery.refreshAll(requireContext, false);
                    return Boolean.TRUE;
                } catch (IOException e) {
                    Log.w(ContactSelectionListFragment.TAG, e);
                    return Boolean.FALSE;
                }
            }

            public void onPostExecute(Boolean bool) {
                if (bool.booleanValue()) {
                    ContactSelectionListFragment.this.showContactsLayout.setVisibility(8);
                    ContactSelectionListFragment.this.swipeRefresh.setVisibility(0);
                    ContactSelectionListFragment.this.reset();
                } else if (ContactSelectionListFragment.this.getContext() != null) {
                    Toast.makeText(ContactSelectionListFragment.this.getContext(), (int) R.string.ContactSelectionListFragment_error_retrieving_contacts_check_your_network_connection, 1).show();
                    ContactSelectionListFragment.this.initializeNoContactsPermission();
                }
            }
        }.execute(new Void[0]);
    }

    public void markSelected(Set<ShareContact> set) {
        if (!set.isEmpty()) {
            Set<SelectedContact> set2 = (Set) Collection$EL.stream(set).map(new Function() { // from class: org.thoughtcrime.securesms.ContactSelectionListFragment$$ExternalSyntheticLambda10
                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return ContactSelectionListFragment.lambda$markSelected$7((ShareContact) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.ContactSelectionListFragment$$ExternalSyntheticLambda11
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return ContactSelectionListFragment.this.lambda$markSelected$8((SelectedContact) obj);
                }
            }).collect(j$.util.stream.Collectors.toSet());
            if (!set2.isEmpty()) {
                for (SelectedContact selectedContact : set2) {
                    markContactSelected(selectedContact);
                }
                ContactSelectionListAdapter contactSelectionListAdapter = this.cursorRecyclerViewAdapter;
                contactSelectionListAdapter.notifyItemRangeChanged(0, contactSelectionListAdapter.getItemCount());
            }
        }
    }

    public static /* synthetic */ SelectedContact lambda$markSelected$7(ShareContact shareContact) {
        if (shareContact.getRecipientId().isPresent()) {
            return SelectedContact.forRecipientId(shareContact.getRecipientId().get());
        }
        return SelectedContact.forPhone(null, shareContact.getNumber());
    }

    public /* synthetic */ boolean lambda$markSelected$8(SelectedContact selectedContact) {
        return !this.cursorRecyclerViewAdapter.isSelectedContact(selectedContact);
    }

    /* loaded from: classes.dex */
    public class ListClickListener implements ContactSelectionListAdapter.ItemClickListener {
        private ListClickListener() {
            ContactSelectionListFragment.this = r1;
        }

        public void onItemClick(ContactSelectionListItem contactSelectionListItem) {
            SelectedContact selectedContact;
            if (contactSelectionListItem.isUsernameType()) {
                selectedContact = SelectedContact.forUsername(contactSelectionListItem.getRecipientId().orElse(null), contactSelectionListItem.getNumber());
            } else {
                selectedContact = SelectedContact.forPhone(contactSelectionListItem.getRecipientId().orElse(null), contactSelectionListItem.getNumber());
            }
            if (!ContactSelectionListFragment.this.canSelectSelf && Recipient.self().getId().equals(selectedContact.getOrCreateRecipientId(ContactSelectionListFragment.this.requireContext()))) {
                Toast.makeText(ContactSelectionListFragment.this.requireContext(), (int) R.string.ContactSelectionListFragment_you_do_not_need_to_add_yourself_to_the_group, 0).show();
            } else if (ContactSelectionListFragment.this.isMulti && ContactSelectionListFragment.this.cursorRecyclerViewAdapter.isSelectedContact(selectedContact)) {
                ContactSelectionListFragment.this.markContactUnselected(selectedContact);
                ContactSelectionListFragment.this.cursorRecyclerViewAdapter.notifyItemRangeChanged(0, ContactSelectionListFragment.this.cursorRecyclerViewAdapter.getItemCount(), 1);
                if (ContactSelectionListFragment.this.onContactSelectedListener != null) {
                    ContactSelectionListFragment.this.onContactSelectedListener.onContactDeselected(contactSelectionListItem.getRecipientId(), contactSelectionListItem.getNumber());
                }
            } else if (ContactSelectionListFragment.this.selectionHardLimitReached()) {
                if (ContactSelectionListFragment.this.onSelectionLimitReachedListener != null) {
                    ContactSelectionListFragment.this.onSelectionLimitReachedListener.onHardLimitReached(ContactSelectionListFragment.this.selectionLimit.getHardLimit());
                } else {
                    GroupLimitDialog.showHardLimitMessage(ContactSelectionListFragment.this.requireContext());
                }
            } else if (contactSelectionListItem.isUsernameType()) {
                SimpleTask.run(ContactSelectionListFragment.this.getViewLifecycleOwner().getLifecycle(), new ContactSelectionListFragment$ListClickListener$$ExternalSyntheticLambda2(contactSelectionListItem), new ContactSelectionListFragment$ListClickListener$$ExternalSyntheticLambda3(this, SimpleProgressDialog.show(ContactSelectionListFragment.this.requireContext()), contactSelectionListItem));
            } else if (ContactSelectionListFragment.this.onContactSelectedListener != null) {
                ContactSelectionListFragment.this.onContactSelectedListener.onBeforeContactSelected(contactSelectionListItem.getRecipientId(), contactSelectionListItem.getNumber(), new ContactSelectionListFragment$ListClickListener$$ExternalSyntheticLambda4(this, selectedContact));
            } else {
                ContactSelectionListFragment.this.markContactSelected(selectedContact);
                ContactSelectionListFragment.this.cursorRecyclerViewAdapter.notifyItemRangeChanged(0, ContactSelectionListFragment.this.cursorRecyclerViewAdapter.getItemCount(), 1);
            }
        }

        public static /* synthetic */ Optional lambda$onItemClick$0(ContactSelectionListItem contactSelectionListItem) {
            return UsernameUtil.fetchAciForUsername(contactSelectionListItem.getNumber());
        }

        public /* synthetic */ void lambda$onItemClick$3(AlertDialog alertDialog, ContactSelectionListItem contactSelectionListItem, Optional optional) {
            alertDialog.dismiss();
            if (optional.isPresent()) {
                Recipient externalUsername = Recipient.externalUsername((ServiceId) optional.get(), contactSelectionListItem.getNumber());
                SelectedContact forUsername = SelectedContact.forUsername(externalUsername.getId(), contactSelectionListItem.getNumber());
                if (ContactSelectionListFragment.this.onContactSelectedListener != null) {
                    ContactSelectionListFragment.this.onContactSelectedListener.onBeforeContactSelected(Optional.of(externalUsername.getId()), null, new ContactSelectionListFragment$ListClickListener$$ExternalSyntheticLambda0(this, forUsername));
                    return;
                }
                ContactSelectionListFragment.this.markContactSelected(forUsername);
                ContactSelectionListFragment.this.cursorRecyclerViewAdapter.notifyItemRangeChanged(0, ContactSelectionListFragment.this.cursorRecyclerViewAdapter.getItemCount(), 1);
                return;
            }
            new MaterialAlertDialogBuilder(ContactSelectionListFragment.this.requireContext()).setTitle(R.string.ContactSelectionListFragment_username_not_found).setMessage((CharSequence) ContactSelectionListFragment.this.getString(R.string.ContactSelectionListFragment_s_is_not_a_signal_user, contactSelectionListItem.getNumber())).setPositiveButton(17039370, (DialogInterface.OnClickListener) new ContactSelectionListFragment$ListClickListener$$ExternalSyntheticLambda1()).show();
        }

        public /* synthetic */ void lambda$onItemClick$1(SelectedContact selectedContact, Boolean bool) {
            if (bool.booleanValue()) {
                ContactSelectionListFragment.this.markContactSelected(selectedContact);
                ContactSelectionListFragment.this.cursorRecyclerViewAdapter.notifyItemRangeChanged(0, ContactSelectionListFragment.this.cursorRecyclerViewAdapter.getItemCount(), 1);
            }
        }

        public /* synthetic */ void lambda$onItemClick$4(SelectedContact selectedContact, Boolean bool) {
            if (bool.booleanValue()) {
                ContactSelectionListFragment.this.markContactSelected(selectedContact);
                ContactSelectionListFragment.this.cursorRecyclerViewAdapter.notifyItemRangeChanged(0, ContactSelectionListFragment.this.cursorRecyclerViewAdapter.getItemCount(), 1);
            }
        }
    }

    public boolean selectionHardLimitReached() {
        return getChipCount() + this.currentSelection.size() >= this.selectionLimit.getHardLimit();
    }

    private boolean selectionWarningLimitReachedExactly() {
        return getChipCount() + this.currentSelection.size() == this.selectionLimit.getRecommendedLimit();
    }

    private boolean selectionWarningLimitExceeded() {
        return getChipCount() + this.currentSelection.size() > this.selectionLimit.getRecommendedLimit();
    }

    public void markContactSelected(SelectedContact selectedContact) {
        this.cursorRecyclerViewAdapter.addSelectedContact(selectedContact);
        if (this.isMulti) {
            addChipForSelectedContact(selectedContact);
        }
        OnContactSelectedListener onContactSelectedListener = this.onContactSelectedListener;
        if (onContactSelectedListener != null) {
            onContactSelectedListener.onSelectionChanged();
        }
    }

    public void markContactUnselected(SelectedContact selectedContact) {
        this.cursorRecyclerViewAdapter.removeFromSelectedContacts(selectedContact);
        ContactSelectionListAdapter contactSelectionListAdapter = this.cursorRecyclerViewAdapter;
        contactSelectionListAdapter.notifyItemRangeChanged(0, contactSelectionListAdapter.getItemCount(), 1);
        this.contactChipViewModel.remove(selectedContact);
        OnContactSelectedListener onContactSelectedListener = this.onContactSelectedListener;
        if (onContactSelectedListener != null) {
            onContactSelectedListener.onSelectionChanged();
        }
    }

    public void handleSelectedContactsChanged(List<SelectedContacts.Model> list) {
        this.contactChipAdapter.submitList(new MappingModelList(list), new Runnable() { // from class: org.thoughtcrime.securesms.ContactSelectionListFragment$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                ContactSelectionListFragment.this.smoothScrollChipsToEnd();
            }
        });
        if (list.isEmpty()) {
            setChipGroupVisibility(8);
        } else {
            setChipGroupVisibility(0);
        }
        if (selectionWarningLimitReachedExactly()) {
            OnSelectionLimitReachedListener onSelectionLimitReachedListener = this.onSelectionLimitReachedListener;
            if (onSelectionLimitReachedListener != null) {
                onSelectionLimitReachedListener.onSuggestedLimitReached(this.selectionLimit.getRecommendedLimit());
            } else {
                GroupLimitDialog.showRecommendedLimitMessage(requireContext());
            }
        }
    }

    private void addChipForSelectedContact(SelectedContact selectedContact) {
        SimpleTask.run(getViewLifecycleOwner().getLifecycle(), new SimpleTask.BackgroundTask(selectedContact) { // from class: org.thoughtcrime.securesms.ContactSelectionListFragment$$ExternalSyntheticLambda12
            public final /* synthetic */ SelectedContact f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return ContactSelectionListFragment.this.lambda$addChipForSelectedContact$9(this.f$1);
            }
        }, new SimpleTask.ForegroundTask(selectedContact) { // from class: org.thoughtcrime.securesms.ContactSelectionListFragment$$ExternalSyntheticLambda13
            public final /* synthetic */ SelectedContact f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                ContactSelectionListFragment.this.lambda$addChipForSelectedContact$10(this.f$1, (Recipient) obj);
            }
        });
    }

    public /* synthetic */ Recipient lambda$addChipForSelectedContact$9(SelectedContact selectedContact) {
        return Recipient.resolved(selectedContact.getOrCreateRecipientId(requireContext()));
    }

    public /* synthetic */ void lambda$addChipForSelectedContact$10(SelectedContact selectedContact, Recipient recipient) {
        this.contactChipViewModel.add(selectedContact);
    }

    public Unit onChipCloseIconClicked(SelectedContacts.Model model) {
        markContactUnselected(model.getSelectedContact());
        OnContactSelectedListener onContactSelectedListener = this.onContactSelectedListener;
        if (onContactSelectedListener != null) {
            onContactSelectedListener.onContactDeselected(Optional.of(model.getRecipient().getId()), model.getRecipient().getE164().orElse(null));
        }
        return Unit.INSTANCE;
    }

    private int getChipCount() {
        int count = this.contactChipViewModel.getCount() + 0;
        if (count >= 0) {
            return count;
        }
        throw new AssertionError();
    }

    private void setChipGroupVisibility(int i) {
        if (safeArguments().getBoolean("display_chips", requireActivity().getIntent().getBooleanExtra("display_chips", true))) {
            AutoTransition autoTransition = new AutoTransition();
            autoTransition.setDuration(150L);
            autoTransition.excludeChildren((View) this.recyclerView, true);
            autoTransition.excludeTarget((View) this.recyclerView, true);
            TransitionManager.beginDelayedTransition(this.constraintLayout, autoTransition);
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(this.constraintLayout);
            constraintSet.setVisibility(R.id.chipRecycler, i);
            constraintSet.applyTo(this.constraintLayout);
        }
    }

    public void setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener onRefreshListener) {
        this.swipeRefresh.setOnRefreshListener(onRefreshListener);
    }

    public void smoothScrollChipsToEnd() {
        this.chipRecycler.smoothScrollBy(ViewUtil.isLtr(this.chipRecycler) ? this.chipRecycler.getWidth() : 0, 0);
    }
}
