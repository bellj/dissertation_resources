package org.thoughtcrime.securesms.media;

import android.content.Context;
import android.media.MediaDataSource;
import android.net.Uri;
import java.io.IOException;
import org.thoughtcrime.securesms.attachments.AttachmentId;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.media.MediaInput;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.mms.PartUriParser;
import org.thoughtcrime.securesms.providers.BlobProvider;

/* loaded from: classes4.dex */
public final class DecryptableUriMediaInput {
    private DecryptableUriMediaInput() {
    }

    public static MediaInput createForUri(Context context, Uri uri) throws IOException {
        if (BlobProvider.isAuthority(uri)) {
            return new MediaInput.MediaDataSourceMediaInput(BlobProvider.getInstance().getMediaDataSource(context, uri));
        }
        if (PartAuthority.isLocalUri(uri)) {
            return createForAttachmentUri(context, uri);
        }
        return new MediaInput.UriMediaInput(context, uri);
    }

    private static MediaInput createForAttachmentUri(Context context, Uri uri) {
        AttachmentId partId = new PartUriParser(uri).getPartId();
        if (partId.isValid()) {
            MediaDataSource mediaDataSourceFor = SignalDatabase.attachments().mediaDataSourceFor(partId);
            if (mediaDataSourceFor != null) {
                return new MediaInput.MediaDataSourceMediaInput(mediaDataSourceFor);
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }
}
