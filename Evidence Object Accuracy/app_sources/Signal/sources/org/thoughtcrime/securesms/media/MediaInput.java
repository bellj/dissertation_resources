package org.thoughtcrime.securesms.media;

import android.content.Context;
import android.media.MediaDataSource;
import android.media.MediaExtractor;
import android.net.Uri;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.Map;

/* loaded from: classes4.dex */
public abstract class MediaInput implements Closeable {
    public abstract MediaExtractor createExtractor() throws IOException;

    /* loaded from: classes4.dex */
    public static class FileMediaInput extends MediaInput {
        private final File file;

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
        }

        public FileMediaInput(File file) {
            this.file = file;
        }

        @Override // org.thoughtcrime.securesms.media.MediaInput
        public MediaExtractor createExtractor() throws IOException {
            MediaExtractor mediaExtractor = new MediaExtractor();
            mediaExtractor.setDataSource(this.file.getAbsolutePath());
            return mediaExtractor;
        }
    }

    /* loaded from: classes4.dex */
    public static class UriMediaInput extends MediaInput {
        private final Context context;
        private final Uri uri;

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
        }

        public UriMediaInput(Context context, Uri uri) {
            this.uri = uri;
            this.context = context;
        }

        @Override // org.thoughtcrime.securesms.media.MediaInput
        public MediaExtractor createExtractor() throws IOException {
            MediaExtractor mediaExtractor = new MediaExtractor();
            mediaExtractor.setDataSource(this.context, this.uri, (Map<String, String>) null);
            return mediaExtractor;
        }
    }

    /* loaded from: classes4.dex */
    public static class MediaDataSourceMediaInput extends MediaInput {
        private final MediaDataSource mediaDataSource;

        public MediaDataSourceMediaInput(MediaDataSource mediaDataSource) {
            this.mediaDataSource = mediaDataSource;
        }

        @Override // org.thoughtcrime.securesms.media.MediaInput
        public MediaExtractor createExtractor() throws IOException {
            MediaExtractor mediaExtractor = new MediaExtractor();
            mediaExtractor.setDataSource(this.mediaDataSource);
            return mediaExtractor;
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            this.mediaDataSource.close();
        }
    }
}
