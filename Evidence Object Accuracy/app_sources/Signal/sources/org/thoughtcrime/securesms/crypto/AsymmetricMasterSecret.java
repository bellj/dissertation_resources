package org.thoughtcrime.securesms.crypto;

import org.signal.libsignal.protocol.ecc.ECPrivateKey;
import org.signal.libsignal.protocol.ecc.ECPublicKey;

/* loaded from: classes4.dex */
public class AsymmetricMasterSecret {
    private final ECPrivateKey djbPrivateKey;
    private final ECPublicKey djbPublicKey;

    public AsymmetricMasterSecret(ECPublicKey eCPublicKey, ECPrivateKey eCPrivateKey) {
        this.djbPublicKey = eCPublicKey;
        this.djbPrivateKey = eCPrivateKey;
    }

    public ECPublicKey getDjbPublicKey() {
        return this.djbPublicKey;
    }

    public ECPrivateKey getPrivateKey() {
        return this.djbPrivateKey;
    }
}
