package org.thoughtcrime.securesms.crypto;

import android.util.Base64;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.IOException;
import org.thoughtcrime.securesms.util.JsonUtils;

/* loaded from: classes.dex */
public class AttachmentSecret {
    @JsonProperty
    @JsonDeserialize(using = ByteArrayDeserializer.class)
    @JsonSerialize(using = ByteArraySerializer.class)
    private byte[] classicCipherKey;
    @JsonProperty
    @JsonDeserialize(using = ByteArrayDeserializer.class)
    @JsonSerialize(using = ByteArraySerializer.class)
    private byte[] classicMacKey;
    @JsonProperty
    @JsonDeserialize(using = ByteArrayDeserializer.class)
    @JsonSerialize(using = ByteArraySerializer.class)
    private byte[] modernKey;

    public AttachmentSecret(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        this.classicCipherKey = bArr;
        this.classicMacKey = bArr2;
        this.modernKey = bArr3;
    }

    public AttachmentSecret() {
    }

    @JsonIgnore
    public byte[] getClassicCipherKey() {
        return this.classicCipherKey;
    }

    @JsonIgnore
    public byte[] getClassicMacKey() {
        return this.classicMacKey;
    }

    @JsonIgnore
    public byte[] getModernKey() {
        return this.modernKey;
    }

    @JsonIgnore
    public void setClassicCipherKey(byte[] bArr) {
        this.classicCipherKey = bArr;
    }

    @JsonIgnore
    public void setClassicMacKey(byte[] bArr) {
        this.classicMacKey = bArr;
    }

    public String serialize() {
        try {
            return JsonUtils.toJson(this);
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    public static AttachmentSecret fromString(String str) {
        try {
            return (AttachmentSecret) JsonUtils.fromJson(str, AttachmentSecret.class);
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    /* loaded from: classes4.dex */
    private static class ByteArraySerializer extends JsonSerializer<byte[]> {
        private ByteArraySerializer() {
        }

        public void serialize(byte[] bArr, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeString(Base64.encodeToString(bArr, 3));
        }
    }

    /* loaded from: classes4.dex */
    private static class ByteArrayDeserializer extends JsonDeserializer<byte[]> {
        private ByteArrayDeserializer() {
        }

        public byte[] deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            return Base64.decode(jsonParser.getValueAsString(), 3);
        }
    }
}
