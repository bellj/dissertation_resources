package org.thoughtcrime.securesms.crypto;

import android.content.Context;
import android.content.Intent;
import org.thoughtcrime.securesms.service.KeyCachingService;

/* loaded from: classes4.dex */
public class SecurityEvent {
    public static final String SECURITY_UPDATE_EVENT;

    public static void broadcastSecurityUpdateEvent(Context context) {
        Intent intent = new Intent(SECURITY_UPDATE_EVENT);
        intent.setPackage(context.getPackageName());
        context.sendBroadcast(intent, KeyCachingService.KEY_PERMISSION);
    }
}
