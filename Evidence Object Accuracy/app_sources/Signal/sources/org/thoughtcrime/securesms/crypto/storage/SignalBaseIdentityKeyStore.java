package org.thoughtcrime.securesms.crypto.storage;

import android.content.Context;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.state.IdentityKeyStore;
import org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment$getConfiguration$1$$ExternalSyntheticLambda0;
import org.thoughtcrime.securesms.crypto.storage.SignalIdentityKeyStore;
import org.thoughtcrime.securesms.database.IdentityDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.identity.IdentityRecordList;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.database.model.IdentityStoreRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.IdentityUtil;
import org.thoughtcrime.securesms.util.LRUCache;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes4.dex */
public class SignalBaseIdentityKeyStore {
    private static final Object LOCK = new Object();
    private static final String TAG = Log.tag(SignalBaseIdentityKeyStore.class);
    private static final int TIMESTAMP_THRESHOLD_SECONDS;
    private final Cache cache;
    private final Context context;

    public SignalBaseIdentityKeyStore(Context context) {
        this(context, SignalDatabase.identities());
    }

    SignalBaseIdentityKeyStore(Context context, IdentityDatabase identityDatabase) {
        this.context = context;
        this.cache = new Cache(identityDatabase);
    }

    public int getLocalRegistrationId() {
        return SignalStore.account().getRegistrationId();
    }

    public boolean saveIdentity(SignalProtocolAddress signalProtocolAddress, IdentityKey identityKey) {
        return saveIdentity(signalProtocolAddress, identityKey, false) == SignalIdentityKeyStore.SaveResult.UPDATE;
    }

    public SignalIdentityKeyStore.SaveResult saveIdentity(SignalProtocolAddress signalProtocolAddress, IdentityKey identityKey, boolean z) {
        IdentityDatabase.VerifiedStatus verifiedStatus;
        synchronized (LOCK) {
            IdentityStoreRecord identityStoreRecord = this.cache.get(signalProtocolAddress.getName());
            RecipientId fromSidOrE164 = RecipientId.fromSidOrE164(signalProtocolAddress.getName());
            if (identityStoreRecord == null) {
                Log.i(TAG, "Saving new identity for " + signalProtocolAddress);
                this.cache.save(signalProtocolAddress.getName(), fromSidOrE164, identityKey, IdentityDatabase.VerifiedStatus.DEFAULT, true, System.currentTimeMillis(), z);
                return SignalIdentityKeyStore.SaveResult.NEW;
            }
            boolean z2 = !identityStoreRecord.getIdentityKey().equals(identityKey);
            if (z2 && Recipient.self().getId().equals(fromSidOrE164) && Objects.equals(SignalStore.account().getAci(), ServiceId.parseOrNull(signalProtocolAddress.getName()))) {
                Log.w(TAG, "Received different identity key for self, ignoring | Existing: " + identityStoreRecord.getIdentityKey().hashCode() + ", New: " + identityKey.hashCode());
            } else if (z2) {
                Log.i(TAG, "Replacing existing identity for " + signalProtocolAddress + " | Existing: " + identityStoreRecord.getIdentityKey().hashCode() + ", New: " + identityKey.hashCode());
                if (!(identityStoreRecord.getVerifiedStatus() == IdentityDatabase.VerifiedStatus.VERIFIED || identityStoreRecord.getVerifiedStatus() == IdentityDatabase.VerifiedStatus.UNVERIFIED)) {
                    verifiedStatus = IdentityDatabase.VerifiedStatus.DEFAULT;
                    this.cache.save(signalProtocolAddress.getName(), fromSidOrE164, identityKey, verifiedStatus, false, System.currentTimeMillis(), z);
                    IdentityUtil.markIdentityUpdate(this.context, fromSidOrE164);
                    ApplicationDependencies.getProtocolStore().aci().sessions().archiveSiblingSessions(signalProtocolAddress);
                    SignalDatabase.senderKeyShared().deleteAllFor(fromSidOrE164);
                    return SignalIdentityKeyStore.SaveResult.UPDATE;
                }
                verifiedStatus = IdentityDatabase.VerifiedStatus.UNVERIFIED;
                this.cache.save(signalProtocolAddress.getName(), fromSidOrE164, identityKey, verifiedStatus, false, System.currentTimeMillis(), z);
                IdentityUtil.markIdentityUpdate(this.context, fromSidOrE164);
                ApplicationDependencies.getProtocolStore().aci().sessions().archiveSiblingSessions(signalProtocolAddress);
                SignalDatabase.senderKeyShared().deleteAllFor(fromSidOrE164);
                return SignalIdentityKeyStore.SaveResult.UPDATE;
            }
            if (isNonBlockingApprovalRequired(identityStoreRecord)) {
                Log.i(TAG, "Setting approval status for " + signalProtocolAddress);
                this.cache.setApproval(signalProtocolAddress.getName(), fromSidOrE164, identityStoreRecord, z);
                return SignalIdentityKeyStore.SaveResult.NON_BLOCKING_APPROVAL_REQUIRED;
            }
            return SignalIdentityKeyStore.SaveResult.NO_CHANGE;
        }
    }

    public void saveIdentityWithoutSideEffects(RecipientId recipientId, IdentityKey identityKey, IdentityDatabase.VerifiedStatus verifiedStatus, boolean z, long j, boolean z2) {
        Recipient resolved = Recipient.resolved(recipientId);
        if (resolved.hasServiceId()) {
            this.cache.save(resolved.requireServiceId().toString(), recipientId, identityKey, verifiedStatus, z, j, z2);
            return;
        }
        String str = TAG;
        Log.w(str, "[saveIdentity] No serviceId for " + resolved.getId(), new Throwable());
    }

    public boolean isTrustedIdentity(SignalProtocolAddress signalProtocolAddress, IdentityKey identityKey, IdentityKeyStore.Direction direction) {
        Recipient self = Recipient.self();
        if (signalProtocolAddress.getName().equals(self.requireServiceId().toString()) || signalProtocolAddress.getName().equals(self.requireE164())) {
            return identityKey.equals(SignalStore.account().getAciIdentityKey().getPublicKey());
        }
        IdentityStoreRecord identityStoreRecord = this.cache.get(signalProtocolAddress.getName());
        int i = AnonymousClass1.$SwitchMap$org$signal$libsignal$protocol$state$IdentityKeyStore$Direction[direction.ordinal()];
        if (i == 1) {
            return isTrustedForSending(identityKey, identityStoreRecord);
        }
        if (i == 2) {
            return true;
        }
        throw new AssertionError("Unknown direction: " + direction);
    }

    /* renamed from: org.thoughtcrime.securesms.crypto.storage.SignalBaseIdentityKeyStore$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$signal$libsignal$protocol$state$IdentityKeyStore$Direction;

        static {
            int[] iArr = new int[IdentityKeyStore.Direction.values().length];
            $SwitchMap$org$signal$libsignal$protocol$state$IdentityKeyStore$Direction = iArr;
            try {
                iArr[IdentityKeyStore.Direction.SENDING.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$signal$libsignal$protocol$state$IdentityKeyStore$Direction[IdentityKeyStore.Direction.RECEIVING.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    public IdentityKey getIdentity(SignalProtocolAddress signalProtocolAddress) {
        IdentityStoreRecord identityStoreRecord = this.cache.get(signalProtocolAddress.getName());
        if (identityStoreRecord != null) {
            return identityStoreRecord.getIdentityKey();
        }
        return null;
    }

    public Optional<IdentityRecord> getIdentityRecord(RecipientId recipientId) {
        return getIdentityRecord(Recipient.resolved(recipientId));
    }

    public Optional<IdentityRecord> getIdentityRecord(Recipient recipient) {
        if (recipient.hasServiceId()) {
            return Optional.ofNullable(this.cache.get(recipient.requireServiceId().toString())).map(new Function() { // from class: org.thoughtcrime.securesms.crypto.storage.SignalBaseIdentityKeyStore$$ExternalSyntheticLambda0
                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return SignalBaseIdentityKeyStore.m1655$r8$lambda$TbEQi9Tz65bT4wCErj43mxYy8(Recipient.this, (IdentityStoreRecord) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            });
        }
        if (recipient.isRegistered()) {
            String str = TAG;
            Log.w(str, "[getIdentityRecord] No ServiceId for registered user " + recipient.getId(), new Throwable());
        } else {
            String str2 = TAG;
            Log.d(str2, "[getIdentityRecord] No ServiceId for unregistered user " + recipient.getId());
        }
        return Optional.empty();
    }

    public static /* synthetic */ IdentityRecord lambda$getIdentityRecord$0(Recipient recipient, IdentityStoreRecord identityStoreRecord) {
        return identityStoreRecord.toIdentityRecord(recipient.getId());
    }

    public IdentityRecordList getIdentityRecords(List<Recipient> list) {
        if (((List) Collection$EL.stream(list).filter(new SignalBaseIdentityKeyStore$$ExternalSyntheticLambda1()).map(new SignalBaseIdentityKeyStore$$ExternalSyntheticLambda2()).map(new InternalConversationSettingsFragment$getConfiguration$1$$ExternalSyntheticLambda0()).collect(Collectors.toList())).isEmpty()) {
            return IdentityRecordList.EMPTY;
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (Recipient recipient : list) {
            if (recipient.hasServiceId()) {
                IdentityStoreRecord identityStoreRecord = this.cache.get(recipient.requireServiceId().toString());
                if (identityStoreRecord != null) {
                    arrayList.add(identityStoreRecord.toIdentityRecord(recipient.getId()));
                }
            } else if (recipient.isRegistered()) {
                String str = TAG;
                Log.w(str, "[getIdentityRecords] No serviceId for registered user " + recipient.getId(), new Throwable());
            } else {
                String str2 = TAG;
                Log.d(str2, "[getIdentityRecords] No serviceId for unregistered user " + recipient.getId());
            }
        }
        return new IdentityRecordList(arrayList);
    }

    public void setApproval(RecipientId recipientId, boolean z) {
        Recipient resolved = Recipient.resolved(recipientId);
        if (resolved.hasServiceId()) {
            this.cache.setApproval(resolved.requireServiceId().toString(), recipientId, z);
            return;
        }
        String str = TAG;
        Log.w(str, "[setApproval] No serviceId for " + resolved.getId(), new Throwable());
    }

    public void setVerified(RecipientId recipientId, IdentityKey identityKey, IdentityDatabase.VerifiedStatus verifiedStatus) {
        Recipient resolved = Recipient.resolved(recipientId);
        if (resolved.hasServiceId()) {
            this.cache.setVerified(resolved.requireServiceId().toString(), recipientId, identityKey, verifiedStatus);
            return;
        }
        String str = TAG;
        Log.w(str, "[setVerified] No serviceId for " + resolved.getId(), new Throwable());
    }

    public void delete(String str) {
        this.cache.delete(str);
    }

    public void invalidate(String str) {
        this.cache.invalidate(str);
    }

    private boolean isTrustedForSending(IdentityKey identityKey, IdentityStoreRecord identityStoreRecord) {
        if (identityStoreRecord == null) {
            Log.w(TAG, "Nothing here, returning true...");
            return true;
        } else if (!identityKey.equals(identityStoreRecord.getIdentityKey())) {
            String str = TAG;
            Log.w(str, "Identity keys don't match... service: " + identityKey.hashCode() + " database: " + identityStoreRecord.getIdentityKey().hashCode());
            return false;
        } else if (identityStoreRecord.getVerifiedStatus() == IdentityDatabase.VerifiedStatus.UNVERIFIED) {
            Log.w(TAG, "Needs unverified approval!");
            return false;
        } else if (!isNonBlockingApprovalRequired(identityStoreRecord)) {
            return true;
        } else {
            Log.w(TAG, "Needs non-blocking approval!");
            return false;
        }
    }

    private boolean isNonBlockingApprovalRequired(IdentityStoreRecord identityStoreRecord) {
        return !identityStoreRecord.getFirstUse() && !identityStoreRecord.getNonblockingApproval() && System.currentTimeMillis() - identityStoreRecord.getTimestamp() < TimeUnit.SECONDS.toMillis(5);
    }

    /* loaded from: classes4.dex */
    public static final class Cache {
        private final Map<String, IdentityStoreRecord> cache = new LRUCache(200);
        private final IdentityDatabase identityDatabase;

        Cache(IdentityDatabase identityDatabase) {
            this.identityDatabase = identityDatabase;
        }

        public IdentityStoreRecord get(String str) {
            synchronized (this) {
                if (this.cache.containsKey(str)) {
                    return this.cache.get(str);
                }
                IdentityStoreRecord identityStoreRecord = this.identityDatabase.getIdentityStoreRecord(str);
                this.cache.put(str, identityStoreRecord);
                return identityStoreRecord;
            }
        }

        public void save(String str, RecipientId recipientId, IdentityKey identityKey, IdentityDatabase.VerifiedStatus verifiedStatus, boolean z, long j, boolean z2) {
            withWriteLock(new SignalBaseIdentityKeyStore$Cache$$ExternalSyntheticLambda3(this, str, recipientId, identityKey, verifiedStatus, z, j, z2));
        }

        public /* synthetic */ void lambda$save$0(String str, RecipientId recipientId, IdentityKey identityKey, IdentityDatabase.VerifiedStatus verifiedStatus, boolean z, long j, boolean z2) {
            this.identityDatabase.saveIdentity(str, recipientId, identityKey, verifiedStatus, z, j, z2);
            this.cache.put(str, new IdentityStoreRecord(str, identityKey, verifiedStatus, z, j, z2));
        }

        public void setApproval(String str, RecipientId recipientId, boolean z) {
            setApproval(str, recipientId, this.cache.get(str), z);
        }

        public void setApproval(String str, RecipientId recipientId, IdentityStoreRecord identityStoreRecord, boolean z) {
            withWriteLock(new SignalBaseIdentityKeyStore$Cache$$ExternalSyntheticLambda2(this, str, recipientId, z, identityStoreRecord));
        }

        public /* synthetic */ void lambda$setApproval$1(String str, RecipientId recipientId, boolean z, IdentityStoreRecord identityStoreRecord) {
            this.identityDatabase.setApproval(str, recipientId, z);
            if (identityStoreRecord != null) {
                this.cache.put(identityStoreRecord.getAddressName(), new IdentityStoreRecord(identityStoreRecord.getAddressName(), identityStoreRecord.getIdentityKey(), identityStoreRecord.getVerifiedStatus(), identityStoreRecord.getFirstUse(), identityStoreRecord.getTimestamp(), z));
            }
        }

        public void setVerified(String str, RecipientId recipientId, IdentityKey identityKey, IdentityDatabase.VerifiedStatus verifiedStatus) {
            withWriteLock(new SignalBaseIdentityKeyStore$Cache$$ExternalSyntheticLambda1(this, str, recipientId, identityKey, verifiedStatus));
        }

        public /* synthetic */ void lambda$setVerified$2(String str, RecipientId recipientId, IdentityKey identityKey, IdentityDatabase.VerifiedStatus verifiedStatus) {
            this.identityDatabase.setVerified(str, recipientId, identityKey, verifiedStatus);
            IdentityStoreRecord identityStoreRecord = this.cache.get(str);
            if (identityStoreRecord != null) {
                this.cache.put(str, new IdentityStoreRecord(identityStoreRecord.getAddressName(), identityStoreRecord.getIdentityKey(), verifiedStatus, identityStoreRecord.getFirstUse(), identityStoreRecord.getTimestamp(), identityStoreRecord.getNonblockingApproval()));
            }
        }

        public void delete(String str) {
            withWriteLock(new SignalBaseIdentityKeyStore$Cache$$ExternalSyntheticLambda0(this, str));
        }

        public /* synthetic */ void lambda$delete$3(String str) {
            this.identityDatabase.delete(str);
            this.cache.remove(str);
        }

        public synchronized void invalidate(String str) {
            synchronized (this) {
                this.cache.remove(str);
            }
        }

        private void withWriteLock(Runnable runnable) {
            SQLiteDatabase rawDatabase = SignalDatabase.getRawDatabase();
            rawDatabase.beginTransaction();
            try {
                synchronized (this) {
                    runnable.run();
                }
                rawDatabase.setTransactionSuccessful();
            } finally {
                rawDatabase.endTransaction();
            }
        }
    }
}
