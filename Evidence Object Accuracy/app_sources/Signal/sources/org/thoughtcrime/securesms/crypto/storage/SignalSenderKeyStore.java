package org.thoughtcrime.securesms.crypto.storage;

import android.content.Context;
import java.util.Collection;
import java.util.Set;
import java.util.UUID;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.groups.state.SenderKeyRecord;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.whispersystems.signalservice.api.SignalServiceSenderKeyStore;
import org.whispersystems.signalservice.api.push.DistributionId;

/* loaded from: classes4.dex */
public final class SignalSenderKeyStore implements SignalServiceSenderKeyStore {
    private static final Object LOCK = new Object();
    private final Context context;

    public SignalSenderKeyStore(Context context) {
        this.context = context;
    }

    @Override // org.signal.libsignal.protocol.groups.state.SenderKeyStore
    public void storeSenderKey(SignalProtocolAddress signalProtocolAddress, UUID uuid, SenderKeyRecord senderKeyRecord) {
        synchronized (LOCK) {
            SignalDatabase.senderKeys().store(signalProtocolAddress, DistributionId.from(uuid), senderKeyRecord);
        }
    }

    @Override // org.signal.libsignal.protocol.groups.state.SenderKeyStore
    public SenderKeyRecord loadSenderKey(SignalProtocolAddress signalProtocolAddress, UUID uuid) {
        SenderKeyRecord load;
        synchronized (LOCK) {
            load = SignalDatabase.senderKeys().load(signalProtocolAddress, DistributionId.from(uuid));
        }
        return load;
    }

    @Override // org.whispersystems.signalservice.api.SignalServiceSenderKeyStore
    public Set<SignalProtocolAddress> getSenderKeySharedWith(DistributionId distributionId) {
        Set<SignalProtocolAddress> sharedWith;
        synchronized (LOCK) {
            sharedWith = SignalDatabase.senderKeyShared().getSharedWith(distributionId);
        }
        return sharedWith;
    }

    @Override // org.whispersystems.signalservice.api.SignalServiceSenderKeyStore
    public void markSenderKeySharedWith(DistributionId distributionId, Collection<SignalProtocolAddress> collection) {
        synchronized (LOCK) {
            SignalDatabase.senderKeyShared().markAsShared(distributionId, collection);
        }
    }

    @Override // org.whispersystems.signalservice.api.SignalServiceSenderKeyStore
    public void clearSenderKeySharedWith(Collection<SignalProtocolAddress> collection) {
        synchronized (LOCK) {
            SignalDatabase.senderKeyShared().deleteAllFor(collection);
        }
    }

    public void deleteAllFor(String str, DistributionId distributionId) {
        synchronized (LOCK) {
            SignalDatabase.senderKeys().deleteAllFor(str, distributionId);
        }
    }

    public void deleteAll() {
        synchronized (LOCK) {
            SignalDatabase.senderKeys().deleteAll();
        }
    }
}
