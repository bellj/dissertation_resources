package org.thoughtcrime.securesms.crypto.storage;

import android.content.Context;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.IdentityKeyPair;
import org.signal.libsignal.protocol.InvalidKeyIdException;
import org.signal.libsignal.protocol.NoSessionException;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.groups.state.SenderKeyRecord;
import org.signal.libsignal.protocol.state.IdentityKeyStore;
import org.signal.libsignal.protocol.state.PreKeyRecord;
import org.signal.libsignal.protocol.state.SessionRecord;
import org.signal.libsignal.protocol.state.SignedPreKeyRecord;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.SignalServiceAccountDataStore;
import org.whispersystems.signalservice.api.push.DistributionId;

/* loaded from: classes4.dex */
public class SignalServiceAccountDataStoreImpl implements SignalServiceAccountDataStore {
    private final Context context;
    private final SignalIdentityKeyStore identityKeyStore;
    private final TextSecurePreKeyStore preKeyStore;
    private final SignalSenderKeyStore senderKeyStore;
    private final TextSecureSessionStore sessionStore;
    private final TextSecurePreKeyStore signedPreKeyStore;

    public SignalServiceAccountDataStoreImpl(Context context, TextSecurePreKeyStore textSecurePreKeyStore, SignalIdentityKeyStore signalIdentityKeyStore, TextSecureSessionStore textSecureSessionStore, SignalSenderKeyStore signalSenderKeyStore) {
        this.context = context;
        this.preKeyStore = textSecurePreKeyStore;
        this.signedPreKeyStore = textSecurePreKeyStore;
        this.identityKeyStore = signalIdentityKeyStore;
        this.sessionStore = textSecureSessionStore;
        this.senderKeyStore = signalSenderKeyStore;
    }

    @Override // org.whispersystems.signalservice.api.SignalServiceAccountDataStore
    public boolean isMultiDevice() {
        return TextSecurePreferences.isMultiDevice(this.context);
    }

    @Override // org.signal.libsignal.protocol.state.IdentityKeyStore
    public IdentityKeyPair getIdentityKeyPair() {
        return this.identityKeyStore.getIdentityKeyPair();
    }

    @Override // org.signal.libsignal.protocol.state.IdentityKeyStore
    public int getLocalRegistrationId() {
        return this.identityKeyStore.getLocalRegistrationId();
    }

    @Override // org.signal.libsignal.protocol.state.IdentityKeyStore
    public boolean saveIdentity(SignalProtocolAddress signalProtocolAddress, IdentityKey identityKey) {
        return this.identityKeyStore.saveIdentity(signalProtocolAddress, identityKey);
    }

    @Override // org.signal.libsignal.protocol.state.IdentityKeyStore
    public boolean isTrustedIdentity(SignalProtocolAddress signalProtocolAddress, IdentityKey identityKey, IdentityKeyStore.Direction direction) {
        return this.identityKeyStore.isTrustedIdentity(signalProtocolAddress, identityKey, direction);
    }

    @Override // org.signal.libsignal.protocol.state.IdentityKeyStore
    public IdentityKey getIdentity(SignalProtocolAddress signalProtocolAddress) {
        return this.identityKeyStore.getIdentity(signalProtocolAddress);
    }

    @Override // org.signal.libsignal.protocol.state.PreKeyStore
    public PreKeyRecord loadPreKey(int i) throws InvalidKeyIdException {
        return this.preKeyStore.loadPreKey(i);
    }

    @Override // org.signal.libsignal.protocol.state.PreKeyStore
    public void storePreKey(int i, PreKeyRecord preKeyRecord) {
        this.preKeyStore.storePreKey(i, preKeyRecord);
    }

    @Override // org.signal.libsignal.protocol.state.PreKeyStore
    public boolean containsPreKey(int i) {
        return this.preKeyStore.containsPreKey(i);
    }

    @Override // org.signal.libsignal.protocol.state.PreKeyStore
    public void removePreKey(int i) {
        this.preKeyStore.removePreKey(i);
    }

    @Override // org.signal.libsignal.protocol.state.SessionStore
    public SessionRecord loadSession(SignalProtocolAddress signalProtocolAddress) {
        return this.sessionStore.loadSession(signalProtocolAddress);
    }

    @Override // org.signal.libsignal.protocol.state.SessionStore
    public List<SessionRecord> loadExistingSessions(List<SignalProtocolAddress> list) throws NoSessionException {
        return this.sessionStore.loadExistingSessions(list);
    }

    @Override // org.signal.libsignal.protocol.state.SessionStore
    public List<Integer> getSubDeviceSessions(String str) {
        return this.sessionStore.getSubDeviceSessions(str);
    }

    @Override // org.whispersystems.signalservice.api.SignalServiceSessionStore
    public Set<SignalProtocolAddress> getAllAddressesWithActiveSessions(List<String> list) {
        return this.sessionStore.getAllAddressesWithActiveSessions(list);
    }

    @Override // org.signal.libsignal.protocol.state.SessionStore
    public void storeSession(SignalProtocolAddress signalProtocolAddress, SessionRecord sessionRecord) {
        this.sessionStore.storeSession(signalProtocolAddress, sessionRecord);
    }

    @Override // org.signal.libsignal.protocol.state.SessionStore
    public boolean containsSession(SignalProtocolAddress signalProtocolAddress) {
        return this.sessionStore.containsSession(signalProtocolAddress);
    }

    @Override // org.signal.libsignal.protocol.state.SessionStore
    public void deleteSession(SignalProtocolAddress signalProtocolAddress) {
        this.sessionStore.deleteSession(signalProtocolAddress);
    }

    @Override // org.signal.libsignal.protocol.state.SessionStore
    public void deleteAllSessions(String str) {
        this.sessionStore.deleteAllSessions(str);
    }

    @Override // org.whispersystems.signalservice.api.SignalServiceSessionStore
    public void archiveSession(SignalProtocolAddress signalProtocolAddress) {
        this.sessionStore.archiveSession(signalProtocolAddress);
        this.senderKeyStore.clearSenderKeySharedWith(Collections.singleton(signalProtocolAddress));
    }

    @Override // org.signal.libsignal.protocol.state.SignedPreKeyStore
    public SignedPreKeyRecord loadSignedPreKey(int i) throws InvalidKeyIdException {
        return this.signedPreKeyStore.loadSignedPreKey(i);
    }

    @Override // org.signal.libsignal.protocol.state.SignedPreKeyStore
    public List<SignedPreKeyRecord> loadSignedPreKeys() {
        return this.signedPreKeyStore.loadSignedPreKeys();
    }

    @Override // org.signal.libsignal.protocol.state.SignedPreKeyStore
    public void storeSignedPreKey(int i, SignedPreKeyRecord signedPreKeyRecord) {
        this.signedPreKeyStore.storeSignedPreKey(i, signedPreKeyRecord);
    }

    @Override // org.signal.libsignal.protocol.state.SignedPreKeyStore
    public boolean containsSignedPreKey(int i) {
        return this.signedPreKeyStore.containsSignedPreKey(i);
    }

    @Override // org.signal.libsignal.protocol.state.SignedPreKeyStore
    public void removeSignedPreKey(int i) {
        this.signedPreKeyStore.removeSignedPreKey(i);
    }

    @Override // org.signal.libsignal.protocol.groups.state.SenderKeyStore
    public void storeSenderKey(SignalProtocolAddress signalProtocolAddress, UUID uuid, SenderKeyRecord senderKeyRecord) {
        this.senderKeyStore.storeSenderKey(signalProtocolAddress, uuid, senderKeyRecord);
    }

    @Override // org.signal.libsignal.protocol.groups.state.SenderKeyStore
    public SenderKeyRecord loadSenderKey(SignalProtocolAddress signalProtocolAddress, UUID uuid) {
        return this.senderKeyStore.loadSenderKey(signalProtocolAddress, uuid);
    }

    @Override // org.whispersystems.signalservice.api.SignalServiceSenderKeyStore
    public Set<SignalProtocolAddress> getSenderKeySharedWith(DistributionId distributionId) {
        return this.senderKeyStore.getSenderKeySharedWith(distributionId);
    }

    @Override // org.whispersystems.signalservice.api.SignalServiceSenderKeyStore
    public void markSenderKeySharedWith(DistributionId distributionId, Collection<SignalProtocolAddress> collection) {
        this.senderKeyStore.markSenderKeySharedWith(distributionId, collection);
    }

    @Override // org.whispersystems.signalservice.api.SignalServiceSenderKeyStore
    public void clearSenderKeySharedWith(Collection<SignalProtocolAddress> collection) {
        this.senderKeyStore.clearSenderKeySharedWith(collection);
    }

    public SignalIdentityKeyStore identities() {
        return this.identityKeyStore;
    }

    public TextSecurePreKeyStore preKeys() {
        return this.preKeyStore;
    }

    public TextSecureSessionStore sessions() {
        return this.sessionStore;
    }

    public SignalSenderKeyStore senderKeys() {
        return this.senderKeyStore;
    }
}
