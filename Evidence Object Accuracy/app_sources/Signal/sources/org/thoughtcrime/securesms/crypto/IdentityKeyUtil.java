package org.thoughtcrime.securesms.crypto;

import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.IdentityKeyPair;
import org.signal.libsignal.protocol.ecc.Curve;
import org.signal.libsignal.protocol.ecc.ECKeyPair;

/* loaded from: classes4.dex */
public class IdentityKeyUtil {
    public static IdentityKeyPair generateIdentityKeyPair() {
        ECKeyPair generateKeyPair = Curve.generateKeyPair();
        return new IdentityKeyPair(new IdentityKey(generateKeyPair.getPublicKey()), generateKeyPair.getPrivateKey());
    }
}
