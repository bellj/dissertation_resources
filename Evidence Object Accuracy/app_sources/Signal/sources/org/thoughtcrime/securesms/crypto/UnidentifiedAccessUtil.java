package org.thoughtcrime.securesms.crypto;

import android.content.Context;
import com.annimon.stream.Stream;
import j$.util.Optional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.metadata.certificate.CertificateValidator;
import org.signal.libsignal.metadata.certificate.InvalidCertificateException;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.ecc.Curve;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.keyvalue.CertificateType;
import org.thoughtcrime.securesms.keyvalue.PhoneNumberPrivacyValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.crypto.UnidentifiedAccess;
import org.whispersystems.signalservice.api.crypto.UnidentifiedAccessPair;

/* loaded from: classes4.dex */
public class UnidentifiedAccessUtil {
    private static final String TAG = Log.tag(UnidentifiedAccessUtil.class);
    private static final byte[] UNRESTRICTED_KEY = new byte[16];

    public static CertificateValidator getCertificateValidator() {
        try {
            return new CertificateValidator(Curve.decodePoint(Base64.decode(BuildConfig.UNIDENTIFIED_SENDER_TRUST_ROOT), 0));
        } catch (IOException | InvalidKeyException e) {
            throw new AssertionError(e);
        }
    }

    public static Optional<UnidentifiedAccessPair> getAccessFor(Context context, Recipient recipient) {
        return getAccessFor(context, recipient, true);
    }

    public static Optional<UnidentifiedAccessPair> getAccessFor(Context context, Recipient recipient, boolean z) {
        return getAccessFor(context, Collections.singletonList(recipient), z).get(0);
    }

    public static List<Optional<UnidentifiedAccessPair>> getAccessFor(Context context, List<Recipient> list) {
        return getAccessFor(context, list, true);
    }

    public static Map<RecipientId, Optional<UnidentifiedAccessPair>> getAccessMapFor(Context context, List<Recipient> list) {
        List<Optional<UnidentifiedAccessPair>> accessFor = getAccessFor(context, list, true);
        Iterator<Optional<UnidentifiedAccessPair>> it = accessFor.iterator();
        HashMap hashMap = new HashMap(list.size());
        for (Recipient recipient : list) {
            hashMap.put(recipient.getId(), it.next());
        }
        return hashMap;
    }

    public static List<Optional<UnidentifiedAccessPair>> getAccessFor(Context context, List<Recipient> list, boolean z) {
        byte[] deriveAccessKeyFrom = UnidentifiedAccess.deriveAccessKeyFrom(ProfileKeyUtil.getSelfProfileKey());
        if (TextSecurePreferences.isUniversalUnidentifiedAccess(context)) {
            deriveAccessKeyFrom = UNRESTRICTED_KEY;
        }
        ArrayList arrayList = new ArrayList(list.size());
        HashMap hashMap = new HashMap();
        for (Recipient recipient : list) {
            byte[] targetUnidentifiedAccessKey = getTargetUnidentifiedAccessKey(recipient);
            CertificateType unidentifiedAccessCertificateType = getUnidentifiedAccessCertificateType(recipient);
            byte[] unidentifiedAccessCertificate = SignalStore.certificateValues().getUnidentifiedAccessCertificate(unidentifiedAccessCertificateType);
            hashMap.put(unidentifiedAccessCertificateType, Integer.valueOf(((Integer) Util.getOrDefault(hashMap, unidentifiedAccessCertificateType, 0)).intValue() + 1));
            if (targetUnidentifiedAccessKey == null || unidentifiedAccessCertificate == null) {
                arrayList.add(Optional.empty());
            } else {
                try {
                    arrayList.add(Optional.of(new UnidentifiedAccessPair(new UnidentifiedAccess(targetUnidentifiedAccessKey, unidentifiedAccessCertificate), new UnidentifiedAccess(deriveAccessKeyFrom, unidentifiedAccessCertificate))));
                } catch (InvalidCertificateException e) {
                    Log.w(TAG, e);
                    arrayList.add(Optional.empty());
                }
            }
        }
        int size = Stream.of(arrayList).filter(new UnidentifiedAccessUtil$$ExternalSyntheticLambda0()).toList().size();
        int size2 = arrayList.size() - size;
        if (z) {
            String str = TAG;
            Log.i(str, "Unidentified: " + size + ", Other: " + size2 + ". Types: " + hashMap);
        }
        return arrayList;
    }

    public static Optional<UnidentifiedAccessPair> getAccessForSync(Context context) {
        try {
            byte[] deriveAccessKeyFrom = UnidentifiedAccess.deriveAccessKeyFrom(ProfileKeyUtil.getSelfProfileKey());
            byte[] unidentifiedAccessCertificate = getUnidentifiedAccessCertificate(Recipient.self());
            if (TextSecurePreferences.isUniversalUnidentifiedAccess(context)) {
                deriveAccessKeyFrom = UNRESTRICTED_KEY;
            }
            if (unidentifiedAccessCertificate != null) {
                return Optional.of(new UnidentifiedAccessPair(new UnidentifiedAccess(deriveAccessKeyFrom, unidentifiedAccessCertificate), new UnidentifiedAccess(deriveAccessKeyFrom, unidentifiedAccessCertificate)));
            }
            return Optional.empty();
        } catch (InvalidCertificateException e) {
            Log.w(TAG, e);
            return Optional.empty();
        }
    }

    private static CertificateType getUnidentifiedAccessCertificateType(Recipient recipient) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$keyvalue$PhoneNumberPrivacyValues$PhoneNumberSharingMode[SignalStore.phoneNumberPrivacy().getPhoneNumberSharingMode().ordinal()];
        if (i == 1) {
            return CertificateType.UUID_AND_E164;
        }
        if (i == 2) {
            return recipient.isSystemContact() ? CertificateType.UUID_AND_E164 : CertificateType.UUID_ONLY;
        }
        if (i == 3) {
            return CertificateType.UUID_ONLY;
        }
        throw new AssertionError();
    }

    private static byte[] getUnidentifiedAccessCertificate(Recipient recipient) {
        return SignalStore.certificateValues().getUnidentifiedAccessCertificate(getUnidentifiedAccessCertificateType(recipient));
    }

    private static byte[] getTargetUnidentifiedAccessKey(Recipient recipient) {
        ProfileKey profileKeyOrNull = ProfileKeyUtil.profileKeyOrNull(recipient.resolve().getProfileKey());
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$database$RecipientDatabase$UnidentifiedAccessMode[recipient.resolve().getUnidentifiedAccessMode().ordinal()];
        if (i != 1) {
            if (i == 2) {
                return null;
            }
            if (i != 3) {
                if (i == 4) {
                    return UNRESTRICTED_KEY;
                }
                throw new AssertionError("Unknown mode: " + recipient.getUnidentifiedAccessMode().getMode());
            } else if (profileKeyOrNull == null) {
                return null;
            } else {
                return UnidentifiedAccess.deriveAccessKeyFrom(profileKeyOrNull);
            }
        } else if (profileKeyOrNull == null) {
            return UNRESTRICTED_KEY;
        } else {
            return UnidentifiedAccess.deriveAccessKeyFrom(profileKeyOrNull);
        }
    }

    /* renamed from: org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$database$RecipientDatabase$UnidentifiedAccessMode;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$keyvalue$PhoneNumberPrivacyValues$PhoneNumberSharingMode;

        static {
            int[] iArr = new int[RecipientDatabase.UnidentifiedAccessMode.values().length];
            $SwitchMap$org$thoughtcrime$securesms$database$RecipientDatabase$UnidentifiedAccessMode = iArr;
            try {
                iArr[RecipientDatabase.UnidentifiedAccessMode.UNKNOWN.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$RecipientDatabase$UnidentifiedAccessMode[RecipientDatabase.UnidentifiedAccessMode.DISABLED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$RecipientDatabase$UnidentifiedAccessMode[RecipientDatabase.UnidentifiedAccessMode.ENABLED.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$RecipientDatabase$UnidentifiedAccessMode[RecipientDatabase.UnidentifiedAccessMode.UNRESTRICTED.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            int[] iArr2 = new int[PhoneNumberPrivacyValues.PhoneNumberSharingMode.values().length];
            $SwitchMap$org$thoughtcrime$securesms$keyvalue$PhoneNumberPrivacyValues$PhoneNumberSharingMode = iArr2;
            try {
                iArr2[PhoneNumberPrivacyValues.PhoneNumberSharingMode.EVERYONE.ordinal()] = 1;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$keyvalue$PhoneNumberPrivacyValues$PhoneNumberSharingMode[PhoneNumberPrivacyValues.PhoneNumberSharingMode.CONTACTS.ordinal()] = 2;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$keyvalue$PhoneNumberPrivacyValues$PhoneNumberSharingMode[PhoneNumberPrivacyValues.PhoneNumberSharingMode.NOBODY.ordinal()] = 3;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }
}
