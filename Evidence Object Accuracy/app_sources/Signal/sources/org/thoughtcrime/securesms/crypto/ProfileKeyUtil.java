package org.thoughtcrime.securesms.crypto;

import j$.util.Optional;
import java.io.IOException;
import java.util.Locale;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes.dex */
public final class ProfileKeyUtil {
    private static final String TAG = Log.tag(ProfileKeyUtil.class);

    private ProfileKeyUtil() {
    }

    public static synchronized ProfileKey getSelfProfileKey() {
        ProfileKey profileKey;
        synchronized (ProfileKeyUtil.class) {
            try {
                profileKey = new ProfileKey(Recipient.self().getProfileKey());
            } catch (InvalidInputException e) {
                throw new AssertionError(e);
            }
        }
        return profileKey;
    }

    public static ProfileKey profileKeyOrNull(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        try {
            return new ProfileKey(bArr);
        } catch (InvalidInputException e) {
            Log.w(TAG, String.format(Locale.US, "Seen non-null profile key of wrong length %d", Integer.valueOf(bArr.length)), e);
            return null;
        }
    }

    public static ProfileKey profileKeyOrNull(String str) {
        if (str == null) {
            return null;
        }
        try {
            byte[] decode = Base64.decode(str);
            try {
                return new ProfileKey(decode);
            } catch (InvalidInputException e) {
                Log.w(TAG, String.format(Locale.US, "Seen non-null profile key of wrong length %d", Integer.valueOf(decode.length)), e);
                return null;
            }
        } catch (IOException unused) {
            Log.w(TAG, "Failed to decode profile key.");
            return null;
        }
    }

    public static ProfileKey profileKeyOrThrow(byte[] bArr) {
        try {
            return new ProfileKey(bArr);
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }

    public static Optional<ProfileKey> profileKeyOptional(byte[] bArr) {
        return Optional.ofNullable(profileKeyOrNull(bArr));
    }

    public static Optional<ProfileKey> profileKeyOptionalOrThrow(byte[] bArr) {
        return Optional.of(profileKeyOrThrow(bArr));
    }

    public static ProfileKey createNew() {
        try {
            return new ProfileKey(Util.getSecretBytes(32));
        } catch (InvalidInputException e) {
            throw new AssertionError(e);
        }
    }
}
