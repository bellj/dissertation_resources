package org.thoughtcrime.securesms.crypto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.signal.core.util.Conversions;
import org.signal.core.util.Hex;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.ecc.Curve;
import org.signal.libsignal.protocol.ecc.ECPublicKey;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class PublicKey {
    public static final int KEY_SIZE;
    private static final String TAG = Log.tag(PublicKey.class);
    private int id;
    private final ECPublicKey publicKey;

    public PublicKey(PublicKey publicKey) {
        this.id = publicKey.id;
        this.publicKey = publicKey.publicKey;
    }

    public PublicKey(int i, ECPublicKey eCPublicKey) {
        this.publicKey = eCPublicKey;
        this.id = i;
    }

    public PublicKey(byte[] bArr, int i) throws InvalidKeyException {
        String str = TAG;
        Log.i(str, "PublicKey Length: " + (bArr.length - i));
        if (bArr.length - i >= 36) {
            this.id = Conversions.byteArrayToMedium(bArr, i);
            this.publicKey = Curve.decodePoint(bArr, i + 3);
            return;
        }
        throw new InvalidKeyException("Provided bytes are too short.");
    }

    public PublicKey(byte[] bArr) throws InvalidKeyException {
        this(bArr, 0);
    }

    public int getType() {
        return this.publicKey.getType();
    }

    public void setId(int i) {
        this.id = i;
    }

    public int getId() {
        return this.id;
    }

    public ECPublicKey getKey() {
        return this.publicKey;
    }

    public String getFingerprint() {
        return Hex.toString(getFingerprintBytes());
    }

    public byte[] getFingerprintBytes() {
        try {
            return MessageDigest.getInstance("SHA-1").digest(serialize());
        } catch (NoSuchAlgorithmException e) {
            Log.w(TAG, "LocalKeyPair", e);
            throw new IllegalArgumentException("SHA-1 isn't supported!");
        }
    }

    public byte[] serialize() {
        byte[] mediumToByteArray = Conversions.mediumToByteArray(this.id);
        byte[] serialize = this.publicKey.serialize();
        String str = TAG;
        Log.i(str, "Serializing public key point: " + Hex.toString(serialize));
        return Util.combine(mediumToByteArray, serialize);
    }
}
