package org.thoughtcrime.securesms.crypto;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.signal.core.util.Hex;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.ecc.Curve;
import org.signal.libsignal.protocol.ecc.ECPrivateKey;
import org.thoughtcrime.securesms.util.Base64;

/* loaded from: classes4.dex */
public class MasterCipher {
    private static final String TAG = Log.tag(MasterCipher.class);
    private final Cipher decryptingCipher;
    private final Cipher encryptingCipher;
    private final Mac hmac;
    private final MasterSecret masterSecret;

    public MasterCipher(MasterSecret masterSecret) {
        try {
            this.masterSecret = masterSecret;
            this.encryptingCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.decryptingCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.hmac = Mac.getInstance("HmacSHA1");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new AssertionError(e);
        }
    }

    public byte[] encryptKey(ECPrivateKey eCPrivateKey) {
        return encryptBytes(eCPrivateKey.serialize());
    }

    public String encryptBody(String str) {
        return encryptAndEncodeBytes(str.getBytes());
    }

    public String decryptBody(String str) throws InvalidMessageException {
        return new String(decodeAndDecryptBytes(str));
    }

    public ECPrivateKey decryptKey(byte[] bArr) throws InvalidKeyException {
        try {
            return Curve.decodePrivatePoint(decryptBytes(bArr));
        } catch (InvalidMessageException e) {
            throw new InvalidKeyException(e);
        }
    }

    public byte[] decryptBytes(byte[] bArr) throws InvalidMessageException {
        try {
            byte[] verifyMacBody = verifyMacBody(getMac(this.masterSecret.getMacKey()), bArr);
            return getDecryptedBody(getDecryptingCipher(this.masterSecret.getEncryptionKey(), verifyMacBody), verifyMacBody);
        } catch (GeneralSecurityException e) {
            throw new InvalidMessageException(e);
        }
    }

    public byte[] encryptBytes(byte[] bArr) {
        try {
            return getMacBody(getMac(this.masterSecret.getMacKey()), getEncryptedBody(getEncryptingCipher(this.masterSecret.getEncryptionKey()), bArr));
        } catch (GeneralSecurityException e) {
            Log.w(TAG, "bodycipher", e);
            return null;
        }
    }

    public boolean verifyMacFor(String str, byte[] bArr) {
        byte[] macFor = getMacFor(str);
        String str2 = TAG;
        Log.i(str2, "Our Mac: " + Hex.toString(macFor));
        Log.i(str2, "Thr Mac: " + Hex.toString(bArr));
        return Arrays.equals(macFor, bArr);
    }

    public byte[] getMacFor(String str) {
        String str2 = TAG;
        Log.w(str2, "Macing: " + str);
        try {
            return getMac(this.masterSecret.getMacKey()).doFinal(str.getBytes());
        } catch (GeneralSecurityException e) {
            throw new AssertionError(e);
        }
    }

    private byte[] decodeAndDecryptBytes(String str) throws InvalidMessageException {
        try {
            return decryptBytes(Base64.decode(str));
        } catch (IOException e) {
            throw new InvalidMessageException("Bad Base64 Encoding...", e);
        }
    }

    private String encryptAndEncodeBytes(byte[] bArr) {
        return Base64.encodeBytes(encryptBytes(bArr));
    }

    private byte[] verifyMacBody(Mac mac, byte[] bArr) throws InvalidMessageException {
        if (bArr.length >= mac.getMacLength()) {
            int length = bArr.length - mac.getMacLength();
            byte[] bArr2 = new byte[length];
            System.arraycopy(bArr, 0, bArr2, 0, length);
            int macLength = mac.getMacLength();
            byte[] bArr3 = new byte[macLength];
            System.arraycopy(bArr, bArr.length - macLength, bArr3, 0, macLength);
            if (Arrays.equals(bArr3, mac.doFinal(bArr2))) {
                return bArr2;
            }
            throw new InvalidMessageException("MAC doesen't match.");
        }
        throw new InvalidMessageException("length(encrypted body + MAC) < length(MAC)");
    }

    private byte[] getDecryptedBody(Cipher cipher, byte[] bArr) throws IllegalBlockSizeException, BadPaddingException {
        return cipher.doFinal(bArr, cipher.getBlockSize(), bArr.length - cipher.getBlockSize());
    }

    private byte[] getEncryptedBody(Cipher cipher, byte[] bArr) throws IllegalBlockSizeException, BadPaddingException {
        byte[] doFinal = cipher.doFinal(bArr);
        byte[] iv = cipher.getIV();
        byte[] bArr2 = new byte[iv.length + doFinal.length];
        System.arraycopy(iv, 0, bArr2, 0, iv.length);
        System.arraycopy(doFinal, 0, bArr2, iv.length, doFinal.length);
        return bArr2;
    }

    private Mac getMac(SecretKeySpec secretKeySpec) throws NoSuchAlgorithmException, java.security.InvalidKeyException {
        this.hmac.init(secretKeySpec);
        return this.hmac;
    }

    private byte[] getMacBody(Mac mac, byte[] bArr) {
        byte[] doFinal = mac.doFinal(bArr);
        byte[] bArr2 = new byte[bArr.length + doFinal.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        System.arraycopy(doFinal, 0, bArr2, bArr.length, doFinal.length);
        return bArr2;
    }

    private Cipher getDecryptingCipher(SecretKeySpec secretKeySpec, byte[] bArr) throws java.security.InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchPaddingException {
        this.decryptingCipher.init(2, secretKeySpec, new IvParameterSpec(bArr, 0, this.decryptingCipher.getBlockSize()));
        return this.decryptingCipher;
    }

    private Cipher getEncryptingCipher(SecretKeySpec secretKeySpec) throws java.security.InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException {
        this.encryptingCipher.init(1, secretKeySpec);
        return this.encryptingCipher;
    }
}
