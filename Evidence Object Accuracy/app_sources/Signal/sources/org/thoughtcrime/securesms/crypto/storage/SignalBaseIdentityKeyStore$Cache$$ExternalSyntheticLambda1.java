package org.thoughtcrime.securesms.crypto.storage;

import org.signal.libsignal.protocol.IdentityKey;
import org.thoughtcrime.securesms.crypto.storage.SignalBaseIdentityKeyStore;
import org.thoughtcrime.securesms.database.IdentityDatabase;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SignalBaseIdentityKeyStore$Cache$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ SignalBaseIdentityKeyStore.Cache f$0;
    public final /* synthetic */ String f$1;
    public final /* synthetic */ RecipientId f$2;
    public final /* synthetic */ IdentityKey f$3;
    public final /* synthetic */ IdentityDatabase.VerifiedStatus f$4;

    public /* synthetic */ SignalBaseIdentityKeyStore$Cache$$ExternalSyntheticLambda1(SignalBaseIdentityKeyStore.Cache cache, String str, RecipientId recipientId, IdentityKey identityKey, IdentityDatabase.VerifiedStatus verifiedStatus) {
        this.f$0 = cache;
        this.f$1 = str;
        this.f$2 = recipientId;
        this.f$3 = identityKey;
        this.f$4 = verifiedStatus;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$setVerified$2(this.f$1, this.f$2, this.f$3, this.f$4);
    }
}
