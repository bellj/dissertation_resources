package org.thoughtcrime.securesms.crypto;

import android.content.Context;
import android.os.Build;
import java.security.SecureRandom;
import org.thoughtcrime.securesms.crypto.KeyStoreHelper;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes.dex */
public class AttachmentSecretProvider {
    private static AttachmentSecretProvider provider;
    private AttachmentSecret attachmentSecret;
    private final Context context;

    public static synchronized AttachmentSecretProvider getInstance(Context context) {
        AttachmentSecretProvider attachmentSecretProvider;
        synchronized (AttachmentSecretProvider.class) {
            if (provider == null) {
                provider = new AttachmentSecretProvider(context.getApplicationContext());
            }
            attachmentSecretProvider = provider;
        }
        return attachmentSecretProvider;
    }

    private AttachmentSecretProvider(Context context) {
        this.context = context.getApplicationContext();
    }

    public synchronized AttachmentSecret getOrCreateAttachmentSecret() {
        AttachmentSecret attachmentSecret = this.attachmentSecret;
        if (attachmentSecret != null) {
            return attachmentSecret;
        }
        String attachmentUnencryptedSecret = TextSecurePreferences.getAttachmentUnencryptedSecret(this.context);
        String attachmentEncryptedSecret = TextSecurePreferences.getAttachmentEncryptedSecret(this.context);
        if (attachmentUnencryptedSecret != null) {
            this.attachmentSecret = getUnencryptedAttachmentSecret(this.context, attachmentUnencryptedSecret);
        } else if (attachmentEncryptedSecret != null) {
            this.attachmentSecret = getEncryptedAttachmentSecret(attachmentEncryptedSecret);
        } else {
            this.attachmentSecret = createAndStoreAttachmentSecret(this.context);
        }
        return this.attachmentSecret;
    }

    public synchronized AttachmentSecret setClassicKey(Context context, byte[] bArr, byte[] bArr2) {
        AttachmentSecret orCreateAttachmentSecret = getOrCreateAttachmentSecret();
        orCreateAttachmentSecret.setClassicCipherKey(bArr);
        orCreateAttachmentSecret.setClassicMacKey(bArr2);
        storeAttachmentSecret(context, this.attachmentSecret);
        return this.attachmentSecret;
    }

    private AttachmentSecret getUnencryptedAttachmentSecret(Context context, String str) {
        AttachmentSecret fromString = AttachmentSecret.fromString(str);
        if (Build.VERSION.SDK_INT < 23) {
            return fromString;
        }
        TextSecurePreferences.setAttachmentEncryptedSecret(context, KeyStoreHelper.seal(fromString.serialize().getBytes()).serialize());
        TextSecurePreferences.setAttachmentUnencryptedSecret(context, null);
        return fromString;
    }

    private AttachmentSecret getEncryptedAttachmentSecret(String str) {
        if (Build.VERSION.SDK_INT >= 23) {
            return AttachmentSecret.fromString(new String(KeyStoreHelper.unseal(KeyStoreHelper.SealedData.fromString(str))));
        }
        throw new AssertionError("OS downgrade not supported. KeyStore sealed data exists on platform < M!");
    }

    private AttachmentSecret createAndStoreAttachmentSecret(Context context) {
        byte[] bArr = new byte[32];
        new SecureRandom().nextBytes(bArr);
        AttachmentSecret attachmentSecret = new AttachmentSecret(null, null, bArr);
        storeAttachmentSecret(context, attachmentSecret);
        return attachmentSecret;
    }

    private void storeAttachmentSecret(Context context, AttachmentSecret attachmentSecret) {
        if (Build.VERSION.SDK_INT >= 23) {
            TextSecurePreferences.setAttachmentEncryptedSecret(context, KeyStoreHelper.seal(attachmentSecret.serialize().getBytes()).serialize());
        } else {
            TextSecurePreferences.setAttachmentUnencryptedSecret(context, attachmentSecret.serialize());
        }
    }
}
