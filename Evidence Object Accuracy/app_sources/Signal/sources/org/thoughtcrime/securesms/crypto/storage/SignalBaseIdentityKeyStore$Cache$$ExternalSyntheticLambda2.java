package org.thoughtcrime.securesms.crypto.storage;

import org.thoughtcrime.securesms.crypto.storage.SignalBaseIdentityKeyStore;
import org.thoughtcrime.securesms.database.model.IdentityStoreRecord;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SignalBaseIdentityKeyStore$Cache$$ExternalSyntheticLambda2 implements Runnable {
    public final /* synthetic */ SignalBaseIdentityKeyStore.Cache f$0;
    public final /* synthetic */ String f$1;
    public final /* synthetic */ RecipientId f$2;
    public final /* synthetic */ boolean f$3;
    public final /* synthetic */ IdentityStoreRecord f$4;

    public /* synthetic */ SignalBaseIdentityKeyStore$Cache$$ExternalSyntheticLambda2(SignalBaseIdentityKeyStore.Cache cache, String str, RecipientId recipientId, boolean z, IdentityStoreRecord identityStoreRecord) {
        this.f$0 = cache;
        this.f$1 = str;
        this.f$2 = recipientId;
        this.f$3 = z;
        this.f$4 = identityStoreRecord;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$setApproval$1(this.f$1, this.f$2, this.f$3, this.f$4);
    }
}
