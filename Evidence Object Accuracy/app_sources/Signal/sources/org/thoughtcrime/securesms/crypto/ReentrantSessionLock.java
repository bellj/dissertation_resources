package org.thoughtcrime.securesms.crypto;

import java.util.Objects;
import java.util.concurrent.locks.ReentrantLock;
import org.whispersystems.signalservice.api.SignalSessionLock;

/* loaded from: classes4.dex */
public enum ReentrantSessionLock implements SignalSessionLock {
    INSTANCE;
    
    private static final ReentrantLock LOCK = new ReentrantLock();

    @Override // org.whispersystems.signalservice.api.SignalSessionLock
    public SignalSessionLock.Lock acquire() {
        ReentrantLock reentrantLock = LOCK;
        reentrantLock.lock();
        Objects.requireNonNull(reentrantLock);
        return new SignalSessionLock.Lock(reentrantLock) { // from class: org.thoughtcrime.securesms.crypto.ReentrantSessionLock$$ExternalSyntheticLambda0
            public final /* synthetic */ ReentrantLock f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.whispersystems.signalservice.api.SignalSessionLock.Lock, java.io.Closeable, java.lang.AutoCloseable
            public final void close() {
                this.f$0.unlock();
            }
        };
    }
}
