package org.thoughtcrime.securesms.crypto.storage;

import j$.util.Optional;
import j$.util.function.Supplier;
import java.util.List;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.IdentityKeyPair;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.state.IdentityKeyStore;
import org.thoughtcrime.securesms.database.IdentityDatabase;
import org.thoughtcrime.securesms.database.identity.IdentityRecordList;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class SignalIdentityKeyStore implements IdentityKeyStore {
    private final SignalBaseIdentityKeyStore baseStore;
    private final Supplier<IdentityKeyPair> identitySupplier;

    /* loaded from: classes4.dex */
    public enum SaveResult {
        NEW,
        UPDATE,
        NON_BLOCKING_APPROVAL_REQUIRED,
        NO_CHANGE
    }

    public SignalIdentityKeyStore(SignalBaseIdentityKeyStore signalBaseIdentityKeyStore, Supplier<IdentityKeyPair> supplier) {
        this.baseStore = signalBaseIdentityKeyStore;
        this.identitySupplier = supplier;
    }

    @Override // org.signal.libsignal.protocol.state.IdentityKeyStore
    public IdentityKeyPair getIdentityKeyPair() {
        return this.identitySupplier.get();
    }

    @Override // org.signal.libsignal.protocol.state.IdentityKeyStore
    public int getLocalRegistrationId() {
        return this.baseStore.getLocalRegistrationId();
    }

    @Override // org.signal.libsignal.protocol.state.IdentityKeyStore
    public boolean saveIdentity(SignalProtocolAddress signalProtocolAddress, IdentityKey identityKey) {
        return this.baseStore.saveIdentity(signalProtocolAddress, identityKey);
    }

    public SaveResult saveIdentity(SignalProtocolAddress signalProtocolAddress, IdentityKey identityKey, boolean z) {
        return this.baseStore.saveIdentity(signalProtocolAddress, identityKey, z);
    }

    public void saveIdentityWithoutSideEffects(RecipientId recipientId, IdentityKey identityKey, IdentityDatabase.VerifiedStatus verifiedStatus, boolean z, long j, boolean z2) {
        this.baseStore.saveIdentityWithoutSideEffects(recipientId, identityKey, verifiedStatus, z, j, z2);
    }

    @Override // org.signal.libsignal.protocol.state.IdentityKeyStore
    public boolean isTrustedIdentity(SignalProtocolAddress signalProtocolAddress, IdentityKey identityKey, IdentityKeyStore.Direction direction) {
        return this.baseStore.isTrustedIdentity(signalProtocolAddress, identityKey, direction);
    }

    @Override // org.signal.libsignal.protocol.state.IdentityKeyStore
    public IdentityKey getIdentity(SignalProtocolAddress signalProtocolAddress) {
        return this.baseStore.getIdentity(signalProtocolAddress);
    }

    public Optional<IdentityRecord> getIdentityRecord(RecipientId recipientId) {
        return this.baseStore.getIdentityRecord(recipientId);
    }

    public IdentityRecordList getIdentityRecords(List<Recipient> list) {
        return this.baseStore.getIdentityRecords(list);
    }

    public void setApproval(RecipientId recipientId, boolean z) {
        this.baseStore.setApproval(recipientId, z);
    }

    public void setVerified(RecipientId recipientId, IdentityKey identityKey, IdentityDatabase.VerifiedStatus verifiedStatus) {
        this.baseStore.setVerified(recipientId, identityKey, verifiedStatus);
    }

    public void delete(String str) {
        this.baseStore.delete(str);
    }

    public void invalidate(String str) {
        this.baseStore.invalidate(str);
    }
}
