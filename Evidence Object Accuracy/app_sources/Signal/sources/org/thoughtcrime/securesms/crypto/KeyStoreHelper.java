package org.thoughtcrime.securesms.crypto;

import android.security.keystore.KeyGenParameterSpec;
import android.util.Base64;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableEntryException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.JsonUtils;

/* loaded from: classes4.dex */
public final class KeyStoreHelper {
    private static final String ANDROID_KEY_STORE;
    private static final String KEY_ALIAS;

    public static SealedData seal(byte[] bArr) {
        SecretKey orCreateKeyStoreEntry = getOrCreateKeyStoreEntry();
        try {
            Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
            instance.init(1, orCreateKeyStoreEntry);
            return new SealedData(instance.getIV(), instance.doFinal(bArr));
        } catch (InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e) {
            throw new AssertionError(e);
        }
    }

    public static byte[] unseal(SealedData sealedData) {
        SecretKey keyStoreEntry = getKeyStoreEntry();
        try {
            Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
            instance.init(2, keyStoreEntry, new GCMParameterSpec(128, sealedData.iv));
            return instance.doFinal(sealedData.data);
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException e) {
            throw new AssertionError(e);
        }
    }

    private static SecretKey getOrCreateKeyStoreEntry() {
        if (hasKeyStoreEntry()) {
            return getKeyStoreEntry();
        }
        return createKeyStoreEntry();
    }

    private static SecretKey createKeyStoreEntry() {
        try {
            KeyGenerator instance = KeyGenerator.getInstance("AES", ANDROID_KEY_STORE);
            instance.init(new KeyGenParameterSpec.Builder(KEY_ALIAS, 3).setBlockModes("GCM").setEncryptionPaddings("NoPadding").build());
            return instance.generateKey();
        } catch (InvalidAlgorithmParameterException | NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new AssertionError(e);
        }
    }

    private static SecretKey getKeyStoreEntry() {
        KeyStore keyStore = getKeyStore();
        try {
            try {
                return getSecretKey(keyStore);
            } catch (UnrecoverableKeyException unused) {
                return getSecretKey(keyStore);
            }
        } catch (UnrecoverableKeyException e) {
            throw new AssertionError(e);
        }
    }

    private static SecretKey getSecretKey(KeyStore keyStore) throws UnrecoverableKeyException {
        Object e;
        try {
            return ((KeyStore.SecretKeyEntry) keyStore.getEntry(KEY_ALIAS, null)).getSecretKey();
        } catch (KeyStoreException e2) {
            e = e2;
            throw new AssertionError(e);
        } catch (NoSuchAlgorithmException e3) {
            e = e3;
            throw new AssertionError(e);
        } catch (UnrecoverableKeyException e4) {
            throw e4;
        } catch (UnrecoverableEntryException e5) {
            e = e5;
            throw new AssertionError(e);
        }
    }

    private static KeyStore getKeyStore() {
        try {
            KeyStore instance = KeyStore.getInstance(ANDROID_KEY_STORE);
            instance.load(null);
            return instance;
        } catch (IOException | KeyStoreException | NoSuchAlgorithmException | CertificateException e) {
            throw new AssertionError(e);
        }
    }

    private static boolean hasKeyStoreEntry() {
        try {
            KeyStore instance = KeyStore.getInstance(ANDROID_KEY_STORE);
            instance.load(null);
            if (instance.containsAlias(KEY_ALIAS)) {
                if (instance.entryInstanceOf(KEY_ALIAS, KeyStore.SecretKeyEntry.class)) {
                    return true;
                }
            }
            return false;
        } catch (IOException | KeyStoreException | NoSuchAlgorithmException | CertificateException e) {
            throw new AssertionError(e);
        }
    }

    /* loaded from: classes.dex */
    public static class SealedData {
        private static final String TAG = Log.tag(SealedData.class);
        @JsonProperty
        @JsonDeserialize(using = ByteArrayDeserializer.class)
        @JsonSerialize(using = ByteArraySerializer.class)
        private byte[] data;
        @JsonProperty
        @JsonDeserialize(using = ByteArrayDeserializer.class)
        @JsonSerialize(using = ByteArraySerializer.class)
        private byte[] iv;

        SealedData(byte[] bArr, byte[] bArr2) {
            this.iv = bArr;
            this.data = bArr2;
        }

        public SealedData() {
        }

        public String serialize() {
            try {
                return JsonUtils.toJson(this);
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }

        public static SealedData fromString(String str) {
            try {
                return (SealedData) JsonUtils.fromJson(str, SealedData.class);
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }

        /* loaded from: classes4.dex */
        private static class ByteArraySerializer extends JsonSerializer<byte[]> {
            private ByteArraySerializer() {
            }

            public void serialize(byte[] bArr, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
                jsonGenerator.writeString(Base64.encodeToString(bArr, 3));
            }
        }

        /* loaded from: classes4.dex */
        private static class ByteArrayDeserializer extends JsonDeserializer<byte[]> {
            private ByteArrayDeserializer() {
            }

            public byte[] deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
                return Base64.decode(jsonParser.getValueAsString(), 3);
            }
        }
    }
}
