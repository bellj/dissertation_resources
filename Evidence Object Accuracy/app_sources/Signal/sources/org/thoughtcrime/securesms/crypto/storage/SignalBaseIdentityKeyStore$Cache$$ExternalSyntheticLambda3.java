package org.thoughtcrime.securesms.crypto.storage;

import org.signal.libsignal.protocol.IdentityKey;
import org.thoughtcrime.securesms.crypto.storage.SignalBaseIdentityKeyStore;
import org.thoughtcrime.securesms.database.IdentityDatabase;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SignalBaseIdentityKeyStore$Cache$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ SignalBaseIdentityKeyStore.Cache f$0;
    public final /* synthetic */ String f$1;
    public final /* synthetic */ RecipientId f$2;
    public final /* synthetic */ IdentityKey f$3;
    public final /* synthetic */ IdentityDatabase.VerifiedStatus f$4;
    public final /* synthetic */ boolean f$5;
    public final /* synthetic */ long f$6;
    public final /* synthetic */ boolean f$7;

    public /* synthetic */ SignalBaseIdentityKeyStore$Cache$$ExternalSyntheticLambda3(SignalBaseIdentityKeyStore.Cache cache, String str, RecipientId recipientId, IdentityKey identityKey, IdentityDatabase.VerifiedStatus verifiedStatus, boolean z, long j, boolean z2) {
        this.f$0 = cache;
        this.f$1 = str;
        this.f$2 = recipientId;
        this.f$3 = identityKey;
        this.f$4 = verifiedStatus;
        this.f$5 = z;
        this.f$6 = j;
        this.f$7 = z2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$save$0(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7);
    }
}
