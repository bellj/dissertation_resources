package org.thoughtcrime.securesms.crypto;

import androidx.recyclerview.widget.RecyclerView;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.LimitedInputStream;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class ClassicDecryptingPartInputStream {
    private static final int IV_LENGTH;
    private static final int MAC_LENGTH;
    private static final String TAG = Log.tag(ClassicDecryptingPartInputStream.class);

    public static InputStream createFor(AttachmentSecret attachmentSecret, File file) throws IOException {
        try {
            if (file.length() > 36) {
                verifyMac(attachmentSecret, file);
                FileInputStream fileInputStream = new FileInputStream(file);
                byte[] bArr = new byte[16];
                readFully(fileInputStream, bArr);
                Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
                instance.init(2, new SecretKeySpec(attachmentSecret.getClassicCipherKey(), "AES"), new IvParameterSpec(bArr));
                return new CipherInputStreamWrapper(new LimitedInputStream(fileInputStream, (file.length() - 20) - 16), instance);
            }
            throw new IOException("File too short");
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new AssertionError(e);
        }
    }

    private static void verifyMac(AttachmentSecret attachmentSecret, File file) throws IOException {
        Mac initializeMac = initializeMac(new SecretKeySpec(attachmentSecret.getClassicMacKey(), "HmacSHA1"));
        FileInputStream fileInputStream = new FileInputStream(file);
        LimitedInputStream limitedInputStream = new LimitedInputStream(new FileInputStream(file), file.length() - 20);
        byte[] bArr = new byte[20];
        if (fileInputStream.skip(file.length() - 20) == file.length() - 20) {
            readFully(fileInputStream, bArr);
            byte[] bArr2 = new byte[RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT];
            while (true) {
                int read = limitedInputStream.read(bArr2);
                if (read == -1) {
                    break;
                }
                initializeMac.update(bArr2, 0, read);
            }
            if (MessageDigest.isEqual(initializeMac.doFinal(), bArr)) {
                fileInputStream.close();
                limitedInputStream.close();
                return;
            }
            throw new IOException("Bad MAC");
        }
        throw new IOException("Unable to seek");
    }

    private static Mac initializeMac(SecretKeySpec secretKeySpec) {
        try {
            Mac instance = Mac.getInstance("HmacSHA1");
            instance.init(secretKeySpec);
            return instance;
        } catch (InvalidKeyException | NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    private static void readFully(InputStream inputStream, byte[] bArr) throws IOException {
        int i = 0;
        do {
            i += inputStream.read(bArr, i, bArr.length - i);
        } while (i < bArr.length);
    }

    /* loaded from: classes4.dex */
    public static class CipherInputStreamWrapper extends CipherInputStream {
        CipherInputStreamWrapper(InputStream inputStream, Cipher cipher) {
            super(inputStream, cipher);
        }

        @Override // javax.crypto.CipherInputStream, java.io.FilterInputStream, java.io.Closeable, java.lang.AutoCloseable, java.io.InputStream
        public void close() throws IOException {
            try {
                super.close();
            } catch (Throwable th) {
                Log.w(ClassicDecryptingPartInputStream.TAG, th);
            }
        }

        @Override // javax.crypto.CipherInputStream, java.io.FilterInputStream, java.io.InputStream
        public long skip(long j) throws IOException {
            int read;
            if (j <= 0) {
                return 0;
            }
            byte[] bArr = new byte[4092];
            long j2 = j;
            while (j2 > 0 && (read = super.read(bArr, 0, Util.toIntExact(Math.min((long) 4092, j2)))) >= 0) {
                j2 -= (long) read;
            }
            return j - j2;
        }
    }
}
