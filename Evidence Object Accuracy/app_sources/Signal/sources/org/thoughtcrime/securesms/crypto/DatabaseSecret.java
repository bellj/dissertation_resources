package org.thoughtcrime.securesms.crypto;

import java.io.IOException;
import org.signal.core.util.Hex;

/* loaded from: classes.dex */
public class DatabaseSecret {
    private final String encoded;
    private final byte[] key;

    public DatabaseSecret(byte[] bArr) {
        this.key = bArr;
        this.encoded = Hex.toStringCondensed(bArr);
    }

    public DatabaseSecret(String str) throws IOException {
        this.key = Hex.fromStringCondensed(str);
        this.encoded = str;
    }

    public String asString() {
        return this.encoded;
    }

    public byte[] asBytes() {
        return this.key;
    }
}
