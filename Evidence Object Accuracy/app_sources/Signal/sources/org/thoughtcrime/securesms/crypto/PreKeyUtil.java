package org.thoughtcrime.securesms.crypto;

import j$.util.Collection$EL;
import j$.util.Comparator;
import j$.util.function.Consumer;
import j$.util.function.Predicate;
import j$.util.function.ToLongFunction;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.InvalidKeyIdException;
import org.signal.libsignal.protocol.ecc.Curve;
import org.signal.libsignal.protocol.ecc.ECKeyPair;
import org.signal.libsignal.protocol.state.PreKeyRecord;
import org.signal.libsignal.protocol.state.SignalProtocolStore;
import org.signal.libsignal.protocol.state.SignedPreKeyRecord;
import org.signal.libsignal.protocol.util.Medium;
import org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore;

/* loaded from: classes4.dex */
public class PreKeyUtil {
    private static final long ARCHIVE_AGE = TimeUnit.DAYS.toMillis(30);
    private static final int BATCH_SIZE;
    private static final String TAG = Log.tag(PreKeyUtil.class);

    public static synchronized List<PreKeyRecord> generateAndStoreOneTimePreKeys(SignalProtocolStore signalProtocolStore, PreKeyMetadataStore preKeyMetadataStore) {
        LinkedList linkedList;
        synchronized (PreKeyUtil.class) {
            Log.i(TAG, "Generating one-time prekeys...");
            linkedList = new LinkedList();
            int nextOneTimePreKeyId = preKeyMetadataStore.getNextOneTimePreKeyId();
            for (int i = 0; i < 100; i++) {
                int i2 = (nextOneTimePreKeyId + i) % Medium.MAX_VALUE;
                PreKeyRecord preKeyRecord = new PreKeyRecord(i2, Curve.generateKeyPair());
                signalProtocolStore.storePreKey(i2, preKeyRecord);
                linkedList.add(preKeyRecord);
            }
            preKeyMetadataStore.setNextOneTimePreKeyId(((nextOneTimePreKeyId + 100) + 1) % Medium.MAX_VALUE);
        }
        return linkedList;
    }

    public static synchronized SignedPreKeyRecord generateAndStoreSignedPreKey(SignalProtocolStore signalProtocolStore, PreKeyMetadataStore preKeyMetadataStore, boolean z) {
        SignedPreKeyRecord signedPreKeyRecord;
        synchronized (PreKeyUtil.class) {
            Log.i(TAG, "Generating signed prekeys...");
            try {
                int nextSignedPreKeyId = preKeyMetadataStore.getNextSignedPreKeyId();
                ECKeyPair generateKeyPair = Curve.generateKeyPair();
                signedPreKeyRecord = new SignedPreKeyRecord(nextSignedPreKeyId, System.currentTimeMillis(), generateKeyPair, Curve.calculateSignature(signalProtocolStore.getIdentityKeyPair().getPrivateKey(), generateKeyPair.getPublicKey().serialize()));
                signalProtocolStore.storeSignedPreKey(nextSignedPreKeyId, signedPreKeyRecord);
                preKeyMetadataStore.setNextSignedPreKeyId((nextSignedPreKeyId + 1) % Medium.MAX_VALUE);
                if (z) {
                    preKeyMetadataStore.setActiveSignedPreKeyId(nextSignedPreKeyId);
                }
            } catch (InvalidKeyException e) {
                throw new AssertionError(e);
            }
        }
        return signedPreKeyRecord;
    }

    public static synchronized void cleanSignedPreKeys(SignalProtocolStore signalProtocolStore, PreKeyMetadataStore preKeyMetadataStore) {
        synchronized (PreKeyUtil.class) {
            Log.i(TAG, "Cleaning signed prekeys...");
            int activeSignedPreKeyId = preKeyMetadataStore.getActiveSignedPreKeyId();
            if (activeSignedPreKeyId >= 0) {
                try {
                    long currentTimeMillis = System.currentTimeMillis();
                    Collection$EL.stream(signalProtocolStore.loadSignedPreKeys()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.crypto.PreKeyUtil$$ExternalSyntheticLambda0
                        @Override // j$.util.function.Predicate
                        public /* synthetic */ Predicate and(Predicate predicate) {
                            return Predicate.CC.$default$and(this, predicate);
                        }

                        @Override // j$.util.function.Predicate
                        public /* synthetic */ Predicate negate() {
                            return Predicate.CC.$default$negate(this);
                        }

                        @Override // j$.util.function.Predicate
                        public /* synthetic */ Predicate or(Predicate predicate) {
                            return Predicate.CC.$default$or(this, predicate);
                        }

                        @Override // j$.util.function.Predicate
                        public final boolean test(Object obj) {
                            return PreKeyUtil.m1654$r8$lambda$KTCXXn1EvUqUxWdyKJ1VddrcCo(SignedPreKeyRecord.this, (SignedPreKeyRecord) obj);
                        }
                    }).filter(new Predicate(currentTimeMillis) { // from class: org.thoughtcrime.securesms.crypto.PreKeyUtil$$ExternalSyntheticLambda1
                        public final /* synthetic */ long f$0;

                        {
                            this.f$0 = r1;
                        }

                        @Override // j$.util.function.Predicate
                        public /* synthetic */ Predicate and(Predicate predicate) {
                            return Predicate.CC.$default$and(this, predicate);
                        }

                        @Override // j$.util.function.Predicate
                        public /* synthetic */ Predicate negate() {
                            return Predicate.CC.$default$negate(this);
                        }

                        @Override // j$.util.function.Predicate
                        public /* synthetic */ Predicate or(Predicate predicate) {
                            return Predicate.CC.$default$or(this, predicate);
                        }

                        @Override // j$.util.function.Predicate
                        public final boolean test(Object obj) {
                            return PreKeyUtil.$r8$lambda$sZglt1EjSV8OZY_HNgPzitru53M(this.f$0, (SignedPreKeyRecord) obj);
                        }
                    }).sorted(Comparator.EL.reversed(Comparator.CC.comparingLong(new ToLongFunction() { // from class: org.thoughtcrime.securesms.crypto.PreKeyUtil$$ExternalSyntheticLambda2
                        @Override // j$.util.function.ToLongFunction
                        public final long applyAsLong(Object obj) {
                            return ((SignedPreKeyRecord) obj).getTimestamp();
                        }
                    }))).skip(1).forEach(new Consumer() { // from class: org.thoughtcrime.securesms.crypto.PreKeyUtil$$ExternalSyntheticLambda3
                        @Override // j$.util.function.Consumer
                        public final void accept(Object obj) {
                            PreKeyUtil.$r8$lambda$k3VBa6FE2RwvADU08C4mybmHXyo(SignalProtocolStore.this, (SignedPreKeyRecord) obj);
                        }

                        @Override // j$.util.function.Consumer
                        public /* synthetic */ Consumer andThen(Consumer consumer) {
                            return Consumer.CC.$default$andThen(this, consumer);
                        }
                    });
                } catch (InvalidKeyIdException e) {
                    Log.w(TAG, e);
                }
            }
        }
    }

    public static /* synthetic */ boolean lambda$cleanSignedPreKeys$0(SignedPreKeyRecord signedPreKeyRecord, SignedPreKeyRecord signedPreKeyRecord2) {
        return signedPreKeyRecord2.getId() != signedPreKeyRecord.getId();
    }

    public static /* synthetic */ boolean lambda$cleanSignedPreKeys$1(long j, SignedPreKeyRecord signedPreKeyRecord) {
        return j - signedPreKeyRecord.getTimestamp() > ARCHIVE_AGE;
    }

    public static /* synthetic */ void lambda$cleanSignedPreKeys$2(SignalProtocolStore signalProtocolStore, SignedPreKeyRecord signedPreKeyRecord) {
        String str = TAG;
        Log.i(str, "Removing signed prekey record: " + signedPreKeyRecord.getId() + " with timestamp: " + signedPreKeyRecord.getTimestamp());
        signalProtocolStore.removeSignedPreKey(signedPreKeyRecord.getId());
    }
}
