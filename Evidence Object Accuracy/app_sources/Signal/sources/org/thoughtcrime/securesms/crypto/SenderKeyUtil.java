package org.thoughtcrime.securesms.crypto;

import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.whispersystems.signalservice.api.SignalSessionLock;
import org.whispersystems.signalservice.api.push.DistributionId;

/* loaded from: classes4.dex */
public final class SenderKeyUtil {
    private SenderKeyUtil() {
    }

    public static void rotateOurKey(DistributionId distributionId) {
        SignalSessionLock.Lock acquire = ReentrantSessionLock.INSTANCE.acquire();
        try {
            ApplicationDependencies.getProtocolStore().aci().senderKeys().deleteAllFor(SignalStore.account().requireAci().toString(), distributionId);
            SignalDatabase.senderKeyShared().deleteAllFor(distributionId);
            if (acquire != null) {
                acquire.close();
            }
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static long getCreateTimeForOurKey(DistributionId distributionId) {
        return SignalDatabase.senderKeys().getCreatedTime(new SignalProtocolAddress(SignalStore.account().requireAci().toString(), SignalStore.account().getDeviceId()), distributionId);
    }

    public static void clearAllState() {
        SignalSessionLock.Lock acquire = ReentrantSessionLock.INSTANCE.acquire();
        try {
            ApplicationDependencies.getProtocolStore().aci().senderKeys().deleteAll();
            SignalDatabase.senderKeyShared().deleteAll();
            if (acquire != null) {
                acquire.close();
            }
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }
}
