package org.thoughtcrime.securesms.crypto;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.signal.core.util.Conversions;

/* loaded from: classes4.dex */
public class ModernDecryptingPartInputStream {
    public static InputStream createFor(AttachmentSecret attachmentSecret, byte[] bArr, File file, long j) throws IOException {
        return createFor(attachmentSecret, bArr, new FileInputStream(file), j);
    }

    public static InputStream createFor(AttachmentSecret attachmentSecret, File file, long j) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] bArr = new byte[32];
        readFully(fileInputStream, bArr);
        return createFor(attachmentSecret, bArr, fileInputStream, j);
    }

    private static InputStream createFor(AttachmentSecret attachmentSecret, byte[] bArr, InputStream inputStream, long j) throws IOException {
        try {
            Mac instance = Mac.getInstance("HmacSHA256");
            instance.init(new SecretKeySpec(attachmentSecret.getModernKey(), "HmacSHA256"));
            byte[] bArr2 = new byte[16];
            int i = (int) (j % 16);
            Conversions.longTo4ByteArray(bArr2, 12, j / 16);
            byte[] doFinal = instance.doFinal(bArr);
            Cipher instance2 = Cipher.getInstance("AES/CTR/NoPadding");
            instance2.init(2, new SecretKeySpec(doFinal, "AES"), new IvParameterSpec(bArr2));
            long j2 = j - ((long) i);
            long skip = inputStream.skip(j2);
            if (skip == j2) {
                CipherInputStream cipherInputStream = new CipherInputStream(inputStream, instance2);
                readFully(cipherInputStream, new byte[i]);
                return cipherInputStream;
            }
            throw new IOException("Skip failed: " + skip + " vs " + j2);
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new AssertionError(e);
        }
    }

    private static void readFully(InputStream inputStream, byte[] bArr) throws IOException {
        int i = 0;
        do {
            int read = inputStream.read(bArr, i, bArr.length - i);
            if (read != -1) {
                i += read;
            } else {
                throw new IOException("Prematurely reached end of stream!");
            }
        } while (i < bArr.length);
    }
}
