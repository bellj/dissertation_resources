package org.thoughtcrime.securesms.crypto;

import com.annimon.stream.function.Predicate;
import j$.util.Optional;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class UnidentifiedAccessUtil$$ExternalSyntheticLambda0 implements Predicate {
    @Override // com.annimon.stream.function.Predicate
    public final boolean test(Object obj) {
        return ((Optional) obj).isPresent();
    }
}
