package org.thoughtcrime.securesms.crypto;

import android.util.Pair;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* loaded from: classes4.dex */
public class ModernEncryptingPartOutputStream {
    public static long getPlaintextLength(long j) {
        return j - 32;
    }

    public static Pair<byte[], OutputStream> createFor(AttachmentSecret attachmentSecret, File file, boolean z) throws IOException {
        byte[] bArr = new byte[32];
        new SecureRandom().nextBytes(bArr);
        try {
            Mac instance = Mac.getInstance("HmacSHA256");
            instance.init(new SecretKeySpec(attachmentSecret.getModernKey(), "HmacSHA256"));
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            byte[] doFinal = instance.doFinal(bArr);
            Cipher instance2 = Cipher.getInstance("AES/CTR/NoPadding");
            instance2.init(1, new SecretKeySpec(doFinal, "AES"), new IvParameterSpec(new byte[16]));
            if (z) {
                fileOutputStream.write(bArr);
            }
            return new Pair<>(bArr, new CipherOutputStream(fileOutputStream, instance2));
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new AssertionError(e);
        }
    }
}
