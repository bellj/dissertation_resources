package org.thoughtcrime.securesms.crypto;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import javax.crypto.spec.SecretKeySpec;

/* loaded from: classes4.dex */
public class MasterSecret implements Parcelable {
    public static final Parcelable.Creator<MasterSecret> CREATOR = new Parcelable.Creator<MasterSecret>() { // from class: org.thoughtcrime.securesms.crypto.MasterSecret.1
        @Override // android.os.Parcelable.Creator
        public MasterSecret createFromParcel(Parcel parcel) {
            return new MasterSecret(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public MasterSecret[] newArray(int i) {
            return new MasterSecret[i];
        }
    };
    private final SecretKeySpec encryptionKey;
    private final SecretKeySpec macKey;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public MasterSecret(SecretKeySpec secretKeySpec, SecretKeySpec secretKeySpec2) {
        this.encryptionKey = secretKeySpec;
        this.macKey = secretKeySpec2;
    }

    private MasterSecret(Parcel parcel) {
        byte[] bArr = new byte[parcel.readInt()];
        parcel.readByteArray(bArr);
        byte[] bArr2 = new byte[parcel.readInt()];
        parcel.readByteArray(bArr2);
        this.encryptionKey = new SecretKeySpec(bArr, "AES");
        this.macKey = new SecretKeySpec(bArr2, "HmacSHA1");
        Arrays.fill(bArr, (byte) 0);
        Arrays.fill(bArr2, (byte) 0);
    }

    public SecretKeySpec getEncryptionKey() {
        return this.encryptionKey;
    }

    public SecretKeySpec getMacKey() {
        return this.macKey;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.encryptionKey.getEncoded().length);
        parcel.writeByteArray(this.encryptionKey.getEncoded());
        parcel.writeInt(this.macKey.getEncoded().length);
        parcel.writeByteArray(this.macKey.getEncoded());
    }

    public MasterSecret parcelClone() {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        obtain.writeValue(this);
        byte[] marshall = obtain.marshall();
        obtain2.unmarshall(marshall, 0, marshall.length);
        obtain2.setDataPosition(0);
        MasterSecret masterSecret = (MasterSecret) obtain2.readValue(MasterSecret.class.getClassLoader());
        obtain.recycle();
        obtain2.recycle();
        return masterSecret;
    }
}
