package org.thoughtcrime.securesms.crypto.storage;

import android.content.Context;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.SignalServiceDataStore;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes4.dex */
public final class SignalServiceDataStoreImpl implements SignalServiceDataStore {
    private final SignalServiceAccountDataStoreImpl aciStore;
    private final Context context;
    private final SignalServiceAccountDataStoreImpl pniStore;

    public SignalServiceDataStoreImpl(Context context, SignalServiceAccountDataStoreImpl signalServiceAccountDataStoreImpl, SignalServiceAccountDataStoreImpl signalServiceAccountDataStoreImpl2) {
        this.context = context;
        this.aciStore = signalServiceAccountDataStoreImpl;
        this.pniStore = signalServiceAccountDataStoreImpl2;
    }

    @Override // org.whispersystems.signalservice.api.SignalServiceDataStore
    public SignalServiceAccountDataStoreImpl get(ServiceId serviceId) {
        if (serviceId.equals(SignalStore.account().getAci())) {
            return this.aciStore;
        }
        if (serviceId.equals(SignalStore.account().getPni())) {
            return this.pniStore;
        }
        throw new IllegalArgumentException("No matching store found for " + serviceId);
    }

    @Override // org.whispersystems.signalservice.api.SignalServiceDataStore
    public SignalServiceAccountDataStoreImpl aci() {
        return this.aciStore;
    }

    @Override // org.whispersystems.signalservice.api.SignalServiceDataStore
    public SignalServiceAccountDataStoreImpl pni() {
        return this.pniStore;
    }

    @Override // org.whispersystems.signalservice.api.SignalServiceDataStore
    public boolean isMultiDevice() {
        return TextSecurePreferences.isMultiDevice(this.context);
    }
}
