package org.thoughtcrime.securesms.crypto.storage;

import kotlin.Metadata;

/* compiled from: PreKeyMetadataStore.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\r\bf\u0018\u00002\u00020\u0001R\u0018\u0010\u0002\u001a\u00020\u0003X¦\u000e¢\u0006\f\u001a\u0004\b\u0004\u0010\u0005\"\u0004\b\u0006\u0010\u0007R\u0018\u0010\b\u001a\u00020\tX¦\u000e¢\u0006\f\u001a\u0004\b\b\u0010\n\"\u0004\b\u000b\u0010\fR\u0018\u0010\r\u001a\u00020\u0003X¦\u000e¢\u0006\f\u001a\u0004\b\u000e\u0010\u0005\"\u0004\b\u000f\u0010\u0007R\u0018\u0010\u0010\u001a\u00020\u0003X¦\u000e¢\u0006\f\u001a\u0004\b\u0011\u0010\u0005\"\u0004\b\u0012\u0010\u0007R\u0018\u0010\u0013\u001a\u00020\u0003X¦\u000e¢\u0006\f\u001a\u0004\b\u0014\u0010\u0005\"\u0004\b\u0015\u0010\u0007¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/crypto/storage/PreKeyMetadataStore;", "", "activeSignedPreKeyId", "", "getActiveSignedPreKeyId", "()I", "setActiveSignedPreKeyId", "(I)V", "isSignedPreKeyRegistered", "", "()Z", "setSignedPreKeyRegistered", "(Z)V", "nextOneTimePreKeyId", "getNextOneTimePreKeyId", "setNextOneTimePreKeyId", "nextSignedPreKeyId", "getNextSignedPreKeyId", "setNextSignedPreKeyId", "signedPreKeyFailureCount", "getSignedPreKeyFailureCount", "setSignedPreKeyFailureCount", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public interface PreKeyMetadataStore {
    int getActiveSignedPreKeyId();

    int getNextOneTimePreKeyId();

    int getNextSignedPreKeyId();

    int getSignedPreKeyFailureCount();

    boolean isSignedPreKeyRegistered();

    void setActiveSignedPreKeyId(int i);

    void setNextOneTimePreKeyId(int i);

    void setNextSignedPreKeyId(int i);

    void setSignedPreKeyFailureCount(int i);

    void setSignedPreKeyRegistered(boolean z);
}
