package org.thoughtcrime.securesms.crypto;

import android.content.Context;
import android.os.Build;
import java.io.IOException;
import java.security.SecureRandom;
import org.thoughtcrime.securesms.crypto.KeyStoreHelper;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes.dex */
public final class DatabaseSecretProvider {
    private static volatile DatabaseSecret instance;

    public static DatabaseSecret getOrCreateDatabaseSecret(Context context) {
        if (instance == null) {
            synchronized (DatabaseSecretProvider.class) {
                if (instance == null) {
                    instance = getOrCreate(context);
                }
            }
        }
        return instance;
    }

    private DatabaseSecretProvider() {
    }

    private static DatabaseSecret getOrCreate(Context context) {
        String databaseUnencryptedSecret = TextSecurePreferences.getDatabaseUnencryptedSecret(context);
        String databaseEncryptedSecret = TextSecurePreferences.getDatabaseEncryptedSecret(context);
        if (databaseUnencryptedSecret != null) {
            return getUnencryptedDatabaseSecret(context, databaseUnencryptedSecret);
        }
        if (databaseEncryptedSecret != null) {
            return getEncryptedDatabaseSecret(databaseEncryptedSecret);
        }
        return createAndStoreDatabaseSecret(context);
    }

    private static DatabaseSecret getUnencryptedDatabaseSecret(Context context, String str) {
        try {
            DatabaseSecret databaseSecret = new DatabaseSecret(str);
            if (Build.VERSION.SDK_INT < 23) {
                return databaseSecret;
            }
            TextSecurePreferences.setDatabaseEncryptedSecret(context, KeyStoreHelper.seal(databaseSecret.asBytes()).serialize());
            TextSecurePreferences.setDatabaseUnencryptedSecret(context, null);
            return databaseSecret;
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    private static DatabaseSecret getEncryptedDatabaseSecret(String str) {
        if (Build.VERSION.SDK_INT >= 23) {
            return new DatabaseSecret(KeyStoreHelper.unseal(KeyStoreHelper.SealedData.fromString(str)));
        }
        throw new AssertionError("OS downgrade not supported. KeyStore sealed data exists on platform < M!");
    }

    private static DatabaseSecret createAndStoreDatabaseSecret(Context context) {
        byte[] bArr = new byte[32];
        new SecureRandom().nextBytes(bArr);
        DatabaseSecret databaseSecret = new DatabaseSecret(bArr);
        if (Build.VERSION.SDK_INT >= 23) {
            TextSecurePreferences.setDatabaseEncryptedSecret(context, KeyStoreHelper.seal(databaseSecret.asBytes()).serialize());
        } else {
            TextSecurePreferences.setDatabaseUnencryptedSecret(context, databaseSecret.asString());
        }
        return databaseSecret;
    }
}
