package org.thoughtcrime.securesms.crypto;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.ecc.Curve;
import org.signal.libsignal.protocol.ecc.ECKeyPair;
import org.signal.libsignal.protocol.ecc.ECPrivateKey;
import org.signal.libsignal.protocol.ecc.ECPublicKey;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class MasterSecretUtil {
    private static final String ASYMMETRIC_LOCAL_PRIVATE_DJB;
    private static final String ASYMMETRIC_LOCAL_PUBLIC_DJB;
    public static final String PREFERENCES_NAME;
    private static final String TAG = Log.tag(MasterSecretUtil.class);
    public static final String UNENCRYPTED_PASSPHRASE;
    private static SharedPreferences preferences;

    public static MasterSecret changeMasterSecretPassphrase(Context context, MasterSecret masterSecret, String str) {
        try {
            byte[] combine = Util.combine(masterSecret.getEncryptionKey().getEncoded(), masterSecret.getMacKey().getEncoded());
            byte[] generateSalt = generateSalt();
            int generateIterationCount = generateIterationCount(str, generateSalt);
            byte[] encryptWithPassphrase = encryptWithPassphrase(generateSalt, generateIterationCount, combine, str);
            byte[] generateSalt2 = generateSalt();
            byte[] macWithPassphrase = macWithPassphrase(generateSalt2, generateIterationCount, encryptWithPassphrase, str);
            save(context, "encryption_salt", generateSalt);
            save(context, "mac_salt", generateSalt2);
            save(context, "passphrase_iterations", generateIterationCount);
            save(context, "master_secret", macWithPassphrase);
            save(context, "passphrase_initialized", true);
            return masterSecret;
        } catch (GeneralSecurityException e) {
            throw new AssertionError(e);
        }
    }

    public static MasterSecret changeMasterSecretPassphrase(Context context, String str, String str2) throws InvalidPassphraseException {
        MasterSecret masterSecret = getMasterSecret(context, str);
        changeMasterSecretPassphrase(context, masterSecret, str2);
        return masterSecret;
    }

    public static MasterSecret getMasterSecret(Context context, String str) throws InvalidPassphraseException {
        try {
            byte[] retrieve = retrieve(context, "master_secret");
            byte[] retrieve2 = retrieve(context, "mac_salt");
            int retrieve3 = retrieve(context, "passphrase_iterations", 100);
            byte[] decryptWithPassphrase = decryptWithPassphrase(retrieve(context, "encryption_salt"), retrieve3, verifyMac(retrieve2, retrieve3, retrieve, str), str);
            return new MasterSecret(new SecretKeySpec(Util.split(decryptWithPassphrase, 16, 20)[0], "AES"), new SecretKeySpec(Util.split(decryptWithPassphrase, 16, 20)[1], "HmacSHA1"));
        } catch (IOException e) {
            Log.w(TAG, e);
            return null;
        } catch (GeneralSecurityException e2) {
            Log.w(TAG, e2);
            return null;
        }
    }

    public static AsymmetricMasterSecret getAsymmetricMasterSecret(Context context, MasterSecret masterSecret) {
        try {
            byte[] retrieve = retrieve(context, ASYMMETRIC_LOCAL_PUBLIC_DJB);
            byte[] retrieve2 = retrieve(context, ASYMMETRIC_LOCAL_PRIVATE_DJB);
            ECPrivateKey eCPrivateKey = null;
            ECPublicKey decodePoint = retrieve != null ? Curve.decodePoint(retrieve, 0) : null;
            if (masterSecret != null) {
                MasterCipher masterCipher = new MasterCipher(masterSecret);
                if (retrieve2 != null) {
                    eCPrivateKey = masterCipher.decryptKey(retrieve2);
                }
            }
            return new AsymmetricMasterSecret(decodePoint, eCPrivateKey);
        } catch (IOException | InvalidKeyException e) {
            throw new AssertionError(e);
        }
    }

    public static AsymmetricMasterSecret generateAsymmetricMasterSecret(Context context, MasterSecret masterSecret) {
        MasterCipher masterCipher = new MasterCipher(masterSecret);
        ECKeyPair generateKeyPair = Curve.generateKeyPair();
        save(context, ASYMMETRIC_LOCAL_PUBLIC_DJB, generateKeyPair.getPublicKey().serialize());
        save(context, ASYMMETRIC_LOCAL_PRIVATE_DJB, masterCipher.encryptKey(generateKeyPair.getPrivateKey()));
        return new AsymmetricMasterSecret(generateKeyPair.getPublicKey(), generateKeyPair.getPrivateKey());
    }

    public static MasterSecret generateMasterSecret(Context context, String str) {
        try {
            byte[] generateEncryptionSecret = generateEncryptionSecret();
            byte[] generateMacSecret = generateMacSecret();
            byte[] combine = Util.combine(generateEncryptionSecret, generateMacSecret);
            byte[] generateSalt = generateSalt();
            int generateIterationCount = generateIterationCount(str, generateSalt);
            byte[] encryptWithPassphrase = encryptWithPassphrase(generateSalt, generateIterationCount, combine, str);
            byte[] generateSalt2 = generateSalt();
            byte[] macWithPassphrase = macWithPassphrase(generateSalt2, generateIterationCount, encryptWithPassphrase, str);
            save(context, "encryption_salt", generateSalt);
            save(context, "mac_salt", generateSalt2);
            save(context, "passphrase_iterations", generateIterationCount);
            save(context, "master_secret", macWithPassphrase);
            save(context, "passphrase_initialized", true);
            return new MasterSecret(new SecretKeySpec(generateEncryptionSecret, "AES"), new SecretKeySpec(generateMacSecret, "HmacSHA1"));
        } catch (GeneralSecurityException e) {
            Log.w(TAG, e);
            return null;
        }
    }

    public static boolean hasAsymmericMasterSecret(Context context) {
        return getSharedPreferences(context).contains(ASYMMETRIC_LOCAL_PUBLIC_DJB);
    }

    public static boolean isPassphraseInitialized(Context context) {
        return getSharedPreferences(context).getBoolean("passphrase_initialized", false);
    }

    private static void save(Context context, String str, int i) {
        if (!getSharedPreferences(context).edit().putInt(str, i).commit()) {
            throw new AssertionError("failed to save a shared pref in MasterSecretUtil");
        }
    }

    private static void save(Context context, String str, byte[] bArr) {
        if (!getSharedPreferences(context).edit().putString(str, Base64.encodeBytes(bArr)).commit()) {
            throw new AssertionError("failed to save a shared pref in MasterSecretUtil");
        }
    }

    private static void save(Context context, String str, boolean z) {
        if (!getSharedPreferences(context).edit().putBoolean(str, z).commit()) {
            throw new AssertionError("failed to save a shared pref in MasterSecretUtil");
        }
    }

    private static byte[] retrieve(Context context, String str) throws IOException {
        String string = getSharedPreferences(context).getString(str, "");
        if (TextUtils.isEmpty(string)) {
            return null;
        }
        return Base64.decode(string);
    }

    private static int retrieve(Context context, String str, int i) throws IOException {
        return getSharedPreferences(context).getInt(str, i);
    }

    private static byte[] generateEncryptionSecret() {
        try {
            KeyGenerator instance = KeyGenerator.getInstance("AES");
            instance.init(128);
            return instance.generateKey().getEncoded();
        } catch (NoSuchAlgorithmException e) {
            Log.w(TAG, e);
            return null;
        }
    }

    private static byte[] generateMacSecret() {
        try {
            return KeyGenerator.getInstance("HmacSHA1").generateKey().getEncoded();
        } catch (NoSuchAlgorithmException e) {
            Log.w(TAG, e);
            return null;
        }
    }

    private static byte[] generateSalt() {
        byte[] bArr = new byte[16];
        new SecureRandom().nextBytes(bArr);
        return bArr;
    }

    private static int generateIterationCount(String str, byte[] bArr) {
        try {
            PBEKeySpec pBEKeySpec = new PBEKeySpec(str.toCharArray(), bArr, 10000);
            SecretKeyFactory instance = SecretKeyFactory.getInstance("PBEWITHSHA1AND128BITAES-CBC-BC");
            long currentTimeMillis = System.currentTimeMillis();
            instance.generateSecret(pBEKeySpec);
            double d = (double) 10000;
            double currentTimeMillis2 = (double) (System.currentTimeMillis() - currentTimeMillis);
            Double.isNaN(d);
            Double.isNaN(currentTimeMillis2);
            double d2 = d / currentTimeMillis2;
            double d3 = (double) 50;
            Double.isNaN(d3);
            int i = (int) (d2 * d3);
            if (i < 100) {
                return 100;
            }
            return i;
        } catch (NoSuchAlgorithmException e) {
            Log.w(TAG, e);
            return 100;
        } catch (InvalidKeySpecException e2) {
            Log.w(TAG, e2);
            return 100;
        }
    }

    private static SecretKey getKeyFromPassphrase(String str, byte[] bArr, int i) throws GeneralSecurityException {
        return SecretKeyFactory.getInstance("PBEWITHSHA1AND128BITAES-CBC-BC").generateSecret(new PBEKeySpec(str.toCharArray(), bArr, i));
    }

    private static Cipher getCipherFromPassphrase(String str, byte[] bArr, int i, int i2) throws GeneralSecurityException {
        SecretKey keyFromPassphrase = getKeyFromPassphrase(str, bArr, i);
        Cipher instance = Cipher.getInstance(keyFromPassphrase.getAlgorithm());
        instance.init(i2, keyFromPassphrase, new PBEParameterSpec(bArr, i));
        return instance;
    }

    private static byte[] encryptWithPassphrase(byte[] bArr, int i, byte[] bArr2, String str) throws GeneralSecurityException {
        return getCipherFromPassphrase(str, bArr, i, 1).doFinal(bArr2);
    }

    private static byte[] decryptWithPassphrase(byte[] bArr, int i, byte[] bArr2, String str) throws GeneralSecurityException, IOException {
        return getCipherFromPassphrase(str, bArr, i, 2).doFinal(bArr2);
    }

    private static Mac getMacForPassphrase(String str, byte[] bArr, int i) throws GeneralSecurityException {
        SecretKeySpec secretKeySpec = new SecretKeySpec(getKeyFromPassphrase(str, bArr, i).getEncoded(), "HmacSHA1");
        Mac instance = Mac.getInstance("HmacSHA1");
        instance.init(secretKeySpec);
        return instance;
    }

    private static byte[] verifyMac(byte[] bArr, int i, byte[] bArr2, String str) throws InvalidPassphraseException, GeneralSecurityException, IOException {
        Mac macForPassphrase = getMacForPassphrase(str, bArr, i);
        int length = bArr2.length - macForPassphrase.getMacLength();
        byte[] bArr3 = new byte[length];
        System.arraycopy(bArr2, 0, bArr3, 0, length);
        int macLength = macForPassphrase.getMacLength();
        byte[] bArr4 = new byte[macLength];
        System.arraycopy(bArr2, bArr2.length - macForPassphrase.getMacLength(), bArr4, 0, macLength);
        if (Arrays.equals(bArr4, macForPassphrase.doFinal(bArr3))) {
            return bArr3;
        }
        throw new InvalidPassphraseException("MAC Error");
    }

    private static byte[] macWithPassphrase(byte[] bArr, int i, byte[] bArr2, String str) throws GeneralSecurityException {
        byte[] doFinal = getMacForPassphrase(str, bArr, i).doFinal(bArr2);
        byte[] bArr3 = new byte[bArr2.length + doFinal.length];
        System.arraycopy(bArr2, 0, bArr3, 0, bArr2.length);
        System.arraycopy(doFinal, 0, bArr3, bArr2.length, doFinal.length);
        return bArr3;
    }

    private static SharedPreferences getSharedPreferences(Context context) {
        if (preferences == null) {
            preferences = context.getSharedPreferences(PREFERENCES_NAME, 0);
        }
        return preferences;
    }
}
