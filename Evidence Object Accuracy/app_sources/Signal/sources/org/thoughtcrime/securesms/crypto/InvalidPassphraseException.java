package org.thoughtcrime.securesms.crypto;

/* loaded from: classes4.dex */
public class InvalidPassphraseException extends Exception {
    public InvalidPassphraseException() {
    }

    public InvalidPassphraseException(String str) {
        super(str);
    }

    public InvalidPassphraseException(Throwable th) {
        super(th);
    }

    public InvalidPassphraseException(String str, Throwable th) {
        super(str, th);
    }
}
