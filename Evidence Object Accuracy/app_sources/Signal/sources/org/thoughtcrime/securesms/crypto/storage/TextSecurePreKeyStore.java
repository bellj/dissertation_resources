package org.thoughtcrime.securesms.crypto.storage;

import java.util.List;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidKeyIdException;
import org.signal.libsignal.protocol.state.PreKeyRecord;
import org.signal.libsignal.protocol.state.PreKeyStore;
import org.signal.libsignal.protocol.state.SignedPreKeyRecord;
import org.signal.libsignal.protocol.state.SignedPreKeyStore;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes4.dex */
public class TextSecurePreKeyStore implements PreKeyStore, SignedPreKeyStore {
    private static final Object LOCK = new Object();
    private static final String TAG = Log.tag(TextSecurePreKeyStore.class);
    private final ServiceId accountId;

    public TextSecurePreKeyStore(ServiceId serviceId) {
        this.accountId = serviceId;
    }

    @Override // org.signal.libsignal.protocol.state.PreKeyStore
    public PreKeyRecord loadPreKey(int i) throws InvalidKeyIdException {
        PreKeyRecord preKeyRecord;
        synchronized (LOCK) {
            preKeyRecord = SignalDatabase.oneTimePreKeys().get(this.accountId, i);
            if (preKeyRecord == null) {
                throw new InvalidKeyIdException("No such key: " + i);
            }
        }
        return preKeyRecord;
    }

    @Override // org.signal.libsignal.protocol.state.SignedPreKeyStore
    public SignedPreKeyRecord loadSignedPreKey(int i) throws InvalidKeyIdException {
        SignedPreKeyRecord signedPreKeyRecord;
        synchronized (LOCK) {
            signedPreKeyRecord = SignalDatabase.signedPreKeys().get(this.accountId, i);
            if (signedPreKeyRecord == null) {
                throw new InvalidKeyIdException("No such signed prekey: " + i);
            }
        }
        return signedPreKeyRecord;
    }

    @Override // org.signal.libsignal.protocol.state.SignedPreKeyStore
    public List<SignedPreKeyRecord> loadSignedPreKeys() {
        List<SignedPreKeyRecord> all;
        synchronized (LOCK) {
            all = SignalDatabase.signedPreKeys().getAll(this.accountId);
        }
        return all;
    }

    @Override // org.signal.libsignal.protocol.state.PreKeyStore
    public void storePreKey(int i, PreKeyRecord preKeyRecord) {
        synchronized (LOCK) {
            SignalDatabase.oneTimePreKeys().insert(this.accountId, i, preKeyRecord);
        }
    }

    @Override // org.signal.libsignal.protocol.state.SignedPreKeyStore
    public void storeSignedPreKey(int i, SignedPreKeyRecord signedPreKeyRecord) {
        synchronized (LOCK) {
            SignalDatabase.signedPreKeys().insert(this.accountId, i, signedPreKeyRecord);
        }
    }

    @Override // org.signal.libsignal.protocol.state.PreKeyStore
    public boolean containsPreKey(int i) {
        return SignalDatabase.oneTimePreKeys().get(this.accountId, i) != null;
    }

    @Override // org.signal.libsignal.protocol.state.SignedPreKeyStore
    public boolean containsSignedPreKey(int i) {
        return SignalDatabase.signedPreKeys().get(this.accountId, i) != null;
    }

    @Override // org.signal.libsignal.protocol.state.PreKeyStore
    public void removePreKey(int i) {
        SignalDatabase.oneTimePreKeys().delete(this.accountId, i);
    }

    @Override // org.signal.libsignal.protocol.state.SignedPreKeyStore
    public void removeSignedPreKey(int i) {
        SignalDatabase.signedPreKeys().delete(this.accountId, i);
    }
}
