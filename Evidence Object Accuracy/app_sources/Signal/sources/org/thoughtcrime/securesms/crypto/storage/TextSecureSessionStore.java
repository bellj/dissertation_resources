package org.thoughtcrime.securesms.crypto.storage;

import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.util.List;
import java.util.Set;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.NoSessionException;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.state.SessionRecord;
import org.thoughtcrime.securesms.conversation.ConversationItemBodyBubble$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.database.SessionDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.SignalServiceSessionStore;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes4.dex */
public class TextSecureSessionStore implements SignalServiceSessionStore {
    private static final Object LOCK = new Object();
    private static final String TAG = Log.tag(TextSecureSessionStore.class);
    private final ServiceId accountId;

    public TextSecureSessionStore(ServiceId serviceId) {
        this.accountId = serviceId;
    }

    @Override // org.signal.libsignal.protocol.state.SessionStore
    public SessionRecord loadSession(SignalProtocolAddress signalProtocolAddress) {
        synchronized (LOCK) {
            SessionRecord load = SignalDatabase.sessions().load(this.accountId, signalProtocolAddress);
            if (load != null) {
                return load;
            }
            String str = TAG;
            Log.w(str, "No existing session information found for " + signalProtocolAddress);
            return new SessionRecord();
        }
    }

    @Override // org.signal.libsignal.protocol.state.SessionStore
    public List<SessionRecord> loadExistingSessions(List<SignalProtocolAddress> list) throws NoSessionException {
        List<SessionRecord> load;
        synchronized (LOCK) {
            load = SignalDatabase.sessions().load(this.accountId, list);
            if (load.size() != list.size()) {
                String str = "Mismatch! Asked for " + list.size() + " sessions, but only found " + load.size() + "!";
                Log.w(TAG, str);
                throw new NoSessionException(str);
            } else if (Collection$EL.stream(load).anyMatch(new Predicate() { // from class: org.thoughtcrime.securesms.crypto.storage.TextSecureSessionStore$$ExternalSyntheticLambda0
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return ConversationItemBodyBubble$$ExternalSyntheticBackport0.m((SessionRecord) obj);
                }
            })) {
                throw new NoSessionException("Failed to find one or more sessions.");
            }
        }
        return load;
    }

    @Override // org.signal.libsignal.protocol.state.SessionStore
    public void storeSession(SignalProtocolAddress signalProtocolAddress, SessionRecord sessionRecord) {
        synchronized (LOCK) {
            SignalDatabase.sessions().store(this.accountId, signalProtocolAddress, sessionRecord);
        }
    }

    @Override // org.signal.libsignal.protocol.state.SessionStore
    public boolean containsSession(SignalProtocolAddress signalProtocolAddress) {
        boolean z;
        synchronized (LOCK) {
            SessionRecord load = SignalDatabase.sessions().load(this.accountId, signalProtocolAddress);
            z = load != null && load.hasSenderChain() && load.getSessionVersion() == 3;
        }
        return z;
    }

    @Override // org.signal.libsignal.protocol.state.SessionStore
    public void deleteSession(SignalProtocolAddress signalProtocolAddress) {
        synchronized (LOCK) {
            String str = TAG;
            Log.w(str, "Deleting session for " + signalProtocolAddress);
            SignalDatabase.sessions().delete(this.accountId, signalProtocolAddress);
        }
    }

    @Override // org.signal.libsignal.protocol.state.SessionStore
    public void deleteAllSessions(String str) {
        synchronized (LOCK) {
            String str2 = TAG;
            Log.w(str2, "Deleting all sessions for " + str);
            SignalDatabase.sessions().deleteAllFor(this.accountId, str);
        }
    }

    @Override // org.signal.libsignal.protocol.state.SessionStore
    public List<Integer> getSubDeviceSessions(String str) {
        List<Integer> subDevices;
        synchronized (LOCK) {
            subDevices = SignalDatabase.sessions().getSubDevices(this.accountId, str);
        }
        return subDevices;
    }

    @Override // org.whispersystems.signalservice.api.SignalServiceSessionStore
    public Set<SignalProtocolAddress> getAllAddressesWithActiveSessions(List<String> list) {
        Set<SignalProtocolAddress> set;
        synchronized (LOCK) {
            set = (Set) Collection$EL.stream(SignalDatabase.sessions().getAllFor(this.accountId, list)).filter(new Predicate() { // from class: org.thoughtcrime.securesms.crypto.storage.TextSecureSessionStore$$ExternalSyntheticLambda1
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return TextSecureSessionStore.lambda$getAllAddressesWithActiveSessions$0((SessionDatabase.SessionRow) obj);
                }
            }).map(new Function() { // from class: org.thoughtcrime.securesms.crypto.storage.TextSecureSessionStore$$ExternalSyntheticLambda2
                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return TextSecureSessionStore.lambda$getAllAddressesWithActiveSessions$1((SessionDatabase.SessionRow) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).collect(Collectors.toSet());
        }
        return set;
    }

    public static /* synthetic */ boolean lambda$getAllAddressesWithActiveSessions$0(SessionDatabase.SessionRow sessionRow) {
        return isActive(sessionRow.getRecord());
    }

    public static /* synthetic */ SignalProtocolAddress lambda$getAllAddressesWithActiveSessions$1(SessionDatabase.SessionRow sessionRow) {
        return new SignalProtocolAddress(sessionRow.getAddress(), sessionRow.getDeviceId());
    }

    @Override // org.whispersystems.signalservice.api.SignalServiceSessionStore
    public void archiveSession(SignalProtocolAddress signalProtocolAddress) {
        synchronized (LOCK) {
            SessionRecord load = SignalDatabase.sessions().load(this.accountId, signalProtocolAddress);
            if (load != null) {
                load.archiveCurrentState();
                SignalDatabase.sessions().store(this.accountId, signalProtocolAddress, load);
            }
        }
    }

    public void archiveSession(RecipientId recipientId, int i) {
        synchronized (LOCK) {
            Recipient resolved = Recipient.resolved(recipientId);
            if (resolved.hasServiceId()) {
                archiveSession(new SignalProtocolAddress(resolved.requireServiceId().toString(), i));
            }
            if (resolved.hasE164()) {
                archiveSession(new SignalProtocolAddress(resolved.requireE164(), i));
            }
        }
    }

    public void archiveSiblingSessions(SignalProtocolAddress signalProtocolAddress) {
        synchronized (LOCK) {
            for (SessionDatabase.SessionRow sessionRow : SignalDatabase.sessions().getAllFor(this.accountId, signalProtocolAddress.getName())) {
                if (sessionRow.getDeviceId() != signalProtocolAddress.getDeviceId()) {
                    sessionRow.getRecord().archiveCurrentState();
                    storeSession(new SignalProtocolAddress(sessionRow.getAddress(), sessionRow.getDeviceId()), sessionRow.getRecord());
                }
            }
        }
    }

    public void archiveAllSessions() {
        synchronized (LOCK) {
            for (SessionDatabase.SessionRow sessionRow : SignalDatabase.sessions().getAll(this.accountId)) {
                sessionRow.getRecord().archiveCurrentState();
                storeSession(new SignalProtocolAddress(sessionRow.getAddress(), sessionRow.getDeviceId()), sessionRow.getRecord());
            }
        }
    }

    private static boolean isActive(SessionRecord sessionRecord) {
        return sessionRecord != null && sessionRecord.hasSenderChain() && sessionRecord.getSessionVersion() == 3;
    }
}
