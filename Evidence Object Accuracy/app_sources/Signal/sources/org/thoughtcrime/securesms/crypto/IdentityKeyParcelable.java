package org.thoughtcrime.securesms.crypto;

import android.os.Parcel;
import android.os.Parcelable;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.thoughtcrime.securesms.util.ParcelUtil;

/* loaded from: classes4.dex */
public class IdentityKeyParcelable implements Parcelable {
    public static final Parcelable.Creator<IdentityKeyParcelable> CREATOR = new Parcelable.Creator<IdentityKeyParcelable>() { // from class: org.thoughtcrime.securesms.crypto.IdentityKeyParcelable.1
        @Override // android.os.Parcelable.Creator
        public IdentityKeyParcelable createFromParcel(Parcel parcel) {
            try {
                return new IdentityKeyParcelable(parcel);
            } catch (InvalidKeyException e) {
                throw new AssertionError(e);
            }
        }

        @Override // android.os.Parcelable.Creator
        public IdentityKeyParcelable[] newArray(int i) {
            return new IdentityKeyParcelable[i];
        }
    };
    private final IdentityKey identityKey;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public IdentityKeyParcelable(IdentityKey identityKey) {
        this.identityKey = identityKey;
    }

    public IdentityKeyParcelable(Parcel parcel) throws InvalidKeyException {
        byte[] readByteArray = ParcelUtil.readByteArray(parcel);
        this.identityKey = readByteArray != null ? new IdentityKey(readByteArray, 0) : null;
    }

    public IdentityKey get() {
        return this.identityKey;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        IdentityKey identityKey = this.identityKey;
        ParcelUtil.writeByteArray(parcel, identityKey != null ? identityKey.serialize() : null);
    }
}
