package org.thoughtcrime.securesms.crypto.storage;

import org.thoughtcrime.securesms.crypto.storage.SignalBaseIdentityKeyStore;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SignalBaseIdentityKeyStore$Cache$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ SignalBaseIdentityKeyStore.Cache f$0;
    public final /* synthetic */ String f$1;

    public /* synthetic */ SignalBaseIdentityKeyStore$Cache$$ExternalSyntheticLambda0(SignalBaseIdentityKeyStore.Cache cache, String str) {
        this.f$0 = cache;
        this.f$1 = str;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$delete$3(this.f$1);
    }
}
