package org.thoughtcrime.securesms.crypto;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.signal.core.util.Conversions;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.ecc.Curve;
import org.signal.libsignal.protocol.ecc.ECKeyPair;
import org.signal.libsignal.protocol.ecc.ECPublicKey;
import org.thoughtcrime.securesms.notifications.NotificationIds;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class AsymmetricMasterCipher {
    private final AsymmetricMasterSecret asymmetricMasterSecret;

    public AsymmetricMasterCipher(AsymmetricMasterSecret asymmetricMasterSecret) {
        this.asymmetricMasterSecret = asymmetricMasterSecret;
    }

    public byte[] encryptBytes(byte[] bArr) {
        try {
            ECPublicKey djbPublicKey = this.asymmetricMasterSecret.getDjbPublicKey();
            ECKeyPair generateKeyPair = Curve.generateKeyPair();
            return Util.combine(new PublicKey((int) NotificationIds.SMS_IMPORT_COMPLETE, generateKeyPair.getPublicKey()).serialize(), getMasterCipherForSecret(Curve.calculateAgreement(djbPublicKey, generateKeyPair.getPrivateKey())).encryptBytes(bArr));
        } catch (InvalidKeyException e) {
            throw new AssertionError(e);
        }
    }

    public byte[] decryptBytes(byte[] bArr) throws IOException, InvalidMessageException {
        try {
            byte[][] split = Util.split(bArr, 36, bArr.length - 36);
            PublicKey publicKey = new PublicKey(split[0], 0);
            return getMasterCipherForSecret(Curve.calculateAgreement(publicKey.getKey(), this.asymmetricMasterSecret.getPrivateKey())).decryptBytes(split[1]);
        } catch (InvalidKeyException e) {
            throw new InvalidMessageException(e);
        }
    }

    public String decryptBody(String str) throws IOException, InvalidMessageException {
        return new String(decryptBytes(Base64.decode(str)));
    }

    public String encryptBody(String str) {
        return Base64.encodeBytes(encryptBytes(str.getBytes()));
    }

    private MasterCipher getMasterCipherForSecret(byte[] bArr) {
        return new MasterCipher(new MasterSecret(deriveCipherKey(bArr), deriveMacKey(bArr)));
    }

    private SecretKeySpec deriveMacKey(byte[] bArr) {
        byte[] bArr2 = new byte[20];
        System.arraycopy(getDigestedBytes(bArr, 1), 0, bArr2, 0, 20);
        return new SecretKeySpec(bArr2, "HmacSHA1");
    }

    private SecretKeySpec deriveCipherKey(byte[] bArr) {
        byte[] bArr2 = new byte[16];
        System.arraycopy(getDigestedBytes(bArr, 0), 0, bArr2, 0, 16);
        return new SecretKeySpec(bArr2, "AES");
    }

    private byte[] getDigestedBytes(byte[] bArr, int i) {
        try {
            Mac instance = Mac.getInstance("HmacSHA256");
            instance.init(new SecretKeySpec(bArr, "HmacSHA256"));
            return instance.doFinal(Conversions.intToByteArray(i));
        } catch (java.security.InvalidKeyException | NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }
}
