package org.thoughtcrime.securesms.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ImageView;
import android.widget.PopupWindow;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts$StartActivityForResult;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.ActionMenuView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewGroupKt;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.NavOptions;
import androidx.navigation.ViewKt;
import androidx.navigation.fragment.FragmentNavigatorExtrasKt;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Iterator;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.components.Material3SearchToolbar;
import org.thoughtcrime.securesms.components.TooltipPopup;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsActivity;
import org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment;
import org.thoughtcrime.securesms.conversationlist.ConversationListFragment;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfiles;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.stories.tabs.ConversationListTab;
import org.thoughtcrime.securesms.stories.tabs.ConversationListTabsState;
import org.thoughtcrime.securesms.stories.tabs.ConversationListTabsViewModel;
import org.thoughtcrime.securesms.util.AvatarUtil;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.Material3OnScrollHelper;
import org.thoughtcrime.securesms.util.TopToastPopup;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.ViewsKt;
import org.thoughtcrime.securesms.util.views.Stub;
import org.whispersystems.signalservice.api.websocket.WebSocketConnectionState;

/* compiled from: MainActivityListHostFragment.kt */
@Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 O2\u00020\u00012\u00020\u00022\u00020\u0003:\u0002OPB\u0005¢\u0006\u0002\u0010\u0004J\u0010\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#H\u0016J\u0012\u0010$\u001a\u0004\u0018\u00010\u000e2\u0006\u0010%\u001a\u00020\u0007H\u0002J\u000e\u0010&\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u0016J\b\u0010'\u001a\u00020\tH\u0016J\u000e\u0010(\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0006H\u0016J\b\u0010)\u001a\u00020\u0007H\u0016J\b\u0010*\u001a\u00020\u000eH\u0016J\u0018\u0010+\u001a\u00020!2\u0006\u0010,\u001a\u00020-2\u0006\u0010.\u001a\u00020/H\u0002J\u0018\u00100\u001a\u00020!2\u0006\u0010,\u001a\u00020-2\u0006\u0010.\u001a\u00020/H\u0002J\b\u00101\u001a\u00020!H\u0002J\u0010\u00102\u001a\u00020!2\u0006\u00103\u001a\u000204H\u0002J\b\u00105\u001a\u00020!H\u0002J\b\u00106\u001a\u00020!H\u0016J\b\u00107\u001a\u00020!H\u0016J\b\u00108\u001a\u00020!H\u0016J\b\u00109\u001a\u00020!H\u0016J\b\u0010:\u001a\u00020!H\u0002J\b\u0010;\u001a\u00020!H\u0016J\b\u0010<\u001a\u00020!H\u0016J\b\u0010=\u001a\u00020!H\u0016J\u001a\u0010>\u001a\u00020!2\u0006\u0010?\u001a\u00020\u000e2\b\u0010@\u001a\u0004\u0018\u00010AH\u0016J\b\u0010B\u001a\u00020!H\u0002J\b\u0010C\u001a\u00020!H\u0002J\u0010\u0010D\u001a\u00020!2\u0006\u0010E\u001a\u00020FH\u0002J\b\u0010G\u001a\u00020!H\u0002J\b\u0010H\u001a\u00020!H\u0002J\u0016\u0010I\u001a\u00020!2\f\u0010J\u001a\b\u0012\u0004\u0012\u00020L0KH\u0016J\u0010\u0010M\u001a\u00020!2\u0006\u0010,\u001a\u00020NH\u0016R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X.¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX.¢\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0006X.¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0007X.¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX.¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000eX.¢\u0006\u0002\n\u0000R\u001b\u0010\u0010\u001a\u00020\u00118BX\u0002¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0012\u0010\u0013R\u0012\u0010\u0016\u001a\u00060\u0017R\u00020\u0000X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\tX.¢\u0006\u0002\n\u0000R\u001c\u0010\u0019\u001a\u0010\u0012\f\u0012\n \u001c*\u0004\u0018\u00010\u001b0\u001b0\u001aX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u001d\u001a\u0004\u0018\u00010\u001eX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\tX.¢\u0006\u0002\n\u0000¨\u0006Q"}, d2 = {"Lorg/thoughtcrime/securesms/main/MainActivityListHostFragment;", "Landroidx/fragment/app/Fragment;", "Lorg/thoughtcrime/securesms/conversationlist/ConversationListFragment$Callback;", "Lorg/thoughtcrime/securesms/main/Material3OnScrollHelperBinder;", "()V", "_basicToolbar", "Lorg/thoughtcrime/securesms/util/views/Stub;", "Landroidx/appcompat/widget/Toolbar;", "_searchAction", "Landroid/widget/ImageView;", "_searchToolbar", "Lorg/thoughtcrime/securesms/components/Material3SearchToolbar;", "_toolbar", "_toolbarBackground", "Landroid/view/View;", "_unreadPaymentsDot", "conversationListTabsViewModel", "Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabsViewModel;", "getConversationListTabsViewModel", "()Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabsViewModel;", "conversationListTabsViewModel$delegate", "Lkotlin/Lazy;", "destinationChangedListener", "Lorg/thoughtcrime/securesms/main/MainActivityListHostFragment$DestinationChangedListener;", "notificationProfileStatus", "openSettings", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "kotlin.jvm.PlatformType", "previousTopToastPopup", "Lorg/thoughtcrime/securesms/util/TopToastPopup;", "proxyStatus", "bindScrollHelper", "", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "findOverflowMenuButton", "viewGroup", "getBasicToolbar", "getSearchAction", "getSearchToolbar", "getToolbar", "getUnreadPaymentsDot", "goToStateFromConversationList", "state", "Lorg/thoughtcrime/securesms/stories/tabs/ConversationListTabsState;", "navController", "Landroidx/navigation/NavController;", "goToStateFromStories", "handleNotificationProfile", "initializeProfileIcon", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "initializeSettingsTouchTarget", "onDestroyView", "onMultiSelectFinished", "onMultiSelectStarted", "onPause", "onProxyStatusClicked", "onResume", "onSearchClosed", "onSearchOpened", "onViewCreated", "view", "savedInstanceState", "Landroid/os/Bundle;", "presentToolbarForConversationListArchiveFragment", "presentToolbarForConversationListFragment", "presentToolbarForDestination", "destination", "Landroidx/navigation/NavDestination;", "presentToolbarForMultiselect", "presentToolbarForStoriesLandingFragment", "updateNotificationProfileStatus", "notificationProfiles", "", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", "updateProxyStatus", "Lorg/whispersystems/signalservice/api/websocket/WebSocketConnectionState;", "Companion", "DestinationChangedListener", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MainActivityListHostFragment extends Fragment implements ConversationListFragment.Callback, Material3OnScrollHelperBinder {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(MainActivityListHostFragment.class);
    private Stub<Toolbar> _basicToolbar;
    private ImageView _searchAction;
    private Stub<Material3SearchToolbar> _searchToolbar;
    private Toolbar _toolbar;
    private View _toolbarBackground;
    private View _unreadPaymentsDot;
    private final Lazy conversationListTabsViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(ConversationListTabsViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.main.MainActivityListHostFragment$conversationListTabsViewModel$2
        final /* synthetic */ MainActivityListHostFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            FragmentActivity requireActivity = this.this$0.requireActivity();
            Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
            return requireActivity;
        }
    }) { // from class: org.thoughtcrime.securesms.main.MainActivityListHostFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);
    private final DestinationChangedListener destinationChangedListener = new DestinationChangedListener();
    private ImageView notificationProfileStatus;
    private final ActivityResultLauncher<Intent> openSettings;
    private TopToastPopup previousTopToastPopup;
    private ImageView proxyStatus;

    /* compiled from: MainActivityListHostFragment.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[WebSocketConnectionState.values().length];
            iArr[WebSocketConnectionState.CONNECTING.ordinal()] = 1;
            iArr[WebSocketConnectionState.DISCONNECTING.ordinal()] = 2;
            iArr[WebSocketConnectionState.DISCONNECTED.ordinal()] = 3;
            iArr[WebSocketConnectionState.CONNECTED.ordinal()] = 4;
            iArr[WebSocketConnectionState.AUTHENTICATION_FAILED.ordinal()] = 5;
            iArr[WebSocketConnectionState.FAILED.ordinal()] = 6;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public MainActivityListHostFragment() {
        super(R.layout.main_activity_list_host_fragment);
        ActivityResultLauncher<Intent> registerForActivityResult = registerForActivityResult(new ActivityResultContracts$StartActivityForResult(), new ActivityResultCallback() { // from class: org.thoughtcrime.securesms.main.MainActivityListHostFragment$$ExternalSyntheticLambda3
            @Override // androidx.activity.result.ActivityResultCallback
            public final void onActivityResult(Object obj) {
                MainActivityListHostFragment.m2035$r8$lambda$C5IWIMxLcGYBUZXD8Hiup7LpUs(MainActivityListHostFragment.this, (ActivityResult) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(registerForActivityResult, "registerForActivityResul…ty().recreate()\n    }\n  }");
        this.openSettings = registerForActivityResult;
    }

    /* compiled from: MainActivityListHostFragment.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/main/MainActivityListHostFragment$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    private final ConversationListTabsViewModel getConversationListTabsViewModel() {
        return (ConversationListTabsViewModel) this.conversationListTabsViewModel$delegate.getValue();
    }

    /* renamed from: openSettings$lambda-0 */
    public static final void m2043openSettings$lambda0(MainActivityListHostFragment mainActivityListHostFragment, ActivityResult activityResult) {
        Intrinsics.checkNotNullParameter(mainActivityListHostFragment, "this$0");
        if (activityResult.getResultCode() == 902) {
            mainActivityListHostFragment.requireActivity().recreate();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.toolbar_background);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.toolbar_background)");
        this._toolbarBackground = findViewById;
        View findViewById2 = view.findViewById(R.id.toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.toolbar)");
        this._toolbar = (Toolbar) findViewById2;
        this._basicToolbar = new Stub<>((ViewStub) view.findViewById(R.id.toolbar_basic_stub));
        View findViewById3 = view.findViewById(R.id.conversation_list_notification_profile_status);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.c…ification_profile_status)");
        this.notificationProfileStatus = (ImageView) findViewById3;
        View findViewById4 = view.findViewById(R.id.conversation_list_proxy_status);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.c…sation_list_proxy_status)");
        this.proxyStatus = (ImageView) findViewById4;
        View findViewById5 = view.findViewById(R.id.search_action);
        Intrinsics.checkNotNullExpressionValue(findViewById5, "view.findViewById(R.id.search_action)");
        this._searchAction = (ImageView) findViewById5;
        this._searchToolbar = new Stub<>((ViewStub) view.findViewById(R.id.search_toolbar));
        View findViewById6 = view.findViewById(R.id.unread_payments_indicator);
        Intrinsics.checkNotNullExpressionValue(findViewById6, "view.findViewById(R.id.unread_payments_indicator)");
        this._unreadPaymentsDot = findViewById6;
        ImageView imageView = this.notificationProfileStatus;
        Toolbar toolbar = null;
        if (imageView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("notificationProfileStatus");
            imageView = null;
        }
        imageView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.main.MainActivityListHostFragment$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                MainActivityListHostFragment.$r8$lambda$5YYcpDMapWHIcVwo1AlzGfmmrqg(MainActivityListHostFragment.this, view2);
            }
        });
        ImageView imageView2 = this.proxyStatus;
        if (imageView2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("proxyStatus");
            imageView2 = null;
        }
        imageView2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.main.MainActivityListHostFragment$$ExternalSyntheticLambda5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                MainActivityListHostFragment.$r8$lambda$31h9gOigbE75n4vN_0Ho4QEWszE(MainActivityListHostFragment.this, view2);
            }
        });
        initializeSettingsTouchTarget();
        AppCompatActivity appCompatActivity = (AppCompatActivity) requireActivity();
        Toolbar toolbar2 = this._toolbar;
        if (toolbar2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("_toolbar");
        } else {
            toolbar = toolbar2;
        }
        appCompatActivity.setSupportActionBar(toolbar);
        getConversationListTabsViewModel().getState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.main.MainActivityListHostFragment$$ExternalSyntheticLambda6
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                MainActivityListHostFragment.m2037$r8$lambda$tYkDL5x9Hqxl35lcvmG7ok0BzY(MainActivityListHostFragment.this, (ConversationListTabsState) obj);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m2040onViewCreated$lambda1(MainActivityListHostFragment mainActivityListHostFragment, View view) {
        Intrinsics.checkNotNullParameter(mainActivityListHostFragment, "this$0");
        mainActivityListHostFragment.handleNotificationProfile();
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final void m2041onViewCreated$lambda2(MainActivityListHostFragment mainActivityListHostFragment, View view) {
        Intrinsics.checkNotNullParameter(mainActivityListHostFragment, "this$0");
        mainActivityListHostFragment.onProxyStatusClicked();
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final void m2042onViewCreated$lambda3(MainActivityListHostFragment mainActivityListHostFragment, ConversationListTabsState conversationListTabsState) {
        Intrinsics.checkNotNullParameter(mainActivityListHostFragment, "this$0");
        View findViewById = mainActivityListHostFragment.requireView().findViewById(R.id.fragment_container);
        Intrinsics.checkNotNullExpressionValue(findViewById, "requireView().findViewBy…(R.id.fragment_container)");
        NavController findNavController = ViewKt.findNavController(findViewById);
        NavDestination currentDestination = findNavController.getCurrentDestination();
        Integer valueOf = currentDestination != null ? Integer.valueOf(currentDestination.getId()) : null;
        if (valueOf != null && valueOf.intValue() == R.id.conversationListFragment) {
            Intrinsics.checkNotNullExpressionValue(conversationListTabsState, "state");
            mainActivityListHostFragment.goToStateFromConversationList(conversationListTabsState, findNavController);
        } else if ((valueOf == null || valueOf.intValue() != R.id.conversationListArchiveFragment) && valueOf != null && valueOf.intValue() == R.id.storiesLandingFragment) {
            Intrinsics.checkNotNullExpressionValue(conversationListTabsState, "state");
            mainActivityListHostFragment.goToStateFromStories(conversationListTabsState, findNavController);
        }
    }

    private final void goToStateFromConversationList(ConversationListTabsState conversationListTabsState, NavController navController) {
        if (conversationListTabsState.getTab() != ConversationListTab.CHATS) {
            View findViewById = requireView().findViewById(R.id.camera_fab);
            View findViewById2 = requireView().findViewById(R.id.fab);
            navController.navigate(R.id.action_conversationListFragment_to_storiesLandingFragment, (Bundle) null, (NavOptions) null, (findViewById == null || findViewById2 == null) ? null : FragmentNavigatorExtrasKt.FragmentNavigatorExtras(TuplesKt.to(findViewById, "camera_fab"), TuplesKt.to(findViewById2, "new_convo_fab")));
        }
    }

    private final void goToStateFromStories(ConversationListTabsState conversationListTabsState, NavController navController) {
        if (conversationListTabsState.getTab() != ConversationListTab.STORIES) {
            navController.popBackStack();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        SimpleTask.run(getViewLifecycleOwner().getLifecycle(), new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.main.MainActivityListHostFragment$$ExternalSyntheticLambda0
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return MainActivityListHostFragment.$r8$lambda$3szC2m3qdTwqHp6AbwQVdTYaDJ0();
            }
        }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.main.MainActivityListHostFragment$$ExternalSyntheticLambda1
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                MainActivityListHostFragment.$r8$lambda$ZQZkr5qGm_FH71EZU7zrj1uz5bw(MainActivityListHostFragment.this, (Recipient) obj);
            }
        });
        View findViewById = requireView().findViewById(R.id.fragment_container);
        Intrinsics.checkNotNullExpressionValue(findViewById, "requireView()\n      .fin…(R.id.fragment_container)");
        ViewKt.findNavController(findViewById).addOnDestinationChangedListener(this.destinationChangedListener);
    }

    /* renamed from: onResume$lambda-4 */
    public static final Recipient m2039onResume$lambda4() {
        return Recipient.self();
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        View findViewById = requireView().findViewById(R.id.fragment_container);
        Intrinsics.checkNotNullExpressionValue(findViewById, "requireView()\n      .fin…(R.id.fragment_container)");
        ViewKt.findNavController(findViewById).removeOnDestinationChangedListener(this.destinationChangedListener);
    }

    private final void presentToolbarForConversationListFragment() {
        Stub<Toolbar> stub = this._basicToolbar;
        Stub<Toolbar> stub2 = null;
        if (stub == null) {
            Intrinsics.throwUninitializedPropertyAccessException("_basicToolbar");
            stub = null;
        }
        if (stub.resolved()) {
            Stub<Toolbar> stub3 = this._basicToolbar;
            if (stub3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("_basicToolbar");
                stub3 = null;
            }
            Toolbar toolbar = stub3.get();
            Intrinsics.checkNotNullExpressionValue(toolbar, "_basicToolbar.get()");
            if (ViewExtensionsKt.getVisible(toolbar)) {
                Toolbar toolbar2 = this._toolbar;
                if (toolbar2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("_toolbar");
                    toolbar2 = null;
                }
                ViewsKt.runRevealAnimation(toolbar2, R.anim.slide_from_start);
            }
        }
        Toolbar toolbar3 = this._toolbar;
        if (toolbar3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("_toolbar");
            toolbar3 = null;
        }
        ViewExtensionsKt.setVisible(toolbar3, true);
        ImageView imageView = this._searchAction;
        if (imageView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("_searchAction");
            imageView = null;
        }
        ViewExtensionsKt.setVisible(imageView, true);
        Stub<Toolbar> stub4 = this._basicToolbar;
        if (stub4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("_basicToolbar");
            stub4 = null;
        }
        if (stub4.resolved()) {
            Stub<Toolbar> stub5 = this._basicToolbar;
            if (stub5 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("_basicToolbar");
                stub5 = null;
            }
            Toolbar toolbar4 = stub5.get();
            Intrinsics.checkNotNullExpressionValue(toolbar4, "_basicToolbar.get()");
            if (ViewExtensionsKt.getVisible(toolbar4)) {
                Stub<Toolbar> stub6 = this._basicToolbar;
                if (stub6 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("_basicToolbar");
                } else {
                    stub2 = stub6;
                }
                Toolbar toolbar5 = stub2.get();
                Intrinsics.checkNotNullExpressionValue(toolbar5, "_basicToolbar.get()");
                ViewsKt.runHideAnimation(toolbar5, R.anim.slide_to_end);
            }
        }
    }

    private final void presentToolbarForConversationListArchiveFragment() {
        Toolbar toolbar = this._toolbar;
        Stub<Toolbar> stub = null;
        if (toolbar == null) {
            Intrinsics.throwUninitializedPropertyAccessException("_toolbar");
            toolbar = null;
        }
        ViewsKt.runHideAnimation(toolbar, R.anim.slide_to_start);
        Stub<Toolbar> stub2 = this._basicToolbar;
        if (stub2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("_basicToolbar");
        } else {
            stub = stub2;
        }
        Toolbar toolbar2 = stub.get();
        Intrinsics.checkNotNullExpressionValue(toolbar2, "_basicToolbar.get()");
        ViewsKt.runRevealAnimation(toolbar2, R.anim.slide_from_end);
    }

    private final void presentToolbarForStoriesLandingFragment() {
        Toolbar toolbar = this._toolbar;
        Stub<Toolbar> stub = null;
        if (toolbar == null) {
            Intrinsics.throwUninitializedPropertyAccessException("_toolbar");
            toolbar = null;
        }
        ViewExtensionsKt.setVisible(toolbar, true);
        ImageView imageView = this._searchAction;
        if (imageView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("_searchAction");
            imageView = null;
        }
        ViewExtensionsKt.setVisible(imageView, false);
        Stub<Toolbar> stub2 = this._basicToolbar;
        if (stub2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("_basicToolbar");
            stub2 = null;
        }
        if (stub2.resolved()) {
            Stub<Toolbar> stub3 = this._basicToolbar;
            if (stub3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("_basicToolbar");
            } else {
                stub = stub3;
            }
            Toolbar toolbar2 = stub.get();
            Intrinsics.checkNotNullExpressionValue(toolbar2, "_basicToolbar.get()");
            ViewExtensionsKt.setVisible(toolbar2, false);
        }
    }

    private final void presentToolbarForMultiselect() {
        Toolbar toolbar = this._toolbar;
        Stub<Toolbar> stub = null;
        if (toolbar == null) {
            Intrinsics.throwUninitializedPropertyAccessException("_toolbar");
            toolbar = null;
        }
        ViewExtensionsKt.setVisible(toolbar, false);
        Stub<Toolbar> stub2 = this._basicToolbar;
        if (stub2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("_basicToolbar");
            stub2 = null;
        }
        if (stub2.resolved()) {
            Stub<Toolbar> stub3 = this._basicToolbar;
            if (stub3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("_basicToolbar");
            } else {
                stub = stub3;
            }
            Toolbar toolbar2 = stub.get();
            Intrinsics.checkNotNullExpressionValue(toolbar2, "_basicToolbar.get()");
            ViewExtensionsKt.setVisible(toolbar2, false);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        this.previousTopToastPopup = null;
        super.onDestroyView();
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListFragment.Callback
    public Toolbar getToolbar() {
        Toolbar toolbar = this._toolbar;
        if (toolbar != null) {
            return toolbar;
        }
        Intrinsics.throwUninitializedPropertyAccessException("_toolbar");
        return null;
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListFragment.Callback
    public ImageView getSearchAction() {
        ImageView imageView = this._searchAction;
        if (imageView != null) {
            return imageView;
        }
        Intrinsics.throwUninitializedPropertyAccessException("_searchAction");
        return null;
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListFragment.Callback
    public Stub<Material3SearchToolbar> getSearchToolbar() {
        Stub<Material3SearchToolbar> stub = this._searchToolbar;
        if (stub != null) {
            return stub;
        }
        Intrinsics.throwUninitializedPropertyAccessException("_searchToolbar");
        return null;
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListFragment.Callback
    public View getUnreadPaymentsDot() {
        View view = this._unreadPaymentsDot;
        if (view != null) {
            return view;
        }
        Intrinsics.throwUninitializedPropertyAccessException("_unreadPaymentsDot");
        return null;
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListFragment.Callback
    public Stub<Toolbar> getBasicToolbar() {
        Stub<Toolbar> stub = this._basicToolbar;
        if (stub != null) {
            return stub;
        }
        Intrinsics.throwUninitializedPropertyAccessException("_basicToolbar");
        return null;
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListFragment.Callback
    public void onSearchOpened() {
        getConversationListTabsViewModel().onSearchOpened();
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListFragment.Callback
    public void onSearchClosed() {
        getConversationListTabsViewModel().onSearchClosed();
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListFragment.Callback
    public void onMultiSelectStarted() {
        presentToolbarForMultiselect();
        getConversationListTabsViewModel().onMultiSelectStarted();
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListFragment.Callback
    public void onMultiSelectFinished() {
        View findViewById = requireView().findViewById(R.id.fragment_container);
        Intrinsics.checkNotNullExpressionValue(findViewById, "requireView().findViewBy…(R.id.fragment_container)");
        NavDestination currentDestination = ViewKt.findNavController(findViewById).getCurrentDestination();
        if (currentDestination != null) {
            presentToolbarForDestination(currentDestination);
        }
        getConversationListTabsViewModel().onMultiSelectFinished();
    }

    public final void initializeProfileIcon(Recipient recipient) {
        Log.d(TAG, "Initializing profile icon");
        View findViewById = requireView().findViewById(R.id.toolbar_badge);
        Intrinsics.checkNotNullExpressionValue(findViewById, "requireView().findViewById(R.id.toolbar_badge)");
        ((BadgeImageView) findViewById).setBadgeFromRecipient(recipient);
        AvatarUtil.loadIconIntoImageView(recipient, (ImageView) requireView().findViewById(R.id.toolbar_icon), getResources().getDimensionPixelSize(R.dimen.toolbar_avatar_size));
    }

    private final void initializeSettingsTouchTarget() {
        requireView().findViewById(R.id.toolbar_settings_touch_area).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.main.MainActivityListHostFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                MainActivityListHostFragment.$r8$lambda$cfDLsmqNFxKM4NYvHpYHSVSb_aA(MainActivityListHostFragment.this, view);
            }
        });
    }

    /* renamed from: initializeSettingsTouchTarget$lambda-5 */
    public static final void m2038initializeSettingsTouchTarget$lambda5(MainActivityListHostFragment mainActivityListHostFragment, View view) {
        Intrinsics.checkNotNullParameter(mainActivityListHostFragment, "this$0");
        ActivityResultLauncher<Intent> activityResultLauncher = mainActivityListHostFragment.openSettings;
        AppSettingsActivity.Companion companion = AppSettingsActivity.Companion;
        Context requireContext = mainActivityListHostFragment.requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        activityResultLauncher.launch(AppSettingsActivity.Companion.home$default(companion, requireContext, null, 2, null));
    }

    private final void handleNotificationProfile() {
        NotificationProfileSelectionFragment.Companion companion = NotificationProfileSelectionFragment.Companion;
        FragmentManager parentFragmentManager = getParentFragmentManager();
        Intrinsics.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
        companion.show(parentFragmentManager);
    }

    private final void onProxyStatusClicked() {
        AppSettingsActivity.Companion companion = AppSettingsActivity.Companion;
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        startActivity(companion.proxy(requireContext));
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListFragment.Callback
    public void updateProxyStatus(WebSocketConnectionState webSocketConnectionState) {
        Intrinsics.checkNotNullParameter(webSocketConnectionState, "state");
        ImageView imageView = null;
        if (SignalStore.proxy().isProxyEnabled()) {
            ImageView imageView2 = this.proxyStatus;
            if (imageView2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("proxyStatus");
                imageView2 = null;
            }
            imageView2.setVisibility(0);
            switch (WhenMappings.$EnumSwitchMapping$0[webSocketConnectionState.ordinal()]) {
                case 1:
                case 2:
                case 3:
                    ImageView imageView3 = this.proxyStatus;
                    if (imageView3 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("proxyStatus");
                    } else {
                        imageView = imageView3;
                    }
                    imageView.setImageResource(R.drawable.ic_proxy_connecting_24);
                    return;
                case 4:
                    ImageView imageView4 = this.proxyStatus;
                    if (imageView4 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("proxyStatus");
                    } else {
                        imageView = imageView4;
                    }
                    imageView.setImageResource(R.drawable.ic_proxy_connected_24);
                    return;
                case 5:
                case 6:
                    ImageView imageView5 = this.proxyStatus;
                    if (imageView5 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("proxyStatus");
                    } else {
                        imageView = imageView5;
                    }
                    imageView.setImageResource(R.drawable.ic_proxy_failed_24);
                    return;
                default:
                    ImageView imageView6 = this.proxyStatus;
                    if (imageView6 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("proxyStatus");
                    } else {
                        imageView = imageView6;
                    }
                    imageView.setVisibility(8);
                    return;
            }
        } else {
            ImageView imageView7 = this.proxyStatus;
            if (imageView7 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("proxyStatus");
            } else {
                imageView = imageView7;
            }
            imageView.setVisibility(8);
        }
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListFragment.Callback
    public void updateNotificationProfileStatus(List<NotificationProfile> list) {
        Intrinsics.checkNotNullParameter(list, "notificationProfiles");
        NotificationProfile activeProfile$default = NotificationProfiles.getActiveProfile$default(list, 0, null, 6, null);
        Toolbar toolbar = null;
        if (activeProfile$default != null) {
            if (activeProfile$default.getId() != SignalStore.notificationProfileValues().getLastProfilePopup()) {
                requireView().postDelayed(new Runnable(this) { // from class: org.thoughtcrime.securesms.main.MainActivityListHostFragment$$ExternalSyntheticLambda7
                    public final /* synthetic */ MainActivityListHostFragment f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        MainActivityListHostFragment.m2036$r8$lambda$MaUMhYbjYGVavWZd8ibz_Z0PRc(NotificationProfile.this, this.f$1);
                    }
                }, 500);
            }
            ImageView imageView = this.notificationProfileStatus;
            if (imageView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("notificationProfileStatus");
                imageView = null;
            }
            imageView.setVisibility(0);
        } else {
            ImageView imageView2 = this.notificationProfileStatus;
            if (imageView2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("notificationProfileStatus");
                imageView2 = null;
            }
            imageView2.setVisibility(8);
        }
        if (!SignalStore.notificationProfileValues().getHasSeenTooltip() && Util.hasItems(list)) {
            Toolbar toolbar2 = this._toolbar;
            if (toolbar2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("_toolbar");
            } else {
                toolbar = toolbar2;
            }
            View findOverflowMenuButton = findOverflowMenuButton(toolbar);
            if (findOverflowMenuButton != null) {
                TooltipPopup.forTarget(findOverflowMenuButton).setText(R.string.ConversationListFragment__turn_your_notification_profile_on_or_off_here).setBackgroundTint(ContextCompat.getColor(requireContext(), R.color.signal_button_primary)).setTextColor(ContextCompat.getColor(requireContext(), R.color.signal_button_primary_text)).setOnDismissListener(new PopupWindow.OnDismissListener() { // from class: org.thoughtcrime.securesms.main.MainActivityListHostFragment$$ExternalSyntheticLambda8
                    @Override // android.widget.PopupWindow.OnDismissListener
                    public final void onDismiss() {
                        MainActivityListHostFragment.$r8$lambda$ZMfrqZUR0LEsLkapMXGFTdRlfC8();
                    }
                }).show(1);
            } else {
                Log.w(TAG, "Unable to find overflow menu to show Notification Profile tooltip");
            }
        }
    }

    /* renamed from: updateNotificationProfileStatus$lambda-6 */
    public static final void m2044updateNotificationProfileStatus$lambda6(NotificationProfile notificationProfile, MainActivityListHostFragment mainActivityListHostFragment) {
        TopToastPopup topToastPopup;
        Intrinsics.checkNotNullParameter(mainActivityListHostFragment, "this$0");
        SignalStore.notificationProfileValues().setLastProfilePopup(notificationProfile.getId());
        SignalStore.notificationProfileValues().setLastProfilePopupTime(System.currentTimeMillis());
        TopToastPopup topToastPopup2 = mainActivityListHostFragment.previousTopToastPopup;
        if ((topToastPopup2 != null && topToastPopup2.isShowing()) && (topToastPopup = mainActivityListHostFragment.previousTopToastPopup) != null) {
            topToastPopup.dismiss();
        }
        ViewGroup viewGroup = (ViewGroup) mainActivityListHostFragment.requireView();
        Fragment findFragmentByTag = mainActivityListHostFragment.getParentFragmentManager().findFragmentByTag(BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
        if (!(findFragmentByTag == null || !findFragmentByTag.isAdded() || findFragmentByTag.getView() == null)) {
            viewGroup = (ViewGroup) findFragmentByTag.requireView();
        }
        try {
            TopToastPopup.Companion companion = TopToastPopup.Companion;
            String string = mainActivityListHostFragment.getString(R.string.ConversationListFragment__s_on, notificationProfile.getName());
            Intrinsics.checkNotNullExpressionValue(string, "getString(R.string.Conve…s_on, activeProfile.name)");
            mainActivityListHostFragment.previousTopToastPopup = companion.show(viewGroup, R.drawable.ic_moon_16, string);
        } catch (Exception e) {
            Log.w(TAG, "Unable to show toast popup", e);
        }
    }

    /* renamed from: updateNotificationProfileStatus$lambda-7 */
    public static final void m2045updateNotificationProfileStatus$lambda7() {
        SignalStore.notificationProfileValues().setHasSeenTooltip(true);
    }

    private final View findOverflowMenuButton(Toolbar toolbar) {
        View view;
        Iterator<View> it = ViewGroupKt.getChildren(toolbar).iterator();
        while (true) {
            if (!it.hasNext()) {
                view = null;
                break;
            }
            view = it.next();
            if (view instanceof ActionMenuView) {
                break;
            }
        }
        return view;
    }

    public final void presentToolbarForDestination(NavDestination navDestination) {
        switch (navDestination.getId()) {
            case R.id.conversationListArchiveFragment /* 2131362662 */:
                getConversationListTabsViewModel().isShowingArchived(true);
                presentToolbarForConversationListArchiveFragment();
                return;
            case R.id.conversationListFragment /* 2131362663 */:
                getConversationListTabsViewModel().isShowingArchived(false);
                presentToolbarForConversationListFragment();
                return;
            case R.id.storiesLandingFragment /* 2131364545 */:
                getConversationListTabsViewModel().isShowingArchived(false);
                presentToolbarForStoriesLandingFragment();
                return;
            default:
                return;
        }
    }

    /* compiled from: MainActivityListHostFragment.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0016¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/main/MainActivityListHostFragment$DestinationChangedListener;", "Landroidx/navigation/NavController$OnDestinationChangedListener;", "(Lorg/thoughtcrime/securesms/main/MainActivityListHostFragment;)V", "onDestinationChanged", "", "controller", "Landroidx/navigation/NavController;", "destination", "Landroidx/navigation/NavDestination;", "arguments", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    private final class DestinationChangedListener implements NavController.OnDestinationChangedListener {
        public DestinationChangedListener() {
            MainActivityListHostFragment.this = r1;
        }

        @Override // androidx.navigation.NavController.OnDestinationChangedListener
        public void onDestinationChanged(NavController navController, NavDestination navDestination, Bundle bundle) {
            Intrinsics.checkNotNullParameter(navController, "controller");
            Intrinsics.checkNotNullParameter(navDestination, "destination");
            MainActivityListHostFragment.this.presentToolbarForDestination(navDestination);
        }
    }

    @Override // org.thoughtcrime.securesms.main.Material3OnScrollHelperBinder
    public void bindScrollHelper(RecyclerView recyclerView) {
        Intrinsics.checkNotNullParameter(recyclerView, "recyclerView");
        FragmentActivity requireActivity = requireActivity();
        Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
        View view = this._toolbarBackground;
        Stub<Material3SearchToolbar> stub = null;
        if (view == null) {
            Intrinsics.throwUninitializedPropertyAccessException("_toolbarBackground");
            view = null;
        }
        List list = CollectionsKt__CollectionsJVMKt.listOf(view);
        Stub<Material3SearchToolbar> stub2 = this._searchToolbar;
        if (stub2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("_searchToolbar");
        } else {
            stub = stub2;
        }
        new Material3OnScrollHelper(requireActivity, list, CollectionsKt__CollectionsJVMKt.listOf(stub)).attach(recyclerView);
    }
}
