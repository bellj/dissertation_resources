package org.thoughtcrime.securesms.main;

import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;

/* compiled from: Material3OnScrollHelperBinder.kt */
@Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/main/Material3OnScrollHelperBinder;", "", "bindScrollHelper", "", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public interface Material3OnScrollHelperBinder {
    void bindScrollHelper(RecyclerView recyclerView);
}
