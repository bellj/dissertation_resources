package org.thoughtcrime.securesms;

import android.content.Context;

/* loaded from: classes.dex */
public class MainFragment extends LoggingFragment {
    @Override // androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        super.onAttach(context);
        if (!(requireActivity() instanceof MainActivity)) {
            throw new IllegalStateException("Can only be used inside of MainActivity!");
        }
    }

    public MainNavigator getNavigator() {
        return MainNavigator.get(requireActivity());
    }
}
