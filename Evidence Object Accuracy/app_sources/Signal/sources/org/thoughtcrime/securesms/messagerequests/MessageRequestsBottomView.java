package org.thoughtcrime.securesms.messagerequests;

import android.content.Context;
import android.content.res.ColorStateList;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Group;
import androidx.core.text.HtmlCompat;
import com.google.android.material.button.MaterialButton;
import j$.util.function.Consumer;
import j$.util.stream.Stream;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.messagerequests.MessageRequestViewModel;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.Debouncer;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.HtmlUtil;
import org.thoughtcrime.securesms.util.views.LearnMoreTextView;

/* loaded from: classes4.dex */
public class MessageRequestsBottomView extends ConstraintLayout {
    private MaterialButton accept;
    private Group activeGroup;
    private MaterialButton bigDelete;
    private MaterialButton bigUnblock;
    private MaterialButton block;
    private Group blockedButtons;
    private View busyIndicator;
    private MaterialButton delete;
    private MaterialButton gv1Continue;
    private Group gv1MigrationButtons;
    private Group normalButtons;
    private LearnMoreTextView question;
    private final Debouncer showProgressDebouncer = new Debouncer(250);

    public MessageRequestsBottomView(Context context) {
        super(context);
    }

    public MessageRequestsBottomView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public MessageRequestsBottomView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @Override // android.view.View
    protected void onFinishInflate() {
        super.onFinishInflate();
        ViewGroup.inflate(getContext(), R.layout.message_request_bottom_bar, this);
        this.question = (LearnMoreTextView) findViewById(R.id.message_request_question);
        this.accept = (MaterialButton) findViewById(R.id.message_request_accept);
        this.block = (MaterialButton) findViewById(R.id.message_request_block);
        this.delete = (MaterialButton) findViewById(R.id.message_request_delete);
        this.bigDelete = (MaterialButton) findViewById(R.id.message_request_big_delete);
        this.bigUnblock = (MaterialButton) findViewById(R.id.message_request_big_unblock);
        this.gv1Continue = (MaterialButton) findViewById(R.id.message_request_gv1_migration);
        this.normalButtons = (Group) findViewById(R.id.message_request_normal_buttons);
        this.blockedButtons = (Group) findViewById(R.id.message_request_blocked_buttons);
        this.gv1MigrationButtons = (Group) findViewById(R.id.message_request_gv1_migration_buttons);
        this.busyIndicator = findViewById(R.id.message_request_busy_indicator);
        setWallpaperEnabled(false);
    }

    public void setMessageData(MessageRequestViewModel.MessageData messageData) {
        int i;
        Recipient recipient = messageData.getRecipient();
        this.question.setLearnMoreVisible(false);
        this.question.setOnLinkClickListener(null);
        switch (AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestState[messageData.getMessageState().ordinal()]) {
            case 1:
                if (recipient.isReleaseNotes()) {
                    i = R.string.MessageRequestBottomView_get_updates_and_news_from_s_you_wont_receive_any_updates_until_you_unblock_them;
                } else {
                    i = recipient.isRegistered() ? R.string.MessageRequestBottomView_do_you_want_to_let_s_message_you_wont_receive_any_messages_until_you_unblock_them : R.string.MessageRequestBottomView_do_you_want_to_let_s_message_you_wont_receive_any_messages_until_you_unblock_them_SMS;
                }
                this.question.setText(HtmlCompat.fromHtml(getContext().getString(i, HtmlUtil.bold(recipient.getShortDisplayName(getContext()))), 0));
                setActiveInactiveGroups(this.blockedButtons, this.normalButtons, this.gv1MigrationButtons);
                return;
            case 2:
                this.question.setText(R.string.MessageRequestBottomView_unblock_this_group_and_share_your_name_and_photo_with_its_members);
                setActiveInactiveGroups(this.blockedButtons, this.normalButtons, this.gv1MigrationButtons);
                return;
            case 3:
                this.question.setText(getContext().getString(R.string.MessageRequestBottomView_continue_your_conversation_with_s_and_share_your_name_and_photo, recipient.getShortDisplayName(getContext())));
                this.question.setLearnMoreVisible(true);
                this.question.setOnLinkClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestsBottomView$$ExternalSyntheticLambda4
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        MessageRequestsBottomView.this.lambda$setMessageData$0(view);
                    }
                });
                setActiveInactiveGroups(this.normalButtons, this.blockedButtons, this.gv1MigrationButtons);
                this.accept.setText(R.string.MessageRequestBottomView_continue);
                return;
            case 4:
                this.question.setText(R.string.MessageRequestBottomView_continue_your_conversation_with_this_group_and_share_your_name_and_photo);
                this.question.setLearnMoreVisible(true);
                this.question.setOnLinkClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestsBottomView$$ExternalSyntheticLambda5
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        MessageRequestsBottomView.this.lambda$setMessageData$1(view);
                    }
                });
                setActiveInactiveGroups(this.normalButtons, this.blockedButtons, this.gv1MigrationButtons);
                this.accept.setText(R.string.MessageRequestBottomView_continue);
                return;
            case 5:
                this.question.setText(R.string.MessageRequestBottomView_upgrade_this_group_to_activate_new_features);
                setActiveInactiveGroups(this.gv1MigrationButtons, this.normalButtons, this.blockedButtons);
                this.gv1Continue.setVisibility(0);
                return;
            case 6:
                this.question.setText(getContext().getString(R.string.MessageRequestBottomView_this_legacy_group_can_no_longer_be_used, Integer.valueOf(FeatureFlags.groupLimits().getHardLimit() - 1)));
                setActiveInactiveGroups(this.gv1MigrationButtons, this.normalButtons, this.blockedButtons);
                this.gv1Continue.setVisibility(8);
                return;
            case 7:
                this.question.setText(R.string.MessageRequestBottomView_do_you_want_to_join_this_group_they_wont_know_youve_seen_their_messages_until_you_accept);
                setActiveInactiveGroups(this.normalButtons, this.blockedButtons, this.gv1MigrationButtons);
                this.accept.setText(R.string.MessageRequestBottomView_accept);
                return;
            case 8:
                this.question.setText(R.string.MessageRequestBottomView_do_you_want_to_join_this_group_you_wont_see_their_messages);
                setActiveInactiveGroups(this.normalButtons, this.blockedButtons, this.gv1MigrationButtons);
                this.accept.setText(R.string.MessageRequestBottomView_accept);
                return;
            case 9:
                this.question.setText(R.string.MessageRequestBottomView_join_this_group_they_wont_know_youve_seen_their_messages_until_you_accept);
                setActiveInactiveGroups(this.normalButtons, this.blockedButtons, this.gv1MigrationButtons);
                this.accept.setText(R.string.MessageRequestBottomView_accept);
                return;
            case 10:
                this.question.setText(HtmlCompat.fromHtml(getContext().getString(R.string.MessageRequestBottomView_do_you_want_to_let_s_message_you_they_wont_know_youve_seen_their_messages_until_you_accept, HtmlUtil.bold(recipient.getShortDisplayName(getContext()))), 0));
                setActiveInactiveGroups(this.normalButtons, this.blockedButtons, this.gv1MigrationButtons);
                this.accept.setText(R.string.MessageRequestBottomView_accept);
                return;
            default:
                return;
        }
    }

    /* renamed from: org.thoughtcrime.securesms.messagerequests.MessageRequestsBottomView$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestState;

        static {
            int[] iArr = new int[MessageRequestState.values().length];
            $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestState = iArr;
            try {
                iArr[MessageRequestState.BLOCKED_INDIVIDUAL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestState[MessageRequestState.BLOCKED_GROUP.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestState[MessageRequestState.LEGACY_INDIVIDUAL.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestState[MessageRequestState.LEGACY_GROUP_V1.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestState[MessageRequestState.DEPRECATED_GROUP_V1.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestState[MessageRequestState.DEPRECATED_GROUP_V1_TOO_LARGE.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestState[MessageRequestState.GROUP_V1.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestState[MessageRequestState.GROUP_V2_INVITE.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestState[MessageRequestState.GROUP_V2_ADD.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestState[MessageRequestState.INDIVIDUAL.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
        }
    }

    public /* synthetic */ void lambda$setMessageData$0(View view) {
        CommunicationActions.openBrowserLink(getContext(), getContext().getString(R.string.MessageRequestBottomView_legacy_learn_more_url));
    }

    public /* synthetic */ void lambda$setMessageData$1(View view) {
        CommunicationActions.openBrowserLink(getContext(), getContext().getString(R.string.MessageRequestBottomView_legacy_learn_more_url));
    }

    private void setActiveInactiveGroups(Group group, Group... groupArr) {
        Group group2 = this.activeGroup;
        int visibility = group2 != null ? group2.getVisibility() : 0;
        this.activeGroup = group;
        for (Group group3 : groupArr) {
            group3.setVisibility(8);
        }
        group.setVisibility(visibility);
    }

    public /* synthetic */ void lambda$showBusy$2() {
        this.busyIndicator.setVisibility(0);
    }

    public void showBusy() {
        this.showProgressDebouncer.publish(new Runnable() { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestsBottomView$$ExternalSyntheticLambda3
            @Override // java.lang.Runnable
            public final void run() {
                MessageRequestsBottomView.this.lambda$showBusy$2();
            }
        });
        Group group = this.activeGroup;
        if (group != null) {
            group.setVisibility(4);
        }
    }

    public void hideBusy() {
        this.showProgressDebouncer.clear();
        this.busyIndicator.setVisibility(8);
        Group group = this.activeGroup;
        if (group != null) {
            group.setVisibility(0);
        }
    }

    public void setWallpaperEnabled(boolean z) {
        MessageRequestBarColorTheme resolveTheme = MessageRequestBarColorTheme.resolveTheme(z);
        Stream.CC.of((Object[]) new MaterialButton[]{this.delete, this.bigDelete, this.block, this.bigUnblock, this.accept, this.gv1Continue}).forEach(new Consumer(resolveTheme) { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestsBottomView$$ExternalSyntheticLambda0
            public final /* synthetic */ MessageRequestBarColorTheme f$1;

            {
                this.f$1 = r2;
            }

            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                MessageRequestsBottomView.this.lambda$setWallpaperEnabled$3(this.f$1, (MaterialButton) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ Consumer andThen(Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
        Stream.CC.of((Object[]) new MaterialButton[]{this.delete, this.bigDelete, this.block}).forEach(new Consumer(resolveTheme) { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestsBottomView$$ExternalSyntheticLambda1
            public final /* synthetic */ MessageRequestBarColorTheme f$1;

            {
                this.f$1 = r2;
            }

            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                MessageRequestsBottomView.this.lambda$setWallpaperEnabled$4(this.f$1, (MaterialButton) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ Consumer andThen(Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
        Stream.CC.of((Object[]) new MaterialButton[]{this.accept, this.bigUnblock, this.gv1Continue}).forEach(new Consumer(resolveTheme) { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestsBottomView$$ExternalSyntheticLambda2
            public final /* synthetic */ MessageRequestBarColorTheme f$1;

            {
                this.f$1 = r2;
            }

            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                MessageRequestsBottomView.this.lambda$setWallpaperEnabled$5(this.f$1, (MaterialButton) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ Consumer andThen(Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
        setBackgroundColor(resolveTheme.getContainerButtonBackgroundColor(getContext()));
    }

    public /* synthetic */ void lambda$setWallpaperEnabled$3(MessageRequestBarColorTheme messageRequestBarColorTheme, MaterialButton materialButton) {
        materialButton.setBackgroundTintList(ColorStateList.valueOf(messageRequestBarColorTheme.getButtonBackgroundColor(getContext())));
    }

    public /* synthetic */ void lambda$setWallpaperEnabled$4(MessageRequestBarColorTheme messageRequestBarColorTheme, MaterialButton materialButton) {
        materialButton.setTextColor(messageRequestBarColorTheme.getButtonForegroundDenyColor(getContext()));
    }

    public /* synthetic */ void lambda$setWallpaperEnabled$5(MessageRequestBarColorTheme messageRequestBarColorTheme, MaterialButton materialButton) {
        materialButton.setTextColor(messageRequestBarColorTheme.getButtonForegroundAcceptColor(getContext()));
    }

    public void setAcceptOnClickListener(View.OnClickListener onClickListener) {
        this.accept.setOnClickListener(onClickListener);
    }

    public void setDeleteOnClickListener(View.OnClickListener onClickListener) {
        this.delete.setOnClickListener(onClickListener);
        this.bigDelete.setOnClickListener(onClickListener);
    }

    public void setBlockOnClickListener(View.OnClickListener onClickListener) {
        this.block.setOnClickListener(onClickListener);
    }

    public void setUnblockOnClickListener(View.OnClickListener onClickListener) {
        this.bigUnblock.setOnClickListener(onClickListener);
    }

    public void setGroupV1MigrationContinueListener(View.OnClickListener onClickListener) {
        this.gv1Continue.setOnClickListener(onClickListener);
    }
}
