package org.thoughtcrime.securesms.messagerequests;

import android.app.Application;
import android.content.Context;
import androidx.core.util.Consumer;
import j$.util.function.Function;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.Executor;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupChangeException;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.groups.ui.GroupChangeErrorCallback;
import org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason;
import org.thoughtcrime.securesms.jobs.MultiDeviceMessageRequestResponseJob;
import org.thoughtcrime.securesms.jobs.ReportSpamJob;
import org.thoughtcrime.securesms.jobs.SendViewedReceiptJob;
import org.thoughtcrime.securesms.notifications.MarkReadReceiver;
import org.thoughtcrime.securesms.recipients.LiveRecipient;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public final class MessageRequestRepository {
    private static final String TAG = Log.tag(MessageRequestRepository.class);
    private final Context context;
    private final Executor executor = SignalExecutors.BOUNDED;

    public MessageRequestRepository(Context context) {
        this.context = context.getApplicationContext();
    }

    public void getGroups(RecipientId recipientId, Consumer<List<String>> consumer) {
        this.executor.execute(new Runnable(recipientId) { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestRepository$$ExternalSyntheticLambda6
            public final /* synthetic */ RecipientId f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MessageRequestRepository.$r8$lambda$j4WKXOGMuiTkbWeOv_qGLM23c7g(Consumer.this, this.f$1);
            }
        });
    }

    public static /* synthetic */ void lambda$getGroups$0(Consumer consumer, RecipientId recipientId) {
        consumer.accept(SignalDatabase.groups().getPushGroupNamesContainingMember(recipientId));
    }

    public void getGroupInfo(RecipientId recipientId, Consumer<GroupInfo> consumer) {
        this.executor.execute(new Runnable(consumer) { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestRepository$$ExternalSyntheticLambda7
            public final /* synthetic */ Consumer f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MessageRequestRepository.m2328$r8$lambda$qFdumtsGJZMYnFLQpEJb8w4_tE(RecipientId.this, this.f$1);
            }
        });
    }

    public static /* synthetic */ void lambda$getGroupInfo$2(RecipientId recipientId, Consumer consumer) {
        consumer.accept((GroupInfo) SignalDatabase.groups().getGroup(recipientId).map(new Function() { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestRepository$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return MessageRequestRepository.$r8$lambda$MlBOzLo3Q4c5i8McV9O9BwxI3i4((GroupDatabase.GroupRecord) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(GroupInfo.ZERO));
    }

    public static /* synthetic */ GroupInfo lambda$getGroupInfo$1(GroupDatabase.GroupRecord groupRecord) {
        if (!groupRecord.isV2Group()) {
            return new GroupInfo(groupRecord.getMembers().size(), 0, "");
        }
        DecryptedGroup decryptedGroup = groupRecord.requireV2GroupProperties().getDecryptedGroup();
        return new GroupInfo(decryptedGroup.getMembersCount(), decryptedGroup.getPendingMembersCount(), decryptedGroup.getDescription());
    }

    public MessageRequestState getMessageRequestState(Recipient recipient, long j) {
        if (recipient.isBlocked()) {
            if (recipient.isGroup()) {
                return MessageRequestState.BLOCKED_GROUP;
            }
            return MessageRequestState.BLOCKED_INDIVIDUAL;
        } else if (j <= 0) {
            return MessageRequestState.NONE;
        } else {
            if (recipient.isPushV2Group()) {
                int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$database$GroupDatabase$MemberLevel[getGroupMemberLevel(recipient.getId()).ordinal()];
                if (i == 1) {
                    return MessageRequestState.NONE;
                }
                if (i == 2) {
                    return MessageRequestState.GROUP_V2_INVITE;
                }
                if (RecipientUtil.isMessageRequestAccepted(this.context, j)) {
                    return MessageRequestState.NONE;
                }
                return MessageRequestState.GROUP_V2_ADD;
            } else if (RecipientUtil.isLegacyProfileSharingAccepted(recipient) || !isLegacyThread(recipient)) {
                if (recipient.isPushV1Group()) {
                    if (RecipientUtil.isMessageRequestAccepted(this.context, j)) {
                        if (recipient.getParticipantIds().size() > FeatureFlags.groupLimits().getHardLimit()) {
                            return MessageRequestState.DEPRECATED_GROUP_V1_TOO_LARGE;
                        }
                        return MessageRequestState.DEPRECATED_GROUP_V1;
                    } else if (!recipient.isActiveGroup()) {
                        return MessageRequestState.NONE;
                    } else {
                        return MessageRequestState.GROUP_V1;
                    }
                } else if (RecipientUtil.isMessageRequestAccepted(this.context, j)) {
                    return MessageRequestState.NONE;
                } else {
                    return MessageRequestState.INDIVIDUAL;
                }
            } else if (recipient.isGroup()) {
                return MessageRequestState.LEGACY_GROUP_V1;
            } else {
                return MessageRequestState.LEGACY_INDIVIDUAL;
            }
        }
    }

    /* renamed from: org.thoughtcrime.securesms.messagerequests.MessageRequestRepository$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$database$GroupDatabase$MemberLevel;

        static {
            int[] iArr = new int[GroupDatabase.MemberLevel.values().length];
            $SwitchMap$org$thoughtcrime$securesms$database$GroupDatabase$MemberLevel = iArr;
            try {
                iArr[GroupDatabase.MemberLevel.NOT_A_MEMBER.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$GroupDatabase$MemberLevel[GroupDatabase.MemberLevel.PENDING_MEMBER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    public void acceptMessageRequest(LiveRecipient liveRecipient, long j, Runnable runnable, GroupChangeErrorCallback groupChangeErrorCallback) {
        this.executor.execute(new Runnable(liveRecipient, runnable, groupChangeErrorCallback, j) { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ LiveRecipient f$1;
            public final /* synthetic */ Runnable f$2;
            public final /* synthetic */ GroupChangeErrorCallback f$3;
            public final /* synthetic */ long f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MessageRequestRepository.m2327$r8$lambda$ZhpaCXPynjHX64k2kX8J8XTxBQ(MessageRequestRepository.this, this.f$1, this.f$2, this.f$3, this.f$4);
            }
        });
    }

    public /* synthetic */ void lambda$acceptMessageRequest$3(LiveRecipient liveRecipient, Runnable runnable, GroupChangeErrorCallback groupChangeErrorCallback, long j) {
        if (liveRecipient.get().isPushV2Group()) {
            try {
                Log.i(TAG, "GV2 accepting invite");
                GroupManager.acceptInvite(this.context, liveRecipient.get().requireGroupId().requireV2());
                SignalDatabase.recipients().setProfileSharing(liveRecipient.getId(), true);
                runnable.run();
            } catch (IOException | GroupChangeException e) {
                Log.w(TAG, e);
                groupChangeErrorCallback.onError(GroupChangeFailureReason.fromException(e));
            }
        } else {
            SignalDatabase.recipients().setProfileSharing(liveRecipient.getId(), true);
            MessageSender.sendProfileKey(j);
            List<MessageDatabase.MarkedMessageInfo> entireThreadRead = SignalDatabase.threads().setEntireThreadRead(j);
            ApplicationDependencies.getMessageNotifier().updateNotification(this.context);
            MarkReadReceiver.process(this.context, entireThreadRead);
            SendViewedReceiptJob.enqueue(j, liveRecipient.getId(), SignalDatabase.mms().getViewedIncomingMessages(j));
            if (TextSecurePreferences.isMultiDevice(this.context)) {
                ApplicationDependencies.getJobManager().add(MultiDeviceMessageRequestResponseJob.forAccept(liveRecipient.getId()));
            }
            runnable.run();
        }
    }

    public void deleteMessageRequest(LiveRecipient liveRecipient, long j, Runnable runnable, GroupChangeErrorCallback groupChangeErrorCallback) {
        this.executor.execute(new Runnable(liveRecipient, groupChangeErrorCallback, j, runnable) { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestRepository$$ExternalSyntheticLambda8
            public final /* synthetic */ LiveRecipient f$1;
            public final /* synthetic */ GroupChangeErrorCallback f$2;
            public final /* synthetic */ long f$3;
            public final /* synthetic */ Runnable f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r6;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MessageRequestRepository.$r8$lambda$bDD_QrEe1QIVbp2R7hu5NDrJIhw(MessageRequestRepository.this, this.f$1, this.f$2, this.f$3, this.f$4);
            }
        });
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x006b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ void lambda$deleteMessageRequest$4(org.thoughtcrime.securesms.recipients.LiveRecipient r5, org.thoughtcrime.securesms.groups.ui.GroupChangeErrorCallback r6, long r7, java.lang.Runnable r9) {
        /*
            r4 = this;
            org.thoughtcrime.securesms.recipients.Recipient r0 = r5.resolve()
            boolean r1 = r0.isGroup()
            if (r1 == 0) goto L_0x0063
            org.thoughtcrime.securesms.groups.GroupId r1 = r0.requireGroupId()
            boolean r1 = r1.isPush()
            if (r1 == 0) goto L_0x0063
            android.content.Context r1 = r4.context     // Catch: GroupChangeException -> 0x0032, GroupPatchNotAcceptedException -> 0x0030, IOException -> 0x0022
            org.thoughtcrime.securesms.groups.GroupId r2 = r0.requireGroupId()     // Catch: GroupChangeException -> 0x0032, GroupPatchNotAcceptedException -> 0x0030, IOException -> 0x0022
            org.thoughtcrime.securesms.groups.GroupId$Push r2 = r2.requirePush()     // Catch: GroupChangeException -> 0x0032, GroupPatchNotAcceptedException -> 0x0030, IOException -> 0x0022
            org.thoughtcrime.securesms.groups.GroupManager.leaveGroupFromBlockOrMessageRequest(r1, r2)     // Catch: GroupChangeException -> 0x0032, GroupPatchNotAcceptedException -> 0x0030, IOException -> 0x0022
            goto L_0x0063
        L_0x0022:
            r5 = move-exception
            java.lang.String r7 = org.thoughtcrime.securesms.messagerequests.MessageRequestRepository.TAG
            org.signal.core.util.logging.Log.w(r7, r5)
            org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason r5 = org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason.fromException(r5)
            r6.onError(r5)
            return
        L_0x0030:
            r1 = move-exception
            goto L_0x0033
        L_0x0032:
            r1 = move-exception
        L_0x0033:
            org.thoughtcrime.securesms.database.GroupDatabase r2 = org.thoughtcrime.securesms.database.SignalDatabase.groups()
            org.thoughtcrime.securesms.groups.GroupId r0 = r0.requireGroupId()
            org.thoughtcrime.securesms.groups.GroupId$Push r0 = r0.requirePush()
            org.thoughtcrime.securesms.recipients.Recipient r3 = org.thoughtcrime.securesms.recipients.Recipient.self()
            org.thoughtcrime.securesms.recipients.RecipientId r3 = r3.getId()
            boolean r0 = r2.isCurrentMember(r0, r3)
            if (r0 == 0) goto L_0x005c
            java.lang.String r5 = org.thoughtcrime.securesms.messagerequests.MessageRequestRepository.TAG
            java.lang.String r7 = "Failed to leave group, and we're still a member."
            org.signal.core.util.logging.Log.w(r5, r7, r1)
            org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason r5 = org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason.fromException(r1)
            r6.onError(r5)
            return
        L_0x005c:
            java.lang.String r6 = org.thoughtcrime.securesms.messagerequests.MessageRequestRepository.TAG
            java.lang.String r0 = "Failed to leave group, but we're not a member, so ignoring."
            org.signal.core.util.logging.Log.w(r6, r0)
        L_0x0063:
            android.content.Context r6 = r4.context
            boolean r6 = org.thoughtcrime.securesms.util.TextSecurePreferences.isMultiDevice(r6)
            if (r6 == 0) goto L_0x007a
            org.thoughtcrime.securesms.jobmanager.JobManager r6 = org.thoughtcrime.securesms.dependencies.ApplicationDependencies.getJobManager()
            org.thoughtcrime.securesms.recipients.RecipientId r5 = r5.getId()
            org.thoughtcrime.securesms.jobs.MultiDeviceMessageRequestResponseJob r5 = org.thoughtcrime.securesms.jobs.MultiDeviceMessageRequestResponseJob.forDelete(r5)
            r6.add(r5)
        L_0x007a:
            org.thoughtcrime.securesms.database.ThreadDatabase r5 = org.thoughtcrime.securesms.database.SignalDatabase.threads()
            r5.deleteConversation(r7)
            r9.run()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.messagerequests.MessageRequestRepository.lambda$deleteMessageRequest$4(org.thoughtcrime.securesms.recipients.LiveRecipient, org.thoughtcrime.securesms.groups.ui.GroupChangeErrorCallback, long, java.lang.Runnable):void");
    }

    public void blockMessageRequest(LiveRecipient liveRecipient, Runnable runnable, GroupChangeErrorCallback groupChangeErrorCallback) {
        this.executor.execute(new Runnable(liveRecipient, groupChangeErrorCallback, runnable) { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestRepository$$ExternalSyntheticLambda4
            public final /* synthetic */ LiveRecipient f$1;
            public final /* synthetic */ GroupChangeErrorCallback f$2;
            public final /* synthetic */ Runnable f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MessageRequestRepository.$r8$lambda$XcbtbIFlgG2EXlcF45k1hlBLV_o(MessageRequestRepository.this, this.f$1, this.f$2, this.f$3);
            }
        });
    }

    public /* synthetic */ void lambda$blockMessageRequest$5(LiveRecipient liveRecipient, GroupChangeErrorCallback groupChangeErrorCallback, Runnable runnable) {
        try {
            RecipientUtil.block(this.context, liveRecipient.resolve());
            liveRecipient.refresh();
            if (TextSecurePreferences.isMultiDevice(this.context)) {
                ApplicationDependencies.getJobManager().add(MultiDeviceMessageRequestResponseJob.forBlock(liveRecipient.getId()));
            }
            runnable.run();
        } catch (IOException | GroupChangeException e) {
            Log.w(TAG, e);
            groupChangeErrorCallback.onError(GroupChangeFailureReason.fromException(e));
        }
    }

    public void blockAndReportSpamMessageRequest(LiveRecipient liveRecipient, long j, Runnable runnable, GroupChangeErrorCallback groupChangeErrorCallback) {
        this.executor.execute(new Runnable(liveRecipient, groupChangeErrorCallback, j, runnable) { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ LiveRecipient f$1;
            public final /* synthetic */ GroupChangeErrorCallback f$2;
            public final /* synthetic */ long f$3;
            public final /* synthetic */ Runnable f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r6;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MessageRequestRepository.$r8$lambda$225jgj_ntu9O24aOOZAevM1slKU(MessageRequestRepository.this, this.f$1, this.f$2, this.f$3, this.f$4);
            }
        });
    }

    public /* synthetic */ void lambda$blockAndReportSpamMessageRequest$6(LiveRecipient liveRecipient, GroupChangeErrorCallback groupChangeErrorCallback, long j, Runnable runnable) {
        try {
            RecipientUtil.block(this.context, liveRecipient.resolve());
            liveRecipient.refresh();
            ApplicationDependencies.getJobManager().add(new ReportSpamJob(j, System.currentTimeMillis()));
            if (TextSecurePreferences.isMultiDevice(this.context)) {
                ApplicationDependencies.getJobManager().add(MultiDeviceMessageRequestResponseJob.forBlockAndReportSpam(liveRecipient.getId()));
            }
            runnable.run();
        } catch (IOException | GroupChangeException e) {
            Log.w(TAG, e);
            groupChangeErrorCallback.onError(GroupChangeFailureReason.fromException(e));
        }
    }

    public void unblockAndAccept(LiveRecipient liveRecipient, long j, Runnable runnable) {
        this.executor.execute(new Runnable(liveRecipient, runnable) { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestRepository$$ExternalSyntheticLambda5
            public final /* synthetic */ LiveRecipient f$1;
            public final /* synthetic */ Runnable f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MessageRequestRepository.$r8$lambda$hKimy73VcEcAfg8aA5zJlRmrSM4(MessageRequestRepository.this, this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$unblockAndAccept$7(LiveRecipient liveRecipient, Runnable runnable) {
        RecipientUtil.unblock(this.context, liveRecipient.resolve());
        if (TextSecurePreferences.isMultiDevice(this.context)) {
            ApplicationDependencies.getJobManager().add(MultiDeviceMessageRequestResponseJob.forAccept(liveRecipient.getId()));
        }
        runnable.run();
    }

    private GroupDatabase.MemberLevel getGroupMemberLevel(RecipientId recipientId) {
        return (GroupDatabase.MemberLevel) SignalDatabase.groups().getGroup(recipientId).map(new Function() { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestRepository$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return MessageRequestRepository.$r8$lambda$VUHKtF8i6Aj0lWVJwituVPmSKnA((GroupDatabase.GroupRecord) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(GroupDatabase.MemberLevel.NOT_A_MEMBER);
    }

    public static /* synthetic */ GroupDatabase.MemberLevel lambda$getGroupMemberLevel$8(GroupDatabase.GroupRecord groupRecord) {
        return groupRecord.memberLevel(Recipient.self());
    }

    private boolean isLegacyThread(Recipient recipient) {
        Application application = ApplicationDependencies.getApplication();
        Long threadIdFor = SignalDatabase.threads().getThreadIdFor(recipient.getId());
        return threadIdFor != null && (RecipientUtil.hasSentMessageInThread(application, threadIdFor) || RecipientUtil.isPreMessageRequestThread(application, threadIdFor));
    }
}
