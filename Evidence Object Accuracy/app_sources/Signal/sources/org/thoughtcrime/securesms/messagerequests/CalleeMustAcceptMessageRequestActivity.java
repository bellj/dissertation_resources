package org.thoughtcrime.securesms.messagerequests;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.BaseActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.ResourceContactPhoto;
import org.thoughtcrime.securesms.messagerequests.CalleeMustAcceptMessageRequestViewModel;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class CalleeMustAcceptMessageRequestActivity extends BaseActivity {
    private static final String RECIPIENT_ID_EXTRA;
    private static final long TIMEOUT_MS = TimeUnit.SECONDS.toMillis(10);
    private AvatarImageView avatar;
    private TextView description;
    private final Runnable finisher = new Runnable() { // from class: org.thoughtcrime.securesms.messagerequests.CalleeMustAcceptMessageRequestActivity$$ExternalSyntheticLambda2
        @Override // java.lang.Runnable
        public final void run() {
            CalleeMustAcceptMessageRequestActivity.this.finish();
        }
    };
    private final Handler handler = new Handler(Looper.getMainLooper());
    private View okay;

    public static Intent createIntent(Context context, RecipientId recipientId) {
        Intent intent = new Intent(context, CalleeMustAcceptMessageRequestActivity.class);
        intent.setFlags(65536);
        intent.putExtra(RECIPIENT_ID_EXTRA, recipientId);
        return intent;
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.callee_must_accept_message_request_dialog_fragment);
        if (getResources().getConfiguration().densityDpi < 480) {
            setRequestedOrientation(1);
        }
        this.description = (TextView) findViewById(R.id.description);
        this.avatar = (AvatarImageView) findViewById(R.id.avatar);
        this.okay = findViewById(R.id.okay);
        this.avatar.setFallbackPhotoProvider(new FallbackPhotoProvider());
        this.okay.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.messagerequests.CalleeMustAcceptMessageRequestActivity$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CalleeMustAcceptMessageRequestActivity.this.lambda$onCreate$0(view);
            }
        });
        ((CalleeMustAcceptMessageRequestViewModel) ViewModelProviders.of(this, new CalleeMustAcceptMessageRequestViewModel.Factory((RecipientId) getIntent().getParcelableExtra(RECIPIENT_ID_EXTRA))).get(CalleeMustAcceptMessageRequestViewModel.class)).getRecipient().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.messagerequests.CalleeMustAcceptMessageRequestActivity$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                CalleeMustAcceptMessageRequestActivity.this.lambda$onCreate$1((Recipient) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onCreate$0(View view) {
        finish();
    }

    public /* synthetic */ void lambda$onCreate$1(Recipient recipient) {
        this.description.setText(getString(R.string.CalleeMustAcceptMessageRequestDialogFragment__s_will_get_a_message_request_from_you, new Object[]{recipient.getDisplayName(this)}));
        this.avatar.setAvatar(GlideApp.with((FragmentActivity) this), recipient, false);
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.handler.postDelayed(this.finisher, TIMEOUT_MS);
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onPause() {
        super.onPause();
        this.handler.removeCallbacks(this.finisher);
    }

    /* loaded from: classes4.dex */
    private static class FallbackPhotoProvider extends Recipient.FallbackPhotoProvider {
        private FallbackPhotoProvider() {
        }

        @Override // org.thoughtcrime.securesms.recipients.Recipient.FallbackPhotoProvider
        public FallbackContactPhoto getPhotoForRecipientWithoutName() {
            return new ResourceContactPhoto(R.drawable.ic_profile_80);
        }
    }
}
