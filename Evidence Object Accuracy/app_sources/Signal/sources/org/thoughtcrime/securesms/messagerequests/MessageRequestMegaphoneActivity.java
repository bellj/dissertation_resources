package org.thoughtcrime.securesms.messagerequests;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.airbnb.lottie.LottieAnimationView;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.profiles.ProfileName;
import org.thoughtcrime.securesms.profiles.edit.EditProfileActivity;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;

/* loaded from: classes4.dex */
public class MessageRequestMegaphoneActivity extends PassphraseRequiredActivity {
    public static final short EDIT_PROFILE_REQUEST_CODE;
    private DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();

    @Override // androidx.activity.ComponentActivity, android.app.Activity
    public void onBackPressed() {
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        this.dynamicTheme.onCreate(this);
        setContentView(R.layout.message_requests_megaphone_activity);
        LottieAnimationView lottieAnimationView = (LottieAnimationView) findViewById(R.id.message_requests_lottie);
        lottieAnimationView.setAnimation(R.raw.lottie_message_requests_splash);
        lottieAnimationView.playAnimation();
        ((TextView) findViewById(R.id.message_requests_confirm_profile_name)).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestMegaphoneActivity$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                MessageRequestMegaphoneActivity.$r8$lambda$zJxIbWaI9qz3n6ulBbTvX7m9gVw(MessageRequestMegaphoneActivity.this, view);
            }
        });
    }

    public /* synthetic */ void lambda$onCreate$0(View view) {
        Intent intent = new Intent(this, EditProfileActivity.class);
        intent.putExtra(EditProfileActivity.SHOW_TOOLBAR, false);
        intent.putExtra(EditProfileActivity.NEXT_BUTTON_TEXT, R.string.save);
        startActivityForResult(intent, 24563);
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 24563 && i2 == -1 && Recipient.self().getProfileName() != ProfileName.EMPTY) {
            setResult(-1);
            finish();
        }
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }
}
