package org.thoughtcrime.securesms.messagerequests;

import android.content.Context;
import androidx.core.content.ContextCompat;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;

/* compiled from: MessageRequestBarColorTheme.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0001\u0018\u0000 \u00102\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0010B'\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003¢\u0006\u0002\u0010\u0007J\u0010\u0010\b\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\nH\u0007J\u0010\u0010\u000b\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\nH\u0007J\u0010\u0010\f\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\nH\u0007J\u0010\u0010\r\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\nH\u0007R\u000e\u0010\u0004\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000j\u0002\b\u000ej\u0002\b\u000f¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/messagerequests/MessageRequestBarColorTheme;", "", "containerBackgroundColor", "", "buttonBackgroundColor", "buttonForegroundDenyColor", "buttonForegroundAcceptColor", "(Ljava/lang/String;IIIII)V", "getButtonBackgroundColor", "context", "Landroid/content/Context;", "getButtonForegroundAcceptColor", "getButtonForegroundDenyColor", "getContainerButtonBackgroundColor", "WALLPAPER", "NORMAL", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public enum MessageRequestBarColorTheme {
    WALLPAPER(R.color.message_request_bar_container_background_wallpaper, R.color.message_request_bar_background_wallpaper, R.color.message_request_bar_denyForeground_wallpaper, R.color.message_request_bar_acceptForeground_wallpaper),
    NORMAL(R.color.message_request_bar_container_background_normal, R.color.message_request_bar_background_normal, R.color.message_request_bar_denyForeground_normal, R.color.message_request_bar_acceptForeground_normal);
    
    public static final Companion Companion = new Companion(null);
    private final int buttonBackgroundColor;
    private final int buttonForegroundAcceptColor;
    private final int buttonForegroundDenyColor;
    private final int containerBackgroundColor;

    @JvmStatic
    public static final MessageRequestBarColorTheme resolveTheme(boolean z) {
        return Companion.resolveTheme(z);
    }

    MessageRequestBarColorTheme(int i, int i2, int i3, int i4) {
        this.containerBackgroundColor = i;
        this.buttonBackgroundColor = i2;
        this.buttonForegroundDenyColor = i3;
        this.buttonForegroundAcceptColor = i4;
    }

    public final int getContainerButtonBackgroundColor(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        return ContextCompat.getColor(context, this.containerBackgroundColor);
    }

    public final int getButtonBackgroundColor(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        return ContextCompat.getColor(context, this.buttonBackgroundColor);
    }

    public final int getButtonForegroundDenyColor(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        return ContextCompat.getColor(context, this.buttonForegroundDenyColor);
    }

    public final int getButtonForegroundAcceptColor(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        return ContextCompat.getColor(context, this.buttonForegroundAcceptColor);
    }

    /* compiled from: MessageRequestBarColorTheme.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/messagerequests/MessageRequestBarColorTheme$Companion;", "", "()V", "resolveTheme", "Lorg/thoughtcrime/securesms/messagerequests/MessageRequestBarColorTheme;", "hasWallpaper", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final MessageRequestBarColorTheme resolveTheme(boolean z) {
            return z ? MessageRequestBarColorTheme.WALLPAPER : MessageRequestBarColorTheme.NORMAL;
        }
    }
}
