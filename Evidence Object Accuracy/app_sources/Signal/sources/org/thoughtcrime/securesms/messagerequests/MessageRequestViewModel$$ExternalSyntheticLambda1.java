package org.thoughtcrime.securesms.messagerequests;

import org.thoughtcrime.securesms.groups.ui.GroupChangeErrorCallback;
import org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MessageRequestViewModel$$ExternalSyntheticLambda1 implements GroupChangeErrorCallback {
    public final /* synthetic */ MessageRequestViewModel f$0;

    public /* synthetic */ MessageRequestViewModel$$ExternalSyntheticLambda1(MessageRequestViewModel messageRequestViewModel) {
        this.f$0 = messageRequestViewModel;
    }

    @Override // org.thoughtcrime.securesms.groups.ui.GroupChangeErrorCallback
    public final void onError(GroupChangeFailureReason groupChangeFailureReason) {
        this.f$0.onGroupChangeError(groupChangeFailureReason);
    }
}
