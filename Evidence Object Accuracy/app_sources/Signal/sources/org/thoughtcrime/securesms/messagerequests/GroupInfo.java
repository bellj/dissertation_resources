package org.thoughtcrime.securesms.messagerequests;

/* loaded from: classes4.dex */
public final class GroupInfo {
    static final GroupInfo ZERO = new GroupInfo(0, 0, "");
    private final String description;
    private final int fullMemberCount;
    private final int pendingMemberCount;

    public GroupInfo(int i, int i2, String str) {
        this.fullMemberCount = i;
        this.pendingMemberCount = i2;
        this.description = str;
    }

    public int getFullMemberCount() {
        return this.fullMemberCount;
    }

    public int getPendingMemberCount() {
        return this.pendingMemberCount;
    }

    public String getDescription() {
        return this.description;
    }
}
