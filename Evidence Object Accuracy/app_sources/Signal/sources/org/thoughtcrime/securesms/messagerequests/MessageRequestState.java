package org.thoughtcrime.securesms.messagerequests;

/* loaded from: classes4.dex */
public enum MessageRequestState {
    NONE,
    BLOCKED_INDIVIDUAL,
    BLOCKED_GROUP,
    LEGACY_INDIVIDUAL,
    LEGACY_GROUP_V1,
    DEPRECATED_GROUP_V1,
    DEPRECATED_GROUP_V1_TOO_LARGE,
    GROUP_V1,
    GROUP_V2_INVITE,
    GROUP_V2_ADD,
    INDIVIDUAL
}
