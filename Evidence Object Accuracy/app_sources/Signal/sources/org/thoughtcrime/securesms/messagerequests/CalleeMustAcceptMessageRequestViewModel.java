package org.thoughtcrime.securesms.messagerequests;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class CalleeMustAcceptMessageRequestViewModel extends ViewModel {
    private final LiveData<Recipient> recipient;

    private CalleeMustAcceptMessageRequestViewModel(RecipientId recipientId) {
        this.recipient = Recipient.live(recipientId).getLiveData();
    }

    public LiveData<Recipient> getRecipient() {
        return this.recipient;
    }

    /* loaded from: classes4.dex */
    public static class Factory implements ViewModelProvider.Factory {
        private final RecipientId recipientId;

        public Factory(RecipientId recipientId) {
            this.recipientId = recipientId;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new CalleeMustAcceptMessageRequestViewModel(this.recipientId));
        }
    }
}
