package org.thoughtcrime.securesms.messagerequests;

import android.content.Context;
import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import j$.util.function.Function;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.blocked.BlockedUsersViewModel$$ExternalSyntheticLambda2;
import org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason;
import org.thoughtcrime.securesms.messagerequests.MessageRequestViewModel;
import org.thoughtcrime.securesms.profiles.spoofing.ReviewUtil;
import org.thoughtcrime.securesms.recipients.LiveRecipient;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientForeverObserver;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.thoughtcrime.securesms.util.livedata.Store;

/* loaded from: classes4.dex */
public class MessageRequestViewModel extends ViewModel {
    private final SingleLiveEvent<GroupChangeFailureReason> failures;
    private final MutableLiveData<GroupInfo> groupInfo;
    private final MutableLiveData<List<String>> groups;
    private LiveRecipient liveRecipient;
    private final LiveData<MessageData> messageData;
    private final MutableLiveData<Recipient> recipient;
    private final Store<RecipientInfo> recipientInfoStore;
    private final RecipientForeverObserver recipientObserver;
    private final MessageRequestRepository repository;
    private final LiveData<RequestReviewDisplayState> requestReviewDisplayState;
    private final SingleLiveEvent<Status> status;
    private long threadId;

    /* loaded from: classes4.dex */
    public enum RequestReviewDisplayState {
        HIDDEN,
        SHOWN,
        NONE
    }

    /* loaded from: classes4.dex */
    public enum Status {
        IDLE,
        BLOCKING,
        BLOCKED,
        BLOCKED_AND_REPORTED,
        DELETING,
        DELETED,
        ACCEPTING,
        ACCEPTED
    }

    public /* synthetic */ void lambda$new$0(Recipient recipient) {
        loadGroupInfo();
        this.recipient.setValue(recipient);
    }

    private MessageRequestViewModel(MessageRequestRepository messageRequestRepository) {
        this.status = new SingleLiveEvent<>();
        this.failures = new SingleLiveEvent<>();
        MutableLiveData<Recipient> mutableLiveData = new MutableLiveData<>();
        this.recipient = mutableLiveData;
        MutableLiveData<List<String>> mutableLiveData2 = new MutableLiveData<>(Collections.emptyList());
        this.groups = mutableLiveData2;
        MutableLiveData<GroupInfo> mutableLiveData3 = new MutableLiveData<>(GroupInfo.ZERO);
        this.groupInfo = mutableLiveData3;
        Store<RecipientInfo> store = new Store<>(new RecipientInfo(null, null, null, null));
        this.recipientInfoStore = store;
        this.recipientObserver = new RecipientForeverObserver() { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestViewModel$$ExternalSyntheticLambda6
            @Override // org.thoughtcrime.securesms.recipients.RecipientForeverObserver
            public final void onRecipientChanged(Recipient recipient) {
                MessageRequestViewModel.this.lambda$new$0(recipient);
            }
        };
        this.repository = messageRequestRepository;
        LiveData mapAsync = LiveDataUtil.mapAsync(mutableLiveData, new Function() { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestViewModel$$ExternalSyntheticLambda7
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return MessageRequestViewModel.this.createMessageDataForRecipient((Recipient) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        this.messageData = mapAsync;
        this.requestReviewDisplayState = LiveDataUtil.mapAsync(mapAsync, new Function() { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestViewModel$$ExternalSyntheticLambda8
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return MessageRequestViewModel.transformHolderToReviewDisplayState((MessageRequestViewModel.MessageData) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        store.update(mutableLiveData, new Store.Action() { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestViewModel$$ExternalSyntheticLambda9
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return MessageRequestViewModel.lambda$new$1((Recipient) obj, (MessageRequestViewModel.RecipientInfo) obj2);
            }
        });
        store.update(mutableLiveData3, new Store.Action() { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestViewModel$$ExternalSyntheticLambda10
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return MessageRequestViewModel.lambda$new$2((GroupInfo) obj, (MessageRequestViewModel.RecipientInfo) obj2);
            }
        });
        store.update(mutableLiveData2, new Store.Action() { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestViewModel$$ExternalSyntheticLambda11
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return MessageRequestViewModel.lambda$new$3((List) obj, (MessageRequestViewModel.RecipientInfo) obj2);
            }
        });
        store.update(mapAsync, new Store.Action() { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestViewModel$$ExternalSyntheticLambda12
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return MessageRequestViewModel.lambda$new$4((MessageRequestViewModel.MessageData) obj, (MessageRequestViewModel.RecipientInfo) obj2);
            }
        });
    }

    public static /* synthetic */ RecipientInfo lambda$new$1(Recipient recipient, RecipientInfo recipientInfo) {
        return new RecipientInfo(recipient, recipientInfo.groupInfo, recipientInfo.sharedGroups, recipientInfo.messageRequestState);
    }

    public static /* synthetic */ RecipientInfo lambda$new$2(GroupInfo groupInfo, RecipientInfo recipientInfo) {
        return new RecipientInfo(recipientInfo.recipient, groupInfo, recipientInfo.sharedGroups, recipientInfo.messageRequestState);
    }

    public static /* synthetic */ RecipientInfo lambda$new$3(List list, RecipientInfo recipientInfo) {
        return new RecipientInfo(recipientInfo.recipient, recipientInfo.groupInfo, list, recipientInfo.messageRequestState);
    }

    public static /* synthetic */ RecipientInfo lambda$new$4(MessageData messageData, RecipientInfo recipientInfo) {
        return new RecipientInfo(recipientInfo.recipient, recipientInfo.groupInfo, recipientInfo.sharedGroups, messageData.messageState);
    }

    public void setConversationInfo(RecipientId recipientId, long j) {
        LiveRecipient liveRecipient = this.liveRecipient;
        if (liveRecipient != null) {
            liveRecipient.lambda$asObservable$6(this.recipientObserver);
        }
        this.liveRecipient = Recipient.live(recipientId);
        this.threadId = j;
        loadRecipient();
        loadGroups();
        loadGroupInfo();
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        LiveRecipient liveRecipient = this.liveRecipient;
        if (liveRecipient != null) {
            liveRecipient.lambda$asObservable$6(this.recipientObserver);
        }
    }

    public LiveData<RequestReviewDisplayState> getRequestReviewDisplayState() {
        return this.requestReviewDisplayState;
    }

    public LiveData<Recipient> getRecipient() {
        return this.recipient;
    }

    public LiveData<MessageData> getMessageData() {
        return this.messageData;
    }

    public LiveData<RecipientInfo> getRecipientInfo() {
        return this.recipientInfoStore.getStateLiveData();
    }

    public LiveData<Status> getMessageRequestStatus() {
        return this.status;
    }

    public LiveData<GroupChangeFailureReason> getFailures() {
        return this.failures;
    }

    public boolean shouldShowMessageRequest() {
        MessageData value = this.messageData.getValue();
        return (value == null || value.getMessageState() == MessageRequestState.NONE) ? false : true;
    }

    public void onAccept() {
        this.status.setValue(Status.ACCEPTING);
        this.repository.acceptMessageRequest(this.liveRecipient, this.threadId, new Runnable() { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestViewModel$$ExternalSyntheticLambda3
            @Override // java.lang.Runnable
            public final void run() {
                MessageRequestViewModel.this.lambda$onAccept$5();
            }
        }, new MessageRequestViewModel$$ExternalSyntheticLambda1(this));
    }

    public /* synthetic */ void lambda$onAccept$5() {
        this.status.postValue(Status.ACCEPTED);
    }

    public void onDelete() {
        this.status.setValue(Status.DELETING);
        this.repository.deleteMessageRequest(this.liveRecipient, this.threadId, new Runnable() { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestViewModel$$ExternalSyntheticLambda14
            @Override // java.lang.Runnable
            public final void run() {
                MessageRequestViewModel.this.lambda$onDelete$6();
            }
        }, new MessageRequestViewModel$$ExternalSyntheticLambda1(this));
    }

    public /* synthetic */ void lambda$onDelete$6() {
        this.status.postValue(Status.DELETED);
    }

    public void onBlock() {
        this.status.setValue(Status.BLOCKING);
        this.repository.blockMessageRequest(this.liveRecipient, new Runnable() { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestViewModel$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                MessageRequestViewModel.this.lambda$onBlock$7();
            }
        }, new MessageRequestViewModel$$ExternalSyntheticLambda1(this));
    }

    public /* synthetic */ void lambda$onBlock$7() {
        this.status.postValue(Status.BLOCKED);
    }

    public void onUnblock() {
        this.repository.unblockAndAccept(this.liveRecipient, this.threadId, new Runnable() { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestViewModel$$ExternalSyntheticLambda4
            @Override // java.lang.Runnable
            public final void run() {
                MessageRequestViewModel.this.lambda$onUnblock$8();
            }
        });
    }

    public /* synthetic */ void lambda$onUnblock$8() {
        this.status.postValue(Status.ACCEPTED);
    }

    public void onBlockAndReportSpam() {
        this.repository.blockAndReportSpamMessageRequest(this.liveRecipient, this.threadId, new Runnable() { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestViewModel$$ExternalSyntheticLambda2
            @Override // java.lang.Runnable
            public final void run() {
                MessageRequestViewModel.this.lambda$onBlockAndReportSpam$9();
            }
        }, new MessageRequestViewModel$$ExternalSyntheticLambda1(this));
    }

    public /* synthetic */ void lambda$onBlockAndReportSpam$9() {
        this.status.postValue(Status.BLOCKED_AND_REPORTED);
    }

    public void onGroupChangeError(GroupChangeFailureReason groupChangeFailureReason) {
        this.status.postValue(Status.IDLE);
        this.failures.postValue(groupChangeFailureReason);
    }

    private void loadRecipient() {
        this.liveRecipient.observeForever(this.recipientObserver);
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestViewModel$$ExternalSyntheticLambda5
            @Override // java.lang.Runnable
            public final void run() {
                MessageRequestViewModel.this.lambda$loadRecipient$10();
            }
        });
    }

    public /* synthetic */ void lambda$loadRecipient$10() {
        this.recipient.postValue(this.liveRecipient.get());
    }

    private void loadGroups() {
        MessageRequestRepository messageRequestRepository = this.repository;
        RecipientId id = this.liveRecipient.getId();
        MutableLiveData<List<String>> mutableLiveData = this.groups;
        Objects.requireNonNull(mutableLiveData);
        messageRequestRepository.getGroups(id, new BlockedUsersViewModel$$ExternalSyntheticLambda2(mutableLiveData));
    }

    private void loadGroupInfo() {
        MessageRequestRepository messageRequestRepository = this.repository;
        RecipientId id = this.liveRecipient.getId();
        MutableLiveData<GroupInfo> mutableLiveData = this.groupInfo;
        Objects.requireNonNull(mutableLiveData);
        messageRequestRepository.getGroupInfo(id, new Consumer() { // from class: org.thoughtcrime.securesms.messagerequests.MessageRequestViewModel$$ExternalSyntheticLambda13
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                MutableLiveData.this.postValue((GroupInfo) obj);
            }
        });
    }

    public static RequestReviewDisplayState transformHolderToReviewDisplayState(MessageData messageData) {
        if (messageData.getMessageState() != MessageRequestState.INDIVIDUAL) {
            return RequestReviewDisplayState.NONE;
        }
        if (ReviewUtil.isRecipientReviewSuggested(messageData.getRecipient().getId())) {
            return RequestReviewDisplayState.SHOWN;
        }
        return RequestReviewDisplayState.HIDDEN;
    }

    public MessageData createMessageDataForRecipient(Recipient recipient) {
        return new MessageData(recipient, this.repository.getMessageRequestState(recipient, this.threadId));
    }

    /* loaded from: classes4.dex */
    public static class RecipientInfo {
        private final GroupInfo groupInfo;
        private final MessageRequestState messageRequestState;
        private final Recipient recipient;
        private final List<String> sharedGroups;

        private RecipientInfo(Recipient recipient, GroupInfo groupInfo, List<String> list, MessageRequestState messageRequestState) {
            this.recipient = recipient;
            this.groupInfo = groupInfo == null ? GroupInfo.ZERO : groupInfo;
            this.sharedGroups = list == null ? Collections.emptyList() : list;
            this.messageRequestState = messageRequestState;
        }

        public Recipient getRecipient() {
            return this.recipient;
        }

        public int getGroupMemberCount() {
            return this.groupInfo.getFullMemberCount();
        }

        public int getGroupPendingMemberCount() {
            return this.groupInfo.getPendingMemberCount();
        }

        public String getGroupDescription() {
            return this.groupInfo.getDescription();
        }

        public List<String> getSharedGroups() {
            return this.sharedGroups;
        }

        public MessageRequestState getMessageRequestState() {
            return this.messageRequestState;
        }
    }

    /* loaded from: classes4.dex */
    public static final class MessageData {
        private final MessageRequestState messageState;
        private final Recipient recipient;

        public MessageData(Recipient recipient, MessageRequestState messageRequestState) {
            this.recipient = recipient;
            this.messageState = messageRequestState;
        }

        public Recipient getRecipient() {
            return this.recipient;
        }

        public MessageRequestState getMessageState() {
            return this.messageState;
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements ViewModelProvider.Factory {
        private final Context context;

        public Factory(Context context) {
            this.context = context;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return new MessageRequestViewModel(new MessageRequestRepository(this.context.getApplicationContext()));
        }
    }
}
