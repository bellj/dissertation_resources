package org.thoughtcrime.securesms;

import android.animation.Animator;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import androidx.core.view.ViewCompat;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.functions.Consumer;
import org.signal.qr.QrScannerView;
import org.signal.qr.kitkat.ScanListener;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes.dex */
public class DeviceAddFragment extends LoggingFragment {
    private ImageView devicesImage;
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private ScanListener scanListener;

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ViewGroup viewGroup2 = (ViewGroup) ViewUtil.inflate(layoutInflater, viewGroup, R.layout.device_add_fragment);
        QrScannerView qrScannerView = (QrScannerView) viewGroup2.findViewById(R.id.scanner);
        ImageView imageView = (ImageView) viewGroup2.findViewById(R.id.devices);
        this.devicesImage = imageView;
        ViewCompat.setTransitionName(imageView, "devices");
        if (Build.VERSION.SDK_INT >= 21) {
            viewGroup2.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: org.thoughtcrime.securesms.DeviceAddFragment.1
                @Override // android.view.View.OnLayoutChangeListener
                public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                    view.removeOnLayoutChangeListener(this);
                    Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(view, i3, i4, 0.0f, (float) ((int) Math.hypot((double) i3, (double) i4)));
                    createCircularReveal.setInterpolator(new DecelerateInterpolator(2.0f));
                    createCircularReveal.setDuration(800);
                    createCircularReveal.start();
                }
            });
        }
        qrScannerView.start(getViewLifecycleOwner(), FeatureFlags.useQrLegacyScan());
        this.lifecycleDisposable.bindTo(getViewLifecycleOwner());
        this.lifecycleDisposable.add(qrScannerView.getQrData().distinctUntilChanged().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.DeviceAddFragment$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                DeviceAddFragment.this.lambda$onCreateView$0((String) obj);
            }
        }));
        return viewGroup2;
    }

    public /* synthetic */ void lambda$onCreateView$0(String str) throws Throwable {
        ScanListener scanListener = this.scanListener;
        if (scanListener != null) {
            scanListener.onQrDataFound(str);
        }
    }

    public ImageView getDevicesImage() {
        return this.devicesImage;
    }

    public void setScanListener(ScanListener scanListener) {
        this.scanListener = scanListener;
    }
}
