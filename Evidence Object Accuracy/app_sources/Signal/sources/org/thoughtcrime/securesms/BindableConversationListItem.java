package org.thoughtcrime.securesms;

import androidx.lifecycle.LifecycleOwner;
import java.util.Locale;
import java.util.Set;
import org.thoughtcrime.securesms.conversationlist.model.ConversationSet;
import org.thoughtcrime.securesms.database.model.ThreadRecord;
import org.thoughtcrime.securesms.mms.GlideRequests;

/* loaded from: classes.dex */
public interface BindableConversationListItem extends Unbindable {
    void bind(LifecycleOwner lifecycleOwner, ThreadRecord threadRecord, GlideRequests glideRequests, Locale locale, Set<Long> set, ConversationSet conversationSet);

    void setSelectedConversations(ConversationSet conversationSet);

    void updateTypingIndicator(Set<Long> set);
}
