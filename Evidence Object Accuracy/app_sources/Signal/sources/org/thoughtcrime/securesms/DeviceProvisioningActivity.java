package org.thoughtcrime.securesms;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import org.signal.core.util.logging.Log;

/* loaded from: classes.dex */
public class DeviceProvisioningActivity extends PassphraseRequiredActivity {
    private static final String TAG = Log.tag(DeviceProvisioningActivity.class);

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onPreCreate() {
        supportRequestWindowFeature(1);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        AlertDialog create = new AlertDialog.Builder(this).setTitle(getString(R.string.DeviceProvisioningActivity_link_a_signal_device)).setMessage(getString(R.string.DeviceProvisioningActivity_it_looks_like_youre_trying_to_link_a_signal_device_using_a_3rd_party_scanner)).setPositiveButton(R.string.DeviceProvisioningActivity_continue, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.DeviceProvisioningActivity$$ExternalSyntheticLambda0
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                DeviceProvisioningActivity.$r8$lambda$2dmw9FqVgiL9KA_UswoMNU0qj1k(DeviceProvisioningActivity.this, dialogInterface, i);
            }
        }).setNegativeButton(17039360, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.DeviceProvisioningActivity$$ExternalSyntheticLambda1
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                DeviceProvisioningActivity.$r8$lambda$PjIbpRac4M3dpWPdbhzxnKi95NA(DeviceProvisioningActivity.this, dialogInterface, i);
            }
        }).setOnDismissListener(new DialogInterface.OnDismissListener() { // from class: org.thoughtcrime.securesms.DeviceProvisioningActivity$$ExternalSyntheticLambda2
            @Override // android.content.DialogInterface.OnDismissListener
            public final void onDismiss(DialogInterface dialogInterface) {
                DeviceProvisioningActivity.$r8$lambda$qs2S10iiI241wX7ra_1bo1vkSpc(DeviceProvisioningActivity.this, dialogInterface);
            }
        }).create();
        create.setIcon(getResources().getDrawable(R.drawable.icon_dialog));
        create.show();
    }

    public /* synthetic */ void lambda$onCreate$0(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent(this, DeviceActivity.class);
        intent.putExtra("add", true);
        startActivity(intent);
        finish();
    }

    public /* synthetic */ void lambda$onCreate$1(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        finish();
    }

    public /* synthetic */ void lambda$onCreate$2(DialogInterface dialogInterface) {
        finish();
    }
}
