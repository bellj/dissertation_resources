package org.thoughtcrime.securesms.deeplinks;

import android.content.Intent;
import android.os.Bundle;
import org.thoughtcrime.securesms.MainActivity;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;

/* loaded from: classes4.dex */
public class DeepLinkEntryActivity extends PassphraseRequiredActivity {
    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        Intent clearTop = MainActivity.clearTop(this);
        clearTop.setData(getIntent().getData());
        startActivity(clearTop);
    }
}
