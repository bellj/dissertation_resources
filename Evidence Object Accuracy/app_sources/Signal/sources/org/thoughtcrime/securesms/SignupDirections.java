package org.thoughtcrime.securesms;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;

/* loaded from: classes.dex */
public class SignupDirections {
    private SignupDirections() {
    }

    public static NavDirections actionRestartToWelcomeFragment() {
        return new ActionOnlyNavDirections(R.id.action_restart_to_welcomeFragment);
    }
}
