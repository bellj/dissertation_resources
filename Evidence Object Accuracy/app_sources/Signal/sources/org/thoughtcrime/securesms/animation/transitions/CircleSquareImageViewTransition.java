package org.thoughtcrime.securesms.animation.transitions;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.graphics.drawable.Drawable;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.util.Property;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;

/* loaded from: classes.dex */
abstract class CircleSquareImageViewTransition extends Transition {
    private static final String CIRCLE_RATIO;
    private final boolean toCircle;

    public CircleSquareImageViewTransition(boolean z) {
        this.toCircle = z;
    }

    @Override // android.transition.Transition
    public void captureStartValues(TransitionValues transitionValues) {
        if (transitionValues.view instanceof ImageView) {
            transitionValues.values.put(CIRCLE_RATIO, Float.valueOf(this.toCircle ? 0.0f : 1.0f));
        }
    }

    @Override // android.transition.Transition
    public void captureEndValues(TransitionValues transitionValues) {
        if (transitionValues.view instanceof ImageView) {
            transitionValues.values.put(CIRCLE_RATIO, Float.valueOf(this.toCircle ? 1.0f : 0.0f));
        }
    }

    @Override // android.transition.Transition
    public Animator createAnimator(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
        if (transitionValues == null || transitionValues2 == null) {
            return null;
        }
        return ObjectAnimator.ofFloat((ImageView) transitionValues2.view, new RadiusRatioProperty(), ((Float) transitionValues.values.get(CIRCLE_RATIO)).floatValue(), ((Float) transitionValues2.values.get(CIRCLE_RATIO)).floatValue());
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes.dex */
    public static final class RadiusRatioProperty extends Property<ImageView, Float> {
        private float ratio;

        RadiusRatioProperty() {
            super(Float.class, "circle_ratio");
        }

        public final void set(ImageView imageView, Float f) {
            this.ratio = f.floatValue();
            Drawable drawable = imageView.getDrawable();
            if (drawable instanceof RoundedBitmapDrawable) {
                RoundedBitmapDrawable roundedBitmapDrawable = (RoundedBitmapDrawable) drawable;
                if (((double) f.floatValue()) > 0.95d) {
                    roundedBitmapDrawable.setCircular(true);
                } else {
                    roundedBitmapDrawable.setCornerRadius(((float) Math.min(roundedBitmapDrawable.getIntrinsicWidth(), roundedBitmapDrawable.getIntrinsicHeight())) * f.floatValue() * 0.5f);
                }
            }
        }

        public Float get(ImageView imageView) {
            return Float.valueOf(this.ratio);
        }
    }
}
