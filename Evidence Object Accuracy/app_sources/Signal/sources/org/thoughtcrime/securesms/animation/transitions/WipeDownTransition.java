package org.thoughtcrime.securesms.animation.transitions;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.RectEvaluator;
import android.content.Context;
import android.graphics.Rect;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentContainerView;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: WipeDownTransition.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\f\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0002J&\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\n2\b\u0010\u0012\u001a\u0004\u0018\u00010\nH\u0016¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/animation/transitions/WipeDownTransition;", "Landroid/transition/Transition;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "captureEndValues", "", "transitionValues", "Landroid/transition/TransitionValues;", "captureStartValues", "captureValues", "createAnimator", "Landroid/animation/Animator;", "sceneRoot", "Landroid/view/ViewGroup;", "startValues", "endValues", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class WipeDownTransition extends Transition {
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public WipeDownTransition(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    @Override // android.transition.Transition
    public void captureStartValues(TransitionValues transitionValues) {
        Intrinsics.checkNotNullParameter(transitionValues, "transitionValues");
        captureValues(transitionValues);
    }

    @Override // android.transition.Transition
    public void captureEndValues(TransitionValues transitionValues) {
        Intrinsics.checkNotNullParameter(transitionValues, "transitionValues");
        captureValues(transitionValues);
    }

    private final void captureValues(TransitionValues transitionValues) {
        View view = transitionValues.view;
        Intrinsics.checkNotNullExpressionValue(view, "transitionValues.view");
        if (view instanceof ViewGroup) {
            Rect rect = new Rect();
            view.getLocalVisibleRect(rect);
            Map map = transitionValues.values;
            Intrinsics.checkNotNullExpressionValue(map, "transitionValues.values");
            map.put("signal.wipedowntransition.bottom", rect);
        }
    }

    @Override // android.transition.Transition
    public Animator createAnimator(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
        Intrinsics.checkNotNullParameter(viewGroup, "sceneRoot");
        Rect rect = null;
        if (transitionValues == null || transitionValues2 == null) {
            return null;
        }
        View view = transitionValues2.view;
        Intrinsics.checkNotNullExpressionValue(view, "endValues.view");
        if (!(view instanceof FragmentContainerView)) {
            return null;
        }
        Object obj = transitionValues.values.get("signal.wipedowntransition.bottom");
        Rect rect2 = obj instanceof Rect ? (Rect) obj : null;
        if (rect2 == null) {
            rect2 = new Rect();
            view.getLocalVisibleRect(rect2);
        }
        Object obj2 = transitionValues2.values.get("signal.wipedowntransition.bottom");
        if (obj2 instanceof Rect) {
            rect = (Rect) obj2;
        }
        if (rect == null) {
            rect = new Rect();
            view.getLocalVisibleRect(rect);
        }
        ObjectAnimator ofObject = ObjectAnimator.ofObject(view, "clipBounds", new RectEvaluator(), rect2, rect);
        Intrinsics.checkNotNullExpressionValue(ofObject, "");
        ofObject.addListener(new Animator.AnimatorListener(view) { // from class: org.thoughtcrime.securesms.animation.transitions.WipeDownTransition$createAnimator$lambda-3$$inlined$addListener$default$1
            final /* synthetic */ View $view$inlined;

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationCancel(Animator animator) {
                Intrinsics.checkNotNullParameter(animator, "animator");
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationRepeat(Animator animator) {
                Intrinsics.checkNotNullParameter(animator, "animator");
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationStart(Animator animator) {
                Intrinsics.checkNotNullParameter(animator, "animator");
            }

            {
                this.$view$inlined = r1;
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                Intrinsics.checkNotNullParameter(animator, "animator");
                ((FragmentContainerView) this.$view$inlined).setClipBounds(null);
            }
        });
        return ofObject;
    }
}
