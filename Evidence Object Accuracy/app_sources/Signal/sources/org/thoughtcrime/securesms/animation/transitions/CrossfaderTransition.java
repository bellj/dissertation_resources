package org.thoughtcrime.securesms.animation.transitions;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.animation.transitions.CrossfaderTransition;

/* compiled from: CrossfaderTransition.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0007\u0018\u0000 \u00122\u00020\u0001:\u0002\u0012\u0013B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J(\u0010\f\u001a\u0004\u0018\u00010\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\n2\b\u0010\u0011\u001a\u0004\u0018\u00010\nH\u0016¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/animation/transitions/CrossfaderTransition;", "Landroid/transition/Transition;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "captureEndValues", "", "transitionValues", "Landroid/transition/TransitionValues;", "captureStartValues", "createAnimator", "Landroid/animation/Animator;", "sceneRoot", "Landroid/view/ViewGroup;", "startValues", "endValues", "Companion", "Crossfadeable", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class CrossfaderTransition extends Transition {
    public static final Companion Companion = new Companion(null);
    private static final String WIDTH;

    /* compiled from: CrossfaderTransition.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0010\u0010\b\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H&¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/animation/transitions/CrossfaderTransition$Crossfadeable;", "", "onCrossfadeAnimationUpdated", "", "progress", "", "reverse", "", "onCrossfadeFinished", "onCrossfadeStarted", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public interface Crossfadeable {
        void onCrossfadeAnimationUpdated(float f, boolean z);

        void onCrossfadeFinished(boolean z);

        void onCrossfadeStarted(boolean z);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public CrossfaderTransition(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    /* compiled from: CrossfaderTransition.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/animation/transitions/CrossfaderTransition$Companion;", "", "()V", "WIDTH", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    @Override // android.transition.Transition
    public void captureStartValues(TransitionValues transitionValues) {
        Intrinsics.checkNotNullParameter(transitionValues, "transitionValues");
        if (transitionValues.view instanceof Crossfadeable) {
            Map map = transitionValues.values;
            Intrinsics.checkNotNullExpressionValue(map, "transitionValues.values");
            map.put(WIDTH, Integer.valueOf(transitionValues.view.getWidth()));
        }
    }

    @Override // android.transition.Transition
    public void captureEndValues(TransitionValues transitionValues) {
        Intrinsics.checkNotNullParameter(transitionValues, "transitionValues");
        if (transitionValues.view instanceof Crossfadeable) {
            Map map = transitionValues.values;
            Intrinsics.checkNotNullExpressionValue(map, "transitionValues.values");
            map.put(WIDTH, Integer.valueOf(transitionValues.view.getWidth()));
        }
    }

    @Override // android.transition.Transition
    public Animator createAnimator(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
        if (transitionValues == null || transitionValues2 == null) {
            return null;
        }
        int i = transitionValues.values.get(WIDTH);
        boolean z = false;
        if (i == null) {
            i = 0;
        }
        int intValue = ((Integer) i).intValue();
        int i2 = transitionValues2.values.get(WIDTH);
        if (i2 == null) {
            i2 = 0;
        }
        int intValue2 = ((Integer) i2).intValue();
        View view = transitionValues2.view;
        Crossfadeable crossfadeable = view instanceof Crossfadeable ? (Crossfadeable) view : null;
        if (crossfadeable == null) {
            return null;
        }
        if (intValue > intValue2) {
            z = true;
        }
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(z) { // from class: org.thoughtcrime.securesms.animation.transitions.CrossfaderTransition$$ExternalSyntheticLambda0
            public final /* synthetic */ boolean f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                CrossfaderTransition.m267$r8$lambda$CY2WgpYxru_fSbaph4aziylLQ8(CrossfaderTransition.Crossfadeable.this, this.f$1, valueAnimator);
            }
        });
        Intrinsics.checkNotNullExpressionValue(ofFloat, "");
        ofFloat.addListener(new Animator.AnimatorListener(crossfadeable, z) { // from class: org.thoughtcrime.securesms.animation.transitions.CrossfaderTransition$createAnimator$lambda-3$$inlined$doOnStart$1
            final /* synthetic */ boolean $reverse$inlined;
            final /* synthetic */ CrossfaderTransition.Crossfadeable $view$inlined;

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationCancel(Animator animator) {
                Intrinsics.checkNotNullParameter(animator, "animator");
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                Intrinsics.checkNotNullParameter(animator, "animator");
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationRepeat(Animator animator) {
                Intrinsics.checkNotNullParameter(animator, "animator");
            }

            {
                this.$view$inlined = r1;
                this.$reverse$inlined = r2;
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationStart(Animator animator) {
                Intrinsics.checkNotNullParameter(animator, "animator");
                this.$view$inlined.onCrossfadeStarted(this.$reverse$inlined);
            }
        });
        ofFloat.addListener(new Animator.AnimatorListener(crossfadeable, z) { // from class: org.thoughtcrime.securesms.animation.transitions.CrossfaderTransition$createAnimator$lambda-3$$inlined$doOnEnd$1
            final /* synthetic */ boolean $reverse$inlined;
            final /* synthetic */ CrossfaderTransition.Crossfadeable $view$inlined;

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationCancel(Animator animator) {
                Intrinsics.checkNotNullParameter(animator, "animator");
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationRepeat(Animator animator) {
                Intrinsics.checkNotNullParameter(animator, "animator");
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationStart(Animator animator) {
                Intrinsics.checkNotNullParameter(animator, "animator");
            }

            {
                this.$view$inlined = r1;
                this.$reverse$inlined = r2;
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                Intrinsics.checkNotNullParameter(animator, "animator");
                this.$view$inlined.onCrossfadeFinished(this.$reverse$inlined);
            }
        });
        return ofFloat;
    }

    /* renamed from: createAnimator$lambda-3$lambda-0 */
    public static final void m268createAnimator$lambda3$lambda0(Crossfadeable crossfadeable, boolean z, ValueAnimator valueAnimator) {
        Intrinsics.checkNotNullParameter(crossfadeable, "$view");
        Object animatedValue = valueAnimator.getAnimatedValue();
        if (animatedValue != null) {
            crossfadeable.onCrossfadeAnimationUpdated(((Float) animatedValue).floatValue(), z);
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Float");
    }
}
