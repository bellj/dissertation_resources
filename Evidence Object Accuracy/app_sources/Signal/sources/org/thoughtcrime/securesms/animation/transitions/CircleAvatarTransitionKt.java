package org.thoughtcrime.securesms.animation.transitions;

import kotlin.Metadata;

/* compiled from: CircleAvatarTransition.kt */
@Metadata(d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\"\u000e\u0010\u0000\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000¨\u0006\u0004"}, d2 = {"HEIGHT", "", "POSITION_ON_SCREEN", "WIDTH", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class CircleAvatarTransitionKt {
    private static final String HEIGHT;
    private static final String POSITION_ON_SCREEN;
    private static final String WIDTH;
}
