package org.thoughtcrime.securesms.animation.transitions;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.TypeEvaluator;
import android.content.Context;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: CircleAvatarTransition.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0007\u0018\u00002\u00020\u0001:\u0001\u0013B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\f\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0002J&\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\n2\b\u0010\u0012\u001a\u0004\u0018\u00010\nH\u0016¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/animation/transitions/CircleAvatarTransition;", "Landroid/transition/Transition;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "captureEndValues", "", "transitionValues", "Landroid/transition/TransitionValues;", "captureStartValues", "captureValues", "createAnimator", "Landroid/animation/Animator;", "sceneRoot", "Landroid/view/ViewGroup;", "startValues", "endValues", "FloatInterpolatorEvaluator", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class CircleAvatarTransition extends Transition {
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public CircleAvatarTransition(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    @Override // android.transition.Transition
    public void captureStartValues(TransitionValues transitionValues) {
        Intrinsics.checkNotNullParameter(transitionValues, "transitionValues");
        captureValues(transitionValues);
    }

    @Override // android.transition.Transition
    public void captureEndValues(TransitionValues transitionValues) {
        Intrinsics.checkNotNullParameter(transitionValues, "transitionValues");
        captureValues(transitionValues);
    }

    private final void captureValues(TransitionValues transitionValues) {
        View view = transitionValues.view;
        Intrinsics.checkNotNullExpressionValue(view, "transitionValues.view");
        if (Intrinsics.areEqual(view.getTransitionName(), "avatar")) {
            int[] iArr = {0, 0};
            view.getLocationOnScreen(iArr);
            Map map = transitionValues.values;
            Intrinsics.checkNotNullExpressionValue(map, "transitionValues.values");
            map.put("signal.circleavatartransition.positiononscreen", iArr);
            Map map2 = transitionValues.values;
            Intrinsics.checkNotNullExpressionValue(map2, "transitionValues.values");
            map2.put("signal.circleavatartransition.width", Integer.valueOf(view.getMeasuredWidth()));
            Map map3 = transitionValues.values;
            Intrinsics.checkNotNullExpressionValue(map3, "transitionValues.values");
            map3.put("signal.circleavatartransition.height", Integer.valueOf(view.getMeasuredHeight()));
        }
    }

    @Override // android.transition.Transition
    public Animator createAnimator(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
        Intrinsics.checkNotNullParameter(viewGroup, "sceneRoot");
        Integer num = null;
        if (transitionValues == null || transitionValues2 == null) {
            return null;
        }
        View view = transitionValues2.view;
        Intrinsics.checkNotNullExpressionValue(view, "endValues.view");
        if (!Intrinsics.areEqual(view.getTransitionName(), "avatar")) {
            return null;
        }
        Object obj = transitionValues.values.get("signal.circleavatartransition.positiononscreen");
        int[] iArr = obj instanceof int[] ? (int[]) obj : null;
        if (iArr == null) {
            iArr = new int[]{0, 0};
            view.getLocationOnScreen(iArr);
        }
        Object obj2 = transitionValues2.values.get("signal.circleavatartransition.positiononscreen");
        int[] iArr2 = obj2 instanceof int[] ? (int[]) obj2 : null;
        if (iArr2 == null) {
            iArr2 = new int[]{0, 0};
            view.getLocationOnScreen(iArr2);
        }
        Object obj3 = transitionValues.values.get("signal.circleavatartransition.width");
        Integer num2 = obj3 instanceof Integer ? (Integer) obj3 : null;
        int intValue = num2 != null ? num2.intValue() : view.getMeasuredWidth();
        Object obj4 = transitionValues2.values.get("signal.circleavatartransition.width");
        Integer num3 = obj4 instanceof Integer ? (Integer) obj4 : null;
        int intValue2 = num3 != null ? num3.intValue() : view.getMeasuredWidth();
        Object obj5 = transitionValues.values.get("signal.circleavatartransition.height");
        Integer num4 = obj5 instanceof Integer ? (Integer) obj5 : null;
        int intValue3 = num4 != null ? num4.intValue() : view.getMeasuredHeight();
        Object obj6 = transitionValues2.values.get("signal.circleavatartransition.height");
        if (obj6 instanceof Integer) {
            num = (Integer) obj6;
        }
        int intValue4 = num != null ? num.intValue() : view.getMeasuredHeight();
        PropertyValuesHolder ofFloat = PropertyValuesHolder.ofFloat("translationX", ((float) (iArr[0] - iArr2[0])) - (((float) (intValue2 - intValue)) / 2.0f), 0.0f);
        ofFloat.setEvaluator(new FloatInterpolatorEvaluator(new DecelerateInterpolator()));
        PropertyValuesHolder ofFloat2 = PropertyValuesHolder.ofFloat("translationY", ((float) (iArr[1] - iArr2[1])) - (((float) (intValue4 - intValue3)) / 2.0f), 0.0f);
        ofFloat2.setEvaluator(new FloatInterpolatorEvaluator(new AccelerateInterpolator()));
        return ObjectAnimator.ofPropertyValuesHolder(view, ofFloat, ofFloat2, PropertyValuesHolder.ofFloat("scaleX", ((float) intValue) / ((float) intValue2), 1.0f), PropertyValuesHolder.ofFloat("scaleY", ((float) intValue3) / ((float) intValue4), 1.0f));
    }

    /* compiled from: CircleAvatarTransition.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J%\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u00022\u0006\u0010\b\u001a\u00020\u00022\u0006\u0010\t\u001a\u00020\u0002H\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/animation/transitions/CircleAvatarTransition$FloatInterpolatorEvaluator;", "Landroid/animation/TypeEvaluator;", "", "interpolator", "Landroid/view/animation/Interpolator;", "(Landroid/view/animation/Interpolator;)V", "evaluate", "fraction", "startValue", "endValue", "(FFF)Ljava/lang/Float;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    private static final class FloatInterpolatorEvaluator implements TypeEvaluator<Float> {
        private final Interpolator interpolator;

        public FloatInterpolatorEvaluator(Interpolator interpolator) {
            Intrinsics.checkNotNullParameter(interpolator, "interpolator");
            this.interpolator = interpolator;
        }

        @Override // android.animation.TypeEvaluator
        public /* bridge */ /* synthetic */ Float evaluate(float f, Float f2, Float f3) {
            return evaluate(f, f2.floatValue(), f3.floatValue());
        }

        public Float evaluate(float f, float f2, float f3) {
            return Float.valueOf(((f3 - f2) * this.interpolator.getInterpolation(f)) + f2);
        }
    }
}
