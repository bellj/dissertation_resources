package org.thoughtcrime.securesms.animation;

import android.animation.Animator;
import androidx.core.util.Consumer;

/* loaded from: classes.dex */
public final class AnimationRepeatListener implements Animator.AnimatorListener {
    private final Consumer<Animator> animationConsumer;

    @Override // android.animation.Animator.AnimatorListener
    public final void onAnimationCancel(Animator animator) {
    }

    @Override // android.animation.Animator.AnimatorListener
    public final void onAnimationEnd(Animator animator) {
    }

    @Override // android.animation.Animator.AnimatorListener
    public final void onAnimationStart(Animator animator) {
    }

    public AnimationRepeatListener(Consumer<Animator> consumer) {
        this.animationConsumer = consumer;
    }

    @Override // android.animation.Animator.AnimatorListener
    public final void onAnimationRepeat(Animator animator) {
        this.animationConsumer.accept(animator);
    }
}
