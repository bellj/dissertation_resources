package org.thoughtcrime.securesms.animation.transitions;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.util.AttributeSet;
import android.view.ViewGroup;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: FabElevationFadeTransform.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0007\u0018\u0000 \u00122\u00020\u0001:\u0001\u0012B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J(\u0010\f\u001a\u0004\u0018\u00010\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\n2\b\u0010\u0011\u001a\u0004\u0018\u00010\nH\u0016¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/animation/transitions/FabElevationFadeTransform;", "Landroid/transition/Transition;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "captureEndValues", "", "transitionValues", "Landroid/transition/TransitionValues;", "captureStartValues", "createAnimator", "Landroid/animation/Animator;", "sceneRoot", "Landroid/view/ViewGroup;", "startValues", "endValues", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class FabElevationFadeTransform extends Transition {
    public static final Companion Companion = new Companion(null);
    private static final String ELEVATION;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public FabElevationFadeTransform(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    /* compiled from: FabElevationFadeTransform.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/animation/transitions/FabElevationFadeTransform$Companion;", "", "()V", "ELEVATION", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    @Override // android.transition.Transition
    public void captureStartValues(TransitionValues transitionValues) {
        Intrinsics.checkNotNullParameter(transitionValues, "transitionValues");
        if (transitionValues.view instanceof FloatingActionButton) {
            Map map = transitionValues.values;
            Intrinsics.checkNotNullExpressionValue(map, "transitionValues.values");
            map.put(ELEVATION, Float.valueOf(transitionValues.view.getElevation()));
        }
    }

    @Override // android.transition.Transition
    public void captureEndValues(TransitionValues transitionValues) {
        Intrinsics.checkNotNullParameter(transitionValues, "transitionValues");
        if (transitionValues.view instanceof FloatingActionButton) {
            Map map = transitionValues.values;
            Intrinsics.checkNotNullExpressionValue(map, "transitionValues.values");
            map.put(ELEVATION, Float.valueOf(transitionValues.view.getElevation()));
        }
    }

    @Override // android.transition.Transition
    public Animator createAnimator(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
        if ((transitionValues != null ? transitionValues.view : null) instanceof FloatingActionButton) {
            if ((transitionValues2 != null ? transitionValues2.view : null) instanceof FloatingActionButton) {
                if (transitionValues.view.getElevation() == transitionValues2.view.getElevation()) {
                    return null;
                }
                float[] fArr = new float[2];
                Object obj = transitionValues.values.get(ELEVATION);
                if (obj != null) {
                    fArr[0] = ((Float) obj).floatValue();
                    Object obj2 = transitionValues2.values.get(ELEVATION);
                    if (obj2 != null) {
                        fArr[1] = ((Float) obj2).floatValue();
                        ValueAnimator ofFloat = ValueAnimator.ofFloat(fArr);
                        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(transitionValues2) { // from class: org.thoughtcrime.securesms.animation.transitions.FabElevationFadeTransform$$ExternalSyntheticLambda0
                            public final /* synthetic */ TransitionValues f$0;

                            {
                                this.f$0 = r1;
                            }

                            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                                FabElevationFadeTransform.$r8$lambda$ipPbo3Ov6qGdWmIgAObUOJb2xYQ(this.f$0, valueAnimator);
                            }
                        });
                        return ofFloat;
                    }
                    throw new NullPointerException("null cannot be cast to non-null type kotlin.Float");
                }
                throw new NullPointerException("null cannot be cast to non-null type kotlin.Float");
            }
        }
        return null;
    }

    /* renamed from: createAnimator$lambda-1$lambda-0 */
    public static final void m269createAnimator$lambda1$lambda0(TransitionValues transitionValues, ValueAnimator valueAnimator) {
        Object animatedValue = valueAnimator.getAnimatedValue();
        if (animatedValue != null) {
            transitionValues.view.setElevation(((Float) animatedValue).floatValue());
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Float");
    }
}
