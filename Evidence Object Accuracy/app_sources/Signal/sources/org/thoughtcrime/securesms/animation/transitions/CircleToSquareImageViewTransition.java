package org.thoughtcrime.securesms.animation.transitions;

import android.animation.Animator;
import android.content.Context;
import android.transition.TransitionValues;
import android.util.AttributeSet;
import android.view.ViewGroup;

/* loaded from: classes.dex */
public final class CircleToSquareImageViewTransition extends CircleSquareImageViewTransition {
    @Override // org.thoughtcrime.securesms.animation.transitions.CircleSquareImageViewTransition, android.transition.Transition
    public /* bridge */ /* synthetic */ void captureEndValues(TransitionValues transitionValues) {
        super.captureEndValues(transitionValues);
    }

    @Override // org.thoughtcrime.securesms.animation.transitions.CircleSquareImageViewTransition, android.transition.Transition
    public /* bridge */ /* synthetic */ void captureStartValues(TransitionValues transitionValues) {
        super.captureStartValues(transitionValues);
    }

    @Override // org.thoughtcrime.securesms.animation.transitions.CircleSquareImageViewTransition, android.transition.Transition
    public /* bridge */ /* synthetic */ Animator createAnimator(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
        return super.createAnimator(viewGroup, transitionValues, transitionValues2);
    }

    public CircleToSquareImageViewTransition(Context context, AttributeSet attributeSet) {
        super(false);
    }
}
