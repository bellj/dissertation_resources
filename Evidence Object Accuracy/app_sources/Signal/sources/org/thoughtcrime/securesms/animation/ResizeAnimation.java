package org.thoughtcrime.securesms.animation;

import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/* loaded from: classes.dex */
public class ResizeAnimation extends Animation {
    private int startHeight;
    private int startWidth;
    private final View target;
    private final int targetHeightPx;
    private final int targetWidthPx;

    @Override // android.view.animation.Animation
    public boolean willChangeBounds() {
        return true;
    }

    public ResizeAnimation(View view, int i, int i2) {
        this.target = view;
        this.targetWidthPx = i;
        this.targetHeightPx = i2;
    }

    @Override // android.view.animation.Animation
    protected void applyTransformation(float f, Transformation transformation) {
        int i = this.startWidth;
        int i2 = (int) (((float) i) + (((float) (this.targetWidthPx - i)) * f));
        int i3 = this.startHeight;
        ViewGroup.LayoutParams layoutParams = this.target.getLayoutParams();
        layoutParams.width = i2;
        layoutParams.height = (int) (((float) i3) + (((float) (this.targetHeightPx - i3)) * f));
        this.target.setLayoutParams(layoutParams);
    }

    @Override // android.view.animation.Animation
    public void initialize(int i, int i2, int i3, int i4) {
        super.initialize(i, i2, i3, i4);
        this.startWidth = i;
        this.startHeight = i2;
    }
}
