package org.thoughtcrime.securesms.mms;

import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;
import j$.util.Optional;
import java.io.File;
import java.io.InputStream;
import java.security.MessageDigest;

/* loaded from: classes4.dex */
public class AttachmentStreamUriLoader implements ModelLoader<AttachmentModel, InputStream> {
    public boolean handles(AttachmentModel attachmentModel) {
        return true;
    }

    public ModelLoader.LoadData<InputStream> buildLoadData(AttachmentModel attachmentModel, int i, int i2, Options options) {
        return new ModelLoader.LoadData<>(attachmentModel, new AttachmentStreamLocalUriFetcher(attachmentModel.attachment, attachmentModel.plaintextLength, attachmentModel.key, attachmentModel.digest));
    }

    /* loaded from: classes4.dex */
    static class Factory implements ModelLoaderFactory<AttachmentModel, InputStream> {
        public void teardown() {
        }

        public ModelLoader<AttachmentModel, InputStream> build(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new AttachmentStreamUriLoader();
        }
    }

    /* loaded from: classes4.dex */
    public static class AttachmentModel implements Key {
        public File attachment;
        public Optional<byte[]> digest;
        public byte[] key;
        public long plaintextLength;

        public AttachmentModel(File file, byte[] bArr, long j, Optional<byte[]> optional) {
            this.attachment = file;
            this.key = bArr;
            this.digest = optional;
            this.plaintextLength = j;
        }

        public void updateDiskCacheKey(MessageDigest messageDigest) {
            messageDigest.update(this.attachment.toString().getBytes());
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            return this.attachment.equals(((AttachmentModel) obj).attachment);
        }

        public int hashCode() {
            return this.attachment.hashCode();
        }
    }
}
