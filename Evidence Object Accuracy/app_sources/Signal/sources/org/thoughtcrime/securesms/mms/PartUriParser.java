package org.thoughtcrime.securesms.mms;

import android.content.ContentUris;
import android.net.Uri;
import org.thoughtcrime.securesms.attachments.AttachmentId;

/* loaded from: classes4.dex */
public class PartUriParser {
    private final Uri uri;

    public PartUriParser(Uri uri) {
        this.uri = uri;
    }

    public AttachmentId getPartId() {
        return new AttachmentId(getId(), getUniqueId());
    }

    private long getId() {
        return ContentUris.parseId(this.uri);
    }

    private long getUniqueId() {
        return Long.parseLong(this.uri.getPathSegments().get(1));
    }
}
