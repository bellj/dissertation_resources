package org.thoughtcrime.securesms.mms;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.DataFetcher;
import j$.util.Optional;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.whispersystems.signalservice.api.crypto.AttachmentCipherInputStream;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public class AttachmentStreamLocalUriFetcher implements DataFetcher<InputStream> {
    private static final String TAG = Log.tag(AttachmentStreamLocalUriFetcher.class);
    private final File attachment;
    private final Optional<byte[]> digest;
    private InputStream is;
    private final byte[] key;
    private final long plaintextLength;

    @Override // com.bumptech.glide.load.data.DataFetcher
    public void cancel() {
    }

    public AttachmentStreamLocalUriFetcher(File file, long j, byte[] bArr, Optional<byte[]> optional) {
        this.attachment = file;
        this.plaintextLength = j;
        this.digest = optional;
        this.key = bArr;
    }

    @Override // com.bumptech.glide.load.data.DataFetcher
    public void loadData(Priority priority, DataFetcher.DataCallback<? super InputStream> dataCallback) {
        try {
            if (this.digest.isPresent()) {
                InputStream createForAttachment = AttachmentCipherInputStream.createForAttachment(this.attachment, this.plaintextLength, this.key, this.digest.get());
                this.is = createForAttachment;
                dataCallback.onDataReady(createForAttachment);
                return;
            }
            throw new InvalidMessageException("No attachment digest!");
        } catch (IOException | InvalidMessageException e) {
            dataCallback.onLoadFailed(e);
        }
    }

    @Override // com.bumptech.glide.load.data.DataFetcher
    public void cleanup() {
        try {
            InputStream inputStream = this.is;
            if (inputStream != null) {
                inputStream.close();
            }
            this.is = null;
        } catch (IOException unused) {
            Log.w(TAG, "ioe");
        }
    }

    @Override // com.bumptech.glide.load.data.DataFetcher
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }

    @Override // com.bumptech.glide.load.data.DataFetcher
    public DataSource getDataSource() {
        return DataSource.LOCAL;
    }
}
