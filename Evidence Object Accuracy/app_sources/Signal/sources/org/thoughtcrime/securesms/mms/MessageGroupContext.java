package org.thoughtcrime.securesms.mms;

import com.annimon.stream.Stream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.groups.GroupMasterKey;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;
import org.signal.storageservice.protos.groups.local.DecryptedMember;
import org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2Context;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.Base64;
import org.whispersystems.signalservice.api.groupsv2.DecryptedGroupUtil;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.util.UuidUtil;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes4.dex */
public final class MessageGroupContext {
    private final String encodedGroupContext;
    private final GroupProperties group;
    private final GroupV1Properties groupV1;
    private final GroupV2Properties groupV2;

    /* access modifiers changed from: package-private */
    /* loaded from: classes4.dex */
    public interface GroupProperties {
        List<RecipientId> getMembersListExcludingSelf();

        String getName();
    }

    public MessageGroupContext(String str, boolean z) throws IOException {
        this.encodedGroupContext = str;
        if (z) {
            this.groupV1 = null;
            GroupV2Properties groupV2Properties = new GroupV2Properties(DecryptedGroupV2Context.parseFrom(Base64.decode(str)));
            this.groupV2 = groupV2Properties;
            this.group = groupV2Properties;
            return;
        }
        GroupV1Properties groupV1Properties = new GroupV1Properties(SignalServiceProtos.GroupContext.parseFrom(Base64.decode(str)));
        this.groupV1 = groupV1Properties;
        this.groupV2 = null;
        this.group = groupV1Properties;
    }

    public MessageGroupContext(SignalServiceProtos.GroupContext groupContext) {
        this.encodedGroupContext = Base64.encodeBytes(groupContext.toByteArray());
        GroupV1Properties groupV1Properties = new GroupV1Properties(groupContext);
        this.groupV1 = groupV1Properties;
        this.groupV2 = null;
        this.group = groupV1Properties;
    }

    public MessageGroupContext(DecryptedGroupV2Context decryptedGroupV2Context) {
        this.encodedGroupContext = Base64.encodeBytes(decryptedGroupV2Context.toByteArray());
        this.groupV1 = null;
        GroupV2Properties groupV2Properties = new GroupV2Properties(decryptedGroupV2Context);
        this.groupV2 = groupV2Properties;
        this.group = groupV2Properties;
    }

    public GroupV1Properties requireGroupV1Properties() {
        GroupV1Properties groupV1Properties = this.groupV1;
        if (groupV1Properties != null) {
            return groupV1Properties;
        }
        throw new AssertionError();
    }

    public GroupV2Properties requireGroupV2Properties() {
        GroupV2Properties groupV2Properties = this.groupV2;
        if (groupV2Properties != null) {
            return groupV2Properties;
        }
        throw new AssertionError();
    }

    public boolean isV2Group() {
        return this.groupV2 != null;
    }

    public String getEncodedGroupContext() {
        return this.encodedGroupContext;
    }

    public String getName() {
        return this.group.getName();
    }

    public List<RecipientId> getMembersListExcludingSelf() {
        return this.group.getMembersListExcludingSelf();
    }

    /* loaded from: classes4.dex */
    public static class GroupV1Properties implements GroupProperties {
        private final SignalServiceProtos.GroupContext groupContext;

        private GroupV1Properties(SignalServiceProtos.GroupContext groupContext) {
            this.groupContext = groupContext;
        }

        public SignalServiceProtos.GroupContext getGroupContext() {
            return this.groupContext;
        }

        public boolean isQuit() {
            return this.groupContext.getType().getNumber() == 3;
        }

        public boolean isUpdate() {
            return this.groupContext.getType().getNumber() == 1;
        }

        @Override // org.thoughtcrime.securesms.mms.MessageGroupContext.GroupProperties
        public String getName() {
            return this.groupContext.getName();
        }

        @Override // org.thoughtcrime.securesms.mms.MessageGroupContext.GroupProperties
        public List<RecipientId> getMembersListExcludingSelf() {
            RecipientId id = Recipient.self().getId();
            Stream map = Stream.of(this.groupContext.getMembersList()).map(new MessageGroupContext$GroupV1Properties$$ExternalSyntheticLambda0()).withoutNulls().map(new MessageGroupContext$GroupV1Properties$$ExternalSyntheticLambda1());
            Objects.requireNonNull(id);
            return map.filterNot(new MessageGroupContext$GroupV1Properties$$ExternalSyntheticLambda2(id)).toList();
        }
    }

    /* loaded from: classes4.dex */
    public static class GroupV2Properties implements GroupProperties {
        private final DecryptedGroupV2Context decryptedGroupV2Context;
        private final SignalServiceProtos.GroupContextV2 groupContext;
        private final GroupMasterKey groupMasterKey;

        private GroupV2Properties(DecryptedGroupV2Context decryptedGroupV2Context) {
            this.decryptedGroupV2Context = decryptedGroupV2Context;
            SignalServiceProtos.GroupContextV2 context = decryptedGroupV2Context.getContext();
            this.groupContext = context;
            try {
                this.groupMasterKey = new GroupMasterKey(context.getMasterKey().toByteArray());
            } catch (InvalidInputException e) {
                throw new AssertionError(e);
            }
        }

        public SignalServiceProtos.GroupContextV2 getGroupContext() {
            return this.groupContext;
        }

        public GroupMasterKey getGroupMasterKey() {
            return this.groupMasterKey;
        }

        public DecryptedGroupChange getChange() {
            return this.decryptedGroupV2Context.getChange();
        }

        public List<UUID> getAllActivePendingAndRemovedMembers() {
            LinkedList linkedList = new LinkedList();
            DecryptedGroup groupState = this.decryptedGroupV2Context.getGroupState();
            DecryptedGroupChange change = this.decryptedGroupV2Context.getChange();
            linkedList.addAll(DecryptedGroupUtil.membersToUuidList(groupState.getMembersList()));
            linkedList.addAll(DecryptedGroupUtil.pendingToUuidList(groupState.getPendingMembersList()));
            linkedList.addAll(DecryptedGroupUtil.removedMembersUuidList(change));
            linkedList.addAll(DecryptedGroupUtil.removedPendingMembersUuidList(change));
            linkedList.addAll(DecryptedGroupUtil.removedRequestingMembersUuidList(change));
            return UuidUtil.filterKnown(linkedList);
        }

        @Override // org.thoughtcrime.securesms.mms.MessageGroupContext.GroupProperties
        public String getName() {
            return this.decryptedGroupV2Context.getGroupState().getTitle();
        }

        @Override // org.thoughtcrime.securesms.mms.MessageGroupContext.GroupProperties
        public List<RecipientId> getMembersListExcludingSelf() {
            ArrayList arrayList = new ArrayList(this.decryptedGroupV2Context.getGroupState().getMembersCount());
            for (DecryptedMember decryptedMember : this.decryptedGroupV2Context.getGroupState().getMembersList()) {
                RecipientId from = RecipientId.from(ServiceId.fromByteString(decryptedMember.getUuid()));
                if (!Recipient.self().getId().equals(from)) {
                    arrayList.add(from);
                }
            }
            return arrayList;
        }
    }
}
