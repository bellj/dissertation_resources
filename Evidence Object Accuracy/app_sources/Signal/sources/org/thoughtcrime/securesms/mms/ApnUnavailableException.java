package org.thoughtcrime.securesms.mms;

/* loaded from: classes4.dex */
public class ApnUnavailableException extends Exception {
    public ApnUnavailableException() {
    }

    public ApnUnavailableException(String str) {
        super(str);
    }

    public ApnUnavailableException(Throwable th) {
        super(th);
    }

    public ApnUnavailableException(String str, Throwable th) {
        super(str, th);
    }
}
