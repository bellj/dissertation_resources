package org.thoughtcrime.securesms.mms;

import android.content.Context;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import j$.util.Optional;
import java.util.LinkedList;
import java.util.List;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class SlideDeck {
    private final List<Slide> slides;

    public SlideDeck(Context context, List<? extends Attachment> list) {
        this.slides = new LinkedList();
        for (Attachment attachment : list) {
            Slide slideForAttachment = MediaUtil.getSlideForAttachment(context, attachment);
            if (slideForAttachment != null) {
                this.slides.add(slideForAttachment);
            }
        }
    }

    public SlideDeck(Context context, Attachment attachment) {
        LinkedList linkedList = new LinkedList();
        this.slides = linkedList;
        Slide slideForAttachment = MediaUtil.getSlideForAttachment(context, attachment);
        if (slideForAttachment != null) {
            linkedList.add(slideForAttachment);
        }
    }

    public SlideDeck() {
        this.slides = new LinkedList();
    }

    public void clear() {
        this.slides.clear();
    }

    public String getBody() {
        String str = "";
        for (Slide slide : this.slides) {
            Optional<String> body = slide.getBody();
            if (body.isPresent()) {
                str = body.get();
            }
        }
        return str;
    }

    public List<Attachment> asAttachments() {
        LinkedList linkedList = new LinkedList();
        for (Slide slide : this.slides) {
            linkedList.add(slide.asAttachment());
        }
        return linkedList;
    }

    public void addSlide(Slide slide) {
        this.slides.add(slide);
    }

    public List<Slide> getSlides() {
        return this.slides;
    }

    /* JADX WARNING: Removed duplicated region for block: B:5:0x000c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean containsMediaSlide() {
        /*
            r3 = this;
            java.util.List<org.thoughtcrime.securesms.mms.Slide> r0 = r3.slides
            java.util.Iterator r0 = r0.iterator()
        L_0x0006:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0038
            java.lang.Object r1 = r0.next()
            org.thoughtcrime.securesms.mms.Slide r1 = (org.thoughtcrime.securesms.mms.Slide) r1
            boolean r2 = r1.hasImage()
            if (r2 != 0) goto L_0x0036
            boolean r2 = r1.hasVideo()
            if (r2 != 0) goto L_0x0036
            boolean r2 = r1.hasAudio()
            if (r2 != 0) goto L_0x0036
            boolean r2 = r1.hasDocument()
            if (r2 != 0) goto L_0x0036
            boolean r2 = r1.hasSticker()
            if (r2 != 0) goto L_0x0036
            boolean r1 = r1.hasViewOnce()
            if (r1 == 0) goto L_0x0006
        L_0x0036:
            r0 = 1
            return r0
        L_0x0038:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.mms.SlideDeck.containsMediaSlide():boolean");
    }

    public Slide getThumbnailSlide() {
        for (Slide slide : this.slides) {
            if (slide.hasImage()) {
                return slide;
            }
        }
        return null;
    }

    public List<Slide> getThumbnailSlides() {
        return Stream.of(this.slides).filter(new Predicate() { // from class: org.thoughtcrime.securesms.mms.SlideDeck$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ((Slide) obj).hasImage();
            }
        }).toList();
    }

    public AudioSlide getAudioSlide() {
        for (Slide slide : this.slides) {
            if (slide.hasAudio()) {
                return (AudioSlide) slide;
            }
        }
        return null;
    }

    public DocumentSlide getDocumentSlide() {
        for (Slide slide : this.slides) {
            if (slide.hasDocument()) {
                return (DocumentSlide) slide;
            }
        }
        return null;
    }

    public TextSlide getTextSlide() {
        for (Slide slide : this.slides) {
            if (MediaUtil.isLongTextType(slide.getContentType())) {
                return (TextSlide) slide;
            }
        }
        return null;
    }

    public StickerSlide getStickerSlide() {
        for (Slide slide : this.slides) {
            if (slide.hasSticker()) {
                return (StickerSlide) slide;
            }
        }
        return null;
    }

    public Slide getFirstSlide() {
        if (Util.hasItems(this.slides)) {
            return this.slides.get(0);
        }
        return null;
    }
}
