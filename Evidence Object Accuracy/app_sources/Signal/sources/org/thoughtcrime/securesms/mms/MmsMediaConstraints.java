package org.thoughtcrime.securesms.mms;

import android.content.Context;
import com.android.mms.service_alt.MmsConfig;

/* loaded from: classes4.dex */
public final class MmsMediaConstraints extends MediaConstraints {
    private static final int MIN_IMAGE_DIMEN;
    private final int subscriptionId;

    public MmsMediaConstraints(int i) {
        this.subscriptionId = i;
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getImageMaxWidth(Context context) {
        return Math.max((int) MIN_IMAGE_DIMEN, getOverriddenMmsConfig(context).getMaxImageWidth());
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getImageMaxHeight(Context context) {
        return Math.max((int) MIN_IMAGE_DIMEN, getOverriddenMmsConfig(context).getMaxImageHeight());
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int[] getImageDimensionTargets(Context context) {
        int[] iArr = new int[4];
        iArr[0] = getImageMaxHeight(context);
        for (int i = 1; i < 4; i++) {
            iArr[i] = iArr[i - 1] / 2;
        }
        return iArr;
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getImageMaxSize(Context context) {
        return getMaxMessageSize(context);
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getGifMaxSize(Context context) {
        return getMaxMessageSize(context);
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getVideoMaxSize(Context context) {
        return getMaxMessageSize(context);
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getUncompressedVideoMaxSize(Context context) {
        return Math.max(getVideoMaxSize(context), 15728640);
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getAudioMaxSize(Context context) {
        return getMaxMessageSize(context);
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getDocumentMaxSize(Context context) {
        return getMaxMessageSize(context);
    }

    private int getMaxMessageSize(Context context) {
        return getOverriddenMmsConfig(context).getMaxMessageSize();
    }

    private MmsConfig.Overridden getOverriddenMmsConfig(Context context) {
        return new MmsConfig.Overridden(MmsConfigManager.getMmsConfig(context, this.subscriptionId), null);
    }
}
