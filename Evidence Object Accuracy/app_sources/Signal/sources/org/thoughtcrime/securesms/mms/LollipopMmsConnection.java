package org.thoughtcrime.securesms.mms;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import java.util.concurrent.TimeoutException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public abstract class LollipopMmsConnection extends BroadcastReceiver {
    private static final String TAG = Log.tag(LollipopMmsConnection.class);
    private final String action;
    private final Context context;
    private boolean resultAvailable;

    public abstract void onResult(Context context, Intent intent);

    public LollipopMmsConnection(Context context, String str) {
        this.context = context;
        this.action = str;
    }

    @Override // android.content.BroadcastReceiver
    public synchronized void onReceive(Context context, Intent intent) {
        String str = TAG;
        Log.i(str, "onReceive()");
        if (!this.action.equals(intent.getAction())) {
            Log.w(str, "received broadcast with unexpected action " + intent.getAction());
            return;
        }
        onResult(context, intent);
        this.resultAvailable = true;
        notifyAll();
    }

    public void beginTransaction() {
        getContext().getApplicationContext().registerReceiver(this, new IntentFilter(this.action));
    }

    public void endTransaction() {
        getContext().getApplicationContext().unregisterReceiver(this);
        this.resultAvailable = false;
    }

    public void waitForResult() throws TimeoutException {
        long currentTimeMillis = System.currentTimeMillis() + 60000;
        while (!this.resultAvailable) {
            Util.wait(this, Math.max(1L, currentTimeMillis - System.currentTimeMillis()));
            if (System.currentTimeMillis() >= currentTimeMillis) {
                throw new TimeoutException("timeout when waiting for MMS");
            }
        }
    }

    public PendingIntent getPendingIntent() {
        return PendingIntent.getBroadcast(getContext(), 1, new Intent(this.action), 1073741824);
    }

    public Context getContext() {
        return this.context;
    }
}
