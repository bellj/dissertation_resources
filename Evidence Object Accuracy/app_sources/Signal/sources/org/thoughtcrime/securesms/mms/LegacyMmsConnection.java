package org.thoughtcrime.securesms.mms;

import android.content.Context;
import android.net.ConnectivityManager;
import android.text.TextUtils;
import androidx.core.content.ContextCompat;
import j$.util.Optional;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.URL;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import org.apache.http.Header;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.NoConnectionReuseStrategyHC4;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.ApnDatabase;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.TelephonyUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public abstract class LegacyMmsConnection {
    private static final String TAG = Log.tag(LegacyMmsConnection.class);
    public static final String USER_AGENT;
    protected final Apn apn;
    protected final Context context;

    public LegacyMmsConnection(Context context) throws ApnUnavailableException {
        this.context = context;
        this.apn = getApn(context);
    }

    public static Apn getApn(Context context) throws ApnUnavailableException {
        try {
            Optional<Apn> mmsConnectionParameters = ApnDatabase.getInstance(context).getMmsConnectionParameters(TelephonyUtil.getMccMnc(context), TelephonyUtil.getApn(context));
            if (mmsConnectionParameters.isPresent()) {
                return mmsConnectionParameters.get();
            }
            throw new ApnUnavailableException("No parameters available from ApnDefaults.");
        } catch (IOException e) {
            throw new ApnUnavailableException("ApnDatabase threw an IOException", e);
        }
    }

    public boolean isDirectConnect() {
        return ServiceUtil.getTelephonyManager(this.context).getPhoneType() == 2 && new HashSet<String>() { // from class: org.thoughtcrime.securesms.mms.LegacyMmsConnection.1
            {
                add("312530");
                add("311880");
                add("311870");
                add("311490");
                add("310120");
                add("316010");
                add("312190");
            }
        }.contains(TelephonyUtil.getMccMnc(this.context));
    }

    public static boolean checkRouteToHost(Context context, String str, boolean z) throws IOException {
        InetAddress byName = InetAddress.getByName(str);
        if (!z) {
            if (!byName.isSiteLocalAddress()) {
                Log.w(TAG, "returning vacuous success since MMS radio is not in use");
                return true;
            }
            throw new IOException("RFC1918 address in non-MMS radio situation!");
        } else if (byName == null) {
            throw new IOException("Unable to lookup host: InetAddress.getByName() returned null.");
        } else if (byName.getAddress() == null) {
            Log.w(TAG, "resolved IP address bytes are null, returning true to attempt a connection anyway.");
            return true;
        } else {
            String str2 = TAG;
            Log.i(str2, "Checking route to address: " + str + ", " + byName.getHostAddress());
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            try {
                boolean booleanValue = ((Boolean) connectivityManager.getClass().getMethod("requestRouteToHostAddress", Integer.TYPE, InetAddress.class).invoke(connectivityManager, 2, byName)).booleanValue();
                Log.i(str2, "requestRouteToHostAddress(" + byName + ") -> " + booleanValue);
                return booleanValue;
            } catch (IllegalAccessException e) {
                Log.w(TAG, e);
                return false;
            } catch (NoSuchMethodException e2) {
                Log.w(TAG, e2);
                return false;
            } catch (InvocationTargetException e3) {
                Log.w(TAG, e3);
                return false;
            }
        }
    }

    protected static byte[] parseResponse(InputStream inputStream) throws IOException {
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        StreamUtil.copy(bufferedInputStream, byteArrayOutputStream);
        String str = TAG;
        Log.i(str, "Received full server response, " + byteArrayOutputStream.size() + " bytes");
        return byteArrayOutputStream.toByteArray();
    }

    protected CloseableHttpClient constructHttpClient() throws IOException {
        RequestConfig build = RequestConfig.custom().setConnectTimeout(20000).setConnectionRequestTimeout(20000).setSocketTimeout(20000).setMaxRedirects(20).build();
        URL url = new URL(this.apn.getMmsc());
        CredentialsProvider basicCredentialsProvider = new BasicCredentialsProvider();
        if (this.apn.hasAuthentication()) {
            basicCredentialsProvider.setCredentials(new AuthScope(url.getHost(), url.getPort() > -1 ? url.getPort() : url.getDefaultPort()), new UsernamePasswordCredentials(this.apn.getUsername(), this.apn.getPassword()));
        }
        return HttpClients.custom().setConnectionReuseStrategy(new NoConnectionReuseStrategyHC4()).setRedirectStrategy(new LaxRedirectStrategy()).setUserAgent(TextSecurePreferences.getMmsUserAgent(this.context, USER_AGENT)).setConnectionManager(new BasicHttpClientConnectionManager()).setDefaultRequestConfig(build).setDefaultCredentialsProvider(basicCredentialsProvider).build();
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0085  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] execute(org.apache.http.client.methods.HttpUriRequest r5) throws java.io.IOException {
        /*
            r4 = this;
            java.lang.String r0 = org.thoughtcrime.securesms.mms.LegacyMmsConnection.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "connecting to "
            r1.append(r2)
            org.thoughtcrime.securesms.mms.LegacyMmsConnection$Apn r2 = r4.apn
            java.lang.String r2 = r2.getMmsc()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            org.signal.core.util.logging.Log.i(r0, r1)
            r1 = 0
            org.apache.http.impl.client.CloseableHttpClient r2 = r4.constructHttpClient()     // Catch: NullPointerException -> 0x0073, all -> 0x0070
            org.apache.http.client.methods.CloseableHttpResponse r1 = r2.execute(r5)     // Catch: NullPointerException -> 0x006c, all -> 0x006a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch: NullPointerException -> 0x006c, all -> 0x006a
            r5.<init>()     // Catch: NullPointerException -> 0x006c, all -> 0x006a
            java.lang.String r3 = "* response code: "
            r5.append(r3)     // Catch: NullPointerException -> 0x006c, all -> 0x006a
            org.apache.http.StatusLine r3 = r1.getStatusLine()     // Catch: NullPointerException -> 0x006c, all -> 0x006a
            r5.append(r3)     // Catch: NullPointerException -> 0x006c, all -> 0x006a
            java.lang.String r5 = r5.toString()     // Catch: NullPointerException -> 0x006c, all -> 0x006a
            org.signal.core.util.logging.Log.i(r0, r5)     // Catch: NullPointerException -> 0x006c, all -> 0x006a
            org.apache.http.StatusLine r5 = r1.getStatusLine()     // Catch: NullPointerException -> 0x006c, all -> 0x006a
            int r5 = r5.getStatusCode()     // Catch: NullPointerException -> 0x006c, all -> 0x006a
            r0 = 200(0xc8, float:2.8E-43)
            if (r5 != r0) goto L_0x005c
            org.apache.http.HttpEntity r5 = r1.getEntity()     // Catch: NullPointerException -> 0x006c, all -> 0x006a
            java.io.InputStream r5 = r5.getContent()     // Catch: NullPointerException -> 0x006c, all -> 0x006a
            byte[] r5 = parseResponse(r5)     // Catch: NullPointerException -> 0x006c, all -> 0x006a
            r1.close()
            r2.close()
            return r5
        L_0x005c:
            r1.close()
            r2.close()
            java.io.IOException r5 = new java.io.IOException
            java.lang.String r0 = "unhandled response code"
            r5.<init>(r0)
            throw r5
        L_0x006a:
            r5 = move-exception
            goto L_0x007e
        L_0x006c:
            r5 = move-exception
            r0 = r1
            r1 = r2
            goto L_0x0075
        L_0x0070:
            r5 = move-exception
            r2 = r1
            goto L_0x007e
        L_0x0073:
            r5 = move-exception
            r0 = r1
        L_0x0075:
            java.io.IOException r2 = new java.io.IOException     // Catch: all -> 0x007b
            r2.<init>(r5)     // Catch: all -> 0x007b
            throw r2     // Catch: all -> 0x007b
        L_0x007b:
            r5 = move-exception
            r2 = r1
            r1 = r0
        L_0x007e:
            if (r1 == 0) goto L_0x0083
            r1.close()
        L_0x0083:
            if (r2 == 0) goto L_0x0088
            r2.close()
        L_0x0088:
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.mms.LegacyMmsConnection.execute(org.apache.http.client.methods.HttpUriRequest):byte[]");
    }

    public List<Header> getBaseHeaders() {
        return new LinkedList<Header>(getLine1Number(this.context)) { // from class: org.thoughtcrime.securesms.mms.LegacyMmsConnection.2
            final /* synthetic */ String val$number;

            {
                this.val$number = r4;
                add(new BasicHeader("Accept", "*/*, application/vnd.wap.mms-message, application/vnd.wap.sic"));
                add(new BasicHeader("x-wap-profile", "http://www.google.com/oha/rdf/ua-profile-kila.xml"));
                add(new BasicHeader("Content-Type", "application/vnd.wap.mms-message"));
                add(new BasicHeader("x-carrier-magic", "http://magic.google.com"));
                if (!TextUtils.isEmpty(r4)) {
                    add(new BasicHeader("x-up-calling-line-id", r4));
                    add(new BasicHeader("X-MDN", r4));
                }
            }
        };
    }

    private static String getLine1Number(Context context) {
        if (ContextCompat.checkSelfPermission(context, "android.permission.READ_SMS") == 0 || ContextCompat.checkSelfPermission(context, "android.permission.READ_PHONE_NUMBERS") == 0 || ContextCompat.checkSelfPermission(context, "android.permission.READ_PHONE_STATE") == 0) {
            return TelephonyUtil.getManager(context).getLine1Number();
        }
        return "";
    }

    /* loaded from: classes4.dex */
    public static class Apn {
        public static Apn EMPTY = new Apn("", "", "", "", "");
        private final String mmsc;
        private final String password;
        private final String port;
        private final String proxy;
        private final String username;

        public Apn(String str, String str2, String str3, String str4, String str5) {
            this.mmsc = str;
            this.proxy = str2;
            this.port = str3;
            this.username = str4;
            this.password = str5;
        }

        public Apn(Apn apn, Apn apn2, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
            this.mmsc = z ? apn.mmsc : apn2.mmsc;
            this.proxy = z2 ? apn.proxy : apn2.proxy;
            this.port = z3 ? apn.port : apn2.port;
            this.username = z4 ? apn.username : apn2.username;
            this.password = z5 ? apn.password : apn2.password;
        }

        public boolean hasProxy() {
            return !TextUtils.isEmpty(this.proxy);
        }

        public String getMmsc() {
            return this.mmsc;
        }

        public String getProxy() {
            if (hasProxy()) {
                return this.proxy;
            }
            return null;
        }

        public int getPort() {
            if (TextUtils.isEmpty(this.port)) {
                return 80;
            }
            return Integer.parseInt(this.port);
        }

        public boolean hasAuthentication() {
            return !TextUtils.isEmpty(this.username);
        }

        public String getUsername() {
            return this.username;
        }

        public String getPassword() {
            return this.password;
        }

        public String toString() {
            String str;
            String str2;
            StringBuilder sb = new StringBuilder();
            sb.append(Log.tag(Apn.class));
            sb.append("{ mmsc: \"");
            sb.append(this.mmsc);
            sb.append("\", proxy: ");
            String str3 = "none";
            if (this.proxy == null) {
                str = str3;
            } else {
                str = '\"' + this.proxy + '\"';
            }
            sb.append(str);
            sb.append(", port: ");
            String str4 = this.port;
            if (str4 == null) {
                str4 = "(none)";
            }
            sb.append(str4);
            sb.append(", user: ");
            if (this.username == null) {
                str2 = str3;
            } else {
                str2 = '\"' + this.username + '\"';
            }
            sb.append(str2);
            sb.append(", pass: ");
            if (this.password != null) {
                str3 = '\"' + this.password + '\"';
            }
            sb.append(str3);
            sb.append(" }");
            return sb.toString();
        }
    }
}
