package org.thoughtcrime.securesms.mms;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MessageGroupContext$GroupV1Properties$$ExternalSyntheticLambda1 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return RecipientId.fromE164((String) obj);
    }
}
