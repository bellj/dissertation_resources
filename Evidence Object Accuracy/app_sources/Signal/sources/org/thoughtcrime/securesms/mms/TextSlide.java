package org.thoughtcrime.securesms.mms;

import android.content.Context;
import android.net.Uri;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.util.MediaUtil;

/* loaded from: classes4.dex */
public class TextSlide extends Slide {
    public TextSlide(Context context, Attachment attachment) {
        super(context, attachment);
    }

    public TextSlide(Context context, Uri uri, String str, long j) {
        super(context, Slide.constructAttachmentFromUri(context, uri, MediaUtil.LONG_TEXT, j, 0, 0, true, str, null, null, null, null, false, false, false, false));
    }
}
