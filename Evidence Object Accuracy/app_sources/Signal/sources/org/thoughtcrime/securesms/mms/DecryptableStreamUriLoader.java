package org.thoughtcrime.securesms.mms;

import android.content.Context;
import android.net.Uri;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;
import java.io.InputStream;
import java.security.MessageDigest;

/* loaded from: classes4.dex */
public class DecryptableStreamUriLoader implements ModelLoader<DecryptableUri, InputStream> {
    private final Context context;

    public boolean handles(DecryptableUri decryptableUri) {
        return true;
    }

    private DecryptableStreamUriLoader(Context context) {
        this.context = context;
    }

    public ModelLoader.LoadData<InputStream> buildLoadData(DecryptableUri decryptableUri, int i, int i2, Options options) {
        return new ModelLoader.LoadData<>(decryptableUri, new DecryptableStreamLocalUriFetcher(this.context, decryptableUri.uri));
    }

    /* loaded from: classes4.dex */
    static class Factory implements ModelLoaderFactory<DecryptableUri, InputStream> {
        private final Context context;

        public void teardown() {
        }

        public Factory(Context context) {
            this.context = context.getApplicationContext();
        }

        public ModelLoader<DecryptableUri, InputStream> build(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new DecryptableStreamUriLoader(this.context);
        }
    }

    /* loaded from: classes4.dex */
    public static class DecryptableUri implements Key {
        public Uri uri;

        public DecryptableUri(Uri uri) {
            this.uri = uri;
        }

        public void updateDiskCacheKey(MessageDigest messageDigest) {
            messageDigest.update(this.uri.toString().getBytes());
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            return this.uri.equals(((DecryptableUri) obj).uri);
        }

        public int hashCode() {
            return this.uri.hashCode();
        }
    }
}
