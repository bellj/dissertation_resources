package org.thoughtcrime.securesms.mms;

import android.view.View;

/* loaded from: classes4.dex */
public interface SlideClickListener {
    void onClick(View view, Slide slide);
}
