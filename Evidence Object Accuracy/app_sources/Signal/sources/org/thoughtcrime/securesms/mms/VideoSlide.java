package org.thoughtcrime.securesms.mms;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4PlaybackPolicy;
import org.thoughtcrime.securesms.util.MediaUtil;

/* loaded from: classes4.dex */
public class VideoSlide extends Slide {
    @Override // org.thoughtcrime.securesms.mms.Slide
    public int getPlaceholderRes(Resources.Theme theme) {
        return R.drawable.ic_video;
    }

    @Override // org.thoughtcrime.securesms.mms.Slide
    public boolean hasImage() {
        return true;
    }

    @Override // org.thoughtcrime.securesms.mms.Slide
    public boolean hasPlaceholder() {
        return true;
    }

    @Override // org.thoughtcrime.securesms.mms.Slide
    public boolean hasVideo() {
        return true;
    }

    public VideoSlide(Context context, Uri uri, long j, boolean z) {
        this(context, uri, j, z, null, null);
    }

    public VideoSlide(Context context, Uri uri, long j, boolean z, String str, AttachmentDatabase.TransformProperties transformProperties) {
        super(context, Slide.constructAttachmentFromUri(context, uri, MediaUtil.VIDEO_UNSPECIFIED, j, 0, 0, MediaUtil.hasVideoThumbnail(context, uri), null, str, null, null, null, false, false, z, false, transformProperties));
    }

    public VideoSlide(Context context, Uri uri, long j, boolean z, int i, int i2, String str, AttachmentDatabase.TransformProperties transformProperties) {
        super(context, Slide.constructAttachmentFromUri(context, uri, MediaUtil.VIDEO_UNSPECIFIED, j, i, i2, MediaUtil.hasVideoThumbnail(context, uri), null, str, null, null, null, false, false, z, false, transformProperties));
    }

    public VideoSlide(Context context, Attachment attachment) {
        super(context, attachment);
    }

    @Override // org.thoughtcrime.securesms.mms.Slide
    public boolean hasPlayOverlay() {
        return !isVideoGif() || !GiphyMp4PlaybackPolicy.autoplay() || Build.VERSION.SDK_INT < 23;
    }

    @Override // org.thoughtcrime.securesms.mms.Slide
    public String getContentDescription() {
        return this.context.getString(R.string.Slide_video);
    }
}
