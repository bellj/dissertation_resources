package org.thoughtcrime.securesms.mms;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.util.Pair;
import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.util.BitmapDecodingException;
import org.thoughtcrime.securesms.util.BitmapUtil;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.MemoryFileDescriptor;

/* loaded from: classes4.dex */
public abstract class MediaConstraints {
    private static final String TAG = Log.tag(MediaConstraints.class);

    public abstract int getAudioMaxSize(Context context);

    public abstract int getDocumentMaxSize(Context context);

    public abstract int getGifMaxSize(Context context);

    public int getImageCompressionQualitySetting(Context context) {
        return 70;
    }

    public abstract int[] getImageDimensionTargets(Context context);

    public abstract int getImageMaxHeight(Context context);

    public abstract int getImageMaxSize(Context context);

    public abstract int getImageMaxWidth(Context context);

    public abstract int getVideoMaxSize(Context context);

    public boolean isHighQuality() {
        return false;
    }

    public static MediaConstraints getPushMediaConstraints() {
        return getPushMediaConstraints(null);
    }

    public static MediaConstraints getPushMediaConstraints(SentMediaQuality sentMediaQuality) {
        return new PushMediaConstraints(sentMediaQuality);
    }

    public static MediaConstraints getMmsMediaConstraints(int i) {
        return new MmsMediaConstraints(i);
    }

    public int getUncompressedVideoMaxSize(Context context) {
        return getVideoMaxSize(context);
    }

    public int getCompressedVideoMaxSize(Context context) {
        return getVideoMaxSize(context);
    }

    public boolean isSatisfied(Context context, Attachment attachment) {
        try {
            if ((!MediaUtil.isGif(attachment) || attachment.getSize() > ((long) getGifMaxSize(context)) || !isWithinBounds(context, attachment.getUri())) && ((!MediaUtil.isImage(attachment) || attachment.getSize() > ((long) getImageMaxSize(context)) || !isWithinBounds(context, attachment.getUri())) && ((!MediaUtil.isAudio(attachment) || attachment.getSize() > ((long) getAudioMaxSize(context))) && (!MediaUtil.isVideo(attachment) || attachment.getSize() > ((long) getVideoMaxSize(context)))))) {
                if (!MediaUtil.isFile(attachment)) {
                    return false;
                }
                if (attachment.getSize() > ((long) getDocumentMaxSize(context))) {
                    return false;
                }
            }
            return true;
        } catch (IOException e) {
            Log.w(TAG, "Failed to determine if media's constraints are satisfied.", e);
            return false;
        }
    }

    public boolean isSatisfied(Context context, Uri uri, String str, long j) {
        try {
            if ((!MediaUtil.isGif(str) || j > ((long) getGifMaxSize(context)) || !isWithinBounds(context, uri)) && ((!MediaUtil.isImageType(str) || j > ((long) getImageMaxSize(context)) || !isWithinBounds(context, uri)) && ((!MediaUtil.isAudioType(str) || j > ((long) getAudioMaxSize(context))) && (!MediaUtil.isVideoType(str) || j > ((long) getVideoMaxSize(context)))))) {
                if (j > ((long) getDocumentMaxSize(context))) {
                    return false;
                }
            }
            return true;
        } catch (IOException e) {
            Log.w(TAG, "Failed to determine if media's constraints are satisfied.", e);
            return false;
        }
    }

    private boolean isWithinBounds(Context context, Uri uri) throws IOException {
        try {
            Pair<Integer, Integer> dimensions = BitmapUtil.getDimensions(PartAuthority.getAttachmentStream(context, uri));
            if (((Integer) dimensions.first).intValue() > 0 && ((Integer) dimensions.first).intValue() <= getImageMaxWidth(context) && ((Integer) dimensions.second).intValue() > 0) {
                if (((Integer) dimensions.second).intValue() <= getImageMaxHeight(context)) {
                    return true;
                }
            }
            return false;
        } catch (BitmapDecodingException e) {
            throw new IOException(e);
        }
    }

    public boolean canResize(Attachment attachment) {
        return (MediaUtil.isImage(attachment) && !MediaUtil.isGif(attachment)) || (MediaUtil.isVideo(attachment) && isVideoTranscodeAvailable());
    }

    public boolean canResize(String str) {
        return (MediaUtil.isImageType(str) && !MediaUtil.isGif(str)) || (MediaUtil.isVideoType(str) && isVideoTranscodeAvailable());
    }

    public static boolean isVideoTranscodeAvailable() {
        return Build.VERSION.SDK_INT >= 26 && (FeatureFlags.useStreamingVideoMuxer() || MemoryFileDescriptor.supported());
    }
}
