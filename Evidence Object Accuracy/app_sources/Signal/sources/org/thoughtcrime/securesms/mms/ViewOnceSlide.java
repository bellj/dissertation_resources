package org.thoughtcrime.securesms.mms;

import android.content.Context;
import org.thoughtcrime.securesms.attachments.Attachment;

/* loaded from: classes4.dex */
public class ViewOnceSlide extends Slide {
    @Override // org.thoughtcrime.securesms.mms.Slide
    public boolean hasViewOnce() {
        return true;
    }

    public ViewOnceSlide(Context context, Attachment attachment) {
        super(context, attachment);
    }
}
