package org.thoughtcrime.securesms.mms;

/* loaded from: classes4.dex */
public class MmsSendResult {
    private final byte[] messageId;
    private final int responseStatus;

    public MmsSendResult(byte[] bArr, int i) {
        this.messageId = bArr;
        this.responseStatus = i;
    }

    public int getResponseStatus() {
        return this.responseStatus;
    }

    public byte[] getMessageId() {
        return this.messageId;
    }
}
