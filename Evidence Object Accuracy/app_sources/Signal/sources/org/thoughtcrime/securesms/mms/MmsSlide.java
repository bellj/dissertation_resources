package org.thoughtcrime.securesms.mms;

import android.content.Context;
import org.thoughtcrime.securesms.attachments.Attachment;

/* loaded from: classes4.dex */
public class MmsSlide extends ImageSlide {
    @Override // org.thoughtcrime.securesms.mms.ImageSlide, org.thoughtcrime.securesms.mms.Slide
    public String getContentDescription() {
        return "MMS";
    }

    public MmsSlide(Context context, Attachment attachment) {
        super(context, attachment);
    }
}
