package org.thoughtcrime.securesms.mms;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.util.Pair;
import android.view.View;
import android.widget.Toast;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import j$.util.Optional;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.ExecutionException;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.components.AudioView;
import org.thoughtcrime.securesms.components.DocumentView;
import org.thoughtcrime.securesms.components.RemovableEditableMediaView;
import org.thoughtcrime.securesms.components.ThumbnailView;
import org.thoughtcrime.securesms.components.location.SignalMapView;
import org.thoughtcrime.securesms.components.location.SignalPlace;
import org.thoughtcrime.securesms.conversation.MessageSendType;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.giph.ui.GiphyActivity;
import org.thoughtcrime.securesms.maps.PlacePickerActivity;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionActivity;
import org.thoughtcrime.securesms.mms.SlideFactory;
import org.thoughtcrime.securesms.payments.create.CreatePaymentFragmentArgs;
import org.thoughtcrime.securesms.payments.preferences.PaymentsActivity;
import org.thoughtcrime.securesms.payments.preferences.model.PayeeParcelable;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.providers.DeprecatedPersistentBlobProvider;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.BitmapUtil;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.concurrent.AssertedSuccessListener;
import org.thoughtcrime.securesms.util.concurrent.ListenableFuture;
import org.thoughtcrime.securesms.util.concurrent.SettableFuture;
import org.thoughtcrime.securesms.util.views.Stub;

/* loaded from: classes4.dex */
public class AttachmentManager {
    private static final String TAG = Log.tag(AttachmentManager.class);
    private final AttachmentListener attachmentListener;
    private final Stub<View> attachmentViewStub;
    private AudioView audioView;
    private Uri captureUri;
    private final Context context;
    private DocumentView documentView;
    private List<Uri> garbage = new LinkedList();
    private SignalMapView mapView;
    private RemovableEditableMediaView removableMediaView;
    private Optional<Slide> slide = Optional.empty();
    private ThumbnailView thumbnail;

    /* loaded from: classes4.dex */
    public interface AttachmentListener {
        void onAttachmentChanged();
    }

    public AttachmentManager(Activity activity, AttachmentListener attachmentListener) {
        this.context = activity;
        this.attachmentListener = attachmentListener;
        this.attachmentViewStub = ViewUtil.findStubById(activity, (int) R.id.attachment_editor_stub);
    }

    private void inflateStub() {
        if (!this.attachmentViewStub.resolved()) {
            View view = this.attachmentViewStub.get();
            this.thumbnail = (ThumbnailView) view.findViewById(R.id.attachment_thumbnail);
            this.audioView = (AudioView) view.findViewById(R.id.attachment_audio);
            this.documentView = (DocumentView) view.findViewById(R.id.attachment_document);
            this.mapView = (SignalMapView) view.findViewById(R.id.attachment_location);
            RemovableEditableMediaView removableEditableMediaView = (RemovableEditableMediaView) view.findViewById(R.id.removable_media_view);
            this.removableMediaView = removableEditableMediaView;
            removableEditableMediaView.setRemoveClickListener(new RemoveButtonListener());
            this.thumbnail.setOnClickListener(new ThumbnailClickListener());
            this.documentView.getBackground().setColorFilter(ContextCompat.getColor(this.context, R.color.signal_background_secondary), PorterDuff.Mode.MULTIPLY);
        }
    }

    public void clear(final GlideRequests glideRequests, boolean z) {
        if (this.attachmentViewStub.resolved()) {
            if (z) {
                ViewUtil.fadeOut(this.attachmentViewStub.get(), 200).addListener(new ListenableFuture.Listener<Boolean>() { // from class: org.thoughtcrime.securesms.mms.AttachmentManager.1
                    @Override // org.thoughtcrime.securesms.util.concurrent.ListenableFuture.Listener
                    public void onFailure(ExecutionException executionException) {
                    }

                    public void onSuccess(Boolean bool) {
                        AttachmentManager.this.thumbnail.clear(glideRequests);
                        ((View) AttachmentManager.this.attachmentViewStub.get()).setVisibility(8);
                        AttachmentManager.this.attachmentListener.onAttachmentChanged();
                    }
                });
            } else {
                this.thumbnail.clear(glideRequests);
                this.attachmentViewStub.get().setVisibility(8);
                this.attachmentListener.onAttachmentChanged();
            }
            markGarbage(getSlideUri());
            this.slide = Optional.empty();
        }
    }

    public void cleanup() {
        cleanup(this.captureUri);
        cleanup(getSlideUri());
        this.captureUri = null;
        this.slide = Optional.empty();
        ListIterator<Uri> listIterator = this.garbage.listIterator();
        while (listIterator.hasNext()) {
            cleanup(listIterator.next());
            listIterator.remove();
        }
    }

    private void cleanup(Uri uri) {
        if (uri != null && DeprecatedPersistentBlobProvider.isAuthority(this.context, uri)) {
            String str = TAG;
            Log.d(str, "cleaning up " + uri);
            DeprecatedPersistentBlobProvider.getInstance(this.context).delete(this.context, uri);
        } else if (uri != null && BlobProvider.isAuthority(uri)) {
            BlobProvider.getInstance().delete(this.context, uri);
        }
    }

    private void markGarbage(Uri uri) {
        if (uri == null) {
            return;
        }
        if (DeprecatedPersistentBlobProvider.isAuthority(this.context, uri) || BlobProvider.isAuthority(uri)) {
            String str = TAG;
            Log.d(str, "Marking garbage that needs cleaning: " + uri);
            this.garbage.add(uri);
        }
    }

    public void setSlide(Slide slide) {
        if (getSlideUri() != null) {
            cleanup(getSlideUri());
        }
        Uri uri = this.captureUri;
        if (uri != null && !uri.equals(slide.getUri())) {
            cleanup(this.captureUri);
            this.captureUri = null;
        }
        this.slide = Optional.of(slide);
    }

    public ListenableFuture<Boolean> setLocation(final SignalPlace signalPlace, MediaConstraints mediaConstraints) {
        inflateStub();
        final SettableFuture settableFuture = new SettableFuture();
        ListenableFuture<Bitmap> display = this.mapView.display(signalPlace);
        this.attachmentViewStub.get().setVisibility(0);
        this.removableMediaView.display(this.mapView, false);
        display.addListener(new AssertedSuccessListener<Bitmap>() { // from class: org.thoughtcrime.securesms.mms.AttachmentManager.2
            public void onSuccess(Bitmap bitmap) {
                byte[] byteArray = BitmapUtil.toByteArray(bitmap);
                ThreadUtil.runOnMain(new AttachmentManager$2$$ExternalSyntheticLambda0(this, new LocationSlide(AttachmentManager.this.context, BlobProvider.getInstance().forData(byteArray).withMimeType(MediaUtil.IMAGE_JPEG).createForSingleSessionInMemory(), (long) byteArray.length, signalPlace), settableFuture));
            }

            public /* synthetic */ void lambda$onSuccess$0(LocationSlide locationSlide, SettableFuture settableFuture2) {
                AttachmentManager.this.setSlide(locationSlide);
                AttachmentManager.this.attachmentListener.onAttachmentChanged();
                settableFuture2.set(Boolean.TRUE);
            }
        });
        return settableFuture;
    }

    public ListenableFuture<Boolean> setMedia(final GlideRequests glideRequests, final Uri uri, final SlideFactory.MediaType mediaType, final MediaConstraints mediaConstraints, final int i, final int i2) {
        inflateStub();
        final SettableFuture settableFuture = new SettableFuture();
        new AsyncTask<Void, Void, Slide>() { // from class: org.thoughtcrime.securesms.mms.AttachmentManager.3
            @Override // android.os.AsyncTask
            protected void onPreExecute() {
                AttachmentManager.this.thumbnail.clear(glideRequests);
                AttachmentManager.this.thumbnail.showProgressSpinner();
                ((View) AttachmentManager.this.attachmentViewStub.get()).setVisibility(0);
            }

            public Slide doInBackground(Void... voidArr) {
                try {
                    if (PartAuthority.isLocalUri(uri)) {
                        return getManuallyCalculatedSlideInfo(uri, i, i2);
                    }
                    Slide contentResolverSlideInfo = getContentResolverSlideInfo(uri, i, i2);
                    return contentResolverSlideInfo == null ? getManuallyCalculatedSlideInfo(uri, i, i2) : contentResolverSlideInfo;
                } catch (IOException e) {
                    Log.w(AttachmentManager.TAG, e);
                    return null;
                }
            }

            public void onPostExecute(Slide slide) {
                boolean z = false;
                if (slide == null) {
                    ((View) AttachmentManager.this.attachmentViewStub.get()).setVisibility(8);
                    Toast.makeText(AttachmentManager.this.context, (int) R.string.ConversationActivity_sorry_there_was_an_error_setting_your_attachment, 0).show();
                    settableFuture.set(Boolean.FALSE);
                    return;
                }
                AttachmentManager attachmentManager = AttachmentManager.this;
                if (!attachmentManager.areConstraintsSatisfied(attachmentManager.context, slide, mediaConstraints)) {
                    ((View) AttachmentManager.this.attachmentViewStub.get()).setVisibility(8);
                    Toast.makeText(AttachmentManager.this.context, (int) R.string.ConversationActivity_attachment_exceeds_size_limits, 0).show();
                    settableFuture.set(Boolean.FALSE);
                    return;
                }
                AttachmentManager.this.setSlide(slide);
                ((View) AttachmentManager.this.attachmentViewStub.get()).setVisibility(0);
                if (slide.hasAudio()) {
                    AttachmentManager.this.audioView.setAudio((AudioSlide) slide, null, false, false);
                    AttachmentManager.this.removableMediaView.display(AttachmentManager.this.audioView, false);
                    settableFuture.set(Boolean.TRUE);
                } else if (slide.hasDocument()) {
                    AttachmentManager.this.documentView.setDocument((DocumentSlide) slide, false);
                    AttachmentManager.this.removableMediaView.display(AttachmentManager.this.documentView, false);
                    settableFuture.set(Boolean.TRUE);
                } else {
                    Attachment asAttachment = slide.asAttachment();
                    settableFuture.deferTo(AttachmentManager.this.thumbnail.setImageResource(glideRequests, slide, false, true, asAttachment.getWidth(), asAttachment.getHeight()));
                    RemovableEditableMediaView removableEditableMediaView = AttachmentManager.this.removableMediaView;
                    ThumbnailView thumbnailView = AttachmentManager.this.thumbnail;
                    if (mediaType == SlideFactory.MediaType.IMAGE) {
                        z = true;
                    }
                    removableEditableMediaView.display(thumbnailView, z);
                }
                AttachmentManager.this.attachmentListener.onAttachmentChanged();
            }

            private Slide getContentResolverSlideInfo(Uri uri2, int i3, int i4) {
                Throwable th;
                int i5;
                int i6;
                long currentTimeMillis = System.currentTimeMillis();
                Cursor cursor = null;
                try {
                    Cursor query = AttachmentManager.this.context.getContentResolver().query(uri2, null, null, null, null);
                    if (query != null) {
                        try {
                            if (query.moveToFirst()) {
                                String string = query.getString(query.getColumnIndexOrThrow("_display_name"));
                                long j = query.getLong(query.getColumnIndexOrThrow("_size"));
                                String type = AttachmentManager.this.context.getContentResolver().getType(uri2);
                                if (!(i3 == 0 || i4 == 0)) {
                                    i6 = i3;
                                    i5 = i4;
                                    String str = AttachmentManager.TAG;
                                    Log.d(str, "remote slide with size " + j + " took " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
                                    Slide createSlide = mediaType.createSlide(AttachmentManager.this.context, uri2, string, type, null, j, i6, i5, false, null);
                                    query.close();
                                    return createSlide;
                                }
                                Pair<Integer, Integer> dimensions = MediaUtil.getDimensions(AttachmentManager.this.context, type, uri2);
                                int intValue = ((Integer) dimensions.first).intValue();
                                i5 = ((Integer) dimensions.second).intValue();
                                i6 = intValue;
                                String str = AttachmentManager.TAG;
                                Log.d(str, "remote slide with size " + j + " took " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
                                Slide createSlide = mediaType.createSlide(AttachmentManager.this.context, uri2, string, type, null, j, i6, i5, false, null);
                                query.close();
                                return createSlide;
                            }
                        } catch (Throwable th2) {
                            th = th2;
                            cursor = query;
                            if (cursor != null) {
                                cursor.close();
                            }
                            throw th;
                        }
                    }
                    if (query != null) {
                        query.close();
                    }
                    return null;
                } catch (Throwable th3) {
                    th = th3;
                }
            }

            private Slide getManuallyCalculatedSlideInfo(Uri uri2, int i3, int i4) throws IOException {
                AttachmentDatabase.TransformProperties transformProperties;
                boolean z;
                String str;
                String str2;
                int i5;
                int i6;
                long currentTimeMillis = System.currentTimeMillis();
                Long l = null;
                if (PartAuthority.isLocalUri(uri2)) {
                    l = PartAuthority.getAttachmentSize(AttachmentManager.this.context, uri2);
                    str2 = PartAuthority.getAttachmentFileName(AttachmentManager.this.context, uri2);
                    str = PartAuthority.getAttachmentContentType(AttachmentManager.this.context, uri2);
                    boolean attachmentIsVideoGif = PartAuthority.getAttachmentIsVideoGif(AttachmentManager.this.context, uri2);
                    transformProperties = PartAuthority.getAttachmentTransformProperties(uri2);
                    z = attachmentIsVideoGif;
                } else {
                    str2 = null;
                    str = null;
                    transformProperties = null;
                    z = false;
                }
                if (l == null) {
                    l = Long.valueOf(MediaUtil.getMediaSize(AttachmentManager.this.context, uri2));
                }
                if (str == null) {
                    str = MediaUtil.getMimeType(AttachmentManager.this.context, uri2);
                }
                if (i3 == 0 || i4 == 0) {
                    Pair<Integer, Integer> dimensions = MediaUtil.getDimensions(AttachmentManager.this.context, str, uri2);
                    int intValue = ((Integer) dimensions.first).intValue();
                    i5 = ((Integer) dimensions.second).intValue();
                    i6 = intValue;
                } else {
                    i6 = i3;
                    i5 = i4;
                }
                Log.d(AttachmentManager.TAG, "local slide with size " + l + " took " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
                return mediaType.createSlide(AttachmentManager.this.context, uri2, str2, str, null, l.longValue(), i6, i5, z, transformProperties);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        return settableFuture;
    }

    public boolean isAttachmentPresent() {
        return this.attachmentViewStub.resolved() && this.attachmentViewStub.get().getVisibility() == 0;
    }

    public SlideDeck buildSlideDeck() {
        SlideDeck slideDeck = new SlideDeck();
        if (this.slide.isPresent()) {
            slideDeck.addSlide(this.slide.get());
        }
        return slideDeck;
    }

    public static void selectDocument(Fragment fragment, int i) {
        selectMediaType(fragment, MediaUtil.UNKNOWN, null, i);
    }

    public static void selectGallery(Fragment fragment, int i, Recipient recipient, CharSequence charSequence, MessageSendType messageSendType, boolean z) {
        Permissions.with(fragment).request("android.permission.READ_EXTERNAL_STORAGE").ifNecessary().withPermanentDenialDialog(fragment.getString(R.string.AttachmentManager_signal_requires_the_external_storage_permission_in_order_to_attach_photos_videos_or_audio)).onAllGranted(new Runnable(messageSendType, recipient, charSequence, z, i) { // from class: org.thoughtcrime.securesms.mms.AttachmentManager$$ExternalSyntheticLambda1
            public final /* synthetic */ MessageSendType f$1;
            public final /* synthetic */ Recipient f$2;
            public final /* synthetic */ CharSequence f$3;
            public final /* synthetic */ boolean f$4;
            public final /* synthetic */ int f$5;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AttachmentManager.lambda$selectGallery$0(Fragment.this, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5);
            }
        }).execute();
    }

    public static /* synthetic */ void lambda$selectGallery$0(Fragment fragment, MessageSendType messageSendType, Recipient recipient, CharSequence charSequence, boolean z, int i) {
        fragment.startActivityForResult(MediaSelectionActivity.gallery(fragment.requireContext(), messageSendType, Collections.emptyList(), recipient.getId(), charSequence, z), i);
    }

    public static void selectContactInfo(Fragment fragment, int i) {
        Permissions.with(fragment).request("android.permission.READ_CONTACTS").ifNecessary().withPermanentDenialDialog(fragment.getString(R.string.AttachmentManager_signal_requires_contacts_permission_in_order_to_attach_contact_information)).onAllGranted(new Runnable(i) { // from class: org.thoughtcrime.securesms.mms.AttachmentManager$$ExternalSyntheticLambda0
            public final /* synthetic */ int f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AttachmentManager.lambda$selectContactInfo$1(Fragment.this, this.f$1);
            }
        }).execute();
    }

    public static /* synthetic */ void lambda$selectContactInfo$1(Fragment fragment, int i) {
        fragment.startActivityForResult(new Intent("android.intent.action.PICK", ContactsContract.Contacts.CONTENT_URI), i);
    }

    public static void selectLocation(Fragment fragment, int i, int i2) {
        Permissions.with(fragment).request("android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION").ifNecessary().withPermanentDenialDialog(fragment.getString(R.string.AttachmentManager_signal_requires_location_information_in_order_to_attach_a_location)).onAllGranted(new Runnable(i, i2) { // from class: org.thoughtcrime.securesms.mms.AttachmentManager$$ExternalSyntheticLambda2
            public final /* synthetic */ int f$1;
            public final /* synthetic */ int f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                PlacePickerActivity.startActivityForResultAtCurrentLocation(Fragment.this, this.f$1, this.f$2);
            }
        }).execute();
    }

    public static void selectGif(Fragment fragment, int i, RecipientId recipientId, MessageSendType messageSendType, boolean z, CharSequence charSequence) {
        Intent intent = new Intent(fragment.requireContext(), GiphyActivity.class);
        intent.putExtra(GiphyActivity.EXTRA_IS_MMS, z);
        intent.putExtra(GiphyActivity.EXTRA_RECIPIENT_ID, recipientId);
        intent.putExtra(GiphyActivity.EXTRA_TRANSPORT, messageSendType);
        intent.putExtra(GiphyActivity.EXTRA_TEXT, charSequence);
        fragment.startActivityForResult(intent, i);
    }

    public static void selectPayment(Fragment fragment, RecipientId recipientId) {
        Intent intent = new Intent(fragment.requireContext(), PaymentsActivity.class);
        intent.putExtra(PaymentsActivity.EXTRA_PAYMENTS_STARTING_ACTION, R.id.action_directly_to_createPayment);
        intent.putExtra(PaymentsActivity.EXTRA_STARTING_ARGUMENTS, new CreatePaymentFragmentArgs.Builder(new PayeeParcelable(recipientId)).build().toBundle());
        fragment.startActivity(intent);
    }

    private Uri getSlideUri() {
        if (this.slide.isPresent()) {
            return this.slide.get().getUri();
        }
        return null;
    }

    public Uri getCaptureUri() {
        return this.captureUri;
    }

    private static void selectMediaType(Fragment fragment, String str, String[] strArr, int i) {
        Intent intent = new Intent();
        intent.setType(str);
        if (strArr != null) {
            intent.putExtra("android.intent.extra.MIME_TYPES", strArr);
        }
        intent.setAction("android.intent.action.OPEN_DOCUMENT");
        try {
            fragment.startActivityForResult(intent, i);
        } catch (ActivityNotFoundException unused) {
            Log.w(TAG, "couldn't complete ACTION_OPEN_DOCUMENT, no activity found. falling back.");
            intent.setAction("android.intent.action.GET_CONTENT");
            try {
                fragment.startActivityForResult(intent, i);
            } catch (ActivityNotFoundException unused2) {
                Log.w(TAG, "couldn't complete ACTION_GET_CONTENT intent, no activity found. falling back.");
                Toast.makeText(fragment.requireContext(), (int) R.string.AttachmentManager_cant_open_media_selection, 1).show();
            }
        }
    }

    public boolean areConstraintsSatisfied(Context context, Slide slide, MediaConstraints mediaConstraints) {
        return slide == null || mediaConstraints.isSatisfied(context, slide.asAttachment()) || mediaConstraints.canResize(slide.asAttachment());
    }

    public void previewImageDraft(Slide slide) {
        if (MediaPreviewActivity.isContentTypeSupported(slide.getContentType()) && slide.getUri() != null) {
            Intent intent = new Intent(this.context, MediaPreviewActivity.class);
            intent.addFlags(1);
            intent.putExtra(MediaPreviewActivity.SIZE_EXTRA, slide.asAttachment().getSize());
            intent.putExtra(MediaPreviewActivity.CAPTION_EXTRA, slide.getCaption().orElse(null));
            intent.setDataAndType(slide.getUri(), slide.getContentType());
            this.context.startActivity(intent);
        }
    }

    /* loaded from: classes4.dex */
    public class ThumbnailClickListener implements View.OnClickListener {
        private ThumbnailClickListener() {
            AttachmentManager.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            if (AttachmentManager.this.slide.isPresent()) {
                AttachmentManager attachmentManager = AttachmentManager.this;
                attachmentManager.previewImageDraft((Slide) attachmentManager.slide.get());
            }
        }
    }

    /* loaded from: classes4.dex */
    public class RemoveButtonListener implements View.OnClickListener {
        private RemoveButtonListener() {
            AttachmentManager.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            AttachmentManager.this.cleanup();
            AttachmentManager attachmentManager = AttachmentManager.this;
            attachmentManager.clear(GlideApp.with(attachmentManager.context.getApplicationContext()), true);
        }
    }
}
