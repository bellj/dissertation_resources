package org.thoughtcrime.securesms.mms;

import android.view.View;
import java.util.List;

/* loaded from: classes4.dex */
public interface SlidesClickedListener {
    void onClick(View view, List<Slide> list);
}
