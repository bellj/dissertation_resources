package org.thoughtcrime.securesms.mms;

/* loaded from: classes4.dex */
public class MediaTooLargeException extends Exception {
    public MediaTooLargeException() {
    }

    public MediaTooLargeException(String str) {
        super(str);
    }

    public MediaTooLargeException(Throwable th) {
        super(th);
    }

    public MediaTooLargeException(String str, Throwable th) {
        super(str, th);
    }
}
