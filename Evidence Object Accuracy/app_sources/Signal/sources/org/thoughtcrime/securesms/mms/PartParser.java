package org.thoughtcrime.securesms.mms;

import com.google.android.mms.ContentType;
import com.google.android.mms.pdu_alt.CharacterSets;
import com.google.android.mms.pdu_alt.PduBody;
import com.google.android.mms.pdu_alt.PduPart;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class PartParser {
    private static final List<String> DOCUMENT_TYPES = Arrays.asList("text/vcard", MediaUtil.VCARD);
    private static final String TAG = Log.tag(PartParser.class);

    public static String getMessageText(PduBody pduBody) {
        String str;
        String str2 = null;
        for (int i = 0; i < pduBody.getPartsNum(); i++) {
            if (isText(pduBody.getPart(i)) && !isDocument(pduBody.getPart(i))) {
                try {
                    String mimeName = CharacterSets.getMimeName(pduBody.getPart(i).getCharset());
                    if (mimeName.equals("*")) {
                        mimeName = "utf-8";
                    }
                    str = pduBody.getPart(i).getData() != null ? new String(pduBody.getPart(i).getData(), mimeName) : "";
                } catch (UnsupportedEncodingException e) {
                    Log.w(TAG, e);
                    str = "Unsupported Encoding!";
                }
                if (str2 != null) {
                    str = str2 + " " + str;
                }
                str2 = str;
            }
        }
        return str2;
    }

    public static PduBody getSupportedMediaParts(PduBody pduBody) {
        PduBody pduBody2 = new PduBody();
        for (int i = 0; i < pduBody.getPartsNum(); i++) {
            if (isDisplayableMedia(pduBody.getPart(i)) || isDocument(pduBody.getPart(i))) {
                pduBody2.addPart(pduBody.getPart(i));
            }
        }
        return pduBody2;
    }

    public static int getSupportedMediaPartCount(PduBody pduBody) {
        int i = 0;
        for (int i2 = 0; i2 < pduBody.getPartsNum(); i2++) {
            if (isDisplayableMedia(pduBody.getPart(i2)) || isDocument(pduBody.getPart(i2))) {
                i++;
            }
        }
        return i;
    }

    public static boolean isImage(PduPart pduPart) {
        return ContentType.isImageType(Util.toIsoString(pduPart.getContentType()));
    }

    public static boolean isAudio(PduPart pduPart) {
        return ContentType.isAudioType(Util.toIsoString(pduPart.getContentType()));
    }

    public static boolean isVideo(PduPart pduPart) {
        return ContentType.isVideoType(Util.toIsoString(pduPart.getContentType()));
    }

    public static boolean isText(PduPart pduPart) {
        return ContentType.isTextType(Util.toIsoString(pduPart.getContentType()));
    }

    public static boolean isDisplayableMedia(PduPart pduPart) {
        return isImage(pduPart) || isAudio(pduPart) || isVideo(pduPart);
    }

    public static boolean isDocument(PduPart pduPart) {
        return DOCUMENT_TYPES.contains(Util.toIsoString(pduPart.getContentType()).toLowerCase());
    }
}
