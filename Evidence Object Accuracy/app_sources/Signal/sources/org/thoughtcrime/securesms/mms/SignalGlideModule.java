package org.thoughtcrime.securesms.mms;

import android.content.Context;
import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.Registry;
import com.bumptech.glide.module.AppGlideModule;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: SignalGlideModule.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J\b\u0010\t\u001a\u00020\nH\u0016J \u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/mms/SignalGlideModule;", "Lcom/bumptech/glide/module/AppGlideModule;", "()V", "applyOptions", "", "context", "Landroid/content/Context;", "builder", "Lcom/bumptech/glide/GlideBuilder;", "isManifestParsingEnabled", "", "registerComponents", "glide", "Lcom/bumptech/glide/Glide;", "registry", "Lcom/bumptech/glide/Registry;", "Companion", "glide-config_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class SignalGlideModule extends AppGlideModule {
    public static final Companion Companion = new Companion(null);
    public static RegisterGlideComponents registerGlideComponents;

    public static final RegisterGlideComponents getRegisterGlideComponents() {
        return Companion.getRegisterGlideComponents();
    }

    public static final void setRegisterGlideComponents(RegisterGlideComponents registerGlideComponents2) {
        Companion.setRegisterGlideComponents(registerGlideComponents2);
    }

    @Override // com.bumptech.glide.module.AppGlideModule
    public boolean isManifestParsingEnabled() {
        return false;
    }

    @Override // com.bumptech.glide.module.LibraryGlideModule
    public void registerComponents(Context context, Glide glide, Registry registry) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(glide, "glide");
        Intrinsics.checkNotNullParameter(registry, "registry");
        Companion.getRegisterGlideComponents().registerComponents(context, glide, registry);
    }

    @Override // com.bumptech.glide.module.AppGlideModule
    public void applyOptions(Context context, GlideBuilder glideBuilder) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(glideBuilder, "builder");
        glideBuilder.setLogLevel(6);
    }

    /* compiled from: SignalGlideModule.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R$\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X.¢\u0006\u0014\n\u0000\u0012\u0004\b\u0005\u0010\u0002\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/mms/SignalGlideModule$Companion;", "", "()V", "registerGlideComponents", "Lorg/thoughtcrime/securesms/mms/RegisterGlideComponents;", "getRegisterGlideComponents$annotations", "getRegisterGlideComponents", "()Lorg/thoughtcrime/securesms/mms/RegisterGlideComponents;", "setRegisterGlideComponents", "(Lorg/thoughtcrime/securesms/mms/RegisterGlideComponents;)V", "glide-config_release"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        @JvmStatic
        public static /* synthetic */ void getRegisterGlideComponents$annotations() {
        }

        private Companion() {
        }

        public final RegisterGlideComponents getRegisterGlideComponents() {
            RegisterGlideComponents registerGlideComponents = SignalGlideModule.registerGlideComponents;
            if (registerGlideComponents != null) {
                return registerGlideComponents;
            }
            Intrinsics.throwUninitializedPropertyAccessException("registerGlideComponents");
            return null;
        }

        public final void setRegisterGlideComponents(RegisterGlideComponents registerGlideComponents) {
            Intrinsics.checkNotNullParameter(registerGlideComponents, "<set-?>");
            SignalGlideModule.registerGlideComponents = registerGlideComponents;
        }
    }
}
