package org.thoughtcrime.securesms.mms;

import j$.util.Optional;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.MmsSmsColumns;
import org.thoughtcrime.securesms.database.StickerDatabase;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.database.model.ParentStoryId;
import org.thoughtcrime.securesms.database.model.StoryType;
import org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: IncomingMediaMessage.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\b\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0002\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0007\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\f\u0012\b\b\u0002\u0010\u0012\u001a\u00020\u0011\u0012\b\b\u0002\u0010\u0016\u001a\u00020\u0015\u0012\n\b\u0002\u0010\u001b\u001a\u0004\u0018\u00010\u001a\u0012\b\b\u0002\u0010\u001f\u001a\u00020\u0011\u0012\u0006\u0010!\u001a\u00020 \u0012\u0006\u0010%\u001a\u00020 \u0012\u0006\u0010'\u001a\u00020 \u0012\b\b\u0002\u0010*\u001a\u00020)\u0012\b\b\u0002\u0010.\u001a\u00020 \u0012\b\b\u0002\u00100\u001a\u00020\u0011\u0012\n\b\u0002\u00102\u001a\u0004\u0018\u000101\u0012\b\b\u0002\u00106\u001a\u00020\u0011\u0012\b\b\u0002\u00107\u001a\u00020\u0011\u0012\n\b\u0002\u00108\u001a\u0004\u0018\u00010\f\u0012\n\b\u0002\u0010;\u001a\u0004\u0018\u00010:\u0012\u000e\b\u0002\u0010F\u001a\b\u0012\u0004\u0012\u00020E0D\u0012\u000e\b\u0002\u0010K\u001a\b\u0012\u0004\u0012\u00020J0D\u0012\u000e\b\u0002\u0010N\u001a\b\u0012\u0004\u0012\u00020M0D\u0012\u000e\b\u0002\u0010Q\u001a\b\u0012\u0004\u0012\u00020P0D\u0012\n\b\u0002\u0010@\u001a\u0004\u0018\u00010?¢\u0006\u0004\bT\u0010UB\u0001\b\u0016\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070V\u0012\b\u0010\r\u001a\u0004\u0018\u00010\f\u0012\u0006\u0010!\u001a\u00020 \u0012\u0006\u0010%\u001a\u00020 \u0012\u0006\u0010'\u001a\u00020 \u0012\u000e\u0010F\u001a\n\u0012\u0004\u0012\u00020E\u0018\u00010D\u0012\u0006\u0010*\u001a\u00020)\u0012\u0006\u0010.\u001a\u00020 \u0012\u0006\u0010W\u001a\u00020\u0011\u0012\u0006\u0010X\u001a\u00020\u0011\u0012\u0006\u0010Y\u001a\u00020\u0011\u0012\u0012\u0010K\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020J0D0V¢\u0006\u0004\bT\u0010ZB\u0002\b\u0016\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u0012\u0006\u0010!\u001a\u00020 \u0012\u0006\u0010%\u001a\u00020 \u0012\u0006\u0010'\u001a\u00020 \u0012\u0006\u0010\u0016\u001a\u00020\u0015\u0012\b\u0010\u001b\u001a\u0004\u0018\u00010\u001a\u0012\u0006\u0010\u001f\u001a\u00020\u0011\u0012\u0006\u0010*\u001a\u00020)\u0012\u0006\u0010.\u001a\u00020 \u0012\u0006\u0010W\u001a\u00020\u0011\u0012\u0006\u0010X\u001a\u00020\u0011\u0012\u0006\u0010Y\u001a\u00020\u0011\u0012\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0V\u0012\f\u0010\\\u001a\b\u0012\u0004\u0012\u00020[0V\u0012\u0012\u0010F\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020]0D0V\u0012\f\u00102\u001a\b\u0012\u0004\u0012\u0002010V\u0012\u0012\u0010K\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020J0D0V\u0012\u0012\u0010N\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020M0D0V\u0012\u0012\u0010Q\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020P0D0V\u0012\f\u0010^\u001a\b\u0012\u0004\u0012\u00020E0V\u0012\b\u00108\u001a\u0004\u0018\u00010\f\u0012\b\u0010@\u001a\u0004\u0018\u00010?¢\u0006\u0004\bT\u0010_R\u0019\u0010\u0003\u001a\u0004\u0018\u00010\u00028\u0006¢\u0006\f\n\u0004\b\u0003\u0010\u0004\u001a\u0004\b\u0005\u0010\u0006R\u0019\u0010\b\u001a\u0004\u0018\u00010\u00078\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\n\u0010\u000bR\u0019\u0010\r\u001a\u0004\u0018\u00010\f8\u0006¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000f\u0010\u0010R\u0017\u0010\u0012\u001a\u00020\u00118\u0006¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0012\u0010\u0014R\u0017\u0010\u0016\u001a\u00020\u00158\u0006¢\u0006\f\n\u0004\b\u0016\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u0019\u0010\u001b\u001a\u0004\u0018\u00010\u001a8\u0006¢\u0006\f\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u001d\u0010\u001eR\u0017\u0010\u001f\u001a\u00020\u00118\u0006¢\u0006\f\n\u0004\b\u001f\u0010\u0013\u001a\u0004\b\u001f\u0010\u0014R\u0017\u0010!\u001a\u00020 8\u0006¢\u0006\f\n\u0004\b!\u0010\"\u001a\u0004\b#\u0010$R\u0017\u0010%\u001a\u00020 8\u0006¢\u0006\f\n\u0004\b%\u0010\"\u001a\u0004\b&\u0010$R\u0017\u0010'\u001a\u00020 8\u0006¢\u0006\f\n\u0004\b'\u0010\"\u001a\u0004\b(\u0010$R\u0017\u0010*\u001a\u00020)8\u0006¢\u0006\f\n\u0004\b*\u0010+\u001a\u0004\b,\u0010-R\u0017\u0010.\u001a\u00020 8\u0006¢\u0006\f\n\u0004\b.\u0010\"\u001a\u0004\b/\u0010$R\u0017\u00100\u001a\u00020\u00118\u0006¢\u0006\f\n\u0004\b0\u0010\u0013\u001a\u0004\b0\u0010\u0014R\u0019\u00102\u001a\u0004\u0018\u0001018\u0006¢\u0006\f\n\u0004\b2\u00103\u001a\u0004\b4\u00105R\u0017\u00106\u001a\u00020\u00118\u0006¢\u0006\f\n\u0004\b6\u0010\u0013\u001a\u0004\b6\u0010\u0014R\u0017\u00107\u001a\u00020\u00118\u0006¢\u0006\f\n\u0004\b7\u0010\u0013\u001a\u0004\b7\u0010\u0014R\u0019\u00108\u001a\u0004\u0018\u00010\f8\u0006¢\u0006\f\n\u0004\b8\u0010\u000e\u001a\u0004\b9\u0010\u0010R\u0019\u0010;\u001a\u0004\u0018\u00010:8\u0006¢\u0006\f\n\u0004\b;\u0010<\u001a\u0004\b=\u0010>R\u0019\u0010@\u001a\u0004\u0018\u00010?8\u0006¢\u0006\f\n\u0004\b@\u0010A\u001a\u0004\bB\u0010CR\u001d\u0010F\u001a\b\u0012\u0004\u0012\u00020E0D8\u0006¢\u0006\f\n\u0004\bF\u0010G\u001a\u0004\bH\u0010IR\u001d\u0010K\u001a\b\u0012\u0004\u0012\u00020J0D8\u0006¢\u0006\f\n\u0004\bK\u0010G\u001a\u0004\bL\u0010IR\u001d\u0010N\u001a\b\u0012\u0004\u0012\u00020M0D8\u0006¢\u0006\f\n\u0004\bN\u0010G\u001a\u0004\bO\u0010IR\u001d\u0010Q\u001a\b\u0012\u0004\u0012\u00020P0D8\u0006¢\u0006\f\n\u0004\bQ\u0010G\u001a\u0004\bR\u0010IR\u0017\u0010S\u001a\u00020\u00118\u0006¢\u0006\f\n\u0004\bS\u0010\u0013\u001a\u0004\bS\u0010\u0014¨\u0006`"}, d2 = {"Lorg/thoughtcrime/securesms/mms/IncomingMediaMessage;", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "from", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getFrom", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Lorg/thoughtcrime/securesms/groups/GroupId;", "groupId", "Lorg/thoughtcrime/securesms/groups/GroupId;", "getGroupId", "()Lorg/thoughtcrime/securesms/groups/GroupId;", "", "body", "Ljava/lang/String;", "getBody", "()Ljava/lang/String;", "", "isPushMessage", "Z", "()Z", "Lorg/thoughtcrime/securesms/database/model/StoryType;", "storyType", "Lorg/thoughtcrime/securesms/database/model/StoryType;", "getStoryType", "()Lorg/thoughtcrime/securesms/database/model/StoryType;", "Lorg/thoughtcrime/securesms/database/model/ParentStoryId;", "parentStoryId", "Lorg/thoughtcrime/securesms/database/model/ParentStoryId;", "getParentStoryId", "()Lorg/thoughtcrime/securesms/database/model/ParentStoryId;", "isStoryReaction", "", "sentTimeMillis", "J", "getSentTimeMillis", "()J", "serverTimeMillis", "getServerTimeMillis", "receivedTimeMillis", "getReceivedTimeMillis", "", "subscriptionId", "I", "getSubscriptionId", "()I", "expiresIn", "getExpiresIn", "isExpirationUpdate", "Lorg/thoughtcrime/securesms/mms/QuoteModel;", DraftDatabase.Draft.QUOTE, "Lorg/thoughtcrime/securesms/mms/QuoteModel;", "getQuote", "()Lorg/thoughtcrime/securesms/mms/QuoteModel;", "isUnidentified", "isViewOnce", "serverGuid", "getServerGuid", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/BodyRangeList;", "messageRanges", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/BodyRangeList;", "getMessageRanges", "()Lorg/thoughtcrime/securesms/database/model/databaseprotos/BodyRangeList;", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;", "giftBadge", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;", "getGiftBadge", "()Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;", "", "Lorg/thoughtcrime/securesms/attachments/Attachment;", "attachments", "Ljava/util/List;", "getAttachments", "()Ljava/util/List;", "Lorg/thoughtcrime/securesms/contactshare/Contact;", "sharedContacts", "getSharedContacts", "Lorg/thoughtcrime/securesms/linkpreview/LinkPreview;", "linkPreviews", "getLinkPreviews", "Lorg/thoughtcrime/securesms/database/model/Mention;", "mentions", "getMentions", "isGroupMessage", "<init>", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/groups/GroupId;Ljava/lang/String;ZLorg/thoughtcrime/securesms/database/model/StoryType;Lorg/thoughtcrime/securesms/database/model/ParentStoryId;ZJJJIJZLorg/thoughtcrime/securesms/mms/QuoteModel;ZZLjava/lang/String;Lorg/thoughtcrime/securesms/database/model/databaseprotos/BodyRangeList;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;)V", "j$/util/Optional", "expirationUpdate", "viewOnce", MmsSmsColumns.UNIDENTIFIED, "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lj$/util/Optional;Ljava/lang/String;JJJLjava/util/List;IJZZZLj$/util/Optional;)V", "Lorg/whispersystems/signalservice/api/messages/SignalServiceGroupContext;", "group", "Lorg/whispersystems/signalservice/api/messages/SignalServiceAttachment;", StickerDatabase.TABLE_NAME, "(Lorg/thoughtcrime/securesms/recipients/RecipientId;JJJLorg/thoughtcrime/securesms/database/model/StoryType;Lorg/thoughtcrime/securesms/database/model/ParentStoryId;ZIJZZZLj$/util/Optional;Lj$/util/Optional;Lj$/util/Optional;Lj$/util/Optional;Lj$/util/Optional;Lj$/util/Optional;Lj$/util/Optional;Lj$/util/Optional;Ljava/lang/String;Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class IncomingMediaMessage {
    private final List<Attachment> attachments;
    private final String body;
    private final long expiresIn;
    private final RecipientId from;
    private final GiftBadge giftBadge;
    private final GroupId groupId;
    private final boolean isExpirationUpdate;
    private final boolean isGroupMessage;
    private final boolean isPushMessage;
    private final boolean isStoryReaction;
    private final boolean isUnidentified;
    private final boolean isViewOnce;
    private final List<LinkPreview> linkPreviews;
    private final List<Mention> mentions;
    private final BodyRangeList messageRanges;
    private final ParentStoryId parentStoryId;
    private final QuoteModel quote;
    private final long receivedTimeMillis;
    private final long sentTimeMillis;
    private final String serverGuid;
    private final long serverTimeMillis;
    private final List<Contact> sharedContacts;
    private final StoryType storyType;
    private final int subscriptionId;

    public IncomingMediaMessage(RecipientId recipientId, GroupId groupId, String str, boolean z, StoryType storyType, ParentStoryId parentStoryId, boolean z2, long j, long j2, long j3, int i, long j4, boolean z3, QuoteModel quoteModel, boolean z4, boolean z5, String str2, BodyRangeList bodyRangeList, List<? extends Attachment> list, List<? extends Contact> list2, List<? extends LinkPreview> list3, List<? extends Mention> list4, GiftBadge giftBadge) {
        Intrinsics.checkNotNullParameter(storyType, "storyType");
        Intrinsics.checkNotNullParameter(list, "attachments");
        Intrinsics.checkNotNullParameter(list2, "sharedContacts");
        Intrinsics.checkNotNullParameter(list3, "linkPreviews");
        Intrinsics.checkNotNullParameter(list4, "mentions");
        this.from = recipientId;
        this.groupId = groupId;
        this.body = str;
        this.isPushMessage = z;
        this.storyType = storyType;
        this.parentStoryId = parentStoryId;
        this.isStoryReaction = z2;
        this.sentTimeMillis = j;
        this.serverTimeMillis = j2;
        this.receivedTimeMillis = j3;
        this.subscriptionId = i;
        this.expiresIn = j4;
        this.isExpirationUpdate = z3;
        this.quote = quoteModel;
        this.isUnidentified = z4;
        this.isViewOnce = z5;
        this.serverGuid = str2;
        this.messageRanges = bodyRangeList;
        this.giftBadge = giftBadge;
        this.attachments = new ArrayList(list);
        this.sharedContacts = new ArrayList(list2);
        this.linkPreviews = new ArrayList(list3);
        this.mentions = new ArrayList(list4);
        this.isGroupMessage = groupId != null;
    }

    public final RecipientId getFrom() {
        return this.from;
    }

    public final GroupId getGroupId() {
        return this.groupId;
    }

    public final String getBody() {
        return this.body;
    }

    public final boolean isPushMessage() {
        return this.isPushMessage;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ IncomingMediaMessage(org.thoughtcrime.securesms.recipients.RecipientId r32, org.thoughtcrime.securesms.groups.GroupId r33, java.lang.String r34, boolean r35, org.thoughtcrime.securesms.database.model.StoryType r36, org.thoughtcrime.securesms.database.model.ParentStoryId r37, boolean r38, long r39, long r41, long r43, int r45, long r46, boolean r48, org.thoughtcrime.securesms.mms.QuoteModel r49, boolean r50, boolean r51, java.lang.String r52, org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList r53, java.util.List r54, java.util.List r55, java.util.List r56, java.util.List r57, org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge r58, int r59, kotlin.jvm.internal.DefaultConstructorMarker r60) {
        /*
        // Method dump skipped, instructions count: 213
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.mms.IncomingMediaMessage.<init>(org.thoughtcrime.securesms.recipients.RecipientId, org.thoughtcrime.securesms.groups.GroupId, java.lang.String, boolean, org.thoughtcrime.securesms.database.model.StoryType, org.thoughtcrime.securesms.database.model.ParentStoryId, boolean, long, long, long, int, long, boolean, org.thoughtcrime.securesms.mms.QuoteModel, boolean, boolean, java.lang.String, org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList, java.util.List, java.util.List, java.util.List, java.util.List, org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final StoryType getStoryType() {
        return this.storyType;
    }

    public final ParentStoryId getParentStoryId() {
        return this.parentStoryId;
    }

    public final boolean isStoryReaction() {
        return this.isStoryReaction;
    }

    public final long getSentTimeMillis() {
        return this.sentTimeMillis;
    }

    public final long getServerTimeMillis() {
        return this.serverTimeMillis;
    }

    public final long getReceivedTimeMillis() {
        return this.receivedTimeMillis;
    }

    public final int getSubscriptionId() {
        return this.subscriptionId;
    }

    public final long getExpiresIn() {
        return this.expiresIn;
    }

    public final boolean isExpirationUpdate() {
        return this.isExpirationUpdate;
    }

    public final QuoteModel getQuote() {
        return this.quote;
    }

    public final boolean isUnidentified() {
        return this.isUnidentified;
    }

    public final boolean isViewOnce() {
        return this.isViewOnce;
    }

    public final String getServerGuid() {
        return this.serverGuid;
    }

    public final BodyRangeList getMessageRanges() {
        return this.messageRanges;
    }

    public final GiftBadge getGiftBadge() {
        return this.giftBadge;
    }

    public final List<Attachment> getAttachments() {
        return this.attachments;
    }

    public final List<Contact> getSharedContacts() {
        return this.sharedContacts;
    }

    public final List<LinkPreview> getLinkPreviews() {
        return this.linkPreviews;
    }

    public final List<Mention> getMentions() {
        return this.mentions;
    }

    public final boolean isGroupMessage() {
        return this.isGroupMessage;
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public IncomingMediaMessage(RecipientId recipientId, Optional<GroupId> optional, String str, long j, long j2, long j3, List<? extends Attachment> list, int i, long j4, boolean z, boolean z2, boolean z3, Optional<List<Contact>> optional2) {
        this(recipientId, optional.orElse(null), str, false, null, null, false, j, j2, j3, i, j4, z, null, z3, z2, null, null, list != null ? new ArrayList(list) : CollectionsKt__CollectionsKt.emptyList(), new ArrayList(optional2.orElse(CollectionsKt__CollectionsKt.emptyList())), null, null, null, 7471216, null);
        Intrinsics.checkNotNullParameter(optional, "groupId");
        Intrinsics.checkNotNullParameter(optional2, "sharedContacts");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public IncomingMediaMessage(org.thoughtcrime.securesms.recipients.RecipientId r37, long r38, long r40, long r42, org.thoughtcrime.securesms.database.model.StoryType r44, org.thoughtcrime.securesms.database.model.ParentStoryId r45, boolean r46, int r47, long r48, boolean r50, boolean r51, boolean r52, j$.util.Optional<java.lang.String> r53, j$.util.Optional<org.whispersystems.signalservice.api.messages.SignalServiceGroupContext> r54, j$.util.Optional<java.util.List<org.whispersystems.signalservice.api.messages.SignalServiceAttachment>> r55, j$.util.Optional<org.thoughtcrime.securesms.mms.QuoteModel> r56, j$.util.Optional<java.util.List<org.thoughtcrime.securesms.contactshare.Contact>> r57, j$.util.Optional<java.util.List<org.thoughtcrime.securesms.linkpreview.LinkPreview>> r58, j$.util.Optional<java.util.List<org.thoughtcrime.securesms.database.model.Mention>> r59, j$.util.Optional<org.thoughtcrime.securesms.attachments.Attachment> r60, java.lang.String r61, org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge r62) {
        /*
            r36 = this;
            r0 = r53
            r1 = r56
            r2 = r57
            r3 = r58
            r4 = r59
            java.lang.String r5 = "storyType"
            r11 = r44
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r11, r5)
            java.lang.String r5 = "body"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r0, r5)
            java.lang.String r5 = "group"
            r6 = r54
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r6, r5)
            java.lang.String r5 = "attachments"
            r7 = r55
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r7, r5)
            java.lang.String r5 = "quote"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r1, r5)
            java.lang.String r5 = "sharedContacts"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r2, r5)
            java.lang.String r5 = "linkPreviews"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r3, r5)
            java.lang.String r5 = "mentions"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r4, r5)
            java.lang.String r5 = "sticker"
            r8 = r60
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r8, r5)
            boolean r5 = r54.isPresent()
            r9 = 0
            if (r5 == 0) goto L_0x0051
            java.lang.Object r5 = r54.get()
            org.whispersystems.signalservice.api.messages.SignalServiceGroupContext r5 = (org.whispersystems.signalservice.api.messages.SignalServiceGroupContext) r5
            org.thoughtcrime.securesms.groups.GroupId r5 = org.thoughtcrime.securesms.util.GroupUtil.idFromGroupContextOrThrow(r5)
            goto L_0x0052
        L_0x0051:
            r5 = r9
        L_0x0052:
            java.lang.Object r0 = r0.orElse(r9)
            java.lang.String r0 = (java.lang.String) r0
            r10 = 1
            java.lang.Object r1 = r1.orElse(r9)
            r24 = r1
            org.thoughtcrime.securesms.mms.QuoteModel r24 = (org.thoughtcrime.securesms.mms.QuoteModel) r24
            r28 = 0
            java.util.List r1 = org.thoughtcrime.securesms.attachments.PointerAttachment.forPointers(r55)
            boolean r6 = r60.isPresent()
            if (r6 == 0) goto L_0x0074
            java.lang.Object r6 = r60.get()
            r1.add(r6)
        L_0x0074:
            kotlin.Unit r6 = kotlin.Unit.INSTANCE
            java.lang.String r6 = "forPointers(attachments)…ent) add(sticker.get()) }"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r1, r6)
            java.util.List r6 = kotlin.collections.CollectionsKt.emptyList()
            java.lang.Object r2 = r2.orElse(r6)
            java.lang.String r6 = "sharedContacts.orElse(emptyList())"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r2, r6)
            r30 = r2
            java.util.List r30 = (java.util.List) r30
            java.util.List r2 = kotlin.collections.CollectionsKt.emptyList()
            java.lang.Object r2 = r3.orElse(r2)
            java.lang.String r3 = "linkPreviews.orElse(emptyList())"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r2, r3)
            r31 = r2
            java.util.List r31 = (java.util.List) r31
            java.util.List r2 = kotlin.collections.CollectionsKt.emptyList()
            java.lang.Object r2 = r4.orElse(r2)
            java.lang.String r3 = "mentions.orElse(emptyList())"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r2, r3)
            r32 = r2
            java.util.List r32 = (java.util.List) r32
            r34 = 131072(0x20000, float:1.83671E-40)
            r35 = 0
            r6 = r36
            r7 = r37
            r8 = r5
            r9 = r0
            r11 = r44
            r12 = r45
            r13 = r46
            r14 = r38
            r16 = r40
            r18 = r42
            r20 = r47
            r21 = r48
            r23 = r50
            r25 = r52
            r26 = r51
            r27 = r61
            r29 = r1
            r33 = r62
            r6.<init>(r7, r8, r9, r10, r11, r12, r13, r14, r16, r18, r20, r21, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33, r34, r35)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.mms.IncomingMediaMessage.<init>(org.thoughtcrime.securesms.recipients.RecipientId, long, long, long, org.thoughtcrime.securesms.database.model.StoryType, org.thoughtcrime.securesms.database.model.ParentStoryId, boolean, int, long, boolean, boolean, boolean, j$.util.Optional, j$.util.Optional, j$.util.Optional, j$.util.Optional, j$.util.Optional, j$.util.Optional, j$.util.Optional, j$.util.Optional, java.lang.String, org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge):void");
    }
}
