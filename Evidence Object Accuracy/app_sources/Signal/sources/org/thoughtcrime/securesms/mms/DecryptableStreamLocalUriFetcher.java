package org.thoughtcrime.securesms.mms;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import com.bumptech.glide.load.data.StreamLocalUriFetcher;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.MediaUtil;

/* loaded from: classes4.dex */
public class DecryptableStreamLocalUriFetcher extends StreamLocalUriFetcher {
    private static final String TAG = Log.tag(DecryptableStreamLocalUriFetcher.class);
    private Context context;

    public DecryptableStreamLocalUriFetcher(Context context, Uri uri) {
        super(context.getContentResolver(), uri);
        this.context = context;
    }

    @Override // com.bumptech.glide.load.data.StreamLocalUriFetcher, com.bumptech.glide.load.data.LocalUriFetcher
    public InputStream loadResource(Uri uri, ContentResolver contentResolver) throws FileNotFoundException {
        Bitmap videoThumbnail;
        if (!MediaUtil.hasVideoThumbnail(this.context, uri) || (videoThumbnail = MediaUtil.getVideoThumbnail(this.context, uri, 1000)) == null) {
            try {
                return PartAuthority.getAttachmentThumbnailStream(this.context, uri);
            } catch (IOException e) {
                Log.w(TAG, e);
                throw new FileNotFoundException("PartAuthority couldn't load Uri resource.");
            }
        } else {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            videoThumbnail.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
            videoThumbnail.recycle();
            return byteArrayInputStream;
        }
    }
}
