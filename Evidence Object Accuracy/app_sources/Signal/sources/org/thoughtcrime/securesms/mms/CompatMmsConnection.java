package org.thoughtcrime.securesms.mms;

import android.content.Context;
import android.os.Build;
import com.google.android.mms.pdu_alt.RetrieveConf;
import com.google.android.mms.pdu_alt.SendConf;
import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.transport.UndeliverableMessageException;

/* loaded from: classes4.dex */
public class CompatMmsConnection implements OutgoingMmsConnection, IncomingMmsConnection {
    private static final String TAG = Log.tag(CompatMmsConnection.class);
    private Context context;

    public CompatMmsConnection(Context context) {
        this.context = context;
    }

    @Override // org.thoughtcrime.securesms.mms.OutgoingMmsConnection
    public SendConf send(byte[] bArr, int i) throws UndeliverableMessageException {
        if (Build.VERSION.SDK_INT >= 22) {
            try {
                Log.i(TAG, "Sending via Lollipop API");
                return new OutgoingLollipopMmsConnection(this.context).send(bArr, i);
            } catch (UndeliverableMessageException e) {
                String str = TAG;
                Log.w(str, e);
                Log.i(str, "Falling back to legacy connection...");
            }
        }
        if (i == -1) {
            String str2 = TAG;
            Log.i(str2, "Sending via legacy connection");
            try {
                SendConf send = new OutgoingLegacyMmsConnection(this.context).send(bArr, i);
                if (send != null && send.getResponseStatus() == 128) {
                    return send;
                }
                StringBuilder sb = new StringBuilder();
                sb.append("Got bad legacy response: ");
                sb.append(send != null ? Integer.valueOf(send.getResponseStatus()) : null);
                Log.w(str2, sb.toString());
            } catch (ApnUnavailableException | UndeliverableMessageException e2) {
                Log.w(TAG, e2);
            }
        }
        int i2 = Build.VERSION.SDK_INT;
        if (i2 < 21 || i2 >= 22) {
            throw new UndeliverableMessageException("Both lollipop and legacy connections failed...");
        }
        Log.i(TAG, "Falling back to sending via Lollipop API");
        return new OutgoingLollipopMmsConnection(this.context).send(bArr, i);
    }

    @Override // org.thoughtcrime.securesms.mms.IncomingMmsConnection
    public RetrieveConf retrieve(String str, byte[] bArr, int i) throws MmsException, MmsRadioException, ApnUnavailableException, IOException {
        if (Build.VERSION.SDK_INT >= 22) {
            Log.i(TAG, "Receiving via Lollipop API");
            try {
                return new IncomingLollipopMmsConnection(this.context).retrieve(str, bArr, i);
            } catch (MmsException e) {
                String str2 = TAG;
                Log.w(str2, e);
                Log.i(str2, "Falling back to receiving via legacy connection");
            }
        }
        if (Build.VERSION.SDK_INT < 22 || i == -1) {
            Log.i(TAG, "Receiving via legacy API");
            try {
                return new IncomingLegacyMmsConnection(this.context).retrieve(str, bArr, i);
            } catch (IOException | ApnUnavailableException | MmsRadioException e2) {
                Log.w(TAG, e2);
            }
        }
        int i2 = Build.VERSION.SDK_INT;
        if (i2 < 21 || i2 >= 22) {
            throw new IOException("Both lollipop and fallback APIs failed...");
        }
        Log.i(TAG, "Falling back to receiving via Lollipop API");
        return new IncomingLollipopMmsConnection(this.context).retrieve(str, bArr, i);
    }
}
