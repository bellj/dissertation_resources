package org.thoughtcrime.securesms.mms;

import j$.util.function.Predicate;
import org.thoughtcrime.securesms.mms.PushMediaConstraints;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class PushMediaConstraints$MediaConfig$$ExternalSyntheticLambda0 implements Predicate {
    public final /* synthetic */ int f$0;
    public final /* synthetic */ boolean f$1;

    public /* synthetic */ PushMediaConstraints$MediaConfig$$ExternalSyntheticLambda0(int i, boolean z) {
        this.f$0 = i;
        this.f$1 = z;
    }

    @Override // j$.util.function.Predicate
    public /* synthetic */ Predicate and(Predicate predicate) {
        return Predicate.CC.$default$and(this, predicate);
    }

    @Override // j$.util.function.Predicate
    public /* synthetic */ Predicate negate() {
        return Predicate.CC.$default$negate(this);
    }

    @Override // j$.util.function.Predicate
    public /* synthetic */ Predicate or(Predicate predicate) {
        return Predicate.CC.$default$or(this, predicate);
    }

    @Override // j$.util.function.Predicate
    public final boolean test(Object obj) {
        return PushMediaConstraints.MediaConfig.lambda$forLevel$0(this.f$0, this.f$1, (PushMediaConstraints.MediaConfig) obj);
    }
}
