package org.thoughtcrime.securesms.mms;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.blurhash.BlurHash;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.util.MediaUtil;

/* loaded from: classes4.dex */
public class ImageSlide extends Slide {
    private static final String TAG = Log.tag(ImageSlide.class);
    private final boolean borderless;

    @Override // org.thoughtcrime.securesms.mms.Slide
    public int getPlaceholderRes(Resources.Theme theme) {
        return 0;
    }

    @Override // org.thoughtcrime.securesms.mms.Slide
    public boolean hasImage() {
        return true;
    }

    public ImageSlide(Context context, Attachment attachment) {
        super(context, attachment);
        this.borderless = attachment.isBorderless();
    }

    public ImageSlide(Context context, Uri uri, long j, int i, int i2, BlurHash blurHash) {
        this(context, uri, MediaUtil.IMAGE_JPEG, j, i, i2, false, null, blurHash);
    }

    public ImageSlide(Context context, Uri uri, String str, long j, int i, int i2, boolean z, String str2, BlurHash blurHash) {
        this(context, uri, str, j, i, i2, z, str2, blurHash, null);
    }

    public ImageSlide(Context context, Uri uri, String str, long j, int i, int i2, boolean z, String str2, BlurHash blurHash, AttachmentDatabase.TransformProperties transformProperties) {
        super(context, Slide.constructAttachmentFromUri(context, uri, str, j, i, i2, true, null, str2, null, blurHash, null, false, z, false, false, transformProperties));
        this.borderless = z;
    }

    @Override // org.thoughtcrime.securesms.mms.Slide
    public boolean hasPlaceholder() {
        return getPlaceholderBlur() != null;
    }

    @Override // org.thoughtcrime.securesms.mms.Slide
    public boolean isBorderless() {
        return this.borderless;
    }

    @Override // org.thoughtcrime.securesms.mms.Slide
    public String getContentDescription() {
        return this.context.getString(R.string.Slide_image);
    }
}
