package org.thoughtcrime.securesms.mms;

import android.content.Context;
import android.net.Uri;
import j$.util.Optional;
import org.thoughtcrime.securesms.components.location.SignalPlace;

/* loaded from: classes4.dex */
public class LocationSlide extends ImageSlide {
    private final SignalPlace place;

    @Override // org.thoughtcrime.securesms.mms.Slide
    public boolean hasLocation() {
        return true;
    }

    public LocationSlide(Context context, Uri uri, long j, SignalPlace signalPlace) {
        super(context, uri, j, 0, 0, null);
        this.place = signalPlace;
    }

    @Override // org.thoughtcrime.securesms.mms.Slide
    public Optional<String> getBody() {
        return Optional.of(this.place.getDescription());
    }

    public SignalPlace getPlace() {
        return this.place;
    }
}
