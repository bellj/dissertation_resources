package org.thoughtcrime.securesms.mms;

import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;

/* loaded from: classes4.dex */
public class QuoteModel {
    private final List<Attachment> attachments;
    private final RecipientId author;
    private final long id;
    private final List<Mention> mentions;
    private final boolean missing;
    private final String text;
    private final Type type;

    public QuoteModel(long j, RecipientId recipientId, String str, boolean z, List<Attachment> list, List<Mention> list2, Type type) {
        this.id = j;
        this.author = recipientId;
        this.text = str;
        this.missing = z;
        this.attachments = list;
        this.mentions = list2 == null ? Collections.emptyList() : list2;
        this.type = type;
    }

    public long getId() {
        return this.id;
    }

    public RecipientId getAuthor() {
        return this.author;
    }

    public String getText() {
        return this.text;
    }

    public boolean isOriginalMissing() {
        return this.missing;
    }

    public List<Attachment> getAttachments() {
        return this.attachments;
    }

    public List<Mention> getMentions() {
        return this.mentions;
    }

    public Type getType() {
        return this.type;
    }

    /* loaded from: classes4.dex */
    public enum Type {
        NORMAL(0, SignalServiceDataMessage.Quote.Type.NORMAL),
        GIFT_BADGE(1, SignalServiceDataMessage.Quote.Type.GIFT_BADGE);
        
        private final int code;
        private final SignalServiceDataMessage.Quote.Type dataMessageType;

        Type(int i, SignalServiceDataMessage.Quote.Type type) {
            this.code = i;
            this.dataMessageType = type;
        }

        public int getCode() {
            return this.code;
        }

        public SignalServiceDataMessage.Quote.Type getDataMessageType() {
            return this.dataMessageType;
        }

        public static Type fromCode(int i) {
            Type[] values = values();
            for (Type type : values) {
                if (type.code == i) {
                    return type;
                }
            }
            throw new IllegalArgumentException("Invalid code: " + i);
        }

        public static Type fromDataMessageType(SignalServiceDataMessage.Quote.Type type) {
            Type[] values = values();
            for (Type type2 : values) {
                if (type2.dataMessageType == type) {
                    return type2;
                }
            }
            return NORMAL;
        }
    }
}
