package org.thoughtcrime.securesms.mms;

import android.content.ContentUris;
import android.content.Context;
import android.content.UriMatcher;
import android.net.Uri;
import java.io.IOException;
import java.io.InputStream;
import org.thoughtcrime.securesms.attachments.AttachmentId;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.avatar.AvatarPickerStorage;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.emoji.EmojiFiles;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.providers.DeprecatedPersistentBlobProvider;
import org.thoughtcrime.securesms.providers.PartProvider;
import org.thoughtcrime.securesms.wallpaper.WallpaperStorage;

/* loaded from: classes4.dex */
public class PartAuthority {
    private static final String AUTHORITY;
    private static final Uri AVATAR_PICKER_CONTENT_URI = Uri.parse(AVATAR_PICKER_URI_STRING);
    private static final int AVATAR_PICKER_ROW;
    private static final String AVATAR_PICKER_URI_STRING;
    private static final int BLOB_ROW;
    private static final Uri EMOJI_CONTENT_URI = Uri.parse(EMOJI_URI_STRING);
    private static final int EMOJI_ROW;
    private static final String EMOJI_URI_STRING;
    private static final Uri PART_CONTENT_URI = Uri.parse(PART_URI_STRING);
    private static final int PART_ROW;
    private static final String PART_URI_STRING;
    private static final int PERSISTENT_ROW;
    private static final Uri STICKER_CONTENT_URI = Uri.parse(STICKER_URI_STRING);
    private static final int STICKER_ROW;
    private static final String STICKER_URI_STRING;
    private static final Uri WALLPAPER_CONTENT_URI = Uri.parse(WALLPAPER_URI_STRING);
    private static final int WALLPAPER_ROW;
    private static final String WALLPAPER_URI_STRING;
    private static final UriMatcher uriMatcher;

    static {
        PART_CONTENT_URI = Uri.parse(PART_URI_STRING);
        STICKER_CONTENT_URI = Uri.parse(STICKER_URI_STRING);
        WALLPAPER_CONTENT_URI = Uri.parse(WALLPAPER_URI_STRING);
        EMOJI_CONTENT_URI = Uri.parse(EMOJI_URI_STRING);
        AVATAR_PICKER_CONTENT_URI = Uri.parse(AVATAR_PICKER_URI_STRING);
        UriMatcher uriMatcher2 = new UriMatcher(-1);
        uriMatcher = uriMatcher2;
        uriMatcher2.addURI("org.thoughtcrime.securesms", "part/*/#", 1);
        uriMatcher2.addURI("org.thoughtcrime.securesms", "sticker/#", 4);
        uriMatcher2.addURI("org.thoughtcrime.securesms", "wallpaper/*", 5);
        uriMatcher2.addURI("org.thoughtcrime.securesms", "emoji/*", 6);
        uriMatcher2.addURI("org.thoughtcrime.securesms", "avatar_picker/*", 7);
        uriMatcher2.addURI("org.thoughtcrime.securesms", DeprecatedPersistentBlobProvider.EXPECTED_PATH_OLD, 2);
        uriMatcher2.addURI("org.thoughtcrime.securesms", DeprecatedPersistentBlobProvider.EXPECTED_PATH_NEW, 2);
        uriMatcher2.addURI(BlobProvider.AUTHORITY, BlobProvider.PATH, 3);
    }

    public static InputStream getAttachmentThumbnailStream(Context context, Uri uri) throws IOException {
        return getAttachmentStream(context, uri);
    }

    public static InputStream getAttachmentStream(Context context, Uri uri) throws IOException {
        try {
            switch (uriMatcher.match(uri)) {
                case 1:
                    return SignalDatabase.attachments().getAttachmentStream(new PartUriParser(uri).getPartId(), 0);
                case 2:
                    return DeprecatedPersistentBlobProvider.getInstance(context).getStream(context, ContentUris.parseId(uri));
                case 3:
                    return BlobProvider.getInstance().getStream(context, uri);
                case 4:
                    return SignalDatabase.stickers().getStickerStream(ContentUris.parseId(uri));
                case 5:
                    return WallpaperStorage.read(context, getWallpaperFilename(uri));
                case 6:
                    return EmojiFiles.openForReading(context, getEmojiFilename(uri));
                case 7:
                    return AvatarPickerStorage.read(context, getAvatarPickerFilename(uri));
                default:
                    return context.getContentResolver().openInputStream(uri);
            }
        } catch (SecurityException e) {
            throw new IOException(e);
        }
    }

    public static String getAttachmentFileName(Context context, Uri uri) {
        int match = uriMatcher.match(uri);
        if (match == 1) {
            DatabaseAttachment attachment = SignalDatabase.attachments().getAttachment(new PartUriParser(uri).getPartId());
            if (attachment != null) {
                return attachment.getFileName();
            }
            return null;
        } else if (match == 2) {
            return DeprecatedPersistentBlobProvider.getFileName(context, uri);
        } else {
            if (match != 3) {
                return null;
            }
            return BlobProvider.getFileName(uri);
        }
    }

    public static Long getAttachmentSize(Context context, Uri uri) {
        int match = uriMatcher.match(uri);
        if (match == 1) {
            DatabaseAttachment attachment = SignalDatabase.attachments().getAttachment(new PartUriParser(uri).getPartId());
            if (attachment != null) {
                return Long.valueOf(attachment.getSize());
            }
            return null;
        } else if (match == 2) {
            return DeprecatedPersistentBlobProvider.getFileSize(context, uri);
        } else {
            if (match != 3) {
                return null;
            }
            return BlobProvider.getFileSize(uri);
        }
    }

    public static String getAttachmentContentType(Context context, Uri uri) {
        int match = uriMatcher.match(uri);
        if (match == 1) {
            DatabaseAttachment attachment = SignalDatabase.attachments().getAttachment(new PartUriParser(uri).getPartId());
            if (attachment != null) {
                return attachment.getContentType();
            }
            return null;
        } else if (match == 2) {
            return DeprecatedPersistentBlobProvider.getMimeType(context, uri);
        } else {
            if (match != 3) {
                return null;
            }
            return BlobProvider.getMimeType(uri);
        }
    }

    public static boolean getAttachmentIsVideoGif(Context context, Uri uri) {
        DatabaseAttachment attachment;
        if (uriMatcher.match(uri) == 1 && (attachment = SignalDatabase.attachments().getAttachment(new PartUriParser(uri).getPartId())) != null) {
            return attachment.isVideoGif();
        }
        return false;
    }

    public static AttachmentDatabase.TransformProperties getAttachmentTransformProperties(Uri uri) {
        if (uriMatcher.match(uri) != 1) {
            return null;
        }
        return SignalDatabase.attachments().getTransformProperties(new PartUriParser(uri).getPartId());
    }

    public static Uri getAttachmentPublicUri(Uri uri) {
        return PartProvider.getContentUri(new PartUriParser(uri).getPartId());
    }

    public static Uri getAttachmentDataUri(AttachmentId attachmentId) {
        return ContentUris.withAppendedId(Uri.withAppendedPath(PART_CONTENT_URI, String.valueOf(attachmentId.getUniqueId())), attachmentId.getRowId());
    }

    public static Uri getAttachmentThumbnailUri(AttachmentId attachmentId) {
        return getAttachmentDataUri(attachmentId);
    }

    public static Uri getStickerUri(long j) {
        return ContentUris.withAppendedId(STICKER_CONTENT_URI, j);
    }

    public static Uri getWallpaperUri(String str) {
        return Uri.withAppendedPath(WALLPAPER_CONTENT_URI, str);
    }

    public static Uri getAvatarPickerUri(String str) {
        return Uri.withAppendedPath(AVATAR_PICKER_CONTENT_URI, str);
    }

    public static Uri getEmojiUri(String str) {
        return Uri.withAppendedPath(EMOJI_CONTENT_URI, str);
    }

    public static String getWallpaperFilename(Uri uri) {
        return uri.getPathSegments().get(1);
    }

    public static String getEmojiFilename(Uri uri) {
        return uri.getPathSegments().get(1);
    }

    public static String getAvatarPickerFilename(Uri uri) {
        return uri.getPathSegments().get(1);
    }

    public static boolean isLocalUri(Uri uri) {
        int match = uriMatcher.match(uri);
        return match == 1 || match == 2 || match == 3;
    }

    public static boolean isAttachmentUri(Uri uri) {
        return uriMatcher.match(uri) == 1;
    }

    public static AttachmentId requireAttachmentId(Uri uri) {
        return new PartUriParser(uri).getPartId();
    }
}
