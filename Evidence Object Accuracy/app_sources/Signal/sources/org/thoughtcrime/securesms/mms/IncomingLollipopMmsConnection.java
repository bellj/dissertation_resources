package org.thoughtcrime.securesms.mms;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.TextUtils;
import com.google.android.mms.InvalidHeaderValueException;
import com.google.android.mms.pdu_alt.NotifyRespInd;
import com.google.android.mms.pdu_alt.PduComposer;
import com.google.android.mms.pdu_alt.PduParser;
import com.google.android.mms.pdu_alt.RetrieveConf;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.TimeoutException;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.providers.MmsBodyProvider;
import org.thoughtcrime.securesms.transport.UndeliverableMessageException;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class IncomingLollipopMmsConnection extends LollipopMmsConnection implements IncomingMmsConnection {
    public static final String ACTION = (IncomingLollipopMmsConnection.class.getCanonicalName() + "MMS_DOWNLOADED_ACTION");
    private static final String TAG = Log.tag(IncomingLollipopMmsConnection.class);

    public IncomingLollipopMmsConnection(Context context) {
        super(context, ACTION);
    }

    @Override // org.thoughtcrime.securesms.mms.LollipopMmsConnection
    public synchronized void onResult(Context context, Intent intent) {
        if (Build.VERSION.SDK_INT >= 22) {
            String str = TAG;
            Log.i(str, "HTTP status: " + intent.getIntExtra("android.telephony.extra.MMS_HTTP_STATUS", -1));
        }
        String str2 = TAG;
        Log.i(str2, "code: " + getResultCode() + ", result string: " + getResultData());
    }

    @Override // org.thoughtcrime.securesms.mms.IncomingMmsConnection
    public synchronized RetrieveConf retrieve(String str, byte[] bArr, int i) throws MmsException {
        SmsManager smsManager;
        beginTransaction();
        try {
            MmsBodyProvider.Pointer makeTemporaryPointer = MmsBodyProvider.makeTemporaryPointer(getContext());
            String isoString = Util.toIsoString(bArr);
            String str2 = TAG;
            Log.i(str2, String.format(Locale.ENGLISH, "Downloading subscriptionId=%s multimedia from '%s' [transactionId='%s'] to '%s'", Integer.valueOf(i), str, isoString, makeTemporaryPointer.getUri()));
            if (Build.VERSION.SDK_INT < 22 || i == -1) {
                smsManager = SmsManager.getDefault();
            } else {
                smsManager = SmsManager.getSmsManagerForSubscriptionId(i);
            }
            Bundle carrierConfigValues = smsManager.getCarrierConfigValues();
            if (carrierConfigValues.getBoolean("enabledTransID")) {
                if (!str.contains(isoString)) {
                    Log.i(str2, "Appending transactionId to contentLocation at the direction of CarrierConfigValues. New location: " + str);
                    str = str + isoString;
                } else {
                    Log.i(str2, "Skipping 'append transaction id' as contentLocation already contains it");
                }
            }
            if (TextUtils.isEmpty(carrierConfigValues.getString("userAgent"))) {
                carrierConfigValues.remove("userAgent");
            }
            if (TextUtils.isEmpty(carrierConfigValues.getString("uaProfUrl"))) {
                carrierConfigValues.remove("uaProfUrl");
            }
            smsManager.downloadMultimediaMessage(getContext(), str, makeTemporaryPointer.getUri(), carrierConfigValues, getPendingIntent());
            waitForResult();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            StreamUtil.copy(makeTemporaryPointer.getInputStream(), byteArrayOutputStream);
            makeTemporaryPointer.close();
            Log.i(str2, byteArrayOutputStream.size() + "-byte response: ");
            try {
                RetrieveConf retrieveConf = (RetrieveConf) new PduParser(byteArrayOutputStream.toByteArray(), smsManager.getCarrierConfigValues().getBoolean("supportMmsContentDisposition")).parse();
                if (retrieveConf == null) {
                    endTransaction();
                    return null;
                }
                sendRetrievedAcknowledgement(bArr, retrieveConf.getMmsVersion(), i);
                endTransaction();
                return retrieveConf;
            } catch (NullPointerException e) {
                Log.w(TAG, "Badly formatted MMS message caused the parser to fail.", e);
                throw new MmsException(e);
            }
        } catch (IOException | TimeoutException e2) {
            Log.w(TAG, e2);
            throw new MmsException(e2);
        }
    }

    private void sendRetrievedAcknowledgement(byte[] bArr, int i, int i2) {
        try {
            new OutgoingLollipopMmsConnection(getContext()).send(new PduComposer(getContext(), new NotifyRespInd(i, bArr, 129)).make(), i2);
        } catch (InvalidHeaderValueException e) {
            Log.w(TAG, e);
        } catch (UndeliverableMessageException e2) {
            Log.w(TAG, e2);
        }
    }
}
