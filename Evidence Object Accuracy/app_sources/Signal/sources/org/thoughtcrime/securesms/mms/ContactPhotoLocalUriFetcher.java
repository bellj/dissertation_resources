package org.thoughtcrime.securesms.mms;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.provider.ContactsContract;
import com.bumptech.glide.load.data.StreamLocalUriFetcher;
import java.io.FileNotFoundException;
import java.io.InputStream;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
class ContactPhotoLocalUriFetcher extends StreamLocalUriFetcher {
    private static final String TAG = Log.tag(ContactPhotoLocalUriFetcher.class);

    ContactPhotoLocalUriFetcher(Context context, Uri uri) {
        super(context.getContentResolver(), uri);
    }

    @Override // com.bumptech.glide.load.data.StreamLocalUriFetcher, com.bumptech.glide.load.data.LocalUriFetcher
    public InputStream loadResource(Uri uri, ContentResolver contentResolver) throws FileNotFoundException {
        return ContactsContract.Contacts.openContactPhotoInputStream(contentResolver, uri, true);
    }
}
