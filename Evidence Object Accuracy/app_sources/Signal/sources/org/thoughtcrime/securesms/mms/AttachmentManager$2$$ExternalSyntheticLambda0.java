package org.thoughtcrime.securesms.mms;

import org.thoughtcrime.securesms.mms.AttachmentManager;
import org.thoughtcrime.securesms.util.concurrent.SettableFuture;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class AttachmentManager$2$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ AttachmentManager.AnonymousClass2 f$0;
    public final /* synthetic */ LocationSlide f$1;
    public final /* synthetic */ SettableFuture f$2;

    public /* synthetic */ AttachmentManager$2$$ExternalSyntheticLambda0(AttachmentManager.AnonymousClass2 r1, LocationSlide locationSlide, SettableFuture settableFuture) {
        this.f$0 = r1;
        this.f$1 = locationSlide;
        this.f$2 = settableFuture;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$onSuccess$0(this.f$1, this.f$2);
    }
}
