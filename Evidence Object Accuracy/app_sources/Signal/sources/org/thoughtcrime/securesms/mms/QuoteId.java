package org.thoughtcrime.securesms.mms;

import android.content.Context;
import org.json.JSONException;
import org.json.JSONObject;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class QuoteId {
    private static final String AUTHOR;
    private static final String AUTHOR_DEPRECATED;
    private static final String ID;
    private static final String TAG = Log.tag(QuoteId.class);
    private final RecipientId author;
    private final long id;

    public QuoteId(long j, RecipientId recipientId) {
        this.id = j;
        this.author = recipientId;
    }

    public long getId() {
        return this.id;
    }

    public RecipientId getAuthor() {
        return this.author;
    }

    public String serialize() {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("id", this.id);
            jSONObject.put(AUTHOR, this.author.serialize());
            return jSONObject.toString();
        } catch (JSONException e) {
            Log.e(TAG, "Failed to serialize to json", e);
            return "";
        }
    }

    public static QuoteId deserialize(Context context, String str) {
        RecipientId recipientId;
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has(AUTHOR)) {
                recipientId = RecipientId.from(jSONObject.getString(AUTHOR));
            } else {
                recipientId = Recipient.external(context, jSONObject.getString(AUTHOR_DEPRECATED)).getId();
            }
            return new QuoteId(jSONObject.getLong("id"), recipientId);
        } catch (JSONException e) {
            Log.e(TAG, "Failed to deserialize from json", e);
            return null;
        }
    }
}
