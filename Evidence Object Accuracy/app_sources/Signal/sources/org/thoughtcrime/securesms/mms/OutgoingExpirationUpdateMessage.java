package org.thoughtcrime.securesms.mms;

import java.util.Collections;
import java.util.LinkedList;
import org.thoughtcrime.securesms.database.model.StoryType;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class OutgoingExpirationUpdateMessage extends OutgoingSecureMediaMessage {
    @Override // org.thoughtcrime.securesms.mms.OutgoingMediaMessage
    public boolean isExpirationUpdate() {
        return true;
    }

    public OutgoingExpirationUpdateMessage(Recipient recipient, long j, long j2) {
        super(recipient, "", new LinkedList(), j, 2, j2, false, StoryType.NONE, null, false, null, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), null);
    }
}
