package org.thoughtcrime.securesms.mms;

import android.content.Context;
import android.net.Uri;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.util.StorageUtil;

/* loaded from: classes4.dex */
public class DocumentSlide extends Slide {
    @Override // org.thoughtcrime.securesms.mms.Slide
    public boolean hasDocument() {
        return true;
    }

    public DocumentSlide(Context context, Attachment attachment) {
        super(context, attachment);
    }

    public DocumentSlide(Context context, Uri uri, String str, long j, String str2) {
        super(context, Slide.constructAttachmentFromUri(context, uri, str, j, 0, 0, true, StorageUtil.getCleanFileName(str2), null, null, null, null, false, false, false, false));
    }
}
