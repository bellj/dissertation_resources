package org.thoughtcrime.securesms.mms;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

/* loaded from: classes4.dex */
public class MediaStream implements Closeable {
    private final int height;
    private final String mimeType;
    private final InputStream stream;
    private final int width;

    public MediaStream(InputStream inputStream, String str, int i, int i2) {
        this.stream = inputStream;
        this.mimeType = str;
        this.width = i;
        this.height = i2;
    }

    public InputStream getStream() {
        return this.stream;
    }

    public String getMimeType() {
        return this.mimeType;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.stream.close();
    }
}
