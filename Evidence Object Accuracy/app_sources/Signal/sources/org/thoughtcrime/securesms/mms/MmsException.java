package org.thoughtcrime.securesms.mms;

/* loaded from: classes4.dex */
public class MmsException extends Exception {
    private static final long serialVersionUID;

    public MmsException() {
    }

    public MmsException(String str) {
        super(str);
    }

    public MmsException(Throwable th) {
        super(th);
    }

    public MmsException(String str, Throwable th) {
        super(str, th);
    }
}
