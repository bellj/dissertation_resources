package org.thoughtcrime.securesms.mms;

import com.google.android.mms.pdu_alt.SendConf;
import org.thoughtcrime.securesms.transport.UndeliverableMessageException;

/* loaded from: classes4.dex */
public interface OutgoingMmsConnection {
    SendConf send(byte[] bArr, int i) throws UndeliverableMessageException;
}
