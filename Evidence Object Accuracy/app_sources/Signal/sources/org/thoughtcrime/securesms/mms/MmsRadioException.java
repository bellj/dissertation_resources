package org.thoughtcrime.securesms.mms;

/* loaded from: classes4.dex */
public class MmsRadioException extends Throwable {
    public MmsRadioException(String str) {
        super(str);
    }

    public MmsRadioException(Exception exc) {
        super(exc);
    }
}
