package org.thoughtcrime.securesms.mms;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.stickers.StickerLocator;

/* loaded from: classes4.dex */
public class StickerSlide extends Slide {
    public static final int HEIGHT;
    public static final int WIDTH;
    private final StickerLocator stickerLocator;

    @Override // org.thoughtcrime.securesms.mms.Slide
    public int getPlaceholderRes(Resources.Theme theme) {
        return 0;
    }

    @Override // org.thoughtcrime.securesms.mms.Slide
    public boolean hasSticker() {
        return true;
    }

    @Override // org.thoughtcrime.securesms.mms.Slide
    public boolean isBorderless() {
        return true;
    }

    public StickerSlide(Context context, Attachment attachment) {
        super(context, attachment);
        StickerLocator sticker = attachment.getSticker();
        Objects.requireNonNull(sticker);
        this.stickerLocator = sticker;
    }

    public StickerSlide(Context context, Uri uri, long j, StickerLocator stickerLocator, String str) {
        super(context, Slide.constructAttachmentFromUri(context, uri, str, j, 512, 512, true, null, null, stickerLocator, null, null, false, false, false, false));
        StickerLocator sticker = this.attachment.getSticker();
        Objects.requireNonNull(sticker);
        this.stickerLocator = sticker;
    }

    @Override // org.thoughtcrime.securesms.mms.Slide
    public String getContentDescription() {
        return this.context.getString(R.string.Slide_sticker);
    }

    public String getEmoji() {
        return this.stickerLocator.getEmoji();
    }
}
