package org.thoughtcrime.securesms.mms;

import android.content.Context;
import android.net.Uri;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.util.MediaUtil;

/* loaded from: classes4.dex */
public class GifSlide extends ImageSlide {
    private final boolean borderless;

    @Override // org.thoughtcrime.securesms.mms.Slide
    public boolean isVideoGif() {
        return true;
    }

    public GifSlide(Context context, Attachment attachment) {
        super(context, attachment);
        this.borderless = attachment.isBorderless();
    }

    public GifSlide(Context context, Uri uri, long j, int i, int i2) {
        this(context, uri, j, i, i2, false, null);
    }

    public GifSlide(Context context, Uri uri, long j, int i, int i2, boolean z, String str) {
        super(context, Slide.constructAttachmentFromUri(context, uri, MediaUtil.IMAGE_GIF, j, i, i2, true, null, str, null, null, null, false, z, true, false));
        this.borderless = z;
    }

    @Override // org.thoughtcrime.securesms.mms.ImageSlide, org.thoughtcrime.securesms.mms.Slide
    public boolean isBorderless() {
        return this.borderless;
    }
}
