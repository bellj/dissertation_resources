package org.thoughtcrime.securesms.mms;

import android.text.TextUtils;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.database.documents.IdentityKeyMismatch;
import org.thoughtcrime.securesms.database.documents.NetworkFailure;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.database.model.ParentStoryId;
import org.thoughtcrime.securesms.database.model.StoryType;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class OutgoingMediaMessage {
    protected final List<Attachment> attachments;
    protected final String body;
    private final List<Contact> contacts;
    private final int distributionType;
    private final long expiresIn;
    private final GiftBadge giftBadge;
    private final Set<IdentityKeyMismatch> identityKeyMismatches;
    private final boolean isStoryReaction;
    private final List<LinkPreview> linkPreviews;
    private final List<Mention> mentions;
    private final Set<NetworkFailure> networkFailures;
    private final QuoteModel outgoingQuote;
    private final ParentStoryId parentStoryId;
    private final Recipient recipient;
    private final long sentTimeMillis;
    private final StoryType storyType;
    private final int subscriptionId;
    private final boolean viewOnce;

    public boolean isExpirationUpdate() {
        return false;
    }

    public boolean isGroup() {
        return false;
    }

    public boolean isSecure() {
        return false;
    }

    public OutgoingMediaMessage(Recipient recipient, String str, List<Attachment> list, long j, int i, long j2, boolean z, int i2, StoryType storyType, ParentStoryId parentStoryId, boolean z2, QuoteModel quoteModel, List<Contact> list2, List<LinkPreview> list3, List<Mention> list4, Set<NetworkFailure> set, Set<IdentityKeyMismatch> set2, GiftBadge giftBadge) {
        HashSet hashSet = new HashSet();
        this.networkFailures = hashSet;
        HashSet hashSet2 = new HashSet();
        this.identityKeyMismatches = hashSet2;
        LinkedList linkedList = new LinkedList();
        this.contacts = linkedList;
        LinkedList linkedList2 = new LinkedList();
        this.linkPreviews = linkedList2;
        LinkedList linkedList3 = new LinkedList();
        this.mentions = linkedList3;
        this.recipient = recipient;
        this.body = str;
        this.sentTimeMillis = j;
        this.distributionType = i2;
        this.attachments = list;
        this.subscriptionId = i;
        this.expiresIn = j2;
        this.viewOnce = z;
        this.outgoingQuote = quoteModel;
        this.storyType = storyType;
        this.parentStoryId = parentStoryId;
        this.isStoryReaction = z2;
        this.giftBadge = giftBadge;
        linkedList.addAll(list2);
        linkedList2.addAll(list3);
        linkedList3.addAll(list4);
        hashSet.addAll(set);
        hashSet2.addAll(set2);
    }

    public OutgoingMediaMessage(Recipient recipient, SlideDeck slideDeck, String str, long j, int i, long j2, boolean z, int i2, StoryType storyType, ParentStoryId parentStoryId, boolean z2, QuoteModel quoteModel, List<Contact> list, List<LinkPreview> list2, List<Mention> list3, GiftBadge giftBadge) {
        this(recipient, buildMessage(slideDeck, str), slideDeck.asAttachments(), j, i, j2, z, i2, storyType, parentStoryId, z2, quoteModel, list, list2, list3, new HashSet(), new HashSet(), giftBadge);
    }

    public OutgoingMediaMessage(OutgoingMediaMessage outgoingMediaMessage) {
        HashSet hashSet = new HashSet();
        this.networkFailures = hashSet;
        HashSet hashSet2 = new HashSet();
        this.identityKeyMismatches = hashSet2;
        LinkedList linkedList = new LinkedList();
        this.contacts = linkedList;
        LinkedList linkedList2 = new LinkedList();
        this.linkPreviews = linkedList2;
        LinkedList linkedList3 = new LinkedList();
        this.mentions = linkedList3;
        this.recipient = outgoingMediaMessage.getRecipient();
        this.body = outgoingMediaMessage.body;
        this.distributionType = outgoingMediaMessage.distributionType;
        this.attachments = outgoingMediaMessage.attachments;
        this.sentTimeMillis = outgoingMediaMessage.sentTimeMillis;
        this.subscriptionId = outgoingMediaMessage.subscriptionId;
        this.expiresIn = outgoingMediaMessage.expiresIn;
        this.viewOnce = outgoingMediaMessage.viewOnce;
        this.outgoingQuote = outgoingMediaMessage.outgoingQuote;
        this.storyType = outgoingMediaMessage.storyType;
        this.parentStoryId = outgoingMediaMessage.parentStoryId;
        this.isStoryReaction = outgoingMediaMessage.isStoryReaction;
        this.giftBadge = outgoingMediaMessage.giftBadge;
        hashSet2.addAll(outgoingMediaMessage.identityKeyMismatches);
        hashSet.addAll(outgoingMediaMessage.networkFailures);
        linkedList.addAll(outgoingMediaMessage.contacts);
        linkedList2.addAll(outgoingMediaMessage.linkPreviews);
        linkedList3.addAll(outgoingMediaMessage.mentions);
    }

    public OutgoingMediaMessage withExpiry(long j) {
        return new OutgoingMediaMessage(getRecipient(), this.body, this.attachments, this.sentTimeMillis, this.subscriptionId, j, this.viewOnce, this.distributionType, this.storyType, this.parentStoryId, this.isStoryReaction, this.outgoingQuote, this.contacts, this.linkPreviews, this.mentions, this.networkFailures, this.identityKeyMismatches, this.giftBadge);
    }

    public Recipient getRecipient() {
        return this.recipient;
    }

    public String getBody() {
        return this.body;
    }

    public List<Attachment> getAttachments() {
        return this.attachments;
    }

    public int getDistributionType() {
        return this.distributionType;
    }

    public long getSentTimeMillis() {
        return this.sentTimeMillis;
    }

    public int getSubscriptionId() {
        return this.subscriptionId;
    }

    public long getExpiresIn() {
        return this.expiresIn;
    }

    public boolean isViewOnce() {
        return this.viewOnce;
    }

    public StoryType getStoryType() {
        return this.storyType;
    }

    public ParentStoryId getParentStoryId() {
        return this.parentStoryId;
    }

    public boolean isStoryReaction() {
        return this.isStoryReaction;
    }

    public QuoteModel getOutgoingQuote() {
        return this.outgoingQuote;
    }

    public List<Contact> getSharedContacts() {
        return this.contacts;
    }

    public List<LinkPreview> getLinkPreviews() {
        return this.linkPreviews;
    }

    public List<Mention> getMentions() {
        return this.mentions;
    }

    public Set<NetworkFailure> getNetworkFailures() {
        return this.networkFailures;
    }

    public Set<IdentityKeyMismatch> getIdentityKeyMismatches() {
        return this.identityKeyMismatches;
    }

    public GiftBadge getGiftBadge() {
        return this.giftBadge;
    }

    private static String buildMessage(SlideDeck slideDeck, String str) {
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(slideDeck.getBody())) {
            return slideDeck.getBody() + "\n\n" + str;
        } else if (!TextUtils.isEmpty(str)) {
            return str;
        } else {
            return slideDeck.getBody();
        }
    }
}
