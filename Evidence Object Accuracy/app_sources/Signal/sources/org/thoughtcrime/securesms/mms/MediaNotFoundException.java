package org.thoughtcrime.securesms.mms;

/* loaded from: classes4.dex */
public class MediaNotFoundException extends Exception {
    public MediaNotFoundException() {
    }

    public MediaNotFoundException(String str) {
        super(str);
    }

    public MediaNotFoundException(Throwable th) {
        super(th);
    }

    public MediaNotFoundException(String str, Throwable th) {
        super(str, th);
    }
}
