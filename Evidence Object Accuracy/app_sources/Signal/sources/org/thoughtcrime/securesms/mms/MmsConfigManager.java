package org.thoughtcrime.securesms.mms;

import android.content.Context;
import android.content.res.Configuration;
import com.android.mms.service_alt.MmsConfig;
import j$.util.Optional;
import java.util.HashMap;
import java.util.Map;
import org.thoughtcrime.securesms.util.dualsim.SubscriptionInfoCompat;
import org.thoughtcrime.securesms.util.dualsim.SubscriptionManagerCompat;

/* loaded from: classes4.dex */
public final class MmsConfigManager {
    private static final Map<Integer, MmsConfig> mmsConfigMap = new HashMap();

    MmsConfigManager() {
    }

    public static synchronized MmsConfig getMmsConfig(Context context, int i) {
        synchronized (MmsConfigManager.class) {
            Map<Integer, MmsConfig> map = mmsConfigMap;
            MmsConfig mmsConfig = map.get(Integer.valueOf(i));
            if (mmsConfig != null) {
                return mmsConfig;
            }
            MmsConfig loadMmsConfig = loadMmsConfig(context, i);
            map.put(Integer.valueOf(i), loadMmsConfig);
            return loadMmsConfig;
        }
    }

    private static MmsConfig loadMmsConfig(Context context, int i) {
        Optional<SubscriptionInfoCompat> activeSubscriptionInfo = new SubscriptionManagerCompat(context).getActiveSubscriptionInfo(i);
        if (!activeSubscriptionInfo.isPresent()) {
            return new MmsConfig(context, i);
        }
        SubscriptionInfoCompat subscriptionInfoCompat = activeSubscriptionInfo.get();
        Configuration configuration = context.getResources().getConfiguration();
        configuration.mcc = subscriptionInfoCompat.getMcc();
        configuration.mnc = subscriptionInfoCompat.getMnc();
        return new MmsConfig(context.createConfigurationContext(configuration), i);
    }
}
