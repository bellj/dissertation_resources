package org.thoughtcrime.securesms.mms;

import com.google.android.mms.pdu_alt.RetrieveConf;
import java.io.IOException;

/* loaded from: classes4.dex */
public interface IncomingMmsConnection {
    RetrieveConf retrieve(String str, byte[] bArr, int i) throws MmsException, MmsRadioException, ApnUnavailableException, IOException;
}
