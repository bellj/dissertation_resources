package org.thoughtcrime.securesms.mms;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import j$.util.Optional;
import java.security.SecureRandom;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.UriAttachment;
import org.thoughtcrime.securesms.audio.AudioHash;
import org.thoughtcrime.securesms.blurhash.BlurHash;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.stickers.StickerLocator;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public abstract class Slide {
    protected final Attachment attachment;
    protected final Context context;

    public String getContentDescription() {
        return "";
    }

    public boolean hasAudio() {
        return false;
    }

    public boolean hasDocument() {
        return false;
    }

    public boolean hasImage() {
        return false;
    }

    public boolean hasLocation() {
        return false;
    }

    public boolean hasPlaceholder() {
        return false;
    }

    public boolean hasPlayOverlay() {
        return false;
    }

    public boolean hasSticker() {
        return false;
    }

    public boolean hasVideo() {
        return false;
    }

    public boolean hasViewOnce() {
        return false;
    }

    public boolean isBorderless() {
        return false;
    }

    public Slide(Context context, Attachment attachment) {
        this.context = context;
        this.attachment = attachment;
    }

    public String getContentType() {
        return this.attachment.getContentType();
    }

    public Uri getUri() {
        return this.attachment.getUri();
    }

    public Uri getPublicUri() {
        if (Build.VERSION.SDK_INT >= 28) {
            return this.attachment.getPublicUri();
        }
        return this.attachment.getUri();
    }

    public Optional<String> getBody() {
        return Optional.empty();
    }

    public Optional<String> getCaption() {
        return Optional.ofNullable(this.attachment.getCaption());
    }

    public Optional<String> getFileName() {
        return Optional.ofNullable(this.attachment.getFileName());
    }

    public String getFastPreflightId() {
        return this.attachment.getFastPreflightId();
    }

    public long getFileSize() {
        return this.attachment.getSize();
    }

    public boolean isVideoGif() {
        return hasVideo() && this.attachment.isVideoGif();
    }

    public Attachment asAttachment() {
        return this.attachment;
    }

    public boolean isInProgress() {
        return this.attachment.isInProgress();
    }

    public boolean isPendingDownload() {
        return getTransferState() == 3 || getTransferState() == 2;
    }

    public int getTransferState() {
        return this.attachment.getTransferState();
    }

    public int getPlaceholderRes(Resources.Theme theme) {
        throw new AssertionError("getPlaceholderRes() called for non-drawable slide");
    }

    public BlurHash getPlaceholderBlur() {
        return this.attachment.getBlurHash();
    }

    public static Attachment constructAttachmentFromUri(Context context, Uri uri, String str, long j, int i, int i2, boolean z, String str2, String str3, StickerLocator stickerLocator, BlurHash blurHash, AudioHash audioHash, boolean z2, boolean z3, boolean z4, boolean z5) {
        return constructAttachmentFromUri(context, uri, str, j, i, i2, z, str2, str3, stickerLocator, blurHash, audioHash, z2, z3, z4, z5, null);
    }

    public static Attachment constructAttachmentFromUri(Context context, Uri uri, String str, long j, int i, int i2, boolean z, String str2, String str3, StickerLocator stickerLocator, BlurHash blurHash, AudioHash audioHash, boolean z2, boolean z3, boolean z4, boolean z5, AttachmentDatabase.TransformProperties transformProperties) {
        return new UriAttachment(uri, (String) Optional.ofNullable(MediaUtil.getMimeType(context, uri)).orElse(str), 1, j, i, i2, str2, String.valueOf(new SecureRandom().nextLong()), z2, z3, z4, z5, str3, stickerLocator, blurHash, audioHash, transformProperties);
    }

    public Optional<String> getFileType(Context context) {
        Optional<String> fileName = getFileName();
        if (fileName.isPresent()) {
            String fileType = getFileType(fileName);
            if (!fileType.isEmpty()) {
                return Optional.of(fileType);
            }
        }
        return Optional.ofNullable(MediaUtil.getExtension(context, getUri()));
    }

    private static String getFileType(Optional<String> optional) {
        if (!optional.isPresent()) {
            return "";
        }
        String[] split = optional.get().split("\\.");
        if (split.length < 2) {
            return "";
        }
        String str = split[split.length - 1];
        if (str.length() <= 3) {
            return str;
        }
        return "";
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Slide)) {
            return false;
        }
        Slide slide = (Slide) obj;
        if (Util.equals(getContentType(), slide.getContentType()) && hasAudio() == slide.hasAudio() && hasImage() == slide.hasImage() && hasVideo() == slide.hasVideo() && getTransferState() == slide.getTransferState() && Util.equals(getUri(), slide.getUri())) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return Util.hashCode(getContentType(), Boolean.valueOf(hasAudio()), Boolean.valueOf(hasImage()), Boolean.valueOf(hasVideo()), getUri(), Integer.valueOf(getTransferState()));
    }
}
