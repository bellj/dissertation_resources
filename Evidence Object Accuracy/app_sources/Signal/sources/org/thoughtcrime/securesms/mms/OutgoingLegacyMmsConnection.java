package org.thoughtcrime.securesms.mms;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Uri;
import com.google.android.mms.pdu_alt.PduParser;
import com.google.android.mms.pdu_alt.SendConf;
import java.io.IOException;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPostHC4;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntityHC4;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.transport.UndeliverableMessageException;

/* loaded from: classes4.dex */
public class OutgoingLegacyMmsConnection extends LegacyMmsConnection implements OutgoingMmsConnection {
    private static final String TAG = Log.tag(OutgoingLegacyMmsConnection.class);

    public OutgoingLegacyMmsConnection(Context context) throws ApnUnavailableException {
        super(context);
    }

    private HttpUriRequest constructRequest(byte[] bArr, boolean z) throws IOException {
        try {
            HttpPostHC4 httpPostHC4 = new HttpPostHC4(this.apn.getMmsc());
            for (Header header : getBaseHeaders()) {
                httpPostHC4.addHeader(header);
            }
            httpPostHC4.setEntity(new ByteArrayEntityHC4(bArr));
            if (z) {
                httpPostHC4.setConfig(RequestConfig.custom().setProxy(new HttpHost(this.apn.getProxy(), this.apn.getPort())).build());
            }
            return httpPostHC4;
        } catch (IllegalArgumentException e) {
            throw new IOException(e);
        }
    }

    public void sendNotificationReceived(byte[] bArr, boolean z, boolean z2) throws IOException {
        sendBytes(bArr, z, z2);
    }

    @Override // org.thoughtcrime.securesms.mms.OutgoingMmsConnection
    public SendConf send(byte[] bArr, int i) throws UndeliverableMessageException {
        try {
            MmsRadio instance = MmsRadio.getInstance(this.context);
            if (isDirectConnect()) {
                Log.i(TAG, "Sending MMS directly without radio change...");
                try {
                    return send(bArr, false, false);
                } catch (IOException e) {
                    Log.w(TAG, e);
                }
            }
            Log.i(TAG, "Sending MMS with radio change and proxy...");
            instance.connect();
            try {
                SendConf send = send(bArr, true, true);
                instance.disconnect();
                return send;
            } catch (IOException e2) {
                Log.w(TAG, e2);
                Log.i(TAG, "Sending MMS with radio change and without proxy...");
                try {
                    SendConf send2 = send(bArr, true, false);
                    instance.disconnect();
                    return send2;
                } catch (IOException e3) {
                    Log.w(TAG, e3);
                    throw new UndeliverableMessageException(e3);
                }
            }
        } catch (MmsRadioException e4) {
            Log.w(TAG, e4);
            throw new UndeliverableMessageException(e4);
        }
    }

    private SendConf send(byte[] bArr, boolean z, boolean z2) throws IOException {
        return (SendConf) new PduParser(sendBytes(bArr, z, z2)).parse();
    }

    private byte[] sendBytes(byte[] bArr, boolean z, boolean z2) throws IOException {
        String str;
        boolean z3 = z2 && this.apn.hasProxy();
        if (z3) {
            str = this.apn.getProxy();
        } else {
            str = Uri.parse(this.apn.getMmsc()).getHost();
        }
        String str2 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Sending MMS of length: ");
        sb.append(bArr.length);
        String str3 = "";
        sb.append(z ? ", using mms radio" : str3);
        if (z3) {
            str3 = ", using proxy";
        }
        sb.append(str3);
        Log.i(str2, sb.toString());
        try {
            if (LegacyMmsConnection.checkRouteToHost(this.context, str, z)) {
                Log.i(str2, "got successful route to host " + str);
                byte[] execute = execute(constructRequest(bArr, z3));
                if (execute != null) {
                    return execute;
                }
            }
        } catch (IOException e) {
            Log.w(TAG, e);
        }
        throw new IOException("Connection manager could not obtain route to host.");
    }

    public static boolean isConnectionPossible(Context context) {
        try {
            if (((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(2) == null) {
                Log.w(TAG, "MMS network info was null, unsupported by this device");
                return false;
            }
            LegacyMmsConnection.getApn(context);
            return true;
        } catch (ApnUnavailableException e) {
            Log.w(TAG, e);
            return false;
        }
    }
}
