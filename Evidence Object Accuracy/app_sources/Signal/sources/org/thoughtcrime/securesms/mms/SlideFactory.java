package org.thoughtcrime.securesms.mms;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Pair;
import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.blurhash.BlurHash;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.util.MediaUtil;

/* loaded from: classes4.dex */
public final class SlideFactory {
    private static final String TAG = Log.tag(SlideFactory.class);

    private SlideFactory() {
    }

    public static Slide getSlide(Context context, String str, Uri uri, int i, int i2, AttachmentDatabase.TransformProperties transformProperties) {
        MediaType from = MediaType.from(str);
        try {
            if (PartAuthority.isLocalUri(uri)) {
                return getManuallyCalculatedSlideInfo(context, from, uri, i, i2, transformProperties);
            }
            Slide contentResolverSlideInfo = getContentResolverSlideInfo(context, from, uri, i, i2, transformProperties);
            return contentResolverSlideInfo == null ? getManuallyCalculatedSlideInfo(context, from, uri, i, i2, transformProperties) : contentResolverSlideInfo;
        } catch (IOException e) {
            Log.w(TAG, e);
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x005e A[Catch: all -> 0x009f, TryCatch #1 {all -> 0x009f, blocks: (B:4:0x0016, B:6:0x001c, B:11:0x0044, B:13:0x005e, B:15:0x0063), top: B:29:0x0016 }] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0061  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static org.thoughtcrime.securesms.mms.Slide getContentResolverSlideInfo(android.content.Context r16, org.thoughtcrime.securesms.mms.SlideFactory.MediaType r17, android.net.Uri r18, int r19, int r20, org.thoughtcrime.securesms.database.AttachmentDatabase.TransformProperties r21) {
        /*
            r0 = r18
            long r7 = java.lang.System.currentTimeMillis()
            android.content.ContentResolver r1 = r16.getContentResolver()
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r2 = r18
            android.database.Cursor r13 = r1.query(r2, r3, r4, r5, r6)
            if (r13 == 0) goto L_0x00ab
            boolean r1 = r13.moveToFirst()     // Catch: all -> 0x009f
            if (r1 == 0) goto L_0x00ab
            java.lang.String r1 = "_display_name"
            int r1 = r13.getColumnIndexOrThrow(r1)     // Catch: all -> 0x009f
            java.lang.String r4 = r13.getString(r1)     // Catch: all -> 0x009f
            java.lang.String r1 = "_size"
            int r1 = r13.getColumnIndexOrThrow(r1)     // Catch: all -> 0x009f
            long r9 = r13.getLong(r1)     // Catch: all -> 0x009f
            android.content.ContentResolver r1 = r16.getContentResolver()     // Catch: all -> 0x009f
            java.lang.String r5 = r1.getType(r0)     // Catch: all -> 0x009f
            if (r19 == 0) goto L_0x0044
            if (r20 != 0) goto L_0x003d
            goto L_0x0044
        L_0x003d:
            r2 = r16
            r11 = r19
            r12 = r20
            goto L_0x005c
        L_0x0044:
            r2 = r16
            android.util.Pair r1 = org.thoughtcrime.securesms.util.MediaUtil.getDimensions(r2, r5, r0)     // Catch: all -> 0x009f
            java.lang.Object r3 = r1.first     // Catch: all -> 0x009f
            java.lang.Integer r3 = (java.lang.Integer) r3     // Catch: all -> 0x009f
            int r3 = r3.intValue()     // Catch: all -> 0x009f
            java.lang.Object r1 = r1.second     // Catch: all -> 0x009f
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch: all -> 0x009f
            int r1 = r1.intValue()     // Catch: all -> 0x009f
            r12 = r1
            r11 = r3
        L_0x005c:
            if (r17 != 0) goto L_0x0061
            org.thoughtcrime.securesms.mms.SlideFactory$MediaType r1 = org.thoughtcrime.securesms.mms.SlideFactory.MediaType.DOCUMENT     // Catch: all -> 0x009f
            goto L_0x0063
        L_0x0061:
            r1 = r17
        L_0x0063:
            java.lang.String r3 = org.thoughtcrime.securesms.mms.SlideFactory.TAG     // Catch: all -> 0x009f
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch: all -> 0x009f
            r6.<init>()     // Catch: all -> 0x009f
            java.lang.String r14 = "remote slide with size "
            r6.append(r14)     // Catch: all -> 0x009f
            r6.append(r9)     // Catch: all -> 0x009f
            java.lang.String r14 = " took "
            r6.append(r14)     // Catch: all -> 0x009f
            long r14 = java.lang.System.currentTimeMillis()     // Catch: all -> 0x009f
            long r14 = r14 - r7
            r6.append(r14)     // Catch: all -> 0x009f
            java.lang.String r7 = "ms"
            r6.append(r7)     // Catch: all -> 0x009f
            java.lang.String r6 = r6.toString()     // Catch: all -> 0x009f
            org.signal.core.util.logging.Log.d(r3, r6)     // Catch: all -> 0x009f
            r6 = 0
            r14 = 0
            r2 = r16
            r3 = r18
            r7 = r9
            r9 = r11
            r10 = r12
            r11 = r14
            r12 = r21
            org.thoughtcrime.securesms.mms.Slide r0 = r1.createSlide(r2, r3, r4, r5, r6, r7, r9, r10, r11, r12)     // Catch: all -> 0x009f
            r13.close()
            return r0
        L_0x009f:
            r0 = move-exception
            r1 = r0
            r13.close()     // Catch: all -> 0x00a5
            goto L_0x00aa
        L_0x00a5:
            r0 = move-exception
            r2 = r0
            r1.addSuppressed(r2)
        L_0x00aa:
            throw r1
        L_0x00ab:
            if (r13 == 0) goto L_0x00b0
            r13.close()
        L_0x00b0:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.mms.SlideFactory.getContentResolverSlideInfo(android.content.Context, org.thoughtcrime.securesms.mms.SlideFactory$MediaType, android.net.Uri, int, int, org.thoughtcrime.securesms.database.AttachmentDatabase$TransformProperties):org.thoughtcrime.securesms.mms.Slide");
    }

    private static Slide getManuallyCalculatedSlideInfo(Context context, MediaType mediaType, Uri uri, int i, int i2, AttachmentDatabase.TransformProperties transformProperties) throws IOException {
        boolean z;
        String str;
        Long l;
        int i3;
        int i4;
        long currentTimeMillis = System.currentTimeMillis();
        String str2 = null;
        if (PartAuthority.isLocalUri(uri)) {
            Long attachmentSize = PartAuthority.getAttachmentSize(context, uri);
            String attachmentFileName = PartAuthority.getAttachmentFileName(context, uri);
            str = PartAuthority.getAttachmentContentType(context, uri);
            z = PartAuthority.getAttachmentIsVideoGif(context, uri);
            str2 = attachmentFileName;
            l = attachmentSize;
        } else {
            l = null;
            str = null;
            z = false;
        }
        if (l == null) {
            l = Long.valueOf(MediaUtil.getMediaSize(context, uri));
        }
        if (str == null) {
            str = MediaUtil.getMimeType(context, uri);
        }
        if (i == 0 || i2 == 0) {
            Pair<Integer, Integer> dimensions = MediaUtil.getDimensions(context, str, uri);
            i4 = ((Integer) dimensions.first).intValue();
            i3 = ((Integer) dimensions.second).intValue();
        } else {
            i4 = i;
            i3 = i2;
        }
        MediaType mediaType2 = mediaType == null ? MediaType.DOCUMENT : mediaType;
        Log.d(TAG, "local slide with size " + l + " took " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
        return mediaType2.createSlide(context, uri, str2, str, null, l.longValue(), i4, i3, z, transformProperties);
    }

    /* loaded from: classes4.dex */
    public enum MediaType {
        IMAGE(MediaUtil.IMAGE_JPEG),
        GIF(MediaUtil.IMAGE_GIF),
        AUDIO(MediaUtil.AUDIO_AAC),
        VIDEO("video/mp4"),
        DOCUMENT(MediaUtil.UNKNOWN),
        VCARD(MediaUtil.VCARD);
        
        private final String fallbackMimeType;

        MediaType(String str) {
            this.fallbackMimeType = str;
        }

        public Slide createSlide(Context context, Uri uri, String str, String str2, BlurHash blurHash, long j, int i, int i2, boolean z, AttachmentDatabase.TransformProperties transformProperties) {
            String str3 = str2 == null ? MediaUtil.OCTET : str2;
            switch (AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$mms$SlideFactory$MediaType[ordinal()]) {
                case 1:
                    return new ImageSlide(context, uri, str3, j, i, i2, false, null, blurHash, transformProperties);
                case 2:
                    return new GifSlide(context, uri, j, i, i2);
                case 3:
                    return new AudioSlide(context, uri, j, false);
                case 4:
                    return new VideoSlide(context, uri, j, z);
                case 5:
                case 6:
                    return new DocumentSlide(context, uri, str3, j, str);
                default:
                    throw new AssertionError("unrecognized enum");
            }
        }

        public static MediaType from(String str) {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            if (MediaUtil.isGif(str)) {
                return GIF;
            }
            if (MediaUtil.isImageType(str)) {
                return IMAGE;
            }
            if (MediaUtil.isAudioType(str)) {
                return AUDIO;
            }
            if (MediaUtil.isVideoType(str)) {
                return VIDEO;
            }
            if (MediaUtil.isVcard(str)) {
                return VCARD;
            }
            return DOCUMENT;
        }

        public String toFallbackMimeType() {
            return this.fallbackMimeType;
        }
    }

    /* renamed from: org.thoughtcrime.securesms.mms.SlideFactory$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$mms$SlideFactory$MediaType;

        static {
            int[] iArr = new int[MediaType.values().length];
            $SwitchMap$org$thoughtcrime$securesms$mms$SlideFactory$MediaType = iArr;
            try {
                iArr[MediaType.IMAGE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$mms$SlideFactory$MediaType[MediaType.GIF.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$mms$SlideFactory$MediaType[MediaType.AUDIO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$mms$SlideFactory$MediaType[MediaType.VIDEO.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$mms$SlideFactory$MediaType[MediaType.VCARD.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$mms$SlideFactory$MediaType[MediaType.DOCUMENT.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
        }
    }
}
