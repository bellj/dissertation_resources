package org.thoughtcrime.securesms.mms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.PowerManager;
import java.lang.reflect.InvocationTargetException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class MmsRadio {
    private static final int APN_ALREADY_ACTIVE;
    private static final String FEATURE_ENABLE_MMS;
    private static final String TAG = Log.tag(MmsRadio.class);
    public static final int TYPE_MOBILE_MMS;
    private static MmsRadio instance;
    private int connectedCounter = 0;
    private ConnectivityListener connectivityListener;
    private ConnectivityManager connectivityManager;
    private final Context context;
    private PowerManager.WakeLock wakeLock;

    public static synchronized MmsRadio getInstance(Context context) {
        MmsRadio mmsRadio;
        synchronized (MmsRadio.class) {
            if (instance == null) {
                instance = new MmsRadio(context.getApplicationContext());
            }
            mmsRadio = instance;
        }
        return mmsRadio;
    }

    private MmsRadio(Context context) {
        this.context = context;
        this.connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        PowerManager.WakeLock newWakeLock = ((PowerManager) context.getSystemService("power")).newWakeLock(1, "signal:mms");
        this.wakeLock = newWakeLock;
        newWakeLock.setReferenceCounted(true);
    }

    public synchronized void disconnect() {
        String str = TAG;
        Log.i(str, "MMS Radio Disconnect Called...");
        this.wakeLock.release();
        this.connectedCounter--;
        Log.i(str, "Reference count: " + this.connectedCounter);
        if (this.connectedCounter == 0) {
            Log.i(str, "Turning off MMS radio...");
            try {
                try {
                    this.connectivityManager.getClass().getMethod("stopUsingNetworkFeature", Integer.TYPE, String.class).invoke(this.connectivityManager, 0, FEATURE_ENABLE_MMS);
                } catch (InvocationTargetException e) {
                    Log.w(TAG, e);
                }
            } catch (IllegalAccessException e2) {
                Log.w(TAG, e2);
            } catch (NoSuchMethodException e3) {
                Log.w(TAG, e3);
            }
            if (this.connectivityListener != null) {
                Log.i(TAG, "Unregistering receiver...");
                this.context.unregisterReceiver(this.connectivityListener);
                this.connectivityListener = null;
            }
        }
    }

    public synchronized void connect() throws MmsRadioException {
        try {
            synchronized (this) {
                try {
                    try {
                        try {
                            int intValue = ((Integer) this.connectivityManager.getClass().getMethod("startUsingNetworkFeature", Integer.TYPE, String.class).invoke(this.connectivityManager, 0, FEATURE_ENABLE_MMS)).intValue();
                            String str = TAG;
                            Log.i(str, "startUsingNetworkFeature status: " + intValue);
                            if (intValue == 0) {
                                this.wakeLock.acquire();
                                this.connectedCounter++;
                                return;
                            }
                            this.wakeLock.acquire();
                            this.connectedCounter++;
                            if (this.connectivityListener == null) {
                                IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
                                ConnectivityListener connectivityListener = new ConnectivityListener();
                                this.connectivityListener = connectivityListener;
                                this.context.registerReceiver(connectivityListener, intentFilter);
                            }
                            Util.wait(this, 30000);
                            if (!isConnected()) {
                                Log.w(str, "Got back from connectivity wait, and not connected...");
                                disconnect();
                                throw new MmsRadioException("Unable to successfully enable MMS radio.");
                            }
                        } catch (IllegalAccessException e) {
                            throw new MmsRadioException(e);
                        }
                    } catch (InvocationTargetException e2) {
                        throw new MmsRadioException(e2);
                    }
                } catch (NoSuchMethodException e3) {
                    throw new MmsRadioException(e3);
                }
            }
        } catch (Throwable th) {
            throw th;
        }
    }

    private boolean isConnected() {
        NetworkInfo networkInfo = this.connectivityManager.getNetworkInfo(2);
        String str = TAG;
        Log.i(str, "Connected: " + networkInfo);
        return networkInfo != null && networkInfo.getType() == 2 && networkInfo.isConnected();
    }

    private boolean isConnectivityPossible() {
        NetworkInfo networkInfo = this.connectivityManager.getNetworkInfo(2);
        return networkInfo != null && networkInfo.isAvailable();
    }

    private boolean isConnectivityFailure() {
        NetworkInfo networkInfo = this.connectivityManager.getNetworkInfo(2);
        return networkInfo == null || networkInfo.getDetailedState() == NetworkInfo.DetailedState.FAILED;
    }

    public synchronized void issueConnectivityChange() {
        if (isConnected()) {
            Log.i(TAG, "Notifying connected...");
            notifyAll();
        } else if (!isConnected() && (isConnectivityFailure() || !isConnectivityPossible())) {
            Log.i(TAG, "Notifying not connected...");
            notifyAll();
        }
    }

    /* loaded from: classes4.dex */
    public class ConnectivityListener extends BroadcastReceiver {
        private ConnectivityListener() {
            MmsRadio.this = r1;
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            Log.i(MmsRadio.TAG, "Got connectivity change...");
            MmsRadio.this.issueConnectivityChange();
        }
    }
}
