package org.thoughtcrime.securesms.mms;

import android.content.Context;
import j$.util.DesugarArrays;
import j$.util.function.Function;
import j$.util.function.IntFunction;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public enum SentMediaQuality {
    STANDARD(0, R.string.DataAndStorageSettingsFragment__standard),
    HIGH(1, R.string.DataAndStorageSettingsFragment__high);
    
    private final int code;
    private final int label;

    SentMediaQuality(int i, int i2) {
        this.code = i;
        this.label = i2;
    }

    public static SentMediaQuality fromCode(int i) {
        SentMediaQuality sentMediaQuality = HIGH;
        if (sentMediaQuality.code == i) {
            return sentMediaQuality;
        }
        return STANDARD;
    }

    public static String[] getLabels(Context context) {
        return (String[]) DesugarArrays.stream(values()).map(new Function(context) { // from class: org.thoughtcrime.securesms.mms.SentMediaQuality$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return SentMediaQuality.lambda$getLabels$0(this.f$0, (SentMediaQuality) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).toArray(new IntFunction() { // from class: org.thoughtcrime.securesms.mms.SentMediaQuality$$ExternalSyntheticLambda1
            @Override // j$.util.function.IntFunction
            public final Object apply(int i) {
                return SentMediaQuality.lambda$getLabels$1(i);
            }
        });
    }

    public static /* synthetic */ String lambda$getLabels$0(Context context, SentMediaQuality sentMediaQuality) {
        return context.getString(sentMediaQuality.label);
    }

    public static /* synthetic */ String[] lambda$getLabels$1(int i) {
        return new String[i];
    }

    public int getCode() {
        return this.code;
    }
}
