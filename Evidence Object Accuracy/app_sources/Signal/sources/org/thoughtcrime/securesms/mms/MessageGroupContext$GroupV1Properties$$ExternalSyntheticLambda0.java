package org.thoughtcrime.securesms.mms;

import com.annimon.stream.function.Function;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MessageGroupContext$GroupV1Properties$$ExternalSyntheticLambda0 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return ((SignalServiceProtos.GroupContext.Member) obj).getE164();
    }
}
