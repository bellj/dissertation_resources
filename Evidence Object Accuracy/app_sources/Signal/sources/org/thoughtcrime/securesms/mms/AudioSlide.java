package org.thoughtcrime.securesms.mms;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.UriAttachment;
import org.thoughtcrime.securesms.components.voice.VoiceNoteDraft;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.util.MediaUtil;

/* loaded from: classes4.dex */
public class AudioSlide extends Slide {
    @Override // org.thoughtcrime.securesms.mms.Slide
    public int getPlaceholderRes(Resources.Theme theme) {
        return R.drawable.ic_audio;
    }

    @Override // org.thoughtcrime.securesms.mms.Slide
    public boolean hasAudio() {
        return true;
    }

    @Override // org.thoughtcrime.securesms.mms.Slide
    public boolean hasImage() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.mms.Slide
    public boolean hasPlaceholder() {
        return true;
    }

    public static AudioSlide createFromVoiceNoteDraft(Context context, DraftDatabase.Draft draft) {
        VoiceNoteDraft fromDraft = VoiceNoteDraft.fromDraft(draft);
        return new AudioSlide(context, new UriAttachment(fromDraft.getUri(), MediaUtil.AUDIO_AAC, 0, fromDraft.getSize(), 0, 0, null, null, true, false, false, false, null, null, null, null, null));
    }

    public AudioSlide(Context context, Uri uri, long j, boolean z) {
        super(context, Slide.constructAttachmentFromUri(context, uri, MediaUtil.AUDIO_UNSPECIFIED, j, 0, 0, false, null, null, null, null, null, z, false, false, false));
    }

    public AudioSlide(Context context, Uri uri, long j, String str, boolean z) {
        super(context, new UriAttachment(uri, str, 1, j, 0, 0, null, null, z, false, false, false, null, null, null, null, null));
    }

    public AudioSlide(Context context, Attachment attachment) {
        super(context, attachment);
    }

    @Override // org.thoughtcrime.securesms.mms.Slide
    public String getContentDescription() {
        return this.context.getString(R.string.Slide_audio);
    }
}
