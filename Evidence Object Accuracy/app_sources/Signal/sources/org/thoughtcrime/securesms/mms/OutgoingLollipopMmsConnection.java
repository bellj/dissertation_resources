package org.thoughtcrime.securesms.mms;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsManager;
import com.android.mms.service_alt.MmsConfig;
import com.google.android.mms.pdu_alt.PduParser;
import com.google.android.mms.pdu_alt.SendConf;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.concurrent.TimeoutException;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.providers.MmsBodyProvider;
import org.thoughtcrime.securesms.transport.UndeliverableMessageException;

/* loaded from: classes4.dex */
public class OutgoingLollipopMmsConnection extends LollipopMmsConnection implements OutgoingMmsConnection {
    private static final String ACTION = (OutgoingLollipopMmsConnection.class.getCanonicalName() + "MMS_SENT_ACTION");
    private static final String TAG = Log.tag(OutgoingLollipopMmsConnection.class);
    private byte[] response;

    public OutgoingLollipopMmsConnection(Context context) {
        super(context, ACTION);
    }

    @Override // org.thoughtcrime.securesms.mms.LollipopMmsConnection
    public synchronized void onResult(Context context, Intent intent) {
        if (Build.VERSION.SDK_INT >= 22) {
            String str = TAG;
            Log.i(str, "HTTP status: " + intent.getIntExtra("android.telephony.extra.MMS_HTTP_STATUS", -1));
        }
        this.response = intent.getByteArrayExtra("android.telephony.extra.MMS_DATA");
    }

    @Override // org.thoughtcrime.securesms.mms.OutgoingMmsConnection
    public synchronized SendConf send(byte[] bArr, int i) throws UndeliverableMessageException {
        SmsManager smsManager;
        SendConf sendConf;
        beginTransaction();
        try {
            MmsBodyProvider.Pointer makeTemporaryPointer = MmsBodyProvider.makeTemporaryPointer(getContext());
            StreamUtil.copy(new ByteArrayInputStream(bArr), makeTemporaryPointer.getOutputStream());
            if (Build.VERSION.SDK_INT < 22 || i == -1) {
                smsManager = SmsManager.getDefault();
            } else {
                smsManager = SmsManager.getSmsManagerForSubscriptionId(i);
            }
            Bundle bundle = new Bundle();
            bundle.putBoolean("enableGroupMms", true);
            MmsConfig mmsConfig = MmsConfigManager.getMmsConfig(getContext(), i);
            if (mmsConfig != null) {
                MmsConfig.Overridden overridden = new MmsConfig.Overridden(mmsConfig, new Bundle());
                bundle.putString("httpParams", overridden.getHttpParams());
                bundle.putInt("maxMessageSize", overridden.getMaxMessageSize());
            }
            smsManager.sendMultimediaMessage(getContext(), makeTemporaryPointer.getUri(), null, bundle, getPendingIntent());
            waitForResult();
            Log.i(TAG, "MMS broadcast received and processed.");
            makeTemporaryPointer.close();
            if (this.response != null) {
                sendConf = (SendConf) new PduParser(this.response).parse();
                endTransaction();
            } else {
                throw new UndeliverableMessageException("Null response.");
            }
        } catch (IOException | TimeoutException e) {
            throw new UndeliverableMessageException(e);
        }
        return sendConf;
    }
}
