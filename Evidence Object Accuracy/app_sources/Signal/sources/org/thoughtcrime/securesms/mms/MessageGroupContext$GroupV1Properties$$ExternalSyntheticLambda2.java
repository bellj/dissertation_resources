package org.thoughtcrime.securesms.mms;

import com.annimon.stream.function.Predicate;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MessageGroupContext$GroupV1Properties$$ExternalSyntheticLambda2 implements Predicate {
    public final /* synthetic */ RecipientId f$0;

    public /* synthetic */ MessageGroupContext$GroupV1Properties$$ExternalSyntheticLambda2(RecipientId recipientId) {
        this.f$0 = recipientId;
    }

    @Override // com.annimon.stream.function.Predicate
    public final boolean test(Object obj) {
        return this.f$0.equals((RecipientId) obj);
    }
}
