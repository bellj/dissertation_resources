package org.thoughtcrime.securesms.mms;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import j$.util.DesugarArrays;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.util.LocaleFeatureFlags;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class PushMediaConstraints extends MediaConstraints {
    private static final int KB;
    private static final int MB;
    private final MediaConfig currentConfig;

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getAudioMaxSize(Context context) {
        return 104857600;
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getDocumentMaxSize(Context context) {
        return 104857600;
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getGifMaxSize(Context context) {
        return 26214400;
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getVideoMaxSize(Context context) {
        return 104857600;
    }

    public PushMediaConstraints(SentMediaQuality sentMediaQuality) {
        this.currentConfig = getCurrentConfig(ApplicationDependencies.getApplication(), sentMediaQuality);
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public boolean isHighQuality() {
        return this.currentConfig == MediaConfig.LEVEL_3;
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getImageMaxWidth(Context context) {
        return this.currentConfig.imageSizeTargets[0];
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getImageMaxHeight(Context context) {
        return getImageMaxWidth(context);
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getImageMaxSize(Context context) {
        return this.currentConfig.maxImageFileSize;
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int[] getImageDimensionTargets(Context context) {
        return this.currentConfig.imageSizeTargets;
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getUncompressedVideoMaxSize(Context context) {
        if (MediaConstraints.isVideoTranscodeAvailable()) {
            return 524288000;
        }
        return getVideoMaxSize(context);
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getCompressedVideoMaxSize(Context context) {
        return Util.isLowMemory(context) ? 31457280 : 52428800;
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getImageCompressionQualitySetting(Context context) {
        return this.currentConfig.qualitySetting;
    }

    private static MediaConfig getCurrentConfig(Context context, SentMediaQuality sentMediaQuality) {
        if (Util.isLowMemory(context)) {
            return MediaConfig.LEVEL_1_LOW_MEMORY;
        }
        if (sentMediaQuality == SentMediaQuality.HIGH) {
            return MediaConfig.LEVEL_3;
        }
        return LocaleFeatureFlags.getMediaQualityLevel().orElse(MediaConfig.getDefault(context));
    }

    /* loaded from: classes4.dex */
    public enum MediaConfig {
        LEVEL_1_LOW_MEMORY(true, 1, PushMediaConstraints.MB, new int[]{768, 512}, 70),
        LEVEL_1(false, 1, PushMediaConstraints.MB, new int[]{1600, PushMediaConstraints.KB, 768, 512}, 70),
        LEVEL_2(false, 2, 1572864, new int[]{RecyclerView.ItemAnimator.FLAG_MOVED, 1600, PushMediaConstraints.KB, 768, 512}, 75),
        LEVEL_3(false, 3, 3145728, new int[]{RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT, 3072, RecyclerView.ItemAnimator.FLAG_MOVED, 1600, PushMediaConstraints.KB, 768, 512}, 75);
        
        private final int[] imageSizeTargets;
        private final boolean isLowMemory;
        private final int level;
        private final int maxImageFileSize;
        private final int qualitySetting;

        MediaConfig(boolean z, int i, int i2, int[] iArr, int i3) {
            this.isLowMemory = z;
            this.level = i;
            this.maxImageFileSize = i2;
            this.imageSizeTargets = iArr;
            this.qualitySetting = i3;
        }

        public int getMaxImageFileSize() {
            return this.maxImageFileSize;
        }

        public int[] getImageSizeTargets() {
            return this.imageSizeTargets;
        }

        public int getQualitySetting() {
            return this.qualitySetting;
        }

        public static MediaConfig forLevel(int i) {
            return (MediaConfig) DesugarArrays.stream(values()).filter(new PushMediaConstraints$MediaConfig$$ExternalSyntheticLambda0(i, Util.isLowMemory(ApplicationDependencies.getApplication()))).findFirst().orElse(null);
        }

        public static /* synthetic */ boolean lambda$forLevel$0(int i, boolean z, MediaConfig mediaConfig) {
            return mediaConfig.level == i && mediaConfig.isLowMemory == z;
        }

        public static MediaConfig getDefault(Context context) {
            return Util.isLowMemory(context) ? LEVEL_1_LOW_MEMORY : LEVEL_1;
        }
    }
}
