package org.thoughtcrime.securesms.mms;

import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.database.model.StoryType;
import org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2Context;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mms.MessageGroupContext;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.sms.GroupV2UpdateMessageUtil;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes4.dex */
public final class OutgoingGroupUpdateMessage extends OutgoingSecureMediaMessage {
    private final MessageGroupContext messageGroupContext;

    @Override // org.thoughtcrime.securesms.mms.OutgoingMediaMessage
    public boolean isGroup() {
        return true;
    }

    public OutgoingGroupUpdateMessage(Recipient recipient, MessageGroupContext messageGroupContext, List<Attachment> list, long j, long j2, boolean z, QuoteModel quoteModel, List<Contact> list2, List<LinkPreview> list3, List<Mention> list4) {
        super(recipient, messageGroupContext.getEncodedGroupContext(), list, j, 2, j2, z, StoryType.NONE, null, false, quoteModel, list2, list3, list4, null);
        this.messageGroupContext = messageGroupContext;
    }

    public OutgoingGroupUpdateMessage(Recipient recipient, SignalServiceProtos.GroupContext groupContext, Attachment attachment, long j, long j2, boolean z, QuoteModel quoteModel, List<Contact> list, List<LinkPreview> list2, List<Mention> list3) {
        this(recipient, new MessageGroupContext(groupContext), getAttachments(attachment), j, j2, z, quoteModel, list, list2, list3);
    }

    public OutgoingGroupUpdateMessage(Recipient recipient, DecryptedGroupV2Context decryptedGroupV2Context, Attachment attachment, long j, long j2, boolean z, QuoteModel quoteModel, List<Contact> list, List<LinkPreview> list2, List<Mention> list3) {
        this(recipient, new MessageGroupContext(decryptedGroupV2Context), getAttachments(attachment), j, j2, z, quoteModel, list, list2, list3);
    }

    public boolean isV2Group() {
        return GroupV2UpdateMessageUtil.isGroupV2(this.messageGroupContext);
    }

    public boolean isJustAGroupLeave() {
        return GroupV2UpdateMessageUtil.isJustAGroupLeave(this.messageGroupContext);
    }

    public MessageGroupContext.GroupV1Properties requireGroupV1Properties() {
        return this.messageGroupContext.requireGroupV1Properties();
    }

    public MessageGroupContext.GroupV2Properties requireGroupV2Properties() {
        return this.messageGroupContext.requireGroupV2Properties();
    }

    private static List<Attachment> getAttachments(Attachment attachment) {
        return attachment == null ? Collections.emptyList() : Collections.singletonList(attachment);
    }
}
