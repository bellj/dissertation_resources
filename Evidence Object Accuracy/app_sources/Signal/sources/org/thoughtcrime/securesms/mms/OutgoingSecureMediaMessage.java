package org.thoughtcrime.securesms.mms;

import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.database.model.ParentStoryId;
import org.thoughtcrime.securesms.database.model.StoryType;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class OutgoingSecureMediaMessage extends OutgoingMediaMessage {
    @Override // org.thoughtcrime.securesms.mms.OutgoingMediaMessage
    public boolean isSecure() {
        return true;
    }

    public OutgoingSecureMediaMessage(Recipient recipient, String str, List<Attachment> list, long j, int i, long j2, boolean z, StoryType storyType, ParentStoryId parentStoryId, boolean z2, QuoteModel quoteModel, List<Contact> list2, List<LinkPreview> list3, List<Mention> list4, GiftBadge giftBadge) {
        super(recipient, str, list, j, -1, j2, z, i, storyType, parentStoryId, z2, quoteModel, list2, list3, list4, Collections.emptySet(), Collections.emptySet(), giftBadge);
    }

    public OutgoingSecureMediaMessage(OutgoingMediaMessage outgoingMediaMessage) {
        super(outgoingMediaMessage);
    }

    @Override // org.thoughtcrime.securesms.mms.OutgoingMediaMessage
    public OutgoingMediaMessage withExpiry(long j) {
        return new OutgoingSecureMediaMessage(getRecipient(), getBody(), getAttachments(), getSentTimeMillis(), getDistributionType(), j, isViewOnce(), getStoryType(), getParentStoryId(), isStoryReaction(), getOutgoingQuote(), getSharedContacts(), getLinkPreviews(), getMentions(), getGiftBadge());
    }

    public OutgoingSecureMediaMessage withSentTimestamp(long j) {
        return new OutgoingSecureMediaMessage(getRecipient(), getBody(), getAttachments(), j, getDistributionType(), getExpiresIn(), isViewOnce(), getStoryType(), getParentStoryId(), isStoryReaction(), getOutgoingQuote(), getSharedContacts(), getLinkPreviews(), getMentions(), getGiftBadge());
    }

    public OutgoingSecureMediaMessage stripAttachments() {
        return new OutgoingSecureMediaMessage(getRecipient(), getBody(), Collections.emptyList(), getSentTimeMillis(), getDistributionType(), getExpiresIn(), isViewOnce(), getStoryType(), getParentStoryId(), isStoryReaction(), getOutgoingQuote(), Collections.emptyList(), getLinkPreviews(), getMentions(), getGiftBadge());
    }
}
