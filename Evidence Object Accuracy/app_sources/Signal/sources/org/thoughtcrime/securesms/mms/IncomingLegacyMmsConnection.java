package org.thoughtcrime.securesms.mms;

import android.content.Context;
import android.net.Uri;
import com.google.android.mms.InvalidHeaderValueException;
import com.google.android.mms.pdu_alt.NotifyRespInd;
import com.google.android.mms.pdu_alt.PduComposer;
import com.google.android.mms.pdu_alt.PduParser;
import com.google.android.mms.pdu_alt.RetrieveConf;
import java.io.IOException;
import java.util.Arrays;
import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGetHC4;
import org.apache.http.client.methods.HttpUriRequest;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.mms.LegacyMmsConnection;

/* loaded from: classes4.dex */
public class IncomingLegacyMmsConnection extends LegacyMmsConnection implements IncomingMmsConnection {
    private static final String TAG = Log.tag(IncomingLegacyMmsConnection.class);

    public IncomingLegacyMmsConnection(Context context) throws ApnUnavailableException {
        super(context);
    }

    private HttpUriRequest constructRequest(LegacyMmsConnection.Apn apn, boolean z) throws IOException {
        try {
            HttpGetHC4 httpGetHC4 = new HttpGetHC4(apn.getMmsc());
            for (Header header : getBaseHeaders()) {
                httpGetHC4.addHeader(header);
            }
            if (z) {
                httpGetHC4.setConfig(RequestConfig.custom().setProxy(new HttpHost(apn.getProxy(), apn.getPort())).build());
            }
            return httpGetHC4;
        } catch (IllegalArgumentException e) {
            throw new IOException(e);
        }
    }

    @Override // org.thoughtcrime.securesms.mms.IncomingMmsConnection
    public RetrieveConf retrieve(String str, byte[] bArr, int i) throws MmsRadioException, ApnUnavailableException, IOException {
        MmsRadio instance = MmsRadio.getInstance(this.context);
        LegacyMmsConnection.Apn apn = new LegacyMmsConnection.Apn(str, this.apn.getProxy(), Integer.toString(this.apn.getPort()), this.apn.getUsername(), this.apn.getPassword());
        if (isDirectConnect()) {
            Log.i(TAG, "Connecting directly...");
            try {
                return retrieve(apn, bArr, false, false);
            } catch (IOException | ApnUnavailableException e) {
                Log.w(TAG, e);
            }
        }
        String str2 = TAG;
        Log.i(str2, "Changing radio to MMS mode..");
        instance.connect();
        try {
            Log.i(str2, "Downloading in MMS mode with proxy...");
            try {
                return retrieve(apn, bArr, true, true);
            } catch (IOException | ApnUnavailableException e2) {
                String str3 = TAG;
                Log.w(str3, e2);
                Log.i(str3, "Downloading in MMS mode without proxy...");
                return retrieve(apn, bArr, true, false);
            }
        } finally {
            instance.disconnect();
        }
    }

    public RetrieveConf retrieve(LegacyMmsConnection.Apn apn, byte[] bArr, boolean z, boolean z2) throws IOException, ApnUnavailableException {
        String str;
        byte[] bArr2;
        boolean z3 = z2 && apn.hasProxy();
        if (z3) {
            str = apn.getProxy();
        } else {
            str = Uri.parse(apn.getMmsc()).getHost();
        }
        if (LegacyMmsConnection.checkRouteToHost(this.context, str, z)) {
            String str2 = TAG;
            Log.i(str2, "got successful route to host " + str);
            bArr2 = execute(constructRequest(apn, z3));
        } else {
            bArr2 = null;
        }
        if (bArr2 != null) {
            RetrieveConf retrieveConf = (RetrieveConf) new PduParser(bArr2).parse();
            if (retrieveConf != null) {
                sendRetrievedAcknowledgement(bArr, z, z3);
                return retrieveConf;
            }
            String str3 = TAG;
            Log.w(str3, "Couldn't parse PDU, byte response: " + Arrays.toString(bArr2));
            Log.w(str3, "Couldn't parse PDU, ASCII:         " + new String(bArr2));
            throw new IOException("Bad retrieved PDU");
        }
        throw new IOException("Connection manager could not obtain route to host.");
    }

    private void sendRetrievedAcknowledgement(byte[] bArr, boolean z, boolean z2) throws ApnUnavailableException {
        try {
            new OutgoingLegacyMmsConnection(this.context).sendNotificationReceived(new PduComposer(this.context, new NotifyRespInd(18, bArr, 129)).make(), z, z2);
        } catch (InvalidHeaderValueException | IOException e) {
            Log.w(TAG, e);
        }
    }
}
