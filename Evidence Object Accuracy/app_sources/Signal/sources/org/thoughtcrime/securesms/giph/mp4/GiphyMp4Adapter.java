package org.thoughtcrime.securesms.giph.mp4;

import android.view.View;
import j$.util.function.Function;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.giph.model.GiphyImage;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4Adapter;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;
import org.thoughtcrime.securesms.util.adapter.mapping.PagingMappingAdapter;

/* loaded from: classes4.dex */
public final class GiphyMp4Adapter extends PagingMappingAdapter<String> {

    /* loaded from: classes4.dex */
    public interface Callback {
        void onClick(GiphyImage giphyImage);
    }

    public GiphyMp4Adapter(Callback callback) {
        registerFactory(GiphyImage.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.giph.mp4.GiphyMp4Adapter$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return GiphyMp4Adapter.$r8$lambda$uN7girU1wZ2sFQWhUwFJX6kbgBI(GiphyMp4Adapter.Callback.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.giphy_mp4));
    }

    public static /* synthetic */ MappingViewHolder lambda$new$0(Callback callback, View view) {
        return new GiphyMp4ViewHolder(view, callback);
    }
}
