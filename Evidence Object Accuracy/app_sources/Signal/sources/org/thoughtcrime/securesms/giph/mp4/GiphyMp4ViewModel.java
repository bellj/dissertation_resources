package org.thoughtcrime.securesms.giph.mp4;

import android.text.TextUtils;
import androidx.arch.core.util.Function;
import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import java.util.List;
import java.util.Objects;
import org.signal.paging.LivePagedData;
import org.signal.paging.PagedData;
import org.signal.paging.PagingConfig;
import org.signal.paging.PagingController;
import org.thoughtcrime.securesms.giph.model.GiphyImage;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4SaveResult;
import org.thoughtcrime.securesms.util.DefaultValueLiveData;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;

/* loaded from: classes4.dex */
public final class GiphyMp4ViewModel extends ViewModel {
    private final LiveData<MappingModelList> images;
    private final boolean isForMms;
    private final MutableLiveData<LivePagedData<String, GiphyImage>> pagedData;
    private final LiveData<PagingController<String>> pagingController;
    private String query;
    private final GiphyMp4Repository repository;
    private final SingleLiveEvent<GiphyMp4SaveResult> saveResultEvents;

    public static /* synthetic */ boolean lambda$new$0(GiphyImage giphyImage) {
        return giphyImage != null;
    }

    private GiphyMp4ViewModel(boolean z) {
        this.isForMms = z;
        this.repository = new GiphyMp4Repository();
        DefaultValueLiveData defaultValueLiveData = new DefaultValueLiveData(getGiphyImagePagedData(null));
        this.pagedData = defaultValueLiveData;
        this.saveResultEvents = new SingleLiveEvent<>();
        this.pagingController = Transformations.map(defaultValueLiveData, new Function() { // from class: org.thoughtcrime.securesms.giph.mp4.GiphyMp4ViewModel$$ExternalSyntheticLambda1
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ((LivePagedData) obj).getController();
            }
        });
        this.images = Transformations.switchMap(defaultValueLiveData, new Function(z) { // from class: org.thoughtcrime.securesms.giph.mp4.GiphyMp4ViewModel$$ExternalSyntheticLambda2
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return GiphyMp4ViewModel.lambda$new$5(this.f$0, (LivePagedData) obj);
            }
        });
    }

    public static /* synthetic */ LiveData lambda$new$5(boolean z, LivePagedData livePagedData) {
        return Transformations.map(livePagedData.getData(), new Function(z) { // from class: org.thoughtcrime.securesms.giph.mp4.GiphyMp4ViewModel$$ExternalSyntheticLambda3
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return GiphyMp4ViewModel.lambda$new$4(this.f$0, (List) obj);
            }
        });
    }

    public static /* synthetic */ MappingModelList lambda$new$4(boolean z, List list) {
        return (MappingModelList) Stream.of(list).filter(new Predicate() { // from class: org.thoughtcrime.securesms.giph.mp4.GiphyMp4ViewModel$$ExternalSyntheticLambda4
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return GiphyMp4ViewModel.lambda$new$0((GiphyImage) obj);
            }
        }).filterNot(new Predicate(z) { // from class: org.thoughtcrime.securesms.giph.mp4.GiphyMp4ViewModel$$ExternalSyntheticLambda5
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return GiphyMp4ViewModel.lambda$new$1(this.f$0, (GiphyImage) obj);
            }
        }).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.giph.mp4.GiphyMp4ViewModel$$ExternalSyntheticLambda6
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return GiphyMp4ViewModel.lambda$new$2((GiphyImage) obj);
            }
        }).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.giph.mp4.GiphyMp4ViewModel$$ExternalSyntheticLambda7
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return GiphyMp4ViewModel.lambda$new$3((GiphyImage) obj);
            }
        }).collect(MappingModelList.toMappingModelList());
    }

    public static /* synthetic */ boolean lambda$new$1(boolean z, GiphyImage giphyImage) {
        return TextUtils.isEmpty(z ? giphyImage.getGifMmsUrl() : giphyImage.getGifUrl());
    }

    public static /* synthetic */ boolean lambda$new$2(GiphyImage giphyImage) {
        return TextUtils.isEmpty(giphyImage.getMp4PreviewUrl());
    }

    public static /* synthetic */ boolean lambda$new$3(GiphyImage giphyImage) {
        return TextUtils.isEmpty(giphyImage.getStillUrl());
    }

    public LiveData<LivePagedData<String, GiphyImage>> getPagedData() {
        return this.pagedData;
    }

    public void updateSearchQuery(String str) {
        if (!Objects.equals(str, this.query)) {
            this.query = str;
            this.pagedData.setValue(getGiphyImagePagedData(str));
        }
    }

    public void saveToBlob(GiphyImage giphyImage) {
        this.saveResultEvents.postValue(new GiphyMp4SaveResult.InProgress());
        GiphyMp4Repository giphyMp4Repository = this.repository;
        boolean z = this.isForMms;
        SingleLiveEvent<GiphyMp4SaveResult> singleLiveEvent = this.saveResultEvents;
        Objects.requireNonNull(singleLiveEvent);
        giphyMp4Repository.saveToBlob(giphyImage, z, new Consumer() { // from class: org.thoughtcrime.securesms.giph.mp4.GiphyMp4ViewModel$$ExternalSyntheticLambda0
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                SingleLiveEvent.this.postValue((GiphyMp4SaveResult) obj);
            }
        });
    }

    public LiveData<GiphyMp4SaveResult> getSaveResultEvents() {
        return this.saveResultEvents;
    }

    public LiveData<MappingModelList> getImages() {
        return this.images;
    }

    public LiveData<PagingController<String>> getPagingController() {
        return this.pagingController;
    }

    private LivePagedData<String, GiphyImage> getGiphyImagePagedData(String str) {
        return PagedData.createForLiveData(new GiphyMp4PagedDataSource(str), new PagingConfig.Builder().setPageSize(20).setBufferPages(1).build());
    }

    /* loaded from: classes4.dex */
    public static class Factory implements ViewModelProvider.Factory {
        private final boolean isForMms;

        public Factory(boolean z) {
            this.isForMms = z;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            T cast = cls.cast(new GiphyMp4ViewModel(this.isForMms));
            Objects.requireNonNull(cast);
            return cast;
        }
    }
}
