package org.thoughtcrime.securesms.giph.mp4;

import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.util.DeviceProperties;

/* loaded from: classes4.dex */
public final class GiphyMp4PlaybackPolicy {
    public static int maxRepeatsOfSinglePlayback() {
        return 4;
    }

    public static int maxSimultaneousPlaybackInConversation() {
        return 4;
    }

    public static int maxSimultaneousPlaybackInSearchResults() {
        return 12;
    }

    private GiphyMp4PlaybackPolicy() {
    }

    public static boolean autoplay() {
        return !DeviceProperties.isLowMemoryDevice(ApplicationDependencies.getApplication());
    }

    public static long maxDurationOfSinglePlayback() {
        return TimeUnit.SECONDS.toMillis(8);
    }
}
