package org.thoughtcrime.securesms.giph.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/* loaded from: classes.dex */
public class GiphyPagination {
    @JsonProperty
    private int count;
    @JsonProperty
    private int offset;
    @JsonProperty
    private int total_count;

    public int getTotalCount() {
        return this.total_count;
    }
}
