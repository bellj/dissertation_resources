package org.thoughtcrime.securesms.giph.mp4;

import android.util.SparseArray;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4PlaybackController;
import org.thoughtcrime.securesms.util.Projection;

/* loaded from: classes4.dex */
public final class GiphyMp4ProjectionRecycler implements GiphyMp4PlaybackController.Callback {
    private final List<GiphyMp4ProjectionPlayerHolder> holders;
    private final SparseArray<GiphyMp4ProjectionPlayerHolder> notPlaying;
    private final SparseArray<GiphyMp4ProjectionPlayerHolder> playing;

    public GiphyMp4ProjectionRecycler(List<GiphyMp4ProjectionPlayerHolder> list) {
        this.holders = list;
        this.playing = new SparseArray<>(list.size());
        this.notPlaying = new SparseArray<>(list.size());
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4PlaybackController.Callback
    public void update(RecyclerView recyclerView, List<GiphyMp4Playable> list, Set<Integer> set) {
        stopAndReleaseAssignedVideos(set);
        for (GiphyMp4Playable giphyMp4Playable : list) {
            if (set.contains(Integer.valueOf(giphyMp4Playable.getAdapterPosition()))) {
                startPlayback(recyclerView, acquireHolderForPosition(giphyMp4Playable.getAdapterPosition()), giphyMp4Playable);
            } else {
                giphyMp4Playable.showProjectionArea();
            }
        }
        for (GiphyMp4Playable giphyMp4Playable2 : list) {
            updateVideoDisplayPositionAndSize(recyclerView, giphyMp4Playable2);
        }
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4PlaybackController.Callback
    public void updateVideoDisplayPositionAndSize(RecyclerView recyclerView, GiphyMp4Playable giphyMp4Playable) {
        GiphyMp4ProjectionPlayerHolder currentHolder = getCurrentHolder(giphyMp4Playable.getAdapterPosition());
        if (currentHolder != null) {
            updateVideoDisplayPositionAndSize(recyclerView, currentHolder, giphyMp4Playable);
        }
    }

    private void stopAndReleaseAssignedVideos(Set<Integer> set) {
        ArrayList<Integer> arrayList = new ArrayList(this.playing.size());
        for (int i = 0; i < this.playing.size(); i++) {
            if (!set.contains(Integer.valueOf(this.playing.keyAt(i)))) {
                this.notPlaying.put(this.playing.keyAt(i), this.playing.valueAt(i));
                this.playing.valueAt(i).clearMedia();
                this.playing.valueAt(i).setOnPlaybackReady(null);
                arrayList.add(Integer.valueOf(this.playing.keyAt(i)));
            }
        }
        for (Integer num : arrayList) {
            this.playing.remove(num.intValue());
        }
        for (int i2 = 0; i2 < this.notPlaying.size(); i2++) {
            this.notPlaying.valueAt(i2).hide();
        }
    }

    private void updateVideoDisplayPositionAndSize(RecyclerView recyclerView, GiphyMp4ProjectionPlayerHolder giphyMp4ProjectionPlayerHolder, GiphyMp4Playable giphyMp4Playable) {
        if (giphyMp4Playable.canPlayContent()) {
            Projection giphyMp4PlayableProjection = giphyMp4Playable.getGiphyMp4PlayableProjection(recyclerView);
            giphyMp4ProjectionPlayerHolder.getContainer().setX(giphyMp4PlayableProjection.getX());
            giphyMp4ProjectionPlayerHolder.getContainer().setY(giphyMp4PlayableProjection.getY() + recyclerView.getTranslationY());
            ViewGroup.LayoutParams layoutParams = giphyMp4ProjectionPlayerHolder.getContainer().getLayoutParams();
            if (!(layoutParams.width == giphyMp4PlayableProjection.getWidth() && layoutParams.height == giphyMp4PlayableProjection.getHeight())) {
                layoutParams.width = giphyMp4PlayableProjection.getWidth();
                layoutParams.height = giphyMp4PlayableProjection.getHeight();
                giphyMp4ProjectionPlayerHolder.getContainer().setLayoutParams(layoutParams);
            }
            giphyMp4ProjectionPlayerHolder.setCorners(giphyMp4PlayableProjection.getCorners());
            giphyMp4PlayableProjection.release();
        }
    }

    private void startPlayback(RecyclerView recyclerView, GiphyMp4ProjectionPlayerHolder giphyMp4ProjectionPlayerHolder, GiphyMp4Playable giphyMp4Playable) {
        if (!Objects.equals(giphyMp4ProjectionPlayerHolder.getMediaItem(), giphyMp4Playable.getMediaItem())) {
            giphyMp4ProjectionPlayerHolder.setOnPlaybackReady(null);
            giphyMp4Playable.showProjectionArea();
            giphyMp4ProjectionPlayerHolder.show();
            giphyMp4ProjectionPlayerHolder.setOnPlaybackReady(new Runnable(recyclerView) { // from class: org.thoughtcrime.securesms.giph.mp4.GiphyMp4ProjectionRecycler$$ExternalSyntheticLambda0
                public final /* synthetic */ RecyclerView f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    GiphyMp4ProjectionRecycler.lambda$startPlayback$0(GiphyMp4Playable.this, this.f$1);
                }
            });
            giphyMp4ProjectionPlayerHolder.playContent(giphyMp4Playable.getMediaItem(), giphyMp4Playable.getPlaybackPolicyEnforcer());
            return;
        }
        giphyMp4ProjectionPlayerHolder.setOnPlaybackReady(new Runnable(recyclerView) { // from class: org.thoughtcrime.securesms.giph.mp4.GiphyMp4ProjectionRecycler$$ExternalSyntheticLambda1
            public final /* synthetic */ RecyclerView f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                GiphyMp4ProjectionRecycler.lambda$startPlayback$1(GiphyMp4Playable.this, this.f$1);
            }
        });
    }

    public static /* synthetic */ void lambda$startPlayback$0(GiphyMp4Playable giphyMp4Playable, RecyclerView recyclerView) {
        giphyMp4Playable.hideProjectionArea();
        recyclerView.invalidate();
    }

    public static /* synthetic */ void lambda$startPlayback$1(GiphyMp4Playable giphyMp4Playable, RecyclerView recyclerView) {
        giphyMp4Playable.hideProjectionArea();
        recyclerView.invalidate();
    }

    public GiphyMp4ProjectionPlayerHolder getCurrentHolder(int i) {
        if (this.playing.get(i) != null) {
            return this.playing.get(i);
        }
        if (this.notPlaying.get(i) != null) {
            return this.notPlaying.get(i);
        }
        return null;
    }

    private GiphyMp4ProjectionPlayerHolder acquireHolderForPosition(int i) {
        GiphyMp4ProjectionPlayerHolder giphyMp4ProjectionPlayerHolder = this.playing.get(i);
        if (giphyMp4ProjectionPlayerHolder == null) {
            if (this.notPlaying.size() != 0) {
                giphyMp4ProjectionPlayerHolder = this.notPlaying.get(i);
                if (giphyMp4ProjectionPlayerHolder == null) {
                    int keyAt = this.notPlaying.keyAt(0);
                    GiphyMp4ProjectionPlayerHolder giphyMp4ProjectionPlayerHolder2 = this.notPlaying.get(keyAt);
                    Objects.requireNonNull(giphyMp4ProjectionPlayerHolder2);
                    this.notPlaying.remove(keyAt);
                    giphyMp4ProjectionPlayerHolder = giphyMp4ProjectionPlayerHolder2;
                } else {
                    this.notPlaying.remove(i);
                }
            } else {
                giphyMp4ProjectionPlayerHolder = this.holders.remove(0);
            }
            this.playing.put(i, giphyMp4ProjectionPlayerHolder);
        }
        return giphyMp4ProjectionPlayerHolder;
    }
}
