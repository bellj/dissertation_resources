package org.thoughtcrime.securesms.giph.model;

import android.text.TextUtils;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.thoughtcrime.securesms.giph.model.GiphyImage;
import org.thoughtcrime.securesms.util.ByteUnit;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* loaded from: classes.dex */
public class GiphyImage implements MappingModel<GiphyImage> {
    private static final int MAX_SIZE = ((int) ByteUnit.MEGABYTES.toBytes(2));
    @JsonProperty
    private ImageTypes images;
    @JsonProperty("is_sticker")
    private boolean isSticker;

    /* loaded from: classes.dex */
    public static class ImageData {
        @JsonProperty
        private int height;
        @JsonProperty
        private String mp4;
        @JsonProperty
        private int mp4_size;
        @JsonProperty
        private int size;
        @JsonProperty
        private String url;
        @JsonProperty
        private String webp;
        @JsonProperty
        private int width;
    }

    /* loaded from: classes.dex */
    public static class ImageTypes {
        @JsonProperty
        private ImageData downsized;
        @JsonProperty
        private ImageData downsized_small;
        @JsonProperty
        private ImageData fixed_height;
        @JsonProperty
        private ImageData fixed_height_small;
        @JsonProperty
        private ImageData fixed_height_small_still;
        @JsonProperty
        private ImageData fixed_width;
        @JsonProperty
        private ImageData fixed_width_small;
        @JsonProperty
        private ImageData fixed_width_small_still;
        @JsonProperty
        private ImageData preview;
    }

    /* loaded from: classes4.dex */
    public interface SizeFunction {
        int getSize(ImageData imageData);
    }

    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
    public /* synthetic */ Object getChangePayload(GiphyImage giphyImage) {
        return MappingModel.CC.$default$getChangePayload(this, giphyImage);
    }

    public boolean areItemsTheSame(GiphyImage giphyImage) {
        return getMp4Url().equals(giphyImage.getMp4Url());
    }

    public boolean areContentsTheSame(GiphyImage giphyImage) {
        return areItemsTheSame(giphyImage);
    }

    public boolean isSticker() {
        return this.isSticker;
    }

    public String getGifUrl() {
        ImageData gifData = getGifData();
        if (gifData != null) {
            return gifData.url;
        }
        return null;
    }

    public String getMp4Url() {
        ImageData mp4Data = getMp4Data();
        if (mp4Data != null) {
            return mp4Data.mp4;
        }
        return null;
    }

    public String getMp4PreviewUrl() {
        ImageData mp4PreviewData = getMp4PreviewData();
        if (mp4PreviewData != null) {
            return mp4PreviewData.mp4;
        }
        return null;
    }

    public long getGifSize() {
        ImageData gifData = getGifData();
        if (gifData != null) {
            return (long) gifData.size;
        }
        return 0;
    }

    public String getGifMmsUrl() {
        ImageData gifMmsData = getGifMmsData();
        if (gifMmsData != null) {
            return gifMmsData.url;
        }
        return null;
    }

    public long getMmsGifSize() {
        ImageData gifMmsData = getGifMmsData();
        if (gifMmsData != null) {
            return (long) gifMmsData.size;
        }
        return 0;
    }

    public float getGifAspectRatio() {
        return ((float) this.images.downsized_small.width) / ((float) this.images.downsized_small.height);
    }

    public int getGifWidth() {
        ImageData gifData = getGifData();
        if (gifData != null) {
            return gifData.width;
        }
        return 0;
    }

    public int getGifHeight() {
        ImageData gifData = getGifData();
        if (gifData != null) {
            return gifData.height;
        }
        return 0;
    }

    public String getStillUrl() {
        ImageData stillData = getStillData();
        if (stillData != null) {
            return stillData.url;
        }
        return null;
    }

    public long getStillSize() {
        ImageData stillData = getStillData();
        if (stillData != null) {
            return (long) stillData.size;
        }
        return 0;
    }

    private ImageData getMp4Data() {
        return getLargestMp4WithinSizeConstraint(this.images.fixed_width, this.images.fixed_height, this.images.fixed_width_small, this.images.fixed_height_small, this.images.downsized_small);
    }

    private ImageData getMp4PreviewData() {
        return this.images.preview;
    }

    private ImageData getGifData() {
        return getLargestGifWithinSizeConstraint(this.images.downsized, this.images.fixed_width, this.images.fixed_height, this.images.fixed_width_small, this.images.fixed_height_small);
    }

    private ImageData getGifMmsData() {
        return getLargestGifWithinSizeConstraint(this.images.fixed_width_small, this.images.fixed_height_small);
    }

    private ImageData getStillData() {
        return getFirstNonEmpty(this.images.fixed_width_small_still, this.images.fixed_height_small_still);
    }

    private static ImageData getFirstNonEmpty(ImageData... imageDataArr) {
        for (ImageData imageData : imageDataArr) {
            if (!TextUtils.isEmpty(imageData.url)) {
                return imageData;
            }
        }
        return null;
    }

    private ImageData getLargestGifWithinSizeConstraint(ImageData... imageDataArr) {
        return getLargestWithinSizeConstraint(new SizeFunction() { // from class: org.thoughtcrime.securesms.giph.model.GiphyImage$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.giph.model.GiphyImage.SizeFunction
            public final int getSize(GiphyImage.ImageData imageData) {
                return GiphyImage.$r8$lambda$dWTReewIlbA2C4Ervk8MGwKZYNg(imageData);
            }
        }, imageDataArr);
    }

    private ImageData getLargestMp4WithinSizeConstraint(ImageData... imageDataArr) {
        return getLargestWithinSizeConstraint(new SizeFunction() { // from class: org.thoughtcrime.securesms.giph.model.GiphyImage$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.giph.model.GiphyImage.SizeFunction
            public final int getSize(GiphyImage.ImageData imageData) {
                return GiphyImage.$r8$lambda$9pZpXEUU_PqJW_ZNwxJhnhWboKY(imageData);
            }
        }, imageDataArr);
    }

    private ImageData getLargestWithinSizeConstraint(SizeFunction sizeFunction, ImageData... imageDataArr) {
        int size;
        ImageData imageData = null;
        int i = 0;
        for (ImageData imageData2 : imageDataArr) {
            if (imageData2 != null && (size = sizeFunction.getSize(imageData2)) <= MAX_SIZE && size > i) {
                imageData = imageData2;
                i = size;
            }
        }
        return imageData;
    }
}
