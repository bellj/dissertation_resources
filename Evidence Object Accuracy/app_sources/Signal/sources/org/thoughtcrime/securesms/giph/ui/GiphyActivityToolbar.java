package org.thoughtcrime.securesms.giph.ui;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class GiphyActivityToolbar extends Toolbar {
    private ImageView action;
    private ImageView clearToggle;
    private OnFilterChangedListener filterListener;
    private EditText searchText;
    private LinearLayout toggleContainer;

    /* loaded from: classes4.dex */
    public interface OnFilterChangedListener {
        void onFilterChanged(String str);
    }

    public GiphyActivityToolbar(Context context) {
        this(context, null);
    }

    public GiphyActivityToolbar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.toolbarStyle);
    }

    public GiphyActivityToolbar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ViewGroup.inflate(context, R.layout.giphy_activity_toolbar, this);
        this.action = (ImageView) findViewById(R.id.action_icon);
        this.searchText = (EditText) findViewById(R.id.search_view);
        this.clearToggle = (ImageView) findViewById(R.id.search_clear);
        this.toggleContainer = (LinearLayout) findViewById(R.id.toggle_container);
        this.clearToggle.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.giph.ui.GiphyActivityToolbar.1
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                GiphyActivityToolbar.this.searchText.setText("");
                GiphyActivityToolbar.this.clearToggle.setVisibility(4);
            }
        });
        this.searchText.addTextChangedListener(new TextWatcher() { // from class: org.thoughtcrime.securesms.giph.ui.GiphyActivityToolbar.2
            @Override // android.text.TextWatcher
            public void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
            }

            @Override // android.text.TextWatcher
            public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
            }

            @Override // android.text.TextWatcher
            public void afterTextChanged(Editable editable) {
                if (SearchUtil.isEmpty(GiphyActivityToolbar.this.searchText)) {
                    GiphyActivityToolbar.this.clearToggle.setVisibility(4);
                } else {
                    GiphyActivityToolbar.this.clearToggle.setVisibility(0);
                }
                GiphyActivityToolbar.this.notifyListener();
            }
        });
        this.searchText.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: org.thoughtcrime.securesms.giph.ui.GiphyActivityToolbar.3
            @Override // android.widget.TextView.OnEditorActionListener
            public boolean onEditorAction(TextView textView, int i2, KeyEvent keyEvent) {
                if (i2 == 3) {
                    ((InputMethodManager) GiphyActivityToolbar.this.getContext().getSystemService("input_method")).hideSoftInputFromWindow(GiphyActivityToolbar.this.searchText.getWindowToken(), 0);
                }
                return false;
            }
        });
        setLogo((Drawable) null);
        setNavigationIcon((Drawable) null);
        setContentInsetStartWithNavigation(0);
        expandTapArea(this, this.action);
    }

    @Override // androidx.appcompat.widget.Toolbar
    public void setNavigationIcon(int i) {
        this.action.setImageResource(i);
    }

    public void clear() {
        this.searchText.setText("");
        notifyListener();
    }

    public void setOnFilterChangedListener(OnFilterChangedListener onFilterChangedListener) {
        this.filterListener = onFilterChangedListener;
    }

    public void notifyListener() {
        OnFilterChangedListener onFilterChangedListener = this.filterListener;
        if (onFilterChangedListener != null) {
            onFilterChangedListener.onFilterChanged(this.searchText.getText().toString());
        }
    }

    private void expandTapArea(View view, View view2) {
        view.post(new Runnable(view2, getResources().getDimensionPixelSize(R.dimen.contact_selection_actions_tap_area), view) { // from class: org.thoughtcrime.securesms.giph.ui.GiphyActivityToolbar$$ExternalSyntheticLambda0
            public final /* synthetic */ View f$0;
            public final /* synthetic */ int f$1;
            public final /* synthetic */ View f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                GiphyActivityToolbar.m1803$r8$lambda$6XSXW_RBwktgahQ58VkwmBKMRU(this.f$0, this.f$1, this.f$2);
            }
        });
    }

    public static /* synthetic */ void lambda$expandTapArea$0(View view, int i, View view2) {
        Rect rect = new Rect();
        view.getHitRect(rect);
        rect.top -= i;
        rect.left -= i;
        rect.right += i;
        rect.bottom += i;
        view2.setTouchDelegate(new TouchDelegate(rect, view));
    }

    /* loaded from: classes4.dex */
    private static class SearchUtil {
        private SearchUtil() {
        }

        public static boolean isTextInput(EditText editText) {
            return (editText.getInputType() & 15) == 1;
        }

        public static boolean isPhoneInput(EditText editText) {
            return (editText.getInputType() & 15) == 3;
        }

        public static boolean isEmpty(EditText editText) {
            return editText.getText().length() <= 0;
        }
    }
}
