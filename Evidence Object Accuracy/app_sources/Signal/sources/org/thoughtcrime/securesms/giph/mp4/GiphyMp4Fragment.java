package org.thoughtcrime.securesms.giph.mp4;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import java.util.Objects;
import org.signal.paging.LivePagedData;
import org.signal.paging.PagingController;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.giph.model.GiphyImage;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4Adapter;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4ViewModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;

/* loaded from: classes4.dex */
public class GiphyMp4Fragment extends Fragment {
    private static final String IS_FOR_MMS;

    public GiphyMp4Fragment() {
        super(R.layout.giphy_mp4_fragment);
    }

    public static Fragment create(boolean z) {
        GiphyMp4Fragment giphyMp4Fragment = new GiphyMp4Fragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(IS_FOR_MMS, z);
        giphyMp4Fragment.setArguments(bundle);
        return giphyMp4Fragment;
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        boolean z = requireArguments().getBoolean(IS_FOR_MMS, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.giphy_recycler);
        ContentLoadingProgressBar contentLoadingProgressBar = (ContentLoadingProgressBar) view.findViewById(R.id.content_loading);
        GiphyMp4ViewModel giphyMp4ViewModel = (GiphyMp4ViewModel) ViewModelProviders.of(requireActivity(), new GiphyMp4ViewModel.Factory(z)).get(GiphyMp4ViewModel.class);
        Objects.requireNonNull(giphyMp4ViewModel);
        GiphyMp4Adapter giphyMp4Adapter = new GiphyMp4Adapter(new GiphyMp4Adapter.Callback() { // from class: org.thoughtcrime.securesms.giph.mp4.GiphyMp4Fragment$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Adapter.Callback
            public final void onClick(GiphyImage giphyImage) {
                GiphyMp4ViewModel.this.saveToBlob(giphyImage);
            }
        });
        GiphyMp4ProjectionRecycler giphyMp4ProjectionRecycler = new GiphyMp4ProjectionRecycler(GiphyMp4ProjectionPlayerHolder.injectVideoViews(requireContext(), getViewLifecycleOwner().getLifecycle(), (FrameLayout) view.findViewById(R.id.giphy_parent), GiphyMp4PlaybackPolicy.maxSimultaneousPlaybackInSearchResults()));
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, 1));
        recyclerView.setAdapter(giphyMp4Adapter);
        recyclerView.setItemAnimator(null);
        contentLoadingProgressBar.show();
        GiphyMp4PlaybackController.attach(recyclerView, giphyMp4ProjectionRecycler, GiphyMp4PlaybackPolicy.maxSimultaneousPlaybackInSearchResults());
        giphyMp4ViewModel.getImages().observe(getViewLifecycleOwner(), new Observer((TextView) view.findViewById(R.id.nothing_found), giphyMp4Adapter, contentLoadingProgressBar) { // from class: org.thoughtcrime.securesms.giph.mp4.GiphyMp4Fragment$$ExternalSyntheticLambda1
            public final /* synthetic */ TextView f$0;
            public final /* synthetic */ GiphyMp4Adapter f$1;
            public final /* synthetic */ ContentLoadingProgressBar f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                GiphyMp4Fragment.m1799$r8$lambda$vXKu2Hbv0L7IrijQZD1Zdo3Yw(this.f$0, this.f$1, this.f$2, (MappingModelList) obj);
            }
        });
        giphyMp4ViewModel.getPagingController().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.giph.mp4.GiphyMp4Fragment$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                GiphyMp4Adapter.this.setPagingController((PagingController) obj);
            }
        });
        giphyMp4ViewModel.getPagedData().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.giph.mp4.GiphyMp4Fragment$$ExternalSyntheticLambda3
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                GiphyMp4Fragment.$r8$lambda$nOmaoUw2X5U72WmbhdmYz80Zbho(RecyclerView.this, (LivePagedData) obj);
            }
        });
    }

    public static /* synthetic */ void lambda$onViewCreated$0(TextView textView, GiphyMp4Adapter giphyMp4Adapter, ContentLoadingProgressBar contentLoadingProgressBar, MappingModelList mappingModelList) {
        textView.setVisibility(mappingModelList.isEmpty() ? 0 : 4);
        Objects.requireNonNull(contentLoadingProgressBar);
        giphyMp4Adapter.submitList(mappingModelList, new Runnable() { // from class: org.thoughtcrime.securesms.giph.mp4.GiphyMp4Fragment$$ExternalSyntheticLambda4
            @Override // java.lang.Runnable
            public final void run() {
                ContentLoadingProgressBar.this.hide();
            }
        });
    }
}
