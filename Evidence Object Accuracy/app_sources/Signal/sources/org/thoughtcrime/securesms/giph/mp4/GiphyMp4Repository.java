package org.thoughtcrime.securesms.giph.mp4;

import android.net.Uri;
import androidx.core.util.Consumer;
import java.io.IOException;
import java.util.concurrent.Executor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.giph.model.GiphyImage;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4SaveResult;
import org.thoughtcrime.securesms.net.ContentProxySelector;
import org.thoughtcrime.securesms.net.StandardUserAgentInterceptor;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.push.SignalServiceNetworkAccess;
import org.thoughtcrime.securesms.util.MediaUtil;

/* loaded from: classes4.dex */
public final class GiphyMp4Repository {
    private static final Executor EXECUTOR = SignalExecutors.BOUNDED;
    private final OkHttpClient client = new OkHttpClient.Builder().proxySelector(new ContentProxySelector()).addInterceptor(new StandardUserAgentInterceptor()).dns(SignalServiceNetworkAccess.DNS).build();

    public void saveToBlob(GiphyImage giphyImage, boolean z, Consumer<GiphyMp4SaveResult> consumer) {
        EXECUTOR.execute(new Runnable(giphyImage, z, consumer) { // from class: org.thoughtcrime.securesms.giph.mp4.GiphyMp4Repository$$ExternalSyntheticLambda0
            public final /* synthetic */ GiphyImage f$1;
            public final /* synthetic */ boolean f$2;
            public final /* synthetic */ Consumer f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                GiphyMp4Repository.$r8$lambda$4d7rHE47BmS9QGj00w0JmFxbF5A(GiphyMp4Repository.this, this.f$1, this.f$2, this.f$3);
            }
        });
    }

    public /* synthetic */ void lambda$saveToBlob$0(GiphyImage giphyImage, boolean z, Consumer consumer) {
        try {
            consumer.accept(new GiphyMp4SaveResult.Success(saveToBlobInternal(giphyImage, z), giphyImage));
        } catch (IOException e) {
            consumer.accept(new GiphyMp4SaveResult.Error(e));
        }
    }

    private Uri saveToBlobInternal(GiphyImage giphyImage, boolean z) throws IOException {
        String str;
        String str2;
        if (z) {
            str2 = giphyImage.getGifMmsUrl();
            str = MediaUtil.IMAGE_GIF;
        } else {
            str2 = giphyImage.getMp4Url();
            str = "video/mp4";
        }
        Response execute = this.client.newCall(new Request.Builder().url(str2).build()).execute();
        try {
            if (execute.code() < 200 || execute.code() >= 300) {
                throw new IOException("Unexpected response code: " + execute.code());
            }
            Uri createForSingleSessionOnDisk = BlobProvider.getInstance().forData(execute.body().byteStream(), execute.body().contentLength()).withMimeType(str).createForSingleSessionOnDisk(ApplicationDependencies.getApplication());
            execute.close();
            return createForSingleSessionOnDisk;
        } catch (Throwable th) {
            if (execute != null) {
                try {
                    execute.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }
}
