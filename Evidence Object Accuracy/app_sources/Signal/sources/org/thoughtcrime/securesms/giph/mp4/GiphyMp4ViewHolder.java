package org.thoughtcrime.securesms.giph.mp4;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.bumptech.glide.TransitionOptions;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.colors.ChatColorsPalette;
import org.thoughtcrime.securesms.conversation.colors.NameColor;
import org.thoughtcrime.securesms.giph.model.ChunkedImageUrl;
import org.thoughtcrime.securesms.giph.model.GiphyImage;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4Adapter;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.util.Projection;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* loaded from: classes4.dex */
public final class GiphyMp4ViewHolder extends MappingViewHolder<GiphyImage> implements GiphyMp4Playable {
    private static final Projection.Corners CORNERS = new Projection.Corners((float) ViewUtil.dpToPx(8));
    private float aspectRatio;
    private final AspectRatioFrameLayout container;
    private final GiphyMp4Adapter.Callback listener;
    private MediaItem mediaItem;
    private final Drawable placeholder;
    private final ImageView stillImage;

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public boolean canPlayContent() {
        return true;
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public /* synthetic */ GiphyMp4PlaybackPolicyEnforcer getPlaybackPolicyEnforcer() {
        return GiphyMp4Playable.CC.$default$getPlaybackPolicyEnforcer(this);
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public boolean shouldProjectContent() {
        return true;
    }

    public GiphyMp4ViewHolder(View view, GiphyMp4Adapter.Callback callback) {
        super(view);
        AspectRatioFrameLayout aspectRatioFrameLayout = (AspectRatioFrameLayout) view.findViewById(R.id.container);
        this.container = aspectRatioFrameLayout;
        this.listener = callback;
        this.stillImage = (ImageView) view.findViewById(R.id.still_image);
        this.placeholder = new ColorDrawable(((NameColor) Util.getRandomElement(ChatColorsPalette.Names.getAll())).getColor(view.getContext()));
        aspectRatioFrameLayout.setResizeMode(1);
    }

    public void bind(GiphyImage giphyImage) {
        this.aspectRatio = giphyImage.getGifAspectRatio();
        this.mediaItem = MediaItem.fromUri(Uri.parse(giphyImage.getMp4PreviewUrl()));
        this.container.setAspectRatio(this.aspectRatio);
        loadPlaceholderImage(giphyImage);
        this.itemView.setOnClickListener(new View.OnClickListener(giphyImage) { // from class: org.thoughtcrime.securesms.giph.mp4.GiphyMp4ViewHolder$$ExternalSyntheticLambda0
            public final /* synthetic */ GiphyImage f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GiphyMp4ViewHolder.this.lambda$bind$0(this.f$1, view);
            }
        });
    }

    public /* synthetic */ void lambda$bind$0(GiphyImage giphyImage, View view) {
        this.listener.onClick(giphyImage);
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public void showProjectionArea() {
        this.container.setAlpha(1.0f);
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public void hideProjectionArea() {
        this.container.setAlpha(0.0f);
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public MediaItem getMediaItem() {
        return this.mediaItem;
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public Projection getGiphyMp4PlayableProjection(ViewGroup viewGroup) {
        return Projection.relativeToParent(viewGroup, this.container, CORNERS);
    }

    private void loadPlaceholderImage(GiphyImage giphyImage) {
        GlideApp.with(this.itemView).load((Object) new ChunkedImageUrl(giphyImage.getStillUrl())).placeholder(this.placeholder).diskCacheStrategy(DiskCacheStrategy.ALL).transition((TransitionOptions<?, ? super Drawable>) DrawableTransitionOptions.withCrossFade()).into(this.stillImage);
    }
}
