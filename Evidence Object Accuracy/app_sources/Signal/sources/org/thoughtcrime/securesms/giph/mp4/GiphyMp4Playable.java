package org.thoughtcrime.securesms.giph.mp4;

import android.view.ViewGroup;
import com.google.android.exoplayer2.MediaItem;
import org.thoughtcrime.securesms.util.Projection;

/* loaded from: classes4.dex */
public interface GiphyMp4Playable {

    /* renamed from: org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable$-CC */
    /* loaded from: classes4.dex */
    public final /* synthetic */ class CC {
        public static MediaItem $default$getMediaItem(GiphyMp4Playable giphyMp4Playable) {
            return null;
        }

        public static GiphyMp4PlaybackPolicyEnforcer $default$getPlaybackPolicyEnforcer(GiphyMp4Playable giphyMp4Playable) {
            return null;
        }
    }

    boolean canPlayContent();

    int getAdapterPosition();

    Projection getGiphyMp4PlayableProjection(ViewGroup viewGroup);

    MediaItem getMediaItem();

    GiphyMp4PlaybackPolicyEnforcer getPlaybackPolicyEnforcer();

    void hideProjectionArea();

    boolean shouldProjectContent();

    void showProjectionArea();
}
