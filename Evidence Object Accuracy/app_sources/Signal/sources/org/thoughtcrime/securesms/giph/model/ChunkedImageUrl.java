package org.thoughtcrime.securesms.giph.model;

import com.bumptech.glide.load.Key;
import java.security.MessageDigest;
import org.signal.core.util.Conversions;

/* loaded from: classes4.dex */
public class ChunkedImageUrl implements Key {
    public static final long SIZE_UNKNOWN;
    private final long size;
    private final String url;

    public ChunkedImageUrl(String str) {
        this(str, -1);
    }

    public ChunkedImageUrl(String str, long j) {
        if (str != null) {
            this.url = str;
            this.size = j;
            return;
        }
        throw new RuntimeException();
    }

    public String getUrl() {
        return this.url;
    }

    public long getSize() {
        return this.size;
    }

    @Override // com.bumptech.glide.load.Key
    public void updateDiskCacheKey(MessageDigest messageDigest) {
        messageDigest.update(this.url.getBytes());
        messageDigest.update(Conversions.longToByteArray(this.size));
    }

    @Override // com.bumptech.glide.load.Key
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ChunkedImageUrl)) {
            return false;
        }
        ChunkedImageUrl chunkedImageUrl = (ChunkedImageUrl) obj;
        if (!this.url.equals(chunkedImageUrl.url) || this.size != chunkedImageUrl.size) {
            return false;
        }
        return true;
    }

    @Override // com.bumptech.glide.load.Key
    public int hashCode() {
        return this.url.hashCode() ^ ((int) this.size);
    }
}
