package org.thoughtcrime.securesms.giph.mp4;

import android.view.View;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/* loaded from: classes4.dex */
public final class GiphyMp4PlaybackController extends RecyclerView.OnScrollListener implements View.OnLayoutChangeListener {
    private final Callback callback;
    private final int maxSimultaneousPlayback;

    /* loaded from: classes4.dex */
    public interface Callback {
        void update(RecyclerView recyclerView, List<GiphyMp4Playable> list, Set<Integer> set);

        void updateVideoDisplayPositionAndSize(RecyclerView recyclerView, GiphyMp4Playable giphyMp4Playable);
    }

    private GiphyMp4PlaybackController(Callback callback, int i) {
        this.maxSimultaneousPlayback = i;
        this.callback = callback;
    }

    public static void attach(RecyclerView recyclerView, Callback callback, int i) {
        GiphyMp4PlaybackController giphyMp4PlaybackController = new GiphyMp4PlaybackController(callback, i);
        recyclerView.addOnScrollListener(giphyMp4PlaybackController);
        recyclerView.addOnLayoutChangeListener(giphyMp4PlaybackController);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
    public void onScrolled(RecyclerView recyclerView, int i, int i2) {
        performPlaybackUpdate(recyclerView);
    }

    @Override // android.view.View.OnLayoutChangeListener
    public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        performPlaybackUpdate((RecyclerView) view);
    }

    private void performPlaybackUpdate(RecyclerView recyclerView) {
        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager != null) {
            LinkedList linkedList = new LinkedList();
            HashSet hashSet = new HashSet();
            for (int i = 0; i < recyclerView.getChildCount(); i++) {
                RecyclerView.ViewHolder childViewHolder = recyclerView.getChildViewHolder(recyclerView.getChildAt(i));
                if (childViewHolder instanceof GiphyMp4Playable) {
                    GiphyMp4Playable giphyMp4Playable = (GiphyMp4Playable) childViewHolder;
                    linkedList.add(giphyMp4Playable);
                    if (giphyMp4Playable.canPlayContent()) {
                        hashSet.add(Integer.valueOf(giphyMp4Playable.getAdapterPosition()));
                    }
                }
            }
            this.callback.update(recyclerView, linkedList, getPlaybackSetForMaximumDistance(hashSet, findFirstVisibleItemPositions(layoutManager), findLastVisibleItemPositions(layoutManager)));
        }
    }

    private Set<Integer> getPlaybackSetForMaximumDistance(Set<Integer> set, int[] iArr, int[] iArr2) {
        int i = Integer.MAX_VALUE;
        int i2 = Integer.MIN_VALUE;
        for (int i3 = 0; i3 < iArr.length; i3++) {
            i = Math.min(i, iArr[i3]);
            i2 = Math.max(i2, iArr2[i3]);
        }
        return getPlaybackSet(set, i, i2);
    }

    private Set<Integer> getPlaybackSet(Set<Integer> set, int i, int i2) {
        Stream<Integer> sorted = Stream.rangeClosed(i, i2).sorted(new RangeComparator(i, i2));
        Objects.requireNonNull(set);
        return (Set) sorted.filter(new Predicate(set) { // from class: org.thoughtcrime.securesms.giph.mp4.GiphyMp4PlaybackController$$ExternalSyntheticLambda0
            public final /* synthetic */ Set f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return this.f$0.contains((Integer) obj);
            }
        }).limit((long) this.maxSimultaneousPlayback).collect(Collectors.toSet());
    }

    private static int[] findFirstVisibleItemPositions(RecyclerView.LayoutManager layoutManager) {
        if (layoutManager instanceof LinearLayoutManager) {
            return new int[]{((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition()};
        }
        if (layoutManager instanceof StaggeredGridLayoutManager) {
            return ((StaggeredGridLayoutManager) layoutManager).findFirstVisibleItemPositions(null);
        }
        throw new IllegalStateException("Unsupported type: " + layoutManager.getClass().getName());
    }

    private static int[] findLastVisibleItemPositions(RecyclerView.LayoutManager layoutManager) {
        if (layoutManager instanceof LinearLayoutManager) {
            return new int[]{((LinearLayoutManager) layoutManager).findLastVisibleItemPosition()};
        }
        if (layoutManager instanceof StaggeredGridLayoutManager) {
            return ((StaggeredGridLayoutManager) layoutManager).findLastVisibleItemPositions(null);
        }
        throw new IllegalStateException("Unsupported type: " + layoutManager.getClass().getName());
    }

    /* loaded from: classes4.dex */
    public static final class RangeComparator implements Comparator<Integer> {
        private final int center;

        RangeComparator(int i, int i2) {
            this.center = i + ((i2 - i) / 2);
        }

        public int compare(Integer num, Integer num2) {
            int compare = Integer.compare(Math.abs(num.intValue() - this.center), Math.abs(num2.intValue() - this.center));
            return compare == 0 ? Integer.compare(num.intValue(), num2.intValue()) : compare;
        }
    }
}
