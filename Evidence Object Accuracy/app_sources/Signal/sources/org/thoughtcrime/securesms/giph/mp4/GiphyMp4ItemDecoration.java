package org.thoughtcrime.securesms.giph.mp4;

import android.graphics.Canvas;
import android.view.View;
import androidx.core.view.ViewGroupKt;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.sequences.SequencesKt___SequencesJvmKt;
import kotlin.sequences.SequencesKt___SequencesKt;
import org.thoughtcrime.securesms.conversation.ConversationAdapter;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4PlaybackController;

/* compiled from: GiphyMp4ItemDecoration.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0007\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005¢\u0006\u0002\u0010\bJ \u0010\r\u001a\u00020\u00072\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0010\u0010\u0014\u001a\u00020\u00072\u0006\u0010\u0010\u001a\u00020\u0011H\u0002R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u001d\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/giph/mp4/GiphyMp4ItemDecoration;", "Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;", "callback", "Lorg/thoughtcrime/securesms/giph/mp4/GiphyMp4PlaybackController$Callback;", "onRecyclerVerticalTranslationSet", "Lkotlin/Function1;", "", "", "(Lorg/thoughtcrime/securesms/giph/mp4/GiphyMp4PlaybackController$Callback;Lkotlin/jvm/functions/Function1;)V", "getCallback", "()Lorg/thoughtcrime/securesms/giph/mp4/GiphyMp4PlaybackController$Callback;", "getOnRecyclerVerticalTranslationSet", "()Lkotlin/jvm/functions/Function1;", "onDraw", "c", "Landroid/graphics/Canvas;", "parent", "Landroidx/recyclerview/widget/RecyclerView;", "state", "Landroidx/recyclerview/widget/RecyclerView$State;", "setParentRecyclerTranslationY", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class GiphyMp4ItemDecoration extends RecyclerView.ItemDecoration {
    private final GiphyMp4PlaybackController.Callback callback;
    private final Function1<Float, Unit> onRecyclerVerticalTranslationSet;

    public final GiphyMp4PlaybackController.Callback getCallback() {
        return this.callback;
    }

    public final Function1<Float, Unit> getOnRecyclerVerticalTranslationSet() {
        return this.onRecyclerVerticalTranslationSet;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: kotlin.jvm.functions.Function1<? super java.lang.Float, kotlin.Unit> */
    /* JADX WARN: Multi-variable type inference failed */
    public GiphyMp4ItemDecoration(GiphyMp4PlaybackController.Callback callback, Function1<? super Float, Unit> function1) {
        Intrinsics.checkNotNullParameter(callback, "callback");
        Intrinsics.checkNotNullParameter(function1, "onRecyclerVerticalTranslationSet");
        this.callback = callback;
        this.onRecyclerVerticalTranslationSet = function1;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
    public void onDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        Intrinsics.checkNotNullParameter(canvas, "c");
        Intrinsics.checkNotNullParameter(recyclerView, "parent");
        Intrinsics.checkNotNullParameter(state, "state");
        setParentRecyclerTranslationY(recyclerView);
        for (GiphyMp4Playable giphyMp4Playable : SequencesKt___SequencesJvmKt.filterIsInstance(SequencesKt___SequencesKt.map(ViewGroupKt.getChildren(recyclerView), new Function1<View, RecyclerView.ViewHolder>(recyclerView) { // from class: org.thoughtcrime.securesms.giph.mp4.GiphyMp4ItemDecoration$onDraw$1
            final /* synthetic */ RecyclerView $parent;

            /* access modifiers changed from: package-private */
            {
                this.$parent = r1;
            }

            public final RecyclerView.ViewHolder invoke(View view) {
                Intrinsics.checkNotNullParameter(view, "it");
                return this.$parent.getChildViewHolder(view);
            }
        }), GiphyMp4Playable.class)) {
            this.callback.updateVideoDisplayPositionAndSize(recyclerView, giphyMp4Playable);
        }
    }

    private final void setParentRecyclerTranslationY(RecyclerView recyclerView) {
        if (recyclerView.getChildCount() == 0 || recyclerView.canScrollVertically(-1) || recyclerView.canScrollVertically(1)) {
            recyclerView.setTranslationY(0.0f);
            this.onRecyclerVerticalTranslationSet.invoke(Float.valueOf(recyclerView.getTranslationY()));
            return;
        }
        ConversationAdapter.FooterViewHolder footerViewHolder = (ConversationAdapter.FooterViewHolder) SequencesKt___SequencesKt.firstOrNull(SequencesKt___SequencesJvmKt.filterIsInstance(SequencesKt___SequencesKt.map(ViewGroupKt.getChildren(recyclerView), new Function1<View, RecyclerView.ViewHolder>(recyclerView) { // from class: org.thoughtcrime.securesms.giph.mp4.GiphyMp4ItemDecoration$setParentRecyclerTranslationY$footerViewHolder$1
            final /* synthetic */ RecyclerView $parent;

            /* access modifiers changed from: package-private */
            {
                this.$parent = r1;
            }

            public final RecyclerView.ViewHolder invoke(View view) {
                Intrinsics.checkNotNullParameter(view, "it");
                return this.$parent.getChildViewHolder(view);
            }
        }), ConversationAdapter.FooterViewHolder.class));
        if (footerViewHolder == null) {
            recyclerView.setTranslationY(0.0f);
            this.onRecyclerVerticalTranslationSet.invoke(Float.valueOf(recyclerView.getTranslationY()));
            return;
        }
        recyclerView.setTranslationY((float) Math.min(0, -footerViewHolder.itemView.getTop()));
        this.onRecyclerVerticalTranslationSet.invoke(Float.valueOf(recyclerView.getTranslationY()));
    }
}
