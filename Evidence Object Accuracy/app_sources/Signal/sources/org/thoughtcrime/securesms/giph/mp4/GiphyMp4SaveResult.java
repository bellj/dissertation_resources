package org.thoughtcrime.securesms.giph.mp4;

import android.net.Uri;
import org.thoughtcrime.securesms.giph.model.GiphyImage;

/* loaded from: classes4.dex */
public abstract class GiphyMp4SaveResult {
    private GiphyMp4SaveResult() {
    }

    /* loaded from: classes4.dex */
    public static final class Success extends GiphyMp4SaveResult {
        private final Uri blobUri;
        private final int height;
        private final boolean isBorderless;
        private final int width;

        public Success(Uri uri, GiphyImage giphyImage) {
            super();
            this.blobUri = uri;
            this.width = giphyImage.getGifWidth();
            this.height = giphyImage.getGifHeight();
            this.isBorderless = giphyImage.isSticker();
        }

        public int getHeight() {
            return this.height;
        }

        public int getWidth() {
            return this.width;
        }

        public Uri getBlobUri() {
            return this.blobUri;
        }

        public boolean isBorderless() {
            return this.isBorderless;
        }
    }

    /* loaded from: classes4.dex */
    public static final class InProgress extends GiphyMp4SaveResult {
        public InProgress() {
            super();
        }
    }

    /* loaded from: classes4.dex */
    public static final class Error extends GiphyMp4SaveResult {
        private final Exception exception;

        public Error(Exception exc) {
            super();
            this.exception = exc;
        }
    }
}
