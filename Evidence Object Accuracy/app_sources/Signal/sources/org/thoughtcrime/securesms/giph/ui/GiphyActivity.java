package org.thoughtcrime.securesms.giph.ui;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import j$.util.Optional;
import java.util.Collections;
import java.util.Objects;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.MessageSendType;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4Fragment;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4SaveResult;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4ViewModel;
import org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionActivity;
import org.thoughtcrime.securesms.mms.SlideFactory;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;

/* loaded from: classes4.dex */
public class GiphyActivity extends PassphraseRequiredActivity implements KeyboardPageSearchView.Callbacks {
    public static final String EXTRA_IS_MMS;
    public static final String EXTRA_RECIPIENT_ID;
    public static final String EXTRA_TEXT;
    public static final String EXTRA_TRANSPORT;
    private static final int MEDIA_SENDER;
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();
    private GiphyMp4ViewModel giphyMp4ViewModel;
    private AlertDialog progressDialog;
    private RecipientId recipientId;
    private MessageSendType sendType;
    private CharSequence text;

    @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
    public void onClicked() {
    }

    @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
    public void onFocusGained() {
    }

    @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
    public void onFocusLost() {
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onPreCreate() {
        this.dynamicTheme.onCreate(this);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        setContentView(R.layout.giphy_activity);
        boolean booleanExtra = getIntent().getBooleanExtra(EXTRA_IS_MMS, false);
        this.recipientId = (RecipientId) getIntent().getParcelableExtra(EXTRA_RECIPIENT_ID);
        this.sendType = (MessageSendType) getIntent().getParcelableExtra(EXTRA_TRANSPORT);
        this.text = getIntent().getCharSequenceExtra(EXTRA_TEXT);
        GiphyMp4ViewModel giphyMp4ViewModel = (GiphyMp4ViewModel) ViewModelProviders.of(this, new GiphyMp4ViewModel.Factory(booleanExtra)).get(GiphyMp4ViewModel.class);
        this.giphyMp4ViewModel = giphyMp4ViewModel;
        giphyMp4ViewModel.getSaveResultEvents().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.giph.ui.GiphyActivity$$ExternalSyntheticLambda0
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                GiphyActivity.$r8$lambda$u6egQtpbP2PovzhmG5R5d9zVJmU(GiphyActivity.this, (GiphyMp4SaveResult) obj);
            }
        });
        initializeToolbar();
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, GiphyMp4Fragment.create(booleanExtra)).commit();
        ViewUtil.focusAndShowKeyboard(findViewById(R.id.emoji_search_entry));
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 12 && i2 == -1) {
            setResult(-1, intent);
            finish();
            return;
        }
        super.onActivityResult(i, i2, intent);
    }

    private void initializeToolbar() {
        KeyboardPageSearchView keyboardPageSearchView = (KeyboardPageSearchView) findViewById(R.id.giphy_search_text);
        keyboardPageSearchView.setCallbacks(this);
        keyboardPageSearchView.enableBackNavigation(true);
        ViewUtil.focusAndShowKeyboard(keyboardPageSearchView);
    }

    public void handleGiphyMp4SaveResult(GiphyMp4SaveResult giphyMp4SaveResult) {
        if (giphyMp4SaveResult instanceof GiphyMp4SaveResult.Success) {
            hideProgressDialog();
            handleGiphyMp4SuccessfulResult((GiphyMp4SaveResult.Success) giphyMp4SaveResult);
        } else if (giphyMp4SaveResult instanceof GiphyMp4SaveResult.Error) {
            hideProgressDialog();
            handleGiphyMp4ErrorResult((GiphyMp4SaveResult.Error) giphyMp4SaveResult);
        } else {
            this.progressDialog = SimpleProgressDialog.show(this);
        }
    }

    private void hideProgressDialog() {
        AlertDialog alertDialog = this.progressDialog;
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
    }

    private void handleGiphyMp4SuccessfulResult(GiphyMp4SaveResult.Success success) {
        SlideFactory.MediaType from = SlideFactory.MediaType.from(BlobProvider.getMimeType(success.getBlobUri()));
        Objects.requireNonNull(from);
        String mimeType = MediaUtil.getMimeType(this, success.getBlobUri());
        if (mimeType == null) {
            mimeType = from.toFallbackMimeType();
        }
        startActivityForResult(MediaSelectionActivity.editor(this, this.sendType, Collections.singletonList(new Media(success.getBlobUri(), mimeType, 0, success.getWidth(), success.getHeight(), 0, 0, false, true, Optional.empty(), Optional.empty(), Optional.empty())), this.recipientId, this.text), 12);
    }

    private void handleGiphyMp4ErrorResult(GiphyMp4SaveResult.Error error) {
        Toast.makeText(this, (int) R.string.GiphyActivity_error_while_retrieving_full_resolution_gif, 1).show();
    }

    @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
    public void onQueryChanged(String str) {
        this.giphyMp4ViewModel.updateSearchQuery(str);
    }

    @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
    public void onNavigationClicked() {
        ViewUtil.hideKeyboard(this, findViewById(16908290));
        finish();
    }
}
