package org.thoughtcrime.securesms.giph.mp4;

import android.net.Uri;
import android.text.TextUtils;
import j$.util.Optional;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.signal.core.util.logging.Log;
import org.signal.paging.PagedDataSource;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.giph.model.GiphyImage;
import org.thoughtcrime.securesms.giph.model.GiphyResponse;
import org.thoughtcrime.securesms.net.ContentProxySelector;
import org.thoughtcrime.securesms.util.JsonUtils;

/* loaded from: classes4.dex */
public final class GiphyMp4PagedDataSource implements PagedDataSource<String, GiphyImage> {
    private static final Uri BASE_GIPHY_URI;
    private static final Uri SEARCH_URI;
    private static final String TAG = Log.tag(GiphyMp4PagedDataSource.class);
    private static final Uri TRENDING_URI;
    private final OkHttpClient client = ApplicationDependencies.getOkHttpClient().newBuilder().proxySelector(new ContentProxySelector()).build();
    private final String searchString;

    static {
        Uri build = Uri.parse("https://api.giphy.com/v1/gifs/").buildUpon().appendQueryParameter("api_key", BuildConfig.GIPHY_API_KEY).build();
        BASE_GIPHY_URI = build;
        TRENDING_URI = build.buildUpon().appendPath("trending").build();
        SEARCH_URI = build.buildUpon().appendPath("search").build();
        TAG = Log.tag(GiphyMp4PagedDataSource.class);
    }

    public GiphyMp4PagedDataSource(String str) {
        this.searchString = (String) Optional.ofNullable(str).map(new GiphyMp4PagedDataSource$$ExternalSyntheticLambda0()).orElse("");
    }

    @Override // org.signal.paging.PagedDataSource
    public int size() {
        try {
            return performFetch(0, 1).getPagination().getTotalCount();
        } catch (IOException | NullPointerException e) {
            Log.w(TAG, "Failed to get size", e);
            return 0;
        }
    }

    @Override // org.signal.paging.PagedDataSource
    public List<GiphyImage> load(int i, int i2, PagedDataSource.CancellationSignal cancellationSignal) {
        try {
            String str = TAG;
            Log.d(str, "Loading from " + i + " to " + (i + i2));
            return new LinkedList(performFetch(i, i2).getData());
        } catch (IOException | NullPointerException e) {
            Log.w(TAG, "Failed to load content", e);
            return new LinkedList();
        }
    }

    public String getKey(GiphyImage giphyImage) {
        return giphyImage.getGifUrl();
    }

    public GiphyImage load(String str) {
        throw new UnsupportedOperationException("Not implemented!");
    }

    private GiphyResponse performFetch(int i, int i2) throws IOException {
        String str;
        if (TextUtils.isEmpty(this.searchString)) {
            str = getTrendingUrl(i, i2);
        } else {
            str = getSearchUrl(i, i2, this.searchString);
        }
        Response execute = this.client.newCall(new Request.Builder().url(str).build()).execute();
        try {
            if (!execute.isSuccessful()) {
                throw new IOException("Unexpected code " + execute);
            } else if (execute.body() != null) {
                GiphyResponse giphyResponse = (GiphyResponse) JsonUtils.fromJson(execute.body().byteStream(), GiphyResponse.class);
                execute.close();
                return giphyResponse;
            } else {
                throw new IOException("Response body was not present");
            }
        } catch (Throwable th) {
            if (execute != null) {
                try {
                    execute.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    private String getTrendingUrl(int i, int i2) {
        return TRENDING_URI.buildUpon().appendQueryParameter("offset", String.valueOf(i)).appendQueryParameter("limit", String.valueOf(i2)).build().toString();
    }

    private String getSearchUrl(int i, int i2, String str) {
        return SEARCH_URI.buildUpon().appendQueryParameter("offset", String.valueOf(i)).appendQueryParameter("limit", String.valueOf(i2)).appendQueryParameter("q", str).build().toString();
    }
}
