package org.thoughtcrime.securesms.giph.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;

/* loaded from: classes4.dex */
public class AspectRatioImageView extends AppCompatImageView {
    static final int ADJUST_DIMENSION_HEIGHT;
    static final int ADJUST_DIMENSION_WIDTH;
    private static final int DEFAULT_ADJUST_DIMENSION;
    private static final float DEFAULT_ASPECT_RATIO;
    private double aspectRatio;
    private int dimensionToAdjust;

    public AspectRatioImageView(Context context) {
        this(context, null);
    }

    public AspectRatioImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public double getAspectRatio() {
        return this.aspectRatio;
    }

    public int getDimensionToAdjust() {
        return this.dimensionToAdjust;
    }

    public void setAspectRatio(double d) {
        this.aspectRatio = d;
    }

    public void resetSize() {
        if (getMeasuredWidth() != 0 || getMeasuredHeight() != 0) {
            measure(View.MeasureSpec.makeMeasureSpec(0, 1073741824), View.MeasureSpec.makeMeasureSpec(0, 1073741824));
            layout(0, 0, 0, 0);
        }
    }

    @Override // android.widget.ImageView, android.view.View
    protected void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (this.dimensionToAdjust == 0) {
            measuredHeight = calculateHeight(measuredWidth, this.aspectRatio);
        } else {
            measuredWidth = calculateWidth(measuredHeight, this.aspectRatio);
        }
        setMeasuredDimension(measuredWidth, measuredHeight);
    }

    int calculateHeight(int i, double d) {
        if (d == 0.0d) {
            return 0;
        }
        double d2 = (double) i;
        Double.isNaN(d2);
        return (int) Math.round(d2 / d);
    }

    int calculateWidth(int i, double d) {
        double d2 = (double) i;
        Double.isNaN(d2);
        return (int) Math.round(d2 * d);
    }
}
