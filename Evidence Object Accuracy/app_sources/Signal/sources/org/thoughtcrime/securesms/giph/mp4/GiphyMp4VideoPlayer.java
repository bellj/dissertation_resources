package org.thoughtcrime.securesms.giph.mp4;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.TextureView;
import android.view.View;
import android.widget.FrameLayout;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerView;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.CornerMask;
import org.thoughtcrime.securesms.util.Projection;

/* loaded from: classes4.dex */
public final class GiphyMp4VideoPlayer extends FrameLayout implements DefaultLifecycleObserver {
    private static final String TAG = Log.tag(GiphyMp4VideoPlayer.class);
    private CornerMask cornerMask;
    private SimpleExoPlayer exoPlayer;
    private final PlayerView exoView;
    private MediaItem mediaItem;

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onCreate(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onCreate(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onDestroy(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onDestroy(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onPause(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onPause(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onResume(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onResume(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onStart(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onStart(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onStop(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onStop(this, lifecycleOwner);
    }

    public GiphyMp4VideoPlayer(Context context) {
        this(context, null);
    }

    public GiphyMp4VideoPlayer(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public GiphyMp4VideoPlayer(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        FrameLayout.inflate(context, R.layout.gif_player, this);
        this.exoView = (PlayerView) findViewById(R.id.video_view);
    }

    @Override // android.view.View, android.view.ViewGroup
    protected void onDetachedFromWindow() {
        Log.d(TAG, "onDetachedFromWindow");
        super.onDetachedFromWindow();
    }

    @Override // android.view.View, android.view.ViewGroup
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        CornerMask cornerMask = this.cornerMask;
        if (cornerMask != null) {
            cornerMask.mask(canvas);
        }
    }

    public SimpleExoPlayer getExoPlayer() {
        return this.exoPlayer;
    }

    public void setExoPlayer(SimpleExoPlayer simpleExoPlayer) {
        this.exoView.setPlayer(simpleExoPlayer);
        this.exoPlayer = simpleExoPlayer;
    }

    public int getPlaybackState() {
        SimpleExoPlayer simpleExoPlayer = this.exoPlayer;
        if (simpleExoPlayer != null) {
            return simpleExoPlayer.getPlaybackState();
        }
        return -1;
    }

    public void setVideoItem(MediaItem mediaItem) {
        this.mediaItem = mediaItem;
        this.exoPlayer.setMediaItem(mediaItem);
        this.exoPlayer.prepare();
    }

    public void setCorners(Projection.Corners corners) {
        if (corners == null) {
            this.cornerMask = null;
        } else {
            CornerMask cornerMask = new CornerMask(this);
            this.cornerMask = cornerMask;
            cornerMask.setRadii(corners.getTopLeft(), corners.getTopRight(), corners.getBottomRight(), corners.getBottomLeft());
        }
        invalidate();
    }

    public void play() {
        SimpleExoPlayer simpleExoPlayer = this.exoPlayer;
        if (simpleExoPlayer != null) {
            simpleExoPlayer.setPlayWhenReady(true);
        }
    }

    public void pause() {
        SimpleExoPlayer simpleExoPlayer = this.exoPlayer;
        if (simpleExoPlayer != null) {
            simpleExoPlayer.pause();
        }
    }

    public void stop() {
        SimpleExoPlayer simpleExoPlayer = this.exoPlayer;
        if (simpleExoPlayer != null) {
            simpleExoPlayer.stop();
            this.exoPlayer.clearMediaItems();
            this.mediaItem = null;
        }
    }

    public long getDuration() {
        SimpleExoPlayer simpleExoPlayer = this.exoPlayer;
        if (simpleExoPlayer != null) {
            return simpleExoPlayer.getDuration();
        }
        return -1;
    }

    public void setResizeMode(int i) {
        this.exoView.setResizeMode(i);
    }

    public Bitmap getBitmap() {
        View videoSurfaceView = this.exoView.getVideoSurfaceView();
        if (videoSurfaceView instanceof TextureView) {
            return ((TextureView) videoSurfaceView).getBitmap();
        }
        return null;
    }
}
