package org.thoughtcrime.securesms.giph.mp4;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.MediaMetadata;
import com.google.android.exoplayer2.PlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.device.DeviceInfo;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.video.VideoListener;
import com.google.android.exoplayer2.video.VideoSize;
import java.util.ArrayList;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.util.Projection;
import org.thoughtcrime.securesms.video.exo.ExoPlayerKt;
import org.thoughtcrime.securesms.video.exo.SimpleExoPlayerPool;

/* loaded from: classes4.dex */
public final class GiphyMp4ProjectionPlayerHolder implements Player.Listener, DefaultLifecycleObserver {
    private static final String TAG = Log.tag(GiphyMp4ProjectionPlayerHolder.class);
    private final FrameLayout container;
    private MediaItem mediaItem;
    private Runnable onPlaybackReady;
    private final GiphyMp4VideoPlayer player;
    private GiphyMp4PlaybackPolicyEnforcer policyEnforcer;

    @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.audio.AudioListener
    public /* bridge */ /* synthetic */ void onAudioAttributesChanged(AudioAttributes audioAttributes) {
        Player.Listener.CC.$default$onAudioAttributesChanged(this, audioAttributes);
    }

    public /* bridge */ /* synthetic */ void onAudioSessionIdChanged(int i) {
        Player.Listener.CC.$default$onAudioSessionIdChanged(this, i);
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    public /* bridge */ /* synthetic */ void onAvailableCommandsChanged(Player.Commands commands) {
        Player.Listener.CC.$default$onAvailableCommandsChanged(this, commands);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onCreate(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onCreate(this, lifecycleOwner);
    }

    @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.text.TextOutput
    public /* bridge */ /* synthetic */ void onCues(List list) {
        Player.Listener.CC.$default$onCues(this, list);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onDestroy(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onDestroy(this, lifecycleOwner);
    }

    @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.device.DeviceListener
    public /* bridge */ /* synthetic */ void onDeviceInfoChanged(DeviceInfo deviceInfo) {
        Player.Listener.CC.$default$onDeviceInfoChanged(this, deviceInfo);
    }

    @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.device.DeviceListener
    public /* bridge */ /* synthetic */ void onDeviceVolumeChanged(int i, boolean z) {
        Player.Listener.CC.$default$onDeviceVolumeChanged(this, i, z);
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    public /* bridge */ /* synthetic */ void onEvents(Player player, Player.Events events) {
        Player.Listener.CC.$default$onEvents(this, player, events);
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    public /* bridge */ /* synthetic */ void onIsLoadingChanged(boolean z) {
        Player.Listener.CC.$default$onIsLoadingChanged(this, z);
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    public /* bridge */ /* synthetic */ void onIsPlayingChanged(boolean z) {
        Player.Listener.CC.$default$onIsPlayingChanged(this, z);
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    @Deprecated
    public /* bridge */ /* synthetic */ void onLoadingChanged(boolean z) {
        Player.EventListener.CC.$default$onLoadingChanged(this, z);
    }

    public /* bridge */ /* synthetic */ void onMaxSeekToPreviousPositionChanged(int i) {
        Player.EventListener.CC.$default$onMaxSeekToPreviousPositionChanged(this, i);
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    public /* bridge */ /* synthetic */ void onMediaItemTransition(MediaItem mediaItem, int i) {
        Player.Listener.CC.$default$onMediaItemTransition(this, mediaItem, i);
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    public /* bridge */ /* synthetic */ void onMediaMetadataChanged(MediaMetadata mediaMetadata) {
        Player.Listener.CC.$default$onMediaMetadataChanged(this, mediaMetadata);
    }

    @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.metadata.MetadataOutput
    public /* bridge */ /* synthetic */ void onMetadata(Metadata metadata) {
        Player.Listener.CC.$default$onMetadata(this, metadata);
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    public /* bridge */ /* synthetic */ void onPlayWhenReadyChanged(boolean z, int i) {
        Player.Listener.CC.$default$onPlayWhenReadyChanged(this, z, i);
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    public /* bridge */ /* synthetic */ void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
        Player.Listener.CC.$default$onPlaybackParametersChanged(this, playbackParameters);
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    public /* bridge */ /* synthetic */ void onPlaybackSuppressionReasonChanged(int i) {
        Player.Listener.CC.$default$onPlaybackSuppressionReasonChanged(this, i);
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    public /* bridge */ /* synthetic */ void onPlayerError(PlaybackException playbackException) {
        Player.Listener.CC.$default$onPlayerError(this, playbackException);
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    public /* bridge */ /* synthetic */ void onPlayerErrorChanged(PlaybackException playbackException) {
        Player.Listener.CC.$default$onPlayerErrorChanged(this, playbackException);
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    @Deprecated
    public /* bridge */ /* synthetic */ void onPlayerStateChanged(boolean z, int i) {
        Player.EventListener.CC.$default$onPlayerStateChanged(this, z, i);
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    public /* bridge */ /* synthetic */ void onPlaylistMetadataChanged(MediaMetadata mediaMetadata) {
        Player.Listener.CC.$default$onPlaylistMetadataChanged(this, mediaMetadata);
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    @Deprecated
    public /* bridge */ /* synthetic */ void onPositionDiscontinuity(int i) {
        Player.EventListener.CC.$default$onPositionDiscontinuity(this, i);
    }

    @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.video.VideoListener
    public /* bridge */ /* synthetic */ void onRenderedFirstFrame() {
        Player.Listener.CC.$default$onRenderedFirstFrame(this);
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    public /* bridge */ /* synthetic */ void onRepeatModeChanged(int i) {
        Player.Listener.CC.$default$onRepeatModeChanged(this, i);
    }

    public /* bridge */ /* synthetic */ void onSeekBackIncrementChanged(long j) {
        Player.Listener.CC.$default$onSeekBackIncrementChanged(this, j);
    }

    public /* bridge */ /* synthetic */ void onSeekForwardIncrementChanged(long j) {
        Player.Listener.CC.$default$onSeekForwardIncrementChanged(this, j);
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    @Deprecated
    public /* bridge */ /* synthetic */ void onSeekProcessed() {
        Player.EventListener.CC.$default$onSeekProcessed(this);
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    public /* bridge */ /* synthetic */ void onShuffleModeEnabledChanged(boolean z) {
        Player.Listener.CC.$default$onShuffleModeEnabledChanged(this, z);
    }

    @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.audio.AudioListener
    public /* bridge */ /* synthetic */ void onSkipSilenceEnabledChanged(boolean z) {
        Player.Listener.CC.$default$onSkipSilenceEnabledChanged(this, z);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onStart(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onStart(this, lifecycleOwner);
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    @Deprecated
    public /* bridge */ /* synthetic */ void onStaticMetadataChanged(List list) {
        Player.EventListener.CC.$default$onStaticMetadataChanged(this, list);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onStop(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onStop(this, lifecycleOwner);
    }

    @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.video.VideoListener
    public /* bridge */ /* synthetic */ void onSurfaceSizeChanged(int i, int i2) {
        Player.Listener.CC.$default$onSurfaceSizeChanged(this, i, i2);
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    public /* bridge */ /* synthetic */ void onTimelineChanged(Timeline timeline, int i) {
        Player.Listener.CC.$default$onTimelineChanged(this, timeline, i);
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    public /* bridge */ /* synthetic */ void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
        Player.Listener.CC.$default$onTracksChanged(this, trackGroupArray, trackSelectionArray);
    }

    @Override // com.google.android.exoplayer2.video.VideoListener
    @Deprecated
    public /* bridge */ /* synthetic */ void onVideoSizeChanged(int i, int i2, int i3, float f) {
        VideoListener.CC.$default$onVideoSizeChanged(this, i, i2, i3, f);
    }

    @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.video.VideoListener
    public /* bridge */ /* synthetic */ void onVideoSizeChanged(VideoSize videoSize) {
        Player.Listener.CC.$default$onVideoSizeChanged(this, videoSize);
    }

    @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.audio.AudioListener
    public /* bridge */ /* synthetic */ void onVolumeChanged(float f) {
        Player.Listener.CC.$default$onVolumeChanged(this, f);
    }

    private GiphyMp4ProjectionPlayerHolder(FrameLayout frameLayout, GiphyMp4VideoPlayer giphyMp4VideoPlayer) {
        this.container = frameLayout;
        this.player = giphyMp4VideoPlayer;
    }

    public FrameLayout getContainer() {
        return this.container;
    }

    public void playContent(MediaItem mediaItem, GiphyMp4PlaybackPolicyEnforcer giphyMp4PlaybackPolicyEnforcer) {
        this.mediaItem = mediaItem;
        this.policyEnforcer = giphyMp4PlaybackPolicyEnforcer;
        if (this.player.getExoPlayer() == null) {
            SimpleExoPlayerPool exoPlayerPool = ApplicationDependencies.getExoPlayerPool();
            String str = TAG;
            SimpleExoPlayer simpleExoPlayer = exoPlayerPool.get(str);
            if (simpleExoPlayer == null) {
                Log.i(str, "Could not get exoplayer from pool.");
                return;
            }
            ExoPlayerKt.configureForGifPlayback(simpleExoPlayer);
            simpleExoPlayer.addListener((Player.Listener) this);
            this.player.setExoPlayer(simpleExoPlayer);
        }
        this.player.setVideoItem(mediaItem);
        this.player.play();
    }

    public void clearMedia() {
        this.mediaItem = null;
        this.policyEnforcer = null;
        SimpleExoPlayer exoPlayer = this.player.getExoPlayer();
        if (exoPlayer != null) {
            this.player.stop();
            this.player.setExoPlayer(null);
            exoPlayer.removeListener((Player.Listener) this);
            ApplicationDependencies.getExoPlayerPool().pool(exoPlayer);
        }
    }

    public MediaItem getMediaItem() {
        return this.mediaItem;
    }

    public void setOnPlaybackReady(Runnable runnable) {
        this.onPlaybackReady = runnable;
        if (runnable != null && this.player.getPlaybackState() == 3) {
            runnable.run();
        }
    }

    public void hide() {
        this.container.setVisibility(8);
    }

    public boolean isVisible() {
        return this.container.getVisibility() == 0;
    }

    public void pause() {
        this.player.pause();
    }

    public void show() {
        this.container.setVisibility(0);
    }

    public void resume() {
        this.player.play();
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    public void onPlaybackStateChanged(int i) {
        if (i == 3 && this.onPlaybackReady != null) {
            GiphyMp4PlaybackPolicyEnforcer giphyMp4PlaybackPolicyEnforcer = this.policyEnforcer;
            if (giphyMp4PlaybackPolicyEnforcer != null) {
                giphyMp4PlaybackPolicyEnforcer.setMediaDuration(this.player.getDuration());
            }
            this.onPlaybackReady.run();
        }
    }

    @Override // com.google.android.exoplayer2.Player.EventListener
    public void onPositionDiscontinuity(Player.PositionInfo positionInfo, Player.PositionInfo positionInfo2, int i) {
        GiphyMp4PlaybackPolicyEnforcer giphyMp4PlaybackPolicyEnforcer = this.policyEnforcer;
        if (giphyMp4PlaybackPolicyEnforcer != null && i == 0 && giphyMp4PlaybackPolicyEnforcer.endPlayback()) {
            this.player.stop();
        }
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public void onResume(LifecycleOwner lifecycleOwner) {
        SimpleExoPlayer simpleExoPlayer;
        if (this.mediaItem != null && (simpleExoPlayer = ApplicationDependencies.getExoPlayerPool().get(TAG)) != null) {
            ExoPlayerKt.configureForGifPlayback(simpleExoPlayer);
            simpleExoPlayer.addListener((Player.Listener) this);
            this.player.setExoPlayer(simpleExoPlayer);
            this.player.setVideoItem(this.mediaItem);
            this.player.play();
        }
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public void onPause(LifecycleOwner lifecycleOwner) {
        if (this.player.getExoPlayer() != null) {
            this.player.getExoPlayer().stop();
            this.player.getExoPlayer().clearMediaItems();
            this.player.getExoPlayer().removeListener((Player.Listener) this);
            ApplicationDependencies.getExoPlayerPool().pool(this.player.getExoPlayer());
            this.player.setExoPlayer(null);
        }
    }

    public static List<GiphyMp4ProjectionPlayerHolder> injectVideoViews(Context context, Lifecycle lifecycle, ViewGroup viewGroup, int i) {
        ArrayList arrayList = new ArrayList(i);
        for (int i2 = 0; i2 < i; i2++) {
            FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(context).inflate(R.layout.giphy_mp4_player, viewGroup, false);
            GiphyMp4VideoPlayer giphyMp4VideoPlayer = (GiphyMp4VideoPlayer) frameLayout.findViewById(R.id.video_player);
            GiphyMp4ProjectionPlayerHolder giphyMp4ProjectionPlayerHolder = new GiphyMp4ProjectionPlayerHolder(frameLayout, giphyMp4VideoPlayer);
            lifecycle.addObserver(giphyMp4ProjectionPlayerHolder);
            giphyMp4VideoPlayer.setResizeMode(3);
            arrayList.add(giphyMp4ProjectionPlayerHolder);
            viewGroup.addView(frameLayout);
        }
        return arrayList;
    }

    public void setCorners(Projection.Corners corners) {
        this.player.setCorners(corners);
    }

    public Bitmap getBitmap() {
        return this.player.getBitmap();
    }
}
