package org.thoughtcrime.securesms.giph.mp4;

import androidx.recyclerview.widget.RecyclerView;

/* loaded from: classes4.dex */
public interface GiphyMp4DisplayUpdater {
    void updateDisplay(RecyclerView recyclerView, GiphyMp4Playable giphyMp4Playable);
}
