package org.thoughtcrime.securesms.giph.mp4;

/* loaded from: classes4.dex */
public final class GiphyMp4PlaybackPolicyEnforcer {
    private final Callback callback;
    private long loopsRemaining;
    private final long maxDurationOfSinglePlayback;
    private final long maxRepeatsOfSinglePlayback;

    /* loaded from: classes4.dex */
    public interface Callback {
        void onPlaybackWillEnd();
    }

    public GiphyMp4PlaybackPolicyEnforcer(Callback callback) {
        this(callback, GiphyMp4PlaybackPolicy.maxDurationOfSinglePlayback(), (long) GiphyMp4PlaybackPolicy.maxRepeatsOfSinglePlayback());
    }

    GiphyMp4PlaybackPolicyEnforcer(Callback callback, long j, long j2) {
        this.loopsRemaining = -1;
        this.callback = callback;
        this.maxDurationOfSinglePlayback = j;
        this.maxRepeatsOfSinglePlayback = j2;
    }

    public void setMediaDuration(long j) {
        this.loopsRemaining = Math.min(Math.max(1L, this.maxDurationOfSinglePlayback / j), this.maxRepeatsOfSinglePlayback);
    }

    public boolean endPlayback() {
        long j = this.loopsRemaining;
        if (j < 0) {
            throw new IllegalStateException("Must call setMediaDuration before calling this method.");
        } else if (j == 0) {
            return true;
        } else {
            long j2 = j - 1;
            this.loopsRemaining = j2;
            if (j2 != 0) {
                return false;
            }
            this.callback.onPlaybackWillEnd();
            return true;
        }
    }
}
