package org.thoughtcrime.securesms.conversationlist;

import android.view.View;
import org.thoughtcrime.securesms.conversationlist.ConversationListSearchAdapter;
import org.thoughtcrime.securesms.search.MessageResult;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationListSearchAdapter$SearchResultViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ConversationListSearchAdapter.EventListener f$0;
    public final /* synthetic */ MessageResult f$1;

    public /* synthetic */ ConversationListSearchAdapter$SearchResultViewHolder$$ExternalSyntheticLambda0(ConversationListSearchAdapter.EventListener eventListener, MessageResult messageResult) {
        this.f$0 = eventListener;
        this.f$1 = messageResult;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.onMessageClicked(this.f$1);
    }
}
