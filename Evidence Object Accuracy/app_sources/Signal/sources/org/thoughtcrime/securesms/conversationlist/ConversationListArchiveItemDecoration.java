package org.thoughtcrime.securesms.conversationlist;

import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: ConversationListArchiveItemDecoration.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\n\u001a\u00020\u000bJ \u0010\f\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/conversationlist/ConversationListArchiveItemDecoration;", "Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;", "background", "Landroid/graphics/drawable/Drawable;", "(Landroid/graphics/drawable/Drawable;)V", "archiveAnimationStarted", "", "archiveTriggered", "getBackground", "()Landroid/graphics/drawable/Drawable;", "onArchiveStarted", "", "onDraw", "canvas", "Landroid/graphics/Canvas;", "parent", "Landroidx/recyclerview/widget/RecyclerView;", "state", "Landroidx/recyclerview/widget/RecyclerView$State;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ConversationListArchiveItemDecoration extends RecyclerView.ItemDecoration {
    private boolean archiveAnimationStarted;
    private boolean archiveTriggered;
    private final Drawable background;

    public ConversationListArchiveItemDecoration(Drawable drawable) {
        Intrinsics.checkNotNullParameter(drawable, "background");
        this.background = drawable;
    }

    public final Drawable getBackground() {
        return this.background;
    }

    /* JADX WARNING: Removed duplicated region for block: B:46:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00c0  */
    @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDraw(android.graphics.Canvas r9, androidx.recyclerview.widget.RecyclerView r10, androidx.recyclerview.widget.RecyclerView.State r11) {
        /*
            r8 = this;
            java.lang.String r0 = "canvas"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r9, r0)
            java.lang.String r0 = "parent"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r10, r0)
            java.lang.String r0 = "state"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r11, r0)
            boolean r11 = r8.archiveTriggered
            if (r11 != 0) goto L_0x0014
            return
        L_0x0014:
            boolean r11 = r10.isAnimating()
            r0 = 0
            if (r11 == 0) goto L_0x001f
            r11 = 1
            r8.archiveAnimationStarted = r11
            goto L_0x0028
        L_0x001f:
            boolean r11 = r8.archiveAnimationStarted
            if (r11 == 0) goto L_0x0028
            r8.archiveTriggered = r0
            r8.archiveAnimationStarted = r0
            return
        L_0x0028:
            androidx.recyclerview.widget.RecyclerView$LayoutManager r11 = r10.getLayoutManager()
            if (r11 == 0) goto L_0x0033
            int r11 = r11.getChildCount()
            goto L_0x0034
        L_0x0033:
            r11 = 0
        L_0x0034:
            r1 = 0
            r3 = r1
            r4 = r3
            r2 = 0
        L_0x0038:
            if (r2 >= r11) goto L_0x006b
            androidx.recyclerview.widget.RecyclerView$LayoutManager r5 = r10.getLayoutManager()
            if (r5 == 0) goto L_0x0045
            android.view.View r5 = r5.getChildAt(r2)
            goto L_0x0046
        L_0x0045:
            r5 = r1
        L_0x0046:
            if (r5 == 0) goto L_0x004d
            androidx.recyclerview.widget.RecyclerView$ViewHolder r6 = r10.getChildViewHolder(r5)
            goto L_0x004e
        L_0x004d:
            r6 = r1
        L_0x004e:
            if (r5 == 0) goto L_0x0068
            if (r6 == 0) goto L_0x0068
            float r6 = r5.getTranslationY()
            r7 = 0
            int r6 = (r6 > r7 ? 1 : (r6 == r7 ? 0 : -1))
            if (r6 >= 0) goto L_0x005d
            r3 = r5
            goto L_0x0068
        L_0x005d:
            float r6 = r5.getTranslationY()
            int r6 = (r6 > r7 ? 1 : (r6 == r7 ? 0 : -1))
            if (r6 <= 0) goto L_0x0068
            if (r4 != 0) goto L_0x0068
            r4 = r5
        L_0x0068:
            int r2 = r2 + 1
            goto L_0x0038
        L_0x006b:
            if (r3 == 0) goto L_0x0084
            if (r4 == 0) goto L_0x0084
            int r11 = r3.getBottom()
            float r1 = r3.getTranslationY()
            int r1 = (int) r1
            int r11 = r11 + r1
            int r1 = r4.getTop()
            float r2 = r4.getTranslationY()
        L_0x0081:
            int r2 = (int) r2
            int r1 = r1 + r2
            goto L_0x00a6
        L_0x0084:
            if (r3 == 0) goto L_0x0095
            int r11 = r3.getBottom()
            float r1 = r3.getTranslationY()
            int r1 = (int) r1
            int r11 = r11 + r1
            int r1 = r3.getBottom()
            goto L_0x00a6
        L_0x0095:
            if (r4 == 0) goto L_0x00a4
            int r11 = r4.getTop()
            int r1 = r4.getTop()
            float r2 = r4.getTranslationY()
            goto L_0x0081
        L_0x00a4:
            r11 = 0
            r1 = 0
        L_0x00a6:
            int r2 = r1 - r11
            if (r4 == 0) goto L_0x00af
            int r3 = r4.getHeight()
            goto L_0x00b7
        L_0x00af:
            if (r3 == 0) goto L_0x00b6
            int r3 = r3.getHeight()
            goto L_0x00b7
        L_0x00b6:
            r3 = 0
        L_0x00b7:
            int r3 = r3 * 2
            if (r2 <= r3) goto L_0x00c0
            r8.archiveTriggered = r0
            r8.archiveAnimationStarted = r0
            return
        L_0x00c0:
            android.graphics.drawable.Drawable r2 = r8.background
            int r10 = r10.getWidth()
            r2.setBounds(r0, r11, r10, r1)
            android.graphics.drawable.Drawable r10 = r8.background
            r10.draw(r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversationlist.ConversationListArchiveItemDecoration.onDraw(android.graphics.Canvas, androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$State):void");
    }

    public final void onArchiveStarted() {
        this.archiveTriggered = true;
    }
}
