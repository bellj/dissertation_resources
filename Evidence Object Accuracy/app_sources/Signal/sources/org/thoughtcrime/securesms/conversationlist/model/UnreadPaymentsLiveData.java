package org.thoughtcrime.securesms.conversationlist.model;

import androidx.lifecycle.LiveData;
import j$.util.Optional;
import java.util.List;
import java.util.concurrent.Executor;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.PaymentDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.concurrent.SerialMonoLifoExecutor;

/* loaded from: classes4.dex */
public final class UnreadPaymentsLiveData extends LiveData<Optional<UnreadPayments>> {
    private final Executor executor = new SerialMonoLifoExecutor(SignalExecutors.BOUNDED);
    private final DatabaseObserver.Observer observer = new DatabaseObserver.Observer() { // from class: org.thoughtcrime.securesms.conversationlist.model.UnreadPaymentsLiveData$$ExternalSyntheticLambda0
        @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
        public final void onChanged() {
            UnreadPaymentsLiveData.this.refreshUnreadPayments();
        }
    };
    private final PaymentDatabase paymentDatabase = SignalDatabase.payments();

    @Override // androidx.lifecycle.LiveData
    public void onActive() {
        refreshUnreadPayments();
        ApplicationDependencies.getDatabaseObserver().registerAllPaymentsObserver(this.observer);
    }

    @Override // androidx.lifecycle.LiveData
    public void onInactive() {
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(this.observer);
    }

    public /* synthetic */ void lambda$refreshUnreadPayments$0() {
        postValue(Optional.ofNullable(getUnreadPayments()));
    }

    public void refreshUnreadPayments() {
        this.executor.execute(new Runnable() { // from class: org.thoughtcrime.securesms.conversationlist.model.UnreadPaymentsLiveData$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                UnreadPaymentsLiveData.this.lambda$refreshUnreadPayments$0();
            }
        });
    }

    private UnreadPayments getUnreadPayments() {
        List<PaymentDatabase.PaymentTransaction> unseenPayments = this.paymentDatabase.getUnseenPayments();
        int size = unseenPayments.size();
        Recipient recipient = null;
        if (size == 0) {
            return null;
        }
        if (size != 1) {
            return UnreadPayments.forMultiple(size);
        }
        PaymentDatabase.PaymentTransaction paymentTransaction = unseenPayments.get(0);
        if (paymentTransaction.getPayee().hasRecipientId()) {
            recipient = Recipient.resolved(paymentTransaction.getPayee().requireRecipientId());
        }
        return UnreadPayments.forSingle(recipient, paymentTransaction.getUuid(), paymentTransaction.getAmount());
    }
}
