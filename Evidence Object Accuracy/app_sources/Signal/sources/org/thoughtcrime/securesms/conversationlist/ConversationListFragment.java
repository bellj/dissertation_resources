package org.thoughtcrime.securesms.conversationlist;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.widget.TooltipCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.airbnb.lottie.SimpleColorFilter;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.google.android.material.animation.ArgbEvaluatorCompat;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Predicate;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.core.util.DimensionUnit;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.MainFragment;
import org.thoughtcrime.securesms.MainNavigator;
import org.thoughtcrime.securesms.MuteDialog;
import org.thoughtcrime.securesms.NewConversationActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.badges.self.expired.CantProcessSubscriptionPaymentBottomSheetDialogFragment;
import org.thoughtcrime.securesms.badges.self.expired.ExpiredBadgeBottomSheetDialogFragment;
import org.thoughtcrime.securesms.components.Material3SearchToolbar;
import org.thoughtcrime.securesms.components.RatingManager;
import org.thoughtcrime.securesms.components.UnreadPaymentsView;
import org.thoughtcrime.securesms.components.menu.ActionItem;
import org.thoughtcrime.securesms.components.menu.SignalBottomActionBar;
import org.thoughtcrime.securesms.components.menu.SignalContextMenu;
import org.thoughtcrime.securesms.components.registration.PulsingFloatingActionButton;
import org.thoughtcrime.securesms.components.reminder.DozeReminder;
import org.thoughtcrime.securesms.components.reminder.ExpiredBuildReminder;
import org.thoughtcrime.securesms.components.reminder.OutdatedBuildReminder;
import org.thoughtcrime.securesms.components.reminder.PushRegistrationReminder;
import org.thoughtcrime.securesms.components.reminder.Reminder;
import org.thoughtcrime.securesms.components.reminder.ReminderView;
import org.thoughtcrime.securesms.components.reminder.ServiceOutageReminder;
import org.thoughtcrime.securesms.components.reminder.UnauthorizedReminder;
import org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.UnexpectedSubscriptionCancellation;
import org.thoughtcrime.securesms.components.voice.VoiceNoteMediaControllerOwner;
import org.thoughtcrime.securesms.components.voice.VoiceNotePlayerView;
import org.thoughtcrime.securesms.conversation.ConversationFragment;
import org.thoughtcrime.securesms.conversationlist.ConversationListAdapter;
import org.thoughtcrime.securesms.conversationlist.ConversationListSearchAdapter;
import org.thoughtcrime.securesms.conversationlist.ConversationListViewModel;
import org.thoughtcrime.securesms.conversationlist.model.Conversation;
import org.thoughtcrime.securesms.conversationlist.model.ConversationSet;
import org.thoughtcrime.securesms.conversationlist.model.UnreadPayments;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.database.model.ThreadRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.events.ReminderUpdateEvent;
import org.thoughtcrime.securesms.insights.InsightsLauncher;
import org.thoughtcrime.securesms.jobs.ServiceOutageDetectionJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.main.Material3OnScrollHelperBinder;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionActivity;
import org.thoughtcrime.securesms.megaphone.Megaphone;
import org.thoughtcrime.securesms.megaphone.MegaphoneActionController;
import org.thoughtcrime.securesms.megaphone.MegaphoneViewBuilder;
import org.thoughtcrime.securesms.megaphone.Megaphones;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.notifications.MarkReadReceiver;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.payments.preferences.PaymentsActivity;
import org.thoughtcrime.securesms.payments.preferences.details.PaymentDetailsFragmentArgs;
import org.thoughtcrime.securesms.payments.preferences.details.PaymentDetailsParcelable;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.ratelimit.RecaptchaProofBottomSheetFragment;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.search.MessageResult;
import org.thoughtcrime.securesms.search.SearchResult;
import org.thoughtcrime.securesms.service.KeyCachingService;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.stories.tabs.ConversationListTab;
import org.thoughtcrime.securesms.stories.tabs.ConversationListTabsViewModel;
import org.thoughtcrime.securesms.util.AppForegroundObserver;
import org.thoughtcrime.securesms.util.AppStartup;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.ConversationUtil;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.MessageRecordUtil;
import org.thoughtcrime.securesms.util.PlayStoreUtil;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.SignalLocalMetrics;
import org.thoughtcrime.securesms.util.SignalProxyUtil;
import org.thoughtcrime.securesms.util.SnapToTopDataObserver;
import org.thoughtcrime.securesms.util.StickyHeaderDecoration;
import org.thoughtcrime.securesms.util.Stopwatch;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.WindowUtil;
import org.thoughtcrime.securesms.util.task.SnackbarAsyncTask;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;
import org.thoughtcrime.securesms.util.views.Stub;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;
import org.whispersystems.signalservice.api.websocket.WebSocketConnectionState;

/* loaded from: classes.dex */
public class ConversationListFragment extends MainFragment implements ActionMode.Callback, ConversationListAdapter.OnConversationClickListener, ConversationListSearchAdapter.EventListener, MegaphoneActionController {
    private static final int LIST_SMOOTH_SCROLL_TO_TOP_THRESHOLD;
    private static final int MAXIMUM_PINNED_CONVERSATIONS;
    public static final short MESSAGE_REQUESTS_REQUEST_CODE_CREATE_NAME;
    public static final short SMS_ROLE_REQUEST_CODE;
    private static final String TAG = Log.tag(ConversationListFragment.class);
    private ActionMode actionMode;
    private RecyclerView.Adapter activeAdapter;
    private SignalContextMenu activeContextMenu;
    private AppForegroundObserver.Listener appForegroundObserver;
    protected ConversationListArchiveItemDecoration archiveDecoration;
    private Drawable archiveDrawable;
    private SignalBottomActionBar bottomActionBar;
    private PulsingFloatingActionButton cameraFab;
    private ConversationListTabsViewModel conversationListTabsViewModel;
    private View coordinator;
    private ConversationListAdapter defaultAdapter;
    private Stub<ViewGroup> emptyState;
    private PulsingFloatingActionButton fab;
    protected ConversationListItemAnimator itemAnimator;
    private LifecycleDisposable lifecycleDisposable;
    private RecyclerView list;
    private VoiceNoteMediaControllerOwner mediaControllerOwner;
    private Stub<ViewGroup> megaphoneContainer;
    private Stub<UnreadPaymentsView> paymentNotificationView;
    private Stub<ReminderView> reminderView;
    private ConversationListSearchAdapter searchAdapter;
    private StickyHeaderDecoration searchAdapterDecoration;
    private TextView searchEmptyState;
    private SnapToTopDataObserver snapToTopDataObserver;
    private Stopwatch startupStopwatch;
    private ConversationListViewModel viewModel;
    private VoiceNotePlayerView voiceNotePlayerView;
    private Stub<FrameLayout> voiceNotePlayerViewStub;

    /* loaded from: classes4.dex */
    public interface Callback extends Material3OnScrollHelperBinder {
        Stub<Toolbar> getBasicToolbar();

        ImageView getSearchAction();

        Stub<Material3SearchToolbar> getSearchToolbar();

        Toolbar getToolbar();

        View getUnreadPaymentsDot();

        void onMultiSelectFinished();

        void onMultiSelectStarted();

        void onSearchClosed();

        void onSearchOpened();

        void updateNotificationProfileStatus(List<NotificationProfile> list);

        void updateProxyStatus(WebSocketConnectionState webSocketConnectionState);
    }

    protected int getArchiveIconRes() {
        return R.drawable.ic_archive_24;
    }

    protected int getArchivedSnackbarTitleRes() {
        return R.plurals.ConversationListFragment_conversations_archived;
    }

    protected boolean isArchived() {
        return false;
    }

    @Override // androidx.appcompat.view.ActionMode.Callback
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        return true;
    }

    public static ConversationListFragment newInstance() {
        return new ConversationListFragment();
    }

    @Override // org.thoughtcrime.securesms.MainFragment, androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof VoiceNoteMediaControllerOwner) {
            this.mediaControllerOwner = (VoiceNoteMediaControllerOwner) context;
            return;
        }
        throw new ClassCastException("Expected context to be a Listener");
    }

    @Override // org.thoughtcrime.securesms.LoggingFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setHasOptionsMenu(true);
        this.startupStopwatch = new Stopwatch("startup");
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.conversation_list_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.coordinator = view.findViewById(R.id.coordinator);
        this.list = (RecyclerView) view.findViewById(R.id.list);
        this.searchEmptyState = (TextView) view.findViewById(R.id.search_no_results);
        this.bottomActionBar = (SignalBottomActionBar) view.findViewById(R.id.conversation_list_bottom_action_bar);
        this.reminderView = new Stub<>((ViewStub) view.findViewById(R.id.reminder));
        this.emptyState = new Stub<>((ViewStub) view.findViewById(R.id.empty_state));
        this.megaphoneContainer = new Stub<>((ViewStub) view.findViewById(R.id.megaphone_container));
        this.paymentNotificationView = new Stub<>((ViewStub) view.findViewById(R.id.payments_notification));
        this.voiceNotePlayerViewStub = new Stub<>((ViewStub) view.findViewById(R.id.voice_note_player));
        this.fab = (PulsingFloatingActionButton) view.findViewById(R.id.fab);
        this.cameraFab = (PulsingFloatingActionButton) view.findViewById(R.id.camera_fab);
        this.fab.setVisibility(0);
        this.cameraFab.setVisibility(0);
        this.fab.show();
        this.cameraFab.show();
        this.archiveDecoration = new ConversationListArchiveItemDecoration(new ColorDrawable(getResources().getColor(R.color.conversation_list_archive_background_end)));
        this.itemAnimator = new ConversationListItemAnimator();
        this.list.setLayoutManager(new LinearLayoutManager(requireActivity()));
        this.list.setItemAnimator(this.itemAnimator);
        this.list.addItemDecoration(this.archiveDecoration);
        this.snapToTopDataObserver = new SnapToTopDataObserver(this.list);
        new ItemTouchHelper(new ArchiveListenerCallback(getResources().getColor(R.color.conversation_list_archive_background_start), getResources().getColor(R.color.conversation_list_archive_background_end))).attachToRecyclerView(this.list);
        this.fab.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda67
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ConversationListFragment.this.lambda$onViewCreated$0(view2);
            }
        });
        this.cameraFab.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda68
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ConversationListFragment.this.lambda$onViewCreated$3(view2);
            }
        });
        initializeViewModel();
        initializeListAdapters();
        initializeTypingObserver();
        initializeSearchListener();
        initializeVoiceNotePlayer();
        RatingManager.showRatingDialogIfNecessary(requireContext());
        TooltipCompat.setTooltipText(requireCallback().getSearchAction(), getText(R.string.SearchToolbar_search_for_conversations_contacts_and_messages));
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(true) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment.1
            @Override // androidx.activity.OnBackPressedCallback
            public void handleOnBackPressed() {
                if (!ConversationListFragment.this.closeSearchIfOpen() && !NavHostFragment.findNavController(ConversationListFragment.this).popBackStack()) {
                    ConversationListFragment.this.requireActivity().finish();
                }
            }
        });
        this.lifecycleDisposable = new LifecycleDisposable();
        this.conversationListTabsViewModel = (ConversationListTabsViewModel) new ViewModelProvider(requireActivity()).get(ConversationListTabsViewModel.class);
        this.lifecycleDisposable.bindTo(getViewLifecycleOwner());
        this.lifecycleDisposable.add(this.conversationListTabsViewModel.getTabClickEvents().filter(new Predicate() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda69
            @Override // io.reactivex.rxjava3.functions.Predicate
            public final boolean test(Object obj) {
                return ConversationListFragment.lambda$onViewCreated$4((ConversationListTab) obj);
            }
        }).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda70
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ConversationListFragment.this.lambda$onViewCreated$5((ConversationListTab) obj);
            }
        }));
        requireCallback().bindScrollHelper(this.list);
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        startActivity(new Intent(getActivity(), NewConversationActivity.class));
    }

    public /* synthetic */ void lambda$onViewCreated$3(View view) {
        Permissions.with(this).request("android.permission.CAMERA").ifNecessary().withRationaleDialog(getString(R.string.ConversationActivity_to_capture_photos_and_video_allow_signal_access_to_the_camera), R.drawable.ic_camera_24).withPermanentDenialDialog(getString(R.string.ConversationActivity_signal_needs_the_camera_permission_to_take_photos_or_video)).onAllGranted(new Runnable() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda32
            @Override // java.lang.Runnable
            public final void run() {
                ConversationListFragment.this.lambda$onViewCreated$1();
            }
        }).onAnyDenied(new Runnable() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda33
            @Override // java.lang.Runnable
            public final void run() {
                ConversationListFragment.this.lambda$onViewCreated$2();
            }
        }).execute();
    }

    public /* synthetic */ void lambda$onViewCreated$1() {
        startActivity(MediaSelectionActivity.camera(requireContext()));
    }

    public /* synthetic */ void lambda$onViewCreated$2() {
        Toast.makeText(requireContext(), (int) R.string.ConversationActivity_signal_needs_camera_permissions_to_take_photos_or_video, 1).show();
    }

    public static /* synthetic */ boolean lambda$onViewCreated$4(ConversationListTab conversationListTab) throws Throwable {
        return conversationListTab == ConversationListTab.CHATS;
    }

    public /* synthetic */ void lambda$onViewCreated$5(ConversationListTab conversationListTab) throws Throwable {
        if (((LinearLayoutManager) this.list.getLayoutManager()).findFirstVisibleItemPosition() <= 25) {
            this.list.smoothScrollToPosition(0);
        } else {
            this.list.scrollToPosition(0);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        this.coordinator = null;
        this.list = null;
        this.searchEmptyState = null;
        this.bottomActionBar = null;
        this.reminderView = null;
        this.emptyState = null;
        this.megaphoneContainer = null;
        this.paymentNotificationView = null;
        this.voiceNotePlayerViewStub = null;
        this.fab = null;
        this.cameraFab = null;
        this.snapToTopDataObserver = null;
        this.itemAnimator = null;
        this.activeAdapter = null;
        this.defaultAdapter = null;
        this.searchAdapter = null;
        super.onDestroyView();
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        updateReminders();
        EventBus.getDefault().register(this);
        this.itemAnimator.disable();
        if (Util.isDefaultSmsProvider(requireContext())) {
            InsightsLauncher.showInsightsModal(requireContext(), requireFragmentManager());
        }
        if ((!requireCallback().getSearchToolbar().resolved() || requireCallback().getSearchToolbar().get().getVisibility() != 0) && this.list.getAdapter() != this.defaultAdapter) {
            this.list.removeItemDecoration(this.searchAdapterDecoration);
            setAdapter(this.defaultAdapter);
        }
        RecyclerView.Adapter adapter = this.activeAdapter;
        boolean z = false;
        if (adapter != null) {
            adapter.notifyItemRangeChanged(0, adapter.getItemCount());
        }
        SignalProxyUtil.startListeningToWebsocket();
        if (SignalStore.rateLimit().needsRecaptcha()) {
            Log.i(TAG, "Recaptcha required.");
            RecaptchaProofBottomSheetFragment.show(getChildFragmentManager());
        }
        Badge expiredBadge = SignalStore.donationsValues().getExpiredBadge();
        UnexpectedSubscriptionCancellation fromStatus = UnexpectedSubscriptionCancellation.fromStatus(SignalStore.donationsValues().getUnexpectedSubscriptionCancelationReason());
        long unexpectedSubscriptionCancelationTimestamp = SignalStore.donationsValues().getUnexpectedSubscriptionCancelationTimestamp();
        boolean z2 = SignalStore.donationsValues().getUnexpectedSubscriptionCancelationWatermark() < unexpectedSubscriptionCancelationTimestamp;
        if (fromStatus != null && !SignalStore.donationsValues().isUserManuallyCancelled() && SignalStore.donationsValues().showCantProcessDialog() && z2) {
            String str = TAG;
            Log.w(str, "Displaying bottom sheet for unexpected cancellation: " + fromStatus, true);
            new CantProcessSubscriptionPaymentBottomSheetDialogFragment().show(getChildFragmentManager(), BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
            SignalStore.donationsValues().setUnexpectedSubscriptionCancelationWatermark(unexpectedSubscriptionCancelationTimestamp);
            z = true;
        } else if (fromStatus != null && SignalStore.donationsValues().isUserManuallyCancelled()) {
            String str2 = TAG;
            Log.w(str2, "Unexpected cancellation detected but not displaying dialog because user manually cancelled their subscription: " + fromStatus, true);
        } else if (fromStatus != null && !SignalStore.donationsValues().showCantProcessDialog()) {
            Log.w(TAG, "Unexpected cancellation detected but not displaying dialog because user has silenced it.", true);
        }
        if (expiredBadge != null && !z) {
            SignalStore.donationsValues().setExpiredBadge(null);
            if (expiredBadge.isBoost() || !SignalStore.donationsValues().isUserManuallyCancelled()) {
                Log.w(TAG, "Displaying bottom sheet for an expired badge", true);
                ExpiredBadgeBottomSheetDialogFragment.show(expiredBadge, fromStatus, SignalStore.donationsValues().getUnexpectedSubscriptionCancelationChargeFailure(), getParentFragmentManager());
            }
        }
    }

    @Override // org.thoughtcrime.securesms.LoggingFragment, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        ApplicationDependencies.getAppForegroundObserver().addListener(this.appForegroundObserver);
        this.itemAnimator.disable();
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        this.fab.stopPulse();
        this.cameraFab.stopPulse();
        EventBus.getDefault().unregister(this);
    }

    @Override // org.thoughtcrime.securesms.LoggingFragment, androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        ApplicationDependencies.getAppForegroundObserver().removeListener(this.appForegroundObserver);
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menu.clear();
        menuInflater.inflate(R.menu.text_secure_normal, menu);
    }

    @Override // androidx.fragment.app.Fragment
    public void onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.menu_insights).setVisible(Util.isDefaultSmsProvider(requireContext()));
        menu.findItem(R.id.menu_clear_passphrase).setVisible(!TextSecurePreferences.isPasswordDisabled(requireContext()));
    }

    @Override // androidx.fragment.app.Fragment
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        switch (menuItem.getItemId()) {
            case R.id.menu_clear_passphrase /* 2131363659 */:
                handleClearPassphrase();
                return true;
            case R.id.menu_insights /* 2131363675 */:
                handleInsights();
                return true;
            case R.id.menu_invite /* 2131363676 */:
                handleInvite();
                return true;
            case R.id.menu_mark_all_read /* 2131363678 */:
                handleMarkAllRead();
                return true;
            case R.id.menu_new_group /* 2131363680 */:
                handleCreateGroup();
                return true;
            case R.id.menu_notification_profile /* 2131363681 */:
                handleNotificationProfile();
                return true;
            case R.id.menu_settings /* 2131363686 */:
                handleDisplaySettings();
                return true;
            default:
                return false;
        }
    }

    private boolean isSearchOpen() {
        return isSearchVisible() || this.activeAdapter == this.searchAdapter;
    }

    private boolean isSearchVisible() {
        return requireCallback().getSearchToolbar().resolved() && requireCallback().getSearchToolbar().get().getVisibility() == 0;
    }

    public boolean closeSearchIfOpen() {
        if (!isSearchOpen()) {
            return false;
        }
        this.list.removeItemDecoration(this.searchAdapterDecoration);
        setAdapter(this.defaultAdapter);
        requireCallback().getSearchToolbar().get().collapse();
        requireCallback().onSearchClosed();
        return true;
    }

    @Override // androidx.fragment.app.Fragment
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        Permissions.onRequestPermissionsResult(this, i, strArr, iArr);
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i2 == -1 && i == 27698) {
            Snackbar.make(this.fab, (int) R.string.ConfirmKbsPinFragment__pin_created, 0).show();
            this.viewModel.onMegaphoneCompleted(Megaphones.Event.PINS_FOR_ALL);
        }
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListSearchAdapter.EventListener
    public void onConversationClicked(ThreadRecord threadRecord) {
        hideKeyboard();
        getNavigator().goToConversation(threadRecord.getRecipient().getId(), threadRecord.getThreadId(), threadRecord.getDistributionType(), -1);
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListAdapter.OnConversationClickListener
    public void onShowArchiveClick() {
        if (this.viewModel.currentSelectedConversations().isEmpty()) {
            NavHostFragment.findNavController(this).navigate(ConversationListFragmentDirections.actionConversationListFragmentToConversationListArchiveFragment());
        }
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListSearchAdapter.EventListener
    public void onContactClicked(Recipient recipient) {
        SimpleTask.run(getViewLifecycleOwner().getLifecycle(), new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda3
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return ConversationListFragment.lambda$onContactClicked$6(Recipient.this);
            }
        }, new SimpleTask.ForegroundTask(recipient) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda4
            public final /* synthetic */ Recipient f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                ConversationListFragment.this.lambda$onContactClicked$7(this.f$1, (Long) obj);
            }
        });
    }

    public static /* synthetic */ Long lambda$onContactClicked$6(Recipient recipient) {
        return Long.valueOf(SignalDatabase.threads().getThreadIdIfExistsFor(recipient.getId()));
    }

    public /* synthetic */ void lambda$onContactClicked$7(Recipient recipient, Long l) {
        hideKeyboard();
        getNavigator().goToConversation(recipient.getId(), l.longValue(), 2, -1);
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListSearchAdapter.EventListener
    public void onMessageClicked(MessageResult messageResult) {
        SimpleTask.run(getViewLifecycleOwner().getLifecycle(), new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda34
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return ConversationListFragment.lambda$onMessageClicked$8(MessageResult.this);
            }
        }, new SimpleTask.ForegroundTask(messageResult) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda35
            public final /* synthetic */ MessageResult f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                ConversationListFragment.this.lambda$onMessageClicked$9(this.f$1, (Integer) obj);
            }
        });
    }

    public static /* synthetic */ Integer lambda$onMessageClicked$8(MessageResult messageResult) {
        return Integer.valueOf(Math.max(0, SignalDatabase.mmsSms().getMessagePositionInConversation(messageResult.getThreadId(), messageResult.getReceivedTimestampMs())));
    }

    public /* synthetic */ void lambda$onMessageClicked$9(MessageResult messageResult, Integer num) {
        hideKeyboard();
        getNavigator().goToConversation(messageResult.getConversationRecipient().getId(), messageResult.getThreadId(), 2, num.intValue());
    }

    @Override // org.thoughtcrime.securesms.megaphone.MegaphoneActionController
    public void onMegaphoneNavigationRequested(Intent intent) {
        startActivity(intent);
    }

    @Override // org.thoughtcrime.securesms.megaphone.MegaphoneActionController
    public void onMegaphoneNavigationRequested(Intent intent, int i) {
        startActivityForResult(intent, i);
    }

    @Override // org.thoughtcrime.securesms.megaphone.MegaphoneActionController
    public void onMegaphoneToastRequested(String str) {
        Snackbar.make(this.fab, str, 0).show();
    }

    @Override // org.thoughtcrime.securesms.megaphone.MegaphoneActionController
    public Activity getMegaphoneActivity() {
        return requireActivity();
    }

    @Override // org.thoughtcrime.securesms.megaphone.MegaphoneActionController
    public void onMegaphoneSnooze(Megaphones.Event event) {
        this.viewModel.onMegaphoneSnoozed(event);
    }

    @Override // org.thoughtcrime.securesms.megaphone.MegaphoneActionController
    public void onMegaphoneCompleted(Megaphones.Event event) {
        this.viewModel.onMegaphoneCompleted(event);
    }

    @Override // org.thoughtcrime.securesms.megaphone.MegaphoneActionController
    public void onMegaphoneDialogFragmentRequested(DialogFragment dialogFragment) {
        dialogFragment.show(getChildFragmentManager(), "megaphone_dialog");
    }

    private void initializeReminderView() {
        this.reminderView.get().setOnDismissListener(new ReminderView.OnDismissListener() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda63
            @Override // org.thoughtcrime.securesms.components.reminder.ReminderView.OnDismissListener
            public final void onDismiss() {
                ConversationListFragment.this.updateReminders();
            }
        });
        this.reminderView.get().setOnActionClickListener(new ReminderView.OnActionClickListener() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda64
            @Override // org.thoughtcrime.securesms.components.reminder.ReminderView.OnActionClickListener
            public final void onActionClick(int i) {
                ConversationListFragment.this.onReminderAction(i);
            }
        });
    }

    public void onReminderAction(int i) {
        if (i == R.id.reminder_action_update_now) {
            PlayStoreUtil.openPlayStoreOrOurApkDownloadPage(requireContext());
        }
    }

    private void hideKeyboard() {
        ServiceUtil.getInputMethodManager(requireContext()).hideSoftInputFromWindow(requireView().getWindowToken(), 0);
    }

    private void initializeSearchListener() {
        requireCallback().getSearchAction().setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda65
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ConversationListFragment.this.lambda$initializeSearchListener$10(view);
            }
        });
    }

    public /* synthetic */ void lambda$initializeSearchListener$10(View view) {
        fadeOutButtonsAndMegaphone(ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
        requireCallback().onSearchOpened();
        requireCallback().getSearchToolbar().get().display(requireCallback().getSearchAction().getX() + (((float) requireCallback().getSearchAction().getWidth()) / 2.0f), requireCallback().getSearchAction().getY() + (((float) requireCallback().getSearchAction().getHeight()) / 2.0f));
        requireCallback().getSearchToolbar().get().setListener(new Material3SearchToolbar.Listener() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment.2
            @Override // org.thoughtcrime.securesms.components.Material3SearchToolbar.Listener
            public void onSearchTextChange(String str) {
                String trim = str.trim();
                ConversationListFragment.this.viewModel.onSearchQueryUpdated(trim);
                if (trim.length() > 0) {
                    if (ConversationListFragment.this.activeAdapter != ConversationListFragment.this.searchAdapter) {
                        ConversationListFragment conversationListFragment = ConversationListFragment.this;
                        conversationListFragment.setAdapter(conversationListFragment.searchAdapter);
                        ConversationListFragment.this.list.removeItemDecoration(ConversationListFragment.this.searchAdapterDecoration);
                        ConversationListFragment.this.list.addItemDecoration(ConversationListFragment.this.searchAdapterDecoration);
                    }
                } else if (ConversationListFragment.this.activeAdapter != ConversationListFragment.this.defaultAdapter) {
                    ConversationListFragment.this.list.removeItemDecoration(ConversationListFragment.this.searchAdapterDecoration);
                    ConversationListFragment conversationListFragment2 = ConversationListFragment.this;
                    conversationListFragment2.setAdapter(conversationListFragment2.defaultAdapter);
                }
            }

            @Override // org.thoughtcrime.securesms.components.Material3SearchToolbar.Listener
            public void onSearchClosed() {
                ConversationListFragment.this.list.removeItemDecoration(ConversationListFragment.this.searchAdapterDecoration);
                ConversationListFragment conversationListFragment = ConversationListFragment.this;
                conversationListFragment.setAdapter(conversationListFragment.defaultAdapter);
                ConversationListFragment.this.requireCallback().onSearchClosed();
                ConversationListFragment.this.fadeInButtonsAndMegaphone(ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
            }
        });
    }

    private void initializeVoiceNotePlayer() {
        this.mediaControllerOwner.getVoiceNoteMediaController().getVoiceNotePlayerViewState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda44
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationListFragment.this.lambda$initializeVoiceNotePlayer$11((Optional) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializeVoiceNotePlayer$11(Optional optional) {
        if (optional.isPresent()) {
            requireVoiceNotePlayerView().setState((VoiceNotePlayerView.State) optional.get());
            requireVoiceNotePlayerView().show();
        } else if (this.voiceNotePlayerViewStub.resolved()) {
            requireVoiceNotePlayerView().hide();
        }
    }

    private VoiceNotePlayerView requireVoiceNotePlayerView() {
        if (this.voiceNotePlayerView == null) {
            VoiceNotePlayerView voiceNotePlayerView = (VoiceNotePlayerView) this.voiceNotePlayerViewStub.get().findViewById(R.id.voice_note_player_view);
            this.voiceNotePlayerView = voiceNotePlayerView;
            voiceNotePlayerView.setListener(new VoiceNotePlayerViewListener());
        }
        return this.voiceNotePlayerView;
    }

    private void initializeListAdapters() {
        this.defaultAdapter = new ConversationListAdapter(getViewLifecycleOwner(), GlideApp.with(this), this);
        this.searchAdapter = new ConversationListSearchAdapter(getViewLifecycleOwner(), GlideApp.with(this), this, Locale.getDefault());
        this.searchAdapterDecoration = new StickyHeaderDecoration(this.searchAdapter, false, false, 0);
        setAdapter(this.defaultAdapter);
        this.defaultAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment.3
            @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
            public void onItemRangeInserted(int i, int i2) {
                ConversationListFragment.this.startupStopwatch.split("data-set");
                SignalLocalMetrics.ColdStart.onConversationListDataLoaded();
                ConversationListFragment.this.defaultAdapter.unregisterAdapterDataObserver(this);
                ConversationListFragment.this.list.post(new ConversationListFragment$3$$ExternalSyntheticLambda0(this));
            }

            public /* synthetic */ void lambda$onItemRangeInserted$0() {
                AppStartup.getInstance().onCriticalRenderEventEnd();
                ConversationListFragment.this.startupStopwatch.split("first-render");
                ConversationListFragment.this.startupStopwatch.stop(ConversationListFragment.TAG);
                if (ConversationListFragment.this.getContext() != null) {
                    ConversationFragment.prepare(ConversationListFragment.this.getContext());
                }
            }
        });
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        RecyclerView.Adapter adapter2 = this.activeAdapter;
        this.activeAdapter = adapter;
        if (adapter2 != adapter) {
            if (adapter instanceof ConversationListAdapter) {
                ((ConversationListAdapter) adapter).setPagingController(this.viewModel.getPagingController());
            }
            this.list.setAdapter(adapter);
            ConversationListAdapter conversationListAdapter = this.defaultAdapter;
            if (adapter == conversationListAdapter) {
                conversationListAdapter.registerAdapterDataObserver(this.snapToTopDataObserver);
            } else {
                conversationListAdapter.unregisterAdapterDataObserver(this.snapToTopDataObserver);
            }
        }
    }

    private void initializeTypingObserver() {
        ApplicationDependencies.getTypingStatusRepository().getTypingThreads().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda41
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationListFragment.this.lambda$initializeTypingObserver$12((Set) obj);
            }
        });
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.conversationlist.ConversationListAdapter */
    /* JADX WARN: Multi-variable type inference failed */
    public /* synthetic */ void lambda$initializeTypingObserver$12(Set set) {
        if (set == null) {
            set = Collections.emptySet();
        }
        this.defaultAdapter.setTypingThreads(set);
    }

    private void initializeViewModel() {
        ConversationListViewModel conversationListViewModel = (ConversationListViewModel) new ViewModelProvider(this, new ConversationListViewModel.Factory(isArchived(), getString(R.string.note_to_self))).get(ConversationListViewModel.class);
        this.viewModel = conversationListViewModel;
        conversationListViewModel.getSearchResult().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda5
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationListFragment.this.onSearchResultChanged((SearchResult) obj);
            }
        });
        this.viewModel.getMegaphone().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda6
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationListFragment.this.onMegaphoneChanged((Megaphone) obj);
            }
        });
        this.viewModel.getConversationList().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda7
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationListFragment.this.onConversationListChanged((List) obj);
            }
        });
        this.viewModel.hasNoConversations().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda8
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationListFragment.this.updateEmptyState(((Boolean) obj).booleanValue());
            }
        });
        this.viewModel.getNotificationProfiles().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda9
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationListFragment.this.lambda$initializeViewModel$13((List) obj);
            }
        });
        this.viewModel.getPipeState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda10
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationListFragment.this.lambda$initializeViewModel$14((WebSocketConnectionState) obj);
            }
        });
        this.appForegroundObserver = new AppForegroundObserver.Listener() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment.4
            @Override // org.thoughtcrime.securesms.util.AppForegroundObserver.Listener
            public void onBackground() {
            }

            @Override // org.thoughtcrime.securesms.util.AppForegroundObserver.Listener
            public void onForeground() {
                ConversationListFragment.this.viewModel.onVisible();
            }
        };
        this.viewModel.getUnreadPaymentsLiveData().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda11
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationListFragment.this.onUnreadPaymentsChanged((Optional) obj);
            }
        });
        this.viewModel.getSelectedConversations().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda12
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationListFragment.this.lambda$initializeViewModel$15((ConversationSet) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializeViewModel$13(List list) {
        requireCallback().updateNotificationProfileStatus(list);
    }

    public /* synthetic */ void lambda$initializeViewModel$14(WebSocketConnectionState webSocketConnectionState) {
        requireCallback().updateProxyStatus(webSocketConnectionState);
    }

    public /* synthetic */ void lambda$initializeViewModel$15(ConversationSet conversationSet) {
        this.defaultAdapter.setSelectedConversations(conversationSet);
        updateMultiSelectState();
    }

    public void onConversationListChanged(List<Conversation> list) {
        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) this.list.getLayoutManager();
        this.defaultAdapter.submitList(list, new Runnable(linearLayoutManager != null ? linearLayoutManager.findFirstCompletelyVisibleItemPosition() : -1, list) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda28
            public final /* synthetic */ int f$1;
            public final /* synthetic */ List f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationListFragment.this.lambda$onConversationListChanged$16(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$onConversationListChanged$16(int i, List list) {
        RecyclerView recyclerView = this.list;
        if (recyclerView != null) {
            if (i == 0) {
                recyclerView.scrollToPosition(0);
            }
            onPostSubmitList(list.size());
        }
    }

    public void onUnreadPaymentsChanged(Optional<UnreadPayments> optional) {
        if (optional.isPresent()) {
            this.paymentNotificationView.get().setListener(new PaymentNotificationListener(optional.get()));
            this.paymentNotificationView.get().setUnreadPayments(optional.get());
            animatePaymentUnreadStatusIn();
            return;
        }
        animatePaymentUnreadStatusOut();
    }

    private void animatePaymentUnreadStatusIn() {
        this.paymentNotificationView.get().setVisibility(0);
        requireCallback().getUnreadPaymentsDot().animate().alpha(1.0f);
    }

    private void animatePaymentUnreadStatusOut() {
        if (this.paymentNotificationView.resolved()) {
            this.paymentNotificationView.get().setVisibility(8);
        }
        requireCallback().getUnreadPaymentsDot().animate().alpha(0.0f);
    }

    public void onSearchResultChanged(SearchResult searchResult) {
        if (searchResult == null) {
            searchResult = SearchResult.EMPTY;
        }
        this.searchAdapter.updateResults(searchResult);
        if (!searchResult.isEmpty() || this.activeAdapter != this.searchAdapter) {
            this.searchEmptyState.setVisibility(8);
            return;
        }
        this.searchEmptyState.setText(getString(R.string.SearchFragment_no_results, searchResult.getQuery()));
        this.searchEmptyState.setVisibility(0);
    }

    public void onMegaphoneChanged(Megaphone megaphone) {
        if (megaphone != null) {
            View build = MegaphoneViewBuilder.build(requireContext(), megaphone, this);
            this.megaphoneContainer.get().removeAllViews();
            if (build != null) {
                this.megaphoneContainer.get().addView(build);
                if (isSearchOpen() || this.actionMode != null) {
                    this.megaphoneContainer.get().setVisibility(8);
                } else {
                    this.megaphoneContainer.get().setVisibility(0);
                }
            } else {
                this.megaphoneContainer.get().setVisibility(8);
                if (megaphone.getOnVisibleListener() != null) {
                    megaphone.getOnVisibleListener().onEvent(megaphone, this);
                }
            }
            this.viewModel.onMegaphoneVisible(megaphone);
        } else if (this.megaphoneContainer.resolved()) {
            this.megaphoneContainer.get().setVisibility(8);
            this.megaphoneContainer.get().removeAllViews();
        }
    }

    public void updateReminders() {
        SimpleTask.run(getViewLifecycleOwner().getLifecycle(), new SimpleTask.BackgroundTask(requireContext()) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda39
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return ConversationListFragment.lambda$updateReminders$17(this.f$0);
            }
        }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda40
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                ConversationListFragment.this.lambda$updateReminders$18((Optional) obj);
            }
        });
    }

    public static /* synthetic */ Optional lambda$updateReminders$17(Context context) {
        if (UnauthorizedReminder.isEligible(context)) {
            return Optional.of(new UnauthorizedReminder(context));
        }
        if (ExpiredBuildReminder.isEligible()) {
            return Optional.of(new ExpiredBuildReminder(context));
        }
        if (ServiceOutageReminder.isEligible(context)) {
            ApplicationDependencies.getJobManager().add(new ServiceOutageDetectionJob());
            return Optional.of(new ServiceOutageReminder(context));
        } else if (OutdatedBuildReminder.isEligible()) {
            return Optional.of(new OutdatedBuildReminder(context));
        } else {
            if (PushRegistrationReminder.isEligible(context)) {
                return Optional.of(new PushRegistrationReminder(context));
            }
            if (DozeReminder.isEligible(context)) {
                return Optional.of(new DozeReminder(context));
            }
            return Optional.empty();
        }
    }

    public /* synthetic */ void lambda$updateReminders$18(Optional optional) {
        if (optional.isPresent() && getActivity() != null && !isRemoving()) {
            if (!this.reminderView.resolved()) {
                initializeReminderView();
            }
            this.reminderView.get().showReminder((Reminder) optional.get());
        } else if (this.reminderView.resolved() && !optional.isPresent()) {
            this.reminderView.get().hide();
        }
    }

    private void handleCreateGroup() {
        getNavigator().goToGroupCreation();
    }

    private void handleDisplaySettings() {
        getNavigator().goToAppSettings();
    }

    private void handleClearPassphrase() {
        Intent intent = new Intent(requireActivity(), KeyCachingService.class);
        intent.setAction(KeyCachingService.CLEAR_KEY_ACTION);
        requireActivity().startService(intent);
    }

    private void handleMarkAllRead() {
        SignalExecutors.BOUNDED.execute(new Runnable(requireContext()) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda29
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationListFragment.lambda$handleMarkAllRead$19(this.f$0);
            }
        });
    }

    public static /* synthetic */ void lambda$handleMarkAllRead$19(Context context) {
        List<MessageDatabase.MarkedMessageInfo> allThreadsRead = SignalDatabase.threads().setAllThreadsRead();
        ApplicationDependencies.getMessageNotifier().updateNotification(context);
        MarkReadReceiver.process(context, allThreadsRead);
    }

    /* renamed from: handleMarkAsRead */
    public void lambda$updateMultiSelectState$53(Collection<Long> collection) {
        SimpleTask.run(getViewLifecycleOwner().getLifecycle(), new SimpleTask.BackgroundTask(collection, requireContext()) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda42
            public final /* synthetic */ Collection f$0;
            public final /* synthetic */ Context f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return ConversationListFragment.lambda$handleMarkAsRead$20(this.f$0, this.f$1);
            }
        }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda43
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                ConversationListFragment.this.lambda$handleMarkAsRead$21(obj);
            }
        });
    }

    public static /* synthetic */ Object lambda$handleMarkAsRead$20(Collection collection, Context context) {
        List<MessageDatabase.MarkedMessageInfo> read = SignalDatabase.threads().setRead((Collection<Long>) collection, false);
        ApplicationDependencies.getMessageNotifier().updateNotification(context);
        MarkReadReceiver.process(context, read);
        return null;
    }

    public /* synthetic */ void lambda$handleMarkAsRead$21(Object obj) {
        endActionModeIfActive();
    }

    /* renamed from: handleMarkAsUnread */
    public void lambda$updateMultiSelectState$54(Collection<Long> collection) {
        requireContext();
        SimpleTask.run(getViewLifecycleOwner().getLifecycle(), new SimpleTask.BackgroundTask(collection) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda13
            public final /* synthetic */ Collection f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return ConversationListFragment.lambda$handleMarkAsUnread$22(this.f$0);
            }
        }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda14
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                ConversationListFragment.this.lambda$handleMarkAsUnread$23(obj);
            }
        });
    }

    public static /* synthetic */ Object lambda$handleMarkAsUnread$22(Collection collection) {
        SignalDatabase.threads().setForcedUnread(collection);
        StorageSyncHelper.scheduleSyncForDataChange();
        return null;
    }

    public /* synthetic */ void lambda$handleMarkAsUnread$23(Object obj) {
        endActionModeIfActive();
    }

    private void handleInvite() {
        getNavigator().goToInvite();
    }

    private void handleInsights() {
        getNavigator().goToInsights();
    }

    private void handleNotificationProfile() {
        NotificationProfileSelectionFragment.show(getParentFragmentManager());
    }

    private void handleArchive(Collection<Long> collection, boolean z) {
        final HashSet hashSet = new HashSet(collection);
        int size = hashSet.size();
        new SnackbarAsyncTask<Void>(getViewLifecycleOwner().getLifecycle(), this.coordinator, getResources().getQuantityString(getArchivedSnackbarTitleRes(), size, Integer.valueOf(size)), getString(R.string.ConversationListFragment_undo), getResources().getColor(R.color.amber_500), 0, z) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment.5
            @Override // org.thoughtcrime.securesms.util.task.SnackbarAsyncTask
            public void onPostExecute(Void r1) {
                super.onPostExecute(r1);
                ConversationListFragment.this.endActionModeIfActive();
            }

            public void executeAction(Void r2) {
                ConversationListFragment.this.archiveThreads(hashSet);
            }

            public void reverseAction(Void r2) {
                ConversationListFragment.this.reverseArchiveThreads(hashSet);
            }
        }.executeOnExecutor(SignalExecutors.BOUNDED, new Void[0]);
    }

    /* renamed from: handleDelete */
    public void lambda$updateMultiSelectState$59(Collection<Long> collection) {
        int size = collection.size();
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(requireActivity());
        Context requireContext = requireContext();
        materialAlertDialogBuilder.setTitle((CharSequence) requireContext.getResources().getQuantityString(R.plurals.ConversationListFragment_delete_selected_conversations, size, Integer.valueOf(size)));
        materialAlertDialogBuilder.setMessage((CharSequence) requireContext.getResources().getQuantityString(R.plurals.ConversationListFragment_this_will_permanently_delete_all_n_selected_conversations, size, Integer.valueOf(size)));
        materialAlertDialogBuilder.setCancelable(true);
        materialAlertDialogBuilder.setPositiveButton(R.string.delete, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(collection, requireContext) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda66
            public final /* synthetic */ Collection f$1;
            public final /* synthetic */ Context f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ConversationListFragment.this.lambda$handleDelete$24(this.f$1, this.f$2, dialogInterface, i);
            }
        });
        materialAlertDialogBuilder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        materialAlertDialogBuilder.show();
    }

    public /* synthetic */ void lambda$handleDelete$24(Collection collection, final Context context, DialogInterface dialogInterface, int i) {
        final HashSet hashSet = new HashSet(collection);
        if (!hashSet.isEmpty()) {
            new AsyncTask<Void, Void, Void>() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment.6
                private ProgressDialog dialog;

                @Override // android.os.AsyncTask
                protected void onPreExecute() {
                    this.dialog = ProgressDialog.show(ConversationListFragment.this.requireActivity(), context.getString(R.string.ConversationListFragment_deleting), context.getString(R.string.ConversationListFragment_deleting_selected_conversations), true, false);
                }

                public Void doInBackground(Void... voidArr) {
                    SignalDatabase.threads().deleteConversations(hashSet);
                    ApplicationDependencies.getMessageNotifier().updateNotification(ConversationListFragment.this.requireActivity());
                    return null;
                }

                public void onPostExecute(Void r1) {
                    this.dialog.dismiss();
                    ConversationListFragment.this.endActionModeIfActive();
                }
            }.executeOnExecutor(SignalExecutors.BOUNDED, new Void[0]);
        }
    }

    private void handlePin(Collection<Conversation> collection) {
        LinkedHashSet linkedHashSet = new LinkedHashSet(Stream.of(collection).filterNot(new com.annimon.stream.function.Predicate() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda59
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ConversationListFragment.lambda$handlePin$25((Conversation) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda60
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ConversationListFragment.lambda$handlePin$26((Conversation) obj);
            }
        }).toList());
        if (linkedHashSet.size() + this.viewModel.getPinnedCount() > 4) {
            Snackbar.make(this.fab, getString(R.string.conversation_list__you_can_only_pin_up_to_d_chats, 4), 0).show();
            endActionModeIfActive();
            return;
        }
        SimpleTask.run(SignalExecutors.BOUNDED, new SimpleTask.BackgroundTask(linkedHashSet) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda61
            public final /* synthetic */ Set f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return ConversationListFragment.lambda$handlePin$27(this.f$0);
            }
        }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda62
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                ConversationListFragment.this.lambda$handlePin$28(obj);
            }
        });
    }

    public static /* synthetic */ boolean lambda$handlePin$25(Conversation conversation) {
        return conversation.getThreadRecord().isPinned();
    }

    public static /* synthetic */ Long lambda$handlePin$26(Conversation conversation) {
        return Long.valueOf(conversation.getThreadRecord().getThreadId());
    }

    public static /* synthetic */ Object lambda$handlePin$27(Set set) {
        SignalDatabase.threads().pinConversations(set);
        ConversationUtil.refreshRecipientShortcuts();
        return null;
    }

    public /* synthetic */ void lambda$handlePin$28(Object obj) {
        endActionModeIfActive();
    }

    /* renamed from: handleUnpin */
    public void lambda$updateMultiSelectState$56(Collection<Long> collection) {
        SimpleTask.run(SignalExecutors.BOUNDED, new SimpleTask.BackgroundTask(collection) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda26
            public final /* synthetic */ Collection f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return ConversationListFragment.lambda$handleUnpin$29(this.f$0);
            }
        }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda27
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                ConversationListFragment.this.lambda$handleUnpin$30(obj);
            }
        });
    }

    public static /* synthetic */ Object lambda$handleUnpin$29(Collection collection) {
        SignalDatabase.threads().unpinConversations(collection);
        ConversationUtil.refreshRecipientShortcuts();
        return null;
    }

    public /* synthetic */ void lambda$handleUnpin$30(Object obj) {
        endActionModeIfActive();
    }

    private void handleMute(Collection<Conversation> collection) {
        MuteDialog.show(requireContext(), new MuteDialog.MuteSelectionListener(collection) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ Collection f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.MuteDialog.MuteSelectionListener
            public final void onMuted(long j) {
                ConversationListFragment.this.lambda$handleMute$31(this.f$1, j);
            }
        });
    }

    private void handleUnmute(Collection<Conversation> collection) {
        lambda$handleMute$31(collection, 0);
    }

    /* renamed from: updateMute */
    public void lambda$handleMute$31(Collection<Conversation> collection, long j) {
        SimpleTask.run(SignalExecutors.BOUNDED, new SimpleTask.BackgroundTask(collection, j) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ Collection f$0;
            public final /* synthetic */ long f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return ConversationListFragment.lambda$updateMute$34(this.f$0, this.f$1);
            }
        }, new SimpleTask.ForegroundTask(SimpleProgressDialog.showDelayed(requireContext(), ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION, ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION)) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ SimpleProgressDialog.DismissibleDialog f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                ConversationListFragment.this.lambda$updateMute$35(this.f$1, obj);
            }
        });
    }

    public static /* synthetic */ Object lambda$updateMute$34(Collection collection, long j) {
        SignalDatabase.recipients().setMuted((List) Collection$EL.stream(collection).map(new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda36
            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ConversationListFragment.lambda$updateMute$32((Conversation) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).filter(new j$.util.function.Predicate(j) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda37
            public final /* synthetic */ long f$0;

            {
                this.f$0 = r1;
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate and(j$.util.function.Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate or(j$.util.function.Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return ConversationListFragment.lambda$updateMute$33(this.f$0, (Recipient) obj);
            }
        }).map(new ConversationListFragment$$ExternalSyntheticLambda38()).collect(Collectors.toList()), j);
        return null;
    }

    public static /* synthetic */ Recipient lambda$updateMute$32(Conversation conversation) {
        return conversation.getThreadRecord().getRecipient().live().get();
    }

    public static /* synthetic */ boolean lambda$updateMute$33(long j, Recipient recipient) {
        return recipient.getMuteUntil() != j;
    }

    public /* synthetic */ void lambda$updateMute$35(SimpleProgressDialog.DismissibleDialog dismissibleDialog, Object obj) {
        endActionModeIfActive();
        dismissibleDialog.dismiss();
    }

    private void handleCreateConversation(long j, Recipient recipient, int i) {
        SimpleTask.run(getLifecycle(), new SimpleTask.BackgroundTask(recipient) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda30
            public final /* synthetic */ Recipient f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return ConversationListFragment.this.lambda$handleCreateConversation$36(this.f$1);
            }
        }, new SimpleTask.ForegroundTask(recipient, j, i) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda31
            public final /* synthetic */ Recipient f$1;
            public final /* synthetic */ long f$2;
            public final /* synthetic */ int f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r5;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                ConversationListFragment.this.lambda$handleCreateConversation$37(this.f$1, this.f$2, this.f$3, obj);
            }
        });
    }

    public /* synthetic */ Object lambda$handleCreateConversation$36(Recipient recipient) {
        ChatWallpaper wallpaper = recipient.resolve().getWallpaper();
        if (wallpaper == null || wallpaper.prefetch(requireContext(), 250)) {
            return null;
        }
        Log.w(TAG, "Failed to prefetch wallpaper.");
        return null;
    }

    public /* synthetic */ void lambda$handleCreateConversation$37(Recipient recipient, long j, int i, Object obj) {
        getNavigator().goToConversation(recipient.getId(), j, i, -1);
    }

    private void fadeOutButtonsAndMegaphone(int i) {
        ViewUtil.fadeOut(this.fab, i);
        ViewUtil.fadeOut(this.cameraFab, i);
        if (this.megaphoneContainer.resolved()) {
            ViewUtil.fadeOut(this.megaphoneContainer.get(), i);
        }
    }

    public void fadeInButtonsAndMegaphone(int i) {
        ViewUtil.fadeIn(this.fab, i);
        ViewUtil.fadeIn(this.cameraFab, i);
        if (this.megaphoneContainer.resolved()) {
            ViewUtil.fadeIn(this.megaphoneContainer.get(), i);
        }
    }

    private void startActionMode() {
        this.actionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(this);
        SignalBottomActionBar signalBottomActionBar = this.bottomActionBar;
        ViewUtil.animateIn(signalBottomActionBar, signalBottomActionBar.getEnterAnimation());
        ViewUtil.fadeOut(this.fab, ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
        ViewUtil.fadeOut(this.cameraFab, ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
        if (this.megaphoneContainer.resolved()) {
            ViewUtil.fadeOut(this.megaphoneContainer.get(), ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
        }
        requireCallback().onMultiSelectStarted();
    }

    public void endActionModeIfActive() {
        if (this.actionMode != null) {
            endActionMode();
        }
    }

    private void endActionMode() {
        this.actionMode.finish();
        this.actionMode = null;
        SignalBottomActionBar signalBottomActionBar = this.bottomActionBar;
        ViewUtil.animateOut(signalBottomActionBar, signalBottomActionBar.getExitAnimation());
        ViewUtil.fadeIn(this.fab, ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
        ViewUtil.fadeIn(this.cameraFab, ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
        if (this.megaphoneContainer.resolved()) {
            ViewUtil.fadeIn(this.megaphoneContainer.get(), ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
        }
        requireCallback().onMultiSelectFinished();
    }

    public void updateEmptyState(boolean z) {
        if (z) {
            Log.i(TAG, "Received an empty data set.");
            this.list.setVisibility(4);
            this.emptyState.get().setVisibility(0);
            this.fab.startPulse(3000);
            this.cameraFab.startPulse(3000);
            SignalStore.onboarding().setShowNewGroup(true);
            SignalStore.onboarding().setShowInviteFriends(true);
            return;
        }
        this.list.setVisibility(0);
        this.fab.stopPulse();
        this.cameraFab.stopPulse();
        if (this.emptyState.resolved()) {
            this.emptyState.get().setVisibility(8);
        }
    }

    protected void onPostSubmitList(int i) {
        if (i < 6) {
            return;
        }
        if (SignalStore.onboarding().shouldShowInviteFriends() || SignalStore.onboarding().shouldShowNewGroup()) {
            SignalStore.onboarding().clearAll();
            ApplicationDependencies.getMegaphoneRepository().markFinished(Megaphones.Event.ONBOARDING);
        }
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListAdapter.OnConversationClickListener
    public void onConversationClick(Conversation conversation) {
        if (this.actionMode == null) {
            handleCreateConversation(conversation.getThreadRecord().getThreadId(), conversation.getThreadRecord().getRecipient(), conversation.getThreadRecord().getDistributionType());
            return;
        }
        this.viewModel.toggleConversationSelected(conversation);
        if (this.viewModel.currentSelectedConversations().isEmpty()) {
            endActionModeIfActive();
        } else {
            updateMultiSelectState();
        }
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListAdapter.OnConversationClickListener
    public boolean onConversationLongClick(Conversation conversation, View view) {
        if (this.actionMode != null) {
            onConversationClick(conversation);
            return true;
        } else if (this.activeContextMenu != null) {
            Log.w(TAG, "Already showing a context menu.");
            return true;
        } else {
            view.setSelected(true);
            Set singleton = Collections.singleton(Long.valueOf(conversation.getThreadRecord().getThreadId()));
            ArrayList arrayList = new ArrayList();
            if (!conversation.getThreadRecord().isArchived()) {
                if (conversation.getThreadRecord().isRead()) {
                    arrayList.add(new ActionItem(R.drawable.ic_unread_24, getResources().getQuantityString(R.plurals.ConversationListFragment_unread_plural, 1), new Runnable(singleton) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda15
                        public final /* synthetic */ Collection f$1;

                        {
                            this.f$1 = r2;
                        }

                        @Override // java.lang.Runnable
                        public final void run() {
                            ConversationListFragment.this.lambda$onConversationLongClick$38(this.f$1);
                        }
                    }));
                } else {
                    arrayList.add(new ActionItem(R.drawable.ic_read_24, getResources().getQuantityString(R.plurals.ConversationListFragment_read_plural, 1), new Runnable(singleton) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda17
                        public final /* synthetic */ Collection f$1;

                        {
                            this.f$1 = r2;
                        }

                        @Override // java.lang.Runnable
                        public final void run() {
                            ConversationListFragment.this.lambda$onConversationLongClick$39(this.f$1);
                        }
                    }));
                }
                if (conversation.getThreadRecord().isPinned()) {
                    arrayList.add(new ActionItem(R.drawable.ic_unpin_24, getResources().getQuantityString(R.plurals.ConversationListFragment_unpin_plural, 1), new Runnable(singleton) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda18
                        public final /* synthetic */ Collection f$1;

                        {
                            this.f$1 = r2;
                        }

                        @Override // java.lang.Runnable
                        public final void run() {
                            ConversationListFragment.this.lambda$onConversationLongClick$40(this.f$1);
                        }
                    }));
                } else {
                    arrayList.add(new ActionItem(R.drawable.ic_pin_24, getResources().getQuantityString(R.plurals.ConversationListFragment_pin_plural, 1), new Runnable(conversation) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda19
                        public final /* synthetic */ Conversation f$1;

                        {
                            this.f$1 = r2;
                        }

                        @Override // java.lang.Runnable
                        public final void run() {
                            ConversationListFragment.this.lambda$onConversationLongClick$41(this.f$1);
                        }
                    }));
                }
                if (conversation.getThreadRecord().getRecipient().live().get().isMuted()) {
                    arrayList.add(new ActionItem(R.drawable.ic_unmute_24, getResources().getQuantityString(R.plurals.ConversationListFragment_unmute_plural, 1), new Runnable(conversation) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda20
                        public final /* synthetic */ Conversation f$1;

                        {
                            this.f$1 = r2;
                        }

                        @Override // java.lang.Runnable
                        public final void run() {
                            ConversationListFragment.this.lambda$onConversationLongClick$42(this.f$1);
                        }
                    }));
                } else {
                    arrayList.add(new ActionItem(R.drawable.ic_mute_24, getResources().getQuantityString(R.plurals.ConversationListFragment_mute_plural, 1), new Runnable(conversation) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda21
                        public final /* synthetic */ Conversation f$1;

                        {
                            this.f$1 = r2;
                        }

                        @Override // java.lang.Runnable
                        public final void run() {
                            ConversationListFragment.this.lambda$onConversationLongClick$43(this.f$1);
                        }
                    }));
                }
            }
            arrayList.add(new ActionItem(R.drawable.ic_select_24, getString(R.string.ConversationListFragment_select), new Runnable(conversation) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda22
                public final /* synthetic */ Conversation f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ConversationListFragment.this.lambda$onConversationLongClick$44(this.f$1);
                }
            }));
            if (conversation.getThreadRecord().isArchived()) {
                arrayList.add(new ActionItem(R.drawable.ic_unarchive_24, getResources().getQuantityString(R.plurals.ConversationListFragment_unarchive_plural, 1), new Runnable(singleton) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda23
                    public final /* synthetic */ Collection f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        ConversationListFragment.this.lambda$onConversationLongClick$45(this.f$1);
                    }
                }));
            } else {
                arrayList.add(new ActionItem(R.drawable.ic_archive_24, getResources().getQuantityString(R.plurals.ConversationListFragment_archive_plural, 1), new Runnable(singleton) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda24
                    public final /* synthetic */ Collection f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        ConversationListFragment.this.lambda$onConversationLongClick$46(this.f$1);
                    }
                }));
            }
            arrayList.add(new ActionItem(R.drawable.ic_delete_24, getResources().getQuantityString(R.plurals.ConversationListFragment_delete_plural, 1), new Runnable(singleton) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda25
                public final /* synthetic */ Collection f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ConversationListFragment.this.lambda$onConversationLongClick$47(this.f$1);
                }
            }));
            this.activeContextMenu = new SignalContextMenu.Builder(view, this.list).offsetX(ViewUtil.dpToPx(12)).offsetY(ViewUtil.dpToPx(12)).onDismiss(new Runnable(view) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda16
                public final /* synthetic */ View f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ConversationListFragment.this.lambda$onConversationLongClick$48(this.f$1);
                }
            }).show(arrayList);
            this.list.suppressLayout(true);
            return true;
        }
    }

    public /* synthetic */ void lambda$onConversationLongClick$41(Conversation conversation) {
        handlePin(Collections.singleton(conversation));
    }

    public /* synthetic */ void lambda$onConversationLongClick$42(Conversation conversation) {
        handleUnmute(Collections.singleton(conversation));
    }

    public /* synthetic */ void lambda$onConversationLongClick$43(Conversation conversation) {
        handleMute(Collections.singleton(conversation));
    }

    public /* synthetic */ void lambda$onConversationLongClick$44(Conversation conversation) {
        this.viewModel.startSelection(conversation);
        startActionMode();
    }

    public /* synthetic */ void lambda$onConversationLongClick$45(Collection collection) {
        handleArchive(collection, false);
    }

    public /* synthetic */ void lambda$onConversationLongClick$46(Collection collection) {
        handleArchive(collection, false);
    }

    public /* synthetic */ void lambda$onConversationLongClick$48(View view) {
        this.activeContextMenu = null;
        view.setSelected(false);
        this.list.suppressLayout(false);
    }

    @Override // androidx.appcompat.view.ActionMode.Callback
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        actionMode.setTitle(requireContext().getResources().getQuantityString(R.plurals.ConversationListFragment_s_selected, 1, 1));
        return true;
    }

    @Override // androidx.appcompat.view.ActionMode.Callback
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        updateMultiSelectState();
        return false;
    }

    @Override // androidx.appcompat.view.ActionMode.Callback
    public void onDestroyActionMode(ActionMode actionMode) {
        this.viewModel.endSelection();
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            TypedArray obtainStyledAttributes = getActivity().getTheme().obtainStyledAttributes(new int[]{16843857});
            WindowUtil.setStatusBarColor(getActivity().getWindow(), obtainStyledAttributes.getColor(0, -16777216));
            obtainStyledAttributes.recycle();
        }
        if (i >= 23) {
            TypedArray obtainStyledAttributes2 = getActivity().getTheme().obtainStyledAttributes(new int[]{16844000});
            int systemUiVisibility = getActivity().getWindow().getDecorView().getSystemUiVisibility();
            getActivity().getWindow().getDecorView().setSystemUiVisibility(obtainStyledAttributes2.getBoolean(0, false) ? systemUiVisibility | 8192 : systemUiVisibility & -8193);
            obtainStyledAttributes2.recycle();
        }
        endActionModeIfActive();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ReminderUpdateEvent reminderUpdateEvent) {
        updateReminders();
    }

    @Subscribe(sticky = BuildConfig.PLAY_STORE_DISABLED, threadMode = ThreadMode.MAIN)
    public void onEvent(MessageSender.MessageSentEvent messageSentEvent) {
        EventBus.getDefault().removeStickyEvent(messageSentEvent);
        closeSearchIfOpen();
    }

    private void updateMultiSelectState() {
        int size = this.viewModel.currentSelectedConversations().size();
        boolean anyMatch = Stream.of(this.viewModel.currentSelectedConversations()).anyMatch(new com.annimon.stream.function.Predicate() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda45
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ConversationListFragment.lambda$updateMultiSelectState$49((Conversation) obj);
            }
        });
        boolean anyMatch2 = Stream.of(this.viewModel.currentSelectedConversations()).anyMatch(new com.annimon.stream.function.Predicate() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda50
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ConversationListFragment.lambda$updateMultiSelectState$50((Conversation) obj);
            }
        });
        boolean anyMatch3 = Stream.of(this.viewModel.currentSelectedConversations()).anyMatch(new com.annimon.stream.function.Predicate() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda51
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ConversationListFragment.lambda$updateMultiSelectState$51((Conversation) obj);
            }
        });
        boolean z = this.viewModel.getPinnedCount() < 4;
        ActionMode actionMode = this.actionMode;
        if (actionMode != null) {
            actionMode.setTitle(requireContext().getResources().getQuantityString(R.plurals.ConversationListFragment_s_selected, size, Integer.valueOf(size)));
        }
        ArrayList arrayList = new ArrayList();
        Set set = (Set) Collection$EL.stream(this.viewModel.currentSelectedConversations()).map(new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda52
            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ConversationListFragment.lambda$updateMultiSelectState$52((Conversation) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toSet());
        if (anyMatch) {
            arrayList.add(new ActionItem(R.drawable.ic_read_24, getResources().getQuantityString(R.plurals.ConversationListFragment_read_plural, size), new Runnable(set) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda53
                public final /* synthetic */ Set f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ConversationListFragment.this.lambda$updateMultiSelectState$53(this.f$1);
                }
            }));
        } else {
            arrayList.add(new ActionItem(R.drawable.ic_unread_24, getResources().getQuantityString(R.plurals.ConversationListFragment_unread_plural, size), new Runnable(set) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda54
                public final /* synthetic */ Set f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ConversationListFragment.this.lambda$updateMultiSelectState$54(this.f$1);
                }
            }));
        }
        if (!isArchived() && anyMatch2 && z) {
            arrayList.add(new ActionItem(R.drawable.ic_pin_24, getResources().getQuantityString(R.plurals.ConversationListFragment_pin_plural, size), new Runnable() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda55
                @Override // java.lang.Runnable
                public final void run() {
                    ConversationListFragment.this.lambda$updateMultiSelectState$55();
                }
            }));
        } else if (!isArchived() && !anyMatch2) {
            arrayList.add(new ActionItem(R.drawable.ic_unpin_24, getResources().getQuantityString(R.plurals.ConversationListFragment_unpin_plural, size), new Runnable(set) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda56
                public final /* synthetic */ Set f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ConversationListFragment.this.lambda$updateMultiSelectState$56(this.f$1);
                }
            }));
        }
        if (isArchived()) {
            arrayList.add(new ActionItem(R.drawable.ic_unarchive_24, getResources().getQuantityString(R.plurals.ConversationListFragment_unarchive_plural, size), new Runnable(set) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda57
                public final /* synthetic */ Set f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ConversationListFragment.this.lambda$updateMultiSelectState$57(this.f$1);
                }
            }));
        } else {
            arrayList.add(new ActionItem(R.drawable.ic_archive_24, getResources().getQuantityString(R.plurals.ConversationListFragment_archive_plural, size), new Runnable(set) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda58
                public final /* synthetic */ Set f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ConversationListFragment.this.lambda$updateMultiSelectState$58(this.f$1);
                }
            }));
        }
        arrayList.add(new ActionItem(R.drawable.ic_delete_24, getResources().getQuantityString(R.plurals.ConversationListFragment_delete_plural, size), new Runnable(set) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda46
            public final /* synthetic */ Set f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationListFragment.this.lambda$updateMultiSelectState$59(this.f$1);
            }
        }));
        if (anyMatch3) {
            arrayList.add(new ActionItem(R.drawable.ic_mute_24, getResources().getQuantityString(R.plurals.ConversationListFragment_mute_plural, size), new Runnable() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda47
                @Override // java.lang.Runnable
                public final void run() {
                    ConversationListFragment.this.lambda$updateMultiSelectState$60();
                }
            }));
        } else {
            arrayList.add(new ActionItem(R.drawable.ic_unmute_24, getResources().getQuantityString(R.plurals.ConversationListFragment_unmute_plural, size), new Runnable() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda48
                @Override // java.lang.Runnable
                public final void run() {
                    ConversationListFragment.this.lambda$updateMultiSelectState$61();
                }
            }));
        }
        String string = getString(R.string.ConversationListFragment_select_all);
        ConversationListViewModel conversationListViewModel = this.viewModel;
        Objects.requireNonNull(conversationListViewModel);
        arrayList.add(new ActionItem(R.drawable.ic_select_24, string, new Runnable() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment$$ExternalSyntheticLambda49
            @Override // java.lang.Runnable
            public final void run() {
                ConversationListViewModel.this.onSelectAllClick();
            }
        }));
        this.bottomActionBar.setItems(arrayList);
    }

    public static /* synthetic */ boolean lambda$updateMultiSelectState$49(Conversation conversation) {
        return !conversation.getThreadRecord().isRead();
    }

    public static /* synthetic */ boolean lambda$updateMultiSelectState$50(Conversation conversation) {
        return !conversation.getThreadRecord().isPinned();
    }

    public static /* synthetic */ boolean lambda$updateMultiSelectState$51(Conversation conversation) {
        return !conversation.getThreadRecord().getRecipient().live().get().isMuted();
    }

    public static /* synthetic */ Long lambda$updateMultiSelectState$52(Conversation conversation) {
        return Long.valueOf(conversation.getThreadRecord().getThreadId());
    }

    public /* synthetic */ void lambda$updateMultiSelectState$55() {
        handlePin(this.viewModel.currentSelectedConversations());
    }

    public /* synthetic */ void lambda$updateMultiSelectState$57(Set set) {
        handleArchive(set, true);
    }

    public /* synthetic */ void lambda$updateMultiSelectState$58(Set set) {
        handleArchive(set, true);
    }

    public /* synthetic */ void lambda$updateMultiSelectState$60() {
        handleMute(this.viewModel.currentSelectedConversations());
    }

    public /* synthetic */ void lambda$updateMultiSelectState$61() {
        handleUnmute(this.viewModel.currentSelectedConversations());
    }

    public Callback requireCallback() {
        return (Callback) getParentFragment().getParentFragment();
    }

    protected Toolbar getToolbar(View view) {
        return requireCallback().getToolbar();
    }

    protected void archiveThreads(Set<Long> set) {
        SignalDatabase.threads().setArchived(set, true);
    }

    protected void reverseArchiveThreads(Set<Long> set) {
        SignalDatabase.threads().setArchived(set, false);
    }

    protected void onItemSwiped(final long j, final int i) {
        this.archiveDecoration.onArchiveStarted();
        this.itemAnimator.enable();
        new SnackbarAsyncTask<Long>(getViewLifecycleOwner().getLifecycle(), this.coordinator, getResources().getQuantityString(R.plurals.ConversationListFragment_conversations_archived, 1, 1), getString(R.string.ConversationListFragment_undo), getResources().getColor(R.color.amber_500), 0, false) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListFragment.7
            private List<Long> pinnedThreadIds;
            private final ThreadDatabase threadDatabase = SignalDatabase.threads();

            public void executeAction(Long l) {
                FragmentActivity requireActivity = ConversationListFragment.this.requireActivity();
                this.pinnedThreadIds = this.threadDatabase.getPinnedThreadIds();
                this.threadDatabase.archiveConversation(j);
                if (i > 0) {
                    List<MessageDatabase.MarkedMessageInfo> read = this.threadDatabase.setRead(j, false);
                    ApplicationDependencies.getMessageNotifier().updateNotification(requireActivity);
                    MarkReadReceiver.process(requireActivity, read);
                }
                ConversationUtil.refreshRecipientShortcuts();
            }

            public void reverseAction(Long l) {
                FragmentActivity requireActivity = ConversationListFragment.this.requireActivity();
                this.threadDatabase.unarchiveConversation(j);
                this.threadDatabase.restorePins(this.pinnedThreadIds);
                int i2 = i;
                if (i2 > 0) {
                    this.threadDatabase.incrementUnread(j, i2);
                    ApplicationDependencies.getMessageNotifier().updateNotification(requireActivity);
                }
                ConversationUtil.refreshRecipientShortcuts();
            }
        }.executeOnExecutor(SignalExecutors.BOUNDED, Long.valueOf(j));
    }

    /* loaded from: classes4.dex */
    public class PaymentNotificationListener implements UnreadPaymentsView.Listener {
        private final UnreadPayments unreadPayments;

        private PaymentNotificationListener(UnreadPayments unreadPayments) {
            ConversationListFragment.this = r1;
            this.unreadPayments = unreadPayments;
        }

        @Override // org.thoughtcrime.securesms.components.UnreadPaymentsView.Listener
        public void onOpenPaymentsNotificationClicked() {
            UUID paymentUuid = this.unreadPayments.getPaymentUuid();
            if (paymentUuid == null) {
                goToPaymentsHome();
            } else {
                goToSinglePayment(paymentUuid);
            }
        }

        @Override // org.thoughtcrime.securesms.components.UnreadPaymentsView.Listener
        public void onClosePaymentsNotificationClicked() {
            ConversationListFragment.this.viewModel.onUnreadPaymentsClosed();
        }

        private void goToPaymentsHome() {
            ConversationListFragment.this.startActivity(new Intent(ConversationListFragment.this.requireContext(), PaymentsActivity.class));
        }

        private void goToSinglePayment(UUID uuid) {
            Intent intent = new Intent(ConversationListFragment.this.requireContext(), PaymentsActivity.class);
            intent.putExtra(PaymentsActivity.EXTRA_PAYMENTS_STARTING_ACTION, R.id.action_directly_to_paymentDetails);
            intent.putExtra(PaymentsActivity.EXTRA_STARTING_ARGUMENTS, new PaymentDetailsFragmentArgs.Builder(PaymentDetailsParcelable.forUuid(uuid)).build().toBundle());
            ConversationListFragment.this.startActivity(intent);
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public class ArchiveListenerCallback extends ItemTouchHelper.SimpleCallback {
        private static final float MAX_ICON_SCALE;
        private static final float MIN_ICON_SCALE;
        private static final long SWIPE_ANIMATION_DURATION;
        private final float ESCAPE_VELOCITY = ((float) ViewUtil.dpToPx(MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH));
        private final float VELOCITY_THRESHOLD = ((float) ViewUtil.dpToPx(MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH));
        private final int archiveColorEnd;
        private final int archiveColorStart;
        private WeakReference<RecyclerView.ViewHolder> lastTouched;

        @Override // androidx.recyclerview.widget.ItemTouchHelper.Callback
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2) {
            return false;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        ArchiveListenerCallback(int i, int i2) {
            super(0, 32);
            ConversationListFragment.this = r2;
            this.archiveColorStart = i;
            this.archiveColorEnd = i2;
        }

        @Override // androidx.recyclerview.widget.ItemTouchHelper.Callback
        public float getSwipeEscapeVelocity(float f) {
            return Math.min(this.ESCAPE_VELOCITY, this.VELOCITY_THRESHOLD);
        }

        @Override // androidx.recyclerview.widget.ItemTouchHelper.Callback
        public float getSwipeVelocityThreshold(float f) {
            return this.VELOCITY_THRESHOLD;
        }

        @Override // androidx.recyclerview.widget.ItemTouchHelper.SimpleCallback
        public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            if ((viewHolder.itemView instanceof ConversationListItemAction) || (viewHolder instanceof ConversationListAdapter.HeaderViewHolder) || ConversationListFragment.this.actionMode != null || viewHolder.itemView.isSelected() || ConversationListFragment.this.activeAdapter == ConversationListFragment.this.searchAdapter) {
                return 0;
            }
            this.lastTouched = new WeakReference<>(viewHolder);
            return super.getSwipeDirs(recyclerView, viewHolder);
        }

        @Override // androidx.recyclerview.widget.ItemTouchHelper.Callback
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int i) {
            if (this.lastTouched != null) {
                Log.w(ConversationListFragment.TAG, "Falling back to slower onSwiped() event.");
                onTrueSwipe(viewHolder);
                this.lastTouched = null;
            }
        }

        @Override // androidx.recyclerview.widget.ItemTouchHelper.Callback
        public long getAnimationDuration(RecyclerView recyclerView, int i, float f, float f2) {
            WeakReference<RecyclerView.ViewHolder> weakReference;
            if (i == 2 && (weakReference = this.lastTouched) != null && weakReference.get() != null) {
                onTrueSwipe(this.lastTouched.get());
                this.lastTouched = null;
                return SWIPE_ANIMATION_DURATION;
            } else if (i != 4) {
                return SWIPE_ANIMATION_DURATION;
            } else {
                this.lastTouched = null;
                return SWIPE_ANIMATION_DURATION;
            }
        }

        private void onTrueSwipe(RecyclerView.ViewHolder viewHolder) {
            ConversationListFragment.this.onItemSwiped(((ConversationListItem) viewHolder.itemView).getThreadId(), ((ConversationListItem) viewHolder.itemView).getUnreadCount());
        }

        @Override // androidx.recyclerview.widget.ItemTouchHelper.Callback
        public void onChildDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float f, float f2, int i, boolean z) {
            float abs = Math.abs(f);
            if (i == 1) {
                Resources resources = ConversationListFragment.this.getResources();
                View view = viewHolder.itemView;
                ArgbEvaluatorCompat instance = ArgbEvaluatorCompat.getInstance();
                float f3 = MAX_ICON_SCALE;
                int intValue = instance.evaluate(Math.min((float) MAX_ICON_SCALE, (abs / ((float) view.getWidth())) * 4.0f), Integer.valueOf(this.archiveColorStart), Integer.valueOf(this.archiveColorEnd)).intValue();
                DimensionUnit dimensionUnit = DimensionUnit.DP;
                float pixels = dimensionUnit.toPixels(48.0f);
                float pixels2 = dimensionUnit.toPixels(96.0f);
                if (abs < pixels) {
                    f3 = MIN_ICON_SCALE;
                } else if (abs <= pixels2) {
                    f3 = Math.min((float) MAX_ICON_SCALE, (((abs - pixels) / (pixels2 - pixels)) * 0.14999998f) + MIN_ICON_SCALE);
                }
                if (abs > 0.0f) {
                    if (ConversationListFragment.this.archiveDrawable == null) {
                        ConversationListFragment conversationListFragment = ConversationListFragment.this;
                        Drawable drawable = AppCompatResources.getDrawable(conversationListFragment.requireContext(), ConversationListFragment.this.getArchiveIconRes());
                        Objects.requireNonNull(drawable);
                        conversationListFragment.archiveDrawable = drawable;
                        ConversationListFragment.this.archiveDrawable.setColorFilter(new SimpleColorFilter(ContextCompat.getColor(ConversationListFragment.this.requireContext(), R.color.signal_colorOnPrimary)));
                        ConversationListFragment.this.archiveDrawable.setBounds(0, 0, ConversationListFragment.this.archiveDrawable.getIntrinsicWidth(), ConversationListFragment.this.archiveDrawable.getIntrinsicHeight());
                    }
                    canvas.save();
                    canvas.clipRect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
                    canvas.drawColor(intValue);
                    float dimension = resources.getDimension(R.dimen.dsl_settings_gutter);
                    float dimension2 = resources.getDimension(R.dimen.conversation_list_fragment_archive_padding);
                    if (ViewUtil.isLtr(ConversationListFragment.this.requireContext())) {
                        canvas.translate(((float) view.getLeft()) + dimension + dimension2, ((float) view.getTop()) + (((float) ((view.getBottom() - view.getTop()) - ConversationListFragment.this.archiveDrawable.getIntrinsicHeight())) / 2.0f));
                    } else {
                        canvas.translate((((float) view.getRight()) - dimension) - dimension2, ((float) view.getTop()) + (((float) ((view.getBottom() - view.getTop()) - ConversationListFragment.this.archiveDrawable.getIntrinsicHeight())) / 2.0f));
                    }
                    canvas.scale(f3, f3, ((float) ConversationListFragment.this.archiveDrawable.getIntrinsicWidth()) / 2.0f, ((float) ConversationListFragment.this.archiveDrawable.getIntrinsicHeight()) / 2.0f);
                    ConversationListFragment.this.archiveDrawable.draw(canvas);
                    canvas.restore();
                    ViewCompat.setElevation(viewHolder.itemView, dimensionUnit.toPixels(4.0f));
                } else if (abs == 0.0f) {
                    ViewCompat.setElevation(viewHolder.itemView, dimensionUnit.toPixels(0.0f));
                }
                viewHolder.itemView.setTranslationX(f);
                return;
            }
            super.onChildDraw(canvas, recyclerView, viewHolder, f, f2, i, z);
        }

        @Override // androidx.recyclerview.widget.ItemTouchHelper.Callback
        public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            super.clearView(recyclerView, viewHolder);
            ViewCompat.setElevation(viewHolder.itemView, 0.0f);
            this.lastTouched = null;
            View view = ConversationListFragment.this.getView();
            if (view != null) {
                ConversationListFragment.this.itemAnimator.postDisable(view.getHandler());
            } else {
                ConversationListFragment.this.itemAnimator.disable();
            }
        }
    }

    /* loaded from: classes4.dex */
    public final class VoiceNotePlayerViewListener implements VoiceNotePlayerView.Listener {
        private VoiceNotePlayerViewListener() {
            ConversationListFragment.this = r1;
        }

        @Override // org.thoughtcrime.securesms.components.voice.VoiceNotePlayerView.Listener
        public void onCloseRequested(Uri uri) {
            if (ConversationListFragment.this.voiceNotePlayerViewStub.resolved()) {
                ConversationListFragment.this.mediaControllerOwner.getVoiceNoteMediaController().stopPlaybackAndReset(uri);
            }
        }

        @Override // org.thoughtcrime.securesms.components.voice.VoiceNotePlayerView.Listener
        public void onSpeedChangeRequested(Uri uri, float f) {
            ConversationListFragment.this.mediaControllerOwner.getVoiceNoteMediaController().setPlaybackSpeed(uri, f);
        }

        @Override // org.thoughtcrime.securesms.components.voice.VoiceNotePlayerView.Listener
        public void onPlay(Uri uri, long j, double d) {
            ConversationListFragment.this.mediaControllerOwner.getVoiceNoteMediaController().startSinglePlayback(uri, j, d);
        }

        @Override // org.thoughtcrime.securesms.components.voice.VoiceNotePlayerView.Listener
        public void onPause(Uri uri) {
            ConversationListFragment.this.mediaControllerOwner.getVoiceNoteMediaController().pausePlayback(uri);
        }

        @Override // org.thoughtcrime.securesms.components.voice.VoiceNotePlayerView.Listener
        public void onNavigateToMessage(long j, RecipientId recipientId, RecipientId recipientId2, long j2, long j3) {
            MainNavigator.get(ConversationListFragment.this.requireActivity()).goToConversation(recipientId, j, 2, (int) j3);
        }
    }
}
