package org.thoughtcrime.securesms.conversationlist;

import android.view.View;
import org.thoughtcrime.securesms.conversationlist.ConversationListSearchAdapter;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationListSearchAdapter$SearchResultViewHolder$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ ConversationListSearchAdapter.EventListener f$0;
    public final /* synthetic */ Recipient f$1;

    public /* synthetic */ ConversationListSearchAdapter$SearchResultViewHolder$$ExternalSyntheticLambda2(ConversationListSearchAdapter.EventListener eventListener, Recipient recipient) {
        this.f$0 = eventListener;
        this.f$1 = recipient;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.onContactClicked(this.f$1);
    }
}
