package org.thoughtcrime.securesms.conversationlist;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Collections;
import java.util.Locale;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversationlist.model.ConversationSet;
import org.thoughtcrime.securesms.database.model.ThreadRecord;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.search.MessageResult;
import org.thoughtcrime.securesms.search.SearchResult;
import org.thoughtcrime.securesms.util.StickyHeaderDecoration;

/* loaded from: classes4.dex */
public class ConversationListSearchAdapter extends RecyclerView.Adapter<SearchResultViewHolder> implements StickyHeaderDecoration.StickyHeaderAdapter<HeaderViewHolder> {
    private static final int TYPE_CONTACTS;
    private static final int TYPE_CONVERSATIONS;
    private static final int TYPE_MESSAGES;
    private final EventListener eventListener;
    private final GlideRequests glideRequests;
    private final LifecycleOwner lifecycleOwner;
    private final Locale locale;
    private SearchResult searchResult = SearchResult.EMPTY;

    /* loaded from: classes4.dex */
    public interface EventListener {
        void onContactClicked(Recipient recipient);

        void onConversationClicked(ThreadRecord threadRecord);

        void onMessageClicked(MessageResult messageResult);
    }

    public ConversationListSearchAdapter(LifecycleOwner lifecycleOwner, GlideRequests glideRequests, EventListener eventListener, Locale locale) {
        this.lifecycleOwner = lifecycleOwner;
        this.glideRequests = glideRequests;
        this.eventListener = eventListener;
        this.locale = locale;
    }

    public SearchResultViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new SearchResultViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.conversation_list_item_view, viewGroup, false));
    }

    public void onBindViewHolder(SearchResultViewHolder searchResultViewHolder, int i) {
        ThreadRecord conversationResult = getConversationResult(i);
        if (conversationResult != null) {
            searchResultViewHolder.bind(this.lifecycleOwner, conversationResult, this.glideRequests, this.eventListener, this.locale, this.searchResult.getQuery());
            return;
        }
        Recipient contactResult = getContactResult(i);
        if (contactResult != null) {
            searchResultViewHolder.bind(this.lifecycleOwner, contactResult, this.glideRequests, this.eventListener, this.locale, this.searchResult.getQuery());
            return;
        }
        MessageResult messageResult = getMessageResult(i);
        if (messageResult != null) {
            searchResultViewHolder.bind(this.lifecycleOwner, messageResult, this.glideRequests, this.eventListener, this.locale, this.searchResult.getQuery());
        }
    }

    public void onViewRecycled(SearchResultViewHolder searchResultViewHolder) {
        searchResultViewHolder.recycle();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.searchResult.size();
    }

    @Override // org.thoughtcrime.securesms.util.StickyHeaderDecoration.StickyHeaderAdapter
    public long getHeaderId(int i) {
        if (getConversationResult(i) != null) {
            return 1;
        }
        return getContactResult(i) != null ? 2 : 3;
    }

    public HeaderViewHolder onCreateHeaderViewHolder(ViewGroup viewGroup, int i, int i2) {
        return new HeaderViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.dsl_section_header, viewGroup, false));
    }

    public void onBindHeaderViewHolder(HeaderViewHolder headerViewHolder, int i, int i2) {
        headerViewHolder.bind((int) getHeaderId(i));
    }

    public void updateResults(SearchResult searchResult) {
        this.searchResult = searchResult;
        notifyDataSetChanged();
    }

    private ThreadRecord getConversationResult(int i) {
        if (i < this.searchResult.getConversations().size()) {
            return this.searchResult.getConversations().get(i);
        }
        return null;
    }

    private Recipient getContactResult(int i) {
        if (i < getFirstContactIndex() || i >= getFirstMessageIndex()) {
            return null;
        }
        return this.searchResult.getContacts().get(i - getFirstContactIndex());
    }

    private MessageResult getMessageResult(int i) {
        if (i < getFirstMessageIndex() || i >= this.searchResult.size()) {
            return null;
        }
        return this.searchResult.getMessages().get(i - getFirstMessageIndex());
    }

    private int getFirstContactIndex() {
        return this.searchResult.getConversations().size();
    }

    private int getFirstMessageIndex() {
        return getFirstContactIndex() + this.searchResult.getContacts().size();
    }

    /* loaded from: classes4.dex */
    public static class SearchResultViewHolder extends RecyclerView.ViewHolder {
        private final ConversationListItem root;

        SearchResultViewHolder(View view) {
            super(view);
            this.root = (ConversationListItem) view;
        }

        void bind(LifecycleOwner lifecycleOwner, ThreadRecord threadRecord, GlideRequests glideRequests, EventListener eventListener, Locale locale, String str) {
            this.root.bindThread(lifecycleOwner, threadRecord, glideRequests, locale, Collections.emptySet(), new ConversationSet(), str);
            this.root.setOnClickListener(new ConversationListSearchAdapter$SearchResultViewHolder$$ExternalSyntheticLambda1(eventListener, threadRecord));
        }

        void bind(LifecycleOwner lifecycleOwner, Recipient recipient, GlideRequests glideRequests, EventListener eventListener, Locale locale, String str) {
            this.root.bindContact(lifecycleOwner, recipient, glideRequests, locale, str);
            this.root.setOnClickListener(new ConversationListSearchAdapter$SearchResultViewHolder$$ExternalSyntheticLambda2(eventListener, recipient));
        }

        void bind(LifecycleOwner lifecycleOwner, MessageResult messageResult, GlideRequests glideRequests, EventListener eventListener, Locale locale, String str) {
            this.root.bindMessage(lifecycleOwner, messageResult, glideRequests, locale, str);
            this.root.setOnClickListener(new ConversationListSearchAdapter$SearchResultViewHolder$$ExternalSyntheticLambda0(eventListener, messageResult));
        }

        void recycle() {
            this.root.unbind();
            this.root.setOnClickListener(null);
        }
    }

    /* loaded from: classes4.dex */
    public static class HeaderViewHolder extends RecyclerView.ViewHolder {
        private TextView titleView;

        public HeaderViewHolder(View view) {
            super(view);
            this.titleView = (TextView) view.findViewById(R.id.section_header);
        }

        public void bind(int i) {
            if (i == 1) {
                this.titleView.setText(R.string.SearchFragment_header_conversations);
            } else if (i == 2) {
                this.titleView.setText(R.string.SearchFragment_header_contacts);
            } else if (i == 3) {
                this.titleView.setText(R.string.SearchFragment_header_messages);
            }
        }
    }
}
