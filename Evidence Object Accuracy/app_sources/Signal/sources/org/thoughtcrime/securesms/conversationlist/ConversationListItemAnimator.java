package org.thoughtcrime.securesms.conversationlist;

import android.os.Handler;
import androidx.recyclerview.widget.DefaultItemAnimator;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public class ConversationListItemAnimator extends DefaultItemAnimator {
    private static final long ANIMATION_DURATION;
    private static final String TAG = Log.tag(ConversationListItemAnimator.class);
    private boolean shouldDisable;

    public ConversationListItemAnimator() {
        setSupportsChangeAnimations(false);
        setMoveDuration(0);
        setAddDuration(0);
    }

    public void enable() {
        setMoveDuration(ANIMATION_DURATION);
        this.shouldDisable = false;
    }

    public void disable() {
        setMoveDuration(0);
    }

    public void postDisable(Handler handler) {
        this.shouldDisable = true;
        handler.postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListItemAnimator$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                ConversationListItemAnimator.this.lambda$postDisable$0();
            }
        }, 50);
    }

    public /* synthetic */ void lambda$postDisable$0() {
        if (this.shouldDisable) {
            setMoveDuration(0);
        } else {
            Log.w(TAG, "Disable was canceled by an enable.");
        }
    }
}
