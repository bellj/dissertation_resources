package org.thoughtcrime.securesms.conversationlist;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewStub;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Set;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.registration.PulsingFloatingActionButton;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.util.ConversationUtil;
import org.thoughtcrime.securesms.util.task.SnackbarAsyncTask;
import org.thoughtcrime.securesms.util.views.Stub;

/* loaded from: classes4.dex */
public class ConversationListArchiveFragment extends ConversationListFragment {
    private PulsingFloatingActionButton cameraFab;
    private View coordinator;
    private Stub<View> emptyState;
    private PulsingFloatingActionButton fab;
    private RecyclerView list;
    private Stub<Toolbar> toolbar;

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListFragment
    protected int getArchiveIconRes() {
        return R.drawable.ic_unarchive_24;
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListFragment
    protected int getArchivedSnackbarTitleRes() {
        return R.plurals.ConversationListFragment_moved_conversations_to_inbox;
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListFragment
    protected boolean isArchived() {
        return true;
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListFragment
    public void updateEmptyState(boolean z) {
    }

    public static ConversationListArchiveFragment newInstance() {
        return new ConversationListArchiveFragment();
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListFragment, org.thoughtcrime.securesms.LoggingFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setHasOptionsMenu(false);
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.toolbar = requireCallback().getBasicToolbar();
        super.onViewCreated(view, bundle);
        this.coordinator = view.findViewById(R.id.coordinator);
        this.list = (RecyclerView) view.findViewById(R.id.list);
        this.emptyState = new Stub<>((ViewStub) view.findViewById(R.id.empty_state));
        this.fab = (PulsingFloatingActionButton) view.findViewById(R.id.fab);
        this.cameraFab = (PulsingFloatingActionButton) view.findViewById(R.id.camera_fab);
        this.toolbar.get().setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListArchiveFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ConversationListArchiveFragment.$r8$lambda$I7kfFstH3c6Udr5NP5YQsAMhSzY(ConversationListArchiveFragment.this, view2);
            }
        });
        this.toolbar.get().setTitle(R.string.AndroidManifest_archived_conversations);
        this.fab.hide();
        this.cameraFab.hide();
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        NavHostFragment.findNavController(this).popBackStack();
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListFragment
    protected void onPostSubmitList(int i) {
        this.list.setVisibility(0);
        if (this.emptyState.resolved()) {
            this.emptyState.get().setVisibility(8);
        }
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListFragment
    protected Toolbar getToolbar(View view) {
        return this.toolbar.get();
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListFragment
    protected void archiveThreads(Set<Long> set) {
        SignalDatabase.threads().setArchived(set, false);
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListFragment
    protected void reverseArchiveThreads(Set<Long> set) {
        SignalDatabase.threads().setArchived(set, true);
    }

    @Override // org.thoughtcrime.securesms.conversationlist.ConversationListFragment
    protected void onItemSwiped(final long j, int i) {
        this.archiveDecoration.onArchiveStarted();
        this.itemAnimator.enable();
        new SnackbarAsyncTask<Long>(getViewLifecycleOwner().getLifecycle(), this.coordinator, getResources().getQuantityString(R.plurals.ConversationListFragment_moved_conversations_to_inbox, 1, 1), getString(R.string.ConversationListFragment_undo), getResources().getColor(R.color.amber_500), 0, false) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListArchiveFragment.1
            public void executeAction(Long l) {
                SignalDatabase.threads().unarchiveConversation(j);
                ConversationUtil.refreshRecipientShortcuts();
            }

            public void reverseAction(Long l) {
                SignalDatabase.threads().archiveConversation(j);
                ConversationUtil.refreshRecipientShortcuts();
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Long.valueOf(j));
    }
}
