package org.thoughtcrime.securesms.conversationlist.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.CollectionToArray;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.markers.KMappedMarker;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.contacts.ContactRepository;

/* compiled from: ConversationSet.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u001e\n\u0002\b\u0004\n\u0002\u0010(\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0017\b\u0007\u0012\u000e\b\u0002\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001¢\u0006\u0002\u0010\u0004J\u0011\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0002H\u0003J\u0017\u0010\u0013\u001a\u00020\u00112\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00020\u0015H\u0001J\u000e\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u0017\u001a\u00020\u000bJ\t\u0010\u0018\u001a\u00020\u0011H\u0001J\u000f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00020\u001aH\u0003R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0005\u001a\u00020\u0006X\u0005¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR!\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n8BX\u0002¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\f\u0010\r¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/conversationlist/model/ConversationSet;", "", "Lorg/thoughtcrime/securesms/conversationlist/model/Conversation;", "conversations", "(Ljava/util/Set;)V", MediaPreviewActivity.SIZE_EXTRA, "", "getSize", "()I", "threadIds", "", "", "getThreadIds", "()Ljava/util/List;", "threadIds$delegate", "Lkotlin/Lazy;", "contains", "", "element", "containsAll", "elements", "", "containsThreadId", ContactRepository.ID_COLUMN, "isEmpty", "iterator", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ConversationSet implements Set<Conversation>, KMappedMarker {
    private final Set<Conversation> conversations;
    private final Lazy threadIds$delegate;

    public ConversationSet() {
        this(null, 1, null);
    }

    @Override // java.util.Set, java.util.Collection
    public /* bridge */ /* synthetic */ boolean add(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean add(Conversation conversation) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @Override // java.util.Set, java.util.Collection
    public boolean addAll(Collection<? extends Conversation> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @Override // java.util.Set, java.util.Collection
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean contains(Conversation conversation) {
        Intrinsics.checkNotNullParameter(conversation, "element");
        return this.conversations.contains(conversation);
    }

    @Override // java.util.Set, java.util.Collection
    public boolean containsAll(Collection<? extends Object> collection) {
        Intrinsics.checkNotNullParameter(collection, "elements");
        return this.conversations.containsAll(collection);
    }

    public int getSize() {
        return this.conversations.size();
    }

    @Override // java.util.Set, java.util.Collection
    public boolean isEmpty() {
        return this.conversations.isEmpty();
    }

    @Override // java.util.Set, java.util.Collection, java.lang.Iterable
    public Iterator<Conversation> iterator() {
        return this.conversations.iterator();
    }

    @Override // java.util.Set, java.util.Collection
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @Override // java.util.Set, java.util.Collection
    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @Override // java.util.Set, java.util.Collection
    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @Override // java.util.Set, java.util.Collection
    public Object[] toArray() {
        return CollectionToArray.toArray(this);
    }

    @Override // java.util.Set, java.util.Collection
    public <T> T[] toArray(T[] tArr) {
        Intrinsics.checkNotNullParameter(tArr, "array");
        return (T[]) CollectionToArray.toArray(this, tArr);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.Set<? extends org.thoughtcrime.securesms.conversationlist.model.Conversation> */
    /* JADX WARN: Multi-variable type inference failed */
    public ConversationSet(Set<? extends Conversation> set) {
        Intrinsics.checkNotNullParameter(set, "conversations");
        this.conversations = set;
        this.threadIds$delegate = LazyKt__LazyJVMKt.lazy(new Function0<List<? extends Long>>(this) { // from class: org.thoughtcrime.securesms.conversationlist.model.ConversationSet$threadIds$2
            final /* synthetic */ ConversationSet this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.util.List<java.lang.Long>' to match base method */
            @Override // kotlin.jvm.functions.Function0
            public final List<? extends Long> invoke() {
                Set<Conversation> set2 = this.this$0.conversations;
                ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(set2, 10));
                for (Conversation conversation : set2) {
                    arrayList.add(Long.valueOf(conversation.getThreadRecord().getThreadId()));
                }
                return arrayList;
            }
        });
    }

    @Override // java.util.Set, java.util.Collection
    public final /* bridge */ boolean contains(Object obj) {
        if (!(obj instanceof Conversation)) {
            return false;
        }
        return contains((Conversation) obj);
    }

    @Override // java.util.Set, java.util.Collection
    public final /* bridge */ int size() {
        return getSize();
    }

    public /* synthetic */ ConversationSet(Set set, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? SetsKt__SetsKt.emptySet() : set);
    }

    private final List<Long> getThreadIds() {
        return (List) this.threadIds$delegate.getValue();
    }

    public final boolean containsThreadId(long j) {
        return getThreadIds().contains(Long.valueOf(j));
    }
}
