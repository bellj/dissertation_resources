package org.thoughtcrime.securesms.conversationlist;

import android.app.Application;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.BackpressureStrategy;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.functions.BiFunction;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;
import j$.util.Optional;
import j$.util.function.Consumer;
import j$.util.function.Function;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.signal.paging.LivePagedData;
import org.signal.paging.PagedData;
import org.signal.paging.PagedDataSource;
import org.signal.paging.PagingConfig;
import org.signal.paging.PagingController;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository;
import org.thoughtcrime.securesms.conversationlist.model.Conversation;
import org.thoughtcrime.securesms.conversationlist.model.ConversationSet;
import org.thoughtcrime.securesms.conversationlist.model.UnreadPayments;
import org.thoughtcrime.securesms.conversationlist.model.UnreadPaymentsLiveData;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.megaphone.Megaphone;
import org.thoughtcrime.securesms.megaphone.MegaphoneRepository;
import org.thoughtcrime.securesms.megaphone.Megaphones;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.payments.UnreadPaymentsRepository;
import org.thoughtcrime.securesms.search.ContactSearchResult;
import org.thoughtcrime.securesms.search.MessageSearchResult;
import org.thoughtcrime.securesms.search.SearchRepository;
import org.thoughtcrime.securesms.search.SearchResult;
import org.thoughtcrime.securesms.search.ThreadSearchResult;
import org.thoughtcrime.securesms.util.Debouncer;
import org.thoughtcrime.securesms.util.ThrottledDebouncer;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.thoughtcrime.securesms.util.paging.Invalidator;
import org.whispersystems.signalservice.api.websocket.WebSocketConnectionState;

/* loaded from: classes4.dex */
public class ConversationListViewModel extends ViewModel {
    private static final String TAG = Log.tag(ConversationListViewModel.class);
    private static boolean coldStart = true;
    private String activeQuery;
    private SearchResult activeSearchResult;
    private final Debouncer contactSearchDebouncer;
    private final ConversationListDataSource conversationListDataSource;
    private final CompositeDisposable disposables;
    private final LiveData<Boolean> hasNoConversations;
    private final Set<Conversation> internalSelection;
    private final Invalidator invalidator;
    private final MutableLiveData<Megaphone> megaphone;
    private final MegaphoneRepository megaphoneRepository;
    private final Debouncer messageSearchDebouncer;
    private final NotificationProfilesRepository notificationProfilesRepository;
    private final DatabaseObserver.Observer observer;
    private final LivePagedData<Long, Conversation> pagedData;
    private int pinnedCount;
    private final SearchRepository searchRepository;
    private final MutableLiveData<SearchResult> searchResult;
    private final MutableLiveData<ConversationSet> selectedConversations;
    private final UnreadPaymentsLiveData unreadPaymentsLiveData;
    private final UnreadPaymentsRepository unreadPaymentsRepository;
    private final ThrottledDebouncer updateDebouncer;

    public static /* synthetic */ List lambda$getNotificationProfiles$3(Long l, List list) throws Throwable {
        return list;
    }

    private ConversationListViewModel(Application application, SearchRepository searchRepository, boolean z) {
        this.megaphone = new MutableLiveData<>();
        this.searchResult = new MutableLiveData<>();
        this.internalSelection = new HashSet();
        this.selectedConversations = new MutableLiveData<>(new ConversationSet());
        this.searchRepository = searchRepository;
        this.megaphoneRepository = ApplicationDependencies.getMegaphoneRepository();
        this.unreadPaymentsRepository = new UnreadPaymentsRepository();
        this.notificationProfilesRepository = new NotificationProfilesRepository();
        this.messageSearchDebouncer = new Debouncer(500);
        this.contactSearchDebouncer = new Debouncer(100);
        this.updateDebouncer = new ThrottledDebouncer(500);
        this.activeSearchResult = SearchResult.EMPTY;
        this.invalidator = new Invalidator();
        this.disposables = new CompositeDisposable();
        ConversationListDataSource create = ConversationListDataSource.create(application, z);
        this.conversationListDataSource = create;
        LivePagedData<Long, Conversation> createForLiveData = PagedData.createForLiveData(create, new PagingConfig.Builder().setPageSize(15).setBufferPages(2).build());
        this.pagedData = createForLiveData;
        this.unreadPaymentsLiveData = new UnreadPaymentsLiveData();
        ConversationListViewModel$$ExternalSyntheticLambda2 conversationListViewModel$$ExternalSyntheticLambda2 = new DatabaseObserver.Observer() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListViewModel$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
            public final void onChanged() {
                ConversationListViewModel.this.lambda$new$1();
            }
        };
        this.observer = conversationListViewModel$$ExternalSyntheticLambda2;
        this.hasNoConversations = LiveDataUtil.mapAsync(createForLiveData.getData(), new Function() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListViewModel$$ExternalSyntheticLambda3
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ConversationListViewModel.this.lambda$new$2((List) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        ApplicationDependencies.getDatabaseObserver().registerConversationListObserver(conversationListViewModel$$ExternalSyntheticLambda2);
    }

    public /* synthetic */ void lambda$new$1() {
        this.updateDebouncer.publish(new Runnable() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListViewModel$$ExternalSyntheticLambda4
            @Override // java.lang.Runnable
            public final void run() {
                ConversationListViewModel.this.lambda$new$0();
            }
        });
    }

    public /* synthetic */ void lambda$new$0() {
        if (!TextUtils.isEmpty(this.activeQuery)) {
            onSearchQueryUpdated(this.activeQuery);
        }
        this.pagedData.getController().onDataInvalidated();
    }

    public /* synthetic */ Boolean lambda$new$2(List list) {
        this.pinnedCount = SignalDatabase.threads().getPinnedConversationListCount();
        if (list.size() > 0) {
            return Boolean.FALSE;
        }
        return Boolean.valueOf(SignalDatabase.threads().getArchivedConversationListCount() == 0);
    }

    public LiveData<Boolean> hasNoConversations() {
        return this.hasNoConversations;
    }

    public LiveData<SearchResult> getSearchResult() {
        return this.searchResult;
    }

    public LiveData<Megaphone> getMegaphone() {
        return this.megaphone;
    }

    public LiveData<List<Conversation>> getConversationList() {
        return this.pagedData.getData();
    }

    public PagingController getPagingController() {
        return this.pagedData.getController();
    }

    public LiveData<List<NotificationProfile>> getNotificationProfiles() {
        return LiveDataReactiveStreams.fromPublisher(Observable.combineLatest(Observable.interval(0, 30, TimeUnit.SECONDS), this.notificationProfilesRepository.getProfiles(), new BiFunction() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListViewModel$$ExternalSyntheticLambda9
            @Override // io.reactivex.rxjava3.functions.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return ConversationListViewModel.lambda$getNotificationProfiles$3((Long) obj, (List) obj2);
            }
        }).toFlowable(BackpressureStrategy.LATEST));
    }

    public LiveData<WebSocketConnectionState> getPipeState() {
        return LiveDataReactiveStreams.fromPublisher(ApplicationDependencies.getSignalWebSocket().getWebSocketState().toFlowable(BackpressureStrategy.LATEST));
    }

    public LiveData<Optional<UnreadPayments>> getUnreadPaymentsLiveData() {
        return this.unreadPaymentsLiveData;
    }

    public int getPinnedCount() {
        return this.pinnedCount;
    }

    public void onVisible() {
        MegaphoneRepository megaphoneRepository = this.megaphoneRepository;
        MutableLiveData<Megaphone> mutableLiveData = this.megaphone;
        Objects.requireNonNull(mutableLiveData);
        megaphoneRepository.getNextMegaphone(new MegaphoneRepository.Callback() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListViewModel$$ExternalSyntheticLambda8
            @Override // org.thoughtcrime.securesms.megaphone.MegaphoneRepository.Callback
            public final void onResult(Object obj) {
                MutableLiveData.this.postValue((Megaphone) obj);
            }
        });
        if (!coldStart) {
            ApplicationDependencies.getDatabaseObserver().notifyConversationListListeners();
        }
        coldStart = false;
    }

    public Set<Conversation> currentSelectedConversations() {
        return this.internalSelection;
    }

    public LiveData<ConversationSet> getSelectedConversations() {
        return this.selectedConversations;
    }

    public void startSelection(Conversation conversation) {
        setSelection(Collections.singleton(conversation));
    }

    public void endSelection() {
        setSelection(Collections.emptySet());
    }

    public void toggleConversationSelected(Conversation conversation) {
        HashSet hashSet = new HashSet(this.internalSelection);
        if (hashSet.contains(conversation)) {
            hashSet.remove(conversation);
        } else {
            hashSet.add(conversation);
        }
        setSelection(hashSet);
    }

    public void setSelection(Collection<Conversation> collection) {
        this.internalSelection.clear();
        this.internalSelection.addAll(collection);
        this.selectedConversations.setValue(new ConversationSet(this.internalSelection));
    }

    public void onSelectAllClick() {
        this.disposables.add(Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListViewModel$$ExternalSyntheticLambda6
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return ConversationListViewModel.this.lambda$onSelectAllClick$4();
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListViewModel$$ExternalSyntheticLambda7
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ConversationListViewModel.this.setSelection((List) obj);
            }
        }));
    }

    public /* synthetic */ List lambda$onSelectAllClick$4() throws Exception {
        ConversationListDataSource conversationListDataSource = this.conversationListDataSource;
        int size = conversationListDataSource.size();
        CompositeDisposable compositeDisposable = this.disposables;
        Objects.requireNonNull(compositeDisposable);
        return conversationListDataSource.load(0, size, new PagedDataSource.CancellationSignal() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListViewModel$$ExternalSyntheticLambda12
            @Override // org.signal.paging.PagedDataSource.CancellationSignal
            public final boolean isCanceled() {
                return CompositeDisposable.this.isDisposed();
            }
        });
    }

    public void onMegaphoneCompleted(Megaphones.Event event) {
        this.megaphone.postValue(null);
        this.megaphoneRepository.markFinished(event);
    }

    public void onMegaphoneSnoozed(Megaphones.Event event) {
        this.megaphoneRepository.markSeen(event);
        this.megaphone.postValue(null);
    }

    public void onMegaphoneVisible(Megaphone megaphone) {
        this.megaphoneRepository.markVisible(megaphone.getEvent());
    }

    public void onUnreadPaymentsClosed() {
        this.unreadPaymentsRepository.markAllPaymentsSeen();
    }

    public void onSearchQueryUpdated(String str) {
        this.activeQuery = str;
        this.contactSearchDebouncer.publish(new Runnable(str) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListViewModel$$ExternalSyntheticLambda10
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationListViewModel.this.lambda$onSearchQueryUpdated$7(this.f$1);
            }
        });
        this.messageSearchDebouncer.publish(new Runnable(str) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListViewModel$$ExternalSyntheticLambda11
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationListViewModel.this.lambda$onSearchQueryUpdated$9(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$onSearchQueryUpdated$7(String str) {
        this.searchRepository.queryThreads(str, new j$.util.function.Consumer() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListViewModel$$ExternalSyntheticLambda0
            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                ConversationListViewModel.this.lambda$onSearchQueryUpdated$5((ThreadSearchResult) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ j$.util.function.Consumer andThen(j$.util.function.Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
        this.searchRepository.queryContacts(str, new j$.util.function.Consumer() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListViewModel$$ExternalSyntheticLambda1
            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                ConversationListViewModel.this.lambda$onSearchQueryUpdated$6((ContactSearchResult) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ j$.util.function.Consumer andThen(j$.util.function.Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
    }

    public /* synthetic */ void lambda$onSearchQueryUpdated$5(ThreadSearchResult threadSearchResult) {
        if (threadSearchResult.getQuery().equals(this.activeQuery)) {
            if (!this.activeSearchResult.getQuery().equals(this.activeQuery)) {
                this.activeSearchResult = SearchResult.EMPTY;
            }
            SearchResult merge = this.activeSearchResult.merge(threadSearchResult);
            this.activeSearchResult = merge;
            this.searchResult.postValue(merge);
        }
    }

    public /* synthetic */ void lambda$onSearchQueryUpdated$6(ContactSearchResult contactSearchResult) {
        if (contactSearchResult.getQuery().equals(this.activeQuery)) {
            if (!this.activeSearchResult.getQuery().equals(this.activeQuery)) {
                this.activeSearchResult = SearchResult.EMPTY;
            }
            SearchResult merge = this.activeSearchResult.merge(contactSearchResult);
            this.activeSearchResult = merge;
            this.searchResult.postValue(merge);
        }
    }

    public /* synthetic */ void lambda$onSearchQueryUpdated$9(String str) {
        this.searchRepository.queryMessages(str, new j$.util.function.Consumer() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListViewModel$$ExternalSyntheticLambda5
            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                ConversationListViewModel.this.lambda$onSearchQueryUpdated$8((MessageSearchResult) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ j$.util.function.Consumer andThen(j$.util.function.Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
    }

    public /* synthetic */ void lambda$onSearchQueryUpdated$8(MessageSearchResult messageSearchResult) {
        if (messageSearchResult.getQuery().equals(this.activeQuery)) {
            if (!this.activeSearchResult.getQuery().equals(this.activeQuery)) {
                this.activeSearchResult = SearchResult.EMPTY;
            }
            SearchResult merge = this.activeSearchResult.merge(messageSearchResult);
            this.activeSearchResult = merge;
            this.searchResult.postValue(merge);
        }
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.invalidator.invalidate();
        this.disposables.dispose();
        this.messageSearchDebouncer.clear();
        this.updateDebouncer.clear();
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(this.observer);
    }

    /* loaded from: classes4.dex */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final boolean isArchived;
        private final String noteToSelfTitle;

        public Factory(boolean z, String str) {
            this.isArchived = z;
            this.noteToSelfTitle = str;
        }

        @Override // androidx.lifecycle.ViewModelProvider.NewInstanceFactory, androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new ConversationListViewModel(ApplicationDependencies.getApplication(), new SearchRepository(this.noteToSelfTitle), this.isArchived));
        }
    }
}
