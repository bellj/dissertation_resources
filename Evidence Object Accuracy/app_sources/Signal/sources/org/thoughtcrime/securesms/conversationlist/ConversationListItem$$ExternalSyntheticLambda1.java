package org.thoughtcrime.securesms.conversationlist;

import android.text.style.CharacterStyle;
import org.thoughtcrime.securesms.util.SearchUtil;
import org.thoughtcrime.securesms.util.SpanUtil;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationListItem$$ExternalSyntheticLambda1 implements SearchUtil.StyleFactory {
    @Override // org.thoughtcrime.securesms.util.SearchUtil.StyleFactory
    public final CharacterStyle create() {
        return SpanUtil.getBoldSpan();
    }
}
