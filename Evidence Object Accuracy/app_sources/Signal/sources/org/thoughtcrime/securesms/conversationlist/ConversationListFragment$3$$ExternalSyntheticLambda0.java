package org.thoughtcrime.securesms.conversationlist;

import org.thoughtcrime.securesms.conversationlist.ConversationListFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationListFragment$3$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ ConversationListFragment.AnonymousClass3 f$0;

    public /* synthetic */ ConversationListFragment$3$$ExternalSyntheticLambda0(ConversationListFragment.AnonymousClass3 r1) {
        this.f$0 = r1;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$onItemRangeInserted$0();
    }
}
