package org.thoughtcrime.securesms.conversationlist;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.TouchDelegate;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.makeramen.roundedimageview.RoundedDrawable;
import j$.util.function.Function;
import java.util.Locale;
import java.util.Set;
import org.signal.core.util.DimensionUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.BindableConversationListItem;
import org.thoughtcrime.securesms.OverlayTransformation;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.Unbindable;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.components.AlertView;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.components.DeliveryStatusView;
import org.thoughtcrime.securesms.components.FromTextView;
import org.thoughtcrime.securesms.components.TypingIndicatorView;
import org.thoughtcrime.securesms.components.emoji.EmojiStrings;
import org.thoughtcrime.securesms.conversationlist.model.ConversationSet;
import org.thoughtcrime.securesms.database.MmsSmsColumns;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.database.model.LiveUpdateMessage;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.ThreadRecord;
import org.thoughtcrime.securesms.database.model.UpdateDescription;
import org.thoughtcrime.securesms.glide.GlideLiveDataTarget;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideRequest;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.recipients.LiveRecipient;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.search.MessageResult;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.ExpirationUtil;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.SearchUtil;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* loaded from: classes4.dex */
public final class ConversationListItem extends ConstraintLayout implements BindableConversationListItem, Unbindable {
    private static final Typeface BOLD_TYPEFACE = Typeface.create("sans-serif-medium", 0);
    private static final Typeface LIGHT_TYPEFACE = Typeface.create("sans-serif", 0);
    private static final String TAG = Log.tag(ConversationListItem.class);
    private AlertView alertView;
    private TextView archivedView;
    private BadgeImageView badge;
    private boolean batchMode;
    private View checkContainer;
    private View checkedView;
    private AvatarImageView contactPhotoImage;
    private final Rect conversationAvatarTouchDelegateBounds;
    private TextView dateView;
    private DeliveryStatusView deliveryStatusIndicator;
    private LiveData<SpannableString> displayBody;
    private final Observer<SpannableString> displayBodyObserver;
    private FromTextView fromView;
    private GlideRequests glideRequests;
    private String highlightSubstring;
    private long lastSeen;
    private Locale locale;
    private final Rect newConversationAvatarTouchDelegateBounds;
    private LiveRecipient recipient;
    private final Observer<Recipient> recipientObserver;
    private TextView subjectView;
    private ThreadRecord thread;
    private long threadId;
    private int thumbSize;
    private GlideLiveDataTarget thumbTarget;
    private Set<Long> typingThreads;
    private TypingIndicatorView typingView;
    private View uncheckedView;
    private int unreadCount;
    private TextView unreadIndicator;

    public ConversationListItem(Context context) {
        this(context, null);
    }

    public ConversationListItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.conversationAvatarTouchDelegateBounds = new Rect();
        this.newConversationAvatarTouchDelegateBounds = new Rect();
        this.recipientObserver = new Observer() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListItem$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationListItem.this.onRecipientChanged((Recipient) obj);
            }
        };
        this.displayBodyObserver = new Observer() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListItem$$ExternalSyntheticLambda3
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationListItem.this.onDisplayBodyChanged((SpannableString) obj);
            }
        };
    }

    @Override // android.view.View
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.subjectView = (TextView) findViewById(R.id.conversation_list_item_summary);
        this.typingView = (TypingIndicatorView) findViewById(R.id.conversation_list_item_typing_indicator);
        this.fromView = (FromTextView) findViewById(R.id.conversation_list_item_name);
        this.dateView = (TextView) findViewById(R.id.conversation_list_item_date);
        this.deliveryStatusIndicator = (DeliveryStatusView) findViewById(R.id.conversation_list_item_status);
        this.alertView = (AlertView) findViewById(R.id.conversation_list_item_alert);
        this.contactPhotoImage = (AvatarImageView) findViewById(R.id.conversation_list_item_avatar);
        this.archivedView = (TextView) findViewById(R.id.conversation_list_item_archived);
        this.unreadIndicator = (TextView) findViewById(R.id.conversation_list_item_unread_indicator);
        this.badge = (BadgeImageView) findViewById(R.id.conversation_list_item_badge);
        this.checkContainer = findViewById(R.id.conversation_list_item_check_container);
        this.uncheckedView = findViewById(R.id.conversation_list_item_unchecked);
        this.checkedView = findViewById(R.id.conversation_list_item_checked);
        int pixels = (int) DimensionUnit.SP.toPixels(16.0f);
        this.thumbSize = pixels;
        this.thumbTarget = new GlideLiveDataTarget(pixels, pixels);
        getLayoutTransition().setDuration(150);
    }

    @Override // androidx.constraintlayout.widget.ConstraintLayout, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        this.contactPhotoImage.getHitRect(this.newConversationAvatarTouchDelegateBounds);
        if (getLayoutDirection() == 0) {
            this.newConversationAvatarTouchDelegateBounds.left = i;
        } else {
            this.newConversationAvatarTouchDelegateBounds.right = i3;
        }
        Rect rect = this.newConversationAvatarTouchDelegateBounds;
        rect.top = i2;
        rect.bottom = i4;
        if (getTouchDelegate() == null || !this.newConversationAvatarTouchDelegateBounds.equals(this.conversationAvatarTouchDelegateBounds)) {
            this.conversationAvatarTouchDelegateBounds.set(this.newConversationAvatarTouchDelegateBounds);
            setTouchDelegate(new TouchDelegate(this.conversationAvatarTouchDelegateBounds, this.contactPhotoImage));
        }
    }

    @Override // org.thoughtcrime.securesms.BindableConversationListItem
    public void bind(LifecycleOwner lifecycleOwner, ThreadRecord threadRecord, GlideRequests glideRequests, Locale locale, Set<Long> set, ConversationSet conversationSet) {
        bindThread(lifecycleOwner, threadRecord, glideRequests, locale, set, conversationSet, null);
    }

    public void bindThread(LifecycleOwner lifecycleOwner, ThreadRecord threadRecord, GlideRequests glideRequests, Locale locale, Set<Long> set, ConversationSet conversationSet, String str) {
        int i;
        this.threadId = threadRecord.getThreadId();
        this.glideRequests = glideRequests;
        this.unreadCount = threadRecord.getUnreadCount();
        this.lastSeen = threadRecord.getLastSeen();
        this.thread = threadRecord;
        this.locale = locale;
        this.highlightSubstring = str;
        observeRecipient(lifecycleOwner, threadRecord.getRecipient().live());
        observeDisplayBody(null, null);
        if (str != null) {
            this.fromView.setText(this.recipient.get(), SearchUtil.getHighlightedSpan(locale, new ConversationListItem$$ExternalSyntheticLambda0(), this.recipient.get().isSelf() ? getContext().getString(R.string.note_to_self) : this.recipient.get().getDisplayName(getContext()), str, 1), true, null);
        } else {
            this.fromView.setText(this.recipient.get(), false);
        }
        this.typingThreads = set;
        updateTypingIndicator(set);
        LiveData<SpannableString> threadDisplayBody = getThreadDisplayBody(getContext(), threadRecord, glideRequests, this.thumbSize, this.thumbTarget);
        setSubjectViewText(threadDisplayBody.getValue());
        observeDisplayBody(lifecycleOwner, threadDisplayBody);
        if (threadRecord.getDate() > 0) {
            this.dateView.setText(DateUtils.getBriefRelativeTimeSpanString(getContext(), locale, threadRecord.getDate()));
            this.dateView.setTypeface(threadRecord.isRead() ? LIGHT_TYPEFACE : BOLD_TYPEFACE);
            TextView textView = this.dateView;
            if (threadRecord.isRead()) {
                i = ContextCompat.getColor(getContext(), R.color.signal_text_secondary);
            } else {
                i = ContextCompat.getColor(getContext(), R.color.signal_text_primary);
            }
            textView.setTextColor(i);
        }
        if (threadRecord.isArchived()) {
            this.archivedView.setVisibility(0);
        } else {
            this.archivedView.setVisibility(8);
        }
        setStatusIcons(threadRecord);
        setSelectedConversations(conversationSet);
        setBadgeFromRecipient(this.recipient.get());
        setUnreadIndicator(threadRecord);
        this.contactPhotoImage.setAvatar(glideRequests, this.recipient.get(), !this.batchMode);
    }

    private void setBadgeFromRecipient(Recipient recipient) {
        if (!recipient.isSelf()) {
            this.badge.setBadgeFromRecipient(recipient);
            this.badge.setClickable(false);
            return;
        }
        this.badge.setBadge(null);
    }

    public void bindContact(LifecycleOwner lifecycleOwner, Recipient recipient, GlideRequests glideRequests, Locale locale, String str) {
        this.glideRequests = glideRequests;
        this.locale = locale;
        this.highlightSubstring = str;
        observeRecipient(lifecycleOwner, recipient.live());
        observeDisplayBody(null, null);
        setSubjectViewText(null);
        this.fromView.setText(recipient, SearchUtil.getHighlightedSpan(locale, new ConversationListItem$$ExternalSyntheticLambda0(), new SpannableString(recipient.getDisplayName(getContext())), str, 1), true, null);
        setSubjectViewText(SearchUtil.getHighlightedSpan(locale, new ConversationListItem$$ExternalSyntheticLambda1(), recipient.getE164().orElse(""), str, 1));
        this.dateView.setText("");
        this.archivedView.setVisibility(8);
        this.unreadIndicator.setVisibility(8);
        this.deliveryStatusIndicator.setNone();
        this.alertView.setNone();
        setSelectedConversations(new ConversationSet());
        setBadgeFromRecipient(this.recipient.get());
        this.contactPhotoImage.setAvatar(glideRequests, this.recipient.get(), !this.batchMode);
    }

    public void bindMessage(LifecycleOwner lifecycleOwner, MessageResult messageResult, GlideRequests glideRequests, Locale locale, String str) {
        this.glideRequests = glideRequests;
        this.locale = locale;
        this.highlightSubstring = str;
        observeRecipient(lifecycleOwner, messageResult.getConversationRecipient().live());
        observeDisplayBody(null, null);
        setSubjectViewText(null);
        this.fromView.setText(this.recipient.get(), false);
        setSubjectViewText(SearchUtil.getHighlightedSpan(locale, new ConversationListItem$$ExternalSyntheticLambda1(), messageResult.getBodySnippet(), str, 1));
        this.dateView.setText(DateUtils.getBriefRelativeTimeSpanString(getContext(), locale, messageResult.getReceivedTimestampMs()));
        this.archivedView.setVisibility(8);
        this.unreadIndicator.setVisibility(8);
        this.deliveryStatusIndicator.setNone();
        this.alertView.setNone();
        setSelectedConversations(new ConversationSet());
        setBadgeFromRecipient(this.recipient.get());
        this.contactPhotoImage.setAvatar(glideRequests, this.recipient.get(), !this.batchMode);
    }

    @Override // org.thoughtcrime.securesms.Unbindable
    public void unbind() {
        if (this.recipient != null) {
            observeRecipient(null, null);
            setSelectedConversations(new ConversationSet());
            this.contactPhotoImage.setAvatar(this.glideRequests, (Recipient) null, !this.batchMode);
        }
        observeDisplayBody(null, null);
    }

    @Override // org.thoughtcrime.securesms.BindableConversationListItem
    public void setSelectedConversations(ConversationSet conversationSet) {
        boolean z = !conversationSet.isEmpty();
        this.batchMode = z;
        boolean z2 = z && conversationSet.containsThreadId(this.thread.getThreadId());
        setSelected(z2);
        LiveRecipient liveRecipient = this.recipient;
        if (liveRecipient != null) {
            this.contactPhotoImage.setAvatar(this.glideRequests, liveRecipient.get(), true ^ this.batchMode);
        }
        boolean z3 = this.batchMode;
        if (z3 && z2) {
            this.checkContainer.setVisibility(0);
            this.uncheckedView.setVisibility(8);
            this.checkedView.setVisibility(0);
        } else if (z3) {
            this.checkContainer.setVisibility(0);
            this.uncheckedView.setVisibility(0);
            this.checkedView.setVisibility(8);
        } else {
            this.checkContainer.setVisibility(8);
            this.uncheckedView.setVisibility(8);
            this.checkedView.setVisibility(8);
        }
    }

    @Override // org.thoughtcrime.securesms.BindableConversationListItem
    public void updateTypingIndicator(Set<Long> set) {
        if (set.contains(Long.valueOf(this.threadId))) {
            this.subjectView.setVisibility(4);
            this.typingView.setVisibility(0);
            this.typingView.startAnimation();
            return;
        }
        this.typingView.setVisibility(8);
        this.typingView.stopAnimation();
        this.subjectView.setVisibility(0);
    }

    public Recipient getRecipient() {
        return this.recipient.get();
    }

    public long getThreadId() {
        return this.threadId;
    }

    public ThreadRecord getThread() {
        return this.thread;
    }

    public int getUnreadCount() {
        return this.unreadCount;
    }

    public long getLastSeen() {
        return this.lastSeen;
    }

    private void observeRecipient(LifecycleOwner lifecycleOwner, LiveRecipient liveRecipient) {
        LiveRecipient liveRecipient2 = this.recipient;
        if (liveRecipient2 != null) {
            liveRecipient2.getLiveData().removeObserver(this.recipientObserver);
        }
        this.recipient = liveRecipient;
        if (lifecycleOwner != null && liveRecipient != null) {
            liveRecipient.getLiveData().observe(lifecycleOwner, this.recipientObserver);
        }
    }

    private void observeDisplayBody(LifecycleOwner lifecycleOwner, LiveData<SpannableString> liveData) {
        GlideRequests glideRequests;
        if (liveData == null && (glideRequests = this.glideRequests) != null) {
            glideRequests.clear(this.thumbTarget);
        }
        LiveData<SpannableString> liveData2 = this.displayBody;
        if (liveData2 != null) {
            liveData2.removeObserver(this.displayBodyObserver);
        }
        this.displayBody = liveData;
        if (lifecycleOwner != null && liveData != null) {
            liveData.observe(lifecycleOwner, this.displayBodyObserver);
        }
    }

    private void setSubjectViewText(CharSequence charSequence) {
        if (charSequence == null) {
            this.subjectView.setText((CharSequence) null);
            return;
        }
        this.subjectView.setText(charSequence);
        this.subjectView.setVisibility(0);
    }

    private void setStatusIcons(ThreadRecord threadRecord) {
        if (MmsSmsColumns.Types.isBadDecryptType(threadRecord.getType())) {
            this.deliveryStatusIndicator.setNone();
            this.alertView.setFailed();
        } else if (!threadRecord.isOutgoing() || threadRecord.isOutgoingAudioCall() || threadRecord.isOutgoingVideoCall() || threadRecord.isVerificationStatusChange()) {
            this.deliveryStatusIndicator.setNone();
            this.alertView.setNone();
        } else if (threadRecord.isFailed()) {
            this.deliveryStatusIndicator.setNone();
            this.alertView.setFailed();
        } else if (threadRecord.isPendingInsecureSmsFallback()) {
            this.deliveryStatusIndicator.setNone();
            this.alertView.setPendingApproval();
        } else {
            this.alertView.setNone();
            if (threadRecord.getExtra() == null || !threadRecord.getExtra().isRemoteDelete()) {
                if (threadRecord.isPending()) {
                    this.deliveryStatusIndicator.setPending();
                } else if (threadRecord.isRemoteRead()) {
                    this.deliveryStatusIndicator.setRead();
                } else if (threadRecord.isDelivered()) {
                    this.deliveryStatusIndicator.setDelivered();
                } else {
                    this.deliveryStatusIndicator.setSent();
                }
            } else if (threadRecord.isPending()) {
                this.deliveryStatusIndicator.setPending();
            } else {
                this.deliveryStatusIndicator.setNone();
            }
        }
    }

    private void setUnreadIndicator(ThreadRecord threadRecord) {
        if ((!threadRecord.isOutgoing() || threadRecord.isForcedUnread()) && !threadRecord.isRead()) {
            TextView textView = this.unreadIndicator;
            int i = this.unreadCount;
            textView.setText(i > 0 ? String.valueOf(i) : " ");
            this.unreadIndicator.setVisibility(0);
            return;
        }
        this.unreadIndicator.setVisibility(8);
    }

    public void onRecipientChanged(Recipient recipient) {
        LiveRecipient liveRecipient = this.recipient;
        if (liveRecipient == null || !liveRecipient.getId().equals(recipient.getId())) {
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Bad change! Local recipient doesn't match. Ignoring. Local: ");
            LiveRecipient liveRecipient2 = this.recipient;
            sb.append(liveRecipient2 == null ? "null" : liveRecipient2.getId());
            sb.append(", Changed: ");
            sb.append(recipient.getId());
            Log.w(str, sb.toString());
            return;
        }
        if (this.highlightSubstring != null) {
            this.fromView.setText(recipient, SearchUtil.getHighlightedSpan(this.locale, new ConversationListItem$$ExternalSyntheticLambda0(), new SpannableString(recipient.isSelf() ? getContext().getString(R.string.note_to_self) : recipient.getDisplayName(getContext())), this.highlightSubstring, 1), true, null);
        } else {
            this.fromView.setText(recipient, false);
        }
        this.contactPhotoImage.setAvatar(this.glideRequests, recipient, true ^ this.batchMode);
        setBadgeFromRecipient(recipient);
    }

    private static LiveData<SpannableString> getThreadDisplayBody(Context context, ThreadRecord threadRecord, GlideRequests glideRequests, int i, GlideLiveDataTarget glideLiveDataTarget) {
        int color = ContextCompat.getColor(context, R.color.signal_text_secondary);
        if (!threadRecord.isMessageRequestAccepted()) {
            return emphasisAdded(context, context.getString(R.string.ThreadRecord_message_request), color);
        }
        if (MmsSmsColumns.Types.isGroupUpdate(threadRecord.getType())) {
            if (threadRecord.getRecipient().isPushV2Group()) {
                return emphasisAdded(context, MessageRecord.getGv2ChangeDescription(context, threadRecord.getBody(), null), color);
            }
            return emphasisAdded(context, context.getString(R.string.ThreadRecord_group_updated), R.drawable.ic_update_group_16, color);
        } else if (MmsSmsColumns.Types.isGroupQuit(threadRecord.getType())) {
            return emphasisAdded(context, context.getString(R.string.ThreadRecord_left_the_group), R.drawable.ic_update_group_leave_16, color);
        } else {
            if (MmsSmsColumns.Types.isKeyExchangeType(threadRecord.getType())) {
                return emphasisAdded(context, context.getString(R.string.ConversationListItem_key_exchange_message), color);
            }
            if (MmsSmsColumns.Types.isChatSessionRefresh(threadRecord.getType())) {
                return emphasisAdded(context, context.getString(R.string.ThreadRecord_chat_session_refreshed), R.drawable.ic_refresh_16, color);
            }
            if (MmsSmsColumns.Types.isNoRemoteSessionType(threadRecord.getType())) {
                return emphasisAdded(context, context.getString(R.string.MessageDisplayHelper_message_encrypted_for_non_existing_session), color);
            }
            if (MmsSmsColumns.Types.isEndSessionType(threadRecord.getType())) {
                return emphasisAdded(context, context.getString(R.string.ThreadRecord_secure_session_reset), color);
            }
            if (MmsSmsColumns.Types.isLegacyType(threadRecord.getType())) {
                return emphasisAdded(context, context.getString(R.string.MessageRecord_message_encrypted_with_a_legacy_protocol_version_that_is_no_longer_supported), color);
            }
            if (MmsSmsColumns.Types.isDraftMessageType(threadRecord.getType())) {
                String string = context.getString(R.string.ThreadRecord_draft);
                return emphasisAdded(context, string + " " + threadRecord.getBody(), color);
            } else if (MmsSmsColumns.Types.isOutgoingAudioCall(threadRecord.getType()) || MmsSmsColumns.Types.isOutgoingVideoCall(threadRecord.getType())) {
                return emphasisAdded(context, context.getString(R.string.ThreadRecord_called), R.drawable.ic_update_audio_call_outgoing_16, color);
            } else {
                if (MmsSmsColumns.Types.isIncomingAudioCall(threadRecord.getType()) || MmsSmsColumns.Types.isIncomingVideoCall(threadRecord.getType())) {
                    return emphasisAdded(context, context.getString(R.string.ThreadRecord_called_you), R.drawable.ic_update_audio_call_incoming_16, color);
                }
                if (MmsSmsColumns.Types.isMissedAudioCall(threadRecord.getType())) {
                    return emphasisAdded(context, context.getString(R.string.ThreadRecord_missed_audio_call), R.drawable.ic_update_audio_call_missed_16, color);
                }
                if (MmsSmsColumns.Types.isMissedVideoCall(threadRecord.getType())) {
                    return emphasisAdded(context, context.getString(R.string.ThreadRecord_missed_video_call), R.drawable.ic_update_video_call_missed_16, color);
                }
                if (MmsSmsColumns.Types.isGroupCall(threadRecord.getType())) {
                    return emphasisAdded(context, MessageRecord.getGroupCallUpdateDescription(context, threadRecord.getBody(), false), color);
                }
                if (MmsSmsColumns.Types.isJoinedType(threadRecord.getType())) {
                    return emphasisAdded(LiveUpdateMessage.recipientToStringAsync(threadRecord.getRecipient().getId(), new Function(context) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListItem$$ExternalSyntheticLambda5
                        public final /* synthetic */ Context f$0;

                        {
                            this.f$0 = r1;
                        }

                        @Override // j$.util.function.Function
                        public /* synthetic */ Function andThen(Function function) {
                            return Function.CC.$default$andThen(this, function);
                        }

                        @Override // j$.util.function.Function
                        public final Object apply(Object obj) {
                            return ConversationListItem.lambda$getThreadDisplayBody$0(this.f$0, (Recipient) obj);
                        }

                        @Override // j$.util.function.Function
                        public /* synthetic */ Function compose(Function function) {
                            return Function.CC.$default$compose(this, function);
                        }
                    }));
                }
                if (MmsSmsColumns.Types.isExpirationTimerUpdate(threadRecord.getType())) {
                    int expiresIn = (int) (threadRecord.getExpiresIn() / 1000);
                    if (expiresIn <= 0) {
                        return emphasisAdded(context, context.getString(R.string.ThreadRecord_disappearing_messages_disabled), R.drawable.ic_update_timer_disabled_16, color);
                    }
                    return emphasisAdded(context, context.getString(R.string.ThreadRecord_disappearing_message_time_updated_to_s, ExpirationUtil.getExpirationDisplayValue(context, expiresIn)), R.drawable.ic_update_timer_16, color);
                } else if (MmsSmsColumns.Types.isIdentityUpdate(threadRecord.getType())) {
                    return emphasisAdded(LiveUpdateMessage.recipientToStringAsync(threadRecord.getRecipient().getId(), new Function(context) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListItem$$ExternalSyntheticLambda6
                        public final /* synthetic */ Context f$0;

                        {
                            this.f$0 = r1;
                        }

                        @Override // j$.util.function.Function
                        public /* synthetic */ Function andThen(Function function) {
                            return Function.CC.$default$andThen(this, function);
                        }

                        @Override // j$.util.function.Function
                        public final Object apply(Object obj) {
                            return ConversationListItem.lambda$getThreadDisplayBody$1(this.f$0, (Recipient) obj);
                        }

                        @Override // j$.util.function.Function
                        public /* synthetic */ Function compose(Function function) {
                            return Function.CC.$default$compose(this, function);
                        }
                    }));
                } else {
                    if (MmsSmsColumns.Types.isIdentityVerified(threadRecord.getType())) {
                        return emphasisAdded(context, context.getString(R.string.ThreadRecord_you_marked_verified), color);
                    }
                    if (MmsSmsColumns.Types.isIdentityDefault(threadRecord.getType())) {
                        return emphasisAdded(context, context.getString(R.string.ThreadRecord_you_marked_unverified), color);
                    }
                    if (MmsSmsColumns.Types.isUnsupportedMessageType(threadRecord.getType())) {
                        return emphasisAdded(context, context.getString(R.string.ThreadRecord_message_could_not_be_processed), color);
                    }
                    if (MmsSmsColumns.Types.isProfileChange(threadRecord.getType())) {
                        return emphasisAdded(context, "", color);
                    }
                    if (MmsSmsColumns.Types.isChangeNumber(threadRecord.getType()) || MmsSmsColumns.Types.isBoostRequest(threadRecord.getType())) {
                        return emphasisAdded(context, "", color);
                    }
                    if (MmsSmsColumns.Types.isBadDecryptType(threadRecord.getType())) {
                        return emphasisAdded(context, context.getString(R.string.ThreadRecord_delivery_issue), color);
                    }
                    ThreadDatabase.Extra extra = threadRecord.getExtra();
                    if (extra != null && extra.isViewOnce()) {
                        return emphasisAdded(context, getViewOnceDescription(context, threadRecord.getContentType()), color);
                    }
                    if (extra == null || !extra.isRemoteDelete()) {
                        String removeNewlines = removeNewlines(threadRecord.getBody());
                        return whileLoadingShow(removeNewlines, Transformations.map(createFinalBodyWithMediaIcon(context, removeNewlines, threadRecord, glideRequests, i, glideLiveDataTarget), new androidx.arch.core.util.Function(context) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListItem$$ExternalSyntheticLambda7
                            public final /* synthetic */ Context f$1;

                            {
                                this.f$1 = r2;
                            }

                            @Override // androidx.arch.core.util.Function
                            public final Object apply(Object obj) {
                                return ConversationListItem.lambda$getThreadDisplayBody$2(ThreadRecord.this, this.f$1, (CharSequence) obj);
                            }
                        }));
                    }
                    return emphasisAdded(context, context.getString(threadRecord.isOutgoing() ? R.string.ThreadRecord_you_deleted_this_message : R.string.ThreadRecord_this_message_was_deleted), color);
                }
            }
        }
    }

    public static /* synthetic */ SpannableString lambda$getThreadDisplayBody$0(Context context, Recipient recipient) {
        return new SpannableString(context.getString(R.string.ThreadRecord_s_is_on_signal, recipient.getDisplayName(context)));
    }

    public static /* synthetic */ SpannableString lambda$getThreadDisplayBody$1(Context context, Recipient recipient) {
        if (recipient.isGroup()) {
            return new SpannableString(context.getString(R.string.ThreadRecord_safety_number_changed));
        }
        return new SpannableString(context.getString(R.string.ThreadRecord_your_safety_number_with_s_has_changed, recipient.getDisplayName(context)));
    }

    public static /* synthetic */ SpannableString lambda$getThreadDisplayBody$2(ThreadRecord threadRecord, Context context, CharSequence charSequence) {
        if (threadRecord.getRecipient().isGroup()) {
            RecipientId groupMessageSender = threadRecord.getGroupMessageSender();
            if (!groupMessageSender.isUnknown()) {
                return createGroupMessageUpdateString(context, charSequence, Recipient.resolved(groupMessageSender));
            }
        }
        return new SpannableString(charSequence);
    }

    private static LiveData<CharSequence> createFinalBodyWithMediaIcon(Context context, String str, ThreadRecord threadRecord, GlideRequests glideRequests, int i, GlideLiveDataTarget glideLiveDataTarget) {
        String str2;
        if (threadRecord.getSnippetUri() == null) {
            return LiveDataUtil.just(str);
        }
        if (str.startsWith(EmojiStrings.GIF)) {
            str2 = str.replaceFirst(EmojiStrings.GIF, "");
        } else if (str.startsWith(EmojiStrings.VIDEO)) {
            str2 = str.replaceFirst(EmojiStrings.VIDEO, "");
        } else if (str.startsWith(EmojiStrings.PHOTO)) {
            str2 = str.replaceFirst(EmojiStrings.PHOTO, "");
        } else if (threadRecord.getExtra() == null || threadRecord.getExtra().getStickerEmoji() == null || !str.startsWith(threadRecord.getExtra().getStickerEmoji())) {
            return LiveDataUtil.just(str);
        } else {
            str2 = str.replaceFirst(threadRecord.getExtra().getStickerEmoji(), "");
        }
        glideRequests.asBitmap().load((Object) new DecryptableStreamUriLoader.DecryptableUri(threadRecord.getSnippetUri())).override(i, i).transform(new OverlayTransformation(ContextCompat.getColor(context, R.color.transparent_black_08)), new CenterCrop()).into((GlideRequest<Bitmap>) glideLiveDataTarget);
        return Transformations.map(glideLiveDataTarget.getLiveData(), new androidx.arch.core.util.Function(str, i, str2) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListItem$$ExternalSyntheticLambda8
            public final /* synthetic */ String f$0;
            public final /* synthetic */ int f$1;
            public final /* synthetic */ String f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ConversationListItem.lambda$createFinalBodyWithMediaIcon$3(this.f$0, this.f$1, this.f$2, (Bitmap) obj);
            }
        });
    }

    public static /* synthetic */ CharSequence lambda$createFinalBodyWithMediaIcon$3(String str, int i, String str2, Bitmap bitmap) {
        if (bitmap == null) {
            return str;
        }
        RoundedDrawable fromBitmap = RoundedDrawable.fromBitmap(bitmap);
        fromBitmap.setBounds(0, 0, i, i);
        fromBitmap.setCornerRadius(DimensionUnit.DP.toPixels(2.0f));
        fromBitmap.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        return new SpannableStringBuilder().append(SpanUtil.buildCenteredImageSpan(fromBitmap)).append((CharSequence) str2);
    }

    private static SpannableString createGroupMessageUpdateString(Context context, CharSequence charSequence, Recipient recipient) {
        String str;
        StringBuilder sb = new StringBuilder();
        if (recipient.isSelf()) {
            str = context.getString(R.string.MessageRecord_you);
        } else {
            str = recipient.getShortDisplayName(context);
        }
        sb.append(str);
        sb.append(": ");
        String sb2 = sb.toString();
        SpannableStringBuilder append = new SpannableStringBuilder(sb2).append(charSequence);
        append.setSpan(SpanUtil.getBoldSpan(), 0, sb2.length(), 33);
        return new SpannableString(append);
    }

    private static LiveData<SpannableString> whileLoadingShow(String str, LiveData<SpannableString> liveData) {
        return LiveDataUtil.until(liveData, LiveDataUtil.delay(250, new SpannableString(str)));
    }

    private static String removeNewlines(String str) {
        if (str == null) {
            return "";
        }
        return str.indexOf(10) >= 0 ? str.replaceAll("\n", " ") : str;
    }

    private static LiveData<SpannableString> emphasisAdded(Context context, String str, int i) {
        return emphasisAdded(context, UpdateDescription.staticDescription(str, 0), i);
    }

    private static LiveData<SpannableString> emphasisAdded(Context context, String str, int i, int i2) {
        return emphasisAdded(context, UpdateDescription.staticDescription(str, i), i2);
    }

    private static LiveData<SpannableString> emphasisAdded(Context context, UpdateDescription updateDescription, int i) {
        return emphasisAdded(LiveUpdateMessage.fromMessageDescription(context, updateDescription, i, false));
    }

    private static LiveData<SpannableString> emphasisAdded(LiveData<SpannableString> liveData) {
        return Transformations.map(liveData, new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListItem$$ExternalSyntheticLambda4
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ConversationListItem.lambda$emphasisAdded$4((SpannableString) obj);
            }
        });
    }

    public static /* synthetic */ SpannableString lambda$emphasisAdded$4(SpannableString spannableString) {
        SpannableString spannableString2 = new SpannableString(spannableString);
        spannableString2.setSpan(new StyleSpan(2), 0, spannableString.length(), 33);
        return spannableString2;
    }

    private static String getViewOnceDescription(Context context, String str) {
        if (MediaUtil.isViewOnceType(str)) {
            return context.getString(R.string.ThreadRecord_view_once_media);
        }
        if (MediaUtil.isVideoType(str)) {
            return context.getString(R.string.ThreadRecord_view_once_video);
        }
        return context.getString(R.string.ThreadRecord_view_once_photo);
    }

    public void onDisplayBodyChanged(SpannableString spannableString) {
        setSubjectViewText(spannableString);
        Set<Long> set = this.typingThreads;
        if (set != null) {
            updateTypingIndicator(set);
        }
    }
}
