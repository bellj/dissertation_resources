package org.thoughtcrime.securesms.conversationlist;

import android.view.View;
import org.thoughtcrime.securesms.conversationlist.ConversationListSearchAdapter;
import org.thoughtcrime.securesms.database.model.ThreadRecord;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationListSearchAdapter$SearchResultViewHolder$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ ConversationListSearchAdapter.EventListener f$0;
    public final /* synthetic */ ThreadRecord f$1;

    public /* synthetic */ ConversationListSearchAdapter$SearchResultViewHolder$$ExternalSyntheticLambda1(ConversationListSearchAdapter.EventListener eventListener, ThreadRecord threadRecord) {
        this.f$0 = eventListener;
        this.f$1 = threadRecord;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.onConversationClicked(this.f$1);
    }
}
