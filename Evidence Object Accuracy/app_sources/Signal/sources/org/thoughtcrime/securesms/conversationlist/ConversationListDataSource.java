package org.thoughtcrime.securesms.conversationlist;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import j$.util.Collection$EL;
import j$.util.stream.Collectors;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.signal.paging.PagedDataSource;
import org.thoughtcrime.securesms.conversationlist.model.Conversation;
import org.thoughtcrime.securesms.conversationlist.model.ConversationReader;
import org.thoughtcrime.securesms.database.MmsSmsColumns;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.ThreadRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.Stopwatch;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public abstract class ConversationListDataSource implements PagedDataSource<Long, Conversation> {
    private static final String TAG = Log.tag(ConversationListDataSource.class);
    protected final ThreadDatabase threadDatabase = SignalDatabase.threads();

    protected abstract Cursor getCursor(long j, long j2);

    protected abstract int getTotalCount();

    protected ConversationListDataSource(Context context) {
    }

    public static ConversationListDataSource create(Context context, boolean z) {
        if (!z) {
            return new UnarchivedConversationListDataSource(context);
        }
        return new ArchivedConversationListDataSource(context);
    }

    @Override // org.signal.paging.PagedDataSource
    public int size() {
        long currentTimeMillis = System.currentTimeMillis();
        int totalCount = getTotalCount();
        String str = TAG;
        Log.d(str, "[size(), " + getClass().getSimpleName() + "] " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
        return totalCount;
    }

    @Override // org.signal.paging.PagedDataSource
    public List<Conversation> load(int i, int i2, PagedDataSource.CancellationSignal cancellationSignal) {
        Stopwatch stopwatch = new Stopwatch("load(" + i + ", " + i2 + "), " + getClass().getSimpleName());
        ArrayList arrayList = new ArrayList(i2);
        LinkedList linkedList = new LinkedList();
        HashSet hashSet = new HashSet();
        ConversationReader conversationReader = new ConversationReader(getCursor((long) i, (long) i2));
        while (true) {
            try {
                ThreadRecord next = conversationReader.getNext();
                if (next == null || cancellationSignal.isCanceled()) {
                    break;
                }
                arrayList.add(new Conversation(next));
                linkedList.add(next.getRecipient());
                hashSet.add(next.getGroupMessageSender());
                if (!MmsSmsColumns.Types.isGroupV2(next.getType())) {
                    hashSet.add(next.getRecipient().getId());
                } else if (MmsSmsColumns.Types.isGroupUpdate(next.getType())) {
                    hashSet.addAll((Collection) Collection$EL.stream(MessageRecord.getGv2ChangeDescription(ApplicationDependencies.getApplication(), next.getBody(), null).getMentioned()).map(new ConversationListDataSource$$ExternalSyntheticLambda0()).collect(Collectors.toList()));
                }
            } catch (Throwable th) {
                try {
                    conversationReader.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        conversationReader.close();
        stopwatch.split("cursor");
        ApplicationDependencies.getRecipientCache().addToCache(linkedList);
        stopwatch.split("cache-recipients");
        Recipient.resolvedList(hashSet);
        stopwatch.split("recipient-resolve");
        stopwatch.stop(TAG);
        return arrayList;
    }

    public Conversation load(Long l) {
        throw new UnsupportedOperationException("Not implemented!");
    }

    public Long getKey(Conversation conversation) {
        return Long.valueOf(conversation.getThreadRecord().getThreadId());
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public static class ArchivedConversationListDataSource extends ConversationListDataSource {
        @Override // org.thoughtcrime.securesms.conversationlist.ConversationListDataSource, org.signal.paging.PagedDataSource
        public /* bridge */ /* synthetic */ Long getKey(Conversation conversation) {
            return ConversationListDataSource.super.getKey(conversation);
        }

        @Override // org.thoughtcrime.securesms.conversationlist.ConversationListDataSource, org.signal.paging.PagedDataSource
        public /* bridge */ /* synthetic */ Conversation load(Long l) {
            return ConversationListDataSource.super.load(l);
        }

        ArchivedConversationListDataSource(Context context) {
            super(context);
        }

        @Override // org.thoughtcrime.securesms.conversationlist.ConversationListDataSource
        protected int getTotalCount() {
            return this.threadDatabase.getArchivedConversationListCount();
        }

        @Override // org.thoughtcrime.securesms.conversationlist.ConversationListDataSource
        protected Cursor getCursor(long j, long j2) {
            return this.threadDatabase.getArchivedConversationList(j, j2);
        }
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes4.dex */
    public static class UnarchivedConversationListDataSource extends ConversationListDataSource {
        private int archivedCount;
        private int pinnedCount;
        private int totalCount;
        private int unpinnedCount;

        @Override // org.thoughtcrime.securesms.conversationlist.ConversationListDataSource, org.signal.paging.PagedDataSource
        public /* bridge */ /* synthetic */ Long getKey(Conversation conversation) {
            return ConversationListDataSource.super.getKey(conversation);
        }

        @Override // org.thoughtcrime.securesms.conversationlist.ConversationListDataSource, org.signal.paging.PagedDataSource
        public /* bridge */ /* synthetic */ Conversation load(Long l) {
            return ConversationListDataSource.super.load(l);
        }

        UnarchivedConversationListDataSource(Context context) {
            super(context);
        }

        @Override // org.thoughtcrime.securesms.conversationlist.ConversationListDataSource
        protected int getTotalCount() {
            int unarchivedConversationListCount = this.threadDatabase.getUnarchivedConversationListCount();
            this.pinnedCount = this.threadDatabase.getPinnedConversationListCount();
            int archivedConversationListCount = this.threadDatabase.getArchivedConversationListCount();
            this.archivedCount = archivedConversationListCount;
            int i = this.pinnedCount;
            int i2 = unarchivedConversationListCount - i;
            this.unpinnedCount = i2;
            this.totalCount = unarchivedConversationListCount;
            if (archivedConversationListCount != 0) {
                this.totalCount = unarchivedConversationListCount + 1;
            }
            if (i != 0) {
                if (i2 != 0) {
                    this.totalCount += 2;
                } else {
                    this.totalCount++;
                }
            }
            return this.totalCount;
        }

        @Override // org.thoughtcrime.securesms.conversationlist.ConversationListDataSource
        protected Cursor getCursor(long j, long j2) {
            long j3;
            ArrayList arrayList = new ArrayList(5);
            if (j != 0 || !hasPinnedHeader()) {
                j3 = j2;
            } else {
                MatrixCursor matrixCursor = new MatrixCursor(ConversationReader.HEADER_COLUMN);
                matrixCursor.addRow(ConversationReader.PINNED_HEADER);
                arrayList.add(matrixCursor);
                j3 = j2 - 1;
            }
            Cursor unarchivedConversationList = this.threadDatabase.getUnarchivedConversationList(true, j, j3);
            arrayList.add(unarchivedConversationList);
            long count = j3 - ((long) unarchivedConversationList.getCount());
            if (j == 0 && hasUnpinnedHeader()) {
                MatrixCursor matrixCursor2 = new MatrixCursor(ConversationReader.HEADER_COLUMN);
                matrixCursor2.addRow(ConversationReader.UNPINNED_HEADER);
                arrayList.add(matrixCursor2);
                count--;
            }
            arrayList.add(this.threadDatabase.getUnarchivedConversationList(false, Math.max(0L, (j - ((long) this.pinnedCount)) - ((long) getHeaderOffset())), count));
            if (j + j2 >= ((long) this.totalCount) && hasArchivedFooter()) {
                MatrixCursor matrixCursor3 = new MatrixCursor(ConversationReader.ARCHIVED_COLUMNS);
                matrixCursor3.addRow(ConversationReader.createArchivedFooterRow(this.archivedCount));
                arrayList.add(matrixCursor3);
            }
            return new MergeCursor((Cursor[]) arrayList.toArray(new Cursor[0]));
        }

        int getHeaderOffset() {
            return (hasPinnedHeader() ? 1 : 0) + (hasUnpinnedHeader() ? 1 : 0);
        }

        boolean hasPinnedHeader() {
            return this.pinnedCount != 0;
        }

        boolean hasUnpinnedHeader() {
            return hasPinnedHeader() && this.unpinnedCount != 0;
        }

        boolean hasArchivedFooter() {
            return this.archivedCount != 0;
        }
    }
}
