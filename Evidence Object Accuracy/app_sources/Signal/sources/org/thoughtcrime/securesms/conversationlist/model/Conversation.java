package org.thoughtcrime.securesms.conversationlist.model;

import org.thoughtcrime.securesms.database.model.ThreadRecord;

/* loaded from: classes4.dex */
public class Conversation {
    private final ThreadRecord threadRecord;
    private final Type type;

    /* loaded from: classes4.dex */
    public enum Type {
        THREAD,
        PINNED_HEADER,
        UNPINNED_HEADER,
        ARCHIVED_FOOTER
    }

    public Conversation(ThreadRecord threadRecord) {
        this.threadRecord = threadRecord;
        if (threadRecord.getThreadId() < 0) {
            this.type = Type.valueOf(threadRecord.getBody());
        } else {
            this.type = Type.THREAD;
        }
    }

    public ThreadRecord getThreadRecord() {
        return this.threadRecord;
    }

    public Type getType() {
        return this.type;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.threadRecord.equals(((Conversation) obj).threadRecord);
    }

    public int hashCode() {
        return this.threadRecord.hashCode();
    }
}
