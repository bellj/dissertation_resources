package org.thoughtcrime.securesms.conversationlist;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class ConversationListFragmentDirections {
    private ConversationListFragmentDirections() {
    }

    public static NavDirections actionConversationListFragmentToConversationListArchiveFragment() {
        return new ActionOnlyNavDirections(R.id.action_conversationListFragment_to_conversationListArchiveFragment);
    }

    public static NavDirections actionConversationListFragmentToStoriesLandingFragment() {
        return new ActionOnlyNavDirections(R.id.action_conversationListFragment_to_storiesLandingFragment);
    }
}
