package org.thoughtcrime.securesms.conversationlist;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import org.signal.paging.PagingController;
import org.thoughtcrime.securesms.BindableConversationListItem;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversationlist.ConversationListAdapter;
import org.thoughtcrime.securesms.conversationlist.model.Conversation;
import org.thoughtcrime.securesms.conversationlist.model.ConversationSet;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.util.CachedInflater;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public class ConversationListAdapter extends ListAdapter<Conversation, RecyclerView.ViewHolder> {
    private static final int TYPE_ACTION;
    private static final int TYPE_HEADER;
    private static final int TYPE_PLACEHOLDER;
    private static final int TYPE_THREAD;
    private final GlideRequests glideRequests;
    private final LifecycleOwner lifecycleOwner;
    private final OnConversationClickListener onConversationClickListener;
    private PagingController pagingController;
    private ConversationSet selectedConversations = new ConversationSet();
    private final Set<Long> typingSet = new HashSet();

    /* loaded from: classes4.dex */
    public interface OnConversationClickListener {
        void onConversationClick(Conversation conversation);

        boolean onConversationLongClick(Conversation conversation, View view);

        void onShowArchiveClick();
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public enum Payload {
        TYPING_INDICATOR,
        SELECTION
    }

    public ConversationListAdapter(LifecycleOwner lifecycleOwner, GlideRequests glideRequests, OnConversationClickListener onConversationClickListener) {
        super(new ConversationDiffCallback(null));
        this.lifecycleOwner = lifecycleOwner;
        this.glideRequests = glideRequests;
        this.onConversationClickListener = onConversationClickListener;
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == 2) {
            ConversationViewHolder conversationViewHolder = new ConversationViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.conversation_list_item_action, viewGroup, false));
            conversationViewHolder.itemView.setOnClickListener(new View.OnClickListener(conversationViewHolder) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListAdapter$$ExternalSyntheticLambda0
                public final /* synthetic */ ConversationListAdapter.ConversationViewHolder f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ConversationListAdapter.m1623$r8$lambda$vAtf_y0gdUW6jelG_ZW9Blr0c(ConversationListAdapter.this, this.f$1, view);
                }
            });
            return conversationViewHolder;
        } else if (i == 1) {
            ConversationViewHolder conversationViewHolder2 = new ConversationViewHolder(CachedInflater.from(viewGroup.getContext()).inflate(R.layout.conversation_list_item_view, viewGroup, false));
            conversationViewHolder2.itemView.setOnClickListener(new View.OnClickListener(conversationViewHolder2) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListAdapter$$ExternalSyntheticLambda1
                public final /* synthetic */ ConversationListAdapter.ConversationViewHolder f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ConversationListAdapter.$r8$lambda$lFRRbdHUYwUNi0Ncj7w6Po7Gpv8(ConversationListAdapter.this, this.f$1, view);
                }
            });
            conversationViewHolder2.itemView.setOnLongClickListener(new View.OnLongClickListener(conversationViewHolder2) { // from class: org.thoughtcrime.securesms.conversationlist.ConversationListAdapter$$ExternalSyntheticLambda2
                public final /* synthetic */ ConversationListAdapter.ConversationViewHolder f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnLongClickListener
                public final boolean onLongClick(View view) {
                    return ConversationListAdapter.$r8$lambda$n0YnxwwqzU8Mq8Bc33rLxqCYOOs(ConversationListAdapter.this, this.f$1, view);
                }
            });
            return conversationViewHolder2;
        } else if (i == 3) {
            FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
            frameLayout.setLayoutParams(new FrameLayout.LayoutParams(1, ViewUtil.dpToPx(100)));
            return new PlaceholderViewHolder(frameLayout);
        } else if (i == 4) {
            return new HeaderViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.dsl_section_header, viewGroup, false));
        } else {
            throw new IllegalStateException("Unknown type! " + i);
        }
    }

    public /* synthetic */ void lambda$onCreateViewHolder$0(ConversationViewHolder conversationViewHolder, View view) {
        if (conversationViewHolder.getAdapterPosition() != -1) {
            this.onConversationClickListener.onShowArchiveClick();
        }
    }

    public /* synthetic */ void lambda$onCreateViewHolder$1(ConversationViewHolder conversationViewHolder, View view) {
        int adapterPosition = conversationViewHolder.getAdapterPosition();
        if (adapterPosition != -1) {
            this.onConversationClickListener.onConversationClick(getItem(adapterPosition));
        }
    }

    public /* synthetic */ boolean lambda$onCreateViewHolder$2(ConversationViewHolder conversationViewHolder, View view) {
        int adapterPosition = conversationViewHolder.getAdapterPosition();
        if (adapterPosition != -1) {
            return this.onConversationClickListener.onConversationLongClick(getItem(adapterPosition), view);
        }
        return false;
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i, List<Object> list) {
        if (list.isEmpty()) {
            onBindViewHolder(viewHolder, i);
        } else if (viewHolder instanceof ConversationViewHolder) {
            for (Object obj : list) {
                if (obj instanceof Payload) {
                    if (((Payload) obj) == Payload.SELECTION) {
                        ((ConversationViewHolder) viewHolder).getConversationListItem().setSelectedConversations(this.selectedConversations);
                    } else {
                        ((ConversationViewHolder) viewHolder).getConversationListItem().updateTypingIndicator(this.typingSet);
                    }
                }
            }
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder.getItemViewType() == 2 || viewHolder.getItemViewType() == 1) {
            Conversation item = getItem(i);
            Objects.requireNonNull(item);
            ((ConversationViewHolder) viewHolder).getConversationListItem().bind(this.lifecycleOwner, item.getThreadRecord(), this.glideRequests, Locale.getDefault(), this.typingSet, this.selectedConversations);
        } else if (viewHolder.getItemViewType() == 4) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) viewHolder;
            Conversation item2 = getItem(i);
            Objects.requireNonNull(item2);
            int i2 = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$conversationlist$model$Conversation$Type[item2.getType().ordinal()];
            if (i2 == 1) {
                headerViewHolder.headerText.setText(R.string.conversation_list__pinned);
            } else if (i2 == 2) {
                headerViewHolder.headerText.setText(R.string.conversation_list__chats);
            } else {
                throw new IllegalArgumentException();
            }
        }
    }

    /* renamed from: org.thoughtcrime.securesms.conversationlist.ConversationListAdapter$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$conversationlist$model$Conversation$Type;

        static {
            int[] iArr = new int[Conversation.Type.values().length];
            $SwitchMap$org$thoughtcrime$securesms$conversationlist$model$Conversation$Type = iArr;
            try {
                iArr[Conversation.Type.PINNED_HEADER.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$conversationlist$model$Conversation$Type[Conversation.Type.UNPINNED_HEADER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$conversationlist$model$Conversation$Type[Conversation.Type.ARCHIVED_FOOTER.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$conversationlist$model$Conversation$Type[Conversation.Type.THREAD.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    public void onViewRecycled(RecyclerView.ViewHolder viewHolder) {
        if (viewHolder instanceof ConversationViewHolder) {
            ((ConversationViewHolder) viewHolder).getConversationListItem().unbind();
        }
    }

    @Override // androidx.recyclerview.widget.ListAdapter
    public Conversation getItem(int i) {
        PagingController pagingController = this.pagingController;
        if (pagingController != null) {
            pagingController.onDataNeededAroundIndex(i);
        }
        return (Conversation) super.getItem(i);
    }

    public void setPagingController(PagingController pagingController) {
        this.pagingController = pagingController;
    }

    public void setTypingThreads(Set<Long> set) {
        this.typingSet.clear();
        this.typingSet.addAll(set);
        notifyItemRangeChanged(0, getItemCount(), Payload.TYPING_INDICATOR);
    }

    public void setSelectedConversations(ConversationSet conversationSet) {
        this.selectedConversations = conversationSet;
        notifyItemRangeChanged(0, getItemCount(), Payload.SELECTION);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        Conversation item = getItem(i);
        if (item == null) {
            return 3;
        }
        int i2 = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$conversationlist$model$Conversation$Type[item.getType().ordinal()];
        if (i2 == 1 || i2 == 2) {
            return 4;
        }
        if (i2 == 3) {
            return 2;
        }
        if (i2 == 4) {
            return 1;
        }
        throw new IllegalArgumentException();
    }

    /* loaded from: classes4.dex */
    public static final class ConversationViewHolder extends RecyclerView.ViewHolder {
        private final BindableConversationListItem conversationListItem;

        ConversationViewHolder(View view) {
            super(view);
            this.conversationListItem = (BindableConversationListItem) view;
        }

        public BindableConversationListItem getConversationListItem() {
            return this.conversationListItem;
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public static final class ConversationDiffCallback extends DiffUtil.ItemCallback<Conversation> {
        private ConversationDiffCallback() {
        }

        /* synthetic */ ConversationDiffCallback(AnonymousClass1 r1) {
            this();
        }

        public boolean areItemsTheSame(Conversation conversation, Conversation conversation2) {
            return conversation.getThreadRecord().getThreadId() == conversation2.getThreadRecord().getThreadId();
        }

        public boolean areContentsTheSame(Conversation conversation, Conversation conversation2) {
            return conversation.equals(conversation2);
        }
    }

    /* loaded from: classes4.dex */
    private static class PlaceholderViewHolder extends RecyclerView.ViewHolder {
        PlaceholderViewHolder(View view) {
            super(view);
        }
    }

    /* loaded from: classes4.dex */
    public static class HeaderViewHolder extends RecyclerView.ViewHolder {
        private TextView headerText;

        public HeaderViewHolder(View view) {
            super(view);
            this.headerText = (TextView) view.findViewById(R.id.section_header);
        }
    }
}
