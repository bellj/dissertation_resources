package org.thoughtcrime.securesms.conversationlist.model;

import android.content.Context;
import java.util.UUID;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.whispersystems.signalservice.api.payments.FormatterOptions;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public abstract class UnreadPayments {
    public abstract String getDescription(Context context);

    public abstract UUID getPaymentUuid();

    public abstract Recipient getRecipient();

    public abstract int getUnreadCount();

    private UnreadPayments() {
    }

    public static UnreadPayments forSingle(Recipient recipient, UUID uuid, Money money) {
        return new SingleRecipient(recipient, uuid, money);
    }

    public static UnreadPayments forMultiple(int i) {
        return new MultipleRecipients(i);
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public static final class SingleRecipient extends UnreadPayments {
        private final Money amount;
        private final UUID paymentId;
        private final Recipient recipient;

        @Override // org.thoughtcrime.securesms.conversationlist.model.UnreadPayments
        public int getUnreadCount() {
            return 1;
        }

        private SingleRecipient(Recipient recipient, UUID uuid, Money money) {
            super();
            this.recipient = recipient;
            this.paymentId = uuid;
            this.amount = money;
        }

        @Override // org.thoughtcrime.securesms.conversationlist.model.UnreadPayments
        public String getDescription(Context context) {
            Recipient recipient = this.recipient;
            if (recipient != null) {
                return context.getString(R.string.UnreadPayments__s_sent_you_s, recipient.getShortDisplayName(context), this.amount.toString(FormatterOptions.defaults()));
            }
            return context.getString(R.string.UnreadPayments__d_new_payment_notifications, 1);
        }

        @Override // org.thoughtcrime.securesms.conversationlist.model.UnreadPayments
        public Recipient getRecipient() {
            return this.recipient;
        }

        @Override // org.thoughtcrime.securesms.conversationlist.model.UnreadPayments
        public UUID getPaymentUuid() {
            return this.paymentId;
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public static final class MultipleRecipients extends UnreadPayments {
        private final int unreadCount;

        @Override // org.thoughtcrime.securesms.conversationlist.model.UnreadPayments
        public UUID getPaymentUuid() {
            return null;
        }

        @Override // org.thoughtcrime.securesms.conversationlist.model.UnreadPayments
        public Recipient getRecipient() {
            return null;
        }

        private MultipleRecipients(int i) {
            super();
            this.unreadCount = i;
        }

        @Override // org.thoughtcrime.securesms.conversationlist.model.UnreadPayments
        public String getDescription(Context context) {
            return context.getString(R.string.UnreadPayments__d_new_payment_notifications, Integer.valueOf(this.unreadCount));
        }

        @Override // org.thoughtcrime.securesms.conversationlist.model.UnreadPayments
        public int getUnreadCount() {
            return this.unreadCount;
        }
    }
}
