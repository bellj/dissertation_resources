package org.thoughtcrime.securesms.conversationlist;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.lifecycle.LifecycleOwner;
import java.util.Locale;
import java.util.Set;
import org.thoughtcrime.securesms.BindableConversationListItem;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversationlist.model.ConversationSet;
import org.thoughtcrime.securesms.database.model.ThreadRecord;
import org.thoughtcrime.securesms.mms.GlideRequests;

/* loaded from: classes4.dex */
public class ConversationListItemAction extends FrameLayout implements BindableConversationListItem {
    private TextView description;

    @Override // org.thoughtcrime.securesms.BindableConversationListItem
    public void setSelectedConversations(ConversationSet conversationSet) {
    }

    @Override // org.thoughtcrime.securesms.Unbindable
    public void unbind() {
    }

    @Override // org.thoughtcrime.securesms.BindableConversationListItem
    public void updateTypingIndicator(Set<Long> set) {
    }

    public ConversationListItemAction(Context context) {
        super(context);
    }

    public ConversationListItemAction(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ConversationListItemAction(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        this.description = (TextView) findViewById(R.id.description);
    }

    @Override // org.thoughtcrime.securesms.BindableConversationListItem
    public void bind(LifecycleOwner lifecycleOwner, ThreadRecord threadRecord, GlideRequests glideRequests, Locale locale, Set<Long> set, ConversationSet conversationSet) {
        this.description.setText(getContext().getString(R.string.ConversationListItemAction_archived_conversations_d, Integer.valueOf(threadRecord.getUnreadCount())));
    }
}
