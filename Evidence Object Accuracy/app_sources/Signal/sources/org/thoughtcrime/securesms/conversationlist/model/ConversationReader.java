package org.thoughtcrime.securesms.conversationlist.model;

import android.database.Cursor;
import me.leolin.shortcutbadger.impl.NewHtcHomeBadger;
import org.signal.core.util.CursorUtil;
import org.thoughtcrime.securesms.conversationlist.model.Conversation;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.database.model.ThreadRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class ConversationReader extends ThreadDatabase.StaticReader {
    public static final String[] ARCHIVED_COLUMNS = {"header", NewHtcHomeBadger.COUNT};
    public static final String[] HEADER_COLUMN = {"header"};
    public static final String[] PINNED_HEADER = {Conversation.Type.PINNED_HEADER.toString()};
    public static final String[] UNPINNED_HEADER = {Conversation.Type.UNPINNED_HEADER.toString()};
    private final Cursor cursor;

    public ConversationReader(Cursor cursor) {
        super(cursor, ApplicationDependencies.getApplication());
        this.cursor = cursor;
    }

    public static String[] createArchivedFooterRow(int i) {
        return new String[]{Conversation.Type.ARCHIVED_FOOTER.toString(), String.valueOf(i)};
    }

    @Override // org.thoughtcrime.securesms.database.ThreadDatabase.StaticReader
    public ThreadRecord getCurrent() {
        if (this.cursor.getColumnIndex(HEADER_COLUMN[0]) == -1) {
            return super.getCurrent();
        }
        return buildThreadRecordForHeader();
    }

    private ThreadRecord buildThreadRecordForHeader() {
        int i = 0;
        Conversation.Type valueOf = Conversation.Type.valueOf(CursorUtil.requireString(this.cursor, HEADER_COLUMN[0]));
        if (valueOf == Conversation.Type.ARCHIVED_FOOTER) {
            i = CursorUtil.requireInt(this.cursor, ARCHIVED_COLUMNS[1]);
        }
        return new ThreadRecord.Builder((long) (-(valueOf.ordinal() + 100))).setBody(valueOf.toString()).setDate(100).setRecipient(Recipient.UNKNOWN).setUnreadCount(i).build();
    }
}
