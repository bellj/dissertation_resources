package org.thoughtcrime.securesms;

import android.os.AsyncTask;
import android.os.Bundle;
import org.thoughtcrime.securesms.crypto.MasterSecret;
import org.thoughtcrime.securesms.crypto.MasterSecretUtil;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.VersionTracker;

/* loaded from: classes.dex */
public class PassphraseCreateActivity extends PassphraseActivity {
    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.create_passphrase_activity);
        initializeResources();
    }

    private void initializeResources() {
        new SecretGenerator().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MasterSecretUtil.UNENCRYPTED_PASSPHRASE);
    }

    /* loaded from: classes.dex */
    public class SecretGenerator extends AsyncTask<String, Void, Void> {
        private MasterSecret masterSecret;

        @Override // android.os.AsyncTask
        protected void onPreExecute() {
        }

        private SecretGenerator() {
            PassphraseCreateActivity.this = r1;
        }

        public Void doInBackground(String... strArr) {
            MasterSecret generateMasterSecret = MasterSecretUtil.generateMasterSecret(PassphraseCreateActivity.this, strArr[0]);
            this.masterSecret = generateMasterSecret;
            MasterSecretUtil.generateAsymmetricMasterSecret(PassphraseCreateActivity.this, generateMasterSecret);
            SignalStore.account().generateAciIdentityKeyIfNecessary();
            SignalStore.account().generatePniIdentityKeyIfNecessary();
            VersionTracker.updateLastSeenVersion(PassphraseCreateActivity.this);
            return null;
        }

        public void onPostExecute(Void r2) {
            PassphraseCreateActivity.this.setMasterSecret(this.masterSecret);
        }
    }

    @Override // org.thoughtcrime.securesms.PassphraseActivity
    protected void cleanup() {
        System.gc();
    }
}
