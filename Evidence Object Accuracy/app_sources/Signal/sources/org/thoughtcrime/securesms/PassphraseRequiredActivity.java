package org.thoughtcrime.securesms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import java.util.Locale;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.greenrobot.eventbus.EventBus;
import org.signal.core.util.logging.Log;
import org.signal.core.util.tracing.Tracer;
import org.signal.devicetransfer.TransferStatus;
import org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberLockActivity;
import org.thoughtcrime.securesms.crypto.MasterSecretUtil;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.devicetransfer.olddevice.OldDeviceTransferActivity;
import org.thoughtcrime.securesms.jobs.PushNotificationReceiveJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.lock.v2.CreateKbsPinActivity;
import org.thoughtcrime.securesms.migrations.ApplicationMigrationActivity;
import org.thoughtcrime.securesms.migrations.ApplicationMigrations;
import org.thoughtcrime.securesms.pin.PinRestoreActivity;
import org.thoughtcrime.securesms.profiles.edit.EditProfileActivity;
import org.thoughtcrime.securesms.push.SignalServiceNetworkAccess;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.registration.RegistrationNavigationActivity;
import org.thoughtcrime.securesms.service.KeyCachingService;
import org.thoughtcrime.securesms.util.AppStartup;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes.dex */
public abstract class PassphraseRequiredActivity extends BaseActivity implements MasterSecretListener {
    public static final String LOCALE_EXTRA;
    public static final String NEXT_INTENT_EXTRA;
    private static final int STATE_CHANGE_NUMBER_LOCK;
    private static final int STATE_CREATE_PASSPHRASE;
    private static final int STATE_CREATE_PROFILE_NAME;
    private static final int STATE_CREATE_SIGNAL_PIN;
    private static final int STATE_ENTER_SIGNAL_PIN;
    private static final int STATE_NORMAL;
    private static final int STATE_PROMPT_PASSPHRASE;
    private static final int STATE_TRANSFER_LOCKED;
    private static final int STATE_TRANSFER_ONGOING;
    private static final int STATE_UI_BLOCKING_UPGRADE;
    private static final int STATE_WELCOME_PUSH_SCREEN;
    private static final String TAG = Log.tag(PassphraseRequiredActivity.class);
    private BroadcastReceiver clearKeyReceiver;
    private SignalServiceNetworkAccess networkAccess;

    public void onCreate(Bundle bundle, boolean z) {
    }

    public void onPreCreate() {
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public final void onCreate(Bundle bundle) {
        Tracer instance = Tracer.getInstance();
        instance.start(Log.tag(getClass()) + "#onCreate()");
        AppStartup.getInstance().onCriticalRenderEventStart();
        this.networkAccess = ApplicationDependencies.getSignalServiceNetworkAccess();
        onPreCreate();
        routeApplicationState(KeyCachingService.isLocked(this));
        super.onCreate(bundle);
        if (!isFinishing()) {
            initializeClearKeyReceiver();
            onCreate(bundle, true);
        }
        AppStartup.getInstance().onCriticalRenderEventEnd();
        Tracer instance2 = Tracer.getInstance();
        instance2.end(Log.tag(getClass()) + "#onCreate()");
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        if (this.networkAccess.isCensored()) {
            ApplicationDependencies.getJobManager().add(new PushNotificationReceiveJob());
        }
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        removeClearKeyReceiver(this);
    }

    @Override // org.thoughtcrime.securesms.MasterSecretListener
    public void onMasterSecretCleared() {
        Log.d(TAG, "onMasterSecretCleared()");
        if (ApplicationDependencies.getAppForegroundObserver().isForegrounded()) {
            routeApplicationState(true);
        } else {
            finish();
        }
    }

    protected <T extends Fragment> T initFragment(int i, T t) {
        return (T) initFragment(i, t, null);
    }

    public <T extends Fragment> T initFragment(int i, T t, Locale locale) {
        return (T) initFragment(i, t, locale, null);
    }

    protected <T extends Fragment> T initFragment(int i, T t, Locale locale, Bundle bundle) {
        Bundle bundle2 = new Bundle();
        bundle2.putSerializable(LOCALE_EXTRA, locale);
        if (bundle != null) {
            bundle2.putAll(bundle);
        }
        t.setArguments(bundle2);
        getSupportFragmentManager().beginTransaction().replace(i, t).commitAllowingStateLoss();
        return t;
    }

    private void routeApplicationState(boolean z) {
        Intent intentForState = getIntentForState(getApplicationState(z));
        if (intentForState != null) {
            startActivity(intentForState);
            finish();
        }
    }

    private Intent getIntentForState(int i) {
        String str = TAG;
        Log.d(str, "routeApplicationState(), state: " + i);
        switch (i) {
            case 1:
                return getCreatePassphraseIntent();
            case 2:
                return getPromptPassphraseIntent();
            case 3:
                return getUiBlockingUpgradeIntent();
            case 4:
                return getPushRegistrationIntent();
            case 5:
                return getEnterSignalPinIntent();
            case 6:
                return getCreateProfileNameIntent();
            case 7:
                return getCreateSignalPinIntent();
            case 8:
                return getOldDeviceTransferIntent();
            case 9:
                return getOldDeviceTransferLockedIntent();
            case 10:
                return getChangeNumberLockIntent();
            default:
                return null;
        }
    }

    private int getApplicationState(boolean z) {
        if (!MasterSecretUtil.isPassphraseInitialized(this)) {
            return 1;
        }
        if (z) {
            return 2;
        }
        if (ApplicationMigrations.isUpdate(this) && ApplicationMigrations.isUiBlockingMigrationRunning()) {
            return 3;
        }
        if (!TextSecurePreferences.hasPromptedPushRegistration(this)) {
            return 4;
        }
        if (SignalStore.storageService().needsAccountRestore()) {
            return 5;
        }
        if (userHasSkippedOrForgottenPin()) {
            return 7;
        }
        if (userMustSetProfileName()) {
            return 6;
        }
        if (userMustCreateSignalPin()) {
            return 7;
        }
        if (EventBus.getDefault().getStickyEvent(TransferStatus.class) != null && getClass() != OldDeviceTransferActivity.class) {
            return 8;
        }
        if (SignalStore.misc().isOldDeviceTransferLocked()) {
            return 9;
        }
        return (!SignalStore.misc().isChangeNumberLocked() || getClass() == ChangeNumberLockActivity.class) ? 0 : 10;
    }

    private boolean userMustCreateSignalPin() {
        return !SignalStore.registrationValues().isRegistrationComplete() && !SignalStore.kbsValues().hasPin() && !SignalStore.kbsValues().lastPinCreateFailed() && !SignalStore.kbsValues().hasOptedOut();
    }

    private boolean userHasSkippedOrForgottenPin() {
        return !SignalStore.registrationValues().isRegistrationComplete() && !SignalStore.kbsValues().hasPin() && !SignalStore.kbsValues().hasOptedOut() && SignalStore.kbsValues().isPinForgottenOrSkipped();
    }

    private boolean userMustSetProfileName() {
        return !SignalStore.registrationValues().isRegistrationComplete() && Recipient.self().getProfileName().isEmpty();
    }

    private Intent getCreatePassphraseIntent() {
        return getRoutedIntent(PassphraseCreateActivity.class, getIntent());
    }

    private Intent getPromptPassphraseIntent() {
        Intent routedIntent = getRoutedIntent(PassphrasePromptActivity.class, getIntent());
        routedIntent.putExtra(PassphrasePromptActivity.FROM_FOREGROUND, ApplicationDependencies.getAppForegroundObserver().isForegrounded());
        return routedIntent;
    }

    private Intent getUiBlockingUpgradeIntent() {
        Intent intent;
        if (TextSecurePreferences.hasPromptedPushRegistration(this)) {
            intent = getConversationListIntent();
        } else {
            intent = getPushRegistrationIntent();
        }
        return getRoutedIntent(ApplicationMigrationActivity.class, intent);
    }

    private Intent getPushRegistrationIntent() {
        return RegistrationNavigationActivity.newIntentForNewRegistration(this, getIntent());
    }

    private Intent getEnterSignalPinIntent() {
        return getRoutedIntent(PinRestoreActivity.class, getIntent());
    }

    private Intent getCreateSignalPinIntent() {
        Intent intent;
        if (userMustSetProfileName()) {
            intent = getCreateProfileNameIntent();
        } else {
            intent = getIntent();
        }
        return getRoutedIntent(CreateKbsPinActivity.class, intent);
    }

    private Intent getCreateProfileNameIntent() {
        return getRoutedIntent(EditProfileActivity.class, getIntent());
    }

    private Intent getOldDeviceTransferIntent() {
        Intent intent = new Intent(this, OldDeviceTransferActivity.class);
        intent.setFlags(SQLiteDatabase.ENABLE_WRITE_AHEAD_LOGGING);
        return intent;
    }

    private Intent getOldDeviceTransferLockedIntent() {
        if (getClass() == MainActivity.class) {
            return null;
        }
        return MainActivity.clearTop(this);
    }

    private Intent getChangeNumberLockIntent() {
        return ChangeNumberLockActivity.createIntent(this);
    }

    private Intent getRoutedIntent(Class<?> cls, Intent intent) {
        Intent intent2 = new Intent(this, cls);
        if (intent != null) {
            intent2.putExtra("next_intent", intent);
        }
        return intent2;
    }

    private Intent getConversationListIntent() {
        return MainActivity.clearTop(this);
    }

    private void initializeClearKeyReceiver() {
        this.clearKeyReceiver = new BroadcastReceiver() { // from class: org.thoughtcrime.securesms.PassphraseRequiredActivity.1
            @Override // android.content.BroadcastReceiver
            public void onReceive(Context context, Intent intent) {
                String str = PassphraseRequiredActivity.TAG;
                Log.i(str, "onReceive() for clear key event. PasswordDisabled: " + TextSecurePreferences.isPasswordDisabled(context) + ", ScreenLock: " + TextSecurePreferences.isScreenLockEnabled(context));
                if (TextSecurePreferences.isScreenLockEnabled(context) || !TextSecurePreferences.isPasswordDisabled(context)) {
                    PassphraseRequiredActivity.this.onMasterSecretCleared();
                }
            }
        };
        registerReceiver(this.clearKeyReceiver, new IntentFilter(KeyCachingService.CLEAR_KEY_EVENT), KeyCachingService.KEY_PERMISSION, null);
    }

    private void removeClearKeyReceiver(Context context) {
        BroadcastReceiver broadcastReceiver = this.clearKeyReceiver;
        if (broadcastReceiver != null) {
            context.unregisterReceiver(broadcastReceiver);
            this.clearKeyReceiver = null;
        }
    }

    public static Intent chainIntent(Intent intent, Intent intent2) {
        intent.putExtra("next_intent", intent2);
        return intent;
    }
}
