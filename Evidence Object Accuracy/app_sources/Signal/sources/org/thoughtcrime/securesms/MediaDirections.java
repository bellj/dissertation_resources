package org.thoughtcrime.securesms;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;

/* loaded from: classes.dex */
public class MediaDirections {
    private MediaDirections() {
    }

    public static NavDirections actionDirectlyToMediaCaptureFragment() {
        return new ActionOnlyNavDirections(R.id.action_directly_to_mediaCaptureFragment);
    }

    public static NavDirections actionDirectlyToMediaGalleryFragment() {
        return new ActionOnlyNavDirections(R.id.action_directly_to_mediaGalleryFragment);
    }

    public static NavDirections actionDirectlyToMediaReviewFragment() {
        return new ActionOnlyNavDirections(R.id.action_directly_to_mediaReviewFragment);
    }

    public static NavDirections actionDirectlyToTextPostCreationFragment() {
        return new ActionOnlyNavDirections(R.id.action_directly_to_textPostCreationFragment);
    }
}
