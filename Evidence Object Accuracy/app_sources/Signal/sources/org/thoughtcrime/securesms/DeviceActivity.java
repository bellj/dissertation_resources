package org.thoughtcrime.securesms;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.text.TextUtils;
import android.transition.TransitionInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import java.io.IOException;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.IdentityKeyPair;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.ecc.Curve;
import org.signal.libsignal.protocol.ecc.ECPublicKey;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.signal.qr.kitkat.ScanListener;
import org.thoughtcrime.securesms.DeviceLinkFragment;
import org.thoughtcrime.securesms.crypto.ProfileKeyUtil;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.DynamicLanguage;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.task.ProgressDialogAsyncTask;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.push.exceptions.NotFoundException;
import org.whispersystems.signalservice.internal.push.DeviceLimitExceededException;

/* loaded from: classes.dex */
public class DeviceActivity extends PassphraseRequiredActivity implements View.OnClickListener, ScanListener, DeviceLinkFragment.LinkClickedListener {
    private static final String TAG = Log.tag(DeviceActivity.class);
    private DeviceAddFragment deviceAddFragment;
    private DeviceLinkFragment deviceLinkFragment;
    private DeviceListFragment deviceListFragment;
    private final DynamicLanguage dynamicLanguage = new DynamicLanguage();
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onPreCreate() {
        this.dynamicTheme.onCreate(this);
        this.dynamicLanguage.onCreate(this);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        setContentView(R.layout.device_activity);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        requireSupportActionBar().setDisplayHomeAsUpEnabled(true);
        requireSupportActionBar().setTitle(R.string.AndroidManifest__linked_devices);
        this.deviceAddFragment = new DeviceAddFragment();
        this.deviceListFragment = new DeviceListFragment();
        this.deviceLinkFragment = new DeviceLinkFragment();
        this.deviceListFragment.setAddDeviceButtonListener(this);
        this.deviceAddFragment.setScanListener(this);
        if (getIntent().getBooleanExtra("add", false)) {
            initFragment(R.id.fragment_container, this.deviceAddFragment, this.dynamicLanguage.getCurrentLocale());
        } else {
            initFragment(R.id.fragment_container, this.deviceListFragment, this.dynamicLanguage.getCurrentLocale());
        }
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
        this.dynamicLanguage.onResume(this);
    }

    @Override // android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        finish();
        return true;
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        Permissions.with(this).request("android.permission.CAMERA").ifNecessary().withPermanentDenialDialog(getString(R.string.DeviceActivity_signal_needs_the_camera_permission_in_order_to_scan_a_qr_code)).onAllGranted(new Runnable() { // from class: org.thoughtcrime.securesms.DeviceActivity$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                DeviceActivity.m249$r8$lambda$J1Uh7pLTBWewwizaMgONhaUe6E(DeviceActivity.this);
            }
        }).onAnyDenied(new Runnable() { // from class: org.thoughtcrime.securesms.DeviceActivity$$ExternalSyntheticLambda2
            @Override // java.lang.Runnable
            public final void run() {
                DeviceActivity.$r8$lambda$_ZshBjn_xFxCOECegPzVXH7o6pg(DeviceActivity.this);
            }
        }).execute();
    }

    public /* synthetic */ void lambda$onClick$0() {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, this.deviceAddFragment).addToBackStack(null).commitAllowingStateLoss();
    }

    public /* synthetic */ void lambda$onClick$1() {
        Toast.makeText(this, (int) R.string.DeviceActivity_unable_to_scan_a_qr_code_without_the_camera_permission, 1).show();
    }

    @Override // org.signal.qr.kitkat.ScanListener
    public void onQrDataFound(String str) {
        ThreadUtil.runOnMain(new Runnable(str) { // from class: org.thoughtcrime.securesms.DeviceActivity$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DeviceActivity.$r8$lambda$aayeXPxEz7ji6n58Z1DuUIffKnc(DeviceActivity.this, this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$onQrDataFound$2(String str) {
        ((Vibrator) getSystemService("vibrator")).vibrate(50);
        this.deviceLinkFragment.setLinkClickedListener(Uri.parse(str), this);
        if (Build.VERSION.SDK_INT >= 21) {
            this.deviceAddFragment.setSharedElementReturnTransition(TransitionInflater.from(this).inflateTransition(R.transition.fragment_shared));
            this.deviceAddFragment.setExitTransition(TransitionInflater.from(this).inflateTransition(17760258));
            this.deviceLinkFragment.setSharedElementEnterTransition(TransitionInflater.from(this).inflateTransition(R.transition.fragment_shared));
            this.deviceLinkFragment.setEnterTransition(TransitionInflater.from(this).inflateTransition(17760258));
            getSupportFragmentManager().beginTransaction().addToBackStack(null).addSharedElement(this.deviceAddFragment.getDevicesImage(), "devices").replace(R.id.fragment_container, this.deviceLinkFragment).commit();
            return;
        }
        getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_from_bottom, R.anim.slide_to_bottom, R.anim.slide_from_bottom, R.anim.slide_to_bottom).replace(R.id.fragment_container, this.deviceLinkFragment).addToBackStack(null).commit();
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, android.app.Activity
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        Permissions.onRequestPermissionsResult(this, i, strArr, iArr);
    }

    @Override // org.thoughtcrime.securesms.DeviceLinkFragment.LinkClickedListener
    public void onLink(final Uri uri) {
        new ProgressDialogAsyncTask<Void, Void, Integer>(this, R.string.DeviceProvisioningActivity_content_progress_title, R.string.DeviceProvisioningActivity_content_progress_content) { // from class: org.thoughtcrime.securesms.DeviceActivity.1
            private static final int BAD_CODE;
            private static final int KEY_ERROR;
            private static final int LIMIT_EXCEEDED;
            private static final int NETWORK_ERROR;
            private static final int NO_DEVICE;
            private static final int SUCCESS;

            public Integer doInBackground(Void... voidArr) {
                boolean isMultiDevice = TextSecurePreferences.isMultiDevice(DeviceActivity.this);
                try {
                    SignalServiceAccountManager signalServiceAccountManager = ApplicationDependencies.getSignalServiceAccountManager();
                    String newDeviceVerificationCode = signalServiceAccountManager.getNewDeviceVerificationCode();
                    String queryParameter = uri.getQueryParameter(RecipientDatabase.SERVICE_ID);
                    String queryParameter2 = uri.getQueryParameter("pub_key");
                    if (!TextUtils.isEmpty(queryParameter) && !TextUtils.isEmpty(queryParameter2)) {
                        ECPublicKey decodePoint = Curve.decodePoint(Base64.decode(queryParameter2), 0);
                        IdentityKeyPair aciIdentityKey = SignalStore.account().getAciIdentityKey();
                        IdentityKeyPair pniIdentityKey = SignalStore.account().getPniIdentityKey();
                        ProfileKey selfProfileKey = ProfileKeyUtil.getSelfProfileKey();
                        TextSecurePreferences.setMultiDevice(DeviceActivity.this, true);
                        signalServiceAccountManager.addDevice(queryParameter, decodePoint, aciIdentityKey, pniIdentityKey, selfProfileKey, newDeviceVerificationCode);
                        return 0;
                    }
                    Log.w(DeviceActivity.TAG, "UUID or Key is empty!");
                    return 5;
                } catch (NotFoundException e) {
                    Log.w(DeviceActivity.TAG, e);
                    TextSecurePreferences.setMultiDevice(DeviceActivity.this, isMultiDevice);
                    return 1;
                } catch (IOException e2) {
                    Log.w(DeviceActivity.TAG, e2);
                    TextSecurePreferences.setMultiDevice(DeviceActivity.this, isMultiDevice);
                    return 2;
                } catch (InvalidKeyException e3) {
                    Log.w(DeviceActivity.TAG, e3);
                    TextSecurePreferences.setMultiDevice(DeviceActivity.this, isMultiDevice);
                    return 3;
                } catch (DeviceLimitExceededException e4) {
                    Log.w(DeviceActivity.TAG, e4);
                    TextSecurePreferences.setMultiDevice(DeviceActivity.this, isMultiDevice);
                    return 4;
                }
            }

            public void onPostExecute(Integer num) {
                super.onPostExecute((AnonymousClass1) num);
                DeviceActivity deviceActivity = DeviceActivity.this;
                int intValue = num.intValue();
                if (intValue != 0) {
                    if (intValue == 1) {
                        Toast.makeText(deviceActivity, (int) R.string.DeviceProvisioningActivity_content_progress_no_device, 1).show();
                    } else if (intValue == 2) {
                        Toast.makeText(deviceActivity, (int) R.string.DeviceProvisioningActivity_content_progress_network_error, 1).show();
                    } else if (intValue == 3) {
                        Toast.makeText(deviceActivity, (int) R.string.DeviceProvisioningActivity_content_progress_key_error, 1).show();
                    } else if (intValue == 4) {
                        Toast.makeText(deviceActivity, (int) R.string.DeviceProvisioningActivity_sorry_you_have_too_many_devices_linked_already, 1).show();
                    } else if (intValue == 5) {
                        Toast.makeText(deviceActivity, (int) R.string.DeviceActivity_sorry_this_is_not_a_valid_device_link_qr_code, 1).show();
                    }
                    DeviceActivity.this.getSupportFragmentManager().popBackStackImmediate();
                    return;
                }
                Toast.makeText(deviceActivity, (int) R.string.DeviceProvisioningActivity_content_progress_success, 0).show();
                DeviceActivity.this.finish();
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
    }
}
