package org.thoughtcrime.securesms.profiles.edit;

import androidx.arch.core.util.Function;
import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import java.util.Arrays;
import java.util.Objects;
import org.signal.core.util.StringUtil;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.profiles.ProfileName;
import org.thoughtcrime.securesms.profiles.edit.EditProfileRepository;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* loaded from: classes4.dex */
public class EditProfileViewModel extends ViewModel {
    private final MutableLiveData<AvatarColor> avatarColor;
    private Media avatarMedia;
    private final MutableLiveData<String> familyName;
    private final MutableLiveData<String> givenName;
    private final GroupId groupId;
    private final MutableLiveData<byte[]> internalAvatar;
    private final LiveData<ProfileName> internalProfileName;
    private final LiveData<Boolean> isFormValid;
    private final MutableLiveData<byte[]> originalAvatar;
    private String originalDescription;
    private final MutableLiveData<String> originalDisplayName;
    private final EditProfileRepository repository;
    private final LiveData<String> trimmedFamilyName;
    private final LiveData<String> trimmedGivenName;
    private final SingleLiveEvent<EditProfileRepository.UploadResult> uploadResult;

    private EditProfileViewModel(EditProfileRepository editProfileRepository, boolean z, GroupId groupId) {
        LiveData<Boolean> liveData;
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        this.givenName = mutableLiveData;
        MutableLiveData<String> mutableLiveData2 = new MutableLiveData<>();
        this.familyName = mutableLiveData2;
        LiveData<String> map = Transformations.map(mutableLiveData, new Function() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileViewModel$$ExternalSyntheticLambda0
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return StringUtil.trimToVisualBounds((String) obj);
            }
        });
        this.trimmedGivenName = map;
        LiveData<String> map2 = Transformations.map(mutableLiveData2, new Function() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileViewModel$$ExternalSyntheticLambda0
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return StringUtil.trimToVisualBounds((String) obj);
            }
        });
        this.trimmedFamilyName = map2;
        this.internalProfileName = LiveDataUtil.combineLatest(map, map2, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileViewModel$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return ProfileName.fromParts((String) obj, (String) obj2);
            }
        });
        this.internalAvatar = new MutableLiveData<>();
        this.originalAvatar = new MutableLiveData<>();
        MutableLiveData<String> mutableLiveData3 = new MutableLiveData<>();
        this.originalDisplayName = mutableLiveData3;
        this.uploadResult = new SingleLiveEvent<>();
        MutableLiveData<AvatarColor> mutableLiveData4 = new MutableLiveData<>();
        this.avatarColor = mutableLiveData4;
        this.repository = editProfileRepository;
        this.groupId = groupId;
        if (groupId == null || !groupId.isMms()) {
            liveData = Transformations.map(map, new Function() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileViewModel$$ExternalSyntheticLambda2
                @Override // androidx.arch.core.util.Function
                public final Object apply(Object obj) {
                    return EditProfileViewModel.lambda$new$0((String) obj);
                }
            });
        } else {
            liveData = LiveDataUtil.just(Boolean.TRUE);
        }
        this.isFormValid = liveData;
        if (!z) {
            if (groupId != null) {
                Objects.requireNonNull(mutableLiveData3);
                editProfileRepository.getCurrentDisplayName(new Consumer() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileViewModel$$ExternalSyntheticLambda3
                    @Override // androidx.core.util.Consumer
                    public final void accept(Object obj) {
                        MutableLiveData.this.setValue((String) obj);
                    }
                });
                Objects.requireNonNull(mutableLiveData);
                editProfileRepository.getCurrentName(new Consumer() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileViewModel$$ExternalSyntheticLambda3
                    @Override // androidx.core.util.Consumer
                    public final void accept(Object obj) {
                        MutableLiveData.this.setValue((String) obj);
                    }
                });
                editProfileRepository.getCurrentDescription(new Consumer() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileViewModel$$ExternalSyntheticLambda4
                    @Override // androidx.core.util.Consumer
                    public final void accept(Object obj) {
                        EditProfileViewModel.this.lambda$new$1((String) obj);
                    }
                });
            } else {
                editProfileRepository.getCurrentProfileName(new Consumer() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileViewModel$$ExternalSyntheticLambda5
                    @Override // androidx.core.util.Consumer
                    public final void accept(Object obj) {
                        EditProfileViewModel.this.lambda$new$2((ProfileName) obj);
                    }
                });
            }
            editProfileRepository.getCurrentAvatar(new Consumer() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileViewModel$$ExternalSyntheticLambda6
                @Override // androidx.core.util.Consumer
                public final void accept(Object obj) {
                    EditProfileViewModel.this.lambda$new$3((byte[]) obj);
                }
            });
            Objects.requireNonNull(mutableLiveData4);
            editProfileRepository.getCurrentAvatarColor(new Consumer() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileViewModel$$ExternalSyntheticLambda7
                @Override // androidx.core.util.Consumer
                public final void accept(Object obj) {
                    MutableLiveData.this.setValue((AvatarColor) obj);
                }
            });
        }
    }

    public static /* synthetic */ Boolean lambda$new$0(String str) {
        return Boolean.valueOf(str.length() > 0);
    }

    public /* synthetic */ void lambda$new$1(String str) {
        this.originalDescription = str;
        this.familyName.setValue(str);
    }

    public /* synthetic */ void lambda$new$2(ProfileName profileName) {
        this.givenName.setValue(profileName.getGivenName());
        this.familyName.setValue(profileName.getFamilyName());
    }

    public /* synthetic */ void lambda$new$3(byte[] bArr) {
        this.internalAvatar.setValue(bArr);
        this.originalAvatar.setValue(bArr);
    }

    public LiveData<AvatarColor> avatarColor() {
        return Transformations.distinctUntilChanged(this.avatarColor);
    }

    public LiveData<String> givenName() {
        return Transformations.distinctUntilChanged(this.givenName);
    }

    public LiveData<String> familyName() {
        return Transformations.distinctUntilChanged(this.familyName);
    }

    public LiveData<ProfileName> profileName() {
        return Transformations.distinctUntilChanged(this.internalProfileName);
    }

    public LiveData<Boolean> isFormValid() {
        return Transformations.distinctUntilChanged(this.isFormValid);
    }

    public LiveData<byte[]> avatar() {
        return Transformations.distinctUntilChanged(this.internalAvatar);
    }

    public boolean hasAvatar() {
        return this.internalAvatar.getValue() != null;
    }

    public boolean isGroup() {
        return this.groupId != null;
    }

    public Media getAvatarMedia() {
        return this.avatarMedia;
    }

    public void setAvatarMedia(Media media) {
        this.avatarMedia = media;
    }

    public GroupId getGroupId() {
        return this.groupId;
    }

    public boolean canRemoveProfilePhoto() {
        return hasAvatar();
    }

    public SingleLiveEvent<EditProfileRepository.UploadResult> getUploadResult() {
        return this.uploadResult;
    }

    public void setGivenName(String str) {
        this.givenName.setValue(str);
    }

    public void setFamilyName(String str) {
        this.familyName.setValue(str);
    }

    public void setAvatar(byte[] bArr) {
        this.internalAvatar.setValue(bArr);
    }

    public void submitProfile() {
        String str;
        ProfileName value = isGroup() ? ProfileName.EMPTY : this.internalProfileName.getValue();
        String value2 = isGroup() ? this.givenName.getValue() : "";
        if (isGroup()) {
            str = this.familyName.getValue();
        } else {
            str = "";
        }
        if (value != null && value2 != null) {
            byte[] value3 = this.originalAvatar.getValue();
            byte[] value4 = this.internalAvatar.getValue();
            String str2 = null;
            String value5 = isGroup() ? this.originalDisplayName.getValue() : null;
            if (isGroup()) {
                str2 = this.originalDescription;
            }
            SingleLiveEvent<EditProfileRepository.UploadResult> singleLiveEvent = this.uploadResult;
            Objects.requireNonNull(singleLiveEvent);
            this.repository.uploadProfile(value, value2, !Objects.equals(StringUtil.stripBidiProtection(value5), value2), str, !Objects.equals(StringUtil.stripBidiProtection(str2), str), value4, !Arrays.equals(value3, value4), new Consumer() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileViewModel$$ExternalSyntheticLambda8
                @Override // androidx.core.util.Consumer
                public final void accept(Object obj) {
                    SingleLiveEvent.this.postValue((EditProfileRepository.UploadResult) obj);
                }
            });
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements ViewModelProvider.Factory {
        private final GroupId groupId;
        private final boolean hasInstanceState;
        private final EditProfileRepository repository;

        public Factory(EditProfileRepository editProfileRepository, boolean z, GroupId groupId) {
            this.repository = editProfileRepository;
            this.hasInstanceState = z;
            this.groupId = groupId;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return new EditProfileViewModel(this.repository, this.hasInstanceState, this.groupId);
        }
    }
}
