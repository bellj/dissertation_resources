package org.thoughtcrime.securesms.profiles.manage;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.arch.core.util.Function;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.FragmentResultListener;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import com.airbnb.lottie.SimpleColorFilter;
import com.bumptech.glide.Glide;
import j$.util.Optional;
import java.util.Arrays;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.AvatarPreviewActivity;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.avatar.Avatars;
import org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.badges.self.none.BecomeASustainerFragment;
import org.thoughtcrime.securesms.components.emoji.EmojiUtil;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.profiles.ProfileName;
import org.thoughtcrime.securesms.profiles.manage.ManageProfileViewModel;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.NameUtil;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;

/* loaded from: classes4.dex */
public class ManageProfileFragment extends LoggingFragment {
    private static final String TAG = Log.tag(ManageProfileFragment.class);
    private View aboutContainer;
    private ImageView aboutEmojiView;
    private TextView aboutView;
    private ImageView avatarBackground;
    private TextView avatarInitials;
    private ImageView avatarPlaceholderView;
    private AlertDialog avatarProgress;
    private ImageView avatarView;
    private BadgeImageView badgeView;
    private View badgesContainer;
    private View profileNameContainer;
    private TextView profileNameView;
    private Toolbar toolbar;
    private View usernameContainer;
    private TextView usernameView;
    private ManageProfileViewModel viewModel;

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.manage_profile_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        this.avatarView = (ImageView) view.findViewById(R.id.manage_profile_avatar);
        this.avatarPlaceholderView = (ImageView) view.findViewById(R.id.manage_profile_avatar_placeholder);
        this.profileNameView = (TextView) view.findViewById(R.id.manage_profile_name);
        this.profileNameContainer = view.findViewById(R.id.manage_profile_name_container);
        this.usernameView = (TextView) view.findViewById(R.id.manage_profile_username);
        this.usernameContainer = view.findViewById(R.id.manage_profile_username_container);
        this.aboutView = (TextView) view.findViewById(R.id.manage_profile_about);
        this.aboutContainer = view.findViewById(R.id.manage_profile_about_container);
        this.aboutEmojiView = (ImageView) view.findViewById(R.id.manage_profile_about_icon);
        this.avatarInitials = (TextView) view.findViewById(R.id.manage_profile_avatar_initials);
        this.avatarBackground = (ImageView) view.findViewById(R.id.manage_profile_avatar_background);
        this.badgesContainer = view.findViewById(R.id.manage_profile_badges_container);
        this.badgeView = (BadgeImageView) view.findViewById(R.id.manage_profile_badge);
        initializeViewModel();
        this.toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ManageProfileFragment.$r8$lambda$dmU6DpfiV36eZGvmliRhxoUCvvQ(ManageProfileFragment.this, view2);
            }
        });
        view.findViewById(R.id.manage_profile_edit_photo).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ManageProfileFragment.$r8$lambda$FJCrAEV45E8kJcpW7vn0JLrQn3c(ManageProfileFragment.this, view2);
            }
        });
        this.profileNameContainer.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ManageProfileFragment.m2488$r8$lambda$ezfAtzoNHpXMVv2sjSuPEx3M(view2);
            }
        });
        this.usernameContainer.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileFragment$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ManageProfileFragment.m2485$r8$lambda$LNq8AzJjCnogcYgiANePHfc9TU(view2);
            }
        });
        this.aboutContainer.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileFragment$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ManageProfileFragment.$r8$lambda$isyuavbWesxwmDiFIun7f15GU6k(view2);
            }
        });
        getParentFragmentManager().setFragmentResultListener(AvatarPickerFragment.REQUEST_KEY_SELECT_AVATAR, getViewLifecycleOwner(), new FragmentResultListener() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileFragment$$ExternalSyntheticLambda5
            @Override // androidx.fragment.app.FragmentResultListener
            public final void onFragmentResult(String str, Bundle bundle2) {
                ManageProfileFragment.$r8$lambda$P6ZxZbX7zz5E_IzICQPSC7CnaWY(ManageProfileFragment.this, str, bundle2);
            }
        });
        this.avatarInitials.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileFragment$$ExternalSyntheticLambda6
            @Override // android.view.View.OnLayoutChangeListener
            public final void onLayoutChange(View view2, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                ManageProfileFragment.m2484$r8$lambda$1cm_4EmfwbKCNQ1XSKRtZkvdY(ManageProfileFragment.this, view2, i, i2, i3, i4, i5, i6, i7, i8);
            }
        });
        if (FeatureFlags.donorBadges()) {
            this.badgesContainer.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileFragment$$ExternalSyntheticLambda7
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    ManageProfileFragment.$r8$lambda$tJt19duRgiDATYUlkt4OTZd7Z1I(ManageProfileFragment.this, view2);
                }
            });
        } else {
            this.badgesContainer.setVisibility(8);
        }
        this.avatarView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileFragment$$ExternalSyntheticLambda8
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ManageProfileFragment.$r8$lambda$BdVyAGRkwcH7HhOe6ned5S5x964(ManageProfileFragment.this, view2);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        requireActivity().finish();
    }

    public /* synthetic */ void lambda$onViewCreated$1(View view) {
        onEditAvatarClicked();
    }

    public static /* synthetic */ void lambda$onViewCreated$2(View view) {
        SafeNavigation.safeNavigate(Navigation.findNavController(view), ManageProfileFragmentDirections.actionManageProfileName());
    }

    public static /* synthetic */ void lambda$onViewCreated$3(View view) {
        SafeNavigation.safeNavigate(Navigation.findNavController(view), ManageProfileFragmentDirections.actionManageUsername());
    }

    public static /* synthetic */ void lambda$onViewCreated$4(View view) {
        SafeNavigation.safeNavigate(Navigation.findNavController(view), ManageProfileFragmentDirections.actionManageAbout());
    }

    public /* synthetic */ void lambda$onViewCreated$5(String str, Bundle bundle) {
        if (bundle.getBoolean(AvatarPickerFragment.SELECT_AVATAR_CLEAR)) {
            this.viewModel.onAvatarSelected(requireContext(), null);
            return;
        }
        this.viewModel.onAvatarSelected(requireContext(), (Media) bundle.getParcelable(AvatarPickerFragment.SELECT_AVATAR_MEDIA));
    }

    public /* synthetic */ void lambda$onViewCreated$6(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        if (this.avatarInitials.length() > 0) {
            updateInitials(this.avatarInitials.getText().toString());
        }
    }

    public /* synthetic */ void lambda$onViewCreated$7(View view) {
        if (Recipient.self().getBadges().isEmpty()) {
            BecomeASustainerFragment.show(getParentFragmentManager());
        } else {
            SafeNavigation.safeNavigate(Navigation.findNavController(view), ManageProfileFragmentDirections.actionManageProfileFragmentToBadgeManageFragment());
        }
    }

    public /* synthetic */ void lambda$onViewCreated$8(View view) {
        startActivity(AvatarPreviewActivity.intentFromRecipientId(requireContext(), Recipient.self().getId()), AvatarPreviewActivity.createTransitionBundle(requireActivity(), this.avatarView));
    }

    private void initializeViewModel() {
        ManageProfileViewModel manageProfileViewModel = (ManageProfileViewModel) ViewModelProviders.of(this, new ManageProfileViewModel.Factory()).get(ManageProfileViewModel.class);
        this.viewModel = manageProfileViewModel;
        Transformations.map(LiveDataUtil.distinctUntilChanged(manageProfileViewModel.getAvatar(), new LiveDataUtil.EqualityChecker() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileFragment$$ExternalSyntheticLambda9
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.EqualityChecker
            public final boolean contentsMatch(Object obj, Object obj2) {
                return ManageProfileFragment.$r8$lambda$2gyhIz2Nh3XG95PfB6npLo_eBDQ((ManageProfileViewModel.AvatarState) obj, (ManageProfileViewModel.AvatarState) obj2);
            }
        }), new Function() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileFragment$$ExternalSyntheticLambda10
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ManageProfileFragment.m2483$r8$lambda$8tQ01_ENdm7ZNSqp4X7ZPlEOr8((ManageProfileViewModel.AvatarState) obj);
            }
        }).observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileFragment$$ExternalSyntheticLambda11
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ManageProfileFragment.$r8$lambda$NhPVHVM6Ab00AdKZVu3dXIF6szc(ManageProfileFragment.this, (Optional) obj);
            }
        });
        this.viewModel.getAvatar().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileFragment$$ExternalSyntheticLambda12
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ManageProfileFragment.$r8$lambda$PbLiff8UciRb5YAD3T64gRQDseE(ManageProfileFragment.this, (ManageProfileViewModel.AvatarState) obj);
            }
        });
        this.viewModel.getProfileName().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileFragment$$ExternalSyntheticLambda13
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ManageProfileFragment.$r8$lambda$_8r4QfbZ0_dmTSW0PeITZoEdU4I(ManageProfileFragment.this, (ProfileName) obj);
            }
        });
        this.viewModel.getEvents().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileFragment$$ExternalSyntheticLambda14
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ManageProfileFragment.$r8$lambda$8vodWRY8PVD1W7ywGy5DLJ_gvII(ManageProfileFragment.this, (ManageProfileViewModel.Event) obj);
            }
        });
        this.viewModel.getAbout().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileFragment$$ExternalSyntheticLambda15
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ManageProfileFragment.m2486$r8$lambda$RWzcEE7UjgdTAAZBJemnYd_YoY(ManageProfileFragment.this, (String) obj);
            }
        });
        this.viewModel.getAboutEmoji().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileFragment$$ExternalSyntheticLambda16
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ManageProfileFragment.$r8$lambda$G2ZpQycqIQjgs5QEEkjUbqF_N4g(ManageProfileFragment.this, (String) obj);
            }
        });
        this.viewModel.getBadge().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileFragment$$ExternalSyntheticLambda17
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ManageProfileFragment.m2487$r8$lambda$U9LLZKADvudds5r1MuViik52s(ManageProfileFragment.this, (Optional) obj);
            }
        });
        if (this.viewModel.shouldShowUsername()) {
            this.viewModel.getUsername().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileFragment$$ExternalSyntheticLambda18
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    ManageProfileFragment.$r8$lambda$6fIsXjcIkCCvd_TcCm_kmBMJ71w(ManageProfileFragment.this, (String) obj);
                }
            });
        } else {
            this.usernameContainer.setVisibility(8);
        }
    }

    public static /* synthetic */ boolean lambda$initializeViewModel$9(ManageProfileViewModel.AvatarState avatarState, ManageProfileViewModel.AvatarState avatarState2) {
        return Arrays.equals(avatarState.getAvatar(), avatarState2.getAvatar());
    }

    public static /* synthetic */ Optional lambda$initializeViewModel$10(ManageProfileViewModel.AvatarState avatarState) {
        return Optional.ofNullable(avatarState.getAvatar());
    }

    public void presentAvatarImage(Optional<byte[]> optional) {
        if (optional.isPresent()) {
            Glide.with(this).load(optional.get()).circleCrop().into(this.avatarView);
        } else {
            Glide.with(this).load((Drawable) null).into(this.avatarView);
        }
    }

    public void presentAvatarPlaceholder(ManageProfileViewModel.AvatarState avatarState) {
        if (avatarState.getAvatar() == null) {
            String abbreviation = NameUtil.getAbbreviation(avatarState.getSelf().getDisplayName(requireContext()));
            Avatars.ForegroundColor foregroundColor = Avatars.getForegroundColor(avatarState.getSelf().getAvatarColor());
            this.avatarBackground.setColorFilter(new SimpleColorFilter(avatarState.getSelf().getAvatarColor().colorInt()));
            this.avatarPlaceholderView.setColorFilter(new SimpleColorFilter(foregroundColor.getColorInt()));
            this.avatarInitials.setTextColor(foregroundColor.getColorInt());
            if (TextUtils.isEmpty(abbreviation)) {
                this.avatarPlaceholderView.setVisibility(0);
                this.avatarInitials.setVisibility(8);
            } else {
                updateInitials(abbreviation.toString());
                this.avatarPlaceholderView.setVisibility(8);
                this.avatarInitials.setVisibility(0);
            }
        } else {
            this.avatarPlaceholderView.setVisibility(8);
            this.avatarInitials.setVisibility(8);
        }
        if (this.avatarProgress == null && avatarState.getLoadingState() == ManageProfileViewModel.LoadingState.LOADING) {
            this.avatarProgress = SimpleProgressDialog.show(requireContext());
        } else if (this.avatarProgress != null && avatarState.getLoadingState() == ManageProfileViewModel.LoadingState.LOADED) {
            this.avatarProgress.dismiss();
        }
    }

    private void updateInitials(String str) {
        this.avatarInitials.setTextSize(0, Avatars.getTextSizeForLength(requireContext(), str, ((float) this.avatarInitials.getMeasuredWidth()) * 0.8f, ((float) this.avatarInitials.getMeasuredWidth()) * 0.45f));
        this.avatarInitials.setText(str);
    }

    public void presentProfileName(ProfileName profileName) {
        if (profileName == null || profileName.isEmpty()) {
            this.profileNameView.setText(R.string.ManageProfileFragment_profile_name);
        } else {
            this.profileNameView.setText(profileName.toString());
        }
    }

    public void presentUsername(String str) {
        if (str == null || str.isEmpty()) {
            this.usernameView.setText(R.string.ManageProfileFragment_username);
        } else {
            this.usernameView.setText(str);
        }
    }

    public void presentAbout(String str) {
        if (str == null || str.isEmpty()) {
            this.aboutView.setText(R.string.ManageProfileFragment_about);
        } else {
            this.aboutView.setText(str);
        }
    }

    public void presentAboutEmoji(String str) {
        if (str == null || str.isEmpty()) {
            this.aboutEmojiView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_compose_24, null));
            return;
        }
        Drawable convertToDrawable = EmojiUtil.convertToDrawable(requireContext(), str);
        if (convertToDrawable != null) {
            this.aboutEmojiView.setImageDrawable(convertToDrawable);
        } else {
            this.aboutEmojiView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_compose_24, null));
        }
    }

    public void presentBadge(Optional<Badge> optional) {
        if (!optional.isPresent() || !optional.get().getVisible() || optional.get().isExpired()) {
            this.badgeView.setBadge(null);
        } else {
            this.badgeView.setBadge(optional.orElse(null));
        }
    }

    /* renamed from: org.thoughtcrime.securesms.profiles.manage.ManageProfileFragment$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$profiles$manage$ManageProfileViewModel$Event;

        static {
            int[] iArr = new int[ManageProfileViewModel.Event.values().length];
            $SwitchMap$org$thoughtcrime$securesms$profiles$manage$ManageProfileViewModel$Event = iArr;
            try {
                iArr[ManageProfileViewModel.Event.AVATAR_DISK_FAILURE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$ManageProfileViewModel$Event[ManageProfileViewModel.Event.AVATAR_NETWORK_FAILURE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    public void presentEvent(ManageProfileViewModel.Event event) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$profiles$manage$ManageProfileViewModel$Event[event.ordinal()];
        if (i == 1) {
            Toast.makeText(requireContext(), (int) R.string.ManageProfileFragment_failed_to_set_avatar, 1).show();
        } else if (i == 2) {
            Toast.makeText(requireContext(), (int) R.string.EditProfileNameFragment_failed_to_save_due_to_network_issues_try_again_later, 1).show();
        }
    }

    private void onEditAvatarClicked() {
        SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), ManageProfileFragmentDirections.actionManageProfileFragmentToAvatarPicker(null, null));
    }
}
