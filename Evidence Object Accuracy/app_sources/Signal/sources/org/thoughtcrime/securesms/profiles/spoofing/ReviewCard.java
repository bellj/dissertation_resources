package org.thoughtcrime.securesms.profiles.spoofing;

import org.thoughtcrime.securesms.database.model.databaseprotos.ProfileChangeDetails;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class ReviewCard {
    private final CardType cardType;
    private final int inCommonGroupsCount;
    private final Action primaryAction;
    private final ReviewRecipient reviewRecipient;
    private final Action secondaryAction;

    /* loaded from: classes4.dex */
    public enum Action {
        UPDATE_CONTACT,
        DELETE,
        BLOCK,
        REMOVE_FROM_GROUP
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes4.dex */
    public enum CardType {
        MEMBER,
        REQUEST,
        YOUR_CONTACT
    }

    public ReviewCard(ReviewRecipient reviewRecipient, int i, CardType cardType, Action action, Action action2) {
        this.reviewRecipient = reviewRecipient;
        this.inCommonGroupsCount = i;
        this.cardType = cardType;
        this.primaryAction = action;
        this.secondaryAction = action2;
    }

    public Recipient getReviewRecipient() {
        return this.reviewRecipient.getRecipient();
    }

    public CardType getCardType() {
        return this.cardType;
    }

    public int getInCommonGroupsCount() {
        return this.inCommonGroupsCount;
    }

    public ProfileChangeDetails.StringChange getNameChange() {
        if (this.reviewRecipient.getProfileChangeDetails() == null || !this.reviewRecipient.getProfileChangeDetails().hasProfileNameChange()) {
            return null;
        }
        return this.reviewRecipient.getProfileChangeDetails().getProfileNameChange();
    }

    public Action getPrimaryAction() {
        return this.primaryAction;
    }

    public Action getSecondaryAction() {
        return this.secondaryAction;
    }
}
