package org.thoughtcrime.securesms.profiles;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.text.TextUtils;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.mms.MediaConstraints;
import org.thoughtcrime.securesms.util.BitmapDecodingException;
import org.thoughtcrime.securesms.util.BitmapUtil;
import org.thoughtcrime.securesms.util.concurrent.ListenableFuture;
import org.thoughtcrime.securesms.util.concurrent.SettableFuture;

/* loaded from: classes4.dex */
public class SystemProfileUtil {
    private static final String TAG = Log.tag(SystemProfileUtil.class);

    public static ListenableFuture<byte[]> getSystemProfileAvatar(final Context context, final MediaConstraints mediaConstraints) {
        final SettableFuture settableFuture = new SettableFuture();
        new AsyncTask<Void, Void, byte[]>() { // from class: org.thoughtcrime.securesms.profiles.SystemProfileUtil.1
            public byte[] doInBackground(Void... voidArr) {
                try {
                    Cursor query = context.getContentResolver().query(ContactsContract.Profile.CONTENT_URI, null, null, null, null);
                    while (query != null && query.moveToNext()) {
                        String string = query.getString(query.getColumnIndexOrThrow("photo_uri"));
                        if (!TextUtils.isEmpty(string)) {
                            try {
                                byte[] bitmap = BitmapUtil.createScaledBytes(context, Uri.parse(string), mediaConstraints).getBitmap();
                                query.close();
                                return bitmap;
                            } catch (BitmapDecodingException e) {
                                Log.w(SystemProfileUtil.TAG, e);
                            }
                        }
                    }
                    if (query == null) {
                        return null;
                    }
                    query.close();
                    return null;
                } catch (SecurityException e2) {
                    Log.w(SystemProfileUtil.TAG, e2);
                    return null;
                }
            }

            public void onPostExecute(byte[] bArr) {
                settableFuture.set(bArr);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        return settableFuture;
    }

    public static ListenableFuture<String> getSystemProfileName(final Context context) {
        final SettableFuture settableFuture = new SettableFuture();
        new AsyncTask<Void, Void, String>() { // from class: org.thoughtcrime.securesms.profiles.SystemProfileUtil.2
            public String doInBackground(Void... voidArr) {
                String str = null;
                try {
                    Cursor query = context.getContentResolver().query(ContactsContract.Profile.CONTENT_URI, null, null, null, null);
                    if (query != null && query.moveToNext()) {
                        str = query.getString(query.getColumnIndexOrThrow("display_name"));
                    }
                    if (query != null) {
                        query.close();
                    }
                } catch (SecurityException e) {
                    Log.w(SystemProfileUtil.TAG, e);
                }
                if (str != null) {
                    return str;
                }
                Account[] accountsByType = AccountManager.get(context).getAccountsByType("com.google");
                for (Account account : accountsByType) {
                    if (!TextUtils.isEmpty(account.name)) {
                        if (!account.name.contains("@")) {
                            return account.name.replace('.', ' ');
                        } else {
                            String str2 = account.name;
                            return str2.substring(0, str2.indexOf("@")).replace('.', ' ');
                        }
                    }
                }
                return str;
            }

            public void onPostExecute(String str) {
                settableFuture.set(str);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        return settableFuture;
    }
}
