package org.thoughtcrime.securesms.profiles.spoofing;

import android.content.Context;
import org.thoughtcrime.securesms.database.model.databaseprotos.ProfileChangeDetails;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class ReviewRecipient {
    private final ProfileChangeDetails profileChangeDetails;
    private final Recipient recipient;

    public ReviewRecipient(Recipient recipient) {
        this(recipient, null);
    }

    public ReviewRecipient(Recipient recipient, ProfileChangeDetails profileChangeDetails) {
        this.recipient = recipient;
        this.profileChangeDetails = profileChangeDetails;
    }

    public Recipient getRecipient() {
        return this.recipient;
    }

    public ProfileChangeDetails getProfileChangeDetails() {
        return this.profileChangeDetails;
    }

    /* loaded from: classes4.dex */
    public static class Comparator implements java.util.Comparator<ReviewRecipient> {
        private final RecipientId alwaysFirstId;
        private final Context context;

        public Comparator(Context context, RecipientId recipientId) {
            this.context = context;
            this.alwaysFirstId = recipientId;
        }

        public int compare(ReviewRecipient reviewRecipient, ReviewRecipient reviewRecipient2) {
            int i = -100;
            int i2 = reviewRecipient.getRecipient().getId().equals(this.alwaysFirstId) ? -100 : 0;
            if (!reviewRecipient2.getRecipient().getId().equals(this.alwaysFirstId)) {
                i = 0;
            }
            if (reviewRecipient.getProfileChangeDetails() != null && reviewRecipient.getProfileChangeDetails().hasProfileNameChange()) {
                i2--;
            }
            if (reviewRecipient2.getProfileChangeDetails() != null && reviewRecipient2.getProfileChangeDetails().hasProfileNameChange()) {
                i--;
            }
            if (reviewRecipient.getRecipient().isSystemContact()) {
                i2++;
            }
            if (reviewRecipient2.getRecipient().isSystemContact()) {
                i2++;
            }
            if (i2 == i) {
                return reviewRecipient.getRecipient().getDisplayName(this.context).compareTo(reviewRecipient2.getRecipient().getDisplayName(this.context));
            }
            return Integer.compare(i2, i);
        }
    }
}
