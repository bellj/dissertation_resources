package org.thoughtcrime.securesms.profiles.manage;

import android.app.Application;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import j$.util.Optional;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.profiles.manage.UsernameEditRepository;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.thoughtcrime.securesms.util.UsernameUtil;

/* loaded from: classes4.dex */
public class UsernameEditViewModel extends ViewModel {
    private static final String TAG = Log.tag(UsernameEditViewModel.class);
    private final Application application;
    private final SingleLiveEvent<Event> events;
    private final UsernameEditRepository repo;
    private final MutableLiveData<State> uiState;

    /* loaded from: classes4.dex */
    public enum ButtonState {
        SUBMIT,
        SUBMIT_DISABLED,
        SUBMIT_LOADING,
        DELETE,
        DELETE_LOADING,
        DELETE_DISABLED
    }

    /* loaded from: classes4.dex */
    public enum Event {
        NETWORK_FAILURE,
        SUBMIT_SUCCESS,
        DELETE_SUCCESS,
        SUBMIT_FAIL_INVALID,
        SUBMIT_FAIL_TAKEN
    }

    /* loaded from: classes4.dex */
    public enum UsernameStatus {
        NONE,
        AVAILABLE,
        TAKEN,
        TOO_SHORT,
        TOO_LONG,
        CANNOT_START_WITH_NUMBER,
        INVALID_CHARACTERS,
        INVALID_GENERIC
    }

    /* synthetic */ UsernameEditViewModel(AnonymousClass1 r1) {
        this();
    }

    private UsernameEditViewModel() {
        this.application = ApplicationDependencies.getApplication();
        this.repo = new UsernameEditRepository();
        MutableLiveData<State> mutableLiveData = new MutableLiveData<>();
        this.uiState = mutableLiveData;
        this.events = new SingleLiveEvent<>();
        mutableLiveData.setValue(new State(ButtonState.SUBMIT_DISABLED, UsernameStatus.NONE, null));
    }

    public void onUsernameUpdated(String str) {
        if (TextUtils.isEmpty(str) && Recipient.self().getUsername().isPresent()) {
            this.uiState.setValue(new State(ButtonState.DELETE, UsernameStatus.NONE, null));
        } else if (str.equals(Recipient.self().getUsername().orElse(null))) {
            this.uiState.setValue(new State(ButtonState.SUBMIT_DISABLED, UsernameStatus.NONE, null));
        } else {
            Optional<UsernameUtil.InvalidReason> checkUsername = UsernameUtil.checkUsername(str);
            if (checkUsername.isPresent()) {
                this.uiState.setValue(new State(ButtonState.SUBMIT_DISABLED, mapUsernameError(checkUsername.get()), null));
            } else {
                this.uiState.setValue(new State(ButtonState.SUBMIT, UsernameStatus.NONE, null));
            }
        }
    }

    public void onUsernameSubmitted(String str) {
        if (str.equals(Recipient.self().getUsername().orElse(null))) {
            this.uiState.setValue(new State(ButtonState.SUBMIT_DISABLED, UsernameStatus.NONE, null));
            return;
        }
        Optional<UsernameUtil.InvalidReason> checkUsername = UsernameUtil.checkUsername(str);
        if (checkUsername.isPresent()) {
            this.uiState.setValue(new State(ButtonState.SUBMIT_DISABLED, mapUsernameError(checkUsername.get()), null));
            return;
        }
        this.uiState.setValue(new State(ButtonState.SUBMIT_LOADING, UsernameStatus.NONE, null));
        this.repo.setUsername(str, new UsernameEditRepository.Callback() { // from class: org.thoughtcrime.securesms.profiles.manage.UsernameEditViewModel$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.profiles.manage.UsernameEditRepository.Callback
            public final void onComplete(Object obj) {
                UsernameEditViewModel.this.lambda$onUsernameSubmitted$1((UsernameEditRepository.UsernameSetResult) obj);
            }
        });
    }

    /* renamed from: org.thoughtcrime.securesms.profiles.manage.UsernameEditViewModel$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditRepository$UsernameDeleteResult;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditRepository$UsernameSetResult;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$util$UsernameUtil$InvalidReason;

        static {
            int[] iArr = new int[UsernameEditRepository.UsernameSetResult.values().length];
            $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditRepository$UsernameSetResult = iArr;
            try {
                iArr[UsernameEditRepository.UsernameSetResult.SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditRepository$UsernameSetResult[UsernameEditRepository.UsernameSetResult.USERNAME_INVALID.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditRepository$UsernameSetResult[UsernameEditRepository.UsernameSetResult.USERNAME_UNAVAILABLE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditRepository$UsernameSetResult[UsernameEditRepository.UsernameSetResult.NETWORK_ERROR.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            int[] iArr2 = new int[UsernameEditRepository.UsernameDeleteResult.values().length];
            $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditRepository$UsernameDeleteResult = iArr2;
            try {
                iArr2[UsernameEditRepository.UsernameDeleteResult.SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditRepository$UsernameDeleteResult[UsernameEditRepository.UsernameDeleteResult.NETWORK_ERROR.ordinal()] = 2;
            } catch (NoSuchFieldError unused6) {
            }
            int[] iArr3 = new int[UsernameUtil.InvalidReason.values().length];
            $SwitchMap$org$thoughtcrime$securesms$util$UsernameUtil$InvalidReason = iArr3;
            try {
                iArr3[UsernameUtil.InvalidReason.TOO_SHORT.ordinal()] = 1;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$util$UsernameUtil$InvalidReason[UsernameUtil.InvalidReason.TOO_LONG.ordinal()] = 2;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$util$UsernameUtil$InvalidReason[UsernameUtil.InvalidReason.STARTS_WITH_NUMBER.ordinal()] = 3;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$util$UsernameUtil$InvalidReason[UsernameUtil.InvalidReason.INVALID_CHARACTERS.ordinal()] = 4;
            } catch (NoSuchFieldError unused10) {
            }
        }
    }

    public /* synthetic */ void lambda$onUsernameSubmitted$1(UsernameEditRepository.UsernameSetResult usernameSetResult) {
        ThreadUtil.runOnMain(new Runnable(usernameSetResult) { // from class: org.thoughtcrime.securesms.profiles.manage.UsernameEditViewModel$$ExternalSyntheticLambda3
            public final /* synthetic */ UsernameEditRepository.UsernameSetResult f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                UsernameEditViewModel.this.lambda$onUsernameSubmitted$0(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$onUsernameSubmitted$0(UsernameEditRepository.UsernameSetResult usernameSetResult) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditRepository$UsernameSetResult[usernameSetResult.ordinal()];
        if (i == 1) {
            this.uiState.setValue(new State(ButtonState.SUBMIT_DISABLED, UsernameStatus.NONE, null));
            this.events.postValue(Event.SUBMIT_SUCCESS);
        } else if (i == 2) {
            this.uiState.setValue(new State(ButtonState.SUBMIT_DISABLED, UsernameStatus.INVALID_GENERIC, null));
            this.events.postValue(Event.SUBMIT_FAIL_INVALID);
        } else if (i == 3) {
            this.uiState.setValue(new State(ButtonState.SUBMIT_DISABLED, UsernameStatus.TAKEN, null));
            this.events.postValue(Event.SUBMIT_FAIL_TAKEN);
        } else if (i == 4) {
            this.uiState.setValue(new State(ButtonState.SUBMIT, UsernameStatus.NONE, null));
            this.events.postValue(Event.NETWORK_FAILURE);
        }
    }

    public void onUsernameDeleted() {
        this.uiState.setValue(new State(ButtonState.DELETE_LOADING, UsernameStatus.NONE, null));
        this.repo.deleteUsername(new UsernameEditRepository.Callback() { // from class: org.thoughtcrime.securesms.profiles.manage.UsernameEditViewModel$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.profiles.manage.UsernameEditRepository.Callback
            public final void onComplete(Object obj) {
                UsernameEditViewModel.this.lambda$onUsernameDeleted$3((UsernameEditRepository.UsernameDeleteResult) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onUsernameDeleted$3(UsernameEditRepository.UsernameDeleteResult usernameDeleteResult) {
        ThreadUtil.runOnMain(new Runnable(usernameDeleteResult) { // from class: org.thoughtcrime.securesms.profiles.manage.UsernameEditViewModel$$ExternalSyntheticLambda2
            public final /* synthetic */ UsernameEditRepository.UsernameDeleteResult f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                UsernameEditViewModel.this.lambda$onUsernameDeleted$2(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$onUsernameDeleted$2(UsernameEditRepository.UsernameDeleteResult usernameDeleteResult) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditRepository$UsernameDeleteResult[usernameDeleteResult.ordinal()];
        if (i == 1) {
            this.uiState.postValue(new State(ButtonState.DELETE_DISABLED, UsernameStatus.NONE, null));
            this.events.postValue(Event.DELETE_SUCCESS);
        } else if (i == 2) {
            this.uiState.postValue(new State(ButtonState.DELETE, UsernameStatus.NONE, null));
            this.events.postValue(Event.NETWORK_FAILURE);
        }
    }

    public LiveData<State> getUiState() {
        return this.uiState;
    }

    public LiveData<Event> getEvents() {
        return this.events;
    }

    private static UsernameStatus mapUsernameError(UsernameUtil.InvalidReason invalidReason) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$util$UsernameUtil$InvalidReason[invalidReason.ordinal()];
        if (i == 1) {
            return UsernameStatus.TOO_SHORT;
        }
        if (i == 2) {
            return UsernameStatus.TOO_LONG;
        }
        if (i == 3) {
            return UsernameStatus.CANNOT_START_WITH_NUMBER;
        }
        if (i != 4) {
            return UsernameStatus.INVALID_GENERIC;
        }
        return UsernameStatus.INVALID_CHARACTERS;
    }

    /* loaded from: classes4.dex */
    public static class State {
        private final ButtonState buttonState;
        private final UsernameStatus usernameStatus;

        /* synthetic */ State(ButtonState buttonState, UsernameStatus usernameStatus, AnonymousClass1 r3) {
            this(buttonState, usernameStatus);
        }

        private State(ButtonState buttonState, UsernameStatus usernameStatus) {
            this.buttonState = buttonState;
            this.usernameStatus = usernameStatus;
        }

        public ButtonState getButtonState() {
            return this.buttonState;
        }

        public UsernameStatus getUsernameStatus() {
            return this.usernameStatus;
        }
    }

    /* loaded from: classes4.dex */
    static class Factory extends ViewModelProvider.NewInstanceFactory {
        @Override // androidx.lifecycle.ViewModelProvider.NewInstanceFactory, androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new UsernameEditViewModel(null));
        }
    }
}
