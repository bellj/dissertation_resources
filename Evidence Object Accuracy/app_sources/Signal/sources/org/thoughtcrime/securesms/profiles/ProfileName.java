package org.thoughtcrime.securesms.profiles;

import android.os.Parcel;
import android.os.Parcelable;
import com.annimon.stream.Stream;
import com.annimon.stream.function.BiFunction;
import com.annimon.stream.function.Predicate;
import java.util.Objects;
import org.signal.core.util.StringUtil;
import org.thoughtcrime.securesms.util.cjkv.CJKVUtil;

/* loaded from: classes.dex */
public final class ProfileName implements Parcelable {
    public static final Parcelable.Creator<ProfileName> CREATOR = new Parcelable.Creator<ProfileName>() { // from class: org.thoughtcrime.securesms.profiles.ProfileName.1
        @Override // android.os.Parcelable.Creator
        public ProfileName createFromParcel(Parcel parcel) {
            return new ProfileName(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public ProfileName[] newArray(int i) {
            return new ProfileName[i];
        }
    };
    public static final ProfileName EMPTY = new ProfileName("", "");
    public static final int MAX_PART_LENGTH;
    private final String familyName;
    private final String givenName;
    private final String joinedName;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    private ProfileName(String str, String str2) {
        str = str == null ? "" : str;
        this.givenName = str;
        str2 = str2 == null ? "" : str2;
        this.familyName = str2;
        this.joinedName = getJoinedName(str, str2);
    }

    private ProfileName(Parcel parcel) {
        this(parcel.readString(), parcel.readString());
    }

    public String getGivenName() {
        return this.givenName;
    }

    public String getFamilyName() {
        return this.familyName;
    }

    boolean isProfileNameCJKV() {
        return isCJKV(this.givenName, this.familyName);
    }

    public boolean isEmpty() {
        return this.joinedName.isEmpty();
    }

    public boolean isGivenNameEmpty() {
        return this.givenName.isEmpty();
    }

    public String serialize() {
        if (isGivenNameEmpty()) {
            return "";
        }
        if (this.familyName.isEmpty()) {
            return this.givenName;
        }
        return String.format("%s\u0000%s", this.givenName, this.familyName);
    }

    @Override // java.lang.Object
    public String toString() {
        return this.joinedName;
    }

    public static ProfileName fromSerialized(String str) {
        if (str == null || str.isEmpty()) {
            return EMPTY;
        }
        String[] split = str.split("\u0000");
        if (split.length == 0) {
            return EMPTY;
        }
        if (split.length == 1) {
            return fromParts(split[0], "");
        }
        return fromParts(split[0], split[1]);
    }

    public static ProfileName asGiven(String str) {
        return fromParts(str, null);
    }

    public static ProfileName fromParts(String str, String str2) {
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "";
        }
        String trimToFit = StringUtil.trimToFit(str.trim(), 128);
        String trimToFit2 = StringUtil.trimToFit(str2.trim(), 128);
        if (!trimToFit.isEmpty() || !trimToFit2.isEmpty()) {
            return new ProfileName(trimToFit, trimToFit2);
        }
        return EMPTY;
    }

    private static String getJoinedName(String str, String str2) {
        if (str.isEmpty() && str2.isEmpty()) {
            return "";
        }
        if (str.isEmpty()) {
            return str2;
        }
        if (str2.isEmpty()) {
            return str;
        }
        if (isCJKV(str, str2)) {
            return String.format("%s %s", str2, str);
        }
        return String.format("%s %s", str, str2);
    }

    private static boolean isCJKV(String str, String str2) {
        if (!str.isEmpty() || !str2.isEmpty()) {
            return ((Boolean) Stream.of(str, str2).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.profiles.ProfileName$$ExternalSyntheticLambda0
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return ((String) obj).isEmpty();
                }
            }).reduce(Boolean.TRUE, new BiFunction() { // from class: org.thoughtcrime.securesms.profiles.ProfileName$$ExternalSyntheticLambda1
                @Override // com.annimon.stream.function.BiFunction
                public final Object apply(Object obj, Object obj2) {
                    return ProfileName.lambda$isCJKV$0((Boolean) obj, (String) obj2);
                }
            })).booleanValue();
        }
        return false;
    }

    public static /* synthetic */ Boolean lambda$isCJKV$0(Boolean bool, String str) {
        return Boolean.valueOf(bool.booleanValue() && CJKVUtil.isCJKV(str));
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.givenName);
        parcel.writeString(this.familyName);
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || ProfileName.class != obj.getClass()) {
            return false;
        }
        ProfileName profileName = (ProfileName) obj;
        if (!Objects.equals(this.givenName, profileName.givenName) || !Objects.equals(this.familyName, profileName.familyName)) {
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Objects.hash(this.givenName, this.familyName);
    }
}
