package org.thoughtcrime.securesms.profiles;

import android.content.Context;
import org.thoughtcrime.securesms.mms.MediaConstraints;

/* loaded from: classes4.dex */
public class ProfileMediaConstraints extends MediaConstraints {
    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getAudioMaxSize(Context context) {
        return 0;
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getDocumentMaxSize(Context context) {
        return 0;
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getGifMaxSize(Context context) {
        return 0;
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getImageMaxHeight(Context context) {
        return 640;
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getImageMaxSize(Context context) {
        return 5242880;
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getImageMaxWidth(Context context) {
        return 640;
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int getVideoMaxSize(Context context) {
        return 0;
    }

    @Override // org.thoughtcrime.securesms.mms.MediaConstraints
    public int[] getImageDimensionTargets(Context context) {
        return new int[]{getImageMaxWidth(context)};
    }
}
