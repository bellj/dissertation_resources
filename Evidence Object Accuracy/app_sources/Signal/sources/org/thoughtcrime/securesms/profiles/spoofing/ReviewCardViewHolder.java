package org.thoughtcrime.securesms.profiles.spoofing;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.profiles.spoofing.ReviewCard;
import org.thoughtcrime.securesms.profiles.spoofing.ReviewCardViewHolder;
import org.thoughtcrime.securesms.util.SpanUtil;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public class ReviewCardViewHolder extends RecyclerView.ViewHolder {
    private final AvatarImageView avatar;
    private final int groupsInCommonResId;
    private final TextView name;
    private final int noGroupsInCommonResId;
    private final Button primaryAction;
    private final Button secondaryAction;
    private final TextView subtextLine1;
    private final TextView subtextLine2;
    private final TextView title;

    /* loaded from: classes4.dex */
    public interface Callbacks {
        void onCardClicked(int i);

        void onPrimaryActionItemClicked(int i);

        void onSecondaryActionItemClicked(int i);
    }

    public ReviewCardViewHolder(View view, int i, int i2, Callbacks callbacks) {
        super(view);
        this.noGroupsInCommonResId = i;
        this.groupsInCommonResId = i2;
        this.title = (TextView) view.findViewById(R.id.card_title);
        this.avatar = (AvatarImageView) view.findViewById(R.id.card_avatar);
        this.name = (TextView) view.findViewById(R.id.card_name);
        this.subtextLine1 = (TextView) view.findViewById(R.id.card_subtext_line1);
        this.subtextLine2 = (TextView) view.findViewById(R.id.card_subtext_line2);
        Button button = (Button) view.findViewById(R.id.card_primary_action_button);
        this.primaryAction = button;
        Button button2 = (Button) view.findViewById(R.id.card_secondary_action_button);
        this.secondaryAction = button2;
        view.findViewById(R.id.card_tap_target).setOnClickListener(new View.OnClickListener(callbacks) { // from class: org.thoughtcrime.securesms.profiles.spoofing.ReviewCardViewHolder$$ExternalSyntheticLambda0
            public final /* synthetic */ ReviewCardViewHolder.Callbacks f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ReviewCardViewHolder.this.lambda$new$0(this.f$1, view2);
            }
        });
        button.setOnClickListener(new View.OnClickListener(callbacks) { // from class: org.thoughtcrime.securesms.profiles.spoofing.ReviewCardViewHolder$$ExternalSyntheticLambda1
            public final /* synthetic */ ReviewCardViewHolder.Callbacks f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ReviewCardViewHolder.this.lambda$new$1(this.f$1, view2);
            }
        });
        button2.setOnClickListener(new View.OnClickListener(callbacks) { // from class: org.thoughtcrime.securesms.profiles.spoofing.ReviewCardViewHolder$$ExternalSyntheticLambda2
            public final /* synthetic */ ReviewCardViewHolder.Callbacks f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ReviewCardViewHolder.this.lambda$new$2(this.f$1, view2);
            }
        });
    }

    public /* synthetic */ void lambda$new$0(Callbacks callbacks, View view) {
        if (getAdapterPosition() != -1) {
            callbacks.onCardClicked(getAdapterPosition());
        }
    }

    public /* synthetic */ void lambda$new$1(Callbacks callbacks, View view) {
        if (getAdapterPosition() != -1) {
            callbacks.onPrimaryActionItemClicked(getAdapterPosition());
        }
    }

    public /* synthetic */ void lambda$new$2(Callbacks callbacks, View view) {
        if (getAdapterPosition() != -1) {
            callbacks.onSecondaryActionItemClicked(getAdapterPosition());
        }
    }

    public void bind(ReviewCard reviewCard) {
        Context context = this.itemView.getContext();
        this.avatar.setAvatar(reviewCard.getReviewRecipient());
        this.name.setText(reviewCard.getReviewRecipient().getDisplayName(context));
        this.title.setText(getTitleResId(reviewCard.getCardType()));
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCard$CardType[reviewCard.getCardType().ordinal()];
        if (i == 1 || i == 2) {
            setNonContactSublines(context, reviewCard);
        } else if (i == 3) {
            this.subtextLine1.setText(reviewCard.getReviewRecipient().getE164().orElse(null));
            this.subtextLine2.setText(getGroupsInCommon(reviewCard.getInCommonGroupsCount()));
        } else {
            throw new AssertionError();
        }
        setActions(reviewCard);
    }

    private void setNonContactSublines(Context context, ReviewCard reviewCard) {
        this.subtextLine1.setText(getGroupsInCommon(reviewCard.getInCommonGroupsCount()));
        if (reviewCard.getNameChange() != null) {
            this.subtextLine2.setText(SpanUtil.italic(context.getString(R.string.ReviewCard__recently_changed, reviewCard.getNameChange().getPrevious(), reviewCard.getNameChange().getNew())));
        }
    }

    private void setActions(ReviewCard reviewCard) {
        setAction(reviewCard.getPrimaryAction(), this.primaryAction);
        setAction(reviewCard.getSecondaryAction(), this.secondaryAction);
    }

    private String getGroupsInCommon(int i) {
        if (i == 0) {
            return this.itemView.getContext().getString(this.noGroupsInCommonResId);
        }
        return this.itemView.getResources().getQuantityString(this.groupsInCommonResId, i, Integer.valueOf(i));
    }

    private static void setAction(ReviewCard.Action action, Button button) {
        if (action != null) {
            button.setText(getActionLabelResId(action));
            button.setVisibility(0);
            return;
        }
        button.setVisibility(8);
    }

    private static int getTitleResId(ReviewCard.CardType cardType) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCard$CardType[cardType.ordinal()];
        if (i == 1) {
            return R.string.ReviewCard__member;
        }
        if (i == 2) {
            return R.string.ReviewCard__request;
        }
        if (i == 3) {
            return R.string.ReviewCard__your_contact;
        }
        throw new IllegalArgumentException("Unsupported card type " + cardType);
    }

    /* renamed from: org.thoughtcrime.securesms.profiles.spoofing.ReviewCardViewHolder$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCard$Action;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCard$CardType;

        static {
            int[] iArr = new int[ReviewCard.Action.values().length];
            $SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCard$Action = iArr;
            try {
                iArr[ReviewCard.Action.UPDATE_CONTACT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCard$Action[ReviewCard.Action.DELETE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCard$Action[ReviewCard.Action.BLOCK.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCard$Action[ReviewCard.Action.REMOVE_FROM_GROUP.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            int[] iArr2 = new int[ReviewCard.CardType.values().length];
            $SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCard$CardType = iArr2;
            try {
                iArr2[ReviewCard.CardType.MEMBER.ordinal()] = 1;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCard$CardType[ReviewCard.CardType.REQUEST.ordinal()] = 2;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCard$CardType[ReviewCard.CardType.YOUR_CONTACT.ordinal()] = 3;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    private static int getActionLabelResId(ReviewCard.Action action) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCard$Action[action.ordinal()];
        if (i == 1) {
            return R.string.ReviewCard__update_contact;
        }
        if (i == 2) {
            return R.string.ReviewCard__delete;
        }
        if (i == 3) {
            return R.string.ReviewCard__block;
        }
        if (i == 4) {
            return R.string.ReviewCard__remove_from_group;
        }
        throw new IllegalArgumentException("Unsupported action: " + action);
    }
}
