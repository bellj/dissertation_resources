package org.thoughtcrime.securesms.profiles.manage;

import android.app.Application;
import java.io.IOException;
import java.util.concurrent.Executor;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.profiles.manage.UsernameEditRepository;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.push.exceptions.UsernameMalformedException;
import org.whispersystems.signalservice.api.push.exceptions.UsernameTakenException;

/* loaded from: classes4.dex */
public class UsernameEditRepository {
    private static final String TAG = Log.tag(UsernameEditRepository.class);
    private final SignalServiceAccountManager accountManager = ApplicationDependencies.getSignalServiceAccountManager();
    private final Application application = ApplicationDependencies.getApplication();
    private final Executor executor = SignalExecutors.UNBOUNDED;

    /* loaded from: classes4.dex */
    public interface Callback<E> {
        void onComplete(E e);
    }

    /* loaded from: classes4.dex */
    enum UsernameAvailableResult {
        TRUE,
        FALSE,
        NETWORK_ERROR
    }

    /* loaded from: classes4.dex */
    public enum UsernameDeleteResult {
        SUCCESS,
        NETWORK_ERROR
    }

    /* loaded from: classes4.dex */
    public enum UsernameSetResult {
        SUCCESS,
        USERNAME_UNAVAILABLE,
        USERNAME_INVALID,
        NETWORK_ERROR
    }

    public /* synthetic */ void lambda$setUsername$0(Callback callback, String str) {
        callback.onComplete(setUsernameInternal(str));
    }

    public void setUsername(String str, Callback<UsernameSetResult> callback) {
        this.executor.execute(new Runnable(callback, str) { // from class: org.thoughtcrime.securesms.profiles.manage.UsernameEditRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ UsernameEditRepository.Callback f$1;
            public final /* synthetic */ String f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                UsernameEditRepository.m2492$r8$lambda$Ps1kvHwnz5D4Jv0Ynn9zEhMTJU(UsernameEditRepository.this, this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$deleteUsername$1(Callback callback) {
        callback.onComplete(deleteUsernameInternal());
    }

    public void deleteUsername(Callback<UsernameDeleteResult> callback) {
        this.executor.execute(new Runnable(callback) { // from class: org.thoughtcrime.securesms.profiles.manage.UsernameEditRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ UsernameEditRepository.Callback f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                UsernameEditRepository.$r8$lambda$pVcZyMOpFQrEsrFr2cPhMhIf2Dc(UsernameEditRepository.this, this.f$1);
            }
        });
    }

    private UsernameSetResult setUsernameInternal(String str) {
        try {
            this.accountManager.setUsername(str);
            SignalDatabase.recipients().setUsername(Recipient.self().getId(), str);
            Log.i(TAG, "[setUsername] Successfully set username.");
            return UsernameSetResult.SUCCESS;
        } catch (UsernameMalformedException unused) {
            Log.w(TAG, "[setUsername] Username malformed.");
            return UsernameSetResult.USERNAME_INVALID;
        } catch (UsernameTakenException unused2) {
            Log.w(TAG, "[setUsername] Username taken.");
            return UsernameSetResult.USERNAME_UNAVAILABLE;
        } catch (IOException e) {
            Log.w(TAG, "[setUsername] Generic network exception.", e);
            return UsernameSetResult.NETWORK_ERROR;
        }
    }

    private UsernameDeleteResult deleteUsernameInternal() {
        try {
            this.accountManager.deleteUsername();
            SignalDatabase.recipients().setUsername(Recipient.self().getId(), null);
            Log.i(TAG, "[deleteUsername] Successfully deleted the username.");
            return UsernameDeleteResult.SUCCESS;
        } catch (IOException e) {
            Log.w(TAG, "[deleteUsername] Generic network exception.", e);
            return UsernameDeleteResult.NETWORK_ERROR;
        }
    }
}
