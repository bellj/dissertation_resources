package org.thoughtcrime.securesms.profiles.manage;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contactshare.SimpleTextWatcher;
import org.thoughtcrime.securesms.profiles.manage.UsernameEditViewModel;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;

/* loaded from: classes4.dex */
public class UsernameEditFragment extends LoggingFragment {
    private static final float DISABLED_ALPHA;
    private CircularProgressMaterialButton deleteButton;
    private CircularProgressMaterialButton submitButton;
    private EditText usernameInput;
    private TextView usernameSubtext;
    private UsernameEditViewModel viewModel;

    public static UsernameEditFragment newInstance() {
        return new UsernameEditFragment();
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.username_edit_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.usernameInput = (EditText) view.findViewById(R.id.username_text);
        this.usernameSubtext = (TextView) view.findViewById(R.id.username_subtext);
        this.submitButton = (CircularProgressMaterialButton) view.findViewById(R.id.username_submit_button);
        this.deleteButton = (CircularProgressMaterialButton) view.findViewById(R.id.username_delete_button);
        ((Toolbar) view.findViewById(R.id.toolbar)).setNavigationOnClickListener(new View.OnClickListener(view) { // from class: org.thoughtcrime.securesms.profiles.manage.UsernameEditFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                UsernameEditFragment.m2491$r8$lambda$TRH7p0LwFTxOioODlTy9iXlCKU(this.f$0, view2);
            }
        });
        UsernameEditViewModel usernameEditViewModel = (UsernameEditViewModel) ViewModelProviders.of(this, new UsernameEditViewModel.Factory()).get(UsernameEditViewModel.class);
        this.viewModel = usernameEditViewModel;
        usernameEditViewModel.getUiState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.profiles.manage.UsernameEditFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                UsernameEditFragment.$r8$lambda$3_1x4XrUy6aGXjflw6zPLJ1wZB0(UsernameEditFragment.this, (UsernameEditViewModel.State) obj);
            }
        });
        this.viewModel.getEvents().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.profiles.manage.UsernameEditFragment$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                UsernameEditFragment.m2490$r8$lambda$IuFpp2uPqufKHjrjukX89EVyW8(UsernameEditFragment.this, (UsernameEditViewModel.Event) obj);
            }
        });
        this.submitButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.profiles.manage.UsernameEditFragment$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                UsernameEditFragment.$r8$lambda$sUkOoAc9OeZGZ4P_kvDTslTGyFo(UsernameEditFragment.this, view2);
            }
        });
        this.deleteButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.profiles.manage.UsernameEditFragment$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                UsernameEditFragment.$r8$lambda$H3F1B7f9jZTa7QuAPsOFteIOWRw(UsernameEditFragment.this, view2);
            }
        });
        this.usernameInput.setText(Recipient.self().getUsername().orElse(null));
        this.usernameInput.addTextChangedListener(new SimpleTextWatcher() { // from class: org.thoughtcrime.securesms.profiles.manage.UsernameEditFragment.1
            @Override // org.thoughtcrime.securesms.contactshare.SimpleTextWatcher
            public void onTextChanged(String str) {
                UsernameEditFragment.this.viewModel.onUsernameUpdated(str);
            }
        });
        this.usernameInput.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: org.thoughtcrime.securesms.profiles.manage.UsernameEditFragment$$ExternalSyntheticLambda5
            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                return UsernameEditFragment.$r8$lambda$KkeedhuyR0jZ06eLcmgWP5HOUZw(UsernameEditFragment.this, textView, i, keyEvent);
            }
        });
    }

    public static /* synthetic */ void lambda$onViewCreated$0(View view, View view2) {
        Navigation.findNavController(view).popBackStack();
    }

    public /* synthetic */ void lambda$onViewCreated$1(View view) {
        this.viewModel.onUsernameSubmitted(this.usernameInput.getText().toString());
    }

    public /* synthetic */ void lambda$onViewCreated$2(View view) {
        this.viewModel.onUsernameDeleted();
    }

    public /* synthetic */ boolean lambda$onViewCreated$3(TextView textView, int i, KeyEvent keyEvent) {
        if (i != 6) {
            return false;
        }
        this.viewModel.onUsernameSubmitted(this.usernameInput.getText().toString());
        return true;
    }

    public void onUiStateChanged(UsernameEditViewModel.State state) {
        this.usernameInput.setEnabled(true);
        switch (AnonymousClass2.$SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$ButtonState[state.getButtonState().ordinal()]) {
            case 1:
                this.submitButton.cancelSpinning();
                this.submitButton.setVisibility(0);
                this.submitButton.setEnabled(true);
                this.submitButton.setAlpha(1.0f);
                this.deleteButton.setVisibility(8);
                break;
            case 2:
                this.submitButton.cancelSpinning();
                this.submitButton.setVisibility(0);
                this.submitButton.setEnabled(false);
                this.submitButton.setAlpha(DISABLED_ALPHA);
                this.deleteButton.setVisibility(8);
                break;
            case 3:
                this.submitButton.setSpinning();
                this.submitButton.setVisibility(0);
                this.submitButton.setAlpha(1.0f);
                this.deleteButton.setVisibility(8);
                this.usernameInput.setEnabled(false);
                break;
            case 4:
                this.deleteButton.cancelSpinning();
                this.deleteButton.setVisibility(0);
                this.deleteButton.setEnabled(true);
                this.deleteButton.setAlpha(1.0f);
                this.submitButton.setVisibility(8);
                break;
            case 5:
                this.deleteButton.cancelSpinning();
                this.deleteButton.setVisibility(0);
                this.deleteButton.setEnabled(false);
                this.deleteButton.setAlpha(DISABLED_ALPHA);
                this.submitButton.setVisibility(8);
                break;
            case 6:
                this.deleteButton.setSpinning();
                this.deleteButton.setVisibility(0);
                this.deleteButton.setAlpha(1.0f);
                this.submitButton.setVisibility(8);
                this.usernameInput.setEnabled(false);
                break;
        }
        switch (AnonymousClass2.$SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$UsernameStatus[state.getUsernameStatus().ordinal()]) {
            case 1:
                this.usernameSubtext.setText("");
                return;
            case 2:
            case 3:
                this.usernameSubtext.setText(getResources().getString(R.string.UsernameEditFragment_usernames_must_be_between_a_and_b_characters, 4, 26));
                this.usernameSubtext.setTextColor(getResources().getColor(R.color.core_red));
                return;
            case 4:
                this.usernameSubtext.setText(R.string.UsernameEditFragment_usernames_can_only_include);
                this.usernameSubtext.setTextColor(getResources().getColor(R.color.core_red));
                return;
            case 5:
                this.usernameSubtext.setText(R.string.UsernameEditFragment_usernames_cannot_begin_with_a_number);
                this.usernameSubtext.setTextColor(getResources().getColor(R.color.core_red));
                return;
            case 6:
                this.usernameSubtext.setText(R.string.UsernameEditFragment_username_is_invalid);
                this.usernameSubtext.setTextColor(getResources().getColor(R.color.core_red));
                return;
            case 7:
                this.usernameSubtext.setText(R.string.UsernameEditFragment_this_username_is_taken);
                this.usernameSubtext.setTextColor(getResources().getColor(R.color.core_red));
                return;
            case 8:
                this.usernameSubtext.setText(R.string.UsernameEditFragment_this_username_is_available);
                this.usernameSubtext.setTextColor(getResources().getColor(R.color.core_green));
                return;
            default:
                return;
        }
    }

    /* renamed from: org.thoughtcrime.securesms.profiles.manage.UsernameEditFragment$2 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$ButtonState;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$Event;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$UsernameStatus;

        static {
            int[] iArr = new int[UsernameEditViewModel.Event.values().length];
            $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$Event = iArr;
            try {
                iArr[UsernameEditViewModel.Event.SUBMIT_SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$Event[UsernameEditViewModel.Event.SUBMIT_FAIL_TAKEN.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$Event[UsernameEditViewModel.Event.SUBMIT_FAIL_INVALID.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$Event[UsernameEditViewModel.Event.DELETE_SUCCESS.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$Event[UsernameEditViewModel.Event.NETWORK_FAILURE.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            int[] iArr2 = new int[UsernameEditViewModel.UsernameStatus.values().length];
            $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$UsernameStatus = iArr2;
            try {
                iArr2[UsernameEditViewModel.UsernameStatus.NONE.ordinal()] = 1;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$UsernameStatus[UsernameEditViewModel.UsernameStatus.TOO_SHORT.ordinal()] = 2;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$UsernameStatus[UsernameEditViewModel.UsernameStatus.TOO_LONG.ordinal()] = 3;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$UsernameStatus[UsernameEditViewModel.UsernameStatus.INVALID_CHARACTERS.ordinal()] = 4;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$UsernameStatus[UsernameEditViewModel.UsernameStatus.CANNOT_START_WITH_NUMBER.ordinal()] = 5;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$UsernameStatus[UsernameEditViewModel.UsernameStatus.INVALID_GENERIC.ordinal()] = 6;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$UsernameStatus[UsernameEditViewModel.UsernameStatus.TAKEN.ordinal()] = 7;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$UsernameStatus[UsernameEditViewModel.UsernameStatus.AVAILABLE.ordinal()] = 8;
            } catch (NoSuchFieldError unused13) {
            }
            int[] iArr3 = new int[UsernameEditViewModel.ButtonState.values().length];
            $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$ButtonState = iArr3;
            try {
                iArr3[UsernameEditViewModel.ButtonState.SUBMIT.ordinal()] = 1;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$ButtonState[UsernameEditViewModel.ButtonState.SUBMIT_DISABLED.ordinal()] = 2;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$ButtonState[UsernameEditViewModel.ButtonState.SUBMIT_LOADING.ordinal()] = 3;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$ButtonState[UsernameEditViewModel.ButtonState.DELETE.ordinal()] = 4;
            } catch (NoSuchFieldError unused17) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$ButtonState[UsernameEditViewModel.ButtonState.DELETE_DISABLED.ordinal()] = 5;
            } catch (NoSuchFieldError unused18) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$ButtonState[UsernameEditViewModel.ButtonState.DELETE_LOADING.ordinal()] = 6;
            } catch (NoSuchFieldError unused19) {
            }
        }
    }

    public void onEvent(UsernameEditViewModel.Event event) {
        int i = AnonymousClass2.$SwitchMap$org$thoughtcrime$securesms$profiles$manage$UsernameEditViewModel$Event[event.ordinal()];
        if (i == 1) {
            Toast.makeText(requireContext(), (int) R.string.UsernameEditFragment_successfully_set_username, 0).show();
            NavHostFragment.findNavController(this).popBackStack();
        } else if (i == 2) {
            Toast.makeText(requireContext(), (int) R.string.UsernameEditFragment_this_username_is_taken, 0).show();
        } else if (i == 3) {
            Toast.makeText(requireContext(), (int) R.string.UsernameEditFragment_username_is_invalid, 0).show();
        } else if (i == 4) {
            Toast.makeText(requireContext(), (int) R.string.UsernameEditFragment_successfully_removed_username, 0).show();
            NavHostFragment.findNavController(this).popBackStack();
        } else if (i == 5) {
            Toast.makeText(requireContext(), (int) R.string.UsernameEditFragment_encountered_a_network_error, 0).show();
        }
    }
}
