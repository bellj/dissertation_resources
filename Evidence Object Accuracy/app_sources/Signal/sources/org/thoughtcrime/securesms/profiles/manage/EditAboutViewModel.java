package org.thoughtcrime.securesms.profiles.manage;

import android.content.Context;
import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import org.thoughtcrime.securesms.profiles.manage.ManageProfileRepository;
import org.thoughtcrime.securesms.util.SingleLiveEvent;

/* loaded from: classes4.dex */
public final class EditAboutViewModel extends ViewModel {
    private final SingleLiveEvent<Event> events = new SingleLiveEvent<>();
    private final ManageProfileRepository repository = new ManageProfileRepository();
    private final MutableLiveData<SaveState> saveState = new MutableLiveData<>(SaveState.IDLE);

    /* loaded from: classes4.dex */
    public enum Event {
        NETWORK_FAILURE
    }

    /* loaded from: classes4.dex */
    public enum SaveState {
        IDLE,
        IN_PROGRESS,
        DONE
    }

    public LiveData<SaveState> getSaveState() {
        return this.saveState;
    }

    public LiveData<Event> getEvents() {
        return this.events;
    }

    public void onSaveClicked(Context context, String str, String str2) {
        this.saveState.setValue(SaveState.IN_PROGRESS);
        this.repository.setAbout(context, str, str2, new Consumer() { // from class: org.thoughtcrime.securesms.profiles.manage.EditAboutViewModel$$ExternalSyntheticLambda0
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                EditAboutViewModel.this.lambda$onSaveClicked$0((ManageProfileRepository.Result) obj);
            }
        });
    }

    /* renamed from: org.thoughtcrime.securesms.profiles.manage.EditAboutViewModel$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$profiles$manage$ManageProfileRepository$Result;

        static {
            int[] iArr = new int[ManageProfileRepository.Result.values().length];
            $SwitchMap$org$thoughtcrime$securesms$profiles$manage$ManageProfileRepository$Result = iArr;
            try {
                iArr[ManageProfileRepository.Result.SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$ManageProfileRepository$Result[ManageProfileRepository.Result.FAILURE_NETWORK.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    public /* synthetic */ void lambda$onSaveClicked$0(ManageProfileRepository.Result result) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$profiles$manage$ManageProfileRepository$Result[result.ordinal()];
        if (i == 1) {
            this.saveState.postValue(SaveState.DONE);
        } else if (i == 2) {
            this.saveState.postValue(SaveState.IDLE);
            this.events.postValue(Event.NETWORK_FAILURE);
        }
    }
}
