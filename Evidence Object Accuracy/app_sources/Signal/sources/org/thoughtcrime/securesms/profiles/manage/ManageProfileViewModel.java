package org.thoughtcrime.securesms.profiles.manage;

import android.content.Context;
import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import j$.util.Optional;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.RetrieveProfileJob;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.profiles.ProfileName;
import org.thoughtcrime.securesms.profiles.manage.ManageProfileRepository;
import org.thoughtcrime.securesms.profiles.manage.ManageProfileViewModel;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientForeverObserver;
import org.thoughtcrime.securesms.util.DefaultValueLiveData;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.whispersystems.signalservice.api.util.StreamDetails;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public class ManageProfileViewModel extends ViewModel {
    private static final String TAG = Log.tag(ManageProfileViewModel.class);
    private final MutableLiveData<String> about = new MutableLiveData<>();
    private final MutableLiveData<String> aboutEmoji = new MutableLiveData<>();
    private final LiveData<AvatarState> avatarState;
    private final MutableLiveData<Optional<Badge>> badge = new DefaultValueLiveData(Optional.empty());
    private final SingleLiveEvent<Event> events = new SingleLiveEvent<>();
    private final MutableLiveData<InternalAvatarState> internalAvatarState;
    private final RecipientForeverObserver observer;
    private byte[] previousAvatar;
    private final MutableLiveData<ProfileName> profileName = new MutableLiveData<>();
    private final ManageProfileRepository repository = new ManageProfileRepository();
    private final MutableLiveData<String> username = new MutableLiveData<>();

    /* loaded from: classes4.dex */
    public enum Event {
        AVATAR_NETWORK_FAILURE,
        AVATAR_DISK_FAILURE
    }

    /* loaded from: classes4.dex */
    public enum LoadingState {
        LOADING,
        LOADED
    }

    public ManageProfileViewModel() {
        MutableLiveData<InternalAvatarState> mutableLiveData = new MutableLiveData<>();
        this.internalAvatarState = mutableLiveData;
        ManageProfileViewModel$$ExternalSyntheticLambda0 manageProfileViewModel$$ExternalSyntheticLambda0 = new RecipientForeverObserver() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileViewModel$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.recipients.RecipientForeverObserver
            public final void onRecipientChanged(Recipient recipient) {
                ManageProfileViewModel.this.onRecipientChanged(recipient);
            }
        };
        this.observer = manageProfileViewModel$$ExternalSyntheticLambda0;
        this.avatarState = LiveDataUtil.combineLatest(Recipient.self().live().getLiveData(), mutableLiveData, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileViewModel$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return ManageProfileViewModel.lambda$new$0((Recipient) obj, (ManageProfileViewModel.InternalAvatarState) obj2);
            }
        });
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileViewModel$$ExternalSyntheticLambda2
            @Override // java.lang.Runnable
            public final void run() {
                ManageProfileViewModel.this.lambda$new$1();
            }
        });
        Recipient.self().live().observeForever(manageProfileViewModel$$ExternalSyntheticLambda0);
    }

    public static /* synthetic */ AvatarState lambda$new$0(Recipient recipient, InternalAvatarState internalAvatarState) {
        return new AvatarState(internalAvatarState, recipient);
    }

    public /* synthetic */ void lambda$new$1() {
        onRecipientChanged(Recipient.self().fresh());
        ApplicationDependencies.getJobManager().add(RetrieveProfileJob.forRecipient(Recipient.self().getId()));
    }

    public LiveData<AvatarState> getAvatar() {
        return Transformations.distinctUntilChanged(this.avatarState);
    }

    public LiveData<ProfileName> getProfileName() {
        return this.profileName;
    }

    public LiveData<String> getUsername() {
        return this.username;
    }

    public LiveData<String> getAbout() {
        return this.about;
    }

    public LiveData<String> getAboutEmoji() {
        return this.aboutEmoji;
    }

    public LiveData<Optional<Badge>> getBadge() {
        return this.badge;
    }

    public LiveData<Event> getEvents() {
        return this.events;
    }

    public boolean shouldShowUsername() {
        return FeatureFlags.usernames();
    }

    public void onAvatarSelected(Context context, Media media) {
        this.previousAvatar = this.internalAvatarState.getValue() != null ? this.internalAvatarState.getValue().getAvatar() : null;
        if (media == null) {
            this.internalAvatarState.postValue(InternalAvatarState.loading(null));
            this.repository.clearAvatar(context, new Consumer() { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileViewModel$$ExternalSyntheticLambda4
                @Override // androidx.core.util.Consumer
                public final void accept(Object obj) {
                    ManageProfileViewModel.this.lambda$onAvatarSelected$2((ManageProfileRepository.Result) obj);
                }
            });
            return;
        }
        SignalExecutors.BOUNDED.execute(new Runnable(context, media) { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileViewModel$$ExternalSyntheticLambda5
            public final /* synthetic */ Context f$1;
            public final /* synthetic */ Media f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ManageProfileViewModel.this.lambda$onAvatarSelected$4(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$onAvatarSelected$2(ManageProfileRepository.Result result) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$profiles$manage$ManageProfileRepository$Result[result.ordinal()];
        if (i == 1) {
            this.internalAvatarState.postValue(InternalAvatarState.loaded(null));
            this.previousAvatar = null;
        } else if (i == 2) {
            this.internalAvatarState.postValue(InternalAvatarState.loaded(this.previousAvatar));
            this.events.postValue(Event.AVATAR_NETWORK_FAILURE);
        }
    }

    public /* synthetic */ void lambda$onAvatarSelected$4(Context context, Media media) {
        try {
            byte[] readFully = StreamUtil.readFully(BlobProvider.getInstance().getStream(context, media.getUri()));
            this.internalAvatarState.postValue(InternalAvatarState.loading(readFully));
            this.repository.setAvatar(context, readFully, media.getMimeType(), new Consumer(readFully) { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileViewModel$$ExternalSyntheticLambda3
                public final /* synthetic */ byte[] f$1;

                {
                    this.f$1 = r2;
                }

                @Override // androidx.core.util.Consumer
                public final void accept(Object obj) {
                    ManageProfileViewModel.this.lambda$onAvatarSelected$3(this.f$1, (ManageProfileRepository.Result) obj);
                }
            });
        } catch (IOException e) {
            Log.w(TAG, "Failed to save avatar!", e);
            this.events.postValue(Event.AVATAR_DISK_FAILURE);
        }
    }

    /* renamed from: org.thoughtcrime.securesms.profiles.manage.ManageProfileViewModel$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$profiles$manage$ManageProfileRepository$Result;

        static {
            int[] iArr = new int[ManageProfileRepository.Result.values().length];
            $SwitchMap$org$thoughtcrime$securesms$profiles$manage$ManageProfileRepository$Result = iArr;
            try {
                iArr[ManageProfileRepository.Result.SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$ManageProfileRepository$Result[ManageProfileRepository.Result.FAILURE_NETWORK.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    public /* synthetic */ void lambda$onAvatarSelected$3(byte[] bArr, ManageProfileRepository.Result result) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$profiles$manage$ManageProfileRepository$Result[result.ordinal()];
        if (i == 1) {
            this.internalAvatarState.postValue(InternalAvatarState.loaded(bArr));
            this.previousAvatar = bArr;
        } else if (i == 2) {
            this.internalAvatarState.postValue(InternalAvatarState.loaded(this.previousAvatar));
            this.events.postValue(Event.AVATAR_NETWORK_FAILURE);
        }
    }

    public boolean canRemoveAvatar() {
        return this.internalAvatarState.getValue() != null;
    }

    public void onRecipientChanged(Recipient recipient) {
        this.profileName.postValue(recipient.getProfileName());
        this.username.postValue(recipient.getUsername().orElse(null));
        this.about.postValue(recipient.getAbout());
        this.aboutEmoji.postValue(recipient.getAboutEmoji());
        this.badge.postValue(Optional.ofNullable(recipient.getFeaturedBadge()));
        renderAvatar(AvatarHelper.getSelfProfileAvatarStream(ApplicationDependencies.getApplication()));
    }

    private void renderAvatar(StreamDetails streamDetails) {
        if (streamDetails != null) {
            try {
                this.internalAvatarState.postValue(InternalAvatarState.loaded(StreamUtil.readFully(streamDetails.getStream())));
            } catch (IOException unused) {
                Log.w(TAG, "Failed to read avatar!");
                this.internalAvatarState.postValue(InternalAvatarState.none());
            }
        } else {
            this.internalAvatarState.postValue(InternalAvatarState.none());
        }
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        Recipient.self().live().lambda$asObservable$6(this.observer);
    }

    /* loaded from: classes4.dex */
    public static final class AvatarState {
        private final InternalAvatarState internalAvatarState;
        private final Recipient self;

        public AvatarState(InternalAvatarState internalAvatarState, Recipient recipient) {
            this.internalAvatarState = internalAvatarState;
            this.self = recipient;
        }

        public byte[] getAvatar() {
            return this.internalAvatarState.avatar;
        }

        public LoadingState getLoadingState() {
            return this.internalAvatarState.loadingState;
        }

        public Recipient getSelf() {
            return this.self;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || AvatarState.class != obj.getClass()) {
                return false;
            }
            AvatarState avatarState = (AvatarState) obj;
            if (!Objects.equals(this.internalAvatarState, avatarState.internalAvatarState) || !Objects.equals(this.self, avatarState.self)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return Objects.hash(this.internalAvatarState, this.self);
        }
    }

    /* loaded from: classes4.dex */
    public static final class InternalAvatarState {
        private final byte[] avatar;
        private final LoadingState loadingState;

        public InternalAvatarState(byte[] bArr, LoadingState loadingState) {
            this.avatar = bArr;
            this.loadingState = loadingState;
        }

        public static InternalAvatarState none() {
            return new InternalAvatarState(null, LoadingState.LOADED);
        }

        public static InternalAvatarState loaded(byte[] bArr) {
            return new InternalAvatarState(bArr, LoadingState.LOADED);
        }

        public static InternalAvatarState loading(byte[] bArr) {
            return new InternalAvatarState(bArr, LoadingState.LOADING);
        }

        public byte[] getAvatar() {
            return this.avatar;
        }

        public LoadingState getLoadingState() {
            return this.loadingState;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || InternalAvatarState.class != obj.getClass()) {
                return false;
            }
            InternalAvatarState internalAvatarState = (InternalAvatarState) obj;
            if (!Arrays.equals(this.avatar, internalAvatarState.avatar) || this.loadingState != internalAvatarState.loadingState) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            return (Objects.hash(this.loadingState) * 31) + Arrays.hashCode(this.avatar);
        }
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes4.dex */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        @Override // androidx.lifecycle.ViewModelProvider.NewInstanceFactory, androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            T cast = cls.cast(new ManageProfileViewModel());
            Objects.requireNonNull(cast);
            return cast;
        }
    }
}
