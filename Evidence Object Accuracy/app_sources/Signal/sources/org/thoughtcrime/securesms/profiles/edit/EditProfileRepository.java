package org.thoughtcrime.securesms.profiles.edit;

import androidx.core.util.Consumer;
import j$.util.Optional;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.profiles.ProfileName;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public interface EditProfileRepository {

    /* loaded from: classes4.dex */
    public enum UploadResult {
        SUCCESS,
        ERROR_IO,
        ERROR_BAD_RECIPIENT
    }

    void getCurrentAvatar(Consumer<byte[]> consumer);

    void getCurrentAvatarColor(Consumer<AvatarColor> consumer);

    void getCurrentDescription(Consumer<String> consumer);

    void getCurrentDisplayName(Consumer<String> consumer);

    void getCurrentName(Consumer<String> consumer);

    void getCurrentProfileName(Consumer<ProfileName> consumer);

    void getCurrentUsername(Consumer<Optional<String>> consumer);

    void uploadProfile(ProfileName profileName, String str, boolean z, String str2, boolean z2, byte[] bArr, boolean z3, Consumer<UploadResult> consumer);
}
