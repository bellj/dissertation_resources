package org.thoughtcrime.securesms.profiles.spoofing;

import android.content.Context;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupChangeException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.jobs.MultiDeviceMessageRequestResponseJob;
import org.thoughtcrime.securesms.profiles.spoofing.ReviewCardRepository;
import org.thoughtcrime.securesms.profiles.spoofing.ReviewRecipient;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class ReviewCardRepository {
    private final Context context;
    private final GroupId.V2 groupId;
    private final RecipientId recipientId;

    /* loaded from: classes4.dex */
    public interface OnRecipientsLoadedListener {
        void onRecipientsLoadFailed();

        void onRecipientsLoaded(List<ReviewRecipient> list);
    }

    /* loaded from: classes4.dex */
    public interface OnRemoveFromGroupListener {
        void onActionCompleted();

        void onActionFailed();
    }

    public ReviewCardRepository(Context context, GroupId.V2 v2) {
        this.context = context;
        this.groupId = v2;
        this.recipientId = null;
    }

    public ReviewCardRepository(Context context, RecipientId recipientId) {
        this.context = context;
        this.groupId = null;
        this.recipientId = recipientId;
    }

    public void loadRecipients(OnRecipientsLoadedListener onRecipientsLoadedListener) {
        GroupId.V2 v2 = this.groupId;
        if (v2 != null) {
            loadRecipientsForGroup(v2, onRecipientsLoadedListener);
            return;
        }
        RecipientId recipientId = this.recipientId;
        if (recipientId != null) {
            loadSimilarRecipients(this.context, recipientId, onRecipientsLoadedListener);
            return;
        }
        throw new AssertionError();
    }

    public int loadGroupsInCommonCount(ReviewRecipient reviewRecipient) {
        return ReviewUtil.getGroupsInCommonCount(this.context, reviewRecipient.getRecipient().getId());
    }

    public void block(ReviewCard reviewCard, Runnable runnable) {
        if (this.recipientId != null) {
            SignalExecutors.BOUNDED.execute(new Runnable(reviewCard, runnable) { // from class: org.thoughtcrime.securesms.profiles.spoofing.ReviewCardRepository$$ExternalSyntheticLambda4
                public final /* synthetic */ ReviewCard f$1;
                public final /* synthetic */ Runnable f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ReviewCardRepository.this.lambda$block$0(this.f$1, this.f$2);
                }
            });
            return;
        }
        throw new UnsupportedOperationException();
    }

    public /* synthetic */ void lambda$block$0(ReviewCard reviewCard, Runnable runnable) {
        RecipientUtil.blockNonGroup(this.context, reviewCard.getReviewRecipient());
        runnable.run();
    }

    public void delete(ReviewCard reviewCard, Runnable runnable) {
        if (this.recipientId != null) {
            SignalExecutors.BOUNDED.execute(new Runnable(runnable) { // from class: org.thoughtcrime.securesms.profiles.spoofing.ReviewCardRepository$$ExternalSyntheticLambda5
                public final /* synthetic */ Runnable f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ReviewCardRepository.this.lambda$delete$1(this.f$1);
                }
            });
            return;
        }
        throw new UnsupportedOperationException();
    }

    public /* synthetic */ void lambda$delete$1(Runnable runnable) {
        if (!Recipient.resolved(this.recipientId).isGroup()) {
            if (TextSecurePreferences.isMultiDevice(this.context)) {
                ApplicationDependencies.getJobManager().add(MultiDeviceMessageRequestResponseJob.forDelete(this.recipientId));
            }
            ThreadDatabase threads = SignalDatabase.threads();
            Long threadIdFor = threads.getThreadIdFor(this.recipientId);
            Objects.requireNonNull(threadIdFor);
            threads.deleteConversation(threadIdFor.longValue());
            runnable.run();
            return;
        }
        throw new AssertionError();
    }

    public void removeFromGroup(ReviewCard reviewCard, OnRemoveFromGroupListener onRemoveFromGroupListener) {
        if (this.groupId != null) {
            SignalExecutors.BOUNDED.execute(new Runnable(reviewCard, onRemoveFromGroupListener) { // from class: org.thoughtcrime.securesms.profiles.spoofing.ReviewCardRepository$$ExternalSyntheticLambda3
                public final /* synthetic */ ReviewCard f$1;
                public final /* synthetic */ ReviewCardRepository.OnRemoveFromGroupListener f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ReviewCardRepository.this.lambda$removeFromGroup$2(this.f$1, this.f$2);
                }
            });
            return;
        }
        throw new UnsupportedOperationException();
    }

    public /* synthetic */ void lambda$removeFromGroup$2(ReviewCard reviewCard, OnRemoveFromGroupListener onRemoveFromGroupListener) {
        try {
            GroupManager.ejectAndBanFromGroup(this.context, this.groupId, reviewCard.getReviewRecipient());
            onRemoveFromGroupListener.onActionCompleted();
        } catch (IOException | GroupChangeException unused) {
            onRemoveFromGroupListener.onActionFailed();
        }
    }

    public static /* synthetic */ void lambda$loadRecipientsForGroup$3(OnRecipientsLoadedListener onRecipientsLoadedListener, GroupId.V2 v2) {
        onRecipientsLoadedListener.onRecipientsLoaded(ReviewUtil.getDuplicatedRecipients(v2));
    }

    private static void loadRecipientsForGroup(GroupId.V2 v2, OnRecipientsLoadedListener onRecipientsLoadedListener) {
        SignalExecutors.BOUNDED.execute(new Runnable(v2) { // from class: org.thoughtcrime.securesms.profiles.spoofing.ReviewCardRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ GroupId.V2 f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ReviewCardRepository.lambda$loadRecipientsForGroup$3(ReviewCardRepository.OnRecipientsLoadedListener.this, this.f$1);
            }
        });
    }

    private static void loadSimilarRecipients(Context context, RecipientId recipientId, OnRecipientsLoadedListener onRecipientsLoadedListener) {
        SignalExecutors.BOUNDED.execute(new Runnable(onRecipientsLoadedListener, context) { // from class: org.thoughtcrime.securesms.profiles.spoofing.ReviewCardRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ ReviewCardRepository.OnRecipientsLoadedListener f$1;
            public final /* synthetic */ Context f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ReviewCardRepository.lambda$loadSimilarRecipients$4(RecipientId.this, this.f$1, this.f$2);
            }
        });
    }

    public static /* synthetic */ void lambda$loadSimilarRecipients$4(RecipientId recipientId, OnRecipientsLoadedListener onRecipientsLoadedListener, Context context) {
        List<RecipientId> similarRecipientIds = SignalDatabase.recipients().getSimilarRecipientIds(Recipient.resolved(recipientId));
        if (similarRecipientIds.isEmpty()) {
            onRecipientsLoadedListener.onRecipientsLoadFailed();
        } else {
            onRecipientsLoadedListener.onRecipientsLoaded(Stream.of(similarRecipientIds).map(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8()).map(new Function() { // from class: org.thoughtcrime.securesms.profiles.spoofing.ReviewCardRepository$$ExternalSyntheticLambda1
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return new ReviewRecipient((Recipient) obj);
                }
            }).sorted(new ReviewRecipient.Comparator(context, recipientId)).toList());
        }
    }
}
