package org.thoughtcrime.securesms.profiles.spoofing;

import android.content.DialogInterface;
import org.thoughtcrime.securesms.profiles.spoofing.ReviewCard;
import org.thoughtcrime.securesms.profiles.spoofing.ReviewCardDialogFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ReviewCardDialogFragment$AdapterCallbacks$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ ReviewCardDialogFragment.AdapterCallbacks f$0;
    public final /* synthetic */ ReviewCard f$1;
    public final /* synthetic */ ReviewCard.Action f$2;

    public /* synthetic */ ReviewCardDialogFragment$AdapterCallbacks$$ExternalSyntheticLambda0(ReviewCardDialogFragment.AdapterCallbacks adapterCallbacks, ReviewCard reviewCard, ReviewCard.Action action) {
        this.f$0 = adapterCallbacks;
        this.f$1 = reviewCard;
        this.f$2 = action;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onActionClicked$0(this.f$1, this.f$2, dialogInterface, i);
    }
}
