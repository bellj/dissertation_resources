package org.thoughtcrime.securesms.profiles.manage;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavGraph;
import androidx.navigation.fragment.NavHostFragment;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* loaded from: classes4.dex */
public class ManageProfileActivity extends PassphraseRequiredActivity implements ReactWithAnyEmojiBottomSheetDialogFragment.Callback {
    public static final int RESULT_BECOME_A_SUSTAINER;
    public static final String START_AT_AVATAR;
    public static final String START_AT_USERNAME;
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();

    /* loaded from: classes4.dex */
    interface EmojiController {
        void onEmojiSelected(String str);
    }

    @Override // org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment.Callback
    public void onReactWithAnyEmojiDialogDismissed() {
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, ManageProfileActivity.class);
    }

    public static Intent getIntentForUsernameEdit(Context context) {
        Intent intent = new Intent(context, ManageProfileActivity.class);
        intent.putExtra(START_AT_USERNAME, true);
        return intent;
    }

    public static Intent getIntentForAvatarEdit(Context context) {
        Intent intent = new Intent(context, ManageProfileActivity.class);
        intent.putExtra(START_AT_AVATAR, true);
        return intent;
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        Bundle bundle2;
        this.dynamicTheme.onCreate(this);
        setContentView(R.layout.manage_profile_activity);
        if (bundle == null) {
            Bundle extras = getIntent().getExtras();
            NavController navController = ((NavHostFragment) getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment)).getNavController();
            NavGraph graph = navController.getGraph();
            if (extras != null) {
                bundle2 = extras;
            } else {
                bundle2 = new Bundle();
            }
            navController.setGraph(graph, bundle2);
            if (extras != null && extras.getBoolean(START_AT_USERNAME, false)) {
                SafeNavigation.safeNavigate(navController, ManageProfileFragmentDirections.actionManageUsername());
            }
            if (extras != null && extras.getBoolean(START_AT_AVATAR, false)) {
                SafeNavigation.safeNavigate(navController, ManageProfileFragmentDirections.actionManageProfileFragmentToAvatarPicker(null, null));
            }
        }
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }

    @Override // org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment.Callback
    public void onReactWithAnyEmojiSelected(String str) {
        Fragment primaryNavigationFragment = ((NavHostFragment) getSupportFragmentManager().getPrimaryNavigationFragment()).getChildFragmentManager().getPrimaryNavigationFragment();
        if (primaryNavigationFragment instanceof EmojiController) {
            ((EmojiController) primaryNavigationFragment).onEmojiSelected(str);
        }
    }
}
