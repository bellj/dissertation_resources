package org.thoughtcrime.securesms.profiles.manage;

import android.view.View;
import org.thoughtcrime.securesms.profiles.manage.EditAboutFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class EditAboutFragment$PresetAdapter$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ EditAboutFragment.PresetAdapter f$0;
    public final /* synthetic */ EditAboutFragment.AboutPreset f$1;

    public /* synthetic */ EditAboutFragment$PresetAdapter$$ExternalSyntheticLambda0(EditAboutFragment.PresetAdapter presetAdapter, EditAboutFragment.AboutPreset aboutPreset) {
        this.f$0 = presetAdapter;
        this.f$1 = aboutPreset;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$onBindViewHolder$0(this.f$1, view);
    }
}
