package org.thoughtcrime.securesms.profiles.spoofing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.FullScreenDialogFragment;
import org.thoughtcrime.securesms.groups.BadGroupIdException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.profiles.spoofing.ReviewCard;
import org.thoughtcrime.securesms.profiles.spoofing.ReviewCardAdapter;
import org.thoughtcrime.securesms.profiles.spoofing.ReviewCardViewModel;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment;

/* loaded from: classes4.dex */
public class ReviewCardDialogFragment extends FullScreenDialogFragment {
    private static final String EXTRA_DESCRIPTION_RES_ID;
    private static final String EXTRA_GROUPS_IN_COMMON_RES_ID;
    private static final String EXTRA_GROUP_ID;
    private static final String EXTRA_NO_GROUPS_IN_COMMON_RES_ID;
    private static final String EXTRA_RECIPIENT_ID;
    private static final String EXTRA_TITLE_RES_ID;
    private ReviewCardViewModel viewModel;

    @Override // org.thoughtcrime.securesms.components.FullScreenDialogFragment
    protected int getDialogLayoutResource() {
        return R.layout.fragment_review;
    }

    public static ReviewCardDialogFragment createForReviewRequest(RecipientId recipientId) {
        return create(R.string.ReviewCardDialogFragment__review_request, R.string.ReviewCardDialogFragment__if_youre_not_sure, R.string.ReviewCardDialogFragment__no_groups_in_common, R.plurals.ReviewCardDialogFragment__d_groups_in_common, recipientId, null);
    }

    public static ReviewCardDialogFragment createForReviewMembers(GroupId.V2 v2) {
        return create(R.string.ReviewCardDialogFragment__review_members, R.string.ReviewCardDialogFragment__d_group_members_have_the_same_name, R.string.ReviewCardDialogFragment__no_other_groups_in_common, R.plurals.ReviewCardDialogFragment__d_other_groups_in_common, null, v2);
    }

    private static ReviewCardDialogFragment create(int i, int i2, int i3, int i4, RecipientId recipientId, GroupId.V2 v2) {
        ReviewCardDialogFragment reviewCardDialogFragment = new ReviewCardDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_TITLE_RES_ID, i);
        bundle.putInt(EXTRA_DESCRIPTION_RES_ID, i2);
        bundle.putInt(EXTRA_GROUPS_IN_COMMON_RES_ID, i4);
        bundle.putInt(EXTRA_NO_GROUPS_IN_COMMON_RES_ID, i3);
        bundle.putParcelable(EXTRA_RECIPIENT_ID, recipientId);
        bundle.putString(EXTRA_GROUP_ID, v2 != null ? v2.toString() : null);
        reviewCardDialogFragment.setArguments(bundle);
        return reviewCardDialogFragment;
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        try {
            initializeViewModel();
            ReviewCardAdapter reviewCardAdapter = new ReviewCardAdapter(getNoGroupsInCommonResId(), getGroupsInCommonResId(), new AdapterCallbacks(this, null));
            ((RecyclerView) view.findViewById(R.id.recycler)).setAdapter(reviewCardAdapter);
            this.viewModel.getReviewCards().observe(getViewLifecycleOwner(), new Observer(reviewCardAdapter, (TextView) view.findViewById(R.id.description)) { // from class: org.thoughtcrime.securesms.profiles.spoofing.ReviewCardDialogFragment$$ExternalSyntheticLambda0
                public final /* synthetic */ ReviewCardAdapter f$1;
                public final /* synthetic */ TextView f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    ReviewCardDialogFragment.this.lambda$onViewCreated$0(this.f$1, this.f$2, (List) obj);
                }
            });
            this.viewModel.getReviewEvents().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.profiles.spoofing.ReviewCardDialogFragment$$ExternalSyntheticLambda1
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    ReviewCardDialogFragment.this.onReviewEvent((ReviewCardViewModel.Event) obj);
                }
            });
        } catch (BadGroupIdException e) {
            throw new IllegalStateException(e);
        }
    }

    public /* synthetic */ void lambda$onViewCreated$0(ReviewCardAdapter reviewCardAdapter, TextView textView, List list) {
        reviewCardAdapter.submitList(list);
        textView.setText(getString(getDescriptionResId(), Integer.valueOf(list.size())));
    }

    private void initializeViewModel() throws BadGroupIdException {
        this.viewModel = (ReviewCardViewModel) ViewModelProviders.of(this, new ReviewCardViewModel.Factory(getRepository(), getGroupId())).get(ReviewCardViewModel.class);
    }

    private int getDescriptionResId() {
        return requireArguments().getInt(EXTRA_DESCRIPTION_RES_ID);
    }

    private int getGroupsInCommonResId() {
        return requireArguments().getInt(EXTRA_GROUPS_IN_COMMON_RES_ID);
    }

    private int getNoGroupsInCommonResId() {
        return requireArguments().getInt(EXTRA_NO_GROUPS_IN_COMMON_RES_ID);
    }

    private RecipientId getRecipientId() {
        return (RecipientId) requireArguments().getParcelable(EXTRA_RECIPIENT_ID);
    }

    private GroupId.V2 getGroupId() throws BadGroupIdException {
        GroupId parseNullable = GroupId.parseNullable(requireArguments().getString(EXTRA_GROUP_ID));
        if (parseNullable != null) {
            return parseNullable.requireV2();
        }
        return null;
    }

    private ReviewCardRepository getRepository() throws BadGroupIdException {
        RecipientId recipientId = getRecipientId();
        GroupId.V2 groupId = getGroupId();
        if (recipientId != null) {
            return new ReviewCardRepository(requireContext(), recipientId);
        }
        if (groupId != null) {
            return new ReviewCardRepository(requireContext(), groupId);
        }
        throw new AssertionError();
    }

    public void onReviewEvent(ReviewCardViewModel.Event event) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCardViewModel$Event[event.ordinal()];
        if (i == 1) {
            dismiss();
        } else if (i == 2) {
            toast(R.string.ReviewCardDialogFragment__failed_to_remove_group_member);
        } else {
            throw new IllegalArgumentException("Unhandled event: " + event);
        }
    }

    private void toast(int i) {
        Toast.makeText(requireContext(), i, 0).show();
    }

    @Override // org.thoughtcrime.securesms.components.FullScreenDialogFragment
    protected int getTitle() {
        return requireArguments().getInt(EXTRA_TITLE_RES_ID);
    }

    /* loaded from: classes4.dex */
    public final class AdapterCallbacks implements ReviewCardAdapter.Callbacks {
        private AdapterCallbacks() {
            ReviewCardDialogFragment.this = r1;
        }

        /* synthetic */ AdapterCallbacks(ReviewCardDialogFragment reviewCardDialogFragment, AnonymousClass1 r2) {
            this();
        }

        @Override // org.thoughtcrime.securesms.profiles.spoofing.ReviewCardAdapter.Callbacks
        public void onCardClicked(ReviewCard reviewCard) {
            RecipientBottomSheetDialogFragment.create(reviewCard.getReviewRecipient().getId(), null).show(ReviewCardDialogFragment.this.requireFragmentManager(), (String) null);
        }

        @Override // org.thoughtcrime.securesms.profiles.spoofing.ReviewCardAdapter.Callbacks
        public void onActionClicked(ReviewCard reviewCard, ReviewCard.Action action) {
            int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCard$Action[action.ordinal()];
            if (i == 1) {
                Intent intent = new Intent("android.intent.action.EDIT");
                intent.setDataAndType(reviewCard.getReviewRecipient().getContactUri(), "vnd.android.cursor.item/contact");
                ReviewCardDialogFragment.this.startActivity(intent);
            } else if (i != 2) {
                ReviewCardDialogFragment.this.viewModel.act(reviewCard, action);
            } else {
                new AlertDialog.Builder(ReviewCardDialogFragment.this.requireContext()).setMessage(ReviewCardDialogFragment.this.getString(R.string.ReviewCardDialogFragment__remove_s_from_group, reviewCard.getReviewRecipient().getDisplayName(ReviewCardDialogFragment.this.requireContext()))).setPositiveButton(R.string.ReviewCardDialogFragment__remove, new ReviewCardDialogFragment$AdapterCallbacks$$ExternalSyntheticLambda0(this, reviewCard, action)).setNegativeButton(17039360, new ReviewCardDialogFragment$AdapterCallbacks$$ExternalSyntheticLambda1()).setCancelable(true).show();
            }
        }

        public /* synthetic */ void lambda$onActionClicked$0(ReviewCard reviewCard, ReviewCard.Action action, DialogInterface dialogInterface, int i) {
            ReviewCardDialogFragment.this.viewModel.act(reviewCard, action);
            dialogInterface.dismiss();
        }
    }

    /* renamed from: org.thoughtcrime.securesms.profiles.spoofing.ReviewCardDialogFragment$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCard$Action;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCardViewModel$Event;

        static {
            int[] iArr = new int[ReviewCard.Action.values().length];
            $SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCard$Action = iArr;
            try {
                iArr[ReviewCard.Action.UPDATE_CONTACT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCard$Action[ReviewCard.Action.REMOVE_FROM_GROUP.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            int[] iArr2 = new int[ReviewCardViewModel.Event.values().length];
            $SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCardViewModel$Event = iArr2;
            try {
                iArr2[ReviewCardViewModel.Event.DISMISS.ordinal()] = 1;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCardViewModel$Event[ReviewCardViewModel.Event.REMOVE_FAILED.ordinal()] = 2;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }
}
