package org.thoughtcrime.securesms.profiles.spoofing;

import android.app.Application;
import android.content.Context;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.databaseprotos.ProfileChangeDetails;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.Base64;

/* loaded from: classes4.dex */
public final class ReviewUtil {
    private static final long TIMEOUT = TimeUnit.HOURS.toMillis(24);

    private ReviewUtil() {
    }

    public static boolean isRecipientReviewSuggested(RecipientId recipientId) {
        Recipient resolved = Recipient.resolved(recipientId);
        if (resolved.isGroup() || resolved.isSystemContact() || SignalDatabase.recipients().getSimilarRecipientIds(resolved).size() <= 1) {
            return false;
        }
        return true;
    }

    public static List<ReviewRecipient> getDuplicatedRecipients(GroupId.V2 v2) {
        Application application = ApplicationDependencies.getApplication();
        List<MessageRecord> profileChangeRecordsForGroup = getProfileChangeRecordsForGroup(application, v2);
        if (profileChangeRecordsForGroup.isEmpty()) {
            return Collections.emptyList();
        }
        List<Recipient> groupMembers = SignalDatabase.groups().getGroupMembers(v2, GroupDatabase.MemberSet.FULL_MEMBERS_INCLUDING_SELF);
        List<ReviewRecipient> list = Stream.of(profileChangeRecordsForGroup).distinctBy(new Function() { // from class: org.thoughtcrime.securesms.profiles.spoofing.ReviewUtil$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ReviewUtil.lambda$getDuplicatedRecipients$0((MessageRecord) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.profiles.spoofing.ReviewUtil$$ExternalSyntheticLambda3
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ReviewUtil.lambda$getDuplicatedRecipients$1((MessageRecord) obj);
            }
        }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.profiles.spoofing.ReviewUtil$$ExternalSyntheticLambda4
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ReviewUtil.lambda$getDuplicatedRecipients$2((ReviewRecipient) obj);
            }
        }).toList();
        LinkedList linkedList = new LinkedList();
        for (ReviewRecipient reviewRecipient : list) {
            if (!linkedList.contains(reviewRecipient)) {
                groupMembers.remove(reviewRecipient.getRecipient());
                for (Recipient recipient : groupMembers) {
                    if (Objects.equals(recipient.getDisplayName(application), reviewRecipient.getRecipient().getDisplayName(application))) {
                        linkedList.add(reviewRecipient);
                        linkedList.add(new ReviewRecipient(recipient));
                    }
                }
            }
        }
        return linkedList;
    }

    public static /* synthetic */ RecipientId lambda$getDuplicatedRecipients$0(MessageRecord messageRecord) {
        return messageRecord.getRecipient().getId();
    }

    public static /* synthetic */ ReviewRecipient lambda$getDuplicatedRecipients$1(MessageRecord messageRecord) {
        return new ReviewRecipient(messageRecord.getRecipient().resolve(), getProfileChangeDetails(messageRecord));
    }

    public static /* synthetic */ boolean lambda$getDuplicatedRecipients$2(ReviewRecipient reviewRecipient) {
        return !reviewRecipient.getRecipient().isSystemContact();
    }

    public static List<MessageRecord> getProfileChangeRecordsForGroup(Context context, GroupId.V2 v2) {
        Long threadIdFor = SignalDatabase.threads().getThreadIdFor(SignalDatabase.recipients().getByGroupId(v2).get());
        if (threadIdFor == null) {
            return Collections.emptyList();
        }
        return SignalDatabase.sms().getProfileChangeDetailsRecords(threadIdFor.longValue(), System.currentTimeMillis() - TIMEOUT);
    }

    public static int getGroupsInCommonCount(Context context, RecipientId recipientId) {
        return Stream.of(SignalDatabase.groups().getPushGroupsContainingMember(recipientId)).filter(new Predicate() { // from class: org.thoughtcrime.securesms.profiles.spoofing.ReviewUtil$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ReviewUtil.lambda$getGroupsInCommonCount$3((GroupDatabase.GroupRecord) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.profiles.spoofing.ReviewUtil$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ((GroupDatabase.GroupRecord) obj).getRecipientId();
            }
        }).toList().size();
    }

    public static /* synthetic */ boolean lambda$getGroupsInCommonCount$3(GroupDatabase.GroupRecord groupRecord) {
        return groupRecord.getMembers().contains(Recipient.self().getId());
    }

    private static ProfileChangeDetails getProfileChangeDetails(MessageRecord messageRecord) {
        try {
            return ProfileChangeDetails.parseFrom(Base64.decode(messageRecord.getBody()));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
