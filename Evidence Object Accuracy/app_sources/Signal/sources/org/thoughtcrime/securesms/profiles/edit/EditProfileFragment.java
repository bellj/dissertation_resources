package org.thoughtcrime.securesms.profiles.edit;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.core.util.Consumer;
import androidx.fragment.app.FragmentResultListener;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import com.airbnb.lottie.SimpleColorFilter;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import java.io.IOException;
import org.signal.core.util.EditTextUtil;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.avatar.Avatars;
import org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.ParcelableGroupId;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.profiles.ProfileName;
import org.thoughtcrime.securesms.profiles.edit.EditProfileRepository;
import org.thoughtcrime.securesms.profiles.edit.EditProfileViewModel;
import org.thoughtcrime.securesms.profiles.manage.EditProfileNameFragment;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.thoughtcrime.securesms.util.text.AfterTextChanged;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;
import org.thoughtcrime.securesms.util.views.LearnMoreTextView;

/* loaded from: classes4.dex */
public class EditProfileFragment extends LoggingFragment {
    private static final int MAX_DESCRIPTION_BYTES;
    private static final int MAX_DESCRIPTION_GLYPHS;
    private static final String TAG = Log.tag(EditProfileFragment.class);
    private ImageView avatar;
    private ImageView avatarPreview;
    private ImageView avatarPreviewBackground;
    private Controller controller;
    private EditText familyName;
    private CircularProgressMaterialButton finishButton;
    private EditText givenName;
    private Intent nextIntent;
    private TextView preview;
    private View reveal;
    private View title;
    private Toolbar toolbar;
    private EditProfileViewModel viewModel;

    /* loaded from: classes4.dex */
    public interface Controller {
        void onProfileNameUploadCompleted();
    }

    @Override // androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Controller) {
            this.controller = (Controller) context;
            return;
        }
        throw new IllegalStateException("Context must subclass Controller");
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.profile_create_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        GroupId parseNullableOrThrow = GroupId.parseNullableOrThrow(requireArguments().getString("group_id", null));
        boolean z = false;
        boolean z2 = requireArguments().getBoolean(EditProfileActivity.EXCLUDE_SYSTEM, false);
        if (bundle != null) {
            z = true;
        }
        initializeViewModel(z2, parseNullableOrThrow, z);
        initializeResources(view, parseNullableOrThrow);
        initializeProfileAvatar();
        initializeProfileName();
        getParentFragmentManager().setFragmentResultListener(AvatarPickerFragment.REQUEST_KEY_SELECT_AVATAR, getViewLifecycleOwner(), new FragmentResultListener() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileFragment$$ExternalSyntheticLambda10
            @Override // androidx.fragment.app.FragmentResultListener
            public final void onFragmentResult(String str, Bundle bundle2) {
                EditProfileFragment.this.lambda$onViewCreated$0(str, bundle2);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$0(String str, Bundle bundle) {
        if (bundle.getBoolean(AvatarPickerFragment.SELECT_AVATAR_CLEAR)) {
            this.viewModel.setAvatarMedia(null);
            this.viewModel.setAvatar(null);
            this.avatar.setImageDrawable(null);
            return;
        }
        handleMediaFromResult((Media) bundle.getParcelable(AvatarPickerFragment.SELECT_AVATAR_MEDIA));
    }

    private void handleMediaFromResult(Media media) {
        SimpleTask.run(new SimpleTask.BackgroundTask(media) { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileFragment$$ExternalSyntheticLambda11
            public final /* synthetic */ Media f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return EditProfileFragment.this.lambda$handleMediaFromResult$1(this.f$1);
            }
        }, new SimpleTask.ForegroundTask(media) { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileFragment$$ExternalSyntheticLambda12
            public final /* synthetic */ Media f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                EditProfileFragment.this.lambda$handleMediaFromResult$2(this.f$1, (byte[]) obj);
            }
        });
    }

    public /* synthetic */ byte[] lambda$handleMediaFromResult$1(Media media) {
        try {
            return StreamUtil.readFully(BlobProvider.getInstance().getStream(requireContext(), media.getUri()));
        } catch (IOException e) {
            Log.w(TAG, e);
            return null;
        }
    }

    public /* synthetic */ void lambda$handleMediaFromResult$2(Media media, byte[] bArr) {
        if (bArr != null) {
            this.viewModel.setAvatarMedia(media);
            this.viewModel.setAvatar(bArr);
            GlideApp.with(this).load(bArr).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).circleCrop().into(this.avatar);
            return;
        }
        Toast.makeText(requireActivity(), (int) R.string.CreateProfileActivity_error_setting_profile_photo, 1).show();
    }

    private void initializeViewModel(boolean z, GroupId groupId, boolean z2) {
        EditProfileRepository editProfileRepository;
        if (groupId != null) {
            editProfileRepository = new EditGroupProfileRepository(requireContext(), groupId);
        } else {
            editProfileRepository = new EditSelfProfileRepository(requireContext(), z);
        }
        this.viewModel = (EditProfileViewModel) ViewModelProviders.of(requireActivity(), new EditProfileViewModel.Factory(editProfileRepository, z2, groupId)).get(EditProfileViewModel.class);
    }

    private void initializeResources(View view, GroupId groupId) {
        Bundle requireArguments = requireArguments();
        boolean z = groupId != null;
        this.toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        this.title = view.findViewById(R.id.title);
        this.avatar = (ImageView) view.findViewById(R.id.avatar);
        this.givenName = (EditText) view.findViewById(R.id.given_name);
        this.familyName = (EditText) view.findViewById(R.id.family_name);
        this.finishButton = (CircularProgressMaterialButton) view.findViewById(R.id.finish_button);
        this.reveal = view.findViewById(R.id.reveal);
        this.preview = (TextView) view.findViewById(R.id.name_preview);
        this.avatarPreviewBackground = (ImageView) view.findViewById(R.id.avatar_background);
        this.avatarPreview = (ImageView) view.findViewById(R.id.avatar_placeholder);
        this.nextIntent = (Intent) requireArguments.getParcelable("next_intent");
        this.avatar.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditProfileFragment.this.lambda$initializeResources$3(view2);
            }
        });
        view.findViewById(R.id.mms_group_hint).setVisibility((!z || !groupId.isMms()) ? 8 : 0);
        if (z) {
            EditTextUtil.addGraphemeClusterLimitFilter(this.givenName, FeatureFlags.getMaxGroupNameGraphemeLength());
            this.givenName.addTextChangedListener(new AfterTextChanged(new Consumer() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileFragment$$ExternalSyntheticLambda1
                @Override // androidx.core.util.Consumer
                public final void accept(Object obj) {
                    EditProfileFragment.this.lambda$initializeResources$4((Editable) obj);
                }
            }));
            this.givenName.setHint(R.string.EditProfileFragment__group_name);
            this.givenName.requestFocus();
            this.toolbar.setTitle(R.string.EditProfileFragment__edit_group);
            this.preview.setVisibility(8);
            if (groupId.isV2()) {
                EditTextUtil.addGraphemeClusterLimitFilter(this.familyName, MAX_DESCRIPTION_GLYPHS);
                this.familyName.addTextChangedListener(new AfterTextChanged(new Consumer() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileFragment$$ExternalSyntheticLambda2
                    @Override // androidx.core.util.Consumer
                    public final void accept(Object obj) {
                        EditProfileFragment.this.lambda$initializeResources$5((Editable) obj);
                    }
                }));
                this.familyName.setHint(R.string.EditProfileFragment__group_description);
                this.familyName.setSingleLine(false);
                this.familyName.setInputType(147457);
                LearnMoreTextView learnMoreTextView = (LearnMoreTextView) view.findViewById(R.id.description_text);
                learnMoreTextView.setLearnMoreVisible(false);
                learnMoreTextView.setText(R.string.CreateProfileActivity_group_descriptions_will_be_visible_to_members_of_this_group_and_people_who_have_been_invited);
            } else {
                this.familyName.setVisibility(8);
                this.familyName.setEnabled(false);
                view.findViewById(R.id.description_text).setVisibility(8);
            }
            ((ImageView) view.findViewById(R.id.avatar_placeholder)).setImageResource(R.drawable.ic_group_outline_40);
        } else {
            EditTextUtil.addGraphemeClusterLimitFilter(this.givenName, 26);
            EditTextUtil.addGraphemeClusterLimitFilter(this.familyName, 26);
            this.givenName.addTextChangedListener(new AfterTextChanged(new Consumer() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileFragment$$ExternalSyntheticLambda3
                @Override // androidx.core.util.Consumer
                public final void accept(Object obj) {
                    EditProfileFragment.this.lambda$initializeResources$6((Editable) obj);
                }
            }));
            this.familyName.addTextChangedListener(new AfterTextChanged(new Consumer() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileFragment$$ExternalSyntheticLambda4
                @Override // androidx.core.util.Consumer
                public final void accept(Object obj) {
                    EditProfileFragment.this.lambda$initializeResources$7((Editable) obj);
                }
            }));
            LearnMoreTextView learnMoreTextView2 = (LearnMoreTextView) view.findViewById(R.id.description_text);
            learnMoreTextView2.setLearnMoreVisible(true);
            learnMoreTextView2.setOnLinkClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileFragment$$ExternalSyntheticLambda5
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    EditProfileFragment.this.lambda$initializeResources$8(view2);
                }
            });
        }
        this.finishButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileFragment$$ExternalSyntheticLambda6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditProfileFragment.this.lambda$initializeResources$9(view2);
            }
        });
        this.finishButton.setText(requireArguments.getInt(EditProfileActivity.NEXT_BUTTON_TEXT, R.string.CreateProfileActivity_next));
        if (requireArguments.getBoolean(EditProfileActivity.SHOW_TOOLBAR, true)) {
            this.toolbar.setVisibility(0);
            this.toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileFragment$$ExternalSyntheticLambda7
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    EditProfileFragment.this.lambda$initializeResources$10(view2);
                }
            });
            this.title.setVisibility(8);
        }
    }

    public /* synthetic */ void lambda$initializeResources$3(View view) {
        startAvatarSelection();
    }

    public /* synthetic */ void lambda$initializeResources$4(Editable editable) {
        this.viewModel.setGivenName(editable.toString());
    }

    public /* synthetic */ void lambda$initializeResources$5(Editable editable) {
        EditProfileNameFragment.trimFieldToMaxByteLength(editable, MAX_DESCRIPTION_BYTES);
        this.viewModel.setFamilyName(editable.toString());
    }

    public /* synthetic */ void lambda$initializeResources$6(Editable editable) {
        EditProfileNameFragment.trimFieldToMaxByteLength(editable);
        this.viewModel.setGivenName(editable.toString());
    }

    public /* synthetic */ void lambda$initializeResources$7(Editable editable) {
        EditProfileNameFragment.trimFieldToMaxByteLength(editable);
        this.viewModel.setFamilyName(editable.toString());
    }

    public /* synthetic */ void lambda$initializeResources$8(View view) {
        CommunicationActions.openBrowserLink(requireContext(), getString(R.string.EditProfileFragment__support_link));
    }

    public /* synthetic */ void lambda$initializeResources$9(View view) {
        this.finishButton.setSpinning();
        handleUpload();
    }

    public /* synthetic */ void lambda$initializeResources$10(View view) {
        requireActivity().finish();
    }

    private void initializeProfileName() {
        this.viewModel.isFormValid().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileFragment$$ExternalSyntheticLambda13
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                EditProfileFragment.this.lambda$initializeProfileName$11((Boolean) obj);
            }
        });
        this.viewModel.givenName().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileFragment$$ExternalSyntheticLambda14
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                EditProfileFragment.this.lambda$initializeProfileName$12((String) obj);
            }
        });
        this.viewModel.familyName().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileFragment$$ExternalSyntheticLambda15
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                EditProfileFragment.this.lambda$initializeProfileName$13((String) obj);
            }
        });
        this.viewModel.profileName().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileFragment$$ExternalSyntheticLambda16
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                EditProfileFragment.this.lambda$initializeProfileName$14((ProfileName) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializeProfileName$11(Boolean bool) {
        this.finishButton.setEnabled(bool.booleanValue());
        this.finishButton.setAlpha(bool.booleanValue() ? 1.0f : 0.5f);
    }

    public /* synthetic */ void lambda$initializeProfileName$12(String str) {
        updateFieldIfNeeded(this.givenName, str);
    }

    public /* synthetic */ void lambda$initializeProfileName$13(String str) {
        updateFieldIfNeeded(this.familyName, str);
    }

    public /* synthetic */ void lambda$initializeProfileName$14(ProfileName profileName) {
        this.preview.setText(profileName.toString());
    }

    private void initializeProfileAvatar() {
        this.viewModel.avatar().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileFragment$$ExternalSyntheticLambda8
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                EditProfileFragment.this.lambda$initializeProfileAvatar$15((byte[]) obj);
            }
        });
        this.viewModel.avatarColor().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileFragment$$ExternalSyntheticLambda9
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                EditProfileFragment.this.lambda$initializeProfileAvatar$16((AvatarColor) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializeProfileAvatar$15(byte[] bArr) {
        if (bArr == null) {
            GlideApp.with(this).clear(this.avatar);
        } else {
            GlideApp.with(this).load(bArr).circleCrop().into(this.avatar);
        }
    }

    public /* synthetic */ void lambda$initializeProfileAvatar$16(AvatarColor avatarColor) {
        this.avatarPreview.getDrawable().setColorFilter(new SimpleColorFilter(Avatars.getForegroundColor(avatarColor).getColorInt()));
        this.avatarPreviewBackground.getDrawable().setColorFilter(new SimpleColorFilter(avatarColor.colorInt()));
    }

    private static void updateFieldIfNeeded(EditText editText, String str) {
        if (!editText.getText().toString().trim().equals(str.trim())) {
            boolean z = editText.getText().length() == 0;
            editText.setText(str);
            if (z) {
                editText.setSelection(editText.getText().length());
            }
        }
    }

    private void startAvatarSelection() {
        if (this.viewModel.isGroup()) {
            SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), EditProfileFragmentDirections.actionCreateProfileFragmentToAvatarPicker((ParcelableGroupId) ParcelableGroupId.from(this.viewModel.getGroupId()), this.viewModel.getAvatarMedia()));
            return;
        }
        SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), EditProfileFragmentDirections.actionCreateProfileFragmentToAvatarPicker(null, null));
    }

    private void handleUpload() {
        this.viewModel.getUploadResult().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileFragment$$ExternalSyntheticLambda17
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                EditProfileFragment.this.lambda$handleUpload$17((EditProfileRepository.UploadResult) obj);
            }
        });
        this.viewModel.submitProfile();
    }

    public /* synthetic */ void lambda$handleUpload$17(EditProfileRepository.UploadResult uploadResult) {
        if (uploadResult != EditProfileRepository.UploadResult.SUCCESS) {
            Toast.makeText(requireContext(), (int) R.string.CreateProfileActivity_problem_setting_profile, 1).show();
        } else if (Build.VERSION.SDK_INT >= 21) {
            handleFinishedLollipop();
        } else {
            handleFinishedLegacy();
        }
    }

    private void handleFinishedLegacy() {
        this.finishButton.cancelSpinning();
        Intent intent = this.nextIntent;
        if (intent != null) {
            startActivity(intent);
        }
        this.controller.onProfileNameUploadCompleted();
    }

    private void handleFinishedLollipop() {
        int[] iArr = new int[2];
        int[] iArr2 = new int[2];
        this.finishButton.getLocationInWindow(iArr);
        this.reveal.getLocationInWindow(iArr2);
        int i = iArr[1] - iArr2[1];
        View view = this.reveal;
        Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(view, (iArr[0] - iArr2[0]) + (this.finishButton.getWidth() / 2), i + (this.finishButton.getHeight() / 2), 0.0f, (float) Math.max(view.getWidth(), this.reveal.getHeight()));
        createCircularReveal.setDuration(500);
        createCircularReveal.addListener(new Animator.AnimatorListener() { // from class: org.thoughtcrime.securesms.profiles.edit.EditProfileFragment.1
            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationCancel(Animator animator) {
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationRepeat(Animator animator) {
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationStart(Animator animator) {
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                EditProfileFragment.this.finishButton.cancelSpinning();
                if (!(EditProfileFragment.this.nextIntent == null || EditProfileFragment.this.getActivity() == null)) {
                    EditProfileFragment editProfileFragment = EditProfileFragment.this;
                    editProfileFragment.startActivity(editProfileFragment.nextIntent);
                }
                EditProfileFragment.this.controller.onProfileNameUploadCompleted();
            }
        });
        this.reveal.setVisibility(0);
        createCircularReveal.start();
    }
}
