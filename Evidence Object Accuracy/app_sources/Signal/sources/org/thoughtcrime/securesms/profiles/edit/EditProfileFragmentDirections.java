package org.thoughtcrime.securesms.profiles.edit;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.groups.ParcelableGroupId;
import org.thoughtcrime.securesms.mediasend.Media;

/* loaded from: classes4.dex */
public class EditProfileFragmentDirections {
    private EditProfileFragmentDirections() {
    }

    public static NavDirections actionEditUsername() {
        return new ActionOnlyNavDirections(R.id.action_editUsername);
    }

    public static ActionCreateProfileFragmentToAvatarPicker actionCreateProfileFragmentToAvatarPicker(ParcelableGroupId parcelableGroupId, Media media) {
        return new ActionCreateProfileFragmentToAvatarPicker(parcelableGroupId, media);
    }

    /* loaded from: classes4.dex */
    public static class ActionCreateProfileFragmentToAvatarPicker implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_createProfileFragment_to_avatar_picker;
        }

        private ActionCreateProfileFragmentToAvatarPicker(ParcelableGroupId parcelableGroupId, Media media) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("group_id", parcelableGroupId);
            hashMap.put("group_avatar_media", media);
        }

        public ActionCreateProfileFragmentToAvatarPicker setGroupId(ParcelableGroupId parcelableGroupId) {
            this.arguments.put("group_id", parcelableGroupId);
            return this;
        }

        public ActionCreateProfileFragmentToAvatarPicker setGroupAvatarMedia(Media media) {
            this.arguments.put("group_avatar_media", media);
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("group_id")) {
                ParcelableGroupId parcelableGroupId = (ParcelableGroupId) this.arguments.get("group_id");
                if (Parcelable.class.isAssignableFrom(ParcelableGroupId.class) || parcelableGroupId == null) {
                    bundle.putParcelable("group_id", (Parcelable) Parcelable.class.cast(parcelableGroupId));
                } else if (Serializable.class.isAssignableFrom(ParcelableGroupId.class)) {
                    bundle.putSerializable("group_id", (Serializable) Serializable.class.cast(parcelableGroupId));
                } else {
                    throw new UnsupportedOperationException(ParcelableGroupId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            if (this.arguments.containsKey("group_avatar_media")) {
                Media media = (Media) this.arguments.get("group_avatar_media");
                if (Parcelable.class.isAssignableFrom(Media.class) || media == null) {
                    bundle.putParcelable("group_avatar_media", (Parcelable) Parcelable.class.cast(media));
                } else if (Serializable.class.isAssignableFrom(Media.class)) {
                    bundle.putSerializable("group_avatar_media", (Serializable) Serializable.class.cast(media));
                } else {
                    throw new UnsupportedOperationException(Media.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            return bundle;
        }

        public ParcelableGroupId getGroupId() {
            return (ParcelableGroupId) this.arguments.get("group_id");
        }

        public Media getGroupAvatarMedia() {
            return (Media) this.arguments.get("group_avatar_media");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionCreateProfileFragmentToAvatarPicker actionCreateProfileFragmentToAvatarPicker = (ActionCreateProfileFragmentToAvatarPicker) obj;
            if (this.arguments.containsKey("group_id") != actionCreateProfileFragmentToAvatarPicker.arguments.containsKey("group_id")) {
                return false;
            }
            if (getGroupId() == null ? actionCreateProfileFragmentToAvatarPicker.getGroupId() != null : !getGroupId().equals(actionCreateProfileFragmentToAvatarPicker.getGroupId())) {
                return false;
            }
            if (this.arguments.containsKey("group_avatar_media") != actionCreateProfileFragmentToAvatarPicker.arguments.containsKey("group_avatar_media")) {
                return false;
            }
            if (getGroupAvatarMedia() == null ? actionCreateProfileFragmentToAvatarPicker.getGroupAvatarMedia() == null : getGroupAvatarMedia().equals(actionCreateProfileFragmentToAvatarPicker.getGroupAvatarMedia())) {
                return getActionId() == actionCreateProfileFragmentToAvatarPicker.getActionId();
            }
            return false;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((getGroupId() != null ? getGroupId().hashCode() : 0) + 31) * 31;
            if (getGroupAvatarMedia() != null) {
                i = getGroupAvatarMedia().hashCode();
            }
            return ((hashCode + i) * 31) + getActionId();
        }

        public String toString() {
            return "ActionCreateProfileFragmentToAvatarPicker(actionId=" + getActionId() + "){groupId=" + getGroupId() + ", groupAvatarMedia=" + getGroupAvatarMedia() + "}";
        }
    }
}
