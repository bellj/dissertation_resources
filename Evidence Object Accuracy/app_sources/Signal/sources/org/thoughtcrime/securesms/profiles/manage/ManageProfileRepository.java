package org.thoughtcrime.securesms.profiles.manage;

import android.content.Context;
import androidx.core.util.Consumer;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.MultiDeviceProfileContentUpdateJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.profiles.ProfileName;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.ProfileUtil;
import org.whispersystems.signalservice.api.util.StreamDetails;

/* loaded from: classes4.dex */
public final class ManageProfileRepository {
    private static final String TAG = Log.tag(ManageProfileRepository.class);

    /* loaded from: classes4.dex */
    public enum Result {
        SUCCESS,
        FAILURE_NETWORK
    }

    public void setName(Context context, ProfileName profileName, Consumer<Result> consumer) {
        SignalExecutors.UNBOUNDED.execute(new Runnable(context, profileName, consumer) { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ ProfileName f$1;
            public final /* synthetic */ Consumer f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ManageProfileRepository.lambda$setName$0(this.f$0, this.f$1, this.f$2);
            }
        });
    }

    public static /* synthetic */ void lambda$setName$0(Context context, ProfileName profileName, Consumer consumer) {
        try {
            ProfileUtil.uploadProfileWithName(context, profileName);
            SignalDatabase.recipients().setProfileName(Recipient.self().getId(), profileName);
            ApplicationDependencies.getJobManager().add(new MultiDeviceProfileContentUpdateJob());
            consumer.accept(Result.SUCCESS);
        } catch (IOException e) {
            Log.w(TAG, "Failed to upload profile during name change.", e);
            consumer.accept(Result.FAILURE_NETWORK);
        }
    }

    public void setAbout(Context context, String str, String str2, Consumer<Result> consumer) {
        SignalExecutors.UNBOUNDED.execute(new Runnable(context, str, str2, consumer) { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ String f$1;
            public final /* synthetic */ String f$2;
            public final /* synthetic */ Consumer f$3;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ManageProfileRepository.lambda$setAbout$1(this.f$0, this.f$1, this.f$2, this.f$3);
            }
        });
    }

    public static /* synthetic */ void lambda$setAbout$1(Context context, String str, String str2, Consumer consumer) {
        try {
            ProfileUtil.uploadProfileWithAbout(context, str, str2);
            SignalDatabase.recipients().setAbout(Recipient.self().getId(), str, str2);
            ApplicationDependencies.getJobManager().add(new MultiDeviceProfileContentUpdateJob());
            consumer.accept(Result.SUCCESS);
        } catch (IOException e) {
            Log.w(TAG, "Failed to upload profile during about change.", e);
            consumer.accept(Result.FAILURE_NETWORK);
        }
    }

    public void setAvatar(Context context, byte[] bArr, String str, Consumer<Result> consumer) {
        SignalExecutors.UNBOUNDED.execute(new Runnable(bArr, str, context, consumer) { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ byte[] f$0;
            public final /* synthetic */ String f$1;
            public final /* synthetic */ Context f$2;
            public final /* synthetic */ Consumer f$3;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ManageProfileRepository.lambda$setAvatar$2(this.f$0, this.f$1, this.f$2, this.f$3);
            }
        });
    }

    public static /* synthetic */ void lambda$setAvatar$2(byte[] bArr, String str, Context context, Consumer consumer) {
        try {
            ProfileUtil.uploadProfileWithAvatar(new StreamDetails(new ByteArrayInputStream(bArr), str, (long) bArr.length));
            AvatarHelper.setAvatar(context, Recipient.self().getId(), new ByteArrayInputStream(bArr));
            SignalStore.misc().markHasEverHadAnAvatar();
            ApplicationDependencies.getJobManager().add(new MultiDeviceProfileContentUpdateJob());
            consumer.accept(Result.SUCCESS);
        } catch (IOException e) {
            Log.w(TAG, "Failed to upload profile during avatar change.", e);
            consumer.accept(Result.FAILURE_NETWORK);
        }
    }

    public void clearAvatar(Context context, Consumer<Result> consumer) {
        SignalExecutors.UNBOUNDED.execute(new Runnable(context, consumer) { // from class: org.thoughtcrime.securesms.profiles.manage.ManageProfileRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ Consumer f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ManageProfileRepository.lambda$clearAvatar$3(this.f$0, this.f$1);
            }
        });
    }

    public static /* synthetic */ void lambda$clearAvatar$3(Context context, Consumer consumer) {
        try {
            ProfileUtil.uploadProfileWithAvatar(null);
            AvatarHelper.delete(context, Recipient.self().getId());
            ApplicationDependencies.getJobManager().add(new MultiDeviceProfileContentUpdateJob());
            consumer.accept(Result.SUCCESS);
        } catch (IOException e) {
            Log.w(TAG, "Failed to upload profile during name change.", e);
            consumer.accept(Result.FAILURE_NETWORK);
        }
    }
}
