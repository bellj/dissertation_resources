package org.thoughtcrime.securesms.profiles.spoofing;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.FallbackPhoto20dp;
import org.thoughtcrime.securesms.contacts.avatars.GeneratedContactPhoto;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class ReviewBannerView extends LinearLayout {
    private View bannerClose;
    private ImageView bannerIcon;
    private TextView bannerMessage;
    private AvatarImageView bottomRightAvatar;
    private View stroke;
    private AvatarImageView topLeftAvatar;

    public ReviewBannerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ReviewBannerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @Override // android.view.View
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.bannerIcon = (ImageView) findViewById(R.id.banner_icon);
        this.bannerMessage = (TextView) findViewById(R.id.banner_message);
        this.bannerClose = findViewById(R.id.banner_close);
        this.topLeftAvatar = (AvatarImageView) findViewById(R.id.banner_avatar_1);
        this.bottomRightAvatar = (AvatarImageView) findViewById(R.id.banner_avatar_2);
        this.stroke = findViewById(R.id.banner_avatar_stroke);
        FallbackPhotoProvider fallbackPhotoProvider = new FallbackPhotoProvider();
        this.topLeftAvatar.setFallbackPhotoProvider(fallbackPhotoProvider);
        this.bottomRightAvatar.setFallbackPhotoProvider(fallbackPhotoProvider);
        this.bannerClose.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.profiles.spoofing.ReviewBannerView$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ReviewBannerView.this.lambda$onFinishInflate$0(view);
            }
        });
    }

    public /* synthetic */ void lambda$onFinishInflate$0(View view) {
        setVisibility(8);
    }

    public void setBannerMessage(CharSequence charSequence) {
        this.bannerMessage.setText(charSequence);
    }

    public void setBannerIcon(Drawable drawable) {
        this.bannerIcon.setImageDrawable(drawable);
        this.bannerIcon.setVisibility(0);
        this.topLeftAvatar.setVisibility(8);
        this.bottomRightAvatar.setVisibility(8);
        this.stroke.setVisibility(8);
    }

    public void setBannerRecipient(Recipient recipient) {
        this.topLeftAvatar.setAvatar(recipient);
        this.bottomRightAvatar.setAvatar(recipient);
        this.bannerIcon.setVisibility(8);
        this.topLeftAvatar.setVisibility(0);
        this.bottomRightAvatar.setVisibility(0);
        this.stroke.setVisibility(0);
    }

    /* loaded from: classes4.dex */
    private static final class FallbackPhotoProvider extends Recipient.FallbackPhotoProvider {
        private FallbackPhotoProvider() {
        }

        @Override // org.thoughtcrime.securesms.recipients.Recipient.FallbackPhotoProvider
        public FallbackContactPhoto getPhotoForGroup() {
            throw new UnsupportedOperationException("This provider does not support groups");
        }

        @Override // org.thoughtcrime.securesms.recipients.Recipient.FallbackPhotoProvider
        public FallbackContactPhoto getPhotoForResolvingRecipient() {
            throw new UnsupportedOperationException("This provider does not support resolving recipients");
        }

        @Override // org.thoughtcrime.securesms.recipients.Recipient.FallbackPhotoProvider
        public FallbackContactPhoto getPhotoForLocalNumber() {
            throw new UnsupportedOperationException("This provider does not support local number");
        }

        @Override // org.thoughtcrime.securesms.recipients.Recipient.FallbackPhotoProvider
        public FallbackContactPhoto getPhotoForRecipientWithName(String str, int i) {
            return new FixedSizeGeneratedContactPhoto(str, R.drawable.ic_profile_outline_20);
        }

        @Override // org.thoughtcrime.securesms.recipients.Recipient.FallbackPhotoProvider
        public FallbackContactPhoto getPhotoForRecipientWithoutName() {
            return new FallbackPhoto20dp(R.drawable.ic_profile_outline_20);
        }
    }

    /* loaded from: classes4.dex */
    private static final class FixedSizeGeneratedContactPhoto extends GeneratedContactPhoto {
        public FixedSizeGeneratedContactPhoto(String str, int i) {
            super(str, i);
        }

        @Override // org.thoughtcrime.securesms.contacts.avatars.GeneratedContactPhoto
        protected Drawable newFallbackDrawable(Context context, AvatarColor avatarColor, boolean z) {
            return new FallbackPhoto20dp(getFallbackResId()).asDrawable(context, avatarColor, z);
        }
    }
}
