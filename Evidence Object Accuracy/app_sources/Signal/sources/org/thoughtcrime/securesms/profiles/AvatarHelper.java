package org.thoughtcrime.securesms.profiles;

import android.content.Context;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collections;
import java.util.Iterator;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.AttachmentSecretProvider;
import org.thoughtcrime.securesms.crypto.ModernDecryptingPartInputStream;
import org.thoughtcrime.securesms.crypto.ModernEncryptingPartOutputStream;
import org.thoughtcrime.securesms.database.model.ProfileAvatarFileDetails;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.ByteUnit;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.whispersystems.signalservice.api.util.StreamDetails;

/* loaded from: classes4.dex */
public class AvatarHelper {
    public static int AVATAR_DIMENSIONS = 1024;
    private static final String AVATAR_DIRECTORY;
    public static long AVATAR_DOWNLOAD_FAILSAFE_MAX_SIZE = ByteUnit.MEGABYTES.toBytes(10);
    private static final String TAG = Log.tag(AvatarHelper.class);
    private static File avatarDirectory;

    private static File getAvatarDirectory(Context context) {
        if (avatarDirectory == null) {
            avatarDirectory = context.getDir(AVATAR_DIRECTORY, 0);
        }
        return avatarDirectory;
    }

    public static long getAvatarCount(Context context) {
        String[] list = getAvatarDirectory(context).list();
        if (list == null) {
            return 0;
        }
        return (long) list.length;
    }

    public static Iterable<Avatar> getAvatars(Context context) {
        File[] listFiles = getAvatarDirectory(context).listFiles();
        if (listFiles == null) {
            return Collections.emptyList();
        }
        return new Iterable(listFiles, context) { // from class: org.thoughtcrime.securesms.profiles.AvatarHelper$$ExternalSyntheticLambda0
            public final /* synthetic */ File[] f$0;
            public final /* synthetic */ Context f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // java.lang.Iterable
            public final Iterator iterator() {
                return AvatarHelper.lambda$getAvatars$0(this.f$0, this.f$1);
            }
        };
    }

    public static /* synthetic */ Iterator lambda$getAvatars$0(final File[] fileArr, final Context context) {
        return new Iterator<Avatar>() { // from class: org.thoughtcrime.securesms.profiles.AvatarHelper.1
            int i = 0;

            @Override // java.util.Iterator
            public boolean hasNext() {
                return this.i < fileArr.length;
            }

            @Override // java.util.Iterator
            public Avatar next() {
                File file = fileArr[this.i];
                try {
                    return new Avatar(AvatarHelper.getAvatar(context, RecipientId.from(file.getName())), file.getName(), ModernEncryptingPartOutputStream.getPlaintextLength(file.length()));
                } catch (IOException unused) {
                    return null;
                } finally {
                    this.i++;
                }
            }
        };
    }

    public static void delete(Context context, RecipientId recipientId) {
        getAvatarFile(context, recipientId).delete();
        Recipient.live(recipientId).refresh();
    }

    public static boolean hasAvatar(Context context, RecipientId recipientId) {
        File avatarFile = getAvatarFile(context, recipientId);
        return avatarFile.exists() && avatarFile.length() > 0;
    }

    public static ProfileAvatarFileDetails getAvatarFileDetails(Context context, RecipientId recipientId) {
        File avatarFile = getAvatarFile(context, recipientId);
        if (!avatarFile.exists() || avatarFile.length() <= 0) {
            return ProfileAvatarFileDetails.NO_DETAILS;
        }
        return new ProfileAvatarFileDetails((long) avatarFile.hashCode(), avatarFile.lastModified());
    }

    public static InputStream getAvatar(Context context, RecipientId recipientId) throws IOException {
        return ModernDecryptingPartInputStream.createFor(AttachmentSecretProvider.getInstance(context).getOrCreateAttachmentSecret(), getAvatarFile(context, recipientId), 0);
    }

    public static byte[] getAvatarBytes(Context context, RecipientId recipientId) throws IOException {
        if (hasAvatar(context, recipientId)) {
            return StreamUtil.readFully(getAvatar(context, recipientId));
        }
        return null;
    }

    public static long getAvatarLength(Context context, RecipientId recipientId) {
        return ModernEncryptingPartOutputStream.getPlaintextLength(getAvatarFile(context, recipientId).length());
    }

    public static void setAvatar(Context context, RecipientId recipientId, InputStream inputStream) throws IOException {
        if (inputStream == null) {
            delete(context, recipientId);
            return;
        }
        OutputStream outputStream = null;
        try {
            outputStream = getOutputStream(context, recipientId, false);
            StreamUtil.copy(inputStream, outputStream);
        } finally {
            StreamUtil.close(outputStream);
        }
    }

    public static void setSyncAvatar(Context context, RecipientId recipientId, InputStream inputStream) throws IOException {
        if (inputStream == null) {
            delete(context, recipientId);
            return;
        }
        OutputStream outputStream = null;
        try {
            outputStream = getOutputStream(context, recipientId, true);
            StreamUtil.copy(inputStream, outputStream);
        } finally {
            StreamUtil.close(outputStream);
        }
    }

    public static OutputStream getOutputStream(Context context, RecipientId recipientId, boolean z) throws IOException {
        return (OutputStream) ModernEncryptingPartOutputStream.createFor(AttachmentSecretProvider.getInstance(context).getOrCreateAttachmentSecret(), getAvatarFile(context, recipientId, z), true).second;
    }

    public static StreamDetails getSelfProfileAvatarStream(Context context) {
        RecipientId id = Recipient.self().getId();
        if (!hasAvatar(context, id)) {
            return null;
        }
        try {
            return new StreamDetails(getAvatar(context, id), MediaUtil.IMAGE_JPEG, getAvatarLength(context, id));
        } catch (IOException e) {
            Log.w(TAG, "Failed to read own avatar!", e);
            return null;
        }
    }

    private static File getAvatarFile(Context context, RecipientId recipientId) {
        boolean z = false;
        File avatarFile = getAvatarFile(context, recipientId, false);
        boolean z2 = avatarFile.exists() && avatarFile.length() > 0;
        File avatarFile2 = getAvatarFile(context, recipientId, true);
        if (avatarFile2.exists() && avatarFile2.length() > 0) {
            z = true;
        }
        if (!SignalStore.settings().isPreferSystemContactPhotos() || !z) {
            return (!z2 && z) ? avatarFile2 : avatarFile;
        }
        return avatarFile2;
    }

    private static File getAvatarFile(Context context, RecipientId recipientId, boolean z) {
        File avatarDirectory2 = getAvatarDirectory(context);
        StringBuilder sb = new StringBuilder();
        sb.append(recipientId.serialize());
        sb.append(z ? "-sync" : "");
        return new File(avatarDirectory2, sb.toString());
    }

    /* loaded from: classes4.dex */
    public static class Avatar {
        private final String filename;
        private final InputStream inputStream;
        private final long length;

        public Avatar(InputStream inputStream, String str, long j) {
            this.inputStream = inputStream;
            this.filename = str;
            this.length = j;
        }

        public InputStream getInputStream() {
            return this.inputStream;
        }

        public String getFilename() {
            return this.filename;
        }

        public long getLength() {
            return this.length;
        }
    }
}
