package org.thoughtcrime.securesms.profiles.manage;

import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.core.util.Consumer;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import org.signal.core.util.EditTextUtil;
import org.signal.core.util.StringUtil;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.profiles.manage.EditProfileNameViewModel;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.text.AfterTextChanged;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;

/* loaded from: classes4.dex */
public class EditProfileNameFragment extends Fragment {
    public static final int NAME_MAX_GLYPHS;
    private EditText familyName;
    private EditText givenName;
    private CircularProgressMaterialButton saveButton;
    private EditProfileNameViewModel viewModel;

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.edit_profile_name_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.givenName = (EditText) view.findViewById(R.id.edit_profile_name_given_name);
        this.familyName = (EditText) view.findViewById(R.id.edit_profile_name_family_name);
        this.saveButton = (CircularProgressMaterialButton) view.findViewById(R.id.edit_profile_name_save);
        initializeViewModel();
        this.givenName.setText(Recipient.self().getProfileName().getGivenName());
        this.familyName.setText(Recipient.self().getProfileName().getFamilyName());
        this.viewModel.onGivenNameChanged(this.givenName.getText().toString());
        ((Toolbar) view.findViewById(R.id.toolbar)).setNavigationOnClickListener(new View.OnClickListener(view) { // from class: org.thoughtcrime.securesms.profiles.manage.EditProfileNameFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditProfileNameFragment.lambda$onViewCreated$0(this.f$0, view2);
            }
        });
        EditTextUtil.addGraphemeClusterLimitFilter(this.givenName, 26);
        EditTextUtil.addGraphemeClusterLimitFilter(this.familyName, 26);
        this.givenName.addTextChangedListener(new AfterTextChanged(new Consumer() { // from class: org.thoughtcrime.securesms.profiles.manage.EditProfileNameFragment$$ExternalSyntheticLambda3
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                EditProfileNameFragment.this.lambda$onViewCreated$1((Editable) obj);
            }
        }));
        this.familyName.addTextChangedListener(new AfterTextChanged(new Consumer() { // from class: org.thoughtcrime.securesms.profiles.manage.EditProfileNameFragment$$ExternalSyntheticLambda4
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                EditProfileNameFragment.trimFieldToMaxByteLength((Editable) obj);
            }
        }));
        this.saveButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.profiles.manage.EditProfileNameFragment$$ExternalSyntheticLambda5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditProfileNameFragment.this.lambda$onViewCreated$2(view2);
            }
        });
        ViewUtil.focusAndMoveCursorToEndAndOpenKeyboard(this.givenName);
    }

    public static /* synthetic */ void lambda$onViewCreated$0(View view, View view2) {
        Navigation.findNavController(view).popBackStack();
    }

    public /* synthetic */ void lambda$onViewCreated$1(Editable editable) {
        trimFieldToMaxByteLength(editable);
        this.viewModel.onGivenNameChanged(editable.toString());
    }

    public /* synthetic */ void lambda$onViewCreated$2(View view) {
        this.viewModel.onSaveClicked(requireContext(), this.givenName.getText().toString(), this.familyName.getText().toString());
    }

    private void initializeViewModel() {
        EditProfileNameViewModel editProfileNameViewModel = (EditProfileNameViewModel) ViewModelProviders.of(this).get(EditProfileNameViewModel.class);
        this.viewModel = editProfileNameViewModel;
        editProfileNameViewModel.getSaveState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.profiles.manage.EditProfileNameFragment$$ExternalSyntheticLambda0
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                EditProfileNameFragment.this.presentSaveState((EditProfileNameViewModel.SaveState) obj);
            }
        });
        this.viewModel.getEvents().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.profiles.manage.EditProfileNameFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                EditProfileNameFragment.this.presentEvent((EditProfileNameViewModel.Event) obj);
            }
        });
    }

    /* renamed from: org.thoughtcrime.securesms.profiles.manage.EditProfileNameFragment$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$profiles$manage$EditProfileNameViewModel$SaveState;

        static {
            int[] iArr = new int[EditProfileNameViewModel.SaveState.values().length];
            $SwitchMap$org$thoughtcrime$securesms$profiles$manage$EditProfileNameViewModel$SaveState = iArr;
            try {
                iArr[EditProfileNameViewModel.SaveState.DISABLED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$EditProfileNameViewModel$SaveState[EditProfileNameViewModel.SaveState.IDLE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$EditProfileNameViewModel$SaveState[EditProfileNameViewModel.SaveState.IN_PROGRESS.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$EditProfileNameViewModel$SaveState[EditProfileNameViewModel.SaveState.DONE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    public void presentSaveState(EditProfileNameViewModel.SaveState saveState) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$profiles$manage$EditProfileNameViewModel$SaveState[saveState.ordinal()];
        if (i == 1) {
            this.saveButton.setClickable(false);
            this.saveButton.setAlpha(0.5f);
            setEditTextEnabled(this.givenName, true);
            setEditTextEnabled(this.familyName, true);
        } else if (i == 2) {
            this.saveButton.setClickable(true);
            this.saveButton.cancelSpinning();
            this.saveButton.setAlpha(1.0f);
            setEditTextEnabled(this.givenName, true);
            setEditTextEnabled(this.familyName, true);
        } else if (i == 3) {
            this.saveButton.setClickable(false);
            this.saveButton.setSpinning();
            this.saveButton.setAlpha(1.0f);
            setEditTextEnabled(this.givenName, false);
            setEditTextEnabled(this.familyName, false);
        } else if (i == 4) {
            this.saveButton.setClickable(false);
            Navigation.findNavController(requireView()).popBackStack();
            this.saveButton.setAlpha(1.0f);
            setEditTextEnabled(this.givenName, false);
            setEditTextEnabled(this.familyName, false);
        }
    }

    public void presentEvent(EditProfileNameViewModel.Event event) {
        if (event == EditProfileNameViewModel.Event.NETWORK_FAILURE) {
            Toast.makeText(requireContext(), (int) R.string.EditProfileNameFragment_failed_to_save_due_to_network_issues_try_again_later, 0).show();
        }
    }

    public static void trimFieldToMaxByteLength(Editable editable) {
        trimFieldToMaxByteLength(editable, 128);
    }

    public static void trimFieldToMaxByteLength(Editable editable, int i) {
        int length = StringUtil.trimToFit(editable.toString(), i).length();
        if (editable.length() > length) {
            editable.delete(length, editable.length());
        }
    }

    private static void setEditTextEnabled(EditText editText, boolean z) {
        editText.setEnabled(z);
        editText.setFocusable(z);
        if (z) {
            editText.setInputType(96);
            return;
        }
        editText.clearFocus();
        editText.setInputType(0);
    }
}
