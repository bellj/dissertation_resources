package org.thoughtcrime.securesms.profiles.edit;

import android.content.Context;
import android.text.TextUtils;
import androidx.core.util.Consumer;
import j$.util.Optional;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.MultiDeviceProfileContentUpdateJob;
import org.thoughtcrime.securesms.jobs.MultiDeviceProfileKeyUpdateJob;
import org.thoughtcrime.securesms.jobs.ProfileUploadJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.profiles.ProfileMediaConstraints;
import org.thoughtcrime.securesms.profiles.ProfileName;
import org.thoughtcrime.securesms.profiles.SystemProfileUtil;
import org.thoughtcrime.securesms.profiles.edit.EditProfileRepository;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.registration.RegistrationUtil;
import org.thoughtcrime.securesms.util.concurrent.ListenableFuture;

/* loaded from: classes4.dex */
public class EditSelfProfileRepository implements EditProfileRepository {
    private static final String TAG = Log.tag(EditSelfProfileRepository.class);
    private final Context context;
    private final boolean excludeSystem;

    public EditSelfProfileRepository(Context context, boolean z) {
        this.context = context.getApplicationContext();
        this.excludeSystem = z;
    }

    public static /* synthetic */ AvatarColor lambda$getCurrentAvatarColor$0() {
        return Recipient.self().getAvatarColor();
    }

    @Override // org.thoughtcrime.securesms.profiles.edit.EditProfileRepository
    public void getCurrentAvatarColor(Consumer<AvatarColor> consumer) {
        EditSelfProfileRepository$$ExternalSyntheticLambda0 editSelfProfileRepository$$ExternalSyntheticLambda0 = new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.profiles.edit.EditSelfProfileRepository$$ExternalSyntheticLambda0
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return EditSelfProfileRepository.lambda$getCurrentAvatarColor$0();
            }
        };
        Objects.requireNonNull(consumer);
        SimpleTask.run(editSelfProfileRepository$$ExternalSyntheticLambda0, new EditGroupProfileRepository$$ExternalSyntheticLambda8(consumer));
    }

    @Override // org.thoughtcrime.securesms.profiles.edit.EditProfileRepository
    public void getCurrentProfileName(final Consumer<ProfileName> consumer) {
        final ProfileName profileName = Recipient.self().getProfileName();
        if (!profileName.isEmpty()) {
            consumer.accept(profileName);
        } else if (!this.excludeSystem) {
            SystemProfileUtil.getSystemProfileName(this.context).addListener(new ListenableFuture.Listener<String>() { // from class: org.thoughtcrime.securesms.profiles.edit.EditSelfProfileRepository.1
                public void onSuccess(String str) {
                    if (!TextUtils.isEmpty(str)) {
                        consumer.accept(ProfileName.fromSerialized(str));
                    } else {
                        consumer.accept(profileName);
                    }
                }

                @Override // org.thoughtcrime.securesms.util.concurrent.ListenableFuture.Listener
                public void onFailure(ExecutionException executionException) {
                    Log.w(EditSelfProfileRepository.TAG, executionException);
                    consumer.accept(profileName);
                }
            });
        } else {
            consumer.accept(profileName);
        }
    }

    @Override // org.thoughtcrime.securesms.profiles.edit.EditProfileRepository
    public void getCurrentAvatar(final Consumer<byte[]> consumer) {
        RecipientId id = Recipient.self().getId();
        if (AvatarHelper.hasAvatar(this.context, id)) {
            EditSelfProfileRepository$$ExternalSyntheticLambda1 editSelfProfileRepository$$ExternalSyntheticLambda1 = new SimpleTask.BackgroundTask(id) { // from class: org.thoughtcrime.securesms.profiles.edit.EditSelfProfileRepository$$ExternalSyntheticLambda1
                public final /* synthetic */ RecipientId f$1;

                {
                    this.f$1 = r2;
                }

                @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
                public final Object run() {
                    return EditSelfProfileRepository.this.lambda$getCurrentAvatar$1(this.f$1);
                }
            };
            Objects.requireNonNull(consumer);
            SimpleTask.run(editSelfProfileRepository$$ExternalSyntheticLambda1, new EditGroupProfileRepository$$ExternalSyntheticLambda10(consumer));
        } else if (!this.excludeSystem) {
            SystemProfileUtil.getSystemProfileAvatar(this.context, new ProfileMediaConstraints()).addListener(new ListenableFuture.Listener<byte[]>() { // from class: org.thoughtcrime.securesms.profiles.edit.EditSelfProfileRepository.2
                public void onSuccess(byte[] bArr) {
                    consumer.accept(bArr);
                }

                @Override // org.thoughtcrime.securesms.util.concurrent.ListenableFuture.Listener
                public void onFailure(ExecutionException executionException) {
                    Log.w(EditSelfProfileRepository.TAG, executionException);
                    consumer.accept(null);
                }
            });
        }
    }

    public /* synthetic */ byte[] lambda$getCurrentAvatar$1(RecipientId recipientId) {
        try {
            return StreamUtil.readFully(AvatarHelper.getAvatar(this.context, recipientId));
        } catch (IOException e) {
            Log.w(TAG, e);
            return null;
        }
    }

    @Override // org.thoughtcrime.securesms.profiles.edit.EditProfileRepository
    public void getCurrentDisplayName(Consumer<String> consumer) {
        consumer.accept("");
    }

    @Override // org.thoughtcrime.securesms.profiles.edit.EditProfileRepository
    public void getCurrentName(Consumer<String> consumer) {
        consumer.accept("");
    }

    @Override // org.thoughtcrime.securesms.profiles.edit.EditProfileRepository
    public void getCurrentDescription(Consumer<String> consumer) {
        consumer.accept("");
    }

    @Override // org.thoughtcrime.securesms.profiles.edit.EditProfileRepository
    public void uploadProfile(ProfileName profileName, String str, boolean z, String str2, boolean z2, byte[] bArr, boolean z3, Consumer<EditProfileRepository.UploadResult> consumer) {
        EditSelfProfileRepository$$ExternalSyntheticLambda2 editSelfProfileRepository$$ExternalSyntheticLambda2 = new SimpleTask.BackgroundTask(profileName, z3, bArr) { // from class: org.thoughtcrime.securesms.profiles.edit.EditSelfProfileRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ ProfileName f$1;
            public final /* synthetic */ boolean f$2;
            public final /* synthetic */ byte[] f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return EditSelfProfileRepository.this.lambda$uploadProfile$2(this.f$1, this.f$2, this.f$3);
            }
        };
        Objects.requireNonNull(consumer);
        SimpleTask.run(editSelfProfileRepository$$ExternalSyntheticLambda2, new EditGroupProfileRepository$$ExternalSyntheticLambda13(consumer));
    }

    public /* synthetic */ EditProfileRepository.UploadResult lambda$uploadProfile$2(ProfileName profileName, boolean z, byte[] bArr) {
        SignalDatabase.recipients().setProfileName(Recipient.self().getId(), profileName);
        if (z) {
            try {
                AvatarHelper.setAvatar(this.context, Recipient.self().getId(), bArr != null ? new ByteArrayInputStream(bArr) : null);
            } catch (IOException unused) {
                return EditProfileRepository.UploadResult.ERROR_IO;
            }
        }
        ApplicationDependencies.getJobManager().startChain(new ProfileUploadJob()).then(Arrays.asList(new MultiDeviceProfileKeyUpdateJob(), new MultiDeviceProfileContentUpdateJob())).enqueue();
        RegistrationUtil.maybeMarkRegistrationComplete(this.context);
        if (bArr != null) {
            SignalStore.misc().markHasEverHadAnAvatar();
        }
        return EditProfileRepository.UploadResult.SUCCESS;
    }

    @Override // org.thoughtcrime.securesms.profiles.edit.EditProfileRepository
    public void getCurrentUsername(Consumer<Optional<String>> consumer) {
        consumer.accept(Recipient.self().getUsername());
    }
}
