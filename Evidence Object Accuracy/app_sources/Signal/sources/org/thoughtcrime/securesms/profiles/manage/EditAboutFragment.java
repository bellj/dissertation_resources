package org.thoughtcrime.securesms.profiles.manage;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.core.util.Consumer;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import j$.util.Optional;
import java.util.Arrays;
import java.util.List;
import org.signal.core.util.BreakIteratorCompat;
import org.signal.core.util.EditTextUtil;
import org.signal.core.util.StringUtil;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiUtil;
import org.thoughtcrime.securesms.profiles.manage.EditAboutViewModel;
import org.thoughtcrime.securesms.profiles.manage.ManageProfileActivity;
import org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.adapter.AlwaysChangedDiffUtil;
import org.thoughtcrime.securesms.util.text.AfterTextChanged;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;

/* loaded from: classes4.dex */
public class EditAboutFragment extends Fragment implements ManageProfileActivity.EmojiController {
    public static final int ABOUT_LIMIT_DISPLAY_THRESHOLD;
    public static final int ABOUT_MAX_GLYPHS;
    private static final String KEY_SELECTED_EMOJI;
    private static final List<AboutPreset> PRESETS = Arrays.asList(new AboutPreset("👋", R.string.EditAboutFragment_speak_freely, null), new AboutPreset("🤐", R.string.EditAboutFragment_encrypted, null), new AboutPreset("🙏", R.string.EditAboutFragment_be_kind, null), new AboutPreset("☕", R.string.EditAboutFragment_coffee_lover, null), new AboutPreset("👍", R.string.EditAboutFragment_free_to_chat, null), new AboutPreset("📵", R.string.EditAboutFragment_taking_a_break, null), new AboutPreset("🚀", R.string.EditAboutFragment_working_on_something_new, null));
    private EditText bodyView;
    private TextView countView;
    private ImageView emojiView;
    private CircularProgressMaterialButton saveButton;
    private String selectedEmoji;
    private EditAboutViewModel viewModel;

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.edit_about_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.emojiView = (ImageView) view.findViewById(R.id.edit_about_emoji);
        this.bodyView = (EditText) view.findViewById(R.id.edit_about_body);
        this.countView = (TextView) view.findViewById(R.id.edit_about_count);
        this.saveButton = (CircularProgressMaterialButton) view.findViewById(R.id.edit_about_save);
        initializeViewModel();
        ((Toolbar) view.findViewById(R.id.toolbar)).setNavigationOnClickListener(new View.OnClickListener(view) { // from class: org.thoughtcrime.securesms.profiles.manage.EditAboutFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditAboutFragment.$r8$lambda$nmmkUMKDzUuaDHso2FMznOGZTH4(this.f$0, view2);
            }
        });
        EditTextUtil.addGraphemeClusterLimitFilter(this.bodyView, ABOUT_MAX_GLYPHS);
        this.bodyView.addTextChangedListener(new AfterTextChanged(new Consumer() { // from class: org.thoughtcrime.securesms.profiles.manage.EditAboutFragment$$ExternalSyntheticLambda1
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                EditAboutFragment.$r8$lambda$GQyNqhl6zqmdS03XysuazKsPIuA(EditAboutFragment.this, (Editable) obj);
            }
        }));
        this.emojiView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.profiles.manage.EditAboutFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditAboutFragment.$r8$lambda$EpYy9xerVuyP9IE4Q04RHwzV4zs(EditAboutFragment.this, view2);
            }
        });
        view.findViewById(R.id.edit_about_clear).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.profiles.manage.EditAboutFragment$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditAboutFragment.m2481$r8$lambda$3Ess52RX4tGbBYnW_1OAsQT2Tk(EditAboutFragment.this, view2);
            }
        });
        this.saveButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.profiles.manage.EditAboutFragment$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditAboutFragment.$r8$lambda$zwO_hX1xyWGz5lLiGbyJM5bvtWE(EditAboutFragment.this, view2);
            }
        });
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.edit_about_presets);
        PresetAdapter presetAdapter = new PresetAdapter();
        recyclerView.setAdapter(presetAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        presetAdapter.submitList(PRESETS);
        if (bundle == null || !bundle.containsKey(KEY_SELECTED_EMOJI)) {
            this.bodyView.setText(Recipient.self().getAbout());
            onEmojiSelectedInternal((String) Optional.ofNullable(Recipient.self().getAboutEmoji()).orElse(""));
        } else {
            onEmojiSelectedInternal(bundle.getString(KEY_SELECTED_EMOJI, ""));
        }
        ViewUtil.focusAndMoveCursorToEndAndOpenKeyboard(this.bodyView);
    }

    public static /* synthetic */ void lambda$onViewCreated$0(View view, View view2) {
        Navigation.findNavController(view).popBackStack();
    }

    public /* synthetic */ void lambda$onViewCreated$1(Editable editable) {
        trimFieldToMaxByteLength(editable);
        presentCount(editable.toString());
    }

    public /* synthetic */ void lambda$onViewCreated$2(View view) {
        ReactWithAnyEmojiBottomSheetDialogFragment.createForAboutSelection().show(requireFragmentManager(), BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
    }

    public /* synthetic */ void lambda$onViewCreated$3(View view) {
        onClearClicked();
    }

    public /* synthetic */ void lambda$onViewCreated$4(View view) {
        this.viewModel.onSaveClicked(requireContext(), this.bodyView.getText().toString(), this.selectedEmoji);
    }

    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putString(KEY_SELECTED_EMOJI, this.selectedEmoji);
    }

    @Override // org.thoughtcrime.securesms.profiles.manage.ManageProfileActivity.EmojiController
    public void onEmojiSelected(String str) {
        onEmojiSelectedInternal(str);
        ViewUtil.focusAndMoveCursorToEndAndOpenKeyboard(this.bodyView);
    }

    private void onEmojiSelectedInternal(String str) {
        Drawable convertToDrawable = EmojiUtil.convertToDrawable(requireContext(), str);
        if (convertToDrawable != null) {
            this.emojiView.setImageDrawable(convertToDrawable);
            this.selectedEmoji = str;
            return;
        }
        this.emojiView.setImageResource(R.drawable.ic_add_emoji);
        this.selectedEmoji = "";
    }

    private void initializeViewModel() {
        EditAboutViewModel editAboutViewModel = (EditAboutViewModel) ViewModelProviders.of(this).get(EditAboutViewModel.class);
        this.viewModel = editAboutViewModel;
        editAboutViewModel.getSaveState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.profiles.manage.EditAboutFragment$$ExternalSyntheticLambda5
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                EditAboutFragment.$r8$lambda$FHI1g6Ud7UNDqSve8qxU0JnhjU0(EditAboutFragment.this, (EditAboutViewModel.SaveState) obj);
            }
        });
        this.viewModel.getEvents().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.profiles.manage.EditAboutFragment$$ExternalSyntheticLambda6
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                EditAboutFragment.$r8$lambda$rtLqLAnkb5j3mpaN55PkyUazEPQ(EditAboutFragment.this, (EditAboutViewModel.Event) obj);
            }
        });
    }

    private void presentCount(String str) {
        BreakIteratorCompat instance = BreakIteratorCompat.getInstance();
        instance.setText(str);
        int countBreaks = instance.countBreaks();
        if (countBreaks >= 120) {
            this.countView.setVisibility(0);
            this.countView.setText(getResources().getString(R.string.EditAboutFragment_count, Integer.valueOf(countBreaks), Integer.valueOf((int) ABOUT_MAX_GLYPHS)));
            return;
        }
        this.countView.setVisibility(8);
    }

    /* renamed from: org.thoughtcrime.securesms.profiles.manage.EditAboutFragment$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$profiles$manage$EditAboutViewModel$SaveState;

        static {
            int[] iArr = new int[EditAboutViewModel.SaveState.values().length];
            $SwitchMap$org$thoughtcrime$securesms$profiles$manage$EditAboutViewModel$SaveState = iArr;
            try {
                iArr[EditAboutViewModel.SaveState.IDLE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$EditAboutViewModel$SaveState[EditAboutViewModel.SaveState.IN_PROGRESS.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$manage$EditAboutViewModel$SaveState[EditAboutViewModel.SaveState.DONE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    public void presentSaveState(EditAboutViewModel.SaveState saveState) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$profiles$manage$EditAboutViewModel$SaveState[saveState.ordinal()];
        if (i == 1) {
            this.saveButton.cancelSpinning();
        } else if (i == 2) {
            this.saveButton.setSpinning();
        } else if (i == 3) {
            this.saveButton.setClickable(false);
            Navigation.findNavController(requireView()).popBackStack();
        }
    }

    public void presentEvent(EditAboutViewModel.Event event) {
        if (event == EditAboutViewModel.Event.NETWORK_FAILURE) {
            Toast.makeText(requireContext(), (int) R.string.EditProfileNameFragment_failed_to_save_due_to_network_issues_try_again_later, 0).show();
        }
    }

    private void onClearClicked() {
        this.bodyView.setText("");
        onEmojiSelectedInternal("");
    }

    private static void trimFieldToMaxByteLength(Editable editable) {
        int length = StringUtil.trimToFit(editable.toString(), 512).length();
        if (editable.length() > length) {
            editable.delete(length, editable.length());
        }
    }

    public void onPresetSelected(AboutPreset aboutPreset) {
        onEmojiSelectedInternal(aboutPreset.getEmoji());
        this.bodyView.setText(requireContext().getString(aboutPreset.getBodyRes()));
        EditText editText = this.bodyView;
        editText.setSelection(editText.length(), this.bodyView.length());
    }

    /* loaded from: classes4.dex */
    public final class PresetAdapter extends ListAdapter<AboutPreset, PresetViewHolder> {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        protected PresetAdapter() {
            super(new AlwaysChangedDiffUtil());
            EditAboutFragment.this = r1;
        }

        public PresetViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            return new PresetViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.about_preset_item, viewGroup, false));
        }

        public void onBindViewHolder(PresetViewHolder presetViewHolder, int i) {
            AboutPreset item = getItem(i);
            presetViewHolder.bind(item);
            presetViewHolder.itemView.setOnClickListener(new EditAboutFragment$PresetAdapter$$ExternalSyntheticLambda0(this, item));
        }

        public /* synthetic */ void lambda$onBindViewHolder$0(AboutPreset aboutPreset, View view) {
            EditAboutFragment.this.onPresetSelected(aboutPreset);
        }
    }

    /* loaded from: classes4.dex */
    public final class PresetViewHolder extends RecyclerView.ViewHolder {
        private final TextView body;
        private final ImageView emoji;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public PresetViewHolder(View view) {
            super(view);
            EditAboutFragment.this = r1;
            this.emoji = (ImageView) view.findViewById(R.id.about_preset_emoji);
            this.body = (TextView) view.findViewById(R.id.about_preset_body);
        }

        public void bind(AboutPreset aboutPreset) {
            this.emoji.setImageDrawable(EmojiUtil.convertToDrawable(EditAboutFragment.this.requireContext(), aboutPreset.getEmoji()));
            this.body.setText(aboutPreset.getBodyRes());
        }
    }

    /* loaded from: classes4.dex */
    public static final class AboutPreset {
        private final int bodyRes;
        private final String emoji;

        /* synthetic */ AboutPreset(String str, int i, AnonymousClass1 r3) {
            this(str, i);
        }

        private AboutPreset(String str, int i) {
            this.emoji = str;
            this.bodyRes = i;
        }

        public String getEmoji() {
            return this.emoji;
        }

        public int getBodyRes() {
            return this.bodyRes;
        }
    }
}
