package org.thoughtcrime.securesms.profiles.edit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.navigation.fragment.NavHostFragment;
import org.thoughtcrime.securesms.BaseActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.profiles.edit.EditProfileFragment;
import org.thoughtcrime.securesms.util.DynamicRegistrationTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;

/* loaded from: classes4.dex */
public class EditProfileActivity extends BaseActivity implements EditProfileFragment.Controller {
    public static final String EXCLUDE_SYSTEM;
    public static final String GROUP_ID;
    public static final String NEXT_BUTTON_TEXT;
    public static final String NEXT_INTENT;
    public static final String SHOW_TOOLBAR;
    private final DynamicTheme dynamicTheme = new DynamicRegistrationTheme();

    public static Intent getIntentForUserProfile(Context context) {
        Intent intent = new Intent(context, EditProfileActivity.class);
        intent.putExtra(SHOW_TOOLBAR, false);
        return intent;
    }

    public static Intent getIntentForUserProfileEdit(Context context) {
        Intent intent = new Intent(context, EditProfileActivity.class);
        intent.putExtra(EXCLUDE_SYSTEM, true);
        intent.putExtra(NEXT_BUTTON_TEXT, R.string.save);
        return intent;
    }

    public static Intent getIntentForGroupProfile(Context context, GroupId groupId) {
        Intent intent = new Intent(context, EditProfileActivity.class);
        intent.putExtra(SHOW_TOOLBAR, true);
        intent.putExtra("group_id", groupId.toString());
        intent.putExtra(NEXT_BUTTON_TEXT, R.string.save);
        return intent;
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.dynamicTheme.onCreate(this);
        setContentView(R.layout.profile_create_activity);
        if (bundle == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, NavHostFragment.create(R.navigation.edit_profile, getIntent().getExtras())).commit();
        }
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }

    @Override // org.thoughtcrime.securesms.profiles.edit.EditProfileFragment.Controller
    public void onProfileNameUploadCompleted() {
        setResult(-1);
        finish();
    }
}
