package org.thoughtcrime.securesms.profiles.spoofing;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.recyclerview.widget.ListAdapter;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.profiles.spoofing.ReviewCard;
import org.thoughtcrime.securesms.profiles.spoofing.ReviewCardViewHolder;
import org.thoughtcrime.securesms.util.adapter.AlwaysChangedDiffUtil;

/* loaded from: classes4.dex */
public class ReviewCardAdapter extends ListAdapter<ReviewCard, ReviewCardViewHolder> {
    private final CallbacksAdapter callbackAdapter;
    private final int groupsInCommonResId;
    private final int noGroupsInCommonResId;

    /* access modifiers changed from: package-private */
    /* loaded from: classes4.dex */
    public interface Callbacks {
        void onActionClicked(ReviewCard reviewCard, ReviewCard.Action action);

        void onCardClicked(ReviewCard reviewCard);
    }

    public ReviewCardAdapter(int i, int i2, Callbacks callbacks) {
        super(new AlwaysChangedDiffUtil());
        this.noGroupsInCommonResId = i;
        this.groupsInCommonResId = i2;
        this.callbackAdapter = new CallbacksAdapter(callbacks);
    }

    public ReviewCardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ReviewCardViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.review_card, viewGroup, false), this.noGroupsInCommonResId, this.groupsInCommonResId, this.callbackAdapter);
    }

    public void onBindViewHolder(ReviewCardViewHolder reviewCardViewHolder, int i) {
        reviewCardViewHolder.bind(getItem(i));
    }

    /* loaded from: classes4.dex */
    public final class CallbacksAdapter implements ReviewCardViewHolder.Callbacks {
        private final Callbacks callback;

        private CallbacksAdapter(Callbacks callbacks) {
            ReviewCardAdapter.this = r1;
            this.callback = callbacks;
        }

        public void onCardClicked(int i) {
            this.callback.onCardClicked((ReviewCard) ReviewCardAdapter.this.getItem(i));
        }

        public void onPrimaryActionItemClicked(int i) {
            ReviewCard reviewCard = (ReviewCard) ReviewCardAdapter.this.getItem(i);
            Callbacks callbacks = this.callback;
            ReviewCard.Action primaryAction = reviewCard.getPrimaryAction();
            Objects.requireNonNull(primaryAction);
            callbacks.onActionClicked(reviewCard, primaryAction);
        }

        public void onSecondaryActionItemClicked(int i) {
            ReviewCard reviewCard = (ReviewCard) ReviewCardAdapter.this.getItem(i);
            Callbacks callbacks = this.callback;
            ReviewCard.Action secondaryAction = reviewCard.getSecondaryAction();
            Objects.requireNonNull(secondaryAction);
            callbacks.onActionClicked(reviewCard, secondaryAction);
        }
    }
}
