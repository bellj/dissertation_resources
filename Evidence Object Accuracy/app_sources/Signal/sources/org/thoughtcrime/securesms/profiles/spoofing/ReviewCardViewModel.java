package org.thoughtcrime.securesms.profiles.spoofing;

import android.util.Pair;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.Stream;
import j$.util.function.Function;
import java.util.List;
import java.util.Objects;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.LiveGroup;
import org.thoughtcrime.securesms.profiles.spoofing.ReviewCard;
import org.thoughtcrime.securesms.profiles.spoofing.ReviewCardRepository;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.DefaultValueLiveData;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* loaded from: classes4.dex */
public class ReviewCardViewModel extends ViewModel {
    private final boolean isGroupThread;
    private final ReviewCardRepository repository;
    private final LiveData<List<ReviewCard>> reviewCards;
    private final SingleLiveEvent<Event> reviewEvents;
    private final MutableLiveData<List<ReviewRecipient>> reviewRecipients;

    /* loaded from: classes4.dex */
    public enum Event {
        DISMISS,
        REMOVE_FAILED
    }

    public ReviewCardViewModel(ReviewCardRepository reviewCardRepository, GroupId groupId) {
        LiveData<Boolean> liveData;
        if (groupId != null) {
            liveData = new LiveGroup(groupId).getRecipientIsAdmin(Recipient.self().getId());
        } else {
            liveData = new DefaultValueLiveData<>(Boolean.FALSE);
        }
        this.isGroupThread = groupId != null;
        this.repository = reviewCardRepository;
        MutableLiveData<List<ReviewRecipient>> mutableLiveData = new MutableLiveData<>();
        this.reviewRecipients = mutableLiveData;
        this.reviewCards = LiveDataUtil.mapAsync(LiveDataUtil.combineLatest(liveData, mutableLiveData, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.profiles.spoofing.ReviewCardViewModel$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return new Pair((Boolean) obj, (List) obj2);
            }
        }), new Function() { // from class: org.thoughtcrime.securesms.profiles.spoofing.ReviewCardViewModel$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ReviewCardViewModel.this.lambda$new$0((Pair) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        this.reviewEvents = new SingleLiveEvent<>();
        reviewCardRepository.loadRecipients(new OnRecipientsLoadedListener(this, null));
    }

    public /* synthetic */ List lambda$new$0(Pair pair) {
        return transformReviewRecipients(((Boolean) pair.first).booleanValue(), (List) pair.second);
    }

    public LiveData<List<ReviewCard>> getReviewCards() {
        return this.reviewCards;
    }

    public LiveData<Event> getReviewEvents() {
        return this.reviewEvents;
    }

    public void act(ReviewCard reviewCard, ReviewCard.Action action) {
        if (reviewCard.getPrimaryAction() == action || reviewCard.getSecondaryAction() == action) {
            performAction(reviewCard, action);
            return;
        }
        throw new IllegalArgumentException("Cannot perform " + action + " on review card.");
    }

    /* renamed from: org.thoughtcrime.securesms.profiles.spoofing.ReviewCardViewModel$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCard$Action;

        static {
            int[] iArr = new int[ReviewCard.Action.values().length];
            $SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCard$Action = iArr;
            try {
                iArr[ReviewCard.Action.BLOCK.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCard$Action[ReviewCard.Action.DELETE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCard$Action[ReviewCard.Action.REMOVE_FROM_GROUP.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    private void performAction(ReviewCard reviewCard, ReviewCard.Action action) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$profiles$spoofing$ReviewCard$Action[action.ordinal()];
        if (i == 1) {
            this.repository.block(reviewCard, new Runnable() { // from class: org.thoughtcrime.securesms.profiles.spoofing.ReviewCardViewModel$$ExternalSyntheticLambda2
                @Override // java.lang.Runnable
                public final void run() {
                    ReviewCardViewModel.this.lambda$performAction$1();
                }
            });
        } else if (i == 2) {
            this.repository.delete(reviewCard, new Runnable() { // from class: org.thoughtcrime.securesms.profiles.spoofing.ReviewCardViewModel$$ExternalSyntheticLambda3
                @Override // java.lang.Runnable
                public final void run() {
                    ReviewCardViewModel.this.lambda$performAction$2();
                }
            });
        } else if (i == 3) {
            this.repository.removeFromGroup(reviewCard, new OnRemoveFromGroupListener(this, null));
        } else {
            throw new IllegalArgumentException("Unsupported action: " + action);
        }
    }

    public /* synthetic */ void lambda$performAction$1() {
        this.reviewEvents.postValue(Event.DISMISS);
    }

    public /* synthetic */ void lambda$performAction$2() {
        this.reviewEvents.postValue(Event.DISMISS);
    }

    private List<ReviewCard> transformReviewRecipients(boolean z, List<ReviewRecipient> list) {
        return Stream.of(list).map(new com.annimon.stream.function.Function(z) { // from class: org.thoughtcrime.securesms.profiles.spoofing.ReviewCardViewModel$$ExternalSyntheticLambda4
            public final /* synthetic */ boolean f$1;

            {
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ReviewCardViewModel.this.lambda$transformReviewRecipients$3(this.f$1, (ReviewRecipient) obj);
            }
        }).toList();
    }

    public /* synthetic */ ReviewCard lambda$transformReviewRecipients$3(boolean z, ReviewRecipient reviewRecipient) {
        return new ReviewCard(reviewRecipient, this.repository.loadGroupsInCommonCount(reviewRecipient) - (this.isGroupThread ? 1 : 0), getCardType(reviewRecipient), getPrimaryAction(reviewRecipient, z), getSecondaryAction(reviewRecipient));
    }

    private ReviewCard.CardType getCardType(ReviewRecipient reviewRecipient) {
        if (reviewRecipient.getRecipient().isSystemContact()) {
            return ReviewCard.CardType.YOUR_CONTACT;
        }
        if (this.isGroupThread) {
            return ReviewCard.CardType.MEMBER;
        }
        return ReviewCard.CardType.REQUEST;
    }

    private ReviewCard.Action getPrimaryAction(ReviewRecipient reviewRecipient, boolean z) {
        if (reviewRecipient.getRecipient().isSystemContact()) {
            return ReviewCard.Action.UPDATE_CONTACT;
        }
        if (!this.isGroupThread || !z) {
            return ReviewCard.Action.BLOCK;
        }
        return ReviewCard.Action.REMOVE_FROM_GROUP;
    }

    private ReviewCard.Action getSecondaryAction(ReviewRecipient reviewRecipient) {
        if (!reviewRecipient.getRecipient().isSystemContact() && !this.isGroupThread) {
            return ReviewCard.Action.DELETE;
        }
        return null;
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public class OnRecipientsLoadedListener implements ReviewCardRepository.OnRecipientsLoadedListener {
        private OnRecipientsLoadedListener() {
            ReviewCardViewModel.this = r1;
        }

        /* synthetic */ OnRecipientsLoadedListener(ReviewCardViewModel reviewCardViewModel, AnonymousClass1 r2) {
            this();
        }

        @Override // org.thoughtcrime.securesms.profiles.spoofing.ReviewCardRepository.OnRecipientsLoadedListener
        public void onRecipientsLoaded(List<ReviewRecipient> list) {
            if (list.size() < 2) {
                ReviewCardViewModel.this.reviewEvents.postValue(Event.DISMISS);
            } else {
                ReviewCardViewModel.this.reviewRecipients.postValue(list);
            }
        }

        @Override // org.thoughtcrime.securesms.profiles.spoofing.ReviewCardRepository.OnRecipientsLoadedListener
        public void onRecipientsLoadFailed() {
            ReviewCardViewModel.this.reviewEvents.postValue(Event.DISMISS);
        }
    }

    /* loaded from: classes4.dex */
    public class OnRemoveFromGroupListener implements ReviewCardRepository.OnRemoveFromGroupListener {
        private OnRemoveFromGroupListener() {
            ReviewCardViewModel.this = r1;
        }

        /* synthetic */ OnRemoveFromGroupListener(ReviewCardViewModel reviewCardViewModel, AnonymousClass1 r2) {
            this();
        }

        @Override // org.thoughtcrime.securesms.profiles.spoofing.ReviewCardRepository.OnRemoveFromGroupListener
        public void onActionCompleted() {
            ReviewCardViewModel.this.repository.loadRecipients(new OnRecipientsLoadedListener(ReviewCardViewModel.this, null));
        }

        @Override // org.thoughtcrime.securesms.profiles.spoofing.ReviewCardRepository.OnRemoveFromGroupListener
        public void onActionFailed() {
            ReviewCardViewModel.this.reviewEvents.postValue(Event.REMOVE_FAILED);
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements ViewModelProvider.Factory {
        private final GroupId groupId;
        private final ReviewCardRepository repository;

        public Factory(ReviewCardRepository reviewCardRepository, GroupId groupId) {
            this.repository = reviewCardRepository;
            this.groupId = groupId;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            T cast = cls.cast(new ReviewCardViewModel(this.repository, this.groupId));
            Objects.requireNonNull(cast);
            return cast;
        }
    }
}
