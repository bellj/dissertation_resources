package org.thoughtcrime.securesms.profiles.edit;

import androidx.core.util.Consumer;
import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.profiles.edit.EditProfileRepository;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class EditGroupProfileRepository$$ExternalSyntheticLambda13 implements SimpleTask.ForegroundTask {
    public final /* synthetic */ Consumer f$0;

    public /* synthetic */ EditGroupProfileRepository$$ExternalSyntheticLambda13(Consumer consumer) {
        this.f$0 = consumer;
    }

    @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
    public final void run(Object obj) {
        this.f$0.accept((EditProfileRepository.UploadResult) obj);
    }
}
