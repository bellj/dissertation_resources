package org.thoughtcrime.securesms.profiles.edit;

import android.content.Context;
import androidx.core.util.Consumer;
import j$.util.Optional;
import j$.util.function.Function;
import j$.util.function.Supplier;
import java.io.IOException;
import java.util.Objects;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.groups.GroupChangeException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.profiles.ProfileName;
import org.thoughtcrime.securesms.profiles.edit.EditProfileRepository;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class EditGroupProfileRepository implements EditProfileRepository {
    private static final String TAG = Log.tag(EditGroupProfileRepository.class);
    private final Context context;
    private final GroupId groupId;

    public EditGroupProfileRepository(Context context, GroupId groupId) {
        this.context = context.getApplicationContext();
        this.groupId = groupId;
    }

    public /* synthetic */ AvatarColor lambda$getCurrentAvatarColor$0() {
        return Recipient.resolved(getRecipientId()).getAvatarColor();
    }

    @Override // org.thoughtcrime.securesms.profiles.edit.EditProfileRepository
    public void getCurrentAvatarColor(Consumer<AvatarColor> consumer) {
        EditGroupProfileRepository$$ExternalSyntheticLambda7 editGroupProfileRepository$$ExternalSyntheticLambda7 = new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.profiles.edit.EditGroupProfileRepository$$ExternalSyntheticLambda7
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return EditGroupProfileRepository.m2470$r8$lambda$NAHgJ8EFQVzjD9qvTmH3IrnGkk(EditGroupProfileRepository.this);
            }
        };
        Objects.requireNonNull(consumer);
        SimpleTask.run(editGroupProfileRepository$$ExternalSyntheticLambda7, new EditGroupProfileRepository$$ExternalSyntheticLambda8(consumer));
    }

    @Override // org.thoughtcrime.securesms.profiles.edit.EditProfileRepository
    public void getCurrentProfileName(Consumer<ProfileName> consumer) {
        consumer.accept(ProfileName.EMPTY);
    }

    @Override // org.thoughtcrime.securesms.profiles.edit.EditProfileRepository
    public void getCurrentAvatar(Consumer<byte[]> consumer) {
        EditGroupProfileRepository$$ExternalSyntheticLambda9 editGroupProfileRepository$$ExternalSyntheticLambda9 = new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.profiles.edit.EditGroupProfileRepository$$ExternalSyntheticLambda9
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return EditGroupProfileRepository.$r8$lambda$LJOqAMH1HzV9kYlObyWZ2qAKy5c(EditGroupProfileRepository.this);
            }
        };
        Objects.requireNonNull(consumer);
        SimpleTask.run(editGroupProfileRepository$$ExternalSyntheticLambda9, new EditGroupProfileRepository$$ExternalSyntheticLambda10(consumer));
    }

    public /* synthetic */ byte[] lambda$getCurrentAvatar$1() {
        RecipientId recipientId = getRecipientId();
        if (AvatarHelper.hasAvatar(this.context, recipientId)) {
            try {
                return StreamUtil.readFully(AvatarHelper.getAvatar(this.context, recipientId));
            } catch (IOException e) {
                Log.w(TAG, e);
            }
        }
        return null;
    }

    public /* synthetic */ String lambda$getCurrentDisplayName$2() {
        return Recipient.resolved(getRecipientId()).getDisplayName(this.context);
    }

    @Override // org.thoughtcrime.securesms.profiles.edit.EditProfileRepository
    public void getCurrentDisplayName(Consumer<String> consumer) {
        EditGroupProfileRepository$$ExternalSyntheticLambda6 editGroupProfileRepository$$ExternalSyntheticLambda6 = new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.profiles.edit.EditGroupProfileRepository$$ExternalSyntheticLambda6
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return EditGroupProfileRepository.$r8$lambda$CAaMRvv_t_Jv4rGNTuRLayXvwvg(EditGroupProfileRepository.this);
            }
        };
        Objects.requireNonNull(consumer);
        SimpleTask.run(editGroupProfileRepository$$ExternalSyntheticLambda6, new EditGroupProfileRepository$$ExternalSyntheticLambda1(consumer));
    }

    @Override // org.thoughtcrime.securesms.profiles.edit.EditProfileRepository
    public void getCurrentName(Consumer<String> consumer) {
        EditGroupProfileRepository$$ExternalSyntheticLambda11 editGroupProfileRepository$$ExternalSyntheticLambda11 = new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.profiles.edit.EditGroupProfileRepository$$ExternalSyntheticLambda11
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return EditGroupProfileRepository.$r8$lambda$U7nHNppLGQXyEofRmPHKnfahmfU(EditGroupProfileRepository.this);
            }
        };
        Objects.requireNonNull(consumer);
        SimpleTask.run(editGroupProfileRepository$$ExternalSyntheticLambda11, new EditGroupProfileRepository$$ExternalSyntheticLambda1(consumer));
    }

    public /* synthetic */ String lambda$getCurrentName$5() {
        RecipientId recipientId = getRecipientId();
        return (String) SignalDatabase.groups().getGroup(recipientId).map(new Function() { // from class: org.thoughtcrime.securesms.profiles.edit.EditGroupProfileRepository$$ExternalSyntheticLambda4
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return EditGroupProfileRepository.m2469$r8$lambda$LTsLRb_SkwplUrkSqyapiFYgXY((GroupDatabase.GroupRecord) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElseGet(new Supplier(Recipient.resolved(recipientId)) { // from class: org.thoughtcrime.securesms.profiles.edit.EditGroupProfileRepository$$ExternalSyntheticLambda5
            public final /* synthetic */ Recipient f$1;

            {
                this.f$1 = r2;
            }

            @Override // j$.util.function.Supplier
            public final Object get() {
                return EditGroupProfileRepository.m2468$r8$lambda$ASj0KN5bb4cqK1779p8yYMq28U(EditGroupProfileRepository.this, this.f$1);
            }
        });
    }

    public static /* synthetic */ String lambda$getCurrentName$3(GroupDatabase.GroupRecord groupRecord) {
        String title = groupRecord.getTitle();
        return title == null ? "" : title;
    }

    public /* synthetic */ String lambda$getCurrentName$4(Recipient recipient) {
        return recipient.getGroupName(this.context);
    }

    @Override // org.thoughtcrime.securesms.profiles.edit.EditProfileRepository
    public void getCurrentDescription(Consumer<String> consumer) {
        EditGroupProfileRepository$$ExternalSyntheticLambda0 editGroupProfileRepository$$ExternalSyntheticLambda0 = new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.profiles.edit.EditGroupProfileRepository$$ExternalSyntheticLambda0
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return EditGroupProfileRepository.m2467$r8$lambda$9DndVKI3vInQRkPFp3vT6WggVQ(EditGroupProfileRepository.this);
            }
        };
        Objects.requireNonNull(consumer);
        SimpleTask.run(editGroupProfileRepository$$ExternalSyntheticLambda0, new EditGroupProfileRepository$$ExternalSyntheticLambda1(consumer));
    }

    public /* synthetic */ String lambda$getCurrentDescription$6() {
        return (String) SignalDatabase.groups().getGroup(getRecipientId()).map(new Function() { // from class: org.thoughtcrime.securesms.profiles.edit.EditGroupProfileRepository$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((GroupDatabase.GroupRecord) obj).getDescription();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse("");
    }

    @Override // org.thoughtcrime.securesms.profiles.edit.EditProfileRepository
    public void uploadProfile(ProfileName profileName, String str, boolean z, String str2, boolean z2, byte[] bArr, boolean z3, Consumer<EditProfileRepository.UploadResult> consumer) {
        EditGroupProfileRepository$$ExternalSyntheticLambda12 editGroupProfileRepository$$ExternalSyntheticLambda12 = new SimpleTask.BackgroundTask(bArr, z3, str, z, str2, z2) { // from class: org.thoughtcrime.securesms.profiles.edit.EditGroupProfileRepository$$ExternalSyntheticLambda12
            public final /* synthetic */ byte[] f$1;
            public final /* synthetic */ boolean f$2;
            public final /* synthetic */ String f$3;
            public final /* synthetic */ boolean f$4;
            public final /* synthetic */ String f$5;
            public final /* synthetic */ boolean f$6;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return EditGroupProfileRepository.$r8$lambda$cnixCxagkUdQruFb07Y0cQ9Km38(EditGroupProfileRepository.this, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6);
            }
        };
        Objects.requireNonNull(consumer);
        SimpleTask.run(editGroupProfileRepository$$ExternalSyntheticLambda12, new EditGroupProfileRepository$$ExternalSyntheticLambda13(consumer));
    }

    public /* synthetic */ EditProfileRepository.UploadResult lambda$uploadProfile$7(byte[] bArr, boolean z, String str, boolean z2, String str2, boolean z3) {
        try {
            GroupManager.updateGroupDetails(this.context, this.groupId, bArr, z, str, z2, str2, z3);
            return EditProfileRepository.UploadResult.SUCCESS;
        } catch (IOException | GroupChangeException unused) {
            return EditProfileRepository.UploadResult.ERROR_IO;
        }
    }

    @Override // org.thoughtcrime.securesms.profiles.edit.EditProfileRepository
    public void getCurrentUsername(Consumer<Optional<String>> consumer) {
        consumer.accept(Optional.empty());
    }

    private RecipientId getRecipientId() {
        return SignalDatabase.recipients().getByGroupId(this.groupId).orElseThrow(new Supplier() { // from class: org.thoughtcrime.securesms.profiles.edit.EditGroupProfileRepository$$ExternalSyntheticLambda3
            @Override // j$.util.function.Supplier
            public final Object get() {
                return EditGroupProfileRepository.$r8$lambda$BxRE5I2dIGxjym5dQRi8S494cd8();
            }
        });
    }

    public static /* synthetic */ AssertionError lambda$getRecipientId$8() {
        return new AssertionError("Recipient ID for Group ID does not exist.");
    }
}
