package org.thoughtcrime.securesms.profiles.manage;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.groups.ParcelableGroupId;
import org.thoughtcrime.securesms.mediasend.Media;

/* loaded from: classes4.dex */
public class ManageProfileFragmentDirections {
    private ManageProfileFragmentDirections() {
    }

    public static NavDirections actionManageUsername() {
        return new ActionOnlyNavDirections(R.id.action_manageUsername);
    }

    public static NavDirections actionManageProfileName() {
        return new ActionOnlyNavDirections(R.id.action_manageProfileName);
    }

    public static NavDirections actionManageAbout() {
        return new ActionOnlyNavDirections(R.id.action_manageAbout);
    }

    public static ActionManageProfileFragmentToAvatarPicker actionManageProfileFragmentToAvatarPicker(ParcelableGroupId parcelableGroupId, Media media) {
        return new ActionManageProfileFragmentToAvatarPicker(parcelableGroupId, media);
    }

    public static NavDirections actionManageProfileFragmentToBadgeManageFragment() {
        return new ActionOnlyNavDirections(R.id.action_manageProfileFragment_to_badgeManageFragment);
    }

    /* loaded from: classes4.dex */
    public static class ActionManageProfileFragmentToAvatarPicker implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_manageProfileFragment_to_avatar_picker;
        }

        private ActionManageProfileFragmentToAvatarPicker(ParcelableGroupId parcelableGroupId, Media media) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("group_id", parcelableGroupId);
            hashMap.put("group_avatar_media", media);
        }

        public ActionManageProfileFragmentToAvatarPicker setGroupId(ParcelableGroupId parcelableGroupId) {
            this.arguments.put("group_id", parcelableGroupId);
            return this;
        }

        public ActionManageProfileFragmentToAvatarPicker setGroupAvatarMedia(Media media) {
            this.arguments.put("group_avatar_media", media);
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("group_id")) {
                ParcelableGroupId parcelableGroupId = (ParcelableGroupId) this.arguments.get("group_id");
                if (Parcelable.class.isAssignableFrom(ParcelableGroupId.class) || parcelableGroupId == null) {
                    bundle.putParcelable("group_id", (Parcelable) Parcelable.class.cast(parcelableGroupId));
                } else if (Serializable.class.isAssignableFrom(ParcelableGroupId.class)) {
                    bundle.putSerializable("group_id", (Serializable) Serializable.class.cast(parcelableGroupId));
                } else {
                    throw new UnsupportedOperationException(ParcelableGroupId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            if (this.arguments.containsKey("group_avatar_media")) {
                Media media = (Media) this.arguments.get("group_avatar_media");
                if (Parcelable.class.isAssignableFrom(Media.class) || media == null) {
                    bundle.putParcelable("group_avatar_media", (Parcelable) Parcelable.class.cast(media));
                } else if (Serializable.class.isAssignableFrom(Media.class)) {
                    bundle.putSerializable("group_avatar_media", (Serializable) Serializable.class.cast(media));
                } else {
                    throw new UnsupportedOperationException(Media.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            return bundle;
        }

        public ParcelableGroupId getGroupId() {
            return (ParcelableGroupId) this.arguments.get("group_id");
        }

        public Media getGroupAvatarMedia() {
            return (Media) this.arguments.get("group_avatar_media");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionManageProfileFragmentToAvatarPicker actionManageProfileFragmentToAvatarPicker = (ActionManageProfileFragmentToAvatarPicker) obj;
            if (this.arguments.containsKey("group_id") != actionManageProfileFragmentToAvatarPicker.arguments.containsKey("group_id")) {
                return false;
            }
            if (getGroupId() == null ? actionManageProfileFragmentToAvatarPicker.getGroupId() != null : !getGroupId().equals(actionManageProfileFragmentToAvatarPicker.getGroupId())) {
                return false;
            }
            if (this.arguments.containsKey("group_avatar_media") != actionManageProfileFragmentToAvatarPicker.arguments.containsKey("group_avatar_media")) {
                return false;
            }
            if (getGroupAvatarMedia() == null ? actionManageProfileFragmentToAvatarPicker.getGroupAvatarMedia() == null : getGroupAvatarMedia().equals(actionManageProfileFragmentToAvatarPicker.getGroupAvatarMedia())) {
                return getActionId() == actionManageProfileFragmentToAvatarPicker.getActionId();
            }
            return false;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((getGroupId() != null ? getGroupId().hashCode() : 0) + 31) * 31;
            if (getGroupAvatarMedia() != null) {
                i = getGroupAvatarMedia().hashCode();
            }
            return ((hashCode + i) * 31) + getActionId();
        }

        public String toString() {
            return "ActionManageProfileFragmentToAvatarPicker(actionId=" + getActionId() + "){groupId=" + getGroupId() + ", groupAvatarMedia=" + getGroupAvatarMedia() + "}";
        }
    }
}
