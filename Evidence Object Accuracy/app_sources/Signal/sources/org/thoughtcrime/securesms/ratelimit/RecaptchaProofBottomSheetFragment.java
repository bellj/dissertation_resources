package org.thoughtcrime.securesms.ratelimit;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.BottomSheetUtil;

/* loaded from: classes4.dex */
public final class RecaptchaProofBottomSheetFragment extends BottomSheetDialogFragment {
    private static final String TAG = Log.tag(RecaptchaProofBottomSheetFragment.class);

    public static void show(FragmentManager fragmentManager) {
        new RecaptchaProofBottomSheetFragment().show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        setStyle(0, R.style.Signal_DayNight_BottomSheet_Rounded);
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.recaptcha_required_bottom_sheet, viewGroup, false);
        inflate.findViewById(R.id.recaptcha_sheet_ok_button).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.ratelimit.RecaptchaProofBottomSheetFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                RecaptchaProofBottomSheetFragment.this.lambda$onCreateView$0(view);
            }
        });
        return inflate;
    }

    public /* synthetic */ void lambda$onCreateView$0(View view) {
        dismissAllowingStateLoss();
        startActivity(RecaptchaProofActivity.getIntent(requireContext()));
    }

    @Override // androidx.fragment.app.DialogFragment
    public void show(FragmentManager fragmentManager, String str) {
        String str2 = TAG;
        Log.i(str2, "Showing reCAPTCHA proof bottom sheet.");
        if (fragmentManager.findFragmentByTag(str) == null) {
            BottomSheetUtil.show(fragmentManager, str, this);
        } else {
            Log.i(str2, "Ignoring repeat show.");
        }
    }
}
