package org.thoughtcrime.securesms.ratelimit;

import android.content.Context;
import java.util.Set;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.JobUpdater;
import org.thoughtcrime.securesms.jobmanager.persistence.JobSpec;
import org.thoughtcrime.securesms.jobs.PushGroupSendJob;
import org.thoughtcrime.securesms.jobs.PushMediaSendJob;
import org.thoughtcrime.securesms.jobs.PushTextSendJob;

/* loaded from: classes.dex */
public final class RateLimitUtil {
    private static final String TAG = Log.tag(RateLimitUtil.class);

    private RateLimitUtil() {
    }

    public static void retryAllRateLimitedMessages(Context context) {
        Set<Long> allRateLimitedMessageIds = SignalDatabase.sms().getAllRateLimitedMessageIds();
        Set<Long> allRateLimitedMessageIds2 = SignalDatabase.mms().getAllRateLimitedMessageIds();
        if (!allRateLimitedMessageIds.isEmpty() || !allRateLimitedMessageIds2.isEmpty()) {
            String str = TAG;
            Log.i(str, "Retrying " + allRateLimitedMessageIds.size() + " sms records and " + allRateLimitedMessageIds2.size() + " mms records.");
            SignalDatabase.sms().clearRateLimitStatus(allRateLimitedMessageIds);
            SignalDatabase.mms().clearRateLimitStatus(allRateLimitedMessageIds2);
            ApplicationDependencies.getJobManager().update(new JobUpdater(allRateLimitedMessageIds, allRateLimitedMessageIds2) { // from class: org.thoughtcrime.securesms.ratelimit.RateLimitUtil$$ExternalSyntheticLambda0
                public final /* synthetic */ Set f$0;
                public final /* synthetic */ Set f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                @Override // org.thoughtcrime.securesms.jobmanager.JobUpdater
                public final JobSpec update(JobSpec jobSpec, Data.Serializer serializer) {
                    return RateLimitUtil.lambda$retryAllRateLimitedMessages$0(this.f$0, this.f$1, jobSpec, serializer);
                }
            });
        }
    }

    public static /* synthetic */ JobSpec lambda$retryAllRateLimitedMessages$0(Set set, Set set2, JobSpec jobSpec, Data.Serializer serializer) {
        Data deserialize = serializer.deserialize(jobSpec.getSerializedData());
        if (jobSpec.getFactoryKey().equals(PushTextSendJob.KEY) && set.contains(Long.valueOf(PushTextSendJob.getMessageId(deserialize)))) {
            return jobSpec.withNextRunAttemptTime(System.currentTimeMillis());
        }
        if (!jobSpec.getFactoryKey().equals(PushMediaSendJob.KEY) || !set2.contains(Long.valueOf(PushMediaSendJob.getMessageId(deserialize)))) {
            return (!jobSpec.getFactoryKey().equals(PushGroupSendJob.KEY) || !set2.contains(Long.valueOf(PushGroupSendJob.getMessageId(deserialize)))) ? jobSpec : jobSpec.withNextRunAttemptTime(System.currentTimeMillis());
        }
        return jobSpec.withNextRunAttemptTime(System.currentTimeMillis());
    }
}
