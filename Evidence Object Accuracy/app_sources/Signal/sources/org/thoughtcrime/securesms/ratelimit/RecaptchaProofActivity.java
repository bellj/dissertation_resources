package org.thoughtcrime.securesms.ratelimit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import java.io.IOException;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.ratelimit.RecaptchaProofActivity;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.MessageRecordUtil;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;
import org.whispersystems.signalservice.api.push.exceptions.PushNetworkException;

/* loaded from: classes4.dex */
public class RecaptchaProofActivity extends PassphraseRequiredActivity {
    private static final String RECAPTCHA_SCHEME;
    private static final String TAG = Log.tag(RecaptchaProofActivity.class);
    private final DynamicTheme dynamicTheme = new DynamicTheme();

    public static Intent getIntent(Context context) {
        return new Intent(context, RecaptchaProofActivity.class);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onPreCreate() {
        this.dynamicTheme.onCreate(this);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        super.onCreate(bundle, z);
        setContentView(R.layout.recaptcha_activity);
        requireSupportActionBar().setDisplayHomeAsUpEnabled(true);
        requireSupportActionBar().setTitle(R.string.RecaptchaProofActivity_complete_verification);
        WebView webView = (WebView) findViewById(R.id.recaptcha_webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.clearCache(true);
        webView.setBackgroundColor(0);
        webView.setWebViewClient(new WebViewClient() { // from class: org.thoughtcrime.securesms.ratelimit.RecaptchaProofActivity.1
            @Override // android.webkit.WebViewClient
            public boolean shouldOverrideUrlLoading(WebView webView2, String str) {
                if (str == null || !str.startsWith(RecaptchaProofActivity.RECAPTCHA_SCHEME)) {
                    return false;
                }
                RecaptchaProofActivity.this.handleToken(str.substring(16));
                return true;
            }
        });
        webView.loadUrl(BuildConfig.RECAPTCHA_PROOF_URL);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }

    @Override // android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        onBackPressed();
        return true;
    }

    public void handleToken(String str) {
        SimpleTask.run(new SimpleTask.BackgroundTask(str) { // from class: org.thoughtcrime.securesms.ratelimit.RecaptchaProofActivity$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return RecaptchaProofActivity.$r8$lambda$97t5f3GuORDHeXFYV8fNFmH0qXM(RecaptchaProofActivity.this, this.f$1);
            }
        }, new SimpleTask.ForegroundTask(SimpleProgressDialog.showDelayed(this, MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH, 500)) { // from class: org.thoughtcrime.securesms.ratelimit.RecaptchaProofActivity$$ExternalSyntheticLambda1
            public final /* synthetic */ SimpleProgressDialog.DismissibleDialog f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                RecaptchaProofActivity.$r8$lambda$JC1BfBwfDcouzBPvLnAqM3EZRRM(RecaptchaProofActivity.this, this.f$1, (RecaptchaProofActivity.TokenResult) obj);
            }
        });
    }

    public /* synthetic */ TokenResult lambda$handleToken$0(String str) {
        String challenge = SignalStore.rateLimit().getChallenge();
        if (Util.isEmpty(challenge)) {
            Log.w(TAG, "No challenge available?");
            return new TokenResult(true, false);
        }
        for (int i = 0; i < 3; i++) {
            try {
                ApplicationDependencies.getSignalServiceAccountManager().submitRateLimitRecaptchaChallenge(challenge, str);
                RateLimitUtil.retryAllRateLimitedMessages(this);
                Log.i(TAG, "Successfully completed reCAPTCHA.");
                return new TokenResult(true, true);
            } catch (PushNetworkException e) {
                try {
                    Log.w(TAG, "Network error during submission. Retrying.", e);
                } catch (IOException e2) {
                    Log.w(TAG, "Terminal failure during submission. Will clear state. May get a 428 later.", e2);
                    return new TokenResult(true, false);
                }
            }
        }
        return new TokenResult(false, false);
    }

    public /* synthetic */ void lambda$handleToken$1(SimpleProgressDialog.DismissibleDialog dismissibleDialog, TokenResult tokenResult) {
        dismissibleDialog.dismiss();
        if (tokenResult.clearState) {
            Log.i(TAG, "Considering the response sufficient to clear the slate.");
            SignalStore.rateLimit().onProofAccepted();
        }
        if (!tokenResult.success) {
            Log.w(TAG, "Response was not a true success.");
            Toast.makeText(this, (int) R.string.RecaptchaProofActivity_failed_to_submit, 1).show();
        }
        finish();
    }

    /* loaded from: classes4.dex */
    public static final class TokenResult {
        final boolean clearState;
        final boolean success;

        private TokenResult(boolean z, boolean z2) {
            this.clearState = z;
            this.success = z2;
        }
    }
}
