package org.thoughtcrime.securesms;

import android.os.Bundle;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import java.util.HashMap;
import org.thoughtcrime.securesms.help.HelpFragment;

/* loaded from: classes.dex */
public class AppSettingsDirections {
    private AppSettingsDirections() {
    }

    public static NavDirections actionDirectToBackupsPreferenceFragment() {
        return new ActionOnlyNavDirections(R.id.action_direct_to_backupsPreferenceFragment);
    }

    public static ActionDirectToHelpFragment actionDirectToHelpFragment() {
        return new ActionDirectToHelpFragment();
    }

    public static NavDirections actionDirectToEditProxyFragment() {
        return new ActionOnlyNavDirections(R.id.action_direct_to_editProxyFragment);
    }

    public static NavDirections actionDirectToNotificationsSettingsFragment() {
        return new ActionOnlyNavDirections(R.id.action_direct_to_notificationsSettingsFragment);
    }

    public static NavDirections actionDirectToChangeNumberFragment() {
        return new ActionOnlyNavDirections(R.id.action_direct_to_changeNumberFragment);
    }

    public static NavDirections actionDirectToSubscriptions() {
        return new ActionOnlyNavDirections(R.id.action_direct_to_subscriptions);
    }

    public static NavDirections actionDirectToManageDonations() {
        return new ActionOnlyNavDirections(R.id.action_direct_to_manageDonations);
    }

    public static NavDirections actionDirectToNotificationProfiles() {
        return new ActionOnlyNavDirections(R.id.action_direct_to_notificationProfiles);
    }

    public static ActionDirectToCreateNotificationProfiles actionDirectToCreateNotificationProfiles() {
        return new ActionDirectToCreateNotificationProfiles();
    }

    public static ActionDirectToNotificationProfileDetails actionDirectToNotificationProfileDetails(long j) {
        return new ActionDirectToNotificationProfileDetails(j);
    }

    public static NavDirections actionDirectToPrivacy() {
        return new ActionOnlyNavDirections(R.id.action_direct_to_privacy);
    }

    /* loaded from: classes.dex */
    public static class ActionDirectToHelpFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_direct_to_helpFragment;
        }

        private ActionDirectToHelpFragment() {
            this.arguments = new HashMap();
        }

        public ActionDirectToHelpFragment setStartCategoryIndex(int i) {
            this.arguments.put(HelpFragment.START_CATEGORY_INDEX, Integer.valueOf(i));
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey(HelpFragment.START_CATEGORY_INDEX)) {
                bundle.putInt(HelpFragment.START_CATEGORY_INDEX, ((Integer) this.arguments.get(HelpFragment.START_CATEGORY_INDEX)).intValue());
            } else {
                bundle.putInt(HelpFragment.START_CATEGORY_INDEX, 0);
            }
            return bundle;
        }

        public int getStartCategoryIndex() {
            return ((Integer) this.arguments.get(HelpFragment.START_CATEGORY_INDEX)).intValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionDirectToHelpFragment actionDirectToHelpFragment = (ActionDirectToHelpFragment) obj;
            return this.arguments.containsKey(HelpFragment.START_CATEGORY_INDEX) == actionDirectToHelpFragment.arguments.containsKey(HelpFragment.START_CATEGORY_INDEX) && getStartCategoryIndex() == actionDirectToHelpFragment.getStartCategoryIndex() && getActionId() == actionDirectToHelpFragment.getActionId();
        }

        public int hashCode() {
            return ((getStartCategoryIndex() + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionDirectToHelpFragment(actionId=" + getActionId() + "){startCategoryIndex=" + getStartCategoryIndex() + "}";
        }
    }

    /* loaded from: classes.dex */
    public static class ActionDirectToCreateNotificationProfiles implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_direct_to_createNotificationProfiles;
        }

        private ActionDirectToCreateNotificationProfiles() {
            this.arguments = new HashMap();
        }

        public ActionDirectToCreateNotificationProfiles setProfileId(long j) {
            this.arguments.put("profileId", Long.valueOf(j));
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("profileId")) {
                bundle.putLong("profileId", ((Long) this.arguments.get("profileId")).longValue());
            } else {
                bundle.putLong("profileId", -1);
            }
            return bundle;
        }

        public long getProfileId() {
            return ((Long) this.arguments.get("profileId")).longValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionDirectToCreateNotificationProfiles actionDirectToCreateNotificationProfiles = (ActionDirectToCreateNotificationProfiles) obj;
            return this.arguments.containsKey("profileId") == actionDirectToCreateNotificationProfiles.arguments.containsKey("profileId") && getProfileId() == actionDirectToCreateNotificationProfiles.getProfileId() && getActionId() == actionDirectToCreateNotificationProfiles.getActionId();
        }

        public int hashCode() {
            return ((((int) (getProfileId() ^ (getProfileId() >>> 32))) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionDirectToCreateNotificationProfiles(actionId=" + getActionId() + "){profileId=" + getProfileId() + "}";
        }
    }

    /* loaded from: classes.dex */
    public static class ActionDirectToNotificationProfileDetails implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_direct_to_notificationProfileDetails;
        }

        private ActionDirectToNotificationProfileDetails(long j) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("profileId", Long.valueOf(j));
        }

        public ActionDirectToNotificationProfileDetails setProfileId(long j) {
            this.arguments.put("profileId", Long.valueOf(j));
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("profileId")) {
                bundle.putLong("profileId", ((Long) this.arguments.get("profileId")).longValue());
            }
            return bundle;
        }

        public long getProfileId() {
            return ((Long) this.arguments.get("profileId")).longValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionDirectToNotificationProfileDetails actionDirectToNotificationProfileDetails = (ActionDirectToNotificationProfileDetails) obj;
            return this.arguments.containsKey("profileId") == actionDirectToNotificationProfileDetails.arguments.containsKey("profileId") && getProfileId() == actionDirectToNotificationProfileDetails.getProfileId() && getActionId() == actionDirectToNotificationProfileDetails.getActionId();
        }

        public int hashCode() {
            return ((((int) (getProfileId() ^ (getProfileId() >>> 32))) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionDirectToNotificationProfileDetails(actionId=" + getActionId() + "){profileId=" + getProfileId() + "}";
        }
    }
}
