package org.thoughtcrime.securesms;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import java.util.ArrayList;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contacts.SelectedContact;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes.dex */
public class PushContactSelectionActivity extends ContactSelectionActivity {
    public static final String KEY_SELECTED_RECIPIENTS;
    private static final String TAG = Log.tag(PushContactSelectionActivity.class);

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onSelectionChanged() {
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionActivity, org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        super.onCreate(bundle, z);
        initializeToolbar();
    }

    protected void initializeToolbar() {
        getToolbar().setNavigationIcon(R.drawable.ic_check_24);
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.PushContactSelectionActivity$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                PushContactSelectionActivity.$r8$lambda$YJK_7s4YetbznqXOZphqePUPFyA(PushContactSelectionActivity.this, view);
            }
        });
    }

    public /* synthetic */ void lambda$initializeToolbar$0(View view) {
        onFinishedSelection();
    }

    public final void onFinishedSelection() {
        Intent intent = getIntent();
        intent.putParcelableArrayListExtra(KEY_SELECTED_RECIPIENTS, new ArrayList<>(Stream.of(this.contactsFragment.getSelectedContacts()).map(new Function() { // from class: org.thoughtcrime.securesms.PushContactSelectionActivity$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return PushContactSelectionActivity.$r8$lambda$o_f4qa1H3PgRw2wS5UY6YmgIQpI(PushContactSelectionActivity.this, (SelectedContact) obj);
            }
        }).toList()));
        setResult(-1, intent);
        finish();
    }

    public /* synthetic */ RecipientId lambda$onFinishedSelection$1(SelectedContact selectedContact) {
        return selectedContact.getOrCreateRecipientId(this);
    }
}
