package org.thoughtcrime.securesms.registration;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.push.VerifyAccountResponse;

/* compiled from: VerifyAccountResponseProcessor.kt */
@Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\b\u0010\u0006\u001a\u00020\u0007H\u0016¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/registration/VerifyAccountResponseWithoutKbs;", "Lorg/thoughtcrime/securesms/registration/VerifyAccountResponseProcessor;", "response", "Lorg/whispersystems/signalservice/internal/ServiceResponse;", "Lorg/whispersystems/signalservice/internal/push/VerifyAccountResponse;", "(Lorg/whispersystems/signalservice/internal/ServiceResponse;)V", "isKbsLocked", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class VerifyAccountResponseWithoutKbs extends VerifyAccountResponseProcessor {
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public VerifyAccountResponseWithoutKbs(ServiceResponse<VerifyAccountResponse> serviceResponse) {
        super(serviceResponse, null);
        Intrinsics.checkNotNullParameter(serviceResponse, "response");
    }

    @Override // org.thoughtcrime.securesms.registration.VerifyAccountResponseProcessor
    public boolean isKbsLocked() {
        return registrationLock() && getLockedException().getBasicStorageCredentials() == null;
    }
}
