package org.thoughtcrime.securesms.registration;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.thoughtcrime.securesms.pin.TokenData;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.ServiceResponseProcessor;
import org.whispersystems.signalservice.internal.push.LockedException;
import org.whispersystems.signalservice.internal.push.VerifyAccountResponse;

/* compiled from: VerifyAccountResponseProcessor.kt */
@Metadata(d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003B\u0015\b\u0004\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\u000b\u001a\u00020\fH\u0016J\n\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016J\u0006\u0010\u000f\u001a\u00020\u0010J\b\u0010\u0011\u001a\u00020\fH&J\b\u0010\u0012\u001a\u00020\fH\u0016J\b\u0010\u0013\u001a\u00020\fH\u0016J\b\u0010\u0014\u001a\u00020\fH\u0016R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u0001\u0003\u0015\u0016\u0017¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/registration/VerifyAccountResponseProcessor;", "Lorg/whispersystems/signalservice/internal/ServiceResponseProcessor;", "Lorg/whispersystems/signalservice/internal/push/VerifyAccountResponse;", "Lorg/thoughtcrime/securesms/registration/VerifyProcessor;", "response", "Lorg/whispersystems/signalservice/internal/ServiceResponse;", "(Lorg/whispersystems/signalservice/internal/ServiceResponse;)V", "tokenData", "Lorg/thoughtcrime/securesms/pin/TokenData;", "getTokenData", "()Lorg/thoughtcrime/securesms/pin/TokenData;", "authorizationFailed", "", "getError", "", "getLockedException", "Lorg/whispersystems/signalservice/internal/push/LockedException;", "isKbsLocked", "isServerSentError", "rateLimit", "registrationLock", "Lorg/thoughtcrime/securesms/registration/VerifyAccountResponseWithoutKbs;", "Lorg/thoughtcrime/securesms/registration/VerifyAccountResponseWithSuccessfulKbs;", "Lorg/thoughtcrime/securesms/registration/VerifyAccountResponseWithFailedKbs;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class VerifyAccountResponseProcessor extends ServiceResponseProcessor<VerifyAccountResponse> implements VerifyProcessor {
    private final TokenData tokenData;

    public /* synthetic */ VerifyAccountResponseProcessor(ServiceResponse serviceResponse, DefaultConstructorMarker defaultConstructorMarker) {
        this(serviceResponse);
    }

    public abstract boolean isKbsLocked();

    private VerifyAccountResponseProcessor(ServiceResponse<VerifyAccountResponse> serviceResponse) {
        super(serviceResponse);
    }

    public TokenData getTokenData() {
        return this.tokenData;
    }

    @Override // org.whispersystems.signalservice.internal.ServiceResponseProcessor
    public boolean authorizationFailed() {
        return super.authorizationFailed();
    }

    @Override // org.whispersystems.signalservice.internal.ServiceResponseProcessor
    public boolean registrationLock() {
        return super.registrationLock();
    }

    @Override // org.whispersystems.signalservice.internal.ServiceResponseProcessor
    public boolean rateLimit() {
        return super.rateLimit();
    }

    @Override // org.whispersystems.signalservice.internal.ServiceResponseProcessor
    public Throwable getError() {
        return super.getError();
    }

    public final LockedException getLockedException() {
        Throwable error = getError();
        if (error != null) {
            return (LockedException) error;
        }
        throw new NullPointerException("null cannot be cast to non-null type org.whispersystems.signalservice.internal.push.LockedException");
    }

    @Override // org.thoughtcrime.securesms.registration.VerifyProcessor
    public boolean isServerSentError() {
        return getError() instanceof NonSuccessfulResponseCodeException;
    }
}
