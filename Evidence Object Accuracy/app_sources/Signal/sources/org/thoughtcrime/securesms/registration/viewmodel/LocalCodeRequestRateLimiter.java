package org.thoughtcrime.securesms.registration.viewmodel;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.thoughtcrime.securesms.registration.VerifyAccountRepository;

/* loaded from: classes4.dex */
public final class LocalCodeRequestRateLimiter implements Parcelable {
    public static final Parcelable.Creator<LocalCodeRequestRateLimiter> CREATOR = new Parcelable.Creator<LocalCodeRequestRateLimiter>() { // from class: org.thoughtcrime.securesms.registration.viewmodel.LocalCodeRequestRateLimiter.1
        @Override // android.os.Parcelable.Creator
        public LocalCodeRequestRateLimiter createFromParcel(Parcel parcel) {
            long readLong = parcel.readLong();
            int readInt = parcel.readInt();
            LocalCodeRequestRateLimiter localCodeRequestRateLimiter = new LocalCodeRequestRateLimiter(readLong);
            for (int i = 0; i < readInt; i++) {
                VerifyAccountRepository.Mode mode = VerifyAccountRepository.Mode.values()[parcel.readInt()];
                String readString = parcel.readString();
                long readLong2 = parcel.readLong();
                Map map = localCodeRequestRateLimiter.dataMap;
                Objects.requireNonNull(readString);
                map.put(mode, new Data(readString, readLong2));
            }
            return localCodeRequestRateLimiter;
        }

        @Override // android.os.Parcelable.Creator
        public LocalCodeRequestRateLimiter[] newArray(int i) {
            return new LocalCodeRequestRateLimiter[i];
        }
    };
    private final Map<VerifyAccountRepository.Mode, Data> dataMap = new HashMap();
    private final long timePeriod;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public LocalCodeRequestRateLimiter(long j) {
        this.timePeriod = j;
    }

    public boolean canRequest(VerifyAccountRepository.Mode mode, String str, long j) {
        Data data = this.dataMap.get(mode);
        return data == null || !data.limited(str, j);
    }

    public void onSuccessfulRequest(VerifyAccountRepository.Mode mode, String str, long j) {
        this.dataMap.put(mode, new Data(str, j + this.timePeriod));
    }

    public void onUnsuccessfulRequest() {
        this.dataMap.clear();
    }

    /* loaded from: classes4.dex */
    public static class Data {
        final String e164Number;
        final long limitedUntil;

        Data(String str, long j) {
            this.e164Number = str;
            this.limitedUntil = j;
        }

        boolean limited(String str, long j) {
            return this.e164Number.equals(str) && j < this.limitedUntil;
        }
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.timePeriod);
        parcel.writeInt(this.dataMap.size());
        for (Map.Entry<VerifyAccountRepository.Mode, Data> entry : this.dataMap.entrySet()) {
            parcel.writeInt(entry.getKey().ordinal());
            parcel.writeString(entry.getValue().e164Number);
            parcel.writeLong(entry.getValue().limitedUntil);
        }
    }
}
