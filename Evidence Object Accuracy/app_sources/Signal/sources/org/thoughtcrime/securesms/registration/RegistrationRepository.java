package org.thoughtcrime.securesms.registration;

import android.app.Application;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import j$.util.Optional;
import java.io.IOException;
import java.util.concurrent.Callable;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.state.SignalProtocolStore;
import org.signal.libsignal.protocol.util.KeyHelper;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.thoughtcrime.securesms.crypto.PreKeyUtil;
import org.thoughtcrime.securesms.crypto.ProfileKeyUtil;
import org.thoughtcrime.securesms.crypto.SenderKeyUtil;
import org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore;
import org.thoughtcrime.securesms.crypto.storage.SignalServiceAccountDataStoreImpl;
import org.thoughtcrime.securesms.database.IdentityDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobs.DirectoryRefreshJob;
import org.thoughtcrime.securesms.jobs.RotateCertificateJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.pin.PinState;
import org.thoughtcrime.securesms.push.AccountManagerFactory;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.registration.VerifyAccountRepository;
import org.thoughtcrime.securesms.service.DirectoryRefreshListener;
import org.thoughtcrime.securesms.service.RotateSignedPreKeyListener;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.KbsPinData;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.push.ACI;
import org.whispersystems.signalservice.api.push.PNI;
import org.whispersystems.signalservice.api.push.ServiceIdType;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.push.VerifyAccountResponse;

/* loaded from: classes4.dex */
public final class RegistrationRepository {
    private static final String TAG = Log.tag(RegistrationRepository.class);
    private final Application context;

    public RegistrationRepository(Application application) {
        this.context = application;
    }

    public int getRegistrationId() {
        int registrationId = SignalStore.account().getRegistrationId();
        if (registrationId != 0) {
            return registrationId;
        }
        int generateRegistrationId = KeyHelper.generateRegistrationId(false);
        SignalStore.account().setRegistrationId(generateRegistrationId);
        return generateRegistrationId;
    }

    public ProfileKey getProfileKey(String str) {
        ProfileKey findExistingProfileKey = findExistingProfileKey(str);
        if (findExistingProfileKey != null) {
            return findExistingProfileKey;
        }
        ProfileKey createNew = ProfileKeyUtil.createNew();
        Log.i(TAG, "No profile key found, created a new one");
        return createNew;
    }

    public Single<ServiceResponse<VerifyAccountResponse>> registerAccountWithoutRegistrationLock(RegistrationData registrationData, VerifyAccountResponse verifyAccountResponse) {
        return registerAccount(registrationData, verifyAccountResponse, null, null);
    }

    public Single<ServiceResponse<VerifyAccountResponse>> registerAccountWithRegistrationLock(RegistrationData registrationData, VerifyAccountRepository.VerifyAccountWithRegistrationLockResponse verifyAccountWithRegistrationLockResponse, String str) {
        return registerAccount(registrationData, verifyAccountWithRegistrationLockResponse.getVerifyAccountResponse(), str, verifyAccountWithRegistrationLockResponse.getKbsData());
    }

    private Single<ServiceResponse<VerifyAccountResponse>> registerAccount(RegistrationData registrationData, VerifyAccountResponse verifyAccountResponse, String str, KbsPinData kbsPinData) {
        return Single.fromCallable(new Callable(registrationData, verifyAccountResponse, str, kbsPinData) { // from class: org.thoughtcrime.securesms.registration.RegistrationRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ RegistrationData f$1;
            public final /* synthetic */ VerifyAccountResponse f$2;
            public final /* synthetic */ String f$3;
            public final /* synthetic */ KbsPinData f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return RegistrationRepository.m2569$r8$lambda$LB9xsWuhsqEjc1mG77D5yHzelU(RegistrationRepository.this, this.f$1, this.f$2, this.f$3, this.f$4);
            }
        }).subscribeOn(Schedulers.io());
    }

    public /* synthetic */ ServiceResponse lambda$registerAccount$0(RegistrationData registrationData, VerifyAccountResponse verifyAccountResponse, String str, KbsPinData kbsPinData) throws Exception {
        try {
            registerAccountInternal(registrationData, verifyAccountResponse, str, kbsPinData);
            JobManager jobManager = ApplicationDependencies.getJobManager();
            jobManager.add(new DirectoryRefreshJob(false));
            jobManager.add(new RotateCertificateJob());
            DirectoryRefreshListener.schedule(this.context);
            RotateSignedPreKeyListener.schedule(this.context);
            return ServiceResponse.forResult(verifyAccountResponse, 200, null);
        } catch (IOException e) {
            return ServiceResponse.forUnknownError(e);
        }
    }

    private void registerAccountInternal(RegistrationData registrationData, VerifyAccountResponse verifyAccountResponse, String str, KbsPinData kbsPinData) throws IOException {
        ACI parseOrThrow = ACI.parseOrThrow(verifyAccountResponse.getUuid());
        PNI parseOrThrow2 = PNI.parseOrThrow(verifyAccountResponse.getPni());
        boolean isStorageCapable = verifyAccountResponse.isStorageCapable();
        SignalStore.account().setAci(parseOrThrow);
        SignalStore.account().setPni(parseOrThrow2);
        ApplicationDependencies.getProtocolStore().aci().sessions().archiveAllSessions();
        ApplicationDependencies.getProtocolStore().pni().sessions().archiveAllSessions();
        SenderKeyUtil.clearAllState();
        SignalServiceAccountManager createAuthenticated = AccountManagerFactory.createAuthenticated(this.context, parseOrThrow, parseOrThrow2, registrationData.getE164(), 1, registrationData.getPassword());
        SignalServiceAccountDataStoreImpl aci = ApplicationDependencies.getProtocolStore().aci();
        SignalServiceAccountDataStoreImpl pni = ApplicationDependencies.getProtocolStore().pni();
        generateAndRegisterPreKeys(ServiceIdType.ACI, createAuthenticated, aci, SignalStore.account().aciPreKeys());
        generateAndRegisterPreKeys(ServiceIdType.PNI, createAuthenticated, pni, SignalStore.account().pniPreKeys());
        if (registrationData.isFcm()) {
            createAuthenticated.setGcmId(Optional.ofNullable(registrationData.getFcmToken()));
        }
        RecipientDatabase recipients = SignalDatabase.recipients();
        RecipientId id = Recipient.externalPush(new SignalServiceAddress(parseOrThrow, registrationData.getE164())).getId();
        recipients.setProfileSharing(id, true);
        recipients.markRegisteredOrThrow(id, parseOrThrow);
        recipients.setPni(id, parseOrThrow2);
        recipients.setProfileKey(id, registrationData.getProfileKey());
        ApplicationDependencies.getRecipientCache().clearSelf();
        SignalStore.account().setE164(registrationData.getE164());
        SignalStore.account().setFcmToken(registrationData.getFcmToken());
        SignalStore.account().setFcmEnabled(registrationData.isFcm());
        long currentTimeMillis = System.currentTimeMillis();
        saveOwnIdentityKey(id, aci, currentTimeMillis);
        saveOwnIdentityKey(id, pni, currentTimeMillis);
        SignalStore.account().setServicePassword(registrationData.getPassword());
        SignalStore.account().setRegistered(true);
        TextSecurePreferences.setPromptedPushRegistration(this.context, true);
        TextSecurePreferences.setUnauthorizedReceived(this.context, false);
        PinState.onRegistration(this.context, kbsPinData, str, isStorageCapable);
    }

    private void generateAndRegisterPreKeys(ServiceIdType serviceIdType, SignalServiceAccountManager signalServiceAccountManager, SignalProtocolStore signalProtocolStore, PreKeyMetadataStore preKeyMetadataStore) throws IOException {
        signalServiceAccountManager.setPreKeys(serviceIdType, signalProtocolStore.getIdentityKeyPair().getPublicKey(), PreKeyUtil.generateAndStoreSignedPreKey(signalProtocolStore, preKeyMetadataStore, true), PreKeyUtil.generateAndStoreOneTimePreKeys(signalProtocolStore, preKeyMetadataStore));
        preKeyMetadataStore.setSignedPreKeyRegistered(true);
    }

    private void saveOwnIdentityKey(RecipientId recipientId, SignalServiceAccountDataStoreImpl signalServiceAccountDataStoreImpl, long j) {
        signalServiceAccountDataStoreImpl.identities().saveIdentityWithoutSideEffects(recipientId, signalServiceAccountDataStoreImpl.getIdentityKeyPair().getPublicKey(), IdentityDatabase.VerifiedStatus.VERIFIED, true, j, true);
    }

    private static ProfileKey findExistingProfileKey(String str) {
        Optional<RecipientId> byE164 = SignalDatabase.recipients().getByE164(str);
        if (byE164.isPresent()) {
            return ProfileKeyUtil.profileKeyOrNull(Recipient.resolved(byE164.get()).getProfileKey());
        }
        return null;
    }
}
