package org.thoughtcrime.securesms.registration.fragments;

import androidx.lifecycle.ViewModelProvider;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel;
import org.thoughtcrime.securesms.registration.viewmodel.RegistrationViewModel;

/* loaded from: classes4.dex */
public class AccountLockedFragment extends BaseAccountLockedFragment {
    public AccountLockedFragment() {
        super(R.layout.account_locked_fragment);
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseAccountLockedFragment
    protected BaseRegistrationViewModel getViewModel() {
        return (BaseRegistrationViewModel) new ViewModelProvider(requireActivity()).get(RegistrationViewModel.class);
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseAccountLockedFragment
    protected void onNext() {
        requireActivity().finish();
    }
}
