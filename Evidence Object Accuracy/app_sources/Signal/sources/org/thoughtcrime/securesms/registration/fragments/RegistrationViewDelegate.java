package org.thoughtcrime.securesms.registration.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.widget.Toast;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.TranslationDetection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.logsubmit.SubmitDebugLogActivity;
import org.thoughtcrime.securesms.phonenumbers.PhoneNumberFormatter;
import org.thoughtcrime.securesms.util.SpanUtil;

/* compiled from: RegistrationViewDelegate.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0007J2\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\t2\b\b\u0001\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u000fH\u0007¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/registration/fragments/RegistrationViewDelegate;", "", "()V", "setDebugLogSubmitMultiTapView", "", "view", "Landroid/view/View;", "showConfirmNumberDialogIfTranslated", "context", "Landroid/content/Context;", "firstMessageLine", "", "e164number", "", "onConfirmed", "Ljava/lang/Runnable;", "onEditNumber", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class RegistrationViewDelegate {
    public static final RegistrationViewDelegate INSTANCE = new RegistrationViewDelegate();

    private RegistrationViewDelegate() {
    }

    @JvmStatic
    public static final void setDebugLogSubmitMultiTapView(View view) {
        if (view != null) {
            view.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.registration.fragments.RegistrationViewDelegate$setDebugLogSubmitMultiTapView$1
                private final int DEBUG_TAP_ANNOUNCE = 4;
                private final int DEBUG_TAP_TARGET = 8;
                private int debugTapCounter;

                @Override // android.view.View.OnClickListener
                public void onClick(View view2) {
                    Intrinsics.checkNotNullParameter(view2, "view");
                    int i = this.debugTapCounter + 1;
                    this.debugTapCounter = i;
                    int i2 = this.DEBUG_TAP_TARGET;
                    if (i >= i2) {
                        view2.getContext().startActivity(new Intent(view2.getContext(), SubmitDebugLogActivity.class));
                    } else if (i >= this.DEBUG_TAP_ANNOUNCE) {
                        int i3 = i2 - i;
                        Toast.makeText(view2.getContext(), view2.getContext().getResources().getQuantityString(R.plurals.RegistrationActivity_debug_log_hint, i3, Integer.valueOf(i3)), 0).show();
                    }
                }
            });
        }
    }

    @JvmStatic
    public static final void showConfirmNumberDialogIfTranslated(Context context, int i, String str, Runnable runnable, Runnable runnable2) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(str, "e164number");
        Intrinsics.checkNotNullParameter(runnable, "onConfirmed");
        Intrinsics.checkNotNullParameter(runnable2, "onEditNumber");
        if (new TranslationDetection(context).textExistsInUsersLanguage(i, R.string.RegistrationActivity_is_your_phone_number_above_correct, R.string.RegistrationActivity_edit_number)) {
            SpannableStringBuilder append = new SpannableStringBuilder().append((CharSequence) context.getString(i)).append((CharSequence) "\n\n").append(SpanUtil.bold(PhoneNumberFormatter.prettyPrint(str))).append((CharSequence) "\n\n").append((CharSequence) context.getString(R.string.RegistrationActivity_is_your_phone_number_above_correct));
            Intrinsics.checkNotNullExpressionValue(append, "SpannableStringBuilder()…ne_number_above_correct))");
            new MaterialAlertDialogBuilder(context).setMessage((CharSequence) append).setPositiveButton(17039370, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(runnable) { // from class: org.thoughtcrime.securesms.registration.fragments.RegistrationViewDelegate$$ExternalSyntheticLambda0
                public final /* synthetic */ Runnable f$0;

                {
                    this.f$0 = r1;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    RegistrationViewDelegate.m2595showConfirmNumberDialogIfTranslated$lambda0(this.f$0, dialogInterface, i2);
                }
            }).setNegativeButton(R.string.RegistrationActivity_edit_number, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(runnable2) { // from class: org.thoughtcrime.securesms.registration.fragments.RegistrationViewDelegate$$ExternalSyntheticLambda1
                public final /* synthetic */ Runnable f$0;

                {
                    this.f$0 = r1;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    RegistrationViewDelegate.m2596showConfirmNumberDialogIfTranslated$lambda1(this.f$0, dialogInterface, i2);
                }
            }).show();
            return;
        }
        runnable.run();
    }

    /* renamed from: showConfirmNumberDialogIfTranslated$lambda-0 */
    public static final void m2595showConfirmNumberDialogIfTranslated$lambda0(Runnable runnable, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(runnable, "$onConfirmed");
        runnable.run();
    }

    /* renamed from: showConfirmNumberDialogIfTranslated$lambda-1 */
    public static final void m2596showConfirmNumberDialogIfTranslated$lambda1(Runnable runnable, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(runnable, "$onEditNumber");
        runnable.run();
    }
}
