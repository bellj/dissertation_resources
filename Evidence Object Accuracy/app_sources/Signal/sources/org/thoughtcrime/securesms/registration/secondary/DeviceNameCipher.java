package org.thoughtcrime.securesms.registration.secondary;

import com.google.protobuf.ByteString;
import java.nio.charset.Charset;
import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import kotlin.Metadata;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.RangesKt___RangesKt;
import org.signal.libsignal.protocol.IdentityKeyPair;
import org.signal.libsignal.protocol.ecc.Curve;
import org.signal.libsignal.protocol.ecc.ECKeyPair;
import org.thoughtcrime.securesms.devicelist.DeviceNameProtos;

/* compiled from: DeviceNameCipher.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0012\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\u0006H\u0002J\u0018\u0010\t\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u0006H\u0002J\u0018\u0010\u000b\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\rH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/registration/secondary/DeviceNameCipher;", "", "()V", "SYNTHETIC_IV_LENGTH", "", "computeCipherKey", "", "masterSecret", "syntheticIv", "computeSyntheticIv", "plaintext", "encryptDeviceName", "identityKeyPair", "Lorg/signal/libsignal/protocol/IdentityKeyPair;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DeviceNameCipher {
    public static final DeviceNameCipher INSTANCE = new DeviceNameCipher();
    private static final int SYNTHETIC_IV_LENGTH;

    private DeviceNameCipher() {
    }

    @JvmStatic
    public static final byte[] encryptDeviceName(byte[] bArr, IdentityKeyPair identityKeyPair) {
        Intrinsics.checkNotNullParameter(bArr, "plaintext");
        Intrinsics.checkNotNullParameter(identityKeyPair, "identityKeyPair");
        ECKeyPair generateKeyPair = Curve.generateKeyPair();
        Intrinsics.checkNotNullExpressionValue(generateKeyPair, "generateKeyPair()");
        byte[] calculateAgreement = Curve.calculateAgreement(identityKeyPair.getPublicKey().getPublicKey(), generateKeyPair.getPrivateKey());
        Intrinsics.checkNotNullExpressionValue(calculateAgreement, "calculateAgreement(ident…emeralKeyPair.privateKey)");
        DeviceNameCipher deviceNameCipher = INSTANCE;
        byte[] computeSyntheticIv = deviceNameCipher.computeSyntheticIv(calculateAgreement, bArr);
        byte[] computeCipherKey = deviceNameCipher.computeCipherKey(calculateAgreement, computeSyntheticIv);
        Cipher instance = Cipher.getInstance("AES/CTR/NoPadding");
        instance.init(1, new SecretKeySpec(computeCipherKey, "AES"), new IvParameterSpec(new byte[16]));
        byte[] byteArray = DeviceNameProtos.DeviceName.newBuilder().setEphemeralPublic(ByteString.copyFrom(generateKeyPair.getPublicKey().serialize())).setSyntheticIv(ByteString.copyFrom(computeSyntheticIv)).setCiphertext(ByteString.copyFrom(instance.doFinal(bArr))).build().toByteArray();
        Intrinsics.checkNotNullExpressionValue(byteArray, "newBuilder()\n      .setE…ld()\n      .toByteArray()");
        return byteArray;
    }

    private final byte[] computeCipherKey(byte[] bArr, byte[] bArr2) {
        Charset forName = Charset.forName("UTF-8");
        Intrinsics.checkNotNullExpressionValue(forName, "forName(\"UTF-8\")");
        byte[] bytes = "cipher".getBytes(forName);
        Intrinsics.checkNotNullExpressionValue(bytes, "this as java.lang.String).getBytes(charset)");
        Mac instance = Mac.getInstance("HmacSHA256");
        instance.init(new SecretKeySpec(bArr, "HmacSHA256"));
        byte[] doFinal = instance.doFinal(bytes);
        Intrinsics.checkNotNullExpressionValue(doFinal, "keyMac.doFinal(input)");
        Mac instance2 = Mac.getInstance("HmacSHA256");
        instance2.init(new SecretKeySpec(doFinal, "HmacSHA256"));
        byte[] doFinal2 = instance2.doFinal(bArr2);
        Intrinsics.checkNotNullExpressionValue(doFinal2, "cipherMac.doFinal(syntheticIv)");
        return doFinal2;
    }

    private final byte[] computeSyntheticIv(byte[] bArr, byte[] bArr2) {
        Charset forName = Charset.forName("UTF-8");
        Intrinsics.checkNotNullExpressionValue(forName, "forName(\"UTF-8\")");
        byte[] bytes = "auth".getBytes(forName);
        Intrinsics.checkNotNullExpressionValue(bytes, "this as java.lang.String).getBytes(charset)");
        Mac instance = Mac.getInstance("HmacSHA256");
        instance.init(new SecretKeySpec(bArr, "HmacSHA256"));
        byte[] doFinal = instance.doFinal(bytes);
        Intrinsics.checkNotNullExpressionValue(doFinal, "keyMac.doFinal(input)");
        Mac instance2 = Mac.getInstance("HmacSHA256");
        instance2.init(new SecretKeySpec(doFinal, "HmacSHA256"));
        byte[] doFinal2 = instance2.doFinal(bArr2);
        Intrinsics.checkNotNullExpressionValue(doFinal2, "ivMac.doFinal(plaintext)");
        return ArraysKt___ArraysKt.sliceArray(doFinal2, RangesKt___RangesKt.until(0, 16));
    }
}
