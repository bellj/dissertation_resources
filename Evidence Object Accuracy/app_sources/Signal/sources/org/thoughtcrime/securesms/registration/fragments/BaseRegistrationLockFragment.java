package org.thoughtcrime.securesms.registration.fragments;

import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.lifecycle.Observer;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.functions.Consumer;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.lock.v2.PinKeyboardType;
import org.thoughtcrime.securesms.pin.TokenData;
import org.thoughtcrime.securesms.registration.VerifyCodeWithRegistrationLockResponseProcessor;
import org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;

/* loaded from: classes4.dex */
public abstract class BaseRegistrationLockFragment extends LoggingFragment {
    private static final int MINIMUM_PIN_LENGTH;
    private static final String TAG = Log.tag(BaseRegistrationLockFragment.class);
    private final LifecycleDisposable disposables = new LifecycleDisposable();
    private TextView errorLabel;
    private View forgotPin;
    private TextView keyboardToggle;
    protected CircularProgressMaterialButton pinButton;
    private EditText pinEntry;
    private long timeRemaining;
    private BaseRegistrationViewModel viewModel;

    protected abstract BaseRegistrationViewModel getViewModel();

    protected abstract void handleSuccessfulPinEntry(String str);

    protected abstract void navigateToAccountLocked();

    protected abstract void sendEmailToSupport();

    public BaseRegistrationLockFragment(int i) {
        super(i);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        RegistrationViewDelegate.setDebugLogSubmitMultiTapView(view.findViewById(R.id.kbs_lock_pin_title));
        this.pinEntry = (EditText) view.findViewById(R.id.kbs_lock_pin_input);
        this.pinButton = (CircularProgressMaterialButton) view.findViewById(R.id.kbs_lock_pin_confirm);
        this.errorLabel = (TextView) view.findViewById(R.id.kbs_lock_pin_input_label);
        this.keyboardToggle = (TextView) view.findViewById(R.id.kbs_lock_keyboard_toggle);
        this.forgotPin = view.findViewById(R.id.kbs_lock_forgot_pin);
        this.timeRemaining = RegistrationLockFragmentArgs.fromBundle(requireArguments()).getTimeRemaining();
        this.forgotPin.setVisibility(8);
        this.forgotPin.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseRegistrationLockFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                BaseRegistrationLockFragment.this.lambda$onViewCreated$0(view2);
            }
        });
        this.pinEntry.setImeOptions(6);
        this.pinEntry.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseRegistrationLockFragment$$ExternalSyntheticLambda3
            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                return BaseRegistrationLockFragment.this.lambda$onViewCreated$1(textView, i, keyEvent);
            }
        });
        enableAndFocusPinEntry();
        this.pinButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseRegistrationLockFragment$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                BaseRegistrationLockFragment.this.lambda$onViewCreated$2(view2);
            }
        });
        this.keyboardToggle.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseRegistrationLockFragment$$ExternalSyntheticLambda5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                BaseRegistrationLockFragment.this.lambda$onViewCreated$3(view2);
            }
        });
        this.keyboardToggle.setText(resolveKeyboardToggleText(getPinEntryKeyboardType().getOther()));
        this.disposables.bindTo(getViewLifecycleOwner().getLifecycle());
        BaseRegistrationViewModel viewModel = getViewModel();
        this.viewModel = viewModel;
        viewModel.getLockedTimeRemaining().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseRegistrationLockFragment$$ExternalSyntheticLambda6
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                BaseRegistrationLockFragment.this.lambda$onViewCreated$4((Long) obj);
            }
        });
        TokenData keyBackupCurrentToken = this.viewModel.getKeyBackupCurrentToken();
        if (keyBackupCurrentToken != null) {
            int triesRemaining = keyBackupCurrentToken.getTriesRemaining();
            if (triesRemaining <= 3) {
                new MaterialAlertDialogBuilder(requireContext()).setTitle(R.string.RegistrationLockFragment__not_many_tries_left).setMessage((CharSequence) getTriesRemainingDialogMessage(triesRemaining, getLockoutDays(this.timeRemaining))).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).setNeutralButton(R.string.PinRestoreEntryFragment_contact_support, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseRegistrationLockFragment$$ExternalSyntheticLambda7
                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        BaseRegistrationLockFragment.this.lambda$onViewCreated$5(dialogInterface, i);
                    }
                }).show();
            }
            if (triesRemaining < 5) {
                this.errorLabel.setText(requireContext().getResources().getQuantityString(R.plurals.RegistrationLockFragment__d_attempts_remaining, triesRemaining, Integer.valueOf(triesRemaining)));
            }
        }
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        handleForgottenPin(this.timeRemaining);
    }

    public /* synthetic */ boolean lambda$onViewCreated$1(TextView textView, int i, KeyEvent keyEvent) {
        if (i != 6) {
            return false;
        }
        ViewUtil.hideKeyboard(requireContext(), textView);
        handlePinEntry();
        return true;
    }

    public /* synthetic */ void lambda$onViewCreated$2(View view) {
        ViewUtil.hideKeyboard(requireContext(), this.pinEntry);
        handlePinEntry();
    }

    public /* synthetic */ void lambda$onViewCreated$3(View view) {
        PinKeyboardType pinEntryKeyboardType = getPinEntryKeyboardType();
        updateKeyboard(pinEntryKeyboardType.getOther());
        this.keyboardToggle.setText(resolveKeyboardToggleText(pinEntryKeyboardType));
    }

    public /* synthetic */ void lambda$onViewCreated$4(Long l) {
        this.timeRemaining = l.longValue();
    }

    public /* synthetic */ void lambda$onViewCreated$5(DialogInterface dialogInterface, int i) {
        sendEmailToSupport();
    }

    private String getTriesRemainingDialogMessage(int i, int i2) {
        Resources resources = requireContext().getResources();
        String quantityString = resources.getQuantityString(R.plurals.RegistrationLockFragment__you_have_d_attempts_remaining, i, Integer.valueOf(i));
        String quantityString2 = resources.getQuantityString(R.plurals.RegistrationLockFragment__if_you_run_out_of_attempts_your_account_will_be_locked_for_d_days, i2, Integer.valueOf(i2));
        return quantityString + " " + quantityString2;
    }

    public PinKeyboardType getPinEntryKeyboardType() {
        return (this.pinEntry.getInputType() & 15) == 2 ? PinKeyboardType.NUMERIC : PinKeyboardType.ALPHA_NUMERIC;
    }

    private void handlePinEntry() {
        this.pinEntry.setEnabled(false);
        String obj = this.pinEntry.getText().toString();
        int length = obj.replace(" ", "").length();
        if (length == 0) {
            Toast.makeText(requireContext(), (int) R.string.RegistrationActivity_you_must_enter_your_registration_lock_PIN, 1).show();
            enableAndFocusPinEntry();
        } else if (length < 4) {
            Toast.makeText(requireContext(), getString(R.string.RegistrationActivity_your_pin_has_at_least_d_digits_or_characters, 4), 1).show();
            enableAndFocusPinEntry();
        } else {
            this.pinButton.setSpinning();
            this.disposables.add(this.viewModel.verifyCodeAndRegisterAccountWithRegistrationLock(obj).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer(obj) { // from class: org.thoughtcrime.securesms.registration.fragments.BaseRegistrationLockFragment$$ExternalSyntheticLambda1
                public final /* synthetic */ String f$1;

                {
                    this.f$1 = r2;
                }

                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj2) {
                    BaseRegistrationLockFragment.this.lambda$handlePinEntry$6(this.f$1, (VerifyCodeWithRegistrationLockResponseProcessor) obj2);
                }
            }));
        }
    }

    public /* synthetic */ void lambda$handlePinEntry$6(String str, VerifyCodeWithRegistrationLockResponseProcessor verifyCodeWithRegistrationLockResponseProcessor) throws Throwable {
        if (verifyCodeWithRegistrationLockResponseProcessor.hasResult()) {
            handleSuccessfulPinEntry(str);
        } else if (verifyCodeWithRegistrationLockResponseProcessor.wrongPin()) {
            onIncorrectKbsRegistrationLockPin(verifyCodeWithRegistrationLockResponseProcessor.getToken());
        } else if (verifyCodeWithRegistrationLockResponseProcessor.isKbsLocked() || verifyCodeWithRegistrationLockResponseProcessor.registrationLock()) {
            onKbsAccountLocked();
        } else if (verifyCodeWithRegistrationLockResponseProcessor.rateLimit()) {
            onRateLimited();
        } else {
            Log.w(TAG, "Unable to verify code with registration lock", verifyCodeWithRegistrationLockResponseProcessor.getError());
            onError();
        }
    }

    public void onIncorrectKbsRegistrationLockPin(TokenData tokenData) {
        this.pinButton.cancelSpinning();
        this.pinEntry.getText().clear();
        enableAndFocusPinEntry();
        this.viewModel.setKeyBackupTokenData(tokenData);
        int triesRemaining = tokenData.getTriesRemaining();
        if (triesRemaining == 0) {
            Log.w(TAG, "Account locked. User out of attempts on KBS.");
            onAccountLocked();
            return;
        }
        if (triesRemaining == 3) {
            new MaterialAlertDialogBuilder(requireContext()).setTitle(R.string.RegistrationLockFragment__incorrect_pin).setMessage((CharSequence) getTriesRemainingDialogMessage(triesRemaining, getLockoutDays(this.timeRemaining))).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).show();
        }
        if (triesRemaining > 5) {
            this.errorLabel.setText(R.string.RegistrationLockFragment__incorrect_pin_try_again);
            return;
        }
        this.errorLabel.setText(requireContext().getResources().getQuantityString(R.plurals.RegistrationLockFragment__incorrect_pin_d_attempts_remaining, triesRemaining, Integer.valueOf(triesRemaining)));
        this.forgotPin.setVisibility(0);
    }

    public void onRateLimited() {
        this.pinButton.cancelSpinning();
        enableAndFocusPinEntry();
        new MaterialAlertDialogBuilder(requireContext()).setTitle(R.string.RegistrationActivity_too_many_attempts).setMessage(R.string.RegistrationActivity_you_have_made_too_many_incorrect_registration_lock_pin_attempts_please_try_again_in_a_day).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).show();
    }

    public void onKbsAccountLocked() {
        onAccountLocked();
    }

    public void onError() {
        this.pinButton.cancelSpinning();
        enableAndFocusPinEntry();
        Toast.makeText(requireContext(), (int) R.string.RegistrationActivity_error_connecting_to_service, 1).show();
    }

    private void handleForgottenPin(long j) {
        int lockoutDays = getLockoutDays(j);
        new MaterialAlertDialogBuilder(requireContext()).setTitle(R.string.RegistrationLockFragment__forgot_your_pin).setMessage((CharSequence) requireContext().getResources().getQuantityString(R.plurals.RegistrationLockFragment__for_your_privacy_and_security_there_is_no_way_to_recover, lockoutDays, Integer.valueOf(lockoutDays))).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).setNeutralButton(R.string.PinRestoreEntryFragment_contact_support, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseRegistrationLockFragment$$ExternalSyntheticLambda0
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                BaseRegistrationLockFragment.this.lambda$handleForgottenPin$7(dialogInterface, i);
            }
        }).show();
    }

    public /* synthetic */ void lambda$handleForgottenPin$7(DialogInterface dialogInterface, int i) {
        sendEmailToSupport();
    }

    private static int getLockoutDays(long j) {
        return ((int) TimeUnit.MILLISECONDS.toDays(j)) + 1;
    }

    private void onAccountLocked() {
        navigateToAccountLocked();
    }

    private void updateKeyboard(PinKeyboardType pinKeyboardType) {
        this.pinEntry.setInputType(pinKeyboardType == PinKeyboardType.ALPHA_NUMERIC ? 129 : 18);
        this.pinEntry.getText().clear();
    }

    private static int resolveKeyboardToggleText(PinKeyboardType pinKeyboardType) {
        return pinKeyboardType == PinKeyboardType.ALPHA_NUMERIC ? R.string.RegistrationLockFragment__enter_alphanumeric_pin : R.string.RegistrationLockFragment__enter_numeric_pin;
    }

    private void enableAndFocusPinEntry() {
        this.pinEntry.setEnabled(true);
        this.pinEntry.setFocusable(true);
        if (this.pinEntry.requestFocus()) {
            ServiceUtil.getInputMethodManager(this.pinEntry.getContext()).showSoftInput(this.pinEntry, 0);
        }
    }
}
