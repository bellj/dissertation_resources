package org.thoughtcrime.securesms.registration.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.fragment.NavHostFragment;
import java.io.Serializable;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel;
import org.thoughtcrime.securesms.registration.viewmodel.RegistrationViewModel;

/* loaded from: classes4.dex */
public final class CaptchaFragment extends LoggingFragment {
    public static final String EXTRA_VIEW_MODEL_PROVIDER;
    private BaseRegistrationViewModel viewModel;

    /* loaded from: classes4.dex */
    public interface CaptchaViewModelProvider extends Serializable {
        BaseRegistrationViewModel get(CaptchaFragment captchaFragment);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_registration_captcha, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        WebView webView = (WebView) view.findViewById(R.id.registration_captcha_web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.clearCache(true);
        webView.setWebViewClient(new WebViewClient() { // from class: org.thoughtcrime.securesms.registration.fragments.CaptchaFragment.1
            @Override // android.webkit.WebViewClient
            public boolean shouldOverrideUrlLoading(WebView webView2, String str) {
                if (str == null || !str.startsWith("signalcaptcha://")) {
                    return false;
                }
                CaptchaFragment.this.handleToken(str.substring(16));
                return true;
            }
        });
        webView.loadUrl(BuildConfig.SIGNAL_CAPTCHA_URL);
        CaptchaViewModelProvider captchaViewModelProvider = getArguments() != null ? (CaptchaViewModelProvider) requireArguments().getSerializable(EXTRA_VIEW_MODEL_PROVIDER) : null;
        if (captchaViewModelProvider == null) {
            this.viewModel = (BaseRegistrationViewModel) ViewModelProviders.of(requireActivity()).get(RegistrationViewModel.class);
        } else {
            this.viewModel = captchaViewModelProvider.get(this);
        }
    }

    public void handleToken(String str) {
        this.viewModel.setCaptchaResponse(str);
        NavHostFragment.findNavController(this).navigateUp();
    }
}
