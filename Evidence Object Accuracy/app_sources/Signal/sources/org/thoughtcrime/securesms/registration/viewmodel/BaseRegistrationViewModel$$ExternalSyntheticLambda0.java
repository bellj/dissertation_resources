package org.thoughtcrime.securesms.registration.viewmodel;

import io.reactivex.rxjava3.functions.Function;
import org.thoughtcrime.securesms.registration.VerifyAccountResponseWithoutKbs;
import org.whispersystems.signalservice.internal.ServiceResponse;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class BaseRegistrationViewModel$$ExternalSyntheticLambda0 implements Function {
    @Override // io.reactivex.rxjava3.functions.Function
    public final Object apply(Object obj) {
        return new VerifyAccountResponseWithoutKbs((ServiceResponse) obj);
    }
}
