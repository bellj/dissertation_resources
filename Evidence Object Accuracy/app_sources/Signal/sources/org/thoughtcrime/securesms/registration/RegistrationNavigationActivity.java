package org.thoughtcrime.securesms.registration;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import com.google.android.gms.common.api.Status;
import j$.util.Optional;
import org.greenrobot.eventbus.EventBus;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.registration.viewmodel.RegistrationViewModel;
import org.thoughtcrime.securesms.service.VerificationCodeParser;
import org.thoughtcrime.securesms.util.CommunicationActions;

/* loaded from: classes4.dex */
public final class RegistrationNavigationActivity extends AppCompatActivity {
    public static final String RE_REGISTRATION_EXTRA;
    private static final String TAG = Log.tag(RegistrationNavigationActivity.class);
    private SmsRetrieverReceiver smsRetrieverReceiver;
    private RegistrationViewModel viewModel;

    public static Intent newIntentForNewRegistration(Context context, Intent intent) {
        Intent intent2 = new Intent(context, RegistrationNavigationActivity.class);
        intent2.putExtra(RE_REGISTRATION_EXTRA, false);
        if (intent != null) {
            intent2.setData(intent.getData());
        }
        return intent2;
    }

    public static Intent newIntentForReRegistration(Context context) {
        Intent intent = new Intent(context, RegistrationNavigationActivity.class);
        intent.putExtra(RE_REGISTRATION_EXTRA, true);
        return intent;
    }

    @Override // androidx.appcompat.app.AppCompatActivity, android.app.Activity, android.view.ContextThemeWrapper, android.content.ContextWrapper
    public void attachBaseContext(Context context) {
        getDelegate().setLocalNightMode(1);
        super.attachBaseContext(context);
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.viewModel = (RegistrationViewModel) new ViewModelProvider(this, new RegistrationViewModel.Factory(this, isReregister(getIntent()))).get(RegistrationViewModel.class);
        setContentView(R.layout.activity_registration_navigation);
        initializeChallengeListener();
        if (getIntent() != null && getIntent().getData() != null) {
            CommunicationActions.handlePotentialProxyLinkUrl(this, getIntent().getDataString());
        }
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getData() != null) {
            CommunicationActions.handlePotentialProxyLinkUrl(this, intent.getDataString());
        }
        this.viewModel.setIsReregister(isReregister(intent));
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        shutdownChallengeListener();
    }

    private boolean isReregister(Intent intent) {
        return intent.getBooleanExtra(RE_REGISTRATION_EXTRA, false);
    }

    private void initializeChallengeListener() {
        SmsRetrieverReceiver smsRetrieverReceiver = new SmsRetrieverReceiver();
        this.smsRetrieverReceiver = smsRetrieverReceiver;
        registerReceiver(smsRetrieverReceiver, new IntentFilter("com.google.android.gms.auth.api.phone.SMS_RETRIEVED"));
    }

    private void shutdownChallengeListener() {
        SmsRetrieverReceiver smsRetrieverReceiver = this.smsRetrieverReceiver;
        if (smsRetrieverReceiver != null) {
            unregisterReceiver(smsRetrieverReceiver);
            this.smsRetrieverReceiver = null;
        }
    }

    /* loaded from: classes4.dex */
    public class SmsRetrieverReceiver extends BroadcastReceiver {
        private SmsRetrieverReceiver() {
            RegistrationNavigationActivity.this = r1;
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            Log.i(RegistrationNavigationActivity.TAG, "SmsRetrieverReceiver received a broadcast...");
            if ("com.google.android.gms.auth.api.phone.SMS_RETRIEVED".equals(intent.getAction())) {
                Bundle extras = intent.getExtras();
                int statusCode = ((Status) extras.get("com.google.android.gms.auth.api.phone.EXTRA_STATUS")).getStatusCode();
                if (statusCode == 0) {
                    Optional<String> parse = VerificationCodeParser.parse((String) extras.get("com.google.android.gms.auth.api.phone.EXTRA_SMS_MESSAGE"));
                    if (parse.isPresent()) {
                        Log.i(RegistrationNavigationActivity.TAG, "Received verification code.");
                        RegistrationNavigationActivity.this.handleVerificationCodeReceived(parse.get());
                        return;
                    }
                    Log.w(RegistrationNavigationActivity.TAG, "Could not parse verification code.");
                } else if (statusCode == 15) {
                    Log.w(RegistrationNavigationActivity.TAG, "Hit a timeout waiting for the SMS to arrive.");
                }
            } else {
                Log.w(RegistrationNavigationActivity.TAG, "SmsRetrieverReceiver received the wrong action?");
            }
        }
    }

    public void handleVerificationCodeReceived(String str) {
        EventBus.getDefault().post(new ReceivedSmsEvent(str));
    }
}
