package org.thoughtcrime.securesms.registration.viewmodel;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber$PhoneNumber;
import java.util.Objects;
import org.whispersystems.signalservice.api.util.PhoneNumberFormatter;

/* loaded from: classes4.dex */
public final class NumberViewState implements Parcelable {
    public static final Parcelable.Creator<NumberViewState> CREATOR = new Parcelable.Creator<NumberViewState>() { // from class: org.thoughtcrime.securesms.registration.viewmodel.NumberViewState.1
        @Override // android.os.Parcelable.Creator
        public NumberViewState createFromParcel(Parcel parcel) {
            return new Builder().selectedCountryDisplayName(parcel.readString()).countryCode(parcel.readInt()).nationalNumber(parcel.readString()).build();
        }

        @Override // android.os.Parcelable.Creator
        public NumberViewState[] newArray(int i) {
            return new NumberViewState[i];
        }
    };
    public static final NumberViewState INITIAL = new Builder().build();
    private final int countryCode;
    private final String nationalNumber;
    private final String selectedCountryName;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    private NumberViewState(Builder builder) {
        this.selectedCountryName = builder.countryDisplayName;
        this.countryCode = builder.countryCode;
        this.nationalNumber = builder.nationalNumber;
    }

    public Builder toBuilder() {
        return new Builder().countryCode(this.countryCode).selectedCountryDisplayName(this.selectedCountryName).nationalNumber(this.nationalNumber);
    }

    public int getCountryCode() {
        return this.countryCode;
    }

    public String getNationalNumber() {
        return this.nationalNumber;
    }

    public String getCountryDisplayName() {
        String actualCountry;
        String str = this.selectedCountryName;
        if (str != null) {
            return str;
        }
        PhoneNumberUtil instance = PhoneNumberUtil.getInstance();
        if (!isValid() || (actualCountry = getActualCountry(instance, getE164Number())) == null) {
            return PhoneNumberFormatter.getRegionDisplayNameLegacy(instance.getRegionCodeForCountryCode(this.countryCode));
        }
        return actualCountry;
    }

    private static String getActualCountry(PhoneNumberUtil phoneNumberUtil, String str) {
        try {
            String regionCodeForNumber = phoneNumberUtil.getRegionCodeForNumber(getPhoneNumber(phoneNumberUtil, str));
            if (regionCodeForNumber != null) {
                return PhoneNumberFormatter.getRegionDisplayNameLegacy(regionCodeForNumber);
            }
        } catch (NumberParseException unused) {
        }
        return null;
    }

    public boolean isValid() {
        return PhoneNumberFormatter.isValidNumber(getE164Number(), Integer.toString(getCountryCode()));
    }

    @Override // java.lang.Object
    public int hashCode() {
        int i = this.countryCode * 31;
        String str = this.nationalNumber;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.selectedCountryName;
        if (str2 != null) {
            i2 = str2.hashCode();
        }
        return hashCode + i2;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != NumberViewState.class) {
            return false;
        }
        NumberViewState numberViewState = (NumberViewState) obj;
        if (numberViewState.countryCode != this.countryCode || !Objects.equals(numberViewState.nationalNumber, this.nationalNumber) || !Objects.equals(numberViewState.selectedCountryName, this.selectedCountryName)) {
            return false;
        }
        return true;
    }

    public String getE164Number() {
        return getConfiguredE164Number(this.countryCode, this.nationalNumber);
    }

    public String getFullFormattedNumber() {
        return formatNumber(PhoneNumberUtil.getInstance(), getE164Number());
    }

    private static String formatNumber(PhoneNumberUtil phoneNumberUtil, String str) {
        try {
            return phoneNumberUtil.format(getPhoneNumber(phoneNumberUtil, str), PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
        } catch (NumberParseException unused) {
            return str;
        }
    }

    private static String getConfiguredE164Number(int i, String str) {
        return PhoneNumberFormatter.formatE164(String.valueOf(i), str);
    }

    private static Phonenumber$PhoneNumber getPhoneNumber(PhoneNumberUtil phoneNumberUtil, String str) throws NumberParseException {
        return phoneNumberUtil.parse(str, null);
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private int countryCode;
        private String countryDisplayName;
        private String nationalNumber;

        public Builder countryCode(int i) {
            this.countryCode = i;
            return this;
        }

        public Builder selectedCountryDisplayName(String str) {
            this.countryDisplayName = str;
            return this;
        }

        public Builder nationalNumber(String str) {
            this.nationalNumber = str;
            return this;
        }

        public NumberViewState build() {
            return new NumberViewState(this);
        }
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.selectedCountryName);
        parcel.writeInt(this.countryCode);
        parcel.writeString(this.nationalNumber);
    }
}
