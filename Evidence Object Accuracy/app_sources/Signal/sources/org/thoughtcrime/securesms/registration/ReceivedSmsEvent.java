package org.thoughtcrime.securesms.registration;

/* loaded from: classes4.dex */
public final class ReceivedSmsEvent {
    private final String code;

    public ReceivedSmsEvent(String str) {
        this.code = str;
    }

    public String getCode() {
        return this.code;
    }
}
