package org.thoughtcrime.securesms.registration.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.activity.OnBackPressedCallback;
import androidx.lifecycle.Observer;
import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel;

/* loaded from: classes4.dex */
public abstract class BaseAccountLockedFragment extends LoggingFragment {
    protected abstract BaseRegistrationViewModel getViewModel();

    protected abstract void onNext();

    public BaseAccountLockedFragment(int i) {
        super(i);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        getViewModel().getLockedTimeRemaining().observe(getViewLifecycleOwner(), new Observer((TextView) view.findViewById(R.id.account_locked_description)) { // from class: org.thoughtcrime.securesms.registration.fragments.BaseAccountLockedFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ TextView f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                BaseAccountLockedFragment.this.lambda$onViewCreated$0(this.f$1, (Long) obj);
            }
        });
        view.findViewById(R.id.account_locked_next).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseAccountLockedFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                BaseAccountLockedFragment.this.lambda$onViewCreated$1(view2);
            }
        });
        view.findViewById(R.id.account_locked_learn_more).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseAccountLockedFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                BaseAccountLockedFragment.this.lambda$onViewCreated$2(view2);
            }
        });
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(true) { // from class: org.thoughtcrime.securesms.registration.fragments.BaseAccountLockedFragment.1
            @Override // androidx.activity.OnBackPressedCallback
            public void handleOnBackPressed() {
                BaseAccountLockedFragment.this.onNext();
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$0(TextView textView, Long l) {
        textView.setText(getString(R.string.AccountLockedFragment__your_account_has_been_locked_to_protect_your_privacy, Long.valueOf(durationToDays(l.longValue()))));
    }

    public /* synthetic */ void lambda$onViewCreated$1(View view) {
        onNext();
    }

    public /* synthetic */ void lambda$onViewCreated$2(View view) {
        learnMore();
    }

    private void learnMore() {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(getString(R.string.AccountLockedFragment__learn_more_url)));
        startActivity(intent);
    }

    private static long durationToDays(long j) {
        if (j != 0) {
            return (long) getLockoutDays(j);
        }
        return 7;
    }

    private static int getLockoutDays(long j) {
        return ((int) TimeUnit.MILLISECONDS.toDays(j)) + 1;
    }
}
