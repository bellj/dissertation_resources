package org.thoughtcrime.securesms.registration.fragments;

import android.os.Bundle;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class RegistrationLockFragmentArgs {
    private final HashMap arguments;

    private RegistrationLockFragmentArgs() {
        this.arguments = new HashMap();
    }

    private RegistrationLockFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static RegistrationLockFragmentArgs fromBundle(Bundle bundle) {
        RegistrationLockFragmentArgs registrationLockFragmentArgs = new RegistrationLockFragmentArgs();
        bundle.setClassLoader(RegistrationLockFragmentArgs.class.getClassLoader());
        if (bundle.containsKey("timeRemaining")) {
            registrationLockFragmentArgs.arguments.put("timeRemaining", Long.valueOf(bundle.getLong("timeRemaining")));
            return registrationLockFragmentArgs;
        }
        throw new IllegalArgumentException("Required argument \"timeRemaining\" is missing and does not have an android:defaultValue");
    }

    public long getTimeRemaining() {
        return ((Long) this.arguments.get("timeRemaining")).longValue();
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("timeRemaining")) {
            bundle.putLong("timeRemaining", ((Long) this.arguments.get("timeRemaining")).longValue());
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        RegistrationLockFragmentArgs registrationLockFragmentArgs = (RegistrationLockFragmentArgs) obj;
        return this.arguments.containsKey("timeRemaining") == registrationLockFragmentArgs.arguments.containsKey("timeRemaining") && getTimeRemaining() == registrationLockFragmentArgs.getTimeRemaining();
    }

    public int hashCode() {
        return 31 + ((int) (getTimeRemaining() ^ (getTimeRemaining() >>> 32)));
    }

    public String toString() {
        return "RegistrationLockFragmentArgs{timeRemaining=" + getTimeRemaining() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(RegistrationLockFragmentArgs registrationLockFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(registrationLockFragmentArgs.arguments);
        }

        public Builder(long j) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("timeRemaining", Long.valueOf(j));
        }

        public RegistrationLockFragmentArgs build() {
            return new RegistrationLockFragmentArgs(this.arguments);
        }

        public Builder setTimeRemaining(long j) {
            this.arguments.put("timeRemaining", Long.valueOf(j));
            return this;
        }

        public long getTimeRemaining() {
            return ((Long) this.arguments.get("timeRemaining")).longValue();
        }
    }
}
