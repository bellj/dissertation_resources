package org.thoughtcrime.securesms.registration.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.SignupDirections;

/* loaded from: classes4.dex */
public class WelcomeFragmentDirections {
    private WelcomeFragmentDirections() {
    }

    public static ActionRestore actionRestore() {
        return new ActionRestore();
    }

    public static NavDirections actionSkipRestore() {
        return new ActionOnlyNavDirections(R.id.action_skip_restore);
    }

    public static NavDirections actionTransferOrRestore() {
        return new ActionOnlyNavDirections(R.id.action_transfer_or_restore);
    }

    public static NavDirections actionWelcomeFragmentToDeviceTransferSetup() {
        return new ActionOnlyNavDirections(R.id.action_welcomeFragment_to_deviceTransferSetup);
    }

    public static NavDirections actionRestartToWelcomeFragment() {
        return SignupDirections.actionRestartToWelcomeFragment();
    }

    /* loaded from: classes4.dex */
    public static class ActionRestore implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_restore;
        }

        private ActionRestore() {
            this.arguments = new HashMap();
        }

        public ActionRestore setUri(Uri uri) {
            this.arguments.put("uri", uri);
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("uri")) {
                Uri uri = (Uri) this.arguments.get("uri");
                if (Parcelable.class.isAssignableFrom(Uri.class) || uri == null) {
                    bundle.putParcelable("uri", (Parcelable) Parcelable.class.cast(uri));
                } else if (Serializable.class.isAssignableFrom(Uri.class)) {
                    bundle.putSerializable("uri", (Serializable) Serializable.class.cast(uri));
                } else {
                    throw new UnsupportedOperationException(Uri.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            } else {
                bundle.putSerializable("uri", null);
            }
            return bundle;
        }

        public Uri getUri() {
            return (Uri) this.arguments.get("uri");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionRestore actionRestore = (ActionRestore) obj;
            if (this.arguments.containsKey("uri") != actionRestore.arguments.containsKey("uri")) {
                return false;
            }
            if (getUri() == null ? actionRestore.getUri() == null : getUri().equals(actionRestore.getUri())) {
                return getActionId() == actionRestore.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((getUri() != null ? getUri().hashCode() : 0) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionRestore(actionId=" + getActionId() + "){uri=" + getUri() + "}";
        }
    }
}
