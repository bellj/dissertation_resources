package org.thoughtcrime.securesms.registration.fragments;

import android.os.Bundle;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import java.util.HashMap;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.SignupDirections;

/* loaded from: classes4.dex */
public class EnterSmsCodeFragmentDirections {
    private EnterSmsCodeFragmentDirections() {
    }

    public static ActionRequireKbsLockPin actionRequireKbsLockPin(long j) {
        return new ActionRequireKbsLockPin(j);
    }

    public static NavDirections actionRequestCaptcha() {
        return new ActionOnlyNavDirections(R.id.action_requestCaptcha);
    }

    public static NavDirections actionSuccessfulRegistration() {
        return new ActionOnlyNavDirections(R.id.action_successfulRegistration);
    }

    public static NavDirections actionAccountLocked() {
        return new ActionOnlyNavDirections(R.id.action_accountLocked);
    }

    public static NavDirections actionRestartToWelcomeFragment() {
        return SignupDirections.actionRestartToWelcomeFragment();
    }

    /* loaded from: classes4.dex */
    public static class ActionRequireKbsLockPin implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_requireKbsLockPin;
        }

        private ActionRequireKbsLockPin(long j) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("timeRemaining", Long.valueOf(j));
        }

        public ActionRequireKbsLockPin setTimeRemaining(long j) {
            this.arguments.put("timeRemaining", Long.valueOf(j));
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("timeRemaining")) {
                bundle.putLong("timeRemaining", ((Long) this.arguments.get("timeRemaining")).longValue());
            }
            return bundle;
        }

        public long getTimeRemaining() {
            return ((Long) this.arguments.get("timeRemaining")).longValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionRequireKbsLockPin actionRequireKbsLockPin = (ActionRequireKbsLockPin) obj;
            return this.arguments.containsKey("timeRemaining") == actionRequireKbsLockPin.arguments.containsKey("timeRemaining") && getTimeRemaining() == actionRequireKbsLockPin.getTimeRemaining() && getActionId() == actionRequireKbsLockPin.getActionId();
        }

        public int hashCode() {
            return ((((int) (getTimeRemaining() ^ (getTimeRemaining() >>> 32))) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionRequireKbsLockPin(actionId=" + getActionId() + "){timeRemaining=" + getTimeRemaining() + "}";
        }
    }
}
