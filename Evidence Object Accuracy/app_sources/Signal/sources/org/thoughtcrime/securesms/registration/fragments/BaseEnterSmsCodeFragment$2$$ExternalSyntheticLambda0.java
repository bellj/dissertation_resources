package org.thoughtcrime.securesms.registration.fragments;

import android.content.DialogInterface;
import org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class BaseEnterSmsCodeFragment$2$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ BaseEnterSmsCodeFragment.AnonymousClass2 f$0;

    public /* synthetic */ BaseEnterSmsCodeFragment$2$$ExternalSyntheticLambda0(BaseEnterSmsCodeFragment.AnonymousClass2 r1) {
        this.f$0 = r1;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onSuccess$0(dialogInterface, i);
    }
}
