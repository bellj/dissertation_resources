package org.thoughtcrime.securesms.registration.secondary;

import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import kotlin.Metadata;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.RangesKt___RangesKt;
import kotlin.text.Charsets;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.IdentityKeyPair;
import org.signal.libsignal.protocol.ecc.Curve;
import org.signal.libsignal.protocol.ecc.ECPublicKey;
import org.signal.libsignal.protocol.kdf.HKDF;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.thoughtcrime.securesms.crypto.IdentityKeyUtil;
import org.thoughtcrime.securesms.database.CdsDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.whispersystems.signalservice.api.util.UuidUtil;
import org.whispersystems.signalservice.internal.crypto.PrimaryProvisioningCipher;
import org.whispersystems.signalservice.internal.push.ProvisioningProtos;

/* compiled from: SecondaryProvisioningCipher.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0002\b\u0007\u0018\u0000 \u00132\u00020\u0001:\u0002\u0013\u0014B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u001a\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u000eH\u0002J \u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u000eH\u0002R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/registration/secondary/SecondaryProvisioningCipher;", "", "secondaryIdentityKeyPair", "Lorg/signal/libsignal/protocol/IdentityKeyPair;", "(Lorg/signal/libsignal/protocol/IdentityKeyPair;)V", "secondaryDevicePublicKey", "Lorg/signal/libsignal/protocol/IdentityKey;", "getSecondaryDevicePublicKey", "()Lorg/signal/libsignal/protocol/IdentityKey;", "decrypt", "Lorg/thoughtcrime/securesms/registration/secondary/SecondaryProvisioningCipher$ProvisionDecryptResult;", "envelope", "Lorg/whispersystems/signalservice/internal/push/ProvisioningProtos$ProvisionEnvelope;", "getMac", "", "key", "message", "getPlaintext", "iv", "Companion", "ProvisionDecryptResult", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SecondaryProvisioningCipher {
    public static final Companion Companion = new Companion(null);
    private static final int IV_LENGTH;
    private static final int MAC_LENGTH;
    private static final int VERSION_LENGTH;
    private final IdentityKey secondaryDevicePublicKey;
    private final IdentityKeyPair secondaryIdentityKeyPair;

    public /* synthetic */ SecondaryProvisioningCipher(IdentityKeyPair identityKeyPair, DefaultConstructorMarker defaultConstructorMarker) {
        this(identityKeyPair);
    }

    private SecondaryProvisioningCipher(IdentityKeyPair identityKeyPair) {
        this.secondaryIdentityKeyPair = identityKeyPair;
        IdentityKey publicKey = identityKeyPair.getPublicKey();
        Intrinsics.checkNotNullExpressionValue(publicKey, "secondaryIdentityKeyPair.publicKey");
        this.secondaryDevicePublicKey = publicKey;
    }

    public final IdentityKey getSecondaryDevicePublicKey() {
        return this.secondaryDevicePublicKey;
    }

    public final ProvisionDecryptResult decrypt(ProvisioningProtos.ProvisionEnvelope provisionEnvelope) {
        Intrinsics.checkNotNullParameter(provisionEnvelope, "envelope");
        byte[] byteArray = provisionEnvelope.getPublicKey().toByteArray();
        byte[] byteArray2 = provisionEnvelope.getBody().toByteArray();
        if (((byteArray2.length - 1) - 16) - 32 <= 0) {
            return ProvisionDecryptResult.Error.INSTANCE;
        }
        if (byteArray2[0] != 1) {
            return ProvisionDecryptResult.Error.INSTANCE;
        }
        Intrinsics.checkNotNullExpressionValue(byteArray2, "body");
        byte[] bArr = ArraysKt___ArraysKt.sliceArray(byteArray2, RangesKt___RangesKt.until(1, 17));
        byte[] bArr2 = ArraysKt___ArraysKt.sliceArray(byteArray2, RangesKt___RangesKt.until(byteArray2.length - 32, byteArray2.length));
        byte[] bArr3 = ArraysKt___ArraysKt.sliceArray(byteArray2, RangesKt___RangesKt.until(0, byteArray2.length - 32));
        byte[] bArr4 = ArraysKt___ArraysKt.sliceArray(byteArray2, RangesKt___RangesKt.until(17, byteArray2.length - 32));
        byte[] calculateAgreement = Curve.calculateAgreement(new ECPublicKey(byteArray), this.secondaryIdentityKeyPair.getPrivateKey());
        byte[] bytes = PrimaryProvisioningCipher.PROVISIONING_MESSAGE.getBytes(Charsets.UTF_8);
        Intrinsics.checkNotNullExpressionValue(bytes, "this as java.lang.String).getBytes(charset)");
        byte[] deriveSecrets = HKDF.deriveSecrets(calculateAgreement, bytes, 64);
        Intrinsics.checkNotNullExpressionValue(deriveSecrets, "deriveSecrets(sharedSecr…ESSAGE.toByteArray(), 64)");
        byte[] bArr5 = ArraysKt___ArraysKt.sliceArray(deriveSecrets, RangesKt___RangesKt.until(0, 32));
        if (!MessageDigest.isEqual(bArr2, getMac(ArraysKt___ArraysKt.sliceArray(deriveSecrets, RangesKt___RangesKt.until(32, 64)), bArr3))) {
            return ProvisionDecryptResult.Error.INSTANCE;
        }
        try {
            ProvisioningProtos.ProvisionMessage parseFrom = ProvisioningProtos.ProvisionMessage.parseFrom(getPlaintext(bArr5, bArr, bArr4));
            UUID parseOrThrow = UuidUtil.parseOrThrow(parseFrom.getAci());
            Intrinsics.checkNotNullExpressionValue(parseOrThrow, "parseOrThrow(provisioningMessage.aci)");
            String number = parseFrom.getNumber();
            Intrinsics.checkNotNullExpressionValue(number, "provisioningMessage.number");
            IdentityKeyPair identityKeyPair = new IdentityKeyPair(new IdentityKey(parseFrom.getAciIdentityKeyPublic().toByteArray()), Curve.decodePrivatePoint(parseFrom.getAciIdentityKeyPrivate().toByteArray()));
            ProfileKey profileKey = new ProfileKey(parseFrom.getProfileKey().toByteArray());
            boolean readReceipts = parseFrom.getReadReceipts();
            String userAgent = parseFrom.getUserAgent();
            String provisioningCode = parseFrom.getProvisioningCode();
            Intrinsics.checkNotNullExpressionValue(provisioningCode, "provisioningMessage.provisioningCode");
            return new ProvisionDecryptResult.Success(parseOrThrow, number, identityKeyPair, profileKey, readReceipts, userAgent, provisioningCode, parseFrom.getProvisioningVersion());
        } catch (Exception unused) {
            return ProvisionDecryptResult.Error.INSTANCE;
        }
    }

    private final byte[] getMac(byte[] bArr, byte[] bArr2) {
        try {
            Mac instance = Mac.getInstance("HmacSHA256");
            instance.init(new SecretKeySpec(bArr, "HmacSHA256"));
            return instance.doFinal(bArr2);
        } catch (InvalidKeyException e) {
            throw new AssertionError(e);
        } catch (NoSuchAlgorithmException e2) {
            throw new AssertionError(e2);
        }
    }

    private final byte[] getPlaintext(byte[] bArr, byte[] bArr2, byte[] bArr3) {
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
        instance.init(2, new SecretKeySpec(bArr, "AES"), new IvParameterSpec(bArr2));
        byte[] doFinal = instance.doFinal(bArr3);
        Intrinsics.checkNotNullExpressionValue(doFinal, "cipher.doFinal(message)");
        return doFinal;
    }

    /* compiled from: SecondaryProvisioningCipher.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/registration/secondary/SecondaryProvisioningCipher$Companion;", "", "()V", "IV_LENGTH", "", "MAC_LENGTH", "VERSION_LENGTH", "generate", "Lorg/thoughtcrime/securesms/registration/secondary/SecondaryProvisioningCipher;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final SecondaryProvisioningCipher generate() {
            IdentityKeyPair generateIdentityKeyPair = IdentityKeyUtil.generateIdentityKeyPair();
            Intrinsics.checkNotNullExpressionValue(generateIdentityKeyPair, "generateIdentityKeyPair()");
            return new SecondaryProvisioningCipher(generateIdentityKeyPair, null);
        }
    }

    /* compiled from: SecondaryProvisioningCipher.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0003\u0004B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0002\u0005\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/registration/secondary/SecondaryProvisioningCipher$ProvisionDecryptResult;", "", "()V", "Error", "Success", "Lorg/thoughtcrime/securesms/registration/secondary/SecondaryProvisioningCipher$ProvisionDecryptResult$Error;", "Lorg/thoughtcrime/securesms/registration/secondary/SecondaryProvisioningCipher$ProvisionDecryptResult$Success;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class ProvisionDecryptResult {
        public /* synthetic */ ProvisionDecryptResult(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* compiled from: SecondaryProvisioningCipher.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/registration/secondary/SecondaryProvisioningCipher$ProvisionDecryptResult$Error;", "Lorg/thoughtcrime/securesms/registration/secondary/SecondaryProvisioningCipher$ProvisionDecryptResult;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Error extends ProvisionDecryptResult {
            public static final Error INSTANCE = new Error();

            private Error() {
                super(null);
            }
        }

        private ProvisionDecryptResult() {
        }

        /* compiled from: SecondaryProvisioningCipher.kt */
        @Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u001a\n\u0002\u0010\u0000\n\u0002\b\u0003\b\b\u0018\u00002\u00020\u0001BG\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\u0006\u0010\u000e\u001a\u00020\u000f¢\u0006\u0002\u0010\u0010J\t\u0010\u001f\u001a\u00020\u0003HÆ\u0003J\t\u0010 \u001a\u00020\u0005HÆ\u0003J\t\u0010!\u001a\u00020\u0007HÆ\u0003J\t\u0010\"\u001a\u00020\tHÆ\u0003J\t\u0010#\u001a\u00020\u000bHÆ\u0003J\u000b\u0010$\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\t\u0010%\u001a\u00020\u0005HÆ\u0003J\t\u0010&\u001a\u00020\u000fHÆ\u0003J[\u0010'\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u000b2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\r\u001a\u00020\u00052\b\b\u0002\u0010\u000e\u001a\u00020\u000fHÆ\u0001J\u0013\u0010(\u001a\u00020\u000b2\b\u0010)\u001a\u0004\u0018\u00010*HÖ\u0003J\t\u0010+\u001a\u00020\u000fHÖ\u0001J\t\u0010,\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0013\u0010\f\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0014R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\r\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0014R\u0011\u0010\u000e\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001e¨\u0006-"}, d2 = {"Lorg/thoughtcrime/securesms/registration/secondary/SecondaryProvisioningCipher$ProvisionDecryptResult$Success;", "Lorg/thoughtcrime/securesms/registration/secondary/SecondaryProvisioningCipher$ProvisionDecryptResult;", RecipientDatabase.SERVICE_ID, "Ljava/util/UUID;", CdsDatabase.E164, "", "identityKeyPair", "Lorg/signal/libsignal/protocol/IdentityKeyPair;", "profileKey", "Lorg/signal/libsignal/zkgroup/profiles/ProfileKey;", "areReadReceiptsEnabled", "", "primaryUserAgent", "provisioningCode", "provisioningVersion", "", "(Ljava/util/UUID;Ljava/lang/String;Lorg/signal/libsignal/protocol/IdentityKeyPair;Lorg/signal/libsignal/zkgroup/profiles/ProfileKey;ZLjava/lang/String;Ljava/lang/String;I)V", "getAreReadReceiptsEnabled", "()Z", "getE164", "()Ljava/lang/String;", "getIdentityKeyPair", "()Lorg/signal/libsignal/protocol/IdentityKeyPair;", "getPrimaryUserAgent", "getProfileKey", "()Lorg/signal/libsignal/zkgroup/profiles/ProfileKey;", "getProvisioningCode", "getProvisioningVersion", "()I", "getUuid", "()Ljava/util/UUID;", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "copy", "equals", "other", "", "hashCode", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Success extends ProvisionDecryptResult {
            private final boolean areReadReceiptsEnabled;
            private final String e164;
            private final IdentityKeyPair identityKeyPair;
            private final String primaryUserAgent;
            private final ProfileKey profileKey;
            private final String provisioningCode;
            private final int provisioningVersion;
            private final UUID uuid;

            public final UUID component1() {
                return this.uuid;
            }

            public final String component2() {
                return this.e164;
            }

            public final IdentityKeyPair component3() {
                return this.identityKeyPair;
            }

            public final ProfileKey component4() {
                return this.profileKey;
            }

            public final boolean component5() {
                return this.areReadReceiptsEnabled;
            }

            public final String component6() {
                return this.primaryUserAgent;
            }

            public final String component7() {
                return this.provisioningCode;
            }

            public final int component8() {
                return this.provisioningVersion;
            }

            public final Success copy(UUID uuid, String str, IdentityKeyPair identityKeyPair, ProfileKey profileKey, boolean z, String str2, String str3, int i) {
                Intrinsics.checkNotNullParameter(uuid, RecipientDatabase.SERVICE_ID);
                Intrinsics.checkNotNullParameter(str, CdsDatabase.E164);
                Intrinsics.checkNotNullParameter(identityKeyPair, "identityKeyPair");
                Intrinsics.checkNotNullParameter(profileKey, "profileKey");
                Intrinsics.checkNotNullParameter(str3, "provisioningCode");
                return new Success(uuid, str, identityKeyPair, profileKey, z, str2, str3, i);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Success)) {
                    return false;
                }
                Success success = (Success) obj;
                return Intrinsics.areEqual(this.uuid, success.uuid) && Intrinsics.areEqual(this.e164, success.e164) && Intrinsics.areEqual(this.identityKeyPair, success.identityKeyPair) && Intrinsics.areEqual(this.profileKey, success.profileKey) && this.areReadReceiptsEnabled == success.areReadReceiptsEnabled && Intrinsics.areEqual(this.primaryUserAgent, success.primaryUserAgent) && Intrinsics.areEqual(this.provisioningCode, success.provisioningCode) && this.provisioningVersion == success.provisioningVersion;
            }

            public int hashCode() {
                int hashCode = ((((((this.uuid.hashCode() * 31) + this.e164.hashCode()) * 31) + this.identityKeyPair.hashCode()) * 31) + this.profileKey.hashCode()) * 31;
                boolean z = this.areReadReceiptsEnabled;
                if (z) {
                    z = true;
                }
                int i = z ? 1 : 0;
                int i2 = z ? 1 : 0;
                int i3 = z ? 1 : 0;
                int i4 = (hashCode + i) * 31;
                String str = this.primaryUserAgent;
                return ((((i4 + (str == null ? 0 : str.hashCode())) * 31) + this.provisioningCode.hashCode()) * 31) + this.provisioningVersion;
            }

            public String toString() {
                return "Success(uuid=" + this.uuid + ", e164=" + this.e164 + ", identityKeyPair=" + this.identityKeyPair + ", profileKey=" + this.profileKey + ", areReadReceiptsEnabled=" + this.areReadReceiptsEnabled + ", primaryUserAgent=" + this.primaryUserAgent + ", provisioningCode=" + this.provisioningCode + ", provisioningVersion=" + this.provisioningVersion + ')';
            }

            public final UUID getUuid() {
                return this.uuid;
            }

            public final String getE164() {
                return this.e164;
            }

            public final IdentityKeyPair getIdentityKeyPair() {
                return this.identityKeyPair;
            }

            public final ProfileKey getProfileKey() {
                return this.profileKey;
            }

            public final boolean getAreReadReceiptsEnabled() {
                return this.areReadReceiptsEnabled;
            }

            public final String getPrimaryUserAgent() {
                return this.primaryUserAgent;
            }

            public final String getProvisioningCode() {
                return this.provisioningCode;
            }

            public final int getProvisioningVersion() {
                return this.provisioningVersion;
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Success(UUID uuid, String str, IdentityKeyPair identityKeyPair, ProfileKey profileKey, boolean z, String str2, String str3, int i) {
                super(null);
                Intrinsics.checkNotNullParameter(uuid, RecipientDatabase.SERVICE_ID);
                Intrinsics.checkNotNullParameter(str, CdsDatabase.E164);
                Intrinsics.checkNotNullParameter(identityKeyPair, "identityKeyPair");
                Intrinsics.checkNotNullParameter(profileKey, "profileKey");
                Intrinsics.checkNotNullParameter(str3, "provisioningCode");
                this.uuid = uuid;
                this.e164 = str;
                this.identityKeyPair = identityKeyPair;
                this.profileKey = profileKey;
                this.areReadReceiptsEnabled = z;
                this.primaryUserAgent = str2;
                this.provisioningCode = str3;
                this.provisioningVersion = i;
            }
        }
    }
}
