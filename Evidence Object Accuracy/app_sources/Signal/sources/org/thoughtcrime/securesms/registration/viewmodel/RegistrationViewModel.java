package org.thoughtcrime.securesms.registration.viewmodel;

import androidx.lifecycle.AbstractSavedStateViewModelFactory;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;
import androidx.savedstate.SavedStateRegistryOwner;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.pin.KbsRepository;
import org.thoughtcrime.securesms.pin.TokenData;
import org.thoughtcrime.securesms.registration.RegistrationData;
import org.thoughtcrime.securesms.registration.RegistrationRepository;
import org.thoughtcrime.securesms.registration.RequestVerificationCodeResponseProcessor;
import org.thoughtcrime.securesms.registration.VerifyAccountRepository;
import org.thoughtcrime.securesms.registration.VerifyAccountResponseProcessor;
import org.thoughtcrime.securesms.registration.VerifyCodeWithRegistrationLockResponseProcessor;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.push.VerifyAccountResponse;

/* loaded from: classes4.dex */
public final class RegistrationViewModel extends BaseRegistrationViewModel {
    private static final String STATE_BACKUP_COMPLETED;
    private static final String STATE_FCM_TOKEN;
    private static final String STATE_IS_REREGISTER;
    private static final String STATE_RESTORE_FLOW_SHOWN;
    private final RegistrationRepository registrationRepository;

    public RegistrationViewModel(SavedStateHandle savedStateHandle, boolean z, VerifyAccountRepository verifyAccountRepository, KbsRepository kbsRepository, RegistrationRepository registrationRepository) {
        super(savedStateHandle, verifyAccountRepository, kbsRepository, Util.getSecret(18));
        this.registrationRepository = registrationRepository;
        Boolean bool = Boolean.FALSE;
        setInitialDefaultValue(STATE_RESTORE_FLOW_SHOWN, bool);
        setInitialDefaultValue(STATE_BACKUP_COMPLETED, bool);
        this.savedState.set(STATE_IS_REREGISTER, Boolean.valueOf(z));
    }

    public boolean isReregister() {
        return ((Boolean) this.savedState.get(STATE_IS_REREGISTER)).booleanValue();
    }

    public void onNumberDetected(int i, String str) {
        setViewState(getNumber().toBuilder().countryCode(i).nationalNumber(str).build());
    }

    public String getFcmToken() {
        return (String) this.savedState.get(STATE_FCM_TOKEN);
    }

    public void setFcmToken(String str) {
        this.savedState.set(STATE_FCM_TOKEN, str);
    }

    public void setWelcomeSkippedOnRestore() {
        this.savedState.set(STATE_RESTORE_FLOW_SHOWN, Boolean.TRUE);
    }

    public boolean hasRestoreFlowBeenShown() {
        return ((Boolean) this.savedState.get(STATE_RESTORE_FLOW_SHOWN)).booleanValue();
    }

    public void setIsReregister(boolean z) {
        this.savedState.set(STATE_IS_REREGISTER, Boolean.valueOf(z));
    }

    public void markBackupCompleted() {
        this.savedState.set(STATE_BACKUP_COMPLETED, Boolean.TRUE);
    }

    public boolean hasBackupCompleted() {
        Boolean bool = (Boolean) this.savedState.get(STATE_BACKUP_COMPLETED);
        if (bool != null) {
            return bool.booleanValue();
        }
        return false;
    }

    @Override // org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel
    public Single<RequestVerificationCodeResponseProcessor> requestVerificationCode(VerifyAccountRepository.Mode mode) {
        return super.requestVerificationCode(mode).doOnSuccess(new Consumer() { // from class: org.thoughtcrime.securesms.registration.viewmodel.RegistrationViewModel$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                RegistrationViewModel.this.lambda$requestVerificationCode$0((RequestVerificationCodeResponseProcessor) obj);
            }
        });
    }

    public /* synthetic */ void lambda$requestVerificationCode$0(RequestVerificationCodeResponseProcessor requestVerificationCodeResponseProcessor) throws Throwable {
        if (requestVerificationCodeResponseProcessor.hasResult()) {
            setFcmToken(requestVerificationCodeResponseProcessor.getResult().getFcmToken().orElse(null));
        }
    }

    @Override // org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel
    protected Single<ServiceResponse<VerifyAccountResponse>> verifyAccountWithoutRegistrationLock() {
        return this.verifyAccountRepository.verifyAccount(getRegistrationData());
    }

    @Override // org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel
    protected Single<ServiceResponse<VerifyAccountRepository.VerifyAccountWithRegistrationLockResponse>> verifyAccountWithRegistrationLock(String str, TokenData tokenData) {
        return this.verifyAccountRepository.verifyAccountWithPin(getRegistrationData(), str, tokenData);
    }

    /* JADX DEBUG: Type inference failed for r3v4. Raw type applied. Possible types: io.reactivex.rxjava3.core.Single<R>, io.reactivex.rxjava3.core.Single<org.thoughtcrime.securesms.registration.VerifyAccountResponseProcessor> */
    @Override // org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel
    protected Single<VerifyAccountResponseProcessor> onVerifySuccess(VerifyAccountResponseProcessor verifyAccountResponseProcessor) {
        return this.registrationRepository.registerAccountWithoutRegistrationLock(getRegistrationData(), verifyAccountResponseProcessor.getResult()).map(new BaseRegistrationViewModel$$ExternalSyntheticLambda0());
    }

    /* JADX DEBUG: Type inference failed for r4v1. Raw type applied. Possible types: io.reactivex.rxjava3.core.Single<R>, io.reactivex.rxjava3.core.Single<org.thoughtcrime.securesms.registration.VerifyCodeWithRegistrationLockResponseProcessor> */
    @Override // org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel
    protected Single<VerifyCodeWithRegistrationLockResponseProcessor> onVerifySuccessWithRegistrationLock(VerifyCodeWithRegistrationLockResponseProcessor verifyCodeWithRegistrationLockResponseProcessor, String str) {
        return this.registrationRepository.registerAccountWithRegistrationLock(getRegistrationData(), verifyCodeWithRegistrationLockResponseProcessor.getResult(), str).map(new Function() { // from class: org.thoughtcrime.securesms.registration.viewmodel.RegistrationViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return VerifyCodeWithRegistrationLockResponseProcessor.this.updatedIfRegistrationFailed((ServiceResponse) obj);
            }
        });
    }

    private RegistrationData getRegistrationData() {
        return new RegistrationData(getTextCodeEntered(), getNumber().getE164Number(), getRegistrationSecret(), this.registrationRepository.getRegistrationId(), this.registrationRepository.getProfileKey(getNumber().getE164Number()), getFcmToken());
    }

    /* loaded from: classes4.dex */
    public static final class Factory extends AbstractSavedStateViewModelFactory {
        private final boolean isReregister;

        public Factory(SavedStateRegistryOwner savedStateRegistryOwner, boolean z) {
            super(savedStateRegistryOwner, null);
            this.isReregister = z;
        }

        @Override // androidx.lifecycle.AbstractSavedStateViewModelFactory
        protected <T extends ViewModel> T create(String str, Class<T> cls, SavedStateHandle savedStateHandle) {
            return cls.cast(new RegistrationViewModel(savedStateHandle, this.isReregister, new VerifyAccountRepository(ApplicationDependencies.getApplication()), new KbsRepository(), new RegistrationRepository(ApplicationDependencies.getApplication())));
        }
    }
}
