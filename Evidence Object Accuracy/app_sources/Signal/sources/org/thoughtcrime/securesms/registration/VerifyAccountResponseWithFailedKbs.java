package org.thoughtcrime.securesms.registration;

import kotlin.Metadata;

/* compiled from: VerifyAccountResponseProcessor.kt */
@Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\b\u0010\u0006\u001a\u00020\u0007H\u0016¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/registration/VerifyAccountResponseWithFailedKbs;", "Lorg/thoughtcrime/securesms/registration/VerifyAccountResponseProcessor;", "response", "Lorg/whispersystems/signalservice/internal/ServiceResponse;", "Lorg/thoughtcrime/securesms/pin/TokenData;", "(Lorg/whispersystems/signalservice/internal/ServiceResponse;)V", "isKbsLocked", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class VerifyAccountResponseWithFailedKbs extends VerifyAccountResponseProcessor {
    @Override // org.thoughtcrime.securesms.registration.VerifyAccountResponseProcessor
    public boolean isKbsLocked() {
        return false;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public VerifyAccountResponseWithFailedKbs(org.whispersystems.signalservice.internal.ServiceResponse<org.thoughtcrime.securesms.pin.TokenData> r2) {
        /*
            r1 = this;
            java.lang.String r0 = "response"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r2, r0)
            org.whispersystems.signalservice.internal.ServiceResponse r2 = org.whispersystems.signalservice.internal.ServiceResponse.coerceError(r2)
            java.lang.String r0 = "coerceError(response)"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r2, r0)
            r0 = 0
            r1.<init>(r2, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.registration.VerifyAccountResponseWithFailedKbs.<init>(org.whispersystems.signalservice.internal.ServiceResponse):void");
    }
}
