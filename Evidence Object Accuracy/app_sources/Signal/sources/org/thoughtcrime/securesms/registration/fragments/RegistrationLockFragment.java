package org.thoughtcrime.securesms.registration.fragments;

import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.StorageAccountRestoreJob;
import org.thoughtcrime.securesms.jobs.StorageSyncJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel;
import org.thoughtcrime.securesms.registration.viewmodel.RegistrationViewModel;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.Stopwatch;
import org.thoughtcrime.securesms.util.SupportEmailUtil;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* loaded from: classes4.dex */
public final class RegistrationLockFragment extends BaseRegistrationLockFragment {
    private static final String TAG = Log.tag(RegistrationLockFragment.class);

    public RegistrationLockFragment() {
        super(R.layout.fragment_registration_lock);
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseRegistrationLockFragment
    protected BaseRegistrationViewModel getViewModel() {
        return (BaseRegistrationViewModel) new ViewModelProvider(requireActivity()).get(RegistrationViewModel.class);
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseRegistrationLockFragment
    protected void navigateToAccountLocked() {
        SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), RegistrationLockFragmentDirections.actionAccountLocked());
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseRegistrationLockFragment
    protected void handleSuccessfulPinEntry(String str) {
        SignalStore.pinValues().setKeyboardType(getPinEntryKeyboardType());
        SimpleTask.run(new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.registration.fragments.RegistrationLockFragment$$ExternalSyntheticLambda0
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return RegistrationLockFragment.$r8$lambda$7xtOe8DrmU8W1hzVp49GmCUJGEo();
            }
        }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.registration.fragments.RegistrationLockFragment$$ExternalSyntheticLambda1
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                RegistrationLockFragment.m2593$r8$lambda$Fm4wg_tU6eZ5ZD5IohEOQcVp8w(RegistrationLockFragment.this, obj);
            }
        });
    }

    public static /* synthetic */ Object lambda$handleSuccessfulPinEntry$0() {
        SignalStore.onboarding().clearAll();
        Stopwatch stopwatch = new Stopwatch("RegistrationLockRestore");
        ApplicationDependencies.getJobManager().runSynchronously(new StorageAccountRestoreJob(), StorageAccountRestoreJob.LIFESPAN);
        stopwatch.split("AccountRestore");
        ApplicationDependencies.getJobManager().runSynchronously(new StorageSyncJob(), TimeUnit.SECONDS.toMillis(10));
        stopwatch.split("ContactRestore");
        try {
            FeatureFlags.refreshSync();
        } catch (IOException e) {
            Log.w(TAG, "Failed to refresh flags.", e);
        }
        stopwatch.split("FeatureFlags");
        stopwatch.stop(TAG);
        return null;
    }

    public /* synthetic */ void lambda$handleSuccessfulPinEntry$1(Object obj) {
        this.pinButton.cancelSpinning();
        SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), RegistrationLockFragmentDirections.actionSuccessfulRegistration());
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseRegistrationLockFragment
    protected void sendEmailToSupport() {
        CommunicationActions.openEmail(requireContext(), SupportEmailUtil.getSupportEmailAddress(requireContext()), getString(R.string.RegistrationLockFragment__signal_registration_need_help_with_pin_for_android_v2_pin), SupportEmailUtil.generateSupportEmailBody(requireContext(), R.string.RegistrationLockFragment__signal_registration_need_help_with_pin_for_android_v2_pin, null, null));
    }
}
