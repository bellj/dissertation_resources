package org.thoughtcrime.securesms.registration;

import j$.util.Optional;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.core.util.logging.Log;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;

/* loaded from: classes4.dex */
public final class PushChallengeRequest {
    private static final String TAG = Log.tag(PushChallengeRequest.class);

    public static Optional<String> getPushChallengeBlocking(SignalServiceAccountManager signalServiceAccountManager, Optional<String> optional, String str, long j) {
        if (!optional.isPresent()) {
            Log.w(TAG, "Push challenge not requested, as no FCM token was present");
            return Optional.empty();
        }
        long currentTimeMillis = System.currentTimeMillis();
        String str2 = TAG;
        Log.i(str2, "Requesting a push challenge");
        Optional<String> requestAndReceiveChallengeBlocking = new Request(signalServiceAccountManager, optional.get(), str, j).requestAndReceiveChallengeBlocking();
        long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
        if (requestAndReceiveChallengeBlocking.isPresent()) {
            Log.i(str2, String.format(Locale.US, "Received a push challenge \"%s\" in %d ms", requestAndReceiveChallengeBlocking.get(), Long.valueOf(currentTimeMillis2)));
        } else {
            Log.w(str2, String.format(Locale.US, "Did not received a push challenge in %d ms", Long.valueOf(currentTimeMillis2)));
        }
        return requestAndReceiveChallengeBlocking;
    }

    public static void postChallengeResponse(String str) {
        EventBus.getDefault().post(new PushChallengeEvent(str));
    }

    /* loaded from: classes.dex */
    public static class Request {
        private final SignalServiceAccountManager accountManager;
        private final AtomicReference<String> challenge;
        private final String e164number;
        private final String fcmToken;
        private final CountDownLatch latch;
        private final long timeoutMs;

        private Request(SignalServiceAccountManager signalServiceAccountManager, String str, String str2, long j) {
            this.latch = new CountDownLatch(1);
            this.challenge = new AtomicReference<>();
            this.accountManager = signalServiceAccountManager;
            this.fcmToken = str;
            this.e164number = str2;
            this.timeoutMs = j;
        }

        /*  JADX ERROR: JadxRuntimeException in pass: SSATransform
            jadx.core.utils.exceptions.JadxRuntimeException: Not initialized variable reg: 0, insn: 0x003f: INVOKE  (r0 I:org.greenrobot.eventbus.EventBus), (r5 I:java.lang.Object) type: VIRTUAL call: org.greenrobot.eventbus.EventBus.unregister(java.lang.Object):void, block:B:11:0x003f
            	at jadx.core.dex.visitors.ssa.SSATransform.renameVarsInBlock(SSATransform.java:171)
            	at jadx.core.dex.visitors.ssa.SSATransform.renameVariables(SSATransform.java:143)
            	at jadx.core.dex.visitors.ssa.SSATransform.process(SSATransform.java:60)
            	at jadx.core.dex.visitors.ssa.SSATransform.visit(SSATransform.java:41)
            */
        public j$.util.Optional<java.lang.String> requestAndReceiveChallengeBlocking() {
            /*
                r5 = this;
                org.greenrobot.eventbus.EventBus r0 = org.greenrobot.eventbus.EventBus.getDefault()
                r0.register(r5)
                org.whispersystems.signalservice.api.SignalServiceAccountManager r1 = r5.accountManager     // Catch: InterruptedException -> 0x002d, IOException -> 0x002b, all -> 0x0029
                java.lang.String r2 = r5.fcmToken     // Catch: InterruptedException -> 0x002d, IOException -> 0x002b, all -> 0x0029
                java.lang.String r3 = r5.e164number     // Catch: InterruptedException -> 0x002d, IOException -> 0x002b, all -> 0x0029
                r1.requestRegistrationPushChallenge(r2, r3)     // Catch: InterruptedException -> 0x002d, IOException -> 0x002b, all -> 0x0029
                java.util.concurrent.CountDownLatch r1 = r5.latch     // Catch: InterruptedException -> 0x002d, IOException -> 0x002b, all -> 0x0029
                long r2 = r5.timeoutMs     // Catch: InterruptedException -> 0x002d, IOException -> 0x002b, all -> 0x0029
                java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch: InterruptedException -> 0x002d, IOException -> 0x002b, all -> 0x0029
                r1.await(r2, r4)     // Catch: InterruptedException -> 0x002d, IOException -> 0x002b, all -> 0x0029
                java.util.concurrent.atomic.AtomicReference<java.lang.String> r1 = r5.challenge     // Catch: InterruptedException -> 0x002d, IOException -> 0x002b, all -> 0x0029
                java.lang.Object r1 = r1.get()     // Catch: InterruptedException -> 0x002d, IOException -> 0x002b, all -> 0x0029
                java.lang.String r1 = (java.lang.String) r1     // Catch: InterruptedException -> 0x002d, IOException -> 0x002b, all -> 0x0029
                j$.util.Optional r1 = j$.util.Optional.ofNullable(r1)     // Catch: InterruptedException -> 0x002d, IOException -> 0x002b, all -> 0x0029
                r0.unregister(r5)
                return r1
            L_0x0029:
                r1 = move-exception
                goto L_0x003f
            L_0x002b:
                r1 = move-exception
                goto L_0x002e
            L_0x002d:
                r1 = move-exception
            L_0x002e:
                java.lang.String r2 = org.thoughtcrime.securesms.registration.PushChallengeRequest.access$200()     // Catch: all -> 0x0029
                java.lang.String r3 = "Error getting push challenge"
                org.signal.core.util.logging.Log.w(r2, r3, r1)     // Catch: all -> 0x0029
                j$.util.Optional r1 = j$.util.Optional.empty()     // Catch: all -> 0x0029
                r0.unregister(r5)
                return r1
            L_0x003f:
                r0.unregister(r5)
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.registration.PushChallengeRequest.Request.requestAndReceiveChallengeBlocking():j$.util.Optional");
        }

        @Subscribe(threadMode = ThreadMode.POSTING)
        public void onChallengeEvent(PushChallengeEvent pushChallengeEvent) {
            this.challenge.set(pushChallengeEvent.challenge);
            this.latch.countDown();
        }
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes4.dex */
    public static class PushChallengeEvent {
        private final String challenge;

        PushChallengeEvent(String str) {
            this.challenge = str;
        }
    }
}
