package org.thoughtcrime.securesms.registration.fragments;

import android.animation.Animator;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.lifecycle.Observer;
import androidx.navigation.Navigation;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.functions.Consumer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.registration.CallMeCountDownView;
import org.thoughtcrime.securesms.components.registration.VerificationCodeView;
import org.thoughtcrime.securesms.components.registration.VerificationPinKeyboard;
import org.thoughtcrime.securesms.registration.ReceivedSmsEvent;
import org.thoughtcrime.securesms.registration.RequestVerificationCodeResponseProcessor;
import org.thoughtcrime.securesms.registration.VerifyAccountRepository;
import org.thoughtcrime.securesms.registration.VerifyAccountResponseProcessor;
import org.thoughtcrime.securesms.registration.fragments.SignalStrengthPhoneStateListener;
import org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.SupportEmailUtil;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.concurrent.AssertedSuccessListener;

/* loaded from: classes.dex */
public abstract class BaseEnterSmsCodeFragment<ViewModel extends BaseRegistrationViewModel> extends LoggingFragment implements SignalStrengthPhoneStateListener.Callback {
    private static final String TAG = Log.tag(BaseEnterSmsCodeFragment.class);
    private boolean autoCompleting;
    private CallMeCountDownView callMeCountDown;
    protected final LifecycleDisposable disposables = new LifecycleDisposable();
    private TextView header;
    private VerificationPinKeyboard keyboard;
    private View noCodeReceivedHelp;
    private ScrollView scrollView;
    private View serviceWarning;
    private VerificationCodeView verificationCodeView;
    private ViewModel viewModel;
    private View wrongNumber;

    protected abstract ViewModel getViewModel();

    protected abstract void handleSuccessfulVerify();

    protected abstract void navigateToCaptcha();

    protected abstract void navigateToKbsAccountLocked();

    protected abstract void navigateToRegistrationLock(long j);

    public BaseEnterSmsCodeFragment(int i) {
        super(i);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        RegistrationViewDelegate.setDebugLogSubmitMultiTapView(view.findViewById(R.id.verify_header));
        this.scrollView = (ScrollView) view.findViewById(R.id.scroll_view);
        this.header = (TextView) view.findViewById(R.id.verify_header);
        this.verificationCodeView = (VerificationCodeView) view.findViewById(R.id.code);
        this.keyboard = (VerificationPinKeyboard) view.findViewById(R.id.keyboard);
        this.callMeCountDown = (CallMeCountDownView) view.findViewById(R.id.call_me_count_down);
        this.wrongNumber = view.findViewById(R.id.wrong_number);
        this.noCodeReceivedHelp = view.findViewById(R.id.no_code);
        this.serviceWarning = view.findViewById(R.id.cell_service_warning);
        new SignalStrengthPhoneStateListener(this, this);
        connectKeyboard(this.verificationCodeView, this.keyboard);
        ViewUtil.hideKeyboard(requireContext(), view);
        setOnCodeFullyEnteredListener(this.verificationCodeView);
        this.wrongNumber.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                BaseEnterSmsCodeFragment.this.lambda$onViewCreated$0(view2);
            }
        });
        this.callMeCountDown.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment$$ExternalSyntheticLambda5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                BaseEnterSmsCodeFragment.this.lambda$onViewCreated$1(view2);
            }
        });
        this.callMeCountDown.setListener(new CallMeCountDownView.Listener() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment$$ExternalSyntheticLambda6
            @Override // org.thoughtcrime.securesms.components.registration.CallMeCountDownView.Listener
            public final void onRemaining(CallMeCountDownView callMeCountDownView, int i) {
                BaseEnterSmsCodeFragment.this.lambda$onViewCreated$2(callMeCountDownView, i);
            }
        });
        this.noCodeReceivedHelp.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment$$ExternalSyntheticLambda7
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                BaseEnterSmsCodeFragment.this.lambda$onViewCreated$3(view2);
            }
        });
        this.disposables.bindTo(getViewLifecycleOwner().getLifecycle());
        ViewModel viewModel = getViewModel();
        this.viewModel = viewModel;
        viewModel.getSuccessfulCodeRequestAttempts().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment$$ExternalSyntheticLambda8
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                BaseEnterSmsCodeFragment.this.lambda$onViewCreated$5((Integer) obj);
            }
        });
        this.viewModel.onStartEnterCode();
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        onWrongNumber();
    }

    public /* synthetic */ void lambda$onViewCreated$1(View view) {
        handlePhoneCallRequest();
    }

    public /* synthetic */ void lambda$onViewCreated$2(CallMeCountDownView callMeCountDownView, int i) {
        if (i <= 30) {
            this.scrollView.smoothScrollTo(0, callMeCountDownView.getBottom());
            this.callMeCountDown.setListener(null);
        }
    }

    public /* synthetic */ void lambda$onViewCreated$3(View view) {
        sendEmailToSupport();
    }

    public /* synthetic */ void lambda$onViewCreated$5(Integer num) {
        if (num.intValue() >= 3) {
            this.noCodeReceivedHelp.setVisibility(0);
            this.scrollView.postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment$$ExternalSyntheticLambda3
                @Override // java.lang.Runnable
                public final void run() {
                    BaseEnterSmsCodeFragment.this.lambda$onViewCreated$4();
                }
            }, 15000);
        }
    }

    public /* synthetic */ void lambda$onViewCreated$4() {
        this.scrollView.smoothScrollTo(0, this.noCodeReceivedHelp.getBottom());
    }

    public void onWrongNumber() {
        Navigation.findNavController(requireView()).navigateUp();
    }

    private void setOnCodeFullyEnteredListener(VerificationCodeView verificationCodeView) {
        verificationCodeView.setOnCompleteListener(new VerificationCodeView.OnCodeEnteredListener() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.components.registration.VerificationCodeView.OnCodeEnteredListener
            public final void onCodeComplete(String str) {
                BaseEnterSmsCodeFragment.this.lambda$setOnCodeFullyEnteredListener$7(str);
            }
        });
    }

    public /* synthetic */ void lambda$setOnCodeFullyEnteredListener$7(String str) {
        this.callMeCountDown.setVisibility(4);
        this.wrongNumber.setVisibility(4);
        this.keyboard.displayProgress();
        this.disposables.add(this.viewModel.verifyCodeWithoutRegistrationLock(str).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment$$ExternalSyntheticLambda14
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                BaseEnterSmsCodeFragment.this.lambda$setOnCodeFullyEnteredListener$6((VerifyAccountResponseProcessor) obj);
            }
        }));
    }

    public /* synthetic */ void lambda$setOnCodeFullyEnteredListener$6(VerifyAccountResponseProcessor verifyAccountResponseProcessor) throws Throwable {
        if (!verifyAccountResponseProcessor.hasResult()) {
            Log.w(TAG, "post verify: ", verifyAccountResponseProcessor.getError());
        }
        if (verifyAccountResponseProcessor.hasResult()) {
            handleSuccessfulVerify();
        } else if (verifyAccountResponseProcessor.rateLimit()) {
            handleRateLimited();
        } else if (verifyAccountResponseProcessor.registrationLock() && !verifyAccountResponseProcessor.isKbsLocked()) {
            handleRegistrationLock(verifyAccountResponseProcessor.getLockedException().getTimeRemaining());
        } else if (verifyAccountResponseProcessor.isKbsLocked()) {
            handleKbsAccountLocked();
        } else if (verifyAccountResponseProcessor.authorizationFailed()) {
            handleIncorrectCodeError();
        } else {
            Log.w(TAG, "Unable to verify code", verifyAccountResponseProcessor.getError());
            handleGeneralError();
        }
    }

    public void displaySuccess(final Runnable runnable) {
        this.keyboard.displaySuccess().addListener(new AssertedSuccessListener<Boolean>() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment.1
            public void onSuccess(Boolean bool) {
                runnable.run();
            }
        });
    }

    protected void handleRateLimited() {
        this.keyboard.displayFailure().addListener(new AssertedSuccessListener<Boolean>() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment.2
            public void onSuccess(Boolean bool) {
                new MaterialAlertDialogBuilder(BaseEnterSmsCodeFragment.this.requireContext()).setTitle(R.string.RegistrationActivity_too_many_attempts).setMessage(R.string.RegistrationActivity_you_have_made_too_many_attempts_please_try_again_later).setPositiveButton(17039370, (DialogInterface.OnClickListener) new BaseEnterSmsCodeFragment$2$$ExternalSyntheticLambda0(this)).show();
            }

            public /* synthetic */ void lambda$onSuccess$0(DialogInterface dialogInterface, int i) {
                BaseEnterSmsCodeFragment.this.callMeCountDown.setVisibility(0);
                BaseEnterSmsCodeFragment.this.wrongNumber.setVisibility(0);
                BaseEnterSmsCodeFragment.this.verificationCodeView.clear();
                BaseEnterSmsCodeFragment.this.keyboard.displayKeyboard();
            }
        });
    }

    protected void handleRegistrationLock(final long j) {
        this.keyboard.displayLocked().addListener(new AssertedSuccessListener<Boolean>() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment.3
            public void onSuccess(Boolean bool) {
                BaseEnterSmsCodeFragment.this.navigateToRegistrationLock(j);
            }
        });
    }

    protected void handleKbsAccountLocked() {
        navigateToKbsAccountLocked();
    }

    protected void handleIncorrectCodeError() {
        Toast.makeText(requireContext(), (int) R.string.RegistrationActivity_incorrect_code, 1).show();
        this.keyboard.displayFailure().addListener(new AssertedSuccessListener<Boolean>() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment.4
            public void onSuccess(Boolean bool) {
                BaseEnterSmsCodeFragment.this.callMeCountDown.setVisibility(0);
                BaseEnterSmsCodeFragment.this.wrongNumber.setVisibility(0);
                BaseEnterSmsCodeFragment.this.verificationCodeView.clear();
                BaseEnterSmsCodeFragment.this.keyboard.displayKeyboard();
            }
        });
    }

    protected void handleGeneralError() {
        Toast.makeText(requireContext(), (int) R.string.RegistrationActivity_error_connecting_to_service, 1).show();
        this.keyboard.displayFailure().addListener(new AssertedSuccessListener<Boolean>() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment.5
            public void onSuccess(Boolean bool) {
                BaseEnterSmsCodeFragment.this.callMeCountDown.setVisibility(0);
                BaseEnterSmsCodeFragment.this.wrongNumber.setVisibility(0);
                BaseEnterSmsCodeFragment.this.verificationCodeView.clear();
                BaseEnterSmsCodeFragment.this.keyboard.displayKeyboard();
            }
        });
    }

    @Override // org.thoughtcrime.securesms.LoggingFragment, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override // org.thoughtcrime.securesms.LoggingFragment, androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onVerificationCodeReceived(ReceivedSmsEvent receivedSmsEvent) {
        this.verificationCodeView.clear();
        List<Integer> convertVerificationCodeToDigits = convertVerificationCodeToDigits(receivedSmsEvent.getCode());
        this.autoCompleting = true;
        int size = convertVerificationCodeToDigits.size();
        for (int i = 0; i < size; i++) {
            this.verificationCodeView.postDelayed(new Runnable(convertVerificationCodeToDigits, i, size) { // from class: org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment$$ExternalSyntheticLambda12
                public final /* synthetic */ List f$1;
                public final /* synthetic */ int f$2;
                public final /* synthetic */ int f$3;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    BaseEnterSmsCodeFragment.this.lambda$onVerificationCodeReceived$8(this.f$1, this.f$2, this.f$3);
                }
            }, ((long) i) * 200);
        }
    }

    public /* synthetic */ void lambda$onVerificationCodeReceived$8(List list, int i, int i2) {
        this.verificationCodeView.append(((Integer) list.get(i)).intValue());
        if (i == i2 - 1) {
            this.autoCompleting = false;
        }
    }

    private static List<Integer> convertVerificationCodeToDigits(String str) {
        if (str == null || str.length() != 6) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(str.length());
        for (int i = 0; i < str.length(); i++) {
            try {
                arrayList.add(Integer.valueOf(Integer.parseInt(Character.toString(str.charAt(i)))));
            } catch (NumberFormatException e) {
                Log.w(TAG, "Failed to convert code into digits.", e);
                return Collections.emptyList();
            }
        }
        return arrayList;
    }

    private void handlePhoneCallRequest() {
        RegistrationViewDelegate.showConfirmNumberDialogIfTranslated(requireContext(), R.string.RegistrationActivity_you_will_receive_a_call_to_verify_this_number, this.viewModel.getNumber().getE164Number(), new Runnable() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment$$ExternalSyntheticLambda9
            @Override // java.lang.Runnable
            public final void run() {
                BaseEnterSmsCodeFragment.this.handlePhoneCallRequestAfterConfirm();
            }
        }, new Runnable() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment$$ExternalSyntheticLambda10
            @Override // java.lang.Runnable
            public final void run() {
                BaseEnterSmsCodeFragment.this.onWrongNumber();
            }
        });
    }

    public void handlePhoneCallRequestAfterConfirm() {
        this.disposables.add(this.viewModel.requestVerificationCode(VerifyAccountRepository.Mode.PHONE_CALL).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment$$ExternalSyntheticLambda11
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                BaseEnterSmsCodeFragment.this.lambda$handlePhoneCallRequestAfterConfirm$9((RequestVerificationCodeResponseProcessor) obj);
            }
        }));
    }

    public /* synthetic */ void lambda$handlePhoneCallRequestAfterConfirm$9(RequestVerificationCodeResponseProcessor requestVerificationCodeResponseProcessor) throws Throwable {
        if (requestVerificationCodeResponseProcessor.hasResult()) {
            Toast.makeText(requireContext(), (int) R.string.RegistrationActivity_call_requested, 1).show();
        } else if (requestVerificationCodeResponseProcessor.captchaRequired()) {
            navigateToCaptcha();
        } else if (requestVerificationCodeResponseProcessor.rateLimit()) {
            Toast.makeText(requireContext(), (int) R.string.RegistrationActivity_rate_limited_to_service, 1).show();
        } else {
            Log.w(TAG, "Unable to request phone code", requestVerificationCodeResponseProcessor.getError());
            Toast.makeText(requireContext(), (int) R.string.RegistrationActivity_unable_to_connect_to_service, 1).show();
        }
    }

    private void connectKeyboard(VerificationCodeView verificationCodeView, VerificationPinKeyboard verificationPinKeyboard) {
        verificationPinKeyboard.setOnKeyPressListener(new VerificationPinKeyboard.OnKeyPressListener(verificationCodeView) { // from class: org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ VerificationCodeView f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.components.registration.VerificationPinKeyboard.OnKeyPressListener
            public final void onKeyPress(int i) {
                BaseEnterSmsCodeFragment.this.lambda$connectKeyboard$10(this.f$1, i);
            }
        });
    }

    public /* synthetic */ void lambda$connectKeyboard$10(VerificationCodeView verificationCodeView, int i) {
        if (this.autoCompleting) {
            return;
        }
        if (i >= 0) {
            verificationCodeView.append(i);
        } else {
            verificationCodeView.delete();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        this.header.setText(requireContext().getString(R.string.RegistrationActivity_enter_the_code_we_sent_to_s, this.viewModel.getNumber().getFullFormattedNumber()));
        this.viewModel.getCanCallAtTime().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment$$ExternalSyntheticLambda13
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                BaseEnterSmsCodeFragment.this.lambda$onResume$11((Long) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onResume$11(Long l) {
        this.callMeCountDown.startCountDownTo(l.longValue());
    }

    private void sendEmailToSupport() {
        CommunicationActions.openEmail(requireContext(), SupportEmailUtil.getSupportEmailAddress(requireContext()), getString(R.string.RegistrationActivity_code_support_subject), SupportEmailUtil.generateSupportEmailBody(requireContext(), R.string.RegistrationActivity_code_support_subject, null, null));
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.SignalStrengthPhoneStateListener.Callback
    public void onNoCellSignalPresent() {
        if (this.serviceWarning.getVisibility() != 0) {
            this.serviceWarning.setVisibility(0);
            this.serviceWarning.animate().alpha(1.0f).setListener(null).start();
            this.scrollView.postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment$$ExternalSyntheticLambda1
                @Override // java.lang.Runnable
                public final void run() {
                    BaseEnterSmsCodeFragment.this.lambda$onNoCellSignalPresent$12();
                }
            }, 1000);
        }
    }

    public /* synthetic */ void lambda$onNoCellSignalPresent$12() {
        if (this.serviceWarning.getVisibility() == 0) {
            this.scrollView.smoothScrollTo(0, this.serviceWarning.getBottom());
        }
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.SignalStrengthPhoneStateListener.Callback
    public void onCellSignalPresent() {
        if (this.serviceWarning.getVisibility() == 0) {
            this.serviceWarning.animate().alpha(0.0f).setListener(new Animator.AnimatorListener() { // from class: org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment.6
                @Override // android.animation.Animator.AnimatorListener
                public void onAnimationCancel(Animator animator) {
                }

                @Override // android.animation.Animator.AnimatorListener
                public void onAnimationRepeat(Animator animator) {
                }

                @Override // android.animation.Animator.AnimatorListener
                public void onAnimationStart(Animator animator) {
                }

                @Override // android.animation.Animator.AnimatorListener
                public void onAnimationEnd(Animator animator) {
                    BaseEnterSmsCodeFragment.this.serviceWarning.setVisibility(8);
                }
            }).start();
        }
    }
}
