package org.thoughtcrime.securesms.registration.fragments;

/* loaded from: classes4.dex */
final class RegistrationConstants {
    static final String SIGNAL_CAPTCHA_SCHEME;
    static final String TERMS_AND_CONDITIONS_URL;

    private RegistrationConstants() {
    }
}
