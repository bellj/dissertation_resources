package org.thoughtcrime.securesms.registration.fragments;

import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.SignupDirections;

/* loaded from: classes4.dex */
public class AccountLockedFragmentDirections {
    private AccountLockedFragmentDirections() {
    }

    public static NavDirections actionRestartToWelcomeFragment() {
        return SignupDirections.actionRestartToWelcomeFragment();
    }
}
