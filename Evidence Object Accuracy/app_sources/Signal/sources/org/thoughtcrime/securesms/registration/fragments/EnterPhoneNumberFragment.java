package org.thoughtcrime.securesms.registration.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber$PhoneNumber;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.LabeledEditText;
import org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberEnterPhoneNumberFragment$$ExternalSyntheticLambda4;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.registration.RequestVerificationCodeResponseProcessor;
import org.thoughtcrime.securesms.registration.VerifyAccountRepository;
import org.thoughtcrime.securesms.registration.util.RegistrationNumberInputController;
import org.thoughtcrime.securesms.registration.viewmodel.NumberViewState;
import org.thoughtcrime.securesms.registration.viewmodel.RegistrationViewModel;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.Dialogs;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.PlayServicesUtil;
import org.thoughtcrime.securesms.util.SupportEmailUtil;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;

/* loaded from: classes4.dex */
public final class EnterPhoneNumberFragment extends LoggingFragment implements RegistrationNumberInputController.Callbacks {
    private static final String TAG = Log.tag(EnterPhoneNumberFragment.class);
    private View cancel;
    private LabeledEditText countryCode;
    private Spinner countrySpinner;
    private final LifecycleDisposable disposables = new LifecycleDisposable();
    private LabeledEditText number;
    private CircularProgressMaterialButton register;
    private ScrollView scrollView;
    private RegistrationViewModel viewModel;

    @Override // org.thoughtcrime.securesms.registration.util.RegistrationNumberInputController.Callbacks
    public void onNumberInputNext(View view) {
    }

    @Override // org.thoughtcrime.securesms.LoggingFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setHasOptionsMenu(true);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_registration_enter_phone_number, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        RegistrationViewDelegate.setDebugLogSubmitMultiTapView(view.findViewById(R.id.verify_header));
        this.countryCode = (LabeledEditText) view.findViewById(R.id.country_code);
        this.number = (LabeledEditText) view.findViewById(R.id.number);
        this.countrySpinner = (Spinner) view.findViewById(R.id.country_spinner);
        this.cancel = view.findViewById(R.id.cancel_button);
        this.scrollView = (ScrollView) view.findViewById(R.id.scroll_view);
        this.register = (CircularProgressMaterialButton) view.findViewById(R.id.registerButton);
        RegistrationNumberInputController registrationNumberInputController = new RegistrationNumberInputController(requireContext(), this.countryCode, this.number, this.countrySpinner, true, this);
        this.register.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.registration.fragments.EnterPhoneNumberFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EnterPhoneNumberFragment.$r8$lambda$C9V7ph6kMwonn1Z2KazcWVzm2iY(EnterPhoneNumberFragment.this, view2);
            }
        });
        this.disposables.bindTo(getViewLifecycleOwner().getLifecycle());
        RegistrationViewModel registrationViewModel = (RegistrationViewModel) new ViewModelProvider(requireActivity()).get(RegistrationViewModel.class);
        this.viewModel = registrationViewModel;
        if (registrationViewModel.isReregister()) {
            this.cancel.setVisibility(0);
            this.cancel.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.registration.fragments.EnterPhoneNumberFragment$$ExternalSyntheticLambda1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    EnterPhoneNumberFragment.$r8$lambda$ozZ6_4wKNS4G1lHxVoRltfZSAzY(view2);
                }
            });
        } else {
            this.cancel.setVisibility(8);
        }
        this.viewModel.getLiveNumber().observe(getViewLifecycleOwner(), new ChangeNumberEnterPhoneNumberFragment$$ExternalSyntheticLambda4(registrationNumberInputController));
        if (this.viewModel.hasCaptchaToken()) {
            ThreadUtil.runOnMainDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.registration.fragments.EnterPhoneNumberFragment$$ExternalSyntheticLambda2
                @Override // java.lang.Runnable
                public final void run() {
                    EnterPhoneNumberFragment.$r8$lambda$iaIE2Y4eDre9LWVE92pSlvLxrr0(EnterPhoneNumberFragment.this);
                }
            }, 250);
        }
        ((AppCompatActivity) requireActivity()).setSupportActionBar((Toolbar) view.findViewById(R.id.toolbar));
        ((AppCompatActivity) requireActivity()).getSupportActionBar().setTitle((CharSequence) null);
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        handleRegister(requireContext());
    }

    public static /* synthetic */ void lambda$onViewCreated$1(View view) {
        Navigation.findNavController(view).navigateUp();
    }

    public /* synthetic */ void lambda$onViewCreated$2() {
        handleRegister(requireContext());
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.enter_phone_number, menu);
    }

    @Override // androidx.fragment.app.Fragment
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.phone_menu_use_proxy) {
            return false;
        }
        SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), EnterPhoneNumberFragmentDirections.actionEditProxy());
        return true;
    }

    private void handleRegister(Context context) {
        if (TextUtils.isEmpty(this.countryCode.getText())) {
            Toast.makeText(context, getString(R.string.RegistrationActivity_you_must_specify_your_country_code), 1).show();
        } else if (TextUtils.isEmpty(this.number.getText())) {
            Toast.makeText(context, getString(R.string.RegistrationActivity_you_must_specify_your_phone_number), 1).show();
        } else {
            NumberViewState number = this.viewModel.getNumber();
            String e164Number = number.getE164Number();
            if (!number.isValid()) {
                Dialogs.showAlertDialog(context, getString(R.string.RegistrationActivity_invalid_number), String.format(getString(R.string.RegistrationActivity_the_number_you_specified_s_is_invalid), e164Number));
                return;
            }
            PlayServicesUtil.PlayServicesStatus playServicesStatus = PlayServicesUtil.getPlayServicesStatus(context);
            if (playServicesStatus == PlayServicesUtil.PlayServicesStatus.SUCCESS) {
                confirmNumberPrompt(context, e164Number, new Runnable(context) { // from class: org.thoughtcrime.securesms.registration.fragments.EnterPhoneNumberFragment$$ExternalSyntheticLambda7
                    public final /* synthetic */ Context f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        EnterPhoneNumberFragment.$r8$lambda$0KHzmPBPya6I4iqTWWVWtGJ2tsg(EnterPhoneNumberFragment.this, this.f$1);
                    }
                });
            } else if (playServicesStatus == PlayServicesUtil.PlayServicesStatus.MISSING) {
                confirmNumberPrompt(context, e164Number, new Runnable(context) { // from class: org.thoughtcrime.securesms.registration.fragments.EnterPhoneNumberFragment$$ExternalSyntheticLambda8
                    public final /* synthetic */ Context f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        EnterPhoneNumberFragment.$r8$lambda$YMmoeItVcAtj6p9AAVUB6S4gjek(EnterPhoneNumberFragment.this, this.f$1);
                    }
                });
            } else if (playServicesStatus == PlayServicesUtil.PlayServicesStatus.NEEDS_UPDATE) {
                GoogleApiAvailability.getInstance().getErrorDialog(requireActivity(), 2, 0).show();
            } else {
                Dialogs.showAlertDialog(context, getString(R.string.RegistrationActivity_play_services_error), getString(R.string.RegistrationActivity_google_play_services_is_updating_or_unavailable));
            }
        }
    }

    public /* synthetic */ void lambda$handleRegister$3(Context context) {
        handleRequestVerification(context, true);
    }

    private void handleRequestVerification(Context context, boolean z) {
        this.register.setSpinning();
        disableAllEntries();
        if (z) {
            Task<Void> startSmsRetriever = SmsRetriever.getClient(context).startSmsRetriever();
            startSmsRetriever.addOnSuccessListener(new OnSuccessListener() { // from class: org.thoughtcrime.securesms.registration.fragments.EnterPhoneNumberFragment$$ExternalSyntheticLambda5
                @Override // com.google.android.gms.tasks.OnSuccessListener
                public final void onSuccess(Object obj) {
                    EnterPhoneNumberFragment.m2588$r8$lambda$dU97m7qpX_T_kfMumFvPBfmza8(EnterPhoneNumberFragment.this, (Void) obj);
                }
            });
            startSmsRetriever.addOnFailureListener(new OnFailureListener() { // from class: org.thoughtcrime.securesms.registration.fragments.EnterPhoneNumberFragment$$ExternalSyntheticLambda6
                @Override // com.google.android.gms.tasks.OnFailureListener
                public final void onFailure(Exception exc) {
                    EnterPhoneNumberFragment.m2586$r8$lambda$QOo9roJEMToWN8SDWC_dde0Kco(EnterPhoneNumberFragment.this, exc);
                }
            });
            return;
        }
        Log.i(TAG, "FCM is not supported, using no SMS listener");
        requestVerificationCode(VerifyAccountRepository.Mode.SMS_WITHOUT_LISTENER);
    }

    public /* synthetic */ void lambda$handleRequestVerification$5(Void r2) {
        Log.i(TAG, "Successfully registered SMS listener.");
        requestVerificationCode(VerifyAccountRepository.Mode.SMS_WITH_LISTENER);
    }

    public /* synthetic */ void lambda$handleRequestVerification$6(Exception exc) {
        Log.w(TAG, "Failed to register SMS listener.", exc);
        requestVerificationCode(VerifyAccountRepository.Mode.SMS_WITHOUT_LISTENER);
    }

    private void disableAllEntries() {
        this.countryCode.setEnabled(false);
        this.number.setEnabled(false);
        this.countrySpinner.setEnabled(false);
        this.cancel.setVisibility(8);
    }

    private void enableAllEntries() {
        this.countryCode.setEnabled(true);
        this.number.setEnabled(true);
        this.countrySpinner.setEnabled(true);
        if (this.viewModel.isReregister()) {
            this.cancel.setVisibility(0);
        }
    }

    private void requestVerificationCode(VerifyAccountRepository.Mode mode) {
        this.disposables.add(this.viewModel.requestVerificationCode(mode).doOnSubscribe(new Consumer() { // from class: org.thoughtcrime.securesms.registration.fragments.EnterPhoneNumberFragment$$ExternalSyntheticLambda13
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                EnterPhoneNumberFragment.m2583$r8$lambda$7Izz9yBRZKdQpEzJfIO0pNzkJY((Disposable) obj);
            }
        }).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer(NavHostFragment.findNavController(this), mode) { // from class: org.thoughtcrime.securesms.registration.fragments.EnterPhoneNumberFragment$$ExternalSyntheticLambda14
            public final /* synthetic */ NavController f$1;
            public final /* synthetic */ VerifyAccountRepository.Mode f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                EnterPhoneNumberFragment.m2584$r8$lambda$K9f0bMDOCWGbuVzbnDDHGLcMQ(EnterPhoneNumberFragment.this, this.f$1, this.f$2, (RequestVerificationCodeResponseProcessor) obj);
            }
        }));
    }

    public static /* synthetic */ void lambda$requestVerificationCode$7(Disposable disposable) throws Throwable {
        SignalStore.account().setRegistered(false);
    }

    public /* synthetic */ void lambda$requestVerificationCode$8(NavController navController, VerifyAccountRepository.Mode mode, RequestVerificationCodeResponseProcessor requestVerificationCodeResponseProcessor) throws Throwable {
        if (requestVerificationCodeResponseProcessor.hasResult()) {
            SafeNavigation.safeNavigate(navController, EnterPhoneNumberFragmentDirections.actionEnterVerificationCode());
        } else if (requestVerificationCodeResponseProcessor.localRateLimit()) {
            Log.i(TAG, "Unable to request sms code due to local rate limit");
            SafeNavigation.safeNavigate(navController, EnterPhoneNumberFragmentDirections.actionEnterVerificationCode());
        } else if (requestVerificationCodeResponseProcessor.captchaRequired()) {
            Log.i(TAG, "Unable to request sms code due to captcha required");
            SafeNavigation.safeNavigate(navController, EnterPhoneNumberFragmentDirections.actionRequestCaptcha());
        } else if (requestVerificationCodeResponseProcessor.rateLimit()) {
            Log.i(TAG, "Unable to request sms code due to rate limit");
            Toast.makeText(this.register.getContext(), (int) R.string.RegistrationActivity_rate_limited_to_service, 1).show();
        } else if (requestVerificationCodeResponseProcessor.isImpossibleNumber()) {
            Log.w(TAG, "Impossible number", requestVerificationCodeResponseProcessor.getError());
            Dialogs.showAlertDialog(requireContext(), getString(R.string.RegistrationActivity_invalid_number), String.format(getString(R.string.RegistrationActivity_the_number_you_specified_s_is_invalid), this.viewModel.getNumber().getFullFormattedNumber()));
        } else if (requestVerificationCodeResponseProcessor.isNonNormalizedNumber()) {
            handleNonNormalizedNumberError(requestVerificationCodeResponseProcessor.getOriginalNumber(), requestVerificationCodeResponseProcessor.getNormalizedNumber(), mode);
        } else {
            Log.i(TAG, "Unknown error during verification code request", requestVerificationCodeResponseProcessor.getError());
            Toast.makeText(this.register.getContext(), (int) R.string.RegistrationActivity_unable_to_connect_to_service, 1).show();
        }
        this.register.cancelSpinning();
        enableAllEntries();
    }

    public /* synthetic */ void lambda$onNumberFocused$9() {
        this.scrollView.smoothScrollTo(0, this.register.getBottom());
    }

    @Override // org.thoughtcrime.securesms.registration.util.RegistrationNumberInputController.Callbacks
    public void onNumberFocused() {
        this.scrollView.postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.registration.fragments.EnterPhoneNumberFragment$$ExternalSyntheticLambda15
            @Override // java.lang.Runnable
            public final void run() {
                EnterPhoneNumberFragment.m2587$r8$lambda$V0rBXGyKBZh2_cN1VkGjXYlDyQ(EnterPhoneNumberFragment.this);
            }
        }, 250);
    }

    @Override // org.thoughtcrime.securesms.registration.util.RegistrationNumberInputController.Callbacks
    public void onNumberInputDone(View view) {
        ViewUtil.hideKeyboard(requireContext(), view);
        handleRegister(requireContext());
    }

    @Override // org.thoughtcrime.securesms.registration.util.RegistrationNumberInputController.Callbacks
    public void onPickCountry(View view) {
        SafeNavigation.safeNavigate(Navigation.findNavController(view), (int) R.id.action_pickCountry);
    }

    @Override // org.thoughtcrime.securesms.registration.util.RegistrationNumberInputController.Callbacks
    public void setNationalNumber(String str) {
        this.viewModel.setNationalNumber(str);
    }

    @Override // org.thoughtcrime.securesms.registration.util.RegistrationNumberInputController.Callbacks
    public void setCountry(int i) {
        this.viewModel.onCountrySelected(null, i);
    }

    private void handleNonNormalizedNumberError(String str, String str2, VerifyAccountRepository.Mode mode) {
        try {
            new MaterialAlertDialogBuilder(requireContext()).setTitle(R.string.RegistrationActivity_non_standard_number_format).setMessage((CharSequence) getString(R.string.RegistrationActivity_the_number_you_entered_appears_to_be_a_non_standard, str, str2)).setNegativeButton(17039369, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.registration.fragments.EnterPhoneNumberFragment$$ExternalSyntheticLambda9
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    EnterPhoneNumberFragment.$r8$lambda$7Awft7fjbdwyurgUhnrzsggHa4I(dialogInterface, i);
                }
            }).setNeutralButton(R.string.RegistrationActivity_contact_signal_support, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.registration.fragments.EnterPhoneNumberFragment$$ExternalSyntheticLambda10
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    EnterPhoneNumberFragment.$r8$lambda$vRBZSwpW3tYloT8jFzqgMtI66ds(EnterPhoneNumberFragment.this, dialogInterface, i);
                }
            }).setPositiveButton(R.string.yes, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(PhoneNumberUtil.getInstance().parse(str2, null), mode) { // from class: org.thoughtcrime.securesms.registration.fragments.EnterPhoneNumberFragment$$ExternalSyntheticLambda11
                public final /* synthetic */ Phonenumber$PhoneNumber f$1;
                public final /* synthetic */ VerifyAccountRepository.Mode f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    EnterPhoneNumberFragment.m2589$r8$lambda$mDTFqjCMWDcLiS_Jac_0FaEl68(EnterPhoneNumberFragment.this, this.f$1, this.f$2, dialogInterface, i);
                }
            }).show();
        } catch (NumberParseException e) {
            Log.w(TAG, "Failed to parse number!", e);
            Dialogs.showAlertDialog(requireContext(), getString(R.string.RegistrationActivity_invalid_number), String.format(getString(R.string.RegistrationActivity_the_number_you_specified_s_is_invalid), this.viewModel.getNumber().getFullFormattedNumber()));
        }
    }

    public /* synthetic */ void lambda$handleNonNormalizedNumberError$11(DialogInterface dialogInterface, int i) {
        CommunicationActions.openEmail(requireContext(), SupportEmailUtil.getSupportEmailAddress(requireContext()), getString(R.string.RegistrationActivity_signal_android_phone_number_format), SupportEmailUtil.generateSupportEmailBody(requireContext(), R.string.RegistrationActivity_signal_android_phone_number_format, null, null));
        dialogInterface.dismiss();
    }

    public /* synthetic */ void lambda$handleNonNormalizedNumberError$12(Phonenumber$PhoneNumber phonenumber$PhoneNumber, VerifyAccountRepository.Mode mode, DialogInterface dialogInterface, int i) {
        this.countryCode.setText(String.valueOf(phonenumber$PhoneNumber.getCountryCode()));
        this.number.setText(String.valueOf(phonenumber$PhoneNumber.getNationalNumber()));
        requestVerificationCode(mode);
        dialogInterface.dismiss();
    }

    /* renamed from: handlePromptForNoPlayServices */
    public void lambda$handleRegister$4(Context context) {
        new MaterialAlertDialogBuilder(context).setTitle(R.string.RegistrationActivity_missing_google_play_services).setMessage(R.string.RegistrationActivity_this_device_is_missing_google_play_services).setPositiveButton(R.string.RegistrationActivity_i_understand, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(context) { // from class: org.thoughtcrime.securesms.registration.fragments.EnterPhoneNumberFragment$$ExternalSyntheticLambda12
            public final /* synthetic */ Context f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                EnterPhoneNumberFragment.m2591$r8$lambda$oyCsW9qiOdIOXEb0VB2mKVBvYk(EnterPhoneNumberFragment.this, this.f$1, dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).show();
    }

    public /* synthetic */ void lambda$handlePromptForNoPlayServices$13(Context context, DialogInterface dialogInterface, int i) {
        handleRequestVerification(context, false);
    }

    protected final void confirmNumberPrompt(Context context, String str, Runnable runnable) {
        RegistrationViewDelegate.showConfirmNumberDialogIfTranslated(context, R.string.RegistrationActivity_a_verification_code_will_be_sent_to, str, new Runnable(context, runnable) { // from class: org.thoughtcrime.securesms.registration.fragments.EnterPhoneNumberFragment$$ExternalSyntheticLambda3
            public final /* synthetic */ Context f$1;
            public final /* synthetic */ Runnable f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                EnterPhoneNumberFragment.m2590$r8$lambda$mZnK0UrqaeXGNMeb_h86rUxFLk(EnterPhoneNumberFragment.this, this.f$1, this.f$2);
            }
        }, new Runnable() { // from class: org.thoughtcrime.securesms.registration.fragments.EnterPhoneNumberFragment$$ExternalSyntheticLambda4
            @Override // java.lang.Runnable
            public final void run() {
                EnterPhoneNumberFragment.m2585$r8$lambda$OihnWu9uhIK29jfu9oyzSQH4Ac(EnterPhoneNumberFragment.this);
            }
        });
    }

    public /* synthetic */ void lambda$confirmNumberPrompt$14(Context context, Runnable runnable) {
        ViewUtil.hideKeyboard(context, this.number.getInput());
        runnable.run();
    }

    public /* synthetic */ void lambda$confirmNumberPrompt$15() {
        this.number.focusAndMoveCursorToEndAndOpenKeyboard();
    }
}
