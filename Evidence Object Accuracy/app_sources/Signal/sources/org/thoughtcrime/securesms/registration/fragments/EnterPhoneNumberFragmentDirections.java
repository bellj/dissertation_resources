package org.thoughtcrime.securesms.registration.fragments;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.SignupDirections;

/* loaded from: classes4.dex */
public class EnterPhoneNumberFragmentDirections {
    private EnterPhoneNumberFragmentDirections() {
    }

    public static NavDirections actionPickCountry() {
        return new ActionOnlyNavDirections(R.id.action_pickCountry);
    }

    public static NavDirections actionEnterVerificationCode() {
        return new ActionOnlyNavDirections(R.id.action_enterVerificationCode);
    }

    public static NavDirections actionRequestCaptcha() {
        return new ActionOnlyNavDirections(R.id.action_requestCaptcha);
    }

    public static NavDirections actionEditProxy() {
        return new ActionOnlyNavDirections(R.id.action_editProxy);
    }

    public static NavDirections actionRestartToWelcomeFragment() {
        return SignupDirections.actionRestartToWelcomeFragment();
    }
}
