package org.thoughtcrime.securesms.registration.fragments;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.core.text.HtmlCompat;
import androidx.navigation.Navigation;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.registration.fragments.ChooseBackupFragmentDirections;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* loaded from: classes4.dex */
public class ChooseBackupFragment extends LoggingFragment {
    private static final short OPEN_FILE_REQUEST_CODE;
    private static final String TAG = Log.tag(ChooseBackupFragment.class);

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_registration_choose_backup, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        view.findViewById(R.id.choose_backup_fragment_button).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.registration.fragments.ChooseBackupFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChooseBackupFragment.$r8$lambda$odnMbt6sUJFF_Fld0tKgDevRqTU(ChooseBackupFragment.this, view2);
            }
        });
        TextView textView = (TextView) view.findViewById(R.id.choose_backup_fragment_learn_more);
        textView.setText(HtmlCompat.fromHtml(String.format("<a href=\"%s\">%s</a>", getString(R.string.backup_support_url), getString(R.string.ChooseBackupFragment__learn_more)), 0));
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 3862 && i2 == -1 && intent != null) {
            ChooseBackupFragmentDirections.ActionRestore actionRestore = ChooseBackupFragmentDirections.actionRestore();
            actionRestore.setUri(intent.getData());
            SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), actionRestore);
        }
    }

    public void onChooseBackupSelected(View view) {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType(MediaUtil.OCTET);
        intent.addCategory("android.intent.category.OPENABLE");
        intent.putExtra("android.intent.extra.LOCAL_ONLY", true);
        if (Build.VERSION.SDK_INT >= 26) {
            intent.putExtra("android.provider.extra.INITIAL_URI", SignalStore.settings().getLatestSignalBackupDirectory());
        }
        try {
            startActivityForResult(intent, 3862);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(requireContext(), (int) R.string.ChooseBackupFragment__no_file_browser_available, 1).show();
            Log.w(TAG, "No matching activity!", e);
        }
    }
}
