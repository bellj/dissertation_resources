package org.thoughtcrime.securesms.registration.fragments;

import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import java.io.IOException;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.registration.viewmodel.RegistrationViewModel;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* loaded from: classes4.dex */
public final class EnterSmsCodeFragment extends BaseEnterSmsCodeFragment<RegistrationViewModel> {
    private static final String TAG = Log.tag(EnterSmsCodeFragment.class);

    public EnterSmsCodeFragment() {
        super(R.layout.fragment_registration_enter_code);
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment
    public RegistrationViewModel getViewModel() {
        return (RegistrationViewModel) ViewModelProviders.of(requireActivity()).get(RegistrationViewModel.class);
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment
    protected void handleSuccessfulVerify() {
        SimpleTask.run(new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.registration.fragments.EnterSmsCodeFragment$$ExternalSyntheticLambda0
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return EnterSmsCodeFragment.$r8$lambda$AeKY1b2WJr4zhu7rokyRwoqylio();
            }
        }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.registration.fragments.EnterSmsCodeFragment$$ExternalSyntheticLambda1
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                EnterSmsCodeFragment.m2592$r8$lambda$46KF5OK6ge3mO0sWTK92JfmZU(EnterSmsCodeFragment.this, obj);
            }
        });
    }

    public static /* synthetic */ Object lambda$handleSuccessfulVerify$0() {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            FeatureFlags.refreshSync();
            String str = TAG;
            Log.i(str, "Took " + (System.currentTimeMillis() - currentTimeMillis) + " ms to get feature flags.");
            return null;
        } catch (IOException e) {
            String str2 = TAG;
            Log.w(str2, "Failed to refresh flags after " + (System.currentTimeMillis() - currentTimeMillis) + " ms.", e);
            return null;
        }
    }

    public /* synthetic */ void lambda$handleSuccessfulVerify$1() {
        SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), EnterSmsCodeFragmentDirections.actionSuccessfulRegistration());
    }

    public /* synthetic */ void lambda$handleSuccessfulVerify$2(Object obj) {
        displaySuccess(new Runnable() { // from class: org.thoughtcrime.securesms.registration.fragments.EnterSmsCodeFragment$$ExternalSyntheticLambda2
            @Override // java.lang.Runnable
            public final void run() {
                EnterSmsCodeFragment.$r8$lambda$LQgVAiLz2AW4Ylgm9pLt7dkZbio(EnterSmsCodeFragment.this);
            }
        });
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment
    protected void navigateToRegistrationLock(long j) {
        SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), EnterSmsCodeFragmentDirections.actionRequireKbsLockPin(j));
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment
    protected void navigateToCaptcha() {
        SafeNavigation.safeNavigate(NavHostFragment.findNavController(this), EnterSmsCodeFragmentDirections.actionRequestCaptcha());
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment
    protected void navigateToKbsAccountLocked() {
        SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), RegistrationLockFragmentDirections.actionAccountLocked());
    }
}
