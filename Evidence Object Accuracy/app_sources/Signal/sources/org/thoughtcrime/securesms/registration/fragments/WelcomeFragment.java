package org.thoughtcrime.securesms.registration.fragments;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.ActivityNavigator;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber$PhoneNumber;
import j$.util.Optional;
import org.greenrobot.eventbus.EventBus;
import org.signal.core.util.logging.Log;
import org.signal.devicetransfer.DeviceToDeviceTransferService;
import org.signal.devicetransfer.TransferStatus;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.registration.fragments.RestoreBackupFragment;
import org.thoughtcrime.securesms.registration.viewmodel.RegistrationViewModel;
import org.thoughtcrime.securesms.util.BackupUtil;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;

/* loaded from: classes4.dex */
public final class WelcomeFragment extends LoggingFragment {
    private static final int[] HEADERS = {R.drawable.ic_contacts_white_48dp, R.drawable.ic_folder_white_48dp};
    private static final int[] HEADERS_API_29 = {R.drawable.ic_contacts_white_48dp};
    private static final String[] PERMISSIONS = {"android.permission.WRITE_CONTACTS", "android.permission.READ_CONTACTS", "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.READ_PHONE_STATE"};
    private static final String[] PERMISSIONS_API_26 = {"android.permission.WRITE_CONTACTS", "android.permission.READ_CONTACTS", "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE", "android.permission.READ_PHONE_STATE", "android.permission.READ_PHONE_NUMBERS"};
    private static final String[] PERMISSIONS_API_29 = {"android.permission.WRITE_CONTACTS", "android.permission.READ_CONTACTS", "android.permission.READ_PHONE_STATE", "android.permission.READ_PHONE_NUMBERS"};
    private static final int RATIONALE;
    private static final int RATIONALE_API_29;
    private static final String TAG = Log.tag(WelcomeFragment.class);
    private CircularProgressMaterialButton continueButton;
    private RegistrationViewModel viewModel;

    private static int getContinueRationale(boolean z) {
        return z ? R.string.RegistrationActivity_signal_needs_access_to_your_contacts_in_order_to_connect_with_friends : R.string.RegistrationActivity_signal_needs_access_to_your_contacts_and_media_in_order_to_connect_with_friends;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_registration_welcome, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        RegistrationViewModel registrationViewModel = (RegistrationViewModel) ViewModelProviders.of(requireActivity()).get(RegistrationViewModel.class);
        this.viewModel = registrationViewModel;
        if (!registrationViewModel.isReregister()) {
            RegistrationViewDelegate.setDebugLogSubmitMultiTapView(view.findViewById(R.id.image));
            RegistrationViewDelegate.setDebugLogSubmitMultiTapView(view.findViewById(R.id.title));
            CircularProgressMaterialButton circularProgressMaterialButton = (CircularProgressMaterialButton) view.findViewById(R.id.welcome_continue_button);
            this.continueButton = circularProgressMaterialButton;
            circularProgressMaterialButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.registration.fragments.WelcomeFragment$$ExternalSyntheticLambda2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    WelcomeFragment.$r8$lambda$XJa_5X59rodfxmjF8uWGgPp6nLQ(WelcomeFragment.this, view2);
                }
            });
            Button button = (Button) view.findViewById(R.id.welcome_transfer_or_restore);
            button.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.registration.fragments.WelcomeFragment$$ExternalSyntheticLambda3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    WelcomeFragment.$r8$lambda$RiMsDDPGnHnn6TXQIyDWjEAX_rw(WelcomeFragment.this, view2);
                }
            });
            ((TextView) view.findViewById(R.id.welcome_terms_button)).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.registration.fragments.WelcomeFragment$$ExternalSyntheticLambda4
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    WelcomeFragment.$r8$lambda$7lD3GdT5rEsxl5JQJkVtR92b8b8(WelcomeFragment.this, view2);
                }
            });
            if (!canUserSelectBackup()) {
                button.setText(R.string.registration_activity__transfer_account);
            }
        } else if (this.viewModel.hasRestoreFlowBeenShown()) {
            Log.i(TAG, "We've come back to the home fragment on a restore, user must be backing out");
            if (!Navigation.findNavController(view).popBackStack()) {
                FragmentActivity requireActivity = requireActivity();
                requireActivity.finish();
                ActivityNavigator.applyPopAnimationsToPendingTransition(requireActivity);
            }
        } else {
            initializeNumber();
            Log.i(TAG, "Skipping restore because this is a reregistration.");
            this.viewModel.setWelcomeSkippedOnRestore();
            SafeNavigation.safeNavigate(NavHostFragment.findNavController(this), WelcomeFragmentDirections.actionSkipRestore());
        }
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        onTermsClicked();
    }

    @Override // androidx.fragment.app.Fragment
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        Permissions.onRequestPermissionsResult(this, i, strArr, iArr);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        if (EventBus.getDefault().getStickyEvent(TransferStatus.class) != null) {
            Log.i(TAG, "Found existing transferStatus, redirect to transfer flow");
            SafeNavigation.safeNavigate(NavHostFragment.findNavController(this), (int) R.id.action_welcomeFragment_to_deviceTransferSetup);
            return;
        }
        DeviceToDeviceTransferService.stop(requireContext());
    }

    public void continueClicked(View view) {
        boolean isUserSelectionRequired = BackupUtil.isUserSelectionRequired(requireContext());
        Permissions.with(this).request(getContinuePermissions(isUserSelectionRequired)).ifNecessary().withRationaleDialog(getString(getContinueRationale(isUserSelectionRequired)), getContinueHeaders(isUserSelectionRequired)).onAnyResult(new Runnable() { // from class: org.thoughtcrime.securesms.registration.fragments.WelcomeFragment$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                WelcomeFragment.$r8$lambda$mb7YMno9cMfZUA9w0QKkZAV4hnk(WelcomeFragment.this);
            }
        }).execute();
    }

    public /* synthetic */ void lambda$continueClicked$1() {
        gatherInformationAndContinue(this.continueButton);
    }

    public void restoreFromBackupClicked(View view) {
        boolean isUserSelectionRequired = BackupUtil.isUserSelectionRequired(requireContext());
        Permissions.with(this).request(getContinuePermissions(isUserSelectionRequired)).ifNecessary().withRationaleDialog(getString(getContinueRationale(isUserSelectionRequired)), getContinueHeaders(isUserSelectionRequired)).onAnyResult(new Runnable() { // from class: org.thoughtcrime.securesms.registration.fragments.WelcomeFragment$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                WelcomeFragment.$r8$lambda$4S_JmlZecyl59X85Rlk3NILx_Kg(WelcomeFragment.this);
            }
        }).execute();
    }

    public /* synthetic */ void lambda$restoreFromBackupClicked$2() {
        gatherInformationAndChooseBackup(this.continueButton);
    }

    private void gatherInformationAndContinue(View view) {
        this.continueButton.setSpinning();
        RestoreBackupFragment.searchForBackup(new RestoreBackupFragment.OnBackupSearchResultListener() { // from class: org.thoughtcrime.securesms.registration.fragments.WelcomeFragment$$ExternalSyntheticLambda5
            @Override // org.thoughtcrime.securesms.registration.fragments.RestoreBackupFragment.OnBackupSearchResultListener
            public final void run(BackupUtil.BackupInfo backupInfo) {
                WelcomeFragment.$r8$lambda$xkW6GgOoWGNBDq81rv0579CwLyM(WelcomeFragment.this, backupInfo);
            }
        });
    }

    public /* synthetic */ void lambda$gatherInformationAndContinue$3(BackupUtil.BackupInfo backupInfo) {
        if (getContext() == null) {
            Log.i(TAG, "No context on fragment, must have navigated away.");
            return;
        }
        TextSecurePreferences.setHasSeenWelcomeScreen(requireContext(), true);
        initializeNumber();
        this.continueButton.cancelSpinning();
        if (backupInfo == null) {
            Log.i(TAG, "Skipping backup. No backup found, or no permission to look.");
            SafeNavigation.safeNavigate(NavHostFragment.findNavController(this), WelcomeFragmentDirections.actionSkipRestore());
            return;
        }
        SafeNavigation.safeNavigate(NavHostFragment.findNavController(this), WelcomeFragmentDirections.actionRestore());
    }

    private void gatherInformationAndChooseBackup(View view) {
        TextSecurePreferences.setHasSeenWelcomeScreen(requireContext(), true);
        initializeNumber();
        SafeNavigation.safeNavigate(NavHostFragment.findNavController(this), WelcomeFragmentDirections.actionTransferOrRestore());
    }

    private void initializeNumber() {
        Optional<Phonenumber$PhoneNumber> empty = Optional.empty();
        if (Permissions.hasAll(requireContext(), "android.permission.READ_PHONE_STATE", "android.permission.READ_PHONE_NUMBERS")) {
            empty = Util.getDeviceNumber(requireContext());
        } else {
            Log.i(TAG, "No phone permission");
        }
        if (empty.isPresent()) {
            Log.i(TAG, "Phone number detected");
            Phonenumber$PhoneNumber phonenumber$PhoneNumber = empty.get();
            this.viewModel.onNumberDetected(phonenumber$PhoneNumber.getCountryCode(), PhoneNumberUtil.getInstance().format(phonenumber$PhoneNumber, PhoneNumberUtil.PhoneNumberFormat.NATIONAL));
            return;
        }
        Log.i(TAG, "No number detected");
        Optional<String> simCountryIso = Util.getSimCountryIso(requireContext());
        if (simCountryIso.isPresent() && !TextUtils.isEmpty(simCountryIso.get())) {
            this.viewModel.onNumberDetected(PhoneNumberUtil.getInstance().getCountryCodeForRegion(simCountryIso.get()), "");
        }
    }

    private void onTermsClicked() {
        CommunicationActions.openBrowserLink(requireContext(), "https://signal.org/legal");
    }

    private boolean canUserSelectBackup() {
        return BackupUtil.isUserSelectionRequired(requireContext()) && !this.viewModel.isReregister() && !SignalStore.settings().isBackupEnabled();
    }

    private static String[] getContinuePermissions(boolean z) {
        if (z) {
            return PERMISSIONS_API_29;
        }
        if (Build.VERSION.SDK_INT >= 26) {
            return PERMISSIONS_API_26;
        }
        return PERMISSIONS;
    }

    private static int[] getContinueHeaders(boolean z) {
        return z ? HEADERS_API_29 : HEADERS;
    }
}
