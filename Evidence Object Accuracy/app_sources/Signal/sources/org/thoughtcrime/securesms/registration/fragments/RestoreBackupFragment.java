package org.thoughtcrime.securesms.registration.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.style.ReplacementSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import java.io.IOException;
import java.util.Locale;
import java.util.Objects;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.AppInitialization;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.backup.BackupPassphrase;
import org.thoughtcrime.securesms.backup.FullBackupBase;
import org.thoughtcrime.securesms.backup.FullBackupImporter;
import org.thoughtcrime.securesms.crypto.AttachmentSecretProvider;
import org.thoughtcrime.securesms.database.NoExternalStorageException;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.registration.fragments.RestoreBackupFragment;
import org.thoughtcrime.securesms.registration.viewmodel.RegistrationViewModel;
import org.thoughtcrime.securesms.service.LocalBackupListener;
import org.thoughtcrime.securesms.util.BackupUtil;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;

/* loaded from: classes.dex */
public final class RestoreBackupFragment extends LoggingFragment {
    private static final short OPEN_DOCUMENT_TREE_RESULT_CODE;
    private static final String TAG = Log.tag(RestoreBackupFragment.class);
    private TextView restoreBackupProgress;
    private TextView restoreBackupSize;
    private TextView restoreBackupTime;
    private CircularProgressMaterialButton restoreButton;
    private View skipRestoreButton;
    private RegistrationViewModel viewModel;

    /* loaded from: classes4.dex */
    public enum BackupImportResult {
        SUCCESS,
        FAILURE_VERSION_DOWNGRADE,
        FAILURE_UNKNOWN
    }

    /* loaded from: classes4.dex */
    public interface OnBackupSearchResultListener {
        void run(BackupUtil.BackupInfo backupInfo);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_registration_restore_backup, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        RegistrationViewDelegate.setDebugLogSubmitMultiTapView(view.findViewById(R.id.verify_header));
        String str = TAG;
        Log.i(str, "Backup restore.");
        this.restoreBackupSize = (TextView) view.findViewById(R.id.backup_size_text);
        this.restoreBackupTime = (TextView) view.findViewById(R.id.backup_created_text);
        this.restoreBackupProgress = (TextView) view.findViewById(R.id.backup_progress_text);
        this.restoreButton = (CircularProgressMaterialButton) view.findViewById(R.id.restore_button);
        View findViewById = view.findViewById(R.id.skip_restore_button);
        this.skipRestoreButton = findViewById;
        findViewById.setOnClickListener(new View.OnClickListener(view) { // from class: org.thoughtcrime.securesms.registration.fragments.RestoreBackupFragment$$ExternalSyntheticLambda4
            public final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                RestoreBackupFragment.lambda$onViewCreated$0(this.f$0, view2);
            }
        });
        RegistrationViewModel registrationViewModel = (RegistrationViewModel) new ViewModelProvider(requireActivity()).get(RegistrationViewModel.class);
        this.viewModel = registrationViewModel;
        if (registrationViewModel.isReregister()) {
            Log.i(str, "Skipping backup restore during re-register.");
            SafeNavigation.safeNavigate(Navigation.findNavController(view), RestoreBackupFragmentDirections.actionSkipNoReturn());
        } else if (this.viewModel.hasBackupCompleted()) {
            onBackupComplete();
        } else if (SignalStore.settings().isBackupEnabled()) {
            Log.i(str, "Backups enabled, so a backup must have been previously restored.");
            SafeNavigation.safeNavigate(Navigation.findNavController(view), RestoreBackupFragmentDirections.actionSkipNoReturn());
        } else {
            RestoreBackupFragmentArgs fromBundle = RestoreBackupFragmentArgs.fromBundle(requireArguments());
            if ((Build.VERSION.SDK_INT < 29 || BackupUtil.isUserSelectionRequired(requireContext())) && fromBundle.getUri() != null) {
                Log.i(str, "Restoring backup from passed uri");
                initializeBackupForUri(view, fromBundle.getUri());
            } else if (BackupUtil.canUserAccessBackupDirectory(requireContext())) {
                initializeBackupDetection(view);
            } else {
                Log.i(str, "Skipping backup detection. We don't have the permission.");
                SafeNavigation.safeNavigate(Navigation.findNavController(view), RestoreBackupFragmentDirections.actionSkipNoReturn());
            }
        }
    }

    public static /* synthetic */ void lambda$onViewCreated$0(View view, View view2) {
        Log.i(TAG, "User skipped backup restore.");
        SafeNavigation.safeNavigate(Navigation.findNavController(view), RestoreBackupFragmentDirections.actionSkip());
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 13782 && i2 == -1 && intent != null && intent.getData() != null) {
            Uri data = intent.getData();
            SignalStore.settings().setSignalBackupDirectory(data);
            requireContext().getContentResolver().takePersistableUriPermission(data, 3);
            enableBackups(requireContext());
            SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), RestoreBackupFragmentDirections.actionBackupRestored());
        }
    }

    private void initializeBackupForUri(View view, Uri uri) {
        getFromUri(requireContext(), uri, new OnBackupSearchResultListener(view) { // from class: org.thoughtcrime.securesms.registration.fragments.RestoreBackupFragment$$ExternalSyntheticLambda3
            public final /* synthetic */ View f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.registration.fragments.RestoreBackupFragment.OnBackupSearchResultListener
            public final void run(BackupUtil.BackupInfo backupInfo) {
                RestoreBackupFragment.this.lambda$initializeBackupForUri$1(this.f$1, backupInfo);
            }
        });
    }

    private void initializeBackupDetection(View view) {
        searchForBackup(new OnBackupSearchResultListener(view) { // from class: org.thoughtcrime.securesms.registration.fragments.RestoreBackupFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ View f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.registration.fragments.RestoreBackupFragment.OnBackupSearchResultListener
            public final void run(BackupUtil.BackupInfo backupInfo) {
                RestoreBackupFragment.this.lambda$initializeBackupDetection$2(this.f$1, backupInfo);
            }
        });
    }

    /* renamed from: handleBackupInfo */
    public void lambda$initializeBackupForUri$1(View view, BackupUtil.BackupInfo backupInfo) {
        if (getContext() == null) {
            Log.i(TAG, "No context on fragment, must have navigated away.");
        } else if (backupInfo == null) {
            Log.i(TAG, "Skipping backup detection. No backup found, or permission revoked since.");
            SafeNavigation.safeNavigate(Navigation.findNavController(view), RestoreBackupFragmentDirections.actionNoBackupFound());
        } else {
            this.restoreBackupSize.setText(getString(R.string.RegistrationActivity_backup_size_s, Util.getPrettyFileSize(backupInfo.getSize())));
            this.restoreBackupTime.setText(getString(R.string.RegistrationActivity_backup_timestamp_s, DateUtils.getExtendedRelativeTimeSpanString(requireContext(), Locale.getDefault(), backupInfo.getTimestamp())));
            this.restoreButton.setOnClickListener(new View.OnClickListener(backupInfo) { // from class: org.thoughtcrime.securesms.registration.fragments.RestoreBackupFragment$$ExternalSyntheticLambda5
                public final /* synthetic */ BackupUtil.BackupInfo f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    RestoreBackupFragment.this.lambda$handleBackupInfo$3(this.f$1, view2);
                }
            });
        }
    }

    public /* synthetic */ void lambda$handleBackupInfo$3(BackupUtil.BackupInfo backupInfo, View view) {
        handleRestore(view.getContext(), backupInfo);
    }

    public static void searchForBackup(final OnBackupSearchResultListener onBackupSearchResultListener) {
        new AsyncTask<Void, Void, BackupUtil.BackupInfo>() { // from class: org.thoughtcrime.securesms.registration.fragments.RestoreBackupFragment.1
            public BackupUtil.BackupInfo doInBackground(Void... voidArr) {
                try {
                    return BackupUtil.getLatestBackup();
                } catch (NoExternalStorageException e) {
                    Log.w(RestoreBackupFragment.TAG, e);
                    return null;
                }
            }

            public void onPostExecute(BackupUtil.BackupInfo backupInfo) {
                onBackupSearchResultListener.run(backupInfo);
            }
        }.execute(new Void[0]);
    }

    static void getFromUri(Context context, Uri uri, OnBackupSearchResultListener onBackupSearchResultListener) {
        RestoreBackupFragment$$ExternalSyntheticLambda8 restoreBackupFragment$$ExternalSyntheticLambda8 = new SimpleTask.BackgroundTask(context, uri) { // from class: org.thoughtcrime.securesms.registration.fragments.RestoreBackupFragment$$ExternalSyntheticLambda8
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ Uri f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return RestoreBackupFragment.lambda$getFromUri$4(this.f$0, this.f$1);
            }
        };
        Objects.requireNonNull(onBackupSearchResultListener);
        SimpleTask.run(restoreBackupFragment$$ExternalSyntheticLambda8, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.registration.fragments.RestoreBackupFragment$$ExternalSyntheticLambda9
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                RestoreBackupFragment.OnBackupSearchResultListener.this.run((BackupUtil.BackupInfo) obj);
            }
        });
    }

    public static /* synthetic */ BackupUtil.BackupInfo lambda$getFromUri$4(Context context, Uri uri) {
        try {
            return BackupUtil.getBackupInfoFromSingleUri(context, uri);
        } catch (BackupUtil.BackupFileException e) {
            Log.w(TAG, "Could not restore backup.", e);
            postToastForBackupRestorationFailure(context, e);
            return null;
        }
    }

    private static void postToastForBackupRestorationFailure(Context context, BackupUtil.BackupFileException backupFileException) {
        int i = AnonymousClass3.$SwitchMap$org$thoughtcrime$securesms$util$BackupUtil$BackupFileState[backupFileException.getState().ordinal()];
        if (i != 1) {
            ThreadUtil.postToMain(new Runnable(context, i != 2 ? i != 3 ? R.string.RestoreBackupFragment__backup_could_not_be_read : R.string.RestoreBackupFragment__backup_has_a_bad_extension : R.string.RestoreBackupFragment__backup_not_found) { // from class: org.thoughtcrime.securesms.registration.fragments.RestoreBackupFragment$$ExternalSyntheticLambda0
                public final /* synthetic */ Context f$0;
                public final /* synthetic */ int f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    RestoreBackupFragment.lambda$postToastForBackupRestorationFailure$5(this.f$0, this.f$1);
                }
            });
            return;
        }
        throw new AssertionError("Unexpected error state.");
    }

    public static /* synthetic */ void lambda$postToastForBackupRestorationFailure$5(Context context, int i) {
        Toast.makeText(context, i, 1).show();
    }

    private void handleRestore(Context context, BackupUtil.BackupInfo backupInfo) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.enter_backup_passphrase_dialog, (ViewGroup) null);
        EditText editText = (EditText) inflate.findViewById(R.id.restore_passphrase_input);
        editText.addTextChangedListener(new PassphraseAsYouTypeFormatter());
        new AlertDialog.Builder(context).setTitle(R.string.RegistrationActivity_enter_backup_passphrase).setView(inflate).setPositiveButton(R.string.RegistrationActivity_restore, new DialogInterface.OnClickListener(context, editText, backupInfo) { // from class: org.thoughtcrime.securesms.registration.fragments.RestoreBackupFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ Context f$1;
            public final /* synthetic */ EditText f$2;
            public final /* synthetic */ BackupUtil.BackupInfo f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                RestoreBackupFragment.this.lambda$handleRestore$6(this.f$1, this.f$2, this.f$3, dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).show();
        Log.i(TAG, "Prompt for backup passphrase shown to user.");
    }

    public /* synthetic */ void lambda$handleRestore$6(Context context, EditText editText, BackupUtil.BackupInfo backupInfo, DialogInterface dialogInterface, int i) {
        ((InputMethodManager) context.getSystemService("input_method")).hideSoftInputFromWindow(editText.getWindowToken(), 0);
        this.restoreButton.setSpinning();
        this.skipRestoreButton.setVisibility(4);
        restoreAsynchronously(context, backupInfo, editText.getText().toString());
    }

    private void restoreAsynchronously(final Context context, final BackupUtil.BackupInfo backupInfo, final String str) {
        new AsyncTask<Void, Void, BackupImportResult>() { // from class: org.thoughtcrime.securesms.registration.fragments.RestoreBackupFragment.2
            public BackupImportResult doInBackground(Void... voidArr) {
                try {
                    Log.i(RestoreBackupFragment.TAG, "Starting backup restore.");
                    SQLiteDatabase backupDatabase = SignalDatabase.getBackupDatabase();
                    BackupPassphrase.set(context, str);
                    Context context2 = context;
                    FullBackupImporter.importFile(context2, AttachmentSecretProvider.getInstance(context2).getOrCreateAttachmentSecret(), backupDatabase, backupInfo.getUri(), str);
                    SignalDatabase.upgradeRestored(backupDatabase);
                    NotificationChannels.restoreContactNotificationChannels(context);
                    RestoreBackupFragment.this.enableBackups(context);
                    AppInitialization.onPostBackupRestore(context);
                    Log.i(RestoreBackupFragment.TAG, "Backup restore complete.");
                    return BackupImportResult.SUCCESS;
                } catch (FullBackupImporter.DatabaseDowngradeException e) {
                    Log.w(RestoreBackupFragment.TAG, "Failed due to the backup being from a newer version of Signal.", e);
                    return BackupImportResult.FAILURE_VERSION_DOWNGRADE;
                } catch (IOException e2) {
                    Log.w(RestoreBackupFragment.TAG, e2);
                    return BackupImportResult.FAILURE_UNKNOWN;
                }
            }

            public void onPostExecute(BackupImportResult backupImportResult) {
                RestoreBackupFragment.this.viewModel.markBackupCompleted();
                RestoreBackupFragment.this.restoreButton.cancelSpinning();
                RestoreBackupFragment.this.skipRestoreButton.setVisibility(0);
                RestoreBackupFragment.this.restoreBackupProgress.setText("");
                int i = AnonymousClass3.$SwitchMap$org$thoughtcrime$securesms$registration$fragments$RestoreBackupFragment$BackupImportResult[backupImportResult.ordinal()];
                if (i == 1) {
                    Log.i(RestoreBackupFragment.TAG, "Successful backup restore.");
                } else if (i == 2) {
                    Toast.makeText(context, (int) R.string.RegistrationActivity_backup_failure_downgrade, 1).show();
                } else if (i == 3) {
                    Toast.makeText(context, (int) R.string.RegistrationActivity_incorrect_backup_passphrase, 1).show();
                }
            }
        }.execute(new Void[0]);
    }

    /* renamed from: org.thoughtcrime.securesms.registration.fragments.RestoreBackupFragment$3 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass3 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$registration$fragments$RestoreBackupFragment$BackupImportResult;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$util$BackupUtil$BackupFileState;

        static {
            int[] iArr = new int[BackupImportResult.values().length];
            $SwitchMap$org$thoughtcrime$securesms$registration$fragments$RestoreBackupFragment$BackupImportResult = iArr;
            try {
                iArr[BackupImportResult.SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$registration$fragments$RestoreBackupFragment$BackupImportResult[BackupImportResult.FAILURE_VERSION_DOWNGRADE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$registration$fragments$RestoreBackupFragment$BackupImportResult[BackupImportResult.FAILURE_UNKNOWN.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            int[] iArr2 = new int[BackupUtil.BackupFileState.values().length];
            $SwitchMap$org$thoughtcrime$securesms$util$BackupUtil$BackupFileState = iArr2;
            try {
                iArr2[BackupUtil.BackupFileState.READABLE.ordinal()] = 1;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$util$BackupUtil$BackupFileState[BackupUtil.BackupFileState.NOT_FOUND.ordinal()] = 2;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$util$BackupUtil$BackupFileState[BackupUtil.BackupFileState.UNSUPPORTED_FILE_EXTENSION.ordinal()] = 3;
            } catch (NoSuchFieldError unused6) {
            }
        }
    }

    @Override // org.thoughtcrime.securesms.LoggingFragment, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        RegistrationViewModel registrationViewModel = this.viewModel;
        if (registrationViewModel != null && registrationViewModel.hasBackupCompleted()) {
            onBackupComplete();
        }
    }

    @Override // org.thoughtcrime.securesms.LoggingFragment, androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(FullBackupBase.BackupEvent backupEvent) {
        long count = backupEvent.getCount();
        if (count == 0) {
            this.restoreBackupProgress.setText(R.string.RegistrationActivity_checking);
        } else {
            this.restoreBackupProgress.setText(getString(R.string.RegistrationActivity_d_messages_so_far, Long.valueOf(count)));
        }
        this.restoreButton.setSpinning();
        this.skipRestoreButton.setVisibility(4);
        if (backupEvent.getType() == FullBackupBase.BackupEvent.Type.FINISHED) {
            onBackupComplete();
        }
    }

    private void onBackupComplete() {
        if (!BackupUtil.isUserSelectionRequired(requireContext()) || BackupUtil.canUserAccessBackupDirectory(requireContext())) {
            SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), RestoreBackupFragmentDirections.actionBackupRestored());
        } else {
            displayConfirmationDialog(requireContext());
        }
    }

    public void enableBackups(Context context) {
        if (BackupUtil.canUserAccessBackupDirectory(context)) {
            LocalBackupListener.setNextBackupTimeToIntervalFromNow(context);
            SignalStore.settings().setBackupEnabled(true);
            LocalBackupListener.schedule(context);
        }
    }

    private void displayConfirmationDialog(Context context) {
        new AlertDialog.Builder(context).setTitle(R.string.RestoreBackupFragment__restore_complete).setMessage(R.string.RestoreBackupFragment__to_continue_using_backups_please_choose_a_folder).setPositiveButton(R.string.RestoreBackupFragment__choose_folder, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.registration.fragments.RestoreBackupFragment$$ExternalSyntheticLambda6
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                RestoreBackupFragment.this.lambda$displayConfirmationDialog$7(dialogInterface, i);
            }
        }).setNegativeButton(R.string.RestoreBackupFragment__not_now, new DialogInterface.OnClickListener(context) { // from class: org.thoughtcrime.securesms.registration.fragments.RestoreBackupFragment$$ExternalSyntheticLambda7
            public final /* synthetic */ Context f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                RestoreBackupFragment.this.lambda$displayConfirmationDialog$8(this.f$1, dialogInterface, i);
            }
        }).setCancelable(false).show();
    }

    public /* synthetic */ void lambda$displayConfirmationDialog$7(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent("android.intent.action.OPEN_DOCUMENT_TREE");
        intent.addFlags(67);
        startActivityForResult(intent, 13782);
    }

    public /* synthetic */ void lambda$displayConfirmationDialog$8(Context context, DialogInterface dialogInterface, int i) {
        BackupPassphrase.set(context, null);
        dialogInterface.dismiss();
        SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), RestoreBackupFragmentDirections.actionBackupRestored());
    }

    /* loaded from: classes4.dex */
    public static class PassphraseAsYouTypeFormatter implements TextWatcher {
        private static final int GROUP_SIZE;

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            removeSpans(editable);
            addSpans(editable);
        }

        private static void removeSpans(Editable editable) {
            for (SpaceSpan spaceSpan : (SpaceSpan[]) editable.getSpans(0, editable.length(), SpaceSpan.class)) {
                editable.removeSpan(spaceSpan);
            }
        }

        private static void addSpans(Editable editable) {
            int length = editable.length();
            for (int i = 5; i < length; i += 5) {
                editable.setSpan(new SpaceSpan(), i - 1, i, 33);
            }
            if (editable.length() > 30) {
                editable.delete(30, editable.length());
            }
        }
    }

    /* loaded from: classes4.dex */
    public static class SpaceSpan extends ReplacementSpan {
        private SpaceSpan() {
        }

        @Override // android.text.style.ReplacementSpan
        public int getSize(Paint paint, CharSequence charSequence, int i, int i2, Paint.FontMetricsInt fontMetricsInt) {
            return (int) (paint.measureText(charSequence, i, i2) * 1.7f);
        }

        @Override // android.text.style.ReplacementSpan
        public void draw(Canvas canvas, CharSequence charSequence, int i, int i2, float f, int i3, int i4, int i5, Paint paint) {
            canvas.drawText(charSequence.subSequence(i, i2).toString(), f, (float) i4, paint);
        }
    }
}
