package org.thoughtcrime.securesms.registration.fragments;

import android.os.Bundle;
import java.util.HashMap;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;

/* loaded from: classes4.dex */
public class CountryPickerFragmentArgs {
    private final HashMap arguments;

    private CountryPickerFragmentArgs() {
        this.arguments = new HashMap();
    }

    private CountryPickerFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static CountryPickerFragmentArgs fromBundle(Bundle bundle) {
        CountryPickerFragmentArgs countryPickerFragmentArgs = new CountryPickerFragmentArgs();
        bundle.setClassLoader(CountryPickerFragmentArgs.class.getClassLoader());
        if (bundle.containsKey(MultiselectForwardFragment.RESULT_KEY)) {
            countryPickerFragmentArgs.arguments.put(MultiselectForwardFragment.RESULT_KEY, bundle.getString(MultiselectForwardFragment.RESULT_KEY));
        } else {
            countryPickerFragmentArgs.arguments.put(MultiselectForwardFragment.RESULT_KEY, null);
        }
        return countryPickerFragmentArgs;
    }

    public String getResultKey() {
        return (String) this.arguments.get(MultiselectForwardFragment.RESULT_KEY);
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey(MultiselectForwardFragment.RESULT_KEY)) {
            bundle.putString(MultiselectForwardFragment.RESULT_KEY, (String) this.arguments.get(MultiselectForwardFragment.RESULT_KEY));
        } else {
            bundle.putString(MultiselectForwardFragment.RESULT_KEY, null);
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        CountryPickerFragmentArgs countryPickerFragmentArgs = (CountryPickerFragmentArgs) obj;
        if (this.arguments.containsKey(MultiselectForwardFragment.RESULT_KEY) != countryPickerFragmentArgs.arguments.containsKey(MultiselectForwardFragment.RESULT_KEY)) {
            return false;
        }
        return getResultKey() == null ? countryPickerFragmentArgs.getResultKey() == null : getResultKey().equals(countryPickerFragmentArgs.getResultKey());
    }

    public int hashCode() {
        return 31 + (getResultKey() != null ? getResultKey().hashCode() : 0);
    }

    public String toString() {
        return "CountryPickerFragmentArgs{resultKey=" + getResultKey() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(CountryPickerFragmentArgs countryPickerFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(countryPickerFragmentArgs.arguments);
        }

        public Builder() {
            this.arguments = new HashMap();
        }

        public CountryPickerFragmentArgs build() {
            return new CountryPickerFragmentArgs(this.arguments);
        }

        public Builder setResultKey(String str) {
            this.arguments.put(MultiselectForwardFragment.RESULT_KEY, str);
            return this;
        }

        public String getResultKey() {
            return (String) this.arguments.get(MultiselectForwardFragment.RESULT_KEY);
        }
    }
}
