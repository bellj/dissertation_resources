package org.thoughtcrime.securesms.registration;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.pin.TokenData;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.push.VerifyAccountResponse;

/* compiled from: VerifyAccountResponseProcessor.kt */
@Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u001b\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\b\u0010\n\u001a\u00020\u000bH\u0016R\u0014\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/registration/VerifyAccountResponseWithSuccessfulKbs;", "Lorg/thoughtcrime/securesms/registration/VerifyAccountResponseProcessor;", "response", "Lorg/whispersystems/signalservice/internal/ServiceResponse;", "Lorg/whispersystems/signalservice/internal/push/VerifyAccountResponse;", "tokenData", "Lorg/thoughtcrime/securesms/pin/TokenData;", "(Lorg/whispersystems/signalservice/internal/ServiceResponse;Lorg/thoughtcrime/securesms/pin/TokenData;)V", "getTokenData", "()Lorg/thoughtcrime/securesms/pin/TokenData;", "isKbsLocked", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class VerifyAccountResponseWithSuccessfulKbs extends VerifyAccountResponseProcessor {
    private final TokenData tokenData;

    @Override // org.thoughtcrime.securesms.registration.VerifyAccountResponseProcessor
    public TokenData getTokenData() {
        return this.tokenData;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public VerifyAccountResponseWithSuccessfulKbs(ServiceResponse<VerifyAccountResponse> serviceResponse, TokenData tokenData) {
        super(serviceResponse, null);
        Intrinsics.checkNotNullParameter(serviceResponse, "response");
        Intrinsics.checkNotNullParameter(tokenData, "tokenData");
        this.tokenData = tokenData;
    }

    @Override // org.thoughtcrime.securesms.registration.VerifyAccountResponseProcessor
    public boolean isKbsLocked() {
        return registrationLock() && getTokenData().getTriesRemaining() == 0;
    }
}
