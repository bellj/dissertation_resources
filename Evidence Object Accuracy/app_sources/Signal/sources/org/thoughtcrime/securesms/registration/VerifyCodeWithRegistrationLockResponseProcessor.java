package org.thoughtcrime.securesms.registration;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.pin.KeyBackupSystemWrongPinException;
import org.thoughtcrime.securesms.pin.TokenData;
import org.thoughtcrime.securesms.registration.VerifyAccountRepository;
import org.whispersystems.signalservice.api.KeyBackupSystemNoDataException;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.ServiceResponseProcessor;
import org.whispersystems.signalservice.internal.contacts.entities.TokenResponse;
import org.whispersystems.signalservice.internal.push.VerifyAccountResponse;

/* compiled from: VerifyCodeWithRegistrationLockResponseProcessor.kt */
@Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0003\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003B\u001b\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\n\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0016J\u0006\u0010\r\u001a\u00020\u000eJ\u0006\u0010\u000f\u001a\u00020\u0010J\b\u0010\u0011\u001a\u00020\u0010H\u0016J\b\u0010\u0012\u001a\u00020\u0010H\u0016J\b\u0010\u0013\u001a\u00020\u0010H\u0016J\u0014\u0010\u0014\u001a\u00020\u00002\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00150\u0005J\u0006\u0010\u0016\u001a\u00020\u0010R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/registration/VerifyCodeWithRegistrationLockResponseProcessor;", "Lorg/whispersystems/signalservice/internal/ServiceResponseProcessor;", "Lorg/thoughtcrime/securesms/registration/VerifyAccountRepository$VerifyAccountWithRegistrationLockResponse;", "Lorg/thoughtcrime/securesms/registration/VerifyProcessor;", "response", "Lorg/whispersystems/signalservice/internal/ServiceResponse;", "token", "Lorg/thoughtcrime/securesms/pin/TokenData;", "(Lorg/whispersystems/signalservice/internal/ServiceResponse;Lorg/thoughtcrime/securesms/pin/TokenData;)V", "getToken", "()Lorg/thoughtcrime/securesms/pin/TokenData;", "getError", "", "getTokenResponse", "Lorg/whispersystems/signalservice/internal/contacts/entities/TokenResponse;", "isKbsLocked", "", "isServerSentError", "rateLimit", "registrationLock", "updatedIfRegistrationFailed", "Lorg/whispersystems/signalservice/internal/push/VerifyAccountResponse;", "wrongPin", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class VerifyCodeWithRegistrationLockResponseProcessor extends ServiceResponseProcessor<VerifyAccountRepository.VerifyAccountWithRegistrationLockResponse> implements VerifyProcessor {
    private final TokenData token;

    public final TokenData getToken() {
        return this.token;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public VerifyCodeWithRegistrationLockResponseProcessor(ServiceResponse<VerifyAccountRepository.VerifyAccountWithRegistrationLockResponse> serviceResponse, TokenData tokenData) {
        super(serviceResponse);
        Intrinsics.checkNotNullParameter(serviceResponse, "response");
        Intrinsics.checkNotNullParameter(tokenData, "token");
        this.token = tokenData;
    }

    @Override // org.whispersystems.signalservice.internal.ServiceResponseProcessor
    public boolean rateLimit() {
        return super.rateLimit();
    }

    @Override // org.whispersystems.signalservice.internal.ServiceResponseProcessor
    public Throwable getError() {
        return super.getError();
    }

    @Override // org.whispersystems.signalservice.internal.ServiceResponseProcessor
    public boolean registrationLock() {
        return super.registrationLock();
    }

    public final boolean wrongPin() {
        return getError() instanceof KeyBackupSystemWrongPinException;
    }

    public final TokenResponse getTokenResponse() {
        Throwable error = getError();
        if (error != null) {
            TokenResponse tokenResponse = ((KeyBackupSystemWrongPinException) error).getTokenResponse();
            Intrinsics.checkNotNullExpressionValue(tokenResponse, "error as KeyBackupSystem…nException).tokenResponse");
            return tokenResponse;
        }
        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.pin.KeyBackupSystemWrongPinException");
    }

    public final boolean isKbsLocked() {
        return getError() instanceof KeyBackupSystemNoDataException;
    }

    public final VerifyCodeWithRegistrationLockResponseProcessor updatedIfRegistrationFailed(ServiceResponse<VerifyAccountResponse> serviceResponse) {
        Intrinsics.checkNotNullParameter(serviceResponse, "response");
        if (serviceResponse.getResult().isPresent()) {
            return this;
        }
        ServiceResponse coerceError = ServiceResponse.coerceError(serviceResponse);
        Intrinsics.checkNotNullExpressionValue(coerceError, "coerceError(response)");
        return new VerifyCodeWithRegistrationLockResponseProcessor(coerceError, this.token);
    }

    @Override // org.thoughtcrime.securesms.registration.VerifyProcessor
    public boolean isServerSentError() {
        return (getError() instanceof NonSuccessfulResponseCodeException) || (getError() instanceof KeyBackupSystemWrongPinException) || (getError() instanceof KeyBackupSystemNoDataException);
    }
}
