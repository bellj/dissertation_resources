package org.thoughtcrime.securesms.registration;

import android.app.Application;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import j$.util.Optional;
import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.AppCapabilities;
import org.thoughtcrime.securesms.database.CdsDatabase;
import org.thoughtcrime.securesms.gcm.FcmUtil;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.pin.KbsRepository;
import org.thoughtcrime.securesms.pin.KeyBackupSystemWrongPinException;
import org.thoughtcrime.securesms.pin.TokenData;
import org.thoughtcrime.securesms.push.AccountManagerFactory;
import org.thoughtcrime.securesms.registration.VerifyAccountRepository;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.KbsPinData;
import org.whispersystems.signalservice.api.KeyBackupSystemNoDataException;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.crypto.UnidentifiedAccess;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.push.RequestVerificationCodeResponse;
import org.whispersystems.signalservice.internal.push.VerifyAccountResponse;

/* compiled from: VerifyAccountRepository.kt */
@Metadata(d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 \u00182\u00020\u0001:\u0003\u0018\u0019\u001aB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J6\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u00062\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\r2\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\nJ\u001a\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u00070\u00062\u0006\u0010\u0011\u001a\u00020\u0012J*\u0010\u0013\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00140\u00070\u00062\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\n2\u0006\u0010\u0016\u001a\u00020\u0017R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/registration/VerifyAccountRepository;", "", "context", "Landroid/app/Application;", "(Landroid/app/Application;)V", "requestVerificationCode", "Lio/reactivex/rxjava3/core/Single;", "Lorg/whispersystems/signalservice/internal/ServiceResponse;", "Lorg/whispersystems/signalservice/internal/push/RequestVerificationCodeResponse;", CdsDatabase.E164, "", "password", "mode", "Lorg/thoughtcrime/securesms/registration/VerifyAccountRepository$Mode;", "captchaToken", "verifyAccount", "Lorg/whispersystems/signalservice/internal/push/VerifyAccountResponse;", "registrationData", "Lorg/thoughtcrime/securesms/registration/RegistrationData;", "verifyAccountWithPin", "Lorg/thoughtcrime/securesms/registration/VerifyAccountRepository$VerifyAccountWithRegistrationLockResponse;", "pin", "tokenData", "Lorg/thoughtcrime/securesms/pin/TokenData;", "Companion", "Mode", "VerifyAccountWithRegistrationLockResponse", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class VerifyAccountRepository {
    public static final Companion Companion = new Companion(null);
    private static final long PUSH_REQUEST_TIMEOUT = TimeUnit.SECONDS.toMillis(5);
    private static final String TAG = Log.tag(VerifyAccountRepository.class);
    private final Application context;

    public VerifyAccountRepository(Application application) {
        Intrinsics.checkNotNullParameter(application, "context");
        this.context = application;
    }

    public static /* synthetic */ Single requestVerificationCode$default(VerifyAccountRepository verifyAccountRepository, String str, String str2, Mode mode, String str3, int i, Object obj) {
        if ((i & 8) != 0) {
            str3 = null;
        }
        return verifyAccountRepository.requestVerificationCode(str, str2, mode, str3);
    }

    public final Single<ServiceResponse<RequestVerificationCodeResponse>> requestVerificationCode(String str, String str2, Mode mode, String str3) {
        Intrinsics.checkNotNullParameter(str, CdsDatabase.E164);
        Intrinsics.checkNotNullParameter(str2, "password");
        Intrinsics.checkNotNullParameter(mode, "mode");
        Log.d(TAG, "SMS Verification requested");
        Single<ServiceResponse<RequestVerificationCodeResponse>> subscribeOn = Single.fromCallable(new Callable(str, str2, mode, str3) { // from class: org.thoughtcrime.securesms.registration.VerifyAccountRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ String f$1;
            public final /* synthetic */ String f$2;
            public final /* synthetic */ VerifyAccountRepository.Mode f$3;
            public final /* synthetic */ String f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return VerifyAccountRepository.m2571requestVerificationCode$lambda0(VerifyAccountRepository.this, this.f$1, this.f$2, this.f$3, this.f$4);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable {\n      val…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: requestVerificationCode$lambda-0 */
    public static final ServiceResponse m2571requestVerificationCode$lambda0(VerifyAccountRepository verifyAccountRepository, String str, String str2, Mode mode, String str3) {
        Intrinsics.checkNotNullParameter(verifyAccountRepository, "this$0");
        Intrinsics.checkNotNullParameter(str, "$e164");
        Intrinsics.checkNotNullParameter(str2, "$password");
        Intrinsics.checkNotNullParameter(mode, "$mode");
        Optional<String> token = FcmUtil.getToken(verifyAccountRepository.context);
        Intrinsics.checkNotNullExpressionValue(token, "getToken(context)");
        SignalServiceAccountManager createUnauthenticated = AccountManagerFactory.createUnauthenticated(verifyAccountRepository.context, str, 1, str2);
        Intrinsics.checkNotNullExpressionValue(createUnauthenticated, "createUnauthenticated(co…AULT_DEVICE_ID, password)");
        Optional<String> pushChallengeBlocking = PushChallengeRequest.getPushChallengeBlocking(createUnauthenticated, token, str, PUSH_REQUEST_TIMEOUT);
        if (mode == Mode.PHONE_CALL) {
            return createUnauthenticated.requestVoiceVerificationCode(Locale.getDefault(), Optional.ofNullable(str3), pushChallengeBlocking, token);
        }
        return createUnauthenticated.requestSmsVerificationCode(mode.isSmsRetrieverSupported(), Optional.ofNullable(str3), pushChallengeBlocking, token);
    }

    public final Single<ServiceResponse<VerifyAccountResponse>> verifyAccount(RegistrationData registrationData) {
        Intrinsics.checkNotNullParameter(registrationData, "registrationData");
        boolean isUniversalUnidentifiedAccess = TextSecurePreferences.isUniversalUnidentifiedAccess(this.context);
        byte[] deriveAccessKeyFrom = UnidentifiedAccess.deriveAccessKeyFrom(registrationData.getProfileKey());
        Intrinsics.checkNotNullExpressionValue(deriveAccessKeyFrom, "deriveAccessKeyFrom(registrationData.profileKey)");
        SignalServiceAccountManager createUnauthenticated = AccountManagerFactory.createUnauthenticated(this.context, registrationData.getE164(), 1, registrationData.getPassword());
        Intrinsics.checkNotNullExpressionValue(createUnauthenticated, "createUnauthenticated(\n …rationData.password\n    )");
        Single<ServiceResponse<VerifyAccountResponse>> subscribeOn = Single.fromCallable(new Callable(registrationData, deriveAccessKeyFrom, isUniversalUnidentifiedAccess) { // from class: org.thoughtcrime.securesms.registration.VerifyAccountRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ RegistrationData f$1;
            public final /* synthetic */ byte[] f$2;
            public final /* synthetic */ boolean f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return VerifyAccountRepository.m2572verifyAccount$lambda1(SignalServiceAccountManager.this, this.f$1, this.f$2, this.f$3);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable {\n      acc…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: verifyAccount$lambda-1 */
    public static final ServiceResponse m2572verifyAccount$lambda1(SignalServiceAccountManager signalServiceAccountManager, RegistrationData registrationData, byte[] bArr, boolean z) {
        Intrinsics.checkNotNullParameter(signalServiceAccountManager, "$accountManager");
        Intrinsics.checkNotNullParameter(registrationData, "$registrationData");
        Intrinsics.checkNotNullParameter(bArr, "$unidentifiedAccessKey");
        return signalServiceAccountManager.verifyAccount(registrationData.getCode(), registrationData.getRegistrationId(), registrationData.isNotFcm(), bArr, z, AppCapabilities.getCapabilities(true), SignalStore.phoneNumberPrivacy().getPhoneNumberListingMode().isDiscoverable());
    }

    public final Single<ServiceResponse<VerifyAccountWithRegistrationLockResponse>> verifyAccountWithPin(RegistrationData registrationData, String str, TokenData tokenData) {
        Intrinsics.checkNotNullParameter(registrationData, "registrationData");
        Intrinsics.checkNotNullParameter(str, "pin");
        Intrinsics.checkNotNullParameter(tokenData, "tokenData");
        boolean isUniversalUnidentifiedAccess = TextSecurePreferences.isUniversalUnidentifiedAccess(this.context);
        byte[] deriveAccessKeyFrom = UnidentifiedAccess.deriveAccessKeyFrom(registrationData.getProfileKey());
        Intrinsics.checkNotNullExpressionValue(deriveAccessKeyFrom, "deriveAccessKeyFrom(registrationData.profileKey)");
        SignalServiceAccountManager createUnauthenticated = AccountManagerFactory.createUnauthenticated(this.context, registrationData.getE164(), 1, registrationData.getPassword());
        Intrinsics.checkNotNullExpressionValue(createUnauthenticated, "createUnauthenticated(\n …rationData.password\n    )");
        Single<ServiceResponse<VerifyAccountWithRegistrationLockResponse>> subscribeOn = Single.fromCallable(new Callable(str, tokenData, createUnauthenticated, registrationData, deriveAccessKeyFrom, isUniversalUnidentifiedAccess) { // from class: org.thoughtcrime.securesms.registration.VerifyAccountRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ String f$0;
            public final /* synthetic */ TokenData f$1;
            public final /* synthetic */ SignalServiceAccountManager f$2;
            public final /* synthetic */ RegistrationData f$3;
            public final /* synthetic */ byte[] f$4;
            public final /* synthetic */ boolean f$5;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return VerifyAccountRepository.m2573verifyAccountWithPin$lambda2(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable {\n      try…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: verifyAccountWithPin$lambda-2 */
    public static final ServiceResponse m2573verifyAccountWithPin$lambda2(String str, TokenData tokenData, SignalServiceAccountManager signalServiceAccountManager, RegistrationData registrationData, byte[] bArr, boolean z) {
        Intrinsics.checkNotNullParameter(str, "$pin");
        Intrinsics.checkNotNullParameter(tokenData, "$tokenData");
        Intrinsics.checkNotNullParameter(signalServiceAccountManager, "$accountManager");
        Intrinsics.checkNotNullParameter(registrationData, "$registrationData");
        Intrinsics.checkNotNullParameter(bArr, "$unidentifiedAccessKey");
        try {
            KbsPinData restoreMasterKey = KbsRepository.restoreMasterKey(str, tokenData.getEnclave(), tokenData.getBasicAuth(), tokenData.getTokenResponse());
            Intrinsics.checkNotNull(restoreMasterKey);
            String deriveRegistrationLock = restoreMasterKey.getMasterKey().deriveRegistrationLock();
            Intrinsics.checkNotNullExpressionValue(deriveRegistrationLock, "kbsData.masterKey.deriveRegistrationLock()");
            ServiceResponse<VerifyAccountResponse> verifyAccountWithRegistrationLockPin = signalServiceAccountManager.verifyAccountWithRegistrationLockPin(registrationData.getCode(), registrationData.getRegistrationId(), registrationData.isNotFcm(), deriveRegistrationLock, bArr, z, AppCapabilities.getCapabilities(true), SignalStore.phoneNumberPrivacy().getPhoneNumberListingMode().isDiscoverable());
            Intrinsics.checkNotNullExpressionValue(verifyAccountWithRegistrationLockPin, "accountManager.verifyAcc….isDiscoverable\n        )");
            return VerifyAccountWithRegistrationLockResponse.Companion.from(verifyAccountWithRegistrationLockPin, restoreMasterKey);
        } catch (IOException e) {
            return ServiceResponse.forExecutionError(e);
        } catch (KeyBackupSystemWrongPinException e2) {
            return ServiceResponse.forExecutionError(e2);
        } catch (KeyBackupSystemNoDataException e3) {
            return ServiceResponse.forExecutionError(e3);
        }
    }

    /* compiled from: VerifyAccountRepository.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/registration/VerifyAccountRepository$Mode;", "", "isSmsRetrieverSupported", "", "(Ljava/lang/String;IZ)V", "()Z", "SMS_WITH_LISTENER", "SMS_WITHOUT_LISTENER", "PHONE_CALL", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum Mode {
        SMS_WITH_LISTENER(true),
        SMS_WITHOUT_LISTENER(false),
        PHONE_CALL(false);
        
        private final boolean isSmsRetrieverSupported;

        Mode(boolean z) {
            this.isSmsRetrieverSupported = z;
        }

        public final boolean isSmsRetrieverSupported() {
            return this.isSmsRetrieverSupported;
        }
    }

    /* compiled from: VerifyAccountRepository.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/registration/VerifyAccountRepository$Companion;", "", "()V", "PUSH_REQUEST_TIMEOUT", "", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    /* compiled from: VerifyAccountRepository.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/registration/VerifyAccountRepository$VerifyAccountWithRegistrationLockResponse;", "", "verifyAccountResponse", "Lorg/whispersystems/signalservice/internal/push/VerifyAccountResponse;", "kbsData", "Lorg/whispersystems/signalservice/api/KbsPinData;", "(Lorg/whispersystems/signalservice/internal/push/VerifyAccountResponse;Lorg/whispersystems/signalservice/api/KbsPinData;)V", "getKbsData", "()Lorg/whispersystems/signalservice/api/KbsPinData;", "getVerifyAccountResponse", "()Lorg/whispersystems/signalservice/internal/push/VerifyAccountResponse;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class VerifyAccountWithRegistrationLockResponse {
        public static final Companion Companion = new Companion(null);
        private final KbsPinData kbsData;
        private final VerifyAccountResponse verifyAccountResponse;

        public static /* synthetic */ VerifyAccountWithRegistrationLockResponse copy$default(VerifyAccountWithRegistrationLockResponse verifyAccountWithRegistrationLockResponse, VerifyAccountResponse verifyAccountResponse, KbsPinData kbsPinData, int i, Object obj) {
            if ((i & 1) != 0) {
                verifyAccountResponse = verifyAccountWithRegistrationLockResponse.verifyAccountResponse;
            }
            if ((i & 2) != 0) {
                kbsPinData = verifyAccountWithRegistrationLockResponse.kbsData;
            }
            return verifyAccountWithRegistrationLockResponse.copy(verifyAccountResponse, kbsPinData);
        }

        public final VerifyAccountResponse component1() {
            return this.verifyAccountResponse;
        }

        public final KbsPinData component2() {
            return this.kbsData;
        }

        public final VerifyAccountWithRegistrationLockResponse copy(VerifyAccountResponse verifyAccountResponse, KbsPinData kbsPinData) {
            Intrinsics.checkNotNullParameter(verifyAccountResponse, "verifyAccountResponse");
            Intrinsics.checkNotNullParameter(kbsPinData, "kbsData");
            return new VerifyAccountWithRegistrationLockResponse(verifyAccountResponse, kbsPinData);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof VerifyAccountWithRegistrationLockResponse)) {
                return false;
            }
            VerifyAccountWithRegistrationLockResponse verifyAccountWithRegistrationLockResponse = (VerifyAccountWithRegistrationLockResponse) obj;
            return Intrinsics.areEqual(this.verifyAccountResponse, verifyAccountWithRegistrationLockResponse.verifyAccountResponse) && Intrinsics.areEqual(this.kbsData, verifyAccountWithRegistrationLockResponse.kbsData);
        }

        public int hashCode() {
            return (this.verifyAccountResponse.hashCode() * 31) + this.kbsData.hashCode();
        }

        public String toString() {
            return "VerifyAccountWithRegistrationLockResponse(verifyAccountResponse=" + this.verifyAccountResponse + ", kbsData=" + this.kbsData + ')';
        }

        /* compiled from: VerifyAccountRepository.kt */
        @Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00070\u00042\u0006\u0010\b\u001a\u00020\t¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/registration/VerifyAccountRepository$VerifyAccountWithRegistrationLockResponse$Companion;", "", "()V", "from", "Lorg/whispersystems/signalservice/internal/ServiceResponse;", "Lorg/thoughtcrime/securesms/registration/VerifyAccountRepository$VerifyAccountWithRegistrationLockResponse;", "response", "Lorg/whispersystems/signalservice/internal/push/VerifyAccountResponse;", "kbsData", "Lorg/whispersystems/signalservice/api/KbsPinData;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            public final ServiceResponse<VerifyAccountWithRegistrationLockResponse> from(ServiceResponse<VerifyAccountResponse> serviceResponse, KbsPinData kbsPinData) {
                Intrinsics.checkNotNullParameter(serviceResponse, "response");
                Intrinsics.checkNotNullParameter(kbsPinData, "kbsData");
                if (serviceResponse.getResult().isPresent()) {
                    VerifyAccountResponse verifyAccountResponse = serviceResponse.getResult().get();
                    Intrinsics.checkNotNullExpressionValue(verifyAccountResponse, "response.result.get()");
                    ServiceResponse<VerifyAccountWithRegistrationLockResponse> forResult = ServiceResponse.forResult(new VerifyAccountWithRegistrationLockResponse(verifyAccountResponse, kbsPinData), 200, null);
                    Intrinsics.checkNotNullExpressionValue(forResult, "{\n          ServiceRespo…ta), 200, null)\n        }");
                    return forResult;
                }
                ServiceResponse<VerifyAccountWithRegistrationLockResponse> coerceError = ServiceResponse.coerceError(serviceResponse);
                Intrinsics.checkNotNullExpressionValue(coerceError, "{\n          ServiceRespo…Error(response)\n        }");
                return coerceError;
            }
        }

        public VerifyAccountWithRegistrationLockResponse(VerifyAccountResponse verifyAccountResponse, KbsPinData kbsPinData) {
            Intrinsics.checkNotNullParameter(verifyAccountResponse, "verifyAccountResponse");
            Intrinsics.checkNotNullParameter(kbsPinData, "kbsData");
            this.verifyAccountResponse = verifyAccountResponse;
            this.kbsData = kbsPinData;
        }

        public final KbsPinData getKbsData() {
            return this.kbsData;
        }

        public final VerifyAccountResponse getVerifyAccountResponse() {
            return this.verifyAccountResponse;
        }
    }
}
