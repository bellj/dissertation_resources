package org.thoughtcrime.securesms.registration;

import android.content.Context;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.DirectoryRefreshJob;
import org.thoughtcrime.securesms.jobs.StorageSyncJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes.dex */
public final class RegistrationUtil {
    private static final String TAG = Log.tag(RegistrationUtil.class);

    private RegistrationUtil() {
    }

    public static void maybeMarkRegistrationComplete(Context context) {
        if (!SignalStore.registrationValues().isRegistrationComplete() && SignalStore.account().isRegistered() && !Recipient.self().getProfileName().isEmpty() && (SignalStore.kbsValues().hasPin() || SignalStore.kbsValues().hasOptedOut())) {
            Log.i(TAG, "Marking registration completed.", new Throwable());
            SignalStore.registrationValues().setRegistrationComplete();
            ApplicationDependencies.getJobManager().startChain(new StorageSyncJob()).then(new DirectoryRefreshJob(false)).enqueue();
        } else if (!SignalStore.registrationValues().isRegistrationComplete()) {
            Log.i(TAG, "Registration is not yet complete.", new Throwable());
        }
    }
}
