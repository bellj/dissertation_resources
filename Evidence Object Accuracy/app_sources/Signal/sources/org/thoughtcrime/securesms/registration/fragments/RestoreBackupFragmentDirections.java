package org.thoughtcrime.securesms.registration.fragments;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.SignupDirections;

/* loaded from: classes4.dex */
public class RestoreBackupFragmentDirections {
    private RestoreBackupFragmentDirections() {
    }

    public static NavDirections actionBackupRestored() {
        return new ActionOnlyNavDirections(R.id.action_backupRestored);
    }

    public static NavDirections actionSkip() {
        return new ActionOnlyNavDirections(R.id.action_skip);
    }

    public static NavDirections actionNoBackupFound() {
        return new ActionOnlyNavDirections(R.id.action_noBackupFound);
    }

    public static NavDirections actionSkipNoReturn() {
        return new ActionOnlyNavDirections(R.id.action_skip_no_return);
    }

    public static NavDirections actionRestartToWelcomeFragment() {
        return SignupDirections.actionRestartToWelcomeFragment();
    }
}
