package org.thoughtcrime.securesms.registration.util;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.google.i18n.phonenumbers.AsYouTypeFormatter;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.LabeledEditText;
import org.thoughtcrime.securesms.registration.viewmodel.NumberViewState;

/* loaded from: classes4.dex */
public final class RegistrationNumberInputController {
    private final Callbacks callbacks;
    private final Context context;
    private final LabeledEditText countryCode;
    private AsYouTypeFormatter countryFormatter;
    private ArrayAdapter<String> countrySpinnerAdapter;
    private boolean isUpdating = true;
    private final boolean lastInput;
    private final LabeledEditText number;

    /* loaded from: classes4.dex */
    public interface Callbacks {
        void onNumberFocused();

        void onNumberInputDone(View view);

        void onNumberInputNext(View view);

        void onPickCountry(View view);

        void setCountry(int i);

        void setNationalNumber(String str);
    }

    public RegistrationNumberInputController(Context context, LabeledEditText labeledEditText, LabeledEditText labeledEditText2, Spinner spinner, boolean z, Callbacks callbacks) {
        this.context = context;
        this.countryCode = labeledEditText;
        this.number = labeledEditText2;
        this.lastInput = z;
        this.callbacks = callbacks;
        initializeSpinner(spinner);
        setUpNumberInput();
        labeledEditText.getInput().addTextChangedListener(new CountryCodeChangedListener());
        labeledEditText.getInput().setImeOptions(5);
    }

    private void setUpNumberInput() {
        EditText input = this.number.getInput();
        input.addTextChangedListener(new NumberChangedListener());
        this.number.setOnFocusChangeListener(new View.OnFocusChangeListener() { // from class: org.thoughtcrime.securesms.registration.util.RegistrationNumberInputController$$ExternalSyntheticLambda2
            @Override // android.view.View.OnFocusChangeListener
            public final void onFocusChange(View view, boolean z) {
                RegistrationNumberInputController.this.lambda$setUpNumberInput$0(view, z);
            }
        });
        input.setImeOptions(this.lastInput ? 6 : 5);
        input.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: org.thoughtcrime.securesms.registration.util.RegistrationNumberInputController$$ExternalSyntheticLambda3
            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                return RegistrationNumberInputController.this.lambda$setUpNumberInput$1(textView, i, keyEvent);
            }
        });
    }

    public /* synthetic */ void lambda$setUpNumberInput$0(View view, boolean z) {
        if (z) {
            this.callbacks.onNumberFocused();
        }
    }

    public /* synthetic */ boolean lambda$setUpNumberInput$1(TextView textView, int i, KeyEvent keyEvent) {
        if (i == 5) {
            this.callbacks.onNumberInputNext(textView);
            return true;
        } else if (i != 6) {
            return false;
        } else {
            this.callbacks.onNumberInputDone(textView);
            return true;
        }
    }

    private void initializeSpinner(Spinner spinner) {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this.context, 17367048);
        this.countrySpinnerAdapter = arrayAdapter;
        arrayAdapter.setDropDownViewResource(17367049);
        setCountryDisplay(this.context.getString(R.string.RegistrationActivity_select_your_country));
        spinner.setAdapter((SpinnerAdapter) this.countrySpinnerAdapter);
        spinner.setOnTouchListener(new View.OnTouchListener() { // from class: org.thoughtcrime.securesms.registration.util.RegistrationNumberInputController$$ExternalSyntheticLambda0
            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                return RegistrationNumberInputController.this.lambda$initializeSpinner$2(view, motionEvent);
            }
        });
        spinner.setOnKeyListener(new View.OnKeyListener() { // from class: org.thoughtcrime.securesms.registration.util.RegistrationNumberInputController$$ExternalSyntheticLambda1
            @Override // android.view.View.OnKeyListener
            public final boolean onKey(View view, int i, KeyEvent keyEvent) {
                return RegistrationNumberInputController.this.lambda$initializeSpinner$3(view, i, keyEvent);
            }
        });
    }

    public /* synthetic */ boolean lambda$initializeSpinner$2(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 1) {
            this.callbacks.onPickCountry(view);
        }
        return true;
    }

    public /* synthetic */ boolean lambda$initializeSpinner$3(View view, int i, KeyEvent keyEvent) {
        if (i != 23 || keyEvent.getAction() != 1) {
            return false;
        }
        this.callbacks.onPickCountry(view);
        return true;
    }

    public void updateNumber(NumberViewState numberViewState) {
        int countryCode = numberViewState.getCountryCode();
        String valueOf = String.valueOf(countryCode);
        String nationalNumber = numberViewState.getNationalNumber();
        String countryDisplayName = numberViewState.getCountryDisplayName();
        this.isUpdating = true;
        setCountryDisplay(countryDisplayName);
        if (this.countryCode.getText() == null || !this.countryCode.getText().toString().equals(valueOf)) {
            this.countryCode.setText(valueOf);
            setCountryFormatter(PhoneNumberUtil.getInstance().getRegionCodeForCountryCode(countryCode));
        }
        if (!justDigits(this.number.getText()).equals(nationalNumber) && !TextUtils.isEmpty(nationalNumber)) {
            this.number.setText(nationalNumber);
        }
        this.isUpdating = false;
    }

    private String justDigits(Editable editable) {
        if (editable == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < editable.length(); i++) {
            char charAt = editable.charAt(i);
            if (Character.isDigit(charAt)) {
                sb.append(charAt);
            }
        }
        return sb.toString();
    }

    public void setCountryDisplay(String str) {
        this.countrySpinnerAdapter.clear();
        if (str == null) {
            this.countrySpinnerAdapter.add(this.context.getString(R.string.RegistrationActivity_select_your_country));
        } else {
            this.countrySpinnerAdapter.add(str);
        }
    }

    public void setCountryFormatter(String str) {
        this.countryFormatter = str != null ? PhoneNumberUtil.getInstance().getAsYouTypeFormatter(str) : null;
        reformatText(this.number.getText());
    }

    public String reformatText(Editable editable) {
        if (this.countryFormatter == null || TextUtils.isEmpty(editable)) {
            return null;
        }
        this.countryFormatter.clear();
        StringBuilder sb = new StringBuilder();
        String str = null;
        for (int i = 0; i < editable.length(); i++) {
            char charAt = editable.charAt(i);
            if (Character.isDigit(charAt)) {
                str = this.countryFormatter.inputDigit(charAt);
                sb.append(charAt);
            }
        }
        if (str != null && !editable.toString().equals(str)) {
            editable.replace(0, editable.length(), str);
        }
        if (sb.length() == 0) {
            return null;
        }
        return sb.toString();
    }

    /* loaded from: classes4.dex */
    public class NumberChangedListener implements TextWatcher {
        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        private NumberChangedListener() {
            RegistrationNumberInputController.this = r1;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            String reformatText = RegistrationNumberInputController.this.reformatText(editable);
            if (reformatText != null && !RegistrationNumberInputController.this.isUpdating) {
                RegistrationNumberInputController.this.callbacks.setNationalNumber(reformatText);
            }
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public class CountryCodeChangedListener implements TextWatcher {
        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        private CountryCodeChangedListener() {
            RegistrationNumberInputController.this = r1;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            if (TextUtils.isEmpty(editable) || !TextUtils.isDigitsOnly(editable)) {
                RegistrationNumberInputController.this.setCountryDisplay(null);
                RegistrationNumberInputController.this.countryFormatter = null;
                return;
            }
            int parseInt = Integer.parseInt(editable.toString());
            String regionCodeForCountryCode = PhoneNumberUtil.getInstance().getRegionCodeForCountryCode(parseInt);
            RegistrationNumberInputController.this.setCountryFormatter(regionCodeForCountryCode);
            if (!TextUtils.isEmpty(regionCodeForCountryCode) && !regionCodeForCountryCode.equals("ZZ")) {
                if (!RegistrationNumberInputController.this.isUpdating) {
                    RegistrationNumberInputController.this.number.requestFocus();
                }
                int length = RegistrationNumberInputController.this.number.getText().length();
                RegistrationNumberInputController.this.number.getInput().setSelection(length, length);
            }
            if (!RegistrationNumberInputController.this.isUpdating) {
                RegistrationNumberInputController.this.callbacks.setCountry(parseInt);
            }
        }
    }
}
