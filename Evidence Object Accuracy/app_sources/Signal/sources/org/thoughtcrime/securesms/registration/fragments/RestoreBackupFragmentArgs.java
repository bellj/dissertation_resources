package org.thoughtcrime.securesms.registration.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class RestoreBackupFragmentArgs {
    private final HashMap arguments;

    private RestoreBackupFragmentArgs() {
        this.arguments = new HashMap();
    }

    private RestoreBackupFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static RestoreBackupFragmentArgs fromBundle(Bundle bundle) {
        RestoreBackupFragmentArgs restoreBackupFragmentArgs = new RestoreBackupFragmentArgs();
        bundle.setClassLoader(RestoreBackupFragmentArgs.class.getClassLoader());
        if (!bundle.containsKey("uri")) {
            restoreBackupFragmentArgs.arguments.put("uri", null);
        } else if (Parcelable.class.isAssignableFrom(Uri.class) || Serializable.class.isAssignableFrom(Uri.class)) {
            restoreBackupFragmentArgs.arguments.put("uri", (Uri) bundle.get("uri"));
        } else {
            throw new UnsupportedOperationException(Uri.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
        return restoreBackupFragmentArgs;
    }

    public Uri getUri() {
        return (Uri) this.arguments.get("uri");
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("uri")) {
            Uri uri = (Uri) this.arguments.get("uri");
            if (Parcelable.class.isAssignableFrom(Uri.class) || uri == null) {
                bundle.putParcelable("uri", (Parcelable) Parcelable.class.cast(uri));
            } else if (Serializable.class.isAssignableFrom(Uri.class)) {
                bundle.putSerializable("uri", (Serializable) Serializable.class.cast(uri));
            } else {
                throw new UnsupportedOperationException(Uri.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        } else {
            bundle.putSerializable("uri", null);
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        RestoreBackupFragmentArgs restoreBackupFragmentArgs = (RestoreBackupFragmentArgs) obj;
        if (this.arguments.containsKey("uri") != restoreBackupFragmentArgs.arguments.containsKey("uri")) {
            return false;
        }
        return getUri() == null ? restoreBackupFragmentArgs.getUri() == null : getUri().equals(restoreBackupFragmentArgs.getUri());
    }

    public int hashCode() {
        return 31 + (getUri() != null ? getUri().hashCode() : 0);
    }

    public String toString() {
        return "RestoreBackupFragmentArgs{uri=" + getUri() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(RestoreBackupFragmentArgs restoreBackupFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(restoreBackupFragmentArgs.arguments);
        }

        public Builder() {
            this.arguments = new HashMap();
        }

        public RestoreBackupFragmentArgs build() {
            return new RestoreBackupFragmentArgs(this.arguments);
        }

        public Builder setUri(Uri uri) {
            this.arguments.put("uri", uri);
            return this;
        }

        public Uri getUri() {
            return (Uri) this.arguments.get("uri");
        }
    }
}
