package org.thoughtcrime.securesms.registration;

import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.whispersystems.signalservice.api.push.exceptions.ImpossiblePhoneNumberException;
import org.whispersystems.signalservice.api.push.exceptions.LocalRateLimitException;
import org.whispersystems.signalservice.api.push.exceptions.NonNormalizedPhoneNumberException;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.ServiceResponseProcessor;
import org.whispersystems.signalservice.internal.push.RequestVerificationCodeResponse;

/* compiled from: RequestVerificationCodeResponseProcessor.kt */
@Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u0000 \u00112\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0011B\u0013\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00020\u0004¢\u0006\u0002\u0010\u0005J\b\u0010\u0006\u001a\u00020\u0007H\u0016J\n\u0010\b\u001a\u0004\u0018\u00010\tH\u0016J\u0006\u0010\n\u001a\u00020\u000bJ\u0006\u0010\f\u001a\u00020\u000bJ\u0006\u0010\r\u001a\u00020\u0007J\u0006\u0010\u000e\u001a\u00020\u0007J\u0006\u0010\u000f\u001a\u00020\u0007J\b\u0010\u0010\u001a\u00020\u0007H\u0016¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/registration/RequestVerificationCodeResponseProcessor;", "Lorg/whispersystems/signalservice/internal/ServiceResponseProcessor;", "Lorg/whispersystems/signalservice/internal/push/RequestVerificationCodeResponse;", "response", "Lorg/whispersystems/signalservice/internal/ServiceResponse;", "(Lorg/whispersystems/signalservice/internal/ServiceResponse;)V", "captchaRequired", "", "getError", "", "getNormalizedNumber", "", "getOriginalNumber", "isImpossibleNumber", "isNonNormalizedNumber", "localRateLimit", "rateLimit", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class RequestVerificationCodeResponseProcessor extends ServiceResponseProcessor<RequestVerificationCodeResponse> {
    public static final Companion Companion = new Companion(null);

    @JvmStatic
    public static final RequestVerificationCodeResponseProcessor forLocalRateLimit() {
        return Companion.forLocalRateLimit();
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public RequestVerificationCodeResponseProcessor(ServiceResponse<RequestVerificationCodeResponse> serviceResponse) {
        super(serviceResponse);
        Intrinsics.checkNotNullParameter(serviceResponse, "response");
    }

    @Override // org.whispersystems.signalservice.internal.ServiceResponseProcessor
    public boolean captchaRequired() {
        return super.captchaRequired();
    }

    @Override // org.whispersystems.signalservice.internal.ServiceResponseProcessor
    public boolean rateLimit() {
        return super.rateLimit();
    }

    @Override // org.whispersystems.signalservice.internal.ServiceResponseProcessor
    public Throwable getError() {
        return super.getError();
    }

    public final boolean localRateLimit() {
        return getError() instanceof LocalRateLimitException;
    }

    public final boolean isImpossibleNumber() {
        return getError() instanceof ImpossiblePhoneNumberException;
    }

    public final boolean isNonNormalizedNumber() {
        return getError() instanceof NonNormalizedPhoneNumberException;
    }

    public final String getOriginalNumber() {
        if (getError() instanceof NonNormalizedPhoneNumberException) {
            Throwable error = getError();
            if (error != null) {
                String originalNumber = ((NonNormalizedPhoneNumberException) error).getOriginalNumber();
                Intrinsics.checkNotNullExpressionValue(originalNumber, "error as NonNormalizedPh…Exception).originalNumber");
                return originalNumber;
            }
            throw new NullPointerException("null cannot be cast to non-null type org.whispersystems.signalservice.api.push.exceptions.NonNormalizedPhoneNumberException");
        }
        throw new IllegalStateException("This can only be called when isNonNormalizedNumber()");
    }

    public final String getNormalizedNumber() {
        if (getError() instanceof NonNormalizedPhoneNumberException) {
            Throwable error = getError();
            if (error != null) {
                String normalizedNumber = ((NonNormalizedPhoneNumberException) error).getNormalizedNumber();
                Intrinsics.checkNotNullExpressionValue(normalizedNumber, "error as NonNormalizedPh…ception).normalizedNumber");
                return normalizedNumber;
            }
            throw new NullPointerException("null cannot be cast to non-null type org.whispersystems.signalservice.api.push.exceptions.NonNormalizedPhoneNumberException");
        }
        throw new IllegalStateException("This can only be called when isNonNormalizedNumber()");
    }

    /* compiled from: RequestVerificationCodeResponseProcessor.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0007¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/registration/RequestVerificationCodeResponseProcessor$Companion;", "", "()V", "forLocalRateLimit", "Lorg/thoughtcrime/securesms/registration/RequestVerificationCodeResponseProcessor;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final RequestVerificationCodeResponseProcessor forLocalRateLimit() {
            ServiceResponse forExecutionError = ServiceResponse.forExecutionError(new LocalRateLimitException());
            Intrinsics.checkNotNullExpressionValue(forExecutionError, "forExecutionError(LocalRateLimitException())");
            return new RequestVerificationCodeResponseProcessor(forExecutionError);
        }
    }
}
