package org.thoughtcrime.securesms.registration.fragments;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.SignupDirections;

/* loaded from: classes4.dex */
public class RegistrationLockFragmentDirections {
    private RegistrationLockFragmentDirections() {
    }

    public static NavDirections actionSuccessfulRegistration() {
        return new ActionOnlyNavDirections(R.id.action_successfulRegistration);
    }

    public static NavDirections actionAccountLocked() {
        return new ActionOnlyNavDirections(R.id.action_accountLocked);
    }

    public static NavDirections actionRestartToWelcomeFragment() {
        return SignupDirections.actionRestartToWelcomeFragment();
    }
}
