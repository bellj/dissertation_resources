package org.thoughtcrime.securesms.registration.fragments;

import android.os.Build;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.registration.fragments.SignalStrengthPhoneStateListener;
import org.thoughtcrime.securesms.util.Debouncer;

/* loaded from: classes4.dex */
public final class SignalStrengthPhoneStateListener extends PhoneStateListener implements DefaultLifecycleObserver {
    private static final String TAG = Log.tag(SignalStrengthPhoneStateListener.class);
    private final Callback callback;
    private final Debouncer debouncer = new Debouncer(1000);

    /* loaded from: classes4.dex */
    public interface Callback {
        void onCellSignalPresent();

        void onNoCellSignalPresent();
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onCreate(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onCreate(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onDestroy(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onDestroy(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onStart(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onStart(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onStop(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onStop(this, lifecycleOwner);
    }

    public SignalStrengthPhoneStateListener(LifecycleOwner lifecycleOwner, Callback callback) {
        this.callback = callback;
        lifecycleOwner.getLifecycle().addObserver(this);
    }

    @Override // android.telephony.PhoneStateListener
    public void onSignalStrengthsChanged(SignalStrength signalStrength) {
        if (signalStrength != null) {
            if (isLowLevel(signalStrength)) {
                Log.w(TAG, "No cell signal detected");
                Debouncer debouncer = this.debouncer;
                Callback callback = this.callback;
                Objects.requireNonNull(callback);
                debouncer.publish(new Runnable() { // from class: org.thoughtcrime.securesms.registration.fragments.SignalStrengthPhoneStateListener$$ExternalSyntheticLambda0
                    @Override // java.lang.Runnable
                    public final void run() {
                        SignalStrengthPhoneStateListener.Callback.this.onNoCellSignalPresent();
                    }
                });
                return;
            }
            Log.i(TAG, "Cell signal detected");
            this.debouncer.clear();
            this.callback.onCellSignalPresent();
        }
    }

    private boolean isLowLevel(SignalStrength signalStrength) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (signalStrength.getLevel() == 0) {
                return true;
            }
            return false;
        } else if (signalStrength.getGsmSignalStrength() == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public void onResume(LifecycleOwner lifecycleOwner) {
        ((TelephonyManager) ApplicationDependencies.getApplication().getSystemService(RecipientDatabase.PHONE)).listen(this, 256);
        Log.i(TAG, "Listening to cell phone signal strength changes");
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public void onPause(LifecycleOwner lifecycleOwner) {
        ((TelephonyManager) ApplicationDependencies.getApplication().getSystemService(RecipientDatabase.PHONE)).listen(this, 0);
        Log.i(TAG, "Stopped listening to cell phone signal strength changes");
    }
}
