package org.thoughtcrime.securesms.registration;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.thoughtcrime.securesms.database.CdsDatabase;

/* compiled from: RegistrationData.kt */
@Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0013\b\b\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u000bJ\t\u0010\u0019\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001a\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001b\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001c\u001a\u00020\u0007HÆ\u0003J\t\u0010\u001d\u001a\u00020\tHÆ\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u0003HÆ\u0003JG\u0010\u001f\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010 \u001a\u00020\u00112\b\u0010!\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\"\u001a\u00020\u0007HÖ\u0001J\t\u0010#\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0013\u0010\n\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\rR\u0011\u0010\u0010\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0012R\u0011\u0010\u0013\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0012R\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\rR\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018¨\u0006$"}, d2 = {"Lorg/thoughtcrime/securesms/registration/RegistrationData;", "", "code", "", CdsDatabase.E164, "password", "registrationId", "", "profileKey", "Lorg/signal/libsignal/zkgroup/profiles/ProfileKey;", "fcmToken", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILorg/signal/libsignal/zkgroup/profiles/ProfileKey;Ljava/lang/String;)V", "getCode", "()Ljava/lang/String;", "getE164", "getFcmToken", "isFcm", "", "()Z", "isNotFcm", "getPassword", "getProfileKey", "()Lorg/signal/libsignal/zkgroup/profiles/ProfileKey;", "getRegistrationId", "()I", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "other", "hashCode", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class RegistrationData {
    private final String code;
    private final String e164;
    private final String fcmToken;
    private final boolean isFcm;
    private final boolean isNotFcm;
    private final String password;
    private final ProfileKey profileKey;
    private final int registrationId;

    public static /* synthetic */ RegistrationData copy$default(RegistrationData registrationData, String str, String str2, String str3, int i, ProfileKey profileKey, String str4, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = registrationData.code;
        }
        if ((i2 & 2) != 0) {
            str2 = registrationData.e164;
        }
        if ((i2 & 4) != 0) {
            str3 = registrationData.password;
        }
        if ((i2 & 8) != 0) {
            i = registrationData.registrationId;
        }
        if ((i2 & 16) != 0) {
            profileKey = registrationData.profileKey;
        }
        if ((i2 & 32) != 0) {
            str4 = registrationData.fcmToken;
        }
        return registrationData.copy(str, str2, str3, i, profileKey, str4);
    }

    public final String component1() {
        return this.code;
    }

    public final String component2() {
        return this.e164;
    }

    public final String component3() {
        return this.password;
    }

    public final int component4() {
        return this.registrationId;
    }

    public final ProfileKey component5() {
        return this.profileKey;
    }

    public final String component6() {
        return this.fcmToken;
    }

    public final RegistrationData copy(String str, String str2, String str3, int i, ProfileKey profileKey, String str4) {
        Intrinsics.checkNotNullParameter(str, "code");
        Intrinsics.checkNotNullParameter(str2, CdsDatabase.E164);
        Intrinsics.checkNotNullParameter(str3, "password");
        Intrinsics.checkNotNullParameter(profileKey, "profileKey");
        return new RegistrationData(str, str2, str3, i, profileKey, str4);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof RegistrationData)) {
            return false;
        }
        RegistrationData registrationData = (RegistrationData) obj;
        return Intrinsics.areEqual(this.code, registrationData.code) && Intrinsics.areEqual(this.e164, registrationData.e164) && Intrinsics.areEqual(this.password, registrationData.password) && this.registrationId == registrationData.registrationId && Intrinsics.areEqual(this.profileKey, registrationData.profileKey) && Intrinsics.areEqual(this.fcmToken, registrationData.fcmToken);
    }

    public int hashCode() {
        int hashCode = ((((((((this.code.hashCode() * 31) + this.e164.hashCode()) * 31) + this.password.hashCode()) * 31) + this.registrationId) * 31) + this.profileKey.hashCode()) * 31;
        String str = this.fcmToken;
        return hashCode + (str == null ? 0 : str.hashCode());
    }

    public String toString() {
        return "RegistrationData(code=" + this.code + ", e164=" + this.e164 + ", password=" + this.password + ", registrationId=" + this.registrationId + ", profileKey=" + this.profileKey + ", fcmToken=" + this.fcmToken + ')';
    }

    public RegistrationData(String str, String str2, String str3, int i, ProfileKey profileKey, String str4) {
        Intrinsics.checkNotNullParameter(str, "code");
        Intrinsics.checkNotNullParameter(str2, CdsDatabase.E164);
        Intrinsics.checkNotNullParameter(str3, "password");
        Intrinsics.checkNotNullParameter(profileKey, "profileKey");
        this.code = str;
        this.e164 = str2;
        this.password = str3;
        this.registrationId = i;
        this.profileKey = profileKey;
        this.fcmToken = str4;
        boolean z = true;
        this.isFcm = str4 != null;
        this.isNotFcm = str4 != null ? false : z;
    }

    public final String getCode() {
        return this.code;
    }

    public final String getE164() {
        return this.e164;
    }

    public final String getPassword() {
        return this.password;
    }

    public final int getRegistrationId() {
        return this.registrationId;
    }

    public final ProfileKey getProfileKey() {
        return this.profileKey;
    }

    public final String getFcmToken() {
        return this.fcmToken;
    }

    public final boolean isFcm() {
        return this.isFcm;
    }

    public final boolean isNotFcm() {
        return this.isNotFcm;
    }
}
