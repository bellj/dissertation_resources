package org.thoughtcrime.securesms.registration.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import androidx.fragment.app.ListFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.navigation.fragment.NavHostFragment;
import java.util.ArrayList;
import java.util.Map;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.loaders.CountryListLoader;
import org.thoughtcrime.securesms.registration.viewmodel.RegistrationViewModel;

/* loaded from: classes4.dex */
public final class CountryPickerFragment extends ListFragment implements LoaderManager.LoaderCallbacks<ArrayList<Map<String, String>>> {
    public static final String KEY_COUNTRY;
    public static final String KEY_COUNTRY_CODE;
    private EditText countryFilter;
    private RegistrationViewModel model;
    private String resultKey;

    @Override // androidx.loader.app.LoaderManager.LoaderCallbacks
    public /* bridge */ /* synthetic */ void onLoadFinished(Loader loader, Object obj) {
        onLoadFinished((Loader<ArrayList<Map<String, String>>>) loader, (ArrayList) obj);
    }

    @Override // androidx.fragment.app.ListFragment, androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_registration_country_picker, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        if (getArguments() != null) {
            this.resultKey = CountryPickerFragmentArgs.fromBundle(requireArguments()).getResultKey();
        }
        if (this.resultKey == null) {
            this.model = (RegistrationViewModel) new ViewModelProvider(requireActivity()).get(RegistrationViewModel.class);
        }
        EditText editText = (EditText) view.findViewById(R.id.country_search);
        this.countryFilter = editText;
        editText.addTextChangedListener(new FilterWatcher());
        LoaderManager.getInstance(this).initLoader(0, null, this).forceLoad();
    }

    @Override // androidx.fragment.app.ListFragment
    public void onListItemClick(ListView listView, View view, int i, long j) {
        Map map = (Map) getListAdapter().getItem(i);
        int parseInt = Integer.parseInt(((String) map.get(KEY_COUNTRY_CODE)).replace("+", ""));
        String str = (String) map.get("country_name");
        if (this.resultKey == null) {
            this.model.onCountrySelected(str, parseInt);
        } else {
            Bundle bundle = new Bundle();
            bundle.putString(KEY_COUNTRY, str);
            bundle.putInt(KEY_COUNTRY_CODE, parseInt);
            getParentFragmentManager().setFragmentResult(this.resultKey, bundle);
        }
        NavHostFragment.findNavController(this).navigateUp();
    }

    @Override // androidx.loader.app.LoaderManager.LoaderCallbacks
    public Loader<ArrayList<Map<String, String>>> onCreateLoader(int i, Bundle bundle) {
        return new CountryListLoader(getActivity());
    }

    public void onLoadFinished(Loader<ArrayList<Map<String, String>>> loader, ArrayList<Map<String, String>> arrayList) {
        ((TextView) getListView().getEmptyView()).setText(R.string.country_selection_fragment__no_matching_countries);
        setListAdapter(new SimpleAdapter(getActivity(), arrayList, R.layout.country_list_item, new String[]{"country_name", KEY_COUNTRY_CODE}, new int[]{R.id.country_name, R.id.country_code}));
        applyFilter(this.countryFilter.getText());
    }

    public void applyFilter(CharSequence charSequence) {
        SimpleAdapter simpleAdapter = (SimpleAdapter) getListAdapter();
        if (simpleAdapter != null) {
            simpleAdapter.getFilter().filter(charSequence);
        }
    }

    public void onLoaderReset(Loader<ArrayList<Map<String, String>>> loader) {
        setListAdapter(null);
    }

    /* loaded from: classes4.dex */
    private class FilterWatcher implements TextWatcher {
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        private FilterWatcher() {
            CountryPickerFragment.this = r1;
        }

        public void afterTextChanged(Editable editable) {
            CountryPickerFragment.this.applyFilter(editable);
        }
    }
}
