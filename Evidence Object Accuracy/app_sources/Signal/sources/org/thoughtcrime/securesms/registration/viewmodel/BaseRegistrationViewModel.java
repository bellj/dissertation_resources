package org.thoughtcrime.securesms.registration.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.pin.KbsRepository;
import org.thoughtcrime.securesms.pin.TokenData;
import org.thoughtcrime.securesms.registration.RequestVerificationCodeResponseProcessor;
import org.thoughtcrime.securesms.registration.VerifyAccountRepository;
import org.thoughtcrime.securesms.registration.VerifyAccountResponseProcessor;
import org.thoughtcrime.securesms.registration.VerifyAccountResponseWithFailedKbs;
import org.thoughtcrime.securesms.registration.VerifyAccountResponseWithSuccessfulKbs;
import org.thoughtcrime.securesms.registration.VerifyAccountResponseWithoutKbs;
import org.thoughtcrime.securesms.registration.VerifyCodeWithRegistrationLockResponseProcessor;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.push.VerifyAccountResponse;

/* loaded from: classes4.dex */
public abstract class BaseRegistrationViewModel extends ViewModel {
    private static final long FIRST_CALL_AVAILABLE_AFTER_MS;
    private static final String STATE_CAN_CALL_AT_TIME;
    private static final String STATE_CAPTCHA;
    private static final String STATE_KBS_TOKEN;
    private static final String STATE_NUMBER;
    private static final String STATE_REGISTRATION_SECRET;
    private static final String STATE_REQUEST_RATE_LIMITER;
    private static final String STATE_SUCCESSFUL_CODE_REQUEST_ATTEMPTS;
    private static final String STATE_TIME_REMAINING;
    private static final String STATE_VERIFICATION_CODE;
    private static final long SUBSEQUENT_CALL_AVAILABLE_AFTER_MS;
    protected final KbsRepository kbsRepository;
    protected final SavedStateHandle savedState;
    protected final VerifyAccountRepository verifyAccountRepository;

    protected abstract Single<VerifyAccountResponseProcessor> onVerifySuccess(VerifyAccountResponseProcessor verifyAccountResponseProcessor);

    protected abstract Single<VerifyCodeWithRegistrationLockResponseProcessor> onVerifySuccessWithRegistrationLock(VerifyCodeWithRegistrationLockResponseProcessor verifyCodeWithRegistrationLockResponseProcessor, String str);

    protected abstract Single<ServiceResponse<VerifyAccountRepository.VerifyAccountWithRegistrationLockResponse>> verifyAccountWithRegistrationLock(String str, TokenData tokenData);

    protected abstract Single<ServiceResponse<VerifyAccountResponse>> verifyAccountWithoutRegistrationLock();

    static {
        TimeUnit timeUnit = TimeUnit.SECONDS;
        FIRST_CALL_AVAILABLE_AFTER_MS = timeUnit.toMillis(64);
        SUBSEQUENT_CALL_AVAILABLE_AFTER_MS = timeUnit.toMillis(300);
    }

    public BaseRegistrationViewModel(SavedStateHandle savedStateHandle, VerifyAccountRepository verifyAccountRepository, KbsRepository kbsRepository, String str) {
        this.savedState = savedStateHandle;
        this.verifyAccountRepository = verifyAccountRepository;
        this.kbsRepository = kbsRepository;
        setInitialDefaultValue(STATE_NUMBER, NumberViewState.INITIAL);
        setInitialDefaultValue(STATE_REGISTRATION_SECRET, str);
        setInitialDefaultValue(STATE_VERIFICATION_CODE, "");
        setInitialDefaultValue(STATE_SUCCESSFUL_CODE_REQUEST_ATTEMPTS, 0);
        setInitialDefaultValue(STATE_REQUEST_RATE_LIMITER, new LocalCodeRequestRateLimiter(60000));
    }

    public <T> void setInitialDefaultValue(String str, T t) {
        if (!this.savedState.contains(str) || this.savedState.get(str) == null) {
            this.savedState.set(str, t);
        }
    }

    public NumberViewState getNumber() {
        return (NumberViewState) this.savedState.get(STATE_NUMBER);
    }

    public LiveData<NumberViewState> getLiveNumber() {
        return this.savedState.getLiveData(STATE_NUMBER);
    }

    public void onCountrySelected(String str, int i) {
        setViewState(getNumber().toBuilder().selectedCountryDisplayName(str).countryCode(i).build());
    }

    public void setNationalNumber(String str) {
        setViewState(getNumber().toBuilder().nationalNumber(str).build());
    }

    public void setViewState(NumberViewState numberViewState) {
        if (!numberViewState.equals(getNumber())) {
            this.savedState.set(STATE_NUMBER, numberViewState);
        }
    }

    public String getRegistrationSecret() {
        return (String) this.savedState.get(STATE_REGISTRATION_SECRET);
    }

    public String getTextCodeEntered() {
        return (String) this.savedState.get(STATE_VERIFICATION_CODE);
    }

    public String getCaptchaToken() {
        return (String) this.savedState.get(STATE_CAPTCHA);
    }

    public boolean hasCaptchaToken() {
        return getCaptchaToken() != null;
    }

    public void setCaptchaResponse(String str) {
        this.savedState.set(STATE_CAPTCHA, str);
    }

    public void clearCaptchaResponse() {
        setCaptchaResponse(null);
    }

    public void onVerificationCodeEntered(String str) {
        this.savedState.set(STATE_VERIFICATION_CODE, str);
    }

    public void markASuccessfulAttempt() {
        SavedStateHandle savedStateHandle = this.savedState;
        savedStateHandle.set(STATE_SUCCESSFUL_CODE_REQUEST_ATTEMPTS, Integer.valueOf(((Integer) savedStateHandle.get(STATE_SUCCESSFUL_CODE_REQUEST_ATTEMPTS)).intValue() + 1));
    }

    public LiveData<Integer> getSuccessfulCodeRequestAttempts() {
        return this.savedState.getLiveData(STATE_SUCCESSFUL_CODE_REQUEST_ATTEMPTS, 0);
    }

    public LocalCodeRequestRateLimiter getRequestLimiter() {
        return (LocalCodeRequestRateLimiter) this.savedState.get(STATE_REQUEST_RATE_LIMITER);
    }

    public void updateLimiter() {
        SavedStateHandle savedStateHandle = this.savedState;
        savedStateHandle.set(STATE_REQUEST_RATE_LIMITER, savedStateHandle.get(STATE_REQUEST_RATE_LIMITER));
    }

    public TokenData getKeyBackupCurrentToken() {
        return (TokenData) this.savedState.get(STATE_KBS_TOKEN);
    }

    public void setKeyBackupTokenData(TokenData tokenData) {
        this.savedState.set(STATE_KBS_TOKEN, tokenData);
    }

    public LiveData<Long> getLockedTimeRemaining() {
        return this.savedState.getLiveData(STATE_TIME_REMAINING, 0L);
    }

    public LiveData<Long> getCanCallAtTime() {
        return this.savedState.getLiveData(STATE_CAN_CALL_AT_TIME, 0L);
    }

    public void setLockedTimeRemaining(long j) {
        this.savedState.set(STATE_TIME_REMAINING, Long.valueOf(j));
    }

    public void onStartEnterCode() {
        this.savedState.set(STATE_CAN_CALL_AT_TIME, Long.valueOf(System.currentTimeMillis() + FIRST_CALL_AVAILABLE_AFTER_MS));
    }

    public void onCallRequested() {
        this.savedState.set(STATE_CAN_CALL_AT_TIME, Long.valueOf(System.currentTimeMillis() + SUBSEQUENT_CALL_AVAILABLE_AFTER_MS));
    }

    public Single<RequestVerificationCodeResponseProcessor> requestVerificationCode(VerifyAccountRepository.Mode mode) {
        String captchaToken = getCaptchaToken();
        clearCaptchaResponse();
        if (mode == VerifyAccountRepository.Mode.PHONE_CALL) {
            onCallRequested();
        } else if (!getRequestLimiter().canRequest(mode, getNumber().getE164Number(), System.currentTimeMillis())) {
            return Single.just(RequestVerificationCodeResponseProcessor.forLocalRateLimit());
        }
        return this.verifyAccountRepository.requestVerificationCode(getNumber().getE164Number(), getRegistrationSecret(), mode, captchaToken).map(new Function() { // from class: org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel$$ExternalSyntheticLambda3
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return new RequestVerificationCodeResponseProcessor((ServiceResponse) obj);
            }
        }).observeOn(AndroidSchedulers.mainThread()).doOnSuccess(new Consumer(mode) { // from class: org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel$$ExternalSyntheticLambda4
            public final /* synthetic */ VerifyAccountRepository.Mode f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                BaseRegistrationViewModel.this.lambda$requestVerificationCode$0(this.f$1, (RequestVerificationCodeResponseProcessor) obj);
            }
        });
    }

    public /* synthetic */ void lambda$requestVerificationCode$0(VerifyAccountRepository.Mode mode, RequestVerificationCodeResponseProcessor requestVerificationCodeResponseProcessor) throws Throwable {
        if (requestVerificationCodeResponseProcessor.hasResult()) {
            markASuccessfulAttempt();
            getRequestLimiter().onSuccessfulRequest(mode, getNumber().getE164Number(), System.currentTimeMillis());
        } else {
            getRequestLimiter().onUnsuccessfulRequest();
        }
        updateLimiter();
    }

    public Single<VerifyAccountResponseProcessor> verifyCodeWithoutRegistrationLock(String str) {
        onVerificationCodeEntered(str);
        return verifyAccountWithoutRegistrationLock().map(new BaseRegistrationViewModel$$ExternalSyntheticLambda0()).flatMap(new Function() { // from class: org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return BaseRegistrationViewModel.this.lambda$verifyCodeWithoutRegistrationLock$2((VerifyAccountResponseWithoutKbs) obj);
            }
        }).observeOn(AndroidSchedulers.mainThread()).doOnSuccess(new Consumer() { // from class: org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                BaseRegistrationViewModel.this.lambda$verifyCodeWithoutRegistrationLock$3((VerifyAccountResponseProcessor) obj);
            }
        });
    }

    public /* synthetic */ SingleSource lambda$verifyCodeWithoutRegistrationLock$2(VerifyAccountResponseWithoutKbs verifyAccountResponseWithoutKbs) throws Throwable {
        if (verifyAccountResponseWithoutKbs.hasResult()) {
            return onVerifySuccess(verifyAccountResponseWithoutKbs);
        }
        if (!verifyAccountResponseWithoutKbs.registrationLock() || verifyAccountResponseWithoutKbs.isKbsLocked()) {
            return Single.just(verifyAccountResponseWithoutKbs);
        }
        return this.kbsRepository.getToken(verifyAccountResponseWithoutKbs.getLockedException().getBasicStorageCredentials()).map(new Function() { // from class: org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel$$ExternalSyntheticLambda8
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return BaseRegistrationViewModel.lambda$verifyCodeWithoutRegistrationLock$1(VerifyAccountResponseWithoutKbs.this, (ServiceResponse) obj);
            }
        });
    }

    public static /* synthetic */ VerifyAccountResponseProcessor lambda$verifyCodeWithoutRegistrationLock$1(VerifyAccountResponseWithoutKbs verifyAccountResponseWithoutKbs, ServiceResponse serviceResponse) throws Throwable {
        if (serviceResponse.getResult().isPresent()) {
            return new VerifyAccountResponseWithSuccessfulKbs(verifyAccountResponseWithoutKbs.getResponse(), (TokenData) serviceResponse.getResult().get());
        }
        return new VerifyAccountResponseWithFailedKbs(serviceResponse);
    }

    public /* synthetic */ void lambda$verifyCodeWithoutRegistrationLock$3(VerifyAccountResponseProcessor verifyAccountResponseProcessor) throws Throwable {
        if (verifyAccountResponseProcessor.registrationLock() && !verifyAccountResponseProcessor.isKbsLocked()) {
            setLockedTimeRemaining(verifyAccountResponseProcessor.getLockedException().getTimeRemaining());
            setKeyBackupTokenData(verifyAccountResponseProcessor.getTokenData());
        } else if (verifyAccountResponseProcessor.isKbsLocked()) {
            setLockedTimeRemaining(verifyAccountResponseProcessor.getLockedException().getTimeRemaining());
        }
    }

    public Single<VerifyCodeWithRegistrationLockResponseProcessor> verifyCodeAndRegisterAccountWithRegistrationLock(String str) {
        TokenData keyBackupCurrentToken = getKeyBackupCurrentToken();
        Objects.requireNonNull(keyBackupCurrentToken);
        return verifyAccountWithRegistrationLock(str, keyBackupCurrentToken).map(new Function() { // from class: org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel$$ExternalSyntheticLambda5
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return BaseRegistrationViewModel.lambda$verifyCodeAndRegisterAccountWithRegistrationLock$4(TokenData.this, (ServiceResponse) obj);
            }
        }).flatMap(new Function(str, keyBackupCurrentToken) { // from class: org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel$$ExternalSyntheticLambda6
            public final /* synthetic */ String f$1;
            public final /* synthetic */ TokenData f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return BaseRegistrationViewModel.this.lambda$verifyCodeAndRegisterAccountWithRegistrationLock$5(this.f$1, this.f$2, (VerifyCodeWithRegistrationLockResponseProcessor) obj);
            }
        }).observeOn(AndroidSchedulers.mainThread()).doOnSuccess(new Consumer() { // from class: org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel$$ExternalSyntheticLambda7
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                BaseRegistrationViewModel.this.lambda$verifyCodeAndRegisterAccountWithRegistrationLock$6((VerifyCodeWithRegistrationLockResponseProcessor) obj);
            }
        });
    }

    public static /* synthetic */ VerifyCodeWithRegistrationLockResponseProcessor lambda$verifyCodeAndRegisterAccountWithRegistrationLock$4(TokenData tokenData, ServiceResponse serviceResponse) throws Throwable {
        return new VerifyCodeWithRegistrationLockResponseProcessor(serviceResponse, tokenData);
    }

    public /* synthetic */ SingleSource lambda$verifyCodeAndRegisterAccountWithRegistrationLock$5(String str, TokenData tokenData, VerifyCodeWithRegistrationLockResponseProcessor verifyCodeWithRegistrationLockResponseProcessor) throws Throwable {
        if (verifyCodeWithRegistrationLockResponseProcessor.hasResult()) {
            return onVerifySuccessWithRegistrationLock(verifyCodeWithRegistrationLockResponseProcessor, str);
        }
        if (!verifyCodeWithRegistrationLockResponseProcessor.wrongPin()) {
            return Single.just(verifyCodeWithRegistrationLockResponseProcessor);
        }
        return Single.just(new VerifyCodeWithRegistrationLockResponseProcessor(verifyCodeWithRegistrationLockResponseProcessor.getResponse(), TokenData.withResponse(tokenData, verifyCodeWithRegistrationLockResponseProcessor.getTokenResponse())));
    }

    public /* synthetic */ void lambda$verifyCodeAndRegisterAccountWithRegistrationLock$6(VerifyCodeWithRegistrationLockResponseProcessor verifyCodeWithRegistrationLockResponseProcessor) throws Throwable {
        if (verifyCodeWithRegistrationLockResponseProcessor.wrongPin()) {
            setKeyBackupTokenData(verifyCodeWithRegistrationLockResponseProcessor.getToken());
        }
    }
}
