package org.thoughtcrime.securesms.registration.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.ActivityNavigator;
import java.util.Arrays;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.MainActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.MultiDeviceProfileContentUpdateJob;
import org.thoughtcrime.securesms.jobs.MultiDeviceProfileKeyUpdateJob;
import org.thoughtcrime.securesms.jobs.ProfileUploadJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.lock.v2.CreateKbsPinActivity;
import org.thoughtcrime.securesms.pin.PinRestoreActivity;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.profiles.edit.EditProfileActivity;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.registration.RegistrationUtil;
import org.thoughtcrime.securesms.registration.viewmodel.RegistrationViewModel;

/* loaded from: classes4.dex */
public final class RegistrationCompleteFragment extends LoggingFragment {
    private static final String TAG = Log.tag(RegistrationCompleteFragment.class);

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_registration_blank, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        FragmentActivity requireActivity = requireActivity();
        RegistrationViewModel registrationViewModel = (RegistrationViewModel) new ViewModelProvider(requireActivity).get(RegistrationViewModel.class);
        if (SignalStore.storageService().needsAccountRestore()) {
            Log.i(TAG, "Performing pin restore");
            requireActivity.startActivity(new Intent(requireActivity, PinRestoreActivity.class));
        } else if (!registrationViewModel.isReregister()) {
            boolean z = Recipient.self().getProfileName().isEmpty() || !AvatarHelper.hasAvatar(requireActivity, Recipient.self().getId());
            boolean z2 = !SignalStore.kbsValues().hasPin();
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Pin restore flow not required. profile name: ");
            sb.append(Recipient.self().getProfileName().isEmpty());
            sb.append(" profile avatar: ");
            sb.append(!AvatarHelper.hasAvatar(requireActivity, Recipient.self().getId()));
            sb.append(" needsPin:");
            sb.append(z2);
            Log.i(str, sb.toString());
            Intent clearTop = MainActivity.clearTop(requireActivity);
            if (z2) {
                clearTop = chainIntents(CreateKbsPinActivity.getIntentForPinCreate(requireContext()), clearTop);
            }
            if (z) {
                clearTop = chainIntents(EditProfileActivity.getIntentForUserProfile(requireActivity), clearTop);
            }
            if (!z && !z2) {
                ApplicationDependencies.getJobManager().startChain(new ProfileUploadJob()).then(Arrays.asList(new MultiDeviceProfileKeyUpdateJob(), new MultiDeviceProfileContentUpdateJob())).enqueue();
                RegistrationUtil.maybeMarkRegistrationComplete(requireContext());
            }
            requireActivity.startActivity(clearTop);
        }
        requireActivity.finish();
        ActivityNavigator.applyPopAnimationsToPendingTransition(requireActivity);
    }

    private static Intent chainIntents(Intent intent, Intent intent2) {
        intent.putExtra("next_intent", intent2);
        return intent;
    }
}
