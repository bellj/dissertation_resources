package org.thoughtcrime.securesms.shakereport;

import android.app.Application;
import android.content.DialogInterface;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import j$.util.Optional;
import java.lang.ref.WeakReference;
import java.util.Collections;
import org.signal.core.util.ShakeDetector;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragmentArgs;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.logsubmit.SubmitDebugLogRepository;
import org.thoughtcrime.securesms.sharing.MultiShareArgs;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;

/* loaded from: classes.dex */
public final class ShakeToReport implements ShakeDetector.Listener {
    private static final String TAG = Log.tag(ShakeToReport.class);
    private final Application application;
    private final ShakeDetector detector = new ShakeDetector(this);
    private WeakReference<AppCompatActivity> weakActivity = new WeakReference<>(null);

    public ShakeToReport(Application application) {
        this.application = application;
    }

    public void enable() {
        if (SignalStore.internalValues().shakeToReport()) {
            this.detector.start(ServiceUtil.getSensorManager(this.application));
        }
    }

    public void disable() {
        if (SignalStore.internalValues().shakeToReport()) {
            this.detector.stop();
        }
    }

    public void registerActivity(AppCompatActivity appCompatActivity) {
        if (SignalStore.internalValues().shakeToReport()) {
            this.weakActivity = new WeakReference<>(appCompatActivity);
        }
    }

    @Override // org.signal.core.util.ShakeDetector.Listener
    public void onShakeDetected() {
        if (SignalStore.internalValues().shakeToReport()) {
            AppCompatActivity appCompatActivity = this.weakActivity.get();
            if (appCompatActivity == null) {
                Log.w(TAG, "No registered activity!");
            } else if (appCompatActivity.getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED)) {
                disable();
                new MaterialAlertDialogBuilder(appCompatActivity).setTitle(R.string.ShakeToReport_shake_detected).setMessage(R.string.ShakeToReport_submit_debug_log).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.shakereport.ShakeToReport$$ExternalSyntheticLambda4
                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        ShakeToReport.this.lambda$onShakeDetected$0(dialogInterface, i);
                    }
                }).setPositiveButton(R.string.ShakeToReport_submit, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(appCompatActivity) { // from class: org.thoughtcrime.securesms.shakereport.ShakeToReport$$ExternalSyntheticLambda5
                    public final /* synthetic */ AppCompatActivity f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        ShakeToReport.this.lambda$onShakeDetected$1(this.f$1, dialogInterface, i);
                    }
                }).show();
            }
        }
    }

    public /* synthetic */ void lambda$onShakeDetected$0(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        enableIfVisible();
    }

    public /* synthetic */ void lambda$onShakeDetected$1(AppCompatActivity appCompatActivity, DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        submitLog(appCompatActivity);
    }

    private void submitLog(AppCompatActivity appCompatActivity) {
        AlertDialog show = SimpleProgressDialog.show(appCompatActivity);
        SubmitDebugLogRepository submitDebugLogRepository = new SubmitDebugLogRepository();
        Log.i(TAG, "Submitting log...");
        submitDebugLogRepository.buildAndSubmitLog(new SubmitDebugLogRepository.Callback(show, appCompatActivity) { // from class: org.thoughtcrime.securesms.shakereport.ShakeToReport$$ExternalSyntheticLambda1
            public final /* synthetic */ AlertDialog f$1;
            public final /* synthetic */ AppCompatActivity f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.thoughtcrime.securesms.logsubmit.SubmitDebugLogRepository.Callback
            public final void onResult(Object obj) {
                ShakeToReport.this.lambda$submitLog$3(this.f$1, this.f$2, (Optional) obj);
            }
        });
    }

    public /* synthetic */ void lambda$submitLog$3(AlertDialog alertDialog, AppCompatActivity appCompatActivity, Optional optional) {
        Log.i(TAG, "Logs uploaded!");
        ThreadUtil.runOnMain(new Runnable(alertDialog, optional, appCompatActivity) { // from class: org.thoughtcrime.securesms.shakereport.ShakeToReport$$ExternalSyntheticLambda0
            public final /* synthetic */ AlertDialog f$1;
            public final /* synthetic */ Optional f$2;
            public final /* synthetic */ AppCompatActivity f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ShakeToReport.this.lambda$submitLog$2(this.f$1, this.f$2, this.f$3);
            }
        });
    }

    public /* synthetic */ void lambda$submitLog$2(AlertDialog alertDialog, Optional optional, AppCompatActivity appCompatActivity) {
        alertDialog.dismiss();
        if (optional.isPresent()) {
            showPostSubmitDialog(appCompatActivity, (String) optional.get());
            return;
        }
        Toast.makeText(appCompatActivity, (int) R.string.ShakeToReport_failed_to_submit, 0).show();
        enableIfVisible();
    }

    private void showPostSubmitDialog(AppCompatActivity appCompatActivity, String str) {
        ((TextView) new MaterialAlertDialogBuilder(appCompatActivity).setTitle(R.string.ShakeToReport_success).setMessage((CharSequence) str).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.shakereport.ShakeToReport$$ExternalSyntheticLambda2
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ShakeToReport.this.lambda$showPostSubmitDialog$4(dialogInterface, i);
            }
        }).setPositiveButton(R.string.ShakeToReport_share, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(appCompatActivity, str) { // from class: org.thoughtcrime.securesms.shakereport.ShakeToReport$$ExternalSyntheticLambda3
            public final /* synthetic */ AppCompatActivity f$1;
            public final /* synthetic */ String f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ShakeToReport.this.lambda$showPostSubmitDialog$5(this.f$1, this.f$2, dialogInterface, i);
            }
        }).show().findViewById(16908299)).setTextIsSelectable(true);
    }

    public /* synthetic */ void lambda$showPostSubmitDialog$4(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        enableIfVisible();
    }

    public /* synthetic */ void lambda$showPostSubmitDialog$5(AppCompatActivity appCompatActivity, String str, DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        enableIfVisible();
        MultiselectForwardFragment.showFullScreen(appCompatActivity.getSupportFragmentManager(), new MultiselectForwardFragmentArgs(true, Collections.singletonList(new MultiShareArgs.Builder().withDraftText(str).build()), R.string.MultiselectForwardFragment__share_with));
    }

    private void enableIfVisible() {
        if (ApplicationDependencies.getAppForegroundObserver().isForegrounded()) {
            enable();
        }
    }
}
