package org.thoughtcrime.securesms;

import android.content.DialogInterface;
import org.thoughtcrime.securesms.InviteActivity;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class InviteActivity$SmsSendClickListener$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ InviteActivity.SmsSendClickListener f$0;

    public /* synthetic */ InviteActivity$SmsSendClickListener$$ExternalSyntheticLambda0(InviteActivity.SmsSendClickListener smsSendClickListener) {
        this.f$0 = smsSendClickListener;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onClick$0(dialogInterface, i);
    }
}
