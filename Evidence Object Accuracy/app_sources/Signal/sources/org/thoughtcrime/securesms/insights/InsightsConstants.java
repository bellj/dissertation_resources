package org.thoughtcrime.securesms.insights;

import java.util.concurrent.TimeUnit;

/* loaded from: classes4.dex */
public final class InsightsConstants {
    public static final long PERIOD_IN_DAYS;
    public static final long PERIOD_IN_MILLIS = TimeUnit.DAYS.toMillis(7);

    private InsightsConstants() {
    }
}
