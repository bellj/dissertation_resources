package org.thoughtcrime.securesms.insights;

import android.content.Context;
import androidx.core.util.Consumer;
import com.annimon.stream.Stream;
import j$.util.Optional;
import java.util.List;
import java.util.Objects;
import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contacts.avatars.GeneratedContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.ProfileContactPhoto;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.MmsSmsDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.insights.InsightsDashboardViewModel;
import org.thoughtcrime.securesms.insights.InsightsModalViewModel;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.sms.OutgoingTextMessage;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class InsightsRepository implements InsightsDashboardViewModel.Repository, InsightsModalViewModel.Repository {
    private final Context context;

    public InsightsRepository(Context context) {
        this.context = context.getApplicationContext();
    }

    @Override // org.thoughtcrime.securesms.insights.InsightsDashboardViewModel.Repository, org.thoughtcrime.securesms.insights.InsightsModalViewModel.Repository
    public void getInsightsData(Consumer<InsightsData> consumer) {
        InsightsRepository$$ExternalSyntheticLambda6 insightsRepository$$ExternalSyntheticLambda6 = new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.insights.InsightsRepository$$ExternalSyntheticLambda6
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return InsightsRepository.lambda$getInsightsData$0();
            }
        };
        Objects.requireNonNull(consumer);
        SimpleTask.run(insightsRepository$$ExternalSyntheticLambda6, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.insights.InsightsRepository$$ExternalSyntheticLambda7
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                Consumer.this.accept((InsightsData) obj);
            }
        });
    }

    public static /* synthetic */ InsightsData lambda$getInsightsData$0() {
        MmsSmsDatabase mmsSms = SignalDatabase.mmsSms();
        int insecureMessageCountForInsights = mmsSms.getInsecureMessageCountForInsights();
        int secureMessageCountForInsights = mmsSms.getSecureMessageCountForInsights() + insecureMessageCountForInsights;
        if (secureMessageCountForInsights == 0) {
            return new InsightsData(false, 0);
        }
        return new InsightsData(true, Util.clamp((int) Math.ceil((double) ((((float) insecureMessageCountForInsights) * 100.0f) / ((float) secureMessageCountForInsights))), 0, 100));
    }

    @Override // org.thoughtcrime.securesms.insights.InsightsDashboardViewModel.Repository
    public void getInsecureRecipients(Consumer<List<Recipient>> consumer) {
        InsightsRepository$$ExternalSyntheticLambda0 insightsRepository$$ExternalSyntheticLambda0 = new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.insights.InsightsRepository$$ExternalSyntheticLambda0
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return InsightsRepository.lambda$getInsecureRecipients$1();
            }
        };
        Objects.requireNonNull(consumer);
        SimpleTask.run(insightsRepository$$ExternalSyntheticLambda0, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.insights.InsightsRepository$$ExternalSyntheticLambda1
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                Consumer.this.accept((List) obj);
            }
        });
    }

    public static /* synthetic */ List lambda$getInsecureRecipients$1() {
        return Stream.of(SignalDatabase.recipients().getUninvitedRecipientsForInsights()).map(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8()).toList();
    }

    @Override // org.thoughtcrime.securesms.insights.InsightsDashboardViewModel.Repository, org.thoughtcrime.securesms.insights.InsightsModalViewModel.Repository
    public void getUserAvatar(Consumer<InsightsUserAvatar> consumer) {
        InsightsRepository$$ExternalSyntheticLambda4 insightsRepository$$ExternalSyntheticLambda4 = new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.insights.InsightsRepository$$ExternalSyntheticLambda4
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return InsightsRepository.this.lambda$getUserAvatar$2();
            }
        };
        Objects.requireNonNull(consumer);
        SimpleTask.run(insightsRepository$$ExternalSyntheticLambda4, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.insights.InsightsRepository$$ExternalSyntheticLambda5
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                Consumer.this.accept((InsightsUserAvatar) obj);
            }
        });
    }

    public /* synthetic */ InsightsUserAvatar lambda$getUserAvatar$2() {
        Recipient resolve = Recipient.self().resolve();
        return new InsightsUserAvatar(new ProfileContactPhoto(resolve), resolve.getAvatarColor(), new GeneratedContactPhoto((String) Optional.of(resolve.getDisplayName(this.context)).orElse(""), R.drawable.ic_profile_outline_40));
    }

    @Override // org.thoughtcrime.securesms.insights.InsightsDashboardViewModel.Repository
    public void sendSmsInvite(Recipient recipient, Runnable runnable) {
        SimpleTask.run(new SimpleTask.BackgroundTask(recipient) { // from class: org.thoughtcrime.securesms.insights.InsightsRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ Recipient f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return InsightsRepository.this.lambda$sendSmsInvite$3(this.f$1);
            }
        }, new SimpleTask.ForegroundTask(runnable) { // from class: org.thoughtcrime.securesms.insights.InsightsRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ Runnable f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                this.f$0.run();
            }
        });
    }

    public /* synthetic */ Object lambda$sendSmsInvite$3(Recipient recipient) {
        Recipient resolve = recipient.resolve();
        int intValue = resolve.getDefaultSubscriptionId().orElse(-1).intValue();
        Context context = this.context;
        MessageSender.send(this.context, new OutgoingTextMessage(resolve, context.getString(R.string.InviteActivity_lets_switch_to_signal, context.getString(R.string.install_url)), intValue), -1L, true, (String) null, (MessageDatabase.InsertListener) null);
        SignalDatabase.recipients().setHasSentInvite(recipient.getId());
        return null;
    }
}
