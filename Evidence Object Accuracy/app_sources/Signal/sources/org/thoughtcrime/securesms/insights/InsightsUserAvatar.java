package org.thoughtcrime.securesms.insights;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.ProfileContactPhoto;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.mms.GlideApp;

/* loaded from: classes4.dex */
public class InsightsUserAvatar {
    private final AvatarColor fallbackColor;
    private final FallbackContactPhoto fallbackContactPhoto;
    private final ProfileContactPhoto profileContactPhoto;

    public InsightsUserAvatar(ProfileContactPhoto profileContactPhoto, AvatarColor avatarColor, FallbackContactPhoto fallbackContactPhoto) {
        this.profileContactPhoto = profileContactPhoto;
        this.fallbackColor = avatarColor;
        this.fallbackContactPhoto = fallbackContactPhoto;
    }

    private Drawable fallbackDrawable(Context context) {
        return this.fallbackContactPhoto.asDrawable(context, this.fallbackColor);
    }

    public void load(ImageView imageView) {
        GlideApp.with(imageView).load((Object) this.profileContactPhoto).error(fallbackDrawable(imageView.getContext())).circleCrop().diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
    }
}
