package org.thoughtcrime.securesms.insights;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.core.util.Consumer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.recipients.Recipient;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public final class InsightsInsecureRecipientsAdapter extends RecyclerView.Adapter<ViewHolder> {
    private List<Recipient> data = Collections.emptyList();
    private final Consumer<Recipient> onInviteClickedConsumer;

    public InsightsInsecureRecipientsAdapter(Consumer<Recipient> consumer) {
        this.onInviteClickedConsumer = consumer;
    }

    public void updateData(List<Recipient> list) {
        List<Recipient> list2 = this.data;
        this.data = list;
        DiffUtil.calculateDiff(new DiffCallback(list2, list)).dispatchUpdatesTo(this);
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.insights_dashboard_adapter_item, viewGroup, false), new Consumer() { // from class: org.thoughtcrime.securesms.insights.InsightsInsecureRecipientsAdapter$$ExternalSyntheticLambda0
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                InsightsInsecureRecipientsAdapter.this.handleInviteClicked((Integer) obj);
            }
        });
    }

    public void handleInviteClicked(Integer num) {
        this.onInviteClickedConsumer.accept(this.data.get(num.intValue()));
    }

    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.bind(this.data.get(i));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.data.size();
    }

    /* loaded from: classes4.dex */
    public static final class ViewHolder extends RecyclerView.ViewHolder {
        private AvatarImageView avatarImageView;
        private TextView displayName;

        private ViewHolder(View view, Consumer<Integer> consumer) {
            super(view);
            this.avatarImageView = (AvatarImageView) view.findViewById(R.id.recipient_avatar);
            this.displayName = (TextView) view.findViewById(R.id.recipient_display_name);
            ((Button) view.findViewById(R.id.recipient_invite)).setOnClickListener(new InsightsInsecureRecipientsAdapter$ViewHolder$$ExternalSyntheticLambda0(this, consumer));
        }

        public /* synthetic */ void lambda$new$0(Consumer consumer, View view) {
            int adapterPosition = getAdapterPosition();
            if (adapterPosition != -1) {
                consumer.accept(Integer.valueOf(adapterPosition));
            }
        }

        public void bind(Recipient recipient) {
            this.displayName.setText(recipient.getDisplayName(this.itemView.getContext()));
            this.avatarImageView.setAvatar(GlideApp.with(this.itemView), recipient, false);
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public static class DiffCallback extends DiffUtil.Callback {
        private final List<Recipient> newData;
        private final List<Recipient> oldData;

        private DiffCallback(List<Recipient> list, List<Recipient> list2) {
            this.oldData = list;
            this.newData = list2;
        }

        public int getOldListSize() {
            return this.oldData.size();
        }

        public int getNewListSize() {
            return this.newData.size();
        }

        public boolean areItemsTheSame(int i, int i2) {
            return this.oldData.get(i).getId() == this.newData.get(i2).getId();
        }

        public boolean areContentsTheSame(int i, int i2) {
            return this.oldData.get(i).equals(this.newData.get(i2));
        }
    }
}
