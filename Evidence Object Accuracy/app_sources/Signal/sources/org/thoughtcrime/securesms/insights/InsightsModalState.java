package org.thoughtcrime.securesms.insights;

/* loaded from: classes4.dex */
public final class InsightsModalState {
    private final InsightsData insightsData;
    private final InsightsUserAvatar userAvatar;

    private InsightsModalState(Builder builder) {
        this.insightsData = builder.insightsData;
        this.userAvatar = builder.userAvatar;
    }

    public static Builder builder() {
        return new Builder();
    }

    public Builder buildUpon() {
        return builder().withUserAvatar(this.userAvatar).withData(this.insightsData);
    }

    public InsightsUserAvatar getUserAvatar() {
        return this.userAvatar;
    }

    public InsightsData getData() {
        return this.insightsData;
    }

    /* loaded from: classes4.dex */
    public static final class Builder {
        private InsightsData insightsData;
        private InsightsUserAvatar userAvatar;

        private Builder() {
        }

        public Builder withData(InsightsData insightsData) {
            this.insightsData = insightsData;
            return this;
        }

        public Builder withUserAvatar(InsightsUserAvatar insightsUserAvatar) {
            this.userAvatar = insightsUserAvatar;
            return this;
        }

        public InsightsModalState build() {
            return new InsightsModalState(this);
        }
    }
}
