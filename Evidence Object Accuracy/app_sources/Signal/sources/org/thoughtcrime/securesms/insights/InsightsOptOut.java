package org.thoughtcrime.securesms.insights;

import android.content.Context;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public final class InsightsOptOut {
    private static final String INSIGHTS_OPT_OUT_PREFERENCE;

    private InsightsOptOut() {
    }

    public static boolean userHasOptedOut(Context context) {
        return TextSecurePreferences.getBooleanPreference(context, INSIGHTS_OPT_OUT_PREFERENCE, false);
    }

    public static void userRequestedOptOut(Context context) {
        TextSecurePreferences.setBooleanPreference(context, INSIGHTS_OPT_OUT_PREFERENCE, true);
    }
}
