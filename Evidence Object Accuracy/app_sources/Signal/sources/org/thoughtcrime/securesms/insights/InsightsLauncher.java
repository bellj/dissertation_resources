package org.thoughtcrime.securesms.insights;

import android.content.Context;
import androidx.fragment.app.FragmentManager;

/* loaded from: classes4.dex */
public final class InsightsLauncher {
    private static final String MODAL_TAG;

    public static void showInsightsModal(Context context, FragmentManager fragmentManager) {
        if (!InsightsOptOut.userHasOptedOut(context) && fragmentManager.findFragmentByTag(MODAL_TAG) == null) {
            new InsightsModalDialogFragment().show(fragmentManager, MODAL_TAG);
        }
    }

    public static void showInsightsDashboard(FragmentManager fragmentManager) {
        new InsightsDashboardDialogFragment().show(fragmentManager, (String) null);
    }
}
