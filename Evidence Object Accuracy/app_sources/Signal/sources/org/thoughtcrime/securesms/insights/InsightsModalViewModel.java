package org.thoughtcrime.securesms.insights;

import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import org.thoughtcrime.securesms.insights.InsightsModalState;

/* loaded from: classes4.dex */
public final class InsightsModalViewModel extends ViewModel {
    private final MutableLiveData<InsightsModalState> internalState;

    /* loaded from: classes4.dex */
    public interface Repository {
        void getInsightsData(Consumer<InsightsData> consumer);

        void getUserAvatar(Consumer<InsightsUserAvatar> consumer);
    }

    private InsightsModalViewModel(Repository repository) {
        this.internalState = new MutableLiveData<>(InsightsModalState.builder().build());
        repository.getInsightsData(new Consumer() { // from class: org.thoughtcrime.securesms.insights.InsightsModalViewModel$$ExternalSyntheticLambda0
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                InsightsModalViewModel.this.lambda$new$1((InsightsData) obj);
            }
        });
        repository.getUserAvatar(new Consumer() { // from class: org.thoughtcrime.securesms.insights.InsightsModalViewModel$$ExternalSyntheticLambda1
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                InsightsModalViewModel.this.lambda$new$3((InsightsUserAvatar) obj);
            }
        });
    }

    public /* synthetic */ void lambda$new$1(InsightsData insightsData) {
        this.internalState.setValue(getNewState(new Consumer() { // from class: org.thoughtcrime.securesms.insights.InsightsModalViewModel$$ExternalSyntheticLambda2
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                ((InsightsModalState.Builder) obj).withData(InsightsData.this);
            }
        }));
    }

    public /* synthetic */ void lambda$new$3(InsightsUserAvatar insightsUserAvatar) {
        this.internalState.setValue(getNewState(new Consumer() { // from class: org.thoughtcrime.securesms.insights.InsightsModalViewModel$$ExternalSyntheticLambda3
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                ((InsightsModalState.Builder) obj).withUserAvatar(InsightsUserAvatar.this);
            }
        }));
    }

    private InsightsModalState getNewState(Consumer<InsightsModalState.Builder> consumer) {
        InsightsModalState.Builder buildUpon = this.internalState.getValue().buildUpon();
        consumer.accept(buildUpon);
        return buildUpon.build();
    }

    public LiveData<InsightsModalState> getState() {
        return this.internalState;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final Repository repository;

        public Factory(Repository repository) {
            this.repository = repository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return new InsightsModalViewModel(this.repository);
        }
    }
}
