package org.thoughtcrime.securesms.insights;

import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import java.util.List;
import org.thoughtcrime.securesms.insights.InsightsDashboardState;
import org.thoughtcrime.securesms.recipients.Recipient;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public final class InsightsDashboardViewModel extends ViewModel {
    private final MutableLiveData<InsightsDashboardState> internalState;
    private final Repository repository;

    /* loaded from: classes4.dex */
    public interface Repository {
        void getInsecureRecipients(Consumer<List<Recipient>> consumer);

        @Override // org.thoughtcrime.securesms.insights.InsightsModalViewModel.Repository
        void getInsightsData(Consumer<InsightsData> consumer);

        @Override // org.thoughtcrime.securesms.insights.InsightsModalViewModel.Repository
        void getUserAvatar(Consumer<InsightsUserAvatar> consumer);

        void sendSmsInvite(Recipient recipient, Runnable runnable);
    }

    private InsightsDashboardViewModel(Repository repository) {
        this.internalState = new MutableLiveData<>(InsightsDashboardState.builder().build());
        this.repository = repository;
        repository.getInsightsData(new Consumer() { // from class: org.thoughtcrime.securesms.insights.InsightsDashboardViewModel$$ExternalSyntheticLambda5
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                InsightsDashboardViewModel.this.lambda$new$1((InsightsData) obj);
            }
        });
        repository.getUserAvatar(new Consumer() { // from class: org.thoughtcrime.securesms.insights.InsightsDashboardViewModel$$ExternalSyntheticLambda6
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                InsightsDashboardViewModel.this.lambda$new$3((InsightsUserAvatar) obj);
            }
        });
        updateInsecureRecipients();
    }

    public /* synthetic */ void lambda$new$1(InsightsData insightsData) {
        this.internalState.setValue(getNewState(new Consumer() { // from class: org.thoughtcrime.securesms.insights.InsightsDashboardViewModel$$ExternalSyntheticLambda1
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                ((InsightsDashboardState.Builder) obj).withData(InsightsData.this);
            }
        }));
    }

    public /* synthetic */ void lambda$new$3(InsightsUserAvatar insightsUserAvatar) {
        this.internalState.setValue(getNewState(new Consumer() { // from class: org.thoughtcrime.securesms.insights.InsightsDashboardViewModel$$ExternalSyntheticLambda3
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                ((InsightsDashboardState.Builder) obj).withUserAvatar(InsightsUserAvatar.this);
            }
        }));
    }

    public /* synthetic */ void lambda$updateInsecureRecipients$5(List list) {
        this.internalState.setValue(getNewState(new Consumer(list) { // from class: org.thoughtcrime.securesms.insights.InsightsDashboardViewModel$$ExternalSyntheticLambda2
            public final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                ((InsightsDashboardState.Builder) obj).withInsecureRecipients(this.f$0);
            }
        }));
    }

    public void updateInsecureRecipients() {
        this.repository.getInsecureRecipients(new Consumer() { // from class: org.thoughtcrime.securesms.insights.InsightsDashboardViewModel$$ExternalSyntheticLambda0
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                InsightsDashboardViewModel.this.lambda$updateInsecureRecipients$5((List) obj);
            }
        });
    }

    private InsightsDashboardState getNewState(Consumer<InsightsDashboardState.Builder> consumer) {
        InsightsDashboardState.Builder buildUpon = this.internalState.getValue().buildUpon();
        consumer.accept(buildUpon);
        return buildUpon.build();
    }

    public LiveData<InsightsDashboardState> getState() {
        return this.internalState;
    }

    public void sendSmsInvite(Recipient recipient) {
        this.repository.sendSmsInvite(recipient, new Runnable() { // from class: org.thoughtcrime.securesms.insights.InsightsDashboardViewModel$$ExternalSyntheticLambda4
            @Override // java.lang.Runnable
            public final void run() {
                InsightsDashboardViewModel.this.updateInsecureRecipients();
            }
        });
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final Repository repository;

        public Factory(Repository repository) {
            this.repository = repository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return new InsightsDashboardViewModel(this.repository);
        }
    }
}
