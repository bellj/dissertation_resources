package org.thoughtcrime.securesms.insights;

import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public final class InsightsDashboardState {
    private final List<Recipient> insecureRecipients;
    private final InsightsData insightsData;
    private final InsightsUserAvatar userAvatar;

    private InsightsDashboardState(Builder builder) {
        this.insecureRecipients = builder.insecureRecipients;
        this.insightsData = builder.insightsData;
        this.userAvatar = builder.userAvatar;
    }

    public static Builder builder() {
        return new Builder();
    }

    public Builder buildUpon() {
        return builder().withData(this.insightsData).withUserAvatar(this.userAvatar).withInsecureRecipients(this.insecureRecipients);
    }

    public List<Recipient> getInsecureRecipients() {
        return this.insecureRecipients;
    }

    public InsightsUserAvatar getUserAvatar() {
        return this.userAvatar;
    }

    public InsightsData getData() {
        return this.insightsData;
    }

    /* loaded from: classes4.dex */
    public static final class Builder {
        private List<Recipient> insecureRecipients;
        private InsightsData insightsData;
        private InsightsUserAvatar userAvatar;

        private Builder() {
            this.insecureRecipients = Collections.emptyList();
        }

        public Builder withInsecureRecipients(List<Recipient> list) {
            this.insecureRecipients = list;
            return this;
        }

        public Builder withData(InsightsData insightsData) {
            this.insightsData = insightsData;
            return this;
        }

        public Builder withUserAvatar(InsightsUserAvatar insightsUserAvatar) {
            this.userAvatar = insightsUserAvatar;
            return this;
        }

        public InsightsDashboardState build() {
            return new InsightsDashboardState(this);
        }
    }
}
