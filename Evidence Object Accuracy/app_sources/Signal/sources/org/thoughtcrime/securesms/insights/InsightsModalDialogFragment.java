package org.thoughtcrime.securesms.insights;

import android.animation.AnimatorSet;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.ArcProgressBar;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.insights.InsightsAnimatorSetFactory;
import org.thoughtcrime.securesms.insights.InsightsModalViewModel;

/* loaded from: classes4.dex */
public final class InsightsModalDialogFragment extends DialogFragment {
    private AnimatorSet animatorSet;
    private AvatarImageView avatarImageView;
    private ArcProgressBar progress;
    private View progressContainer;
    private TextView securePercentage;

    @Override // androidx.fragment.app.Fragment, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        requireFragmentManager().beginTransaction().detach(this).attach(this).commit();
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(2, R.style.Theme_Signal_Insights_Modal);
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog onCreateDialog(Bundle bundle) {
        Dialog onCreateDialog = super.onCreateDialog(bundle);
        onCreateDialog.getWindow().setBackgroundDrawableResource(17170445);
        return onCreateDialog;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.insights_modal, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        View findViewById = view.findViewById(R.id.insights_modal_close);
        this.progress = (ArcProgressBar) view.findViewById(R.id.insights_modal_progress);
        this.securePercentage = (TextView) view.findViewById(R.id.insights_modal_percent_secure);
        this.avatarImageView = (AvatarImageView) view.findViewById(R.id.insights_modal_avatar);
        this.progressContainer = view.findViewById(R.id.insights_modal_percent_container);
        findViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.insights.InsightsModalDialogFragment$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                InsightsModalDialogFragment.this.lambda$onViewCreated$0(view2);
            }
        });
        ((Button) view.findViewById(R.id.insights_modal_view_insights)).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.insights.InsightsModalDialogFragment$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                InsightsModalDialogFragment.this.lambda$onViewCreated$1(view2);
            }
        });
        initializeViewModel();
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        dismiss();
    }

    public /* synthetic */ void lambda$onViewCreated$1(View view) {
        openInsightsAndDismiss();
    }

    private void initializeViewModel() {
        ((InsightsModalViewModel) ViewModelProviders.of(this, new InsightsModalViewModel.Factory(new InsightsRepository(requireContext()))).get(InsightsModalViewModel.class)).getState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.insights.InsightsModalDialogFragment$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                InsightsModalDialogFragment.this.lambda$initializeViewModel$2((InsightsModalState) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializeViewModel$2(InsightsModalState insightsModalState) {
        updateInsecurePercent(insightsModalState.getData());
        updateUserAvatar(insightsModalState.getUserAvatar());
    }

    private void updateInsecurePercent(InsightsData insightsData) {
        if (insightsData != null && this.animatorSet == null) {
            AnimatorSet create = InsightsAnimatorSetFactory.create(insightsData.getPercentInsecure(), new InsightsAnimatorSetFactory.UpdateListener() { // from class: org.thoughtcrime.securesms.insights.InsightsModalDialogFragment$$ExternalSyntheticLambda0
                @Override // org.thoughtcrime.securesms.insights.InsightsAnimatorSetFactory.UpdateListener
                public final void onUpdate(float f) {
                    InsightsModalDialogFragment.this.setProgressPercentage(f);
                }
            }, null, new InsightsAnimatorSetFactory.UpdateListener() { // from class: org.thoughtcrime.securesms.insights.InsightsModalDialogFragment$$ExternalSyntheticLambda1
                @Override // org.thoughtcrime.securesms.insights.InsightsAnimatorSetFactory.UpdateListener
                public final void onUpdate(float f) {
                    InsightsModalDialogFragment.this.setPercentSecureScale(f);
                }
            }, null);
            this.animatorSet = create;
            create.start();
        }
    }

    public void setProgressPercentage(float f) {
        this.securePercentage.setText(String.valueOf(Math.round(100.0f * f)));
        this.progress.setProgress(f);
    }

    public void setPercentSecureScale(float f) {
        this.progressContainer.setScaleX(f);
        this.progressContainer.setScaleY(f);
    }

    private void updateUserAvatar(InsightsUserAvatar insightsUserAvatar) {
        if (insightsUserAvatar == null) {
            this.avatarImageView.setImageDrawable(null);
        } else {
            insightsUserAvatar.load(this.avatarImageView);
        }
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        InsightsOptOut.userRequestedOptOut(requireContext());
    }

    private void openInsightsAndDismiss() {
        InsightsLauncher.showInsightsDashboard(requireFragmentManager());
        dismiss();
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        AnimatorSet animatorSet = this.animatorSet;
        if (animatorSet != null) {
            animatorSet.cancel();
            this.animatorSet = null;
        }
        super.onDestroyView();
    }
}
