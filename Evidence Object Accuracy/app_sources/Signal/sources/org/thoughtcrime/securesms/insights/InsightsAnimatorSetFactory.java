package org.thoughtcrime.securesms.insights;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.view.animation.DecelerateInterpolator;
import com.annimon.stream.Stream;
import com.annimon.stream.function.IntFunction;
import com.annimon.stream.function.Predicate;
import org.thoughtcrime.securesms.insights.InsightsAnimatorSetFactory;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public final class InsightsAnimatorSetFactory {
    private static final int ANIMATION_START_DELAY;
    private static final int DETAILS_ANIMATION_DURATION;
    private static final int LOTTIE_ANIMATION_DURATION;
    private static final int PERCENT_SECURE_ANIMATION_DURATION;
    private static final float PERCENT_SECURE_MAX_SCALE;
    private static final int PROGRESS_ANIMATION_DURATION;

    /* loaded from: classes4.dex */
    public interface UpdateListener {
        void onUpdate(float f);
    }

    public static /* synthetic */ boolean lambda$create$0(Animator animator) {
        return animator != null;
    }

    private InsightsAnimatorSetFactory() {
    }

    public static AnimatorSet create(int i, UpdateListener updateListener, UpdateListener updateListener2, UpdateListener updateListener3, UpdateListener updateListener4) {
        AnimatorSet animatorSet = new AnimatorSet();
        Animator[] animatorArr = {createProgressAnimator(100 - i, updateListener), createDetailsAnimator(updateListener2), createPercentSecureAnimator(updateListener3), createLottieAnimator(updateListener4)};
        animatorSet.setInterpolator(new DecelerateInterpolator());
        animatorSet.playTogether((ValueAnimator[]) Stream.of(animatorArr).filter(new Predicate() { // from class: org.thoughtcrime.securesms.insights.InsightsAnimatorSetFactory$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return InsightsAnimatorSetFactory.$r8$lambda$JXgJIoaNVcFTj2Kxw6yxBAyiD2s((Animator) obj);
            }
        }).toArray(new IntFunction() { // from class: org.thoughtcrime.securesms.insights.InsightsAnimatorSetFactory$$ExternalSyntheticLambda3
            @Override // com.annimon.stream.function.IntFunction
            public final Object apply(int i2) {
                return InsightsAnimatorSetFactory.$r8$lambda$8rXc0GxbqVlEeCqYEAu_G8Wj_jc(i2);
            }
        }));
        return animatorSet;
    }

    public static /* synthetic */ ValueAnimator[] lambda$create$1(int i) {
        return new ValueAnimator[i];
    }

    private static Animator createProgressAnimator(int i, UpdateListener updateListener) {
        if (updateListener == null) {
            return null;
        }
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, ((float) i) / 100.0f);
        ofFloat.setDuration(800L);
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.thoughtcrime.securesms.insights.InsightsAnimatorSetFactory$$ExternalSyntheticLambda4
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                InsightsAnimatorSetFactory.$r8$lambda$PABQUWIykDFebpsE_zBWvVje9l4(InsightsAnimatorSetFactory.UpdateListener.this, valueAnimator);
            }
        });
        return ofFloat;
    }

    public static /* synthetic */ void lambda$createProgressAnimator$2(UpdateListener updateListener, ValueAnimator valueAnimator) {
        updateListener.onUpdate(((Float) valueAnimator.getAnimatedValue()).floatValue());
    }

    private static Animator createDetailsAnimator(UpdateListener updateListener) {
        if (updateListener == null) {
            return null;
        }
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
        ofFloat.setDuration(200L);
        ofFloat.setStartDelay(600);
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.thoughtcrime.securesms.insights.InsightsAnimatorSetFactory$$ExternalSyntheticLambda1
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                InsightsAnimatorSetFactory.m1876$r8$lambda$b43KtaFg6TndwDW60P_pX_7g0(InsightsAnimatorSetFactory.UpdateListener.this, valueAnimator);
            }
        });
        return ofFloat;
    }

    public static /* synthetic */ void lambda$createDetailsAnimator$3(UpdateListener updateListener, ValueAnimator valueAnimator) {
        updateListener.onUpdate(((Float) valueAnimator.getAnimatedValue()).floatValue());
    }

    private static Animator createPercentSecureAnimator(UpdateListener updateListener) {
        if (updateListener == null) {
            return null;
        }
        ValueAnimator ofFloat = ValueAnimator.ofFloat(1.0f, PERCENT_SECURE_MAX_SCALE, 1.0f);
        ofFloat.setStartDelay(600);
        ofFloat.setDuration(400L);
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.thoughtcrime.securesms.insights.InsightsAnimatorSetFactory$$ExternalSyntheticLambda0
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                InsightsAnimatorSetFactory.$r8$lambda$fSoAhC1vIBg_nXLsfDQySn5Ktio(InsightsAnimatorSetFactory.UpdateListener.this, valueAnimator);
            }
        });
        return ofFloat;
    }

    public static /* synthetic */ void lambda$createPercentSecureAnimator$4(UpdateListener updateListener, ValueAnimator valueAnimator) {
        updateListener.onUpdate(((Float) valueAnimator.getAnimatedValue()).floatValue());
    }

    private static Animator createLottieAnimator(UpdateListener updateListener) {
        if (updateListener == null) {
            return null;
        }
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
        ofFloat.setStartDelay(600);
        ofFloat.setDuration(1500L);
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.thoughtcrime.securesms.insights.InsightsAnimatorSetFactory$$ExternalSyntheticLambda5
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                InsightsAnimatorSetFactory.$r8$lambda$J720rZURaqnUmIeys7mdcKBwyLs(InsightsAnimatorSetFactory.UpdateListener.this, valueAnimator);
            }
        });
        return ofFloat;
    }

    public static /* synthetic */ void lambda$createLottieAnimator$5(UpdateListener updateListener, ValueAnimator valueAnimator) {
        updateListener.onUpdate(((Float) valueAnimator.getAnimatedValue()).floatValue());
    }
}
