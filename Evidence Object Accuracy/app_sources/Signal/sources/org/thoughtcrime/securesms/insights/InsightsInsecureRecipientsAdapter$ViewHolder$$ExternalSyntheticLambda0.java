package org.thoughtcrime.securesms.insights;

import android.view.View;
import androidx.core.util.Consumer;
import org.thoughtcrime.securesms.insights.InsightsInsecureRecipientsAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class InsightsInsecureRecipientsAdapter$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ InsightsInsecureRecipientsAdapter.ViewHolder f$0;
    public final /* synthetic */ Consumer f$1;

    public /* synthetic */ InsightsInsecureRecipientsAdapter$ViewHolder$$ExternalSyntheticLambda0(InsightsInsecureRecipientsAdapter.ViewHolder viewHolder, Consumer consumer) {
        this.f$0 = viewHolder;
        this.f$1 = consumer;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$new$0(this.f$1, view);
    }
}
