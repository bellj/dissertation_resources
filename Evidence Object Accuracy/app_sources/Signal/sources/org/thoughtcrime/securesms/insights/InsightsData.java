package org.thoughtcrime.securesms.insights;

/* loaded from: classes4.dex */
public final class InsightsData {
    private final boolean hasEnoughData;
    private final int percentInsecure;

    public InsightsData(boolean z, int i) {
        this.hasEnoughData = z;
        this.percentInsecure = i;
    }

    public boolean hasEnoughData() {
        return this.hasEnoughData;
    }

    public int getPercentInsecure() {
        return this.percentInsecure;
    }
}
