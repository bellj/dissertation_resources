package org.thoughtcrime.securesms.insights;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.util.Consumer;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import com.airbnb.lottie.LottieAnimationView;
import java.util.List;
import org.thoughtcrime.securesms.NewConversationActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.ArcProgressBar;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.insights.InsightsAnimatorSetFactory;
import org.thoughtcrime.securesms.insights.InsightsDashboardViewModel;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.ThemeUtil;

/* loaded from: classes4.dex */
public final class InsightsDashboardDialogFragment extends DialogFragment {
    private InsightsInsecureRecipientsAdapter adapter;
    private AnimatorSet animatorSet;
    private AvatarImageView avatarImageView;
    private TextView description;
    private TextView encryptedMessages;
    private RecyclerView insecureRecipients;
    private TextView locallyGenerated;
    private LottieAnimationView lottieAnimationView;
    private ArcProgressBar progress;
    private View progressContainer;
    private TextView securePercentage;
    private Button startAConversation;
    private TextView tagline;
    private TextView title;
    private Toolbar toolbar;
    private InsightsDashboardViewModel viewModel;

    @Override // androidx.fragment.app.Fragment, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        requireFragmentManager().beginTransaction().detach(this).attach(this).commit();
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (ThemeUtil.isDarkTheme(requireActivity())) {
            setStyle(2, R.style.TextSecure_DarkTheme);
        } else {
            setStyle(2, R.style.TextSecure_LightTheme);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.insights_dashboard, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.securePercentage = (TextView) view.findViewById(R.id.insights_dashboard_percent_secure);
        this.progress = (ArcProgressBar) view.findViewById(R.id.insights_dashboard_progress);
        this.progressContainer = view.findViewById(R.id.insights_dashboard_percent_container);
        this.encryptedMessages = (TextView) view.findViewById(R.id.insights_dashboard_encrypted_messages);
        this.tagline = (TextView) view.findViewById(R.id.insights_dashboard_tagline);
        this.title = (TextView) view.findViewById(R.id.insights_dashboard_make_signal_secure);
        this.description = (TextView) view.findViewById(R.id.insights_dashboard_invite_your_contacts);
        this.insecureRecipients = (RecyclerView) view.findViewById(R.id.insights_dashboard_recycler);
        this.locallyGenerated = (TextView) view.findViewById(R.id.insights_dashboard_this_stat_was_generated_locally);
        this.avatarImageView = (AvatarImageView) view.findViewById(R.id.insights_dashboard_avatar);
        this.startAConversation = (Button) view.findViewById(R.id.insights_dashboard_start_a_conversation);
        this.lottieAnimationView = (LottieAnimationView) view.findViewById(R.id.insights_dashboard_lottie_animation);
        this.toolbar = (Toolbar) view.findViewById(R.id.insights_dashboard_toolbar);
        setupStartAConversation();
        setDashboardDetailsAlpha(0.0f);
        setNotEnoughDataAlpha(0.0f);
        setupToolbar();
        setupRecycler();
        initializeViewModel();
    }

    public /* synthetic */ void lambda$setupStartAConversation$0(View view) {
        startActivity(new Intent(requireActivity(), NewConversationActivity.class));
    }

    private void setupStartAConversation() {
        this.startAConversation.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.insights.InsightsDashboardDialogFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                InsightsDashboardDialogFragment.$r8$lambda$1XQqam5O3LcIQDwnahHr052nFbc(InsightsDashboardDialogFragment.this, view);
            }
        });
    }

    public void setDashboardDetailsAlpha(float f) {
        this.tagline.setAlpha(f);
        this.title.setAlpha(f);
        this.description.setAlpha(f);
        this.insecureRecipients.setAlpha(f);
        this.locallyGenerated.setAlpha(f);
        this.encryptedMessages.setAlpha(f);
    }

    public /* synthetic */ void lambda$setupToolbar$1(View view) {
        dismiss();
    }

    private void setupToolbar() {
        this.toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.insights.InsightsDashboardDialogFragment$$ExternalSyntheticLambda10
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                InsightsDashboardDialogFragment.m1878$r8$lambda$ozAyU0yhzWTwan9RfLqxqjJ85w(InsightsDashboardDialogFragment.this, view);
            }
        });
    }

    private void setupRecycler() {
        InsightsInsecureRecipientsAdapter insightsInsecureRecipientsAdapter = new InsightsInsecureRecipientsAdapter(new Consumer() { // from class: org.thoughtcrime.securesms.insights.InsightsDashboardDialogFragment$$ExternalSyntheticLambda0
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                InsightsDashboardDialogFragment.$r8$lambda$YsbPquZGdAt2u3syknHbl53xOLY(InsightsDashboardDialogFragment.this, (Recipient) obj);
            }
        });
        this.adapter = insightsInsecureRecipientsAdapter;
        this.insecureRecipients.setAdapter(insightsInsecureRecipientsAdapter);
    }

    private void initializeViewModel() {
        InsightsDashboardViewModel insightsDashboardViewModel = (InsightsDashboardViewModel) ViewModelProviders.of(this, new InsightsDashboardViewModel.Factory(new InsightsRepository(requireContext()))).get(InsightsDashboardViewModel.class);
        this.viewModel = insightsDashboardViewModel;
        insightsDashboardViewModel.getState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.insights.InsightsDashboardDialogFragment$$ExternalSyntheticLambda9
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                InsightsDashboardDialogFragment.$r8$lambda$8JABX0iLQ5XKCotz6FpvdWHmUf4(InsightsDashboardDialogFragment.this, (InsightsDashboardState) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializeViewModel$2(InsightsDashboardState insightsDashboardState) {
        updateInsecurePercent(insightsDashboardState.getData());
        updateInsecureRecipients(insightsDashboardState.getInsecureRecipients());
        updateUserAvatar(insightsDashboardState.getUserAvatar());
    }

    private void updateInsecurePercent(InsightsData insightsData) {
        if (insightsData != null) {
            if (insightsData.hasEnoughData()) {
                setTitleAndDescriptionText(insightsData.getPercentInsecure());
                animateProgress(insightsData.getPercentInsecure());
                return;
            }
            setNotEnoughDataText();
            animateNotEnoughData();
        }
    }

    private void animateProgress(int i) {
        this.startAConversation.setVisibility(8);
        if (this.animatorSet == null) {
            AnimatorSet create = InsightsAnimatorSetFactory.create(i, new InsightsAnimatorSetFactory.UpdateListener() { // from class: org.thoughtcrime.securesms.insights.InsightsDashboardDialogFragment$$ExternalSyntheticLambda2
                @Override // org.thoughtcrime.securesms.insights.InsightsAnimatorSetFactory.UpdateListener
                public final void onUpdate(float f) {
                    InsightsDashboardDialogFragment.$r8$lambda$bInLY6S6SP0YKkYNlj31llHe6jo(InsightsDashboardDialogFragment.this, f);
                }
            }, new InsightsAnimatorSetFactory.UpdateListener() { // from class: org.thoughtcrime.securesms.insights.InsightsDashboardDialogFragment$$ExternalSyntheticLambda3
                @Override // org.thoughtcrime.securesms.insights.InsightsAnimatorSetFactory.UpdateListener
                public final void onUpdate(float f) {
                    InsightsDashboardDialogFragment.$r8$lambda$CN9DmaxaxcSGqJGAr3etaY9cfwE(InsightsDashboardDialogFragment.this, f);
                }
            }, new InsightsAnimatorSetFactory.UpdateListener() { // from class: org.thoughtcrime.securesms.insights.InsightsDashboardDialogFragment$$ExternalSyntheticLambda4
                @Override // org.thoughtcrime.securesms.insights.InsightsAnimatorSetFactory.UpdateListener
                public final void onUpdate(float f) {
                    InsightsDashboardDialogFragment.m1877$r8$lambda$7GWClrknBGwOrHixXG1EoHm0NA(InsightsDashboardDialogFragment.this, f);
                }
            }, i == 0 ? new InsightsAnimatorSetFactory.UpdateListener() { // from class: org.thoughtcrime.securesms.insights.InsightsDashboardDialogFragment$$ExternalSyntheticLambda5
                @Override // org.thoughtcrime.securesms.insights.InsightsAnimatorSetFactory.UpdateListener
                public final void onUpdate(float f) {
                    InsightsDashboardDialogFragment.$r8$lambda$dbguOgh8Ft0CVVtPH50q_18vf2A(InsightsDashboardDialogFragment.this, f);
                }
            } : null);
            this.animatorSet = create;
            if (i == 0) {
                create.addListener(new ToolbarBackgroundColorAnimationListener());
            }
            this.animatorSet.start();
        }
    }

    public void setProgressPercentage(float f) {
        this.securePercentage.setText(String.valueOf(Math.round(100.0f * f)));
        this.progress.setProgress(f);
    }

    public void setPercentSecureScale(float f) {
        this.progressContainer.setScaleX(f);
        this.progressContainer.setScaleY(f);
    }

    public void setLottieProgress(float f) {
        this.lottieAnimationView.setProgress(f);
    }

    private void setTitleAndDescriptionText(int i) {
        this.startAConversation.setVisibility(8);
        this.progressContainer.setVisibility(0);
        this.insecureRecipients.setVisibility(0);
        this.encryptedMessages.setText(R.string.InsightsDashboardFragment__encrypted_messages);
        this.tagline.setText(getString(R.string.InsightsDashboardFragment__signal_protocol_automatically_protected, Integer.valueOf(100 - i), 7L));
        if (i == 0) {
            this.lottieAnimationView.setVisibility(0);
            this.title.setVisibility(8);
            this.description.setVisibility(8);
            return;
        }
        this.lottieAnimationView.setVisibility(8);
        this.title.setText(R.string.InsightsDashboardFragment__spread_the_word);
        this.description.setText(R.string.InsightsDashboardFragment__invite_your_contacts);
        this.title.setVisibility(0);
        this.description.setVisibility(0);
    }

    private void setNotEnoughDataText() {
        this.startAConversation.setVisibility(0);
        this.progressContainer.setVisibility(4);
        this.insecureRecipients.setVisibility(8);
        this.encryptedMessages.setText(R.string.InsightsDashboardFragment__not_enough_data);
        this.tagline.setText(getString(R.string.InsightsDashboardFragment__your_insights_percentage_is_calculated_based_on, 7L));
    }

    private void animateNotEnoughData() {
        if (this.animatorSet == null) {
            AnimatorSet create = InsightsAnimatorSetFactory.create(0, null, new InsightsAnimatorSetFactory.UpdateListener() { // from class: org.thoughtcrime.securesms.insights.InsightsDashboardDialogFragment$$ExternalSyntheticLambda8
                @Override // org.thoughtcrime.securesms.insights.InsightsAnimatorSetFactory.UpdateListener
                public final void onUpdate(float f) {
                    InsightsDashboardDialogFragment.$r8$lambda$RF2sTqJ5I2kV6OHEEwBhpl3d2fk(InsightsDashboardDialogFragment.this, f);
                }
            }, null, null);
            this.animatorSet = create;
            create.start();
        }
    }

    public void setNotEnoughDataAlpha(float f) {
        this.encryptedMessages.setAlpha(f);
        this.tagline.setAlpha(f);
        this.startAConversation.setAlpha(f);
    }

    private void updateInsecureRecipients(List<Recipient> list) {
        this.adapter.updateData(list);
    }

    private void updateUserAvatar(InsightsUserAvatar insightsUserAvatar) {
        if (insightsUserAvatar == null) {
            this.avatarImageView.setImageDrawable(null);
        } else {
            insightsUserAvatar.load(this.avatarImageView);
        }
    }

    public void handleInviteRecipient(Recipient recipient) {
        new AlertDialog.Builder(requireContext()).setTitle(getResources().getQuantityString(R.plurals.InviteActivity_send_sms_invites, 1, 1)).setMessage(getString(R.string.InviteActivity_lets_switch_to_signal, getString(R.string.install_url))).setPositiveButton(R.string.InsightsDashboardFragment__send, new DialogInterface.OnClickListener(recipient) { // from class: org.thoughtcrime.securesms.insights.InsightsDashboardDialogFragment$$ExternalSyntheticLambda6
            public final /* synthetic */ Recipient f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                InsightsDashboardDialogFragment.$r8$lambda$m2MN6Ii0KUQrJEmXMcOi1RaRXi8(InsightsDashboardDialogFragment.this, this.f$1, dialogInterface, i);
            }
        }).setNegativeButton(R.string.InsightsDashboardFragment__cancel, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.insights.InsightsDashboardDialogFragment$$ExternalSyntheticLambda7
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                InsightsDashboardDialogFragment.$r8$lambda$ELkeYHO4h1TkprBgBj2pp2FRfjo(dialogInterface, i);
            }
        }).show();
    }

    public /* synthetic */ void lambda$handleInviteRecipient$3(Recipient recipient, DialogInterface dialogInterface, int i) {
        this.viewModel.sendSmsInvite(recipient);
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        AnimatorSet animatorSet = this.animatorSet;
        if (animatorSet != null) {
            animatorSet.cancel();
            this.animatorSet = null;
        }
        super.onDestroyView();
    }

    /* loaded from: classes4.dex */
    public final class ToolbarBackgroundColorAnimationListener implements Animator.AnimatorListener {
        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationRepeat(Animator animator) {
        }

        private ToolbarBackgroundColorAnimationListener() {
            InsightsDashboardDialogFragment.this = r1;
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            InsightsDashboardDialogFragment.this.toolbar.setBackgroundResource(R.color.transparent);
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            InsightsDashboardDialogFragment.this.toolbar.setBackgroundColor(ThemeUtil.getThemedColor(InsightsDashboardDialogFragment.this.requireContext(), 16842836));
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
            InsightsDashboardDialogFragment.this.toolbar.setBackgroundColor(ThemeUtil.getThemedColor(InsightsDashboardDialogFragment.this.requireContext(), 16842836));
        }
    }
}
