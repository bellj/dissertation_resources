package org.thoughtcrime.securesms;

import android.app.Activity;
import android.os.Bundle;

/* loaded from: classes.dex */
public class DummyActivity extends Activity {
    @Override // android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        finish();
    }
}
