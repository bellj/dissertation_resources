package org.thoughtcrime.securesms.search;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: MessageResult.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0018\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001BE\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t\u0012\u0006\u0010\u000b\u001a\u00020\t\u0012\u0006\u0010\f\u001a\u00020\r¢\u0006\u0002\u0010\u000eJ\t\u0010\u001a\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001b\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001c\u001a\u00020\u0006HÆ\u0003J\t\u0010\u001d\u001a\u00020\u0006HÆ\u0003J\t\u0010\u001e\u001a\u00020\tHÆ\u0003J\t\u0010\u001f\u001a\u00020\tHÆ\u0003J\t\u0010 \u001a\u00020\tHÆ\u0003J\t\u0010!\u001a\u00020\rHÆ\u0003JY\u0010\"\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\t2\b\b\u0002\u0010\u000b\u001a\u00020\t2\b\b\u0002\u0010\f\u001a\u00020\rHÆ\u0001J\u0013\u0010#\u001a\u00020\r2\b\u0010$\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010%\u001a\u00020&HÖ\u0001J\t\u0010'\u001a\u00020\u0006HÖ\u0001R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\f\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0014R\u0011\u0010\n\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0013R\u0011\u0010\u000b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0016R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0016¨\u0006("}, d2 = {"Lorg/thoughtcrime/securesms/search/MessageResult;", "", "conversationRecipient", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "messageRecipient", "body", "", "bodySnippet", "threadId", "", "messageId", "receivedTimestampMs", "isMms", "", "(Lorg/thoughtcrime/securesms/recipients/Recipient;Lorg/thoughtcrime/securesms/recipients/Recipient;Ljava/lang/String;Ljava/lang/String;JJJZ)V", "getBody", "()Ljava/lang/String;", "getBodySnippet", "getConversationRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "()Z", "getMessageId", "()J", "getMessageRecipient", "getReceivedTimestampMs", "getThreadId", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "copy", "equals", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MessageResult {
    private final String body;
    private final String bodySnippet;
    private final Recipient conversationRecipient;
    private final boolean isMms;
    private final long messageId;
    private final Recipient messageRecipient;
    private final long receivedTimestampMs;
    private final long threadId;

    public final Recipient component1() {
        return this.conversationRecipient;
    }

    public final Recipient component2() {
        return this.messageRecipient;
    }

    public final String component3() {
        return this.body;
    }

    public final String component4() {
        return this.bodySnippet;
    }

    public final long component5() {
        return this.threadId;
    }

    public final long component6() {
        return this.messageId;
    }

    public final long component7() {
        return this.receivedTimestampMs;
    }

    public final boolean component8() {
        return this.isMms;
    }

    public final MessageResult copy(Recipient recipient, Recipient recipient2, String str, String str2, long j, long j2, long j3, boolean z) {
        Intrinsics.checkNotNullParameter(recipient, "conversationRecipient");
        Intrinsics.checkNotNullParameter(recipient2, "messageRecipient");
        Intrinsics.checkNotNullParameter(str, "body");
        Intrinsics.checkNotNullParameter(str2, "bodySnippet");
        return new MessageResult(recipient, recipient2, str, str2, j, j2, j3, z);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MessageResult)) {
            return false;
        }
        MessageResult messageResult = (MessageResult) obj;
        return Intrinsics.areEqual(this.conversationRecipient, messageResult.conversationRecipient) && Intrinsics.areEqual(this.messageRecipient, messageResult.messageRecipient) && Intrinsics.areEqual(this.body, messageResult.body) && Intrinsics.areEqual(this.bodySnippet, messageResult.bodySnippet) && this.threadId == messageResult.threadId && this.messageId == messageResult.messageId && this.receivedTimestampMs == messageResult.receivedTimestampMs && this.isMms == messageResult.isMms;
    }

    public int hashCode() {
        int hashCode = ((((((((((((this.conversationRecipient.hashCode() * 31) + this.messageRecipient.hashCode()) * 31) + this.body.hashCode()) * 31) + this.bodySnippet.hashCode()) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.threadId)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.messageId)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.receivedTimestampMs)) * 31;
        boolean z = this.isMms;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return hashCode + i;
    }

    public String toString() {
        return "MessageResult(conversationRecipient=" + this.conversationRecipient + ", messageRecipient=" + this.messageRecipient + ", body=" + this.body + ", bodySnippet=" + this.bodySnippet + ", threadId=" + this.threadId + ", messageId=" + this.messageId + ", receivedTimestampMs=" + this.receivedTimestampMs + ", isMms=" + this.isMms + ')';
    }

    public MessageResult(Recipient recipient, Recipient recipient2, String str, String str2, long j, long j2, long j3, boolean z) {
        Intrinsics.checkNotNullParameter(recipient, "conversationRecipient");
        Intrinsics.checkNotNullParameter(recipient2, "messageRecipient");
        Intrinsics.checkNotNullParameter(str, "body");
        Intrinsics.checkNotNullParameter(str2, "bodySnippet");
        this.conversationRecipient = recipient;
        this.messageRecipient = recipient2;
        this.body = str;
        this.bodySnippet = str2;
        this.threadId = j;
        this.messageId = j2;
        this.receivedTimestampMs = j3;
        this.isMms = z;
    }

    public final Recipient getConversationRecipient() {
        return this.conversationRecipient;
    }

    public final Recipient getMessageRecipient() {
        return this.messageRecipient;
    }

    public final String getBody() {
        return this.body;
    }

    public final String getBodySnippet() {
        return this.bodySnippet;
    }

    public final long getThreadId() {
        return this.threadId;
    }

    public final long getMessageId() {
        return this.messageId;
    }

    public final long getReceivedTimestampMs() {
        return this.receivedTimestampMs;
    }

    public final boolean isMms() {
        return this.isMms;
    }
}
