package org.thoughtcrime.securesms.search;

import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: MessageSearchResult.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u001b\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u000f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\t\u0010\r\u001a\u00020\u0006HÆ\u0003J#\u0010\u000e\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0006HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0006HÖ\u0001R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/search/MessageSearchResult;", "", "results", "", "Lorg/thoughtcrime/securesms/search/MessageResult;", "query", "", "(Ljava/util/List;Ljava/lang/String;)V", "getQuery", "()Ljava/lang/String;", "getResults", "()Ljava/util/List;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MessageSearchResult {
    private final String query;
    private final List<MessageResult> results;

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.search.MessageSearchResult */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ MessageSearchResult copy$default(MessageSearchResult messageSearchResult, List list, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            list = messageSearchResult.results;
        }
        if ((i & 2) != 0) {
            str = messageSearchResult.query;
        }
        return messageSearchResult.copy(list, str);
    }

    public final List<MessageResult> component1() {
        return this.results;
    }

    public final String component2() {
        return this.query;
    }

    public final MessageSearchResult copy(List<MessageResult> list, String str) {
        Intrinsics.checkNotNullParameter(list, "results");
        Intrinsics.checkNotNullParameter(str, "query");
        return new MessageSearchResult(list, str);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MessageSearchResult)) {
            return false;
        }
        MessageSearchResult messageSearchResult = (MessageSearchResult) obj;
        return Intrinsics.areEqual(this.results, messageSearchResult.results) && Intrinsics.areEqual(this.query, messageSearchResult.query);
    }

    public int hashCode() {
        return (this.results.hashCode() * 31) + this.query.hashCode();
    }

    public String toString() {
        return "MessageSearchResult(results=" + this.results + ", query=" + this.query + ')';
    }

    public MessageSearchResult(List<MessageResult> list, String str) {
        Intrinsics.checkNotNullParameter(list, "results");
        Intrinsics.checkNotNullParameter(str, "query");
        this.results = list;
        this.query = str;
    }

    public final String getQuery() {
        return this.query;
    }

    public final List<MessageResult> getResults() {
        return this.results;
    }
}
