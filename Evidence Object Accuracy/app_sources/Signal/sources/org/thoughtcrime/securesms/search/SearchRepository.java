package org.thoughtcrime.securesms.search;

import android.content.Context;
import android.database.Cursor;
import android.database.MergeCursor;
import android.text.TextUtils;
import androidx.recyclerview.widget.ItemTouchHelper;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import j$.util.function.Consumer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.concurrent.LatestPrioritizedSerialExecutor;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.MentionDatabase;
import org.thoughtcrime.securesms.database.MentionUtil;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.MmsSmsColumns;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SearchDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.ThreadRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.search.SearchRepository;
import org.thoughtcrime.securesms.util.FtsUtil;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.concurrent.SerialExecutor;

/* loaded from: classes4.dex */
public class SearchRepository {
    private static final String TAG = Log.tag(SearchRepository.class);
    private final ContactRepository contactRepository;
    private final Context context;
    private final MentionDatabase mentionDatabase = SignalDatabase.mentions();
    private final MessageDatabase mmsDatabase = SignalDatabase.mms();
    private final String noteToSelfTitle;
    private final RecipientDatabase recipientDatabase = SignalDatabase.recipients();
    private final SearchDatabase searchDatabase = SignalDatabase.messageSearch();
    private final LatestPrioritizedSerialExecutor searchExecutor;
    private final Executor serialExecutor;
    private final ThreadDatabase threadDatabase = SignalDatabase.threads();

    /* loaded from: classes4.dex */
    public interface Callback<E> {
        void onResult(E e);
    }

    /* loaded from: classes4.dex */
    public interface ModelBuilder<T> {
        T build(Cursor cursor);
    }

    public SearchRepository(String str) {
        Context applicationContext = ApplicationDependencies.getApplication().getApplicationContext();
        this.context = applicationContext;
        this.noteToSelfTitle = str;
        this.contactRepository = new ContactRepository(applicationContext, str);
        ExecutorService executorService = SignalExecutors.BOUNDED;
        this.searchExecutor = new LatestPrioritizedSerialExecutor(executorService);
        this.serialExecutor = new SerialExecutor(executorService);
    }

    public void queryThreads(String str, Consumer<ThreadSearchResult> consumer) {
        this.searchExecutor.execute(2, new Runnable(str, consumer) { // from class: org.thoughtcrime.securesms.search.SearchRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ String f$1;
            public final /* synthetic */ Consumer f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SearchRepository.this.lambda$queryThreads$0(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$queryThreads$0(String str, Consumer consumer) {
        long currentTimeMillis = System.currentTimeMillis();
        List<ThreadRecord> queryConversations = queryConversations(str);
        String str2 = TAG;
        Log.d(str2, "[threads] Search took " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
        consumer.accept(new ThreadSearchResult(queryConversations, str));
    }

    public void queryContacts(String str, Consumer<ContactSearchResult> consumer) {
        this.searchExecutor.execute(1, new Runnable(str, consumer) { // from class: org.thoughtcrime.securesms.search.SearchRepository$$ExternalSyntheticLambda5
            public final /* synthetic */ String f$1;
            public final /* synthetic */ Consumer f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SearchRepository.this.lambda$queryContacts$1(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$queryContacts$1(String str, Consumer consumer) {
        long currentTimeMillis = System.currentTimeMillis();
        List<Recipient> queryContacts = queryContacts(str);
        String str2 = TAG;
        Log.d(str2, "[contacts] Search took " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
        consumer.accept(new ContactSearchResult(queryContacts, str));
    }

    public void queryMessages(String str, Consumer<MessageSearchResult> consumer) {
        this.searchExecutor.execute(0, new Runnable(str, consumer) { // from class: org.thoughtcrime.securesms.search.SearchRepository$$ExternalSyntheticLambda4
            public final /* synthetic */ String f$1;
            public final /* synthetic */ Consumer f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SearchRepository.this.lambda$queryMessages$2(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$queryMessages$2(String str, Consumer consumer) {
        long currentTimeMillis = System.currentTimeMillis();
        List<MessageResult> mergeMessagesAndMentions = mergeMessagesAndMentions(queryMessages(FtsUtil.sanitize(str)), queryMentions(sanitizeQueryAsTokens(str)));
        String str2 = TAG;
        Log.d(str2, "[messages] Search took " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
        consumer.accept(new MessageSearchResult(mergeMessagesAndMentions, str));
    }

    public void query(String str, long j, Callback<List<MessageResult>> callback) {
        if (TextUtils.isEmpty(str)) {
            callback.onResult(Collections.emptyList());
        } else {
            this.serialExecutor.execute(new Runnable(str, j, callback) { // from class: org.thoughtcrime.securesms.search.SearchRepository$$ExternalSyntheticLambda1
                public final /* synthetic */ String f$1;
                public final /* synthetic */ long f$2;
                public final /* synthetic */ SearchRepository.Callback f$3;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r5;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    SearchRepository.this.lambda$query$3(this.f$1, this.f$2, this.f$3);
                }
            });
        }
    }

    public /* synthetic */ void lambda$query$3(String str, long j, Callback callback) {
        long currentTimeMillis = System.currentTimeMillis();
        List<MessageResult> queryMessages = queryMessages(FtsUtil.sanitize(str), j);
        List<MessageResult> queryMentions = queryMentions(sanitizeQueryAsTokens(str), j);
        String str2 = TAG;
        Log.d(str2, "[ConversationQuery] " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
        callback.onResult(mergeMessagesAndMentions(queryMessages, queryMentions));
    }

    private List<Recipient> queryContacts(String str) {
        Throwable th;
        if (Util.isEmpty(str)) {
            return Collections.emptyList();
        }
        MergeCursor mergeCursor = null;
        try {
            MergeCursor mergeCursor2 = new MergeCursor(new Cursor[]{this.contactRepository.querySignalContacts(str), this.contactRepository.queryNonSignalContacts(str)});
            try {
                List<Recipient> readToList = readToList(mergeCursor2, new RecipientModelBuilder(), ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
                mergeCursor2.close();
                return readToList;
            } catch (Throwable th2) {
                th = th2;
                mergeCursor = mergeCursor2;
                if (mergeCursor != null) {
                    mergeCursor.close();
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
        }
    }

    private List<ThreadRecord> queryConversations(String str) {
        if (Util.isEmpty(str)) {
            return Collections.emptyList();
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        Cursor queryAllContacts = SignalDatabase.recipients().queryAllContacts(str);
        while (queryAllContacts != null) {
            try {
                if (!queryAllContacts.moveToNext()) {
                    break;
                }
                linkedHashSet.add(RecipientId.from(CursorUtil.requireString(queryAllContacts, "_id")));
            } catch (Throwable th) {
                try {
                    queryAllContacts.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (queryAllContacts != null) {
            queryAllContacts.close();
        }
        GroupDatabase.Reader queryGroupsByTitle = SignalDatabase.groups().queryGroupsByTitle(str, true, false, false);
        while (true) {
            try {
                GroupDatabase.GroupRecord next = queryGroupsByTitle.getNext();
                if (next == null) {
                    break;
                }
                linkedHashSet.add(next.getRecipientId());
            } catch (Throwable th3) {
                if (queryGroupsByTitle != null) {
                    try {
                        queryGroupsByTitle.close();
                    } catch (Throwable th4) {
                        th3.addSuppressed(th4);
                    }
                }
                throw th3;
            }
        }
        queryGroupsByTitle.close();
        if (this.noteToSelfTitle.toLowerCase().contains(str.toLowerCase())) {
            linkedHashSet.add(Recipient.self().getId());
        }
        Cursor filteredConversationList = this.threadDatabase.getFilteredConversationList(new ArrayList(linkedHashSet));
        try {
            List<ThreadRecord> readToList = readToList(filteredConversationList, new ThreadModelBuilder(this.threadDatabase));
            if (filteredConversationList != null) {
                filteredConversationList.close();
            }
            return readToList;
        } catch (Throwable th5) {
            if (filteredConversationList != null) {
                try {
                    filteredConversationList.close();
                } catch (Throwable th6) {
                    th5.addSuppressed(th6);
                }
            }
            throw th5;
        }
    }

    private List<MessageResult> queryMessages(String str) {
        if (Util.isEmpty(str)) {
            return Collections.emptyList();
        }
        Cursor queryMessages = this.searchDatabase.queryMessages(str);
        try {
            List<MessageResult> readToList = readToList(queryMessages, new MessageModelBuilder());
            if (queryMessages != null) {
                queryMessages.close();
            }
            LinkedList linkedList = new LinkedList();
            for (MessageResult messageResult : readToList) {
                if (messageResult.isMms()) {
                    linkedList.add(Long.valueOf(messageResult.getMessageId()));
                }
            }
            if (linkedList.isEmpty()) {
                return readToList;
            }
            Map<Long, List<Mention>> mentionsForMessages = SignalDatabase.mentions().getMentionsForMessages(linkedList);
            if (mentionsForMessages.isEmpty()) {
                return readToList;
            }
            ArrayList arrayList = new ArrayList(readToList.size());
            for (MessageResult messageResult2 : readToList) {
                if (!messageResult2.isMms() || !mentionsForMessages.containsKey(Long.valueOf(messageResult2.getMessageId()))) {
                    arrayList.add(messageResult2);
                } else {
                    List<Mention> list = mentionsForMessages.get(Long.valueOf(messageResult2.getMessageId()));
                    arrayList.add(new MessageResult(messageResult2.getConversationRecipient(), messageResult2.getMessageRecipient(), MentionUtil.updateBodyAndMentionsWithDisplayNames(this.context, messageResult2.getBody(), list).getBody().toString(), updateSnippetWithDisplayNames(messageResult2.getBody(), messageResult2.getBodySnippet(), list), messageResult2.getThreadId(), messageResult2.getMessageId(), messageResult2.getReceivedTimestampMs(), messageResult2.isMms()));
                }
            }
            return arrayList;
        } catch (Throwable th) {
            if (queryMessages != null) {
                try {
                    queryMessages.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    private String updateSnippetWithDisplayNames(String str, String str2, List<Mention> list) {
        int i;
        String str3;
        if (str2.startsWith(SearchDatabase.SNIPPET_WRAP)) {
            str3 = str2.substring(3);
            i = 3;
        } else {
            str3 = str2;
            i = 0;
        }
        if (str3.endsWith(SearchDatabase.SNIPPET_WRAP)) {
            str3 = str3.substring(0, str3.length() - 3);
        }
        int indexOf = str.indexOf(str3);
        if (indexOf == -1) {
            return str2;
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (Mention mention : list) {
            int start = (mention.getStart() - indexOf) + i;
            if (start >= 0 && mention.getLength() + start <= str3.length()) {
                arrayList.add(new Mention(mention.getRecipientId(), start, mention.getLength()));
            }
        }
        return MentionUtil.updateBodyAndMentionsWithDisplayNames(this.context, str2, arrayList).getBody().toString();
    }

    private List<MessageResult> queryMessages(String str, long j) {
        Cursor queryMessages = this.searchDatabase.queryMessages(str, j);
        try {
            List<MessageResult> readToList = readToList(queryMessages, new MessageModelBuilder());
            if (queryMessages != null) {
                queryMessages.close();
            }
            return readToList;
        } catch (Throwable th) {
            if (queryMessages != null) {
                try {
                    queryMessages.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    private List<MessageResult> queryMentions(List<String> list) {
        Map<Long, List<Mention>> map;
        HashSet hashSet = new HashSet();
        for (String str : list) {
            for (Recipient recipient : this.recipientDatabase.queryRecipientsForMentions(str)) {
                hashSet.add(recipient.getId());
            }
        }
        Map<Long, List<Mention>> mentionsContainingRecipients = this.mentionDatabase.getMentionsContainingRecipients(hashSet, 500);
        if (mentionsContainingRecipients.isEmpty()) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        MessageDatabase.Reader messages = this.mmsDatabase.getMessages(mentionsContainingRecipients.keySet());
        while (true) {
            try {
                MessageRecord next = messages.getNext();
                if (next != null) {
                    List<Mention> list2 = mentionsContainingRecipients.get(Long.valueOf(next.getId()));
                    if (Util.hasItems(list2)) {
                        MentionUtil.UpdatedBodyAndMentions updateBodyAndMentionsWithDisplayNames = MentionUtil.updateBodyAndMentionsWithDisplayNames(this.context, next.getBody(), list2);
                        String charSequence = updateBodyAndMentionsWithDisplayNames.getBody() != null ? updateBodyAndMentionsWithDisplayNames.getBody().toString() : next.getBody();
                        map = mentionsContainingRecipients;
                        arrayList.add(new MessageResult(this.threadDatabase.getRecipientForThreadId(next.getThreadId()), next.getRecipient(), charSequence, makeSnippet(list, charSequence), next.getThreadId(), next.getId(), next.getDateReceived(), true));
                    } else {
                        map = mentionsContainingRecipients;
                    }
                    mentionsContainingRecipients = map;
                } else {
                    messages.close();
                    return arrayList;
                }
            } catch (Throwable th) {
                if (messages != null) {
                    try {
                        messages.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
    }

    private List<MessageResult> queryMentions(List<String> list, long j) {
        SearchRepository searchRepository = this;
        HashSet hashSet = new HashSet();
        for (String str : list) {
            for (Recipient recipient : searchRepository.recipientDatabase.queryRecipientsForMentions(str)) {
                hashSet.add(recipient.getId());
            }
        }
        Map<Long, List<Mention>> mentionsContainingRecipients = searchRepository.mentionDatabase.getMentionsContainingRecipients(hashSet, j, 500);
        if (mentionsContainingRecipients.isEmpty()) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        MessageDatabase.Reader messages = searchRepository.mmsDatabase.getMessages(mentionsContainingRecipients.keySet());
        while (true) {
            try {
                MessageRecord next = messages.getNext();
                if (next != null) {
                    arrayList.add(new MessageResult(searchRepository.threadDatabase.getRecipientForThreadId(next.getThreadId()), next.getRecipient(), next.getBody(), next.getBody(), next.getThreadId(), next.getId(), next.getDateReceived(), true));
                    searchRepository = this;
                } else {
                    messages.close();
                    return arrayList;
                }
            } catch (Throwable th) {
                if (messages != null) {
                    try {
                        messages.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
    }

    private String makeSnippet(List<String> list, String str) {
        if (str.length() < 50) {
            return str;
        }
        String lowerCase = str.toLowerCase();
        for (String str2 : list) {
            int indexOf = lowerCase.indexOf(str2.toLowerCase());
            if (indexOf != -1) {
                int max = Math.max(0, Math.max(str.lastIndexOf(32, indexOf - 5) + 1, indexOf - 15));
                int indexOf2 = str.indexOf(32, indexOf + 30);
                int min = Math.min(str.length(), indexOf2 > 0 ? Math.min(indexOf2, indexOf + 40) : indexOf + 40);
                StringBuilder sb = new StringBuilder();
                String str3 = SearchDatabase.SNIPPET_WRAP;
                sb.append(max > 0 ? str3 : "");
                sb.append(str.substring(max, min));
                if (min >= str.length()) {
                    str3 = "";
                }
                sb.append(str3);
                return sb.toString();
            }
        }
        return str;
    }

    private <T> List<T> readToList(Cursor cursor, ModelBuilder<T> modelBuilder) {
        return readToList(cursor, modelBuilder, -1);
    }

    private <T> List<T> readToList(Cursor cursor, ModelBuilder<T> modelBuilder, int i) {
        if (cursor == null) {
            return Collections.emptyList();
        }
        int i2 = 0;
        ArrayList arrayList = new ArrayList(cursor.getCount());
        while (cursor.moveToNext() && (i < 0 || i2 < i)) {
            arrayList.add(modelBuilder.build(cursor));
            i2++;
        }
        return arrayList;
    }

    private List<String> sanitizeQueryAsTokens(String str) {
        String[] split = str.split("\\s+");
        if (split.length > 3) {
            return Collections.emptyList();
        }
        return Stream.of(split).map(new Function() { // from class: org.thoughtcrime.securesms.search.SearchRepository$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return FtsUtil.sanitize((String) obj);
            }
        }).toList();
    }

    private static List<MessageResult> mergeMessagesAndMentions(List<MessageResult> list, List<MessageResult> list2) {
        HashSet hashSet = new HashSet();
        ArrayList arrayList = new ArrayList(list.size() + list2.size());
        for (MessageResult messageResult : list) {
            arrayList.add(messageResult);
            if (messageResult.isMms()) {
                hashSet.add(Long.valueOf(messageResult.getMessageId()));
            }
        }
        for (MessageResult messageResult2 : list2) {
            if (!hashSet.contains(Long.valueOf(messageResult2.getMessageId()))) {
                arrayList.add(messageResult2);
            }
        }
        Collections.sort(arrayList, Collections.reverseOrder(new Comparator() { // from class: org.thoughtcrime.securesms.search.SearchRepository$$ExternalSyntheticLambda3
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return SearchRepository.lambda$mergeMessagesAndMentions$4((MessageResult) obj, (MessageResult) obj2);
            }
        }));
        return arrayList;
    }

    public static /* synthetic */ int lambda$mergeMessagesAndMentions$4(MessageResult messageResult, MessageResult messageResult2) {
        return Long.compare(messageResult.getReceivedTimestampMs(), messageResult2.getReceivedTimestampMs());
    }

    /* loaded from: classes4.dex */
    public static class RecipientModelBuilder implements ModelBuilder<Recipient> {
        private RecipientModelBuilder() {
        }

        @Override // org.thoughtcrime.securesms.search.SearchRepository.ModelBuilder
        public Recipient build(Cursor cursor) {
            return Recipient.resolved(RecipientId.from(cursor.getLong(cursor.getColumnIndexOrThrow(ContactRepository.ID_COLUMN))));
        }
    }

    /* loaded from: classes4.dex */
    public static class ThreadModelBuilder implements ModelBuilder<ThreadRecord> {
        private final ThreadDatabase threadDatabase;

        ThreadModelBuilder(ThreadDatabase threadDatabase) {
            this.threadDatabase = threadDatabase;
        }

        @Override // org.thoughtcrime.securesms.search.SearchRepository.ModelBuilder
        public ThreadRecord build(Cursor cursor) {
            return this.threadDatabase.readerFor(cursor).getCurrent();
        }
    }

    /* loaded from: classes4.dex */
    public static class MessageModelBuilder implements ModelBuilder<MessageResult> {
        private MessageModelBuilder() {
        }

        @Override // org.thoughtcrime.securesms.search.SearchRepository.ModelBuilder
        public MessageResult build(Cursor cursor) {
            RecipientId from = RecipientId.from(CursorUtil.requireLong(cursor, SearchDatabase.CONVERSATION_RECIPIENT));
            RecipientId from2 = RecipientId.from(CursorUtil.requireLong(cursor, SearchDatabase.MESSAGE_RECIPIENT));
            Recipient recipient = Recipient.live(from).get();
            Recipient recipient2 = Recipient.live(from2).get();
            String requireString = CursorUtil.requireString(cursor, "body");
            String requireString2 = CursorUtil.requireString(cursor, "snippet");
            long requireLong = CursorUtil.requireLong(cursor, MmsSmsColumns.NORMALIZED_DATE_RECEIVED);
            return new MessageResult(recipient, recipient2, requireString, requireString2, CursorUtil.requireLong(cursor, "thread_id"), (long) CursorUtil.requireInt(cursor, "message_id"), requireLong, CursorUtil.requireInt(cursor, "is_mms") == 1);
        }
    }
}
