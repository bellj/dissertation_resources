package org.thoughtcrime.securesms.search;

import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.contactshare.ContactShareEditActivity;
import org.thoughtcrime.securesms.database.model.ThreadRecord;
import org.thoughtcrime.securesms.mediasend.MediaSendActivityResult;
import org.thoughtcrime.securesms.notifications.v2.MessageNotifierV2;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: SearchResult.kt */
@Metadata(d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\f\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\b\u0018\u0000 %2\u00020\u0001:\u0001%B7\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0005\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0005¢\u0006\u0002\u0010\u000bJ\t\u0010\u0015\u001a\u00020\u0003HÆ\u0003J\u000f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J\u000f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\b0\u0005HÆ\u0003J\u000f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\n0\u0005HÆ\u0003JC\u0010\u0019\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u00052\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0005HÆ\u0001J\u0013\u0010\u001a\u001a\u00020\u00102\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001c\u001a\u00020\u001dHÖ\u0001J\u000e\u0010\u001e\u001a\u00020\u00002\u0006\u0010\u001f\u001a\u00020 J\u000e\u0010\u001e\u001a\u00020\u00002\u0006\u0010\u001f\u001a\u00020!J\u000e\u0010\u001e\u001a\u00020\u00002\u0006\u0010\u001f\u001a\u00020\"J\u0006\u0010#\u001a\u00020\u001dJ\t\u0010$\u001a\u00020\u0003HÖ\u0001R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u000f\u001a\u00020\u00108F¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0011R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014¨\u0006&"}, d2 = {"Lorg/thoughtcrime/securesms/search/SearchResult;", "", "query", "", ContactShareEditActivity.KEY_CONTACTS, "", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "conversations", "Lorg/thoughtcrime/securesms/database/model/ThreadRecord;", MessageNotifierV2.NOTIFICATION_GROUP, "Lorg/thoughtcrime/securesms/search/MessageResult;", "(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V", "getContacts", "()Ljava/util/List;", "getConversations", "isEmpty", "", "()Z", "getMessages", "getQuery", "()Ljava/lang/String;", "component1", "component2", "component3", "component4", "copy", "equals", "other", "hashCode", "", "merge", MediaSendActivityResult.EXTRA_RESULT, "Lorg/thoughtcrime/securesms/search/ContactSearchResult;", "Lorg/thoughtcrime/securesms/search/MessageSearchResult;", "Lorg/thoughtcrime/securesms/search/ThreadSearchResult;", MediaPreviewActivity.SIZE_EXTRA, "toString", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SearchResult {
    public static final Companion Companion = new Companion(null);
    public static final SearchResult EMPTY = new SearchResult("", CollectionsKt__CollectionsKt.emptyList(), CollectionsKt__CollectionsKt.emptyList(), CollectionsKt__CollectionsKt.emptyList());
    private final List<Recipient> contacts;
    private final List<ThreadRecord> conversations;
    private final List<MessageResult> messages;
    private final String query;

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.search.SearchResult */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ SearchResult copy$default(SearchResult searchResult, String str, List list, List list2, List list3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = searchResult.query;
        }
        if ((i & 2) != 0) {
            list = searchResult.contacts;
        }
        if ((i & 4) != 0) {
            list2 = searchResult.conversations;
        }
        if ((i & 8) != 0) {
            list3 = searchResult.messages;
        }
        return searchResult.copy(str, list, list2, list3);
    }

    public final String component1() {
        return this.query;
    }

    public final List<Recipient> component2() {
        return this.contacts;
    }

    public final List<ThreadRecord> component3() {
        return this.conversations;
    }

    public final List<MessageResult> component4() {
        return this.messages;
    }

    public final SearchResult copy(String str, List<? extends Recipient> list, List<ThreadRecord> list2, List<MessageResult> list3) {
        Intrinsics.checkNotNullParameter(str, "query");
        Intrinsics.checkNotNullParameter(list, ContactShareEditActivity.KEY_CONTACTS);
        Intrinsics.checkNotNullParameter(list2, "conversations");
        Intrinsics.checkNotNullParameter(list3, MessageNotifierV2.NOTIFICATION_GROUP);
        return new SearchResult(str, list, list2, list3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SearchResult)) {
            return false;
        }
        SearchResult searchResult = (SearchResult) obj;
        return Intrinsics.areEqual(this.query, searchResult.query) && Intrinsics.areEqual(this.contacts, searchResult.contacts) && Intrinsics.areEqual(this.conversations, searchResult.conversations) && Intrinsics.areEqual(this.messages, searchResult.messages);
    }

    public int hashCode() {
        return (((((this.query.hashCode() * 31) + this.contacts.hashCode()) * 31) + this.conversations.hashCode()) * 31) + this.messages.hashCode();
    }

    public String toString() {
        return "SearchResult(query=" + this.query + ", contacts=" + this.contacts + ", conversations=" + this.conversations + ", messages=" + this.messages + ')';
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.Recipient> */
    /* JADX WARN: Multi-variable type inference failed */
    public SearchResult(String str, List<? extends Recipient> list, List<ThreadRecord> list2, List<MessageResult> list3) {
        Intrinsics.checkNotNullParameter(str, "query");
        Intrinsics.checkNotNullParameter(list, ContactShareEditActivity.KEY_CONTACTS);
        Intrinsics.checkNotNullParameter(list2, "conversations");
        Intrinsics.checkNotNullParameter(list3, MessageNotifierV2.NOTIFICATION_GROUP);
        this.query = str;
        this.contacts = list;
        this.conversations = list2;
        this.messages = list3;
    }

    public final String getQuery() {
        return this.query;
    }

    public final List<Recipient> getContacts() {
        return this.contacts;
    }

    public final List<ThreadRecord> getConversations() {
        return this.conversations;
    }

    public final List<MessageResult> getMessages() {
        return this.messages;
    }

    public final int size() {
        return this.contacts.size() + this.conversations.size() + this.messages.size();
    }

    public final boolean isEmpty() {
        return size() == 0;
    }

    public final SearchResult merge(ContactSearchResult contactSearchResult) {
        Intrinsics.checkNotNullParameter(contactSearchResult, MediaSendActivityResult.EXTRA_RESULT);
        return copy$default(this, contactSearchResult.getQuery(), contactSearchResult.getResults(), null, null, 12, null);
    }

    public final SearchResult merge(ThreadSearchResult threadSearchResult) {
        Intrinsics.checkNotNullParameter(threadSearchResult, MediaSendActivityResult.EXTRA_RESULT);
        return copy$default(this, threadSearchResult.getQuery(), null, threadSearchResult.getResults(), null, 10, null);
    }

    public final SearchResult merge(MessageSearchResult messageSearchResult) {
        Intrinsics.checkNotNullParameter(messageSearchResult, MediaSendActivityResult.EXTRA_RESULT);
        return copy$default(this, messageSearchResult.getQuery(), null, null, messageSearchResult.getResults(), 6, null);
    }

    /* compiled from: SearchResult.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/search/SearchResult$Companion;", "", "()V", "EMPTY", "Lorg/thoughtcrime/securesms/search/SearchResult;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
