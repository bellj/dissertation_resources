package org.thoughtcrime.securesms.search;

import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: ContactSearchResult.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u001b\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u000f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\t\u0010\r\u001a\u00020\u0006HÆ\u0003J#\u0010\u000e\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0006HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0006HÖ\u0001R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/search/ContactSearchResult;", "", "results", "", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "query", "", "(Ljava/util/List;Ljava/lang/String;)V", "getQuery", "()Ljava/lang/String;", "getResults", "()Ljava/util/List;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ContactSearchResult {
    private final String query;
    private final List<Recipient> results;

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.search.ContactSearchResult */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ ContactSearchResult copy$default(ContactSearchResult contactSearchResult, List list, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            list = contactSearchResult.results;
        }
        if ((i & 2) != 0) {
            str = contactSearchResult.query;
        }
        return contactSearchResult.copy(list, str);
    }

    public final List<Recipient> component1() {
        return this.results;
    }

    public final String component2() {
        return this.query;
    }

    public final ContactSearchResult copy(List<? extends Recipient> list, String str) {
        Intrinsics.checkNotNullParameter(list, "results");
        Intrinsics.checkNotNullParameter(str, "query");
        return new ContactSearchResult(list, str);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ContactSearchResult)) {
            return false;
        }
        ContactSearchResult contactSearchResult = (ContactSearchResult) obj;
        return Intrinsics.areEqual(this.results, contactSearchResult.results) && Intrinsics.areEqual(this.query, contactSearchResult.query);
    }

    public int hashCode() {
        return (this.results.hashCode() * 31) + this.query.hashCode();
    }

    public String toString() {
        return "ContactSearchResult(results=" + this.results + ", query=" + this.query + ')';
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.Recipient> */
    /* JADX WARN: Multi-variable type inference failed */
    public ContactSearchResult(List<? extends Recipient> list, String str) {
        Intrinsics.checkNotNullParameter(list, "results");
        Intrinsics.checkNotNullParameter(str, "query");
        this.results = list;
        this.query = str;
    }

    public final String getQuery() {
        return this.query;
    }

    public final List<Recipient> getResults() {
        return this.results;
    }
}
