package org.thoughtcrime.securesms;

import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.MuteDialog;

/* loaded from: classes.dex */
public class MuteDialog extends AlertDialog {

    /* loaded from: classes.dex */
    public interface MuteSelectionListener {
        void onMuted(long j);
    }

    protected MuteDialog(Context context) {
        super(context);
    }

    protected MuteDialog(Context context, boolean z, DialogInterface.OnCancelListener onCancelListener) {
        super(context, z, onCancelListener);
    }

    protected MuteDialog(Context context, int i) {
        super(context, i);
    }

    public static void show(Context context, MuteSelectionListener muteSelectionListener) {
        show(context, muteSelectionListener, null);
    }

    public static void show(Context context, MuteSelectionListener muteSelectionListener, Runnable runnable) {
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(context);
        materialAlertDialogBuilder.setTitle(R.string.MuteDialog_mute_notifications);
        materialAlertDialogBuilder.setItems(R.array.mute_durations, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.MuteDialog$$ExternalSyntheticLambda0
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                MuteDialog.m255$r8$lambda$oQpY5zT3ZL0i2fNpvzIopgs82o(MuteDialog.MuteSelectionListener.this, dialogInterface, i);
            }
        });
        if (runnable != null) {
            materialAlertDialogBuilder.setOnCancelListener((DialogInterface.OnCancelListener) new DialogInterface.OnCancelListener(runnable) { // from class: org.thoughtcrime.securesms.MuteDialog$$ExternalSyntheticLambda1
                public final /* synthetic */ Runnable f$0;

                {
                    this.f$0 = r1;
                }

                @Override // android.content.DialogInterface.OnCancelListener
                public final void onCancel(DialogInterface dialogInterface) {
                    MuteDialog.m256$r8$lambda$uB93FwgWr3VYAKX8HRtS7yAIjE(this.f$0, dialogInterface);
                }
            });
        }
        materialAlertDialogBuilder.show();
    }

    public static /* synthetic */ void lambda$show$0(MuteSelectionListener muteSelectionListener, DialogInterface dialogInterface, int i) {
        long j;
        long j2;
        long j3;
        if (i == 0) {
            j2 = System.currentTimeMillis();
            j3 = TimeUnit.HOURS.toMillis(1);
        } else if (i == 1) {
            j2 = System.currentTimeMillis();
            j3 = TimeUnit.HOURS.toMillis(8);
        } else if (i == 2) {
            j2 = System.currentTimeMillis();
            j3 = TimeUnit.DAYS.toMillis(1);
        } else if (i == 3) {
            j2 = System.currentTimeMillis();
            j3 = TimeUnit.DAYS.toMillis(7);
        } else if (i != 4) {
            j2 = System.currentTimeMillis();
            j3 = TimeUnit.HOURS.toMillis(1);
        } else {
            j = Long.MAX_VALUE;
            muteSelectionListener.onMuted(j);
        }
        j = j2 + j3;
        muteSelectionListener.onMuted(j);
    }

    public static /* synthetic */ void lambda$show$1(Runnable runnable, DialogInterface dialogInterface) {
        runnable.run();
        dialogInterface.dismiss();
    }
}
