package org.thoughtcrime.securesms.audio;

import android.os.ParcelFileDescriptor;
import java.io.IOException;

/* loaded from: classes.dex */
public interface Recorder {
    void start(ParcelFileDescriptor parcelFileDescriptor) throws IOException;

    void stop();
}
