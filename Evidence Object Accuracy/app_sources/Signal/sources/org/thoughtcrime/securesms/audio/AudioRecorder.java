package org.thoughtcrime.securesms.audio;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.components.voice.VoiceNoteDraft;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.concurrent.ListenableFuture;
import org.thoughtcrime.securesms.util.concurrent.SettableFuture;

/* loaded from: classes.dex */
public class AudioRecorder {
    private static final String TAG = Log.tag(AudioRecorder.class);
    private static final ExecutorService executor = SignalExecutors.newCachedSingleThreadExecutor("signal-AudioRecorder");
    private Uri captureUri;
    private final Context context;
    private Recorder recorder;

    public AudioRecorder(Context context) {
        this.context = context;
    }

    public void startRecording() {
        Log.i(TAG, "startRecording()");
        executor.execute(new Runnable() { // from class: org.thoughtcrime.securesms.audio.AudioRecorder$$ExternalSyntheticLambda5
            @Override // java.lang.Runnable
            public final void run() {
                AudioRecorder.m270$r8$lambda$5RbVKUXLjLZsgJVURvLvR3XFQ(AudioRecorder.this);
            }
        });
    }

    public /* synthetic */ void lambda$startRecording$2() {
        String str = TAG;
        Log.i(str, "Running startRecording() + " + Thread.currentThread().getId());
        try {
            if (this.recorder == null) {
                ParcelFileDescriptor[] createPipe = ParcelFileDescriptor.createPipe();
                this.captureUri = BlobProvider.getInstance().forData(new ParcelFileDescriptor.AutoCloseInputStream(createPipe[0]), 0).withMimeType(MediaUtil.AUDIO_AAC).createForDraftAttachmentAsync(this.context, new BlobProvider.SuccessListener() { // from class: org.thoughtcrime.securesms.audio.AudioRecorder$$ExternalSyntheticLambda2
                    @Override // org.thoughtcrime.securesms.providers.BlobProvider.SuccessListener
                    public final void onSuccess() {
                        AudioRecorder.$r8$lambda$Tf_5xEhr_8zcdUeKp1HZfabuljM();
                    }
                }, new BlobProvider.ErrorListener() { // from class: org.thoughtcrime.securesms.audio.AudioRecorder$$ExternalSyntheticLambda3
                    @Override // org.thoughtcrime.securesms.providers.BlobProvider.ErrorListener
                    public final void onError(IOException iOException) {
                        AudioRecorder.$r8$lambda$YpTI6JqRcJCqtx7AGSofW_iT3mg(iOException);
                    }
                });
                Recorder mediaRecorderWrapper = Build.VERSION.SDK_INT >= 26 ? new MediaRecorderWrapper() : new AudioCodec();
                this.recorder = mediaRecorderWrapper;
                mediaRecorderWrapper.start(createPipe[1]);
                return;
            }
            throw new AssertionError("We can only record once at a time.");
        } catch (IOException e) {
            Log.w(TAG, e);
        }
    }

    public static /* synthetic */ void lambda$startRecording$0() {
        Log.i(TAG, "Write successful.");
    }

    public static /* synthetic */ void lambda$startRecording$1(IOException iOException) {
        Log.w(TAG, "Error during recording", iOException);
    }

    public ListenableFuture<VoiceNoteDraft> stopRecording() {
        Log.i(TAG, "stopRecording()");
        SettableFuture settableFuture = new SettableFuture();
        executor.execute(new Runnable(settableFuture) { // from class: org.thoughtcrime.securesms.audio.AudioRecorder$$ExternalSyntheticLambda4
            public final /* synthetic */ SettableFuture f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AudioRecorder.m272$r8$lambda$mZNbrWt6EfrLKngPWvaZdNh6Tg(AudioRecorder.this, this.f$1);
            }
        });
        return settableFuture;
    }

    public /* synthetic */ void lambda$stopRecording$3(SettableFuture settableFuture) {
        Recorder recorder = this.recorder;
        if (recorder == null) {
            sendToFuture(settableFuture, (Exception) new IOException("MediaRecorder was never initialized successfully!"));
            return;
        }
        recorder.stop();
        try {
            sendToFuture((SettableFuture<SettableFuture>) settableFuture, (SettableFuture) new VoiceNoteDraft(this.captureUri, MediaUtil.getMediaSize(this.context, this.captureUri)));
        } catch (IOException e) {
            Log.w(TAG, e);
            sendToFuture(settableFuture, (Exception) e);
        }
        this.recorder = null;
        this.captureUri = null;
    }

    private <T> void sendToFuture(SettableFuture<T> settableFuture, Exception exc) {
        ThreadUtil.runOnMain(new Runnable(exc) { // from class: org.thoughtcrime.securesms.audio.AudioRecorder$$ExternalSyntheticLambda0
            public final /* synthetic */ Exception f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AudioRecorder.$r8$lambda$Spu9qWnbIG88TlWVH7GgYIn_taI(SettableFuture.this, this.f$1);
            }
        });
    }

    private <T> void sendToFuture(SettableFuture<T> settableFuture, T t) {
        ThreadUtil.runOnMain(new Runnable(t) { // from class: org.thoughtcrime.securesms.audio.AudioRecorder$$ExternalSyntheticLambda1
            public final /* synthetic */ Object f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AudioRecorder.m271$r8$lambda$HbGULy9rerOGHgvt61to66lmbE(SettableFuture.this, this.f$1);
            }
        });
    }
}
