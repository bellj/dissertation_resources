package org.thoughtcrime.securesms.audio;

import android.content.Context;
import android.net.Uri;
import android.util.LruCache;
import androidx.core.util.Consumer;
import com.google.protobuf.ByteString;
import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.audio.AudioWaveForm;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.databaseprotos.AudioWaveFormData;
import org.thoughtcrime.securesms.mms.AudioSlide;
import org.thoughtcrime.securesms.util.concurrent.SerialExecutor;

/* loaded from: classes.dex */
public final class AudioWaveForm {
    private static final Executor AUDIO_DECODER_EXECUTOR = new SerialExecutor(SignalExecutors.BOUNDED);
    private static final int BAR_COUNT;
    private static final int SAMPLES_PER_BAR;
    private static final String TAG = Log.tag(AudioWaveForm.class);
    private static final LruCache<String, AudioFileInfo> WAVE_FORM_CACHE = new LruCache<>(200);
    private final Context context;
    private final AudioSlide slide;

    public AudioWaveForm(Context context, AudioSlide audioSlide) {
        this.context = context.getApplicationContext();
        this.slide = audioSlide;
    }

    public void getWaveForm(Consumer<AudioFileInfo> consumer, Runnable runnable) {
        Uri uri = this.slide.getUri();
        Attachment asAttachment = this.slide.asAttachment();
        if (uri == null) {
            Log.w(TAG, "No uri");
            ThreadUtil.runOnMain(runnable);
            return;
        }
        String uri2 = uri.toString();
        AudioFileInfo audioFileInfo = WAVE_FORM_CACHE.get(uri2);
        if (audioFileInfo != null) {
            String str = TAG;
            Log.i(str, "Loaded wave form from cache " + uri2);
            ThreadUtil.runOnMain(new Runnable(audioFileInfo) { // from class: org.thoughtcrime.securesms.audio.AudioWaveForm$$ExternalSyntheticLambda4
                public final /* synthetic */ AudioWaveForm.AudioFileInfo f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    AudioWaveForm.m273$r8$lambda$4WKgYE_RWkJ3F9sZ7kqA9ENR7I(Consumer.this, this.f$1);
                }
            });
            return;
        }
        AUDIO_DECODER_EXECUTOR.execute(new Runnable(uri2, consumer, asAttachment, runnable, uri) { // from class: org.thoughtcrime.securesms.audio.AudioWaveForm$$ExternalSyntheticLambda5
            public final /* synthetic */ String f$1;
            public final /* synthetic */ Consumer f$2;
            public final /* synthetic */ Attachment f$3;
            public final /* synthetic */ Runnable f$4;
            public final /* synthetic */ Uri f$5;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AudioWaveForm.$r8$lambda$PvwUXRG8KQeqGLhTbqDNysQsQ7M(AudioWaveForm.this, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5);
            }
        });
    }

    public /* synthetic */ void lambda$getWaveForm$5(String str, Consumer consumer, Attachment attachment, Runnable runnable, Uri uri) {
        LruCache<String, AudioFileInfo> lruCache = WAVE_FORM_CACHE;
        AudioFileInfo audioFileInfo = lruCache.get(str);
        if (audioFileInfo != null) {
            String str2 = TAG;
            Log.i(str2, "Loaded wave form from cache inside executor" + str);
            ThreadUtil.runOnMain(new Runnable(audioFileInfo) { // from class: org.thoughtcrime.securesms.audio.AudioWaveForm$$ExternalSyntheticLambda0
                public final /* synthetic */ AudioWaveForm.AudioFileInfo f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    AudioWaveForm.m274$r8$lambda$VCoyTwS2uXSWwKSLlfYJnAjWew(Consumer.this, this.f$1);
                }
            });
            return;
        }
        AudioHash audioHash = attachment.getAudioHash();
        if (audioHash != null) {
            AudioFileInfo fromDatabaseProtobuf = AudioFileInfo.fromDatabaseProtobuf(audioHash.getAudioWaveForm());
            if (fromDatabaseProtobuf.waveForm.length == 0) {
                String str3 = TAG;
                Log.w(str3, "Recovering from a wave form generation error  " + str);
                ThreadUtil.runOnMain(runnable);
                return;
            } else if (fromDatabaseProtobuf.waveForm.length != 46) {
                String str4 = TAG;
                Log.w(str4, "Wave form from database does not match bar count, regenerating " + str);
            } else {
                lruCache.put(str, fromDatabaseProtobuf);
                String str5 = TAG;
                Log.i(str5, "Loaded wave form from DB " + str);
                ThreadUtil.runOnMain(new Runnable(fromDatabaseProtobuf) { // from class: org.thoughtcrime.securesms.audio.AudioWaveForm$$ExternalSyntheticLambda1
                    public final /* synthetic */ AudioWaveForm.AudioFileInfo f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        AudioWaveForm.$r8$lambda$pbpuCvSw6ceMLlF0Y1Dmelz_yK4(Consumer.this, this.f$1);
                    }
                });
                return;
            }
        }
        if (attachment instanceof DatabaseAttachment) {
            try {
                AttachmentDatabase attachments = SignalDatabase.attachments();
                DatabaseAttachment databaseAttachment = (DatabaseAttachment) attachment;
                long currentTimeMillis = System.currentTimeMillis();
                attachments.writeAudioHash(databaseAttachment.getAttachmentId(), AudioWaveFormData.getDefaultInstance());
                String str6 = TAG;
                Log.i(str6, String.format("Starting wave form generation (%s)", str));
                AudioFileInfo generateWaveForm = generateWaveForm(uri);
                Log.i(str6, String.format(Locale.US, "Audio wave form generation time %d ms (%s)", Long.valueOf(System.currentTimeMillis() - currentTimeMillis), str));
                attachments.writeAudioHash(databaseAttachment.getAttachmentId(), generateWaveForm.toDatabaseProtobuf());
                lruCache.put(str, generateWaveForm);
                ThreadUtil.runOnMain(new Runnable(generateWaveForm) { // from class: org.thoughtcrime.securesms.audio.AudioWaveForm$$ExternalSyntheticLambda2
                    public final /* synthetic */ AudioWaveForm.AudioFileInfo f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        AudioWaveForm.$r8$lambda$d0VXfAZePqAc9UjLfCGcZIcB2ww(Consumer.this, this.f$1);
                    }
                });
            } catch (Throwable th) {
                String str7 = TAG;
                Log.w(str7, "Failed to create audio wave form for " + str, th);
                ThreadUtil.runOnMain(runnable);
            }
        } else {
            try {
                String str8 = TAG;
                Log.i(str8, "Not in database and not cached. Generating wave form on-the-fly.");
                long currentTimeMillis2 = System.currentTimeMillis();
                Log.i(str8, String.format("Starting wave form generation (%s)", str));
                AudioFileInfo generateWaveForm2 = generateWaveForm(uri);
                Log.i(str8, String.format(Locale.US, "Audio wave form generation time %d ms (%s)", Long.valueOf(System.currentTimeMillis() - currentTimeMillis2), str));
                lruCache.put(str, generateWaveForm2);
                ThreadUtil.runOnMain(new Runnable(generateWaveForm2) { // from class: org.thoughtcrime.securesms.audio.AudioWaveForm$$ExternalSyntheticLambda3
                    public final /* synthetic */ AudioWaveForm.AudioFileInfo f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        AudioWaveForm.m275$r8$lambda$nAA9HChWu6WHVgUyoA5litN2Tw(Consumer.this, this.f$1);
                    }
                });
            } catch (IOException e) {
                String str9 = TAG;
                Log.w(str9, "Failed to create audio wave form for " + str, e);
                ThreadUtil.runOnMain(runnable);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:108:0x01fe A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00fb A[Catch: all -> 0x01f8, TryCatch #2 {all -> 0x01f8, blocks: (B:3:0x000e, B:5:0x001c, B:7:0x0027, B:9:0x003a, B:11:0x0044, B:47:0x00f5, B:49:0x00fb, B:52:0x0100, B:53:0x010e, B:55:0x0112), top: B:111:0x000e }] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0144  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0181 A[LOOP:3: B:46:0x00f3->B:74:0x0181, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.thoughtcrime.securesms.audio.AudioWaveForm.AudioFileInfo generateWaveForm(android.net.Uri r30) throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 522
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.audio.AudioWaveForm.generateWaveForm(android.net.Uri):org.thoughtcrime.securesms.audio.AudioWaveForm$AudioFileInfo");
    }

    /* loaded from: classes.dex */
    public static class AudioFileInfo {
        private final long durationUs;
        private final float[] waveForm;
        private final byte[] waveFormBytes;

        public static AudioFileInfo fromDatabaseProtobuf(AudioWaveFormData audioWaveFormData) {
            return new AudioFileInfo(audioWaveFormData.getDurationUs(), audioWaveFormData.getWaveForm().toByteArray());
        }

        private AudioFileInfo(long j, byte[] bArr) {
            this.durationUs = j;
            this.waveFormBytes = bArr;
            this.waveForm = new float[bArr.length];
            for (int i = 0; i < bArr.length; i++) {
                this.waveForm[i] = ((float) (bArr[i] & 255)) / 255.0f;
            }
        }

        public long getDuration(TimeUnit timeUnit) {
            return timeUnit.convert(this.durationUs, TimeUnit.MICROSECONDS);
        }

        public float[] getWaveForm() {
            return this.waveForm;
        }

        public AudioWaveFormData toDatabaseProtobuf() {
            return AudioWaveFormData.newBuilder().setDurationUs(this.durationUs).setWaveForm(ByteString.copyFrom(this.waveFormBytes)).build();
        }
    }
}
