package org.thoughtcrime.securesms.audio;

import java.io.IOException;
import org.thoughtcrime.securesms.database.model.databaseprotos.AudioWaveFormData;
import org.whispersystems.util.Base64;

/* loaded from: classes.dex */
public final class AudioHash {
    private final AudioWaveFormData audioWaveForm;
    private final String hash;

    private AudioHash(String str, AudioWaveFormData audioWaveFormData) {
        this.hash = str;
        this.audioWaveForm = audioWaveFormData;
    }

    public AudioHash(AudioWaveFormData audioWaveFormData) {
        this(Base64.encodeBytes(audioWaveFormData.toByteArray()), audioWaveFormData);
    }

    public static AudioHash parseOrNull(String str) {
        if (str == null) {
            return null;
        }
        try {
            return new AudioHash(str, AudioWaveFormData.parseFrom(Base64.decode(str)));
        } catch (IOException unused) {
            return null;
        }
    }

    public AudioWaveFormData getAudioWaveForm() {
        return this.audioWaveForm;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AudioHash.class != obj.getClass()) {
            return false;
        }
        return this.hash.equals(((AudioHash) obj).hash);
    }

    public int hashCode() {
        return this.hash.hashCode();
    }

    public String getHash() {
        return this.hash;
    }
}
