package org.thoughtcrime.securesms.audio;

import android.media.MediaRecorder;
import android.os.ParcelFileDescriptor;
import java.io.IOException;
import org.signal.core.util.logging.Log;

/* loaded from: classes.dex */
public class MediaRecorderWrapper implements Recorder {
    private static final int BIT_RATE;
    private static final int CHANNELS;
    private static final int SAMPLE_RATE;
    private static final String TAG = Log.tag(MediaRecorderWrapper.class);
    private MediaRecorder recorder = null;

    @Override // org.thoughtcrime.securesms.audio.Recorder
    public void start(ParcelFileDescriptor parcelFileDescriptor) throws IOException {
        Log.i(TAG, "Recording voice note using MediaRecorderWrapper.");
        MediaRecorder mediaRecorder = new MediaRecorder();
        this.recorder = mediaRecorder;
        try {
            mediaRecorder.setAudioSource(1);
            this.recorder.setOutputFormat(6);
            this.recorder.setOutputFile(parcelFileDescriptor.getFileDescriptor());
            this.recorder.setAudioEncoder(3);
            this.recorder.setAudioSamplingRate(SAMPLE_RATE);
            this.recorder.setAudioEncodingBitRate(BIT_RATE);
            this.recorder.setAudioChannels(1);
            this.recorder.prepare();
            this.recorder.start();
        } catch (IllegalStateException e) {
            Log.w(TAG, "Unable to start recording", e);
            this.recorder.release();
            this.recorder = null;
            throw new IOException(e);
        }
    }

    @Override // org.thoughtcrime.securesms.audio.Recorder
    public void stop() {
        MediaRecorder mediaRecorder = this.recorder;
        if (mediaRecorder != null) {
            try {
                try {
                    mediaRecorder.stop();
                } catch (RuntimeException e) {
                    if (e.getClass() == RuntimeException.class) {
                        Log.d(TAG, "Recording stopped with no data captured.");
                    } else {
                        throw e;
                    }
                }
            } finally {
                this.recorder.release();
                this.recorder = null;
            }
        }
    }
}
