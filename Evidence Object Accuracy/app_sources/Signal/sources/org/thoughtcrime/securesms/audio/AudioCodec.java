package org.thoughtcrime.securesms.audio;

import android.media.AudioRecord;
import android.media.MediaCodec;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.os.ParcelFileDescriptor;
import android.view.Surface;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.video.VideoUtil;

/* loaded from: classes.dex */
public class AudioCodec implements Recorder {
    private static final int BIT_RATE;
    private static final int CHANNELS;
    private static final int SAMPLE_RATE;
    private static final int SAMPLE_RATE_INDEX;
    private static final String TAG = Log.tag(AudioCodec.class);
    private final AudioRecord audioRecord;
    private final int bufferSize;
    private boolean failed = false;
    private boolean finished = false;
    private final MediaCodec mediaCodec;
    private boolean running = true;

    private byte[] createAdtsHeader(int i) {
        int i2 = i + 7;
        byte[] bArr = new byte[7];
        bArr[0] = -1;
        bArr[1] = -15;
        bArr[2] = 64;
        byte b = (byte) (64 | 16);
        bArr[2] = b;
        bArr[2] = (byte) (0 | b);
        bArr[3] = (byte) (((i2 >> 11) & 3) | 64);
        bArr[4] = (byte) ((i2 >> 3) & 255);
        bArr[5] = (byte) (((i2 & 7) << 5) | 31);
        bArr[6] = -4;
        return bArr;
    }

    public AudioCodec() throws IOException {
        int minBufferSize = AudioRecord.getMinBufferSize(SAMPLE_RATE, 16, 2);
        this.bufferSize = minBufferSize;
        AudioRecord createAudioRecord = createAudioRecord(minBufferSize);
        this.audioRecord = createAudioRecord;
        MediaCodec createMediaCodec = createMediaCodec(minBufferSize);
        this.mediaCodec = createMediaCodec;
        createMediaCodec.start();
        try {
            createAudioRecord.startRecording();
        } catch (Exception e) {
            Log.w(TAG, e);
            this.mediaCodec.release();
            throw new IOException(e);
        }
    }

    @Override // org.thoughtcrime.securesms.audio.Recorder
    public void start(ParcelFileDescriptor parcelFileDescriptor) {
        Log.i(TAG, "Recording voice note using AudioCodec.");
        start(new ParcelFileDescriptor.AutoCloseOutputStream(parcelFileDescriptor));
    }

    @Override // org.thoughtcrime.securesms.audio.Recorder
    public synchronized void stop() {
        this.running = false;
        while (!this.finished) {
            Util.wait(this, 0);
        }
    }

    private void start(final OutputStream outputStream) {
        new Thread(new Runnable() { // from class: org.thoughtcrime.securesms.audio.AudioCodec.1
            @Override // java.lang.Runnable
            public void run() {
                IllegalStateException e;
                boolean isRunning;
                MediaCodec.BufferInfo bufferInfo = new MediaCodec.BufferInfo();
                byte[] bArr = new byte[AudioCodec.this.bufferSize];
                ByteBuffer[] inputBuffers = AudioCodec.this.mediaCodec.getInputBuffers();
                ByteBuffer[] outputBuffers = AudioCodec.this.mediaCodec.getOutputBuffers();
                do {
                    try {
                        try {
                            isRunning = AudioCodec.this.isRunning();
                            AudioCodec audioCodec = AudioCodec.this;
                            audioCodec.handleCodecInput(audioCodec.audioRecord, bArr, AudioCodec.this.mediaCodec, inputBuffers, isRunning);
                            AudioCodec audioCodec2 = AudioCodec.this;
                            audioCodec2.handleCodecOutput(audioCodec2.mediaCodec, outputBuffers, bufferInfo, outputStream);
                        } catch (IOException e2) {
                            Log.w(AudioCodec.TAG, e2);
                            try {
                                AudioCodec.this.mediaCodec.stop();
                            } catch (IllegalStateException e3) {
                                Log.w(AudioCodec.TAG, "mediaCodec stop failed.", e3);
                            }
                            try {
                                AudioCodec.this.audioRecord.stop();
                            } catch (IllegalStateException e4) {
                                Log.w(AudioCodec.TAG, "audioRecord stop failed.", e4);
                            }
                            try {
                                AudioCodec.this.mediaCodec.release();
                            } catch (IllegalStateException e5) {
                                e = e5;
                                Log.w(AudioCodec.TAG, "mediaCodec release failed. Probably already released.", e);
                                AudioCodec.this.audioRecord.release();
                                StreamUtil.close(outputStream);
                                AudioCodec.this.setFinished();
                            }
                        }
                    } catch (Throwable th) {
                        try {
                            AudioCodec.this.mediaCodec.stop();
                        } catch (IllegalStateException e6) {
                            Log.w(AudioCodec.TAG, "mediaCodec stop failed.", e6);
                        }
                        try {
                            AudioCodec.this.audioRecord.stop();
                        } catch (IllegalStateException e7) {
                            Log.w(AudioCodec.TAG, "audioRecord stop failed.", e7);
                        }
                        try {
                            AudioCodec.this.mediaCodec.release();
                        } catch (IllegalStateException e8) {
                            Log.w(AudioCodec.TAG, "mediaCodec release failed. Probably already released.", e8);
                        }
                        AudioCodec.this.audioRecord.release();
                        StreamUtil.close(outputStream);
                        AudioCodec.this.setFinished();
                        throw th;
                    }
                } while (isRunning);
                try {
                    AudioCodec.this.mediaCodec.stop();
                } catch (IllegalStateException e9) {
                    Log.w(AudioCodec.TAG, "mediaCodec stop failed.", e9);
                }
                try {
                    AudioCodec.this.audioRecord.stop();
                } catch (IllegalStateException e10) {
                    Log.w(AudioCodec.TAG, "audioRecord stop failed.", e10);
                }
                try {
                    AudioCodec.this.mediaCodec.release();
                } catch (IllegalStateException e11) {
                    e = e11;
                    Log.w(AudioCodec.TAG, "mediaCodec release failed. Probably already released.", e);
                    AudioCodec.this.audioRecord.release();
                    StreamUtil.close(outputStream);
                    AudioCodec.this.setFinished();
                }
                AudioCodec.this.audioRecord.release();
                StreamUtil.close(outputStream);
                AudioCodec.this.setFinished();
            }
        }, "signal-AudioCodec").start();
    }

    public synchronized boolean isRunning() {
        return this.running;
    }

    public synchronized void setFinished() {
        this.finished = true;
        notifyAll();
    }

    public void handleCodecInput(AudioRecord audioRecord, byte[] bArr, MediaCodec mediaCodec, ByteBuffer[] byteBufferArr, boolean z) {
        int read = audioRecord.read(bArr, 0, bArr.length);
        int dequeueInputBuffer = mediaCodec.dequeueInputBuffer(10000);
        if (dequeueInputBuffer >= 0) {
            ByteBuffer byteBuffer = byteBufferArr[dequeueInputBuffer];
            byteBuffer.clear();
            byteBuffer.put(bArr);
            mediaCodec.queueInputBuffer(dequeueInputBuffer, 0, read, 0, z ? 0 : 4);
        }
    }

    public void handleCodecOutput(MediaCodec mediaCodec, ByteBuffer[] byteBufferArr, MediaCodec.BufferInfo bufferInfo, OutputStream outputStream) throws IOException {
        int dequeueOutputBuffer = mediaCodec.dequeueOutputBuffer(bufferInfo, 0);
        while (dequeueOutputBuffer != -1) {
            if (dequeueOutputBuffer >= 0) {
                ByteBuffer byteBuffer = byteBufferArr[dequeueOutputBuffer];
                byteBuffer.position(bufferInfo.offset);
                byteBuffer.limit(bufferInfo.offset + bufferInfo.size);
                if ((bufferInfo.flags & 2) != 2) {
                    outputStream.write(createAdtsHeader(bufferInfo.size - bufferInfo.offset));
                    byte[] bArr = new byte[byteBuffer.remaining()];
                    byteBuffer.get(bArr);
                    outputStream.write(bArr);
                }
                byteBuffer.clear();
                mediaCodec.releaseOutputBuffer(dequeueOutputBuffer, false);
            } else if (dequeueOutputBuffer == -3) {
                byteBufferArr = mediaCodec.getOutputBuffers();
            }
            dequeueOutputBuffer = mediaCodec.dequeueOutputBuffer(bufferInfo, 0);
        }
    }

    private AudioRecord createAudioRecord(int i) {
        return new AudioRecord(1, SAMPLE_RATE, 16, 2, i * 10);
    }

    private MediaCodec createMediaCodec(int i) throws IOException {
        MediaCodec createEncoderByType = MediaCodec.createEncoderByType(VideoUtil.AUDIO_MIME_TYPE);
        MediaFormat mediaFormat = new MediaFormat();
        mediaFormat.setString("mime", VideoUtil.AUDIO_MIME_TYPE);
        mediaFormat.setInteger("sample-rate", SAMPLE_RATE);
        mediaFormat.setInteger("channel-count", 1);
        mediaFormat.setInteger("max-input-size", i);
        mediaFormat.setInteger("bitrate", BIT_RATE);
        mediaFormat.setInteger("aac-profile", 2);
        try {
            createEncoderByType.configure(mediaFormat, (Surface) null, (MediaCrypto) null, 1);
            return createEncoderByType;
        } catch (Exception e) {
            Log.w(TAG, e);
            createEncoderByType.release();
            throw new IOException(e);
        }
    }
}
