package org.thoughtcrime.securesms;

import android.animation.Animator;
import android.app.KeyguardManager;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.text.style.TypefaceSpan;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.animation.AnimationCompleteListener;
import org.thoughtcrime.securesms.components.AnimatingToggle;
import org.thoughtcrime.securesms.crypto.InvalidPassphraseException;
import org.thoughtcrime.securesms.crypto.MasterSecretUtil;
import org.thoughtcrime.securesms.logsubmit.SubmitDebugLogActivity;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.DynamicIntroTheme;
import org.thoughtcrime.securesms.util.DynamicLanguage;
import org.thoughtcrime.securesms.util.SupportEmailUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes.dex */
public class PassphrasePromptActivity extends PassphraseActivity {
    static final /* synthetic */ boolean $assertionsDisabled;
    private static final int ALLOWED_AUTHENTICATORS;
    private static final short AUTHENTICATE_REQUEST_CODE;
    private static final int BIOMETRIC_AUTHENTICATORS;
    private static final String BUNDLE_ALREADY_SHOWN;
    public static final String FROM_FOREGROUND;
    private static final String TAG = Log.tag(PassphrasePromptActivity.class);
    private boolean alreadyShown;
    private boolean authenticated;
    private BiometricManager biometricManager;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo biometricPromptInfo;
    private DynamicLanguage dynamicLanguage = new DynamicLanguage();
    private DynamicIntroTheme dynamicTheme = new DynamicIntroTheme();
    private ImageView fingerprintPrompt;
    private boolean hadFailure;
    private ImageButton hideButton;
    private TextView lockScreenButton;
    private View passphraseAuthContainer;
    private EditText passphraseText;
    private final Runnable resumeScreenLockRunnable = new Runnable() { // from class: org.thoughtcrime.securesms.PassphrasePromptActivity$$ExternalSyntheticLambda1
        @Override // java.lang.Runnable
        public final void run() {
            PassphrasePromptActivity.m257$r8$lambda$K6sSfiOoURd4RdSVkVcUbDp2FA(PassphrasePromptActivity.this);
        }
    };
    private ImageButton showButton;
    private AnimatingToggle visibilityToggle;

    public /* synthetic */ void lambda$new$0() {
        resumeScreenLock(!this.alreadyShown);
        this.alreadyShown = true;
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        Log.i(TAG, "onCreate()");
        this.dynamicTheme.onCreate(this);
        this.dynamicLanguage.onCreate(this);
        super.onCreate(bundle);
        setContentView(R.layout.prompt_passphrase_activity);
        initializeResources();
        boolean z = false;
        if ((bundle != null && bundle.getBoolean(BUNDLE_ALREADY_SHOWN)) || getIntent().getBooleanExtra(FROM_FOREGROUND, false)) {
            z = true;
        }
        this.alreadyShown = z;
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean(BUNDLE_ALREADY_SHOWN, this.alreadyShown);
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
        this.dynamicLanguage.onResume(this);
        setLockTypeVisibility();
        if (TextSecurePreferences.isScreenLockEnabled(this) && !this.authenticated && !this.hadFailure) {
            ThreadUtil.postToMain(this.resumeScreenLockRunnable);
        }
        this.hadFailure = false;
        this.fingerprintPrompt.setImageResource(R.drawable.ic_fingerprint_white_48dp);
        this.fingerprintPrompt.getBackground().setColorFilter(getResources().getColor(R.color.signal_accent_primary), PorterDuff.Mode.SRC_IN);
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onPause() {
        super.onPause();
        ThreadUtil.cancelRunnableOnMain(this.resumeScreenLockRunnable);
        this.biometricPrompt.cancelAuthentication();
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override // android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menu.clear();
        menuInflater.inflate(R.menu.passphrase_prompt, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override // android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        if (menuItem.getItemId() == R.id.menu_submit_debug_logs) {
            handleLogSubmit();
            return true;
        } else if (menuItem.getItemId() != R.id.menu_contact_support) {
            return false;
        } else {
            sendEmailToSupport();
            return true;
        }
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 1007) {
            if (i2 == -1) {
                handleAuthenticated();
                return;
            }
            Log.w(TAG, "Authentication failed");
            this.hadFailure = true;
        }
    }

    private void handleLogSubmit() {
        startActivity(new Intent(this, SubmitDebugLogActivity.class));
    }

    public void handlePassphrase() {
        String str;
        try {
            Editable text = this.passphraseText.getText();
            if (text == null) {
                str = "";
            } else {
                str = text.toString();
            }
            setMasterSecret(MasterSecretUtil.getMasterSecret(this, str));
        } catch (InvalidPassphraseException unused) {
            this.passphraseText.setText("");
            this.passphraseText.setError(getString(R.string.PassphrasePromptActivity_invalid_passphrase_exclamation));
        }
    }

    public void handleAuthenticated() {
        try {
            this.authenticated = true;
            setMasterSecret(MasterSecretUtil.getMasterSecret(this, MasterSecretUtil.UNENCRYPTED_PASSPHRASE));
        } catch (InvalidPassphraseException e) {
            throw new AssertionError(e);
        }
    }

    public void setPassphraseVisibility(boolean z) {
        int selectionStart = this.passphraseText.getSelectionStart();
        if (z) {
            this.passphraseText.setInputType(145);
        } else {
            this.passphraseText.setInputType(129);
        }
        this.passphraseText.setSelection(selectionStart);
    }

    private void initializeResources() {
        this.showButton = (ImageButton) findViewById(R.id.passphrase_visibility);
        this.hideButton = (ImageButton) findViewById(R.id.passphrase_visibility_off);
        this.visibilityToggle = (AnimatingToggle) findViewById(R.id.button_toggle);
        this.passphraseText = (EditText) findViewById(R.id.passphrase_edit);
        this.passphraseAuthContainer = findViewById(R.id.password_auth_container);
        this.fingerprintPrompt = (ImageView) findViewById(R.id.fingerprint_auth_container);
        this.lockScreenButton = (TextView) findViewById(R.id.lock_screen_auth_container);
        this.biometricManager = BiometricManager.from(this);
        this.biometricPrompt = new BiometricPrompt(this, new BiometricAuthenticationListener());
        this.biometricPromptInfo = new BiometricPrompt.PromptInfo.Builder().setAllowedAuthenticators(ALLOWED_AUTHENTICATORS).setTitle(getString(R.string.PassphrasePromptActivity_unlock_signal)).build();
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setTitle("");
        SpannableString spannableString = new SpannableString("  " + getString(R.string.PassphrasePromptActivity_enter_passphrase));
        spannableString.setSpan(new RelativeSizeSpan(0.9f), 0, spannableString.length(), 18);
        spannableString.setSpan(new TypefaceSpan("sans-serif"), 0, spannableString.length(), 18);
        this.passphraseText.setHint(spannableString);
        ((ImageButton) findViewById(R.id.ok_button)).setOnClickListener(new OkButtonClickListener());
        this.showButton.setOnClickListener(new ShowButtonOnClickListener());
        this.hideButton.setOnClickListener(new HideButtonOnClickListener());
        this.passphraseText.setOnEditorActionListener(new PassphraseActionListener());
        this.passphraseText.setImeActionLabel(getString(R.string.prompt_passphrase_activity__unlock), 6);
        this.fingerprintPrompt.setImageResource(R.drawable.ic_fingerprint_white_48dp);
        this.fingerprintPrompt.getBackground().setColorFilter(getResources().getColor(R.color.core_ultramarine), PorterDuff.Mode.SRC_IN);
        this.lockScreenButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.PassphrasePromptActivity$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                PassphrasePromptActivity.m258$r8$lambda$M0iG9e2mp_9JlFL3sFZ0FzK9vE(PassphrasePromptActivity.this, view);
            }
        });
    }

    public /* synthetic */ void lambda$initializeResources$1(View view) {
        resumeScreenLock(true);
    }

    private void setLockTypeVisibility() {
        int i = 8;
        if (TextSecurePreferences.isScreenLockEnabled(this)) {
            this.passphraseAuthContainer.setVisibility(8);
            ImageView imageView = this.fingerprintPrompt;
            if (this.biometricManager.canAuthenticate(BIOMETRIC_AUTHENTICATORS) == 0) {
                i = 0;
            }
            imageView.setVisibility(i);
            this.lockScreenButton.setVisibility(0);
            return;
        }
        this.passphraseAuthContainer.setVisibility(0);
        this.fingerprintPrompt.setVisibility(8);
        this.lockScreenButton.setVisibility(8);
    }

    private void resumeScreenLock(boolean z) {
        KeyguardManager keyguardManager = (KeyguardManager) getSystemService("keyguard");
        if (!keyguardManager.isKeyguardSecure()) {
            Log.w(TAG, "Keyguard not secure...");
            handleAuthenticated();
            return;
        }
        int i = Build.VERSION.SDK_INT;
        if (i == 29 || this.biometricManager.canAuthenticate(ALLOWED_AUTHENTICATORS) != 0) {
            if (i < 21) {
                Log.w(TAG, "Not compatible...");
                handleAuthenticated();
            } else if (z) {
                Log.i(TAG, "firing intent...");
                startActivityForResult(keyguardManager.createConfirmDeviceCredentialIntent(getString(R.string.PassphrasePromptActivity_unlock_signal), ""), 1007);
            } else {
                Log.i(TAG, "Skipping firing intent unless forced");
            }
        } else if (z) {
            Log.i(TAG, "Listening for biometric authentication...");
            this.biometricPrompt.authenticate(this.biometricPromptInfo);
        } else {
            Log.i(TAG, "Skipping show system biometric dialog unless forced");
        }
    }

    private void sendEmailToSupport() {
        CommunicationActions.openEmail(this, SupportEmailUtil.getSupportEmailAddress(this), getString(R.string.PassphrasePromptActivity_signal_android_lock_screen), SupportEmailUtil.generateSupportEmailBody(this, R.string.PassphrasePromptActivity_signal_android_lock_screen, null, null));
    }

    /* loaded from: classes.dex */
    public class PassphraseActionListener implements TextView.OnEditorActionListener {
        private PassphraseActionListener() {
            PassphrasePromptActivity.this = r1;
        }

        @Override // android.widget.TextView.OnEditorActionListener
        public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if ((keyEvent == null && i == 6) || (keyEvent != null && keyEvent.getAction() == 0 && i == 0)) {
                PassphrasePromptActivity.this.handlePassphrase();
                return true;
            } else if (keyEvent != null && keyEvent.getAction() == 1 && i == 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    /* loaded from: classes.dex */
    public class OkButtonClickListener implements View.OnClickListener {
        private OkButtonClickListener() {
            PassphrasePromptActivity.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            PassphrasePromptActivity.this.handlePassphrase();
        }
    }

    /* loaded from: classes.dex */
    public class ShowButtonOnClickListener implements View.OnClickListener {
        private ShowButtonOnClickListener() {
            PassphrasePromptActivity.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            PassphrasePromptActivity.this.visibilityToggle.display(PassphrasePromptActivity.this.hideButton);
            PassphrasePromptActivity.this.setPassphraseVisibility(true);
        }
    }

    /* loaded from: classes.dex */
    public class HideButtonOnClickListener implements View.OnClickListener {
        private HideButtonOnClickListener() {
            PassphrasePromptActivity.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            PassphrasePromptActivity.this.visibilityToggle.display(PassphrasePromptActivity.this.showButton);
            PassphrasePromptActivity.this.setPassphraseVisibility(false);
        }
    }

    @Override // org.thoughtcrime.securesms.PassphraseActivity
    protected void cleanup() {
        this.passphraseText.setText("");
        System.gc();
    }

    /* loaded from: classes.dex */
    public class BiometricAuthenticationListener extends BiometricPrompt.AuthenticationCallback {
        private BiometricAuthenticationListener() {
            PassphrasePromptActivity.this = r1;
        }

        @Override // androidx.biometric.BiometricPrompt.AuthenticationCallback
        public void onAuthenticationError(int i, CharSequence charSequence) {
            String str = PassphrasePromptActivity.TAG;
            Log.w(str, "Authentication error: " + i);
            PassphrasePromptActivity.this.hadFailure = true;
            if (i != 5 && i != 10) {
                onAuthenticationFailed();
            }
        }

        @Override // androidx.biometric.BiometricPrompt.AuthenticationCallback
        public void onAuthenticationSucceeded(BiometricPrompt.AuthenticationResult authenticationResult) {
            Log.i(PassphrasePromptActivity.TAG, "onAuthenticationSucceeded");
            PassphrasePromptActivity.this.fingerprintPrompt.setImageResource(R.drawable.ic_check_white_48dp);
            PassphrasePromptActivity.this.fingerprintPrompt.getBackground().setColorFilter(PassphrasePromptActivity.this.getResources().getColor(R.color.green_500), PorterDuff.Mode.SRC_IN);
            PassphrasePromptActivity.this.fingerprintPrompt.animate().setInterpolator(new BounceInterpolator()).scaleX(1.1f).scaleY(1.1f).setDuration(500).setListener(new AnimationCompleteListener() { // from class: org.thoughtcrime.securesms.PassphrasePromptActivity.BiometricAuthenticationListener.1
                @Override // org.thoughtcrime.securesms.animation.AnimationCompleteListener, android.animation.Animator.AnimatorListener
                public void onAnimationEnd(Animator animator) {
                    PassphrasePromptActivity.this.handleAuthenticated();
                }
            }).start();
        }

        @Override // androidx.biometric.BiometricPrompt.AuthenticationCallback
        public void onAuthenticationFailed() {
            Log.w(PassphrasePromptActivity.TAG, "onAuthenticationFailed()");
            PassphrasePromptActivity.this.fingerprintPrompt.setImageResource(R.drawable.ic_close_white_48dp);
            PassphrasePromptActivity.this.fingerprintPrompt.getBackground().setColorFilter(PassphrasePromptActivity.this.getResources().getColor(R.color.red_500), PorterDuff.Mode.SRC_IN);
            TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 30.0f, 0.0f, 0.0f);
            translateAnimation.setDuration(50);
            translateAnimation.setRepeatCount(7);
            translateAnimation.setAnimationListener(new Animation.AnimationListener() { // from class: org.thoughtcrime.securesms.PassphrasePromptActivity.BiometricAuthenticationListener.2
                @Override // android.view.animation.Animation.AnimationListener
                public void onAnimationRepeat(Animation animation) {
                }

                @Override // android.view.animation.Animation.AnimationListener
                public void onAnimationStart(Animation animation) {
                }

                @Override // android.view.animation.Animation.AnimationListener
                public void onAnimationEnd(Animation animation) {
                    PassphrasePromptActivity.this.fingerprintPrompt.setImageResource(R.drawable.ic_fingerprint_white_48dp);
                    PassphrasePromptActivity.this.fingerprintPrompt.getBackground().setColorFilter(PassphrasePromptActivity.this.getResources().getColor(R.color.signal_accent_primary), PorterDuff.Mode.SRC_IN);
                }
            });
            PassphrasePromptActivity.this.fingerprintPrompt.startAnimation(translateAnimation);
        }
    }
}
