package org.thoughtcrime.securesms.revealable;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import com.pnikosis.materialishprogress.ProgressWheel;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.events.PartProgressEvent;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.util.ContextUtil;
import org.thoughtcrime.securesms.util.DrawableUtil;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes.dex */
public class ViewOnceMessageView extends LinearLayout {
    private static final String TAG = Log.tag(ViewOnceMessageView.class);
    private Attachment attachment;
    private int circleColor;
    private int circleColorWithWallpaper;
    private AppCompatImageView icon;
    private int openedIconColor;
    private ProgressWheel progress;
    private TextView text;
    private int textColor;
    private int unopenedIconColor;

    public ViewOnceMessageView(Context context) {
        super(context);
        init(null);
    }

    public ViewOnceMessageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(attributeSet);
    }

    private void init(AttributeSet attributeSet) {
        LinearLayout.inflate(getContext(), R.layout.revealable_message_view, this);
        setOrientation(0);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, R.styleable.ViewOnceMessageView, 0, 0);
            this.textColor = obtainStyledAttributes.getColor(3, -16777216);
            this.openedIconColor = obtainStyledAttributes.getColor(2, -16777216);
            this.unopenedIconColor = obtainStyledAttributes.getColor(4, -16777216);
            this.circleColor = obtainStyledAttributes.getColor(0, -16777216);
            this.circleColorWithWallpaper = obtainStyledAttributes.getColor(1, -16777216);
            obtainStyledAttributes.recycle();
        }
        this.icon = (AppCompatImageView) findViewById(R.id.revealable_icon);
        this.progress = (ProgressWheel) findViewById(R.id.revealable_progress);
        this.text = (TextView) findViewById(R.id.revealable_text);
    }

    @Override // android.view.View, android.view.ViewGroup
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override // android.view.View, android.view.ViewGroup
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        EventBus.getDefault().unregister(this);
    }

    public boolean requiresTapToDownload(MmsMessageRecord mmsMessageRecord) {
        if (mmsMessageRecord.isOutgoing() || mmsMessageRecord.getSlideDeck().getThumbnailSlide() == null) {
            return false;
        }
        Attachment asAttachment = mmsMessageRecord.getSlideDeck().getThumbnailSlide().asAttachment();
        if (asAttachment.getTransferState() == 3 || asAttachment.getTransferState() == 2) {
            return true;
        }
        return false;
    }

    public void setMessage(MmsMessageRecord mmsMessageRecord, boolean z) {
        this.attachment = mmsMessageRecord.getSlideDeck().getThumbnailSlide() != null ? mmsMessageRecord.getSlideDeck().getThumbnailSlide().asAttachment() : null;
        presentMessage(mmsMessageRecord, z);
    }

    public void presentMessage(MmsMessageRecord mmsMessageRecord, boolean z) {
        presentText(mmsMessageRecord, z);
    }

    private void presentText(MmsMessageRecord mmsMessageRecord, boolean z) {
        int i;
        int i2;
        boolean z2 = true;
        if (!mmsMessageRecord.isOutgoing() || !networkInProgress(mmsMessageRecord)) {
            if (!mmsMessageRecord.isOutgoing()) {
                if (ViewOnceUtil.isViewable(mmsMessageRecord)) {
                    i2 = this.unopenedIconColor;
                    this.text.setText(getDescriptionId(mmsMessageRecord));
                    this.icon.setImageResource(R.drawable.ic_view_once_24);
                } else if (networkInProgress(mmsMessageRecord)) {
                    i = this.unopenedIconColor;
                    this.text.setText("");
                    this.icon.setImageResource(0);
                } else if (requiresTapToDownload(mmsMessageRecord)) {
                    i2 = this.unopenedIconColor;
                    this.text.setText(formatFileSize(mmsMessageRecord));
                    this.icon.setImageResource(R.drawable.ic_arrow_down_circle_outline_24);
                } else {
                    i = this.openedIconColor;
                    this.text.setText(R.string.RevealableMessageView_viewed);
                    this.icon.setImageResource(R.drawable.ic_viewed_once_24);
                }
                i = i2;
            } else if (mmsMessageRecord.isRemoteViewed()) {
                i = this.openedIconColor;
                this.text.setText(R.string.RevealableMessageView_viewed);
                this.icon.setImageResource(R.drawable.ic_viewed_once_24);
            } else {
                i = this.unopenedIconColor;
                this.text.setText(R.string.RevealableMessageView_media);
                this.icon.setImageResource(R.drawable.ic_view_once_24);
            }
            z2 = false;
        } else {
            i = this.openedIconColor;
            this.text.setText(R.string.RevealableMessageView_media);
            this.icon.setImageResource(0);
        }
        this.text.setTextColor(this.textColor);
        this.icon.setColorFilter(i);
        this.icon.setBackgroundDrawable(getBackgroundDrawable(z));
        this.progress.setBarColor(i);
        this.progress.setRimColor(0);
        if (z2) {
            this.progress.setVisibility(0);
        } else {
            this.progress.setVisibility(8);
        }
    }

    private Drawable getBackgroundDrawable(boolean z) {
        return DrawableUtil.tint(ContextUtil.requireDrawable(getContext(), R.drawable.circle_tintable), z ? this.circleColorWithWallpaper : this.circleColor);
    }

    private boolean networkInProgress(MmsMessageRecord mmsMessageRecord) {
        if (mmsMessageRecord.getSlideDeck().getThumbnailSlide() != null && mmsMessageRecord.getSlideDeck().getThumbnailSlide().asAttachment().getTransferState() == 1) {
            return true;
        }
        return false;
    }

    private String formatFileSize(MmsMessageRecord mmsMessageRecord) {
        if (mmsMessageRecord.getSlideDeck().getThumbnailSlide() == null) {
            return "";
        }
        return Util.getPrettyFileSize(mmsMessageRecord.getSlideDeck().getThumbnailSlide().getFileSize());
    }

    private static int getDescriptionId(MmsMessageRecord mmsMessageRecord) {
        Slide thumbnailSlide = mmsMessageRecord.getSlideDeck().getThumbnailSlide();
        return (thumbnailSlide == null || !MediaUtil.isVideoType(thumbnailSlide.getContentType())) ? R.string.RevealableMessageView_view_photo : R.string.RevealableMessageView_view_video;
    }

    @Subscribe(sticky = BuildConfig.PLAY_STORE_DISABLED, threadMode = ThreadMode.MAIN)
    public void onEventAsync(PartProgressEvent partProgressEvent) {
        if (partProgressEvent.attachment.equals(this.attachment)) {
            this.progress.setInstantProgress(((float) partProgressEvent.progress) / ((float) partProgressEvent.total));
        }
    }
}
