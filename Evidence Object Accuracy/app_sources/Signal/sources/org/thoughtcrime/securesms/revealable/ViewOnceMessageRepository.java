package org.thoughtcrime.securesms.revealable;

import android.content.Context;
import j$.util.Optional;
import java.util.Collections;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.NoSuchMessageException;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.MultiDeviceViewedUpdateJob;
import org.thoughtcrime.securesms.jobs.SendViewedReceiptJob;
import org.thoughtcrime.securesms.revealable.ViewOnceMessageRepository;

/* loaded from: classes4.dex */
public class ViewOnceMessageRepository {
    private static final String TAG = Log.tag(ViewOnceMessageRepository.class);
    private final MessageDatabase mmsDatabase = SignalDatabase.mms();

    /* loaded from: classes4.dex */
    public interface Callback<T> {
        void onComplete(T t);
    }

    public ViewOnceMessageRepository(Context context) {
    }

    public void getMessage(long j, Callback<Optional<MmsMessageRecord>> callback) {
        SignalExecutors.BOUNDED.execute(new Runnable(j, callback) { // from class: org.thoughtcrime.securesms.revealable.ViewOnceMessageRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ long f$1;
            public final /* synthetic */ ViewOnceMessageRepository.Callback f$2;

            {
                this.f$1 = r2;
                this.f$2 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ViewOnceMessageRepository.this.lambda$getMessage$0(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$getMessage$0(long j, Callback callback) {
        try {
            MmsMessageRecord mmsMessageRecord = (MmsMessageRecord) this.mmsDatabase.getMessageRecord(j);
            MessageDatabase.MarkedMessageInfo incomingMessageViewed = this.mmsDatabase.setIncomingMessageViewed(mmsMessageRecord.getId());
            if (incomingMessageViewed != null) {
                ApplicationDependencies.getJobManager().add(new SendViewedReceiptJob(mmsMessageRecord.getThreadId(), incomingMessageViewed.getSyncMessageId().getRecipientId(), incomingMessageViewed.getSyncMessageId().getTimetamp(), incomingMessageViewed.getMessageId()));
                MultiDeviceViewedUpdateJob.enqueue(Collections.singletonList(incomingMessageViewed.getSyncMessageId()));
            }
            callback.onComplete(Optional.ofNullable(mmsMessageRecord));
        } catch (NoSuchMessageException unused) {
            callback.onComplete(Optional.empty());
        }
    }
}
