package org.thoughtcrime.securesms.revealable;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import j$.util.Optional;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.mms.VideoSlide;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.revealable.ViewOnceMessageViewModel;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.video.VideoPlayer;

/* loaded from: classes4.dex */
public class ViewOnceMessageActivity extends PassphraseRequiredActivity implements VideoPlayer.PlayerStateCallback {
    private static final String KEY_MESSAGE_ID;
    private static final String KEY_URI;
    private static final String TAG = Log.tag(ViewOnceMessageActivity.class);
    private View closeButton;
    private TextView duration;
    private final Runnable durationUpdateRunnable = new Runnable() { // from class: org.thoughtcrime.securesms.revealable.ViewOnceMessageActivity$$ExternalSyntheticLambda0
        @Override // java.lang.Runnable
        public final void run() {
            ViewOnceMessageActivity.this.lambda$new$0();
        }
    };
    private final Handler handler = new Handler(Looper.getMainLooper());
    private ImageView image;
    private Uri uri;
    private VideoPlayer video;
    private ViewOnceMessageViewModel viewModel;

    public /* synthetic */ void lambda$new$0() {
        long seconds = TimeUnit.MILLISECONDS.toSeconds(this.video.getDuration() - this.video.getPlaybackPosition());
        this.duration.setText(getString(R.string.ViewOnceMessageActivity_video_duration, new Object[]{Long.valueOf(seconds / 60), Long.valueOf(seconds % 60)}));
        scheduleDurationUpdate();
    }

    public static Intent getIntent(Context context, long j, Uri uri) {
        Intent intent = new Intent(context, ViewOnceMessageActivity.class);
        intent.putExtra("message_id", j);
        intent.putExtra(KEY_URI, uri);
        return intent;
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity, android.view.ContextThemeWrapper, android.content.ContextWrapper
    public void attachBaseContext(Context context) {
        getDelegate().setLocalNightMode(2);
        super.attachBaseContext(context);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        super.onCreate(bundle, z);
        setContentView(R.layout.view_once_message_activity);
        this.image = (ImageView) findViewById(R.id.view_once_image);
        this.video = (VideoPlayer) findViewById(R.id.view_once_video);
        this.duration = (TextView) findViewById(R.id.view_once_duration);
        this.closeButton = findViewById(R.id.view_once_close_button);
        this.uri = (Uri) getIntent().getParcelableExtra(KEY_URI);
        this.closeButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.revealable.ViewOnceMessageActivity$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ViewOnceMessageActivity.this.lambda$onCreate$1(view);
            }
        });
        initViewModel(getIntent().getLongExtra("message_id", -1), this.uri);
    }

    public /* synthetic */ void lambda$onCreate$1(View view) {
        finish();
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onStop() {
        super.onStop();
        cancelDurationUpdate();
        this.video.cleanup();
        BlobProvider.getInstance().delete(this, this.uri);
        finish();
    }

    @Override // org.thoughtcrime.securesms.video.VideoPlayer.PlayerStateCallback
    public void onPlayerReady() {
        this.handler.post(this.durationUpdateRunnable);
    }

    private void initViewModel(long j, Uri uri) {
        ViewOnceMessageViewModel viewOnceMessageViewModel = (ViewOnceMessageViewModel) ViewModelProviders.of(this, new ViewOnceMessageViewModel.Factory(j, new ViewOnceMessageRepository(this))).get(ViewOnceMessageViewModel.class);
        this.viewModel = viewOnceMessageViewModel;
        viewOnceMessageViewModel.getMessage().observe(this, new Observer(uri) { // from class: org.thoughtcrime.securesms.revealable.ViewOnceMessageActivity$$ExternalSyntheticLambda2
            public final /* synthetic */ Uri f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ViewOnceMessageActivity.this.lambda$initViewModel$2(this.f$1, (Optional) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initViewModel$2(Uri uri, Optional optional) {
        if (optional != null) {
            if (optional.isPresent()) {
                displayMedia(uri);
                return;
            }
            this.image.setImageDrawable(null);
            finish();
        }
    }

    private void displayMedia(Uri uri) {
        if (MediaUtil.isVideoType(PartAuthority.getAttachmentContentType(this, uri))) {
            displayVideo(uri);
        } else {
            displayImage(uri);
        }
    }

    private void displayVideo(Uri uri) {
        this.video.setVisibility(0);
        this.image.setVisibility(8);
        this.duration.setVisibility(0);
        VideoSlide videoSlide = new VideoSlide(this, uri, 0, false);
        this.video.setWindow(getWindow());
        this.video.setPlayerStateCallbacks(this);
        this.video.setVideoSource(videoSlide, true, TAG);
        this.video.hideControls();
        this.video.loopForever();
    }

    private void displayImage(Uri uri) {
        this.video.setVisibility(8);
        this.image.setVisibility(0);
        this.duration.setVisibility(8);
        GlideApp.with((FragmentActivity) this).load((Object) new DecryptableStreamUriLoader.DecryptableUri(uri)).into(this.image);
    }

    private void scheduleDurationUpdate() {
        this.handler.postDelayed(this.durationUpdateRunnable, 100);
    }

    private void cancelDurationUpdate() {
        this.handler.removeCallbacks(this.durationUpdateRunnable);
    }

    /* loaded from: classes4.dex */
    private class ViewOnceGestureListener extends GestureDetector.SimpleOnGestureListener {
        private final View view;

        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
        public boolean onDown(MotionEvent motionEvent) {
            return true;
        }

        private ViewOnceGestureListener(View view) {
            ViewOnceMessageActivity.this = r1;
            this.view = view;
        }

        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnDoubleTapListener
        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            this.view.performClick();
            return true;
        }

        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
        public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            ViewOnceMessageActivity.this.finish();
            return true;
        }
    }
}
