package org.thoughtcrime.securesms.revealable;

import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;

/* loaded from: classes4.dex */
public class ViewOnceUtil {
    public static final long MAX_LIFESPAN = TimeUnit.DAYS.toMillis(30);

    public static boolean isViewable(MmsMessageRecord mmsMessageRecord) {
        if (!mmsMessageRecord.isViewOnce()) {
            return true;
        }
        if (!mmsMessageRecord.isOutgoing() && mmsMessageRecord.getSlideDeck().getThumbnailSlide() != null && mmsMessageRecord.getSlideDeck().getThumbnailSlide().getUri() != null && mmsMessageRecord.getSlideDeck().getThumbnailSlide().getTransferState() == 0 && !isViewed(mmsMessageRecord)) {
            return true;
        }
        return false;
    }

    public static boolean isViewed(MmsMessageRecord mmsMessageRecord) {
        if (!mmsMessageRecord.isViewOnce()) {
            return false;
        }
        if (mmsMessageRecord.getDateReceived() + MAX_LIFESPAN <= System.currentTimeMillis()) {
            return true;
        }
        if (mmsMessageRecord.getSlideDeck().getThumbnailSlide() != null && mmsMessageRecord.getSlideDeck().getThumbnailSlide().getTransferState() != 0) {
            return false;
        }
        if (mmsMessageRecord.getSlideDeck().getThumbnailSlide() == null || mmsMessageRecord.getSlideDeck().getThumbnailSlide().getUri() == null || mmsMessageRecord.isOutgoing()) {
            return true;
        }
        return false;
    }
}
