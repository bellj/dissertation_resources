package org.thoughtcrime.securesms.revealable;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.service.TimedEventManager;

/* loaded from: classes.dex */
public class ViewOnceMessageManager extends TimedEventManager<ViewOnceExpirationInfo> {
    private static final String TAG = Log.tag(ViewOnceMessageManager.class);
    private final AttachmentDatabase attachmentDatabase = SignalDatabase.attachments();
    private final MessageDatabase mmsDatabase = SignalDatabase.mms();

    public ViewOnceMessageManager(Application application) {
        super(application, "RevealableMessageManager");
        scheduleIfNecessary();
    }

    @Override // org.thoughtcrime.securesms.service.TimedEventManager
    public ViewOnceExpirationInfo getNextClosestEvent() {
        ViewOnceExpirationInfo nearestExpiringViewOnceMessage = this.mmsDatabase.getNearestExpiringViewOnceMessage();
        if (nearestExpiringViewOnceMessage != null) {
            String str = TAG;
            Log.i(str, "Next closest expiration is in " + getDelayForEvent(nearestExpiringViewOnceMessage) + " ms for messsage " + nearestExpiringViewOnceMessage.getMessageId() + ".");
        } else {
            Log.i(TAG, "No messages to schedule.");
        }
        return nearestExpiringViewOnceMessage;
    }

    public void executeEvent(ViewOnceExpirationInfo viewOnceExpirationInfo) {
        String str = TAG;
        Log.i(str, "Deleting attachments for message " + viewOnceExpirationInfo.getMessageId());
        this.attachmentDatabase.deleteAttachmentFilesForViewOnceMessage(viewOnceExpirationInfo.getMessageId());
    }

    public long getDelayForEvent(ViewOnceExpirationInfo viewOnceExpirationInfo) {
        return Math.max(0L, (viewOnceExpirationInfo.getReceiveTime() + ViewOnceUtil.MAX_LIFESPAN) - System.currentTimeMillis());
    }

    @Override // org.thoughtcrime.securesms.service.TimedEventManager
    protected void scheduleAlarm(Application application, long j) {
        TimedEventManager.setAlarm(application, j, ViewOnceAlarm.class);
    }

    /* loaded from: classes4.dex */
    public static class ViewOnceAlarm extends BroadcastReceiver {
        private static final String TAG = Log.tag(ViewOnceAlarm.class);

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive()");
            ApplicationDependencies.getViewOnceMessageManager().scheduleIfNecessary();
        }
    }
}
