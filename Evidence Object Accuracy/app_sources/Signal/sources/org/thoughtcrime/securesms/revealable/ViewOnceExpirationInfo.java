package org.thoughtcrime.securesms.revealable;

/* loaded from: classes4.dex */
public class ViewOnceExpirationInfo {
    private final long messageId;
    private final long receiveTime;

    public ViewOnceExpirationInfo(long j, long j2) {
        this.messageId = j;
        this.receiveTime = j2;
    }

    public long getMessageId() {
        return this.messageId;
    }

    public long getReceiveTime() {
        return this.receiveTime;
    }
}
