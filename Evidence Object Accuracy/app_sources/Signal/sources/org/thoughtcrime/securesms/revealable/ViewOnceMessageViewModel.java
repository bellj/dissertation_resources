package org.thoughtcrime.securesms.revealable;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import j$.util.Optional;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.revealable.ViewOnceMessageRepository;

/* loaded from: classes4.dex */
public class ViewOnceMessageViewModel extends ViewModel {
    private static final String TAG = Log.tag(ViewOnceMessageViewModel.class);
    private final MutableLiveData<Optional<MmsMessageRecord>> message;
    private final DatabaseObserver.Observer observer;

    private ViewOnceMessageViewModel(long j, ViewOnceMessageRepository viewOnceMessageRepository) {
        this.message = new MutableLiveData<>();
        this.observer = new DatabaseObserver.Observer(viewOnceMessageRepository, j) { // from class: org.thoughtcrime.securesms.revealable.ViewOnceMessageViewModel$$ExternalSyntheticLambda1
            public final /* synthetic */ ViewOnceMessageRepository f$1;
            public final /* synthetic */ long f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
            public final void onChanged() {
                ViewOnceMessageViewModel.this.lambda$new$0(this.f$1, this.f$2);
            }
        };
        viewOnceMessageRepository.getMessage(j, new ViewOnceMessageRepository.Callback() { // from class: org.thoughtcrime.securesms.revealable.ViewOnceMessageViewModel$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.revealable.ViewOnceMessageRepository.Callback
            public final void onComplete(Object obj) {
                ViewOnceMessageViewModel.this.lambda$new$1((Optional) obj);
            }
        });
    }

    public /* synthetic */ void lambda$new$0(ViewOnceMessageRepository viewOnceMessageRepository, long j) {
        viewOnceMessageRepository.getMessage(j, new ViewOnceMessageRepository.Callback() { // from class: org.thoughtcrime.securesms.revealable.ViewOnceMessageViewModel$$ExternalSyntheticLambda3
            @Override // org.thoughtcrime.securesms.revealable.ViewOnceMessageRepository.Callback
            public final void onComplete(Object obj) {
                ViewOnceMessageViewModel.this.onMessageRetrieved((Optional) obj);
            }
        });
    }

    public /* synthetic */ void lambda$new$1(Optional optional) {
        if (optional.isPresent()) {
            ApplicationDependencies.getDatabaseObserver().registerConversationObserver(((MmsMessageRecord) optional.get()).getThreadId(), this.observer);
        }
        onMessageRetrieved(optional);
    }

    public LiveData<Optional<MmsMessageRecord>> getMessage() {
        return this.message;
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(this.observer);
    }

    public void onMessageRetrieved(Optional<MmsMessageRecord> optional) {
        ThreadUtil.runOnMain(new Runnable(optional) { // from class: org.thoughtcrime.securesms.revealable.ViewOnceMessageViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ Optional f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ViewOnceMessageViewModel.this.lambda$onMessageRetrieved$2(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$onMessageRetrieved$2(Optional optional) {
        MmsMessageRecord orElse = this.message.getValue() != null ? this.message.getValue().orElse(null) : null;
        MmsMessageRecord mmsMessageRecord = (MmsMessageRecord) optional.orElse(null);
        if (orElse == null || mmsMessageRecord == null || orElse.getId() != mmsMessageRecord.getId()) {
            this.message.setValue(optional);
        } else {
            Log.d(TAG, "Same ID -- skipping update");
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final long messageId;
        private final ViewOnceMessageRepository repository;

        public Factory(long j, ViewOnceMessageRepository viewOnceMessageRepository) {
            this.messageId = j;
            this.repository = viewOnceMessageRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.NewInstanceFactory, androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new ViewOnceMessageViewModel(this.messageId, this.repository));
        }
    }
}
