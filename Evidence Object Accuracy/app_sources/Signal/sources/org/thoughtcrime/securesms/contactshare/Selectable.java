package org.thoughtcrime.securesms.contactshare;

/* loaded from: classes4.dex */
public interface Selectable {
    boolean isSelected();

    void setSelected(boolean z);
}
