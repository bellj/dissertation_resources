package org.thoughtcrime.securesms.contactshare;

import android.net.Uri;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import java.util.ArrayList;
import java.util.List;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.contactshare.SharedContactRepository;
import org.thoughtcrime.securesms.util.SingleLiveEvent;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public class ContactShareEditViewModel extends ViewModel {
    private final MutableLiveData<List<Contact>> contacts = new MutableLiveData<>();
    private final SingleLiveEvent<Event> events = new SingleLiveEvent<>();
    private final SharedContactRepository repo;

    /* loaded from: classes4.dex */
    public enum Event {
        BAD_CONTACT
    }

    ContactShareEditViewModel(List<Uri> list, SharedContactRepository sharedContactRepository) {
        this.repo = sharedContactRepository;
        sharedContactRepository.getContacts(list, new SharedContactRepository.ValueCallback() { // from class: org.thoughtcrime.securesms.contactshare.ContactShareEditViewModel$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.contactshare.SharedContactRepository.ValueCallback
            public final void onComplete(Object obj) {
                ContactShareEditViewModel.this.lambda$new$0((List) obj);
            }
        });
    }

    public /* synthetic */ void lambda$new$0(List list) {
        if (list.isEmpty()) {
            this.events.postValue(Event.BAD_CONTACT);
        } else {
            this.contacts.postValue(list);
        }
    }

    public LiveData<List<Contact>> getContacts() {
        return this.contacts;
    }

    public List<Contact> getFinalizedContacts() {
        List<Contact> currentContacts = getCurrentContacts();
        ArrayList arrayList = new ArrayList(currentContacts.size());
        for (Contact contact : currentContacts) {
            arrayList.add(new Contact(contact.getName(), contact.getOrganization(), trimSelectables(contact.getPhoneNumbers()), trimSelectables(contact.getEmails()), trimSelectables(contact.getPostalAddresses()), (contact.getAvatar() == null || !contact.getAvatar().isSelected()) ? null : contact.getAvatar()));
        }
        return arrayList;
    }

    public LiveData<Event> getEvents() {
        return this.events;
    }

    public void updateContactName(int i, Contact.Name name) {
        if (name.isEmpty()) {
            this.events.postValue(Event.BAD_CONTACT);
            return;
        }
        List<Contact> currentContacts = getCurrentContacts();
        Contact remove = currentContacts.remove(i);
        currentContacts.add(new Contact(name, remove.getOrganization(), remove.getPhoneNumbers(), remove.getEmails(), remove.getPostalAddresses(), remove.getAvatar()));
        this.contacts.postValue(currentContacts);
    }

    private <E extends Selectable> List<E> trimSelectables(List<E> list) {
        return Stream.of(list).filter(new Predicate() { // from class: org.thoughtcrime.securesms.contactshare.ContactShareEditViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ((Selectable) obj).isSelected();
            }
        }).toList();
    }

    private List<Contact> getCurrentContacts() {
        List<Contact> value = this.contacts.getValue();
        return value != null ? value : new ArrayList();
    }

    /* loaded from: classes4.dex */
    static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final SharedContactRepository contactRepository;
        private final List<Uri> contactUris;

        public Factory(List<Uri> list, SharedContactRepository sharedContactRepository) {
            this.contactUris = list;
            this.contactRepository = sharedContactRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.NewInstanceFactory, androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new ContactShareEditViewModel(this.contactUris, this.contactRepository));
        }
    }
}
