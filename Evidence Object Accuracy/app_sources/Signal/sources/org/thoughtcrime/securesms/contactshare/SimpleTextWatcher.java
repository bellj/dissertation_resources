package org.thoughtcrime.securesms.contactshare;

import android.text.Editable;
import android.text.TextWatcher;

/* loaded from: classes4.dex */
public abstract class SimpleTextWatcher implements TextWatcher {
    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public abstract void onTextChanged(String str);

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        onTextChanged(charSequence.toString());
    }
}
