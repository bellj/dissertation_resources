package org.thoughtcrime.securesms.contactshare;

import android.view.View;
import org.thoughtcrime.securesms.contactshare.ContactFieldAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ContactFieldAdapter$ContactFieldViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ContactFieldAdapter.ContactFieldViewHolder f$0;
    public final /* synthetic */ ContactFieldAdapter.Field f$1;

    public /* synthetic */ ContactFieldAdapter$ContactFieldViewHolder$$ExternalSyntheticLambda0(ContactFieldAdapter.ContactFieldViewHolder contactFieldViewHolder, ContactFieldAdapter.Field field) {
        this.f$0 = contactFieldViewHolder;
        this.f$1 = field;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$bind$0(this.f$1, view);
    }
}
