package org.thoughtcrime.securesms.contactshare;

import android.text.TextUtils;
import ezvcard.Ezvcard;
import ezvcard.VCard;
import ezvcard.property.Address;
import ezvcard.property.Email;
import ezvcard.property.StructuredName;
import ezvcard.property.Telephone;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contactshare.Contact;

/* loaded from: classes4.dex */
public final class VCardUtil {
    private static final String TAG = Log.tag(VCardUtil.class);

    private VCardUtil() {
    }

    public static List<Contact> parseContacts(String str) {
        List<VCard> all = Ezvcard.parse(str).all();
        LinkedList linkedList = new LinkedList();
        for (VCard vCard : all) {
            linkedList.add(getContactFromVcard(vCard));
        }
        return linkedList;
    }

    public static Contact getContactFromVcard(VCard vCard) {
        StructuredName structuredName = vCard.getStructuredName();
        List<Telephone> telephoneNumbers = vCard.getTelephoneNumbers();
        List<Email> emails = vCard.getEmails();
        List<Address> addresses = vCard.getAddresses();
        String str = (vCard.getOrganization() == null || vCard.getOrganization().getValues().isEmpty()) ? null : vCard.getOrganization().getValues().get(0);
        String value = vCard.getFormattedName() != null ? vCard.getFormattedName().getValue() : null;
        if (value == null && structuredName != null) {
            value = structuredName.getGiven();
        }
        String str2 = (value != null || vCard.getOrganization() == null) ? value : str;
        if (str2 == null) {
            Log.w(TAG, "Failed to parse the vcard: No valid name.");
            return null;
        }
        Contact.Name name = new Contact.Name(str2, structuredName != null ? structuredName.getGiven() : null, structuredName != null ? structuredName.getFamily() : null, (structuredName == null || structuredName.getPrefixes().isEmpty()) ? null : structuredName.getPrefixes().get(0), (structuredName == null || structuredName.getSuffixes().isEmpty()) ? null : structuredName.getSuffixes().get(0), null);
        ArrayList arrayList = new ArrayList(telephoneNumbers.size());
        for (Telephone telephone : telephoneNumbers) {
            String cleanedVcardType = !telephone.getTypes().isEmpty() ? getCleanedVcardType(telephone.getTypes().get(0).getValue()) : null;
            String text = telephone.getText();
            if (text == null) {
                text = telephone.getUri().getNumber();
            }
            arrayList.add(new Contact.Phone(text, phoneTypeFromVcardType(cleanedVcardType), cleanedVcardType));
        }
        ArrayList arrayList2 = new ArrayList(emails.size());
        for (Email email : emails) {
            String cleanedVcardType2 = !email.getTypes().isEmpty() ? getCleanedVcardType(email.getTypes().get(0).getValue()) : null;
            arrayList2.add(new Contact.Email(email.getValue(), emailTypeFromVcardType(cleanedVcardType2), cleanedVcardType2));
        }
        ArrayList arrayList3 = new ArrayList(addresses.size());
        for (Address address : addresses) {
            String cleanedVcardType3 = !address.getTypes().isEmpty() ? getCleanedVcardType(address.getTypes().get(0).getValue()) : null;
            arrayList3.add(new Contact.PostalAddress(postalAddressTypeFromVcardType(cleanedVcardType3), cleanedVcardType3, address.getStreetAddress(), address.getPoBox(), null, address.getLocality(), address.getRegion(), address.getPostalCode(), address.getCountry()));
        }
        return new Contact(name, str, arrayList, arrayList2, arrayList3, null);
    }

    public static Contact.Phone.Type phoneTypeFromContactType(int i) {
        if (i == 1) {
            return Contact.Phone.Type.HOME;
        }
        if (i == 2) {
            return Contact.Phone.Type.MOBILE;
        }
        if (i != 3) {
            return Contact.Phone.Type.CUSTOM;
        }
        return Contact.Phone.Type.WORK;
    }

    private static Contact.Phone.Type phoneTypeFromVcardType(String str) {
        if ("home".equalsIgnoreCase(str)) {
            return Contact.Phone.Type.HOME;
        }
        if ("cell".equalsIgnoreCase(str)) {
            return Contact.Phone.Type.MOBILE;
        }
        if ("work".equalsIgnoreCase(str)) {
            return Contact.Phone.Type.WORK;
        }
        return Contact.Phone.Type.CUSTOM;
    }

    public static Contact.Email.Type emailTypeFromContactType(int i) {
        if (i == 1) {
            return Contact.Email.Type.HOME;
        }
        if (i == 2) {
            return Contact.Email.Type.WORK;
        }
        if (i != 4) {
            return Contact.Email.Type.CUSTOM;
        }
        return Contact.Email.Type.MOBILE;
    }

    private static Contact.Email.Type emailTypeFromVcardType(String str) {
        if ("home".equalsIgnoreCase(str)) {
            return Contact.Email.Type.HOME;
        }
        if ("cell".equalsIgnoreCase(str)) {
            return Contact.Email.Type.MOBILE;
        }
        if ("work".equalsIgnoreCase(str)) {
            return Contact.Email.Type.WORK;
        }
        return Contact.Email.Type.CUSTOM;
    }

    public static Contact.PostalAddress.Type postalAddressTypeFromContactType(int i) {
        if (i == 1) {
            return Contact.PostalAddress.Type.HOME;
        }
        if (i != 2) {
            return Contact.PostalAddress.Type.CUSTOM;
        }
        return Contact.PostalAddress.Type.WORK;
    }

    private static Contact.PostalAddress.Type postalAddressTypeFromVcardType(String str) {
        if ("home".equalsIgnoreCase(str)) {
            return Contact.PostalAddress.Type.HOME;
        }
        if ("work".equalsIgnoreCase(str)) {
            return Contact.PostalAddress.Type.WORK;
        }
        return Contact.PostalAddress.Type.CUSTOM;
    }

    private static String getCleanedVcardType(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        return (!str.startsWith("x-") || str.length() <= 2) ? str : str.substring(2);
    }
}
