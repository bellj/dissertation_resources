package org.thoughtcrime.securesms.contactshare;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import android.text.TextUtils;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiStrings;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.contactshare.ContactUtil;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.phonenumbers.PhoneNumberFormatter;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.SpanUtil;

/* loaded from: classes4.dex */
public final class ContactUtil {
    private static final String TAG = Log.tag(ContactUtil.class);

    /* loaded from: classes4.dex */
    public interface RecipientSelectedCallback {
        void onSelected(Recipient recipient);
    }

    public static long getContactIdFromUri(Uri uri) {
        try {
            return Long.parseLong(uri.getLastPathSegment());
        } catch (NumberFormatException unused) {
            return -1;
        }
    }

    public static CharSequence getStringSummary(Context context, Contact contact) {
        String displayName = getDisplayName(contact);
        if (!TextUtils.isEmpty(displayName)) {
            return context.getString(R.string.MessageNotifier_contact_message, EmojiStrings.BUST_IN_SILHOUETTE, displayName);
        }
        return SpanUtil.italic(context.getString(R.string.MessageNotifier_unknown_contact_message));
    }

    public static String getDisplayName(Contact contact) {
        if (contact == null) {
            return "";
        }
        if (!TextUtils.isEmpty(contact.getName().getDisplayName())) {
            return contact.getName().getDisplayName();
        }
        if (!TextUtils.isEmpty(contact.getOrganization())) {
            return contact.getOrganization();
        }
        return "";
    }

    public static String getDisplayNumber(Contact contact, Locale locale) {
        Contact.Phone primaryNumber = getPrimaryNumber(contact);
        if (primaryNumber != null) {
            return getPrettyPhoneNumber(primaryNumber, locale);
        }
        return contact.getEmails().size() > 0 ? contact.getEmails().get(0).getEmail() : "";
    }

    private static Contact.Phone getPrimaryNumber(Contact contact) {
        if (contact.getPhoneNumbers().size() == 0) {
            return null;
        }
        List list = Stream.of(contact.getPhoneNumbers()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.contactshare.ContactUtil$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ContactUtil.lambda$getPrimaryNumber$0((Contact.Phone) obj);
            }
        }).toList();
        if (list.size() > 0) {
            return (Contact.Phone) list.get(0);
        }
        return contact.getPhoneNumbers().get(0);
    }

    public static /* synthetic */ boolean lambda$getPrimaryNumber$0(Contact.Phone phone) {
        return phone.getType() == Contact.Phone.Type.MOBILE;
    }

    public static String getPrettyPhoneNumber(Contact.Phone phone, Locale locale) {
        return getPrettyPhoneNumber(phone.getNumber(), locale);
    }

    private static String getPrettyPhoneNumber(String str, Locale locale) {
        PhoneNumberUtil instance = PhoneNumberUtil.getInstance();
        try {
            return instance.format(instance.parse(str, locale.getCountry()), PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
        } catch (NumberParseException unused) {
            return str;
        }
    }

    public static String getNormalizedPhoneNumber(Context context, String str) {
        return PhoneNumberFormatter.get(context).format(str);
    }

    public static void selectRecipientThroughDialog(Context context, List<Recipient> list, Locale locale, RecipientSelectedCallback recipientSelectedCallback) {
        if (list.size() > 1) {
            int size = list.size();
            CharSequence[] charSequenceArr = new CharSequence[size];
            for (int i = 0; i < size; i++) {
                charSequenceArr[i] = getPrettyPhoneNumber(list.get(i).requireE164(), locale);
            }
            new MaterialAlertDialogBuilder(context).setItems(charSequenceArr, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(list) { // from class: org.thoughtcrime.securesms.contactshare.ContactUtil$$ExternalSyntheticLambda0
                public final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    ContactUtil.lambda$selectRecipientThroughDialog$1(ContactUtil.RecipientSelectedCallback.this, this.f$1, dialogInterface, i2);
                }
            }).create().show();
            return;
        }
        recipientSelectedCallback.onSelected(list.get(0));
    }

    public static /* synthetic */ void lambda$selectRecipientThroughDialog$1(RecipientSelectedCallback recipientSelectedCallback, List list, DialogInterface dialogInterface, int i) {
        recipientSelectedCallback.onSelected((Recipient) list.get(i));
    }

    public static List<RecipientId> getRecipients(Context context, Contact contact) {
        return Stream.of(contact.getPhoneNumbers()).map(new Function(context) { // from class: org.thoughtcrime.securesms.contactshare.ContactUtil$$ExternalSyntheticLambda2
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ContactUtil.lambda$getRecipients$2(this.f$0, (Contact.Phone) obj);
            }
        }).map(new ContactUtil$$ExternalSyntheticLambda3()).toList();
    }

    public static /* synthetic */ Recipient lambda$getRecipients$2(Context context, Contact.Phone phone) {
        return Recipient.external(context, phone.getNumber());
    }

    public static Intent buildAddToContactsIntent(Context context, Contact contact) {
        Intent intent = new Intent("android.intent.action.INSERT_OR_EDIT");
        intent.setType("vnd.android.cursor.item/contact");
        if (!TextUtils.isEmpty(contact.getName().getDisplayName())) {
            intent.putExtra("name", contact.getName().getDisplayName());
        }
        if (!TextUtils.isEmpty(contact.getOrganization())) {
            intent.putExtra("company", contact.getOrganization());
        }
        if (contact.getPhoneNumbers().size() > 0) {
            intent.putExtra(RecipientDatabase.PHONE, contact.getPhoneNumbers().get(0).getNumber());
            intent.putExtra("phone_type", getSystemType(contact.getPhoneNumbers().get(0).getType()));
        }
        if (contact.getPhoneNumbers().size() > 1) {
            intent.putExtra("secondary_phone", contact.getPhoneNumbers().get(1).getNumber());
            intent.putExtra("secondary_phone_type", getSystemType(contact.getPhoneNumbers().get(1).getType()));
        }
        if (contact.getPhoneNumbers().size() > 2) {
            intent.putExtra("tertiary_phone", contact.getPhoneNumbers().get(2).getNumber());
            intent.putExtra("tertiary_phone_type", getSystemType(contact.getPhoneNumbers().get(2).getType()));
        }
        if (contact.getEmails().size() > 0) {
            intent.putExtra(RecipientDatabase.EMAIL, contact.getEmails().get(0).getEmail());
            intent.putExtra("email_type", getSystemType(contact.getEmails().get(0).getType()));
        }
        if (contact.getEmails().size() > 1) {
            intent.putExtra("secondary_email", contact.getEmails().get(1).getEmail());
            intent.putExtra("secondary_email_type", getSystemType(contact.getEmails().get(1).getType()));
        }
        if (contact.getEmails().size() > 2) {
            intent.putExtra("tertiary_email", contact.getEmails().get(2).getEmail());
            intent.putExtra("tertiary_email_type", getSystemType(contact.getEmails().get(2).getType()));
        }
        if (contact.getPostalAddresses().size() > 0) {
            intent.putExtra("postal", contact.getPostalAddresses().get(0).toString());
            intent.putExtra("postal_type", getSystemType(contact.getPostalAddresses().get(0).getType()));
        }
        if (!(contact.getAvatarAttachment() == null || contact.getAvatarAttachment().getUri() == null)) {
            try {
                ContentValues contentValues = new ContentValues();
                contentValues.put("mimetype", "vnd.android.cursor.item/photo");
                contentValues.put("data15", StreamUtil.readFully(PartAuthority.getAttachmentStream(context, contact.getAvatarAttachment().getUri())));
                ArrayList<? extends Parcelable> arrayList = new ArrayList<>(1);
                arrayList.add(contentValues);
                intent.putParcelableArrayListExtra("data", arrayList);
            } catch (IOException e) {
                Log.w(TAG, "Failed to read avatar into a byte array.", e);
            }
        }
        return intent;
    }

    private static int getSystemType(Contact.Phone.Type type) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Phone$Type[type.ordinal()];
        int i2 = 1;
        if (i != 1) {
            i2 = 2;
            if (i != 2) {
                i2 = 3;
                if (i != 3) {
                    return 0;
                }
            }
        }
        return i2;
    }

    private static int getSystemType(Contact.Email.Type type) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Email$Type[type.ordinal()];
        if (i == 1) {
            return 1;
        }
        if (i != 2) {
            return i != 3 ? 0 : 2;
        }
        return 4;
    }

    /* renamed from: org.thoughtcrime.securesms.contactshare.ContactUtil$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Email$Type;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Phone$Type;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$PostalAddress$Type;

        static {
            int[] iArr = new int[Contact.PostalAddress.Type.values().length];
            $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$PostalAddress$Type = iArr;
            try {
                iArr[Contact.PostalAddress.Type.HOME.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$PostalAddress$Type[Contact.PostalAddress.Type.WORK.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            int[] iArr2 = new int[Contact.Email.Type.values().length];
            $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Email$Type = iArr2;
            try {
                iArr2[Contact.Email.Type.HOME.ordinal()] = 1;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Email$Type[Contact.Email.Type.MOBILE.ordinal()] = 2;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Email$Type[Contact.Email.Type.WORK.ordinal()] = 3;
            } catch (NoSuchFieldError unused5) {
            }
            int[] iArr3 = new int[Contact.Phone.Type.values().length];
            $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Phone$Type = iArr3;
            try {
                iArr3[Contact.Phone.Type.HOME.ordinal()] = 1;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Phone$Type[Contact.Phone.Type.MOBILE.ordinal()] = 2;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Phone$Type[Contact.Phone.Type.WORK.ordinal()] = 3;
            } catch (NoSuchFieldError unused8) {
            }
        }
    }

    private static int getSystemType(Contact.PostalAddress.Type type) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$PostalAddress$Type[type.ordinal()];
        int i2 = 1;
        if (i != 1) {
            i2 = 2;
            if (i != 2) {
                return 0;
            }
        }
        return i2;
    }
}
