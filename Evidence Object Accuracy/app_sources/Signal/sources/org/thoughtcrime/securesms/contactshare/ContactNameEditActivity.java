package org.thoughtcrime.securesms.contactshare;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProvider;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;

/* loaded from: classes4.dex */
public class ContactNameEditActivity extends PassphraseRequiredActivity {
    public static final String KEY_CONTACT_INDEX;
    public static final String KEY_NAME;
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();
    private ContactNameEditViewModel viewModel;

    public static Intent getIntent(Context context, Contact.Name name, int i) {
        Intent intent = new Intent(context, ContactNameEditActivity.class);
        intent.putExtra("name", name);
        intent.putExtra(KEY_CONTACT_INDEX, i);
        return intent;
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onPreCreate() {
        this.dynamicTheme.onCreate(this);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        super.onCreate(bundle, z);
        if (getIntent() != null) {
            Contact.Name name = (Contact.Name) getIntent().getParcelableExtra("name");
            if (name != null) {
                setContentView(R.layout.activity_contact_name_edit);
                initializeToolbar();
                initializeViews(name);
                ContactNameEditViewModel contactNameEditViewModel = (ContactNameEditViewModel) new ViewModelProvider(this).get(ContactNameEditViewModel.class);
                this.viewModel = contactNameEditViewModel;
                contactNameEditViewModel.setName(name);
                return;
            }
            throw new IllegalStateException("You must supply a name to this activity. Please use the #getIntent() method.");
        }
        throw new IllegalStateException("You must supply extras to this activity. Please use the #getIntent() method.");
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }

    private void initializeToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.contactshare.ContactNameEditActivity$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ContactNameEditActivity.$r8$lambda$zOVdOJeOqlF9DE2C3xtKsyVPh8g(ContactNameEditActivity.this, view);
            }
        });
    }

    public /* synthetic */ void lambda$initializeToolbar$0(View view) {
        Intent intent = new Intent();
        intent.putExtra("name", this.viewModel.getName());
        intent.putExtra(KEY_CONTACT_INDEX, getIntent().getIntExtra(KEY_CONTACT_INDEX, -1));
        setResult(-1, intent);
        finish();
    }

    private void initializeViews(Contact.Name name) {
        TextView textView = (TextView) findViewById(R.id.name_edit_given_name);
        TextView textView2 = (TextView) findViewById(R.id.name_edit_family_name);
        TextView textView3 = (TextView) findViewById(R.id.name_edit_middle_name);
        TextView textView4 = (TextView) findViewById(R.id.name_edit_prefix);
        TextView textView5 = (TextView) findViewById(R.id.name_edit_suffix);
        textView.setText(name.getGivenName());
        textView2.setText(name.getFamilyName());
        textView3.setText(name.getMiddleName());
        textView4.setText(name.getPrefix());
        textView5.setText(name.getSuffix());
        textView.addTextChangedListener(new SimpleTextWatcher() { // from class: org.thoughtcrime.securesms.contactshare.ContactNameEditActivity.1
            @Override // org.thoughtcrime.securesms.contactshare.SimpleTextWatcher
            public void onTextChanged(String str) {
                ContactNameEditActivity.this.viewModel.updateGivenName(str);
            }
        });
        textView2.addTextChangedListener(new SimpleTextWatcher() { // from class: org.thoughtcrime.securesms.contactshare.ContactNameEditActivity.2
            @Override // org.thoughtcrime.securesms.contactshare.SimpleTextWatcher
            public void onTextChanged(String str) {
                ContactNameEditActivity.this.viewModel.updateFamilyName(str);
            }
        });
        textView3.addTextChangedListener(new SimpleTextWatcher() { // from class: org.thoughtcrime.securesms.contactshare.ContactNameEditActivity.3
            @Override // org.thoughtcrime.securesms.contactshare.SimpleTextWatcher
            public void onTextChanged(String str) {
                ContactNameEditActivity.this.viewModel.updateMiddleName(str);
            }
        });
        textView4.addTextChangedListener(new SimpleTextWatcher() { // from class: org.thoughtcrime.securesms.contactshare.ContactNameEditActivity.4
            @Override // org.thoughtcrime.securesms.contactshare.SimpleTextWatcher
            public void onTextChanged(String str) {
                ContactNameEditActivity.this.viewModel.updatePrefix(str);
            }
        });
        textView5.addTextChangedListener(new SimpleTextWatcher() { // from class: org.thoughtcrime.securesms.contactshare.ContactNameEditActivity.5
            @Override // org.thoughtcrime.securesms.contactshare.SimpleTextWatcher
            public void onTextChanged(String str) {
                ContactNameEditActivity.this.viewModel.updateSuffix(str);
            }
        });
    }
}
