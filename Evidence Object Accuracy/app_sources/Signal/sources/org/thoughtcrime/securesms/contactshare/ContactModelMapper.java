package org.thoughtcrime.securesms.contactshare;

import j$.util.Optional;
import java.util.ArrayList;
import java.util.LinkedList;
import org.thoughtcrime.securesms.attachments.PointerAttachment;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.whispersystems.signalservice.api.messages.shared.SharedContact;

/* loaded from: classes4.dex */
public class ContactModelMapper {
    public static SharedContact.Builder localToRemoteBuilder(Contact contact) {
        ArrayList arrayList = new ArrayList(contact.getPhoneNumbers().size());
        ArrayList arrayList2 = new ArrayList(contact.getEmails().size());
        ArrayList arrayList3 = new ArrayList(contact.getPostalAddresses().size());
        for (Contact.Phone phone : contact.getPhoneNumbers()) {
            arrayList.add(new SharedContact.Phone.Builder().setValue(phone.getNumber()).setType(localToRemoteType(phone.getType())).setLabel(phone.getLabel()).build());
        }
        for (Contact.Email email : contact.getEmails()) {
            arrayList2.add(new SharedContact.Email.Builder().setValue(email.getEmail()).setType(localToRemoteType(email.getType())).setLabel(email.getLabel()).build());
        }
        for (Contact.PostalAddress postalAddress : contact.getPostalAddresses()) {
            arrayList3.add(new SharedContact.PostalAddress.Builder().setType(localToRemoteType(postalAddress.getType())).setLabel(postalAddress.getLabel()).setStreet(postalAddress.getStreet()).setPobox(postalAddress.getPoBox()).setNeighborhood(postalAddress.getNeighborhood()).setCity(postalAddress.getCity()).setRegion(postalAddress.getRegion()).setPostcode(postalAddress.getPostalCode()).setCountry(postalAddress.getCountry()).build());
        }
        return new SharedContact.Builder().setName(new SharedContact.Name.Builder().setDisplay(contact.getName().getDisplayName()).setGiven(contact.getName().getGivenName()).setFamily(contact.getName().getFamilyName()).setPrefix(contact.getName().getPrefix()).setSuffix(contact.getName().getSuffix()).setMiddle(contact.getName().getMiddleName()).build()).withOrganization(contact.getOrganization()).withPhones(arrayList).withEmails(arrayList2).withAddresses(arrayList3);
    }

    public static Contact remoteToLocal(SharedContact sharedContact) {
        Contact.Name name = new Contact.Name(sharedContact.getName().getDisplay().orElse(null), sharedContact.getName().getGiven().orElse(null), sharedContact.getName().getFamily().orElse(null), sharedContact.getName().getPrefix().orElse(null), sharedContact.getName().getSuffix().orElse(null), sharedContact.getName().getMiddle().orElse(null));
        LinkedList linkedList = new LinkedList();
        if (sharedContact.getPhone().isPresent()) {
            for (SharedContact.Phone phone : sharedContact.getPhone().get()) {
                linkedList.add(new Contact.Phone(phone.getValue(), remoteToLocalType(phone.getType()), phone.getLabel().orElse(null)));
            }
        }
        LinkedList linkedList2 = new LinkedList();
        if (sharedContact.getEmail().isPresent()) {
            for (SharedContact.Email email : sharedContact.getEmail().get()) {
                linkedList2.add(new Contact.Email(email.getValue(), remoteToLocalType(email.getType()), email.getLabel().orElse(null)));
            }
        }
        LinkedList linkedList3 = new LinkedList();
        if (sharedContact.getAddress().isPresent()) {
            for (SharedContact.PostalAddress postalAddress : sharedContact.getAddress().get()) {
                linkedList3.add(new Contact.PostalAddress(remoteToLocalType(postalAddress.getType()), postalAddress.getLabel().orElse(null), postalAddress.getStreet().orElse(null), postalAddress.getPobox().orElse(null), postalAddress.getNeighborhood().orElse(null), postalAddress.getCity().orElse(null), postalAddress.getRegion().orElse(null), postalAddress.getPostcode().orElse(null), postalAddress.getCountry().orElse(null)));
            }
        }
        return new Contact(name, sharedContact.getOrganization().orElse(null), linkedList, linkedList2, linkedList3, sharedContact.getAvatar().isPresent() ? new Contact.Avatar(null, PointerAttachment.forPointer(Optional.of(sharedContact.getAvatar().get().getAttachment().asPointer())).get(), sharedContact.getAvatar().get().isProfile()) : null);
    }

    private static Contact.Phone.Type remoteToLocalType(SharedContact.Phone.Type type) {
        int i = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$Phone$Type[type.ordinal()];
        if (i == 1) {
            return Contact.Phone.Type.HOME;
        }
        if (i == 2) {
            return Contact.Phone.Type.MOBILE;
        }
        if (i != 3) {
            return Contact.Phone.Type.CUSTOM;
        }
        return Contact.Phone.Type.WORK;
    }

    private static Contact.Email.Type remoteToLocalType(SharedContact.Email.Type type) {
        int i = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$Email$Type[type.ordinal()];
        if (i == 1) {
            return Contact.Email.Type.HOME;
        }
        if (i == 2) {
            return Contact.Email.Type.MOBILE;
        }
        if (i != 3) {
            return Contact.Email.Type.CUSTOM;
        }
        return Contact.Email.Type.WORK;
    }

    private static Contact.PostalAddress.Type remoteToLocalType(SharedContact.PostalAddress.Type type) {
        int i = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$PostalAddress$Type[type.ordinal()];
        if (i == 1) {
            return Contact.PostalAddress.Type.HOME;
        }
        if (i != 2) {
            return Contact.PostalAddress.Type.CUSTOM;
        }
        return Contact.PostalAddress.Type.WORK;
    }

    private static SharedContact.Phone.Type localToRemoteType(Contact.Phone.Type type) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Phone$Type[type.ordinal()];
        if (i == 1) {
            return SharedContact.Phone.Type.HOME;
        }
        if (i == 2) {
            return SharedContact.Phone.Type.MOBILE;
        }
        if (i != 3) {
            return SharedContact.Phone.Type.CUSTOM;
        }
        return SharedContact.Phone.Type.WORK;
    }

    private static SharedContact.Email.Type localToRemoteType(Contact.Email.Type type) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Email$Type[type.ordinal()];
        if (i == 1) {
            return SharedContact.Email.Type.HOME;
        }
        if (i == 2) {
            return SharedContact.Email.Type.MOBILE;
        }
        if (i != 3) {
            return SharedContact.Email.Type.CUSTOM;
        }
        return SharedContact.Email.Type.WORK;
    }

    /* renamed from: org.thoughtcrime.securesms.contactshare.ContactModelMapper$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Email$Type;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Phone$Type;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$PostalAddress$Type;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$Email$Type;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$Phone$Type;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$PostalAddress$Type;

        static {
            int[] iArr = new int[Contact.PostalAddress.Type.values().length];
            $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$PostalAddress$Type = iArr;
            try {
                iArr[Contact.PostalAddress.Type.HOME.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$PostalAddress$Type[Contact.PostalAddress.Type.WORK.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            int[] iArr2 = new int[Contact.Email.Type.values().length];
            $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Email$Type = iArr2;
            try {
                iArr2[Contact.Email.Type.HOME.ordinal()] = 1;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Email$Type[Contact.Email.Type.MOBILE.ordinal()] = 2;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Email$Type[Contact.Email.Type.WORK.ordinal()] = 3;
            } catch (NoSuchFieldError unused5) {
            }
            int[] iArr3 = new int[Contact.Phone.Type.values().length];
            $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Phone$Type = iArr3;
            try {
                iArr3[Contact.Phone.Type.HOME.ordinal()] = 1;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Phone$Type[Contact.Phone.Type.MOBILE.ordinal()] = 2;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Phone$Type[Contact.Phone.Type.WORK.ordinal()] = 3;
            } catch (NoSuchFieldError unused8) {
            }
            int[] iArr4 = new int[SharedContact.PostalAddress.Type.values().length];
            $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$PostalAddress$Type = iArr4;
            try {
                iArr4[SharedContact.PostalAddress.Type.HOME.ordinal()] = 1;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$PostalAddress$Type[SharedContact.PostalAddress.Type.WORK.ordinal()] = 2;
            } catch (NoSuchFieldError unused10) {
            }
            int[] iArr5 = new int[SharedContact.Email.Type.values().length];
            $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$Email$Type = iArr5;
            try {
                iArr5[SharedContact.Email.Type.HOME.ordinal()] = 1;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$Email$Type[SharedContact.Email.Type.MOBILE.ordinal()] = 2;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$Email$Type[SharedContact.Email.Type.WORK.ordinal()] = 3;
            } catch (NoSuchFieldError unused13) {
            }
            int[] iArr6 = new int[SharedContact.Phone.Type.values().length];
            $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$Phone$Type = iArr6;
            try {
                iArr6[SharedContact.Phone.Type.HOME.ordinal()] = 1;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$Phone$Type[SharedContact.Phone.Type.MOBILE.ordinal()] = 2;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$shared$SharedContact$Phone$Type[SharedContact.Phone.Type.WORK.ordinal()] = 3;
            } catch (NoSuchFieldError unused16) {
            }
        }
    }

    private static SharedContact.PostalAddress.Type localToRemoteType(Contact.PostalAddress.Type type) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$PostalAddress$Type[type.ordinal()];
        if (i == 1) {
            return SharedContact.PostalAddress.Type.HOME;
        }
        if (i != 2) {
            return SharedContact.PostalAddress.Type.CUSTOM;
        }
        return SharedContact.PostalAddress.Type.WORK;
    }
}
