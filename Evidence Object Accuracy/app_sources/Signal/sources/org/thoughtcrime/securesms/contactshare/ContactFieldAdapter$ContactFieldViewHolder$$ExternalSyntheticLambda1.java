package org.thoughtcrime.securesms.contactshare;

import android.view.View;
import org.thoughtcrime.securesms.contactshare.ContactFieldAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ContactFieldAdapter$ContactFieldViewHolder$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ ContactFieldAdapter.ContactFieldViewHolder f$0;

    public /* synthetic */ ContactFieldAdapter$ContactFieldViewHolder$$ExternalSyntheticLambda1(ContactFieldAdapter.ContactFieldViewHolder contactFieldViewHolder) {
        this.f$0 = contactFieldViewHolder;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$bind$1(view);
    }
}
