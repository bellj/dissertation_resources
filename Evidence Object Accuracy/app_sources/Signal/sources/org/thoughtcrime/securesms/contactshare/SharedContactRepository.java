package org.thoughtcrime.securesms.contactshare;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import ezvcard.Ezvcard;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executor;
import org.signal.contacts.SystemContactsRepository;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackPreparer$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.contacts.avatars.ContactPhoto;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.contactshare.SharedContactRepository;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.phonenumbers.PhoneNumberFormatter;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class SharedContactRepository {
    private static final String TAG = Log.tag(SharedContactRepository.class);
    private final Context context;
    private final Executor executor;

    /* loaded from: classes4.dex */
    public interface ValueCallback<T> {
        void onComplete(T t);
    }

    public SharedContactRepository(Context context, Executor executor) {
        this.context = context.getApplicationContext();
        this.executor = executor;
    }

    public void getContacts(List<Uri> list, ValueCallback<List<Contact>> valueCallback) {
        this.executor.execute(new Runnable(list, valueCallback) { // from class: org.thoughtcrime.securesms.contactshare.SharedContactRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ List f$1;
            public final /* synthetic */ SharedContactRepository.ValueCallback f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SharedContactRepository.this.lambda$getContacts$0(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$getContacts$0(List list, ValueCallback valueCallback) {
        Contact contact;
        ArrayList arrayList = new ArrayList(list.size());
        Iterator it = list.iterator();
        while (it.hasNext()) {
            Uri uri = (Uri) it.next();
            if ("com.android.contacts".equals(uri.getAuthority())) {
                contact = getContactFromSystemContacts(ContactUtil.getContactIdFromUri(uri));
            } else {
                contact = getContactFromVcard(uri);
            }
            if (contact != null) {
                arrayList.add(contact);
            }
        }
        valueCallback.onComplete(arrayList);
    }

    private Contact getContactFromSystemContacts(long j) {
        Contact.Name name = getName(j);
        Contact.Avatar avatar = null;
        if (name == null) {
            Log.w(TAG, "Couldn't find a name associated with the provided contact ID.");
            return null;
        }
        List<Contact.Phone> phoneNumbers = getPhoneNumbers(j);
        AvatarInfo avatarInfo = getAvatarInfo(j, phoneNumbers);
        if (avatarInfo != null) {
            avatar = new Contact.Avatar(avatarInfo.uri, avatarInfo.isProfile);
        }
        return new Contact(name, null, phoneNumbers, getEmails(j), getPostalAddresses(j), avatar);
    }

    private Contact getContactFromVcard(Uri uri) {
        Contact contact = null;
        try {
            InputStream attachmentStream = PartAuthority.getAttachmentStream(this.context, uri);
            contact = VCardUtil.getContactFromVcard(Ezvcard.parse(attachmentStream).first());
            if (attachmentStream != null) {
                attachmentStream.close();
            }
        } catch (IOException e) {
            Log.w(TAG, "Failed to parse the vcard.", e);
        }
        if (BlobProvider.AUTHORITY.equals(uri.getAuthority())) {
            BlobProvider.getInstance().delete(this.context, uri);
        }
        return contact;
    }

    private Contact.Name getName(long j) {
        SystemContactsRepository.NameDetails nameDetails = SystemContactsRepository.getNameDetails(this.context, j);
        if (nameDetails != null) {
            Contact.Name name = new Contact.Name(nameDetails.getDisplayName(), nameDetails.getGivenName(), nameDetails.getFamilyName(), nameDetails.getPrefix(), nameDetails.getSuffix(), nameDetails.getMiddleName());
            if (!name.isEmpty()) {
                return name;
            }
        }
        String organizationName = SystemContactsRepository.getOrganizationName(this.context, j);
        if (!TextUtils.isEmpty(organizationName)) {
            return new Contact.Name(organizationName, organizationName, null, null, null, null);
        }
        return null;
    }

    private List<Contact.Phone> getPhoneNumbers(long j) {
        HashMap hashMap = new HashMap();
        for (SystemContactsRepository.PhoneDetails phoneDetails : SystemContactsRepository.getPhoneDetails(this.context, j)) {
            String normalizedPhoneNumber = ContactUtil.getNormalizedPhoneNumber(this.context, phoneDetails.getNumber());
            Contact.Phone phone = (Contact.Phone) hashMap.get(normalizedPhoneNumber);
            Contact.Phone phone2 = new Contact.Phone(normalizedPhoneNumber, VCardUtil.phoneTypeFromContactType(phoneDetails.getType()), phoneDetails.getLabel());
            if (phone == null || (phone.getType() == Contact.Phone.Type.CUSTOM && phone.getLabel() == null)) {
                hashMap.put(normalizedPhoneNumber, phone2);
            }
        }
        ArrayList arrayList = new ArrayList(hashMap.size());
        arrayList.addAll(hashMap.values());
        return arrayList;
    }

    private List<Contact.Email> getEmails(long j) {
        return (List) Collection$EL.stream(SystemContactsRepository.getEmailDetails(this.context, j)).filter(new Predicate() { // from class: org.thoughtcrime.securesms.contactshare.SharedContactRepository$$ExternalSyntheticLambda2
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return VoiceNotePlaybackPreparer$$ExternalSyntheticBackport0.m((SystemContactsRepository.EmailDetails) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.contactshare.SharedContactRepository$$ExternalSyntheticLambda3
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return SharedContactRepository.lambda$getEmails$1((SystemContactsRepository.EmailDetails) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList());
    }

    public static /* synthetic */ Contact.Email lambda$getEmails$1(SystemContactsRepository.EmailDetails emailDetails) {
        String address = emailDetails.getAddress();
        Objects.requireNonNull(address);
        return new Contact.Email(address, VCardUtil.emailTypeFromContactType(emailDetails.getType()), emailDetails.getLabel());
    }

    private List<Contact.PostalAddress> getPostalAddresses(long j) {
        return (List) Collection$EL.stream(SystemContactsRepository.getPostalAddressDetails(this.context, j)).map(new Function() { // from class: org.thoughtcrime.securesms.contactshare.SharedContactRepository$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return SharedContactRepository.lambda$getPostalAddresses$2((SystemContactsRepository.PostalAddressDetails) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList());
    }

    public static /* synthetic */ Contact.PostalAddress lambda$getPostalAddresses$2(SystemContactsRepository.PostalAddressDetails postalAddressDetails) {
        return new Contact.PostalAddress(VCardUtil.postalAddressTypeFromContactType(postalAddressDetails.getType()), postalAddressDetails.getLabel(), postalAddressDetails.getStreet(), postalAddressDetails.getPoBox(), postalAddressDetails.getNeighborhood(), postalAddressDetails.getCity(), postalAddressDetails.getRegion(), postalAddressDetails.getPostal(), postalAddressDetails.getCountry());
    }

    private AvatarInfo getAvatarInfo(long j, List<Contact.Phone> list) {
        AvatarInfo systemAvatarInfo = getSystemAvatarInfo(j);
        if (systemAvatarInfo != null) {
            return systemAvatarInfo;
        }
        for (Contact.Phone phone : list) {
            AvatarInfo recipientAvatarInfo = getRecipientAvatarInfo(PhoneNumberFormatter.get(this.context).format(phone.getNumber()));
            if (recipientAvatarInfo != null) {
                return recipientAvatarInfo;
            }
        }
        return null;
    }

    private AvatarInfo getSystemAvatarInfo(long j) {
        Uri avatarUri = SystemContactsRepository.getAvatarUri(this.context, j);
        if (avatarUri != null) {
            return new AvatarInfo(avatarUri, false);
        }
        return null;
    }

    private AvatarInfo getRecipientAvatarInfo(String str) {
        Uri uri;
        ContactPhoto contactPhoto = Recipient.external(this.context, str).getContactPhoto();
        if (contactPhoto == null || (uri = contactPhoto.getUri(this.context)) == null) {
            return null;
        }
        return new AvatarInfo(uri, contactPhoto.isProfilePhoto());
    }

    /* loaded from: classes4.dex */
    public static class AvatarInfo {
        private final boolean isProfile;
        private final Uri uri;

        private AvatarInfo(Uri uri, boolean z) {
            this.uri = uri;
            this.isProfile = z;
        }

        public Uri getUri() {
            return this.uri;
        }

        public boolean isProfile() {
            return this.isProfile;
        }
    }
}
