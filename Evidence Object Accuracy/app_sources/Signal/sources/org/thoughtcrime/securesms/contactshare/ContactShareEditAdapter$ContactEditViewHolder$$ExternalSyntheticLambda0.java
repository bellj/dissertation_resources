package org.thoughtcrime.securesms.contactshare;

import android.view.View;
import org.thoughtcrime.securesms.contactshare.ContactShareEditAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ContactShareEditAdapter$ContactEditViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ContactShareEditAdapter.EventListener f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ Contact f$2;

    public /* synthetic */ ContactShareEditAdapter$ContactEditViewHolder$$ExternalSyntheticLambda0(ContactShareEditAdapter.EventListener eventListener, int i, Contact contact) {
        this.f$0 = eventListener;
        this.f$1 = i;
        this.f$2 = contact;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        ContactShareEditAdapter.ContactEditViewHolder.lambda$bind$0(this.f$0, this.f$1, this.f$2, view);
    }
}
