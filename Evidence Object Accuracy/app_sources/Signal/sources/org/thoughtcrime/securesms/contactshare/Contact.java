package org.thoughtcrime.securesms.contactshare;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.AttachmentId;
import org.thoughtcrime.securesms.attachments.UriAttachment;
import org.thoughtcrime.securesms.util.JsonUtils;
import org.thoughtcrime.securesms.util.MediaUtil;

/* loaded from: classes.dex */
public class Contact implements Parcelable {
    public static final Parcelable.Creator<Contact> CREATOR = new Parcelable.Creator<Contact>() { // from class: org.thoughtcrime.securesms.contactshare.Contact.1
        @Override // android.os.Parcelable.Creator
        public Contact createFromParcel(Parcel parcel) {
            return new Contact(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public Contact[] newArray(int i) {
            return new Contact[i];
        }
    };
    @JsonProperty
    private final Avatar avatar;
    @JsonProperty
    private final List<Email> emails;
    @JsonProperty
    private final Name name;
    @JsonProperty
    private final String organization;
    @JsonProperty
    private final List<Phone> phoneNumbers;
    @JsonProperty
    private final List<PostalAddress> postalAddresses;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public Contact(@JsonProperty("name") Name name, @JsonProperty("organization") String str, @JsonProperty("phoneNumbers") List<Phone> list, @JsonProperty("emails") List<Email> list2, @JsonProperty("postalAddresses") List<PostalAddress> list3, @JsonProperty("avatar") Avatar avatar) {
        this.name = name;
        this.organization = str;
        this.phoneNumbers = Collections.unmodifiableList(list);
        this.emails = Collections.unmodifiableList(list2);
        this.postalAddresses = Collections.unmodifiableList(list3);
        this.avatar = avatar;
    }

    public Contact(Contact contact, Avatar avatar) {
        this(contact.getName(), contact.getOrganization(), contact.getPhoneNumbers(), contact.getEmails(), contact.getPostalAddresses(), avatar);
    }

    private Contact(Parcel parcel) {
        this((Name) parcel.readParcelable(Name.class.getClassLoader()), parcel.readString(), parcel.createTypedArrayList(Phone.CREATOR), parcel.createTypedArrayList(Email.CREATOR), parcel.createTypedArrayList(PostalAddress.CREATOR), (Avatar) parcel.readParcelable(Avatar.class.getClassLoader()));
    }

    public Name getName() {
        return this.name;
    }

    public String getOrganization() {
        return this.organization;
    }

    public List<Phone> getPhoneNumbers() {
        return this.phoneNumbers;
    }

    public List<Email> getEmails() {
        return this.emails;
    }

    public List<PostalAddress> getPostalAddresses() {
        return this.postalAddresses;
    }

    public Avatar getAvatar() {
        return this.avatar;
    }

    @JsonIgnore
    public Attachment getAvatarAttachment() {
        Avatar avatar = this.avatar;
        if (avatar != null) {
            return avatar.getAttachment();
        }
        return null;
    }

    public String serialize() throws IOException {
        return JsonUtils.toJson(this);
    }

    public static Contact deserialize(String str) throws IOException {
        return (Contact) JsonUtils.fromJson(str, Contact.class);
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.name, i);
        parcel.writeString(this.organization);
        parcel.writeTypedList(this.phoneNumbers);
        parcel.writeTypedList(this.emails);
        parcel.writeTypedList(this.postalAddresses);
        parcel.writeParcelable(this.avatar, i);
    }

    /* loaded from: classes.dex */
    public static class Name implements Parcelable {
        public static final Parcelable.Creator<Name> CREATOR = new Parcelable.Creator<Name>() { // from class: org.thoughtcrime.securesms.contactshare.Contact.Name.1
            @Override // android.os.Parcelable.Creator
            public Name createFromParcel(Parcel parcel) {
                return new Name(parcel);
            }

            @Override // android.os.Parcelable.Creator
            public Name[] newArray(int i) {
                return new Name[i];
            }
        };
        @JsonProperty
        private final String displayName;
        @JsonProperty
        private final String familyName;
        @JsonProperty
        private final String givenName;
        @JsonProperty
        private final String middleName;
        @JsonProperty
        private final String prefix;
        @JsonProperty
        private final String suffix;

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        public Name(@JsonProperty("displayName") String str, @JsonProperty("givenName") String str2, @JsonProperty("familyName") String str3, @JsonProperty("prefix") String str4, @JsonProperty("suffix") String str5, @JsonProperty("middleName") String str6) {
            this.displayName = str;
            this.givenName = str2;
            this.familyName = str3;
            this.prefix = str4;
            this.suffix = str5;
            this.middleName = str6;
        }

        private Name(Parcel parcel) {
            this(parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString());
        }

        public String getDisplayName() {
            return this.displayName;
        }

        public String getGivenName() {
            return this.givenName;
        }

        public String getFamilyName() {
            return this.familyName;
        }

        public String getPrefix() {
            return this.prefix;
        }

        public String getSuffix() {
            return this.suffix;
        }

        public String getMiddleName() {
            return this.middleName;
        }

        public boolean isEmpty() {
            return TextUtils.isEmpty(this.displayName) && TextUtils.isEmpty(this.givenName) && TextUtils.isEmpty(this.familyName) && TextUtils.isEmpty(this.prefix) && TextUtils.isEmpty(this.suffix) && TextUtils.isEmpty(this.middleName);
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.displayName);
            parcel.writeString(this.givenName);
            parcel.writeString(this.familyName);
            parcel.writeString(this.prefix);
            parcel.writeString(this.suffix);
            parcel.writeString(this.middleName);
        }
    }

    /* loaded from: classes.dex */
    public static class Phone implements Selectable, Parcelable {
        public static final Parcelable.Creator<Phone> CREATOR = new Parcelable.Creator<Phone>() { // from class: org.thoughtcrime.securesms.contactshare.Contact.Phone.1
            @Override // android.os.Parcelable.Creator
            public Phone createFromParcel(Parcel parcel) {
                return new Phone(parcel);
            }

            @Override // android.os.Parcelable.Creator
            public Phone[] newArray(int i) {
                return new Phone[i];
            }
        };
        @JsonProperty
        private final String label;
        @JsonProperty
        private final String number;
        @JsonIgnore
        private boolean selected;
        @JsonProperty
        private final Type type;

        /* loaded from: classes4.dex */
        public enum Type {
            HOME,
            MOBILE,
            WORK,
            CUSTOM
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        public Phone(@JsonProperty("number") String str, @JsonProperty("type") Type type, @JsonProperty("label") String str2) {
            this.number = str;
            this.type = type;
            this.label = str2;
            this.selected = true;
        }

        private Phone(Parcel parcel) {
            this(parcel.readString(), Type.valueOf(parcel.readString()), parcel.readString());
        }

        public String getNumber() {
            return this.number;
        }

        public Type getType() {
            return this.type;
        }

        public String getLabel() {
            return this.label;
        }

        @Override // org.thoughtcrime.securesms.contactshare.Selectable
        public void setSelected(boolean z) {
            this.selected = z;
        }

        @Override // org.thoughtcrime.securesms.contactshare.Selectable
        public boolean isSelected() {
            return this.selected;
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.number);
            parcel.writeString(this.type.name());
            parcel.writeString(this.label);
        }
    }

    /* loaded from: classes.dex */
    public static class Email implements Selectable, Parcelable {
        public static final Parcelable.Creator<Email> CREATOR = new Parcelable.Creator<Email>() { // from class: org.thoughtcrime.securesms.contactshare.Contact.Email.1
            @Override // android.os.Parcelable.Creator
            public Email createFromParcel(Parcel parcel) {
                return new Email(parcel);
            }

            @Override // android.os.Parcelable.Creator
            public Email[] newArray(int i) {
                return new Email[i];
            }
        };
        @JsonProperty
        private final String email;
        @JsonProperty
        private final String label;
        @JsonIgnore
        private boolean selected;
        @JsonProperty
        private final Type type;

        /* loaded from: classes4.dex */
        public enum Type {
            HOME,
            MOBILE,
            WORK,
            CUSTOM
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        public Email(@JsonProperty("email") String str, @JsonProperty("type") Type type, @JsonProperty("label") String str2) {
            this.email = str;
            this.type = type;
            this.label = str2;
            this.selected = true;
        }

        private Email(Parcel parcel) {
            this(parcel.readString(), Type.valueOf(parcel.readString()), parcel.readString());
        }

        public String getEmail() {
            return this.email;
        }

        public Type getType() {
            return this.type;
        }

        public String getLabel() {
            return this.label;
        }

        @Override // org.thoughtcrime.securesms.contactshare.Selectable
        public void setSelected(boolean z) {
            this.selected = z;
        }

        @Override // org.thoughtcrime.securesms.contactshare.Selectable
        public boolean isSelected() {
            return this.selected;
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.email);
            parcel.writeString(this.type.name());
            parcel.writeString(this.label);
        }
    }

    /* loaded from: classes.dex */
    public static class PostalAddress implements Selectable, Parcelable {
        public static final Parcelable.Creator<PostalAddress> CREATOR = new Parcelable.Creator<PostalAddress>() { // from class: org.thoughtcrime.securesms.contactshare.Contact.PostalAddress.1
            @Override // android.os.Parcelable.Creator
            public PostalAddress createFromParcel(Parcel parcel) {
                return new PostalAddress(parcel);
            }

            @Override // android.os.Parcelable.Creator
            public PostalAddress[] newArray(int i) {
                return new PostalAddress[i];
            }
        };
        @JsonProperty
        private final String city;
        @JsonProperty
        private final String country;
        @JsonProperty
        private final String label;
        @JsonProperty
        private final String neighborhood;
        @JsonProperty
        private final String poBox;
        @JsonProperty
        private final String postalCode;
        @JsonProperty
        private final String region;
        @JsonIgnore
        private boolean selected;
        @JsonProperty
        private final String street;
        @JsonProperty
        private final Type type;

        /* loaded from: classes4.dex */
        public enum Type {
            HOME,
            WORK,
            CUSTOM
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        public PostalAddress(@JsonProperty("type") Type type, @JsonProperty("label") String str, @JsonProperty("street") String str2, @JsonProperty("poBox") String str3, @JsonProperty("neighborhood") String str4, @JsonProperty("city") String str5, @JsonProperty("region") String str6, @JsonProperty("postalCode") String str7, @JsonProperty("country") String str8) {
            this.type = type;
            this.label = str;
            this.street = str2;
            this.poBox = str3;
            this.neighborhood = str4;
            this.city = str5;
            this.region = str6;
            this.postalCode = str7;
            this.country = str8;
            this.selected = true;
        }

        private PostalAddress(Parcel parcel) {
            this(Type.valueOf(parcel.readString()), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString(), parcel.readString());
        }

        public Type getType() {
            return this.type;
        }

        public String getLabel() {
            return this.label;
        }

        public String getStreet() {
            return this.street;
        }

        public String getPoBox() {
            return this.poBox;
        }

        public String getNeighborhood() {
            return this.neighborhood;
        }

        public String getCity() {
            return this.city;
        }

        public String getRegion() {
            return this.region;
        }

        public String getPostalCode() {
            return this.postalCode;
        }

        public String getCountry() {
            return this.country;
        }

        @Override // org.thoughtcrime.securesms.contactshare.Selectable
        public void setSelected(boolean z) {
            this.selected = z;
        }

        @Override // org.thoughtcrime.securesms.contactshare.Selectable
        public boolean isSelected() {
            return this.selected;
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.type.name());
            parcel.writeString(this.label);
            parcel.writeString(this.street);
            parcel.writeString(this.poBox);
            parcel.writeString(this.neighborhood);
            parcel.writeString(this.city);
            parcel.writeString(this.region);
            parcel.writeString(this.postalCode);
            parcel.writeString(this.country);
        }

        @Override // java.lang.Object
        public String toString() {
            StringBuilder sb = new StringBuilder();
            if (!TextUtils.isEmpty(this.street)) {
                sb.append(this.street);
                sb.append('\n');
            }
            if (!TextUtils.isEmpty(this.poBox)) {
                sb.append(this.poBox);
                sb.append('\n');
            }
            if (!TextUtils.isEmpty(this.neighborhood)) {
                sb.append(this.neighborhood);
                sb.append('\n');
            }
            if (!TextUtils.isEmpty(this.city) && !TextUtils.isEmpty(this.region)) {
                sb.append(this.city);
                sb.append(", ");
                sb.append(this.region);
            } else if (!TextUtils.isEmpty(this.city)) {
                sb.append(this.city);
                sb.append(' ');
            } else if (!TextUtils.isEmpty(this.region)) {
                sb.append(this.region);
                sb.append(' ');
            }
            if (!TextUtils.isEmpty(this.postalCode)) {
                sb.append(this.postalCode);
            }
            if (!TextUtils.isEmpty(this.country)) {
                sb.append('\n');
                sb.append(this.country);
            }
            return sb.toString().trim();
        }
    }

    /* loaded from: classes.dex */
    public static class Avatar implements Selectable, Parcelable {
        public static final Parcelable.Creator<Avatar> CREATOR = new Parcelable.Creator<Avatar>() { // from class: org.thoughtcrime.securesms.contactshare.Contact.Avatar.1
            @Override // android.os.Parcelable.Creator
            public Avatar createFromParcel(Parcel parcel) {
                return new Avatar(parcel);
            }

            @Override // android.os.Parcelable.Creator
            public Avatar[] newArray(int i) {
                return new Avatar[i];
            }
        };
        @JsonIgnore
        private final Attachment attachment;
        @JsonProperty
        private final AttachmentId attachmentId;
        @JsonProperty
        private final boolean isProfile;
        @JsonIgnore
        private boolean selected;

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        public Avatar(AttachmentId attachmentId, Attachment attachment, boolean z) {
            this.attachmentId = attachmentId;
            this.attachment = attachment;
            this.isProfile = z;
            this.selected = true;
        }

        public Avatar(Uri uri, boolean z) {
            this(null, attachmentFromUri(uri), z);
        }

        @JsonCreator
        private Avatar(@JsonProperty("attachmentId") AttachmentId attachmentId, @JsonProperty("isProfile") boolean z) {
            this(attachmentId, null, z);
        }

        private Avatar(Parcel parcel) {
            this((Uri) parcel.readParcelable(Uri.class.getClassLoader()), parcel.readByte() != 0);
        }

        public AttachmentId getAttachmentId() {
            return this.attachmentId;
        }

        public Attachment getAttachment() {
            return this.attachment;
        }

        public boolean isProfile() {
            return this.isProfile;
        }

        @Override // org.thoughtcrime.securesms.contactshare.Selectable
        public void setSelected(boolean z) {
            this.selected = z;
        }

        @Override // org.thoughtcrime.securesms.contactshare.Selectable
        public boolean isSelected() {
            return this.selected;
        }

        private static Attachment attachmentFromUri(Uri uri) {
            if (uri == null) {
                return null;
            }
            return new UriAttachment(uri, MediaUtil.IMAGE_JPEG, 0, 0, null, false, false, false, false, null, null, null, null, null);
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            Attachment attachment = this.attachment;
            parcel.writeParcelable(attachment != null ? attachment.getUri() : null, i);
            parcel.writeByte(this.isProfile ? (byte) 1 : 0);
        }
    }
}
