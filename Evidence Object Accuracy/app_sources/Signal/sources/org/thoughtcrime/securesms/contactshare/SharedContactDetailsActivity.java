package org.thoughtcrime.securesms.contactshare;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contactshare.ContactUtil;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.DirectoryRefreshJob;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.recipients.LiveRecipient;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.DynamicLanguage;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.WindowUtil;

/* loaded from: classes4.dex */
public class SharedContactDetailsActivity extends PassphraseRequiredActivity {
    private static final int CODE_ADD_EDIT_CONTACT;
    private static final String KEY_CONTACT;
    private final Map<RecipientId, LiveRecipient> activeRecipients = new HashMap();
    private View addButtonView;
    private ImageView avatarView;
    private View callButtonView;
    private Contact contact;
    private ContactFieldAdapter contactFieldAdapter;
    private final DynamicLanguage dynamicLanguage = new DynamicLanguage();
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();
    private ViewGroup engageContainerView;
    private GlideRequests glideRequests;
    private View inviteButtonView;
    private View messageButtonView;
    private TextView nameView;
    private TextView numberView;

    public static Intent getIntent(Context context, Contact contact) {
        Intent intent = new Intent(context, SharedContactDetailsActivity.class);
        intent.putExtra(KEY_CONTACT, contact);
        return intent;
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onPreCreate() {
        this.dynamicTheme.onCreate(this);
        this.dynamicLanguage.onCreate(this);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        setContentView(R.layout.activity_shared_contact_details);
        if (getIntent() != null) {
            Contact contact = (Contact) getIntent().getParcelableExtra(KEY_CONTACT);
            this.contact = contact;
            if (contact != null) {
                initToolbar();
                initViews();
                presentContact(this.contact);
                presentActionButtons(ContactUtil.getRecipients(this, this.contact));
                presentAvatar(this.contact.getAvatarAttachment() != null ? this.contact.getAvatarAttachment().getUri() : null);
                for (LiveRecipient liveRecipient : this.activeRecipients.values()) {
                    liveRecipient.observe(this, new Observer() { // from class: org.thoughtcrime.securesms.contactshare.SharedContactDetailsActivity$$ExternalSyntheticLambda5
                        @Override // androidx.lifecycle.Observer
                        public final void onChanged(Object obj) {
                            SharedContactDetailsActivity.$r8$lambda$IGzF5qAHXm6RevditnMQpznXzzU(SharedContactDetailsActivity.this, (Recipient) obj);
                        }
                    });
                }
                return;
            }
            throw new IllegalStateException("You must supply a contact to this activity. Please use the #getIntent() method.");
        }
        throw new IllegalStateException("You must supply arguments to this activity. Please use the #getIntent() method.");
    }

    public /* synthetic */ void lambda$onCreate$0(Recipient recipient) {
        presentActionButtons(Collections.singletonList(recipient.getId()));
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onCreate(this);
        this.dynamicTheme.onResume(this);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setLogo(null);
        getSupportActionBar().setTitle("");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.contactshare.SharedContactDetailsActivity$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SharedContactDetailsActivity.$r8$lambda$PBx5XY4uNBXhRThFcIo3gXzDkYs(SharedContactDetailsActivity.this, view);
            }
        });
        WindowUtil.setStatusBarColor(getWindow(), ContextCompat.getColor(this, R.color.shared_contact_details_titlebar));
    }

    public /* synthetic */ void lambda$initToolbar$1(View view) {
        onBackPressed();
    }

    private void initViews() {
        this.nameView = (TextView) findViewById(R.id.contact_details_name);
        this.numberView = (TextView) findViewById(R.id.contact_details_number);
        this.avatarView = (ImageView) findViewById(R.id.contact_details_avatar);
        this.addButtonView = findViewById(R.id.contact_details_add_button);
        this.inviteButtonView = findViewById(R.id.contact_details_invite_button);
        this.engageContainerView = (ViewGroup) findViewById(R.id.contact_details_engage_container);
        this.messageButtonView = findViewById(R.id.contact_details_message_button);
        this.callButtonView = findViewById(R.id.contact_details_call_button);
        this.contactFieldAdapter = new ContactFieldAdapter(this.dynamicLanguage.getCurrentLocale(), this.glideRequests, false);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.contact_details_fields);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(this.contactFieldAdapter);
        this.glideRequests = GlideApp.with((FragmentActivity) this);
    }

    private void presentContact(Contact contact) {
        this.contact = contact;
        if (contact != null) {
            this.nameView.setText(ContactUtil.getDisplayName(contact));
            this.numberView.setText(ContactUtil.getDisplayNumber(contact, this.dynamicLanguage.getCurrentLocale()));
            this.addButtonView.setOnClickListener(new View.OnClickListener(contact) { // from class: org.thoughtcrime.securesms.contactshare.SharedContactDetailsActivity$$ExternalSyntheticLambda6
                public final /* synthetic */ Contact f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    SharedContactDetailsActivity.$r8$lambda$3U2RDj5_Hl9hBzXf1sCqLSt3apM(SharedContactDetailsActivity.this, this.f$1, view);
                }
            });
            this.contactFieldAdapter.setFields(this, null, contact.getPhoneNumbers(), contact.getEmails(), contact.getPostalAddresses());
            return;
        }
        this.nameView.setText("");
        this.numberView.setText("");
    }

    public /* synthetic */ void lambda$presentContact$2(final Contact contact, View view) {
        new AsyncTask<Void, Void, Intent>() { // from class: org.thoughtcrime.securesms.contactshare.SharedContactDetailsActivity.1
            public Intent doInBackground(Void... voidArr) {
                return ContactUtil.buildAddToContactsIntent(SharedContactDetailsActivity.this, contact);
            }

            public void onPostExecute(Intent intent) {
                SharedContactDetailsActivity.this.startActivityForResult(intent, SharedContactDetailsActivity.CODE_ADD_EDIT_CONTACT);
            }
        }.execute(new Void[0]);
    }

    public void presentAvatar(Uri uri) {
        if (uri != null) {
            this.glideRequests.load((Object) new DecryptableStreamUriLoader.DecryptableUri(uri)).fallback((int) R.drawable.ic_contact_picture).circleCrop().diskCacheStrategy(DiskCacheStrategy.ALL).into(this.avatarView);
        } else {
            this.glideRequests.load(Integer.valueOf((int) R.drawable.ic_contact_picture)).circleCrop().diskCacheStrategy(DiskCacheStrategy.ALL).into(this.avatarView);
        }
    }

    private void presentActionButtons(List<RecipientId> list) {
        for (RecipientId recipientId : list) {
            this.activeRecipients.put(recipientId, Recipient.live(recipientId));
        }
        ArrayList arrayList = new ArrayList(list.size());
        ArrayList arrayList2 = new ArrayList(list.size());
        for (LiveRecipient liveRecipient : this.activeRecipients.values()) {
            if (liveRecipient.get().getRegistered() == RecipientDatabase.RegisteredState.REGISTERED) {
                arrayList.add(liveRecipient.get());
            } else if (liveRecipient.get().isSystemContact()) {
                arrayList2.add(liveRecipient.get());
            }
        }
        if (!arrayList.isEmpty()) {
            this.engageContainerView.setVisibility(0);
            this.inviteButtonView.setVisibility(8);
            this.messageButtonView.setOnClickListener(new View.OnClickListener(arrayList) { // from class: org.thoughtcrime.securesms.contactshare.SharedContactDetailsActivity$$ExternalSyntheticLambda2
                public final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    SharedContactDetailsActivity.$r8$lambda$ywHsba6LcgKDbZB8Aibjodv8HWM(SharedContactDetailsActivity.this, this.f$1, view);
                }
            });
            this.callButtonView.setOnClickListener(new View.OnClickListener(arrayList) { // from class: org.thoughtcrime.securesms.contactshare.SharedContactDetailsActivity$$ExternalSyntheticLambda3
                public final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    SharedContactDetailsActivity.m1364$r8$lambda$sxouqkWxkq6x6K6ued2HqKf1vo(SharedContactDetailsActivity.this, this.f$1, view);
                }
            });
        } else if (!arrayList2.isEmpty()) {
            this.inviteButtonView.setVisibility(0);
            this.engageContainerView.setVisibility(8);
            this.inviteButtonView.setOnClickListener(new View.OnClickListener(arrayList2) { // from class: org.thoughtcrime.securesms.contactshare.SharedContactDetailsActivity$$ExternalSyntheticLambda4
                public final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    SharedContactDetailsActivity.m1363$r8$lambda$OnrKhfd1bnSvG32G7No9X0piLc(SharedContactDetailsActivity.this, this.f$1, view);
                }
            });
        } else {
            this.inviteButtonView.setVisibility(8);
            this.engageContainerView.setVisibility(8);
        }
    }

    public /* synthetic */ void lambda$presentActionButtons$4(List list, View view) {
        ContactUtil.selectRecipientThroughDialog(this, list, this.dynamicLanguage.getCurrentLocale(), new ContactUtil.RecipientSelectedCallback() { // from class: org.thoughtcrime.securesms.contactshare.SharedContactDetailsActivity$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.contactshare.ContactUtil.RecipientSelectedCallback
            public final void onSelected(Recipient recipient) {
                SharedContactDetailsActivity.$r8$lambda$6cy8Y_jkRIQU10_xzgwr4z3Wz9U(SharedContactDetailsActivity.this, recipient);
            }
        });
    }

    public /* synthetic */ void lambda$presentActionButtons$3(Recipient recipient) {
        CommunicationActions.startConversation(this, recipient, null);
    }

    public /* synthetic */ void lambda$presentActionButtons$5(Recipient recipient) {
        CommunicationActions.startVoiceCall(this, recipient);
    }

    public /* synthetic */ void lambda$presentActionButtons$6(List list, View view) {
        ContactUtil.selectRecipientThroughDialog(this, list, this.dynamicLanguage.getCurrentLocale(), new ContactUtil.RecipientSelectedCallback() { // from class: org.thoughtcrime.securesms.contactshare.SharedContactDetailsActivity$$ExternalSyntheticLambda8
            @Override // org.thoughtcrime.securesms.contactshare.ContactUtil.RecipientSelectedCallback
            public final void onSelected(Recipient recipient) {
                SharedContactDetailsActivity.$r8$lambda$Ps2GWJVmt_Ioa3WhqrosclOSacY(SharedContactDetailsActivity.this, recipient);
            }
        });
    }

    public /* synthetic */ void lambda$presentActionButtons$8(List list, View view) {
        ContactUtil.selectRecipientThroughDialog(this, list, this.dynamicLanguage.getCurrentLocale(), new ContactUtil.RecipientSelectedCallback() { // from class: org.thoughtcrime.securesms.contactshare.SharedContactDetailsActivity$$ExternalSyntheticLambda7
            @Override // org.thoughtcrime.securesms.contactshare.ContactUtil.RecipientSelectedCallback
            public final void onSelected(Recipient recipient) {
                SharedContactDetailsActivity.$r8$lambda$wFZ0cBVvhiUzpLEc118NilF9qZ4(SharedContactDetailsActivity.this, recipient);
            }
        });
    }

    public /* synthetic */ void lambda$presentActionButtons$7(Recipient recipient) {
        CommunicationActions.composeSmsThroughDefaultApp(this, recipient, getString(R.string.InviteActivity_lets_switch_to_signal, new Object[]{getString(R.string.install_url)}));
    }

    private void clearView() {
        this.nameView.setText("");
        this.numberView.setText("");
        this.inviteButtonView.setVisibility(8);
        this.engageContainerView.setVisibility(8);
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == CODE_ADD_EDIT_CONTACT && this.contact != null) {
            ApplicationDependencies.getJobManager().add(new DirectoryRefreshJob(false));
        }
    }
}
