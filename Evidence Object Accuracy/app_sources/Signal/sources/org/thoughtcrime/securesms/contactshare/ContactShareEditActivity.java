package org.thoughtcrime.securesms.contactshare;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.contactshare.ContactShareEditAdapter;
import org.thoughtcrime.securesms.contactshare.ContactShareEditViewModel;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.util.DynamicLanguage;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.Material3OnScrollHelper;

/* loaded from: classes4.dex */
public class ContactShareEditActivity extends PassphraseRequiredActivity implements ContactShareEditAdapter.EventListener {
    private static final int CODE_NAME_EDIT;
    public static final String KEY_CONTACTS;
    private static final String KEY_CONTACT_URIS;
    private static final String KEY_SEND_BUTTON_COLOR;
    private final DynamicLanguage dynamicLanguage = new DynamicLanguage();
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();
    private ContactShareEditViewModel viewModel;

    public static Intent getIntent(Context context, List<Uri> list, int i) {
        ArrayList<? extends Parcelable> arrayList = new ArrayList<>(list);
        Intent intent = new Intent(context, ContactShareEditActivity.class);
        intent.putParcelableArrayListExtra(KEY_CONTACT_URIS, arrayList);
        intent.putExtra(KEY_SEND_BUTTON_COLOR, i);
        return intent;
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onPreCreate() {
        this.dynamicTheme.onCreate(this);
        this.dynamicLanguage.onCreate(this);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        setContentView(R.layout.activity_contact_share_edit);
        if (getIntent() != null) {
            ArrayList parcelableArrayListExtra = getIntent().getParcelableArrayListExtra(KEY_CONTACT_URIS);
            if (parcelableArrayListExtra != null) {
                View findViewById = findViewById(R.id.contact_share_edit_send);
                ViewCompat.setBackgroundTintList(findViewById, ColorStateList.valueOf(getIntent().getIntExtra(KEY_SEND_BUTTON_COLOR, -65536)));
                findViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.contactshare.ContactShareEditActivity$$ExternalSyntheticLambda0
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        ContactShareEditActivity.$r8$lambda$c0gGyCFHpLrzvascEk3i369qE4o(ContactShareEditActivity.this, view);
                    }
                });
                RecyclerView recyclerView = (RecyclerView) findViewById(R.id.contact_share_edit_list);
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
                recyclerView.getLayoutManager().setAutoMeasureEnabled(true);
                Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
                toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.contactshare.ContactShareEditActivity$$ExternalSyntheticLambda1
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        ContactShareEditActivity.$r8$lambda$YmPba9JJt24KEsWNa26hxDvz5M0(ContactShareEditActivity.this, view);
                    }
                });
                new Material3OnScrollHelper(this, Collections.singletonList(toolbar), Collections.emptyList()).attach(recyclerView);
                ContactShareEditAdapter contactShareEditAdapter = new ContactShareEditAdapter(GlideApp.with((FragmentActivity) this), this.dynamicLanguage.getCurrentLocale(), this);
                recyclerView.setAdapter(contactShareEditAdapter);
                ContactShareEditViewModel contactShareEditViewModel = (ContactShareEditViewModel) ViewModelProviders.of(this, new ContactShareEditViewModel.Factory(parcelableArrayListExtra, new SharedContactRepository(this, AsyncTask.THREAD_POOL_EXECUTOR))).get(ContactShareEditViewModel.class);
                this.viewModel = contactShareEditViewModel;
                contactShareEditViewModel.getContacts().observe(this, new Observer(recyclerView) { // from class: org.thoughtcrime.securesms.contactshare.ContactShareEditActivity$$ExternalSyntheticLambda2
                    public final /* synthetic */ RecyclerView f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // androidx.lifecycle.Observer
                    public final void onChanged(Object obj) {
                        ContactShareEditActivity.m1358$r8$lambda$IGGXk5v8WNr4RlKfLTHeqipp80(ContactShareEditAdapter.this, this.f$1, (List) obj);
                    }
                });
                this.viewModel.getEvents().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.contactshare.ContactShareEditActivity$$ExternalSyntheticLambda3
                    @Override // androidx.lifecycle.Observer
                    public final void onChanged(Object obj) {
                        ContactShareEditActivity.$r8$lambda$KwJuOBNXaMNwF_YFjrXxGaK2VYQ(ContactShareEditActivity.this, (ContactShareEditViewModel.Event) obj);
                    }
                });
                return;
            }
            throw new IllegalStateException("You must supply contact Uri's to this activity. Please use the #getIntent() method.");
        }
        throw new IllegalStateException("You must supply extras to this activity. Please use the #getIntent() method.");
    }

    public /* synthetic */ void lambda$onCreate$0(View view) {
        onSendClicked(this.viewModel.getFinalizedContacts());
    }

    public /* synthetic */ void lambda$onCreate$1(View view) {
        onBackPressed();
    }

    public static /* synthetic */ void lambda$onCreate$3(ContactShareEditAdapter contactShareEditAdapter, RecyclerView recyclerView, List list) {
        contactShareEditAdapter.setContacts(list);
        recyclerView.post(new Runnable() { // from class: org.thoughtcrime.securesms.contactshare.ContactShareEditActivity$$ExternalSyntheticLambda4
            @Override // java.lang.Runnable
            public final void run() {
                ContactShareEditActivity.m1359$r8$lambda$csMtWoXPpw94Hq022Z8mgM7cj4(RecyclerView.this);
            }
        });
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
        this.dynamicTheme.onResume(this);
    }

    public void presentEvent(ContactShareEditViewModel.Event event) {
        if (event != null && event == ContactShareEditViewModel.Event.BAD_CONTACT) {
            Toast.makeText(this, (int) R.string.ContactShareEditActivity_invalid_contact, 0).show();
            finish();
        }
    }

    private void onSendClicked(List<Contact> list) {
        Intent intent = new Intent();
        ArrayList arrayList = new ArrayList(list.size());
        arrayList.addAll(list);
        intent.putExtra(KEY_CONTACTS, arrayList);
        setResult(-1, intent);
        finish();
    }

    @Override // org.thoughtcrime.securesms.contactshare.ContactShareEditAdapter.EventListener
    public void onNameEditClicked(int i, Contact.Name name) {
        startActivityForResult(ContactNameEditActivity.getIntent(this, name, i), 55);
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 55 && i2 == -1 && intent != null) {
            int intExtra = intent.getIntExtra(ContactNameEditActivity.KEY_CONTACT_INDEX, -1);
            Contact.Name name = (Contact.Name) intent.getParcelableExtra("name");
            if (name != null) {
                this.viewModel.updateContactName(intExtra, name);
            }
        }
    }
}
