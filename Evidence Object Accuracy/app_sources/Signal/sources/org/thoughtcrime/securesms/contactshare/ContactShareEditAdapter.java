package org.thoughtcrime.securesms.contactshare;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.mms.GlideRequests;

/* loaded from: classes4.dex */
public class ContactShareEditAdapter extends RecyclerView.Adapter<ContactEditViewHolder> {
    private final List<Contact> contacts = new ArrayList();
    private final EventListener eventListener;
    private final GlideRequests glideRequests;
    private final Locale locale;

    /* loaded from: classes4.dex */
    public interface EventListener {
        void onNameEditClicked(int i, Contact.Name name);
    }

    public ContactShareEditAdapter(GlideRequests glideRequests, Locale locale, EventListener eventListener) {
        this.glideRequests = glideRequests;
        this.locale = locale;
        this.eventListener = eventListener;
    }

    public ContactEditViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ContactEditViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_editable_contact, viewGroup, false), this.locale, this.glideRequests);
    }

    public void onBindViewHolder(ContactEditViewHolder contactEditViewHolder, int i) {
        contactEditViewHolder.bind(i, this.contacts.get(i), this.eventListener);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.contacts.size();
    }

    public void setContacts(List<Contact> list) {
        this.contacts.clear();
        if (list != null) {
            this.contacts.addAll(list);
        }
        notifyDataSetChanged();
    }

    /* loaded from: classes4.dex */
    public static class ContactEditViewHolder extends RecyclerView.ViewHolder {
        private final ContactFieldAdapter fieldAdapter;
        private final TextView name;
        private final View nameEditButton;

        ContactEditViewHolder(View view, Locale locale, GlideRequests glideRequests) {
            super(view);
            this.name = (TextView) view.findViewById(R.id.editable_contact_name);
            this.nameEditButton = view.findViewById(R.id.editable_contact_name_edit_button);
            ContactFieldAdapter contactFieldAdapter = new ContactFieldAdapter(locale, glideRequests, true);
            this.fieldAdapter = contactFieldAdapter;
            RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.editable_contact_fields);
            recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
            recyclerView.getLayoutManager().setAutoMeasureEnabled(true);
            recyclerView.setAdapter(contactFieldAdapter);
        }

        void bind(int i, Contact contact, EventListener eventListener) {
            Context context = this.itemView.getContext();
            this.name.setText(ContactUtil.getDisplayName(contact));
            this.nameEditButton.setOnClickListener(new ContactShareEditAdapter$ContactEditViewHolder$$ExternalSyntheticLambda0(eventListener, i, contact));
            this.fieldAdapter.setFields(context, contact.getAvatar(), contact.getPhoneNumbers(), contact.getEmails(), contact.getPostalAddresses());
        }

        public static /* synthetic */ void lambda$bind$0(EventListener eventListener, int i, Contact contact, View view) {
            eventListener.onNameEditClicked(i, contact.getName());
        }
    }
}
