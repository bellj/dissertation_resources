package org.thoughtcrime.securesms.contactshare;

import android.text.TextUtils;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.util.cjkv.CJKVUtil;

/* loaded from: classes4.dex */
public class ContactNameEditViewModel extends ViewModel {
    private final MutableLiveData<String> displayName = new MutableLiveData<>();
    private String familyName;
    private String givenName;
    private String middleName;
    private String prefix;
    private String suffix;

    public void setName(Contact.Name name) {
        this.givenName = name.getGivenName();
        this.familyName = name.getFamilyName();
        this.middleName = name.getMiddleName();
        this.prefix = name.getPrefix();
        this.suffix = name.getSuffix();
        this.displayName.postValue(buildDisplayName());
    }

    public Contact.Name getName() {
        return new Contact.Name(this.displayName.getValue(), this.givenName, this.familyName, this.prefix, this.suffix, this.middleName);
    }

    public void updateGivenName(String str) {
        this.givenName = str;
        this.displayName.postValue(buildDisplayName());
    }

    public void updateFamilyName(String str) {
        this.familyName = str;
        this.displayName.postValue(buildDisplayName());
    }

    public void updatePrefix(String str) {
        this.prefix = str;
        this.displayName.postValue(buildDisplayName());
    }

    public void updateSuffix(String str) {
        this.suffix = str;
        this.displayName.postValue(buildDisplayName());
    }

    public void updateMiddleName(String str) {
        this.middleName = str;
        this.displayName.postValue(buildDisplayName());
    }

    private String buildDisplayName() {
        if (CJKVUtil.isCJKV(this.givenName) && CJKVUtil.isCJKV(this.middleName) && CJKVUtil.isCJKV(this.familyName) && CJKVUtil.isCJKV(this.prefix) && CJKVUtil.isCJKV(this.suffix)) {
            return joinString(this.familyName, this.givenName, this.prefix, this.suffix, this.middleName);
        }
        return joinString(this.prefix, this.givenName, this.middleName, this.familyName, this.suffix);
    }

    private String joinString(String... strArr) {
        StringBuilder sb = new StringBuilder();
        for (String str : strArr) {
            if (!TextUtils.isEmpty(str)) {
                sb.append(str);
                sb.append(' ');
            }
        }
        return sb.toString().trim();
    }
}
