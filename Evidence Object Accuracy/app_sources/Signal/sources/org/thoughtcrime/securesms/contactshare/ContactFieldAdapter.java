package org.thoughtcrime.securesms.contactshare;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.mms.GlideRequests;

/* loaded from: classes4.dex */
public class ContactFieldAdapter extends RecyclerView.Adapter<ContactFieldViewHolder> {
    private final List<Field> fields = new ArrayList();
    private final GlideRequests glideRequests;
    private final Locale locale;
    private final boolean selectable;

    public ContactFieldAdapter(Locale locale, GlideRequests glideRequests, boolean z) {
        this.locale = locale;
        this.glideRequests = glideRequests;
        this.selectable = z;
    }

    public ContactFieldViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ContactFieldViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_selectable_contact_field, viewGroup, false));
    }

    public void onBindViewHolder(ContactFieldViewHolder contactFieldViewHolder, int i) {
        contactFieldViewHolder.bind(this.fields.get(i), this.glideRequests, this.selectable);
    }

    public void onViewRecycled(ContactFieldViewHolder contactFieldViewHolder) {
        contactFieldViewHolder.recycle();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.fields.size();
    }

    public void setFields(Context context, Contact.Avatar avatar, List<Contact.Phone> list, List<Contact.Email> list2, List<Contact.PostalAddress> list3) {
        this.fields.clear();
        if (avatar != null) {
            this.fields.add(new Field(avatar));
        }
        this.fields.addAll(Stream.of(list).map(new Function(context) { // from class: org.thoughtcrime.securesms.contactshare.ContactFieldAdapter$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$1;

            {
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ContactFieldAdapter.$r8$lambda$5ZyE9_pE_ApoyEqHiL_tv812nK0(ContactFieldAdapter.this, this.f$1, (Contact.Phone) obj);
            }
        }).toList());
        this.fields.addAll(Stream.of(list2).map(new Function(context) { // from class: org.thoughtcrime.securesms.contactshare.ContactFieldAdapter$$ExternalSyntheticLambda1
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ContactFieldAdapter.m1356$r8$lambda$qKYwc2Tlw41v2JHxcVe0mWwItc(this.f$0, (Contact.Email) obj);
            }
        }).toList());
        this.fields.addAll(Stream.of(list3).map(new Function(context) { // from class: org.thoughtcrime.securesms.contactshare.ContactFieldAdapter$$ExternalSyntheticLambda2
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ContactFieldAdapter.$r8$lambda$W4ACuU3bMGhSMJdUAqX9sZdKnEA(this.f$0, (Contact.PostalAddress) obj);
            }
        }).toList());
        notifyDataSetChanged();
    }

    public /* synthetic */ Field lambda$setFields$0(Context context, Contact.Phone phone) {
        return new Field(context, phone, this.locale);
    }

    public static /* synthetic */ Field lambda$setFields$1(Context context, Contact.Email email) {
        return new Field(context, email);
    }

    public static /* synthetic */ Field lambda$setFields$2(Context context, Contact.PostalAddress postalAddress) {
        return new Field(context, postalAddress);
    }

    /* loaded from: classes4.dex */
    public static class ContactFieldViewHolder extends RecyclerView.ViewHolder {
        private final ImageView avatar;
        private final CheckBox checkBox;
        private final TextView label;
        private final TextView value;

        ContactFieldViewHolder(View view) {
            super(view);
            this.value = (TextView) view.findViewById(R.id.contact_field_value);
            this.label = (TextView) view.findViewById(R.id.contact_field_label);
            this.avatar = (ImageView) view.findViewById(R.id.contact_field_avatar);
            this.checkBox = (CheckBox) view.findViewById(R.id.contact_field_checkbox);
        }

        void bind(Field field, GlideRequests glideRequests, boolean z) {
            this.value.setMaxLines(field.maxLines);
            this.value.setText(field.value);
            this.label.setText(field.label);
            this.label.setVisibility(TextUtils.isEmpty(field.label) ? 8 : 0);
            if (field.iconUri != null) {
                this.avatar.setVisibility(0);
                glideRequests.load(field.iconUri).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).circleCrop().into(this.avatar);
            } else {
                this.avatar.setVisibility(8);
            }
            if (z) {
                this.checkBox.setVisibility(0);
                this.checkBox.setChecked(field.isSelected());
                this.itemView.setOnClickListener(new ContactFieldAdapter$ContactFieldViewHolder$$ExternalSyntheticLambda0(this, field));
                this.itemView.setOnClickListener(new ContactFieldAdapter$ContactFieldViewHolder$$ExternalSyntheticLambda1(this));
                return;
            }
            this.checkBox.setVisibility(8);
            this.itemView.setOnClickListener(null);
            this.itemView.setClickable(false);
        }

        public /* synthetic */ void lambda$bind$0(Field field, View view) {
            field.setSelected(!field.isSelected());
            this.checkBox.setChecked(field.isSelected());
        }

        public /* synthetic */ void lambda$bind$1(View view) {
            this.checkBox.toggle();
        }

        void recycle() {
            this.itemView.setOnClickListener(null);
            this.itemView.setClickable(false);
        }
    }

    /* loaded from: classes4.dex */
    public static class Field {
        final int iconResId;
        final Uri iconUri;
        final String label;
        final int maxLines;
        final Selectable selectable;
        final String value;

        Field(Context context, Contact.Phone phone, Locale locale) {
            this.value = ContactUtil.getPrettyPhoneNumber(phone, locale);
            this.iconResId = R.drawable.ic_phone_right_unlock_solid_24;
            this.iconUri = null;
            this.maxLines = 1;
            this.selectable = phone;
            int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Phone$Type[phone.getType().ordinal()];
            if (i == 1) {
                this.label = context.getString(R.string.ContactShareEditActivity_type_home);
            } else if (i == 2) {
                this.label = context.getString(R.string.ContactShareEditActivity_type_mobile);
            } else if (i != 3) {
                String str = "";
                if (i != 4) {
                    this.label = str;
                } else {
                    this.label = phone.getLabel() != null ? phone.getLabel() : str;
                }
            } else {
                this.label = context.getString(R.string.ContactShareEditActivity_type_work);
            }
        }

        Field(Context context, Contact.Email email) {
            this.value = email.getEmail();
            this.iconResId = R.drawable.baseline_email_white_24;
            this.iconUri = null;
            this.maxLines = 1;
            this.selectable = email;
            int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Email$Type[email.getType().ordinal()];
            if (i == 1) {
                this.label = context.getString(R.string.ContactShareEditActivity_type_home);
            } else if (i == 2) {
                this.label = context.getString(R.string.ContactShareEditActivity_type_mobile);
            } else if (i != 3) {
                String str = "";
                if (i != 4) {
                    this.label = str;
                } else {
                    this.label = email.getLabel() != null ? email.getLabel() : str;
                }
            } else {
                this.label = context.getString(R.string.ContactShareEditActivity_type_work);
            }
        }

        Field(Context context, Contact.PostalAddress postalAddress) {
            this.value = postalAddress.toString();
            this.iconResId = R.drawable.ic_location_on_white_24dp;
            this.iconUri = null;
            this.maxLines = 3;
            this.selectable = postalAddress;
            int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$PostalAddress$Type[postalAddress.getType().ordinal()];
            if (i == 1) {
                this.label = context.getString(R.string.ContactShareEditActivity_type_home);
            } else if (i == 2) {
                this.label = context.getString(R.string.ContactShareEditActivity_type_work);
            } else if (i != 3) {
                this.label = context.getString(R.string.ContactShareEditActivity_type_missing);
            } else {
                this.label = postalAddress.getLabel() != null ? postalAddress.getLabel() : context.getString(R.string.ContactShareEditActivity_type_missing);
            }
        }

        Field(Contact.Avatar avatar) {
            this.value = "";
            this.iconResId = R.drawable.baseline_account_circle_white_24;
            this.iconUri = avatar.getAttachment() != null ? avatar.getAttachment().getUri() : null;
            this.maxLines = 1;
            this.selectable = avatar;
            this.label = "";
        }

        void setSelected(boolean z) {
            this.selectable.setSelected(z);
        }

        boolean isSelected() {
            return this.selectable.isSelected();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: org.thoughtcrime.securesms.contactshare.ContactFieldAdapter$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Email$Type;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Phone$Type;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$PostalAddress$Type;

        static {
            int[] iArr = new int[Contact.PostalAddress.Type.values().length];
            $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$PostalAddress$Type = iArr;
            try {
                iArr[Contact.PostalAddress.Type.HOME.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$PostalAddress$Type[Contact.PostalAddress.Type.WORK.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$PostalAddress$Type[Contact.PostalAddress.Type.CUSTOM.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            int[] iArr2 = new int[Contact.Email.Type.values().length];
            $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Email$Type = iArr2;
            try {
                iArr2[Contact.Email.Type.HOME.ordinal()] = 1;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Email$Type[Contact.Email.Type.MOBILE.ordinal()] = 2;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Email$Type[Contact.Email.Type.WORK.ordinal()] = 3;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Email$Type[Contact.Email.Type.CUSTOM.ordinal()] = 4;
            } catch (NoSuchFieldError unused7) {
            }
            int[] iArr3 = new int[Contact.Phone.Type.values().length];
            $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Phone$Type = iArr3;
            try {
                iArr3[Contact.Phone.Type.HOME.ordinal()] = 1;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Phone$Type[Contact.Phone.Type.MOBILE.ordinal()] = 2;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Phone$Type[Contact.Phone.Type.WORK.ordinal()] = 3;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$contactshare$Contact$Phone$Type[Contact.Phone.Type.CUSTOM.ordinal()] = 4;
            } catch (NoSuchFieldError unused11) {
            }
        }
    }
}
