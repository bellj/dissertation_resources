package org.thoughtcrime.securesms;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.MasterSecret;
import org.thoughtcrime.securesms.service.KeyCachingService;

/* loaded from: classes.dex */
public abstract class PassphraseActivity extends BaseActivity {
    private static final String TAG = Log.tag(PassphraseActivity.class);
    private KeyCachingService keyCachingService;
    private MasterSecret masterSecret;
    private ServiceConnection serviceConnection = new ServiceConnection() { // from class: org.thoughtcrime.securesms.PassphraseActivity.1
        @Override // android.content.ServiceConnection
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            PassphraseActivity.this.keyCachingService = ((KeyCachingService.KeySetBinder) iBinder).getService();
            PassphraseActivity.this.keyCachingService.setMasterSecret(PassphraseActivity.this.masterSecret);
            PassphraseActivity passphraseActivity = PassphraseActivity.this;
            passphraseActivity.unbindService(passphraseActivity.serviceConnection);
            PassphraseActivity.this.masterSecret = null;
            PassphraseActivity.this.cleanup();
            Intent intent = (Intent) PassphraseActivity.this.getIntent().getParcelableExtra("next_intent");
            if (intent != null) {
                try {
                    PassphraseActivity.this.startActivity(intent);
                } catch (SecurityException unused) {
                    Log.w(PassphraseActivity.TAG, "Access permission not passed from PassphraseActivity, retry sharing.");
                }
            }
            PassphraseActivity.this.finish();
        }

        @Override // android.content.ServiceConnection
        public void onServiceDisconnected(ComponentName componentName) {
            PassphraseActivity.this.keyCachingService = null;
        }
    };

    protected abstract void cleanup();

    public void setMasterSecret(MasterSecret masterSecret) {
        this.masterSecret = masterSecret;
        Intent intent = new Intent(this, KeyCachingService.class);
        startService(intent);
        bindService(intent, this.serviceConnection, 1);
    }
}
