package org.thoughtcrime.securesms.logging;

import android.content.Context;
import android.os.Build;
import java.io.IOException;
import java.security.SecureRandom;
import org.thoughtcrime.securesms.crypto.KeyStoreHelper;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class LogSecretProvider {
    public static byte[] getOrCreateAttachmentSecret(Context context) {
        String logUnencryptedSecret = TextSecurePreferences.getLogUnencryptedSecret(context);
        String logEncryptedSecret = TextSecurePreferences.getLogEncryptedSecret(context);
        if (logUnencryptedSecret != null) {
            return parseUnencryptedSecret(logUnencryptedSecret);
        }
        if (logEncryptedSecret != null) {
            return parseEncryptedSecret(logEncryptedSecret);
        }
        return createAndStoreSecret(context);
    }

    private static byte[] parseUnencryptedSecret(String str) {
        try {
            return Base64.decode(str);
        } catch (IOException unused) {
            throw new AssertionError("Failed to decode the unecrypted secret.");
        }
    }

    private static byte[] parseEncryptedSecret(String str) {
        if (Build.VERSION.SDK_INT >= 23) {
            return KeyStoreHelper.unseal(KeyStoreHelper.SealedData.fromString(str));
        }
        throw new AssertionError("OS downgrade not supported. KeyStore sealed data exists on platform < M!");
    }

    private static byte[] createAndStoreSecret(Context context) {
        byte[] bArr = new byte[32];
        new SecureRandom().nextBytes(bArr);
        if (Build.VERSION.SDK_INT >= 23) {
            TextSecurePreferences.setLogEncryptedSecret(context, KeyStoreHelper.seal(bArr).serialize());
        } else {
            TextSecurePreferences.setLogUnencryptedSecret(context, Base64.encodeBytes(bArr));
        }
        return bArr;
    }
}
