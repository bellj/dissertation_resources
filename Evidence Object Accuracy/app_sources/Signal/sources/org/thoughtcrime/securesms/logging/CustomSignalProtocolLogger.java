package org.thoughtcrime.securesms.logging;

import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.logging.SignalProtocolLogger;

/* loaded from: classes.dex */
public class CustomSignalProtocolLogger implements SignalProtocolLogger {
    @Override // org.signal.libsignal.protocol.logging.SignalProtocolLogger
    public void log(int i, String str, String str2) {
        switch (i) {
            case 2:
                Log.v(str, str2);
                return;
            case 3:
                Log.d(str, str2);
                return;
            case 4:
                Log.i(str, str2);
                return;
            case 5:
                Log.w(str, str2);
                return;
            case 6:
            case 7:
                Log.e(str, str2);
                return;
            default:
                return;
        }
    }
}
