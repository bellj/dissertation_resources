package org.thoughtcrime.securesms.logging;

import android.app.Application;
import android.os.Looper;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__MutableCollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.StringCompanionObject;
import kotlin.text.Charsets;
import kotlin.text.Regex;
import org.signal.core.util.logging.Log;
import org.signal.core.util.logging.Scrubber;
import org.thoughtcrime.securesms.database.LogDatabase;
import org.thoughtcrime.securesms.database.model.LogEntry;

/* compiled from: PersistentLogger.kt */
@Metadata(d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000b\n\u0002\b\r\u0018\u0000 \u001c2\u00020\u0001:\u0004\u001c\u001d\u001e\u001fB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J.\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u00072\b\u0010\u000f\u001a\u0004\u0018\u00010\u00072\b\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J.\u0010\u0014\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u00072\b\u0010\u000f\u001a\u0004\u0018\u00010\u00072\b\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\b\u0010\u0015\u001a\u00020\rH\u0016J\b\u0010\u0016\u001a\u00020\u0007H\u0002J.\u0010\u0017\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u00072\b\u0010\u000f\u001a\u0004\u0018\u00010\u00072\b\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J.\u0010\u0018\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u00072\b\u0010\u000f\u001a\u0004\u0018\u00010\u00072\b\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J.\u0010\u0019\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u00072\b\u0010\u000f\u001a\u0004\u0018\u00010\u00072\b\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J6\u0010\u001a\u001a\u00020\r2\u0006\u0010\u001b\u001a\u00020\u00072\b\u0010\u000e\u001a\u0004\u0018\u00010\u00072\b\u0010\u000f\u001a\u0004\u0018\u00010\u00072\b\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000¨\u0006 "}, d2 = {"Lorg/thoughtcrime/securesms/logging/PersistentLogger;", "Lorg/signal/core/util/logging/Log$Logger;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "cachedThreadString", "Ljava/lang/ThreadLocal;", "", "logDatabase", "Lorg/thoughtcrime/securesms/database/LogDatabase;", "logEntries", "Lorg/thoughtcrime/securesms/logging/PersistentLogger$LogRequests;", "d", "", "tag", "message", "t", "", "keepLonger", "", "e", "flush", "getThreadString", "i", "v", "w", "write", "level", "Companion", "LogRequest", "LogRequests", "WriteThread", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class PersistentLogger extends Log.Logger {
    public static final Companion Companion = new Companion(null);
    private static final String LOG_D;
    private static final String LOG_E;
    private static final String LOG_I;
    private static final String LOG_V;
    private static final String LOG_W;
    private final ThreadLocal<String> cachedThreadString = new ThreadLocal<>();
    private final LogDatabase logDatabase;
    private final LogRequests logEntries;

    public PersistentLogger(Application application) {
        Intrinsics.checkNotNullParameter(application, "application");
        LogRequests logRequests = new LogRequests();
        this.logEntries = logRequests;
        LogDatabase instance = LogDatabase.Companion.getInstance(application);
        this.logDatabase = instance;
        WriteThread writeThread = new WriteThread(logRequests, instance);
        writeThread.setPriority(1);
        writeThread.start();
    }

    /* compiled from: PersistentLogger.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/logging/PersistentLogger$Companion;", "", "()V", "LOG_D", "", "LOG_E", "LOG_I", "LOG_V", "LOG_W", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void v(String str, String str2, Throwable th, boolean z) {
        write(LOG_V, str, str2, th, z);
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void d(String str, String str2, Throwable th, boolean z) {
        write(LOG_D, str, str2, th, z);
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void i(String str, String str2, Throwable th, boolean z) {
        write(LOG_I, str, str2, th, z);
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void w(String str, String str2, Throwable th, boolean z) {
        write(LOG_W, str, str2, th, z);
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void e(String str, String str2, Throwable th, boolean z) {
        write(LOG_E, str, str2, th, z);
    }

    @Override // org.signal.core.util.logging.Log.Logger
    public void flush() {
        this.logEntries.blockForFlushed();
    }

    private final void write(String str, String str2, String str3, Throwable th, boolean z) {
        LogRequests logRequests = this.logEntries;
        if (str2 == null) {
            str2 = "null";
        }
        logRequests.add(new LogRequest(str, str2, str3, new Date(), getThreadString(), th, z));
    }

    private final String getThreadString() {
        String str = this.cachedThreadString.get();
        if (this.cachedThreadString.get() == null) {
            if (Intrinsics.areEqual(Looper.myLooper(), Looper.getMainLooper())) {
                str = "main ";
            } else {
                StringCompanionObject stringCompanionObject = StringCompanionObject.INSTANCE;
                str = String.format("%-5s", Arrays.copyOf(new Object[]{Long.valueOf(Thread.currentThread().getId())}, 1));
                Intrinsics.checkNotNullExpressionValue(str, "format(format, *args)");
            }
            this.cachedThreadString.set(str);
        }
        Intrinsics.checkNotNull(str);
        return str;
    }

    /* compiled from: PersistentLogger.kt */
    @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0017\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001BA\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\b\u0010\t\u001a\u0004\u0018\u00010\n\u0012\u0006\u0010\u000b\u001a\u00020\f¢\u0006\u0002\u0010\rJ\t\u0010\u0019\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001a\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u001b\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010\u001c\u001a\u00020\u0007HÆ\u0003J\t\u0010\u001d\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\nHÆ\u0003J\t\u0010\u001f\u001a\u00020\fHÆ\u0003JS\u0010 \u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\u00032\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\n2\b\b\u0002\u0010\u000b\u001a\u00020\fHÆ\u0001J\u0013\u0010!\u001a\u00020\f2\b\u0010\"\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010#\u001a\u00020$HÖ\u0001J\t\u0010%\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0013R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0013R\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0013R\u0013\u0010\t\u001a\u0004\u0018\u00010\n¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018¨\u0006&"}, d2 = {"Lorg/thoughtcrime/securesms/logging/PersistentLogger$LogRequest;", "", "level", "", "tag", "message", "date", "Ljava/util/Date;", "threadString", "throwable", "", "keepLonger", "", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/lang/Throwable;Z)V", "getDate", "()Ljava/util/Date;", "getKeepLonger", "()Z", "getLevel", "()Ljava/lang/String;", "getMessage", "getTag", "getThreadString", "getThrowable", "()Ljava/lang/Throwable;", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "equals", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class LogRequest {
        private final Date date;
        private final boolean keepLonger;
        private final String level;
        private final String message;
        private final String tag;
        private final String threadString;
        private final Throwable throwable;

        public static /* synthetic */ LogRequest copy$default(LogRequest logRequest, String str, String str2, String str3, Date date, String str4, Throwable th, boolean z, int i, Object obj) {
            if ((i & 1) != 0) {
                str = logRequest.level;
            }
            if ((i & 2) != 0) {
                str2 = logRequest.tag;
            }
            if ((i & 4) != 0) {
                str3 = logRequest.message;
            }
            if ((i & 8) != 0) {
                date = logRequest.date;
            }
            if ((i & 16) != 0) {
                str4 = logRequest.threadString;
            }
            if ((i & 32) != 0) {
                th = logRequest.throwable;
            }
            if ((i & 64) != 0) {
                z = logRequest.keepLonger;
            }
            return logRequest.copy(str, str2, str3, date, str4, th, z);
        }

        public final String component1() {
            return this.level;
        }

        public final String component2() {
            return this.tag;
        }

        public final String component3() {
            return this.message;
        }

        public final Date component4() {
            return this.date;
        }

        public final String component5() {
            return this.threadString;
        }

        public final Throwable component6() {
            return this.throwable;
        }

        public final boolean component7() {
            return this.keepLonger;
        }

        public final LogRequest copy(String str, String str2, String str3, Date date, String str4, Throwable th, boolean z) {
            Intrinsics.checkNotNullParameter(str, "level");
            Intrinsics.checkNotNullParameter(str2, "tag");
            Intrinsics.checkNotNullParameter(date, "date");
            Intrinsics.checkNotNullParameter(str4, "threadString");
            return new LogRequest(str, str2, str3, date, str4, th, z);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof LogRequest)) {
                return false;
            }
            LogRequest logRequest = (LogRequest) obj;
            return Intrinsics.areEqual(this.level, logRequest.level) && Intrinsics.areEqual(this.tag, logRequest.tag) && Intrinsics.areEqual(this.message, logRequest.message) && Intrinsics.areEqual(this.date, logRequest.date) && Intrinsics.areEqual(this.threadString, logRequest.threadString) && Intrinsics.areEqual(this.throwable, logRequest.throwable) && this.keepLonger == logRequest.keepLonger;
        }

        public int hashCode() {
            int hashCode = ((this.level.hashCode() * 31) + this.tag.hashCode()) * 31;
            String str = this.message;
            int i = 0;
            int hashCode2 = (((((hashCode + (str == null ? 0 : str.hashCode())) * 31) + this.date.hashCode()) * 31) + this.threadString.hashCode()) * 31;
            Throwable th = this.throwable;
            if (th != null) {
                i = th.hashCode();
            }
            int i2 = (hashCode2 + i) * 31;
            boolean z = this.keepLonger;
            if (z) {
                z = true;
            }
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            int i5 = z ? 1 : 0;
            return i2 + i3;
        }

        public String toString() {
            return "LogRequest(level=" + this.level + ", tag=" + this.tag + ", message=" + this.message + ", date=" + this.date + ", threadString=" + this.threadString + ", throwable=" + this.throwable + ", keepLonger=" + this.keepLonger + ')';
        }

        public LogRequest(String str, String str2, String str3, Date date, String str4, Throwable th, boolean z) {
            Intrinsics.checkNotNullParameter(str, "level");
            Intrinsics.checkNotNullParameter(str2, "tag");
            Intrinsics.checkNotNullParameter(date, "date");
            Intrinsics.checkNotNullParameter(str4, "threadString");
            this.level = str;
            this.tag = str2;
            this.message = str3;
            this.date = date;
            this.threadString = str4;
            this.throwable = th;
            this.keepLonger = z;
        }

        public final String getLevel() {
            return this.level;
        }

        public final String getTag() {
            return this.tag;
        }

        public final String getMessage() {
            return this.message;
        }

        public final Date getDate() {
            return this.date;
        }

        public final String getThreadString() {
            return this.threadString;
        }

        public final Throwable getThrowable() {
            return this.throwable;
        }

        public final boolean getKeepLonger() {
            return this.keepLonger;
        }
    }

    /* compiled from: PersistentLogger.kt */
    /* access modifiers changed from: private */
    @Metadata(d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J0\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\r2\u0006\u0010\u0012\u001a\u00020\r2\b\u0010\u0013\u001a\u0004\u0018\u00010\rJ\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00160\u00152\u0006\u0010\u0017\u001a\u00020\tJ\b\u0010\u0018\u001a\u00020\u0019H\u0016R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/logging/PersistentLogger$WriteThread;", "Ljava/lang/Thread;", "requests", "Lorg/thoughtcrime/securesms/logging/PersistentLogger$LogRequests;", "db", "Lorg/thoughtcrime/securesms/database/LogDatabase;", "(Lorg/thoughtcrime/securesms/logging/PersistentLogger$LogRequests;Lorg/thoughtcrime/securesms/database/LogDatabase;)V", "buffer", "", "Lorg/thoughtcrime/securesms/logging/PersistentLogger$LogRequest;", "dateFormat", "Ljava/text/SimpleDateFormat;", "formatBody", "", "threadString", "date", "Ljava/util/Date;", "level", "tag", "message", "requestToEntries", "", "Lorg/thoughtcrime/securesms/database/model/LogEntry;", "request", "run", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class WriteThread extends Thread {
        private final List<LogRequest> buffer = new ArrayList();
        private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS zzz", Locale.US);
        private final LogDatabase db;
        private final LogRequests requests;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public WriteThread(LogRequests logRequests, LogDatabase logDatabase) {
            super("signal-logger");
            Intrinsics.checkNotNullParameter(logRequests, "requests");
            Intrinsics.checkNotNullParameter(logDatabase, "db");
            this.requests = logRequests;
            this.db = logDatabase;
        }

        @Override // java.lang.Thread, java.lang.Runnable
        public void run() {
            while (true) {
                this.requests.blockForRequests(this.buffer);
                LogDatabase logDatabase = this.db;
                List<LogRequest> list = this.buffer;
                ArrayList arrayList = new ArrayList();
                for (LogRequest logRequest : list) {
                    boolean unused = CollectionsKt__MutableCollectionsKt.addAll(arrayList, requestToEntries(logRequest));
                }
                logDatabase.insert(arrayList, System.currentTimeMillis());
                this.buffer.clear();
                this.requests.notifyFlushed();
            }
        }

        public final List<LogEntry> requestToEntries(LogRequest logRequest) {
            Intrinsics.checkNotNullParameter(logRequest, "request");
            ArrayList arrayList = new ArrayList();
            arrayList.add(new LogEntry(logRequest.getDate().getTime(), logRequest.getKeepLonger(), formatBody(logRequest.getThreadString(), logRequest.getDate(), logRequest.getLevel(), logRequest.getTag(), logRequest.getMessage())));
            if (logRequest.getThrowable() != null) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                logRequest.getThrowable().printStackTrace(new PrintStream(byteArrayOutputStream));
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                Intrinsics.checkNotNullExpressionValue(byteArray, "outputStream.toByteArray()");
                Object[] array = new Regex("\\n").split(new String(byteArray, Charsets.UTF_8), 0).toArray(new String[0]);
                if (array != null) {
                    String[] strArr = (String[]) array;
                    ArrayList arrayList2 = new ArrayList(strArr.length);
                    for (String str : strArr) {
                        arrayList2.add(new LogEntry(logRequest.getDate().getTime(), logRequest.getKeepLonger(), formatBody(logRequest.getThreadString(), logRequest.getDate(), logRequest.getLevel(), logRequest.getTag(), str)));
                    }
                    arrayList.addAll(arrayList2);
                } else {
                    throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
                }
            }
            return arrayList;
        }

        public final String formatBody(String str, Date date, String str2, String str3, String str4) {
            Intrinsics.checkNotNullParameter(str, "threadString");
            Intrinsics.checkNotNullParameter(date, "date");
            Intrinsics.checkNotNullParameter(str2, "level");
            Intrinsics.checkNotNullParameter(str3, "tag");
            StringBuilder sb = new StringBuilder();
            sb.append("[5.44.4] [");
            sb.append(str);
            sb.append("] ");
            sb.append(this.dateFormat.format(date));
            sb.append(' ');
            sb.append(str2);
            sb.append(' ');
            sb.append(str3);
            sb.append(": ");
            if (str4 == null) {
                str4 = "";
            }
            sb.append((Object) Scrubber.scrub(str4));
            return sb.toString();
        }
    }

    /* compiled from: PersistentLogger.kt */
    @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0006\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0011J\u0006\u0010\u0017\u001a\u00020\u0015J\u0014\u0010\u0018\u001a\u00020\u00152\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010J\u0006\u0010\u001a\u001a\u00020\u0015R\u001a\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\r\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\fR\u0017\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/logging/PersistentLogger$LogRequests;", "", "()V", "flushed", "", "getFlushed", "()Z", "setFlushed", "(Z)V", "flushedLock", "Ljava/lang/Object;", "getFlushedLock", "()Ljava/lang/Object;", "logLock", "getLogLock", "logs", "", "Lorg/thoughtcrime/securesms/logging/PersistentLogger$LogRequest;", "getLogs", "()Ljava/util/List;", "add", "", "entry", "blockForFlushed", "blockForRequests", "buffer", "notifyFlushed", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class LogRequests {
        private boolean flushed;
        private final Object flushedLock = new Object();
        private final Object logLock = new Object();
        private final List<LogRequest> logs = new ArrayList();

        public final List<LogRequest> getLogs() {
            return this.logs;
        }

        public final Object getLogLock() {
            return this.logLock;
        }

        public final boolean getFlushed() {
            return this.flushed;
        }

        public final void setFlushed(boolean z) {
            this.flushed = z;
        }

        public final Object getFlushedLock() {
            return this.flushedLock;
        }

        public final void add(LogRequest logRequest) {
            Intrinsics.checkNotNullParameter(logRequest, "entry");
            synchronized (this.logLock) {
                this.logs.add(logRequest);
                this.logLock.notify();
                Unit unit = Unit.INSTANCE;
            }
        }

        public final void blockForRequests(List<LogRequest> list) {
            Intrinsics.checkNotNullParameter(list, "buffer");
            synchronized (this.logLock) {
                while (this.logs.isEmpty()) {
                    this.logLock.wait();
                }
                list.addAll(this.logs);
                this.logs.clear();
                this.flushed = false;
                Unit unit = Unit.INSTANCE;
            }
        }

        public final void blockForFlushed() {
            synchronized (this.flushedLock) {
                while (!this.flushed) {
                    this.flushedLock.wait();
                }
                Unit unit = Unit.INSTANCE;
            }
        }

        public final void notifyFlushed() {
            synchronized (this.flushedLock) {
                this.flushed = true;
                this.flushedLock.notify();
                Unit unit = Unit.INSTANCE;
            }
        }
    }
}
