package org.thoughtcrime.securesms.stickers;

import android.os.Parcel;
import android.os.Parcelable;

/* loaded from: classes4.dex */
public class StickerLocator implements Parcelable {
    public static final Parcelable.Creator<StickerLocator> CREATOR = new Parcelable.Creator<StickerLocator>() { // from class: org.thoughtcrime.securesms.stickers.StickerLocator.1
        @Override // android.os.Parcelable.Creator
        public StickerLocator createFromParcel(Parcel parcel) {
            return new StickerLocator(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public StickerLocator[] newArray(int i) {
            return new StickerLocator[i];
        }
    };
    private final String emoji;
    private final String packId;
    private final String packKey;
    private final int stickerId;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public StickerLocator(String str, String str2, int i, String str3) {
        this.packId = str;
        this.packKey = str2;
        this.stickerId = i;
        this.emoji = str3;
    }

    private StickerLocator(Parcel parcel) {
        this.packId = parcel.readString();
        this.packKey = parcel.readString();
        this.stickerId = parcel.readInt();
        this.emoji = parcel.readString();
    }

    public String getPackId() {
        return this.packId;
    }

    public String getPackKey() {
        return this.packKey;
    }

    public int getStickerId() {
        return this.stickerId;
    }

    public String getEmoji() {
        return this.emoji;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.packId);
        parcel.writeString(this.packKey);
        parcel.writeInt(this.stickerId);
        parcel.writeString(this.emoji);
    }
}
