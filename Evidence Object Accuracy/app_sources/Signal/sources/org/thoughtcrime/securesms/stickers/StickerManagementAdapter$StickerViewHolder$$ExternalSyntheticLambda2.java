package org.thoughtcrime.securesms.stickers;

import android.view.View;
import org.thoughtcrime.securesms.database.model.StickerPackRecord;
import org.thoughtcrime.securesms.stickers.StickerManagementAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class StickerManagementAdapter$StickerViewHolder$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ StickerManagementAdapter.EventListener f$0;
    public final /* synthetic */ StickerPackRecord f$1;

    public /* synthetic */ StickerManagementAdapter$StickerViewHolder$$ExternalSyntheticLambda2(StickerManagementAdapter.EventListener eventListener, StickerPackRecord stickerPackRecord) {
        this.f$0 = eventListener;
        this.f$1 = stickerPackRecord;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        StickerManagementAdapter.StickerViewHolder.lambda$bind$2(this.f$0, this.f$1, view);
    }
}
