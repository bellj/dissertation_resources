package org.thoughtcrime.securesms.stickers;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;
import java.io.InputStream;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.whispersystems.signalservice.api.SignalServiceMessageReceiver;

/* loaded from: classes4.dex */
public final class StickerRemoteUriLoader implements ModelLoader<StickerRemoteUri, InputStream> {
    private final SignalServiceMessageReceiver receiver;

    public boolean handles(StickerRemoteUri stickerRemoteUri) {
        return true;
    }

    public StickerRemoteUriLoader(SignalServiceMessageReceiver signalServiceMessageReceiver) {
        this.receiver = signalServiceMessageReceiver;
    }

    public ModelLoader.LoadData<InputStream> buildLoadData(StickerRemoteUri stickerRemoteUri, int i, int i2, Options options) {
        return new ModelLoader.LoadData<>(stickerRemoteUri, new StickerRemoteUriFetcher(this.receiver, stickerRemoteUri));
    }

    /* loaded from: classes4.dex */
    public static class Factory implements ModelLoaderFactory<StickerRemoteUri, InputStream> {
        public void teardown() {
        }

        public ModelLoader<StickerRemoteUri, InputStream> build(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new StickerRemoteUriLoader(ApplicationDependencies.getSignalServiceMessageReceiver());
        }
    }
}
