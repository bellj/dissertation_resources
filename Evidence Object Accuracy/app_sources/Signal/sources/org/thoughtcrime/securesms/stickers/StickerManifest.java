package org.thoughtcrime.securesms.stickers;

import android.net.Uri;
import j$.util.Optional;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes4.dex */
public final class StickerManifest {
    private final Optional<String> author;
    private final Optional<Sticker> cover;
    private final String packId;
    private final String packKey;
    private final List<Sticker> stickers;
    private final Optional<String> title;

    public StickerManifest(String str, String str2, Optional<String> optional, Optional<String> optional2, Optional<Sticker> optional3, List<Sticker> list) {
        this.packId = str;
        this.packKey = str2;
        this.title = optional;
        this.author = optional2;
        this.cover = optional3;
        this.stickers = new ArrayList(list);
    }

    public String getPackId() {
        return this.packId;
    }

    public String getPackKey() {
        return this.packKey;
    }

    public Optional<String> getTitle() {
        return this.title;
    }

    public Optional<String> getAuthor() {
        return this.author;
    }

    public Optional<Sticker> getCover() {
        return this.cover;
    }

    public List<Sticker> getStickers() {
        return this.stickers;
    }

    /* loaded from: classes4.dex */
    public static class Sticker {
        private final String contentType;
        private final String emoji;
        private final int id;
        private final String packId;
        private final String packKey;
        private final Optional<Uri> uri;

        public Sticker(String str, String str2, int i, String str3, String str4) {
            this(str, str2, i, str3, str4, null);
        }

        public Sticker(String str, String str2, int i, String str3, String str4, Uri uri) {
            this.packId = str;
            this.packKey = str2;
            this.id = i;
            this.emoji = str3;
            this.contentType = str4;
            this.uri = Optional.ofNullable(uri);
        }

        public String getPackId() {
            return this.packId;
        }

        public String getPackKey() {
            return this.packKey;
        }

        public int getId() {
            return this.id;
        }

        public String getEmoji() {
            return this.emoji;
        }

        public String getContentType() {
            return this.contentType;
        }

        public Optional<Uri> getUri() {
            return this.uri;
        }
    }
}
