package org.thoughtcrime.securesms.stickers;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.TransitionOptions;
import com.bumptech.glide.load.Option;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import java.util.ArrayList;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.glide.cache.ApngOptions;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.stickers.StickerManifest;

/* loaded from: classes4.dex */
public final class StickerPackPreviewAdapter extends RecyclerView.Adapter<StickerViewHolder> {
    private final boolean allowApngAnimation;
    private final EventListener eventListener;
    private final GlideRequests glideRequests;
    private final List<StickerManifest.Sticker> list = new ArrayList();

    /* loaded from: classes4.dex */
    public interface EventListener {
        void onStickerLongPress(View view);
    }

    public StickerPackPreviewAdapter(GlideRequests glideRequests, EventListener eventListener, boolean z) {
        this.glideRequests = glideRequests;
        this.eventListener = eventListener;
        this.allowApngAnimation = z;
    }

    public StickerViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new StickerViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sticker_preview_list_item, viewGroup, false));
    }

    public void onBindViewHolder(StickerViewHolder stickerViewHolder, int i) {
        stickerViewHolder.bind(this.glideRequests, this.list.get(i), this.eventListener, this.allowApngAnimation);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.list.size();
    }

    public void onViewRecycled(StickerViewHolder stickerViewHolder) {
        stickerViewHolder.recycle();
    }

    public void setStickers(List<StickerManifest.Sticker> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    /* loaded from: classes4.dex */
    public static class StickerViewHolder extends RecyclerView.ViewHolder {
        private String currentEmoji;
        private Object currentGlideModel;
        private final ImageView image;

        private StickerViewHolder(View view) {
            super(view);
            this.image = (ImageView) view.findViewById(R.id.sticker_install_item_image);
        }

        /* JADX DEBUG: Type inference failed for r5v2. Raw type applied. Possible types: com.bumptech.glide.load.Option<java.lang.Boolean>, com.bumptech.glide.load.Option<Y> */
        void bind(GlideRequests glideRequests, StickerManifest.Sticker sticker, EventListener eventListener, boolean z) {
            Object obj;
            this.currentEmoji = sticker.getEmoji();
            if (sticker.getUri().isPresent()) {
                obj = new DecryptableStreamUriLoader.DecryptableUri(sticker.getUri().get());
            } else {
                obj = new StickerRemoteUri(sticker.getPackId(), sticker.getPackKey(), sticker.getId());
            }
            this.currentGlideModel = obj;
            glideRequests.load(obj).transition((TransitionOptions<?, ? super Drawable>) DrawableTransitionOptions.withCrossFade()).set((Option<Option>) ApngOptions.ANIMATE, (Option) Boolean.valueOf(z)).into(this.image);
            this.image.setOnLongClickListener(new StickerPackPreviewAdapter$StickerViewHolder$$ExternalSyntheticLambda0(eventListener));
        }

        void recycle() {
            this.image.setOnLongClickListener(null);
        }

        public Object getCurrentGlideModel() {
            return this.currentGlideModel;
        }

        public String getCurrentEmoji() {
            return this.currentEmoji;
        }
    }
}
