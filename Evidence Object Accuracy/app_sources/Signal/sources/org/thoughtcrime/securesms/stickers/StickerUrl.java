package org.thoughtcrime.securesms.stickers;

import android.net.Uri;
import android.text.TextUtils;
import j$.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.signal.core.util.Hex;
import org.signal.libsignal.protocol.util.Pair;
import org.whispersystems.signalservice.api.util.OptionalUtil;

/* loaded from: classes4.dex */
public class StickerUrl {
    private static final Pattern STICKER_URL_PATTERN = Pattern.compile("^https://signal\\.art/addstickers/#pack_id=(.*)&pack_key=(.*)$");

    public static Optional<Pair<String, String>> parseExternalUri(Uri uri) {
        if (uri == null) {
            return Optional.empty();
        }
        return OptionalUtil.or(parseActionUri(uri), parseShareLink(uri.toString()));
    }

    public static Optional<Pair<String, String>> parseActionUri(Uri uri) {
        if (uri == null) {
            return Optional.empty();
        }
        String queryParameter = uri.getQueryParameter("pack_id");
        String queryParameter2 = uri.getQueryParameter("pack_key");
        if (TextUtils.isEmpty(queryParameter) || TextUtils.isEmpty(queryParameter2) || !isValidHex(queryParameter) || !isValidHex(queryParameter2)) {
            return Optional.empty();
        }
        return Optional.of(new Pair(queryParameter, queryParameter2));
    }

    public static Uri createActionUri(String str, String str2) {
        return Uri.parse(String.format("sgnl://addstickers?pack_id=%s&pack_key=%s", str, str2));
    }

    public static boolean isValidShareLink(String str) {
        return parseShareLink(str).isPresent();
    }

    public static Optional<Pair<String, String>> parseShareLink(String str) {
        if (str == null) {
            return Optional.empty();
        }
        Matcher matcher = STICKER_URL_PATTERN.matcher(str);
        if (matcher.matches() && matcher.groupCount() == 2) {
            String group = matcher.group(1);
            String group2 = matcher.group(2);
            if (isValidHex(group) && isValidHex(group2)) {
                return Optional.of(new Pair(group, group2));
            }
        }
        return Optional.empty();
    }

    public static String createShareLink(String str, String str2) {
        return "https://signal.art/addstickers/#pack_id=" + str + "&pack_key=" + str2;
    }

    private static boolean isValidHex(String str) {
        try {
            Hex.fromStringCondensed(str);
            return true;
        } catch (Exception unused) {
            return false;
        }
    }
}
