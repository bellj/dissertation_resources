package org.thoughtcrime.securesms.stickers;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.DataFetcher;
import java.io.IOException;
import java.io.InputStream;
import org.signal.core.util.Hex;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.whispersystems.signalservice.api.SignalServiceMessageReceiver;

/* loaded from: classes4.dex */
public final class StickerRemoteUriFetcher implements DataFetcher<InputStream> {
    private static final String TAG = Log.tag(StickerRemoteUriFetcher.class);
    private final SignalServiceMessageReceiver receiver;
    private final StickerRemoteUri stickerUri;

    @Override // com.bumptech.glide.load.data.DataFetcher
    public void cleanup() {
    }

    public StickerRemoteUriFetcher(SignalServiceMessageReceiver signalServiceMessageReceiver, StickerRemoteUri stickerRemoteUri) {
        this.receiver = signalServiceMessageReceiver;
        this.stickerUri = stickerRemoteUri;
    }

    @Override // com.bumptech.glide.load.data.DataFetcher
    public void loadData(Priority priority, DataFetcher.DataCallback<? super InputStream> dataCallback) {
        try {
            dataCallback.onDataReady(this.receiver.retrieveSticker(Hex.fromStringCondensed(this.stickerUri.getPackId()), Hex.fromStringCondensed(this.stickerUri.getPackKey()), this.stickerUri.getStickerId()));
        } catch (IOException | InvalidMessageException e) {
            dataCallback.onLoadFailed(e);
        }
    }

    @Override // com.bumptech.glide.load.data.DataFetcher
    public void cancel() {
        Log.d(TAG, "Canceled.");
    }

    @Override // com.bumptech.glide.load.data.DataFetcher
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }

    @Override // com.bumptech.glide.load.data.DataFetcher
    public DataSource getDataSource() {
        return DataSource.REMOTE;
    }
}
