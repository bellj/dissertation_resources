package org.thoughtcrime.securesms.stickers;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import java.lang.ref.WeakReference;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerListAdapter;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public class StickerRolloverTouchListener implements RecyclerView.OnItemTouchListener {
    private WeakReference<View> currentView = new WeakReference<>(null);
    private final RolloverEventListener eventListener;
    private boolean hoverMode;
    private final StickerPreviewPopup popup;
    private final RolloverStickerRetriever stickerRetriever;

    /* loaded from: classes4.dex */
    public interface RolloverEventListener {
        void onStickerPopupEnded();

        void onStickerPopupStarted();
    }

    /* loaded from: classes4.dex */
    public interface RolloverStickerRetriever {
        Pair<Object, String> getStickerDataFromView(View view);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.OnItemTouchListener
    public void onRequestDisallowInterceptTouchEvent(boolean z) {
    }

    public StickerRolloverTouchListener(Context context, GlideRequests glideRequests, RolloverEventListener rolloverEventListener, RolloverStickerRetriever rolloverStickerRetriever) {
        this.eventListener = rolloverEventListener;
        this.stickerRetriever = rolloverStickerRetriever;
        StickerPreviewPopup stickerPreviewPopup = new StickerPreviewPopup(context, glideRequests);
        this.popup = stickerPreviewPopup;
        stickerPreviewPopup.setAnimationStyle(R.style.StickerPopupAnimation);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.OnItemTouchListener
    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
        return this.hoverMode;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.OnItemTouchListener
    public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 1 || action == 3) {
            this.hoverMode = false;
            this.popup.dismiss();
            this.eventListener.onStickerPopupEnded();
            this.currentView.clear();
            return;
        }
        int childCount = recyclerView.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = recyclerView.getChildAt(i);
            if (ViewUtil.isPointInsideView(recyclerView, motionEvent.getRawX(), motionEvent.getRawY()) && ViewUtil.isPointInsideView(childAt, motionEvent.getRawX(), motionEvent.getRawY()) && childAt != this.currentView.get()) {
                showStickerForView(recyclerView, childAt);
                this.currentView = new WeakReference<>(childAt);
                return;
            }
        }
    }

    public void enterHoverMode(RecyclerView recyclerView, View view) {
        this.hoverMode = true;
        showStickerForView(recyclerView, view);
    }

    public void enterHoverMode(RecyclerView recyclerView, KeyboardStickerListAdapter.Sticker sticker) {
        this.hoverMode = true;
        showSticker(recyclerView, sticker.getUri(), sticker.getStickerRecord().getEmoji());
    }

    private void showStickerForView(RecyclerView recyclerView, View view) {
        Pair<Object, String> stickerDataFromView = this.stickerRetriever.getStickerDataFromView(view);
        if (stickerDataFromView != null) {
            showSticker(recyclerView, stickerDataFromView.first(), stickerDataFromView.second());
        }
    }

    private void showSticker(RecyclerView recyclerView, Object obj, String str) {
        if (!this.popup.isShowing()) {
            this.popup.showAtLocation(recyclerView, 0, 0, 0);
            this.eventListener.onStickerPopupStarted();
        }
        this.popup.presentSticker(obj, str);
    }
}
