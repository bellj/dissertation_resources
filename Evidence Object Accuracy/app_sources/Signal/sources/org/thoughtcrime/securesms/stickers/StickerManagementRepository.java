package org.thoughtcrime.securesms.stickers;

import android.content.Context;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.StickerDatabase;
import org.thoughtcrime.securesms.database.model.StickerPackRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobs.MultiDeviceStickerPackOperationJob;
import org.thoughtcrime.securesms.jobs.StickerPackDownloadJob;
import org.thoughtcrime.securesms.stickers.StickerManagementRepository;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public final class StickerManagementRepository {
    private final AttachmentDatabase attachmentDatabase = SignalDatabase.attachments();
    private final Context context;
    private final StickerDatabase stickerDatabase = SignalDatabase.stickers();

    /* loaded from: classes4.dex */
    public interface Callback<T> {
        void onComplete(T t);
    }

    public StickerManagementRepository(Context context) {
        this.context = context.getApplicationContext();
    }

    public void deleteOrphanedStickerPacks() {
        ExecutorService executorService = SignalExecutors.SERIAL;
        StickerDatabase stickerDatabase = this.stickerDatabase;
        Objects.requireNonNull(stickerDatabase);
        executorService.execute(new Runnable() { // from class: org.thoughtcrime.securesms.stickers.StickerManagementRepository$$ExternalSyntheticLambda3
            @Override // java.lang.Runnable
            public final void run() {
                StickerDatabase.this.deleteOrphanedPacks();
            }
        });
    }

    public void fetchUnretrievedReferencePacks() {
        SignalExecutors.SERIAL.execute(new Runnable() { // from class: org.thoughtcrime.securesms.stickers.StickerManagementRepository$$ExternalSyntheticLambda5
            @Override // java.lang.Runnable
            public final void run() {
                StickerManagementRepository.this.lambda$fetchUnretrievedReferencePacks$0();
            }
        });
    }

    public /* synthetic */ void lambda$fetchUnretrievedReferencePacks$0() {
        JobManager jobManager = ApplicationDependencies.getJobManager();
        Cursor unavailableStickerPacks = this.attachmentDatabase.getUnavailableStickerPacks();
        while (unavailableStickerPacks != null) {
            try {
                if (!unavailableStickerPacks.moveToNext()) {
                    break;
                }
                jobManager.add(StickerPackDownloadJob.forReference(unavailableStickerPacks.getString(unavailableStickerPacks.getColumnIndexOrThrow(AttachmentDatabase.STICKER_PACK_ID)), unavailableStickerPacks.getString(unavailableStickerPacks.getColumnIndexOrThrow(AttachmentDatabase.STICKER_PACK_KEY))));
            } catch (Throwable th) {
                try {
                    unavailableStickerPacks.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (unavailableStickerPacks != null) {
            unavailableStickerPacks.close();
        }
    }

    public void getStickerPacks(Callback<PackResult> callback) {
        SignalExecutors.SERIAL.execute(new Runnable(callback) { // from class: org.thoughtcrime.securesms.stickers.StickerManagementRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ StickerManagementRepository.Callback f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                StickerManagementRepository.this.lambda$getStickerPacks$1(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$getStickerPacks$1(Callback callback) {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        StickerDatabase.StickerPackRecordReader stickerPackRecordReader = new StickerDatabase.StickerPackRecordReader(this.stickerDatabase.getAllStickerPacks());
        while (true) {
            try {
                StickerPackRecord next = stickerPackRecordReader.getNext();
                if (next == null) {
                    stickerPackRecordReader.close();
                    callback.onComplete(new PackResult(arrayList, arrayList2, arrayList3));
                    return;
                } else if (next.isInstalled()) {
                    arrayList.add(next);
                } else if (BlessedPacks.contains(next.getPackId())) {
                    arrayList3.add(next);
                } else {
                    arrayList2.add(next);
                }
            } catch (Throwable th) {
                try {
                    stickerPackRecordReader.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
    }

    public void uninstallStickerPack(String str, String str2) {
        SignalExecutors.SERIAL.execute(new Runnable(str, str2) { // from class: org.thoughtcrime.securesms.stickers.StickerManagementRepository$$ExternalSyntheticLambda4
            public final /* synthetic */ String f$1;
            public final /* synthetic */ String f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                StickerManagementRepository.this.lambda$uninstallStickerPack$2(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$uninstallStickerPack$2(String str, String str2) {
        this.stickerDatabase.uninstallPack(str);
        if (TextSecurePreferences.isMultiDevice(this.context)) {
            ApplicationDependencies.getJobManager().add(new MultiDeviceStickerPackOperationJob(str, str2, MultiDeviceStickerPackOperationJob.Type.REMOVE));
        }
    }

    public void installStickerPack(String str, String str2, boolean z) {
        SignalExecutors.SERIAL.execute(new Runnable(str, z, str2) { // from class: org.thoughtcrime.securesms.stickers.StickerManagementRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$1;
            public final /* synthetic */ boolean f$2;
            public final /* synthetic */ String f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                StickerManagementRepository.this.lambda$installStickerPack$3(this.f$1, this.f$2, this.f$3);
            }
        });
    }

    public /* synthetic */ void lambda$installStickerPack$3(String str, boolean z, String str2) {
        JobManager jobManager = ApplicationDependencies.getJobManager();
        if (this.stickerDatabase.isPackAvailableAsReference(str)) {
            this.stickerDatabase.markPackAsInstalled(str, z);
        }
        jobManager.add(StickerPackDownloadJob.forInstall(str, str2, z));
        if (TextSecurePreferences.isMultiDevice(this.context)) {
            jobManager.add(new MultiDeviceStickerPackOperationJob(str, str2, MultiDeviceStickerPackOperationJob.Type.INSTALL));
        }
    }

    public void setPackOrder(List<StickerPackRecord> list) {
        SignalExecutors.SERIAL.execute(new Runnable(list) { // from class: org.thoughtcrime.securesms.stickers.StickerManagementRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ List f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                StickerManagementRepository.this.lambda$setPackOrder$4(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$setPackOrder$4(List list) {
        this.stickerDatabase.updatePackOrder(list);
    }

    /* loaded from: classes4.dex */
    public static class PackResult {
        private final List<StickerPackRecord> availablePacks;
        private final List<StickerPackRecord> blessedPacks;
        private final List<StickerPackRecord> installedPacks;

        PackResult(List<StickerPackRecord> list, List<StickerPackRecord> list2, List<StickerPackRecord> list3) {
            this.installedPacks = list;
            this.availablePacks = list2;
            this.blessedPacks = list3;
        }

        public List<StickerPackRecord> getInstalledPacks() {
            return this.installedPacks;
        }

        public List<StickerPackRecord> getAvailablePacks() {
            return this.availablePacks;
        }

        public List<StickerPackRecord> getBlessedPacks() {
            return this.blessedPacks;
        }
    }
}
