package org.thoughtcrime.securesms.stickers;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentResultListener;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.TransitionOptions;
import com.bumptech.glide.load.Option;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import j$.util.Optional;
import java.util.Collections;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragmentArgs;
import org.thoughtcrime.securesms.glide.cache.ApngOptions;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.sharing.MultiShareArgs;
import org.thoughtcrime.securesms.stickers.StickerManifest;
import org.thoughtcrime.securesms.stickers.StickerPackPreviewAdapter;
import org.thoughtcrime.securesms.stickers.StickerPackPreviewRepository;
import org.thoughtcrime.securesms.stickers.StickerPackPreviewViewModel;
import org.thoughtcrime.securesms.stickers.StickerRolloverTouchListener;
import org.thoughtcrime.securesms.util.DeviceProperties;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.whispersystems.signalservice.api.util.OptionalUtil;

/* loaded from: classes4.dex */
public final class StickerPackPreviewActivity extends PassphraseRequiredActivity implements StickerRolloverTouchListener.RolloverEventListener, StickerRolloverTouchListener.RolloverStickerRetriever, StickerPackPreviewAdapter.EventListener {
    private static final String TAG = Log.tag(StickerPackPreviewActivity.class);
    private StickerPackPreviewAdapter adapter;
    private ImageView coverImage;
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();
    private View installButton;
    private GridLayoutManager layoutManager;
    private View removeButton;
    private View shareButton;
    private View shareButtonImage;
    private TextView stickerAuthor;
    private RecyclerView stickerList;
    private TextView stickerTitle;
    private StickerRolloverTouchListener touchListener;
    private StickerPackPreviewViewModel viewModel;

    @Override // org.thoughtcrime.securesms.stickers.StickerRolloverTouchListener.RolloverEventListener
    public void onStickerPopupEnded() {
    }

    @Override // org.thoughtcrime.securesms.stickers.StickerRolloverTouchListener.RolloverEventListener
    public void onStickerPopupStarted() {
    }

    public static Intent getIntent(String str, String str2) {
        Intent intent = new Intent("android.intent.action.VIEW", StickerUrl.createActionUri(str, str2));
        intent.addCategory("android.intent.category.DEFAULT");
        intent.addCategory("android.intent.category.BROWSABLE");
        return intent;
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onPreCreate() {
        super.onPreCreate();
        this.dynamicTheme.onCreate(this);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        setContentView(R.layout.sticker_preview_activity);
        Optional<Pair<String, String>> parseExternalUri = StickerUrl.parseExternalUri(getIntent().getData());
        if (!parseExternalUri.isPresent()) {
            Log.w(TAG, "Invalid URI!");
            presentError();
            return;
        }
        initToolbar();
        initView();
        initViewModel(parseExternalUri.get().first(), parseExternalUri.get().second());
        getSupportFragmentManager().setFragmentResultListener(MultiselectForwardFragment.RESULT_KEY, this, new FragmentResultListener() { // from class: org.thoughtcrime.securesms.stickers.StickerPackPreviewActivity$$ExternalSyntheticLambda4
            @Override // androidx.fragment.app.FragmentResultListener
            public final void onFragmentResult(String str, Bundle bundle2) {
                StickerPackPreviewActivity.this.lambda$onCreate$0(str, bundle2);
            }
        });
    }

    public /* synthetic */ void lambda$onCreate$0(String str, Bundle bundle) {
        if (bundle.getBoolean(MultiselectForwardFragment.RESULT_SENT, false)) {
            finish();
        }
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        onScreenWidthChanged(getScreenWidth());
    }

    @Override // org.thoughtcrime.securesms.stickers.StickerPackPreviewAdapter.EventListener
    public void onStickerLongPress(View view) {
        StickerRolloverTouchListener stickerRolloverTouchListener = this.touchListener;
        if (stickerRolloverTouchListener != null) {
            stickerRolloverTouchListener.enterHoverMode(this.stickerList, view);
        }
    }

    @Override // org.thoughtcrime.securesms.stickers.StickerRolloverTouchListener.RolloverStickerRetriever
    public Pair<Object, String> getStickerDataFromView(View view) {
        StickerPackPreviewAdapter.StickerViewHolder stickerViewHolder;
        RecyclerView recyclerView = this.stickerList;
        if (recyclerView == null || (stickerViewHolder = (StickerPackPreviewAdapter.StickerViewHolder) recyclerView.getChildViewHolder(view)) == null) {
            return null;
        }
        return new Pair<>(stickerViewHolder.getCurrentGlideModel(), stickerViewHolder.getCurrentEmoji());
    }

    private void initView() {
        this.coverImage = (ImageView) findViewById(R.id.sticker_install_cover);
        this.stickerTitle = (TextView) findViewById(R.id.sticker_install_title);
        this.stickerAuthor = (TextView) findViewById(R.id.sticker_install_author);
        this.installButton = findViewById(R.id.sticker_install_button);
        this.removeButton = findViewById(R.id.sticker_install_remove_button);
        this.stickerList = (RecyclerView) findViewById(R.id.sticker_install_list);
        this.shareButton = findViewById(R.id.sticker_install_share_button);
        this.shareButtonImage = findViewById(R.id.sticker_install_share_button_image);
        this.adapter = new StickerPackPreviewAdapter(GlideApp.with((FragmentActivity) this), this, DeviceProperties.shouldAllowApngStickerAnimation(this));
        this.layoutManager = new GridLayoutManager(this, 2);
        this.touchListener = new StickerRolloverTouchListener(this, GlideApp.with((FragmentActivity) this), this, this);
        onScreenWidthChanged(getScreenWidth());
        this.stickerList.setLayoutManager(this.layoutManager);
        this.stickerList.addOnItemTouchListener(this.touchListener);
        this.stickerList.setAdapter(this.adapter);
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.sticker_install_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.StickerPackPreviewActivity_stickers);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stickers.StickerPackPreviewActivity$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StickerPackPreviewActivity.this.lambda$initToolbar$1(view);
            }
        });
    }

    public /* synthetic */ void lambda$initToolbar$1(View view) {
        onBackPressed();
    }

    private void initViewModel(String str, String str2) {
        StickerPackPreviewViewModel stickerPackPreviewViewModel = (StickerPackPreviewViewModel) ViewModelProviders.of(this, new StickerPackPreviewViewModel.Factory(getApplication(), new StickerPackPreviewRepository(this), new StickerManagementRepository(this))).get(StickerPackPreviewViewModel.class);
        this.viewModel = stickerPackPreviewViewModel;
        stickerPackPreviewViewModel.getStickerManifest(str, str2).observe(this, new Observer() { // from class: org.thoughtcrime.securesms.stickers.StickerPackPreviewActivity$$ExternalSyntheticLambda5
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                StickerPackPreviewActivity.this.lambda$initViewModel$2((Optional) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initViewModel$2(Optional optional) {
        if (optional != null) {
            if (optional.isPresent()) {
                presentManifest(((StickerPackPreviewRepository.StickerManifestResult) optional.get()).getManifest());
                presentButton(((StickerPackPreviewRepository.StickerManifestResult) optional.get()).isInstalled());
                presentShareButton(((StickerPackPreviewRepository.StickerManifestResult) optional.get()).isInstalled(), ((StickerPackPreviewRepository.StickerManifestResult) optional.get()).getManifest().getPackId(), ((StickerPackPreviewRepository.StickerManifestResult) optional.get()).getManifest().getPackKey());
                return;
            }
            presentError();
        }
    }

    /* JADX DEBUG: Type inference failed for r0v11. Raw type applied. Possible types: com.bumptech.glide.load.Option<java.lang.Boolean>, com.bumptech.glide.load.Option<Y> */
    private void presentManifest(StickerManifest stickerManifest) {
        Object obj;
        this.stickerTitle.setText(stickerManifest.getTitle().orElse(getString(R.string.StickerPackPreviewActivity_untitled)));
        this.stickerAuthor.setText(stickerManifest.getAuthor().orElse(getString(R.string.StickerPackPreviewActivity_unknown)));
        this.adapter.setStickers(stickerManifest.getStickers());
        StickerManifest.Sticker sticker = (StickerManifest.Sticker) OptionalUtil.or(stickerManifest.getCover(), Optional.ofNullable(stickerManifest.getStickers().isEmpty() ? null : stickerManifest.getStickers().get(0))).orElse(null);
        if (sticker != null) {
            if (sticker.getUri().isPresent()) {
                obj = new DecryptableStreamUriLoader.DecryptableUri(sticker.getUri().get());
            } else {
                obj = new StickerRemoteUri(sticker.getPackId(), sticker.getPackKey(), sticker.getId());
            }
            GlideApp.with((FragmentActivity) this).load(obj).transition((TransitionOptions<?, ? super Drawable>) DrawableTransitionOptions.withCrossFade()).set((Option<Option>) ApngOptions.ANIMATE, (Option) Boolean.valueOf(DeviceProperties.shouldAllowApngStickerAnimation(this))).into(this.coverImage);
            return;
        }
        this.coverImage.setImageDrawable(null);
    }

    private void presentButton(boolean z) {
        if (z) {
            this.removeButton.setVisibility(0);
            this.removeButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stickers.StickerPackPreviewActivity$$ExternalSyntheticLambda1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    StickerPackPreviewActivity.this.lambda$presentButton$3(view);
                }
            });
            this.installButton.setVisibility(8);
            this.installButton.setOnClickListener(null);
            return;
        }
        this.installButton.setVisibility(0);
        this.installButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.stickers.StickerPackPreviewActivity$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                StickerPackPreviewActivity.this.lambda$presentButton$4(view);
            }
        });
        this.removeButton.setVisibility(8);
        this.removeButton.setOnClickListener(null);
    }

    public /* synthetic */ void lambda$presentButton$3(View view) {
        this.viewModel.onRemoveClicked();
        finish();
    }

    public /* synthetic */ void lambda$presentButton$4(View view) {
        this.viewModel.onInstallClicked();
        finish();
    }

    private void presentShareButton(boolean z, String str, String str2) {
        if (z) {
            this.shareButton.setVisibility(0);
            this.shareButtonImage.setVisibility(0);
            this.shareButton.setOnClickListener(new View.OnClickListener(str, str2) { // from class: org.thoughtcrime.securesms.stickers.StickerPackPreviewActivity$$ExternalSyntheticLambda3
                public final /* synthetic */ String f$1;
                public final /* synthetic */ String f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    StickerPackPreviewActivity.this.lambda$presentShareButton$5(this.f$1, this.f$2, view);
                }
            });
            return;
        }
        this.shareButton.setVisibility(8);
        this.shareButtonImage.setVisibility(8);
        this.shareButton.setOnClickListener(null);
    }

    public /* synthetic */ void lambda$presentShareButton$5(String str, String str2, View view) {
        MultiselectForwardFragment.showBottomSheet(getSupportFragmentManager(), new MultiselectForwardFragmentArgs(true, Collections.singletonList(new MultiShareArgs.Builder().withDraftText(StickerUrl.createShareLink(str, str2)).build()), R.string.MultiselectForwardFragment__share_with));
    }

    private void presentError() {
        Toast.makeText(this, (int) R.string.StickerPackPreviewActivity_failed_to_load_sticker_pack, 0).show();
        finish();
    }

    private void onScreenWidthChanged(int i) {
        if (this.layoutManager != null) {
            this.layoutManager.setSpanCount((i - (getResources().getDimensionPixelOffset(R.dimen.sticker_preview_gutter_size) * 2)) / getResources().getDimensionPixelOffset(R.dimen.sticker_preview_sticker_size));
        }
    }

    private int getScreenWidth() {
        Point point = new Point();
        getWindowManager().getDefaultDisplay().getSize(point);
        return point.x;
    }
}
