package org.thoughtcrime.securesms.stickers;

/* loaded from: classes4.dex */
public class StickerPackInstallEvent {
    private final Object iconGlideModel;

    public StickerPackInstallEvent(Object obj) {
        this.iconGlideModel = obj;
    }

    public Object getIconGlideModel() {
        return this.iconGlideModel;
    }
}
