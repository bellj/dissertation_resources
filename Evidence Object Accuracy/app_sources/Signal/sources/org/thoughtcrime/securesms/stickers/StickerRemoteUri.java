package org.thoughtcrime.securesms.stickers;

import com.bumptech.glide.load.Key;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.util.Objects;

/* loaded from: classes4.dex */
public final class StickerRemoteUri implements Key {
    private final String packId;
    private final String packKey;
    private final int stickerId;

    public StickerRemoteUri(String str, String str2, int i) {
        this.packId = str;
        this.packKey = str2;
        this.stickerId = i;
    }

    public String getPackId() {
        return this.packId;
    }

    public String getPackKey() {
        return this.packKey;
    }

    public int getStickerId() {
        return this.stickerId;
    }

    @Override // com.bumptech.glide.load.Key
    public void updateDiskCacheKey(MessageDigest messageDigest) {
        messageDigest.update(this.packId.getBytes());
        messageDigest.update(this.packKey.getBytes());
        messageDigest.update(ByteBuffer.allocate(4).putInt(this.stickerId).array());
    }

    @Override // com.bumptech.glide.load.Key
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || StickerRemoteUri.class != obj.getClass()) {
            return false;
        }
        StickerRemoteUri stickerRemoteUri = (StickerRemoteUri) obj;
        if (this.stickerId != stickerRemoteUri.stickerId || !Objects.equals(this.packId, stickerRemoteUri.packId) || !Objects.equals(this.packKey, stickerRemoteUri.packKey)) {
            return false;
        }
        return true;
    }

    @Override // com.bumptech.glide.load.Key
    public int hashCode() {
        return Objects.hash(this.packId, this.packKey, Integer.valueOf(this.stickerId));
    }
}
