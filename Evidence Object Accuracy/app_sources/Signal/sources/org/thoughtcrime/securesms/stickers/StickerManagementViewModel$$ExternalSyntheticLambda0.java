package org.thoughtcrime.securesms.stickers;

import androidx.lifecycle.MutableLiveData;
import org.thoughtcrime.securesms.stickers.StickerManagementRepository;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class StickerManagementViewModel$$ExternalSyntheticLambda0 implements StickerManagementRepository.Callback {
    public final /* synthetic */ MutableLiveData f$0;

    public /* synthetic */ StickerManagementViewModel$$ExternalSyntheticLambda0(MutableLiveData mutableLiveData) {
        this.f$0 = mutableLiveData;
    }

    @Override // org.thoughtcrime.securesms.stickers.StickerManagementRepository.Callback
    public final void onComplete(Object obj) {
        this.f$0.postValue((StickerManagementRepository.PackResult) obj);
    }
}
