package org.thoughtcrime.securesms.stickers;

import android.content.Context;
import android.database.Cursor;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import j$.util.Optional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.signal.core.util.Hex;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.StickerDatabase;
import org.thoughtcrime.securesms.database.model.StickerPackRecord;
import org.thoughtcrime.securesms.database.model.StickerRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.stickers.StickerManifest;
import org.thoughtcrime.securesms.stickers.StickerPackPreviewRepository;
import org.whispersystems.signalservice.api.SignalServiceMessageReceiver;
import org.whispersystems.signalservice.api.messages.SignalServiceStickerManifest;

/* loaded from: classes4.dex */
public final class StickerPackPreviewRepository {
    private static final String TAG = Log.tag(StickerPackPreviewRepository.class);
    private final SignalServiceMessageReceiver receiver = ApplicationDependencies.getSignalServiceMessageReceiver();
    private final StickerDatabase stickerDatabase = SignalDatabase.stickers();

    /* loaded from: classes4.dex */
    public interface Callback<T> {
        void onComplete(T t);
    }

    public StickerPackPreviewRepository(Context context) {
    }

    public void getStickerManifest(String str, String str2, Callback<Optional<StickerManifestResult>> callback) {
        SignalExecutors.UNBOUNDED.execute(new Runnable(str, callback, str2) { // from class: org.thoughtcrime.securesms.stickers.StickerPackPreviewRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$1;
            public final /* synthetic */ StickerPackPreviewRepository.Callback f$2;
            public final /* synthetic */ String f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                StickerPackPreviewRepository.this.lambda$getStickerManifest$0(this.f$1, this.f$2, this.f$3);
            }
        });
    }

    public /* synthetic */ void lambda$getStickerManifest$0(String str, Callback callback, String str2) {
        Optional<StickerManifestResult> manifestFromDatabase = getManifestFromDatabase(str);
        if (manifestFromDatabase.isPresent()) {
            Log.d(TAG, "Found manifest locally.");
            callback.onComplete(manifestFromDatabase);
            return;
        }
        Log.d(TAG, "Looking for manifest remotely.");
        callback.onComplete(getManifestRemote(str, str2));
    }

    private Optional<StickerManifestResult> getManifestFromDatabase(String str) {
        StickerPackRecord stickerPack = this.stickerDatabase.getStickerPack(str);
        if (stickerPack == null || !stickerPack.isInstalled()) {
            return Optional.empty();
        }
        StickerManifest.Sticker sticker = toSticker(stickerPack.getCover());
        return Optional.of(new StickerManifestResult(new StickerManifest(stickerPack.getPackId(), stickerPack.getPackKey(), stickerPack.getTitle(), stickerPack.getAuthor(), Optional.of(sticker), getStickersFromDatabase(str)), stickerPack.isInstalled()));
    }

    private Optional<StickerManifestResult> getManifestRemote(String str, String str2) {
        try {
            SignalServiceStickerManifest retrieveStickerManifest = this.receiver.retrieveStickerManifest(Hex.fromStringCondensed(str), Hex.fromStringCondensed(str2));
            return Optional.of(new StickerManifestResult(new StickerManifest(str, str2, retrieveStickerManifest.getTitle(), retrieveStickerManifest.getAuthor(), toOptionalSticker(str, str2, retrieveStickerManifest.getCover()), Stream.of(retrieveStickerManifest.getStickers()).map(new Function(str, str2) { // from class: org.thoughtcrime.securesms.stickers.StickerPackPreviewRepository$$ExternalSyntheticLambda1
                public final /* synthetic */ String f$1;
                public final /* synthetic */ String f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return StickerPackPreviewRepository.this.lambda$getManifestRemote$1(this.f$1, this.f$2, (SignalServiceStickerManifest.StickerInfo) obj);
                }
            }).toList()), false));
        } catch (IOException | InvalidMessageException e) {
            Log.w(TAG, "Failed to retrieve pack manifest.", e);
            return Optional.empty();
        }
    }

    private List<StickerManifest.Sticker> getStickersFromDatabase(String str) {
        ArrayList arrayList = new ArrayList();
        Cursor stickersForPack = this.stickerDatabase.getStickersForPack(str);
        try {
            StickerDatabase.StickerRecordReader stickerRecordReader = new StickerDatabase.StickerRecordReader(stickersForPack);
            while (true) {
                StickerRecord next = stickerRecordReader.getNext();
                if (next == null) {
                    break;
                }
                arrayList.add(toSticker(next));
            }
            if (stickersForPack != null) {
                stickersForPack.close();
            }
            return arrayList;
        } catch (Throwable th) {
            if (stickersForPack != null) {
                try {
                    stickersForPack.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    private Optional<StickerManifest.Sticker> toOptionalSticker(String str, String str2, Optional<SignalServiceStickerManifest.StickerInfo> optional) {
        if (optional.isPresent()) {
            return Optional.of(lambda$getManifestRemote$1(str, str2, optional.get()));
        }
        return Optional.empty();
    }

    /* renamed from: toSticker */
    public StickerManifest.Sticker lambda$getManifestRemote$1(String str, String str2, SignalServiceStickerManifest.StickerInfo stickerInfo) {
        return new StickerManifest.Sticker(str, str2, stickerInfo.getId(), stickerInfo.getEmoji(), stickerInfo.getContentType());
    }

    private StickerManifest.Sticker toSticker(StickerRecord stickerRecord) {
        return new StickerManifest.Sticker(stickerRecord.getPackId(), stickerRecord.getPackKey(), stickerRecord.getStickerId(), stickerRecord.getEmoji(), stickerRecord.getContentType(), stickerRecord.getUri());
    }

    /* loaded from: classes4.dex */
    public static class StickerManifestResult {
        private final boolean isInstalled;
        private final StickerManifest manifest;

        StickerManifestResult(StickerManifest stickerManifest, boolean z) {
            this.manifest = stickerManifest;
            this.isInstalled = z;
        }

        public StickerManifest getManifest() {
            return this.manifest;
        }

        public boolean isInstalled() {
            return this.isInstalled;
        }
    }
}
