package org.thoughtcrime.securesms.stickers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.mms.GlideRequests;

/* loaded from: classes4.dex */
public final class StickerPreviewPopup extends PopupWindow {
    private final TextView emojiText = ((TextView) getContentView().findViewById(R.id.sticker_popup_emoji));
    private final GlideRequests glideRequests;
    private final ImageView image = ((ImageView) getContentView().findViewById(R.id.sticker_popup_image));

    public StickerPreviewPopup(Context context, GlideRequests glideRequests) {
        super(LayoutInflater.from(context).inflate(R.layout.sticker_preview_popup, (ViewGroup) null), -1, -1);
        this.glideRequests = glideRequests;
        setTouchable(false);
    }

    public void presentSticker(Object obj, String str) {
        this.emojiText.setText(str);
        this.glideRequests.load(obj).diskCacheStrategy(DiskCacheStrategy.NONE).into(this.image);
    }
}
