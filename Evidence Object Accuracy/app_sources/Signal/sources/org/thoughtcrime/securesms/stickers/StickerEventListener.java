package org.thoughtcrime.securesms.stickers;

import org.thoughtcrime.securesms.database.model.StickerRecord;

/* loaded from: classes4.dex */
public interface StickerEventListener {
    void onStickerManagementClicked();

    void onStickerSelected(StickerRecord stickerRecord);
}
