package org.thoughtcrime.securesms.stickers;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import org.whispersystems.signalservice.internal.util.JsonUtil;

/* loaded from: classes4.dex */
public final class BlessedPacks {
    public static final Pack BANDIT = new Pack("9acc9e8aba563d26a4994e69263e3b25", "5a6dff3948c28efb9b7aaf93ecc375c69fc316e78077ed26867a14d10a0f6a12");
    private static final Set<String> BLESSED_PACK_IDS = new HashSet<String>() { // from class: org.thoughtcrime.securesms.stickers.BlessedPacks.1
        {
            add(BlessedPacks.ZOZO.getPackId());
            add(BlessedPacks.BANDIT.getPackId());
            add(BlessedPacks.SWOON_HANDS.getPackId());
            add(BlessedPacks.SWOON_FACES.getPackId());
            add(BlessedPacks.DAY_BY_DAY.getPackId());
            add(BlessedPacks.MY_DAILY_LIFE.getPackId());
        }
    };
    public static final Pack DAY_BY_DAY = new Pack("cfc50156556893ef9838069d3890fe49", "5f5beab7d382443cb00a1e48eb95297b6b8cadfd0631e5d0d9dc949e6999ff4b");
    public static final Pack MY_DAILY_LIFE = new Pack("ccc89a05dc077856b57351e90697976c", "45730e60f09d5566115223744537a6b7d9ea99ceeacb77a1fbd6801b9607fbcf");
    public static final Pack SWOON_FACES = new Pack("cca32f5b905208b7d0f1e17f23fdc185", "8bf8e95f7a45bdeafe0c8f5b002ef01ab95b8f1b5baac4019ccd6b6be0b1837a");
    public static final Pack SWOON_HANDS = new Pack("e61fa0867031597467ccc036cc65d403", "13ae7b1a7407318280e9b38c1261ded38e0e7138b9f964a6ccbb73e40f737a9b");
    public static final Pack ZOZO = new Pack("fb535407d2f6497ec074df8b9c51dd1d", "17e971c134035622781d2ee249e6473b774583750b68c11bb82b7509c68b6dfd");

    public static boolean contains(String str) {
        return BLESSED_PACK_IDS.contains(str);
    }

    /* loaded from: classes.dex */
    public static class Pack {
        @JsonProperty
        private final String packId;
        @JsonProperty
        private final String packKey;

        public Pack(@JsonProperty("packId") String str, @JsonProperty("packKey") String str2) {
            this.packId = str;
            this.packKey = str2;
        }

        public String getPackId() {
            return this.packId;
        }

        public String getPackKey() {
            return this.packKey;
        }

        public String toJson() {
            return JsonUtil.toJson(this);
        }

        public static Pack fromJson(String str) {
            try {
                return (Pack) JsonUtil.fromJson(str, Pack.class);
            } catch (IOException e) {
                throw new AssertionError(e);
            }
        }
    }
}
