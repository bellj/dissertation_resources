package org.thoughtcrime.securesms.stickers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentResultListener;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Collections;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragmentArgs;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.sharing.MultiShareArgs;
import org.thoughtcrime.securesms.stickers.StickerManagementAdapter;
import org.thoughtcrime.securesms.stickers.StickerManagementItemTouchHelper;
import org.thoughtcrime.securesms.stickers.StickerManagementRepository;
import org.thoughtcrime.securesms.stickers.StickerManagementViewModel;
import org.thoughtcrime.securesms.util.DeviceProperties;
import org.thoughtcrime.securesms.util.DynamicTheme;

/* loaded from: classes4.dex */
public final class StickerManagementActivity extends PassphraseRequiredActivity implements StickerManagementAdapter.EventListener {
    private StickerManagementAdapter adapter;
    private final DynamicTheme dynamicTheme = new DynamicTheme();
    private RecyclerView list;
    private StickerManagementViewModel viewModel;

    public static Intent getIntent(Context context) {
        return new Intent(context, StickerManagementActivity.class);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onPreCreate() {
        super.onPreCreate();
        this.dynamicTheme.onCreate(this);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        setContentView(R.layout.sticker_management_activity);
        initView();
        initToolbar();
        initViewModel();
        getSupportFragmentManager().setFragmentResultListener(MultiselectForwardFragment.RESULT_KEY, this, new FragmentResultListener() { // from class: org.thoughtcrime.securesms.stickers.StickerManagementActivity$$ExternalSyntheticLambda1
            @Override // androidx.fragment.app.FragmentResultListener
            public final void onFragmentResult(String str, Bundle bundle2) {
                StickerManagementActivity.this.lambda$onCreate$0(str, bundle2);
            }
        });
    }

    public /* synthetic */ void lambda$onCreate$0(String str, Bundle bundle) {
        if (bundle.getBoolean(MultiselectForwardFragment.RESULT_SENT, false)) {
            finish();
        }
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onStart() {
        super.onStart();
        this.viewModel.onVisible();
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }

    @Override // android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        onBackPressed();
        return true;
    }

    @Override // org.thoughtcrime.securesms.stickers.StickerManagementAdapter.EventListener
    public void onStickerPackClicked(String str, String str2) {
        startActivity(StickerPackPreviewActivity.getIntent(str, str2));
    }

    @Override // org.thoughtcrime.securesms.stickers.StickerManagementAdapter.EventListener
    public void onStickerPackUninstallClicked(String str, String str2) {
        this.viewModel.onStickerPackUninstallClicked(str, str2);
    }

    @Override // org.thoughtcrime.securesms.stickers.StickerManagementAdapter.EventListener
    public void onStickerPackInstallClicked(String str, String str2) {
        this.viewModel.onStickerPackInstallClicked(str, str2);
    }

    @Override // org.thoughtcrime.securesms.stickers.StickerManagementAdapter.EventListener
    public void onStickerPackShareClicked(String str, String str2) {
        MultiselectForwardFragment.showBottomSheet(getSupportFragmentManager(), new MultiselectForwardFragmentArgs(true, Collections.singletonList(new MultiShareArgs.Builder().withDraftText(StickerUrl.createShareLink(str, str2)).build()), R.string.MultiselectForwardFragment__share_with));
    }

    private void initView() {
        this.list = (RecyclerView) findViewById(R.id.sticker_management_list);
        this.adapter = new StickerManagementAdapter(GlideApp.with((FragmentActivity) this), this, DeviceProperties.shouldAllowApngStickerAnimation(this));
        this.list.setLayoutManager(new LinearLayoutManager(this));
        this.list.setAdapter(this.adapter);
        new ItemTouchHelper(new StickerManagementItemTouchHelper(new ItemTouchCallback())).attachToRecyclerView(this.list);
    }

    private void initToolbar() {
        getSupportActionBar().setTitle(R.string.StickerManagementActivity_stickers);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initViewModel() {
        StickerManagementViewModel stickerManagementViewModel = (StickerManagementViewModel) ViewModelProviders.of(this, new StickerManagementViewModel.Factory(getApplication(), new StickerManagementRepository(this))).get(StickerManagementViewModel.class);
        this.viewModel = stickerManagementViewModel;
        stickerManagementViewModel.init();
        this.viewModel.getStickerPacks().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.stickers.StickerManagementActivity$$ExternalSyntheticLambda0
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                StickerManagementActivity.this.lambda$initViewModel$1((StickerManagementRepository.PackResult) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initViewModel$1(StickerManagementRepository.PackResult packResult) {
        if (packResult != null) {
            this.adapter.setPackLists(packResult.getInstalledPacks(), packResult.getAvailablePacks(), packResult.getBlessedPacks());
        }
    }

    /* loaded from: classes4.dex */
    public class ItemTouchCallback implements StickerManagementItemTouchHelper.Callback {
        private ItemTouchCallback() {
            StickerManagementActivity.this = r1;
        }

        @Override // org.thoughtcrime.securesms.stickers.StickerManagementItemTouchHelper.Callback
        public boolean onMove(int i, int i2) {
            return StickerManagementActivity.this.adapter.onMove(i, i2);
        }

        @Override // org.thoughtcrime.securesms.stickers.StickerManagementItemTouchHelper.Callback
        public boolean isMovable(int i) {
            return StickerManagementActivity.this.adapter.isMovable(i);
        }

        @Override // org.thoughtcrime.securesms.stickers.StickerManagementItemTouchHelper.Callback
        public void onMoveCommitted() {
            StickerManagementActivity.this.viewModel.onOrderChanged(StickerManagementActivity.this.adapter.getInstalledPacksInOrder());
        }
    }
}
