package org.thoughtcrime.securesms.stickers;

import android.view.View;
import org.thoughtcrime.securesms.stickers.StickerPackPreviewAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class StickerPackPreviewAdapter$StickerViewHolder$$ExternalSyntheticLambda0 implements View.OnLongClickListener {
    public final /* synthetic */ StickerPackPreviewAdapter.EventListener f$0;

    public /* synthetic */ StickerPackPreviewAdapter$StickerViewHolder$$ExternalSyntheticLambda0(StickerPackPreviewAdapter.EventListener eventListener) {
        this.f$0 = eventListener;
    }

    @Override // android.view.View.OnLongClickListener
    public final boolean onLongClick(View view) {
        return this.f$0.onStickerLongPress(view);
    }
}
