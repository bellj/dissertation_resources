package org.thoughtcrime.securesms.stickers;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.TransitionOptions;
import com.bumptech.glide.load.Option;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiTextView;
import org.thoughtcrime.securesms.database.model.StickerPackRecord;
import org.thoughtcrime.securesms.glide.cache.ApngOptions;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.util.adapter.SectionedRecyclerViewAdapter;
import org.thoughtcrime.securesms.util.adapter.StableIdGenerator;

/* loaded from: classes4.dex */
public final class StickerManagementAdapter extends SectionedRecyclerViewAdapter<String, StickerSection> {
    private static final String TAG_BLESSED_STICKERS;
    private static final String TAG_MESSAGE_STICKERS;
    private static final String TAG_YOUR_STICKERS;
    private final boolean allowApngAnimation;
    private final EventListener eventListener;
    private final GlideRequests glideRequests;
    private final List<StickerSection> sections = new ArrayList<StickerSection>(3) { // from class: org.thoughtcrime.securesms.stickers.StickerManagementAdapter.1
        {
            StickerSection stickerSection = new StickerSection(StickerManagementAdapter.TAG_YOUR_STICKERS, R.string.StickerManagementAdapter_installed_stickers, R.string.StickerManagementAdapter_no_stickers_installed, new ArrayList(), 0);
            StickerSection stickerSection2 = new StickerSection(StickerManagementAdapter.TAG_MESSAGE_STICKERS, R.string.StickerManagementAdapter_stickers_you_received, R.string.StickerManagementAdapter_stickers_from_incoming_messages_will_appear_here, new ArrayList(), stickerSection.size());
            add(stickerSection);
            add(stickerSection2);
        }
    };

    /* loaded from: classes4.dex */
    public interface EventListener {
        void onStickerPackClicked(String str, String str2);

        void onStickerPackInstallClicked(String str, String str2);

        void onStickerPackShareClicked(String str, String str2);

        void onStickerPackUninstallClicked(String str, String str2);
    }

    public StickerManagementAdapter(GlideRequests glideRequests, EventListener eventListener, boolean z) {
        this.glideRequests = glideRequests;
        this.eventListener = eventListener;
        this.allowApngAnimation = z;
    }

    protected List<StickerSection> getSections() {
        return this.sections;
    }

    protected RecyclerView.ViewHolder createHeaderViewHolder(ViewGroup viewGroup) {
        return new HeaderViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sticker_management_header_item, viewGroup, false));
    }

    protected RecyclerView.ViewHolder createContentViewHolder(ViewGroup viewGroup) {
        return new StickerViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sticker_management_sticker_item, viewGroup, false));
    }

    protected RecyclerView.ViewHolder createEmptyViewHolder(ViewGroup viewGroup) {
        return new EmptyViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sticker_management_empty_item, viewGroup, false));
    }

    public void bindViewHolder(RecyclerView.ViewHolder viewHolder, StickerSection stickerSection, int i) {
        stickerSection.bindViewHolder(viewHolder, i, this.glideRequests, this.eventListener, this.allowApngAnimation);
    }

    public void onViewRecycled(RecyclerView.ViewHolder viewHolder) {
        if (viewHolder instanceof StickerViewHolder) {
            ((StickerViewHolder) viewHolder).recycle();
        }
    }

    public boolean onMove(int i, int i2) {
        StickerSection stickerSection = this.sections.get(0);
        if (!stickerSection.isContent(i) || !stickerSection.isContent(i2)) {
            return false;
        }
        stickerSection.swap(i, i2);
        notifyItemMoved(i, i2);
        return true;
    }

    public boolean isMovable(int i) {
        return this.sections.get(0).isContent(i);
    }

    public List<StickerPackRecord> getInstalledPacksInOrder() {
        return this.sections.get(0).records;
    }

    public void setPackLists(List<StickerPackRecord> list, List<StickerPackRecord> list2, List<StickerPackRecord> list3) {
        StickerSection stickerSection = new StickerSection(TAG_YOUR_STICKERS, R.string.StickerManagementAdapter_installed_stickers, R.string.StickerManagementAdapter_no_stickers_installed, list, 0);
        StickerSection stickerSection2 = new StickerSection(TAG_BLESSED_STICKERS, R.string.StickerManagementAdapter_signal_artist_series, 0, list3, stickerSection.size());
        StickerSection stickerSection3 = new StickerSection(TAG_MESSAGE_STICKERS, R.string.StickerManagementAdapter_stickers_you_received, R.string.StickerManagementAdapter_stickers_from_incoming_messages_will_appear_here, list2, stickerSection.size() + (list3.isEmpty() ? 0 : stickerSection2.size()));
        this.sections.clear();
        this.sections.add(stickerSection);
        if (!list3.isEmpty()) {
            this.sections.add(stickerSection2);
        }
        this.sections.add(stickerSection3);
        notifyDataSetChanged();
    }

    /* loaded from: classes4.dex */
    public static class StickerSection extends SectionedRecyclerViewAdapter.Section<String> {
        private static final String STABLE_ID_HEADER;
        private static final String STABLE_ID_TEXT;
        private final int emptyResId;
        private final List<StickerPackRecord> records;
        private final String tag;
        private final int titleResId;

        public boolean hasEmptyState() {
            return true;
        }

        StickerSection(String str, int i, int i2, List<StickerPackRecord> list, int i3) {
            super(i3);
            this.tag = str;
            this.titleResId = i;
            this.emptyResId = i2;
            this.records = list;
        }

        public int getContentSize() {
            return this.records.size();
        }

        public long getItemId(StableIdGenerator<String> stableIdGenerator, int i) {
            int localPosition = getLocalPosition(i);
            if (localPosition == 0) {
                return stableIdGenerator.getId(this.tag + "_" + STABLE_ID_HEADER);
            } else if (!this.records.isEmpty()) {
                return stableIdGenerator.getId(this.records.get(localPosition - 1).getPackId());
            } else {
                return stableIdGenerator.getId(this.tag + "_text");
            }
        }

        void bindViewHolder(RecyclerView.ViewHolder viewHolder, int i, GlideRequests glideRequests, EventListener eventListener, boolean z) {
            if (i == 0) {
                ((HeaderViewHolder) viewHolder).bind(this.titleResId);
            } else if (this.records.isEmpty()) {
                ((EmptyViewHolder) viewHolder).bind(this.emptyResId);
            } else {
                ((StickerViewHolder) viewHolder).bind(glideRequests, eventListener, this.records.get(i - 1), i == this.records.size(), z);
            }
        }

        void swap(int i, int i2) {
            int localPosition = getLocalPosition(i) - 1;
            int localPosition2 = getLocalPosition(i2) - 1;
            if (localPosition < localPosition2) {
                while (localPosition < localPosition2) {
                    int i3 = localPosition + 1;
                    Collections.swap(this.records, localPosition, i3);
                    localPosition = i3;
                }
                return;
            }
            while (localPosition > localPosition2) {
                Collections.swap(this.records, localPosition, localPosition - 1);
                localPosition--;
            }
        }
    }

    /* loaded from: classes4.dex */
    public static class StickerViewHolder extends RecyclerView.ViewHolder {
        private final View actionButton;
        private final ImageView actionButtonImage;
        private final TextView author;
        private final CharSequence blessedBadge;
        private final ImageView cover;
        private final View divider;
        private final View shareButton;
        private final ImageView shareButtonImage;
        private final EmojiTextView title;

        StickerViewHolder(View view) {
            super(view);
            this.cover = (ImageView) view.findViewById(R.id.sticker_management_cover);
            this.title = (EmojiTextView) view.findViewById(R.id.sticker_management_title);
            this.author = (TextView) view.findViewById(R.id.sticker_management_author);
            this.divider = view.findViewById(R.id.sticker_management_divider);
            this.actionButton = view.findViewById(R.id.sticker_management_action_button);
            this.actionButtonImage = (ImageView) view.findViewById(R.id.sticker_management_action_button_image);
            this.shareButton = view.findViewById(R.id.sticker_management_share_button);
            this.shareButtonImage = (ImageView) view.findViewById(R.id.sticker_management_share_button_image);
            this.blessedBadge = buildBlessedBadge(view.getContext());
        }

        /* JADX DEBUG: Type inference failed for r8v4. Raw type applied. Possible types: com.bumptech.glide.load.Option<java.lang.Boolean>, com.bumptech.glide.load.Option<Y> */
        void bind(GlideRequests glideRequests, EventListener eventListener, StickerPackRecord stickerPackRecord, boolean z, boolean z2) {
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(stickerPackRecord.getTitle().orElse(this.itemView.getResources().getString(R.string.StickerManagementAdapter_untitled)));
            if (BlessedPacks.contains(stickerPackRecord.getPackId())) {
                spannableStringBuilder.append(this.blessedBadge);
            }
            this.title.setText(spannableStringBuilder);
            this.author.setText(stickerPackRecord.getAuthor().orElse(this.itemView.getResources().getString(R.string.StickerManagementAdapter_unknown)));
            this.divider.setVisibility(z ? 8 : 0);
            glideRequests.load((Object) new DecryptableStreamUriLoader.DecryptableUri(stickerPackRecord.getCover().getUri())).transition((TransitionOptions<?, ? super Drawable>) DrawableTransitionOptions.withCrossFade()).set((Option<Option>) ApngOptions.ANIMATE, (Option) Boolean.valueOf(z2)).into(this.cover);
            if (stickerPackRecord.isInstalled()) {
                this.actionButtonImage.setImageResource(R.drawable.ic_x);
                this.actionButton.setOnClickListener(new StickerManagementAdapter$StickerViewHolder$$ExternalSyntheticLambda0(eventListener, stickerPackRecord));
                this.shareButton.setVisibility(0);
                this.shareButtonImage.setVisibility(0);
                this.shareButton.setOnClickListener(new StickerManagementAdapter$StickerViewHolder$$ExternalSyntheticLambda1(eventListener, stickerPackRecord));
            } else {
                this.actionButtonImage.setImageResource(R.drawable.ic_arrow_down);
                this.actionButton.setOnClickListener(new StickerManagementAdapter$StickerViewHolder$$ExternalSyntheticLambda2(eventListener, stickerPackRecord));
                this.shareButton.setVisibility(8);
                this.shareButtonImage.setVisibility(8);
                this.shareButton.setOnClickListener(null);
            }
            this.itemView.setOnClickListener(new StickerManagementAdapter$StickerViewHolder$$ExternalSyntheticLambda3(eventListener, stickerPackRecord));
        }

        public static /* synthetic */ void lambda$bind$0(EventListener eventListener, StickerPackRecord stickerPackRecord, View view) {
            eventListener.onStickerPackUninstallClicked(stickerPackRecord.getPackId(), stickerPackRecord.getPackKey());
        }

        public static /* synthetic */ void lambda$bind$1(EventListener eventListener, StickerPackRecord stickerPackRecord, View view) {
            eventListener.onStickerPackShareClicked(stickerPackRecord.getPackId(), stickerPackRecord.getPackKey());
        }

        public static /* synthetic */ void lambda$bind$2(EventListener eventListener, StickerPackRecord stickerPackRecord, View view) {
            eventListener.onStickerPackInstallClicked(stickerPackRecord.getPackId(), stickerPackRecord.getPackKey());
        }

        public static /* synthetic */ void lambda$bind$3(EventListener eventListener, StickerPackRecord stickerPackRecord, View view) {
            eventListener.onStickerPackClicked(stickerPackRecord.getPackId(), stickerPackRecord.getPackKey());
        }

        void recycle() {
            this.actionButton.setOnClickListener(null);
            this.shareButton.setOnClickListener(null);
            this.itemView.setOnClickListener(null);
        }

        private static CharSequence buildBlessedBadge(Context context) {
            SpannableString spannableString = new SpannableString("  ");
            Drawable drawable = ContextCompat.getDrawable(context, R.drawable.ic_check_circle_white_18dp);
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            drawable.setColorFilter(ContextCompat.getColor(context, R.color.core_ultramarine), PorterDuff.Mode.MULTIPLY);
            spannableString.setSpan(new ImageSpan(drawable), 1, spannableString.length(), 0);
            return spannableString;
        }
    }

    /* loaded from: classes4.dex */
    public static class HeaderViewHolder extends RecyclerView.ViewHolder {
        private final TextView titleView;

        HeaderViewHolder(View view) {
            super(view);
            this.titleView = (TextView) view.findViewById(R.id.sticker_management_header);
        }

        void bind(int i) {
            this.titleView.setText(i);
        }
    }

    /* loaded from: classes4.dex */
    public static class EmptyViewHolder extends RecyclerView.ViewHolder {
        private final TextView text;

        EmptyViewHolder(View view) {
            super(view);
            this.text = (TextView) view.findViewById(R.id.sticker_management_empty_text);
        }

        void bind(int i) {
            this.text.setText(i);
        }
    }

    /* loaded from: classes4.dex */
    private static class NoSectionException extends IllegalStateException {
        private NoSectionException() {
        }
    }
}
