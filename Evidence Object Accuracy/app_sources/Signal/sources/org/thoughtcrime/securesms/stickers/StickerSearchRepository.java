package org.thoughtcrime.securesms.stickers;

import android.content.Context;
import java.util.ArrayList;
import java.util.List;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.components.emoji.EmojiUtil;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.StickerDatabase;
import org.thoughtcrime.securesms.database.model.StickerRecord;
import org.thoughtcrime.securesms.stickers.StickerSearchRepository;

/* loaded from: classes4.dex */
public final class StickerSearchRepository {
    private final AttachmentDatabase attachmentDatabase = SignalDatabase.attachments();
    private final StickerDatabase stickerDatabase = SignalDatabase.stickers();

    /* loaded from: classes4.dex */
    public interface Callback<T> {
        void onResult(T t);
    }

    public StickerSearchRepository(Context context) {
    }

    public void searchByEmoji(String str, Callback<List<StickerRecord>> callback) {
        SignalExecutors.BOUNDED.execute(new Runnable(str, callback) { // from class: org.thoughtcrime.securesms.stickers.StickerSearchRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ String f$1;
            public final /* synthetic */ StickerSearchRepository.Callback f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                StickerSearchRepository.this.lambda$searchByEmoji$0(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$searchByEmoji$0(String str, Callback callback) {
        String canonicalRepresentation = EmojiUtil.getCanonicalRepresentation(str);
        ArrayList arrayList = new ArrayList();
        for (String str2 : EmojiUtil.getAllRepresentations(canonicalRepresentation)) {
            StickerDatabase.StickerRecordReader stickerRecordReader = new StickerDatabase.StickerRecordReader(this.stickerDatabase.getStickersByEmoji(str2));
            while (true) {
                try {
                    StickerRecord next = stickerRecordReader.getNext();
                    if (next != null) {
                        arrayList.add(next);
                    }
                } catch (Throwable th) {
                    try {
                        stickerRecordReader.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                    throw th;
                }
            }
            stickerRecordReader.close();
        }
        callback.onResult(arrayList);
    }

    public void getStickerFeatureAvailability(Callback<Boolean> callback) {
        SignalExecutors.BOUNDED.execute(new Runnable(callback) { // from class: org.thoughtcrime.securesms.stickers.StickerSearchRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ StickerSearchRepository.Callback f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                StickerSearchRepository.this.lambda$getStickerFeatureAvailability$1(this.f$1);
            }
        });
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ void lambda$getStickerFeatureAvailability$1(org.thoughtcrime.securesms.stickers.StickerSearchRepository.Callback r3) {
        /*
            r2 = this;
            org.thoughtcrime.securesms.database.StickerDatabase r0 = r2.stickerDatabase
            java.lang.String r1 = "1"
            android.database.Cursor r0 = r0.getAllStickerPacks(r1)
            if (r0 == 0) goto L_0x0016
            boolean r1 = r0.moveToFirst()     // Catch: all -> 0x0029
            if (r1 == 0) goto L_0x0016
            java.lang.Boolean r1 = java.lang.Boolean.TRUE     // Catch: all -> 0x0029
            r3.onResult(r1)     // Catch: all -> 0x0029
            goto L_0x0023
        L_0x0016:
            org.thoughtcrime.securesms.database.AttachmentDatabase r1 = r2.attachmentDatabase     // Catch: all -> 0x0029
            boolean r1 = r1.hasStickerAttachments()     // Catch: all -> 0x0029
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch: all -> 0x0029
            r3.onResult(r1)     // Catch: all -> 0x0029
        L_0x0023:
            if (r0 == 0) goto L_0x0028
            r0.close()
        L_0x0028:
            return
        L_0x0029:
            r3 = move-exception
            if (r0 == 0) goto L_0x0034
            r0.close()     // Catch: all -> 0x0030
            goto L_0x0034
        L_0x0030:
            r0 = move-exception
            r3.addSuppressed(r0)
        L_0x0034:
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.stickers.StickerSearchRepository.lambda$getStickerFeatureAvailability$1(org.thoughtcrime.securesms.stickers.StickerSearchRepository$Callback):void");
    }
}
