package org.thoughtcrime.securesms.stickers;

import androidx.lifecycle.MutableLiveData;
import j$.util.Optional;
import org.thoughtcrime.securesms.stickers.StickerPackPreviewRepository;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class StickerPackPreviewViewModel$$ExternalSyntheticLambda0 implements StickerPackPreviewRepository.Callback {
    public final /* synthetic */ MutableLiveData f$0;

    public /* synthetic */ StickerPackPreviewViewModel$$ExternalSyntheticLambda0(MutableLiveData mutableLiveData) {
        this.f$0 = mutableLiveData;
    }

    @Override // org.thoughtcrime.securesms.stickers.StickerPackPreviewRepository.Callback
    public final void onComplete(Object obj) {
        this.f$0.postValue((Optional) obj);
    }
}
