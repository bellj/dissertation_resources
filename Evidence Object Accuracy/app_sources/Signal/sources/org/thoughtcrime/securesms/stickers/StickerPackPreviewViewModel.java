package org.thoughtcrime.securesms.stickers;

import android.app.Application;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import j$.util.Optional;
import java.util.Objects;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.stickers.StickerPackPreviewRepository;

/* loaded from: classes4.dex */
public final class StickerPackPreviewViewModel extends ViewModel {
    private final Application application;
    private final StickerManagementRepository managementRepository;
    private String packId;
    private String packKey;
    private final DatabaseObserver.Observer packObserver;
    private final StickerPackPreviewRepository previewRepository;
    private final MutableLiveData<Optional<StickerPackPreviewRepository.StickerManifestResult>> stickerManifest;

    private StickerPackPreviewViewModel(Application application, StickerPackPreviewRepository stickerPackPreviewRepository, StickerManagementRepository stickerManagementRepository) {
        this.application = application;
        this.previewRepository = stickerPackPreviewRepository;
        this.managementRepository = stickerManagementRepository;
        this.stickerManifest = new MutableLiveData<>();
        StickerPackPreviewViewModel$$ExternalSyntheticLambda1 stickerPackPreviewViewModel$$ExternalSyntheticLambda1 = new DatabaseObserver.Observer(stickerPackPreviewRepository) { // from class: org.thoughtcrime.securesms.stickers.StickerPackPreviewViewModel$$ExternalSyntheticLambda1
            public final /* synthetic */ StickerPackPreviewRepository f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
            public final void onChanged() {
                StickerPackPreviewViewModel.this.lambda$new$0(this.f$1);
            }
        };
        this.packObserver = stickerPackPreviewViewModel$$ExternalSyntheticLambda1;
        ApplicationDependencies.getDatabaseObserver().registerStickerPackObserver(stickerPackPreviewViewModel$$ExternalSyntheticLambda1);
    }

    public /* synthetic */ void lambda$new$0(StickerPackPreviewRepository stickerPackPreviewRepository) {
        if (!TextUtils.isEmpty(this.packId) && !TextUtils.isEmpty(this.packKey)) {
            String str = this.packId;
            String str2 = this.packKey;
            MutableLiveData<Optional<StickerPackPreviewRepository.StickerManifestResult>> mutableLiveData = this.stickerManifest;
            Objects.requireNonNull(mutableLiveData);
            stickerPackPreviewRepository.getStickerManifest(str, str2, new StickerPackPreviewViewModel$$ExternalSyntheticLambda0(mutableLiveData));
        }
    }

    public LiveData<Optional<StickerPackPreviewRepository.StickerManifestResult>> getStickerManifest(String str, String str2) {
        this.packId = str;
        this.packKey = str2;
        StickerPackPreviewRepository stickerPackPreviewRepository = this.previewRepository;
        MutableLiveData<Optional<StickerPackPreviewRepository.StickerManifestResult>> mutableLiveData = this.stickerManifest;
        Objects.requireNonNull(mutableLiveData);
        stickerPackPreviewRepository.getStickerManifest(str, str2, new StickerPackPreviewViewModel$$ExternalSyntheticLambda0(mutableLiveData));
        return this.stickerManifest;
    }

    public void onInstallClicked() {
        this.managementRepository.installStickerPack(this.packId, this.packKey, true);
    }

    public void onRemoveClicked() {
        this.managementRepository.uninstallStickerPack(this.packId, this.packKey);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(this.packObserver);
    }

    /* loaded from: classes4.dex */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;
        private final StickerManagementRepository managementRepository;
        private final StickerPackPreviewRepository previewRepository;

        public Factory(Application application, StickerPackPreviewRepository stickerPackPreviewRepository, StickerManagementRepository stickerManagementRepository) {
            this.application = application;
            this.previewRepository = stickerPackPreviewRepository;
            this.managementRepository = stickerManagementRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.NewInstanceFactory, androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new StickerPackPreviewViewModel(this.application, this.previewRepository, this.managementRepository));
        }
    }
}
