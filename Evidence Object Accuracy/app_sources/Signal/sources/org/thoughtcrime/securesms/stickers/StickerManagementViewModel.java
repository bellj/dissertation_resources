package org.thoughtcrime.securesms.stickers;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import java.util.List;
import java.util.Objects;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.model.StickerPackRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.stickers.StickerManagementRepository;

/* loaded from: classes4.dex */
public final class StickerManagementViewModel extends ViewModel {
    private final Application application;
    private final DatabaseObserver.Observer observer;
    private final MutableLiveData<StickerManagementRepository.PackResult> packs;
    private final StickerManagementRepository repository;

    private StickerManagementViewModel(Application application, StickerManagementRepository stickerManagementRepository) {
        this.application = application;
        this.repository = stickerManagementRepository;
        this.packs = new MutableLiveData<>();
        StickerManagementViewModel$$ExternalSyntheticLambda1 stickerManagementViewModel$$ExternalSyntheticLambda1 = new DatabaseObserver.Observer(stickerManagementRepository) { // from class: org.thoughtcrime.securesms.stickers.StickerManagementViewModel$$ExternalSyntheticLambda1
            public final /* synthetic */ StickerManagementRepository f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
            public final void onChanged() {
                StickerManagementViewModel.this.lambda$new$0(this.f$1);
            }
        };
        this.observer = stickerManagementViewModel$$ExternalSyntheticLambda1;
        ApplicationDependencies.getDatabaseObserver().registerStickerPackObserver(stickerManagementViewModel$$ExternalSyntheticLambda1);
    }

    public /* synthetic */ void lambda$new$0(StickerManagementRepository stickerManagementRepository) {
        stickerManagementRepository.deleteOrphanedStickerPacks();
        MutableLiveData<StickerManagementRepository.PackResult> mutableLiveData = this.packs;
        Objects.requireNonNull(mutableLiveData);
        stickerManagementRepository.getStickerPacks(new StickerManagementViewModel$$ExternalSyntheticLambda0(mutableLiveData));
    }

    public void init() {
        this.repository.deleteOrphanedStickerPacks();
        this.repository.fetchUnretrievedReferencePacks();
    }

    public void onVisible() {
        this.repository.deleteOrphanedStickerPacks();
    }

    public LiveData<StickerManagementRepository.PackResult> getStickerPacks() {
        StickerManagementRepository stickerManagementRepository = this.repository;
        MutableLiveData<StickerManagementRepository.PackResult> mutableLiveData = this.packs;
        Objects.requireNonNull(mutableLiveData);
        stickerManagementRepository.getStickerPacks(new StickerManagementViewModel$$ExternalSyntheticLambda0(mutableLiveData));
        return this.packs;
    }

    public void onStickerPackUninstallClicked(String str, String str2) {
        this.repository.uninstallStickerPack(str, str2);
    }

    public void onStickerPackInstallClicked(String str, String str2) {
        this.repository.installStickerPack(str, str2, false);
    }

    public void onOrderChanged(List<StickerPackRecord> list) {
        this.repository.setPackOrder(list);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(this.observer);
    }

    /* loaded from: classes4.dex */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;
        private final StickerManagementRepository repository;

        public Factory(Application application, StickerManagementRepository stickerManagementRepository) {
            this.application = application;
            this.repository = stickerManagementRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.NewInstanceFactory, androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new StickerManagementViewModel(this.application, this.repository));
        }
    }
}
