package org.thoughtcrime.securesms.transport;

/* loaded from: classes3.dex */
public class InsecureFallbackApprovalException extends Exception {
    public InsecureFallbackApprovalException(String str) {
        super(str);
    }

    public InsecureFallbackApprovalException(Throwable th) {
        super(th);
    }
}
