package org.thoughtcrime.securesms.transport;

/* loaded from: classes3.dex */
public class UndeliverableMessageException extends Exception {
    public UndeliverableMessageException() {
    }

    public UndeliverableMessageException(String str) {
        super(str);
    }

    public UndeliverableMessageException(String str, Throwable th) {
        super(str, th);
    }

    public UndeliverableMessageException(Throwable th) {
        super(th);
    }
}
