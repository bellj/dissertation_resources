package org.thoughtcrime.securesms.transport;

/* loaded from: classes3.dex */
public class RetryLaterException extends Exception {
    private final long backoff;

    public RetryLaterException() {
        this(null, -1);
    }

    public RetryLaterException(long j) {
        this(null, j);
    }

    public RetryLaterException(Exception exc) {
        this(exc, -1);
    }

    public RetryLaterException(Exception exc, long j) {
        super(exc);
        this.backoff = j;
    }

    public long getBackoff() {
        return this.backoff;
    }
}
