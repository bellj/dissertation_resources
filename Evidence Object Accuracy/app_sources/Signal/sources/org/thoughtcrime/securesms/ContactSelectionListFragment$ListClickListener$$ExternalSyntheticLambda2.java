package org.thoughtcrime.securesms;

import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.ContactSelectionListFragment;
import org.thoughtcrime.securesms.contacts.ContactSelectionListItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class ContactSelectionListFragment$ListClickListener$$ExternalSyntheticLambda2 implements SimpleTask.BackgroundTask {
    public final /* synthetic */ ContactSelectionListItem f$0;

    public /* synthetic */ ContactSelectionListFragment$ListClickListener$$ExternalSyntheticLambda2(ContactSelectionListItem contactSelectionListItem) {
        this.f$0 = contactSelectionListItem;
    }

    @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
    public final Object run() {
        return ContactSelectionListFragment.ListClickListener.lambda$onItemClick$0(this.f$0);
    }
}
