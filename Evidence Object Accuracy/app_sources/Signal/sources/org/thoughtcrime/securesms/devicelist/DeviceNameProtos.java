package org.thoughtcrime.securesms.devicelist;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.MessageLite;
import com.google.protobuf.MessageLiteOrBuilder;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes4.dex */
public final class DeviceNameProtos {

    /* loaded from: classes4.dex */
    public interface DeviceNameOrBuilder extends MessageLiteOrBuilder {
        ByteString getCiphertext();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ MessageLite getDefaultInstanceForType();

        ByteString getEphemeralPublic();

        ByteString getSyntheticIv();

        boolean hasCiphertext();

        boolean hasEphemeralPublic();

        boolean hasSyntheticIv();

        @Override // com.google.protobuf.MessageLiteOrBuilder
        /* synthetic */ boolean isInitialized();
    }

    public static void registerAllExtensions(ExtensionRegistryLite extensionRegistryLite) {
    }

    private DeviceNameProtos() {
    }

    /* loaded from: classes4.dex */
    public static final class DeviceName extends GeneratedMessageLite<DeviceName, Builder> implements DeviceNameOrBuilder {
        public static final int CIPHERTEXT_FIELD_NUMBER;
        private static final DeviceName DEFAULT_INSTANCE;
        public static final int EPHEMERALPUBLIC_FIELD_NUMBER;
        private static volatile Parser<DeviceName> PARSER;
        public static final int SYNTHETICIV_FIELD_NUMBER;
        private int bitField0_;
        private ByteString ciphertext_;
        private ByteString ephemeralPublic_;
        private ByteString syntheticIv_;

        private DeviceName() {
            ByteString byteString = ByteString.EMPTY;
            this.ephemeralPublic_ = byteString;
            this.syntheticIv_ = byteString;
            this.ciphertext_ = byteString;
        }

        @Override // org.thoughtcrime.securesms.devicelist.DeviceNameProtos.DeviceNameOrBuilder
        public boolean hasEphemeralPublic() {
            return (this.bitField0_ & 1) != 0;
        }

        @Override // org.thoughtcrime.securesms.devicelist.DeviceNameProtos.DeviceNameOrBuilder
        public ByteString getEphemeralPublic() {
            return this.ephemeralPublic_;
        }

        public void setEphemeralPublic(ByteString byteString) {
            byteString.getClass();
            this.bitField0_ |= 1;
            this.ephemeralPublic_ = byteString;
        }

        public void clearEphemeralPublic() {
            this.bitField0_ &= -2;
            this.ephemeralPublic_ = getDefaultInstance().getEphemeralPublic();
        }

        @Override // org.thoughtcrime.securesms.devicelist.DeviceNameProtos.DeviceNameOrBuilder
        public boolean hasSyntheticIv() {
            return (this.bitField0_ & 2) != 0;
        }

        @Override // org.thoughtcrime.securesms.devicelist.DeviceNameProtos.DeviceNameOrBuilder
        public ByteString getSyntheticIv() {
            return this.syntheticIv_;
        }

        public void setSyntheticIv(ByteString byteString) {
            byteString.getClass();
            this.bitField0_ |= 2;
            this.syntheticIv_ = byteString;
        }

        public void clearSyntheticIv() {
            this.bitField0_ &= -3;
            this.syntheticIv_ = getDefaultInstance().getSyntheticIv();
        }

        @Override // org.thoughtcrime.securesms.devicelist.DeviceNameProtos.DeviceNameOrBuilder
        public boolean hasCiphertext() {
            return (this.bitField0_ & 4) != 0;
        }

        @Override // org.thoughtcrime.securesms.devicelist.DeviceNameProtos.DeviceNameOrBuilder
        public ByteString getCiphertext() {
            return this.ciphertext_;
        }

        public void setCiphertext(ByteString byteString) {
            byteString.getClass();
            this.bitField0_ |= 4;
            this.ciphertext_ = byteString;
        }

        public void clearCiphertext() {
            this.bitField0_ &= -5;
            this.ciphertext_ = getDefaultInstance().getCiphertext();
        }

        public static DeviceName parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
            return (DeviceName) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
        }

        public static DeviceName parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (DeviceName) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
        }

        public static DeviceName parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
            return (DeviceName) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
        }

        public static DeviceName parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (DeviceName) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
        }

        public static DeviceName parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
            return (DeviceName) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
        }

        public static DeviceName parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
            return (DeviceName) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
        }

        public static DeviceName parseFrom(InputStream inputStream) throws IOException {
            return (DeviceName) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static DeviceName parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (DeviceName) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static DeviceName parseDelimitedFrom(InputStream inputStream) throws IOException {
            return (DeviceName) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
        }

        public static DeviceName parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (DeviceName) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
        }

        public static DeviceName parseFrom(CodedInputStream codedInputStream) throws IOException {
            return (DeviceName) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
        }

        public static DeviceName parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
            return (DeviceName) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
        }

        public static Builder newBuilder() {
            return DEFAULT_INSTANCE.createBuilder();
        }

        public static Builder newBuilder(DeviceName deviceName) {
            return DEFAULT_INSTANCE.createBuilder(deviceName);
        }

        /* loaded from: classes4.dex */
        public static final class Builder extends GeneratedMessageLite.Builder<DeviceName, Builder> implements DeviceNameOrBuilder {
            /* synthetic */ Builder(AnonymousClass1 r1) {
                this();
            }

            private Builder() {
                super(DeviceName.DEFAULT_INSTANCE);
            }

            @Override // org.thoughtcrime.securesms.devicelist.DeviceNameProtos.DeviceNameOrBuilder
            public boolean hasEphemeralPublic() {
                return ((DeviceName) this.instance).hasEphemeralPublic();
            }

            @Override // org.thoughtcrime.securesms.devicelist.DeviceNameProtos.DeviceNameOrBuilder
            public ByteString getEphemeralPublic() {
                return ((DeviceName) this.instance).getEphemeralPublic();
            }

            public Builder setEphemeralPublic(ByteString byteString) {
                copyOnWrite();
                ((DeviceName) this.instance).setEphemeralPublic(byteString);
                return this;
            }

            public Builder clearEphemeralPublic() {
                copyOnWrite();
                ((DeviceName) this.instance).clearEphemeralPublic();
                return this;
            }

            @Override // org.thoughtcrime.securesms.devicelist.DeviceNameProtos.DeviceNameOrBuilder
            public boolean hasSyntheticIv() {
                return ((DeviceName) this.instance).hasSyntheticIv();
            }

            @Override // org.thoughtcrime.securesms.devicelist.DeviceNameProtos.DeviceNameOrBuilder
            public ByteString getSyntheticIv() {
                return ((DeviceName) this.instance).getSyntheticIv();
            }

            public Builder setSyntheticIv(ByteString byteString) {
                copyOnWrite();
                ((DeviceName) this.instance).setSyntheticIv(byteString);
                return this;
            }

            public Builder clearSyntheticIv() {
                copyOnWrite();
                ((DeviceName) this.instance).clearSyntheticIv();
                return this;
            }

            @Override // org.thoughtcrime.securesms.devicelist.DeviceNameProtos.DeviceNameOrBuilder
            public boolean hasCiphertext() {
                return ((DeviceName) this.instance).hasCiphertext();
            }

            @Override // org.thoughtcrime.securesms.devicelist.DeviceNameProtos.DeviceNameOrBuilder
            public ByteString getCiphertext() {
                return ((DeviceName) this.instance).getCiphertext();
            }

            public Builder setCiphertext(ByteString byteString) {
                copyOnWrite();
                ((DeviceName) this.instance).setCiphertext(byteString);
                return this;
            }

            public Builder clearCiphertext() {
                copyOnWrite();
                ((DeviceName) this.instance).clearCiphertext();
                return this;
            }
        }

        @Override // com.google.protobuf.GeneratedMessageLite
        protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
            switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
                case 1:
                    return new DeviceName();
                case 2:
                    return new Builder(null);
                case 3:
                    return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0001\u0003\u0000\u0001\u0001\u0003\u0003\u0000\u0000\u0000\u0001ည\u0000\u0002ည\u0001\u0003ည\u0002", new Object[]{"bitField0_", "ephemeralPublic_", "syntheticIv_", "ciphertext_"});
                case 4:
                    return DEFAULT_INSTANCE;
                case 5:
                    Parser<DeviceName> parser = PARSER;
                    if (parser == null) {
                        synchronized (DeviceName.class) {
                            parser = PARSER;
                            if (parser == null) {
                                parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                                PARSER = parser;
                            }
                        }
                    }
                    return parser;
                case 6:
                    return (byte) 1;
                case 7:
                    return null;
                default:
                    throw new UnsupportedOperationException();
            }
        }

        static {
            DeviceName deviceName = new DeviceName();
            DEFAULT_INSTANCE = deviceName;
            GeneratedMessageLite.registerDefaultInstance(DeviceName.class, deviceName);
        }

        public static DeviceName getDefaultInstance() {
            return DEFAULT_INSTANCE;
        }

        public static Parser<DeviceName> parser() {
            return DEFAULT_INSTANCE.getParserForType();
        }
    }

    /* renamed from: org.thoughtcrime.securesms.devicelist.DeviceNameProtos$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }
}
