package org.thoughtcrime.securesms.devicelist;

/* loaded from: classes4.dex */
public class Device {
    private final long created;
    private final long id;
    private final long lastSeen;
    private final String name;

    public Device(long j, String str, long j2, long j3) {
        this.id = j;
        this.name = str;
        this.created = j2;
        this.lastSeen = j3;
    }

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public long getCreated() {
        return this.created;
    }

    public long getLastSeen() {
        return this.lastSeen;
    }
}
