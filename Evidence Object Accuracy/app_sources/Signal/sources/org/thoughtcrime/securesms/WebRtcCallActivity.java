package org.thoughtcrime.securesms;

import android.app.PictureInPictureParams;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.util.Pair;
import android.util.Rational;
import android.view.ViewTreeObserver;
import android.widget.PopupWindow;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.window.DisplayFeature;
import androidx.window.FoldingFeature;
import androidx.window.WindowLayoutInfo;
import androidx.window.WindowManager;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import j$.util.Collection$EL;
import j$.util.Optional;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.IdentityKey;
import org.thoughtcrime.securesms.components.TooltipPopup;
import org.thoughtcrime.securesms.components.sensors.DeviceOrientationMonitor;
import org.thoughtcrime.securesms.components.sensors.Orientation;
import org.thoughtcrime.securesms.components.webrtc.CallParticipantListUpdate;
import org.thoughtcrime.securesms.components.webrtc.CallParticipantsListUpdatePopupWindow;
import org.thoughtcrime.securesms.components.webrtc.CallParticipantsState;
import org.thoughtcrime.securesms.components.webrtc.CallToastPopupWindow;
import org.thoughtcrime.securesms.components.webrtc.GroupCallSafetyNumberChangeNotificationUtil;
import org.thoughtcrime.securesms.components.webrtc.WebRtcAudioOutput;
import org.thoughtcrime.securesms.components.webrtc.WebRtcCallView;
import org.thoughtcrime.securesms.components.webrtc.WebRtcCallViewModel;
import org.thoughtcrime.securesms.components.webrtc.WebRtcControls;
import org.thoughtcrime.securesms.components.webrtc.WifiToCellularPopupWindow;
import org.thoughtcrime.securesms.components.webrtc.participantslist.CallParticipantsListDialog;
import org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeDialog;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.messagerequests.CalleeMustAcceptMessageRequestActivity;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet;
import org.thoughtcrime.securesms.service.webrtc.SignalCallManager;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcEphemeralState;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.util.EllapsedTimeFormatter;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.FullscreenHelper;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.ThrottledDebouncer;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.VibrateUtil;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.thoughtcrime.securesms.webrtc.CallParticipantsViewState;
import org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager;
import org.whispersystems.signalservice.api.messages.calls.HangupMessage;

/* loaded from: classes.dex */
public class WebRtcCallActivity extends BaseActivity implements SafetyNumberChangeDialog.Callback {
    public static final String ANSWER_ACTION = (WebRtcCallActivity.class.getCanonicalName() + ".ANSWER_ACTION");
    public static final String DENY_ACTION = (WebRtcCallActivity.class.getCanonicalName() + ".DENY_ACTION");
    public static final String END_CALL_ACTION = (WebRtcCallActivity.class.getCanonicalName() + ".END_CALL_ACTION");
    public static final String EXTRA_ENABLE_VIDEO_IF_AVAILABLE = (WebRtcCallActivity.class.getCanonicalName() + ".ENABLE_VIDEO_IF_AVAILABLE");
    private static final int STANDARD_DELAY_FINISH;
    private static final String TAG = Log.tag(WebRtcCallActivity.class);
    private static final int VIBRATE_DURATION;
    private WebRtcCallView callScreen;
    private DeviceOrientationMonitor deviceOrientationMonitor;
    private boolean enableVideoIfAvailable;
    private Disposable ephemeralStateDisposable = Disposable.CC.empty();
    private FullscreenHelper fullscreenHelper;
    private boolean hasWarnedAboutBluetooth;
    private CallParticipantsListUpdatePopupWindow participantUpdateWindow;
    private ThrottledDebouncer requestNewSizesThrottle;
    private TooltipPopup videoTooltip;
    private WebRtcCallViewModel viewModel;
    private WifiToCellularPopupWindow wifiToCellularPopupWindow;
    private WindowLayoutInfoConsumer windowLayoutInfoConsumer;
    private WindowManager windowManager;

    @Override // org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeDialog.Callback
    public void onMessageResentAfterSafetyNumberChange() {
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity, android.view.ContextThemeWrapper, android.content.ContextWrapper
    public void attachBaseContext(Context context) {
        getDelegate().setLocalNightMode(2);
        super.attachBaseContext(context);
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        Log.i(TAG, "onCreate()");
        getWindow().addFlags(524288);
        getWindow().addFlags(128);
        super.onCreate(bundle);
        boolean z = getResources().getConfiguration().smallestScreenWidthDp >= 480;
        if (!z) {
            setRequestedOrientation(1);
        }
        requestWindowFeature(1);
        setContentView(R.layout.webrtc_call_activity);
        this.fullscreenHelper = new FullscreenHelper(this);
        setVolumeControlStream(0);
        initializeResources();
        initializeViewModel(z);
        processIntent(getIntent());
        Intent intent = getIntent();
        String str = EXTRA_ENABLE_VIDEO_IF_AVAILABLE;
        this.enableVideoIfAvailable = intent.getBooleanExtra(str, false);
        getIntent().removeExtra(str);
        this.windowManager = new WindowManager(this);
        WindowLayoutInfoConsumer windowLayoutInfoConsumer = new WindowLayoutInfoConsumer(this, null);
        this.windowLayoutInfoConsumer = windowLayoutInfoConsumer;
        this.windowManager.registerLayoutChangeCallback(SignalExecutors.BOUNDED, windowLayoutInfoConsumer);
        this.requestNewSizesThrottle = new ThrottledDebouncer(TimeUnit.SECONDS.toMillis(1));
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onStart() {
        super.onStart();
        Flowable<WebRtcEphemeralState> observeOn = ApplicationDependencies.getSignalCallManager().ephemeralStates().observeOn(AndroidSchedulers.mainThread());
        WebRtcCallViewModel webRtcCallViewModel = this.viewModel;
        Objects.requireNonNull(webRtcCallViewModel);
        this.ephemeralStateDisposable = observeOn.subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda18
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                WebRtcCallViewModel.this.updateFromEphemeralState((WebRtcEphemeralState) obj);
            }
        });
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        Log.i(TAG, "onResume()");
        super.onResume();
        initializeScreenshotSecurity();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onNewIntent(Intent intent) {
        Log.i(TAG, "onNewIntent");
        super.onNewIntent(intent);
        processIntent(intent);
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onPause() {
        CallParticipantsState value;
        Log.i(TAG, "onPause");
        super.onPause();
        if (!isInPipMode() || isFinishing()) {
            EventBus.getDefault().unregister(this);
        }
        if (!this.viewModel.isCallStarting() && (value = this.viewModel.getCallParticipantsState().getValue()) != null && value.getCallState().isPreJoinOrNetworkUnavailable()) {
            finish();
        }
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onStop() {
        CallParticipantsState value;
        Log.i(TAG, "onStop");
        super.onStop();
        this.ephemeralStateDisposable.dispose();
        if (!isInPipMode() || isFinishing()) {
            EventBus.getDefault().unregister(this);
            this.requestNewSizesThrottle.clear();
        }
        if (!this.viewModel.isCallStarting() && (value = this.viewModel.getCallParticipantsState().getValue()) != null && value.getCallState().isPreJoinOrNetworkUnavailable()) {
            ApplicationDependencies.getSignalCallManager().cancelPreJoin();
        }
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        this.windowManager.unregisterLayoutChangeCallback(this.windowLayoutInfoConsumer);
        EventBus.getDefault().unregister(this);
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, android.app.Activity
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        Permissions.onRequestPermissionsResult(this, i, strArr, iArr);
    }

    @Override // android.app.Activity
    protected void onUserLeaveHint() {
        enterPipModeIfPossible();
    }

    @Override // androidx.activity.ComponentActivity, android.app.Activity
    public void onBackPressed() {
        if (!enterPipModeIfPossible()) {
            super.onBackPressed();
        }
    }

    @Override // android.app.Activity
    public void onPictureInPictureModeChanged(boolean z, Configuration configuration) {
        this.viewModel.setIsInPipMode(z);
        this.participantUpdateWindow.setEnabled(!z);
    }

    private boolean enterPipModeIfPossible() {
        if (!this.viewModel.canEnterPipMode() || !isSystemPipEnabledAndAvailable()) {
            return false;
        }
        enterPictureInPictureMode(new PictureInPictureParams.Builder().setAspectRatio(new Rational(9, 16)).build());
        CallParticipantsListDialog.dismiss(getSupportFragmentManager());
        return true;
    }

    private boolean isInPipMode() {
        return isSystemPipEnabledAndAvailable() && isInPictureInPictureMode();
    }

    private void processIntent(Intent intent) {
        if (ANSWER_ACTION.equals(intent.getAction())) {
            handleAnswerWithAudio();
        } else if (DENY_ACTION.equals(intent.getAction())) {
            handleDenyCall();
        } else if (END_CALL_ACTION.equals(intent.getAction())) {
            handleEndCall();
        }
    }

    private void initializeScreenshotSecurity() {
        if (TextSecurePreferences.isScreenSecurityEnabled(this)) {
            getWindow().addFlags(8192);
        } else {
            getWindow().clearFlags(8192);
        }
    }

    private void initializeResources() {
        WebRtcCallView webRtcCallView = (WebRtcCallView) findViewById(R.id.callScreen);
        this.callScreen = webRtcCallView;
        webRtcCallView.setControlsListener(new ControlsListener(this, null));
        this.participantUpdateWindow = new CallParticipantsListUpdatePopupWindow(this.callScreen);
        this.wifiToCellularPopupWindow = new WifiToCellularPopupWindow(this.callScreen);
    }

    private void initializeViewModel(boolean z) {
        this.deviceOrientationMonitor = new DeviceOrientationMonitor(this);
        getLifecycle().addObserver(this.deviceOrientationMonitor);
        WebRtcCallViewModel webRtcCallViewModel = (WebRtcCallViewModel) new ViewModelProvider(this, new WebRtcCallViewModel.Factory(this.deviceOrientationMonitor)).get(WebRtcCallViewModel.class);
        this.viewModel = webRtcCallViewModel;
        webRtcCallViewModel.setIsLandscapeEnabled(z);
        this.viewModel.setIsInPipMode(isInPipMode());
        LiveData<Boolean> microphoneEnabled = this.viewModel.getMicrophoneEnabled();
        WebRtcCallView webRtcCallView = this.callScreen;
        Objects.requireNonNull(webRtcCallView);
        microphoneEnabled.observe(this, new Observer() { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda3
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                WebRtcCallView.this.setMicEnabled(((Boolean) obj).booleanValue());
            }
        });
        LiveData<WebRtcControls> webRtcControls = this.viewModel.getWebRtcControls();
        WebRtcCallView webRtcCallView2 = this.callScreen;
        Objects.requireNonNull(webRtcCallView2);
        webRtcControls.observe(this, new Observer() { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda8
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                WebRtcCallView.this.setWebRtcControls((WebRtcControls) obj);
            }
        });
        this.viewModel.getEvents().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda9
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                WebRtcCallActivity.m262$r8$lambda$QHn1QLLvseP24xePdgFTT8N3mA(WebRtcCallActivity.this, (WebRtcCallViewModel.Event) obj);
            }
        });
        this.viewModel.getCallTime().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda10
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                WebRtcCallActivity.$r8$lambda$uB79yBmajYmEGyAJdqWueyV1o00(WebRtcCallActivity.this, ((Long) obj).longValue());
            }
        });
        LiveDataUtil.combineLatest(this.viewModel.getCallParticipantsState(), this.viewModel.getOrientationAndLandscapeEnabled(), this.viewModel.getEphemeralState(), new LiveDataUtil.Combine3() { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda11
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine3
            public final Object apply(Object obj, Object obj2, Object obj3) {
                return WebRtcCallActivity.$r8$lambda$8lHGylSUklltIhUc6mWAWSl_xDc((CallParticipantsState) obj, (Pair) obj2, (WebRtcEphemeralState) obj3);
            }
        }).observe(this, new Observer() { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda12
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                WebRtcCallActivity.$r8$lambda$u5LznMbwa_BeoJygqRKhi4HUkQo(WebRtcCallActivity.this, (CallParticipantsViewState) obj);
            }
        });
        LiveData<CallParticipantListUpdate> callParticipantListUpdate = this.viewModel.getCallParticipantListUpdate();
        CallParticipantsListUpdatePopupWindow callParticipantsListUpdatePopupWindow = this.participantUpdateWindow;
        Objects.requireNonNull(callParticipantsListUpdatePopupWindow);
        callParticipantListUpdate.observe(this, new Observer() { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda13
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                CallParticipantsListUpdatePopupWindow.this.addCallParticipantListUpdate((CallParticipantListUpdate) obj);
            }
        });
        this.viewModel.getSafetyNumberChangeEvent().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda14
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                WebRtcCallActivity.this.handleSafetyNumberChangeEvent((WebRtcCallViewModel.SafetyNumberChangeEvent) obj);
            }
        });
        this.viewModel.getGroupMembersChanged().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda15
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                WebRtcCallActivity.m264$r8$lambda$VI0kg_wXhkeKd52vgsSeabmtkU(WebRtcCallActivity.this, (List) obj);
            }
        });
        this.viewModel.getGroupMemberCount().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda16
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                WebRtcCallActivity.this.handleGroupMemberCountChange(((Integer) obj).intValue());
            }
        });
        this.viewModel.shouldShowSpeakerHint().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda4
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                WebRtcCallActivity.$r8$lambda$dUqL148xN1q6RDQbicyEaCcQZM8(WebRtcCallActivity.this, ((Boolean) obj).booleanValue());
            }
        });
        this.callScreen.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda5
            @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
            public final void onGlobalLayout() {
                WebRtcCallActivity.m265$r8$lambda$oMHiYMT1c1JQzr7a2hWSzXHcKg(WebRtcCallActivity.this);
            }
        });
        this.viewModel.getOrientationAndLandscapeEnabled().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda6
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                WebRtcCallActivity.$r8$lambda$FFMGsu2cycl0OhgnfesEoUm_42c((Pair) obj);
            }
        });
        LiveData<Integer> controlsRotation = this.viewModel.getControlsRotation();
        WebRtcCallView webRtcCallView3 = this.callScreen;
        Objects.requireNonNull(webRtcCallView3);
        controlsRotation.observe(this, new Observer() { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda7
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                WebRtcCallView.this.rotateControls(((Integer) obj).intValue());
            }
        });
    }

    public static /* synthetic */ CallParticipantsViewState lambda$initializeViewModel$0(CallParticipantsState callParticipantsState, Pair pair, WebRtcEphemeralState webRtcEphemeralState) {
        return new CallParticipantsViewState(callParticipantsState, webRtcEphemeralState, pair.first == Orientation.PORTRAIT_BOTTOM_EDGE, ((Boolean) pair.second).booleanValue());
    }

    public /* synthetic */ void lambda$initializeViewModel$1(CallParticipantsViewState callParticipantsViewState) {
        this.callScreen.updateCallParticipants(callParticipantsViewState);
    }

    public /* synthetic */ void lambda$initializeViewModel$2(List list) {
        updateGroupMembersForGroupCall();
    }

    public /* synthetic */ void lambda$initializeViewModel$4() {
        CallParticipantsState value = this.viewModel.getCallParticipantsState().getValue();
        if (value != null && value.needsNewRequestSizes()) {
            this.requestNewSizesThrottle.publish(new Runnable() { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda2
                @Override // java.lang.Runnable
                public final void run() {
                    WebRtcCallActivity.m266$r8$lambda$w1OYrEGoRf6KZQeJE7C8G4Tzk();
                }
            });
        }
    }

    public static /* synthetic */ void lambda$initializeViewModel$3() {
        ApplicationDependencies.getSignalCallManager().updateRenderedResolutions();
    }

    public static /* synthetic */ void lambda$initializeViewModel$5(Pair pair) {
        ApplicationDependencies.getSignalCallManager().orientationChanged(((Boolean) pair.second).booleanValue(), ((Orientation) pair.first).getDegrees());
    }

    public void handleViewModelEvent(WebRtcCallViewModel.Event event) {
        if (event instanceof WebRtcCallViewModel.Event.StartCall) {
            startCall(((WebRtcCallViewModel.Event.StartCall) event).isVideoCall());
        } else if (event instanceof WebRtcCallViewModel.Event.ShowGroupCallSafetyNumberChange) {
            SafetyNumberBottomSheet.forGroupCall(((WebRtcCallViewModel.Event.ShowGroupCallSafetyNumberChange) event).getIdentityRecords()).show(getSupportFragmentManager());
        } else if (event instanceof WebRtcCallViewModel.Event.SwitchToSpeaker) {
            this.callScreen.switchToSpeakerView();
        } else if (event instanceof WebRtcCallViewModel.Event.ShowSwipeToSpeakerHint) {
            CallToastPopupWindow.show(this.callScreen);
        } else if (!isInPipMode()) {
            if (event instanceof WebRtcCallViewModel.Event.ShowVideoTooltip) {
                if (this.videoTooltip == null) {
                    this.videoTooltip = TooltipPopup.forTarget(this.callScreen.getVideoTooltipTarget()).setBackgroundTint(ContextCompat.getColor(this, R.color.core_ultramarine)).setTextColor(ContextCompat.getColor(this, R.color.core_white)).setText(R.string.WebRtcCallActivity__tap_here_to_turn_on_your_video).setOnDismissListener(new PopupWindow.OnDismissListener() { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda20
                        @Override // android.widget.PopupWindow.OnDismissListener
                        public final void onDismiss() {
                            WebRtcCallActivity.$r8$lambda$0Vbf2qCG1qF0QArzLdSyczX9Ku0(WebRtcCallActivity.this);
                        }
                    }).show(0);
                }
            } else if (event instanceof WebRtcCallViewModel.Event.DismissVideoTooltip) {
                TooltipPopup tooltipPopup = this.videoTooltip;
                if (tooltipPopup != null) {
                    tooltipPopup.dismiss();
                    this.videoTooltip = null;
                }
            } else if (event instanceof WebRtcCallViewModel.Event.ShowWifiToCellularPopup) {
                this.wifiToCellularPopupWindow.show();
            } else {
                throw new IllegalArgumentException("Unknown event: " + event);
            }
        }
    }

    public /* synthetic */ void lambda$handleViewModelEvent$6() {
        this.viewModel.onDismissedVideoTooltip();
    }

    public void handleCallTime(long j) {
        EllapsedTimeFormatter fromDurationMillis = EllapsedTimeFormatter.fromDurationMillis(j);
        if (fromDurationMillis != null) {
            this.callScreen.setStatus(getString(R.string.WebRtcCallActivity__signal_s, new Object[]{fromDurationMillis.toString()}));
        }
    }

    public void handleSetAudioHandset() {
        ApplicationDependencies.getSignalCallManager().selectAudioDevice(SignalAudioManager.AudioDevice.EARPIECE);
    }

    public void handleSetAudioSpeaker() {
        ApplicationDependencies.getSignalCallManager().selectAudioDevice(SignalAudioManager.AudioDevice.SPEAKER_PHONE);
    }

    public void handleSetAudioBluetooth() {
        ApplicationDependencies.getSignalCallManager().selectAudioDevice(SignalAudioManager.AudioDevice.BLUETOOTH);
    }

    public void handleSetMuteAudio(boolean z) {
        ApplicationDependencies.getSignalCallManager().setMuteAudio(z);
    }

    public void handleSetMuteVideo(boolean z) {
        Recipient recipient = this.viewModel.getRecipient().get();
        if (!recipient.equals(Recipient.UNKNOWN)) {
            String displayName = recipient.getDisplayName(this);
            Permissions.with(this).request("android.permission.CAMERA").ifNecessary().withRationaleDialog(getString(R.string.WebRtcCallActivity__to_call_s_signal_needs_access_to_your_camera, new Object[]{displayName}), R.drawable.ic_video_solid_24_tinted).withPermanentDenialDialog(getString(R.string.WebRtcCallActivity__to_call_s_signal_needs_access_to_your_camera, new Object[]{displayName})).onAllGranted(new Runnable(z) { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda21
                public final /* synthetic */ boolean f$0;

                {
                    this.f$0 = r1;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    WebRtcCallActivity.$r8$lambda$x7As5JEkzCSEKnDtwjBJp0YnLyk(this.f$0);
                }
            }).execute();
        }
    }

    public static /* synthetic */ void lambda$handleSetMuteVideo$7(boolean z) {
        ApplicationDependencies.getSignalCallManager().setMuteVideo(!z);
    }

    public void handleFlipCamera() {
        ApplicationDependencies.getSignalCallManager().flipCamera();
    }

    public void handleAnswerWithAudio() {
        Permissions.with(this).request("android.permission.RECORD_AUDIO").ifNecessary().withRationaleDialog(getString(R.string.WebRtcCallActivity_to_answer_the_call_give_signal_access_to_your_microphone), R.drawable.ic_mic_solid_24).withPermanentDenialDialog(getString(R.string.WebRtcCallActivity_signal_requires_microphone_and_camera_permissions_in_order_to_make_or_receive_calls)).onAllGranted(new Runnable() { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda22
            @Override // java.lang.Runnable
            public final void run() {
                WebRtcCallActivity.m261$r8$lambda$GhzhSdnSs2TevQEWQlC2y4VOMQ(WebRtcCallActivity.this);
            }
        }).onAnyDenied(new WebRtcCallActivity$$ExternalSyntheticLambda23(this)).execute();
    }

    public /* synthetic */ void lambda$handleAnswerWithAudio$8() {
        this.callScreen.setStatus(getString(R.string.RedPhone_answering));
        ApplicationDependencies.getSignalCallManager().acceptCall(false);
    }

    public void handleAnswerWithVideo() {
        Recipient recipient = this.viewModel.getRecipient().get();
        if (!recipient.equals(Recipient.UNKNOWN)) {
            Permissions.with(this).request("android.permission.RECORD_AUDIO", "android.permission.CAMERA").ifNecessary().withRationaleDialog(getString(R.string.WebRtcCallActivity_to_answer_the_call_from_s_give_signal_access_to_your_microphone, new Object[]{recipient.getDisplayName(this)}), R.drawable.ic_mic_solid_24, R.drawable.ic_video_solid_24_tinted).withPermanentDenialDialog(getString(R.string.WebRtcCallActivity_signal_requires_microphone_and_camera_permissions_in_order_to_make_or_receive_calls)).onAllGranted(new Runnable(recipient) { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda24
                public final /* synthetic */ Recipient f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    WebRtcCallActivity.m263$r8$lambda$V0ejsCzsXlk1YUK0HtU3KelViw(WebRtcCallActivity.this, this.f$1);
                }
            }).onAnyDenied(new WebRtcCallActivity$$ExternalSyntheticLambda23(this)).execute();
        }
    }

    public /* synthetic */ void lambda$handleAnswerWithVideo$9(Recipient recipient) {
        this.callScreen.setRecipient(recipient);
        this.callScreen.setStatus(getString(R.string.RedPhone_answering));
        ApplicationDependencies.getSignalCallManager().acceptCall(true);
        handleSetMuteVideo(false);
    }

    public void handleDenyCall() {
        Recipient recipient = this.viewModel.getRecipient().get();
        if (!recipient.equals(Recipient.UNKNOWN)) {
            ApplicationDependencies.getSignalCallManager().denyCall();
            this.callScreen.setRecipient(recipient);
            this.callScreen.setStatus(getString(R.string.RedPhone_ending_call));
            delayedFinish();
        }
    }

    public void handleEndCall() {
        Log.i(TAG, "Hangup pressed, handling termination now...");
        ApplicationDependencies.getSignalCallManager().localHangup();
    }

    private void handleOutgoingCall(WebRtcViewModel webRtcViewModel) {
        if (webRtcViewModel.getGroupState().isNotIdle()) {
            this.callScreen.setStatusFromGroupCallState(webRtcViewModel.getGroupState());
        } else {
            this.callScreen.setStatus(getString(R.string.WebRtcCallActivity__calling));
        }
    }

    private void handleTerminate(Recipient recipient, HangupMessage.Type type) {
        String str = TAG;
        Log.i(str, "handleTerminate called: " + type.name());
        this.callScreen.setStatusFromHangupType(type);
        EventBus.getDefault().removeStickyEvent(WebRtcViewModel.class);
        if (type == HangupMessage.Type.NEED_PERMISSION) {
            startActivity(CalleeMustAcceptMessageRequestActivity.createIntent(this, recipient.getId()));
        }
        delayedFinish();
    }

    private void handleGlare(Recipient recipient) {
        String str = TAG;
        Log.i(str, "handleGlare: " + recipient.getId());
        this.callScreen.setStatus("");
    }

    private void handleCallRinging() {
        this.callScreen.setStatus(getString(R.string.RedPhone_ringing));
    }

    private void handleCallBusy() {
        EventBus.getDefault().removeStickyEvent(WebRtcViewModel.class);
        this.callScreen.setStatus(getString(R.string.RedPhone_busy));
        delayedFinish(SignalCallManager.BUSY_TONE_LENGTH);
    }

    private void handleCallConnected(WebRtcViewModel webRtcViewModel) {
        getWindow().addFlags(32768);
        if (webRtcViewModel.getGroupState().isNotIdleOrConnected()) {
            this.callScreen.setStatusFromGroupCallState(webRtcViewModel.getGroupState());
        }
    }

    private void handleCallReconnecting() {
        this.callScreen.setStatus(getString(R.string.WebRtcCallActivity__reconnecting));
        VibrateUtil.vibrate(this, 50);
    }

    private void handleRecipientUnavailable() {
        EventBus.getDefault().removeStickyEvent(WebRtcViewModel.class);
        this.callScreen.setStatus(getString(R.string.RedPhone_recipient_unavailable));
        delayedFinish();
    }

    private void handleServerFailure() {
        EventBus.getDefault().removeStickyEvent(WebRtcViewModel.class);
        this.callScreen.setStatus(getString(R.string.RedPhone_network_failed));
    }

    private void handleNoSuchUser(WebRtcViewModel webRtcViewModel) {
        if (!isFinishing()) {
            new AlertDialog.Builder(this).setTitle(R.string.RedPhone_number_not_registered).setIcon(R.drawable.ic_warning).setMessage(R.string.RedPhone_the_number_you_dialed_does_not_support_secure_voice).setCancelable(true).setPositiveButton(R.string.RedPhone_got_it, new DialogInterface.OnClickListener(webRtcViewModel) { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda0
                public final /* synthetic */ WebRtcViewModel f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    WebRtcCallActivity.m260$r8$lambda$GdMXqqgtDUXFElvoYjLOb5ClI0(WebRtcCallActivity.this, this.f$1, dialogInterface, i);
                }
            }).setOnCancelListener(new DialogInterface.OnCancelListener(webRtcViewModel) { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda1
                public final /* synthetic */ WebRtcViewModel f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.content.DialogInterface.OnCancelListener
                public final void onCancel(DialogInterface dialogInterface) {
                    WebRtcCallActivity.$r8$lambda$PksA4wtiKUx6hUWh_LPfy_UtJTY(WebRtcCallActivity.this, this.f$1, dialogInterface);
                }
            }).show();
        }
    }

    public /* synthetic */ void lambda$handleNoSuchUser$10(WebRtcViewModel webRtcViewModel, DialogInterface dialogInterface, int i) {
        handleTerminate(webRtcViewModel.getRecipient(), HangupMessage.Type.NORMAL);
    }

    public /* synthetic */ void lambda$handleNoSuchUser$11(WebRtcViewModel webRtcViewModel, DialogInterface dialogInterface) {
        handleTerminate(webRtcViewModel.getRecipient(), HangupMessage.Type.NORMAL);
    }

    private void handleUntrustedIdentity(WebRtcViewModel webRtcViewModel) {
        IdentityKey identityKey = webRtcViewModel.getRemoteParticipants().get(0).getIdentityKey();
        Recipient recipient = webRtcViewModel.getRemoteParticipants().get(0).getRecipient();
        if (identityKey == null) {
            Log.w(TAG, "Untrusted identity without an identity key, terminating call.");
            handleTerminate(recipient, HangupMessage.Type.NORMAL);
        }
        SafetyNumberBottomSheet.forCall(recipient.getId()).show(getSupportFragmentManager());
    }

    public void handleSafetyNumberChangeEvent(WebRtcCallViewModel.SafetyNumberChangeEvent safetyNumberChangeEvent) {
        if (!Util.hasItems(safetyNumberChangeEvent.getRecipientIds())) {
            return;
        }
        if (safetyNumberChangeEvent.isInPipMode()) {
            GroupCallSafetyNumberChangeNotificationUtil.showNotification(this, this.viewModel.getRecipient().get());
            return;
        }
        GroupCallSafetyNumberChangeNotificationUtil.cancelNotification(this, this.viewModel.getRecipient().get());
        SafetyNumberBottomSheet.forDuringGroupCall(safetyNumberChangeEvent.getRecipientIds()).show(getSupportFragmentManager());
    }

    private void updateGroupMembersForGroupCall() {
        ApplicationDependencies.getSignalCallManager().requestUpdateGroupMembers();
    }

    public void handleGroupMemberCountChange(int i) {
        boolean z = ((long) i) <= FeatureFlags.maxGroupCallRingSize() && FeatureFlags.groupCallRinging();
        this.callScreen.enableRingGroup(z);
        ApplicationDependencies.getSignalCallManager().setRingGroup(z);
    }

    public void updateSpeakerHint(boolean z) {
        if (z) {
            this.callScreen.showSpeakerViewHint();
        } else {
            this.callScreen.hideSpeakerViewHint();
        }
    }

    @Override // org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeDialog.Callback
    public void onSendAnywayAfterSafetyNumberChange(List<RecipientId> list) {
        CallParticipantsState value = this.viewModel.getCallParticipantsState().getValue();
        if (value != null) {
            if (value.getGroupCallState().isConnected()) {
                ApplicationDependencies.getSignalCallManager().groupApproveSafetyChange(list);
            } else {
                this.viewModel.startCall(value.getLocalParticipant().isVideoEnabled());
            }
        }
    }

    @Override // org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeDialog.Callback
    public void onCanceled() {
        CallParticipantsState value = this.viewModel.getCallParticipantsState().getValue();
        if (value == null || !value.getGroupCallState().isNotIdle()) {
            handleTerminate(this.viewModel.getRecipient().get(), HangupMessage.Type.NORMAL);
        } else if (value.getCallState().isPreJoinOrNetworkUnavailable()) {
            ApplicationDependencies.getSignalCallManager().cancelPreJoin();
            finish();
        } else {
            handleEndCall();
        }
    }

    private boolean isSystemPipEnabledAndAvailable() {
        return Build.VERSION.SDK_INT >= 26 && getPackageManager().hasSystemFeature("android.software.picture_in_picture");
    }

    private void delayedFinish() {
        delayedFinish(1000);
    }

    private void delayedFinish(int i) {
        this.callScreen.postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda17
            @Override // java.lang.Runnable
            public final void run() {
                WebRtcCallActivity.this.finish();
            }
        }, (long) i);
    }

    @Subscribe(sticky = BuildConfig.PLAY_STORE_DISABLED, threadMode = ThreadMode.MAIN)
    public void onEventMainThread(WebRtcViewModel webRtcViewModel) {
        String str = TAG;
        Log.i(str, "Got message from service: " + webRtcViewModel);
        this.viewModel.setRecipient(webRtcViewModel.getRecipient());
        this.callScreen.setRecipient(webRtcViewModel.getRecipient());
        switch (AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[webRtcViewModel.getState().ordinal()]) {
            case 1:
                handleCallPreJoin(webRtcViewModel);
                break;
            case 2:
                handleCallConnected(webRtcViewModel);
                break;
            case 3:
                handleCallReconnecting();
                break;
            case 4:
                handleServerFailure();
                break;
            case 5:
                handleCallRinging();
                break;
            case 6:
                handleTerminate(webRtcViewModel.getRecipient(), HangupMessage.Type.NORMAL);
                break;
            case 7:
                handleGlare(webRtcViewModel.getRecipient());
                break;
            case 8:
                handleTerminate(webRtcViewModel.getRecipient(), HangupMessage.Type.ACCEPTED);
                break;
            case 9:
                handleTerminate(webRtcViewModel.getRecipient(), HangupMessage.Type.DECLINED);
                break;
            case 10:
                handleTerminate(webRtcViewModel.getRecipient(), HangupMessage.Type.BUSY);
                break;
            case 11:
                handleTerminate(webRtcViewModel.getRecipient(), HangupMessage.Type.NEED_PERMISSION);
                break;
            case 12:
                handleNoSuchUser(webRtcViewModel);
                break;
            case 13:
                handleRecipientUnavailable();
                break;
            case 14:
                handleOutgoingCall(webRtcViewModel);
                break;
            case 15:
                handleCallBusy();
                break;
            case 16:
                handleUntrustedIdentity(webRtcViewModel);
                break;
        }
        boolean z = webRtcViewModel.getLocalParticipant().getCameraState().getCameraCount() > 0 && this.enableVideoIfAvailable;
        this.viewModel.updateFromWebRtcViewModel(webRtcViewModel, z);
        if (z) {
            this.enableVideoIfAvailable = false;
            handleSetMuteVideo(false);
        }
        if (webRtcViewModel.getBluetoothPermissionDenied() && !this.hasWarnedAboutBluetooth && !isFinishing()) {
            new MaterialAlertDialogBuilder(this).setTitle(R.string.WebRtcCallActivity__bluetooth_permission_denied).setMessage(R.string.WebRtcCallActivity__please_enable_the_nearby_devices_permission_to_use_bluetooth_during_a_call).setPositiveButton(R.string.WebRtcCallActivity__open_settings, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.WebRtcCallActivity$$ExternalSyntheticLambda19
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    WebRtcCallActivity.$r8$lambda$XhKSX_piCKDfTwe0MJdirFUHvb8(WebRtcCallActivity.this, dialogInterface, i);
                }
            }).setNegativeButton(R.string.WebRtcCallActivity__not_now, (DialogInterface.OnClickListener) null).show();
            this.hasWarnedAboutBluetooth = true;
        }
    }

    public /* synthetic */ void lambda$onEventMainThread$12(DialogInterface dialogInterface, int i) {
        startActivity(Permissions.getApplicationSettingsIntent(this));
    }

    private void handleCallPreJoin(WebRtcViewModel webRtcViewModel) {
        if (webRtcViewModel.getGroupState().isNotIdle()) {
            this.callScreen.setStatusFromGroupCallState(webRtcViewModel.getGroupState());
            this.callScreen.setRingGroup(webRtcViewModel.shouldRingGroup());
            if (webRtcViewModel.shouldRingGroup() && webRtcViewModel.areRemoteDevicesInCall()) {
                ApplicationDependencies.getSignalCallManager().setRingGroup(false);
            }
        }
    }

    private void startCall(boolean z) {
        this.enableVideoIfAvailable = z;
        if (z) {
            ApplicationDependencies.getSignalCallManager().startOutgoingVideoCall(this.viewModel.getRecipient().get());
        } else {
            ApplicationDependencies.getSignalCallManager().startOutgoingAudioCall(this.viewModel.getRecipient().get());
        }
        MessageSender.onMessageSent();
    }

    /* loaded from: classes.dex */
    public final class ControlsListener implements WebRtcCallView.ControlsListener {
        private ControlsListener() {
            WebRtcCallActivity.this = r1;
        }

        /* synthetic */ ControlsListener(WebRtcCallActivity webRtcCallActivity, AnonymousClass1 r2) {
            this();
        }

        @Override // org.thoughtcrime.securesms.components.webrtc.WebRtcCallView.ControlsListener
        public void onStartCall(boolean z) {
            WebRtcCallActivity.this.viewModel.startCall(z);
        }

        @Override // org.thoughtcrime.securesms.components.webrtc.WebRtcCallView.ControlsListener
        public void onCancelStartCall() {
            WebRtcCallActivity.this.finish();
        }

        @Override // org.thoughtcrime.securesms.components.webrtc.WebRtcCallView.ControlsListener
        public void onControlsFadeOut() {
            if (WebRtcCallActivity.this.videoTooltip != null) {
                WebRtcCallActivity.this.videoTooltip.dismiss();
            }
        }

        @Override // org.thoughtcrime.securesms.components.webrtc.WebRtcCallView.ControlsListener
        public void showSystemUI() {
            WebRtcCallActivity.this.fullscreenHelper.showSystemUI();
        }

        @Override // org.thoughtcrime.securesms.components.webrtc.WebRtcCallView.ControlsListener
        public void hideSystemUI() {
            WebRtcCallActivity.this.fullscreenHelper.hideSystemUI();
        }

        @Override // org.thoughtcrime.securesms.components.webrtc.WebRtcCallView.ControlsListener
        public void onAudioOutputChanged(WebRtcAudioOutput webRtcAudioOutput) {
            int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$components$webrtc$WebRtcAudioOutput[webRtcAudioOutput.ordinal()];
            if (i == 1) {
                WebRtcCallActivity.this.handleSetAudioHandset();
            } else if (i == 2) {
                WebRtcCallActivity.this.handleSetAudioBluetooth();
            } else if (i == 3) {
                WebRtcCallActivity.this.handleSetAudioSpeaker();
            } else {
                throw new IllegalStateException("Unknown output: " + webRtcAudioOutput);
            }
        }

        @Override // org.thoughtcrime.securesms.components.webrtc.WebRtcCallView.ControlsListener
        public void onVideoChanged(boolean z) {
            WebRtcCallActivity.this.handleSetMuteVideo(!z);
        }

        @Override // org.thoughtcrime.securesms.components.webrtc.WebRtcCallView.ControlsListener
        public void onMicChanged(boolean z) {
            WebRtcCallActivity.this.handleSetMuteAudio(!z);
        }

        @Override // org.thoughtcrime.securesms.components.webrtc.WebRtcCallView.ControlsListener
        public void onCameraDirectionChanged() {
            WebRtcCallActivity.this.handleFlipCamera();
        }

        @Override // org.thoughtcrime.securesms.components.webrtc.WebRtcCallView.ControlsListener
        public void onEndCallPressed() {
            WebRtcCallActivity.this.handleEndCall();
        }

        @Override // org.thoughtcrime.securesms.components.webrtc.WebRtcCallView.ControlsListener
        public void onDenyCallPressed() {
            WebRtcCallActivity.this.handleDenyCall();
        }

        @Override // org.thoughtcrime.securesms.components.webrtc.WebRtcCallView.ControlsListener
        public void onAcceptCallWithVoiceOnlyPressed() {
            WebRtcCallActivity.this.handleAnswerWithAudio();
        }

        @Override // org.thoughtcrime.securesms.components.webrtc.WebRtcCallView.ControlsListener
        public void onAcceptCallPressed() {
            if (WebRtcCallActivity.this.viewModel.isAnswerWithVideoAvailable()) {
                WebRtcCallActivity.this.handleAnswerWithVideo();
            } else {
                WebRtcCallActivity.this.handleAnswerWithAudio();
            }
        }

        @Override // org.thoughtcrime.securesms.components.webrtc.WebRtcCallView.ControlsListener
        public void onShowParticipantsList() {
            CallParticipantsListDialog.show(WebRtcCallActivity.this.getSupportFragmentManager());
        }

        @Override // org.thoughtcrime.securesms.components.webrtc.WebRtcCallView.ControlsListener
        public void onPageChanged(CallParticipantsState.SelectedPage selectedPage) {
            WebRtcCallActivity.this.viewModel.setIsViewingFocusedParticipant(selectedPage);
        }

        @Override // org.thoughtcrime.securesms.components.webrtc.WebRtcCallView.ControlsListener
        public void onLocalPictureInPictureClicked() {
            WebRtcCallActivity.this.viewModel.onLocalPictureInPictureClicked();
        }

        @Override // org.thoughtcrime.securesms.components.webrtc.WebRtcCallView.ControlsListener
        public void onRingGroupChanged(boolean z, boolean z2) {
            if (z2) {
                ApplicationDependencies.getSignalCallManager().setRingGroup(z);
                return;
            }
            ApplicationDependencies.getSignalCallManager().setRingGroup(false);
            Toast.makeText(WebRtcCallActivity.this, (int) R.string.WebRtcCallActivity__group_is_too_large_to_ring_the_participants, 0).show();
        }
    }

    /* renamed from: org.thoughtcrime.securesms.WebRtcCallActivity$1 */
    /* loaded from: classes.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$components$webrtc$WebRtcAudioOutput;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State;

        static {
            int[] iArr = new int[WebRtcAudioOutput.values().length];
            $SwitchMap$org$thoughtcrime$securesms$components$webrtc$WebRtcAudioOutput = iArr;
            try {
                iArr[WebRtcAudioOutput.HANDSET.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$components$webrtc$WebRtcAudioOutput[WebRtcAudioOutput.HEADSET.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$components$webrtc$WebRtcAudioOutput[WebRtcAudioOutput.SPEAKER.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            int[] iArr2 = new int[WebRtcViewModel.State.values().length];
            $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State = iArr2;
            try {
                iArr2[WebRtcViewModel.State.CALL_PRE_JOIN.ordinal()] = 1;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.CALL_CONNECTED.ordinal()] = 2;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.CALL_RECONNECTING.ordinal()] = 3;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.NETWORK_FAILURE.ordinal()] = 4;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.CALL_RINGING.ordinal()] = 5;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.CALL_DISCONNECTED.ordinal()] = 6;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.CALL_DISCONNECTED_GLARE.ordinal()] = 7;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.CALL_ACCEPTED_ELSEWHERE.ordinal()] = 8;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.CALL_DECLINED_ELSEWHERE.ordinal()] = 9;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.CALL_ONGOING_ELSEWHERE.ordinal()] = 10;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.CALL_NEEDS_PERMISSION.ordinal()] = 11;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.NO_SUCH_USER.ordinal()] = 12;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.RECIPIENT_UNAVAILABLE.ordinal()] = 13;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.CALL_OUTGOING.ordinal()] = 14;
            } catch (NoSuchFieldError unused17) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.CALL_BUSY.ordinal()] = 15;
            } catch (NoSuchFieldError unused18) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.UNTRUSTED_IDENTITY.ordinal()] = 16;
            } catch (NoSuchFieldError unused19) {
            }
        }
    }

    /* loaded from: classes.dex */
    public class WindowLayoutInfoConsumer implements androidx.core.util.Consumer<WindowLayoutInfo> {
        private WindowLayoutInfoConsumer() {
            WebRtcCallActivity.this = r1;
        }

        /* synthetic */ WindowLayoutInfoConsumer(WebRtcCallActivity webRtcCallActivity, AnonymousClass1 r2) {
            this();
        }

        public void accept(WindowLayoutInfo windowLayoutInfo) {
            String str = WebRtcCallActivity.TAG;
            Log.d(str, "On WindowLayoutInfo accepted: " + windowLayoutInfo.toString());
            Optional findFirst = Collection$EL.stream(windowLayoutInfo.getDisplayFeatures()).filter(new WebRtcCallActivity$WindowLayoutInfoConsumer$$ExternalSyntheticLambda0()).findFirst();
            WebRtcCallActivity.this.viewModel.setIsLandscapeEnabled(findFirst.isPresent());
            WebRtcCallActivity.this.setRequestedOrientation(findFirst.isPresent() ? -1 : 1);
            if (findFirst.isPresent()) {
                FoldingFeature foldingFeature = (FoldingFeature) findFirst.get();
                Rect bounds = foldingFeature.getBounds();
                if (foldingFeature.isSeparating()) {
                    Log.d(WebRtcCallActivity.TAG, "OnWindowLayoutInfo accepted: ensure call view is in table-top display mode");
                    WebRtcCallActivity.this.viewModel.setFoldableState(WebRtcControls.FoldableState.folded(bounds.top));
                    return;
                }
                Log.d(WebRtcCallActivity.TAG, "OnWindowLayoutInfo accepted: ensure call view is in flat display mode");
                WebRtcCallActivity.this.viewModel.setFoldableState(WebRtcControls.FoldableState.flat());
            }
        }

        public static /* synthetic */ boolean lambda$accept$0(DisplayFeature displayFeature) {
            return displayFeature instanceof FoldingFeature;
        }
    }
}
