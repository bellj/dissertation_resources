package org.thoughtcrime.securesms.blurhash;

/* loaded from: classes3.dex */
public final class Base83 {
    private static final char[] ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz#$%*+,-.:;=?@[]^_{|}~".toCharArray();
    private static final int MAX_LENGTH;

    private static int indexOf(char[] cArr, char c) {
        for (int i = 0; i < cArr.length; i++) {
            if (cArr[i] == c) {
                return i;
            }
        }
        return -1;
    }

    public static void encode(long j, int i, char[] cArr, int i2) {
        int i3 = 1;
        int i4 = 1;
        while (i3 <= i) {
            cArr[(i2 + i) - i3] = ALPHABET[(int) ((j / ((long) i4)) % 83)];
            i3++;
            i4 *= 83;
        }
    }

    public static int decode(String str, int i, int i2) {
        char[] charArray = str.toCharArray();
        int i3 = 0;
        while (i < i2) {
            i3 = (i3 * 83) + indexOf(ALPHABET, charArray[i]);
            i++;
        }
        return i3;
    }

    public static boolean isValid(String str) {
        int length;
        if (str == null || (length = str.length()) == 0 || length > 90) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            if (indexOf(ALPHABET, str.charAt(i)) == -1) {
                return false;
            }
        }
        return true;
    }

    private Base83() {
    }
}
