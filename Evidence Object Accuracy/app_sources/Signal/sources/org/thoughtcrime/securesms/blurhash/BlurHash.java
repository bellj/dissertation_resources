package org.thoughtcrime.securesms.blurhash;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

/* loaded from: classes3.dex */
public class BlurHash implements Parcelable {
    public static final Parcelable.Creator<BlurHash> CREATOR = new Parcelable.Creator<BlurHash>() { // from class: org.thoughtcrime.securesms.blurhash.BlurHash.1
        @Override // android.os.Parcelable.Creator
        public BlurHash createFromParcel(Parcel parcel) {
            return new BlurHash(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public BlurHash[] newArray(int i) {
            return new BlurHash[i];
        }
    };
    private final String hash;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    private BlurHash(String str) {
        this.hash = str;
    }

    protected BlurHash(Parcel parcel) {
        this.hash = parcel.readString();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.hash);
    }

    public static BlurHash parseOrNull(String str) {
        if (Base83.isValid(str)) {
            return new BlurHash(str);
        }
        return null;
    }

    public String getHash() {
        return this.hash;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return Objects.equals(this.hash, ((BlurHash) obj).hash);
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Objects.hash(this.hash);
    }
}
