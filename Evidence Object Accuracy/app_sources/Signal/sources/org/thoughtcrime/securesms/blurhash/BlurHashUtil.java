package org.thoughtcrime.securesms.blurhash;

/* access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public final class BlurHashUtil {
    public static double sRGBToLinear(long j) {
        double d = (double) j;
        Double.isNaN(d);
        double d2 = d / 255.0d;
        if (d2 <= 0.04045d) {
            return d2 / 12.92d;
        }
        return Math.pow((d2 + 0.055d) / 1.055d, 2.4d);
    }

    public static long linearTosRGB(double d) {
        double pow;
        double max = Math.max(0.0d, Math.min(1.0d, d));
        if (max <= 0.0031308d) {
            pow = max * 12.92d;
        } else {
            pow = (Math.pow(max, 0.4166666666666667d) * 1.055d) - 0.055d;
        }
        return (long) ((pow * 255.0d) + 0.5d);
    }

    public static double signPow(double d, double d2) {
        return Math.copySign(Math.pow(Math.abs(d), d2), d);
    }

    public static double max(double[][] dArr, int i, int i2) {
        double d = Double.NEGATIVE_INFINITY;
        while (i < i2) {
            int i3 = 0;
            while (true) {
                double[] dArr2 = dArr[i];
                if (i3 < dArr2.length) {
                    double d2 = dArr2[i3];
                    if (d2 > d) {
                        d = d2;
                    }
                    i3++;
                }
            }
            i++;
        }
        return d;
    }

    private BlurHashUtil() {
    }
}
