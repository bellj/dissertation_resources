package org.thoughtcrime.securesms.blurhash;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.InputStream;
import java.lang.reflect.Array;

/* loaded from: classes3.dex */
public final class BlurHashEncoder {
    private BlurHashEncoder() {
    }

    public static String encode(InputStream inputStream) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 16;
        Bitmap decodeStream = BitmapFactory.decodeStream(inputStream, null, options);
        if (decodeStream == null) {
            return null;
        }
        String encode = encode(decodeStream);
        decodeStream.recycle();
        return encode;
    }

    public static String encode(Bitmap bitmap) {
        return encode(bitmap, 4, 3);
    }

    static String encode(Bitmap bitmap, int i, int i2) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int[] iArr = new int[width * height];
        bitmap.getPixels(iArr, 0, width, 0, 0, width, height);
        return encode(iArr, width, height, i, i2);
    }

    private static String encode(int[] iArr, int i, int i2, int i3, int i4) {
        double d;
        if (i3 < 1 || i3 > 9 || i4 < 1 || i4 > 9) {
            throw new IllegalArgumentException("Blur hash must have between 1 and 9 components");
        } else if (i * i2 == iArr.length) {
            double[][] dArr = (double[][]) Array.newInstance(Double.TYPE, i3 * i4, 3);
            int i5 = 0;
            while (true) {
                d = 1.0d;
                if (i5 >= i4) {
                    break;
                }
                int i6 = 0;
                while (i6 < i3) {
                    applyBasisFunction(iArr, i, i2, (i6 == 0 && i5 == 0) ? 1.0d : 2.0d, i6, i5, dArr, (i5 * i3) + i6);
                    i6++;
                    dArr = dArr;
                    i5 = i5;
                }
                i5++;
            }
            char[] cArr = new char[((dArr.length - 1) * 2) + 6];
            Base83.encode((long) ((i3 - 1) + ((i4 - 1) * 9)), 1, cArr, 0);
            if (dArr.length > 1) {
                double floor = Math.floor(Math.max(0.0d, Math.min(82.0d, Math.floor((BlurHashUtil.max(dArr, 1, dArr.length) * 166.0d) - 0.5d))));
                d = (floor + 1.0d) / 166.0d;
                Base83.encode(Math.round(floor), 1, cArr, 1);
            } else {
                Base83.encode(0, 1, cArr, 1);
            }
            Base83.encode(encodeDC(dArr[0]), 4, cArr, 2);
            for (int i7 = 1; i7 < dArr.length; i7++) {
                Base83.encode(encodeAC(dArr[i7], d), 2, cArr, ((i7 - 1) * 2) + 6);
            }
            return new String(cArr);
        } else {
            throw new IllegalArgumentException("Width and height must match the pixels array");
        }
    }

    private static void applyBasisFunction(int[] iArr, int i, int i2, double d, int i3, int i4, double[][] dArr, int i5) {
        double d2 = 0.0d;
        double d3 = 0.0d;
        double d4 = 0.0d;
        for (int i6 = 0; i6 < i; i6++) {
            int i7 = 0;
            while (i7 < i2) {
                double d5 = (double) i3;
                Double.isNaN(d5);
                double d6 = (double) i6;
                Double.isNaN(d6);
                double d7 = d5 * 3.141592653589793d * d6;
                double d8 = (double) i;
                Double.isNaN(d8);
                double d9 = (double) i4;
                Double.isNaN(d9);
                double d10 = (double) i7;
                Double.isNaN(d10);
                double d11 = d9 * 3.141592653589793d * d10;
                double d12 = (double) i2;
                Double.isNaN(d12);
                double cos = Math.cos(d7 / d8) * d * Math.cos(d11 / d12);
                int i8 = iArr[(i7 * i) + i6];
                double sRGBToLinear = d2 + (BlurHashUtil.sRGBToLinear((long) ((i8 >> 16) & 255)) * cos);
                d3 += BlurHashUtil.sRGBToLinear((long) ((i8 >> 8) & 255)) * cos;
                d4 += cos * BlurHashUtil.sRGBToLinear((long) (i8 & 255));
                i7++;
                d2 = sRGBToLinear;
            }
        }
        double d13 = (double) (i * i2);
        Double.isNaN(d13);
        double d14 = 1.0d / d13;
        double[] dArr2 = dArr[i5];
        dArr2[0] = d2 * d14;
        dArr2[1] = d3 * d14;
        dArr2[2] = d4 * d14;
    }

    private static long encodeDC(double[] dArr) {
        return (BlurHashUtil.linearTosRGB(dArr[0]) << 16) + (BlurHashUtil.linearTosRGB(dArr[1]) << 8) + BlurHashUtil.linearTosRGB(dArr[2]);
    }

    private static long encodeAC(double[] dArr, double d) {
        return Math.round((Math.floor(Math.max(0.0d, Math.min(18.0d, Math.floor((BlurHashUtil.signPow(dArr[0] / d, 0.5d) * 9.0d) + 9.5d)))) * 19.0d * 19.0d) + (Math.floor(Math.max(0.0d, Math.min(18.0d, Math.floor((BlurHashUtil.signPow(dArr[1] / d, 0.5d) * 9.0d) + 9.5d)))) * 19.0d) + Math.floor(Math.max(0.0d, Math.min(18.0d, Math.floor((BlurHashUtil.signPow(dArr[2] / d, 0.5d) * 9.0d) + 9.5d)))));
    }
}
