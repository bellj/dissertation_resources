package org.thoughtcrime.securesms.blurhash;

import android.graphics.Bitmap;
import android.graphics.Color;

/* access modifiers changed from: package-private */
/* loaded from: classes3.dex */
public class BlurHashDecoder {
    BlurHashDecoder() {
    }

    public static Bitmap decode(String str, int i, int i2) {
        return decode(str, i, i2, 1.0d);
    }

    static Bitmap decode(String str, int i, int i2, double d) {
        if (str == null || str.length() < 6) {
            return null;
        }
        int decode = Base83.decode(str, 0, 1);
        int i3 = (decode % 9) + 1;
        int i4 = (decode / 9) + 1;
        if (str.length() != (i3 * 2 * i4) + 4) {
            return null;
        }
        double decode2 = (double) (((float) (Base83.decode(str, 1, 2) + 1)) / 166.0f);
        int i5 = i3 * i4;
        double[][] dArr = new double[i5];
        for (int i6 = 0; i6 < i5; i6++) {
            if (i6 == 0) {
                dArr[i6] = decodeDc(Base83.decode(str, 2, 6));
            } else {
                int i7 = (i6 * 2) + 4;
                int decode3 = Base83.decode(str, i7, i7 + 2);
                Double.isNaN(decode2);
                dArr[i6] = decodeAc(decode3, decode2 * d);
            }
        }
        return composeBitmap(i, i2, i3, i4, dArr);
    }

    private static double[] decodeDc(int i) {
        return new double[]{BlurHashUtil.sRGBToLinear((long) (i >> 16)), BlurHashUtil.sRGBToLinear((long) ((i >> 8) & 255)), BlurHashUtil.sRGBToLinear((long) (i & 255))};
    }

    private static double[] decodeAc(int i, double d) {
        return new double[]{BlurHashUtil.signPow((double) (((float) ((i / 361) - 9)) / 9.0f), 2.0d) * d, BlurHashUtil.signPow((double) (((float) (((i / 19) % 19) - 9)) / 9.0f), 2.0d) * d, BlurHashUtil.signPow((double) (((float) ((i % 19) - 9)) / 9.0f), 2.0d) * d};
    }

    private static Bitmap composeBitmap(int i, int i2, int i3, int i4, double[][] dArr) {
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        int i5 = 0;
        while (i5 < i2) {
            int i6 = 0;
            while (i6 < i) {
                double d = 0.0d;
                int i7 = i4;
                double d2 = 0.0d;
                double d3 = 0.0d;
                int i8 = 0;
                while (i8 < i7) {
                    int i9 = 0;
                    while (i9 < i3) {
                        double d4 = (double) i6;
                        Double.isNaN(d4);
                        double d5 = (double) i9;
                        Double.isNaN(d5);
                        double d6 = d4 * 3.141592653589793d * d5;
                        double d7 = (double) i;
                        Double.isNaN(d7);
                        double cos = Math.cos(d6 / d7);
                        double d8 = (double) i5;
                        Double.isNaN(d8);
                        double d9 = (double) i8;
                        Double.isNaN(d9);
                        double d10 = d8 * 3.141592653589793d * d9;
                        double d11 = (double) i2;
                        Double.isNaN(d11);
                        double cos2 = cos * Math.cos(d10 / d11);
                        double[] dArr2 = dArr[(i8 * i3) + i9];
                        d += dArr2[0] * cos2;
                        d3 += dArr2[2] * cos2;
                        i9++;
                        d2 += dArr2[1] * cos2;
                        i5 = i5;
                    }
                    i8++;
                    i7 = i4;
                    d3 = d3;
                }
                createBitmap.setPixel(i6, i5, Color.rgb((int) BlurHashUtil.linearTosRGB(d), (int) BlurHashUtil.linearTosRGB(d2), (int) BlurHashUtil.linearTosRGB(d3)));
                i6++;
                i5 = i5;
            }
            i5++;
        }
        return createBitmap;
    }
}
