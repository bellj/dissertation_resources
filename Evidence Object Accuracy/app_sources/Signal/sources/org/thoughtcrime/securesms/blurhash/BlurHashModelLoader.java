package org.thoughtcrime.securesms.blurhash;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;
import com.bumptech.glide.signature.ObjectKey;

/* loaded from: classes3.dex */
public final class BlurHashModelLoader implements ModelLoader<BlurHash, BlurHash> {
    public boolean handles(BlurHash blurHash) {
        return true;
    }

    private BlurHashModelLoader() {
    }

    public ModelLoader.LoadData<BlurHash> buildLoadData(BlurHash blurHash, int i, int i2, Options options) {
        return new ModelLoader.LoadData<>(new ObjectKey(blurHash.getHash()), new BlurDataFetcher(blurHash));
    }

    /* loaded from: classes3.dex */
    public final class BlurDataFetcher implements DataFetcher<BlurHash> {
        private final BlurHash blurHash;

        public void cancel() {
        }

        public void cleanup() {
        }

        private BlurDataFetcher(BlurHash blurHash) {
            BlurHashModelLoader.this = r1;
            this.blurHash = blurHash;
        }

        public void loadData(Priority priority, DataFetcher.DataCallback<? super BlurHash> dataCallback) {
            dataCallback.onDataReady(this.blurHash);
        }

        public Class<BlurHash> getDataClass() {
            return BlurHash.class;
        }

        public DataSource getDataSource() {
            return DataSource.LOCAL;
        }
    }

    /* loaded from: classes3.dex */
    public static class Factory implements ModelLoaderFactory<BlurHash, BlurHash> {
        public void teardown() {
        }

        public ModelLoader<BlurHash, BlurHash> build(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new BlurHashModelLoader();
        }
    }
}
