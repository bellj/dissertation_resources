package org.thoughtcrime.securesms.blurhash;

import android.graphics.Bitmap;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.SimpleResource;
import java.io.IOException;

/* loaded from: classes3.dex */
public class BlurHashResourceDecoder implements ResourceDecoder<BlurHash, Bitmap> {
    private static final int MAX_DIMEN;

    public boolean handles(BlurHash blurHash, Options options) throws IOException {
        return true;
    }

    public Resource<Bitmap> decode(BlurHash blurHash, int i, int i2, Options options) throws IOException {
        int i3;
        int i4;
        if (i > i2) {
            i3 = Math.min(i, 20);
            i4 = (int) (((float) (i2 * i3)) / ((float) i));
        } else {
            int min = Math.min(i2, 20);
            i3 = (int) (((float) (i * min)) / ((float) i2));
            i4 = min;
        }
        return new SimpleResource(BlurHashDecoder.decode(blurHash.getHash(), i3, i4));
    }
}
