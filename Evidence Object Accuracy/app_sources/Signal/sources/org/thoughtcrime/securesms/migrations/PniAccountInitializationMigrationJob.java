package org.thoughtcrime.securesms.migrations;

import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.PreKeyUtil;
import org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore;
import org.thoughtcrime.securesms.crypto.storage.SignalServiceAccountDataStoreImpl;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.push.ServiceIdType;

/* loaded from: classes4.dex */
public class PniAccountInitializationMigrationJob extends MigrationJob {
    public static final String KEY;
    private static final String TAG = Log.tag(PniAccountInitializationMigrationJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Data serialize() {
        return super.serialize();
    }

    public PniAccountInitializationMigrationJob() {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).build());
    }

    private PniAccountInitializationMigrationJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public void performMigration() throws IOException {
        if (SignalStore.account().getPni() == null || SignalStore.account().getAci() == null || !Recipient.self().isRegistered()) {
            Log.w(TAG, "Not yet registered! No need to perform this migration.");
            return;
        }
        if (!SignalStore.account().hasPniIdentityKey()) {
            Log.i(TAG, "Generating PNI identity.");
            SignalStore.account().generatePniIdentityKeyIfNecessary();
        } else {
            Log.w(TAG, "Already generated the PNI identity. Skipping this step.");
        }
        SignalServiceAccountManager signalServiceAccountManager = ApplicationDependencies.getSignalServiceAccountManager();
        SignalServiceAccountDataStoreImpl pni = ApplicationDependencies.getProtocolStore().pni();
        PreKeyMetadataStore pniPreKeys = SignalStore.account().pniPreKeys();
        if (!pniPreKeys.isSignedPreKeyRegistered()) {
            Log.i(TAG, "Uploading signed prekey for PNI.");
            signalServiceAccountManager.setPreKeys(ServiceIdType.PNI, pni.getIdentityKeyPair().getPublicKey(), PreKeyUtil.generateAndStoreSignedPreKey(pni, pniPreKeys, true), PreKeyUtil.generateAndStoreOneTimePreKeys(pni, pniPreKeys));
            pniPreKeys.setSignedPreKeyRegistered(true);
            return;
        }
        Log.w(TAG, "Already uploaded signed prekey for PNI. Skipping this step.");
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return exc instanceof IOException;
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<PniAccountInitializationMigrationJob> {
        public PniAccountInitializationMigrationJob create(Job.Parameters parameters, Data data) {
            return new PniAccountInitializationMigrationJob(parameters);
        }
    }
}
