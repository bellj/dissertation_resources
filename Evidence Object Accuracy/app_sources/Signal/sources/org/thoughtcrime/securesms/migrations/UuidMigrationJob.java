package org.thoughtcrime.securesms.migrations;

import android.content.Context;
import android.text.TextUtils;
import java.io.IOException;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.push.ACI;

/* loaded from: classes4.dex */
public class UuidMigrationJob extends MigrationJob {
    public static final String KEY;
    private static final String TAG = Log.tag(UuidMigrationJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    /* access modifiers changed from: package-private */
    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Data serialize() {
        return super.serialize();
    }

    public UuidMigrationJob() {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).build());
    }

    private UuidMigrationJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    void performMigration() throws Exception {
        if (!SignalStore.account().isRegistered() || TextUtils.isEmpty(SignalStore.account().getE164())) {
            Log.w(TAG, "Not registered! Skipping migration, as it wouldn't do anything.");
            return;
        }
        ensureSelfRecipientExists(this.context);
        fetchOwnUuid(this.context);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return exc instanceof IOException;
    }

    private static void ensureSelfRecipientExists(Context context) {
        RecipientDatabase recipients = SignalDatabase.recipients();
        String e164 = SignalStore.account().getE164();
        Objects.requireNonNull(e164);
        recipients.getOrInsertFromE164(e164);
    }

    private static void fetchOwnUuid(Context context) throws IOException {
        RecipientId id = Recipient.self().getId();
        ACI parseOrNull = ACI.parseOrNull(ApplicationDependencies.getSignalServiceAccountManager().getWhoAmI().getAci());
        if (parseOrNull != null) {
            SignalDatabase.recipients().markRegisteredOrThrow(id, parseOrNull);
            SignalStore.account().setAci(parseOrNull);
            return;
        }
        throw new IOException("Invalid UUID!");
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<UuidMigrationJob> {
        public UuidMigrationJob create(Job.Parameters parameters, Data data) {
            return new UuidMigrationJob(parameters);
        }
    }
}
