package org.thoughtcrime.securesms.migrations;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.keyvalue.SettingsValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;

/* loaded from: classes4.dex */
public class TrimByLengthSettingsMigrationJob extends MigrationJob {
    public static final String KEY;
    private static final String TAG = Log.tag(TrimByLengthSettingsMigrationJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    /* access modifiers changed from: package-private */
    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Data serialize() {
        return super.serialize();
    }

    public TrimByLengthSettingsMigrationJob() {
        this(new Job.Parameters.Builder().build());
    }

    private TrimByLengthSettingsMigrationJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    void performMigration() throws Exception {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(ApplicationDependencies.getApplication());
        if (defaultSharedPreferences.contains(SettingsValues.THREAD_TRIM_ENABLED)) {
            SignalStore.settings().setThreadTrimByLengthEnabled(defaultSharedPreferences.getBoolean(SettingsValues.THREAD_TRIM_ENABLED, false));
            SignalStore.settings().setThreadTrimLength(Integer.parseInt(defaultSharedPreferences.getString(SettingsValues.THREAD_TRIM_LENGTH, "500")));
            defaultSharedPreferences.edit().remove(SettingsValues.THREAD_TRIM_ENABLED).remove(SettingsValues.THREAD_TRIM_LENGTH).apply();
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<TrimByLengthSettingsMigrationJob> {
        public TrimByLengthSettingsMigrationJob create(Job.Parameters parameters, Data data) {
            return new TrimByLengthSettingsMigrationJob(parameters);
        }
    }
}
