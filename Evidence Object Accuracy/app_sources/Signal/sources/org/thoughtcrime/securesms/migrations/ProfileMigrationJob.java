package org.thoughtcrime.securesms.migrations;

import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobs.ProfileUploadJob;

/* loaded from: classes4.dex */
public final class ProfileMigrationJob extends MigrationJob {
    public static final String KEY;
    private static final String TAG = Log.tag(ProfileMigrationJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Data serialize() {
        return super.serialize();
    }

    public ProfileMigrationJob() {
        this(new Job.Parameters.Builder().build());
    }

    private ProfileMigrationJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public void performMigration() {
        Log.i(TAG, "Scheduling profile upload job");
        ApplicationDependencies.getJobManager().add(new ProfileUploadJob());
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<ProfileMigrationJob> {
        public ProfileMigrationJob create(Job.Parameters parameters, Data data) {
            return new ProfileMigrationJob(parameters);
        }
    }
}
