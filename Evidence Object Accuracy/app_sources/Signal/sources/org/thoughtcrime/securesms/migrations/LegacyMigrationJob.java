package org.thoughtcrime.securesms.migrations;

import android.content.Context;
import androidx.preference.PreferenceManager;
import java.io.File;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.crypto.MasterSecret;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.PushDatabase;
import org.thoughtcrime.securesms.database.SessionDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.helpers.ClassicOpenHelper;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobs.AttachmentDownloadJob;
import org.thoughtcrime.securesms.jobs.CreateSignedPreKeyJob;
import org.thoughtcrime.securesms.jobs.DirectoryRefreshJob;
import org.thoughtcrime.securesms.jobs.PushDecryptMessageJob;
import org.thoughtcrime.securesms.jobs.RefreshAttributesJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.service.KeyCachingService;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.thoughtcrime.securesms.util.FileUtils;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.VersionTracker;
import org.whispersystems.signalservice.api.messages.SignalServiceEnvelope;

/* loaded from: classes4.dex */
public class LegacyMigrationJob extends MigrationJob {
    public static final int ASYMMETRIC_MASTER_SECRET_FIX_VERSION;
    private static final int BAD_IMPORT_CLEANUP;
    private static final int COLOR_MIGRATION;
    private static final int CONTACTS_ACCOUNT_VERSION;
    private static final int CONVERSATION_SEARCH;
    private static final int CURVE25519_VERSION;
    private static final int FULL_TEXT_SEARCH;
    private static final int IMAGE_CACHE_CLEANUP;
    private static final int INTERNALIZE_CONTACTS;
    public static final String KEY;
    private static final int MEDIA_DOWNLOAD_CONTROLS_VERSION;
    private static final int MIGRATE_SESSION_PLAINTEXT;
    public static final int MMS_BODY_VERSION;
    private static final int NO_DECRYPT_QUEUE_VERSION;
    private static final int NO_MORE_CANONICAL_DB_VERSION;
    public static final int NO_MORE_KEY_EXCHANGE_PREFIX_VERSION;
    private static final int NO_V1_VERSION;
    private static final int PERSISTENT_BLOBS;
    private static final int PROFILES;
    private static final int PUSH_DECRYPT_SERIAL_ID_VERSION;
    private static final int REDPHONE_SUPPORT_VERSION;
    private static final int REMOVE_CACHE;
    private static final int REMOVE_JOURNAL;
    private static final int SCREENSHOTS;
    private static final int SIGNALING_KEY_DEPRECATION;
    private static final int SIGNED_PREKEY_VERSION;
    public static final int SQLCIPHER;
    private static final int SQLCIPHER_COMPLETE;
    private static final String TAG = Log.tag(LegacyMigrationJob.class);
    public static final int TOFU_IDENTITIES_VERSION;
    private static final int UNIDENTIFIED_DELIVERY;
    private static final int WORKMANAGER_MIGRATION;

    /* loaded from: classes4.dex */
    public interface DatabaseUpgradeListener {
        void setProgress(int i, int i2);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return true;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Data serialize() {
        return super.serialize();
    }

    public LegacyMigrationJob() {
        this(new Job.Parameters.Builder().build());
    }

    private LegacyMigrationJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    void performMigration() throws RetryLaterException {
        File databasePath;
        File databasePath2;
        File externalFilesDir;
        Log.i(TAG, "Running background upgrade..");
        int lastSeenVersion = VersionTracker.getLastSeenVersion(this.context);
        MasterSecret masterSecret = KeyCachingService.getMasterSecret(this.context);
        if (lastSeenVersion < 334 && masterSecret != null) {
            SignalDatabase.onApplicationLevelUpgrade(this.context, masterSecret, lastSeenVersion, new DatabaseUpgradeListener() { // from class: org.thoughtcrime.securesms.migrations.LegacyMigrationJob$$ExternalSyntheticLambda0
                @Override // org.thoughtcrime.securesms.migrations.LegacyMigrationJob.DatabaseUpgradeListener
                public final void setProgress(int i, int i2) {
                    LegacyMigrationJob.lambda$performMigration$0(i, i2);
                }
            });
        } else if (lastSeenVersion < 334) {
            throw new RetryLaterException();
        }
        if (lastSeenVersion < 83) {
            File file = new File(this.context.getFilesDir(), SessionDatabase.TABLE_NAME);
            if (file.exists() && file.isDirectory()) {
                File[] listFiles = file.listFiles();
                if (listFiles != null) {
                    for (File file2 : listFiles) {
                        file2.delete();
                    }
                }
                file.delete();
            }
        }
        if (lastSeenVersion < 83) {
            CreateSignedPreKeyJob.enqueueIfNeeded();
        }
        if (lastSeenVersion < 113) {
            scheduleMessagesInPushDatabase(this.context);
        }
        if (lastSeenVersion < PUSH_DECRYPT_SERIAL_ID_VERSION) {
            scheduleMessagesInPushDatabase(this.context);
        }
        if (lastSeenVersion < 136) {
            scheduleMessagesInPushDatabase(this.context);
        }
        if (lastSeenVersion < 136) {
            ApplicationDependencies.getJobManager().add(new DirectoryRefreshJob(false));
        }
        if (lastSeenVersion < MEDIA_DOWNLOAD_CONTROLS_VERSION) {
            schedulePendingIncomingParts(this.context);
        }
        if (lastSeenVersion < REDPHONE_SUPPORT_VERSION) {
            ApplicationDependencies.getJobManager().add(new RefreshAttributesJob());
            ApplicationDependencies.getJobManager().add(new DirectoryRefreshJob(false));
        }
        if (lastSeenVersion < PROFILES) {
            ApplicationDependencies.getJobManager().add(new DirectoryRefreshJob(false));
        }
        if (lastSeenVersion < 300) {
            TextSecurePreferences.setScreenSecurityEnabled(this.context, PreferenceManager.getDefaultSharedPreferences(this.context).getBoolean(TextSecurePreferences.SCREEN_SECURITY_PREF, true));
        }
        if (lastSeenVersion < 317 && (externalFilesDir = this.context.getExternalFilesDir(null)) != null && externalFilesDir.isDirectory() && externalFilesDir.exists()) {
            File[] listFiles2 = externalFilesDir.listFiles();
            for (File file3 : listFiles2) {
                if (file3.exists() && file3.isFile()) {
                    file3.delete();
                }
            }
        }
        if (lastSeenVersion < 317 && SignalStore.account().isRegistered()) {
            TextSecurePreferences.setHasSuccessfullyRetrievedDirectory(this.context, true);
        }
        if (lastSeenVersion < 334) {
            scheduleMessagesInPushDatabase(this.context);
        }
        if (lastSeenVersion < SQLCIPHER_COMPLETE && (databasePath2 = this.context.getDatabasePath(ClassicOpenHelper.NAME)) != null && databasePath2.exists()) {
            databasePath2.delete();
        }
        if (lastSeenVersion < REMOVE_JOURNAL && (databasePath = this.context.getDatabasePath("messages.db-journal")) != null && databasePath.exists()) {
            databasePath.delete();
        }
        if (lastSeenVersion < REMOVE_CACHE) {
            FileUtils.deleteDirectoryContents(this.context.getCacheDir());
        }
        if (lastSeenVersion < IMAGE_CACHE_CLEANUP) {
            FileUtils.deleteDirectoryContents(this.context.getExternalCacheDir());
            GlideApp.get(this.context).clearDiskCache();
        }
        if (lastSeenVersion < COLOR_MIGRATION) {
            long currentTimeMillis = System.currentTimeMillis();
            SignalDatabase.recipients().updateSystemContactColors();
            Log.i(TAG, "Color migration took " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
        }
        if (lastSeenVersion < UNIDENTIFIED_DELIVERY) {
            Log.i(TAG, "Scheduling UD attributes refresh.");
            ApplicationDependencies.getJobManager().add(new RefreshAttributesJob());
        }
        if (lastSeenVersion < SIGNALING_KEY_DEPRECATION) {
            Log.i(TAG, "Scheduling a RefreshAttributesJob to remove the signaling key remotely.");
            ApplicationDependencies.getJobManager().add(new RefreshAttributesJob());
        }
    }

    public static /* synthetic */ void lambda$performMigration$0(int i, int i2) {
        String str = TAG;
        Log.i(str, "onApplicationLevelUpgrade: " + i + "/" + i2);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return exc instanceof RetryLaterException;
    }

    private void schedulePendingIncomingParts(Context context) {
        AttachmentDatabase attachments = SignalDatabase.attachments();
        MmsDatabase mms = SignalDatabase.mms();
        List<DatabaseAttachment> pendingAttachments = SignalDatabase.attachments().getPendingAttachments();
        String str = TAG;
        Log.i(str, pendingAttachments.size() + " pending parts.");
        for (DatabaseAttachment databaseAttachment : pendingAttachments) {
            MmsDatabase.Reader readerFor = MmsDatabase.readerFor(mms.getMessageCursor(databaseAttachment.getMmsId()));
            MessageRecord next = readerFor.getNext();
            if (databaseAttachment.hasData()) {
                String str2 = TAG;
                Log.i(str2, "corrected a pending media part " + databaseAttachment.getAttachmentId() + "that already had data.");
                attachments.setTransferState(databaseAttachment.getMmsId(), databaseAttachment.getAttachmentId(), 0);
            } else if (next != null && !next.isOutgoing() && next.isPush()) {
                String str3 = TAG;
                Log.i(str3, "queuing new attachment download job for incoming push part " + databaseAttachment.getAttachmentId() + ".");
                ApplicationDependencies.getJobManager().add(new AttachmentDownloadJob(databaseAttachment.getMmsId(), databaseAttachment.getAttachmentId(), false));
            }
            readerFor.close();
        }
    }

    private static void scheduleMessagesInPushDatabase(Context context) {
        PushDatabase push = SignalDatabase.push();
        JobManager jobManager = ApplicationDependencies.getJobManager();
        PushDatabase.Reader readerFor = push.readerFor(push.getPending());
        while (true) {
            try {
                SignalServiceEnvelope next = readerFor.getNext();
                if (next != null) {
                    jobManager.add(new PushDecryptMessageJob(context, next));
                } else {
                    readerFor.close();
                    return;
                }
            } catch (Throwable th) {
                if (readerFor != null) {
                    try {
                        readerFor.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<LegacyMigrationJob> {
        public LegacyMigrationJob create(Job.Parameters parameters, Data data) {
            return new LegacyMigrationJob(parameters);
        }
    }
}
