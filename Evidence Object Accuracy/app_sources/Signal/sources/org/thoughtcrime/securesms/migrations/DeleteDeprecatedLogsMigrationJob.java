package org.thoughtcrime.securesms.migrations;

import java.io.File;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;

/* loaded from: classes4.dex */
public class DeleteDeprecatedLogsMigrationJob extends MigrationJob {
    public static final String KEY;
    private static final String TAG = Log.tag(DeleteDeprecatedLogsMigrationJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    /* access modifiers changed from: package-private */
    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Data serialize() {
        return super.serialize();
    }

    public DeleteDeprecatedLogsMigrationJob() {
        this(new Job.Parameters.Builder().build());
    }

    private DeleteDeprecatedLogsMigrationJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    void performMigration() {
        File file = new File(this.context.getCacheDir(), "log");
        if (file.exists()) {
            File[] listFiles = file.listFiles();
            int i = 0;
            if (listFiles != null) {
                int length = listFiles.length;
                int i2 = 0;
                while (i < length) {
                    i2 += listFiles[i].delete() ? 1 : 0;
                    i++;
                }
                i = i2;
            }
            if (!file.delete()) {
                Log.w(TAG, "Failed to delete log directory.");
            }
            String str = TAG;
            Log.i(str, "Deleted " + i + " log files.");
            return;
        }
        Log.w(TAG, "Log directory does not exist.");
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<DeleteDeprecatedLogsMigrationJob> {
        public DeleteDeprecatedLogsMigrationJob create(Job.Parameters parameters, Data data) {
            return new DeleteDeprecatedLogsMigrationJob(parameters);
        }
    }
}
