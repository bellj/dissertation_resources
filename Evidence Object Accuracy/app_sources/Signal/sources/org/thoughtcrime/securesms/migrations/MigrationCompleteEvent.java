package org.thoughtcrime.securesms.migrations;

/* loaded from: classes4.dex */
public class MigrationCompleteEvent {
    private final int version;

    public MigrationCompleteEvent(int i) {
        this.version = i;
    }

    public int getVersion() {
        return this.version;
    }
}
