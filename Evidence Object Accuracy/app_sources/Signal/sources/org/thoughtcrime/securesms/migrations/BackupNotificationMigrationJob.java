package org.thoughtcrime.securesms.migrations;

import android.os.Build;
import java.io.IOException;
import java.util.Locale;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.backup.BackupFileIOError;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.BackupUtil;

/* loaded from: classes4.dex */
public final class BackupNotificationMigrationJob extends MigrationJob {
    public static final String KEY;
    private static final String TAG = Log.tag(BackupNotificationMigrationJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Data serialize() {
        return super.serialize();
    }

    public BackupNotificationMigrationJob() {
        this(new Job.Parameters.Builder().build());
    }

    private BackupNotificationMigrationJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public void performMigration() {
        int i = Build.VERSION.SDK_INT;
        if (i < 29 || SignalStore.settings().isBackupEnabled() || !BackupUtil.hasBackupFiles(this.context)) {
            Log.w(TAG, String.format(Locale.US, "Does not meet criteria. API: %d, BackupsEnabled: %s, HasFiles: %s", Integer.valueOf(i), Boolean.valueOf(SignalStore.settings().isBackupEnabled()), Boolean.valueOf(BackupUtil.hasBackupFiles(this.context))));
            return;
        }
        Log.w(TAG, "Stranded backup! Notifying.");
        BackupFileIOError.UNKNOWN.postNotification(this.context);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return exc instanceof IOException;
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<BackupNotificationMigrationJob> {
        public BackupNotificationMigrationJob create(Job.Parameters parameters, Data data) {
            return new BackupNotificationMigrationJob(parameters);
        }
    }
}
