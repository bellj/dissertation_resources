package org.thoughtcrime.securesms.migrations;

import java.io.File;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;

/* loaded from: classes4.dex */
public class BlobStorageLocationMigrationJob extends MigrationJob {
    public static final String KEY;
    private static final String TAG = Log.tag(BlobStorageLocationMigrationJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    /* access modifiers changed from: package-private */
    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Data serialize() {
        return super.serialize();
    }

    public BlobStorageLocationMigrationJob() {
        this(new Job.Parameters.Builder().build());
    }

    private BlobStorageLocationMigrationJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    void performMigration() {
        File[] listFiles = new File(this.context.getCacheDir(), "multi_session_blobs").listFiles();
        if (listFiles == null) {
            Log.i(TAG, "No files to move.");
            return;
        }
        Log.i(TAG, "Preparing to move " + listFiles.length + " files.");
        File dir = this.context.getDir("multi_session_blobs", 0);
        for (File file : listFiles) {
            if (file.renameTo(new File(dir, file.getName()))) {
                Log.i(TAG, "Successfully moved file: " + file.getName());
            } else {
                Log.w(TAG, "Failed to move file! " + file.getAbsolutePath());
            }
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<BlobStorageLocationMigrationJob> {
        public BlobStorageLocationMigrationJob create(Job.Parameters parameters, Data data) {
            return new BlobStorageLocationMigrationJob(parameters);
        }
    }
}
