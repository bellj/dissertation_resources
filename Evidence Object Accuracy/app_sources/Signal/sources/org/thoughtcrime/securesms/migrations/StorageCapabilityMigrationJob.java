package org.thoughtcrime.securesms.migrations;

import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobs.MultiDeviceKeysUpdateJob;
import org.thoughtcrime.securesms.jobs.MultiDeviceStorageSyncRequestJob;
import org.thoughtcrime.securesms.jobs.RefreshAttributesJob;
import org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob;
import org.thoughtcrime.securesms.jobs.StorageForcePushJob;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class StorageCapabilityMigrationJob extends MigrationJob {
    public static final String KEY;
    private static final String TAG = Log.tag(StorageCapabilityMigrationJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Data serialize() {
        return super.serialize();
    }

    public StorageCapabilityMigrationJob() {
        this(new Job.Parameters.Builder().build());
    }

    private StorageCapabilityMigrationJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public void performMigration() {
        JobManager jobManager = ApplicationDependencies.getJobManager();
        jobManager.startChain(new RefreshAttributesJob()).then(new RefreshOwnProfileJob()).enqueue();
        if (TextSecurePreferences.isMultiDevice(this.context)) {
            Log.i(TAG, "Multi-device.");
            jobManager.startChain(new StorageForcePushJob()).then(new MultiDeviceKeysUpdateJob()).then(new MultiDeviceStorageSyncRequestJob()).enqueue();
            return;
        }
        Log.i(TAG, "Single-device.");
        jobManager.add(new StorageForcePushJob());
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<StorageCapabilityMigrationJob> {
        public StorageCapabilityMigrationJob create(Job.Parameters parameters, Data data) {
            return new StorageCapabilityMigrationJob(parameters);
        }
    }
}
