package org.thoughtcrime.securesms.migrations;

import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;

/* loaded from: classes4.dex */
public final class PassingMigrationJob extends MigrationJob {
    public static final String KEY;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    /* access modifiers changed from: package-private */
    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    void performMigration() {
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Data serialize() {
        return super.serialize();
    }

    private PassingMigrationJob(Job.Parameters parameters) {
        super(parameters);
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<PassingMigrationJob> {
        public PassingMigrationJob create(Job.Parameters parameters, Data data) {
            return new PassingMigrationJob(parameters);
        }
    }
}
