package org.thoughtcrime.securesms.migrations;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.regex.Pattern;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.phonenumbers.NumberUtil;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class AvatarMigrationJob extends MigrationJob {
    public static final String KEY;
    private static final Pattern NUMBER_PATTERN = Pattern.compile("^[0-9\\-+]+$");
    private static final String TAG = Log.tag(AvatarMigrationJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return true;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Data serialize() {
        return super.serialize();
    }

    public AvatarMigrationJob() {
        this(new Job.Parameters.Builder().build());
    }

    private AvatarMigrationJob(Job.Parameters parameters) {
        super(parameters);
    }

    /* JADX INFO: finally extract failed */
    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public void performMigration() {
        File[] listFiles = new File(this.context.getFilesDir(), "avatars").listFiles();
        if (listFiles == null) {
            Log.w(TAG, "Unable to read directory, and therefore unable to migrate any avatars.");
            return;
        }
        Log.i(TAG, "Preparing to move " + listFiles.length + " avatars.");
        for (File file : listFiles) {
            try {
                try {
                    if (isValidFileName(file.getName())) {
                        AvatarHelper.setAvatar(this.context, Recipient.external(this.context, file.getName()).getId(), new ByteArrayInputStream(StreamUtil.readFully(new FileInputStream(file))));
                    } else {
                        Log.w(TAG, "Invalid file name! Can't migrate this file. It'll just get deleted.");
                    }
                } catch (IOException e) {
                    Log.w(TAG, "Failed to copy avatar file. Skipping it.", e);
                }
                file.delete();
            } catch (Throwable th) {
                file.delete();
                throw th;
            }
        }
    }

    private static boolean isValidFileName(String str) {
        return NUMBER_PATTERN.matcher(str).matches() || GroupId.isEncodedGroup(str) || NumberUtil.isValidEmail(str);
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<AvatarMigrationJob> {
        public AvatarMigrationJob create(Job.Parameters parameters, Data data) {
            return new AvatarMigrationJob(parameters);
        }
    }
}
