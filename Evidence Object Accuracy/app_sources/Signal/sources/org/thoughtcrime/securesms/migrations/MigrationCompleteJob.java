package org.thoughtcrime.securesms.migrations;

import org.greenrobot.eventbus.EventBus;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobs.BaseJob;

/* loaded from: classes4.dex */
public class MigrationCompleteJob extends BaseJob {
    public static final String KEY;
    private static final String KEY_VERSION;
    private final int version;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return true;
    }

    public MigrationCompleteJob(int i) {
        this(new Job.Parameters.Builder().setQueue(Job.Parameters.MIGRATION_QUEUE_KEY).setLifespan(-1).setMaxAttempts(-1).build(), i);
    }

    private MigrationCompleteJob(Job.Parameters parameters, int i) {
        super(parameters);
        this.version = i;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putInt(KEY_VERSION, this.version).build();
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        throw new AssertionError("This job should never fail.");
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws Exception {
        EventBus.getDefault().postSticky(new MigrationCompleteEvent(this.version));
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<MigrationCompleteJob> {
        public MigrationCompleteJob create(Job.Parameters parameters, Data data) {
            return new MigrationCompleteJob(parameters, data.getInt(MigrationCompleteJob.KEY_VERSION));
        }
    }
}
