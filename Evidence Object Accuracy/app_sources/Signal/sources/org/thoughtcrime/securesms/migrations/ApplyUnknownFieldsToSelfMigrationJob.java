package org.thoughtcrime.securesms.migrations;

import com.google.protobuf.InvalidProtocolBufferException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.RecipientRecord;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.whispersystems.signalservice.api.storage.SignalAccountRecord;
import org.whispersystems.signalservice.api.storage.StorageId;
import org.whispersystems.signalservice.internal.storage.protos.AccountRecord;

/* loaded from: classes4.dex */
public class ApplyUnknownFieldsToSelfMigrationJob extends MigrationJob {
    public static final String KEY;
    private static final String TAG = Log.tag(ApplyUnknownFieldsToSelfMigrationJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Data serialize() {
        return super.serialize();
    }

    public ApplyUnknownFieldsToSelfMigrationJob() {
        this(new Job.Parameters.Builder().build());
    }

    private ApplyUnknownFieldsToSelfMigrationJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public void performMigration() {
        if (!SignalStore.account().isRegistered() || SignalStore.account().getAci() == null) {
            Log.w(TAG, "Not registered!");
            return;
        }
        try {
            Recipient self = Recipient.self();
            RecipientRecord recordForSync = SignalDatabase.recipients().getRecordForSync(self.getId());
            if (recordForSync == null || recordForSync.getSyncExtras().getStorageProto() == null) {
                Log.d(TAG, "No unknowns to apply");
                return;
            }
            try {
                SignalAccountRecord signalAccountRecord = new SignalAccountRecord(StorageId.forAccount(self.getStorageServiceId()), AccountRecord.parseFrom(recordForSync.getSyncExtras().getStorageProto()));
                Log.d(TAG, "Applying potentially now known unknowns");
                StorageSyncHelper.applyAccountStorageSyncUpdates(this.context, self, signalAccountRecord, false);
            } catch (InvalidProtocolBufferException e) {
                Log.w(TAG, e);
            }
        } catch (RecipientDatabase.MissingRecipientException unused) {
            Log.w(TAG, "Unable to find self");
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<ApplyUnknownFieldsToSelfMigrationJob> {
        public ApplyUnknownFieldsToSelfMigrationJob create(Job.Parameters parameters, Data data) {
            return new ApplyUnknownFieldsToSelfMigrationJob(parameters);
        }
    }
}
