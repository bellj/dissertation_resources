package org.thoughtcrime.securesms.migrations;

import java.io.File;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.util.FileUtils;

/* loaded from: classes4.dex */
public class CachedAttachmentsMigrationJob extends MigrationJob {
    public static final String KEY;
    private static final String TAG = Log.tag(CachedAttachmentsMigrationJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    /* access modifiers changed from: package-private */
    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Data serialize() {
        return super.serialize();
    }

    public CachedAttachmentsMigrationJob() {
        this(new Job.Parameters.Builder().build());
    }

    private CachedAttachmentsMigrationJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    void performMigration() {
        File externalCacheDir = this.context.getExternalCacheDir();
        if (externalCacheDir == null || !externalCacheDir.exists() || !externalCacheDir.isDirectory()) {
            Log.w(TAG, "External Cache Directory either does not exist or isn't a directory. Skipping.");
            return;
        }
        FileUtils.deleteDirectoryContents(this.context.getExternalCacheDir());
        GlideApp.get(this.context).clearDiskCache();
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<CachedAttachmentsMigrationJob> {
        public CachedAttachmentsMigrationJob create(Job.Parameters parameters, Data data) {
            return new CachedAttachmentsMigrationJob(parameters);
        }
    }
}
