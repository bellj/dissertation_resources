package org.thoughtcrime.securesms.migrations;

import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobs.RefreshAttributesJob;
import org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob;
import org.thoughtcrime.securesms.jobs.StorageForcePushJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;

/* loaded from: classes4.dex */
public final class PinOptOutMigration extends MigrationJob {
    public static final String KEY;
    private static final String TAG = Log.tag(PinOptOutMigration.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    /* access modifiers changed from: package-private */
    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Data serialize() {
        return super.serialize();
    }

    public PinOptOutMigration() {
        this(new Job.Parameters.Builder().build());
    }

    private PinOptOutMigration(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    void performMigration() {
        if (SignalStore.kbsValues().hasOptedOut() && SignalStore.kbsValues().hasPin()) {
            Log.w(TAG, "Discovered a legacy opt-out user! Resetting the state.");
            SignalStore.kbsValues().optOut();
            ApplicationDependencies.getJobManager().startChain(new RefreshAttributesJob()).then(new RefreshOwnProfileJob()).then(new StorageForcePushJob()).enqueue();
        } else if (SignalStore.kbsValues().hasOptedOut()) {
            Log.i(TAG, "Discovered an opt-out user, but they're already in a good state. No action required.");
        } else {
            Log.i(TAG, "Discovered a normal PIN user. No action required.");
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<PinOptOutMigration> {
        public PinOptOutMigration create(Job.Parameters parameters, Data data) {
            return new PinOptOutMigration(parameters);
        }
    }
}
