package org.thoughtcrime.securesms.migrations;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.IntFunction;
import java.util.Arrays;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobs.StickerPackDownloadJob;
import org.thoughtcrime.securesms.stickers.BlessedPacks;

/* loaded from: classes4.dex */
public class StickerAdditionMigrationJob extends MigrationJob {
    public static final String KEY;
    private static final String KEY_PACKS;
    private static String TAG = Log.tag(StickerAdditionMigrationJob.class);
    private final List<BlessedPacks.Pack> packs;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    public StickerAdditionMigrationJob(BlessedPacks.Pack... packArr) {
        this(new Job.Parameters.Builder().build(), Arrays.asList(packArr));
    }

    private StickerAdditionMigrationJob(Job.Parameters parameters, List<BlessedPacks.Pack> list) {
        super(parameters);
        this.packs = list;
    }

    public static /* synthetic */ String[] lambda$serialize$0(int i) {
        return new String[i];
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return new Data.Builder().putStringArray(KEY_PACKS, (String[]) Stream.of(this.packs).map(new Function() { // from class: org.thoughtcrime.securesms.migrations.StickerAdditionMigrationJob$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ((BlessedPacks.Pack) obj).toJson();
            }
        }).toArray(new IntFunction() { // from class: org.thoughtcrime.securesms.migrations.StickerAdditionMigrationJob$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.IntFunction
            public final Object apply(int i) {
                return StickerAdditionMigrationJob.lambda$serialize$0(i);
            }
        })).build();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public void performMigration() {
        JobManager jobManager = ApplicationDependencies.getJobManager();
        for (BlessedPacks.Pack pack : this.packs) {
            String str = TAG;
            Log.i(str, "Installing reference for blessed pack: " + pack.getPackId());
            jobManager.add(StickerPackDownloadJob.forReference(pack.getPackId(), pack.getPackKey()));
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<StickerAdditionMigrationJob> {
        public StickerAdditionMigrationJob create(Job.Parameters parameters, Data data) {
            return new StickerAdditionMigrationJob(parameters, Stream.of(data.getStringArray(StickerAdditionMigrationJob.KEY_PACKS)).map(new StickerAdditionMigrationJob$Factory$$ExternalSyntheticLambda0()).toList());
        }
    }
}
