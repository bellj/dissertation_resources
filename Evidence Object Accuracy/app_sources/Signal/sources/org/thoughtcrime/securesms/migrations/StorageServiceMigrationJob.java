package org.thoughtcrime.securesms.migrations;

import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobs.MultiDeviceKeysUpdateJob;
import org.thoughtcrime.securesms.jobs.StorageSyncJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class StorageServiceMigrationJob extends MigrationJob {
    public static final String KEY;
    private static final String TAG = Log.tag(StorageServiceMigrationJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Data serialize() {
        return super.serialize();
    }

    public StorageServiceMigrationJob() {
        this(new Job.Parameters.Builder().build());
    }

    private StorageServiceMigrationJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public void performMigration() {
        if (SignalStore.account().getAci() == null) {
            Log.w(TAG, "Self not yet available.");
            return;
        }
        SignalDatabase.recipients().markNeedsSync(Recipient.self().getId());
        JobManager jobManager = ApplicationDependencies.getJobManager();
        if (TextSecurePreferences.isMultiDevice(this.context)) {
            Log.i(TAG, "Multi-device.");
            jobManager.startChain(new StorageSyncJob()).then(new MultiDeviceKeysUpdateJob()).enqueue();
            return;
        }
        Log.i(TAG, "Single-device.");
        jobManager.add(new StorageSyncJob());
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<StorageServiceMigrationJob> {
        public StorageServiceMigrationJob create(Job.Parameters parameters, Data data) {
            return new StorageServiceMigrationJob(parameters);
        }
    }
}
