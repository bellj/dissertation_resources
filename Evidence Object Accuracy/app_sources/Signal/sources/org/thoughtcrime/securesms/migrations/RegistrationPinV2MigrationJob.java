package org.thoughtcrime.securesms.migrations;

import android.text.TextUtils;
import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.jobs.BaseJob;
import org.thoughtcrime.securesms.pin.PinState;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.internal.contacts.crypto.UnauthenticatedResponseException;

/* loaded from: classes4.dex */
public final class RegistrationPinV2MigrationJob extends BaseJob {
    public static final String KEY;
    private static final String TAG = Log.tag(RegistrationPinV2MigrationJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
    }

    public RegistrationPinV2MigrationJob() {
        this(new Job.Parameters.Builder().setQueue(KEY).setMaxInstancesForFactory(1).addConstraint(NetworkConstraint.KEY).setLifespan(-1).setMaxAttempts(-1).build());
    }

    private RegistrationPinV2MigrationJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected void onRun() throws IOException, UnauthenticatedResponseException, InvalidKeyException {
        if (!TextSecurePreferences.isV1RegistrationLockEnabled(this.context)) {
            Log.i(TAG, "Registration lock disabled");
            return;
        }
        String deprecatedV1RegistrationLockPin = TextSecurePreferences.getDeprecatedV1RegistrationLockPin(this.context);
        if ((deprecatedV1RegistrationLockPin == null) || TextUtils.isEmpty(deprecatedV1RegistrationLockPin)) {
            Log.i(TAG, "No old pin to migrate");
            return;
        }
        String str = TAG;
        Log.i(str, "Migrating pin to Key Backup Service");
        PinState.onMigrateToRegistrationLockV2(this.context, deprecatedV1RegistrationLockPin);
        Log.i(str, "Pin migrated to Key Backup Service");
    }

    @Override // org.thoughtcrime.securesms.jobs.BaseJob
    protected boolean onShouldRetry(Exception exc) {
        return exc instanceof IOException;
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<RegistrationPinV2MigrationJob> {
        public RegistrationPinV2MigrationJob create(Job.Parameters parameters, Data data) {
            return new RegistrationPinV2MigrationJob(parameters);
        }
    }
}
