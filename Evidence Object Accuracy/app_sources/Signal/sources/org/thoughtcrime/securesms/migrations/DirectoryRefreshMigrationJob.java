package org.thoughtcrime.securesms.migrations;

import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscovery;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.keyvalue.SignalStore;

/* loaded from: classes4.dex */
public final class DirectoryRefreshMigrationJob extends MigrationJob {
    public static final String KEY;
    private static final String TAG = Log.tag(DirectoryRefreshMigrationJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Data serialize() {
        return super.serialize();
    }

    public DirectoryRefreshMigrationJob() {
        this(new Job.Parameters.Builder().build());
    }

    private DirectoryRefreshMigrationJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public void performMigration() throws IOException {
        if (!SignalStore.account().isRegistered() || !SignalStore.registrationValues().isRegistrationComplete() || SignalStore.account().getAci() == null) {
            Log.w(TAG, "Not registered! Skipping.");
        } else {
            ContactDiscovery.refreshAll(this.context, true);
        }
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return exc instanceof IOException;
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<DirectoryRefreshMigrationJob> {
        public DirectoryRefreshMigrationJob create(Job.Parameters parameters, Data data) {
            return new DirectoryRefreshMigrationJob(parameters);
        }
    }
}
