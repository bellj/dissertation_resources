package org.thoughtcrime.securesms.migrations;

import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.push.PNI;

/* loaded from: classes4.dex */
public class PniMigrationJob extends MigrationJob {
    public static final String KEY;
    private static final String TAG = Log.tag(PniMigrationJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    /* access modifiers changed from: package-private */
    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Data serialize() {
        return super.serialize();
    }

    public PniMigrationJob() {
        this(new Job.Parameters.Builder().addConstraint(NetworkConstraint.KEY).build());
    }

    private PniMigrationJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    void performMigration() throws Exception {
        if (!SignalStore.account().isRegistered() || SignalStore.account().getAci() == null) {
            Log.w(TAG, "Not registered! Skipping migration, as it wouldn't do anything.");
            return;
        }
        RecipientId id = Recipient.self().getId();
        PNI parseOrNull = PNI.parseOrNull(ApplicationDependencies.getSignalServiceAccountManager().getWhoAmI().getPni());
        if (parseOrNull != null) {
            SignalDatabase.recipients().setPni(id, parseOrNull);
            SignalStore.account().setPni(parseOrNull);
            return;
        }
        throw new IOException("Invalid PNI!");
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return exc instanceof IOException;
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<PniMigrationJob> {
        public PniMigrationJob create(Job.Parameters parameters, Data data) {
            return new PniMigrationJob(parameters);
        }
    }
}
