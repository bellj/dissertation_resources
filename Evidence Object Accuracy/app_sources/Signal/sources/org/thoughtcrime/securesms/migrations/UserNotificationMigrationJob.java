package org.thoughtcrime.securesms.migrations;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.app.TaskStackBuilder;
import java.util.Set;
import org.signal.core.util.SetUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.MainActivity;
import org.thoughtcrime.securesms.NewConversationActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.notifications.NotificationIds;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class UserNotificationMigrationJob extends MigrationJob {
    public static final String KEY;
    private static final String TAG = Log.tag(UserNotificationMigrationJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    /* access modifiers changed from: package-private */
    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Data serialize() {
        return super.serialize();
    }

    public UserNotificationMigrationJob() {
        this(new Job.Parameters.Builder().build());
    }

    private UserNotificationMigrationJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    void performMigration() {
        if (!SignalStore.account().isRegistered() || SignalStore.account().getE164() == null || SignalStore.account().getAci() == null) {
            Log.w(TAG, "Not registered! Skipping.");
        } else if (!SignalStore.settings().isNotifyWhenContactJoinsSignal()) {
            Log.w(TAG, "New contact notifications disabled! Skipping.");
        } else if (TextSecurePreferences.getFirstInstallVersion(this.context) < 759) {
            Log.w(TAG, "Install is older than v5.0.8. Skipping.");
        } else {
            ThreadDatabase threads = SignalDatabase.threads();
            if (threads.getUnarchivedConversationListCount() + threads.getArchivedConversationListCount() >= 3) {
                Log.w(TAG, "Already have 3 or more threads. Skipping.");
                return;
            }
            Set intersection = SetUtil.intersection(SignalDatabase.recipients().getRegistered(), SignalDatabase.recipients().getSystemContacts());
            if (threads.getAllThreadRecipients().containsAll(intersection)) {
                Log.w(TAG, "Threads already exist for all relevant contacts. Skipping.");
                return;
            }
            String quantityString = this.context.getResources().getQuantityString(R.plurals.UserNotificationMigrationJob_d_contacts_are_on_signal, intersection.size(), Integer.valueOf(intersection.size()));
            PendingIntent pendingIntent = TaskStackBuilder.create(this.context).addNextIntent(new Intent(this.context, MainActivity.class)).addNextIntent(new Intent(this.context, NewConversationActivity.class)).getPendingIntent(0, 0);
            Context context = this.context;
            try {
                NotificationManagerCompat.from(this.context).notify(NotificationIds.USER_NOTIFICATION_MIGRATION, new NotificationCompat.Builder(context, NotificationChannels.getMessagesChannel(context)).setSmallIcon(R.drawable.ic_notification).setContentText(quantityString).setContentIntent(pendingIntent).build());
            } catch (Throwable th) {
                Log.w(TAG, "Failed to notify!", th);
            }
        }
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements Job.Factory<UserNotificationMigrationJob> {
        public UserNotificationMigrationJob create(Job.Parameters parameters, Data data) {
            return new UserNotificationMigrationJob(parameters);
        }
    }
}
