package org.thoughtcrime.securesms.migrations;

import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobLogger;
import org.thoughtcrime.securesms.jobmanager.impl.BackoffUtil;
import org.thoughtcrime.securesms.util.FeatureFlags;

/* loaded from: classes4.dex */
public abstract class MigrationJob extends Job {
    private static final String TAG = Log.tag(MigrationJob.class);

    /* access modifiers changed from: package-private */
    public abstract boolean isUiBlocking();

    abstract void performMigration() throws Exception;

    abstract boolean shouldRetry(Exception exc);

    public MigrationJob(Job.Parameters parameters) {
        super(parameters.toBuilder().setQueue(Job.Parameters.MIGRATION_QUEUE_KEY).setMaxInstancesForFactory(1).setLifespan(-1).setMaxAttempts(-1).build());
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Data serialize() {
        return Data.EMPTY;
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public Job.Result run() {
        try {
            String str = TAG;
            Log.i(str, "About to run " + getClass().getSimpleName());
            performMigration();
            return Job.Result.success();
        } catch (RuntimeException e) {
            Log.w(TAG, JobLogger.format(this, "Encountered a runtime exception."), e);
            throw new FailedMigrationError(e);
        } catch (Exception e2) {
            if (shouldRetry(e2)) {
                Log.w(TAG, JobLogger.format(this, "Encountered a retryable exception."), e2);
                return Job.Result.retry(BackoffUtil.exponentialBackoff(getRunAttempt() + 1, FeatureFlags.getDefaultMaxBackoff()));
            }
            Log.w(TAG, JobLogger.format(this, "Encountered a non-runtime fatal exception."), e2);
            throw new FailedMigrationError(e2);
        }
    }

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public void onFailure() {
        throw new AssertionError("This job should never fail. " + getClass().getSimpleName());
    }

    /* loaded from: classes4.dex */
    public static class FailedMigrationError extends Error {
        FailedMigrationError(Throwable th) {
            super(th);
        }
    }
}
