package org.thoughtcrime.securesms.migrations;

import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.keyvalue.SignalStore;

/* loaded from: classes4.dex */
public class ProfileSharingUpdateMigrationJob extends MigrationJob {
    public static final String KEY;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return true;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Data serialize() {
        return super.serialize();
    }

    public ProfileSharingUpdateMigrationJob() {
        this(new Job.Parameters.Builder().build());
    }

    private ProfileSharingUpdateMigrationJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public void performMigration() {
        SignalDatabase.recipients().markPreMessageRequestRecipientsAsProfileSharingEnabled(SignalStore.misc().getMessageRequestEnableTime());
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<ProfileSharingUpdateMigrationJob> {
        public ProfileSharingUpdateMigrationJob create(Job.Parameters parameters, Data data) {
            return new ProfileSharingUpdateMigrationJob(parameters);
        }
    }
}
