package org.thoughtcrime.securesms.migrations;

import android.content.Intent;
import android.os.Bundle;
import androidx.lifecycle.Observer;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.BaseActivity;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class ApplicationMigrationActivity extends BaseActivity {
    private static final String TAG = Log.tag(ApplicationMigrationActivity.class);

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ApplicationMigrations.getUiBlockingMigrationStatus().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.migrations.ApplicationMigrationActivity$$ExternalSyntheticLambda0
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ApplicationMigrationActivity.this.lambda$onCreate$0((Boolean) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onCreate$0(Boolean bool) {
        if (bool != null) {
            if (bool.booleanValue()) {
                Log.i(TAG, "UI-blocking migration is in progress. Showing spinner.");
                setContentView(R.layout.application_migration_activity);
                return;
            }
            Log.i(TAG, "UI-blocking migration is no-longer in progress. Finishing.");
            startActivity((Intent) getIntent().getParcelableExtra("next_intent"));
            finish();
        }
    }
}
