package org.thoughtcrime.securesms.migrations;

import j$.util.Collection$EL;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;

/* loaded from: classes4.dex */
public final class SyncDistributionListsMigrationJob extends MigrationJob {
    public static final String KEY;
    private static final String TAG = Log.tag(SyncDistributionListsMigrationJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Data serialize() {
        return super.serialize();
    }

    public SyncDistributionListsMigrationJob() {
        this(new Job.Parameters.Builder().build());
    }

    private SyncDistributionListsMigrationJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public void performMigration() {
        if (SignalStore.account().getAci() == null) {
            Log.w(TAG, "Self not yet available.");
            return;
        }
        if (Recipient.self().getStoriesCapability() != Recipient.Capability.SUPPORTED) {
            Log.i(TAG, "Stories capability is not supported.");
        }
        SignalDatabase.recipients().markNeedsSync((List) Collection$EL.stream(SignalDatabase.distributionLists().getAllListRecipients()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.migrations.SyncDistributionListsMigrationJob$$ExternalSyntheticLambda0
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return SyncDistributionListsMigrationJob.lambda$performMigration$0((RecipientId) obj);
            }
        }).collect(Collectors.toList()));
        StorageSyncHelper.scheduleSyncForDataChange();
    }

    public static /* synthetic */ boolean lambda$performMigration$0(RecipientId recipientId) {
        try {
            Recipient.resolved(recipientId);
            return true;
        } catch (Exception e) {
            String str = TAG;
            Log.e(str, "Unable to resolve distribution list recipient: " + recipientId, e);
            return false;
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<SyncDistributionListsMigrationJob> {
        public SyncDistributionListsMigrationJob create(Job.Parameters parameters, Data data) {
            return new SyncDistributionListsMigrationJob(parameters);
        }
    }
}
