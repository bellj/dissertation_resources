package org.thoughtcrime.securesms.migrations;

import android.content.Context;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.StickerDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobs.MultiDeviceStickerPackOperationJob;
import org.thoughtcrime.securesms.jobs.StickerPackDownloadJob;
import org.thoughtcrime.securesms.stickers.BlessedPacks;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class StickerLaunchMigrationJob extends MigrationJob {
    public static final String KEY;

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Data serialize() {
        return super.serialize();
    }

    public StickerLaunchMigrationJob() {
        this(new Job.Parameters.Builder().build());
    }

    private StickerLaunchMigrationJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public void performMigration() {
        installPack(this.context, BlessedPacks.ZOZO);
        installPack(this.context, BlessedPacks.BANDIT);
    }

    private static void installPack(Context context, BlessedPacks.Pack pack) {
        JobManager jobManager = ApplicationDependencies.getJobManager();
        StickerDatabase stickers = SignalDatabase.stickers();
        if (stickers.isPackAvailableAsReference(pack.getPackId())) {
            stickers.markPackAsInstalled(pack.getPackId(), false);
        }
        jobManager.add(StickerPackDownloadJob.forInstall(pack.getPackId(), pack.getPackKey(), false));
        if (TextSecurePreferences.isMultiDevice(context)) {
            jobManager.add(new MultiDeviceStickerPackOperationJob(pack.getPackId(), pack.getPackKey(), MultiDeviceStickerPackOperationJob.Type.INSTALL));
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<StickerLaunchMigrationJob> {
        public StickerLaunchMigrationJob create(Job.Parameters parameters, Data data) {
            return new StickerLaunchMigrationJob(parameters);
        }
    }
}
