package org.thoughtcrime.securesms.migrations;

import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Data;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobs.StorageSyncJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class AccountRecordMigrationJob extends MigrationJob {
    public static final String KEY;
    private static final String TAG = Log.tag(AccountRecordMigrationJob.class);

    @Override // org.thoughtcrime.securesms.jobmanager.Job
    public String getFactoryKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public boolean isUiBlocking() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    boolean shouldRetry(Exception exc) {
        return false;
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ void onFailure() {
        super.onFailure();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Job.Result run() {
        return super.run();
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob, org.thoughtcrime.securesms.jobmanager.Job
    public /* bridge */ /* synthetic */ Data serialize() {
        return super.serialize();
    }

    public AccountRecordMigrationJob() {
        this(new Job.Parameters.Builder().build());
    }

    private AccountRecordMigrationJob(Job.Parameters parameters) {
        super(parameters);
    }

    @Override // org.thoughtcrime.securesms.migrations.MigrationJob
    public void performMigration() {
        if (!SignalStore.account().isRegistered() || SignalStore.account().getAci() == null) {
            Log.w(TAG, "Not registered!");
            return;
        }
        SignalDatabase.recipients().markNeedsSync(Recipient.self().getId());
        ApplicationDependencies.getJobManager().add(new StorageSyncJob());
    }

    /* loaded from: classes4.dex */
    public static class Factory implements Job.Factory<AccountRecordMigrationJob> {
        public AccountRecordMigrationJob create(Job.Parameters parameters, Data data) {
            return new AccountRecordMigrationJob(parameters);
        }
    }
}
