package org.thoughtcrime.securesms.migrations;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.stickers.BlessedPacks;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class StickerAdditionMigrationJob$Factory$$ExternalSyntheticLambda0 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return BlessedPacks.Pack.fromJson((String) obj);
    }
}
