package org.thoughtcrime.securesms.migrations;

import android.content.Context;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import java.util.LinkedHashMap;
import java.util.Map;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.stickers.BlessedPacks;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.VersionTracker;

/* loaded from: classes.dex */
public class ApplicationMigrations {
    public static final int CURRENT_VERSION;
    private static final int LEGACY_CANONICAL_VERSION;
    private static final String TAG = Log.tag(ApplicationMigrations.class);
    private static final MutableLiveData<Boolean> UI_BLOCKING_MIGRATION_RUNNING = new MutableLiveData<>();

    /* loaded from: classes4.dex */
    static final class Version {
        static final int ANNOUNCEMENT_GROUP_CAPABILITY;
        static final int APPLY_UNIVERSAL_EXPIRE;
        static final int ATTACHMENT_CLEANUP;
        static final int ATTACHMENT_CLEANUP_2;
        static final int AVATAR_MIGRATION;
        static final int BACKUP_NOTIFICATION;
        static final int BLOB_LOCATION;
        static final int CACHED_ATTACHMENTS;
        static final int CDS;
        static final int CHANGE_NUMBER_CAPABILITY;
        static final int CHANGE_NUMBER_CAPABILITY_2;
        static final int CHANGE_NUMBER_CAPABILITY_4;
        static final int CHANGE_NUMBER_SYNC;
        static final int DAY_BY_DAY_STICKERS;
        static final int DB_AUTOINCREMENT;
        static final int DB_REACTIONS_MIGRATION;
        static final int DEFAULT_REACTIONS_SYNC;
        static final int EMOJI_VERSION_7;
        static final int FIX_DEPRECATION;
        static final int FIX_EMOJI_QUALITY;
        static final int GV1_MIGRATION;
        static final int GV2;
        static final int GV2_2;
        static final int JUMBOMOJI_DOWNLOAD;
        static final int KBS_MIGRATION;
        static final int LEGACY;
        static final int LOG_CLEANUP;
        static final int MUTE_SYNC;
        static final int MY_STORY_PRIVACY_MODE;
        static final int PIN_OPT_OUT;
        static final int PIN_REMINDER;
        static final int PNI;
        static final int PNI_IDENTITY;
        static final int PNI_IDENTITY_2;
        static final int PNI_IDENTITY_3;
        static final int PROFILE_SHARING_UPDATE;
        static final int RECIPIENT_CLEANUP;
        static final int RECIPIENT_ID;
        static final int RECIPIENT_SEARCH;
        static final int REFRESH_EXPIRING_CREDENTIAL;
        static final int REMOVE_AVATAR_ID;
        static final int SENDER_KEY;
        static final int SENDER_KEY_2;
        static final int SENDER_KEY_3;
        static final int SMS_STORAGE_SYNC;
        static final int STICKERS_LAUNCH;
        static final int STICKER_MY_DAILY_LIFE;
        static final int STORAGE_CAPABILITY;
        static final int STORAGE_SERVICE;
        static final int STORY_DISTRIBUTION_LIST_SYNC;
        static final int SWOON_STICKERS;
        static final int SYSTEM_NAME_SPLIT;
        static final int THUMBNAIL_CLEANUP;
        static final int TRIM_SETTINGS;
        static final int USER_NOTIFICATION;
        static final int UUIDS;
        static final int VERSIONED_PROFILE;

        Version() {
        }
    }

    public static void onApplicationCreate(final Context context, JobManager jobManager) {
        if (isLegacyUpdate(context)) {
            String str = TAG;
            Log.i(str, "Detected the need for a legacy update. Last seen canonical version: " + VersionTracker.getLastSeenVersion(context));
            TextSecurePreferences.setAppMigrationVersion(context, 0);
        }
        if (!isUpdate(context)) {
            Log.d(TAG, "Not an update. Skipping.");
            VersionTracker.updateLastSeenVersion(context);
            return;
        }
        String str2 = TAG;
        Log.d(str2, "About to update. Clearing deprecation flag.");
        SignalStore.misc().clearClientDeprecated();
        int appMigrationVersion = TextSecurePreferences.getAppMigrationVersion(context);
        Log.d(str2, "currentVersion: 62,  lastSeenVersion: " + appMigrationVersion);
        LinkedHashMap<Integer, MigrationJob> migrationJobs = getMigrationJobs(context, appMigrationVersion);
        if (migrationJobs.size() > 0) {
            Log.i(str2, "About to enqueue " + migrationJobs.size() + " migration(s).");
            boolean z = true;
            final int i = appMigrationVersion;
            for (Map.Entry<Integer, MigrationJob> entry : migrationJobs.entrySet()) {
                int intValue = entry.getKey().intValue();
                MigrationJob value = entry.getValue();
                z &= value.isUiBlocking();
                if (z) {
                    i = intValue;
                }
                jobManager.add(value);
                jobManager.add(new MigrationCompleteJob(intValue));
            }
            if (i > appMigrationVersion) {
                String str3 = TAG;
                Log.i(str3, "Migration set is UI-blocking through version " + i + ".");
                UI_BLOCKING_MIGRATION_RUNNING.setValue(Boolean.TRUE);
            } else {
                Log.i(TAG, "Migration set is non-UI-blocking.");
                UI_BLOCKING_MIGRATION_RUNNING.setValue(Boolean.FALSE);
            }
            final long currentTimeMillis = System.currentTimeMillis();
            EventBus.getDefault().register(new Object() { // from class: org.thoughtcrime.securesms.migrations.ApplicationMigrations.1
                @Subscribe(sticky = BuildConfig.PLAY_STORE_DISABLED, threadMode = ThreadMode.MAIN)
                public void onMigrationComplete(MigrationCompleteEvent migrationCompleteEvent) {
                    String str4 = ApplicationMigrations.TAG;
                    Log.i(str4, "Received MigrationCompleteEvent for version " + migrationCompleteEvent.getVersion() + ". (Current: 62)");
                    if (migrationCompleteEvent.getVersion() <= 62) {
                        String str5 = ApplicationMigrations.TAG;
                        Log.i(str5, "Updating last migration version to " + migrationCompleteEvent.getVersion());
                        TextSecurePreferences.setAppMigrationVersion(context, migrationCompleteEvent.getVersion());
                        if (migrationCompleteEvent.getVersion() == 62) {
                            String str6 = ApplicationMigrations.TAG;
                            Log.i(str6, "Migration complete. Took " + (System.currentTimeMillis() - currentTimeMillis) + " ms.");
                            EventBus.getDefault().unregister(this);
                            VersionTracker.updateLastSeenVersion(context);
                            ApplicationMigrations.UI_BLOCKING_MIGRATION_RUNNING.setValue(Boolean.FALSE);
                        } else if (migrationCompleteEvent.getVersion() >= i) {
                            Log.i(ApplicationMigrations.TAG, "Version is >= the UI-blocking version. Posting 'false'.");
                            ApplicationMigrations.UI_BLOCKING_MIGRATION_RUNNING.setValue(Boolean.FALSE);
                        }
                    } else {
                        throw new AssertionError("Received a higher version than the current version? App downgrades are not supported. (received: " + migrationCompleteEvent.getVersion() + ", current: 62)");
                    }
                }
            });
            return;
        }
        Log.d(str2, "No migrations.");
        TextSecurePreferences.setAppMigrationVersion(context, 62);
        VersionTracker.updateLastSeenVersion(context);
        UI_BLOCKING_MIGRATION_RUNNING.setValue(Boolean.FALSE);
    }

    public static LiveData<Boolean> getUiBlockingMigrationStatus() {
        return UI_BLOCKING_MIGRATION_RUNNING;
    }

    public static boolean isUiBlockingMigrationRunning() {
        Boolean value = UI_BLOCKING_MIGRATION_RUNNING.getValue();
        return value != null && value.booleanValue();
    }

    public static boolean isUpdate(Context context) {
        return isLegacyUpdate(context) || TextSecurePreferences.getAppMigrationVersion(context) < 62;
    }

    private static LinkedHashMap<Integer, MigrationJob> getMigrationJobs(Context context, int i) {
        LinkedHashMap<Integer, MigrationJob> linkedHashMap = new LinkedHashMap<>();
        if (i < 1) {
            linkedHashMap.put(1, new LegacyMigrationJob());
        }
        if (i < 2) {
            linkedHashMap.put(2, new DatabaseMigrationJob());
        }
        if (i < 3) {
            linkedHashMap.put(3, new RecipientSearchMigrationJob());
        }
        if (i < 4) {
            linkedHashMap.put(4, new DatabaseMigrationJob());
        }
        if (i < 5) {
            linkedHashMap.put(5, new AvatarMigrationJob());
        }
        if (i < 6) {
            linkedHashMap.put(6, new UuidMigrationJob());
        }
        if (i < 7) {
            linkedHashMap.put(7, new CachedAttachmentsMigrationJob());
        }
        if (i < 8) {
            linkedHashMap.put(8, new StickerLaunchMigrationJob());
        }
        if (i < 10) {
            linkedHashMap.put(10, new StickerAdditionMigrationJob(BlessedPacks.SWOON_HANDS, BlessedPacks.SWOON_FACES));
        }
        if (i < 11) {
            linkedHashMap.put(11, new StorageServiceMigrationJob());
        }
        if (i < 13) {
            linkedHashMap.put(13, new AvatarIdRemovalMigrationJob());
        }
        if (i < 14) {
            linkedHashMap.put(14, new StorageCapabilityMigrationJob());
        }
        if (i < 15) {
            linkedHashMap.put(15, new PinReminderMigrationJob());
        }
        if (i < 16) {
            linkedHashMap.put(16, new ProfileMigrationJob());
        }
        if (i < 17) {
            linkedHashMap.put(17, new PinOptOutMigration());
        }
        if (i < 18) {
            linkedHashMap.put(18, new TrimByLengthSettingsMigrationJob());
        }
        if (i < 19) {
            linkedHashMap.put(19, new DatabaseMigrationJob());
        }
        if (i < 20) {
            linkedHashMap.put(20, new AttributesMigrationJob());
        }
        if (i < 21) {
            linkedHashMap.put(21, new AttributesMigrationJob());
        }
        if (i < 22) {
            linkedHashMap.put(22, new DirectoryRefreshMigrationJob());
        }
        if (i < 23) {
            linkedHashMap.put(23, new BackupNotificationMigrationJob());
        }
        if (i < 24) {
            linkedHashMap.put(24, new AttributesMigrationJob());
        }
        if (i < 25) {
            linkedHashMap.put(25, new UserNotificationMigrationJob());
        }
        if (i < 26) {
            linkedHashMap.put(26, new StickerDayByDayMigrationJob());
        }
        if (i < 27) {
            linkedHashMap.put(27, new BlobStorageLocationMigrationJob());
        }
        if (i < 28) {
            linkedHashMap.put(28, new DirectoryRefreshMigrationJob());
        }
        if (i < 31) {
            linkedHashMap.put(31, new StorageServiceMigrationJob());
        }
        if (i < 32) {
            linkedHashMap.put(32, new ProfileSharingUpdateMigrationJob());
        }
        if (i < 33) {
            linkedHashMap.put(33, new AccountRecordMigrationJob());
        }
        if (i < 34) {
            linkedHashMap.put(34, new ApplyUnknownFieldsToSelfMigrationJob());
        }
        if (i < 35) {
            linkedHashMap.put(35, new AttributesMigrationJob());
        }
        if (i < 36) {
            linkedHashMap.put(36, new AttributesMigrationJob());
        }
        if (i < 37) {
            linkedHashMap.put(37, new DatabaseMigrationJob());
        }
        if (i < 38) {
            linkedHashMap.put(38, new AttachmentCleanupMigrationJob());
        }
        if (i < 39) {
            linkedHashMap.put(39, new DeleteDeprecatedLogsMigrationJob());
        }
        if (i < 40) {
            linkedHashMap.put(40, new AttachmentCleanupMigrationJob());
        }
        if (i < 41) {
            linkedHashMap.put(41, new AttributesMigrationJob());
        }
        if (i < 42) {
            linkedHashMap.put(42, new StickerMyDailyLifeMigrationJob());
        }
        if (i < 43) {
            linkedHashMap.put(43, new AttributesMigrationJob());
        }
        if (i < 44) {
            linkedHashMap.put(44, new AccountRecordMigrationJob());
        }
        if (i < 45) {
            linkedHashMap.put(45, new AttributesMigrationJob());
        }
        if (i < 46) {
            linkedHashMap.put(46, new AttributesMigrationJob());
        }
        if (i < 47) {
            linkedHashMap.put(47, new StorageServiceMigrationJob());
        }
        if (i < 48) {
            linkedHashMap.put(48, new DatabaseMigrationJob());
        }
        if (i < 50) {
            linkedHashMap.put(50, new PniMigrationJob());
        }
        if (i < 52) {
            linkedHashMap.put(52, new EmojiDownloadMigrationJob());
        }
        if (i < 53) {
            linkedHashMap.put(53, new EmojiDownloadMigrationJob());
        }
        if (i < 54) {
            linkedHashMap.put(54, new AttributesMigrationJob());
        }
        if (i < 55) {
            linkedHashMap.put(55, new KbsEnclaveMigrationJob());
        }
        if (i < 56) {
            linkedHashMap.put(56, new PniAccountInitializationMigrationJob());
        }
        if (i < 57) {
            linkedHashMap.put(57, new PniAccountInitializationMigrationJob());
        }
        if (i < 58) {
            linkedHashMap.put(58, new PniAccountInitializationMigrationJob());
        }
        if (i < 59) {
            linkedHashMap.put(59, new StorageServiceMigrationJob());
        }
        if (i < 60) {
            linkedHashMap.put(60, new EmojiDownloadMigrationJob());
        }
        if (i < 61) {
            linkedHashMap.put(61, new SyncDistributionListsMigrationJob());
        }
        if (i < 62) {
            linkedHashMap.put(62, new AttributesMigrationJob());
        }
        return linkedHashMap;
    }

    private static boolean isLegacyUpdate(Context context) {
        return VersionTracker.getLastSeenVersion(context) < LEGACY_CANONICAL_VERSION;
    }
}
