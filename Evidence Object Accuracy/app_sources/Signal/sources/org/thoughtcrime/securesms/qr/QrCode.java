package org.thoughtcrime.securesms.qr;

import android.graphics.Bitmap;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.Stopwatch;

/* loaded from: classes4.dex */
public final class QrCode {
    public static final String TAG = Log.tag(QrCode.class);

    private QrCode() {
    }

    public static Bitmap create(String str) {
        return create(str, -16777216, 0);
    }

    public static Bitmap create(String str, int i, int i2) {
        if (str == null || str.length() == 0) {
            Log.w(TAG, "No data");
            return Bitmap.createBitmap(512, 512, Bitmap.Config.ARGB_8888);
        }
        try {
            Stopwatch stopwatch = new Stopwatch("QrGen");
            BitMatrix encode = new QRCodeWriter().encode(str, BarcodeFormat.QR_CODE, 512, 512);
            int width = encode.getWidth();
            int height = encode.getHeight();
            int[] iArr = new int[width * height];
            for (int i3 = 0; i3 < height; i3++) {
                int i4 = i3 * width;
                for (int i5 = 0; i5 < width; i5++) {
                    iArr[i4 + i5] = encode.get(i5, i3) ? i : i2;
                }
            }
            stopwatch.split("Write pixels");
            Bitmap createBitmap = Bitmap.createBitmap(iArr, width, height, Bitmap.Config.ARGB_8888);
            stopwatch.split("Create bitmap");
            stopwatch.stop(TAG);
            return createBitmap;
        } catch (WriterException e) {
            Log.w(TAG, e);
            return Bitmap.createBitmap(512, 512, Bitmap.Config.ARGB_8888);
        }
    }
}
