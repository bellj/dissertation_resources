package org.thoughtcrime.securesms.logsubmit;

import android.content.Context;
import android.database.Cursor;
import org.signal.core.util.AsciiArt;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.logsubmit.LogSection;

/* loaded from: classes4.dex */
public class LogSectionRemappedRecords implements LogSection {
    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public String getTitle() {
        return "REMAPPED RECORDS";
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public /* synthetic */ boolean hasContent() {
        return LogSection.CC.$default$hasContent(this);
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public CharSequence getContent(Context context) {
        StringBuilder sb = new StringBuilder();
        sb.append("--- Recipients");
        sb.append("\n\n");
        Cursor allRecipients = SignalDatabase.remappedRecords().getAllRecipients();
        try {
            sb.append(AsciiArt.tableFor(allRecipients));
            sb.append("\n\n");
            if (allRecipients != null) {
                allRecipients.close();
            }
            sb.append("--- Threads");
            sb.append("\n\n");
            allRecipients = SignalDatabase.remappedRecords().getAllThreads();
            try {
                sb.append(AsciiArt.tableFor(allRecipients));
                sb.append("\n");
                if (allRecipients != null) {
                    allRecipients.close();
                }
                return sb;
            } finally {
            }
        } finally {
        }
    }
}
