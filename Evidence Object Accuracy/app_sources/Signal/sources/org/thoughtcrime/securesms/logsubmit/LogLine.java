package org.thoughtcrime.securesms.logsubmit;

/* loaded from: classes4.dex */
public interface LogLine {

    /* loaded from: classes4.dex */
    public enum Placeholder {
        NONE,
        TRACE
    }

    /* loaded from: classes4.dex */
    public enum Style {
        NONE,
        VERBOSE,
        DEBUG,
        INFO,
        WARNING,
        ERROR
    }

    long getId();

    Placeholder getPlaceholderType();

    Style getStyle();

    String getText();
}
