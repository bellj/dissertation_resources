package org.thoughtcrime.securesms.logsubmit;

import android.content.Context;
import java.util.List;
import org.thoughtcrime.securesms.database.LocalMetricsDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.logsubmit.LogSection;

/* loaded from: classes4.dex */
public final class LogSectionLocalMetrics implements LogSection {
    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public String getTitle() {
        return "LOCAL METRICS";
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public /* synthetic */ boolean hasContent() {
        return LogSection.CC.$default$hasContent(this);
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public CharSequence getContent(Context context) {
        List<LocalMetricsDatabase.EventMetrics> metrics = LocalMetricsDatabase.getInstance(ApplicationDependencies.getApplication()).getMetrics();
        StringBuilder sb = new StringBuilder();
        for (LocalMetricsDatabase.EventMetrics eventMetrics : metrics) {
            sb.append(eventMetrics.getName());
            sb.append('\n');
            sb.append("  ");
            sb.append("count: ");
            sb.append(eventMetrics.getCount());
            sb.append('\n');
            sb.append("  ");
            sb.append("p50: ");
            sb.append(eventMetrics.getP50());
            sb.append('\n');
            sb.append("  ");
            sb.append("p90: ");
            sb.append(eventMetrics.getP90());
            sb.append('\n');
            sb.append("  ");
            sb.append("p99: ");
            sb.append(eventMetrics.getP99());
            sb.append('\n');
            for (LocalMetricsDatabase.SplitMetrics splitMetrics : eventMetrics.getSplits()) {
                sb.append("    ");
                sb.append(splitMetrics.getName());
                sb.append('\n');
                sb.append("      ");
                sb.append("p50: ");
                sb.append(splitMetrics.getP50());
                sb.append('\n');
                sb.append("      ");
                sb.append("p90: ");
                sb.append(splitMetrics.getP90());
                sb.append('\n');
                sb.append("      ");
                sb.append("p99: ");
                sb.append(splitMetrics.getP99());
                sb.append('\n');
            }
            sb.append("\n\n");
        }
        return sb;
    }
}
