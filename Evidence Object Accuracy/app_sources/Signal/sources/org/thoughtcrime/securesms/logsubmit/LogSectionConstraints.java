package org.thoughtcrime.securesms.logsubmit;

import android.content.Context;
import com.annimon.stream.Stream;
import java.util.Map;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Constraint;
import org.thoughtcrime.securesms.jobs.JobManagerFactories;
import org.thoughtcrime.securesms.logsubmit.LogSection;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public final class LogSectionConstraints implements LogSection {
    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public String getTitle() {
        return "CONSTRAINTS";
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public /* synthetic */ boolean hasContent() {
        return LogSection.CC.$default$hasContent(this);
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public CharSequence getContent(Context context) {
        StringBuilder sb = new StringBuilder();
        Map<String, Constraint.Factory> constraintFactories = JobManagerFactories.getConstraintFactories(ApplicationDependencies.getApplication());
        int intValue = ((Integer) Stream.of(constraintFactories.keySet()).map(new LogSectionConstraints$$ExternalSyntheticLambda0()).max(new LogSectionConstraints$$ExternalSyntheticLambda1()).orElse(0)).intValue();
        for (Map.Entry<String, Constraint.Factory> entry : constraintFactories.entrySet()) {
            sb.append(Util.rightPad(entry.getKey(), intValue));
            sb.append(": ");
            sb.append(entry.getValue().create().isMet());
            sb.append("\n");
        }
        return sb;
    }
}
