package org.thoughtcrime.securesms.logsubmit;

import android.content.Context;
import org.thoughtcrime.securesms.AppCapabilities;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.logsubmit.LogSection;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.whispersystems.signalservice.api.account.AccountAttributes;

/* loaded from: classes4.dex */
public final class LogSectionCapabilities implements LogSection {
    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public String getTitle() {
        return "CAPABILITIES";
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public /* synthetic */ boolean hasContent() {
        return LogSection.CC.$default$hasContent(this);
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public CharSequence getContent(Context context) {
        if (!SignalStore.account().isRegistered()) {
            return "Unregistered";
        }
        if (SignalStore.account().getE164() == null || SignalStore.account().getAci() == null) {
            return "Self not yet available!";
        }
        Recipient self = Recipient.self();
        AccountAttributes.Capabilities capabilities = AppCapabilities.getCapabilities(false);
        StringBuilder sb = new StringBuilder();
        sb.append("-- Local");
        sb.append("\n");
        sb.append("GV2                : ");
        sb.append(capabilities.isGv2());
        sb.append("\n");
        sb.append("GV1 Migration      : ");
        sb.append(capabilities.isGv1Migration());
        sb.append("\n");
        sb.append("Sender Key         : ");
        sb.append(capabilities.isSenderKey());
        sb.append("\n");
        sb.append("Announcement Groups: ");
        sb.append(capabilities.isAnnouncementGroup());
        sb.append("\n");
        sb.append("Change Number      : ");
        sb.append(capabilities.isChangeNumber());
        sb.append("\n");
        sb.append("Stories            : ");
        sb.append(capabilities.isStories());
        sb.append("\n");
        sb.append("Gift Badges        : ");
        sb.append(capabilities.isGiftBadges());
        sb.append("\n");
        sb.append("\n");
        sb.append("-- Global");
        sb.append("\n");
        sb.append("GV1 Migration      : ");
        sb.append(self.getGroupsV1MigrationCapability());
        sb.append("\n");
        sb.append("Sender Key         : ");
        sb.append(self.getSenderKeyCapability());
        sb.append("\n");
        sb.append("Announcement Groups: ");
        sb.append(self.getAnnouncementGroupCapability());
        sb.append("\n");
        sb.append("Change Number      : ");
        sb.append(self.getChangeNumberCapability());
        sb.append("\n");
        sb.append("Stories            : ");
        sb.append(self.getStoriesCapability());
        sb.append("\n");
        sb.append("Gift Badges        : ");
        sb.append(self.getGiftBadgesCapability());
        sb.append("\n");
        return sb;
    }
}
