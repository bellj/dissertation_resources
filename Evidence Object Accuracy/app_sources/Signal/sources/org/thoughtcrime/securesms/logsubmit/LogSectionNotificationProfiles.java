package org.thoughtcrime.securesms.logsubmit;

import android.content.Context;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.logsubmit.LogSection;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;

/* compiled from: LogSectionNotificationProfiles.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\b\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/logsubmit/LogSectionNotificationProfiles;", "Lorg/thoughtcrime/securesms/logsubmit/LogSection;", "()V", "getContent", "", "context", "Landroid/content/Context;", "getTitle", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class LogSectionNotificationProfiles implements LogSection {
    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public String getTitle() {
        return "NOTIFICATION PROFILES";
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public /* synthetic */ boolean hasContent() {
        return LogSection.CC.$default$hasContent(this);
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public CharSequence getContent(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        List<NotificationProfile> profiles = SignalDatabase.Companion.notificationProfiles().getProfiles();
        StringBuilder sb = new StringBuilder();
        sb.append("Manually enabled profile: " + SignalStore.notificationProfileValues().getManuallyEnabledProfile() + '\n');
        sb.append("Manually enabled until  : " + SignalStore.notificationProfileValues().getManuallyEnabledUntil() + '\n');
        sb.append("Manually disabled at    : " + SignalStore.notificationProfileValues().getManuallyDisabledAt() + '\n');
        sb.append("Now                     : " + System.currentTimeMillis() + "\n\n");
        sb.append("Profiles:\n");
        if (profiles.isEmpty()) {
            sb.append("  No notification profiles");
        } else {
            for (NotificationProfile notificationProfile : profiles) {
                sb.append("  Profile " + notificationProfile.getId() + '\n');
                sb.append("    allowMentions   : " + notificationProfile.getAllowAllMentions() + '\n');
                sb.append("    allowCalls      : " + notificationProfile.getAllowAllCalls() + '\n');
                sb.append("    schedule enabled: " + notificationProfile.getSchedule().getEnabled() + '\n');
                sb.append("    schedule start  : " + notificationProfile.getSchedule().getStart() + '\n');
                sb.append("    schedule end    : " + notificationProfile.getSchedule().getEnd() + '\n');
                sb.append("    schedule days   : " + CollectionsKt___CollectionsKt.sorted(notificationProfile.getSchedule().getDaysEnabled()) + '\n');
            }
        }
        String sb2 = sb.toString();
        Intrinsics.checkNotNullExpressionValue(sb2, "output.toString()");
        return sb2;
    }
}
