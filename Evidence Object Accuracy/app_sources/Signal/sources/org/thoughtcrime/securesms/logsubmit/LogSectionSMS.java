package org.thoughtcrime.securesms.logsubmit;

import android.content.Context;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.keyvalue.SettingsValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.logsubmit.LogSection;
import org.thoughtcrime.securesms.util.Util;

/* compiled from: LogSectionSMS.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\b\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/logsubmit/LogSectionSMS;", "Lorg/thoughtcrime/securesms/logsubmit/LogSection;", "()V", "getContent", "", "context", "Landroid/content/Context;", "getTitle", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class LogSectionSMS implements LogSection {
    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public String getTitle() {
        return "SMS";
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public /* synthetic */ boolean hasContent() {
        return LogSection.CC.$default$hasContent(this);
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public CharSequence getContent(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        boolean isDefaultSmsProvider = Util.isDefaultSmsProvider(context);
        SettingsValues settingsValues = SignalStore.settings();
        Intrinsics.checkNotNullExpressionValue(settingsValues, "settings()");
        StringBuilder sb = new StringBuilder();
        sb.append("Default SMS          : " + isDefaultSmsProvider + '\n');
        sb.append("SMS delivery reports : " + settingsValues.isSmsDeliveryReportsEnabled() + '\n');
        sb.append("WiFi SMS             : " + settingsValues.isWifiCallingCompatibilityModeEnabled() + '\n');
        return sb;
    }
}
