package org.thoughtcrime.securesms.logsubmit;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import com.google.android.gms.common.GoogleApiAvailability;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Locale;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.emoji.EmojiFiles;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.logsubmit.LogSection;
import org.thoughtcrime.securesms.net.StandardUserAgentInterceptor;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.service.webrtc.AndroidTelecomUtil;
import org.thoughtcrime.securesms.util.AppSignatureUtil;
import org.thoughtcrime.securesms.util.ByteUnit;
import org.thoughtcrime.securesms.util.DeviceProperties;
import org.thoughtcrime.securesms.util.NetworkUtil;
import org.thoughtcrime.securesms.util.ScreenDensity;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.VersionTracker;
import org.whispersystems.signalservice.api.push.ACI;

/* loaded from: classes4.dex */
public class LogSectionSystemInfo implements LogSection {
    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public String getTitle() {
        return "SYSINFO";
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public /* synthetic */ boolean hasContent() {
        return LogSection.CC.$default$hasContent(this);
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public CharSequence getContent(Context context) {
        PackageManager packageManager = context.getPackageManager();
        StringBuilder sb = new StringBuilder();
        sb.append("Time          : ");
        sb.append(System.currentTimeMillis());
        sb.append('\n');
        sb.append("Manufacturer  : ");
        sb.append(Build.MANUFACTURER);
        sb.append("\n");
        sb.append("Model         : ");
        sb.append(Build.MODEL);
        sb.append("\n");
        sb.append("Product       : ");
        sb.append(Build.PRODUCT);
        sb.append("\n");
        sb.append("Screen        : ");
        sb.append(getScreenResolution(context));
        sb.append(", ");
        sb.append(ScreenDensity.get(context));
        sb.append(", ");
        sb.append(getScreenRefreshRate(context));
        sb.append("\n");
        sb.append("Font Scale    : ");
        sb.append(context.getResources().getConfiguration().fontScale);
        sb.append("\n");
        sb.append("Android       : ");
        sb.append(Build.VERSION.RELEASE);
        sb.append(", API ");
        int i = Build.VERSION.SDK_INT;
        sb.append(i);
        sb.append(" (");
        sb.append(Build.VERSION.INCREMENTAL);
        sb.append(", ");
        sb.append(Build.DISPLAY);
        sb.append(")\n");
        sb.append("ABIs          : ");
        sb.append(TextUtils.join(", ", getSupportedAbis()));
        sb.append("\n");
        sb.append("Memory        : ");
        sb.append(getMemoryUsage());
        sb.append("\n");
        sb.append("Memclass      : ");
        sb.append(getMemoryClass(context));
        sb.append("\n");
        sb.append("MemInfo       : ");
        sb.append(getMemoryInfo(context));
        sb.append("\n");
        sb.append("OS Host       : ");
        sb.append(Build.HOST);
        sb.append("\n");
        sb.append("RecipientId   : ");
        Object obj = "N/A";
        sb.append(SignalStore.registrationValues().isRegistrationComplete() ? Recipient.self().getId() : obj);
        sb.append("\n");
        sb.append("ACI           : ");
        sb.append(getCensoredAci(context));
        sb.append("\n");
        sb.append("Device ID     : ");
        sb.append(SignalStore.account().getDeviceId());
        sb.append("\n");
        sb.append("Censored      : ");
        sb.append(ApplicationDependencies.getSignalServiceNetworkAccess().isCensored());
        sb.append("\n");
        sb.append("Network Status: ");
        sb.append(NetworkUtil.getNetworkStatus(context));
        sb.append("\n");
        sb.append("Play Services : ");
        sb.append(getPlayServicesString(context));
        sb.append("\n");
        sb.append("FCM           : ");
        sb.append(SignalStore.account().isFcmEnabled());
        sb.append("\n");
        sb.append("BkgRestricted : ");
        if (i >= 28) {
            obj = Boolean.valueOf(DeviceProperties.isBackgroundRestricted(context));
        }
        sb.append(obj);
        sb.append("\n");
        sb.append("Locale        : ");
        sb.append(Locale.getDefault().toString());
        sb.append("\n");
        sb.append("Linked Devices: ");
        sb.append(TextSecurePreferences.isMultiDevice(context));
        sb.append("\n");
        sb.append("First Version : ");
        sb.append(TextSecurePreferences.getFirstInstallVersion(context));
        sb.append("\n");
        sb.append("Days Installed: ");
        sb.append(VersionTracker.getDaysSinceFirstInstalled(context));
        sb.append("\n");
        sb.append("Build Variant : ");
        sb.append("website");
        sb.append(BuildConfig.BUILD_ENVIRONMENT_TYPE);
        sb.append(BuildConfig.BUILD_VARIANT_TYPE);
        sb.append("\n");
        sb.append("Emoji Version : ");
        sb.append(getEmojiVersionString(context));
        sb.append("\n");
        sb.append("Telecom       : ");
        sb.append(AndroidTelecomUtil.getTelecomSupported());
        sb.append("\n");
        sb.append("User-Agent    : ");
        sb.append(StandardUserAgentInterceptor.USER_AGENT);
        sb.append("\n");
        sb.append("App           : ");
        try {
            sb.append(packageManager.getApplicationLabel(packageManager.getApplicationInfo(context.getPackageName(), 0)));
            sb.append(" ");
            sb.append(packageManager.getPackageInfo(context.getPackageName(), 0).versionName);
            sb.append(" (");
            sb.append(BuildConfig.CANONICAL_VERSION_CODE);
            sb.append(", ");
            sb.append(Util.getManifestApkVersion(context));
            sb.append(") (");
            sb.append(BuildConfig.GIT_HASH);
            sb.append(") \n");
        } catch (PackageManager.NameNotFoundException unused) {
            sb.append("Unknown\n");
        }
        sb.append("Package       : ");
        sb.append("org.thoughtcrime.securesms");
        sb.append(" (");
        sb.append(getSigningString(context));
        sb.append(")");
        return sb;
    }

    private static String getMemoryUsage() {
        Runtime runtime = Runtime.getRuntime();
        long j = runtime.totalMemory();
        Locale locale = Locale.ENGLISH;
        ByteUnit byteUnit = ByteUnit.BYTES;
        return String.format(locale, "%dM (%.2f%% free, %dM max)", Long.valueOf(byteUnit.toMegabytes(j)), Float.valueOf((((float) runtime.freeMemory()) / ((float) j)) * 100.0f), Long.valueOf(byteUnit.toMegabytes(runtime.maxMemory())));
    }

    private static String getMemoryClass(Context context) {
        ActivityManager activityManager = ServiceUtil.getActivityManager(context);
        String str = activityManager.isLowRamDevice() ? ", low-mem device" : "";
        return activityManager.getMemoryClass() + str;
    }

    private static String getMemoryInfo(Context context) {
        ActivityManager.MemoryInfo memoryInfo = DeviceProperties.getMemoryInfo(context);
        Locale locale = Locale.US;
        ByteUnit byteUnit = ByteUnit.BYTES;
        return String.format(locale, "availMem: %d mb, totalMem: %d mb, threshold: %d mb, lowMemory: %b", Long.valueOf(byteUnit.toMegabytes(memoryInfo.availMem)), Long.valueOf(byteUnit.toMegabytes(memoryInfo.totalMem)), Long.valueOf(byteUnit.toMegabytes(memoryInfo.threshold)), Boolean.valueOf(memoryInfo.lowMemory));
    }

    private static Iterable<String> getSupportedAbis() {
        if (Build.VERSION.SDK_INT >= 21) {
            return Arrays.asList(Build.SUPPORTED_ABIS);
        }
        LinkedList linkedList = new LinkedList();
        linkedList.add(Build.CPU_ABI);
        String str = Build.CPU_ABI2;
        if (str != null && !"unknown".equals(str)) {
            linkedList.add(str);
        }
        return linkedList;
    }

    private static String getScreenResolution(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ServiceUtil.getWindowManager(context).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels + "x" + displayMetrics.heightPixels;
    }

    private static String getScreenRefreshRate(Context context) {
        return String.format(Locale.ENGLISH, "%.2f hz", Float.valueOf(ServiceUtil.getWindowManager(context).getDefaultDisplay().getRefreshRate()));
    }

    private static String getSigningString(Context context) {
        return AppSignatureUtil.getAppSignature(context);
    }

    private static String getPlayServicesString(Context context) {
        int isGooglePlayServicesAvailable = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context);
        if (isGooglePlayServicesAvailable == 0) {
            return "true";
        }
        return "false (" + isGooglePlayServicesAvailable + ")";
    }

    private static String getEmojiVersionString(Context context) {
        EmojiFiles.Version readVersion = EmojiFiles.Version.readVersion(context);
        if (readVersion == null) {
            return "None";
        }
        return readVersion.getVersion() + " (" + readVersion.getDensity() + ")";
    }

    private static String getCensoredAci(Context context) {
        ACI aci = SignalStore.account().getAci();
        if (aci == null) {
            return "N/A";
        }
        String serviceId = aci.toString();
        String substring = serviceId.substring(serviceId.length() - 2);
        return "********-****-****-****-**********" + substring;
    }
}
