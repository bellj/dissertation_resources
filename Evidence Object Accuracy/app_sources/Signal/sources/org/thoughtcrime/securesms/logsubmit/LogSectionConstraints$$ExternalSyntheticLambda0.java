package org.thoughtcrime.securesms.logsubmit;

import com.annimon.stream.function.Function;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class LogSectionConstraints$$ExternalSyntheticLambda0 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return Integer.valueOf(((String) obj).length());
    }
}
