package org.thoughtcrime.securesms.logsubmit;

import android.app.Application;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import com.annimon.stream.Stream;
import com.annimon.stream.function.BiFunction;
import com.annimon.stream.function.Function;
import j$.util.Optional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.regex.Pattern;
import java.util.zip.GZIPOutputStream;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.BufferedSink;
import okio.Okio;
import org.json.JSONException;
import org.json.JSONObject;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.signal.core.util.logging.Scrubber;
import org.signal.core.util.tracing.Tracer;
import org.thoughtcrime.securesms.database.LogDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.logsubmit.LogLine;
import org.thoughtcrime.securesms.logsubmit.SubmitDebugLogRepository;
import org.thoughtcrime.securesms.net.StandardUserAgentInterceptor;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.push.SignalServiceNetworkAccess;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.Stopwatch;

/* loaded from: classes4.dex */
public class SubmitDebugLogRepository {
    private static final String API_ENDPOINT;
    private static final int MIN_DECORATIONS;
    private static final List<LogSection> SECTIONS = new ArrayList<LogSection>() { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogRepository.1
        {
            add(new LogSectionSystemInfo());
            add(new LogSectionJobs());
            add(new LogSectionConstraints());
            add(new LogSectionCapabilities());
            add(new LogSectionLocalMetrics());
            add(new LogSectionFeatureFlags());
            add(new LogSectionPin());
            if (Build.VERSION.SDK_INT >= 28) {
                add(new LogSectionPower());
            }
            add(new LogSectionNotifications());
            add(new LogSectionNotificationProfiles());
            add(new LogSectionExoPlayerPool());
            add(new LogSectionKeyPreferences());
            add(new LogSectionSMS());
            add(new LogSectionBadges());
            add(new LogSectionPermissions());
            add(new LogSectionTrace());
            add(new LogSectionThreads());
            add(new LogSectionThreadDump());
            if (FeatureFlags.internalUser()) {
                add(new LogSectionSenderKey());
            }
            add(new LogSectionRemappedRecords());
            add(new LogSectionLogcat());
            add(new LogSectionLoggerHeader());
        }
    };
    private static final int SECTION_SPACING;
    private static final String TAG = Log.tag(SubmitDebugLogRepository.class);
    private static final char TITLE_DECORATION;
    private final Application context = ApplicationDependencies.getApplication();
    private final ExecutorService executor = SignalExecutors.SERIAL;

    /* loaded from: classes4.dex */
    public interface Callback<E> {
        void onResult(E e);
    }

    public static /* synthetic */ LogLine lambda$getLinesForSection$5(SimpleLogLine simpleLogLine) {
        return simpleLogLine;
    }

    public /* synthetic */ void lambda$getPrefixLogLines$0(Callback callback) {
        callback.onResult(getPrefixLogLinesInternal());
    }

    public void getPrefixLogLines(Callback<List<LogLine>> callback) {
        this.executor.execute(new Runnable(callback) { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ SubmitDebugLogRepository.Callback f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SubmitDebugLogRepository.this.lambda$getPrefixLogLines$0(this.f$1);
            }
        });
    }

    public void buildAndSubmitLog(Callback<Optional<String>> callback) {
        SignalExecutors.UNBOUNDED.execute(new Runnable(callback) { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ SubmitDebugLogRepository.Callback f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SubmitDebugLogRepository.this.lambda$buildAndSubmitLog$1(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$buildAndSubmitLog$1(Callback callback) {
        Log.blockUntilAllWritesFinished();
        LogDatabase.getInstance(this.context).trimToSize();
        callback.onResult(submitLogInternal(System.currentTimeMillis(), getPrefixLogLinesInternal(), Tracer.getInstance().serialize()));
    }

    public /* synthetic */ void lambda$submitLogWithPrefixLines$2(Callback callback, long j, List list, byte[] bArr) {
        callback.onResult(submitLogInternal(j, list, bArr));
    }

    public void submitLogWithPrefixLines(long j, List<LogLine> list, byte[] bArr, Callback<Optional<String>> callback) {
        SignalExecutors.UNBOUNDED.execute(new Runnable(callback, j, list, bArr) { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogRepository$$ExternalSyntheticLambda4
            public final /* synthetic */ SubmitDebugLogRepository.Callback f$1;
            public final /* synthetic */ long f$2;
            public final /* synthetic */ List f$3;
            public final /* synthetic */ byte[] f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r5;
                this.f$4 = r6;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SubmitDebugLogRepository.this.lambda$submitLogWithPrefixLines$2(this.f$1, this.f$2, this.f$3, this.f$4);
            }
        });
    }

    private Optional<String> submitLogInternal(long j, List<LogLine> list, byte[] bArr) {
        String uploadContent;
        if (bArr != null) {
            try {
                uploadContent = uploadContent(MediaUtil.OCTET, RequestBody.create(MediaType.get(MediaUtil.OCTET), bArr));
            } catch (IOException e) {
                Log.w(TAG, "Error during trace upload.", e);
                return Optional.empty();
            }
        } else {
            uploadContent = null;
        }
        StringBuilder sb = new StringBuilder();
        for (LogLine logLine : list) {
            int i = AnonymousClass3.$SwitchMap$org$thoughtcrime$securesms$logsubmit$LogLine$Placeholder[logLine.getPlaceholderType().ordinal()];
            if (i == 1) {
                sb.append(logLine.getText());
                sb.append('\n');
            } else if (i == 2) {
                sb.append(uploadContent);
                sb.append('\n');
            }
        }
        try {
            Stopwatch stopwatch = new Stopwatch("log-upload");
            ParcelFileDescriptor[] createPipe = ParcelFileDescriptor.createPipe();
            final Uri createForSingleSessionOnDiskAsync = BlobProvider.getInstance().forData(new ParcelFileDescriptor.AutoCloseInputStream(createPipe[0]), 0).withMimeType("application/gzip").createForSingleSessionOnDiskAsync(this.context, null, null);
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(new ParcelFileDescriptor.AutoCloseOutputStream(createPipe[1]));
            gZIPOutputStream.write(sb.toString().getBytes());
            stopwatch.split("front-matter");
            try {
                LogDatabase.Reader allBeforeTime = LogDatabase.getInstance(this.context).getAllBeforeTime(j);
                while (allBeforeTime.hasNext()) {
                    try {
                        gZIPOutputStream.write(allBeforeTime.next().getBytes());
                        gZIPOutputStream.write("\n".getBytes());
                    } catch (Throwable th) {
                        if (allBeforeTime != null) {
                            try {
                                allBeforeTime.close();
                            } catch (Throwable th2) {
                                th.addSuppressed(th2);
                            }
                        }
                        throw th;
                    }
                }
                allBeforeTime.close();
                StreamUtil.close(gZIPOutputStream);
                stopwatch.split("body");
                String uploadContent2 = uploadContent("application/gzip", new RequestBody() { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogRepository.2
                    @Override // okhttp3.RequestBody
                    public MediaType contentType() {
                        return MediaType.get("application/gzip");
                    }

                    @Override // okhttp3.RequestBody
                    public long contentLength() {
                        return BlobProvider.getInstance().calculateFileSize(SubmitDebugLogRepository.this.context, createForSingleSessionOnDiskAsync);
                    }

                    @Override // okhttp3.RequestBody
                    public void writeTo(BufferedSink bufferedSink) throws IOException {
                        bufferedSink.writeAll(Okio.source(BlobProvider.getInstance().getStream(SubmitDebugLogRepository.this.context, createForSingleSessionOnDiskAsync)));
                    }
                });
                stopwatch.split("upload");
                stopwatch.stop(TAG);
                BlobProvider.getInstance().delete(this.context, createForSingleSessionOnDiskAsync);
                return Optional.of(uploadContent2);
            } catch (IllegalStateException e2) {
                Log.e(TAG, "Failed to read row!", e2);
                return Optional.empty();
            }
        } catch (IOException e3) {
            Log.w(TAG, "Error during log upload.", e3);
            return Optional.empty();
        }
    }

    /* renamed from: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogRepository$3 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass3 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$logsubmit$LogLine$Placeholder;

        static {
            int[] iArr = new int[LogLine.Placeholder.values().length];
            $SwitchMap$org$thoughtcrime$securesms$logsubmit$LogLine$Placeholder = iArr;
            try {
                iArr[LogLine.Placeholder.NONE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$logsubmit$LogLine$Placeholder[LogLine.Placeholder.TRACE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    private String uploadContent(String str, RequestBody requestBody) throws IOException {
        try {
            OkHttpClient build = new OkHttpClient.Builder().addInterceptor(new StandardUserAgentInterceptor()).dns(SignalServiceNetworkAccess.DNS).build();
            Response execute = build.newCall(new Request.Builder().url(API_ENDPOINT).get().build()).execute();
            ResponseBody body = execute.body();
            if (!execute.isSuccessful() || body == null) {
                throw new IOException("Unsuccessful response: " + execute);
            }
            JSONObject jSONObject = new JSONObject(body.string());
            String string = jSONObject.getString("url");
            JSONObject jSONObject2 = jSONObject.getJSONObject("fields");
            String string2 = jSONObject2.getString("key");
            MultipartBody.Builder builder = new MultipartBody.Builder();
            Iterator<String> keys = jSONObject2.keys();
            builder.addFormDataPart("Content-Type", str);
            while (keys.hasNext()) {
                String next = keys.next();
                builder.addFormDataPart(next, jSONObject2.getString(next));
            }
            builder.addFormDataPart("file", "file", requestBody);
            Response execute2 = build.newCall(new Request.Builder().url(string).post(builder.build()).build()).execute();
            if (execute2.isSuccessful()) {
                return "https://debuglogs.org/" + string2;
            }
            throw new IOException("Bad response: " + execute2);
        } catch (JSONException e) {
            Log.w(TAG, "Error during upload.", e);
            throw new IOException(e);
        }
    }

    private List<LogLine> getPrefixLogLinesInternal() {
        long currentTimeMillis = System.currentTimeMillis();
        List<LogSection> list = SECTIONS;
        int intValue = ((Integer) Stream.of(list).reduce(0, new BiFunction() { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogRepository$$ExternalSyntheticLambda5
            @Override // com.annimon.stream.function.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return SubmitDebugLogRepository.lambda$getPrefixLogLinesInternal$3((Integer) obj, (LogSection) obj2);
            }
        })).intValue();
        ArrayList arrayList = new ArrayList();
        for (LogSection logSection : list) {
            List<LogLine> linesForSection = getLinesForSection(this.context, logSection, intValue);
            List<LogSection> list2 = SECTIONS;
            if (list2.indexOf(logSection) != list2.size() - 1) {
                for (int i = 0; i < 3; i++) {
                    linesForSection.add(SimpleLogLine.EMPTY);
                }
            }
            arrayList.addAll(linesForSection);
        }
        ArrayList arrayList2 = new ArrayList(arrayList.size());
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            arrayList2.add(new CompleteLogLine((long) i2, (LogLine) arrayList.get(i2)));
        }
        String str = TAG;
        Log.d(str, "Total time: " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
        return arrayList2;
    }

    public static /* synthetic */ Integer lambda$getPrefixLogLinesInternal$3(Integer num, LogSection logSection) {
        return Integer.valueOf(Math.max(num.intValue(), logSection.getTitle().length()));
    }

    private static List<LogLine> getLinesForSection(Context context, LogSection logSection, int i) {
        long currentTimeMillis = System.currentTimeMillis();
        ArrayList arrayList = new ArrayList();
        arrayList.add(new SimpleLogLine(formatTitle(logSection.getTitle(), i), LogLine.Style.NONE, LogLine.Placeholder.NONE));
        if (logSection.hasContent()) {
            arrayList.addAll(Stream.of(Pattern.compile("\\n").split(Scrubber.scrub(logSection.getContent(context)))).map(new Function() { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogRepository$$ExternalSyntheticLambda0
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return SubmitDebugLogRepository.lambda$getLinesForSection$4((String) obj);
                }
            }).map(new Function() { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogRepository$$ExternalSyntheticLambda1
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return SubmitDebugLogRepository.lambda$getLinesForSection$5((SimpleLogLine) obj);
                }
            }).toList());
        }
        String str = TAG;
        Log.d(str, "[" + logSection.getTitle() + "] Took " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
        return arrayList;
    }

    public static /* synthetic */ SimpleLogLine lambda$getLinesForSection$4(String str) {
        return new SimpleLogLine(str, LogStyleParser.parseStyle(str), LogStyleParser.parsePlaceholderType(str));
    }

    private static String formatTitle(String str, int i) {
        int length = i - str.length();
        int i2 = length / 2;
        int i3 = length - i2;
        StringBuilder sb = new StringBuilder();
        for (int i4 = 0; i4 < i2 + 5; i4++) {
            sb.append(TITLE_DECORATION);
        }
        sb.append(' ');
        sb.append(str);
        sb.append(' ');
        for (int i5 = 0; i5 < i3 + 5; i5++) {
            sb.append(TITLE_DECORATION);
        }
        return sb.toString();
    }
}
