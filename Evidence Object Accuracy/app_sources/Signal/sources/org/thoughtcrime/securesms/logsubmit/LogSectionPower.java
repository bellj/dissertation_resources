package org.thoughtcrime.securesms.logsubmit;

import android.app.usage.UsageStatsManager;
import android.content.Context;
import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.logsubmit.LogSection;
import org.thoughtcrime.securesms.util.BucketInfo;

/* loaded from: classes4.dex */
public class LogSectionPower implements LogSection {
    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public String getTitle() {
        return "POWER";
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public /* synthetic */ boolean hasContent() {
        return LogSection.CC.$default$hasContent(this);
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public CharSequence getContent(Context context) {
        UsageStatsManager usageStatsManager = (UsageStatsManager) context.getSystemService("usagestats");
        if (usageStatsManager == null) {
            return "UsageStatsManager not available";
        }
        BucketInfo info = BucketInfo.getInfo(usageStatsManager, TimeUnit.DAYS.toMillis(3));
        StringBuilder sb = new StringBuilder();
        sb.append("Current bucket: ");
        sb.append(BucketInfo.bucketToString(info.getCurrentBucket()));
        sb.append('\n');
        sb.append("Highest bucket: ");
        sb.append(BucketInfo.bucketToString(info.getBestBucket()));
        sb.append('\n');
        sb.append("Lowest bucket : ");
        sb.append(BucketInfo.bucketToString(info.getWorstBucket()));
        sb.append("\n\n");
        sb.append(info.getHistory());
        return sb;
    }
}
