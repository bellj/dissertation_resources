package org.thoughtcrime.securesms.logsubmit;

import org.thoughtcrime.securesms.logsubmit.LogLine;

/* loaded from: classes4.dex */
public class SimpleLogLine implements LogLine {
    static final SimpleLogLine EMPTY = new SimpleLogLine("", LogLine.Style.NONE, LogLine.Placeholder.NONE);
    private final LogLine.Placeholder placeholder;
    private final LogLine.Style style;
    private final String text;

    @Override // org.thoughtcrime.securesms.logsubmit.LogLine
    public long getId() {
        return -1;
    }

    public SimpleLogLine(String str, LogLine.Style style, LogLine.Placeholder placeholder) {
        this.text = str;
        this.style = style;
        this.placeholder = placeholder;
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogLine
    public String getText() {
        return this.text;
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogLine
    public LogLine.Style getStyle() {
        return this.style;
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogLine
    public LogLine.Placeholder getPlaceholderType() {
        return this.placeholder;
    }
}
