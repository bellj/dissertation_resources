package org.thoughtcrime.securesms.logsubmit;

import android.content.Context;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.logsubmit.LogSection;
import org.thoughtcrime.securesms.video.exo.ExoPlayerPool;

/* compiled from: LogSectionExoPlayerPool.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\b\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/logsubmit/LogSectionExoPlayerPool;", "Lorg/thoughtcrime/securesms/logsubmit/LogSection;", "()V", "getContent", "", "context", "Landroid/content/Context;", "getTitle", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class LogSectionExoPlayerPool implements LogSection {
    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public String getTitle() {
        return "EXOPLAYER POOL";
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public /* synthetic */ boolean hasContent() {
        return LogSection.CC.$default$hasContent(this);
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public CharSequence getContent(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        ExoPlayerPool.PoolStats poolStats = ApplicationDependencies.getExoPlayerPool().getPoolStats();
        List<ExoPlayerPool.OwnershipInfo> owners = poolStats.getOwners();
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object obj : owners) {
            String tag = ((ExoPlayerPool.OwnershipInfo) obj).getTag();
            Object obj2 = linkedHashMap.get(tag);
            if (obj2 == null) {
                obj2 = new ArrayList();
                linkedHashMap.put(tag, obj2);
            }
            ((List) obj2).add(obj);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Total players created: " + poolStats.getCreated() + '\n');
        sb.append("Max allowed unreserved instances: " + poolStats.getMaxUnreserved() + '\n');
        sb.append("Max allowed reserved instances: " + poolStats.getMaxReserved() + '\n');
        sb.append("Available created unreserved instances: " + poolStats.getUnreservedAndAvailable() + '\n');
        sb.append("Available created reserved instances: " + poolStats.getReservedAndAvailable() + '\n');
        sb.append("Total unreserved created: " + poolStats.getUnreserved() + '\n');
        sb.append("Total reserved created: " + poolStats.getReserved() + "\n\n");
        sb.append("Ownership Info:\n");
        if (linkedHashMap.isEmpty()) {
            sb.append("  No ownership info to display.");
        } else {
            for (Map.Entry entry : linkedHashMap.entrySet()) {
                List list = (List) entry.getValue();
                sb.append("  Owner " + ((String) entry.getKey()) + '\n');
                StringBuilder sb2 = new StringBuilder();
                sb2.append("    reserved: ");
                ArrayList arrayList = new ArrayList();
                for (Object obj3 : list) {
                    if (((ExoPlayerPool.OwnershipInfo) obj3).isReserved()) {
                        arrayList.add(obj3);
                    }
                }
                sb2.append(arrayList.size());
                sb2.append('\n');
                sb.append(sb2.toString());
                StringBuilder sb3 = new StringBuilder();
                sb3.append("    unreserved: ");
                ArrayList arrayList2 = new ArrayList();
                for (Object obj4 : list) {
                    if (!((ExoPlayerPool.OwnershipInfo) obj4).isReserved()) {
                        arrayList2.add(obj4);
                    }
                }
                sb3.append(arrayList2.size());
                sb3.append('\n');
                sb.append(sb3.toString());
            }
        }
        return sb;
    }
}
