package org.thoughtcrime.securesms.logsubmit;

import java.util.Comparator;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class LogSectionConstraints$$ExternalSyntheticLambda1 implements Comparator {
    @Override // java.util.Comparator
    public final int compare(Object obj, Object obj2) {
        return ((Integer) obj).compareTo((Integer) obj2);
    }
}
