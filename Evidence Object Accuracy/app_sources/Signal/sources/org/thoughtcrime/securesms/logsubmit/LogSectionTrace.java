package org.thoughtcrime.securesms.logsubmit;

import android.content.Context;
import org.thoughtcrime.securesms.logsubmit.LogSection;

/* loaded from: classes4.dex */
public class LogSectionTrace implements LogSection {
    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public CharSequence getContent(Context context) {
        return LogStyleParser.TRACE_PLACEHOLDER;
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public String getTitle() {
        return "TRACE";
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public /* synthetic */ boolean hasContent() {
        return LogSection.CC.$default$hasContent(this);
    }
}
