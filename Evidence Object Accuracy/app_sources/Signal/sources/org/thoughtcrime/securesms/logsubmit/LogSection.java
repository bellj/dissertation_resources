package org.thoughtcrime.securesms.logsubmit;

import android.content.Context;

/* loaded from: classes4.dex */
public interface LogSection {

    /* renamed from: org.thoughtcrime.securesms.logsubmit.LogSection$-CC */
    /* loaded from: classes4.dex */
    public final /* synthetic */ class CC {
        public static boolean $default$hasContent(LogSection logSection) {
            return true;
        }
    }

    CharSequence getContent(Context context);

    String getTitle();

    boolean hasContent();
}
