package org.thoughtcrime.securesms.logsubmit;

import org.thoughtcrime.securesms.components.ListenableHorizontalScrollView;
import org.thoughtcrime.securesms.logsubmit.SubmitDebugLogAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SubmitDebugLogAdapter$LineViewHolder$$ExternalSyntheticLambda0 implements ListenableHorizontalScrollView.OnScrollListener {
    public final /* synthetic */ SubmitDebugLogAdapter.ScrollManager f$0;

    public /* synthetic */ SubmitDebugLogAdapter$LineViewHolder$$ExternalSyntheticLambda0(SubmitDebugLogAdapter.ScrollManager scrollManager) {
        this.f$0 = scrollManager;
    }

    @Override // org.thoughtcrime.securesms.components.ListenableHorizontalScrollView.OnScrollListener
    public final void onScroll(int i, int i2) {
        SubmitDebugLogAdapter.LineViewHolder.lambda$bind$0(this.f$0, i, i2);
    }
}
