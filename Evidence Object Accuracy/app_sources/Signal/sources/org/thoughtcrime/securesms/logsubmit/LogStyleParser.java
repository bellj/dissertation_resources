package org.thoughtcrime.securesms.logsubmit;

import java.util.HashMap;
import java.util.Map;
import org.thoughtcrime.securesms.logsubmit.LogLine;

/* loaded from: classes4.dex */
public class LogStyleParser {
    private static final Map<String, LogLine.Style> STYLE_MARKERS = new HashMap<String, LogLine.Style>() { // from class: org.thoughtcrime.securesms.logsubmit.LogStyleParser.1
        {
            put(" V ", LogLine.Style.VERBOSE);
            put(" D ", LogLine.Style.DEBUG);
            put(" I ", LogLine.Style.INFO);
            put(" W ", LogLine.Style.WARNING);
            put(" E ", LogLine.Style.ERROR);
        }
    };
    public static final String TRACE_PLACEHOLDER;

    public static LogLine.Style parseStyle(String str) {
        for (Map.Entry<String, LogLine.Style> entry : STYLE_MARKERS.entrySet()) {
            if (str.contains(entry.getKey())) {
                return entry.getValue();
            }
        }
        return LogLine.Style.NONE;
    }

    public static LogLine.Placeholder parsePlaceholderType(String str) {
        if (str.equals(TRACE_PLACEHOLDER)) {
            return LogLine.Placeholder.TRACE;
        }
        return LogLine.Placeholder.NONE;
    }
}
