package org.thoughtcrime.securesms.logsubmit;

import android.view.View;
import org.thoughtcrime.securesms.logsubmit.SubmitDebugLogAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SubmitDebugLogAdapter$LineViewHolder$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ SubmitDebugLogAdapter.Listener f$0;
    public final /* synthetic */ LogLine f$1;

    public /* synthetic */ SubmitDebugLogAdapter$LineViewHolder$$ExternalSyntheticLambda1(SubmitDebugLogAdapter.Listener listener, LogLine logLine) {
        this.f$0 = listener;
        this.f$1 = logLine;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.onLogDeleted(this.f$1);
    }
}
