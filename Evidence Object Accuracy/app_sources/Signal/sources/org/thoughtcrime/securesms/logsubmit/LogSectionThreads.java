package org.thoughtcrime.securesms.logsubmit;

import android.content.Context;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import org.thoughtcrime.securesms.logsubmit.LogSection;

/* loaded from: classes4.dex */
public class LogSectionThreads implements LogSection {
    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public String getTitle() {
        return "THREADS";
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public /* synthetic */ boolean hasContent() {
        return LogSection.CC.$default$hasContent(this);
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public CharSequence getContent(Context context) {
        StringBuilder sb = new StringBuilder();
        ArrayList<Thread> arrayList = new ArrayList(Thread.getAllStackTraces().keySet());
        Collections.sort(arrayList, new Comparator() { // from class: org.thoughtcrime.securesms.logsubmit.LogSectionThreads$$ExternalSyntheticLambda0
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return LogSectionThreads.$r8$lambda$fY7RyP6wEoy3XjapMBui0tx9eTg((Thread) obj, (Thread) obj2);
            }
        });
        for (Thread thread : arrayList) {
            sb.append("[");
            sb.append(thread.getId());
            sb.append("] ");
            sb.append(thread.getName());
            sb.append("\n");
        }
        return sb;
    }

    public static /* synthetic */ int lambda$getContent$0(Thread thread, Thread thread2) {
        return Long.compare(thread.getId(), thread2.getId());
    }
}
