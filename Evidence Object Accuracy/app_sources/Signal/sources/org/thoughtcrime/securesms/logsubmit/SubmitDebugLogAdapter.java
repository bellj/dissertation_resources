package org.thoughtcrime.securesms.logsubmit;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import org.signal.paging.PagingController;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.ListenableHorizontalScrollView;
import org.thoughtcrime.securesms.logsubmit.LogLine;

/* loaded from: classes4.dex */
public class SubmitDebugLogAdapter extends RecyclerView.Adapter<LineViewHolder> {
    private static final int LINE_LENGTH;
    private static final int TYPE_LOG;
    private static final int TYPE_PLACEHOLDER;
    private boolean editing;
    private final List<LogLine> lines = new ArrayList();
    private final Listener listener;
    private final PagingController pagingController;
    private final ScrollManager scrollManager = new ScrollManager(null);

    /* loaded from: classes4.dex */
    public interface Listener {
        void onLogDeleted(LogLine logLine);
    }

    /* loaded from: classes4.dex */
    public interface ScrollObserver {
        void onScrollChanged(int i);
    }

    public SubmitDebugLogAdapter(Listener listener, PagingController pagingController) {
        this.listener = listener;
        this.pagingController = pagingController;
        setHasStableIds(true);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public long getItemId(int i) {
        if (getItem(i) != null) {
            return getItem(i).getId();
        }
        return -1;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        return getItem(i) == null ? 2 : 1;
    }

    protected LogLine getItem(int i) {
        this.pagingController.onDataNeededAroundIndex(i);
        return this.lines.get(i);
    }

    public void submitList(List<LogLine> list) {
        this.lines.clear();
        this.lines.addAll(list);
        notifyDataSetChanged();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.lines.size();
    }

    public LineViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new LineViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.submit_debug_log_line_item, viewGroup, false));
    }

    public void onBindViewHolder(LineViewHolder lineViewHolder, int i) {
        LogLine item = getItem(i);
        if (item == null) {
            item = SimpleLogLine.EMPTY;
        }
        lineViewHolder.bind(item, LINE_LENGTH, this.editing, this.scrollManager, this.listener);
    }

    public void onViewRecycled(LineViewHolder lineViewHolder) {
        lineViewHolder.unbind(this.scrollManager);
    }

    public void setEditing(boolean z) {
        this.editing = z;
        notifyDataSetChanged();
    }

    /* loaded from: classes4.dex */
    public static class ScrollManager {
        private int currentPosition;
        private final List<ScrollObserver> listeners;

        private ScrollManager() {
            this.listeners = new CopyOnWriteArrayList();
        }

        /* synthetic */ ScrollManager(AnonymousClass1 r1) {
            this();
        }

        void subscribe(ScrollObserver scrollObserver) {
            this.listeners.add(scrollObserver);
            scrollObserver.onScrollChanged(this.currentPosition);
        }

        void unsubscribe(ScrollObserver scrollObserver) {
            this.listeners.remove(scrollObserver);
        }

        void notify(int i) {
            this.currentPosition = i;
            for (ScrollObserver scrollObserver : this.listeners) {
                scrollObserver.onScrollChanged(i);
            }
        }
    }

    /* loaded from: classes4.dex */
    public static class LineViewHolder extends RecyclerView.ViewHolder implements ScrollObserver {
        private final ListenableHorizontalScrollView scrollView;
        private final TextView text;

        LineViewHolder(View view) {
            super(view);
            this.text = (TextView) view.findViewById(R.id.log_item_text);
            this.scrollView = (ListenableHorizontalScrollView) view.findViewById(R.id.log_item_scroll);
        }

        void bind(LogLine logLine, int i, boolean z, ScrollManager scrollManager, Listener listener) {
            Context context = this.itemView.getContext();
            if (logLine.getText().length() > i) {
                this.text.setText(logLine.getText().substring(0, i));
            } else if (logLine.getText().length() < i) {
                this.text.setText(padRight(logLine.getText(), i));
            } else {
                this.text.setText(logLine.getText());
            }
            switch (AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$logsubmit$LogLine$Style[logLine.getStyle().ordinal()]) {
                case 1:
                    this.text.setTextColor(ContextCompat.getColor(context, R.color.debuglog_color_none));
                    break;
                case 2:
                    this.text.setTextColor(ContextCompat.getColor(context, R.color.debuglog_color_verbose));
                    break;
                case 3:
                    this.text.setTextColor(ContextCompat.getColor(context, R.color.debuglog_color_debug));
                    break;
                case 4:
                    this.text.setTextColor(ContextCompat.getColor(context, R.color.debuglog_color_info));
                    break;
                case 5:
                    this.text.setTextColor(ContextCompat.getColor(context, R.color.debuglog_color_warn));
                    break;
                case 6:
                    this.text.setTextColor(ContextCompat.getColor(context, R.color.debuglog_color_error));
                    break;
            }
            this.scrollView.setOnScrollListener(new SubmitDebugLogAdapter$LineViewHolder$$ExternalSyntheticLambda0(scrollManager));
            scrollManager.subscribe(this);
            if (z) {
                this.text.setOnClickListener(new SubmitDebugLogAdapter$LineViewHolder$$ExternalSyntheticLambda1(listener, logLine));
            } else {
                this.text.setOnClickListener(null);
            }
        }

        public static /* synthetic */ void lambda$bind$0(ScrollManager scrollManager, int i, int i2) {
            if (i2 - i != 0) {
                scrollManager.notify(i);
            }
        }

        void unbind(ScrollManager scrollManager) {
            this.text.setOnClickListener(null);
            scrollManager.unsubscribe(this);
        }

        public void onScrollChanged(int i) {
            this.scrollView.scrollTo(i, 0);
        }

        private static String padRight(String str, int i) {
            return String.format("%-" + i + "s", str);
        }
    }

    /* renamed from: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogAdapter$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$logsubmit$LogLine$Style;

        static {
            int[] iArr = new int[LogLine.Style.values().length];
            $SwitchMap$org$thoughtcrime$securesms$logsubmit$LogLine$Style = iArr;
            try {
                iArr[LogLine.Style.NONE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$logsubmit$LogLine$Style[LogLine.Style.VERBOSE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$logsubmit$LogLine$Style[LogLine.Style.DEBUG.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$logsubmit$LogLine$Style[LogLine.Style.INFO.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$logsubmit$LogLine$Style[LogLine.Style.WARNING.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$logsubmit$LogLine$Style[LogLine.Style.ERROR.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
        }
    }
}
