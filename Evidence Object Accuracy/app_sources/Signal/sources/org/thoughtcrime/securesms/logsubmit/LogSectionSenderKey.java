package org.thoughtcrime.securesms.logsubmit;

import android.content.Context;
import android.database.Cursor;
import org.signal.core.util.AsciiArt;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.logsubmit.LogSection;

/* loaded from: classes4.dex */
public class LogSectionSenderKey implements LogSection {
    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public String getTitle() {
        return "SENDER KEY";
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public /* synthetic */ boolean hasContent() {
        return LogSection.CC.$default$hasContent(this);
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public CharSequence getContent(Context context) {
        Cursor allSharedWithCursor;
        StringBuilder sb = new StringBuilder();
        sb.append("--- Sender Keys Created By This Device");
        sb.append("\n\n");
        if (SignalStore.account().getAci() != null) {
            allSharedWithCursor = SignalDatabase.senderKeys().getAllCreatedBySelf();
            try {
                sb.append(AsciiArt.tableFor(allSharedWithCursor));
                sb.append("\n\n");
                if (allSharedWithCursor != null) {
                    allSharedWithCursor.close();
                }
            } finally {
            }
        } else {
            sb.append("<no ACI assigned yet>");
            sb.append("\n\n");
        }
        sb.append("--- Sender Key Shared State");
        sb.append("\n\n");
        allSharedWithCursor = SignalDatabase.senderKeyShared().getAllSharedWithCursor();
        try {
            sb.append(AsciiArt.tableFor(allSharedWithCursor));
            sb.append("\n");
            if (allSharedWithCursor != null) {
                allSharedWithCursor.close();
            }
            return sb;
        } finally {
        }
    }
}
