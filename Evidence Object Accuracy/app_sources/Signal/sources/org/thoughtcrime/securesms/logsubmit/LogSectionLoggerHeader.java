package org.thoughtcrime.securesms.logsubmit;

import android.content.Context;

/* loaded from: classes4.dex */
public class LogSectionLoggerHeader implements LogSection {
    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public CharSequence getContent(Context context) {
        return "";
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public String getTitle() {
        return "LOGGER";
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public boolean hasContent() {
        return false;
    }
}
