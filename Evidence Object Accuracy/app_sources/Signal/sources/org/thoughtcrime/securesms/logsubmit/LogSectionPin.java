package org.thoughtcrime.securesms.logsubmit;

import android.content.Context;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.logsubmit.LogSection;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class LogSectionPin implements LogSection {
    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public String getTitle() {
        return "PIN STATE";
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public /* synthetic */ boolean hasContent() {
        return LogSection.CC.$default$hasContent(this);
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public CharSequence getContent(Context context) {
        StringBuilder sb = new StringBuilder();
        sb.append("State: ");
        sb.append(SignalStore.pinValues().getPinState());
        sb.append("\n");
        sb.append("Last Successful Reminder Entry: ");
        sb.append(SignalStore.pinValues().getLastSuccessfulEntryTime());
        sb.append("\n");
        sb.append("Next Reminder Interval: ");
        sb.append(SignalStore.pinValues().getCurrentInterval());
        sb.append("\n");
        sb.append("ReglockV1: ");
        sb.append(TextSecurePreferences.isV1RegistrationLockEnabled(context));
        sb.append("\n");
        sb.append("ReglockV2: ");
        sb.append(SignalStore.kbsValues().isV2RegistrationLockEnabled());
        sb.append("\n");
        sb.append("Signal PIN: ");
        sb.append(SignalStore.kbsValues().hasPin());
        sb.append("\n");
        sb.append("Opted Out: ");
        sb.append(SignalStore.kbsValues().hasOptedOut());
        sb.append("\n");
        sb.append("Last Creation Failed: ");
        sb.append(SignalStore.kbsValues().lastPinCreateFailed());
        sb.append("\n");
        sb.append("Needs Account Restore: ");
        sb.append(SignalStore.storageService().needsAccountRestore());
        sb.append("\n");
        sb.append("PIN Required at Registration: ");
        sb.append(SignalStore.registrationValues().pinWasRequiredAtRegistration());
        sb.append("\n");
        sb.append("Registration Complete: ");
        sb.append(SignalStore.registrationValues().isRegistrationComplete());
        return sb;
    }
}
