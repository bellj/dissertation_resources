package org.thoughtcrime.securesms.logsubmit;

import android.content.Context;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.logsubmit.LogSection;

/* loaded from: classes4.dex */
public class LogSectionJobs implements LogSection {
    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public String getTitle() {
        return "JOBS";
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public /* synthetic */ boolean hasContent() {
        return LogSection.CC.$default$hasContent(this);
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public CharSequence getContent(Context context) {
        return ApplicationDependencies.getJobManager().getDebugInfo();
    }
}
