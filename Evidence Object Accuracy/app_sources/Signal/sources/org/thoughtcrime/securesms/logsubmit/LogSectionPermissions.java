package org.thoughtcrime.securesms.logsubmit;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.logsubmit.LogSection;

/* loaded from: classes4.dex */
public class LogSectionPermissions implements LogSection {
    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public String getTitle() {
        return "PERMISSIONS";
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public /* synthetic */ boolean hasContent() {
        return LogSection.CC.$default$hasContent(this);
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public CharSequence getContent(Context context) {
        StringBuilder sb = new StringBuilder();
        ArrayList<Pair> arrayList = new ArrayList();
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo("org.thoughtcrime.securesms", RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT);
            int i = 0;
            while (true) {
                String[] strArr = packageInfo.requestedPermissions;
                if (i >= strArr.length) {
                    break;
                }
                arrayList.add(new Pair(strArr[i], Boolean.valueOf((packageInfo.requestedPermissionsFlags[i] & 2) != 0)));
                i++;
            }
            Collections.sort(arrayList, new Comparator() { // from class: org.thoughtcrime.securesms.logsubmit.LogSectionPermissions$$ExternalSyntheticLambda0
                @Override // java.util.Comparator
                public final int compare(Object obj, Object obj2) {
                    return LogSectionPermissions.m2026$r8$lambda$YtvT4KXWulmnXTuJpZjRlyIAx8((Pair) obj, (Pair) obj2);
                }
            });
            for (Pair pair : arrayList) {
                sb.append((String) pair.first());
                sb.append(": ");
                sb.append(((Boolean) pair.second()).booleanValue() ? "YES" : "NO");
                sb.append("\n");
            }
            return sb;
        } catch (PackageManager.NameNotFoundException unused) {
            return "Unable to retrieve.";
        }
    }

    public static /* synthetic */ int lambda$getContent$0(Pair pair, Pair pair2) {
        return ((String) pair.first()).compareTo((String) pair2.first());
    }
}
