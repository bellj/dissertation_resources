package org.thoughtcrime.securesms.logsubmit;

import android.content.Context;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.logsubmit.LogSection;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public final class LogSectionBadges implements LogSection {
    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public String getTitle() {
        return "BADGES";
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public /* synthetic */ boolean hasContent() {
        return LogSection.CC.$default$hasContent(this);
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public CharSequence getContent(Context context) {
        if (!SignalStore.account().isRegistered()) {
            return "Unregistered";
        }
        if (SignalStore.account().getE164() == null || SignalStore.account().getAci() == null) {
            return "Self not yet available!";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Badge Count                  : ");
        sb.append(Recipient.self().getBadges().size());
        sb.append("\n");
        sb.append("ExpiredBadge                 : ");
        sb.append(SignalStore.donationsValues().getExpiredBadge() != null);
        sb.append("\n");
        sb.append("LastKeepAliveLaunchTime      : ");
        sb.append(SignalStore.donationsValues().getLastKeepAliveLaunchTime());
        sb.append("\n");
        sb.append("LastEndOfPeriod              : ");
        sb.append(SignalStore.donationsValues().getLastEndOfPeriod());
        sb.append("\n");
        sb.append("IsUserManuallyCancelled      : ");
        sb.append(SignalStore.donationsValues().isUserManuallyCancelled());
        sb.append("\n");
        sb.append("DisplayBadgesOnProfile       : ");
        sb.append(SignalStore.donationsValues().getDisplayBadgesOnProfile());
        sb.append("\n");
        sb.append("SubscriptionRedemptionFailed : ");
        sb.append(SignalStore.donationsValues().getSubscriptionRedemptionFailed());
        sb.append("\n");
        sb.append("ShouldCancelBeforeNextAttempt: ");
        sb.append(SignalStore.donationsValues().getShouldCancelSubscriptionBeforeNextSubscribeAttempt());
        sb.append("\n");
        return sb;
    }
}
