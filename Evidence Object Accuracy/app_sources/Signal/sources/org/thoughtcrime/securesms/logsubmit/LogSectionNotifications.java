package org.thoughtcrime.securesms.logsubmit;

import android.app.NotificationChannel;
import android.content.Context;
import android.os.Build;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.logsubmit.LogSection;
import org.thoughtcrime.securesms.util.ServiceUtil;

/* loaded from: classes4.dex */
public final class LogSectionNotifications implements LogSection {
    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public String getTitle() {
        return "NOTIFICATIONS";
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public /* synthetic */ boolean hasContent() {
        return LogSection.CC.$default$hasContent(this);
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public CharSequence getContent(Context context) {
        StringBuilder sb = new StringBuilder();
        sb.append("Message notifications: ");
        sb.append(SignalStore.settings().isMessageNotificationsEnabled());
        sb.append("\n");
        sb.append("Call notifications   : ");
        sb.append(SignalStore.settings().isCallNotificationsEnabled());
        sb.append("\n");
        sb.append("New contact alerts   : ");
        sb.append(SignalStore.settings().isNotifyWhenContactJoinsSignal());
        sb.append("\n");
        sb.append("In-chat sounds       : ");
        sb.append(SignalStore.settings().isMessageNotificationsInChatSoundsEnabled());
        sb.append("\n");
        sb.append("Repeat alerts        : ");
        sb.append(SignalStore.settings().getMessageNotificationsRepeatAlerts());
        sb.append("\n");
        sb.append("Notification display : ");
        sb.append(SignalStore.settings().getMessageNotificationsPrivacy());
        sb.append("\n\n");
        if (Build.VERSION.SDK_INT >= 26) {
            for (NotificationChannel notificationChannel : ServiceUtil.getNotificationManager(context).getNotificationChannels()) {
                sb.append(buildChannelString(notificationChannel));
            }
        }
        return sb;
    }

    private static String buildChannelString(NotificationChannel notificationChannel) {
        StringBuilder sb = new StringBuilder();
        sb.append("-- ");
        sb.append(notificationChannel.getId());
        sb.append("\nimportance          : ");
        sb.append(importanceString(notificationChannel.getImportance()));
        sb.append("\nhasUserSetImportance: ");
        int i = Build.VERSION.SDK_INT;
        Object obj = "N/A (Requires API 29)";
        sb.append(i >= 29 ? Boolean.valueOf(notificationChannel.hasUserSetImportance()) : obj);
        sb.append("\nhasUserSetSound     : ");
        sb.append(i >= 30 ? Boolean.valueOf(notificationChannel.hasUserSetSound()) : "N/A (Requires API 30)");
        sb.append("\nshouldVibrate       : ");
        sb.append(notificationChannel.shouldVibrate());
        sb.append("\nshouldShowLights    : ");
        sb.append(notificationChannel.shouldShowLights());
        sb.append("\ncanBypassDnd        : ");
        sb.append(notificationChannel.canBypassDnd());
        sb.append("\ncanShowBadge        : ");
        sb.append(notificationChannel.canShowBadge());
        sb.append("\ncanBubble           : ");
        if (i >= 29) {
            obj = Boolean.valueOf(notificationChannel.canBubble());
        }
        sb.append(obj);
        sb.append("\n\n");
        return sb.toString();
    }

    private static String importanceString(int i) {
        if (i == 0) {
            return "NONE (0)";
        }
        if (i == 1) {
            return "MIN (1)";
        }
        if (i == 2) {
            return "LOW (2)";
        }
        if (i == 3) {
            return "DEFAULT (3)";
        }
        if (i == 4) {
            return "HIGH (4)";
        }
        if (i == 5) {
            return "MAX (5)";
        }
        return "UNSPECIFIED (" + i + ")";
    }
}
