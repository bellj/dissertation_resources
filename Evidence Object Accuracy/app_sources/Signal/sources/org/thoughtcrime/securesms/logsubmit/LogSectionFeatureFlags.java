package org.thoughtcrime.securesms.logsubmit;

import android.content.Context;
import com.annimon.stream.Stream;
import java.util.Map;
import org.thoughtcrime.securesms.logsubmit.LogSection;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class LogSectionFeatureFlags implements LogSection {
    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public String getTitle() {
        return "FEATURE FLAGS";
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public /* synthetic */ boolean hasContent() {
        return LogSection.CC.$default$hasContent(this);
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public CharSequence getContent(Context context) {
        StringBuilder sb = new StringBuilder();
        Map<String, Object> memoryValues = FeatureFlags.getMemoryValues();
        Map<String, Object> diskValues = FeatureFlags.getDiskValues();
        Map<String, Object> pendingDiskValues = FeatureFlags.getPendingDiskValues();
        Map<String, Object> forcedValues = FeatureFlags.getForcedValues();
        int intValue = ((Integer) Stream.of(memoryValues.keySet()).map(new LogSectionConstraints$$ExternalSyntheticLambda0()).max(new LogSectionConstraints$$ExternalSyntheticLambda1()).orElse(0)).intValue();
        int intValue2 = ((Integer) Stream.of(diskValues.keySet()).map(new LogSectionConstraints$$ExternalSyntheticLambda0()).max(new LogSectionConstraints$$ExternalSyntheticLambda1()).orElse(0)).intValue();
        int intValue3 = ((Integer) Stream.of(pendingDiskValues.keySet()).map(new LogSectionConstraints$$ExternalSyntheticLambda0()).max(new LogSectionConstraints$$ExternalSyntheticLambda1()).orElse(0)).intValue();
        int intValue4 = ((Integer) Stream.of(forcedValues.keySet()).map(new LogSectionConstraints$$ExternalSyntheticLambda0()).max(new LogSectionConstraints$$ExternalSyntheticLambda1()).orElse(0)).intValue();
        sb.append("-- Memory\n");
        for (Map.Entry<String, Object> entry : memoryValues.entrySet()) {
            sb.append(Util.rightPad(entry.getKey(), intValue));
            sb.append(": ");
            sb.append(entry.getValue());
            sb.append("\n");
        }
        sb.append("\n");
        sb.append("-- Current Disk\n");
        for (Map.Entry<String, Object> entry2 : diskValues.entrySet()) {
            sb.append(Util.rightPad(entry2.getKey(), intValue2));
            sb.append(": ");
            sb.append(entry2.getValue());
            sb.append("\n");
        }
        sb.append("\n");
        sb.append("-- Pending Disk\n");
        for (Map.Entry<String, Object> entry3 : pendingDiskValues.entrySet()) {
            sb.append(Util.rightPad(entry3.getKey(), intValue3));
            sb.append(": ");
            sb.append(entry3.getValue());
            sb.append("\n");
        }
        sb.append("\n");
        sb.append("-- Forced\n");
        if (forcedValues.isEmpty()) {
            sb.append("None\n");
        } else {
            for (Map.Entry<String, Object> entry4 : forcedValues.entrySet()) {
                sb.append(Util.rightPad(entry4.getKey(), intValue4));
                sb.append(": ");
                sb.append(entry4.getValue());
                sb.append("\n");
            }
        }
        return sb;
    }
}
