package org.thoughtcrime.securesms.logsubmit;

import org.thoughtcrime.securesms.logsubmit.LogLine;

/* loaded from: classes4.dex */
public class CompleteLogLine implements LogLine {
    private final long id;
    private final LogLine line;

    public CompleteLogLine(long j, LogLine logLine) {
        this.id = j;
        this.line = logLine;
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogLine
    public long getId() {
        return this.id;
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogLine
    public String getText() {
        return this.line.getText();
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogLine
    public LogLine.Style getStyle() {
        return this.line.getStyle();
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogLine
    public LogLine.Placeholder getPlaceholderType() {
        return this.line.getPlaceholderType();
    }
}
