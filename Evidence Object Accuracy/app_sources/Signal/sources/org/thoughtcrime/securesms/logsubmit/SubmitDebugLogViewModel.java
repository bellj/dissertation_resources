package org.thoughtcrime.securesms.logsubmit;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import j$.util.Optional;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.signal.core.util.tracing.Tracer;
import org.signal.paging.LivePagedData;
import org.signal.paging.PagedData;
import org.signal.paging.PagingConfig;
import org.signal.paging.PagingController;
import org.signal.paging.ProxyPagingController;
import org.thoughtcrime.securesms.database.LogDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.logsubmit.SubmitDebugLogRepository;

/* loaded from: classes4.dex */
public class SubmitDebugLogViewModel extends ViewModel {
    private final long firstViewTime;
    private final MediatorLiveData<List<LogLine>> lines;
    private final MutableLiveData<Mode> mode;
    private final ProxyPagingController<Long> pagingController;
    private final SubmitDebugLogRepository repo;
    private final List<LogLine> staticLines;
    private final byte[] trace;

    /* loaded from: classes4.dex */
    public enum Mode {
        NORMAL,
        EDIT,
        SUBMITTING
    }

    private SubmitDebugLogViewModel() {
        SubmitDebugLogRepository submitDebugLogRepository = new SubmitDebugLogRepository();
        this.repo = submitDebugLogRepository;
        this.mode = new MutableLiveData<>();
        this.trace = Tracer.getInstance().serialize();
        this.pagingController = new ProxyPagingController<>();
        this.firstViewTime = System.currentTimeMillis();
        this.staticLines = new ArrayList();
        this.lines = new MediatorLiveData<>();
        submitDebugLogRepository.getPrefixLogLines(new SubmitDebugLogRepository.Callback() { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogViewModel$$ExternalSyntheticLambda3
            @Override // org.thoughtcrime.securesms.logsubmit.SubmitDebugLogRepository.Callback
            public final void onResult(Object obj) {
                SubmitDebugLogViewModel.this.lambda$new$1((List) obj);
            }
        });
    }

    public /* synthetic */ void lambda$new$1(List list) {
        this.staticLines.addAll(list);
        Log.blockUntilAllWritesFinished();
        LogDatabase.getInstance(ApplicationDependencies.getApplication()).trimToSize();
        ThreadUtil.runOnMain(new Runnable(PagedData.createForLiveData(new LogDataSource(ApplicationDependencies.getApplication(), list, this.firstViewTime), new PagingConfig.Builder().setPageSize(100).setBufferPages(3).setStartIndex(0).build())) { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ LivePagedData f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SubmitDebugLogViewModel.this.lambda$new$0(this.f$1);
            }
        });
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.signal.paging.ProxyPagingController<java.lang.Long> */
    /* JADX WARN: Multi-variable type inference failed */
    public /* synthetic */ void lambda$new$0(LivePagedData livePagedData) {
        this.pagingController.set(livePagedData.getController());
        MediatorLiveData<List<LogLine>> mediatorLiveData = this.lines;
        LiveData<S> data = livePagedData.getData();
        MediatorLiveData<List<LogLine>> mediatorLiveData2 = this.lines;
        Objects.requireNonNull(mediatorLiveData2);
        mediatorLiveData.addSource(data, new Observer() { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogViewModel$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                MediatorLiveData.this.setValue((List) obj);
            }
        });
        this.mode.setValue(Mode.NORMAL);
    }

    public LiveData<List<LogLine>> getLines() {
        return this.lines;
    }

    public PagingController getPagingController() {
        return this.pagingController;
    }

    public LiveData<Mode> getMode() {
        return this.mode;
    }

    public LiveData<Optional<String>> onSubmitClicked() {
        this.mode.postValue(Mode.SUBMITTING);
        MutableLiveData mutableLiveData = new MutableLiveData();
        this.repo.submitLogWithPrefixLines(this.firstViewTime, this.staticLines, this.trace, new SubmitDebugLogRepository.Callback(mutableLiveData) { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogViewModel$$ExternalSyntheticLambda1
            public final /* synthetic */ MutableLiveData f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.logsubmit.SubmitDebugLogRepository.Callback
            public final void onResult(Object obj) {
                SubmitDebugLogViewModel.this.lambda$onSubmitClicked$2(this.f$1, (Optional) obj);
            }
        });
        return mutableLiveData;
    }

    public /* synthetic */ void lambda$onSubmitClicked$2(MutableLiveData mutableLiveData, Optional optional) {
        this.mode.postValue(Mode.NORMAL);
        mutableLiveData.postValue(optional);
    }

    public void onQueryUpdated(String str) {
        throw new UnsupportedOperationException("Not yet implemented.");
    }

    public void onSearchClosed() {
        throw new UnsupportedOperationException("Not yet implemented.");
    }

    public void onEditButtonPressed() {
        throw new UnsupportedOperationException("Not yet implemented.");
    }

    public void onDoneEditingButtonPressed() {
        throw new UnsupportedOperationException("Not yet implemented.");
    }

    public void onLogDeleted(LogLine logLine) {
        throw new UnsupportedOperationException("Not yet implemented.");
    }

    public boolean onBackPressed() {
        if (this.mode.getValue() != Mode.EDIT) {
            return false;
        }
        this.mode.setValue(Mode.NORMAL);
        return true;
    }

    /* loaded from: classes4.dex */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        @Override // androidx.lifecycle.ViewModelProvider.NewInstanceFactory, androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new SubmitDebugLogViewModel());
        }
    }
}
