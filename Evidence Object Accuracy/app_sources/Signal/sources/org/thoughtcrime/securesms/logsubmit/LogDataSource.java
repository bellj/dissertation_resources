package org.thoughtcrime.securesms.logsubmit;

import android.app.Application;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Scrubber;
import org.signal.paging.PagedDataSource;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.database.LogDatabase;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.logsubmit.LogLine;

/* compiled from: LogDataSource.kt */
@Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B#\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007\u0012\u0006\u0010\b\u001a\u00020\u0002¢\u0006\u0002\u0010\tJ\u0010\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0015\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0012\u001a\u00020\u0003H\u0016¢\u0006\u0002\u0010\u0013J&\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00030\u00072\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\u0019\u0010\u0014\u001a\u0004\u0018\u00010\u00032\b\u0010\u001a\u001a\u0004\u0018\u00010\u0002H\u0016¢\u0006\u0002\u0010\u001bJ\b\u0010\u001c\u001a\u00020\u0016H\u0016R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00030\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0002X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/logsubmit/LogDataSource;", "Lorg/signal/paging/PagedDataSource;", "", "Lorg/thoughtcrime/securesms/logsubmit/LogLine;", "application", "Landroid/app/Application;", "prefixLines", "", "untilTime", "(Landroid/app/Application;Ljava/util/List;J)V", "logDatabase", "Lorg/thoughtcrime/securesms/database/LogDatabase;", "getLogDatabase", "()Lorg/thoughtcrime/securesms/database/LogDatabase;", "convertToLogLine", "raw", "", "getKey", "data", "(Lorg/thoughtcrime/securesms/logsubmit/LogLine;)Ljava/lang/Long;", "load", NotificationProfileDatabase.NotificationProfileScheduleTable.START, "", "length", "cancellationSignal", "Lorg/signal/paging/PagedDataSource$CancellationSignal;", "key", "(Ljava/lang/Long;)Lorg/thoughtcrime/securesms/logsubmit/LogLine;", MediaPreviewActivity.SIZE_EXTRA, "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class LogDataSource implements PagedDataSource<Long, LogLine> {
    private final LogDatabase logDatabase;
    private final List<LogLine> prefixLines;
    private final long untilTime;

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.logsubmit.LogLine> */
    /* JADX WARN: Multi-variable type inference failed */
    public LogDataSource(Application application, List<? extends LogLine> list, long j) {
        Intrinsics.checkNotNullParameter(application, "application");
        Intrinsics.checkNotNullParameter(list, "prefixLines");
        this.prefixLines = list;
        this.untilTime = j;
        this.logDatabase = LogDatabase.Companion.getInstance(application);
    }

    public final LogDatabase getLogDatabase() {
        return this.logDatabase;
    }

    @Override // org.signal.paging.PagedDataSource
    public int size() {
        return this.prefixLines.size() + this.logDatabase.getLogCountBeforeTime(this.untilTime);
    }

    @Override // org.signal.paging.PagedDataSource
    public List<LogLine> load(int i, int i2, PagedDataSource.CancellationSignal cancellationSignal) {
        Intrinsics.checkNotNullParameter(cancellationSignal, "cancellationSignal");
        int i3 = i + i2;
        if (i3 < this.prefixLines.size()) {
            return this.prefixLines.subList(i, i3);
        }
        if (i < this.prefixLines.size()) {
            List<LogLine> list = this.prefixLines;
            List<LogLine> subList = list.subList(i, list.size());
            List<String> rangeBeforeTime = this.logDatabase.getRangeBeforeTime(0, i2 - (this.prefixLines.size() - i), this.untilTime);
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(rangeBeforeTime, 10));
            for (String str : rangeBeforeTime) {
                arrayList.add(convertToLogLine(str));
            }
            return CollectionsKt___CollectionsKt.plus((Collection) subList, (Iterable) arrayList);
        }
        List<String> rangeBeforeTime2 = this.logDatabase.getRangeBeforeTime(i - this.prefixLines.size(), i2, this.untilTime);
        ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(rangeBeforeTime2, 10));
        for (String str2 : rangeBeforeTime2) {
            arrayList2.add(convertToLogLine(str2));
        }
        return arrayList2;
    }

    public LogLine load(Long l) {
        throw new UnsupportedOperationException("Not implemented!");
    }

    public Long getKey(LogLine logLine) {
        Intrinsics.checkNotNullParameter(logLine, "data");
        return Long.valueOf(logLine.getId());
    }

    private final LogLine convertToLogLine(String str) {
        String obj = Scrubber.scrub(str).toString();
        return new SimpleLogLine(obj, LogStyleParser.parseStyle(obj), LogLine.Placeholder.NONE);
    }
}
