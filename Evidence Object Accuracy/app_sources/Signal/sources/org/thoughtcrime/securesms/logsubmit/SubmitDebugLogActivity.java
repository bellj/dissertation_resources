package org.thoughtcrime.securesms.logsubmit;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.URLSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.core.app.ShareCompat$IntentBuilder;
import androidx.core.text.util.LinkifyCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import j$.util.Optional;
import java.util.List;
import org.thoughtcrime.securesms.BaseActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.logsubmit.SubmitDebugLogAdapter;
import org.thoughtcrime.securesms.logsubmit.SubmitDebugLogViewModel;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.LongClickCopySpan;
import org.thoughtcrime.securesms.util.LongClickMovementMethod;
import org.thoughtcrime.securesms.util.ThemeUtil;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;

/* loaded from: classes4.dex */
public class SubmitDebugLogActivity extends BaseActivity implements SubmitDebugLogAdapter.Listener {
    private SubmitDebugLogAdapter adapter;
    private MenuItem doneMenuItem;
    private final DynamicTheme dynamicTheme = new DynamicTheme();
    private View editBanner;
    private MenuItem editMenuItem;
    private RecyclerView lineList;
    private AlertDialog loadingDialog;
    private View scrollToBottomButton;
    private View scrollToTopButton;
    private MenuItem searchMenuItem;
    private CircularProgressMaterialButton submitButton;
    private SubmitDebugLogViewModel viewModel;
    private View warningBanner;

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.dynamicTheme.onCreate(this);
        setContentView(R.layout.submit_debug_log_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.HelpSettingsFragment__debug_log);
        this.viewModel = (SubmitDebugLogViewModel) ViewModelProviders.of(this, new SubmitDebugLogViewModel.Factory()).get(SubmitDebugLogViewModel.class);
        initView();
        initViewModel();
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }

    @Override // android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.submit_debug_log_normal, menu);
        this.editMenuItem = menu.findItem(R.id.menu_edit_log);
        this.doneMenuItem = menu.findItem(R.id.menu_done_editing_log);
        MenuItem findItem = menu.findItem(R.id.menu_search);
        this.searchMenuItem = findItem;
        final SearchView searchView = (SearchView) findItem.getActionView();
        final AnonymousClass1 r0 = new SearchView.OnQueryTextListener() { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogActivity.1
            @Override // androidx.appcompat.widget.SearchView.OnQueryTextListener
            public boolean onQueryTextSubmit(String str) {
                SubmitDebugLogActivity.this.viewModel.onQueryUpdated(str);
                return true;
            }

            @Override // androidx.appcompat.widget.SearchView.OnQueryTextListener
            public boolean onQueryTextChange(String str) {
                SubmitDebugLogActivity.this.viewModel.onQueryUpdated(str);
                return true;
            }
        };
        this.searchMenuItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogActivity.2
            @Override // android.view.MenuItem.OnActionExpandListener
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                searchView.setOnQueryTextListener(r0);
                return true;
            }

            @Override // android.view.MenuItem.OnActionExpandListener
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                searchView.setOnQueryTextListener(null);
                SubmitDebugLogActivity.this.viewModel.onSearchClosed();
                return true;
            }
        });
        return true;
    }

    @Override // android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        if (menuItem.getItemId() == 16908332) {
            finish();
            return true;
        } else if (menuItem.getItemId() == R.id.menu_edit_log) {
            this.viewModel.onEditButtonPressed();
            return false;
        } else if (menuItem.getItemId() != R.id.menu_done_editing_log) {
            return false;
        } else {
            this.viewModel.onDoneEditingButtonPressed();
            return false;
        }
    }

    @Override // androidx.activity.ComponentActivity, android.app.Activity
    public void onBackPressed() {
        if (!this.viewModel.onBackPressed()) {
            super.onBackPressed();
        }
    }

    @Override // org.thoughtcrime.securesms.logsubmit.SubmitDebugLogAdapter.Listener
    public void onLogDeleted(LogLine logLine) {
        this.viewModel.onLogDeleted(logLine);
    }

    private void initView() {
        this.lineList = (RecyclerView) findViewById(R.id.debug_log_lines);
        this.warningBanner = findViewById(R.id.debug_log_warning_banner);
        this.editBanner = findViewById(R.id.debug_log_edit_banner);
        this.submitButton = (CircularProgressMaterialButton) findViewById(R.id.debug_log_submit_button);
        this.scrollToBottomButton = findViewById(R.id.debug_log_scroll_to_bottom);
        this.scrollToTopButton = findViewById(R.id.debug_log_scroll_to_top);
        this.adapter = new SubmitDebugLogAdapter(this, this.viewModel.getPagingController());
        this.lineList.setLayoutManager(new LinearLayoutManager(this));
        this.lineList.setAdapter(this.adapter);
        this.lineList.setItemAnimator(null);
        this.submitButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogActivity$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SubmitDebugLogActivity.this.lambda$initView$0(view);
            }
        });
        this.scrollToBottomButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogActivity$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SubmitDebugLogActivity.this.lambda$initView$1(view);
            }
        });
        this.scrollToTopButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogActivity$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SubmitDebugLogActivity.this.lambda$initView$2(view);
            }
        });
        this.lineList.addOnScrollListener(new RecyclerView.OnScrollListener() { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogActivity.3
            @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
            public void onScrolled(RecyclerView recyclerView, int i, int i2) {
                if (((LinearLayoutManager) recyclerView.getLayoutManager()).findLastVisibleItemPosition() < SubmitDebugLogActivity.this.adapter.getItemCount() - 10) {
                    SubmitDebugLogActivity.this.scrollToBottomButton.setVisibility(0);
                } else {
                    SubmitDebugLogActivity.this.scrollToBottomButton.setVisibility(8);
                }
                if (((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition() > 10) {
                    SubmitDebugLogActivity.this.scrollToTopButton.setVisibility(0);
                } else {
                    SubmitDebugLogActivity.this.scrollToTopButton.setVisibility(8);
                }
            }
        });
        this.loadingDialog = SimpleProgressDialog.show(this);
    }

    public /* synthetic */ void lambda$initView$0(View view) {
        onSubmitClicked();
    }

    public /* synthetic */ void lambda$initView$1(View view) {
        this.lineList.scrollToPosition(this.adapter.getItemCount() - 1);
    }

    public /* synthetic */ void lambda$initView$2(View view) {
        this.lineList.scrollToPosition(0);
    }

    private void initViewModel() {
        this.viewModel.getLines().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogActivity$$ExternalSyntheticLambda3
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                SubmitDebugLogActivity.this.presentLines((List) obj);
            }
        });
        this.viewModel.getMode().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogActivity$$ExternalSyntheticLambda4
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                SubmitDebugLogActivity.this.presentMode((SubmitDebugLogViewModel.Mode) obj);
            }
        });
    }

    public void presentLines(List<LogLine> list) {
        if (this.loadingDialog != null && list.size() > 0) {
            this.loadingDialog.dismiss();
            this.loadingDialog = null;
            this.warningBanner.setVisibility(0);
            this.submitButton.setVisibility(0);
        }
        this.adapter.submitList(list);
    }

    /* renamed from: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogActivity$4 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass4 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$logsubmit$SubmitDebugLogViewModel$Mode;

        static {
            int[] iArr = new int[SubmitDebugLogViewModel.Mode.values().length];
            $SwitchMap$org$thoughtcrime$securesms$logsubmit$SubmitDebugLogViewModel$Mode = iArr;
            try {
                iArr[SubmitDebugLogViewModel.Mode.NORMAL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$logsubmit$SubmitDebugLogViewModel$Mode[SubmitDebugLogViewModel.Mode.SUBMITTING.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$logsubmit$SubmitDebugLogViewModel$Mode[SubmitDebugLogViewModel.Mode.EDIT.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    public void presentMode(SubmitDebugLogViewModel.Mode mode) {
        int i = AnonymousClass4.$SwitchMap$org$thoughtcrime$securesms$logsubmit$SubmitDebugLogViewModel$Mode[mode.ordinal()];
        if (i == 1) {
            this.editBanner.setVisibility(8);
            this.adapter.setEditing(false);
        } else if (i == 2) {
            this.editBanner.setVisibility(8);
            this.adapter.setEditing(false);
            this.editMenuItem.setVisible(false);
            this.doneMenuItem.setVisible(false);
            this.searchMenuItem.setVisible(false);
        } else if (i == 3) {
            this.editBanner.setVisibility(0);
            this.adapter.setEditing(true);
            this.editMenuItem.setVisible(false);
            this.doneMenuItem.setVisible(true);
            this.searchMenuItem.setVisible(true);
        }
    }

    private void presentResultDialog(String str) {
        AlertDialog.Builder positiveButton = new AlertDialog.Builder(this).setTitle(R.string.SubmitDebugLogActivity_success).setCancelable(false).setNeutralButton(17039370, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogActivity$$ExternalSyntheticLambda6
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                SubmitDebugLogActivity.this.lambda$presentResultDialog$3(dialogInterface, i);
            }
        }).setPositiveButton(R.string.SubmitDebugLogActivity_share, new DialogInterface.OnClickListener(str) { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogActivity$$ExternalSyntheticLambda7
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                SubmitDebugLogActivity.this.lambda$presentResultDialog$4(this.f$1, dialogInterface, i);
            }
        });
        SpannableString spannableString = new SpannableString(getResources().getString(R.string.SubmitDebugLogActivity_copy_this_url_and_add_it_to_your_issue, str));
        TextView textView = new TextView(positiveButton.getContext());
        LongClickCopySpan longClickCopySpan = new LongClickCopySpan(str);
        LinkifyCompat.addLinks(spannableString, 1);
        URLSpan[] uRLSpanArr = (URLSpan[]) spannableString.getSpans(0, spannableString.length(), URLSpan.class);
        for (URLSpan uRLSpan : uRLSpanArr) {
            spannableString.setSpan(longClickCopySpan, spannableString.getSpanStart(uRLSpan), spannableString.getSpanEnd(uRLSpan), 33);
        }
        textView.setText(spannableString);
        textView.setMovementMethod(LongClickMovementMethod.getInstance(this));
        ViewUtil.setPadding(textView, (int) ThemeUtil.getThemedDimen(this, R.attr.dialogPreferredPadding));
        positiveButton.setView(textView);
        positiveButton.show();
    }

    public /* synthetic */ void lambda$presentResultDialog$3(DialogInterface dialogInterface, int i) {
        finish();
    }

    public /* synthetic */ void lambda$presentResultDialog$4(String str, DialogInterface dialogInterface, int i) {
        ShareCompat$IntentBuilder.from(this).setText(str).setType("text/plain").setEmailTo(new String[]{"support@signal.org"}).startChooser();
    }

    private void onSubmitClicked() {
        this.submitButton.setSpinning();
        this.viewModel.onSubmitClicked().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.logsubmit.SubmitDebugLogActivity$$ExternalSyntheticLambda5
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                SubmitDebugLogActivity.this.lambda$onSubmitClicked$5((Optional) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onSubmitClicked$5(Optional optional) {
        if (optional.isPresent()) {
            presentResultDialog((String) optional.get());
        } else {
            Toast.makeText(this, (int) R.string.SubmitDebugLogActivity_failed_to_submit_logs, 1).show();
        }
        this.submitButton.cancelSpinning();
    }
}
