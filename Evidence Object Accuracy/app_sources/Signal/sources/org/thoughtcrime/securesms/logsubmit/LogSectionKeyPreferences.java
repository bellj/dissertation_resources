package org.thoughtcrime.securesms.logsubmit;

import android.content.Context;
import org.thoughtcrime.securesms.keyvalue.KeepMessagesDuration;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.logsubmit.LogSection;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public final class LogSectionKeyPreferences implements LogSection {
    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public String getTitle() {
        return "KEY PREFERENCES";
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public /* synthetic */ boolean hasContent() {
        return LogSection.CC.$default$hasContent(this);
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public CharSequence getContent(Context context) {
        StringBuilder sb = new StringBuilder();
        sb.append("Screen Lock          : ");
        sb.append(TextSecurePreferences.isScreenLockEnabled(context));
        sb.append("\n");
        sb.append("Screen Lock Timeout  : ");
        sb.append(TextSecurePreferences.getScreenLockTimeout(context));
        sb.append("\n");
        sb.append("Password Disabled    : ");
        sb.append(TextSecurePreferences.isPasswordDisabled(context));
        sb.append("\n");
        sb.append("Prefer Contact Photos: ");
        sb.append(SignalStore.settings().isPreferSystemContactPhotos());
        sb.append("\n");
        sb.append("Call Bandwidth Mode  : ");
        sb.append(SignalStore.settings().getCallBandwidthMode());
        sb.append("\n");
        sb.append("Client Deprecated    : ");
        sb.append(SignalStore.misc().isClientDeprecated());
        sb.append("\n");
        sb.append("Push Registered      : ");
        sb.append(SignalStore.account().isRegistered());
        sb.append("\n");
        sb.append("Unauthorized Received: ");
        sb.append(TextSecurePreferences.isUnauthorizedRecieved(context));
        sb.append("\n");
        sb.append("self.isRegistered()  : ");
        sb.append(SignalStore.account().getAci() == null ? "false" : Boolean.valueOf(Recipient.self().isRegistered()));
        sb.append("\n");
        sb.append("Thread Trimming      : ");
        sb.append(getThreadTrimmingString());
        sb.append("\n");
        sb.append("Censorship Setting   : ");
        sb.append(SignalStore.settings().getCensorshipCircumventionEnabled());
        sb.append("\n");
        sb.append("Network Reachable    : ");
        sb.append(SignalStore.misc().isServiceReachableWithoutCircumvention());
        sb.append(", last checked: ");
        sb.append(SignalStore.misc().getLastCensorshipServiceReachabilityCheckTime());
        sb.append("\n");
        return sb;
    }

    private static String getThreadTrimmingString() {
        if (SignalStore.settings().isTrimByLengthEnabled()) {
            return "Enabled - Max length of " + SignalStore.settings().getThreadTrimLength();
        } else if (SignalStore.settings().getKeepMessagesDuration() == KeepMessagesDuration.FOREVER) {
            return "Disabled";
        } else {
            return "Enabled - Max age of " + SignalStore.settings().getKeepMessagesDuration();
        }
    }
}
