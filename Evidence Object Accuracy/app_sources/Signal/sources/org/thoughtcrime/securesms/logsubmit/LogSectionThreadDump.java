package org.thoughtcrime.securesms.logsubmit;

import android.content.Context;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.logsubmit.LogSection;

/* loaded from: classes4.dex */
public class LogSectionThreadDump implements LogSection {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS zzz", Locale.US);

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public String getTitle() {
        return "LAST THREAD DUMP";
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public /* synthetic */ boolean hasContent() {
        return LogSection.CC.$default$hasContent(this);
    }

    @Override // org.thoughtcrime.securesms.logsubmit.LogSection
    public CharSequence getContent(Context context) {
        Map<Thread, StackTraceElement[]> lastThreadDump = ApplicationDependencies.getDeadlockDetector().getLastThreadDump();
        long lastThreadDumpTime = ApplicationDependencies.getDeadlockDetector().getLastThreadDumpTime();
        if (lastThreadDump == null) {
            return "None";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Time: ");
        sb.append(DATE_FORMAT.format(new Date(lastThreadDumpTime)));
        sb.append(" (");
        sb.append(lastThreadDumpTime);
        sb.append(")\n\n");
        for (Map.Entry<Thread, StackTraceElement[]> entry : lastThreadDump.entrySet()) {
            Thread key = entry.getKey();
            sb.append("-- [");
            sb.append(key.getId());
            sb.append("] ");
            sb.append(key.getName());
            sb.append(" (");
            sb.append(key.getState());
            sb.append(")\n");
            for (StackTraceElement stackTraceElement : entry.getValue()) {
                sb.append(stackTraceElement.toString());
                sb.append("\n");
            }
            sb.append("\n");
        }
        return sb;
    }
}
