package org.thoughtcrime.securesms;

import androidx.lifecycle.Observer;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class MediaPreviewActivity$ViewPagerListener$$ExternalSyntheticLambda0 implements Observer {
    public final /* synthetic */ MediaPreviewActivity.ViewPagerListener f$0;

    public /* synthetic */ MediaPreviewActivity$ViewPagerListener$$ExternalSyntheticLambda0(MediaPreviewActivity.ViewPagerListener viewPagerListener) {
        this.f$0 = viewPagerListener;
    }

    @Override // androidx.lifecycle.Observer
    public final void onChanged(Object obj) {
        this.f$0.lambda$onPageSelected$0((Recipient) obj);
    }
}
