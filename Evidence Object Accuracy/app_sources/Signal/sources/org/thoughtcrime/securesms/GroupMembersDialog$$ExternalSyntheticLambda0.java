package org.thoughtcrime.securesms;

import androidx.lifecycle.Observer;
import java.util.List;
import org.thoughtcrime.securesms.groups.ui.GroupMemberListView;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class GroupMembersDialog$$ExternalSyntheticLambda0 implements Observer {
    public final /* synthetic */ GroupMemberListView f$0;

    public /* synthetic */ GroupMembersDialog$$ExternalSyntheticLambda0(GroupMemberListView groupMemberListView) {
        this.f$0 = groupMemberListView;
    }

    @Override // androidx.lifecycle.Observer
    public final void onChanged(Object obj) {
        this.f$0.setMembers((List) obj);
    }
}
