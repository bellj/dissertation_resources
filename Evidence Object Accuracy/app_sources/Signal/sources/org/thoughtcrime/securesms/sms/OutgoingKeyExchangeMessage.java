package org.thoughtcrime.securesms.sms;

import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class OutgoingKeyExchangeMessage extends OutgoingTextMessage {
    @Override // org.thoughtcrime.securesms.sms.OutgoingTextMessage
    public boolean isKeyExchange() {
        return true;
    }

    public OutgoingKeyExchangeMessage(Recipient recipient, String str) {
        super(recipient, str, -1);
    }

    private OutgoingKeyExchangeMessage(OutgoingKeyExchangeMessage outgoingKeyExchangeMessage, String str) {
        super(outgoingKeyExchangeMessage, str);
    }

    @Override // org.thoughtcrime.securesms.sms.OutgoingTextMessage
    public OutgoingTextMessage withBody(String str) {
        return new OutgoingKeyExchangeMessage(this, str);
    }
}
