package org.thoughtcrime.securesms.sms;

import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.SmsMessage;
import j$.util.Optional;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.notifications.NotificationIds;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class IncomingTextMessage implements Parcelable {
    public static final Parcelable.Creator<IncomingTextMessage> CREATOR = new Parcelable.Creator<IncomingTextMessage>() { // from class: org.thoughtcrime.securesms.sms.IncomingTextMessage.1
        @Override // android.os.Parcelable.Creator
        public IncomingTextMessage createFromParcel(Parcel parcel) {
            return new IncomingTextMessage(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public IncomingTextMessage[] newArray(int i) {
            return new IncomingTextMessage[i];
        }
    };
    private static final String TAG = Log.tag(IncomingTextMessage.class);
    private final long expiresInMillis;
    private final GroupId groupId;
    private final String message;
    private final int protocol;
    private final String pseudoSubject;
    private final boolean push;
    private final long receivedTimestampMillis;
    private final boolean replyPathPresent;
    private final RecipientId sender;
    private final int senderDeviceId;
    private final long sentTimestampMillis;
    private final String serverGuid;
    private final long serverTimestampMillis;
    private final String serviceCenterAddress;
    private final int subscriptionId;
    private final boolean unidentified;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public boolean isContentPreKeyBundle() {
        return false;
    }

    public boolean isEndSession() {
        return false;
    }

    public boolean isGroup() {
        return false;
    }

    public boolean isIdentityDefault() {
        return false;
    }

    public boolean isIdentityUpdate() {
        return false;
    }

    public boolean isIdentityVerified() {
        return false;
    }

    public boolean isJoined() {
        return false;
    }

    public boolean isJustAGroupLeave() {
        return false;
    }

    public boolean isLegacyPreKeyBundle() {
        return false;
    }

    public boolean isSecureMessage() {
        return false;
    }

    public IncomingTextMessage(RecipientId recipientId, SmsMessage smsMessage, int i) {
        this.message = smsMessage.getDisplayMessageBody();
        this.sender = recipientId;
        this.senderDeviceId = 1;
        this.protocol = smsMessage.getProtocolIdentifier();
        this.serviceCenterAddress = smsMessage.getServiceCenterAddress();
        this.replyPathPresent = smsMessage.isReplyPathPresent();
        this.pseudoSubject = smsMessage.getPseudoSubject();
        this.sentTimestampMillis = smsMessage.getTimestampMillis();
        this.serverTimestampMillis = -1;
        this.receivedTimestampMillis = System.currentTimeMillis();
        this.subscriptionId = i;
        this.expiresInMillis = 0;
        this.groupId = null;
        this.push = false;
        this.unidentified = false;
        this.serverGuid = null;
    }

    public IncomingTextMessage(RecipientId recipientId, int i, long j, long j2, long j3, String str, Optional<GroupId> optional, long j4, boolean z, String str2) {
        this.message = str;
        this.sender = recipientId;
        this.senderDeviceId = i;
        this.protocol = NotificationIds.SMS_IMPORT_COMPLETE;
        this.serviceCenterAddress = "GCM";
        this.replyPathPresent = true;
        this.pseudoSubject = "";
        this.sentTimestampMillis = j;
        this.serverTimestampMillis = j2;
        this.receivedTimestampMillis = j3;
        this.push = true;
        this.subscriptionId = -1;
        this.expiresInMillis = j4;
        this.unidentified = z;
        this.groupId = optional.orElse(null);
        this.serverGuid = str2;
    }

    public IncomingTextMessage(Parcel parcel) {
        this.message = parcel.readString();
        this.sender = (RecipientId) parcel.readParcelable(IncomingTextMessage.class.getClassLoader());
        this.senderDeviceId = parcel.readInt();
        this.protocol = parcel.readInt();
        this.serviceCenterAddress = parcel.readString();
        boolean z = false;
        this.replyPathPresent = parcel.readInt() == 1;
        this.pseudoSubject = parcel.readString();
        this.sentTimestampMillis = parcel.readLong();
        this.serverTimestampMillis = parcel.readLong();
        this.receivedTimestampMillis = parcel.readLong();
        this.groupId = GroupId.parseNullableOrThrow(parcel.readString());
        this.push = parcel.readInt() == 1;
        this.subscriptionId = parcel.readInt();
        this.expiresInMillis = parcel.readLong();
        this.unidentified = parcel.readInt() == 1 ? true : z;
        this.serverGuid = parcel.readString();
    }

    public IncomingTextMessage(IncomingTextMessage incomingTextMessage, String str) {
        this.message = str;
        this.sender = incomingTextMessage.getSender();
        this.senderDeviceId = incomingTextMessage.getSenderDeviceId();
        this.protocol = incomingTextMessage.getProtocol();
        this.serviceCenterAddress = incomingTextMessage.getServiceCenterAddress();
        this.replyPathPresent = incomingTextMessage.isReplyPathPresent();
        this.pseudoSubject = incomingTextMessage.getPseudoSubject();
        this.sentTimestampMillis = incomingTextMessage.getSentTimestampMillis();
        this.serverTimestampMillis = incomingTextMessage.getServerTimestampMillis();
        this.receivedTimestampMillis = incomingTextMessage.getReceivedTimestampMillis();
        this.groupId = incomingTextMessage.getGroupId();
        this.push = incomingTextMessage.isPush();
        this.subscriptionId = incomingTextMessage.getSubscriptionId();
        this.expiresInMillis = incomingTextMessage.getExpiresIn();
        this.unidentified = incomingTextMessage.isUnidentified();
        this.serverGuid = incomingTextMessage.getServerGuid();
    }

    public IncomingTextMessage(List<IncomingTextMessage> list) {
        StringBuilder sb = new StringBuilder();
        for (IncomingTextMessage incomingTextMessage : list) {
            sb.append(incomingTextMessage.getMessageBody());
        }
        this.message = sb.toString();
        this.sender = list.get(0).getSender();
        this.senderDeviceId = list.get(0).getSenderDeviceId();
        this.protocol = list.get(0).getProtocol();
        this.serviceCenterAddress = list.get(0).getServiceCenterAddress();
        this.replyPathPresent = list.get(0).isReplyPathPresent();
        this.pseudoSubject = list.get(0).getPseudoSubject();
        this.sentTimestampMillis = list.get(0).getSentTimestampMillis();
        this.serverTimestampMillis = list.get(0).getServerTimestampMillis();
        this.receivedTimestampMillis = list.get(0).getReceivedTimestampMillis();
        this.groupId = list.get(0).getGroupId();
        this.push = list.get(0).isPush();
        this.subscriptionId = list.get(0).getSubscriptionId();
        this.expiresInMillis = list.get(0).getExpiresIn();
        this.unidentified = list.get(0).isUnidentified();
        this.serverGuid = list.get(0).getServerGuid();
    }

    public int getSubscriptionId() {
        return this.subscriptionId;
    }

    public long getExpiresIn() {
        return this.expiresInMillis;
    }

    public long getSentTimestampMillis() {
        return this.sentTimestampMillis;
    }

    public long getServerTimestampMillis() {
        return this.serverTimestampMillis;
    }

    public long getReceivedTimestampMillis() {
        return this.receivedTimestampMillis;
    }

    public String getPseudoSubject() {
        return this.pseudoSubject;
    }

    public String getMessageBody() {
        return this.message;
    }

    public RecipientId getSender() {
        return this.sender;
    }

    public int getSenderDeviceId() {
        return this.senderDeviceId;
    }

    public int getProtocol() {
        return this.protocol;
    }

    public String getServiceCenterAddress() {
        return this.serviceCenterAddress;
    }

    public boolean isReplyPathPresent() {
        return this.replyPathPresent;
    }

    public boolean isPreKeyBundle() {
        return isLegacyPreKeyBundle() || isContentPreKeyBundle();
    }

    public boolean isPush() {
        return this.push;
    }

    public GroupId getGroupId() {
        return this.groupId;
    }

    public boolean isUnidentified() {
        return this.unidentified;
    }

    public String getServerGuid() {
        return this.serverGuid;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.message);
        parcel.writeParcelable(this.sender, i);
        parcel.writeInt(this.senderDeviceId);
        parcel.writeInt(this.protocol);
        parcel.writeString(this.serviceCenterAddress);
        parcel.writeInt(this.replyPathPresent ? 1 : 0);
        parcel.writeString(this.pseudoSubject);
        parcel.writeLong(this.sentTimestampMillis);
        GroupId groupId = this.groupId;
        parcel.writeString(groupId == null ? null : groupId.toString());
        parcel.writeInt(this.push ? 1 : 0);
        parcel.writeInt(this.subscriptionId);
        parcel.writeLong(this.expiresInMillis);
        parcel.writeInt(this.unidentified ? 1 : 0);
        parcel.writeString(this.serverGuid);
    }
}
