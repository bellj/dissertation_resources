package org.thoughtcrime.securesms.sms;

import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class OutgoingIdentityDefaultMessage extends OutgoingTextMessage {
    @Override // org.thoughtcrime.securesms.sms.OutgoingTextMessage
    public boolean isIdentityDefault() {
        return true;
    }

    public OutgoingIdentityDefaultMessage(Recipient recipient) {
        this(recipient, "");
    }

    private OutgoingIdentityDefaultMessage(Recipient recipient, String str) {
        super(recipient, str, -1);
    }

    @Override // org.thoughtcrime.securesms.sms.OutgoingTextMessage
    public OutgoingTextMessage withBody(String str) {
        return new OutgoingIdentityDefaultMessage(getRecipient());
    }
}
