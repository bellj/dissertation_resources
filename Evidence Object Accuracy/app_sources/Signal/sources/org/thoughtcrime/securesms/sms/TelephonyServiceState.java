package org.thoughtcrime.securesms.sms;

import android.content.Context;
import android.os.Looper;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import org.thoughtcrime.securesms.database.RecipientDatabase;

/* loaded from: classes4.dex */
public class TelephonyServiceState {
    public boolean isConnected(Context context) {
        ListenThread listenThread = new ListenThread(context);
        listenThread.start();
        return listenThread.get();
    }

    /* loaded from: classes4.dex */
    private static class ListenThread extends Thread {
        private boolean complete;
        private final Context context;
        private boolean result;

        public ListenThread(Context context) {
            this.context = context.getApplicationContext();
        }

        @Override // java.lang.Thread, java.lang.Runnable
        public void run() {
            ListenCallback listenCallback = new ListenCallback(initializeLooper());
            TelephonyManager telephonyManager = (TelephonyManager) this.context.getSystemService(RecipientDatabase.PHONE);
            telephonyManager.listen(listenCallback, 1);
            Looper.loop();
            telephonyManager.listen(listenCallback, 0);
            set(listenCallback.isConnected());
        }

        private Looper initializeLooper() {
            if (Looper.myLooper() == null) {
                Looper.prepare();
            }
            return Looper.myLooper();
        }

        public synchronized boolean get() {
            while (!this.complete) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    throw new AssertionError(e);
                }
            }
            return this.result;
        }

        private synchronized void set(boolean z) {
            this.result = z;
            this.complete = true;
            notifyAll();
        }
    }

    /* loaded from: classes4.dex */
    private static class ListenCallback extends PhoneStateListener {
        private volatile boolean connected;
        private final Looper looper;

        public ListenCallback(Looper looper) {
            this.looper = looper;
        }

        @Override // android.telephony.PhoneStateListener
        public void onServiceStateChanged(ServiceState serviceState) {
            this.connected = serviceState.getState() == 0;
            this.looper.quit();
        }

        public boolean isConnected() {
            return this.connected;
        }
    }
}
