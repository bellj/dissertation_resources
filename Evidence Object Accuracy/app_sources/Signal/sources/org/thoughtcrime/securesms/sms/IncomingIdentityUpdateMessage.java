package org.thoughtcrime.securesms.sms;

/* loaded from: classes4.dex */
public class IncomingIdentityUpdateMessage extends IncomingTextMessage {
    @Override // org.thoughtcrime.securesms.sms.IncomingTextMessage
    public boolean isIdentityUpdate() {
        return true;
    }

    public IncomingIdentityUpdateMessage(IncomingTextMessage incomingTextMessage) {
        super(incomingTextMessage, "");
    }
}
