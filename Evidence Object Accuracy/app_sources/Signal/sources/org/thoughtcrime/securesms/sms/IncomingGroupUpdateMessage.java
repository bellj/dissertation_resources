package org.thoughtcrime.securesms.sms;

import com.google.protobuf.ByteString;
import j$.util.Optional;
import org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2Context;
import org.thoughtcrime.securesms.mms.MessageGroupContext;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes4.dex */
public final class IncomingGroupUpdateMessage extends IncomingTextMessage {
    private final MessageGroupContext groupContext;

    @Override // org.thoughtcrime.securesms.sms.IncomingTextMessage
    public boolean isGroup() {
        return true;
    }

    public IncomingGroupUpdateMessage(IncomingTextMessage incomingTextMessage, SignalServiceProtos.GroupContext groupContext, String str) {
        this(incomingTextMessage, new MessageGroupContext(groupContext));
    }

    public IncomingGroupUpdateMessage(IncomingTextMessage incomingTextMessage, DecryptedGroupV2Context decryptedGroupV2Context) {
        this(incomingTextMessage, new MessageGroupContext(decryptedGroupV2Context));
    }

    public IncomingGroupUpdateMessage(IncomingTextMessage incomingTextMessage, MessageGroupContext messageGroupContext) {
        super(incomingTextMessage, messageGroupContext.getEncodedGroupContext());
        this.groupContext = messageGroupContext;
    }

    public boolean isUpdate() {
        return GroupV2UpdateMessageUtil.isUpdate(this.groupContext) || this.groupContext.requireGroupV1Properties().isUpdate();
    }

    public boolean isGroupV2() {
        return GroupV2UpdateMessageUtil.isGroupV2(this.groupContext);
    }

    public boolean isQuit() {
        return !isGroupV2() && this.groupContext.requireGroupV1Properties().isQuit();
    }

    @Override // org.thoughtcrime.securesms.sms.IncomingTextMessage
    public boolean isJustAGroupLeave() {
        return GroupV2UpdateMessageUtil.isJustAGroupLeave(this.groupContext);
    }

    public boolean isCancelJoinRequest() {
        return GroupV2UpdateMessageUtil.isJoinRequestCancel(this.groupContext);
    }

    public int getChangeRevision() {
        return GroupV2UpdateMessageUtil.getChangeRevision(this.groupContext);
    }

    public Optional<ByteString> getChangeEditor() {
        return GroupV2UpdateMessageUtil.getChangeEditor(this.groupContext);
    }
}
