package org.thoughtcrime.securesms.sms;

import j$.util.Map;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.MapsKt__MapsJVMKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.collections.SetsKt___SetsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.RangesKt___RangesKt;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.AttachmentId;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.attachments.UriAttachment;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobs.AttachmentCompressionJob;
import org.thoughtcrime.securesms.jobs.AttachmentCopyJob;
import org.thoughtcrime.securesms.jobs.AttachmentUploadJob;
import org.thoughtcrime.securesms.jobs.ResumableUploadSpecJob;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.notifications.v2.MessageNotifierV2;

/* compiled from: UploadDependencyGraph.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0005\u0018\u0000 \u00102\u00020\u0001:\u0003\u000f\u0010\u0011B/\b\u0002\u0012\u0018\u0010\u0002\u001a\u0014\u0012\u0004\u0012\u00020\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0003\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0005¢\u0006\u0002\u0010\tJ\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\b0\u0005R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0005X\u0004¢\u0006\u0002\n\u0000R#\u0010\u0002\u001a\u0014\u0012\u0004\u0012\u00020\u0004\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u000e\u0010\f\u001a\u00020\rX\u000e¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/sms/UploadDependencyGraph;", "", "dependencyMap", "", "Lorg/thoughtcrime/securesms/mms/OutgoingMediaMessage;", "", "Lorg/thoughtcrime/securesms/sms/UploadDependencyGraph$Node;", "deferredJobQueue", "Lorg/thoughtcrime/securesms/jobmanager/JobManager$Chain;", "(Ljava/util/Map;Ljava/util/List;)V", "getDependencyMap", "()Ljava/util/Map;", "hasConsumedJobQueue", "", "consumeDeferredQueue", "AttachmentKey", "Companion", "Node", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class UploadDependencyGraph {
    public static final Companion Companion = new Companion(null);
    public static final UploadDependencyGraph EMPTY = new UploadDependencyGraph(MapsKt__MapsKt.emptyMap(), CollectionsKt__CollectionsKt.emptyList());
    private final List<JobManager.Chain> deferredJobQueue;
    private final Map<OutgoingMediaMessage, List<Node>> dependencyMap;
    private boolean hasConsumedJobQueue;

    public /* synthetic */ UploadDependencyGraph(Map map, List list, DefaultConstructorMarker defaultConstructorMarker) {
        this(map, list);
    }

    @JvmStatic
    public static final UploadDependencyGraph create(List<? extends OutgoingMediaMessage> list, JobManager jobManager, Function1<? super Attachment, ? extends DatabaseAttachment> function1) {
        return Companion.create(list, jobManager, function1);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.util.Map<org.thoughtcrime.securesms.mms.OutgoingMediaMessage, ? extends java.util.List<org.thoughtcrime.securesms.sms.UploadDependencyGraph$Node>> */
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.jobmanager.JobManager$Chain> */
    /* JADX WARN: Multi-variable type inference failed */
    private UploadDependencyGraph(Map<OutgoingMediaMessage, ? extends List<Node>> map, List<? extends JobManager.Chain> list) {
        this.dependencyMap = map;
        this.deferredJobQueue = list;
    }

    public final Map<OutgoingMediaMessage, List<Node>> getDependencyMap() {
        return this.dependencyMap;
    }

    /* compiled from: UploadDependencyGraph.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u0019\u0012\n\u0010\u0002\u001a\u00060\u0003j\u0002`\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\r\u0010\f\u001a\u00060\u0003j\u0002`\u0004HÆ\u0003J\t\u0010\r\u001a\u00020\u0006HÆ\u0003J!\u0010\u000e\u001a\u00020\u00002\f\b\u0002\u0010\u0002\u001a\u00060\u0003j\u0002`\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u0006HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0015\u0010\u0002\u001a\u00060\u0003j\u0002`\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/sms/UploadDependencyGraph$Node;", "", "jobId", "", "Lorg/thoughtcrime/securesms/sms/JobId;", "attachmentId", "Lorg/thoughtcrime/securesms/attachments/AttachmentId;", "(Ljava/lang/String;Lorg/thoughtcrime/securesms/attachments/AttachmentId;)V", "getAttachmentId", "()Lorg/thoughtcrime/securesms/attachments/AttachmentId;", "getJobId", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Node {
        private final AttachmentId attachmentId;
        private final String jobId;

        public static /* synthetic */ Node copy$default(Node node, String str, AttachmentId attachmentId, int i, Object obj) {
            if ((i & 1) != 0) {
                str = node.jobId;
            }
            if ((i & 2) != 0) {
                attachmentId = node.attachmentId;
            }
            return node.copy(str, attachmentId);
        }

        public final String component1() {
            return this.jobId;
        }

        public final AttachmentId component2() {
            return this.attachmentId;
        }

        public final Node copy(String str, AttachmentId attachmentId) {
            Intrinsics.checkNotNullParameter(str, "jobId");
            Intrinsics.checkNotNullParameter(attachmentId, "attachmentId");
            return new Node(str, attachmentId);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Node)) {
                return false;
            }
            Node node = (Node) obj;
            return Intrinsics.areEqual(this.jobId, node.jobId) && Intrinsics.areEqual(this.attachmentId, node.attachmentId);
        }

        public int hashCode() {
            return (this.jobId.hashCode() * 31) + this.attachmentId.hashCode();
        }

        public String toString() {
            return "Node(jobId=" + this.jobId + ", attachmentId=" + this.attachmentId + ')';
        }

        public Node(String str, AttachmentId attachmentId) {
            Intrinsics.checkNotNullParameter(str, "jobId");
            Intrinsics.checkNotNullParameter(attachmentId, "attachmentId");
            this.jobId = str;
            this.attachmentId = attachmentId;
        }

        public final String getJobId() {
            return this.jobId;
        }

        public final AttachmentId getAttachmentId() {
            return this.attachmentId;
        }
    }

    /* compiled from: UploadDependencyGraph.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\u00020\u0003B\u0017\u0012\u0006\u0010\u0004\u001a\u00028\u0000\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u000e\u0010\u000b\u001a\u00028\u0000HÆ\u0003¢\u0006\u0002\u0010\tJ\t\u0010\f\u001a\u00020\u0006HÂ\u0003J(\u0010\r\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\b\b\u0002\u0010\u0004\u001a\u00028\u00002\b\b\u0002\u0010\u0005\u001a\u00020\u0006HÆ\u0001¢\u0006\u0002\u0010\u000eJ\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0003HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0013\u0010\u0004\u001a\u00028\u0000¢\u0006\n\n\u0002\u0010\n\u001a\u0004\b\b\u0010\tR\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/sms/UploadDependencyGraph$AttachmentKey;", "A", "Lorg/thoughtcrime/securesms/attachments/Attachment;", "", "attachment", "transformProperties", "Lorg/thoughtcrime/securesms/database/AttachmentDatabase$TransformProperties;", "(Lorg/thoughtcrime/securesms/attachments/Attachment;Lorg/thoughtcrime/securesms/database/AttachmentDatabase$TransformProperties;)V", "getAttachment", "()Lorg/thoughtcrime/securesms/attachments/Attachment;", "Lorg/thoughtcrime/securesms/attachments/Attachment;", "component1", "component2", "copy", "(Lorg/thoughtcrime/securesms/attachments/Attachment;Lorg/thoughtcrime/securesms/database/AttachmentDatabase$TransformProperties;)Lorg/thoughtcrime/securesms/sms/UploadDependencyGraph$AttachmentKey;", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class AttachmentKey<A extends Attachment> {
        private final A attachment;
        private final AttachmentDatabase.TransformProperties transformProperties;

        private final AttachmentDatabase.TransformProperties component2() {
            return this.transformProperties;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.sms.UploadDependencyGraph$AttachmentKey */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ AttachmentKey copy$default(AttachmentKey attachmentKey, Attachment attachment, AttachmentDatabase.TransformProperties transformProperties, int i, Object obj) {
            if ((i & 1) != 0) {
                attachment = attachmentKey.attachment;
            }
            if ((i & 2) != 0) {
                transformProperties = attachmentKey.transformProperties;
            }
            return attachmentKey.copy(attachment, transformProperties);
        }

        public final A component1() {
            return this.attachment;
        }

        public final AttachmentKey<A> copy(A a, AttachmentDatabase.TransformProperties transformProperties) {
            Intrinsics.checkNotNullParameter(a, "attachment");
            Intrinsics.checkNotNullParameter(transformProperties, "transformProperties");
            return new AttachmentKey<>(a, transformProperties);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof AttachmentKey)) {
                return false;
            }
            AttachmentKey attachmentKey = (AttachmentKey) obj;
            return Intrinsics.areEqual(this.attachment, attachmentKey.attachment) && Intrinsics.areEqual(this.transformProperties, attachmentKey.transformProperties);
        }

        public int hashCode() {
            return (this.attachment.hashCode() * 31) + this.transformProperties.hashCode();
        }

        public String toString() {
            return "AttachmentKey(attachment=" + this.attachment + ", transformProperties=" + this.transformProperties + ')';
        }

        public AttachmentKey(A a, AttachmentDatabase.TransformProperties transformProperties) {
            Intrinsics.checkNotNullParameter(a, "attachment");
            Intrinsics.checkNotNullParameter(transformProperties, "transformProperties");
            this.attachment = a;
            this.transformProperties = transformProperties;
        }

        public final A getAttachment() {
            return this.attachment;
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public /* synthetic */ AttachmentKey(org.thoughtcrime.securesms.attachments.Attachment r1, org.thoughtcrime.securesms.database.AttachmentDatabase.TransformProperties r2, int r3, kotlin.jvm.internal.DefaultConstructorMarker r4) {
            /*
                r0 = this;
                r3 = r3 & 2
                if (r3 == 0) goto L_0x000d
                org.thoughtcrime.securesms.database.AttachmentDatabase$TransformProperties r2 = r1.getTransformProperties()
                java.lang.String r3 = "attachment.transformProperties"
                kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r2, r3)
            L_0x000d:
                r0.<init>(r1, r2)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.sms.UploadDependencyGraph.AttachmentKey.<init>(org.thoughtcrime.securesms.attachments.Attachment, org.thoughtcrime.securesms.database.AttachmentDatabase$TransformProperties, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }
    }

    public final List<JobManager.Chain> consumeDeferredQueue() {
        if (this.hasConsumedJobQueue) {
            return CollectionsKt__CollectionsKt.emptyList();
        }
        synchronized (this) {
            if (this.hasConsumedJobQueue) {
                return CollectionsKt__CollectionsKt.emptyList();
            }
            this.hasConsumedJobQueue = true;
            return this.deferredJobQueue;
        }
    }

    /* compiled from: UploadDependencyGraph.kt */
    @Metadata(d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002JB\u0010\u0005\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u00062\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\f2\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\b0\u000eH\u0002JD\u0010\u0010\u001a\u00020\u00042\u001e\u0010\u0011\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u00062\u0006\u0010\u0012\u001a\u00020\u00132\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\b0\u000eH\u0002J2\u0010\u0014\u001a\u00020\u00042\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\f2\u0006\u0010\u0012\u001a\u00020\u00132\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\b0\u000eH\u0007J(\u0010\u0015\u001a\u0012\u0012\b\u0012\u00060\u0017j\u0002`\u0018\u0012\u0004\u0012\u00020\u00190\u00162\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u001a\u001a\u00020\bH\u0002J\u0012\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\b0\u0007*\u00020\bH\u0002J\u0012\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u001d0\u0007*\u00020\u001dH\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/sms/UploadDependencyGraph$Companion;", "", "()V", "EMPTY", "Lorg/thoughtcrime/securesms/sms/UploadDependencyGraph;", "buildAttachmentMap", "", "Lorg/thoughtcrime/securesms/sms/UploadDependencyGraph$AttachmentKey;", "Lorg/thoughtcrime/securesms/attachments/DatabaseAttachment;", "", "Lorg/thoughtcrime/securesms/mms/OutgoingMediaMessage;", MessageNotifierV2.NOTIFICATION_GROUP, "", "insertAttachmentForPreUpload", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/attachments/Attachment;", "buildDependencyGraph", "attachmentIdToOutgoingMessagesMap", "jobManager", "Lorg/thoughtcrime/securesms/jobmanager/JobManager;", "create", "createAttachmentUploadChain", "Lkotlin/Pair;", "", "Lorg/thoughtcrime/securesms/sms/JobId;", "Lorg/thoughtcrime/securesms/jobmanager/JobManager$Chain;", "databaseAttachment", "asDatabaseAttachmentKey", "asUriAttachmentKey", "Lorg/thoughtcrime/securesms/attachments/UriAttachment;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        private final AttachmentKey<DatabaseAttachment> asDatabaseAttachmentKey(DatabaseAttachment databaseAttachment) {
            AttachmentDatabase.TransformProperties transformProperties = databaseAttachment.getTransformProperties();
            Intrinsics.checkNotNullExpressionValue(transformProperties, "this.transformProperties");
            return new AttachmentKey<>(databaseAttachment, transformProperties);
        }

        private final AttachmentKey<UriAttachment> asUriAttachmentKey(UriAttachment uriAttachment) {
            AttachmentDatabase.TransformProperties transformProperties = uriAttachment.getTransformProperties();
            Intrinsics.checkNotNullExpressionValue(transformProperties, "transformProperties");
            return new AttachmentKey<>(uriAttachment, transformProperties);
        }

        @JvmStatic
        public final UploadDependencyGraph create(List<? extends OutgoingMediaMessage> list, JobManager jobManager, Function1<? super Attachment, ? extends DatabaseAttachment> function1) {
            Intrinsics.checkNotNullParameter(list, MessageNotifierV2.NOTIFICATION_GROUP);
            Intrinsics.checkNotNullParameter(jobManager, "jobManager");
            Intrinsics.checkNotNullParameter(function1, "insertAttachmentForPreUpload");
            return buildDependencyGraph(buildAttachmentMap(list, function1), jobManager, function1);
        }

        private final Map<AttachmentKey<DatabaseAttachment>, Set<OutgoingMediaMessage>> buildAttachmentMap(List<? extends OutgoingMediaMessage> list, Function1<? super Attachment, ? extends DatabaseAttachment> function1) {
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            LinkedHashMap linkedHashMap2 = new LinkedHashMap();
            for (OutgoingMediaMessage outgoingMediaMessage : list) {
                List<Attachment> attachments = outgoingMediaMessage.getAttachments();
                Intrinsics.checkNotNullExpressionValue(attachments, "message.attachments");
                List<LinkPreview> linkPreviews = outgoingMediaMessage.getLinkPreviews();
                Intrinsics.checkNotNullExpressionValue(linkPreviews, "message.linkPreviews");
                ArrayList arrayList = new ArrayList();
                for (LinkPreview linkPreview : linkPreviews) {
                    Attachment orElse = linkPreview.getThumbnail().orElse(null);
                    if (orElse != null) {
                        arrayList.add(orElse);
                    }
                }
                List list2 = CollectionsKt___CollectionsKt.plus((Collection) attachments, (Iterable) arrayList);
                List<Contact> sharedContacts = outgoingMediaMessage.getSharedContacts();
                Intrinsics.checkNotNullExpressionValue(sharedContacts, "message.sharedContacts");
                ArrayList arrayList2 = new ArrayList();
                for (Contact contact : sharedContacts) {
                    Contact.Avatar avatar = contact.getAvatar();
                    Attachment attachment = avatar != null ? avatar.getAttachment() : null;
                    if (attachment != null) {
                        arrayList2.add(attachment);
                    }
                }
                List<Attachment> list3 = CollectionsKt___CollectionsKt.plus((Collection) list2, (Iterable) arrayList2);
                ArrayList arrayList3 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list3, 10));
                for (Attachment attachment2 : list3) {
                    AttachmentDatabase.TransformProperties transformProperties = attachment2.getTransformProperties();
                    Intrinsics.checkNotNullExpressionValue(transformProperties, "it.transformProperties");
                    arrayList3.add(new AttachmentKey(attachment2, transformProperties));
                }
                for (AttachmentKey attachmentKey : CollectionsKt___CollectionsKt.toSet(arrayList3)) {
                    Attachment attachment3 = attachmentKey.getAttachment();
                    if (attachment3 instanceof DatabaseAttachment) {
                        DatabaseAttachment databaseAttachment = (DatabaseAttachment) attachment3;
                        linkedHashMap.put(asDatabaseAttachmentKey(databaseAttachment), SetsKt___SetsKt.plus((Set) Map.EL.getOrDefault(linkedHashMap, asDatabaseAttachmentKey(databaseAttachment), SetsKt__SetsKt.emptySet()), outgoingMediaMessage));
                    } else if (attachment3 instanceof UriAttachment) {
                        AttachmentKey<UriAttachment> asUriAttachmentKey = asUriAttachmentKey((UriAttachment) attachment3);
                        Object obj = linkedHashMap2.get(asUriAttachmentKey);
                        if (obj == null) {
                            obj = (DatabaseAttachment) function1.invoke(attachment3);
                            linkedHashMap2.put(asUriAttachmentKey, obj);
                        }
                        AttachmentKey<DatabaseAttachment> asDatabaseAttachmentKey = asDatabaseAttachmentKey((DatabaseAttachment) obj);
                        linkedHashMap.put(asDatabaseAttachmentKey, SetsKt___SetsKt.plus((Set) Map.EL.getOrDefault(linkedHashMap, asDatabaseAttachmentKey, SetsKt__SetsKt.emptySet()), outgoingMediaMessage));
                    } else {
                        throw new IllegalStateException(("Unsupported attachment subclass - " + attachment3.getClass()).toString());
                    }
                }
            }
            return linkedHashMap;
        }

        private final UploadDependencyGraph buildDependencyGraph(java.util.Map<AttachmentKey<DatabaseAttachment>, ? extends Set<? extends OutgoingMediaMessage>> map, JobManager jobManager, Function1<? super Attachment, ? extends DatabaseAttachment> function1) {
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            ArrayList arrayList = new ArrayList();
            for (Map.Entry<AttachmentKey<DatabaseAttachment>, ? extends Set<? extends OutgoingMediaMessage>> entry : map.entrySet()) {
                AttachmentKey<DatabaseAttachment> key = entry.getKey();
                Set set = (Set) entry.getValue();
                Pair<String, JobManager.Chain> createAttachmentUploadChain = createAttachmentUploadChain(jobManager, key.getAttachment());
                JobManager.Chain component2 = createAttachmentUploadChain.component2();
                OutgoingMediaMessage outgoingMediaMessage = (OutgoingMediaMessage) CollectionsKt___CollectionsKt.first(set);
                List list = CollectionsKt___CollectionsKt.drop(set, 1);
                AttachmentId attachmentId = key.getAttachment().getAttachmentId();
                Intrinsics.checkNotNullExpressionValue(attachmentId, "attachmentKey.attachment.attachmentId");
                linkedHashMap.put(outgoingMediaMessage, CollectionsKt___CollectionsKt.plus((Collection<? extends Node>) ((Collection<? extends Object>) ((List) Map.EL.getOrDefault(linkedHashMap, outgoingMediaMessage, CollectionsKt__CollectionsKt.emptyList()))), new Node(createAttachmentUploadChain.component1(), attachmentId)));
                if (!list.isEmpty()) {
                    LinkedHashMap linkedHashMap2 = new LinkedHashMap(RangesKt___RangesKt.coerceAtLeast(MapsKt__MapsJVMKt.mapCapacity(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10)), 16));
                    for (Object obj : list) {
                        OutgoingMediaMessage outgoingMediaMessage2 = (OutgoingMediaMessage) obj;
                        linkedHashMap2.put(obj, ((DatabaseAttachment) function1.invoke(key.getAttachment())).getAttachmentId());
                    }
                    AttachmentCopyJob attachmentCopyJob = new AttachmentCopyJob(key.getAttachment().getAttachmentId(), CollectionsKt___CollectionsKt.toList(linkedHashMap2.values()));
                    for (Map.Entry entry2 : linkedHashMap2.entrySet()) {
                        OutgoingMediaMessage outgoingMediaMessage3 = (OutgoingMediaMessage) entry2.getKey();
                        String id = attachmentCopyJob.getId();
                        Intrinsics.checkNotNullExpressionValue(id, "copyJob.id");
                        linkedHashMap.put(outgoingMediaMessage3, CollectionsKt___CollectionsKt.plus((Collection<? extends Node>) ((Collection<? extends Object>) ((List) Map.EL.getOrDefault(linkedHashMap, outgoingMediaMessage3, CollectionsKt__CollectionsKt.emptyList()))), new Node(id, (AttachmentId) entry2.getValue())));
                    }
                    component2.then(attachmentCopyJob);
                }
                arrayList.add(component2);
            }
            return new UploadDependencyGraph(linkedHashMap, arrayList, null);
        }

        private final Pair<String, JobManager.Chain> createAttachmentUploadChain(JobManager jobManager, DatabaseAttachment databaseAttachment) {
            AttachmentCompressionJob fromAttachment = AttachmentCompressionJob.fromAttachment(databaseAttachment, false, -1);
            Intrinsics.checkNotNullExpressionValue(fromAttachment, "fromAttachment(databaseAttachment, false, -1)");
            ResumableUploadSpecJob resumableUploadSpecJob = new ResumableUploadSpecJob();
            AttachmentUploadJob attachmentUploadJob = new AttachmentUploadJob(databaseAttachment.getAttachmentId());
            String id = attachmentUploadJob.getId();
            Intrinsics.checkNotNullExpressionValue(id, "uploadJob.id");
            return TuplesKt.to(id, jobManager.startChain(fromAttachment).then(resumableUploadSpecJob).then(attachmentUploadJob));
        }
    }
}
