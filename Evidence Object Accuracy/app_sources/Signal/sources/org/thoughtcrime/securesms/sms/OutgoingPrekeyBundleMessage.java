package org.thoughtcrime.securesms.sms;

/* loaded from: classes4.dex */
public class OutgoingPrekeyBundleMessage extends OutgoingTextMessage {
    @Override // org.thoughtcrime.securesms.sms.OutgoingTextMessage
    public boolean isPreKeyBundle() {
        return true;
    }

    public OutgoingPrekeyBundleMessage(OutgoingTextMessage outgoingTextMessage, String str) {
        super(outgoingTextMessage, str);
    }

    @Override // org.thoughtcrime.securesms.sms.OutgoingTextMessage
    public OutgoingTextMessage withBody(String str) {
        return new OutgoingPrekeyBundleMessage(this, str);
    }
}
