package org.thoughtcrime.securesms.sms;

import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class OutgoingEncryptedMessage extends OutgoingTextMessage {
    @Override // org.thoughtcrime.securesms.sms.OutgoingTextMessage
    public boolean isSecureMessage() {
        return true;
    }

    public OutgoingEncryptedMessage(Recipient recipient, String str, long j) {
        super(recipient, str, j, -1);
    }

    private OutgoingEncryptedMessage(OutgoingEncryptedMessage outgoingEncryptedMessage, String str) {
        super(outgoingEncryptedMessage, str);
    }

    @Override // org.thoughtcrime.securesms.sms.OutgoingTextMessage
    public OutgoingTextMessage withExpiry(long j) {
        return new OutgoingEncryptedMessage(getRecipient(), getMessageBody(), j);
    }

    @Override // org.thoughtcrime.securesms.sms.OutgoingTextMessage
    public OutgoingTextMessage withBody(String str) {
        return new OutgoingEncryptedMessage(this, str);
    }
}
