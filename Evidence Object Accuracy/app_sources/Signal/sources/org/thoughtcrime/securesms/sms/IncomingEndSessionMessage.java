package org.thoughtcrime.securesms.sms;

/* loaded from: classes4.dex */
public class IncomingEndSessionMessage extends IncomingTextMessage {
    @Override // org.thoughtcrime.securesms.sms.IncomingTextMessage
    public boolean isEndSession() {
        return true;
    }

    public IncomingEndSessionMessage(IncomingTextMessage incomingTextMessage) {
        this(incomingTextMessage, incomingTextMessage.getMessageBody());
    }

    public IncomingEndSessionMessage(IncomingTextMessage incomingTextMessage, String str) {
        super(incomingTextMessage, str);
    }
}
