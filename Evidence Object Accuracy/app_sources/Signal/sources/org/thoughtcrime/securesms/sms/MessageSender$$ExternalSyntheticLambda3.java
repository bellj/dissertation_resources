package org.thoughtcrime.securesms.sms;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.sms.MessageSender;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MessageSender$$ExternalSyntheticLambda3 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return ((MessageSender.PreUploadResult) obj).getJobIds();
    }
}
