package org.thoughtcrime.securesms.sms;

/* loaded from: classes4.dex */
public class IncomingEncryptedMessage extends IncomingTextMessage {
    @Override // org.thoughtcrime.securesms.sms.IncomingTextMessage
    public boolean isSecureMessage() {
        return true;
    }

    public IncomingEncryptedMessage(IncomingTextMessage incomingTextMessage, String str) {
        super(incomingTextMessage, str);
    }
}
