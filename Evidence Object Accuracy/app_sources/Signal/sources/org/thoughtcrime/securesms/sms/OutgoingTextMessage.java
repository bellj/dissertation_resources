package org.thoughtcrime.securesms.sms;

import org.thoughtcrime.securesms.database.model.SmsMessageRecord;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class OutgoingTextMessage {
    private final long expiresIn;
    private final String message;
    private final Recipient recipient;
    private final int subscriptionId;

    public boolean isEndSession() {
        return false;
    }

    public boolean isIdentityDefault() {
        return false;
    }

    public boolean isIdentityVerified() {
        return false;
    }

    public boolean isKeyExchange() {
        return false;
    }

    public boolean isPreKeyBundle() {
        return false;
    }

    public boolean isSecureMessage() {
        return false;
    }

    public OutgoingTextMessage(Recipient recipient, String str, int i) {
        this(recipient, str, 0, i);
    }

    public OutgoingTextMessage(Recipient recipient, String str, long j, int i) {
        this.recipient = recipient;
        this.message = str;
        this.expiresIn = j;
        this.subscriptionId = i;
    }

    public OutgoingTextMessage(OutgoingTextMessage outgoingTextMessage, String str) {
        this.recipient = outgoingTextMessage.getRecipient();
        this.subscriptionId = outgoingTextMessage.getSubscriptionId();
        this.expiresIn = outgoingTextMessage.getExpiresIn();
        this.message = str;
    }

    public OutgoingTextMessage withExpiry(long j) {
        return new OutgoingTextMessage(this.recipient, this.message, j, this.subscriptionId);
    }

    public long getExpiresIn() {
        return this.expiresIn;
    }

    public int getSubscriptionId() {
        return this.subscriptionId;
    }

    public String getMessageBody() {
        return this.message;
    }

    public Recipient getRecipient() {
        return this.recipient;
    }

    public static OutgoingTextMessage from(SmsMessageRecord smsMessageRecord) {
        if (smsMessageRecord.isSecure()) {
            return new OutgoingEncryptedMessage(smsMessageRecord.getRecipient(), smsMessageRecord.getBody(), smsMessageRecord.getExpiresIn());
        }
        if (smsMessageRecord.isKeyExchange()) {
            return new OutgoingKeyExchangeMessage(smsMessageRecord.getRecipient(), smsMessageRecord.getBody());
        }
        if (smsMessageRecord.isEndSession()) {
            return new OutgoingEndSessionMessage(new OutgoingTextMessage(smsMessageRecord.getRecipient(), smsMessageRecord.getBody(), 0, -1));
        }
        return new OutgoingTextMessage(smsMessageRecord.getRecipient(), smsMessageRecord.getBody(), smsMessageRecord.getExpiresIn(), smsMessageRecord.getSubscriptionId());
    }

    public OutgoingTextMessage withBody(String str) {
        return new OutgoingTextMessage(this, str);
    }
}
