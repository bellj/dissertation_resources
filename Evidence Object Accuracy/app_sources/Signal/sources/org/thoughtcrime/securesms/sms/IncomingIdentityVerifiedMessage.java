package org.thoughtcrime.securesms.sms;

/* loaded from: classes4.dex */
public class IncomingIdentityVerifiedMessage extends IncomingTextMessage {
    @Override // org.thoughtcrime.securesms.sms.IncomingTextMessage
    public boolean isIdentityVerified() {
        return true;
    }

    public IncomingIdentityVerifiedMessage(IncomingTextMessage incomingTextMessage) {
        super(incomingTextMessage, "");
    }
}
