package org.thoughtcrime.securesms.sms;

/* loaded from: classes4.dex */
public class OutgoingEndSessionMessage extends OutgoingTextMessage {
    @Override // org.thoughtcrime.securesms.sms.OutgoingTextMessage
    public boolean isEndSession() {
        return true;
    }

    public OutgoingEndSessionMessage(OutgoingTextMessage outgoingTextMessage) {
        this(outgoingTextMessage, outgoingTextMessage.getMessageBody());
    }

    public OutgoingEndSessionMessage(OutgoingTextMessage outgoingTextMessage, String str) {
        super(outgoingTextMessage, str);
    }

    @Override // org.thoughtcrime.securesms.sms.OutgoingTextMessage
    public OutgoingTextMessage withBody(String str) {
        return new OutgoingEndSessionMessage(this, str);
    }
}
