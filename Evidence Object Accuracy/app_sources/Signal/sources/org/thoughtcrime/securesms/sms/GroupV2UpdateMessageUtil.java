package org.thoughtcrime.securesms.sms;

import com.google.protobuf.ByteString;
import j$.util.Optional;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;
import org.thoughtcrime.securesms.mms.MessageGroupContext;
import org.whispersystems.signalservice.api.groupsv2.DecryptedGroupUtil;

/* loaded from: classes4.dex */
public final class GroupV2UpdateMessageUtil {
    public static boolean isGroupV2(MessageGroupContext messageGroupContext) {
        return messageGroupContext.isV2Group();
    }

    public static boolean isUpdate(MessageGroupContext messageGroupContext) {
        return messageGroupContext.isV2Group();
    }

    public static boolean isJustAGroupLeave(MessageGroupContext messageGroupContext) {
        if (!isGroupV2(messageGroupContext) || !isUpdate(messageGroupContext)) {
            return false;
        }
        DecryptedGroupChange change = messageGroupContext.requireGroupV2Properties().getChange();
        if (!changeEditorOnlyWasRemoved(change) || !noChangesOtherThanDeletes(change)) {
            return false;
        }
        return true;
    }

    private static boolean changeEditorOnlyWasRemoved(DecryptedGroupChange decryptedGroupChange) {
        if (decryptedGroupChange.getDeleteMembersCount() != 1 || !decryptedGroupChange.getDeleteMembers(0).equals(decryptedGroupChange.getEditor())) {
            return false;
        }
        return true;
    }

    private static boolean noChangesOtherThanDeletes(DecryptedGroupChange decryptedGroupChange) {
        return DecryptedGroupUtil.changeIsEmpty(decryptedGroupChange.toBuilder().clearDeleteMembers().build());
    }

    public static boolean isJoinRequestCancel(MessageGroupContext messageGroupContext) {
        if (!isGroupV2(messageGroupContext) || !isUpdate(messageGroupContext) || messageGroupContext.requireGroupV2Properties().getChange().getDeleteRequestingMembersCount() <= 0) {
            return false;
        }
        return true;
    }

    public static int getChangeRevision(MessageGroupContext messageGroupContext) {
        if (!isGroupV2(messageGroupContext) || !isUpdate(messageGroupContext)) {
            return -1;
        }
        return messageGroupContext.requireGroupV2Properties().getChange().getRevision();
    }

    public static Optional<ByteString> getChangeEditor(MessageGroupContext messageGroupContext) {
        if (!isGroupV2(messageGroupContext) || !isUpdate(messageGroupContext)) {
            return Optional.empty();
        }
        return Optional.ofNullable(messageGroupContext.requireGroupV2Properties().getChange().getEditor());
    }
}
