package org.thoughtcrime.securesms.sms;

/* loaded from: classes4.dex */
public class IncomingIdentityDefaultMessage extends IncomingTextMessage {
    @Override // org.thoughtcrime.securesms.sms.IncomingTextMessage
    public boolean isIdentityDefault() {
        return true;
    }

    public IncomingIdentityDefaultMessage(IncomingTextMessage incomingTextMessage) {
        super(incomingTextMessage, "");
    }
}
