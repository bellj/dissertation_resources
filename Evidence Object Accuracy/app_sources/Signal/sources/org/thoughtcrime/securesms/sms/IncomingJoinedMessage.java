package org.thoughtcrime.securesms.sms;

import j$.util.Optional;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class IncomingJoinedMessage extends IncomingTextMessage {
    @Override // org.thoughtcrime.securesms.sms.IncomingTextMessage
    public boolean isJoined() {
        return true;
    }

    @Override // org.thoughtcrime.securesms.sms.IncomingTextMessage
    public boolean isSecureMessage() {
        return true;
    }

    public IncomingJoinedMessage(RecipientId recipientId) {
        super(recipientId, 1, System.currentTimeMillis(), -1, System.currentTimeMillis(), null, Optional.empty(), 0, false, null);
    }
}
