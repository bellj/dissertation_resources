package org.thoughtcrime.securesms.sms;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import java.util.Collection;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MessageSender$$ExternalSyntheticLambda4 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return Stream.of((Collection) obj);
    }
}
