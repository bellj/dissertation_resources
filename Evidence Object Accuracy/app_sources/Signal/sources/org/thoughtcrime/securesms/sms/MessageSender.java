package org.thoughtcrime.securesms.sms;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.jvm.functions.Function1;
import org.greenrobot.eventbus.EventBus;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.AttachmentId;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscovery;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil$$ExternalSyntheticLambda0;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.MmsSmsDatabase;
import org.thoughtcrime.securesms.database.NoSuchMessageException;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.SmsDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.database.model.ReactionRecord;
import org.thoughtcrime.securesms.database.model.SmsMessageRecord;
import org.thoughtcrime.securesms.database.model.StoryType;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobs.AttachmentCompressionJob;
import org.thoughtcrime.securesms.jobs.AttachmentCopyJob;
import org.thoughtcrime.securesms.jobs.AttachmentMarkUploadedJob;
import org.thoughtcrime.securesms.jobs.AttachmentUploadJob;
import org.thoughtcrime.securesms.jobs.MmsSendJob;
import org.thoughtcrime.securesms.jobs.ProfileKeySendJob;
import org.thoughtcrime.securesms.jobs.PushDistributionListSendJob;
import org.thoughtcrime.securesms.jobs.PushGroupSendJob;
import org.thoughtcrime.securesms.jobs.PushMediaSendJob;
import org.thoughtcrime.securesms.jobs.PushSendJob$$ExternalSyntheticLambda2;
import org.thoughtcrime.securesms.jobs.PushSendJob$$ExternalSyntheticLambda3;
import org.thoughtcrime.securesms.jobs.PushSendJob$$ExternalSyntheticLambda4;
import org.thoughtcrime.securesms.jobs.PushSendJob$$ExternalSyntheticLambda5;
import org.thoughtcrime.securesms.jobs.PushTextSendJob;
import org.thoughtcrime.securesms.jobs.ReactionSendJob;
import org.thoughtcrime.securesms.jobs.RemoteDeleteSendJob;
import org.thoughtcrime.securesms.jobs.ResumableUploadSpecJob;
import org.thoughtcrime.securesms.jobs.SmsSendJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mms.MmsException;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.mms.OutgoingSecureMediaMessage;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.service.ExpiringMessageManager;
import org.thoughtcrime.securesms.sms.UploadDependencyGraph;
import org.thoughtcrime.securesms.util.ParcelUtil;
import org.thoughtcrime.securesms.util.SignalLocalMetrics;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.push.DistributionId;
import org.whispersystems.signalservice.api.util.Preconditions;

/* loaded from: classes4.dex */
public class MessageSender {
    private static final String TAG = Log.tag(MessageSender.class);

    /* loaded from: classes4.dex */
    public enum MessageSentEvent {
        INSTANCE
    }

    public static void sendProfileKey(long j) {
        ProfileKeySendJob create = ProfileKeySendJob.create(j, false);
        if (create != null) {
            ApplicationDependencies.getJobManager().add(create);
        }
    }

    public static long send(Context context, OutgoingTextMessage outgoingTextMessage, long j, boolean z, String str, MessageDatabase.InsertListener insertListener) {
        String str2 = TAG;
        Log.i(str2, "Sending text message to " + outgoingTextMessage.getRecipient().getId() + ", thread: " + j);
        SmsDatabase sms = SignalDatabase.sms();
        Recipient recipient = outgoingTextMessage.getRecipient();
        boolean isKeyExchange = outgoingTextMessage.isKeyExchange();
        long orCreateValidThreadId = SignalDatabase.threads().getOrCreateValidThreadId(recipient, j);
        long insertMessageOutbox = sms.insertMessageOutbox(orCreateValidThreadId, applyUniversalExpireTimerIfNecessary(context, recipient, outgoingTextMessage, orCreateValidThreadId), z, System.currentTimeMillis(), insertListener);
        SignalLocalMetrics.IndividualMessageSend.onInsertedIntoDatabase(insertMessageOutbox, str);
        sendTextMessage(context, recipient, z, isKeyExchange, insertMessageOutbox);
        onMessageSent();
        SignalDatabase.threads().update(j, true);
        return orCreateValidThreadId;
    }

    public static void sendStories(Context context, List<OutgoingSecureMediaMessage> list, String str, MessageDatabase.InsertListener insertListener) {
        MmsDatabase mms;
        List list2;
        try {
            String str2 = TAG;
            Log.i(str2, "Sending story messages to " + list.size() + " targets.");
            ThreadDatabase threads = SignalDatabase.threads();
            mms = SignalDatabase.mms();
            ArrayList arrayList = new ArrayList(list.size());
            ArrayList<Long> arrayList2 = new ArrayList(list.size());
            try {
                mms.beginTransaction();
                for (OutgoingSecureMediaMessage outgoingSecureMediaMessage : list) {
                    long orCreateValidThreadId = threads.getOrCreateValidThreadId(outgoingSecureMediaMessage.getRecipient(), -1, outgoingSecureMediaMessage.getDistributionType());
                    long insertMessageOutbox = mms.insertMessageOutbox(applyUniversalExpireTimerIfNecessary(context, outgoingSecureMediaMessage.getRecipient(), outgoingSecureMediaMessage.stripAttachments(), orCreateValidThreadId), orCreateValidThreadId, false, insertListener);
                    arrayList.add(Long.valueOf(insertMessageOutbox));
                    arrayList2.add(Long.valueOf(orCreateValidThreadId));
                    if (!outgoingSecureMediaMessage.getRecipient().isGroup() || !outgoingSecureMediaMessage.getAttachments().isEmpty() || !outgoingSecureMediaMessage.getLinkPreviews().isEmpty() || !outgoingSecureMediaMessage.getSharedContacts().isEmpty()) {
                        SignalLocalMetrics.GroupMessageSend.cancel(str);
                    } else {
                        SignalLocalMetrics.GroupMessageSend.onInsertedIntoDatabase(insertMessageOutbox, str);
                    }
                }
                for (int i = 0; i < arrayList.size(); i++) {
                    long longValue = ((Long) arrayList.get(i)).longValue();
                    OutgoingSecureMediaMessage outgoingSecureMediaMessage2 = list.get(i);
                    Recipient recipient = outgoingSecureMediaMessage2.getRecipient();
                    if (recipient.isDistributionList()) {
                        DistributionId distributionId = SignalDatabase.distributionLists().getDistributionId(recipient.requireDistributionListId());
                        Objects.requireNonNull(distributionId);
                        SignalDatabase.storySends().insert(longValue, SignalDatabase.distributionLists().getMembers(recipient.requireDistributionListId()), outgoingSecureMediaMessage2.getSentTimeMillis(), outgoingSecureMediaMessage2.getStoryType().isStoryWithReplies(), distributionId);
                    }
                }
                UploadDependencyGraph create = UploadDependencyGraph.create(list, ApplicationDependencies.getJobManager(), new Function1() { // from class: org.thoughtcrime.securesms.sms.MessageSender$$ExternalSyntheticLambda8
                    @Override // kotlin.jvm.functions.Function1
                    public final Object invoke(Object obj) {
                        return MessageSender.lambda$sendStories$0((Attachment) obj);
                    }
                });
                for (int i2 = 0; i2 < arrayList.size(); i2++) {
                    long longValue2 = ((Long) arrayList.get(i2)).longValue();
                    OutgoingSecureMediaMessage outgoingSecureMediaMessage3 = list.get(i2);
                    List<UploadDependencyGraph.Node> list3 = create.getDependencyMap().get(outgoingSecureMediaMessage3);
                    if (list3 != null && !list3.isEmpty()) {
                        List<AttachmentId> list4 = (List) Collection$EL.stream(list3).map(new Function() { // from class: org.thoughtcrime.securesms.sms.MessageSender$$ExternalSyntheticLambda9
                            @Override // j$.util.function.Function
                            public /* synthetic */ Function andThen(Function function) {
                                return Function.CC.$default$andThen(this, function);
                            }

                            @Override // j$.util.function.Function
                            public final Object apply(Object obj) {
                                return ((UploadDependencyGraph.Node) obj).getAttachmentId();
                            }

                            @Override // j$.util.function.Function
                            public /* synthetic */ Function compose(Function function) {
                                return Function.CC.$default$compose(this, function);
                            }
                        }).collect(Collectors.toList());
                        SignalDatabase.attachments().updateMessageId(list4, longValue2, true);
                        for (AttachmentId attachmentId : list4) {
                            SignalDatabase.attachments().updateAttachmentCaption(attachmentId, outgoingSecureMediaMessage3.getBody());
                        }
                    }
                    if (outgoingSecureMediaMessage3.getStoryType().isTextStory()) {
                        Log.d(TAG, "No attachments for given text story. Skipping.");
                    } else {
                        Log.e(TAG, "No attachments for given media story. Aborting.");
                        throw new MmsException("No attachment for story.");
                    }
                }
                mms.setTransactionSuccessful();
                mms.endTransaction();
                for (JobManager.Chain chain : create.consumeDeferredQueue()) {
                    chain.enqueue();
                }
                for (int i3 = 0; i3 < arrayList.size(); i3++) {
                    long longValue3 = ((Long) arrayList.get(i3)).longValue();
                    OutgoingSecureMediaMessage outgoingSecureMediaMessage4 = list.get(i3);
                    Recipient recipient2 = outgoingSecureMediaMessage4.getRecipient();
                    List<UploadDependencyGraph.Node> list5 = create.getDependencyMap().get(outgoingSecureMediaMessage4);
                    if (list5 != null) {
                        list2 = (List) Collection$EL.stream(list5).map(new Function() { // from class: org.thoughtcrime.securesms.sms.MessageSender$$ExternalSyntheticLambda10
                            @Override // j$.util.function.Function
                            public /* synthetic */ Function andThen(Function function) {
                                return Function.CC.$default$andThen(this, function);
                            }

                            @Override // j$.util.function.Function
                            public final Object apply(Object obj) {
                                return ((UploadDependencyGraph.Node) obj).getJobId();
                            }

                            @Override // j$.util.function.Function
                            public /* synthetic */ Function compose(Function function) {
                                return Function.CC.$default$compose(this, function);
                            }
                        }).collect(Collectors.toList());
                    } else {
                        list2 = Collections.emptyList();
                    }
                    sendMediaMessage(context, recipient2, false, longValue3, list2);
                }
                onMessageSent();
                for (Long l : arrayList2) {
                    threads.update(l.longValue(), true);
                }
            } catch (MmsException e) {
                Log.w(TAG, "Failed to send stories.", e);
                mms.endTransaction();
            }
        } catch (Throwable th) {
            mms.endTransaction();
            throw th;
        }
    }

    public static /* synthetic */ DatabaseAttachment lambda$sendStories$0(Attachment attachment) {
        try {
            return SignalDatabase.attachments().insertAttachmentForPreUpload(attachment);
        } catch (MmsException e) {
            Log.e(TAG, e);
            throw new IllegalStateException(e);
        }
    }

    public static long send(Context context, OutgoingMediaMessage outgoingMediaMessage, long j, boolean z, String str, MessageDatabase.InsertListener insertListener) {
        String str2 = TAG;
        Log.i(str2, "Sending media message to " + outgoingMediaMessage.getRecipient().getId() + ", thread: " + j);
        try {
            ThreadDatabase threads = SignalDatabase.threads();
            MmsDatabase mms = SignalDatabase.mms();
            long orCreateValidThreadId = threads.getOrCreateValidThreadId(outgoingMediaMessage.getRecipient(), j, outgoingMediaMessage.getDistributionType());
            Recipient recipient = outgoingMediaMessage.getRecipient();
            long insertMessageOutbox = mms.insertMessageOutbox(applyUniversalExpireTimerIfNecessary(context, recipient, outgoingMediaMessage, orCreateValidThreadId), orCreateValidThreadId, z, insertListener);
            if (!outgoingMediaMessage.getRecipient().isGroup() || !outgoingMediaMessage.getAttachments().isEmpty() || !outgoingMediaMessage.getLinkPreviews().isEmpty() || !outgoingMediaMessage.getSharedContacts().isEmpty()) {
                SignalLocalMetrics.GroupMessageSend.cancel(str);
            } else {
                SignalLocalMetrics.GroupMessageSend.onInsertedIntoDatabase(insertMessageOutbox, str);
            }
            sendMediaMessage(context, recipient, z, insertMessageOutbox, Collections.emptyList());
            onMessageSent();
            threads.update(j, true);
            return orCreateValidThreadId;
        } catch (MmsException e) {
            Log.w(TAG, e);
            return j;
        }
    }

    public static long sendPushWithPreUploadedMedia(Context context, OutgoingMediaMessage outgoingMediaMessage, Collection<PreUploadResult> collection, long j, MessageDatabase.InsertListener insertListener) {
        String str = TAG;
        Log.i(str, "Sending media message with pre-uploads to " + outgoingMediaMessage.getRecipient().getId() + ", thread: " + j);
        Preconditions.checkArgument(outgoingMediaMessage.getAttachments().isEmpty(), "If the media is pre-uploaded, there should be no attachments on the message.");
        try {
            ThreadDatabase threads = SignalDatabase.threads();
            MmsDatabase mms = SignalDatabase.mms();
            AttachmentDatabase attachments = SignalDatabase.attachments();
            long orCreateThreadIdFor = j == -1 ? threads.getOrCreateThreadIdFor(outgoingMediaMessage.getRecipient(), outgoingMediaMessage.getDistributionType()) : j;
            Recipient recipient = outgoingMediaMessage.getRecipient();
            long insertMessageOutbox = mms.insertMessageOutbox(applyUniversalExpireTimerIfNecessary(context, recipient, outgoingMediaMessage, orCreateThreadIdFor), orCreateThreadIdFor, false, insertListener);
            List list = Stream.of(collection).map(new MessageSender$$ExternalSyntheticLambda2()).toList();
            List list2 = Stream.of(collection).map(new MessageSender$$ExternalSyntheticLambda3()).flatMap(new MessageSender$$ExternalSyntheticLambda4()).toList();
            attachments.updateMessageId(list, insertMessageOutbox, outgoingMediaMessage.getStoryType().isStory());
            sendMediaMessage(context, recipient, false, insertMessageOutbox, list2);
            onMessageSent();
            threads.update(j, true);
            return orCreateThreadIdFor;
        } catch (MmsException e) {
            Log.w(TAG, e);
            return j;
        }
    }

    public static /* synthetic */ RecipientId lambda$sendMediaBroadcast$1(OutgoingSecureMediaMessage outgoingSecureMediaMessage) {
        return outgoingSecureMediaMessage.getRecipient().getId();
    }

    public static void sendMediaBroadcast(Context context, List<OutgoingSecureMediaMessage> list, Collection<PreUploadResult> collection) {
        String str = TAG;
        Log.i(str, "Sending media broadcast to " + Stream.of(list).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.sms.MessageSender$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MessageSender.lambda$sendMediaBroadcast$1((OutgoingSecureMediaMessage) obj);
            }
        }).toList());
        Preconditions.checkArgument(list.size() > 0, "No messages!");
        Preconditions.checkArgument(Stream.of(list).allMatch(new Predicate() { // from class: org.thoughtcrime.securesms.sms.MessageSender$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return MessageSender.lambda$sendMediaBroadcast$2((OutgoingSecureMediaMessage) obj);
            }
        }), "Messages can't have attachments! They should be pre-uploaded.");
        JobManager jobManager = ApplicationDependencies.getJobManager();
        AttachmentDatabase attachments = SignalDatabase.attachments();
        MmsDatabase mms = SignalDatabase.mms();
        ThreadDatabase threads = SignalDatabase.threads();
        List<AttachmentId> list2 = Stream.of(collection).map(new MessageSender$$ExternalSyntheticLambda2()).toList();
        List list3 = Stream.of(collection).map(new MessageSender$$ExternalSyntheticLambda3()).flatMap(new MessageSender$$ExternalSyntheticLambda4()).toList();
        ArrayList arrayList = new ArrayList(list.size());
        ArrayList arrayList2 = new ArrayList(list3);
        mms.beginTransaction();
        try {
            try {
                OutgoingSecureMediaMessage outgoingSecureMediaMessage = list.get(0);
                long orCreateThreadIdFor = threads.getOrCreateThreadIdFor(outgoingSecureMediaMessage.getRecipient(), outgoingSecureMediaMessage.getDistributionType());
                long insertMessageOutbox = mms.insertMessageOutbox(applyUniversalExpireTimerIfNecessary(context, outgoingSecureMediaMessage.getRecipient(), outgoingSecureMediaMessage, orCreateThreadIdFor), orCreateThreadIdFor, false, null);
                attachments.updateMessageId(list2, insertMessageOutbox, outgoingSecureMediaMessage.getStoryType().isStory());
                if (outgoingSecureMediaMessage.getStoryType() != StoryType.NONE) {
                    for (AttachmentId attachmentId : list2) {
                        attachments.updateAttachmentCaption(attachmentId, outgoingSecureMediaMessage.getBody());
                    }
                }
                arrayList.add(Long.valueOf(insertMessageOutbox));
                List list4 = Stream.of(list2).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.sms.MessageSender$$ExternalSyntheticLambda5
                    @Override // com.annimon.stream.function.Function
                    public final Object apply(Object obj) {
                        return AttachmentDatabase.this.getAttachment((AttachmentId) obj);
                    }
                }).toList();
                if (list.size() > 0) {
                    List<OutgoingSecureMediaMessage> subList = list.subList(1, list.size());
                    ArrayList arrayList3 = new ArrayList();
                    for (int i = 0; i < list2.size(); i++) {
                        arrayList3.add(new ArrayList(list.size()));
                    }
                    for (OutgoingSecureMediaMessage outgoingSecureMediaMessage2 : subList) {
                        long orCreateThreadIdFor2 = threads.getOrCreateThreadIdFor(outgoingSecureMediaMessage2.getRecipient(), outgoingSecureMediaMessage2.getDistributionType());
                        List list5 = list4;
                        long insertMessageOutbox2 = mms.insertMessageOutbox(applyUniversalExpireTimerIfNecessary(context, outgoingSecureMediaMessage2.getRecipient(), outgoingSecureMediaMessage2, orCreateThreadIdFor2), orCreateThreadIdFor2, false, null);
                        ArrayList arrayList4 = new ArrayList(list2.size());
                        for (int i2 = 0; i2 < list5.size(); i2++) {
                            AttachmentId attachmentId2 = attachments.insertAttachmentForPreUpload((Attachment) list5.get(i2)).getAttachmentId();
                            list5 = list5;
                            ((List) arrayList3.get(i2)).add(attachmentId2);
                            arrayList4.add(attachmentId2);
                        }
                        attachments.updateMessageId(arrayList4, insertMessageOutbox2, outgoingSecureMediaMessage2.getStoryType().isStory());
                        if (outgoingSecureMediaMessage.getStoryType() != StoryType.NONE) {
                            for (AttachmentId attachmentId3 : list2) {
                                attachments.updateAttachmentCaption(attachmentId3, outgoingSecureMediaMessage.getBody());
                            }
                        }
                        arrayList.add(Long.valueOf(insertMessageOutbox2));
                        list4 = list5;
                        arrayList3 = arrayList3;
                        threads = threads;
                    }
                    for (int i3 = 0; i3 < arrayList3.size(); i3++) {
                        AttachmentCopyJob attachmentCopyJob = new AttachmentCopyJob((AttachmentId) list2.get(i3), (List) arrayList3.get(i3));
                        jobManager.add(attachmentCopyJob, list3);
                        arrayList2.add(attachmentCopyJob.getId());
                    }
                }
                for (int i4 = 0; i4 < arrayList.size(); i4++) {
                    long longValue = ((Long) arrayList.get(i4)).longValue();
                    OutgoingSecureMediaMessage outgoingSecureMediaMessage3 = list.get(i4);
                    Recipient recipient = outgoingSecureMediaMessage3.getRecipient();
                    if (recipient.isDistributionList()) {
                        List<RecipientId> members = SignalDatabase.distributionLists().getMembers(recipient.requireDistributionListId());
                        DistributionId distributionId = SignalDatabase.distributionLists().getDistributionId(recipient.requireDistributionListId());
                        Objects.requireNonNull(distributionId);
                        SignalDatabase.storySends().insert(longValue, members, outgoingSecureMediaMessage3.getSentTimeMillis(), outgoingSecureMediaMessage3.getStoryType().isStoryWithReplies(), distributionId);
                    }
                }
                onMessageSent();
                mms.setTransactionSuccessful();
                mms.endTransaction();
                for (int i5 = 0; i5 < arrayList.size(); i5++) {
                    long longValue2 = ((Long) arrayList.get(i5)).longValue();
                    Recipient recipient2 = list.get(i5).getRecipient();
                    if (isLocalSelfSend(context, recipient2, false)) {
                        sendLocalMediaSelf(context, longValue2);
                    } else if (recipient2.isPushGroup()) {
                        jobManager.add(new PushGroupSendJob(longValue2, recipient2.getId(), (Set<RecipientId>) Collections.emptySet(), true), arrayList2, recipient2.getId().toQueueKey());
                    } else if (recipient2.isDistributionList()) {
                        jobManager.add(new PushDistributionListSendJob(longValue2, recipient2.getId(), true, (Set<RecipientId>) Collections.emptySet()), arrayList2, recipient2.getId().toQueueKey());
                    } else {
                        jobManager.add(new PushMediaSendJob(longValue2, recipient2, true), arrayList2, recipient2.getId().toQueueKey());
                    }
                }
            } catch (MmsException e) {
                Log.w(TAG, "Failed to send messages.", e);
                mms.endTransaction();
            }
        } catch (Throwable th) {
            mms.endTransaction();
            throw th;
        }
    }

    public static /* synthetic */ boolean lambda$sendMediaBroadcast$2(OutgoingSecureMediaMessage outgoingSecureMediaMessage) {
        return outgoingSecureMediaMessage.getAttachments().isEmpty();
    }

    public static PreUploadResult preUploadPushAttachment(Context context, Attachment attachment, Recipient recipient, Media media) {
        if (isLocalSelfSend(context, recipient, false)) {
            return null;
        }
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Pre-uploading attachment for ");
        sb.append(recipient != null ? recipient.getId() : "null");
        Log.i(str, sb.toString());
        try {
            DatabaseAttachment insertAttachmentForPreUpload = SignalDatabase.attachments().insertAttachmentForPreUpload(attachment);
            AttachmentCompressionJob fromAttachment = AttachmentCompressionJob.fromAttachment(insertAttachmentForPreUpload, false, -1);
            ResumableUploadSpecJob resumableUploadSpecJob = new ResumableUploadSpecJob();
            AttachmentUploadJob attachmentUploadJob = new AttachmentUploadJob(insertAttachmentForPreUpload.getAttachmentId());
            ApplicationDependencies.getJobManager().startChain(fromAttachment).then(resumableUploadSpecJob).then(attachmentUploadJob).enqueue();
            return new PreUploadResult(media, insertAttachmentForPreUpload.getAttachmentId(), Arrays.asList(fromAttachment.getId(), resumableUploadSpecJob.getId(), attachmentUploadJob.getId()));
        } catch (MmsException e) {
            Log.w(TAG, "preUploadPushAttachment() - Failed to upload!", e);
            return null;
        }
    }

    public static void sendNewReaction(Context context, MessageId messageId, String str) {
        ReactionRecord reactionRecord = new ReactionRecord(str, Recipient.self().getId(), System.currentTimeMillis(), System.currentTimeMillis());
        SignalDatabase.reactions().addReaction(messageId, reactionRecord);
        try {
            ApplicationDependencies.getJobManager().add(ReactionSendJob.create(context, messageId, reactionRecord, false));
            onMessageSent();
        } catch (NoSuchMessageException unused) {
            Log.w(TAG, "[sendNewReaction] Could not find message! Ignoring.");
        }
    }

    public static void sendReactionRemoval(Context context, MessageId messageId, ReactionRecord reactionRecord) {
        SignalDatabase.reactions().deleteReaction(messageId, reactionRecord.getAuthor());
        try {
            ApplicationDependencies.getJobManager().add(ReactionSendJob.create(context, messageId, reactionRecord, true));
            onMessageSent();
        } catch (NoSuchMessageException unused) {
            Log.w(TAG, "[sendReactionRemoval] Could not find message! Ignoring.");
        }
    }

    public static void sendRemoteDelete(long j, boolean z) {
        MessageDatabase mms = z ? SignalDatabase.mms() : SignalDatabase.sms();
        mms.markAsRemoteDelete(j);
        mms.markAsSending(j);
        try {
            RemoteDeleteSendJob.create(j, z).enqueue();
            onMessageSent();
        } catch (NoSuchMessageException unused) {
            Log.w(TAG, "[sendRemoteDelete] Could not find message! Ignoring.");
        }
    }

    public static void resendGroupMessage(Context context, MessageRecord messageRecord, Set<RecipientId> set) {
        if (messageRecord.isMms()) {
            sendGroupPush(context, messageRecord.getRecipient(), messageRecord.getId(), set, Collections.emptyList());
            onMessageSent();
            return;
        }
        throw new AssertionError("Not Group");
    }

    public static void resendDistributionList(Context context, MessageRecord messageRecord, Set<RecipientId> set) {
        if (messageRecord.isMms() || ((MmsMessageRecord) messageRecord).getStoryType().isStory()) {
            sendDistributionList(context, messageRecord.getRecipient(), messageRecord.getId(), set, Collections.emptyList());
            onMessageSent();
            return;
        }
        throw new AssertionError("Not a story");
    }

    public static void resend(Context context, MessageRecord messageRecord) {
        long id = messageRecord.getId();
        boolean isForcedSms = messageRecord.isForcedSms();
        boolean isKeyExchange = messageRecord.isKeyExchange();
        Recipient recipient = messageRecord.getRecipient();
        if (messageRecord.isMms()) {
            sendMediaMessage(context, recipient, isForcedSms, id, Collections.emptyList());
        } else {
            sendTextMessage(context, recipient, isForcedSms, isKeyExchange, id);
        }
        onMessageSent();
    }

    public static void onMessageSent() {
        EventBus.getDefault().postSticky(MessageSentEvent.INSTANCE);
    }

    private static OutgoingTextMessage applyUniversalExpireTimerIfNecessary(Context context, Recipient recipient, OutgoingTextMessage outgoingTextMessage, long j) {
        return (outgoingTextMessage.getExpiresIn() != 0 || !RecipientUtil.setAndSendUniversalExpireTimerIfNecessary(context, recipient, j)) ? outgoingTextMessage : outgoingTextMessage.withExpiry(TimeUnit.SECONDS.toMillis((long) SignalStore.settings().getUniversalExpireTimer()));
    }

    private static OutgoingMediaMessage applyUniversalExpireTimerIfNecessary(Context context, Recipient recipient, OutgoingMediaMessage outgoingMediaMessage, long j) {
        return (outgoingMediaMessage.isExpirationUpdate() || outgoingMediaMessage.getExpiresIn() != 0 || !RecipientUtil.setAndSendUniversalExpireTimerIfNecessary(context, recipient, j)) ? outgoingMediaMessage : outgoingMediaMessage.withExpiry(TimeUnit.SECONDS.toMillis((long) SignalStore.settings().getUniversalExpireTimer()));
    }

    private static void sendMediaMessage(Context context, Recipient recipient, boolean z, long j, Collection<String> collection) {
        if (isLocalSelfSend(context, recipient, z)) {
            sendLocalMediaSelf(context, j);
        } else if (recipient.isPushGroup()) {
            sendGroupPush(context, recipient, j, Collections.emptySet(), collection);
        } else if (recipient.isDistributionList()) {
            sendDistributionList(context, recipient, j, Collections.emptySet(), collection);
        } else if (z || !isPushMediaSend(context, recipient)) {
            sendMms(context, j);
        } else {
            sendMediaPush(context, recipient, j, collection);
        }
    }

    private static void sendTextMessage(Context context, Recipient recipient, boolean z, boolean z2, long j) {
        if (isLocalSelfSend(context, recipient, z)) {
            sendLocalTextSelf(context, j);
        } else if (z || !isPushTextSend(context, recipient, z2)) {
            sendSms(recipient, j);
        } else {
            sendTextPush(recipient, j);
        }
    }

    private static void sendTextPush(Recipient recipient, long j) {
        ApplicationDependencies.getJobManager().add(new PushTextSendJob(j, recipient));
    }

    private static void sendMediaPush(Context context, Recipient recipient, long j, Collection<String> collection) {
        JobManager jobManager = ApplicationDependencies.getJobManager();
        if (collection.size() > 0) {
            jobManager.add(new PushMediaSendJob(j, recipient, true), collection);
        } else {
            PushMediaSendJob.enqueue(context, jobManager, j, recipient);
        }
    }

    private static void sendGroupPush(Context context, Recipient recipient, long j, Set<RecipientId> set, Collection<String> collection) {
        JobManager jobManager = ApplicationDependencies.getJobManager();
        if (collection.size() > 0) {
            jobManager.add(new PushGroupSendJob(j, recipient.getId(), set, !collection.isEmpty()), collection, collection.isEmpty() ? null : recipient.getId().toQueueKey());
        } else {
            PushGroupSendJob.enqueue(context, jobManager, j, recipient.getId(), set);
        }
    }

    private static void sendDistributionList(Context context, Recipient recipient, long j, Set<RecipientId> set, Collection<String> collection) {
        JobManager jobManager = ApplicationDependencies.getJobManager();
        if (collection.size() > 0) {
            jobManager.add(new PushDistributionListSendJob(j, recipient.getId(), !collection.isEmpty(), set), collection, collection.isEmpty() ? null : recipient.getId().toQueueKey());
        } else {
            PushDistributionListSendJob.enqueue(context, jobManager, j, recipient.getId(), set);
        }
    }

    private static void sendSms(Recipient recipient, long j) {
        ApplicationDependencies.getJobManager().add(new SmsSendJob(j, recipient));
    }

    private static void sendMms(Context context, long j) {
        MmsSendJob.enqueue(context, ApplicationDependencies.getJobManager(), j);
    }

    private static boolean isPushTextSend(Context context, Recipient recipient, boolean z) {
        if (SignalStore.account().isRegistered() && !z) {
            return isPushDestination(context, recipient);
        }
        return false;
    }

    private static boolean isPushMediaSend(Context context, Recipient recipient) {
        if (SignalStore.account().isRegistered() && !recipient.isGroup()) {
            return isPushDestination(context, recipient);
        }
        return false;
    }

    private static boolean isPushDestination(Context context, Recipient recipient) {
        RecipientDatabase.RegisteredState registered = recipient.resolve().getRegistered();
        RecipientDatabase.RegisteredState registeredState = RecipientDatabase.RegisteredState.REGISTERED;
        if (registered == registeredState) {
            return true;
        }
        if (recipient.resolve().getRegistered() == RecipientDatabase.RegisteredState.NOT_REGISTERED) {
            return false;
        }
        try {
            if (ContactDiscovery.refresh(context, recipient, false) == registeredState) {
                return true;
            }
            return false;
        } catch (IOException e) {
            Log.w(TAG, e);
            return false;
        }
    }

    public static boolean isLocalSelfSend(Context context, Recipient recipient, boolean z) {
        return recipient != null && recipient.isSelf() && !z && SignalStore.account().isRegistered() && !TextSecurePreferences.isMultiDevice(context);
    }

    private static void sendLocalMediaSelf(Context context, long j) {
        try {
            ExpiringMessageManager expiringMessageManager = ApplicationDependencies.getExpiringMessageManager();
            MmsDatabase mms = SignalDatabase.mms();
            MmsSmsDatabase mmsSms = SignalDatabase.mmsSms();
            OutgoingMediaMessage outgoingMessage = mms.getOutgoingMessage(j);
            MessageDatabase.SyncMessageId syncMessageId = new MessageDatabase.SyncMessageId(Recipient.self().getId(), outgoingMessage.getSentTimeMillis());
            LinkedList linkedList = new LinkedList();
            linkedList.addAll(outgoingMessage.getAttachments());
            linkedList.addAll(Stream.of(outgoingMessage.getLinkPreviews()).map(new PushSendJob$$ExternalSyntheticLambda2()).filter(new UnidentifiedAccessUtil$$ExternalSyntheticLambda0()).map(new PushSendJob$$ExternalSyntheticLambda3()).toList());
            linkedList.addAll(Stream.of(outgoingMessage.getSharedContacts()).map(new PushSendJob$$ExternalSyntheticLambda4()).withoutNulls().map(new PushSendJob$$ExternalSyntheticLambda5()).withoutNulls().toList());
            List<? extends Job> list = Stream.of(linkedList).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.sms.MessageSender$$ExternalSyntheticLambda6
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return MessageSender.lambda$sendLocalMediaSelf$3((Attachment) obj);
                }
            }).toList();
            ApplicationDependencies.getJobManager().startChain(list).then(Stream.of(linkedList).map(new com.annimon.stream.function.Function(j) { // from class: org.thoughtcrime.securesms.sms.MessageSender$$ExternalSyntheticLambda7
                public final /* synthetic */ long f$0;

                {
                    this.f$0 = r1;
                }

                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return MessageSender.lambda$sendLocalMediaSelf$4(this.f$0, (Attachment) obj);
                }
            }).toList()).enqueue();
            mms.markAsSent(j, true);
            mms.markUnidentified(j, true);
            mmsSms.incrementDeliveryReceiptCount(syncMessageId, System.currentTimeMillis());
            mmsSms.incrementReadReceiptCount(syncMessageId, System.currentTimeMillis());
            mmsSms.incrementViewedReceiptCount(syncMessageId, System.currentTimeMillis());
            if (outgoingMessage.getExpiresIn() > 0 && !outgoingMessage.isExpirationUpdate()) {
                mms.markExpireStarted(j);
                expiringMessageManager.scheduleDeletion(j, true, outgoingMessage.getExpiresIn());
            }
        } catch (NoSuchMessageException | MmsException e) {
            Log.w(TAG, "Failed to update self-sent message.", e);
        }
    }

    public static /* synthetic */ AttachmentCompressionJob lambda$sendLocalMediaSelf$3(Attachment attachment) {
        return AttachmentCompressionJob.fromAttachment((DatabaseAttachment) attachment, false, -1);
    }

    public static /* synthetic */ AttachmentMarkUploadedJob lambda$sendLocalMediaSelf$4(long j, Attachment attachment) {
        return new AttachmentMarkUploadedJob(j, ((DatabaseAttachment) attachment).getAttachmentId());
    }

    private static void sendLocalTextSelf(Context context, long j) {
        try {
            ExpiringMessageManager expiringMessageManager = ApplicationDependencies.getExpiringMessageManager();
            SmsDatabase sms = SignalDatabase.sms();
            MmsSmsDatabase mmsSms = SignalDatabase.mmsSms();
            SmsMessageRecord smsMessage = sms.getSmsMessage(j);
            MessageDatabase.SyncMessageId syncMessageId = new MessageDatabase.SyncMessageId(Recipient.self().getId(), smsMessage.getDateSent());
            sms.markAsSent(j, true);
            sms.markUnidentified(j, true);
            mmsSms.incrementDeliveryReceiptCount(syncMessageId, System.currentTimeMillis());
            mmsSms.incrementReadReceiptCount(syncMessageId, System.currentTimeMillis());
            if (smsMessage.getExpiresIn() > 0) {
                sms.markExpireStarted(j);
                expiringMessageManager.scheduleDeletion(smsMessage.getId(), smsMessage.isMms(), smsMessage.getExpiresIn());
            }
        } catch (NoSuchMessageException e) {
            Log.w(TAG, "Failed to update self-sent message.", e);
        }
    }

    /* loaded from: classes4.dex */
    public static class PreUploadResult implements Parcelable {
        public static final Parcelable.Creator<PreUploadResult> CREATOR = new Parcelable.Creator<PreUploadResult>() { // from class: org.thoughtcrime.securesms.sms.MessageSender.PreUploadResult.1
            @Override // android.os.Parcelable.Creator
            public PreUploadResult createFromParcel(Parcel parcel) {
                return new PreUploadResult(parcel);
            }

            @Override // android.os.Parcelable.Creator
            public PreUploadResult[] newArray(int i) {
                return new PreUploadResult[i];
            }
        };
        private final AttachmentId attachmentId;
        private final Collection<String> jobIds;
        private final Media media;

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        PreUploadResult(Media media, AttachmentId attachmentId, Collection<String> collection) {
            this.media = media;
            this.attachmentId = attachmentId;
            this.jobIds = collection;
        }

        private PreUploadResult(Parcel parcel) {
            this.attachmentId = (AttachmentId) parcel.readParcelable(AttachmentId.class.getClassLoader());
            this.jobIds = ParcelUtil.readStringCollection(parcel);
            this.media = (Media) parcel.readParcelable(Media.class.getClassLoader());
        }

        public AttachmentId getAttachmentId() {
            return this.attachmentId;
        }

        public Collection<String> getJobIds() {
            return this.jobIds;
        }

        public Media getMedia() {
            return this.media;
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeParcelable(this.attachmentId, i);
            ParcelUtil.writeStringCollection(parcel, this.jobIds);
            parcel.writeParcelable(this.media, i);
        }
    }
}
