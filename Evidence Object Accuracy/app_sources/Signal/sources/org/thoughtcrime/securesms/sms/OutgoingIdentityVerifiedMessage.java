package org.thoughtcrime.securesms.sms;

import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class OutgoingIdentityVerifiedMessage extends OutgoingTextMessage {
    @Override // org.thoughtcrime.securesms.sms.OutgoingTextMessage
    public boolean isIdentityVerified() {
        return true;
    }

    public OutgoingIdentityVerifiedMessage(Recipient recipient) {
        this(recipient, "");
    }

    private OutgoingIdentityVerifiedMessage(Recipient recipient, String str) {
        super(recipient, str, -1);
    }

    @Override // org.thoughtcrime.securesms.sms.OutgoingTextMessage
    public OutgoingTextMessage withBody(String str) {
        return new OutgoingIdentityVerifiedMessage(getRecipient(), str);
    }
}
