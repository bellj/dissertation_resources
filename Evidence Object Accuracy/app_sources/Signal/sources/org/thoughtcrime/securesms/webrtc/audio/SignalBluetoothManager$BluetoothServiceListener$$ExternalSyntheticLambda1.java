package org.thoughtcrime.securesms.webrtc.audio;

import android.bluetooth.BluetoothProfile;
import org.thoughtcrime.securesms.webrtc.audio.SignalBluetoothManager;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes5.dex */
public final /* synthetic */ class SignalBluetoothManager$BluetoothServiceListener$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ SignalBluetoothManager f$0;
    public final /* synthetic */ BluetoothProfile f$1;

    public /* synthetic */ SignalBluetoothManager$BluetoothServiceListener$$ExternalSyntheticLambda1(SignalBluetoothManager signalBluetoothManager, BluetoothProfile bluetoothProfile) {
        this.f$0 = signalBluetoothManager;
        this.f$1 = bluetoothProfile;
    }

    @Override // java.lang.Runnable
    public final void run() {
        SignalBluetoothManager.BluetoothServiceListener.m3335onServiceConnected$lambda0(this.f$0, this.f$1);
    }
}
