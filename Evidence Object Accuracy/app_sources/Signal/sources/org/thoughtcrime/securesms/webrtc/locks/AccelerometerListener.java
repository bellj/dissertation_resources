package org.thoughtcrime.securesms.webrtc.locks;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import org.signal.core.util.logging.Log;

/* loaded from: classes5.dex */
public final class AccelerometerListener {
    private static final boolean DEBUG;
    private static final int HORIZONTAL_DEBOUNCE;
    private static final int ORIENTATION_CHANGED;
    public static final int ORIENTATION_HORIZONTAL;
    public static final int ORIENTATION_UNKNOWN;
    public static final int ORIENTATION_VERTICAL;
    private static final String TAG;
    private static final boolean VDEBUG;
    private static final double VERTICAL_ANGLE;
    private static final int VERTICAL_DEBOUNCE;
    Handler mHandler = new Handler(Looper.getMainLooper()) { // from class: org.thoughtcrime.securesms.webrtc.locks.AccelerometerListener.2
        @Override // android.os.Handler
        public void handleMessage(Message message) {
            String str;
            if (message.what == AccelerometerListener.ORIENTATION_CHANGED) {
                synchronized (this) {
                    AccelerometerListener accelerometerListener = AccelerometerListener.this;
                    accelerometerListener.mOrientation = accelerometerListener.mPendingOrientation;
                    StringBuilder sb = new StringBuilder();
                    sb.append("orientation: ");
                    if (AccelerometerListener.this.mOrientation == 2) {
                        str = "horizontal";
                    } else {
                        str = AccelerometerListener.this.mOrientation == 1 ? "vertical" : "unknown";
                    }
                    sb.append(str);
                    Log.d(AccelerometerListener.TAG, sb.toString());
                    AccelerometerListener.this.mListener.orientationChanged(AccelerometerListener.this.mOrientation);
                }
            }
        }
    };
    private OrientationListener mListener;
    private int mOrientation;
    private int mPendingOrientation;
    private Sensor mSensor;
    SensorEventListener mSensorListener = new SensorEventListener() { // from class: org.thoughtcrime.securesms.webrtc.locks.AccelerometerListener.1
        @Override // android.hardware.SensorEventListener
        public void onAccuracyChanged(Sensor sensor, int i) {
        }

        @Override // android.hardware.SensorEventListener
        public void onSensorChanged(SensorEvent sensorEvent) {
            AccelerometerListener accelerometerListener = AccelerometerListener.this;
            float[] fArr = sensorEvent.values;
            accelerometerListener.onSensorEvent((double) fArr[0], (double) fArr[1], (double) fArr[2]);
        }
    };
    private SensorManager mSensorManager;

    /* loaded from: classes5.dex */
    public interface OrientationListener {
        void orientationChanged(int i);
    }

    public AccelerometerListener(Context context, OrientationListener orientationListener) {
        this.mListener = orientationListener;
        SensorManager sensorManager = (SensorManager) context.getSystemService("sensor");
        this.mSensorManager = sensorManager;
        this.mSensor = sensorManager.getDefaultSensor(1);
    }

    public void enable(boolean z) {
        Log.d(TAG, "enable(" + z + ")");
        synchronized (this) {
            if (z) {
                this.mOrientation = 0;
                this.mPendingOrientation = 0;
                this.mSensorManager.registerListener(this.mSensorListener, this.mSensor, 3);
            } else {
                this.mSensorManager.unregisterListener(this.mSensorListener);
                this.mHandler.removeMessages(ORIENTATION_CHANGED);
            }
        }
    }

    private void setOrientation(int i) {
        synchronized (this) {
            if (this.mPendingOrientation != i) {
                this.mHandler.removeMessages(ORIENTATION_CHANGED);
                if (this.mOrientation != i) {
                    this.mPendingOrientation = i;
                    this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(ORIENTATION_CHANGED), (long) (i == 1 ? 100 : HORIZONTAL_DEBOUNCE));
                } else {
                    this.mPendingOrientation = 0;
                }
            }
        }
    }

    public void onSensorEvent(double d, double d2, double d3) {
        if (d != 0.0d && d2 != 0.0d && d3 != 0.0d) {
            setOrientation((Math.atan2(Math.sqrt((d * d) + (d2 * d2)), d3) * 180.0d) / 3.141592653589793d > VERTICAL_ANGLE ? 1 : 2);
        }
    }
}
