package org.thoughtcrime.securesms.webrtc;

/* loaded from: classes5.dex */
public enum CallBandwidthMode {
    LOW_ALWAYS(0),
    HIGH_ON_WIFI(1),
    HIGH_ALWAYS(2);
    
    private final int code;

    CallBandwidthMode(int i) {
        this.code = i;
    }

    public int getCode() {
        return this.code;
    }

    public static CallBandwidthMode fromCode(int i) {
        if (i == 1) {
            return HIGH_ON_WIFI;
        }
        if (i != 2) {
            return LOW_ALWAYS;
        }
        return HIGH_ALWAYS;
    }
}
