package org.thoughtcrime.securesms.webrtc.locks;

import android.os.Build;
import android.os.PowerManager;
import j$.util.Optional;
import java.lang.reflect.Method;
import org.signal.core.util.logging.Log;

/* loaded from: classes5.dex */
public class ProximityLock {
    private static final int PROXIMITY_SCREEN_OFF_WAKE_LOCK;
    private static final String TAG = Log.tag(ProximityLock.class);
    private static final int WAIT_FOR_PROXIMITY_NEGATIVE;
    private final Optional<PowerManager.WakeLock> proximityLock;
    private final Method wakelockParameterizedRelease = getWakelockParamterizedReleaseMethod();

    public ProximityLock(PowerManager powerManager) {
        this.proximityLock = getProximityLock(powerManager);
    }

    private Optional<PowerManager.WakeLock> getProximityLock(PowerManager powerManager) {
        if (Build.VERSION.SDK_INT < 21) {
            try {
                return Optional.ofNullable(powerManager.newWakeLock(32, "signal:incall"));
            } catch (Throwable th) {
                Log.e(TAG, "Failed to create proximity lock", th);
                return Optional.empty();
            }
        } else if (powerManager.isWakeLockLevelSupported(32)) {
            return Optional.ofNullable(powerManager.newWakeLock(32, "signal:proximity"));
        } else {
            return Optional.empty();
        }
    }

    public void acquire() {
        if (this.proximityLock.isPresent() && !this.proximityLock.get().isHeld()) {
            this.proximityLock.get().acquire();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0051  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void release() {
        /*
            r6 = this;
            j$.util.Optional<android.os.PowerManager$WakeLock> r0 = r6.proximityLock
            boolean r0 = r0.isPresent()
            if (r0 == 0) goto L_0x007e
            j$.util.Optional<android.os.PowerManager$WakeLock> r0 = r6.proximityLock
            java.lang.Object r0 = r0.get()
            android.os.PowerManager$WakeLock r0 = (android.os.PowerManager.WakeLock) r0
            boolean r0 = r0.isHeld()
            if (r0 != 0) goto L_0x0017
            goto L_0x007e
        L_0x0017:
            int r0 = android.os.Build.VERSION.SDK_INT
            r1 = 21
            r2 = 1
            if (r0 < r1) goto L_0x002a
            j$.util.Optional<android.os.PowerManager$WakeLock> r0 = r6.proximityLock
            java.lang.Object r0 = r0.get()
            android.os.PowerManager$WakeLock r0 = (android.os.PowerManager.WakeLock) r0
            r0.release(r2)
            goto L_0x005c
        L_0x002a:
            java.lang.reflect.Method r0 = r6.wakelockParameterizedRelease
            r1 = 0
            if (r0 == 0) goto L_0x004e
            j$.util.Optional<android.os.PowerManager$WakeLock> r3 = r6.proximityLock     // Catch: IllegalAccessException -> 0x0048, InvocationTargetException -> 0x0041
            java.lang.Object r3 = r3.get()     // Catch: IllegalAccessException -> 0x0048, InvocationTargetException -> 0x0041
            java.lang.Object[] r4 = new java.lang.Object[r2]     // Catch: IllegalAccessException -> 0x0048, InvocationTargetException -> 0x0041
            java.lang.Integer r5 = java.lang.Integer.valueOf(r2)     // Catch: IllegalAccessException -> 0x0048, InvocationTargetException -> 0x0041
            r4[r1] = r5     // Catch: IllegalAccessException -> 0x0048, InvocationTargetException -> 0x0041
            r0.invoke(r3, r4)     // Catch: IllegalAccessException -> 0x0048, InvocationTargetException -> 0x0041
            goto L_0x004f
        L_0x0041:
            r0 = move-exception
            java.lang.String r2 = org.thoughtcrime.securesms.webrtc.locks.ProximityLock.TAG
            org.signal.core.util.logging.Log.w(r2, r0)
            goto L_0x004e
        L_0x0048:
            r0 = move-exception
            java.lang.String r2 = org.thoughtcrime.securesms.webrtc.locks.ProximityLock.TAG
            org.signal.core.util.logging.Log.w(r2, r0)
        L_0x004e:
            r2 = 0
        L_0x004f:
            if (r2 != 0) goto L_0x005c
            j$.util.Optional<android.os.PowerManager$WakeLock> r0 = r6.proximityLock
            java.lang.Object r0 = r0.get()
            android.os.PowerManager$WakeLock r0 = (android.os.PowerManager.WakeLock) r0
            r0.release()
        L_0x005c:
            java.lang.String r0 = org.thoughtcrime.securesms.webrtc.locks.ProximityLock.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Released proximity lock:"
            r1.append(r2)
            j$.util.Optional<android.os.PowerManager$WakeLock> r2 = r6.proximityLock
            java.lang.Object r2 = r2.get()
            android.os.PowerManager$WakeLock r2 = (android.os.PowerManager.WakeLock) r2
            boolean r2 = r2.isHeld()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            org.signal.core.util.logging.Log.d(r0, r1)
        L_0x007e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.webrtc.locks.ProximityLock.release():void");
    }

    private static Method getWakelockParamterizedReleaseMethod() {
        try {
            return PowerManager.WakeLock.class.getDeclaredMethod("release", Integer.TYPE);
        } catch (NoSuchMethodException unused) {
            Log.d(TAG, "Parameterized WakeLock release not available on this device.");
            return null;
        }
    }
}
