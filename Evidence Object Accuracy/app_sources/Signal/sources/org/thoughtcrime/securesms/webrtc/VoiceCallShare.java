package org.thoughtcrime.securesms.webrtc;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.WebRtcCallActivity;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes5.dex */
public class VoiceCallShare extends Activity {
    private static final String TAG = Log.tag(VoiceCallShare.class);

    @Override // android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getIntent().getData() != null && "content".equals(getIntent().getData().getScheme())) {
            Cursor cursor = null;
            try {
                cursor = getContentResolver().query(getIntent().getData(), null, null, null, null);
                if (cursor != null && cursor.moveToNext()) {
                    String string = cursor.getString(cursor.getColumnIndexOrThrow("data1"));
                    SimpleTask.run(new SimpleTask.BackgroundTask(string) { // from class: org.thoughtcrime.securesms.webrtc.VoiceCallShare$$ExternalSyntheticLambda0
                        public final /* synthetic */ String f$1;

                        {
                            this.f$1 = r2;
                        }

                        @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
                        public final Object run() {
                            return VoiceCallShare.m3320$r8$lambda$UGdsIdSWMtItmWV7NtSOg2otkA(VoiceCallShare.this, this.f$1);
                        }
                    }, new SimpleTask.ForegroundTask(string) { // from class: org.thoughtcrime.securesms.webrtc.VoiceCallShare$$ExternalSyntheticLambda1
                        public final /* synthetic */ String f$1;

                        {
                            this.f$1 = r2;
                        }

                        @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
                        public final void run(Object obj) {
                            VoiceCallShare.m3321$r8$lambda$VpQigGv8Ug5slKNvN_boUALWUE(VoiceCallShare.this, this.f$1, (Recipient) obj);
                        }
                    });
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        finish();
    }

    public /* synthetic */ Recipient lambda$onCreate$0(String str) {
        return Recipient.external(this, str);
    }

    public /* synthetic */ void lambda$onCreate$1(String str, Recipient recipient) {
        if (!TextUtils.isEmpty(str)) {
            ApplicationDependencies.getSignalCallManager().startOutgoingAudioCall(recipient);
            Intent intent = new Intent(this, WebRtcCallActivity.class);
            intent.setFlags(SQLiteDatabase.CREATE_IF_NECESSARY);
            startActivity(intent);
        }
    }
}
