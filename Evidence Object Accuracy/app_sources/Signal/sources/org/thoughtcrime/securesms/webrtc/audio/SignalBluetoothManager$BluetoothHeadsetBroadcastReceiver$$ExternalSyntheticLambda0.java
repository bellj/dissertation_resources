package org.thoughtcrime.securesms.webrtc.audio;

import org.thoughtcrime.securesms.webrtc.audio.SignalBluetoothManager;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes5.dex */
public final /* synthetic */ class SignalBluetoothManager$BluetoothHeadsetBroadcastReceiver$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ SignalBluetoothManager f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ SignalBluetoothManager$BluetoothHeadsetBroadcastReceiver$$ExternalSyntheticLambda0(SignalBluetoothManager signalBluetoothManager, int i) {
        this.f$0 = signalBluetoothManager;
        this.f$1 = i;
    }

    @Override // java.lang.Runnable
    public final void run() {
        SignalBluetoothManager.BluetoothHeadsetBroadcastReceiver.m3331onReceive$lambda0(this.f$0, this.f$1);
    }
}
