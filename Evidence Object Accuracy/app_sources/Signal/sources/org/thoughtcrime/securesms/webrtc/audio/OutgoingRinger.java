package org.thoughtcrime.securesms.webrtc.audio;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;

/* loaded from: classes5.dex */
public class OutgoingRinger {
    private static final String TAG = Log.tag(OutgoingRinger.class);
    private final Context context;
    private MediaPlayer mediaPlayer;

    /* loaded from: classes5.dex */
    public enum Type {
        RINGING,
        BUSY
    }

    public OutgoingRinger(Context context) {
        this.context = context;
    }

    public void start(Type type) {
        int i;
        if (type == Type.RINGING) {
            i = R.raw.redphone_outring;
        } else if (type == Type.BUSY) {
            i = R.raw.redphone_busy;
        } else {
            throw new IllegalArgumentException("Not a valid sound type");
        }
        MediaPlayer mediaPlayer = this.mediaPlayer;
        if (mediaPlayer != null) {
            mediaPlayer.release();
        }
        MediaPlayer mediaPlayer2 = new MediaPlayer();
        this.mediaPlayer = mediaPlayer2;
        if (Build.VERSION.SDK_INT <= 21) {
            mediaPlayer2.setAudioStreamType(0);
        } else {
            mediaPlayer2.setAudioAttributes(new AudioAttributes.Builder().setContentType(1).setUsage(2).build());
        }
        this.mediaPlayer.setLooping(true);
        String packageName = this.context.getPackageName();
        try {
            this.mediaPlayer.setDataSource(this.context, Uri.parse("android.resource://" + packageName + "/" + i));
            this.mediaPlayer.prepare();
            this.mediaPlayer.start();
        } catch (IOException | IllegalArgumentException | IllegalStateException | SecurityException e) {
            Log.w(TAG, e);
        }
    }

    public void stop() {
        MediaPlayer mediaPlayer = this.mediaPlayer;
        if (mediaPlayer != null) {
            mediaPlayer.release();
            this.mediaPlayer = null;
        }
    }
}
