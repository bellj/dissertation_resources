package org.thoughtcrime.securesms.webrtc.audio;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.ParcelUtil;
import org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager;

/* compiled from: AudioManagerCommand.kt */
@Metadata(d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\t\n\u000b\f\r\u000e\u000f\u0010\u0011\u0012B\u0007\b\u0004¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0004H\u0016\u0001\b\u0013\u0014\u0015\u0016\u0017\u0018\u0019\u001a¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand;", "Landroid/os/Parcelable;", "()V", "describeContents", "", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Initialize", "ParcelCheat", "SetDefaultDevice", "SetUserDevice", "SilenceIncomingRinger", "Start", "StartIncomingRinger", "StartOutgoingRinger", "Stop", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$Initialize;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$StartIncomingRinger;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$StartOutgoingRinger;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$SilenceIncomingRinger;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$Start;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$Stop;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$SetUserDevice;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$SetDefaultDevice;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes5.dex */
public abstract class AudioManagerCommand implements Parcelable {
    public /* synthetic */ AudioManagerCommand(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkNotNullParameter(parcel, "parcel");
    }

    private AudioManagerCommand() {
    }

    /* compiled from: AudioManagerCommand.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005¢\u0006\u0002\u0010\u0002¨\u0006\u0004"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$Initialize;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand;", "()V", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public static final class Initialize extends AudioManagerCommand {
        public static final Parcelable.Creator<Initialize> CREATOR = new ParcelCheat(AudioManagerCommand$Initialize$Companion$CREATOR$1.INSTANCE);
        public static final Companion Companion = new Companion(null);

        /* compiled from: AudioManagerCommand.kt */
        @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$Initialize$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$Initialize;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes5.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }
        }

        public Initialize() {
            super(null);
        }
    }

    /* compiled from: AudioManagerCommand.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u0000 \u00112\u00020\u0001:\u0001\u0011B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$StartIncomingRinger;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand;", "ringtoneUri", "Landroid/net/Uri;", "vibrate", "", "(Landroid/net/Uri;Z)V", "getRingtoneUri", "()Landroid/net/Uri;", "getVibrate", "()Z", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public static final class StartIncomingRinger extends AudioManagerCommand {
        public static final Parcelable.Creator<StartIncomingRinger> CREATOR = new ParcelCheat(AudioManagerCommand$StartIncomingRinger$Companion$CREATOR$1.INSTANCE);
        public static final Companion Companion = new Companion(null);
        private final Uri ringtoneUri;
        private final boolean vibrate;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public StartIncomingRinger(Uri uri, boolean z) {
            super(null);
            Intrinsics.checkNotNullParameter(uri, "ringtoneUri");
            this.ringtoneUri = uri;
            this.vibrate = z;
        }

        public final Uri getRingtoneUri() {
            return this.ringtoneUri;
        }

        public final boolean getVibrate() {
            return this.vibrate;
        }

        @Override // org.thoughtcrime.securesms.webrtc.audio.AudioManagerCommand, android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkNotNullParameter(parcel, "parcel");
            parcel.writeParcelable(this.ringtoneUri, i);
            ParcelUtil.writeBoolean(parcel, this.vibrate);
        }

        /* compiled from: AudioManagerCommand.kt */
        @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$StartIncomingRinger$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$StartIncomingRinger;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes5.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }
        }
    }

    /* compiled from: AudioManagerCommand.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005¢\u0006\u0002\u0010\u0002¨\u0006\u0004"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$StartOutgoingRinger;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand;", "()V", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public static final class StartOutgoingRinger extends AudioManagerCommand {
        public static final Parcelable.Creator<StartOutgoingRinger> CREATOR = new ParcelCheat(AudioManagerCommand$StartOutgoingRinger$Companion$CREATOR$1.INSTANCE);
        public static final Companion Companion = new Companion(null);

        /* compiled from: AudioManagerCommand.kt */
        @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$StartOutgoingRinger$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$StartOutgoingRinger;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes5.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }
        }

        public StartOutgoingRinger() {
            super(null);
        }
    }

    /* compiled from: AudioManagerCommand.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005¢\u0006\u0002\u0010\u0002¨\u0006\u0004"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$SilenceIncomingRinger;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand;", "()V", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public static final class SilenceIncomingRinger extends AudioManagerCommand {
        public static final Parcelable.Creator<SilenceIncomingRinger> CREATOR = new ParcelCheat(AudioManagerCommand$SilenceIncomingRinger$Companion$CREATOR$1.INSTANCE);
        public static final Companion Companion = new Companion(null);

        /* compiled from: AudioManagerCommand.kt */
        @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$SilenceIncomingRinger$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$SilenceIncomingRinger;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes5.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }
        }

        public SilenceIncomingRinger() {
            super(null);
        }
    }

    /* compiled from: AudioManagerCommand.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005¢\u0006\u0002\u0010\u0002¨\u0006\u0004"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$Start;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand;", "()V", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public static final class Start extends AudioManagerCommand {
        public static final Parcelable.Creator<Start> CREATOR = new ParcelCheat(AudioManagerCommand$Start$Companion$CREATOR$1.INSTANCE);
        public static final Companion Companion = new Companion(null);

        /* compiled from: AudioManagerCommand.kt */
        @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$Start$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$Start;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes5.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }
        }

        public Start() {
            super(null);
        }
    }

    /* compiled from: AudioManagerCommand.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u0000 \r2\u00020\u0001:\u0001\rB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0018\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$Stop;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand;", "playDisconnect", "", "(Z)V", "getPlayDisconnect", "()Z", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public static final class Stop extends AudioManagerCommand {
        public static final Parcelable.Creator<Stop> CREATOR = new ParcelCheat(AudioManagerCommand$Stop$Companion$CREATOR$1.INSTANCE);
        public static final Companion Companion = new Companion(null);
        private final boolean playDisconnect;

        public Stop(boolean z) {
            super(null);
            this.playDisconnect = z;
        }

        public final boolean getPlayDisconnect() {
            return this.playDisconnect;
        }

        @Override // org.thoughtcrime.securesms.webrtc.audio.AudioManagerCommand, android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkNotNullParameter(parcel, "parcel");
            ParcelUtil.writeBoolean(parcel, this.playDisconnect);
        }

        /* compiled from: AudioManagerCommand.kt */
        @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$Stop$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$Stop;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes5.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }
        }
    }

    /* compiled from: AudioManagerCommand.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u0000 \u00112\u00020\u0001:\u0001\u0011B\u0017\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$SetUserDevice;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "device", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$AudioDevice;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$AudioDevice;)V", "getDevice", "()Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$AudioDevice;", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public static final class SetUserDevice extends AudioManagerCommand {
        public static final Parcelable.Creator<SetUserDevice> CREATOR = new ParcelCheat(AudioManagerCommand$SetUserDevice$Companion$CREATOR$1.INSTANCE);
        public static final Companion Companion = new Companion(null);
        private final SignalAudioManager.AudioDevice device;
        private final RecipientId recipientId;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public SetUserDevice(RecipientId recipientId, SignalAudioManager.AudioDevice audioDevice) {
            super(null);
            Intrinsics.checkNotNullParameter(audioDevice, "device");
            this.recipientId = recipientId;
            this.device = audioDevice;
        }

        public final SignalAudioManager.AudioDevice getDevice() {
            return this.device;
        }

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }

        @Override // org.thoughtcrime.securesms.webrtc.audio.AudioManagerCommand, android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkNotNullParameter(parcel, "parcel");
            parcel.writeParcelable(this.recipientId, i);
            parcel.writeSerializable(this.device);
        }

        /* compiled from: AudioManagerCommand.kt */
        @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$SetUserDevice$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$SetUserDevice;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes5.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }
        }
    }

    /* compiled from: AudioManagerCommand.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B\u001f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$SetDefaultDevice;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "device", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$AudioDevice;", "clearUserEarpieceSelection", "", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$AudioDevice;Z)V", "getClearUserEarpieceSelection", "()Z", "getDevice", "()Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$AudioDevice;", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public static final class SetDefaultDevice extends AudioManagerCommand {
        public static final Parcelable.Creator<SetDefaultDevice> CREATOR = new ParcelCheat(AudioManagerCommand$SetDefaultDevice$Companion$CREATOR$1.INSTANCE);
        public static final Companion Companion = new Companion(null);
        private final boolean clearUserEarpieceSelection;
        private final SignalAudioManager.AudioDevice device;
        private final RecipientId recipientId;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public SetDefaultDevice(RecipientId recipientId, SignalAudioManager.AudioDevice audioDevice, boolean z) {
            super(null);
            Intrinsics.checkNotNullParameter(audioDevice, "device");
            this.recipientId = recipientId;
            this.device = audioDevice;
            this.clearUserEarpieceSelection = z;
        }

        public final boolean getClearUserEarpieceSelection() {
            return this.clearUserEarpieceSelection;
        }

        public final SignalAudioManager.AudioDevice getDevice() {
            return this.device;
        }

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }

        @Override // org.thoughtcrime.securesms.webrtc.audio.AudioManagerCommand, android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkNotNullParameter(parcel, "parcel");
            parcel.writeParcelable(this.recipientId, i);
            parcel.writeSerializable(this.device);
            ParcelUtil.writeBoolean(parcel, this.clearUserEarpieceSelection);
        }

        /* compiled from: AudioManagerCommand.kt */
        @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$SetDefaultDevice$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$SetDefaultDevice;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes5.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }
        }
    }

    /* compiled from: AudioManagerCommand.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B\u0019\u0012\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00028\u00000\u0004¢\u0006\u0002\u0010\u0006J\u0015\u0010\u0007\u001a\u00028\u00002\u0006\u0010\b\u001a\u00020\u0005H\u0016¢\u0006\u0002\u0010\tJ\u001d\u0010\n\u001a\n\u0012\u0006\u0012\u0004\u0018\u00018\u00000\u000b2\u0006\u0010\f\u001a\u00020\rH\u0016¢\u0006\u0002\u0010\u000eR\u001a\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00028\u00000\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$ParcelCheat;", "T", "Landroid/os/Parcelable$Creator;", "createFrom", "Lkotlin/Function1;", "Landroid/os/Parcel;", "(Lkotlin/jvm/functions/Function1;)V", "createFromParcel", "parcel", "(Landroid/os/Parcel;)Ljava/lang/Object;", "newArray", "", MediaPreviewActivity.SIZE_EXTRA, "", "(I)[Ljava/lang/Object;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public static final class ParcelCheat<T> implements Parcelable.Creator<T> {
        private final Function1<Parcel, T> createFrom;

        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: kotlin.jvm.functions.Function1<? super android.os.Parcel, ? extends T> */
        /* JADX WARN: Multi-variable type inference failed */
        public ParcelCheat(Function1<? super Parcel, ? extends T> function1) {
            Intrinsics.checkNotNullParameter(function1, "createFrom");
            this.createFrom = function1;
        }

        @Override // android.os.Parcelable.Creator
        public T createFromParcel(Parcel parcel) {
            Intrinsics.checkNotNullParameter(parcel, "parcel");
            return this.createFrom.invoke(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public T[] newArray(int i) {
            throw new UnsupportedOperationException();
        }
    }
}
