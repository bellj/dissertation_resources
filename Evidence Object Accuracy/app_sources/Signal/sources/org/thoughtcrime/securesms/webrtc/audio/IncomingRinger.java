package org.thoughtcrime.securesms.webrtc.audio;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.RingtoneUtil;
import org.thoughtcrime.securesms.util.ServiceUtil;

/* loaded from: classes5.dex */
public class IncomingRinger {
    private static final String TAG = Log.tag(IncomingRinger.class);
    private static final long[] VIBRATE_PATTERN = {0, 1000, 1000};
    private final Context context;
    private MediaPlayer player;
    private final Vibrator vibrator;

    public IncomingRinger(Context context) {
        this.context = context.getApplicationContext();
        this.vibrator = (Vibrator) context.getSystemService("vibrator");
    }

    public void start(Uri uri, boolean z) {
        AudioManager audioManager = ServiceUtil.getAudioManager(this.context);
        MediaPlayer mediaPlayer = this.player;
        if (mediaPlayer != null) {
            mediaPlayer.release();
        }
        if (uri != null) {
            this.player = createPlayer(uri);
        }
        int ringerMode = audioManager.getRingerMode();
        if (shouldVibrate(this.context, this.player, ringerMode, z)) {
            Log.i(TAG, "Starting vibration");
            this.vibrator.vibrate(VIBRATE_PATTERN, 1);
        } else {
            Log.i(TAG, "Skipping vibration");
        }
        MediaPlayer mediaPlayer2 = this.player;
        if (mediaPlayer2 == null || ringerMode != 2) {
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Not ringing, player: ");
            sb.append(this.player != null ? "available" : "null");
            sb.append(" modeInt: ");
            sb.append(ringerMode);
            sb.append(" mode: ");
            sb.append(ringerMode == 0 ? "silent" : "vibrate only");
            Log.w(str, sb.toString());
            return;
        }
        try {
            if (!mediaPlayer2.isPlaying()) {
                this.player.prepare();
                this.player.start();
                Log.i(TAG, "Playing ringtone now...");
                return;
            }
            Log.w(TAG, "Ringtone is already playing, declining to restart.");
            return;
        } catch (IOException | IllegalStateException e) {
            Log.w(TAG, e);
            this.player = null;
        }
        Log.w(TAG, e);
        this.player = null;
    }

    public void stop() {
        if (this.player != null) {
            Log.i(TAG, "Stopping ringer");
            this.player.release();
            this.player = null;
        }
        Log.i(TAG, "Cancelling vibrator");
        this.vibrator.cancel();
    }

    private boolean shouldVibrate(Context context, MediaPlayer mediaPlayer, int i, boolean z) {
        if (mediaPlayer == null) {
            return true;
        }
        Vibrator vibrator = (Vibrator) context.getSystemService("vibrator");
        if (vibrator == null || !vibrator.hasVibrator()) {
            return false;
        }
        if (z) {
            if (i != 0) {
                return true;
            }
            return false;
        } else if (i == 1) {
            return true;
        } else {
            return false;
        }
    }

    private MediaPlayer createPlayer(Uri uri) {
        try {
            MediaPlayer safeCreatePlayer = safeCreatePlayer(uri);
            if (safeCreatePlayer == null) {
                Log.w(TAG, "Failed to create player for incoming call ringer due to custom rom most likely");
                return null;
            }
            safeCreatePlayer.setOnErrorListener(new MediaPlayerErrorListener());
            safeCreatePlayer.setLooping(true);
            if (Build.VERSION.SDK_INT <= 21) {
                safeCreatePlayer.setAudioStreamType(2);
            } else {
                safeCreatePlayer.setAudioAttributes(new AudioAttributes.Builder().setContentType(4).setUsage(6).build());
            }
            return safeCreatePlayer;
        } catch (IOException e) {
            Log.e(TAG, "Failed to create player for incoming call ringer", e);
            return null;
        }
    }

    private MediaPlayer safeCreatePlayer(Uri uri) throws IOException {
        try {
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(this.context, uri);
            return mediaPlayer;
        } catch (IOException | SecurityException e) {
            Log.w(TAG, "Failed to create player with ringtone the normal way", e);
            try {
                Uri actualDefaultRingtoneUri = RingtoneUtil.getActualDefaultRingtoneUri(this.context);
                if (actualDefaultRingtoneUri == null) {
                    return null;
                }
                MediaPlayer mediaPlayer2 = new MediaPlayer();
                mediaPlayer2.setDataSource(this.context, actualDefaultRingtoneUri);
                return mediaPlayer2;
            } catch (SecurityException e2) {
                Log.w(TAG, "Failed to set default ringtone with fallback approach", e2);
                return null;
            }
        }
    }

    /* loaded from: classes5.dex */
    public class MediaPlayerErrorListener implements MediaPlayer.OnErrorListener {
        private MediaPlayerErrorListener() {
            IncomingRinger.this = r1;
        }

        @Override // android.media.MediaPlayer.OnErrorListener
        public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
            String str = IncomingRinger.TAG;
            Log.w(str, "onError(" + mediaPlayer + ", " + i + ", " + i2);
            IncomingRinger.this.player = null;
            return false;
        }
    }
}
