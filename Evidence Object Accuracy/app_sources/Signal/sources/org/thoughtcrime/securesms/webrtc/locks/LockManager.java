package org.thoughtcrime.securesms.webrtc.locks;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.PowerManager;
import android.provider.Settings;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.webrtc.locks.AccelerometerListener;

/* loaded from: classes5.dex */
public class LockManager {
    private static final String TAG = Log.tag(LockManager.class);
    private final AccelerometerListener accelerometerListener;
    private final PowerManager.WakeLock fullLock;
    private int orientation = 0;
    private final PowerManager.WakeLock partialLock;
    private boolean proximityDisabled = false;
    private final ProximityLock proximityLock;
    private final WifiManager.WifiLock wifiLock;
    private final boolean wifiLockEnforced;

    /* loaded from: classes5.dex */
    public enum LockState {
        FULL,
        PARTIAL,
        SLEEP,
        PROXIMITY
    }

    /* loaded from: classes5.dex */
    public enum PhoneState {
        IDLE,
        PROCESSING,
        INTERACTIVE,
        IN_CALL,
        IN_HANDS_FREE_CALL,
        IN_VIDEO
    }

    public LockManager(Context context) {
        PowerManager powerManager = (PowerManager) context.getSystemService("power");
        PowerManager.WakeLock newWakeLock = powerManager.newWakeLock(268435466, "signal:full");
        this.fullLock = newWakeLock;
        PowerManager.WakeLock newWakeLock2 = powerManager.newWakeLock(1, "signal:partial");
        this.partialLock = newWakeLock2;
        this.proximityLock = new ProximityLock(powerManager);
        WifiManager.WifiLock createWifiLock = ((WifiManager) context.getSystemService("wifi")).createWifiLock(3, "signal:wifi");
        this.wifiLock = createWifiLock;
        newWakeLock.setReferenceCounted(false);
        newWakeLock2.setReferenceCounted(false);
        createWifiLock.setReferenceCounted(false);
        this.accelerometerListener = new AccelerometerListener(context, new AccelerometerListener.OrientationListener() { // from class: org.thoughtcrime.securesms.webrtc.locks.LockManager.1
            @Override // org.thoughtcrime.securesms.webrtc.locks.AccelerometerListener.OrientationListener
            public void orientationChanged(int i) {
                LockManager.this.orientation = i;
                String str = LockManager.TAG;
                Log.d(str, "Orentation Update: " + i);
                LockManager.this.updateInCallLockState();
            }
        });
        this.wifiLockEnforced = isWifiPowerActiveModeEnabled(context);
    }

    private boolean isWifiPowerActiveModeEnabled(Context context) {
        int i = Settings.Secure.getInt(context.getContentResolver(), "wifi_pwr_active_mode", -1);
        String str = TAG;
        Log.d(str, "Wifi Activity Policy: " + i);
        return i != 0;
    }

    public void updateInCallLockState() {
        if (this.orientation == 2 || !this.wifiLockEnforced || this.proximityDisabled) {
            setLockState(LockState.FULL);
        } else {
            setLockState(LockState.PROXIMITY);
        }
    }

    public void updatePhoneState(PhoneState phoneState) {
        switch (AnonymousClass2.$SwitchMap$org$thoughtcrime$securesms$webrtc$locks$LockManager$PhoneState[phoneState.ordinal()]) {
            case 1:
                setLockState(LockState.SLEEP);
                this.accelerometerListener.enable(false);
                return;
            case 2:
                setLockState(LockState.PARTIAL);
                this.accelerometerListener.enable(false);
                return;
            case 3:
                setLockState(LockState.FULL);
                this.accelerometerListener.enable(false);
                return;
            case 4:
                setLockState(LockState.PARTIAL);
                this.proximityDisabled = true;
                this.accelerometerListener.enable(false);
                return;
            case 5:
                this.proximityDisabled = true;
                this.accelerometerListener.enable(false);
                updateInCallLockState();
                return;
            case 6:
                this.proximityDisabled = false;
                this.accelerometerListener.enable(true);
                updateInCallLockState();
                return;
            default:
                return;
        }
    }

    /* renamed from: org.thoughtcrime.securesms.webrtc.locks.LockManager$2 */
    /* loaded from: classes5.dex */
    public static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$webrtc$locks$LockManager$LockState;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$webrtc$locks$LockManager$PhoneState;

        static {
            int[] iArr = new int[LockState.values().length];
            $SwitchMap$org$thoughtcrime$securesms$webrtc$locks$LockManager$LockState = iArr;
            try {
                iArr[LockState.FULL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$webrtc$locks$LockManager$LockState[LockState.PARTIAL.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$webrtc$locks$LockManager$LockState[LockState.SLEEP.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$webrtc$locks$LockManager$LockState[LockState.PROXIMITY.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            int[] iArr2 = new int[PhoneState.values().length];
            $SwitchMap$org$thoughtcrime$securesms$webrtc$locks$LockManager$PhoneState = iArr2;
            try {
                iArr2[PhoneState.IDLE.ordinal()] = 1;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$webrtc$locks$LockManager$PhoneState[PhoneState.PROCESSING.ordinal()] = 2;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$webrtc$locks$LockManager$PhoneState[PhoneState.INTERACTIVE.ordinal()] = 3;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$webrtc$locks$LockManager$PhoneState[PhoneState.IN_HANDS_FREE_CALL.ordinal()] = 4;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$webrtc$locks$LockManager$PhoneState[PhoneState.IN_VIDEO.ordinal()] = 5;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$webrtc$locks$LockManager$PhoneState[PhoneState.IN_CALL.ordinal()] = 6;
            } catch (NoSuchFieldError unused10) {
            }
        }
    }

    private synchronized void setLockState(LockState lockState) {
        int i = AnonymousClass2.$SwitchMap$org$thoughtcrime$securesms$webrtc$locks$LockManager$LockState[lockState.ordinal()];
        if (i == 1) {
            this.fullLock.acquire();
            this.partialLock.acquire();
            this.wifiLock.acquire();
            this.proximityLock.release();
        } else if (i == 2) {
            this.partialLock.acquire();
            this.wifiLock.acquire();
            this.fullLock.release();
            this.proximityLock.release();
        } else if (i == 3) {
            this.fullLock.release();
            this.partialLock.release();
            this.wifiLock.release();
            this.proximityLock.release();
        } else if (i == 4) {
            this.partialLock.acquire();
            this.proximityLock.acquire();
            this.wifiLock.acquire();
            this.fullLock.release();
        } else {
            throw new IllegalArgumentException("Unhandled Mode: " + lockState);
        }
        String str = TAG;
        Log.d(str, "Entered Lock State: " + lockState);
    }
}
