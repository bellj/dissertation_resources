package org.thoughtcrime.securesms.webrtc.audio;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import java.util.LinkedHashSet;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.ContextExtensionsKt;
import org.thoughtcrime.securesms.webrtc.audio.OutgoingRinger;
import org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager;
import org.thoughtcrime.securesms.webrtc.audio.SignalBluetoothManager;
import org.whispersystems.signalservice.api.util.Preconditions;

/* compiled from: SignalAudioManager.kt */
@Metadata(d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u00013B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\u001a\u001a\u00020\u001bH\u0014J\u0018\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u001d\u001a\u00020\u000b2\u0006\u0010\u001e\u001a\u00020\u000bH\u0002J\u001a\u0010\u001f\u001a\u00020\u001b2\b\u0010 \u001a\u0004\u0018\u00010!2\u0006\u0010\"\u001a\u00020\tH\u0014J\u0010\u0010#\u001a\u00020\u001b2\u0006\u0010\"\u001a\u00020\tH\u0002J\"\u0010$\u001a\u00020\u001b2\b\u0010 \u001a\u0004\u0018\u00010!2\u0006\u0010%\u001a\u00020\t2\u0006\u0010&\u001a\u00020\u000bH\u0014J\u0010\u0010'\u001a\u00020\u001b2\u0006\u0010(\u001a\u00020\u000bH\u0002J\u0010\u0010)\u001a\u00020\u001b2\u0006\u0010(\u001a\u00020\u000bH\u0002J\b\u0010*\u001a\u00020\u001bH\u0014J\u001a\u0010+\u001a\u00020\u001b2\b\u0010,\u001a\u0004\u0018\u00010-2\u0006\u0010.\u001a\u00020\u000bH\u0014J\b\u0010/\u001a\u00020\u001bH\u0014J\u0010\u00100\u001a\u00020\u001b2\u0006\u00101\u001a\u00020\u000bH\u0014J\u0006\u00102\u001a\u00020\u001bR\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u000bX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000bX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u000bX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u000bX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0018\u001a\b\u0018\u00010\u0019R\u00020\u0000X\u000e¢\u0006\u0002\n\u0000¨\u00064"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/FullSignalAudioManager;", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager;", "context", "Landroid/content/Context;", "eventListener", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$EventListener;", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$EventListener;)V", "audioDevices", "", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$AudioDevice;", "autoSwitchToBluetooth", "", "autoSwitchToWiredHeadset", "defaultAudioDevice", "hasWiredHeadset", "previousBluetoothState", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalBluetoothManager$State;", "savedAudioMode", "", "savedIsMicrophoneMute", "savedIsSpeakerPhoneOn", "signalBluetoothManager", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalBluetoothManager;", "userSelectedAudioDevice", "wiredHeadsetReceiver", "Lorg/thoughtcrime/securesms/webrtc/audio/FullSignalAudioManager$WiredHeadsetReceiver;", "initialize", "", "onWiredHeadsetChange", "pluggedIn", "hasMic", "selectAudioDevice", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "device", "setAudioDevice", "setDefaultAudioDevice", "newDefaultDevice", "clearUserEarpieceSelection", "setMicrophoneMute", "on", "setSpeakerphoneOn", NotificationProfileDatabase.NotificationProfileScheduleTable.START, "startIncomingRinger", "ringtoneUri", "Landroid/net/Uri;", "vibrate", "startOutgoingRinger", "stop", "playDisconnect", "updateAudioDeviceState", "WiredHeadsetReceiver", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes5.dex */
public final class FullSignalAudioManager extends SignalAudioManager {
    private Set<SignalAudioManager.AudioDevice> audioDevices = new LinkedHashSet();
    private boolean autoSwitchToBluetooth = true;
    private boolean autoSwitchToWiredHeadset = true;
    private SignalAudioManager.AudioDevice defaultAudioDevice = SignalAudioManager.AudioDevice.EARPIECE;
    private boolean hasWiredHeadset;
    private SignalBluetoothManager.State previousBluetoothState;
    private int savedAudioMode = -2;
    private boolean savedIsMicrophoneMute;
    private boolean savedIsSpeakerPhoneOn;
    private final SignalBluetoothManager signalBluetoothManager;
    private SignalAudioManager.AudioDevice userSelectedAudioDevice = SignalAudioManager.AudioDevice.NONE;
    private WiredHeadsetReceiver wiredHeadsetReceiver;

    /* compiled from: SignalAudioManager.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[SignalAudioManager.AudioDevice.values().length];
            iArr[SignalAudioManager.AudioDevice.SPEAKER_PHONE.ordinal()] = 1;
            iArr[SignalAudioManager.AudioDevice.EARPIECE.ordinal()] = 2;
            iArr[SignalAudioManager.AudioDevice.WIRED_HEADSET.ordinal()] = 3;
            iArr[SignalAudioManager.AudioDevice.BLUETOOTH.ordinal()] = 4;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public FullSignalAudioManager(Context context, SignalAudioManager.EventListener eventListener) {
        super(context, eventListener, null);
        Intrinsics.checkNotNullParameter(context, "context");
        this.signalBluetoothManager = new SignalBluetoothManager(context, this, getHandler());
    }

    @Override // org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager
    protected void initialize() {
        String str = SignalAudioManagerKt.TAG;
        Log.i(str, "Initializing audio manager state: " + getState());
        if (getState() == SignalAudioManager.State.UNINITIALIZED) {
            this.savedAudioMode = getAndroidAudioManager().getMode();
            this.savedIsSpeakerPhoneOn = getAndroidAudioManager().isSpeakerphoneOn();
            this.savedIsMicrophoneMute = getAndroidAudioManager().isMicrophoneMute();
            this.hasWiredHeadset = getAndroidAudioManager().isWiredHeadsetOn();
            if (!getAndroidAudioManager().requestCallAudioFocus()) {
                getHandler().postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.webrtc.audio.FullSignalAudioManager$$ExternalSyntheticLambda0
                    @Override // java.lang.Runnable
                    public final void run() {
                        FullSignalAudioManager.$r8$lambda$xiVJO3Jw3eHHKyXc5I7xSHyr2Yo(FullSignalAudioManager.this);
                    }
                }, 500);
            }
            setMicrophoneMute(false);
            this.audioDevices.clear();
            this.signalBluetoothManager.start();
            updateAudioDeviceState();
            this.wiredHeadsetReceiver = new WiredHeadsetReceiver();
            getContext().registerReceiver(this.wiredHeadsetReceiver, new IntentFilter("android.intent.action.HEADSET_PLUG"));
            setState(SignalAudioManager.State.PREINITIALIZED);
            Log.d(SignalAudioManagerKt.TAG, "Initialized");
        }
    }

    /* renamed from: initialize$lambda-0 */
    public static final void m3322initialize$lambda0(FullSignalAudioManager fullSignalAudioManager) {
        Intrinsics.checkNotNullParameter(fullSignalAudioManager, "this$0");
        fullSignalAudioManager.getAndroidAudioManager().requestCallAudioFocus();
    }

    @Override // org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager
    protected void start() {
        String str = SignalAudioManagerKt.TAG;
        Log.d(str, "Starting. state: " + getState());
        SignalAudioManager.State state = getState();
        SignalAudioManager.State state2 = SignalAudioManager.State.RUNNING;
        if (state == state2) {
            Log.w(SignalAudioManagerKt.TAG, "Skipping, already active");
            return;
        }
        getIncomingRinger().stop();
        getOutgoingRinger().stop();
        if (!getAndroidAudioManager().requestCallAudioFocus()) {
            getHandler().postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.webrtc.audio.FullSignalAudioManager$$ExternalSyntheticLambda1
                @Override // java.lang.Runnable
                public final void run() {
                    FullSignalAudioManager.$r8$lambda$1b5aaNbKSHM5QooTDYvY1lNkiV0(FullSignalAudioManager.this);
                }
            }, 500);
        }
        setState(state2);
        getAndroidAudioManager().setMode(3);
        float ringVolumeWithMinimum = getAndroidAudioManager().ringVolumeWithMinimum();
        getSoundPool().play(getConnectedSoundId(), ringVolumeWithMinimum, ringVolumeWithMinimum, 0, 0, 1.0f);
        Log.d(SignalAudioManagerKt.TAG, "Started");
    }

    /* renamed from: start$lambda-1 */
    public static final void m3323start$lambda1(FullSignalAudioManager fullSignalAudioManager) {
        Intrinsics.checkNotNullParameter(fullSignalAudioManager, "this$0");
        fullSignalAudioManager.getAndroidAudioManager().requestCallAudioFocus();
    }

    @Override // org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager
    protected void stop(boolean z) {
        String str = SignalAudioManagerKt.TAG;
        Log.d(str, "Stopping. state: " + getState());
        getIncomingRinger().stop();
        getOutgoingRinger().stop();
        if (z && getState() != SignalAudioManager.State.UNINITIALIZED) {
            float ringVolumeWithMinimum = getAndroidAudioManager().ringVolumeWithMinimum();
            getSoundPool().play(getDisconnectedSoundId(), ringVolumeWithMinimum, ringVolumeWithMinimum, 0, 0, 1.0f);
        }
        setState(SignalAudioManager.State.UNINITIALIZED);
        ContextExtensionsKt.safeUnregisterReceiver(getContext(), this.wiredHeadsetReceiver);
        this.wiredHeadsetReceiver = null;
        this.signalBluetoothManager.stop();
        setSpeakerphoneOn(this.savedIsSpeakerPhoneOn);
        setMicrophoneMute(this.savedIsMicrophoneMute);
        getAndroidAudioManager().setMode(this.savedAudioMode);
        getAndroidAudioManager().abandonCallAudioFocus();
        Log.d(SignalAudioManagerKt.TAG, "Abandoned audio focus for VOICE_CALL streams");
        Log.d(SignalAudioManagerKt.TAG, "Stopped");
    }

    public final void updateAudioDeviceState() {
        SignalBluetoothManager.State state;
        SignalAudioManager.EventListener eventListener;
        SignalAudioManager.AudioDevice audioDevice;
        SignalAudioManager.AudioDevice audioDevice2;
        getHandler().assertHandlerThread();
        Log.i(SignalAudioManagerKt.TAG, "updateAudioDeviceState(): wired: " + this.hasWiredHeadset + " bt: " + this.signalBluetoothManager.getState() + " available: " + this.audioDevices + " selected: " + getSelectedAudioDevice() + " userSelected: " + this.userSelectedAudioDevice);
        if (this.signalBluetoothManager.getState().shouldUpdate()) {
            this.signalBluetoothManager.updateDevice();
        }
        boolean z = true;
        SignalAudioManager.AudioDevice audioDevice3 = SignalAudioManager.AudioDevice.SPEAKER_PHONE;
        Set<SignalAudioManager.AudioDevice> set = SetsKt__SetsKt.mutableSetOf(audioDevice3);
        if (this.signalBluetoothManager.getState().hasDevice()) {
            set.add(SignalAudioManager.AudioDevice.BLUETOOTH);
        }
        if (this.hasWiredHeadset) {
            set.add(SignalAudioManager.AudioDevice.WIRED_HEADSET);
        } else {
            this.autoSwitchToWiredHeadset = true;
            if (getAndroidAudioManager().hasEarpiece(getContext())) {
                set.add(SignalAudioManager.AudioDevice.EARPIECE);
            }
        }
        boolean z2 = !Intrinsics.areEqual(this.audioDevices, set);
        this.audioDevices = set;
        SignalBluetoothManager.State state2 = this.signalBluetoothManager.getState();
        SignalBluetoothManager.State state3 = SignalBluetoothManager.State.UNAVAILABLE;
        if (state2 == state3 && this.userSelectedAudioDevice == SignalAudioManager.AudioDevice.BLUETOOTH) {
            this.userSelectedAudioDevice = SignalAudioManager.AudioDevice.NONE;
        }
        boolean z3 = this.hasWiredHeadset;
        if (z3 && this.autoSwitchToWiredHeadset) {
            this.userSelectedAudioDevice = SignalAudioManager.AudioDevice.WIRED_HEADSET;
            this.autoSwitchToWiredHeadset = false;
        }
        if (!z3 && this.userSelectedAudioDevice == SignalAudioManager.AudioDevice.WIRED_HEADSET) {
            this.userSelectedAudioDevice = SignalAudioManager.AudioDevice.NONE;
        }
        boolean z4 = this.signalBluetoothManager.getState() == SignalBluetoothManager.State.AVAILABLE && ((audioDevice2 = this.userSelectedAudioDevice) == SignalAudioManager.AudioDevice.NONE || audioDevice2 == SignalAudioManager.AudioDevice.BLUETOOTH || this.autoSwitchToBluetooth);
        SignalBluetoothManager.State state4 = this.signalBluetoothManager.getState();
        SignalBluetoothManager.State state5 = SignalBluetoothManager.State.CONNECTED;
        boolean z5 = ((state4 != state5 && this.signalBluetoothManager.getState() != SignalBluetoothManager.State.CONNECTING) || (audioDevice = this.userSelectedAudioDevice) == SignalAudioManager.AudioDevice.NONE || audioDevice == SignalAudioManager.AudioDevice.BLUETOOTH) ? false : true;
        if (this.signalBluetoothManager.getState().hasDevice()) {
            Log.i(SignalAudioManagerKt.TAG, "Need bluetooth audio: state: " + this.signalBluetoothManager.getState() + " start: " + z4 + " stop: " + z5);
        }
        if (z5) {
            this.signalBluetoothManager.stopScoAudio();
            this.signalBluetoothManager.updateDevice();
        }
        if (!this.autoSwitchToBluetooth && this.signalBluetoothManager.getState() == state3) {
            this.autoSwitchToBluetooth = true;
        }
        if (!z4 || z5 || this.signalBluetoothManager.startScoAudio()) {
            z = z2;
        } else {
            this.audioDevices.remove(SignalAudioManager.AudioDevice.BLUETOOTH);
        }
        if (this.autoSwitchToBluetooth && this.signalBluetoothManager.getState() == state5) {
            this.userSelectedAudioDevice = SignalAudioManager.AudioDevice.BLUETOOTH;
            this.autoSwitchToBluetooth = false;
        }
        SignalBluetoothManager.State state6 = this.previousBluetoothState;
        if (!(state6 == null || state6 == (state = SignalBluetoothManager.State.PERMISSION_DENIED) || this.signalBluetoothManager.getState() != state || (eventListener = getEventListener()) == null)) {
            eventListener.onBluetoothPermissionDenied();
        }
        this.previousBluetoothState = this.signalBluetoothManager.getState();
        if (this.audioDevices.contains(this.userSelectedAudioDevice)) {
            audioDevice3 = this.userSelectedAudioDevice;
        } else if (this.audioDevices.contains(this.defaultAudioDevice)) {
            audioDevice3 = this.defaultAudioDevice;
        }
        if (audioDevice3 != getSelectedAudioDevice() || z) {
            setAudioDevice(audioDevice3);
            Log.i(SignalAudioManagerKt.TAG, "New device status: available: " + this.audioDevices + ", selected: " + audioDevice3);
            SignalAudioManager.EventListener eventListener2 = getEventListener();
            if (eventListener2 != null) {
                eventListener2.onAudioDeviceChanged(getSelectedAudioDevice(), this.audioDevices);
            }
        }
    }

    @Override // org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager
    protected void setDefaultAudioDevice(RecipientId recipientId, SignalAudioManager.AudioDevice audioDevice, boolean z) {
        Intrinsics.checkNotNullParameter(audioDevice, "newDefaultDevice");
        String str = SignalAudioManagerKt.TAG;
        Log.d(str, "setDefaultAudioDevice(): currentDefault: " + this.defaultAudioDevice + " device: " + audioDevice + " clearUser: " + z);
        int i = WhenMappings.$EnumSwitchMapping$0[audioDevice.ordinal()];
        if (i != 1) {
            if (i != 2) {
                throw new AssertionError("Invalid default audio device selection");
            } else if (!getAndroidAudioManager().hasEarpiece(getContext())) {
                audioDevice = SignalAudioManager.AudioDevice.SPEAKER_PHONE;
            }
        }
        this.defaultAudioDevice = audioDevice;
        if (z && this.userSelectedAudioDevice == SignalAudioManager.AudioDevice.EARPIECE) {
            Log.d(SignalAudioManagerKt.TAG, "Clearing user setting of earpiece");
            this.userSelectedAudioDevice = SignalAudioManager.AudioDevice.NONE;
        }
        String str2 = SignalAudioManagerKt.TAG;
        Log.d(str2, "New default: " + this.defaultAudioDevice + " userSelected: " + this.userSelectedAudioDevice);
        updateAudioDeviceState();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0011, code lost:
        if (r4.contains(r0) != false) goto L_0x0015;
     */
    @Override // org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected void selectAudioDevice(org.thoughtcrime.securesms.recipients.RecipientId r4, org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager.AudioDevice r5) {
        /*
            r3 = this;
            java.lang.String r4 = "device"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r5, r4)
            org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager$AudioDevice r4 = org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager.AudioDevice.EARPIECE
            if (r5 != r4) goto L_0x0014
            java.util.Set<org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager$AudioDevice> r4 = r3.audioDevices
            org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager$AudioDevice r0 = org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager.AudioDevice.WIRED_HEADSET
            boolean r4 = r4.contains(r0)
            if (r4 == 0) goto L_0x0014
            goto L_0x0015
        L_0x0014:
            r0 = r5
        L_0x0015:
            java.lang.String r4 = org.thoughtcrime.securesms.webrtc.audio.SignalAudioManagerKt.access$getTAG$p()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "selectAudioDevice(): device: "
            r1.append(r2)
            r1.append(r5)
            java.lang.String r5 = " actualDevice: "
            r1.append(r5)
            r1.append(r0)
            java.lang.String r5 = r1.toString()
            org.signal.core.util.logging.Log.d(r4, r5)
            java.util.Set<org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager$AudioDevice> r4 = r3.audioDevices
            boolean r4 = r4.contains(r0)
            if (r4 != 0) goto L_0x005f
            java.lang.String r4 = org.thoughtcrime.securesms.webrtc.audio.SignalAudioManagerKt.access$getTAG$p()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r1 = "Can not select "
            r5.append(r1)
            r5.append(r0)
            java.lang.String r1 = " from available "
            r5.append(r1)
            java.util.Set<org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager$AudioDevice> r1 = r3.audioDevices
            r5.append(r1)
            java.lang.String r5 = r5.toString()
            org.signal.core.util.logging.Log.w(r4, r5)
        L_0x005f:
            r3.userSelectedAudioDevice = r0
            r3.updateAudioDeviceState()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.webrtc.audio.FullSignalAudioManager.selectAudioDevice(org.thoughtcrime.securesms.recipients.RecipientId, org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager$AudioDevice):void");
    }

    private final void setAudioDevice(SignalAudioManager.AudioDevice audioDevice) {
        String str = SignalAudioManagerKt.TAG;
        Log.d(str, "setAudioDevice(): device: " + audioDevice);
        Preconditions.checkArgument(this.audioDevices.contains(audioDevice));
        int i = WhenMappings.$EnumSwitchMapping$0[audioDevice.ordinal()];
        if (i == 1) {
            setSpeakerphoneOn(true);
        } else if (i == 2) {
            setSpeakerphoneOn(false);
        } else if (i == 3) {
            setSpeakerphoneOn(false);
        } else if (i == 4) {
            setSpeakerphoneOn(false);
        } else {
            throw new AssertionError("Invalid audio device selection");
        }
        setSelectedAudioDevice(audioDevice);
    }

    private final void setSpeakerphoneOn(boolean z) {
        if (getAndroidAudioManager().isSpeakerphoneOn() != z) {
            getAndroidAudioManager().setSpeakerphoneOn(z);
        }
    }

    private final void setMicrophoneMute(boolean z) {
        if (getAndroidAudioManager().isMicrophoneMute() != z) {
            getAndroidAudioManager().setMicrophoneMute(z);
        }
    }

    @Override // org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager
    protected void startIncomingRinger(Uri uri, boolean z) {
        String str = SignalAudioManagerKt.TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("startIncomingRinger(): uri: ");
        sb.append(uri != null ? "present" : "null");
        sb.append(" vibrate: ");
        sb.append(z);
        Log.i(str, sb.toString());
        getAndroidAudioManager().setMode(1);
        setMicrophoneMute(false);
        setDefaultAudioDevice(null, SignalAudioManager.AudioDevice.SPEAKER_PHONE, false);
        getIncomingRinger().start(uri, z);
    }

    @Override // org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager
    protected void startOutgoingRinger() {
        String str = SignalAudioManagerKt.TAG;
        Log.i(str, "startOutgoingRinger(): currentDevice: " + getSelectedAudioDevice());
        getAndroidAudioManager().setMode(3);
        setMicrophoneMute(false);
        getOutgoingRinger().start(OutgoingRinger.Type.RINGING);
    }

    public final void onWiredHeadsetChange(boolean z, boolean z2) {
        String str = SignalAudioManagerKt.TAG;
        Log.i(str, "onWiredHeadsetChange state: " + getState() + " plug: " + z + " mic: " + z2);
        this.hasWiredHeadset = z;
        updateAudioDeviceState();
    }

    /* compiled from: SignalAudioManager.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/FullSignalAudioManager$WiredHeadsetReceiver;", "Landroid/content/BroadcastReceiver;", "(Lorg/thoughtcrime/securesms/webrtc/audio/FullSignalAudioManager;)V", "onReceive", "", "context", "Landroid/content/Context;", "intent", "Landroid/content/Intent;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public final class WiredHeadsetReceiver extends BroadcastReceiver {
        public WiredHeadsetReceiver() {
            FullSignalAudioManager.this = r1;
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(intent, "intent");
            boolean z = false;
            boolean z2 = intent.getIntExtra("state", 0) == 1;
            if (intent.getIntExtra("microphone", 0) == 1) {
                z = true;
            }
            FullSignalAudioManager.this.getHandler().post(new FullSignalAudioManager$WiredHeadsetReceiver$$ExternalSyntheticLambda0(FullSignalAudioManager.this, z2, z));
        }

        /* renamed from: onReceive$lambda-0 */
        public static final void m3324onReceive$lambda0(FullSignalAudioManager fullSignalAudioManager, boolean z, boolean z2) {
            Intrinsics.checkNotNullParameter(fullSignalAudioManager, "this$0");
            fullSignalAudioManager.onWiredHeadsetChange(z, z2);
        }
    }
}
