package org.thoughtcrime.securesms.webrtc;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.thoughtcrime.securesms.MainActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.WebRtcCallActivity;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.service.webrtc.WebRtcCallService;

/* loaded from: classes5.dex */
public class CallNotificationBuilder {
    public static final int TYPE_ESTABLISHED;
    public static final int TYPE_INCOMING_CONNECTING;
    public static final int TYPE_INCOMING_RINGING;
    public static final int TYPE_OUTGOING_RINGING;
    private static final int WEBRTC_NOTIFICATION;
    private static final int WEBRTC_NOTIFICATION_RINGING;

    public static int getStartingStoppingNotificationId() {
        return WEBRTC_NOTIFICATION;
    }

    public static boolean isWebRtcNotification(int i) {
        return i == WEBRTC_NOTIFICATION || i == WEBRTC_NOTIFICATION_RINGING;
    }

    public static Notification getCallInProgressNotification(Context context, int i, Recipient recipient) {
        Intent intent = new Intent(context, WebRtcCallActivity.class);
        intent.setFlags(SQLiteDatabase.ENABLE_WRITE_AHEAD_LOGGING);
        PendingIntent activity = PendingIntent.getActivity(context, 0, intent, 0);
        NotificationCompat.Builder contentTitle = new NotificationCompat.Builder(context, getNotificationChannel(context, i)).setSmallIcon(R.drawable.ic_call_secure_white_24dp).setContentIntent(activity).setOngoing(true).setContentTitle(recipient.getDisplayName(context));
        if (i == 4) {
            contentTitle.setContentText(context.getString(R.string.CallNotificationBuilder_connecting));
            contentTitle.setPriority(-2);
            contentTitle.setContentIntent(null);
        } else if (i == 1) {
            contentTitle.setContentText(context.getString(recipient.isGroup() ? R.string.NotificationBarManager__incoming_signal_group_call : R.string.NotificationBarManager__incoming_signal_call));
            contentTitle.addAction(getServiceNotificationAction(context, WebRtcCallService.denyCallIntent(context), R.drawable.ic_close_grey600_32dp, R.string.NotificationBarManager__decline_call));
            contentTitle.addAction(getActivityNotificationAction(context, WebRtcCallActivity.ANSWER_ACTION, R.drawable.ic_phone_grey600_32dp, recipient.isGroup() ? R.string.NotificationBarManager__join_call : R.string.NotificationBarManager__answer_call));
            if (callActivityRestricted()) {
                contentTitle.setFullScreenIntent(activity, true);
                contentTitle.setPriority(1);
                contentTitle.setCategory("call");
            }
        } else if (i == 2) {
            contentTitle.setContentText(context.getString(R.string.NotificationBarManager__establishing_signal_call));
            contentTitle.addAction(getServiceNotificationAction(context, WebRtcCallService.hangupIntent(context), R.drawable.ic_call_end_grey600_32dp, R.string.NotificationBarManager__cancel_call));
        } else {
            contentTitle.setContentText(context.getString(R.string.NotificationBarManager_signal_call_in_progress));
            contentTitle.addAction(getServiceNotificationAction(context, WebRtcCallService.hangupIntent(context), R.drawable.ic_call_end_grey600_32dp, R.string.NotificationBarManager__end_call));
        }
        return contentTitle.build();
    }

    public static int getNotificationId(int i) {
        return (!callActivityRestricted() || i != 1) ? WEBRTC_NOTIFICATION : WEBRTC_NOTIFICATION_RINGING;
    }

    public static Notification getStartingNotification(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(SQLiteDatabase.ENABLE_WRITE_AHEAD_LOGGING);
        return new NotificationCompat.Builder(context, NotificationChannels.CALL_STATUS).setSmallIcon(R.drawable.ic_call_secure_white_24dp).setContentIntent(PendingIntent.getActivity(context, 0, intent, 0)).setOngoing(true).setContentTitle(context.getString(R.string.NotificationBarManager__starting_signal_call_service)).setPriority(-2).build();
    }

    public static Notification getStoppingNotification(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(SQLiteDatabase.ENABLE_WRITE_AHEAD_LOGGING);
        return new NotificationCompat.Builder(context, NotificationChannels.CALL_STATUS).setSmallIcon(R.drawable.ic_call_secure_white_24dp).setContentIntent(PendingIntent.getActivity(context, 0, intent, 0)).setOngoing(true).setContentTitle(context.getString(R.string.NotificationBarManager__stopping_signal_call_service)).setPriority(-2).build();
    }

    private static String getNotificationChannel(Context context, int i) {
        return (!callActivityRestricted() || i != 1) ? NotificationChannels.CALL_STATUS : NotificationChannels.CALLS;
    }

    private static NotificationCompat.Action getServiceNotificationAction(Context context, Intent intent, int i, int i2) {
        PendingIntent pendingIntent;
        if (Build.VERSION.SDK_INT >= 26) {
            pendingIntent = PendingIntent.getForegroundService(context, 0, intent, 0);
        } else {
            pendingIntent = PendingIntent.getService(context, 0, intent, 0);
        }
        return new NotificationCompat.Action(i, context.getString(i2), pendingIntent);
    }

    private static NotificationCompat.Action getActivityNotificationAction(Context context, String str, int i, int i2) {
        Intent intent = new Intent(context, WebRtcCallActivity.class);
        intent.setAction(str);
        return new NotificationCompat.Action(i, context.getString(i2), PendingIntent.getActivity(context, 0, intent, 0));
    }

    private static boolean callActivityRestricted() {
        return Build.VERSION.SDK_INT >= 29 && !ApplicationDependencies.getAppForegroundObserver().isForegrounded();
    }
}
