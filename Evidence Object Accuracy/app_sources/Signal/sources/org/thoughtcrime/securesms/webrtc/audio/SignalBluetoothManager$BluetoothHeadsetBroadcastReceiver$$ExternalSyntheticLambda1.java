package org.thoughtcrime.securesms.webrtc.audio;

import org.thoughtcrime.securesms.webrtc.audio.SignalBluetoothManager;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes5.dex */
public final /* synthetic */ class SignalBluetoothManager$BluetoothHeadsetBroadcastReceiver$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ SignalBluetoothManager f$0;
    public final /* synthetic */ int f$1;
    public final /* synthetic */ SignalBluetoothManager.BluetoothHeadsetBroadcastReceiver f$2;

    public /* synthetic */ SignalBluetoothManager$BluetoothHeadsetBroadcastReceiver$$ExternalSyntheticLambda1(SignalBluetoothManager signalBluetoothManager, int i, SignalBluetoothManager.BluetoothHeadsetBroadcastReceiver bluetoothHeadsetBroadcastReceiver) {
        this.f$0 = signalBluetoothManager;
        this.f$1 = i;
        this.f$2 = bluetoothHeadsetBroadcastReceiver;
    }

    @Override // java.lang.Runnable
    public final void run() {
        SignalBluetoothManager.BluetoothHeadsetBroadcastReceiver.m3332onReceive$lambda1(this.f$0, this.f$1, this.f$2);
    }
}
