package org.thoughtcrime.securesms.webrtc.audio;

import android.content.Context;
import android.net.Uri;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.service.webrtc.AndroidTelecomUtil;
import org.thoughtcrime.securesms.webrtc.audio.OutgoingRinger;
import org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager;

/* compiled from: SignalAudioManager.kt */
@Metadata(d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\u0007\u001a\u00020\bH\u0014J\u001a\u0010\t\u001a\u00020\b2\b\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\f\u001a\u00020\rH\u0014J\"\u0010\u000e\u001a\u00020\b2\b\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\u000f\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u0011H\u0014J\b\u0010\u0012\u001a\u00020\bH\u0014J\u001a\u0010\u0013\u001a\u00020\b2\b\u0010\u0014\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u0016\u001a\u00020\u0011H\u0014J\b\u0010\u0017\u001a\u00020\bH\u0014J\u0010\u0010\u0018\u001a\u00020\b2\u0006\u0010\u0019\u001a\u00020\u0011H\u0014¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/TelecomAwareSignalAudioManager;", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager;", "context", "Landroid/content/Context;", "eventListener", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$EventListener;", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$EventListener;)V", "initialize", "", "selectAudioDevice", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "device", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$AudioDevice;", "setDefaultAudioDevice", "newDefaultDevice", "clearUserEarpieceSelection", "", NotificationProfileDatabase.NotificationProfileScheduleTable.START, "startIncomingRinger", "ringtoneUri", "Landroid/net/Uri;", "vibrate", "startOutgoingRinger", "stop", "playDisconnect", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes5.dex */
public final class TelecomAwareSignalAudioManager extends SignalAudioManager {
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public TelecomAwareSignalAudioManager(Context context, SignalAudioManager.EventListener eventListener) {
        super(context, eventListener, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    @Override // org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager
    protected void setDefaultAudioDevice(RecipientId recipientId, SignalAudioManager.AudioDevice audioDevice, boolean z) {
        Intrinsics.checkNotNullParameter(audioDevice, "newDefaultDevice");
        if (recipientId != null && AndroidTelecomUtil.INSTANCE.getSelectedAudioDevice(recipientId) == SignalAudioManager.AudioDevice.EARPIECE) {
            selectAudioDevice(recipientId, audioDevice);
        }
    }

    @Override // org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager
    protected void initialize() {
        if (!getAndroidAudioManager().requestCallAudioFocus()) {
            getHandler().postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.webrtc.audio.TelecomAwareSignalAudioManager$$ExternalSyntheticLambda2
                @Override // java.lang.Runnable
                public final void run() {
                    TelecomAwareSignalAudioManager.m3338initialize$lambda0(TelecomAwareSignalAudioManager.this);
                }
            }, 500);
        }
        setState(SignalAudioManager.State.PREINITIALIZED);
    }

    /* renamed from: initialize$lambda-0 */
    public static final void m3338initialize$lambda0(TelecomAwareSignalAudioManager telecomAwareSignalAudioManager) {
        Intrinsics.checkNotNullParameter(telecomAwareSignalAudioManager, "this$0");
        telecomAwareSignalAudioManager.getAndroidAudioManager().requestCallAudioFocus();
    }

    @Override // org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager
    protected void start() {
        getIncomingRinger().stop();
        getOutgoingRinger().stop();
        if (!getAndroidAudioManager().requestCallAudioFocus()) {
            getHandler().postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.webrtc.audio.TelecomAwareSignalAudioManager$$ExternalSyntheticLambda0
                @Override // java.lang.Runnable
                public final void run() {
                    TelecomAwareSignalAudioManager.m3340start$lambda1(TelecomAwareSignalAudioManager.this);
                }
            }, 500);
        }
        setState(SignalAudioManager.State.RUNNING);
    }

    /* renamed from: start$lambda-1 */
    public static final void m3340start$lambda1(TelecomAwareSignalAudioManager telecomAwareSignalAudioManager) {
        Intrinsics.checkNotNullParameter(telecomAwareSignalAudioManager, "this$0");
        telecomAwareSignalAudioManager.getAndroidAudioManager().requestCallAudioFocus();
    }

    @Override // org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager
    protected void stop(boolean z) {
        getIncomingRinger().stop();
        getOutgoingRinger().stop();
        if (z && getState() != SignalAudioManager.State.UNINITIALIZED) {
            float ringVolumeWithMinimum = getAndroidAudioManager().ringVolumeWithMinimum();
            getSoundPool().play(getDisconnectedSoundId(), ringVolumeWithMinimum, ringVolumeWithMinimum, 0, 0, 1.0f);
        }
        setState(SignalAudioManager.State.UNINITIALIZED);
        getAndroidAudioManager().abandonCallAudioFocus();
    }

    @Override // org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager
    protected void selectAudioDevice(RecipientId recipientId, SignalAudioManager.AudioDevice audioDevice) {
        Intrinsics.checkNotNullParameter(audioDevice, "device");
        if (recipientId != null) {
            setSelectedAudioDevice(audioDevice);
            AndroidTelecomUtil.INSTANCE.selectAudioDevice(recipientId, audioDevice);
            getHandler().postDelayed(new Runnable(this) { // from class: org.thoughtcrime.securesms.webrtc.audio.TelecomAwareSignalAudioManager$$ExternalSyntheticLambda1
                public final /* synthetic */ TelecomAwareSignalAudioManager f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    TelecomAwareSignalAudioManager.m3339selectAudioDevice$lambda2(RecipientId.this, this.f$1);
                }
            }, 1000);
        }
    }

    /* renamed from: selectAudioDevice$lambda-2 */
    public static final void m3339selectAudioDevice$lambda2(RecipientId recipientId, TelecomAwareSignalAudioManager telecomAwareSignalAudioManager) {
        Intrinsics.checkNotNullParameter(telecomAwareSignalAudioManager, "this$0");
        AndroidTelecomUtil.INSTANCE.selectAudioDevice(recipientId, telecomAwareSignalAudioManager.getSelectedAudioDevice());
    }

    @Override // org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager
    protected void startIncomingRinger(Uri uri, boolean z) {
        getIncomingRinger().start(uri, z);
    }

    @Override // org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager
    protected void startOutgoingRinger() {
        getOutgoingRinger().start(OutgoingRinger.Type.RINGING);
    }
}
