package org.thoughtcrime.securesms.webrtc.audio;

import org.thoughtcrime.securesms.webrtc.audio.SignalBluetoothManager;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes5.dex */
public final /* synthetic */ class SignalBluetoothManager$BluetoothServiceListener$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ SignalBluetoothManager f$0;

    public /* synthetic */ SignalBluetoothManager$BluetoothServiceListener$$ExternalSyntheticLambda0(SignalBluetoothManager signalBluetoothManager) {
        this.f$0 = signalBluetoothManager;
    }

    @Override // java.lang.Runnable
    public final void run() {
        SignalBluetoothManager.BluetoothServiceListener.m3336onServiceDisconnected$lambda1(this.f$0);
    }
}
