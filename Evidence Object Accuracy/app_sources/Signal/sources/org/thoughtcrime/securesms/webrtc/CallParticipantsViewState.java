package org.thoughtcrime.securesms.webrtc;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.webrtc.CallParticipantsState;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcEphemeralState;

/* compiled from: CallParticipantsViewState.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0002\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\b\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\f¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/CallParticipantsViewState;", "", "callParticipantsState", "Lorg/thoughtcrime/securesms/components/webrtc/CallParticipantsState;", "ephemeralState", "Lorg/thoughtcrime/securesms/service/webrtc/state/WebRtcEphemeralState;", "isPortrait", "", "isLandscapeEnabled", "(Lorg/thoughtcrime/securesms/components/webrtc/CallParticipantsState;Lorg/thoughtcrime/securesms/service/webrtc/state/WebRtcEphemeralState;ZZ)V", "getCallParticipantsState", "()Lorg/thoughtcrime/securesms/components/webrtc/CallParticipantsState;", "()Z", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes5.dex */
public final class CallParticipantsViewState {
    private final CallParticipantsState callParticipantsState;
    private final boolean isLandscapeEnabled;
    private final boolean isPortrait;

    public CallParticipantsViewState(CallParticipantsState callParticipantsState, WebRtcEphemeralState webRtcEphemeralState, boolean z, boolean z2) {
        Intrinsics.checkNotNullParameter(callParticipantsState, "callParticipantsState");
        Intrinsics.checkNotNullParameter(webRtcEphemeralState, "ephemeralState");
        this.isPortrait = z;
        this.isLandscapeEnabled = z2;
        this.callParticipantsState = CallParticipantsState.Companion.update(callParticipantsState, webRtcEphemeralState);
    }

    public final boolean isPortrait() {
        return this.isPortrait;
    }

    public final boolean isLandscapeEnabled() {
        return this.isLandscapeEnabled;
    }

    public final CallParticipantsState getCallParticipantsState() {
        return this.callParticipantsState;
    }
}
