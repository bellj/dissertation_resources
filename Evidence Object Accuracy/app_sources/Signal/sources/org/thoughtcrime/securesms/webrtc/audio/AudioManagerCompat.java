package org.thoughtcrime.securesms.webrtc.audio;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioDeviceInfo;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.ServiceUtil;

/* loaded from: classes5.dex */
public abstract class AudioManagerCompat {
    private static final int AUDIOFOCUS_GAIN;
    private static final String TAG = Log.tag(AudioManagerCompat.class);
    protected final AudioManager audioManager;
    protected boolean hasFocus;
    protected final AudioManager.OnAudioFocusChangeListener onAudioFocusChangeListener;

    public abstract void abandonCallAudioFocus();

    public abstract SoundPool createSoundPool();

    public abstract boolean requestCallAudioFocus();

    public /* synthetic */ void lambda$new$0(int i) {
        String str = TAG;
        Log.i(str, "onAudioFocusChangeListener: " + i);
        boolean z = true;
        if (i != 1) {
            z = false;
        }
        this.hasFocus = z;
    }

    private AudioManagerCompat(Context context) {
        this.onAudioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() { // from class: org.thoughtcrime.securesms.webrtc.audio.AudioManagerCompat$$ExternalSyntheticLambda0
            @Override // android.media.AudioManager.OnAudioFocusChangeListener
            public final void onAudioFocusChange(int i) {
                AudioManagerCompat.this.lambda$new$0(i);
            }
        };
        this.audioManager = ServiceUtil.getAudioManager(context);
    }

    public boolean isBluetoothScoAvailableOffCall() {
        return this.audioManager.isBluetoothScoAvailableOffCall();
    }

    public void startBluetoothSco() {
        this.audioManager.startBluetoothSco();
    }

    public void stopBluetoothSco() {
        this.audioManager.stopBluetoothSco();
    }

    public boolean isBluetoothScoOn() {
        return this.audioManager.isBluetoothScoOn();
    }

    public void setBluetoothScoOn(boolean z) {
        this.audioManager.setBluetoothScoOn(z);
    }

    public int getMode() {
        return this.audioManager.getMode();
    }

    public void setMode(int i) {
        this.audioManager.setMode(i);
    }

    public boolean isSpeakerphoneOn() {
        return this.audioManager.isSpeakerphoneOn();
    }

    public void setSpeakerphoneOn(boolean z) {
        this.audioManager.setSpeakerphoneOn(z);
    }

    public boolean isMicrophoneMute() {
        return this.audioManager.isMicrophoneMute();
    }

    public void setMicrophoneMute(boolean z) {
        this.audioManager.setMicrophoneMute(z);
    }

    public boolean hasEarpiece(Context context) {
        return context.getPackageManager().hasSystemFeature("android.hardware.telephony");
    }

    public boolean isWiredHeadsetOn() {
        if (Build.VERSION.SDK_INT < 23) {
            return this.audioManager.isWiredHeadsetOn();
        }
        for (AudioDeviceInfo audioDeviceInfo : this.audioManager.getDevices(3)) {
            int type = audioDeviceInfo.getType();
            if (type == 3 || type == 11) {
                return true;
            }
        }
        return false;
    }

    public float ringVolumeWithMinimum() {
        return Math.max(logVolume(this.audioManager.getStreamVolume(2), this.audioManager.getStreamMaxVolume(2)), logVolume(15, 100));
    }

    private static float logVolume(int i, int i2) {
        if (i2 == 0 || i > i2) {
            return 0.5f;
        }
        int i3 = i2 + 1;
        return (float) (1.0d - (Math.log((double) (i3 - i)) / Math.log((double) i3)));
    }

    public static AudioManagerCompat create(Context context) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 26) {
            return new Api26AudioManagerCompat(context);
        }
        if (i >= 21) {
            return new Api21AudioManagerCompat(context);
        }
        return new Api19AudioManagerCompat(context);
    }

    /* loaded from: classes5.dex */
    public static class Api26AudioManagerCompat extends AudioManagerCompat {
        private static AudioAttributes AUDIO_ATTRIBUTES = new AudioAttributes.Builder().setContentType(1).setUsage(2).build();
        private AudioFocusRequest audioFocusRequest;

        private Api26AudioManagerCompat(Context context) {
            super(context);
        }

        @Override // org.thoughtcrime.securesms.webrtc.audio.AudioManagerCompat
        public SoundPool createSoundPool() {
            return new SoundPool.Builder().setAudioAttributes(AUDIO_ATTRIBUTES).setMaxStreams(1).build();
        }

        @Override // org.thoughtcrime.securesms.webrtc.audio.AudioManagerCompat
        public boolean requestCallAudioFocus() {
            AudioFocusRequest audioFocusRequest = this.audioFocusRequest;
            if (audioFocusRequest == null || !this.hasFocus) {
                if (audioFocusRequest == null) {
                    this.audioFocusRequest = new AudioFocusRequest.Builder(4).setAudioAttributes(AUDIO_ATTRIBUTES).setOnAudioFocusChangeListener(this.onAudioFocusChangeListener).build();
                } else {
                    Log.w(AudioManagerCompat.TAG, "Trying again to request audio focus");
                }
                int requestAudioFocus = this.audioManager.requestAudioFocus(this.audioFocusRequest);
                if (requestAudioFocus == 1) {
                    return true;
                }
                String str = AudioManagerCompat.TAG;
                Log.w(str, "Audio focus not granted. Result code: " + requestAudioFocus);
                return false;
            }
            Log.w(AudioManagerCompat.TAG, "Already requested audio focus. Ignoring...");
            return true;
        }

        @Override // org.thoughtcrime.securesms.webrtc.audio.AudioManagerCompat
        public void abandonCallAudioFocus() {
            AudioFocusRequest audioFocusRequest = this.audioFocusRequest;
            if (audioFocusRequest == null) {
                Log.w(AudioManagerCompat.TAG, "Don't currently have audio focus. Ignoring...");
                return;
            }
            int abandonAudioFocusRequest = this.audioManager.abandonAudioFocusRequest(audioFocusRequest);
            if (abandonAudioFocusRequest != 1) {
                String str = AudioManagerCompat.TAG;
                Log.w(str, "Audio focus abandon failed. Result code: " + abandonAudioFocusRequest);
            }
            this.hasFocus = false;
            this.audioFocusRequest = null;
        }
    }

    /* loaded from: classes5.dex */
    public static class Api21AudioManagerCompat extends Api19AudioManagerCompat {
        private static AudioAttributes AUDIO_ATTRIBUTES = new AudioAttributes.Builder().setContentType(1).setUsage(2).setLegacyStreamType(0).build();

        private Api21AudioManagerCompat(Context context) {
            super(context);
        }

        @Override // org.thoughtcrime.securesms.webrtc.audio.AudioManagerCompat.Api19AudioManagerCompat, org.thoughtcrime.securesms.webrtc.audio.AudioManagerCompat
        public SoundPool createSoundPool() {
            return new SoundPool.Builder().setAudioAttributes(AUDIO_ATTRIBUTES).setMaxStreams(1).build();
        }
    }

    /* loaded from: classes5.dex */
    public static class Api19AudioManagerCompat extends AudioManagerCompat {
        private Api19AudioManagerCompat(Context context) {
            super(context);
        }

        @Override // org.thoughtcrime.securesms.webrtc.audio.AudioManagerCompat
        public SoundPool createSoundPool() {
            return new SoundPool(1, 5, 0);
        }

        @Override // org.thoughtcrime.securesms.webrtc.audio.AudioManagerCompat
        public boolean requestCallAudioFocus() {
            int requestAudioFocus = this.audioManager.requestAudioFocus(this.onAudioFocusChangeListener, 0, 4);
            if (requestAudioFocus == 1) {
                return true;
            }
            String str = AudioManagerCompat.TAG;
            Log.w(str, "Audio focus not granted. Result code: " + requestAudioFocus);
            return false;
        }

        @Override // org.thoughtcrime.securesms.webrtc.audio.AudioManagerCompat
        public void abandonCallAudioFocus() {
            int abandonAudioFocus = this.audioManager.abandonAudioFocus(this.onAudioFocusChangeListener);
            if (abandonAudioFocus != 1) {
                String str = AudioManagerCompat.TAG;
                Log.w(str, "Audio focus abandon failed. Result code: " + abandonAudioFocus);
            }
        }
    }
}
