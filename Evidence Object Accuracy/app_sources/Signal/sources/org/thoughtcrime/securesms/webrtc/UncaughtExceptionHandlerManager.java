package org.thoughtcrime.securesms.webrtc;

import java.lang.Thread;
import java.util.ArrayList;
import java.util.List;
import org.signal.core.util.logging.Log;

/* loaded from: classes5.dex */
public class UncaughtExceptionHandlerManager implements Thread.UncaughtExceptionHandler {
    private static final String TAG = Log.tag(UncaughtExceptionHandlerManager.class);
    private final List<Thread.UncaughtExceptionHandler> handlers = new ArrayList();
    private final Thread.UncaughtExceptionHandler originalHandler;

    public UncaughtExceptionHandlerManager() {
        Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        this.originalHandler = defaultUncaughtExceptionHandler;
        registerHandler(defaultUncaughtExceptionHandler);
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    public void registerHandler(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.handlers.add(uncaughtExceptionHandler);
    }

    public void unregister() {
        Thread.setDefaultUncaughtExceptionHandler(this.originalHandler);
    }

    @Override // java.lang.Thread.UncaughtExceptionHandler
    public void uncaughtException(Thread thread, Throwable th) {
        for (int size = this.handlers.size() - 1; size >= 0; size--) {
            try {
                this.handlers.get(size).uncaughtException(thread, th);
            } catch (Throwable th2) {
                Log.e(TAG, "Error in uncaught exception handling", th2);
            }
        }
    }
}
