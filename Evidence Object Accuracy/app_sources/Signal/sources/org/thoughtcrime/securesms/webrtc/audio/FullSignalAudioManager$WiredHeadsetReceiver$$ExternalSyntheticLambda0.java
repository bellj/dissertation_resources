package org.thoughtcrime.securesms.webrtc.audio;

import org.thoughtcrime.securesms.webrtc.audio.FullSignalAudioManager;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes5.dex */
public final /* synthetic */ class FullSignalAudioManager$WiredHeadsetReceiver$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ FullSignalAudioManager f$0;
    public final /* synthetic */ boolean f$1;
    public final /* synthetic */ boolean f$2;

    public /* synthetic */ FullSignalAudioManager$WiredHeadsetReceiver$$ExternalSyntheticLambda0(FullSignalAudioManager fullSignalAudioManager, boolean z, boolean z2) {
        this.f$0 = fullSignalAudioManager;
        this.f$1 = z;
        this.f$2 = z2;
    }

    @Override // java.lang.Runnable
    public final void run() {
        FullSignalAudioManager.WiredHeadsetReceiver.m3324onReceive$lambda0(this.f$0, this.f$1, this.f$2);
    }
}
