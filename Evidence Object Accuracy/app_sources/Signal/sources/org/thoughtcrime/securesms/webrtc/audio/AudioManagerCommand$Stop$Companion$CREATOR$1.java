package org.thoughtcrime.securesms.webrtc.audio;

import android.os.Parcel;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.util.ParcelUtil;
import org.thoughtcrime.securesms.webrtc.audio.AudioManagerCommand;

/* compiled from: AudioManagerCommand.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand$Stop;", "it", "Landroid/os/Parcel;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes5.dex */
final class AudioManagerCommand$Stop$Companion$CREATOR$1 extends Lambda implements Function1<Parcel, AudioManagerCommand.Stop> {
    public static final AudioManagerCommand$Stop$Companion$CREATOR$1 INSTANCE = new AudioManagerCommand$Stop$Companion$CREATOR$1();

    AudioManagerCommand$Stop$Companion$CREATOR$1() {
        super(1);
    }

    public final AudioManagerCommand.Stop invoke(Parcel parcel) {
        Intrinsics.checkNotNullParameter(parcel, "it");
        return new AudioManagerCommand.Stop(ParcelUtil.readBoolean(parcel));
    }
}
