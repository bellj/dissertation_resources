package org.thoughtcrime.securesms.webrtc.audio;

import android.os.Handler;
import android.os.Looper;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: SignalAudioHandler.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\u0005\u001a\u00020\u0006J\u0006\u0010\u0007\u001a\u00020\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioHandler;", "Landroid/os/Handler;", "looper", "Landroid/os/Looper;", "(Landroid/os/Looper;)V", "assertHandlerThread", "", "isOnHandler", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes5.dex */
public final class SignalAudioHandler extends Handler {
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public SignalAudioHandler(Looper looper) {
        super(looper);
        Intrinsics.checkNotNullParameter(looper, "looper");
    }

    public final void assertHandlerThread() {
        if (!isOnHandler()) {
            throw new AssertionError("Must run on audio handler thread.");
        }
    }

    public final boolean isOnHandler() {
        return Intrinsics.areEqual(Looper.myLooper(), getLooper());
    }
}
