package org.thoughtcrime.securesms.webrtc.audio;

import android.content.Context;
import android.media.SoundPool;
import android.net.Uri;
import android.os.HandlerThread;
import android.os.Looper;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.service.webrtc.AndroidTelecomUtil;
import org.thoughtcrime.securesms.webrtc.audio.AudioManagerCommand;

/* compiled from: SignalAudioManager.kt */
@Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 L2\u00020\u0001:\u0004KLMNB\u0019\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u000e\u00104\u001a\u0002052\u0006\u00106\u001a\u000207J\b\u00108\u001a\u000205H$J\u001a\u00109\u001a\u0002052\b\u0010:\u001a\u0004\u0018\u00010;2\u0006\u0010<\u001a\u00020%H$J\"\u0010=\u001a\u0002052\b\u0010:\u001a\u0004\u0018\u00010;2\u0006\u0010>\u001a\u00020%2\u0006\u0010?\u001a\u00020@H$J\u0006\u0010A\u001a\u000205J\b\u0010B\u001a\u000205H\u0014J\b\u0010C\u001a\u000205H$J\u001a\u0010D\u001a\u0002052\b\u0010E\u001a\u0004\u0018\u00010F2\u0006\u0010G\u001a\u00020@H$J\b\u0010H\u001a\u000205H$J\u0010\u0010I\u001a\u0002052\u0006\u0010J\u001a\u00020@H$R\u0014\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0016\u0010\u000b\u001a\n \r*\u0004\u0018\u00010\f0\fX\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u00020\u000fX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0014\u0010\u0014\u001a\u00020\u000fX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0011R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0014\u0010\u0018\u001a\u00020\u0019X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0014\u0010\u001c\u001a\u00020\u001dX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0014\u0010 \u001a\u00020!X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010#R\u001a\u0010$\u001a\u00020%X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b&\u0010'\"\u0004\b(\u0010)R\u0014\u0010*\u001a\u00020+X\u0004¢\u0006\b\n\u0000\u001a\u0004\b,\u0010-R\u001a\u0010.\u001a\u00020/X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b0\u00101\"\u0004\b2\u00103\u0001\u0002OP¨\u0006Q"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager;", "", "context", "Landroid/content/Context;", "eventListener", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$EventListener;", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$EventListener;)V", "androidAudioManager", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCompat;", "getAndroidAudioManager", "()Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCompat;", "commandAndControlThread", "Landroid/os/HandlerThread;", "kotlin.jvm.PlatformType", "connectedSoundId", "", "getConnectedSoundId", "()I", "getContext", "()Landroid/content/Context;", "disconnectedSoundId", "getDisconnectedSoundId", "getEventListener", "()Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$EventListener;", "handler", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioHandler;", "getHandler", "()Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioHandler;", "incomingRinger", "Lorg/thoughtcrime/securesms/webrtc/audio/IncomingRinger;", "getIncomingRinger", "()Lorg/thoughtcrime/securesms/webrtc/audio/IncomingRinger;", "outgoingRinger", "Lorg/thoughtcrime/securesms/webrtc/audio/OutgoingRinger;", "getOutgoingRinger", "()Lorg/thoughtcrime/securesms/webrtc/audio/OutgoingRinger;", "selectedAudioDevice", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$AudioDevice;", "getSelectedAudioDevice", "()Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$AudioDevice;", "setSelectedAudioDevice", "(Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$AudioDevice;)V", "soundPool", "Landroid/media/SoundPool;", "getSoundPool", "()Landroid/media/SoundPool;", "state", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$State;", "getState", "()Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$State;", "setState", "(Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$State;)V", "handleCommand", "", "command", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCommand;", "initialize", "selectAudioDevice", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "device", "setDefaultAudioDevice", "newDefaultDevice", "clearUserEarpieceSelection", "", "shutdown", "silenceIncomingRinger", NotificationProfileDatabase.NotificationProfileScheduleTable.START, "startIncomingRinger", "ringtoneUri", "Landroid/net/Uri;", "vibrate", "startOutgoingRinger", "stop", "playDisconnect", "AudioDevice", "Companion", "EventListener", "State", "Lorg/thoughtcrime/securesms/webrtc/audio/FullSignalAudioManager;", "Lorg/thoughtcrime/securesms/webrtc/audio/TelecomAwareSignalAudioManager;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes5.dex */
public abstract class SignalAudioManager {
    public static final Companion Companion = new Companion(null);
    private final AudioManagerCompat androidAudioManager;
    private HandlerThread commandAndControlThread;
    private final int connectedSoundId;
    private final Context context;
    private final int disconnectedSoundId;
    private final EventListener eventListener;
    private final SignalAudioHandler handler;
    private final IncomingRinger incomingRinger;
    private final OutgoingRinger outgoingRinger;
    private AudioDevice selectedAudioDevice;
    private final SoundPool soundPool;
    private State state;

    /* compiled from: SignalAudioManager.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$AudioDevice;", "", "(Ljava/lang/String;I)V", "SPEAKER_PHONE", "WIRED_HEADSET", "EARPIECE", "BLUETOOTH", "NONE", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public enum AudioDevice {
        SPEAKER_PHONE,
        WIRED_HEADSET,
        EARPIECE,
        BLUETOOTH,
        NONE
    }

    /* compiled from: SignalAudioManager.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u001e\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0007H'J\b\u0010\b\u001a\u00020\u0003H&¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$EventListener;", "", "onAudioDeviceChanged", "", "activeDevice", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$AudioDevice;", "devices", "", "onBluetoothPermissionDenied", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public interface EventListener {
        void onAudioDeviceChanged(AudioDevice audioDevice, Set<AudioDevice> set);

        void onBluetoothPermissionDenied();
    }

    /* compiled from: SignalAudioManager.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$State;", "", "(Ljava/lang/String;I)V", "UNINITIALIZED", "PREINITIALIZED", "RUNNING", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public enum State {
        UNINITIALIZED,
        PREINITIALIZED,
        RUNNING
    }

    public /* synthetic */ SignalAudioManager(Context context, EventListener eventListener, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, eventListener);
    }

    @JvmStatic
    public static final SignalAudioManager create(Context context, EventListener eventListener, boolean z) {
        return Companion.create(context, eventListener, z);
    }

    protected abstract void initialize();

    protected abstract void selectAudioDevice(RecipientId recipientId, AudioDevice audioDevice);

    protected abstract void setDefaultAudioDevice(RecipientId recipientId, AudioDevice audioDevice, boolean z);

    protected abstract void start();

    protected abstract void startIncomingRinger(Uri uri, boolean z);

    protected abstract void startOutgoingRinger();

    protected abstract void stop(boolean z);

    private SignalAudioManager(Context context, EventListener eventListener) {
        this.context = context;
        this.eventListener = eventListener;
        this.commandAndControlThread = SignalExecutors.getAndStartHandlerThread("call-audio");
        Looper looper = this.commandAndControlThread.getLooper();
        Intrinsics.checkNotNullExpressionValue(looper, "commandAndControlThread.looper");
        this.handler = new SignalAudioHandler(looper);
        this.state = State.UNINITIALIZED;
        AudioManagerCompat androidCallAudioManager = ApplicationDependencies.getAndroidCallAudioManager();
        Intrinsics.checkNotNullExpressionValue(androidCallAudioManager, "getAndroidCallAudioManager()");
        this.androidAudioManager = androidCallAudioManager;
        this.selectedAudioDevice = AudioDevice.NONE;
        SoundPool createSoundPool = androidCallAudioManager.createSoundPool();
        Intrinsics.checkNotNullExpressionValue(createSoundPool, "androidAudioManager.createSoundPool()");
        this.soundPool = createSoundPool;
        this.connectedSoundId = createSoundPool.load(context, R.raw.webrtc_completed, 1);
        this.disconnectedSoundId = createSoundPool.load(context, R.raw.webrtc_disconnected, 1);
        this.incomingRinger = new IncomingRinger(context);
        this.outgoingRinger = new OutgoingRinger(context);
    }

    public final Context getContext() {
        return this.context;
    }

    public final EventListener getEventListener() {
        return this.eventListener;
    }

    public final SignalAudioHandler getHandler() {
        return this.handler;
    }

    public final State getState() {
        return this.state;
    }

    public final void setState(State state) {
        Intrinsics.checkNotNullParameter(state, "<set-?>");
        this.state = state;
    }

    public final AudioManagerCompat getAndroidAudioManager() {
        return this.androidAudioManager;
    }

    public final AudioDevice getSelectedAudioDevice() {
        return this.selectedAudioDevice;
    }

    public final void setSelectedAudioDevice(AudioDevice audioDevice) {
        Intrinsics.checkNotNullParameter(audioDevice, "<set-?>");
        this.selectedAudioDevice = audioDevice;
    }

    public final SoundPool getSoundPool() {
        return this.soundPool;
    }

    public final int getConnectedSoundId() {
        return this.connectedSoundId;
    }

    public final int getDisconnectedSoundId() {
        return this.disconnectedSoundId;
    }

    public final IncomingRinger getIncomingRinger() {
        return this.incomingRinger;
    }

    public final OutgoingRinger getOutgoingRinger() {
        return this.outgoingRinger;
    }

    /* compiled from: SignalAudioManager.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\u0006\u0010\t\u001a\u00020\nH\u0007¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$Companion;", "", "()V", "create", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager;", "context", "Landroid/content/Context;", "eventListener", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$EventListener;", "isGroup", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final SignalAudioManager create(Context context, EventListener eventListener, boolean z) {
            Intrinsics.checkNotNullParameter(context, "context");
            if (!AndroidTelecomUtil.getTelecomSupported() || z) {
                return new FullSignalAudioManager(context, eventListener);
            }
            return new TelecomAwareSignalAudioManager(context, eventListener);
        }
    }

    public final void handleCommand(AudioManagerCommand audioManagerCommand) {
        Intrinsics.checkNotNullParameter(audioManagerCommand, "command");
        this.handler.post(new Runnable(this) { // from class: org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager$$ExternalSyntheticLambda0
            public final /* synthetic */ SignalAudioManager f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SignalAudioManager.m3326handleCommand$lambda0(AudioManagerCommand.this, this.f$1);
            }
        });
    }

    /* renamed from: handleCommand$lambda-0 */
    public static final void m3326handleCommand$lambda0(AudioManagerCommand audioManagerCommand, SignalAudioManager signalAudioManager) {
        Intrinsics.checkNotNullParameter(audioManagerCommand, "$command");
        Intrinsics.checkNotNullParameter(signalAudioManager, "this$0");
        if (audioManagerCommand instanceof AudioManagerCommand.Initialize) {
            signalAudioManager.initialize();
        } else if (audioManagerCommand instanceof AudioManagerCommand.Start) {
            signalAudioManager.start();
        } else if (audioManagerCommand instanceof AudioManagerCommand.Stop) {
            signalAudioManager.stop(((AudioManagerCommand.Stop) audioManagerCommand).getPlayDisconnect());
        } else if (audioManagerCommand instanceof AudioManagerCommand.SetDefaultDevice) {
            AudioManagerCommand.SetDefaultDevice setDefaultDevice = (AudioManagerCommand.SetDefaultDevice) audioManagerCommand;
            signalAudioManager.setDefaultAudioDevice(setDefaultDevice.getRecipientId(), setDefaultDevice.getDevice(), setDefaultDevice.getClearUserEarpieceSelection());
        } else if (audioManagerCommand instanceof AudioManagerCommand.SetUserDevice) {
            AudioManagerCommand.SetUserDevice setUserDevice = (AudioManagerCommand.SetUserDevice) audioManagerCommand;
            signalAudioManager.selectAudioDevice(setUserDevice.getRecipientId(), setUserDevice.getDevice());
        } else if (audioManagerCommand instanceof AudioManagerCommand.StartIncomingRinger) {
            AudioManagerCommand.StartIncomingRinger startIncomingRinger = (AudioManagerCommand.StartIncomingRinger) audioManagerCommand;
            signalAudioManager.startIncomingRinger(startIncomingRinger.getRingtoneUri(), startIncomingRinger.getVibrate());
        } else if (audioManagerCommand instanceof AudioManagerCommand.SilenceIncomingRinger) {
            signalAudioManager.silenceIncomingRinger();
        } else if (audioManagerCommand instanceof AudioManagerCommand.StartOutgoingRinger) {
            signalAudioManager.startOutgoingRinger();
        }
    }

    public final void shutdown() {
        this.handler.post(new Runnable() { // from class: org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                SignalAudioManager.m3327shutdown$lambda1(SignalAudioManager.this);
            }
        });
    }

    /* renamed from: shutdown$lambda-1 */
    public static final void m3327shutdown$lambda1(SignalAudioManager signalAudioManager) {
        Intrinsics.checkNotNullParameter(signalAudioManager, "this$0");
        signalAudioManager.stop(false);
        if (signalAudioManager.commandAndControlThread != null) {
            Log.i(SignalAudioManagerKt.TAG, "Shutting down command and control");
            signalAudioManager.commandAndControlThread.quitSafely();
            signalAudioManager.commandAndControlThread = null;
        }
    }

    protected void silenceIncomingRinger() {
        Log.i(SignalAudioManagerKt.TAG, "silenceIncomingRinger():");
        this.incomingRinger.stop();
    }
}
