package org.thoughtcrime.securesms.webrtc.audio;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.util.ContextExtensionsKt;

/* compiled from: SignalBluetoothManager.kt */
@Metadata(d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0012\b\u0007\u0018\u0000 32\u00020\u0001:\u00041234B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010\u001f\u001a\u00020\u0017H\u0002J\u0018\u0010 \u001a\u00020\u00172\u0006\u0010!\u001a\u00020\u00192\u0006\u0010\"\u001a\u00020#H\u0002J\b\u0010$\u001a\u00020\u0017H\u0002J\u0010\u0010%\u001a\u00020\u00172\u0006\u0010&\u001a\u00020\u0019H\u0002J\u0012\u0010'\u001a\u00020\u00172\b\u0010(\u001a\u0004\u0018\u00010\u0010H\u0002J\b\u0010)\u001a\u00020\u0017H\u0002J\u0006\u0010*\u001a\u00020\u0017J\u0006\u0010+\u001a\u00020#J\b\u0010,\u001a\u00020\u0017H\u0002J\u0006\u0010-\u001a\u00020\u0017J\u0006\u0010.\u001a\u00020\u0017J\b\u0010/\u001a\u00020\u0017H\u0002J\u0006\u00100\u001a\u00020\u0017R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\u0011\u001a\u00060\u0012R\u00020\u0000X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\b\u0018\u00010\u0014R\u00020\u0000X\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00170\u0016X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u000e¢\u0006\u0002\n\u0000R \u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u001a\u001a\u00020\u001b8F@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001e¨\u00065"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/SignalBluetoothManager;", "", "context", "Landroid/content/Context;", "audioManager", "Lorg/thoughtcrime/securesms/webrtc/audio/FullSignalAudioManager;", "handler", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioHandler;", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/webrtc/audio/FullSignalAudioManager;Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioHandler;)V", "androidAudioManager", "Lorg/thoughtcrime/securesms/webrtc/audio/AudioManagerCompat;", "bluetoothAdapter", "Landroid/bluetooth/BluetoothAdapter;", "bluetoothDevice", "Landroid/bluetooth/BluetoothDevice;", "bluetoothHeadset", "Landroid/bluetooth/BluetoothHeadset;", "bluetoothListener", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalBluetoothManager$BluetoothServiceListener;", "bluetoothReceiver", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalBluetoothManager$BluetoothHeadsetBroadcastReceiver;", "bluetoothTimeout", "Lkotlin/Function0;", "", "scoConnectionAttempts", "", "<set-?>", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalBluetoothManager$State;", "state", "getState", "()Lorg/thoughtcrime/securesms/webrtc/audio/SignalBluetoothManager$State;", "cancelTimer", "onAudioStateChanged", "audioState", "isInitialStateChange", "", "onBluetoothTimeout", "onHeadsetConnectionStateChanged", "connectionState", "onServiceConnected", "proxy", "onServiceDisconnected", NotificationProfileDatabase.NotificationProfileScheduleTable.START, "startScoAudio", "startTimer", "stop", "stopScoAudio", "updateAudioDeviceState", "updateDevice", "BluetoothHeadsetBroadcastReceiver", "BluetoothServiceListener", "Companion", "State", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes5.dex */
public final class SignalBluetoothManager {
    public static final Companion Companion = new Companion(null);
    private static final int MAX_CONNECTION_ATTEMPTS;
    private static final long SCO_TIMEOUT = TimeUnit.SECONDS.toMillis(4);
    private static final String TAG = Log.tag(SignalBluetoothManager.class);
    private final AudioManagerCompat androidAudioManager;
    private final FullSignalAudioManager audioManager;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothDevice bluetoothDevice;
    private BluetoothHeadset bluetoothHeadset;
    private final BluetoothServiceListener bluetoothListener;
    private BluetoothHeadsetBroadcastReceiver bluetoothReceiver;
    private final Function0<Unit> bluetoothTimeout;
    private final Context context;
    private final SignalAudioHandler handler;
    private int scoConnectionAttempts;
    private State state = State.UNINITIALIZED;

    public SignalBluetoothManager(Context context, FullSignalAudioManager fullSignalAudioManager, SignalAudioHandler signalAudioHandler) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(fullSignalAudioManager, "audioManager");
        Intrinsics.checkNotNullParameter(signalAudioHandler, "handler");
        this.context = context;
        this.audioManager = fullSignalAudioManager;
        this.handler = signalAudioHandler;
        AudioManagerCompat androidCallAudioManager = ApplicationDependencies.getAndroidCallAudioManager();
        Intrinsics.checkNotNullExpressionValue(androidCallAudioManager, "getAndroidCallAudioManager()");
        this.androidAudioManager = androidCallAudioManager;
        this.bluetoothListener = new BluetoothServiceListener();
        this.bluetoothTimeout = new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.webrtc.audio.SignalBluetoothManager$bluetoothTimeout$1
            final /* synthetic */ SignalBluetoothManager this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                this.this$0.onBluetoothTimeout();
            }
        };
    }

    public final State getState() {
        this.handler.assertHandlerThread();
        return this.state;
    }

    public final void start() {
        this.handler.assertHandlerThread();
        String str = TAG;
        Log.d(str, "start(): " + getState());
        if (getState() != State.UNINITIALIZED) {
            Log.w(str, "Invalid starting state");
            return;
        }
        String str2 = null;
        this.bluetoothHeadset = null;
        this.bluetoothDevice = null;
        boolean z = false;
        this.scoConnectionAttempts = 0;
        BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        this.bluetoothAdapter = defaultAdapter;
        if (defaultAdapter == null) {
            Log.i(str, "Device does not support Bluetooth");
        } else if (!this.androidAudioManager.isBluetoothScoAvailableOffCall()) {
            Log.w(str, "Bluetooth SCO audio is not available off call");
        } else {
            BluetoothAdapter bluetoothAdapter = this.bluetoothAdapter;
            if (bluetoothAdapter != null && bluetoothAdapter.getProfileProxy(this.context, this.bluetoothListener, 1)) {
                z = true;
            }
            if (!z) {
                Log.e(str, "BluetoothAdapter.getProfileProxy(HEADSET) failed");
                return;
            }
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction("android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED");
            intentFilter.addAction("android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED");
            BluetoothHeadsetBroadcastReceiver bluetoothHeadsetBroadcastReceiver = new BluetoothHeadsetBroadcastReceiver();
            this.bluetoothReceiver = bluetoothHeadsetBroadcastReceiver;
            this.context.registerReceiver(bluetoothHeadsetBroadcastReceiver, intentFilter);
            StringBuilder sb = new StringBuilder();
            sb.append("Headset profile state: ");
            BluetoothAdapter bluetoothAdapter2 = this.bluetoothAdapter;
            if (bluetoothAdapter2 != null) {
                str2 = SignalBluetoothManagerKt.toStateString(bluetoothAdapter2.getProfileConnectionState(1));
            }
            sb.append(str2);
            Log.i(str, sb.toString());
            Log.i(str, "Bluetooth proxy for headset profile has started");
            this.state = State.UNAVAILABLE;
        }
    }

    public final void stop() {
        this.handler.assertHandlerThread();
        String str = TAG;
        Log.d(str, "stop(): state: " + getState());
        if (this.bluetoothAdapter != null) {
            stopScoAudio();
            ContextExtensionsKt.safeUnregisterReceiver(this.context, this.bluetoothReceiver);
            this.bluetoothReceiver = null;
            cancelTimer();
            BluetoothHeadset bluetoothHeadset = this.bluetoothHeadset;
            if (bluetoothHeadset != null) {
                BluetoothAdapter bluetoothAdapter = this.bluetoothAdapter;
                if (bluetoothAdapter != null) {
                    bluetoothAdapter.closeProfileProxy(1, bluetoothHeadset);
                }
                this.bluetoothHeadset = null;
            }
            this.bluetoothAdapter = null;
            this.bluetoothDevice = null;
            this.state = State.UNINITIALIZED;
        }
    }

    public final boolean startScoAudio() {
        this.handler.assertHandlerThread();
        String str = TAG;
        Log.i(str, "startScoAudio(): " + getState() + " attempts: " + this.scoConnectionAttempts);
        if (this.scoConnectionAttempts >= 2) {
            Log.w(str, "SCO connection attempts maxed out");
            return false;
        } else if (getState() != State.AVAILABLE) {
            Log.w(str, "SCO connection failed as no headset available");
            return false;
        } else {
            this.state = State.CONNECTING;
            this.androidAudioManager.startBluetoothSco();
            this.androidAudioManager.setBluetoothScoOn(true);
            this.scoConnectionAttempts++;
            startTimer();
            return true;
        }
    }

    public final void stopScoAudio() {
        this.handler.assertHandlerThread();
        String str = TAG;
        Log.i(str, "stopScoAudio(): " + getState());
        if (getState() == State.CONNECTING || getState() == State.CONNECTED) {
            cancelTimer();
            this.androidAudioManager.stopBluetoothSco();
            this.androidAudioManager.setBluetoothScoOn(false);
            this.state = State.DISCONNECTING;
        }
    }

    public final void updateDevice() {
        BluetoothHeadset bluetoothHeadset;
        List<BluetoothDevice> connectedDevices;
        this.handler.assertHandlerThread();
        String str = TAG;
        Log.d(str, "updateDevice(): state: " + getState());
        if (getState() != State.UNINITIALIZED && (bluetoothHeadset = this.bluetoothHeadset) != null) {
            Boolean bool = null;
            if (bluetoothHeadset != null) {
                try {
                    connectedDevices = bluetoothHeadset.getConnectedDevices();
                } catch (SecurityException e) {
                    Log.w(TAG, "Unable to get bluetooth devices", e);
                    stop();
                    this.state = State.PERMISSION_DENIED;
                    return;
                }
            } else {
                connectedDevices = null;
            }
            if (connectedDevices == null || connectedDevices.isEmpty()) {
                this.bluetoothDevice = null;
                this.state = State.UNAVAILABLE;
                Log.i(str, "No connected bluetooth headset");
                return;
            }
            this.bluetoothDevice = connectedDevices.get(0);
            this.state = State.AVAILABLE;
            StringBuilder sb = new StringBuilder();
            sb.append("Connected bluetooth headset. headsetState: ");
            BluetoothHeadset bluetoothHeadset2 = this.bluetoothHeadset;
            sb.append(bluetoothHeadset2 != null ? SignalBluetoothManagerKt.toStateString(bluetoothHeadset2.getConnectionState(this.bluetoothDevice)) : null);
            sb.append(" scoAudio: ");
            BluetoothHeadset bluetoothHeadset3 = this.bluetoothHeadset;
            if (bluetoothHeadset3 != null) {
                bool = Boolean.valueOf(bluetoothHeadset3.isAudioConnected(this.bluetoothDevice));
            }
            sb.append(bool);
            Log.i(str, sb.toString());
        }
    }

    private final void updateAudioDeviceState() {
        this.audioManager.updateAudioDeviceState();
    }

    private final void startTimer() {
        this.handler.postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.webrtc.audio.SignalBluetoothManager$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                SignalBluetoothManager.m3329startTimer$lambda1(Function0.this);
            }
        }, SCO_TIMEOUT);
    }

    /* renamed from: startTimer$lambda-1 */
    public static final void m3329startTimer$lambda1(Function0 function0) {
        Intrinsics.checkNotNullParameter(function0, "$tmp0");
        function0.invoke();
    }

    private final void cancelTimer() {
        this.handler.removeCallbacks(new Runnable() { // from class: org.thoughtcrime.securesms.webrtc.audio.SignalBluetoothManager$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                SignalBluetoothManager.m3328cancelTimer$lambda2(Function0.this);
            }
        });
    }

    /* renamed from: cancelTimer$lambda-2 */
    public static final void m3328cancelTimer$lambda2(Function0 function0) {
        Intrinsics.checkNotNullParameter(function0, "$tmp0");
        function0.invoke();
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0096  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onBluetoothTimeout() {
        /*
            r5 = this;
            java.lang.String r0 = org.thoughtcrime.securesms.webrtc.audio.SignalBluetoothManager.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "onBluetoothTimeout: state: "
            r1.append(r2)
            org.thoughtcrime.securesms.webrtc.audio.SignalBluetoothManager$State r2 = r5.getState()
            r1.append(r2)
            java.lang.String r2 = " bluetoothHeadset: "
            r1.append(r2)
            android.bluetooth.BluetoothHeadset r2 = r5.bluetoothHeadset
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            org.signal.core.util.logging.Log.i(r0, r1)
            org.thoughtcrime.securesms.webrtc.audio.SignalBluetoothManager$State r1 = r5.getState()
            org.thoughtcrime.securesms.webrtc.audio.SignalBluetoothManager$State r2 = org.thoughtcrime.securesms.webrtc.audio.SignalBluetoothManager.State.UNINITIALIZED
            if (r1 == r2) goto L_0x00ad
            android.bluetooth.BluetoothHeadset r1 = r5.bluetoothHeadset
            if (r1 == 0) goto L_0x00ad
            org.thoughtcrime.securesms.webrtc.audio.SignalBluetoothManager$State r1 = r5.getState()
            org.thoughtcrime.securesms.webrtc.audio.SignalBluetoothManager$State r2 = org.thoughtcrime.securesms.webrtc.audio.SignalBluetoothManager.State.CONNECTING
            if (r1 == r2) goto L_0x003a
            goto L_0x00ad
        L_0x003a:
            android.bluetooth.BluetoothHeadset r1 = r5.bluetoothHeadset
            if (r1 == 0) goto L_0x0043
            java.util.List r1 = r1.getConnectedDevices()
            goto L_0x0044
        L_0x0043:
            r1 = 0
        L_0x0044:
            r2 = 1
            r3 = 0
            if (r1 == 0) goto L_0x0093
            boolean r4 = r1.isEmpty()
            r4 = r4 ^ r2
            if (r4 == 0) goto L_0x0093
            java.lang.Object r1 = r1.get(r3)
            android.bluetooth.BluetoothDevice r1 = (android.bluetooth.BluetoothDevice) r1
            r5.bluetoothDevice = r1
            android.bluetooth.BluetoothHeadset r4 = r5.bluetoothHeadset
            if (r4 == 0) goto L_0x0063
            boolean r1 = r4.isAudioConnected(r1)
            if (r1 != r2) goto L_0x0063
            r1 = 1
            goto L_0x0064
        L_0x0063:
            r1 = 0
        L_0x0064:
            if (r1 == 0) goto L_0x007d
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r4 = "Connected with "
            r1.append(r4)
            android.bluetooth.BluetoothDevice r4 = r5.bluetoothDevice
            r1.append(r4)
            java.lang.String r1 = r1.toString()
            org.signal.core.util.logging.Log.d(r0, r1)
            goto L_0x0094
        L_0x007d:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Not connected with "
            r1.append(r2)
            android.bluetooth.BluetoothDevice r2 = r5.bluetoothDevice
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            org.signal.core.util.logging.Log.d(r0, r1)
        L_0x0093:
            r2 = 0
        L_0x0094:
            if (r2 == 0) goto L_0x00a2
            java.lang.String r1 = "Device actually connected and not timed out"
            org.signal.core.util.logging.Log.i(r0, r1)
            org.thoughtcrime.securesms.webrtc.audio.SignalBluetoothManager$State r0 = org.thoughtcrime.securesms.webrtc.audio.SignalBluetoothManager.State.CONNECTED
            r5.state = r0
            r5.scoConnectionAttempts = r3
            goto L_0x00aa
        L_0x00a2:
            java.lang.String r1 = "Failed to connect after timeout"
            org.signal.core.util.logging.Log.w(r0, r1)
            r5.stopScoAudio()
        L_0x00aa:
            r5.updateAudioDeviceState()
        L_0x00ad:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.webrtc.audio.SignalBluetoothManager.onBluetoothTimeout():void");
    }

    public final void onServiceConnected(BluetoothHeadset bluetoothHeadset) {
        this.bluetoothHeadset = bluetoothHeadset;
        updateAudioDeviceState();
    }

    public final void onServiceDisconnected() {
        stopScoAudio();
        this.bluetoothHeadset = null;
        this.bluetoothDevice = null;
        this.state = State.UNAVAILABLE;
        updateAudioDeviceState();
    }

    public final void onHeadsetConnectionStateChanged(int i) {
        String str = TAG;
        Log.i(str, "onHeadsetConnectionStateChanged: state: " + getState() + " connectionState: " + SignalBluetoothManagerKt.toStateString(i));
        if (i == 0) {
            stopScoAudio();
            updateAudioDeviceState();
        } else if (i == 2) {
            this.scoConnectionAttempts = 0;
            updateAudioDeviceState();
        }
    }

    public final void onAudioStateChanged(int i, boolean z) {
        String str = TAG;
        Log.i(str, "onAudioStateChanged: state: " + getState() + " audioState: " + SignalBluetoothManagerKt.toStateString(i) + " initialSticky: " + z);
        switch (i) {
            case 10:
                Log.d(str, "Bluetooth audio SCO is now disconnected");
                if (z) {
                    Log.d(str, "Ignore " + SignalBluetoothManagerKt.toStateString(i) + " initial sticky broadcast.");
                    return;
                }
                updateAudioDeviceState();
                return;
            case 11:
                Log.d(str, "Bluetooth audio SCO is now connecting...");
                return;
            case 12:
                cancelTimer();
                if (getState() == State.CONNECTING) {
                    Log.d(str, "Bluetooth audio SCO is now connected");
                    this.state = State.CONNECTED;
                    this.scoConnectionAttempts = 0;
                    updateAudioDeviceState();
                    return;
                }
                Log.w(str, "Unexpected state " + SignalBluetoothManagerKt.toStateString(i));
                return;
            default:
                return;
        }
    }

    /* compiled from: SignalBluetoothManager.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0016J\u0010\u0010\t\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/SignalBluetoothManager$BluetoothServiceListener;", "Landroid/bluetooth/BluetoothProfile$ServiceListener;", "(Lorg/thoughtcrime/securesms/webrtc/audio/SignalBluetoothManager;)V", "onServiceConnected", "", "profile", "", "proxy", "Landroid/bluetooth/BluetoothProfile;", "onServiceDisconnected", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public final class BluetoothServiceListener implements BluetoothProfile.ServiceListener {
        public BluetoothServiceListener() {
            SignalBluetoothManager.this = r1;
        }

        @Override // android.bluetooth.BluetoothProfile.ServiceListener
        public void onServiceConnected(int i, BluetoothProfile bluetoothProfile) {
            if (i == 1) {
                SignalBluetoothManager.this.handler.post(new SignalBluetoothManager$BluetoothServiceListener$$ExternalSyntheticLambda1(SignalBluetoothManager.this, bluetoothProfile));
            }
        }

        /* renamed from: onServiceConnected$lambda-0 */
        public static final void m3335onServiceConnected$lambda0(SignalBluetoothManager signalBluetoothManager, BluetoothProfile bluetoothProfile) {
            Intrinsics.checkNotNullParameter(signalBluetoothManager, "this$0");
            if (signalBluetoothManager.getState() != State.UNINITIALIZED) {
                signalBluetoothManager.onServiceConnected(bluetoothProfile instanceof BluetoothHeadset ? (BluetoothHeadset) bluetoothProfile : null);
            }
        }

        @Override // android.bluetooth.BluetoothProfile.ServiceListener
        public void onServiceDisconnected(int i) {
            if (i == 1) {
                SignalBluetoothManager.this.handler.post(new SignalBluetoothManager$BluetoothServiceListener$$ExternalSyntheticLambda0(SignalBluetoothManager.this));
            }
        }

        /* renamed from: onServiceDisconnected$lambda-1 */
        public static final void m3336onServiceDisconnected$lambda1(SignalBluetoothManager signalBluetoothManager) {
            Intrinsics.checkNotNullParameter(signalBluetoothManager, "this$0");
            if (signalBluetoothManager.getState() != State.UNINITIALIZED) {
                signalBluetoothManager.onServiceDisconnected();
            }
        }
    }

    /* compiled from: SignalBluetoothManager.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/SignalBluetoothManager$BluetoothHeadsetBroadcastReceiver;", "Landroid/content/BroadcastReceiver;", "(Lorg/thoughtcrime/securesms/webrtc/audio/SignalBluetoothManager;)V", "onReceive", "", "context", "Landroid/content/Context;", "intent", "Landroid/content/Intent;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public final class BluetoothHeadsetBroadcastReceiver extends BroadcastReceiver {
        public BluetoothHeadsetBroadcastReceiver() {
            SignalBluetoothManager.this = r1;
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(intent, "intent");
            if (Intrinsics.areEqual(intent.getAction(), "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED")) {
                SignalBluetoothManager.this.handler.post(new SignalBluetoothManager$BluetoothHeadsetBroadcastReceiver$$ExternalSyntheticLambda0(SignalBluetoothManager.this, intent.getIntExtra("android.bluetooth.profile.extra.STATE", 0)));
            } else if (Intrinsics.areEqual(intent.getAction(), "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED")) {
                SignalBluetoothManager.this.handler.post(new SignalBluetoothManager$BluetoothHeadsetBroadcastReceiver$$ExternalSyntheticLambda1(SignalBluetoothManager.this, intent.getIntExtra("android.bluetooth.profile.extra.STATE", 10), this));
            }
        }

        /* renamed from: onReceive$lambda-0 */
        public static final void m3331onReceive$lambda0(SignalBluetoothManager signalBluetoothManager, int i) {
            Intrinsics.checkNotNullParameter(signalBluetoothManager, "this$0");
            if (signalBluetoothManager.getState() != State.UNINITIALIZED) {
                signalBluetoothManager.onHeadsetConnectionStateChanged(i);
            }
        }

        /* renamed from: onReceive$lambda-1 */
        public static final void m3332onReceive$lambda1(SignalBluetoothManager signalBluetoothManager, int i, BluetoothHeadsetBroadcastReceiver bluetoothHeadsetBroadcastReceiver) {
            Intrinsics.checkNotNullParameter(signalBluetoothManager, "this$0");
            Intrinsics.checkNotNullParameter(bluetoothHeadsetBroadcastReceiver, "this$1");
            if (signalBluetoothManager.getState() != State.UNINITIALIZED) {
                signalBluetoothManager.onAudioStateChanged(i, bluetoothHeadsetBroadcastReceiver.isInitialStickyBroadcast());
            }
        }
    }

    /* compiled from: SignalBluetoothManager.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004J\u0006\u0010\u0005\u001a\u00020\u0004j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\r¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/SignalBluetoothManager$State;", "", "(Ljava/lang/String;I)V", "hasDevice", "", "shouldUpdate", "UNINITIALIZED", "UNAVAILABLE", "AVAILABLE", "DISCONNECTING", "CONNECTING", "CONNECTED", "PERMISSION_DENIED", "ERROR", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public enum State {
        UNINITIALIZED,
        UNAVAILABLE,
        AVAILABLE,
        DISCONNECTING,
        CONNECTING,
        CONNECTED,
        PERMISSION_DENIED,
        ERROR;

        public final boolean shouldUpdate() {
            return this == AVAILABLE || this == UNAVAILABLE || this == DISCONNECTING;
        }

        public final boolean hasDevice() {
            return this == CONNECTED || this == CONNECTING || this == AVAILABLE;
        }
    }

    /* compiled from: SignalBluetoothManager.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\n \t*\u0004\u0018\u00010\b0\bX\u0004¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/webrtc/audio/SignalBluetoothManager$Companion;", "", "()V", "MAX_CONNECTION_ATTEMPTS", "", "SCO_TIMEOUT", "", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
