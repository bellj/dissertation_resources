package org.thoughtcrime.securesms.linkpreview;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.util.DateUtils;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class LinkPreviewUtil$OpenGraph$$ExternalSyntheticLambda0 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return Long.valueOf(DateUtils.parseIso8601((String) obj));
    }
}
