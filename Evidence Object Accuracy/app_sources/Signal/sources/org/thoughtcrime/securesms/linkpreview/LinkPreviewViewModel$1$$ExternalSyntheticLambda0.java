package org.thoughtcrime.securesms.linkpreview;

import org.thoughtcrime.securesms.linkpreview.LinkPreviewRepository;
import org.thoughtcrime.securesms.linkpreview.LinkPreviewViewModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class LinkPreviewViewModel$1$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ LinkPreviewViewModel.AnonymousClass1 f$0;
    public final /* synthetic */ LinkPreviewRepository.Error f$1;

    public /* synthetic */ LinkPreviewViewModel$1$$ExternalSyntheticLambda0(LinkPreviewViewModel.AnonymousClass1 r1, LinkPreviewRepository.Error error) {
        this.f$0 = r1;
        this.f$1 = error;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$onError$1(this.f$1);
    }
}
