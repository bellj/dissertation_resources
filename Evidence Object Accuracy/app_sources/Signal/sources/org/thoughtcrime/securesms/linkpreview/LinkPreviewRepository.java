package org.thoughtcrime.securesms.linkpreview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.core.util.Consumer;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import j$.util.Optional;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.ExecutionException;
import okhttp3.CacheControl;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.signal.core.util.Hex;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.util.Pair;
import org.signal.libsignal.zkgroup.VerificationFailedException;
import org.signal.libsignal.zkgroup.groups.GroupMasterKey;
import org.signal.storageservice.protos.groups.local.DecryptedGroupJoinInfo;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.UriAttachment;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.groups.v2.GroupInviteLinkUrl;
import org.thoughtcrime.securesms.jobs.AvatarGroupsV2DownloadJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.linkpreview.LinkPreviewRepository;
import org.thoughtcrime.securesms.linkpreview.LinkPreviewUtil;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.PushMediaConstraints;
import org.thoughtcrime.securesms.net.CallRequestController;
import org.thoughtcrime.securesms.net.CompositeRequestController;
import org.thoughtcrime.securesms.net.RequestController;
import org.thoughtcrime.securesms.net.UserAgentInterceptor;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.stickers.StickerRemoteUri;
import org.thoughtcrime.securesms.stickers.StickerUrl;
import org.thoughtcrime.securesms.util.AvatarUtil;
import org.thoughtcrime.securesms.util.BitmapDecodingException;
import org.thoughtcrime.securesms.util.ByteUnit;
import org.thoughtcrime.securesms.util.ImageCompressionUtil;
import org.thoughtcrime.securesms.util.LinkUtil;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.OkHttpUtil;
import org.whispersystems.signalservice.api.groupsv2.GroupLinkNotActiveException;
import org.whispersystems.signalservice.api.messages.SignalServiceStickerManifest;
import org.whispersystems.signalservice.api.util.OptionalUtil;

/* loaded from: classes4.dex */
public class LinkPreviewRepository {
    private static final long FAILSAFE_MAX_IMAGE_SIZE;
    private static final long FAILSAFE_MAX_TEXT_SIZE;
    private static final CacheControl NO_CACHE = new CacheControl.Builder().noCache().build();
    private static final String TAG = Log.tag(LinkPreviewRepository.class);
    private final OkHttpClient client = new OkHttpClient.Builder().cache(null).addInterceptor(new UserAgentInterceptor("WhatsApp/2")).build();

    /* loaded from: classes4.dex */
    public interface Callback {
        void onError(Error error);

        void onSuccess(LinkPreview linkPreview);
    }

    /* loaded from: classes4.dex */
    public enum Error {
        PREVIEW_NOT_AVAILABLE,
        GROUP_LINK_INACTIVE
    }

    static {
        TAG = Log.tag(LinkPreviewRepository.class);
        NO_CACHE = new CacheControl.Builder().noCache().build();
        ByteUnit byteUnit = ByteUnit.MEGABYTES;
        FAILSAFE_MAX_TEXT_SIZE = byteUnit.toBytes(2);
        FAILSAFE_MAX_IMAGE_SIZE = byteUnit.toBytes(2);
    }

    public RequestController getLinkPreview(Context context, String str, Callback callback) {
        RequestController requestController;
        if (SignalStore.settings().isLinkPreviewsEnabled()) {
            CompositeRequestController compositeRequestController = new CompositeRequestController();
            if (!LinkUtil.isValidPreviewUrl(str)) {
                Log.w(TAG, "Tried to get a link preview for a non-whitelisted domain.");
                callback.onError(Error.PREVIEW_NOT_AVAILABLE);
                return compositeRequestController;
            }
            if (StickerUrl.isValidShareLink(str)) {
                requestController = fetchStickerPackLinkPreview(context, str, callback);
            } else if (GroupInviteLinkUrl.isGroupLink(str)) {
                requestController = fetchGroupLinkPreview(context, str, callback);
            } else {
                requestController = fetchMetadata(str, new Consumer(callback, str, compositeRequestController) { // from class: org.thoughtcrime.securesms.linkpreview.LinkPreviewRepository$$ExternalSyntheticLambda2
                    public final /* synthetic */ LinkPreviewRepository.Callback f$1;
                    public final /* synthetic */ String f$2;
                    public final /* synthetic */ CompositeRequestController f$3;

                    {
                        this.f$1 = r2;
                        this.f$2 = r3;
                        this.f$3 = r4;
                    }

                    @Override // androidx.core.util.Consumer
                    public final void accept(Object obj) {
                        LinkPreviewRepository.this.lambda$getLinkPreview$1(this.f$1, this.f$2, this.f$3, (LinkPreviewRepository.Metadata) obj);
                    }
                });
            }
            compositeRequestController.addController(requestController);
            return compositeRequestController;
        }
        throw new IllegalStateException();
    }

    public /* synthetic */ void lambda$getLinkPreview$1(Callback callback, String str, CompositeRequestController compositeRequestController, Metadata metadata) {
        if (metadata.isEmpty()) {
            callback.onError(Error.PREVIEW_NOT_AVAILABLE);
        } else if (!metadata.getImageUrl().isPresent()) {
            callback.onSuccess(new LinkPreview(str, metadata.getTitle().orElse(""), metadata.getDescription().orElse(""), metadata.getDate(), Optional.empty()));
        } else {
            compositeRequestController.addController(fetchThumbnail(metadata.getImageUrl().get(), new Consumer(callback, str) { // from class: org.thoughtcrime.securesms.linkpreview.LinkPreviewRepository$$ExternalSyntheticLambda0
                public final /* synthetic */ LinkPreviewRepository.Callback f$1;
                public final /* synthetic */ String f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // androidx.core.util.Consumer
                public final void accept(Object obj) {
                    LinkPreviewRepository.lambda$getLinkPreview$0(LinkPreviewRepository.Metadata.this, this.f$1, this.f$2, (Optional) obj);
                }
            }));
        }
    }

    public static /* synthetic */ void lambda$getLinkPreview$0(Metadata metadata, Callback callback, String str, Optional optional) {
        if (metadata.getTitle().isPresent() || optional.isPresent()) {
            callback.onSuccess(new LinkPreview(str, metadata.getTitle().orElse(""), metadata.getDescription().orElse(""), metadata.getDate(), optional));
        } else {
            callback.onError(Error.PREVIEW_NOT_AVAILABLE);
        }
    }

    private RequestController fetchMetadata(String str, final Consumer<Metadata> consumer) {
        Call newCall = this.client.newCall(new Request.Builder().url(str).cacheControl(NO_CACHE).build());
        newCall.enqueue(new okhttp3.Callback() { // from class: org.thoughtcrime.securesms.linkpreview.LinkPreviewRepository.1
            @Override // okhttp3.Callback
            public void onFailure(Call call, IOException iOException) {
                Log.w(LinkPreviewRepository.TAG, "Request failed.", iOException);
                consumer.accept(Metadata.empty());
            }

            @Override // okhttp3.Callback
            public void onResponse(Call call, Response response) throws IOException {
                Optional<String> optional;
                if (!response.isSuccessful()) {
                    String str2 = LinkPreviewRepository.TAG;
                    Log.w(str2, "Non-successful response. Code: " + response.code());
                    consumer.accept(Metadata.empty());
                } else if (response.body() == null) {
                    Log.w(LinkPreviewRepository.TAG, "No response body.");
                    consumer.accept(Metadata.empty());
                } else {
                    LinkPreviewUtil.OpenGraph parseOpenGraphFields = LinkPreviewUtil.parseOpenGraphFields(OkHttpUtil.readAsString(response.body(), LinkPreviewRepository.FAILSAFE_MAX_TEXT_SIZE));
                    Optional<String> title = parseOpenGraphFields.getTitle();
                    Optional<String> description = parseOpenGraphFields.getDescription();
                    Optional<String> imageUrl = parseOpenGraphFields.getImageUrl();
                    long date = parseOpenGraphFields.getDate();
                    if (!imageUrl.isPresent() || LinkUtil.isValidPreviewUrl(imageUrl.get())) {
                        optional = imageUrl;
                    } else {
                        Log.i(LinkPreviewRepository.TAG, "Image URL was invalid or for a non-whitelisted domain. Skipping.");
                        optional = Optional.empty();
                    }
                    consumer.accept(new Metadata(title, description, date, optional));
                }
            }
        });
        return new CallRequestController(newCall);
    }

    private RequestController fetchThumbnail(String str, Consumer<Optional<Attachment>> consumer) {
        Call newCall = this.client.newCall(new Request.Builder().url(str).build());
        CallRequestController callRequestController = new CallRequestController(newCall);
        SignalExecutors.UNBOUNDED.execute(new Runnable(callRequestController, consumer) { // from class: org.thoughtcrime.securesms.linkpreview.LinkPreviewRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ CallRequestController f$1;
            public final /* synthetic */ Consumer f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                LinkPreviewRepository.lambda$fetchThumbnail$2(Call.this, this.f$1, this.f$2);
            }
        });
        return callRequestController;
    }

    public static /* synthetic */ void lambda$fetchThumbnail$2(Call call, CallRequestController callRequestController, Consumer consumer) {
        try {
            Response execute = call.execute();
            if (execute.isSuccessful() && execute.body() != null) {
                InputStream byteStream = execute.body().byteStream();
                callRequestController.setStream(byteStream);
                byte[] readAsBytes = OkHttpUtil.readAsBytes(byteStream, FAILSAFE_MAX_IMAGE_SIZE);
                int i = 0;
                Bitmap decodeByteArray = BitmapFactory.decodeByteArray(readAsBytes, 0, readAsBytes.length);
                Optional empty = Optional.empty();
                PushMediaConstraints.MediaConfig mediaConfig = PushMediaConstraints.MediaConfig.getDefault(ApplicationDependencies.getApplication());
                if (decodeByteArray != null) {
                    int[] imageSizeTargets = mediaConfig.getImageSizeTargets();
                    int length = imageSizeTargets.length;
                    while (true) {
                        if (i >= length) {
                            break;
                        }
                        ImageCompressionUtil.Result compressWithinConstraints = ImageCompressionUtil.compressWithinConstraints(ApplicationDependencies.getApplication(), MediaUtil.IMAGE_JPEG, decodeByteArray, imageSizeTargets[i], mediaConfig.getMaxImageFileSize(), mediaConfig.getQualitySetting());
                        if (compressWithinConstraints != null) {
                            empty = Optional.of(bytesToAttachment(compressWithinConstraints.getData(), compressWithinConstraints.getWidth(), compressWithinConstraints.getHeight(), compressWithinConstraints.getMimeType()));
                            break;
                        }
                        i++;
                    }
                }
                if (decodeByteArray != null) {
                    decodeByteArray.recycle();
                }
                consumer.accept(empty);
            }
        } catch (IOException | IllegalArgumentException | BitmapDecodingException e) {
            Log.w(TAG, "Exception during link preview image retrieval.", e);
            callRequestController.cancel();
            consumer.accept(Optional.empty());
        }
    }

    private static RequestController fetchStickerPackLinkPreview(Context context, String str, Callback callback) {
        SignalExecutors.UNBOUNDED.execute(new Runnable(str, context, callback) { // from class: org.thoughtcrime.securesms.linkpreview.LinkPreviewRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ String f$0;
            public final /* synthetic */ Context f$1;
            public final /* synthetic */ LinkPreviewRepository.Callback f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                LinkPreviewRepository.lambda$fetchStickerPackLinkPreview$3(this.f$0, this.f$1, this.f$2);
            }
        });
        return new RequestController() { // from class: org.thoughtcrime.securesms.linkpreview.LinkPreviewRepository$$ExternalSyntheticLambda4
            @Override // org.thoughtcrime.securesms.net.RequestController
            public final void cancel() {
                LinkPreviewRepository.lambda$fetchStickerPackLinkPreview$4();
            }
        };
    }

    public static /* synthetic */ void lambda$fetchStickerPackLinkPreview$3(String str, Context context, Callback callback) {
        try {
            Pair<String, String> orElse = StickerUrl.parseShareLink(str).orElse(new Pair<>("", ""));
            String first = orElse.first();
            String second = orElse.second();
            SignalServiceStickerManifest retrieveStickerManifest = ApplicationDependencies.getSignalServiceMessageReceiver().retrieveStickerManifest(Hex.fromStringCondensed(first), Hex.fromStringCondensed(second));
            String str2 = (String) OptionalUtil.or(retrieveStickerManifest.getTitle(), retrieveStickerManifest.getAuthor()).orElse("");
            Optional or = OptionalUtil.or(retrieveStickerManifest.getCover(), Optional.ofNullable(retrieveStickerManifest.getStickers().size() > 0 ? retrieveStickerManifest.getStickers().get(0) : null));
            if (or.isPresent()) {
                callback.onSuccess(new LinkPreview(str, str2, "", 0, bitmapToAttachment(GlideApp.with(context).asBitmap().load((Object) new StickerRemoteUri(first, second, ((SignalServiceStickerManifest.StickerInfo) or.get()).getId())).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).centerInside().submit(512, 512).get(), Bitmap.CompressFormat.WEBP, MediaUtil.IMAGE_WEBP)));
            } else {
                callback.onError(Error.PREVIEW_NOT_AVAILABLE);
            }
        } catch (IOException | InterruptedException | ExecutionException | InvalidMessageException unused) {
            Log.w(TAG, "Failed to fetch sticker pack link preview.");
            callback.onError(Error.PREVIEW_NOT_AVAILABLE);
        }
    }

    public static /* synthetic */ void lambda$fetchStickerPackLinkPreview$4() {
        Log.i(TAG, "Cancelled sticker pack link preview fetch -- no effect.");
    }

    private static RequestController fetchGroupLinkPreview(Context context, String str, Callback callback) {
        SignalExecutors.UNBOUNDED.execute(new Runnable(str, context, callback) { // from class: org.thoughtcrime.securesms.linkpreview.LinkPreviewRepository$$ExternalSyntheticLambda5
            public final /* synthetic */ String f$0;
            public final /* synthetic */ Context f$1;
            public final /* synthetic */ LinkPreviewRepository.Callback f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                LinkPreviewRepository.lambda$fetchGroupLinkPreview$5(this.f$0, this.f$1, this.f$2);
            }
        });
        return new RequestController() { // from class: org.thoughtcrime.securesms.linkpreview.LinkPreviewRepository$$ExternalSyntheticLambda6
            @Override // org.thoughtcrime.securesms.net.RequestController
            public final void cancel() {
                LinkPreviewRepository.lambda$fetchGroupLinkPreview$6();
            }
        };
    }

    public static /* synthetic */ void lambda$fetchGroupLinkPreview$5(String str, Context context, Callback callback) {
        Throwable e;
        Throwable e2;
        Optional<Attachment> optional;
        try {
            GroupInviteLinkUrl fromUri = GroupInviteLinkUrl.fromUri(str);
            if (fromUri != null) {
                GroupMasterKey groupMasterKey = fromUri.getGroupMasterKey();
                Optional<GroupDatabase.GroupRecord> group = SignalDatabase.groups().getGroup(GroupId.v2(groupMasterKey));
                if (group.isPresent()) {
                    Log.i(TAG, "Creating preview for locally available group");
                    GroupDatabase.GroupRecord groupRecord = group.get();
                    callback.onSuccess(new LinkPreview(str, groupRecord.getTitle(), getMemberCountDescription(context, groupRecord.getMembers().size()), 0, AvatarHelper.hasAvatar(context, groupRecord.getRecipientId()) ? bitmapToAttachment(AvatarUtil.loadIconBitmapSquareNoCache(context, Recipient.resolved(groupRecord.getRecipientId()), 512, 512), Bitmap.CompressFormat.WEBP, MediaUtil.IMAGE_WEBP) : Optional.empty()));
                    return;
                }
                Log.i(TAG, "Group is not locally available for preview generation, fetching from server");
                DecryptedGroupJoinInfo groupJoinInfoFromServer = GroupManager.getGroupJoinInfoFromServer(context, groupMasterKey, fromUri.getPassword());
                String memberCountDescription = getMemberCountDescription(context, groupJoinInfoFromServer.getMemberCount());
                Optional<Attachment> empty = Optional.empty();
                byte[] downloadGroupAvatarBytes = AvatarGroupsV2DownloadJob.downloadGroupAvatarBytes(context, groupMasterKey, groupJoinInfoFromServer.getAvatar());
                if (downloadGroupAvatarBytes != null) {
                    Bitmap decodeByteArray = BitmapFactory.decodeByteArray(downloadGroupAvatarBytes, 0, downloadGroupAvatarBytes.length);
                    Optional<Attachment> bitmapToAttachment = bitmapToAttachment(decodeByteArray, Bitmap.CompressFormat.WEBP, MediaUtil.IMAGE_WEBP);
                    if (decodeByteArray != null) {
                        decodeByteArray.recycle();
                    }
                    optional = bitmapToAttachment;
                } else {
                    optional = empty;
                }
                callback.onSuccess(new LinkPreview(str, groupJoinInfoFromServer.getTitle(), memberCountDescription, 0, optional));
                return;
            }
            throw new AssertionError();
        } catch (IOException e3) {
            e = e3;
            Log.w(TAG, "Failed to fetch group link preview.", e);
            callback.onError(Error.PREVIEW_NOT_AVAILABLE);
        } catch (InterruptedException e4) {
            e = e4;
            Log.w(TAG, "Failed to fetch group link preview.", e);
            callback.onError(Error.PREVIEW_NOT_AVAILABLE);
        } catch (ExecutionException e5) {
            e = e5;
            Log.w(TAG, "Failed to fetch group link preview.", e);
            callback.onError(Error.PREVIEW_NOT_AVAILABLE);
        } catch (VerificationFailedException e6) {
            e = e6;
            Log.w(TAG, "Failed to fetch group link preview.", e);
            callback.onError(Error.PREVIEW_NOT_AVAILABLE);
        } catch (GroupInviteLinkUrl.InvalidGroupLinkException e7) {
            e2 = e7;
            Log.w(TAG, "Bad group link.", e2);
            callback.onError(Error.PREVIEW_NOT_AVAILABLE);
        } catch (GroupInviteLinkUrl.UnknownGroupLinkVersionException e8) {
            e2 = e8;
            Log.w(TAG, "Bad group link.", e2);
            callback.onError(Error.PREVIEW_NOT_AVAILABLE);
        } catch (GroupLinkNotActiveException e9) {
            Log.w(TAG, "Group link not active.", e9);
            callback.onError(Error.GROUP_LINK_INACTIVE);
        }
    }

    public static /* synthetic */ void lambda$fetchGroupLinkPreview$6() {
        Log.i(TAG, "Cancelled group link preview fetch -- no effect.");
    }

    private static String getMemberCountDescription(Context context, int i) {
        return context.getResources().getQuantityString(R.plurals.LinkPreviewRepository_d_members, i, Integer.valueOf(i));
    }

    private static Optional<Attachment> bitmapToAttachment(Bitmap bitmap, Bitmap.CompressFormat compressFormat, String str) {
        if (bitmap == null) {
            return Optional.empty();
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(compressFormat, 80, byteArrayOutputStream);
        return Optional.of(bytesToAttachment(byteArrayOutputStream.toByteArray(), bitmap.getWidth(), bitmap.getHeight(), str));
    }

    private static Attachment bytesToAttachment(byte[] bArr, int i, int i2, String str) {
        return new UriAttachment(BlobProvider.getInstance().forData(bArr).createForSingleSessionInMemory(), str, 1, (long) bArr.length, i, i2, null, null, false, false, false, false, null, null, null, null, null);
    }

    /* loaded from: classes4.dex */
    public static class Metadata {
        private final long date;
        private final Optional<String> description;
        private final Optional<String> imageUrl;
        private final Optional<String> title;

        Metadata(Optional<String> optional, Optional<String> optional2, long j, Optional<String> optional3) {
            this.title = optional;
            this.description = optional2;
            this.date = j;
            this.imageUrl = optional3;
        }

        static Metadata empty() {
            return new Metadata(Optional.empty(), Optional.empty(), 0, Optional.empty());
        }

        Optional<String> getTitle() {
            return this.title;
        }

        Optional<String> getDescription() {
            return this.description;
        }

        long getDate() {
            return this.date;
        }

        Optional<String> getImageUrl() {
            return this.imageUrl;
        }

        boolean isEmpty() {
            return !this.title.isPresent() && !this.imageUrl.isPresent();
        }
    }
}
