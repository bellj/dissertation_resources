package org.thoughtcrime.securesms.linkpreview;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.linkpreview.LinkPreviewUtil;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class LinkPreviewUtil$Links$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ LinkPreviewUtil.Links f$0;

    public /* synthetic */ LinkPreviewUtil$Links$$ExternalSyntheticLambda0(LinkPreviewUtil.Links links) {
        this.f$0 = links;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return this.f$0.lambda$new$0((Link) obj);
    }
}
