package org.thoughtcrime.securesms.linkpreview;

import org.thoughtcrime.securesms.linkpreview.LinkPreviewViewModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class LinkPreviewViewModel$1$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ LinkPreviewViewModel.AnonymousClass1 f$0;
    public final /* synthetic */ LinkPreview f$1;

    public /* synthetic */ LinkPreviewViewModel$1$$ExternalSyntheticLambda1(LinkPreviewViewModel.AnonymousClass1 r1, LinkPreview linkPreview) {
        this.f$0 = r1;
        this.f$1 = linkPreview;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$onSuccess$0(this.f$1);
    }
}
