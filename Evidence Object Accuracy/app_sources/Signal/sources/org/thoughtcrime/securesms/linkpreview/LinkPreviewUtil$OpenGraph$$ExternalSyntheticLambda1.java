package org.thoughtcrime.securesms.linkpreview;

import com.annimon.stream.function.Predicate;
import org.thoughtcrime.securesms.linkpreview.LinkPreviewUtil;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class LinkPreviewUtil$OpenGraph$$ExternalSyntheticLambda1 implements Predicate {
    @Override // com.annimon.stream.function.Predicate
    public final boolean test(Object obj) {
        return LinkPreviewUtil.OpenGraph.lambda$getDate$0((Long) obj);
    }
}
