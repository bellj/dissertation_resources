package org.thoughtcrime.securesms.linkpreview;

/* loaded from: classes4.dex */
public class Link {
    private final int position;
    private final String url;

    public Link(String str, int i) {
        this.url = str;
        this.position = i;
    }

    public String getUrl() {
        return this.url;
    }

    public int getPosition() {
        return this.position;
    }
}
