package org.thoughtcrime.securesms.linkpreview;

import androidx.core.text.HtmlCompat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import j$.util.Optional;
import java.io.IOException;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.AttachmentId;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.util.JsonUtils;

/* loaded from: classes.dex */
public class LinkPreview {
    @JsonProperty
    private final AttachmentId attachmentId;
    @JsonProperty
    private final long date;
    @JsonProperty
    private final String description;
    @JsonIgnore
    private final Optional<Attachment> thumbnail;
    @JsonProperty
    private final String title;
    @JsonProperty
    private final String url;

    public LinkPreview(String str, String str2, String str3, long j, DatabaseAttachment databaseAttachment) {
        this.url = str;
        this.title = str2;
        this.description = str3;
        this.date = j;
        this.thumbnail = Optional.of(databaseAttachment);
        this.attachmentId = databaseAttachment.getAttachmentId();
    }

    public LinkPreview(String str, String str2, String str3, long j, Optional<Attachment> optional) {
        this.url = str;
        this.title = str2;
        this.description = str3;
        this.date = j;
        this.thumbnail = optional;
        this.attachmentId = null;
    }

    public LinkPreview(@JsonProperty("url") String str, @JsonProperty("title") String str2, @JsonProperty("description") String str3, @JsonProperty("date") long j, @JsonProperty("attachmentId") AttachmentId attachmentId) {
        this.url = str;
        this.title = str2;
        this.description = (String) Optional.ofNullable(str3).orElse("");
        this.date = j;
        this.attachmentId = attachmentId;
        this.thumbnail = Optional.empty();
    }

    public String getUrl() {
        return this.url;
    }

    public String getTitle() {
        return HtmlCompat.fromHtml(this.title, 0).toString();
    }

    public String getDescription() {
        if (this.description.equals(this.title)) {
            return "";
        }
        return HtmlCompat.fromHtml(this.description, 0).toString();
    }

    public long getDate() {
        return this.date;
    }

    public Optional<Attachment> getThumbnail() {
        return this.thumbnail;
    }

    public AttachmentId getAttachmentId() {
        return this.attachmentId;
    }

    public String serialize() throws IOException {
        return JsonUtils.toJson(this);
    }

    public static LinkPreview deserialize(String str) throws IOException {
        return (LinkPreview) JsonUtils.fromJson(str, LinkPreview.class);
    }
}
