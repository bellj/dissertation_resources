package org.thoughtcrime.securesms.linkpreview;

import android.text.SpannableString;
import android.text.style.URLSpan;
import androidx.core.text.HtmlCompat;
import androidx.core.text.util.LinkifyCompat;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import j$.util.Optional;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import okhttp3.HttpUrl;
import org.thoughtcrime.securesms.util.LinkUtil;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.util.OptionalUtil;

/* loaded from: classes4.dex */
public final class LinkPreviewUtil {
    private static final Pattern ARTICLE_TAG_PATTERN = Pattern.compile("<\\s*meta[^>]*property\\s*=\\s*\"\\s*article:([^\"]+)\"[^>]*/?\\s*>");
    private static final Pattern FAVICON_HREF_PATTERN = Pattern.compile("href\\s*=\\s*\"([^\"]*)\"");
    private static final Pattern FAVICON_PATTERN = Pattern.compile("<\\s*link[^>]*rel\\s*=\\s*\".*icon.*\"[^>]*>");
    private static final Pattern OPEN_GRAPH_CONTENT_PATTERN = Pattern.compile("content\\s*=\\s*\"([^\"]*)\"");
    private static final Pattern OPEN_GRAPH_TAG_PATTERN = Pattern.compile("<\\s*meta[^>]*property\\s*=\\s*\"\\s*og:([^\"]+)\"[^>]*/?\\s*>");
    private static final Pattern TITLE_PATTERN = Pattern.compile("<\\s*title[^>]*>(.*)<\\s*/title[^>]*>");

    public static String getTopLevelDomain(String str) {
        HttpUrl parse;
        if (Util.isEmpty(str) || (parse = HttpUrl.parse(str)) == null) {
            return null;
        }
        return parse.topPrivateDomain();
    }

    public static Links findValidPreviewUrls(String str) {
        SpannableString spannableString = new SpannableString(str);
        if (!LinkifyCompat.addLinks(spannableString, 1)) {
            return Links.EMPTY;
        }
        return new Links(Stream.of((URLSpan[]) spannableString.getSpans(0, spannableString.length(), URLSpan.class)).map(new Function(spannableString) { // from class: org.thoughtcrime.securesms.linkpreview.LinkPreviewUtil$$ExternalSyntheticLambda0
            public final /* synthetic */ SpannableString f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return LinkPreviewUtil.lambda$findValidPreviewUrls$0(this.f$0, (URLSpan) obj);
            }
        }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.linkpreview.LinkPreviewUtil$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return LinkPreviewUtil.lambda$findValidPreviewUrls$1((Link) obj);
            }
        }).toList());
    }

    public static /* synthetic */ Link lambda$findValidPreviewUrls$0(SpannableString spannableString, URLSpan uRLSpan) {
        return new Link(uRLSpan.getURL(), spannableString.getSpanStart(uRLSpan));
    }

    public static /* synthetic */ boolean lambda$findValidPreviewUrls$1(Link link) {
        return LinkUtil.isValidPreviewUrl(link.getUrl());
    }

    public static OpenGraph parseOpenGraphFields(String str) {
        if (str == null) {
            return new OpenGraph(Collections.emptyMap(), null, null);
        }
        HashMap hashMap = new HashMap();
        Matcher matcher = OPEN_GRAPH_TAG_PATTERN.matcher(str);
        while (matcher.find()) {
            String group = matcher.group();
            String group2 = matcher.groupCount() > 0 ? matcher.group(1) : null;
            if (group2 != null) {
                Matcher matcher2 = OPEN_GRAPH_CONTENT_PATTERN.matcher(group);
                if (matcher2.find() && matcher2.groupCount() > 0) {
                    hashMap.put(group2.toLowerCase(), fromDoubleEncoded(matcher2.group(1)));
                }
            }
        }
        Matcher matcher3 = ARTICLE_TAG_PATTERN.matcher(str);
        while (matcher3.find()) {
            String group3 = matcher3.group();
            String group4 = matcher3.groupCount() > 0 ? matcher3.group(1) : null;
            if (group4 != null) {
                Matcher matcher4 = OPEN_GRAPH_CONTENT_PATTERN.matcher(group3);
                if (matcher4.find() && matcher4.groupCount() > 0) {
                    hashMap.put(group4.toLowerCase(), fromDoubleEncoded(matcher4.group(1)));
                }
            }
        }
        Matcher matcher5 = TITLE_PATTERN.matcher(str);
        String str2 = "";
        String fromDoubleEncoded = (!matcher5.find() || matcher5.groupCount() <= 0) ? str2 : fromDoubleEncoded(matcher5.group(1));
        Matcher matcher6 = FAVICON_PATTERN.matcher(str);
        if (matcher6.find()) {
            Matcher matcher7 = FAVICON_HREF_PATTERN.matcher(matcher6.group());
            if (matcher7.find() && matcher7.groupCount() > 0) {
                str2 = matcher7.group(1);
            }
        }
        return new OpenGraph(hashMap, fromDoubleEncoded, str2);
    }

    private static String fromDoubleEncoded(String str) {
        return HtmlCompat.fromHtml(HtmlCompat.fromHtml(str, 0).toString(), 0).toString();
    }

    /* loaded from: classes4.dex */
    public static final class OpenGraph {
        private static final String KEY_DESCRIPTION_URL;
        private static final String KEY_IMAGE_URL;
        private static final String KEY_MODIFIED_TIME_1;
        private static final String KEY_MODIFIED_TIME_2;
        private static final String KEY_PUBLISHED_TIME_1;
        private static final String KEY_PUBLISHED_TIME_2;
        private static final String KEY_TITLE;
        private final String faviconUrl;
        private final String htmlTitle;
        private final Map<String, String> values;

        public OpenGraph(Map<String, String> map, String str, String str2) {
            this.values = map;
            this.htmlTitle = str;
            this.faviconUrl = str2;
        }

        public Optional<String> getTitle() {
            return OptionalUtil.absentIfEmpty(Util.getFirstNonEmpty(this.values.get("title"), this.htmlTitle));
        }

        public Optional<String> getImageUrl() {
            return OptionalUtil.absentIfEmpty(Util.getFirstNonEmpty(this.values.get("image"), this.faviconUrl));
        }

        public long getDate() {
            return ((Long) Stream.of(this.values.get(KEY_PUBLISHED_TIME_1), this.values.get(KEY_PUBLISHED_TIME_2), this.values.get(KEY_MODIFIED_TIME_1), this.values.get(KEY_MODIFIED_TIME_2)).map(new LinkPreviewUtil$OpenGraph$$ExternalSyntheticLambda0()).filter(new LinkPreviewUtil$OpenGraph$$ExternalSyntheticLambda1()).findFirst().orElse(0L)).longValue();
        }

        public static /* synthetic */ boolean lambda$getDate$0(Long l) {
            return l.longValue() > 0;
        }

        public Optional<String> getDescription() {
            return OptionalUtil.absentIfEmpty(this.values.get(KEY_DESCRIPTION_URL));
        }
    }

    /* loaded from: classes4.dex */
    public static class Links {
        static final Links EMPTY = new Links(Collections.emptyList());
        private final List<Link> links;
        private final Set<String> urlSet;

        private Links(List<Link> list) {
            this.links = list;
            this.urlSet = (Set) Stream.of(list).map(new LinkPreviewUtil$Links$$ExternalSyntheticLambda0(this)).collect(Collectors.toSet());
        }

        public /* synthetic */ String lambda$new$0(Link link) {
            return trimTrailingSlash(link.getUrl());
        }

        public Optional<Link> findFirst() {
            if (this.links.isEmpty()) {
                return Optional.empty();
            }
            return Optional.of(this.links.get(0));
        }

        public boolean containsUrl(String str) {
            return this.urlSet.contains(trimTrailingSlash(str));
        }

        private String trimTrailingSlash(String str) {
            return str.endsWith("/") ? str.substring(0, str.length() - 1) : str;
        }

        public int size() {
            return this.links.size();
        }
    }
}
