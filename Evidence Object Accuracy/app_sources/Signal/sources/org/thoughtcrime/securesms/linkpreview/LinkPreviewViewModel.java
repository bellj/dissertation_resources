package org.thoughtcrime.securesms.linkpreview;

import android.content.Context;
import android.text.TextUtils;
import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import j$.util.Optional;
import java.util.Collections;
import java.util.List;
import org.signal.core.util.ThreadUtil;
import org.thoughtcrime.securesms.attachments.AttachmentId;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.linkpreview.LinkPreviewRepository;
import org.thoughtcrime.securesms.linkpreview.LinkPreviewViewModel;
import org.thoughtcrime.securesms.net.RequestController;
import org.thoughtcrime.securesms.util.Debouncer;

/* loaded from: classes4.dex */
public class LinkPreviewViewModel extends ViewModel {
    private RequestController activeRequest;
    private String activeUrl;
    private Debouncer debouncer;
    private boolean enabled;
    private final LiveData<LinkPreviewState> linkPreviewSafeState;
    private final MutableLiveData<LinkPreviewState> linkPreviewState;
    private final LinkPreviewRepository repository;
    private boolean userCanceled;

    private LinkPreviewViewModel(LinkPreviewRepository linkPreviewRepository) {
        this.repository = linkPreviewRepository;
        MutableLiveData<LinkPreviewState> mutableLiveData = new MutableLiveData<>();
        this.linkPreviewState = mutableLiveData;
        this.debouncer = new Debouncer(250);
        this.enabled = SignalStore.settings().isLinkPreviewsEnabled();
        this.linkPreviewSafeState = Transformations.map(mutableLiveData, new Function() { // from class: org.thoughtcrime.securesms.linkpreview.LinkPreviewViewModel$$ExternalSyntheticLambda0
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return LinkPreviewViewModel.this.lambda$new$0((LinkPreviewViewModel.LinkPreviewState) obj);
            }
        });
    }

    public /* synthetic */ LinkPreviewState lambda$new$0(LinkPreviewState linkPreviewState) {
        if (this.enabled) {
            return linkPreviewState;
        }
        return LinkPreviewState.forNoLinks();
    }

    public LiveData<LinkPreviewState> getLinkPreviewState() {
        return this.linkPreviewSafeState;
    }

    public boolean hasLinkPreview() {
        return this.linkPreviewSafeState.getValue() != null && this.linkPreviewSafeState.getValue().getLinkPreview().isPresent();
    }

    public boolean hasLinkPreviewUi() {
        return this.linkPreviewSafeState.getValue() != null && this.linkPreviewSafeState.getValue().hasContent();
    }

    public List<LinkPreview> onSend() {
        LinkPreviewState value = this.linkPreviewSafeState.getValue();
        RequestController requestController = this.activeRequest;
        if (requestController != null) {
            requestController.cancel();
            this.activeRequest = null;
        }
        this.userCanceled = false;
        this.activeUrl = null;
        this.debouncer.clear();
        this.linkPreviewState.setValue(LinkPreviewState.forNoLinks());
        if (value == null || !value.getLinkPreview().isPresent()) {
            return Collections.emptyList();
        }
        return Collections.singletonList(value.getLinkPreview().get());
    }

    public List<LinkPreview> onSendWithErrorUrl() {
        LinkPreviewState value = this.linkPreviewSafeState.getValue();
        RequestController requestController = this.activeRequest;
        if (requestController != null) {
            requestController.cancel();
            this.activeRequest = null;
        }
        this.userCanceled = false;
        this.activeUrl = null;
        this.debouncer.clear();
        this.linkPreviewState.setValue(LinkPreviewState.forNoLinks());
        if (value == null) {
            return Collections.emptyList();
        }
        if (value.getLinkPreview().isPresent()) {
            return Collections.singletonList(value.getLinkPreview().get());
        }
        if (value.getActiveUrlForError() == null) {
            return Collections.emptyList();
        }
        String topLevelDomain = LinkPreviewUtil.getTopLevelDomain(value.getActiveUrlForError());
        String activeUrlForError = value.getActiveUrlForError();
        if (topLevelDomain == null) {
            topLevelDomain = value.getActiveUrlForError();
        }
        return Collections.singletonList(new LinkPreview(activeUrlForError, topLevelDomain, (String) null, -1, (AttachmentId) null));
    }

    public void onTextChanged(Context context, String str, int i, int i2) {
        if (this.enabled) {
            this.debouncer.publish(new Runnable(str, i, i2, context.getApplicationContext()) { // from class: org.thoughtcrime.securesms.linkpreview.LinkPreviewViewModel$$ExternalSyntheticLambda1
                public final /* synthetic */ String f$1;
                public final /* synthetic */ int f$2;
                public final /* synthetic */ int f$3;
                public final /* synthetic */ Context f$4;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                    this.f$4 = r5;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    LinkPreviewViewModel.this.lambda$onTextChanged$1(this.f$1, this.f$2, this.f$3, this.f$4);
                }
            });
        }
    }

    public /* synthetic */ void lambda$onTextChanged$1(String str, int i, int i2, Context context) {
        if (TextUtils.isEmpty(str)) {
            this.userCanceled = false;
        }
        if (!this.userCanceled) {
            Optional<Link> findFirst = LinkPreviewUtil.findValidPreviewUrls(str).findFirst();
            if (!findFirst.isPresent() || !findFirst.get().getUrl().equals(this.activeUrl)) {
                RequestController requestController = this.activeRequest;
                if (requestController != null) {
                    requestController.cancel();
                    this.activeRequest = null;
                }
                if (!findFirst.isPresent() || !isCursorPositionValid(str, findFirst.get(), i, i2)) {
                    this.activeUrl = null;
                    this.linkPreviewState.setValue(LinkPreviewState.forNoLinks());
                    return;
                }
                this.linkPreviewState.setValue(LinkPreviewState.forLoading());
                this.activeUrl = findFirst.get().getUrl();
                this.activeRequest = this.repository.getLinkPreview(context, findFirst.get().getUrl(), new LinkPreviewRepository.Callback() { // from class: org.thoughtcrime.securesms.linkpreview.LinkPreviewViewModel.1
                    @Override // org.thoughtcrime.securesms.linkpreview.LinkPreviewRepository.Callback
                    public void onSuccess(LinkPreview linkPreview) {
                        ThreadUtil.runOnMain(new LinkPreviewViewModel$1$$ExternalSyntheticLambda1(this, linkPreview));
                    }

                    public /* synthetic */ void lambda$onSuccess$0(LinkPreview linkPreview) {
                        if (!LinkPreviewViewModel.this.userCanceled) {
                            if (LinkPreviewViewModel.this.activeUrl == null || !LinkPreviewViewModel.this.activeUrl.equals(linkPreview.getUrl())) {
                                LinkPreviewViewModel.this.linkPreviewState.setValue(LinkPreviewState.forNoLinks());
                            } else {
                                LinkPreviewViewModel.this.linkPreviewState.setValue(LinkPreviewState.forPreview(linkPreview));
                            }
                        }
                        LinkPreviewViewModel.this.activeRequest = null;
                    }

                    @Override // org.thoughtcrime.securesms.linkpreview.LinkPreviewRepository.Callback
                    public void onError(LinkPreviewRepository.Error error) {
                        ThreadUtil.runOnMain(new LinkPreviewViewModel$1$$ExternalSyntheticLambda0(this, error));
                    }

                    public /* synthetic */ void lambda$onError$1(LinkPreviewRepository.Error error) {
                        if (!LinkPreviewViewModel.this.userCanceled) {
                            if (LinkPreviewViewModel.this.activeUrl != null) {
                                LinkPreviewViewModel.this.linkPreviewState.setValue(LinkPreviewState.forLinksWithNoPreview(LinkPreviewViewModel.this.activeUrl, error));
                            } else {
                                LinkPreviewViewModel.this.linkPreviewState.setValue(LinkPreviewState.forNoLinks());
                            }
                        }
                        LinkPreviewViewModel.this.activeRequest = null;
                    }
                });
            }
        }
    }

    public void onUserCancel() {
        RequestController requestController = this.activeRequest;
        if (requestController != null) {
            requestController.cancel();
            this.activeRequest = null;
        }
        this.userCanceled = true;
        this.activeUrl = null;
        this.debouncer.clear();
        this.linkPreviewState.setValue(LinkPreviewState.forNoLinks());
    }

    public void onTransportChanged(boolean z) {
        boolean z2 = SignalStore.settings().isLinkPreviewsEnabled() && !z;
        this.enabled = z2;
        if (!z2) {
            onUserCancel();
        }
    }

    public void onEnabled() {
        this.userCanceled = false;
        this.enabled = SignalStore.settings().isLinkPreviewsEnabled();
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        RequestController requestController = this.activeRequest;
        if (requestController != null) {
            requestController.cancel();
        }
        this.debouncer.clear();
    }

    private boolean isCursorPositionValid(String str, Link link, int i, int i2) {
        if (i != i2) {
            return true;
        }
        if ((!str.endsWith(link.getUrl()) || i != link.getPosition() + link.getUrl().length()) && i >= link.getPosition() && i <= link.getPosition() + link.getUrl().length()) {
            return false;
        }
        return true;
    }

    /* loaded from: classes4.dex */
    public static class LinkPreviewState {
        private final String activeUrlForError;
        private final LinkPreviewRepository.Error error;
        private final boolean hasLinks;
        private final boolean isLoading;
        private final Optional<LinkPreview> linkPreview;

        private LinkPreviewState(String str, boolean z, boolean z2, Optional<LinkPreview> optional, LinkPreviewRepository.Error error) {
            this.activeUrlForError = str;
            this.isLoading = z;
            this.hasLinks = z2;
            this.linkPreview = optional;
            this.error = error;
        }

        public static LinkPreviewState forLoading() {
            return new LinkPreviewState(null, true, false, Optional.empty(), null);
        }

        public static LinkPreviewState forPreview(LinkPreview linkPreview) {
            return new LinkPreviewState(null, false, true, Optional.of(linkPreview), null);
        }

        public static LinkPreviewState forLinksWithNoPreview(String str, LinkPreviewRepository.Error error) {
            return new LinkPreviewState(str, false, true, Optional.empty(), error);
        }

        public static LinkPreviewState forNoLinks() {
            return new LinkPreviewState(null, false, false, Optional.empty(), null);
        }

        public String getActiveUrlForError() {
            return this.activeUrlForError;
        }

        public boolean isLoading() {
            return this.isLoading;
        }

        public boolean hasLinks() {
            return this.hasLinks;
        }

        public Optional<LinkPreview> getLinkPreview() {
            return this.linkPreview;
        }

        public LinkPreviewRepository.Error getError() {
            return this.error;
        }

        boolean hasContent() {
            return this.isLoading || this.hasLinks;
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final LinkPreviewRepository repository;

        public Factory(LinkPreviewRepository linkPreviewRepository) {
            this.repository = linkPreviewRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.NewInstanceFactory, androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new LinkPreviewViewModel(this.repository));
        }
    }
}
