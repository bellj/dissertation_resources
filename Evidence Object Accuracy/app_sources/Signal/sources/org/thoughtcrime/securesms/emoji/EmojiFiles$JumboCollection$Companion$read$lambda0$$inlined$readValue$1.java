package org.thoughtcrime.securesms.emoji;

import com.fasterxml.jackson.core.type.TypeReference;
import kotlin.Metadata;
import org.thoughtcrime.securesms.emoji.EmojiFiles;

/* compiled from: Extensions.kt */
@Metadata(bv = {}, d1 = {"\u0000\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00028\u00000\u0001¨\u0006\u0002"}, d2 = {"com/fasterxml/jackson/module/kotlin/ExtensionsKt$jacksonTypeRef$1", "Lcom/fasterxml/jackson/core/type/TypeReference;", "jackson-module-kotlin"}, k = 1, mv = {1, 6, 0})
/* renamed from: org.thoughtcrime.securesms.emoji.EmojiFiles$JumboCollection$Companion$read$lambda-0$$inlined$readValue$1 */
/* loaded from: classes4.dex */
public final class EmojiFiles$JumboCollection$Companion$read$lambda0$$inlined$readValue$1 extends TypeReference<EmojiFiles.JumboCollection> {
}
