package org.thoughtcrime.securesms.emoji;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: EmojiRemote.kt */
@Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0002\u0010\u0007R\u0014\u0010\b\u001a\u00020\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiFileRequest;", "Lorg/thoughtcrime/securesms/emoji/EmojiRequest;", "version", "", "density", "", "name", "(ILjava/lang/String;Ljava/lang/String;)V", "uri", "getUri", "()Ljava/lang/String;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EmojiFileRequest implements EmojiRequest {
    private final String uri;

    public EmojiFileRequest(int i, String str, String str2) {
        Intrinsics.checkNotNullParameter(str, "density");
        Intrinsics.checkNotNullParameter(str2, "name");
        this.uri = "/static/android/emoji/" + i + '/' + str + '/' + str2;
    }

    @Override // org.thoughtcrime.securesms.emoji.EmojiRequest
    public String getUri() {
        return this.uri;
    }
}
