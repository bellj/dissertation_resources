package org.thoughtcrime.securesms.emoji.protos;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* loaded from: classes4.dex */
public final class JumbomojiItem extends GeneratedMessageLite<JumbomojiItem, Builder> implements JumbomojiItemOrBuilder {
    private static final JumbomojiItem DEFAULT_INSTANCE;
    public static final int IMAGE_FIELD_NUMBER;
    public static final int NAME_FIELD_NUMBER;
    private static volatile Parser<JumbomojiItem> PARSER;
    private ByteString image_ = ByteString.EMPTY;
    private String name_ = "";

    private JumbomojiItem() {
    }

    @Override // org.thoughtcrime.securesms.emoji.protos.JumbomojiItemOrBuilder
    public String getName() {
        return this.name_;
    }

    @Override // org.thoughtcrime.securesms.emoji.protos.JumbomojiItemOrBuilder
    public ByteString getNameBytes() {
        return ByteString.copyFromUtf8(this.name_);
    }

    public void setName(String str) {
        str.getClass();
        this.name_ = str;
    }

    public void clearName() {
        this.name_ = getDefaultInstance().getName();
    }

    public void setNameBytes(ByteString byteString) {
        AbstractMessageLite.checkByteStringIsUtf8(byteString);
        this.name_ = byteString.toStringUtf8();
    }

    @Override // org.thoughtcrime.securesms.emoji.protos.JumbomojiItemOrBuilder
    public ByteString getImage() {
        return this.image_;
    }

    public void setImage(ByteString byteString) {
        byteString.getClass();
        this.image_ = byteString;
    }

    public void clearImage() {
        this.image_ = getDefaultInstance().getImage();
    }

    public static JumbomojiItem parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (JumbomojiItem) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static JumbomojiItem parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (JumbomojiItem) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static JumbomojiItem parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (JumbomojiItem) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static JumbomojiItem parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (JumbomojiItem) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static JumbomojiItem parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (JumbomojiItem) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static JumbomojiItem parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (JumbomojiItem) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static JumbomojiItem parseFrom(InputStream inputStream) throws IOException {
        return (JumbomojiItem) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static JumbomojiItem parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (JumbomojiItem) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static JumbomojiItem parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (JumbomojiItem) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static JumbomojiItem parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (JumbomojiItem) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static JumbomojiItem parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (JumbomojiItem) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static JumbomojiItem parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (JumbomojiItem) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(JumbomojiItem jumbomojiItem) {
        return DEFAULT_INSTANCE.createBuilder(jumbomojiItem);
    }

    /* loaded from: classes4.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<JumbomojiItem, Builder> implements JumbomojiItemOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(JumbomojiItem.DEFAULT_INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.emoji.protos.JumbomojiItemOrBuilder
        public String getName() {
            return ((JumbomojiItem) this.instance).getName();
        }

        @Override // org.thoughtcrime.securesms.emoji.protos.JumbomojiItemOrBuilder
        public ByteString getNameBytes() {
            return ((JumbomojiItem) this.instance).getNameBytes();
        }

        public Builder setName(String str) {
            copyOnWrite();
            ((JumbomojiItem) this.instance).setName(str);
            return this;
        }

        public Builder clearName() {
            copyOnWrite();
            ((JumbomojiItem) this.instance).clearName();
            return this;
        }

        public Builder setNameBytes(ByteString byteString) {
            copyOnWrite();
            ((JumbomojiItem) this.instance).setNameBytes(byteString);
            return this;
        }

        @Override // org.thoughtcrime.securesms.emoji.protos.JumbomojiItemOrBuilder
        public ByteString getImage() {
            return ((JumbomojiItem) this.instance).getImage();
        }

        public Builder setImage(ByteString byteString) {
            copyOnWrite();
            ((JumbomojiItem) this.instance).setImage(byteString);
            return this;
        }

        public Builder clearImage() {
            copyOnWrite();
            ((JumbomojiItem) this.instance).clearImage();
            return this;
        }
    }

    /* renamed from: org.thoughtcrime.securesms.emoji.protos.JumbomojiItem$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new JumbomojiItem();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001Ȉ\u0002\n", new Object[]{"name_", "image_"});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<JumbomojiItem> parser = PARSER;
                if (parser == null) {
                    synchronized (JumbomojiItem.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        JumbomojiItem jumbomojiItem = new JumbomojiItem();
        DEFAULT_INSTANCE = jumbomojiItem;
        GeneratedMessageLite.registerDefaultInstance(JumbomojiItem.class, jumbomojiItem);
    }

    public static JumbomojiItem getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<JumbomojiItem> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
