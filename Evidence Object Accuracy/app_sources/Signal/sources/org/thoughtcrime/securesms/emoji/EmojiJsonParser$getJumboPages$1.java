package org.thoughtcrime.securesms.emoji;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.MapsKt__MapsJVMKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import kotlin.ranges.RangesKt___RangesKt;

/* compiled from: EmojiJsonParser.kt */
@Metadata(d1 = {"\u0000\u001c\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010'\n\u0002\u0018\u0002\n\u0002\u0010&\n\u0000\u0010\u0000\u001a\u001e\u0012\f\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0012\f\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012F\u0010\u0004\u001aB\u0012\f\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0012\f\u0012\n \u0003*\u0004\u0018\u00010\u00060\u0006 \u0003* \u0012\f\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0012\f\u0012\n \u0003*\u0004\u0018\u00010\u00060\u0006\u0018\u00010\u00070\u0005H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "", "", "kotlin.jvm.PlatformType", "<name for destructuring parameter 0>", "", "Lcom/fasterxml/jackson/databind/JsonNode;", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EmojiJsonParser$getJumboPages$1 extends Lambda implements Function1<Map.Entry<String, JsonNode>, Map<String, ? extends String>> {
    public static final EmojiJsonParser$getJumboPages$1 INSTANCE = new EmojiJsonParser$getJumboPages$1();

    EmojiJsonParser$getJumboPages$1() {
        super(1);
    }

    public final Map<String, String> invoke(Map.Entry<String, JsonNode> entry) {
        Intrinsics.checkNotNullExpressionValue(entry, "(page: String, node: JsonNode)");
        String key = entry.getKey();
        JsonNode<JsonNode> value = entry.getValue();
        Intrinsics.checkNotNullExpressionValue(value, "node");
        LinkedHashMap linkedHashMap = new LinkedHashMap(RangesKt___RangesKt.coerceAtLeast(MapsKt__MapsJVMKt.mapCapacity(CollectionsKt__IterablesKt.collectionSizeOrDefault(value, 10)), 16));
        for (JsonNode jsonNode : value) {
            Pair pair = TuplesKt.to(jsonNode.textValue(), key);
            linkedHashMap.put(pair.getFirst(), pair.getSecond());
        }
        return linkedHashMap;
    }
}
