package org.thoughtcrime.securesms.emoji;

import android.net.Uri;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.emoji.EmojiPage;

/* compiled from: EmojiSource.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "Lorg/thoughtcrime/securesms/emoji/EmojiPage;", "uri", "Landroid/net/Uri;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EmojiSource$Companion$loadAssetBasedEmojis$1$1 extends Lambda implements Function1<Uri, EmojiPage> {
    public static final EmojiSource$Companion$loadAssetBasedEmojis$1$1 INSTANCE = new EmojiSource$Companion$loadAssetBasedEmojis$1$1();

    EmojiSource$Companion$loadAssetBasedEmojis$1$1() {
        super(1);
    }

    public final EmojiPage invoke(Uri uri) {
        Intrinsics.checkNotNullParameter(uri, "uri");
        return new EmojiPage.Asset(uri);
    }
}
