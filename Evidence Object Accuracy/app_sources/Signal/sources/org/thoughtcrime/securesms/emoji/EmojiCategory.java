package org.thoughtcrime.securesms.emoji;

import java.util.NoSuchElementException;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;

/* compiled from: EmojiCategory.kt */
@Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0013\b\u0001\u0018\u0000 \u00172\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0017B!\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0003¢\u0006\u0002\u0010\u0007J\b\u0010\r\u001a\u00020\u0003H\u0007R\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\tj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011j\u0002\b\u0012j\u0002\b\u0013j\u0002\b\u0014j\u0002\b\u0015j\u0002\b\u0016¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiCategory;", "", "priority", "", "key", "", "icon", "(Ljava/lang/String;IILjava/lang/String;I)V", "getIcon", "()I", "getKey", "()Ljava/lang/String;", "getPriority", "getCategoryLabel", "PEOPLE", "NATURE", "FOODS", "ACTIVITY", "PLACES", "OBJECTS", "SYMBOLS", "FLAGS", "EMOTICONS", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public enum EmojiCategory {
    PEOPLE(0, "People", R.attr.emoji_category_people),
    NATURE(1, "Nature", R.attr.emoji_category_nature),
    FOODS(2, "Foods", R.attr.emoji_category_foods),
    ACTIVITY(3, "Activity", R.attr.emoji_category_activity),
    PLACES(4, "Places", R.attr.emoji_category_places),
    OBJECTS(5, "Objects", R.attr.emoji_category_objects),
    SYMBOLS(6, "Symbols", R.attr.emoji_category_symbols),
    FLAGS(7, "Flags", R.attr.emoji_category_flags),
    EMOTICONS(8, "Emoticons", R.attr.emoji_category_emoticons);
    
    public static final Companion Companion = new Companion(null);
    private final int icon;
    private final String key;
    private final int priority;

    @JvmStatic
    public static final EmojiCategory forKey(String str) {
        return Companion.forKey(str);
    }

    @JvmStatic
    public static final int getCategoryLabel(int i) {
        return Companion.getCategoryLabel(i);
    }

    EmojiCategory(int i, String str, int i2) {
        this.priority = i;
        this.key = str;
        this.icon = i2;
    }

    public final int getIcon() {
        return this.icon;
    }

    public final String getKey() {
        return this.key;
    }

    public final int getPriority() {
        return this.priority;
    }

    public final int getCategoryLabel() {
        return Companion.getCategoryLabel(this.icon);
    }

    /* compiled from: EmojiCategory.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0012\u0010\u0007\u001a\u00020\b2\b\b\u0001\u0010\t\u001a\u00020\bH\u0007¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiCategory$Companion;", "", "()V", "forKey", "Lorg/thoughtcrime/securesms/emoji/EmojiCategory;", "key", "", "getCategoryLabel", "", "iconAttr", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final EmojiCategory forKey(String str) {
            Intrinsics.checkNotNullParameter(str, "key");
            EmojiCategory[] values = EmojiCategory.values();
            for (EmojiCategory emojiCategory : values) {
                if (Intrinsics.areEqual(emojiCategory.getKey(), str)) {
                    return emojiCategory;
                }
            }
            throw new NoSuchElementException("Array contains no element matching the predicate.");
        }

        @JvmStatic
        public final int getCategoryLabel(int i) {
            switch (i) {
                case R.attr.emoji_category_activity /* 2130969020 */:
                    return R.string.ReactWithAnyEmojiBottomSheetDialogFragment__activities;
                case R.attr.emoji_category_emoticons /* 2130969021 */:
                    return R.string.ReactWithAnyEmojiBottomSheetDialogFragment__emoticons;
                case R.attr.emoji_category_flags /* 2130969022 */:
                    return R.string.ReactWithAnyEmojiBottomSheetDialogFragment__flags;
                case R.attr.emoji_category_foods /* 2130969023 */:
                    return R.string.ReactWithAnyEmojiBottomSheetDialogFragment__food;
                case R.attr.emoji_category_nature /* 2130969024 */:
                    return R.string.ReactWithAnyEmojiBottomSheetDialogFragment__nature;
                case R.attr.emoji_category_objects /* 2130969025 */:
                    return R.string.ReactWithAnyEmojiBottomSheetDialogFragment__objects;
                case R.attr.emoji_category_people /* 2130969026 */:
                    return R.string.ReactWithAnyEmojiBottomSheetDialogFragment__smileys_and_people;
                case R.attr.emoji_category_places /* 2130969027 */:
                    return R.string.ReactWithAnyEmojiBottomSheetDialogFragment__places;
                case R.attr.emoji_category_recent /* 2130969028 */:
                default:
                    throw new AssertionError();
                case R.attr.emoji_category_symbols /* 2130969029 */:
                    return R.string.ReactWithAnyEmojiBottomSheetDialogFragment__symbols;
            }
        }
    }
}
