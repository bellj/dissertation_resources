package org.thoughtcrime.securesms.emoji;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.SystemClock;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.io.CloseableKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Ref$ObjectRef;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.components.emoji.parsing.EmojiDrawInfo;
import org.thoughtcrime.securesms.emoji.EmojiFiles;
import org.thoughtcrime.securesms.emoji.protos.JumbomojiItem;
import org.thoughtcrime.securesms.emoji.protos.JumbomojiPack;
import org.thoughtcrime.securesms.jobmanager.impl.AutoDownloadEmojiConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.ListenableFutureTask;
import org.thoughtcrime.securesms.util.SoftHashMap;

/* compiled from: JumboEmoji.kt */
@Metadata(d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010#\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u00020\u0001:\u0003!\"#B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0017\u001a\u00020\u000b2\u0006\u0010\u0018\u001a\u00020\u0019H\u0007J\u0010\u0010\u001a\u001a\u00020\u000b2\u0006\u0010\u001b\u001a\u00020\u001cH\u0007J\u0018\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001b\u001a\u00020\u001cH\u0007J\u0010\u0010\u001f\u001a\u00020 2\u0006\u0010\u0018\u001a\u00020\u0019H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\b0\u000eX\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\n \u0011*\u0004\u0018\u00010\u00100\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0013X\u0004¢\u0006\u0002\n\u0000R\u001c\u0010\u0015\u001a\u0010\u0012\u0004\u0012\u00020\u0016\u0012\u0006\u0012\u0004\u0018\u00010\b0\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006$"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/JumboEmoji;", "", "()V", "JUMBOMOJI_SUPPORTED_VERSION", "", "MAX_JUMBOJI_COUNT", "cache", "", "", "Landroid/graphics/Bitmap;", "canDownload", "", "currentVersion", "downloadedJumbos", "", "executor", "Ljava/util/concurrent/ExecutorService;", "kotlin.jvm.PlatformType", "lastNetworkCheck", "", "networkCheckThrottle", "versionToFormat", "Ljava/util/UUID;", "canDownloadJumbo", "context", "Landroid/content/Context;", "hasJumboEmoji", "drawInfo", "Lorg/thoughtcrime/securesms/components/emoji/parsing/EmojiDrawInfo;", "loadJumboEmoji", "Lorg/thoughtcrime/securesms/emoji/JumboEmoji$LoadResult;", "updateCurrentVersion", "", "CannotAutoDownload", "LoadResult", "NoVersionData", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class JumboEmoji {
    public static final JumboEmoji INSTANCE = new JumboEmoji();
    private static final int JUMBOMOJI_SUPPORTED_VERSION;
    public static final int MAX_JUMBOJI_COUNT;
    private static final Map<String, Bitmap> cache = new SoftHashMap(16);
    private static boolean canDownload;
    private static volatile int currentVersion = -1;
    private static final Set<String> downloadedJumbos = new LinkedHashSet();
    private static final ExecutorService executor = ThreadUtil.trace(SignalExecutors.newCachedSingleThreadExecutor("jumbo-emoji"));
    private static long lastNetworkCheck;
    private static final long networkCheckThrottle = TimeUnit.MINUTES.toMillis(1);
    private static final Map<UUID, String> versionToFormat = new HashMap();

    /* compiled from: JumboEmoji.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/JumboEmoji$CannotAutoDownload;", "Ljava/io/IOException;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class CannotAutoDownload extends IOException {
    }

    /* compiled from: JumboEmoji.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/JumboEmoji$NoVersionData;", "", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class NoVersionData extends Throwable {
    }

    private JumboEmoji() {
    }

    @JvmStatic
    public static final void updateCurrentVersion(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        SignalExecutors.BOUNDED.execute(new Runnable(context) { // from class: org.thoughtcrime.securesms.emoji.JumboEmoji$$ExternalSyntheticLambda4
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                JumboEmoji.m1790updateCurrentVersion$lambda1(this.f$0);
            }
        });
    }

    /* renamed from: updateCurrentVersion$lambda-1 */
    public static final void m1790updateCurrentVersion$lambda1(Context context) {
        Intrinsics.checkNotNullParameter(context, "$context");
        EmojiFiles.Version readVersion = EmojiFiles.Version.Companion.readVersion(context, true);
        if (readVersion != null) {
            ParsedEmojiData latestEmojiData = EmojiFiles.getLatestEmojiData(context, readVersion);
            if ((latestEmojiData != null ? latestEmojiData.getFormat() : null) != null) {
                currentVersion = readVersion.getVersion();
                ThreadUtil.runOnMain(new Runnable() { // from class: org.thoughtcrime.securesms.emoji.JumboEmoji$$ExternalSyntheticLambda5
                    @Override // java.lang.Runnable
                    public final void run() {
                        JumboEmoji.m1791updateCurrentVersion$lambda1$lambda0(EmojiFiles.Version.this);
                    }
                });
            }
        }
    }

    /* renamed from: updateCurrentVersion$lambda-1$lambda-0 */
    public static final void m1791updateCurrentVersion$lambda1$lambda0(EmojiFiles.Version version) {
        Intrinsics.checkNotNullParameter(version, "$version");
        Set<String> set = downloadedJumbos;
        HashSet<String> jumboEmojiSheets = SignalStore.emojiValues().getJumboEmojiSheets(version.getVersion());
        Intrinsics.checkNotNullExpressionValue(jumboEmojiSheets, "emojiValues().getJumboEmojiSheets(version.version)");
        set.addAll(jumboEmojiSheets);
    }

    @JvmStatic
    public static final boolean canDownloadJumbo(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        long elapsedRealtime = SystemClock.elapsedRealtime();
        if (elapsedRealtime - networkCheckThrottle > lastNetworkCheck) {
            canDownload = AutoDownloadEmojiConstraint.canAutoDownloadJumboEmoji(context);
            lastNetworkCheck = elapsedRealtime;
        }
        return canDownload && currentVersion >= 5;
    }

    @JvmStatic
    public static final boolean hasJumboEmoji(EmojiDrawInfo emojiDrawInfo) {
        Intrinsics.checkNotNullParameter(emojiDrawInfo, "drawInfo");
        return CollectionsKt___CollectionsKt.contains(downloadedJumbos, emojiDrawInfo.getJumboSheet());
    }

    @JvmStatic
    public static final LoadResult loadJumboEmoji(Context context, EmojiDrawInfo emojiDrawInfo) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(emojiDrawInfo, "drawInfo");
        Context applicationContext = context.getApplicationContext();
        Intrinsics.checkNotNullExpressionValue(applicationContext, "context.applicationContext");
        String str = "jumbo/" + emojiDrawInfo.getJumboSheet() + ".proto";
        String rawEmoji = emojiDrawInfo.getRawEmoji();
        Intrinsics.checkNotNull(rawEmoji);
        Bitmap bitmap = cache.get(rawEmoji);
        if (bitmap != null) {
            return new LoadResult.Immediate(bitmap);
        }
        ListenableFutureTask listenableFutureTask = new ListenableFutureTask((Callable) new Callable(applicationContext, rawEmoji, str, emojiDrawInfo, context) { // from class: org.thoughtcrime.securesms.emoji.JumboEmoji$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ String f$1;
            public final /* synthetic */ String f$2;
            public final /* synthetic */ EmojiDrawInfo f$3;
            public final /* synthetic */ Context f$4;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return JumboEmoji.m1787loadJumboEmoji$lambda8(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4);
            }
        });
        SimpleTask.run(executor, new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.emoji.JumboEmoji$$ExternalSyntheticLambda1
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return JumboEmoji.loadJumboEmoji$run__proxy(ListenableFutureTask.this);
            }
        }, new SimpleTask.ForegroundTask(rawEmoji, emojiDrawInfo) { // from class: org.thoughtcrime.securesms.emoji.JumboEmoji$$ExternalSyntheticLambda2
            public final /* synthetic */ String f$1;
            public final /* synthetic */ EmojiDrawInfo f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                JumboEmoji.m1789loadJumboEmoji$lambda9(ListenableFutureTask.this, this.f$1, this.f$2, (Unit) obj);
            }
        });
        return new LoadResult.Async(listenableFutureTask);
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [java.lang.Throwable, android.graphics.Rect] */
    /* JADX WARN: Type inference failed for: r1v3, types: [org.thoughtcrime.securesms.emoji.EmojiFiles$JumboCollection, T] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: loadJumboEmoji$lambda-8 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final android.graphics.Bitmap m1787loadJumboEmoji$lambda8(android.content.Context r5, java.lang.String r6, java.lang.String r7, org.thoughtcrime.securesms.components.emoji.parsing.EmojiDrawInfo r8, android.content.Context r9) {
        /*
            java.lang.String r0 = "$applicationContext"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r5, r0)
            java.lang.String r0 = "$emojiName"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r6, r0)
            java.lang.String r0 = "$archiveName"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r7, r0)
            java.lang.String r0 = "$drawInfo"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r8, r0)
            java.lang.String r0 = "$context"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r9, r0)
            org.thoughtcrime.securesms.emoji.EmojiFiles$Version$Companion r0 = org.thoughtcrime.securesms.emoji.EmojiFiles.Version.Companion
            r1 = 1
            org.thoughtcrime.securesms.emoji.EmojiFiles$Version r0 = r0.readVersion(r5, r1)
            if (r0 == 0) goto L_0x00b1
            java.util.Map<java.util.UUID, java.lang.String> r1 = org.thoughtcrime.securesms.emoji.JumboEmoji.versionToFormat
            java.util.UUID r2 = r0.getUuid()
            java.lang.Object r3 = r1.get(r2)
            r4 = 0
            if (r3 != 0) goto L_0x003f
            org.thoughtcrime.securesms.emoji.ParsedEmojiData r9 = org.thoughtcrime.securesms.emoji.EmojiFiles.getLatestEmojiData(r9, r0)
            if (r9 == 0) goto L_0x003b
            java.lang.String r9 = r9.getFormat()
            r3 = r9
            goto L_0x003c
        L_0x003b:
            r3 = r4
        L_0x003c:
            r1.put(r2, r3)
        L_0x003f:
            java.lang.String r3 = (java.lang.String) r3
            if (r3 == 0) goto L_0x00ab
            int r9 = r0.getVersion()
            org.thoughtcrime.securesms.emoji.JumboEmoji.currentVersion = r9
            kotlin.jvm.internal.Ref$ObjectRef r9 = new kotlin.jvm.internal.Ref$ObjectRef
            r9.<init>()
            org.thoughtcrime.securesms.emoji.EmojiFiles$JumboCollection$Companion r1 = org.thoughtcrime.securesms.emoji.EmojiFiles.JumboCollection.Companion
            org.thoughtcrime.securesms.emoji.EmojiFiles$JumboCollection r1 = r1.read(r5, r0)
            r9.element = r1
            java.util.UUID r1 = r1.getUUIDForName(r6)
            if (r1 != 0) goto L_0x008d
            boolean r1 = org.thoughtcrime.securesms.jobmanager.impl.AutoDownloadEmojiConstraint.canAutoDownloadJumboEmoji(r5)
            if (r1 == 0) goto L_0x0087
            java.lang.String r1 = org.thoughtcrime.securesms.emoji.JumboEmojiKt.access$getTAG$p()
            java.lang.String r2 = "No file for emoji, downloading jumbo"
            org.signal.core.util.logging.Log.i(r1, r2)
            java.lang.String r1 = r0.getDensity()
            org.thoughtcrime.securesms.emoji.JumboEmoji$$ExternalSyntheticLambda3 r2 = new org.thoughtcrime.securesms.emoji.JumboEmoji$$ExternalSyntheticLambda3
            r2.<init>(r5, r0, r9)
            org.thoughtcrime.securesms.emoji.EmojiDownloader.streamFileFromRemote(r0, r1, r7, r2)
            org.thoughtcrime.securesms.keyvalue.EmojiValues r7 = org.thoughtcrime.securesms.keyvalue.SignalStore.emojiValues()
            int r1 = r0.getVersion()
            java.lang.String r8 = r8.getJumboSheet()
            r7.addJumboEmojiSheet(r1, r8)
            goto L_0x008d
        L_0x0087:
            org.thoughtcrime.securesms.emoji.JumboEmoji$CannotAutoDownload r5 = new org.thoughtcrime.securesms.emoji.JumboEmoji$CannotAutoDownload
            r5.<init>()
            throw r5
        L_0x008d:
            org.thoughtcrime.securesms.emoji.EmojiFiles r7 = org.thoughtcrime.securesms.emoji.EmojiFiles.INSTANCE
            T r8 = r9.element
            org.thoughtcrime.securesms.emoji.EmojiFiles$JumboCollection r8 = (org.thoughtcrime.securesms.emoji.EmojiFiles.JumboCollection) r8
            java.io.InputStream r5 = r7.openForReadingJumbo(r5, r0, r8, r6)
            android.graphics.BitmapFactory$Options r6 = new android.graphics.BitmapFactory$Options     // Catch: all -> 0x00a4
            r6.<init>()     // Catch: all -> 0x00a4
            android.graphics.Bitmap r6 = android.graphics.BitmapFactory.decodeStream(r5, r4, r6)     // Catch: all -> 0x00a4
            kotlin.io.CloseableKt.closeFinally(r5, r4)
            return r6
        L_0x00a4:
            r6 = move-exception
            throw r6     // Catch: all -> 0x00a6
        L_0x00a6:
            r7 = move-exception
            kotlin.io.CloseableKt.closeFinally(r5, r6)
            throw r7
        L_0x00ab:
            org.thoughtcrime.securesms.emoji.JumboEmoji$NoVersionData r5 = new org.thoughtcrime.securesms.emoji.JumboEmoji$NoVersionData
            r5.<init>()
            throw r5
        L_0x00b1:
            org.thoughtcrime.securesms.emoji.JumboEmoji$NoVersionData r5 = new org.thoughtcrime.securesms.emoji.JumboEmoji$NoVersionData
            r5.<init>()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.emoji.JumboEmoji.m1787loadJumboEmoji$lambda8(android.content.Context, java.lang.String, java.lang.String, org.thoughtcrime.securesms.components.emoji.parsing.EmojiDrawInfo, android.content.Context):android.graphics.Bitmap");
    }

    /* JADX WARN: Type inference failed for: r1v7, types: [org.thoughtcrime.securesms.emoji.EmojiFiles$JumboCollection, T] */
    /* renamed from: loadJumboEmoji$lambda-8$lambda-6 */
    public static final void m1788loadJumboEmoji$lambda8$lambda6(Context context, EmojiFiles.Version version, Ref$ObjectRef ref$ObjectRef, InputStream inputStream) {
        Intrinsics.checkNotNullParameter(context, "$applicationContext");
        Intrinsics.checkNotNullParameter(ref$ObjectRef, "$jumbos");
        try {
            List<JumbomojiItem> itemsList = JumbomojiPack.parseFrom(inputStream).getItemsList();
            Intrinsics.checkNotNullExpressionValue(itemsList, "jumbomojiPack.itemsList");
            Iterator<T> it = itemsList.iterator();
            while (true) {
                th = null;
                if (it.hasNext()) {
                    JumbomojiItem jumbomojiItem = (JumbomojiItem) it.next();
                    String name = jumbomojiItem.getName();
                    Intrinsics.checkNotNullExpressionValue(name, "jumbo.name");
                    UUID randomUUID = UUID.randomUUID();
                    Intrinsics.checkNotNullExpressionValue(randomUUID, "randomUUID()");
                    EmojiFiles.Name name2 = new EmojiFiles.Name(name, randomUUID);
                    OutputStream openForWriting = EmojiFiles.openForWriting(context, version, name2.getUuid());
                    jumbomojiItem.getImage().writeTo(openForWriting);
                    Unit unit = Unit.INSTANCE;
                    CloseableKt.closeFinally(openForWriting, th);
                    ref$ObjectRef.element = EmojiFiles.JumboCollection.Companion.append(context, (EmojiFiles.JumboCollection) ref$ObjectRef.element, name2);
                } else {
                    Unit unit2 = Unit.INSTANCE;
                    return;
                }
            }
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public static final /* synthetic */ Unit loadJumboEmoji$run__proxy(ListenableFutureTask listenableFutureTask) {
        listenableFutureTask.run();
        return Unit.INSTANCE;
    }

    /* renamed from: loadJumboEmoji$lambda-9 */
    public static final void m1789loadJumboEmoji$lambda9(ListenableFutureTask listenableFutureTask, String str, EmojiDrawInfo emojiDrawInfo, Unit unit) {
        Intrinsics.checkNotNullParameter(listenableFutureTask, "$newTask");
        Intrinsics.checkNotNullParameter(str, "$emojiName");
        Intrinsics.checkNotNullParameter(emojiDrawInfo, "$drawInfo");
        try {
            Bitmap bitmap = (Bitmap) listenableFutureTask.get();
            if (bitmap == null) {
                Log.w(JumboEmojiKt.TAG, "Failed to load jumbo emoji");
                return;
            }
            cache.put(str, bitmap);
            Set<String> set = downloadedJumbos;
            String jumboSheet = emojiDrawInfo.getJumboSheet();
            Intrinsics.checkNotNull(jumboSheet);
            set.add(jumboSheet);
        } catch (ExecutionException unused) {
        }
    }

    /* compiled from: JumboEmoji.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0003\u0004B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0002\u0005\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/JumboEmoji$LoadResult;", "", "()V", "Async", "Immediate", "Lorg/thoughtcrime/securesms/emoji/JumboEmoji$LoadResult$Immediate;", "Lorg/thoughtcrime/securesms/emoji/JumboEmoji$LoadResult$Async;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class LoadResult {
        public /* synthetic */ LoadResult(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* compiled from: JumboEmoji.kt */
        @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/JumboEmoji$LoadResult$Immediate;", "Lorg/thoughtcrime/securesms/emoji/JumboEmoji$LoadResult;", "bitmap", "Landroid/graphics/Bitmap;", "(Landroid/graphics/Bitmap;)V", "getBitmap", "()Landroid/graphics/Bitmap;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Immediate extends LoadResult {
            private final Bitmap bitmap;

            public static /* synthetic */ Immediate copy$default(Immediate immediate, Bitmap bitmap, int i, Object obj) {
                if ((i & 1) != 0) {
                    bitmap = immediate.bitmap;
                }
                return immediate.copy(bitmap);
            }

            public final Bitmap component1() {
                return this.bitmap;
            }

            public final Immediate copy(Bitmap bitmap) {
                Intrinsics.checkNotNullParameter(bitmap, "bitmap");
                return new Immediate(bitmap);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof Immediate) && Intrinsics.areEqual(this.bitmap, ((Immediate) obj).bitmap);
            }

            public int hashCode() {
                return this.bitmap.hashCode();
            }

            public String toString() {
                return "Immediate(bitmap=" + this.bitmap + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Immediate(Bitmap bitmap) {
                super(null);
                Intrinsics.checkNotNullParameter(bitmap, "bitmap");
                this.bitmap = bitmap;
            }

            public final Bitmap getBitmap() {
                return this.bitmap;
            }
        }

        private LoadResult() {
        }

        /* compiled from: JumboEmoji.kt */
        @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rHÖ\u0003J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/JumboEmoji$LoadResult$Async;", "Lorg/thoughtcrime/securesms/emoji/JumboEmoji$LoadResult;", "task", "Lorg/thoughtcrime/securesms/util/ListenableFutureTask;", "Landroid/graphics/Bitmap;", "(Lorg/thoughtcrime/securesms/util/ListenableFutureTask;)V", "getTask", "()Lorg/thoughtcrime/securesms/util/ListenableFutureTask;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Async extends LoadResult {
            private final ListenableFutureTask<Bitmap> task;

            /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.emoji.JumboEmoji$LoadResult$Async */
            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Async copy$default(Async async, ListenableFutureTask listenableFutureTask, int i, Object obj) {
                if ((i & 1) != 0) {
                    listenableFutureTask = async.task;
                }
                return async.copy(listenableFutureTask);
            }

            public final ListenableFutureTask<Bitmap> component1() {
                return this.task;
            }

            public final Async copy(ListenableFutureTask<Bitmap> listenableFutureTask) {
                Intrinsics.checkNotNullParameter(listenableFutureTask, "task");
                return new Async(listenableFutureTask);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof Async) && Intrinsics.areEqual(this.task, ((Async) obj).task);
            }

            public int hashCode() {
                return this.task.hashCode();
            }

            public String toString() {
                return "Async(task=" + this.task + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Async(ListenableFutureTask<Bitmap> listenableFutureTask) {
                super(null);
                Intrinsics.checkNotNullParameter(listenableFutureTask, "task");
                this.task = listenableFutureTask;
            }

            public final ListenableFutureTask<Bitmap> getTask() {
                return this.task;
            }
        }
    }
}
