package org.thoughtcrime.securesms.emoji;

import android.content.Context;
import androidx.core.util.Consumer;
import com.mobilecoin.lib.util.Hex;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.BufferedSource;
import okio.Okio;
import okio.Sink;
import org.thoughtcrime.securesms.emoji.EmojiFiles;

/* loaded from: classes4.dex */
public class EmojiDownloader {

    /* loaded from: classes4.dex */
    public interface Producer<T> {
        T produce();
    }

    public static EmojiFiles.Name downloadAndVerifyJsonFromRemote(Context context, EmojiFiles.Version version) throws IOException {
        return downloadAndVerifyFromRemote(context, version, new Producer() { // from class: org.thoughtcrime.securesms.emoji.EmojiDownloader$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.emoji.EmojiDownloader.Producer
            public final Object produce() {
                return EmojiDownloader.m1778$r8$lambda$rqu2_zX_FPHigEn5_aQ6EOPsPk(EmojiFiles.Version.this);
            }
        }, new Producer() { // from class: org.thoughtcrime.securesms.emoji.EmojiDownloader$$ExternalSyntheticLambda3
            @Override // org.thoughtcrime.securesms.emoji.EmojiDownloader.Producer
            public final Object produce() {
                return EmojiFiles.Name.forEmojiDataJson();
            }
        });
    }

    public static /* synthetic */ Response lambda$downloadAndVerifyJsonFromRemote$0(EmojiFiles.Version version) {
        return EmojiRemote.getObject(new EmojiJsonRequest(version.getVersion()));
    }

    public static EmojiFiles.Name downloadAndVerifyImageFromRemote(Context context, EmojiFiles.Version version, String str, String str2, String str3) throws IOException {
        return downloadAndVerifyFromRemote(context, version, new Producer(str, str2, str3) { // from class: org.thoughtcrime.securesms.emoji.EmojiDownloader$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$1;
            public final /* synthetic */ String f$2;
            public final /* synthetic */ String f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // org.thoughtcrime.securesms.emoji.EmojiDownloader.Producer
            public final Object produce() {
                return EmojiDownloader.$r8$lambda$RRSUUZoeFNBia9u_rrKE1OVrxDs(EmojiFiles.Version.this, this.f$1, this.f$2, this.f$3);
            }
        }, new Producer(str2) { // from class: org.thoughtcrime.securesms.emoji.EmojiDownloader$$ExternalSyntheticLambda1
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.emoji.EmojiDownloader.Producer
            public final Object produce() {
                return EmojiDownloader.m1777$r8$lambda$XZrfhskW8DkF5fA6oo9su7_NI(this.f$0);
            }
        });
    }

    public static /* synthetic */ Response lambda$downloadAndVerifyImageFromRemote$1(EmojiFiles.Version version, String str, String str2, String str3) {
        return EmojiRemote.getObject(new EmojiImageRequest(version.getVersion(), str, str2, str3));
    }

    public static /* synthetic */ EmojiFiles.Name lambda$downloadAndVerifyImageFromRemote$2(String str) {
        return new EmojiFiles.Name(str, UUID.randomUUID());
    }

    public static /* synthetic */ Response lambda$streamFileFromRemote$3(EmojiFiles.Version version, String str, String str2) {
        return EmojiRemote.getObject(new EmojiFileRequest(version.getVersion(), str, str2));
    }

    public static void streamFileFromRemote(EmojiFiles.Version version, String str, String str2, Consumer<InputStream> consumer) throws IOException {
        streamFromRemote(new Producer(str, str2) { // from class: org.thoughtcrime.securesms.emoji.EmojiDownloader$$ExternalSyntheticLambda4
            public final /* synthetic */ String f$1;
            public final /* synthetic */ String f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.thoughtcrime.securesms.emoji.EmojiDownloader.Producer
            public final Object produce() {
                return EmojiDownloader.$r8$lambda$KE3SnnX7R6nmhVHhHtyvklzcH3E(EmojiFiles.Version.this, this.f$1, this.f$2);
            }
        }, consumer);
    }

    private static EmojiFiles.Name downloadAndVerifyFromRemote(Context context, EmojiFiles.Version version, Producer<Response> producer, Producer<EmojiFiles.Name> producer2) throws IOException {
        Response produce = producer.produce();
        try {
            if (!produce.isSuccessful()) {
                throw new IOException("Unsuccessful response " + produce.code());
            } else if (produce.body() != null) {
                String mD5FromResponse = getMD5FromResponse(produce);
                if (mD5FromResponse != null) {
                    EmojiFiles.Name produce2 = producer2.produce();
                    OutputStream openForWriting = EmojiFiles.openForWriting(context, version, produce2.getUuid());
                    BufferedSource source = produce.body().source();
                    Sink sink = Okio.sink(openForWriting);
                    Okio.buffer(source).readAll(sink);
                    openForWriting.flush();
                    source.close();
                    sink.close();
                    byte[] md5 = EmojiFiles.getMd5(context, version, produce2.getUuid());
                    openForWriting.close();
                    if (Arrays.equals(md5, Hex.toByteArray(mD5FromResponse))) {
                        produce.close();
                        return produce2;
                    }
                    EmojiFiles.delete(context, version, produce2.getUuid());
                    throw new IOException("MD5 Mismatch.");
                }
                throw new IOException("Invalid ETag on response");
            } else {
                throw new IOException("No response body");
            }
        } catch (Throwable th) {
            if (produce != null) {
                try {
                    produce.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    private static void streamFromRemote(Producer<Response> producer, Consumer<InputStream> consumer) throws IOException {
        Response produce = producer.produce();
        try {
            if (produce.isSuccessful()) {
                ResponseBody body = produce.body();
                if (body != null) {
                    consumer.accept(Okio.buffer(body.source()).inputStream());
                    produce.close();
                    return;
                }
                throw new IOException("No response body");
            }
            throw new IOException("Unsuccessful response " + produce.code());
        } catch (Throwable th) {
            if (produce != null) {
                try {
                    produce.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    private static String getMD5FromResponse(Response response) {
        Matcher matcher = Pattern.compile(".*([a-f0-9]{32}).*").matcher(response.header("etag"));
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }
}
