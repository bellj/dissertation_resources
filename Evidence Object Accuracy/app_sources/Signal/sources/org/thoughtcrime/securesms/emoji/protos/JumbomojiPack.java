package org.thoughtcrime.securesms.emoji.protos;

import com.google.protobuf.AbstractMessageLite;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Parser;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.emoji.protos.JumbomojiItem;

/* loaded from: classes4.dex */
public final class JumbomojiPack extends GeneratedMessageLite<JumbomojiPack, Builder> implements JumbomojiPackOrBuilder {
    private static final JumbomojiPack DEFAULT_INSTANCE;
    public static final int ITEMS_FIELD_NUMBER;
    private static volatile Parser<JumbomojiPack> PARSER;
    private Internal.ProtobufList<JumbomojiItem> items_ = GeneratedMessageLite.emptyProtobufList();

    private JumbomojiPack() {
    }

    @Override // org.thoughtcrime.securesms.emoji.protos.JumbomojiPackOrBuilder
    public List<JumbomojiItem> getItemsList() {
        return this.items_;
    }

    public List<? extends JumbomojiItemOrBuilder> getItemsOrBuilderList() {
        return this.items_;
    }

    @Override // org.thoughtcrime.securesms.emoji.protos.JumbomojiPackOrBuilder
    public int getItemsCount() {
        return this.items_.size();
    }

    @Override // org.thoughtcrime.securesms.emoji.protos.JumbomojiPackOrBuilder
    public JumbomojiItem getItems(int i) {
        return this.items_.get(i);
    }

    public JumbomojiItemOrBuilder getItemsOrBuilder(int i) {
        return this.items_.get(i);
    }

    private void ensureItemsIsMutable() {
        Internal.ProtobufList<JumbomojiItem> protobufList = this.items_;
        if (!protobufList.isModifiable()) {
            this.items_ = GeneratedMessageLite.mutableCopy(protobufList);
        }
    }

    public void setItems(int i, JumbomojiItem jumbomojiItem) {
        jumbomojiItem.getClass();
        ensureItemsIsMutable();
        this.items_.set(i, jumbomojiItem);
    }

    public void addItems(JumbomojiItem jumbomojiItem) {
        jumbomojiItem.getClass();
        ensureItemsIsMutable();
        this.items_.add(jumbomojiItem);
    }

    public void addItems(int i, JumbomojiItem jumbomojiItem) {
        jumbomojiItem.getClass();
        ensureItemsIsMutable();
        this.items_.add(i, jumbomojiItem);
    }

    public void addAllItems(Iterable<? extends JumbomojiItem> iterable) {
        ensureItemsIsMutable();
        AbstractMessageLite.addAll((Iterable) iterable, (List) this.items_);
    }

    public void clearItems() {
        this.items_ = GeneratedMessageLite.emptyProtobufList();
    }

    public void removeItems(int i) {
        ensureItemsIsMutable();
        this.items_.remove(i);
    }

    public static JumbomojiPack parseFrom(ByteBuffer byteBuffer) throws InvalidProtocolBufferException {
        return (JumbomojiPack) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer);
    }

    public static JumbomojiPack parseFrom(ByteBuffer byteBuffer, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (JumbomojiPack) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteBuffer, extensionRegistryLite);
    }

    public static JumbomojiPack parseFrom(ByteString byteString) throws InvalidProtocolBufferException {
        return (JumbomojiPack) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString);
    }

    public static JumbomojiPack parseFrom(ByteString byteString, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (JumbomojiPack) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, byteString, extensionRegistryLite);
    }

    public static JumbomojiPack parseFrom(byte[] bArr) throws InvalidProtocolBufferException {
        return (JumbomojiPack) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr);
    }

    public static JumbomojiPack parseFrom(byte[] bArr, ExtensionRegistryLite extensionRegistryLite) throws InvalidProtocolBufferException {
        return (JumbomojiPack) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, bArr, extensionRegistryLite);
    }

    public static JumbomojiPack parseFrom(InputStream inputStream) throws IOException {
        return (JumbomojiPack) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static JumbomojiPack parseFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (JumbomojiPack) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static JumbomojiPack parseDelimitedFrom(InputStream inputStream) throws IOException {
        return (JumbomojiPack) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream);
    }

    public static JumbomojiPack parseDelimitedFrom(InputStream inputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (JumbomojiPack) GeneratedMessageLite.parseDelimitedFrom(DEFAULT_INSTANCE, inputStream, extensionRegistryLite);
    }

    public static JumbomojiPack parseFrom(CodedInputStream codedInputStream) throws IOException {
        return (JumbomojiPack) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream);
    }

    public static JumbomojiPack parseFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) throws IOException {
        return (JumbomojiPack) GeneratedMessageLite.parseFrom(DEFAULT_INSTANCE, codedInputStream, extensionRegistryLite);
    }

    public static Builder newBuilder() {
        return DEFAULT_INSTANCE.createBuilder();
    }

    public static Builder newBuilder(JumbomojiPack jumbomojiPack) {
        return DEFAULT_INSTANCE.createBuilder(jumbomojiPack);
    }

    /* loaded from: classes4.dex */
    public static final class Builder extends GeneratedMessageLite.Builder<JumbomojiPack, Builder> implements JumbomojiPackOrBuilder {
        /* synthetic */ Builder(AnonymousClass1 r1) {
            this();
        }

        private Builder() {
            super(JumbomojiPack.DEFAULT_INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.emoji.protos.JumbomojiPackOrBuilder
        public List<JumbomojiItem> getItemsList() {
            return Collections.unmodifiableList(((JumbomojiPack) this.instance).getItemsList());
        }

        @Override // org.thoughtcrime.securesms.emoji.protos.JumbomojiPackOrBuilder
        public int getItemsCount() {
            return ((JumbomojiPack) this.instance).getItemsCount();
        }

        @Override // org.thoughtcrime.securesms.emoji.protos.JumbomojiPackOrBuilder
        public JumbomojiItem getItems(int i) {
            return ((JumbomojiPack) this.instance).getItems(i);
        }

        public Builder setItems(int i, JumbomojiItem jumbomojiItem) {
            copyOnWrite();
            ((JumbomojiPack) this.instance).setItems(i, jumbomojiItem);
            return this;
        }

        public Builder setItems(int i, JumbomojiItem.Builder builder) {
            copyOnWrite();
            ((JumbomojiPack) this.instance).setItems(i, builder.build());
            return this;
        }

        public Builder addItems(JumbomojiItem jumbomojiItem) {
            copyOnWrite();
            ((JumbomojiPack) this.instance).addItems(jumbomojiItem);
            return this;
        }

        public Builder addItems(int i, JumbomojiItem jumbomojiItem) {
            copyOnWrite();
            ((JumbomojiPack) this.instance).addItems(i, jumbomojiItem);
            return this;
        }

        public Builder addItems(JumbomojiItem.Builder builder) {
            copyOnWrite();
            ((JumbomojiPack) this.instance).addItems(builder.build());
            return this;
        }

        public Builder addItems(int i, JumbomojiItem.Builder builder) {
            copyOnWrite();
            ((JumbomojiPack) this.instance).addItems(i, builder.build());
            return this;
        }

        public Builder addAllItems(Iterable<? extends JumbomojiItem> iterable) {
            copyOnWrite();
            ((JumbomojiPack) this.instance).addAllItems(iterable);
            return this;
        }

        public Builder clearItems() {
            copyOnWrite();
            ((JumbomojiPack) this.instance).clearItems();
            return this;
        }

        public Builder removeItems(int i) {
            copyOnWrite();
            ((JumbomojiPack) this.instance).removeItems(i);
            return this;
        }
    }

    /* renamed from: org.thoughtcrime.securesms.emoji.protos.JumbomojiPack$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke;

        static {
            int[] iArr = new int[GeneratedMessageLite.MethodToInvoke.values().length];
            $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke = iArr;
            try {
                iArr[GeneratedMessageLite.MethodToInvoke.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.NEW_BUILDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.BUILD_MESSAGE_INFO.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_DEFAULT_INSTANCE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_PARSER.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.GET_MEMOIZED_IS_INITIALIZED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[GeneratedMessageLite.MethodToInvoke.SET_MEMOIZED_IS_INITIALIZED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
        }
    }

    @Override // com.google.protobuf.GeneratedMessageLite
    protected final Object dynamicMethod(GeneratedMessageLite.MethodToInvoke methodToInvoke, Object obj, Object obj2) {
        switch (AnonymousClass1.$SwitchMap$com$google$protobuf$GeneratedMessageLite$MethodToInvoke[methodToInvoke.ordinal()]) {
            case 1:
                return new JumbomojiPack();
            case 2:
                return new Builder(null);
            case 3:
                return GeneratedMessageLite.newMessageInfo(DEFAULT_INSTANCE, "\u0000\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"items_", JumbomojiItem.class});
            case 4:
                return DEFAULT_INSTANCE;
            case 5:
                Parser<JumbomojiPack> parser = PARSER;
                if (parser == null) {
                    synchronized (JumbomojiPack.class) {
                        parser = PARSER;
                        if (parser == null) {
                            parser = new GeneratedMessageLite.DefaultInstanceBasedParser<>(DEFAULT_INSTANCE);
                            PARSER = parser;
                        }
                    }
                }
                return parser;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    static {
        JumbomojiPack jumbomojiPack = new JumbomojiPack();
        DEFAULT_INSTANCE = jumbomojiPack;
        GeneratedMessageLite.registerDefaultInstance(JumbomojiPack.class, jumbomojiPack);
    }

    public static JumbomojiPack getDefaultInstance() {
        return DEFAULT_INSTANCE;
    }

    public static Parser<JumbomojiPack> parser() {
        return DEFAULT_INSTANCE.getParserForType();
    }
}
