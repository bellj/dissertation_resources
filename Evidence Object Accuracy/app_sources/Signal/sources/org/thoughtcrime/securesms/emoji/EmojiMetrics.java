package org.thoughtcrime.securesms.emoji;

import kotlin.Metadata;

/* compiled from: EmojiSource.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0003HÆ\u0003J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J'\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0003HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\b¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiMetrics;", "", "rawHeight", "", "rawWidth", "perRow", "(III)V", "getPerRow", "()I", "getRawHeight", "getRawWidth", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EmojiMetrics {
    private final int perRow;
    private final int rawHeight;
    private final int rawWidth;

    public static /* synthetic */ EmojiMetrics copy$default(EmojiMetrics emojiMetrics, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 1) != 0) {
            i = emojiMetrics.rawHeight;
        }
        if ((i4 & 2) != 0) {
            i2 = emojiMetrics.rawWidth;
        }
        if ((i4 & 4) != 0) {
            i3 = emojiMetrics.perRow;
        }
        return emojiMetrics.copy(i, i2, i3);
    }

    public final int component1() {
        return this.rawHeight;
    }

    public final int component2() {
        return this.rawWidth;
    }

    public final int component3() {
        return this.perRow;
    }

    public final EmojiMetrics copy(int i, int i2, int i3) {
        return new EmojiMetrics(i, i2, i3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof EmojiMetrics)) {
            return false;
        }
        EmojiMetrics emojiMetrics = (EmojiMetrics) obj;
        return this.rawHeight == emojiMetrics.rawHeight && this.rawWidth == emojiMetrics.rawWidth && this.perRow == emojiMetrics.perRow;
    }

    public int hashCode() {
        return (((this.rawHeight * 31) + this.rawWidth) * 31) + this.perRow;
    }

    public String toString() {
        return "EmojiMetrics(rawHeight=" + this.rawHeight + ", rawWidth=" + this.rawWidth + ", perRow=" + this.perRow + ')';
    }

    public EmojiMetrics(int i, int i2, int i3) {
        this.rawHeight = i;
        this.rawWidth = i2;
        this.perRow = i3;
    }

    public final int getPerRow() {
        return this.perRow;
    }

    public final int getRawHeight() {
        return this.rawHeight;
    }

    public final int getRawWidth() {
        return this.rawWidth;
    }
}
