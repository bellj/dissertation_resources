package org.thoughtcrime.securesms.emoji;

import android.content.Context;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.kotlin.ExtensionsKt;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.io.CloseableKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import okio.BufferedSource;
import okio.HashingSink;
import okio.Okio;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.RecipientDatabase;

/* compiled from: EmojiFiles.kt */
@Metadata(d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001:\u0004\u001a\u001b\u001c\u001dB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0007J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u001a\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007J\"\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0007J\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u0014H\u0007J&\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0013\u001a\u00020\u0014J \u0010\u0018\u001a\u00020\u00192\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0007¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiFiles;", "", "()V", "delete", "", "context", "Landroid/content/Context;", "version", "Lorg/thoughtcrime/securesms/emoji/EmojiFiles$Version;", RecipientDatabase.SERVICE_ID, "Ljava/util/UUID;", "getBaseDirectory", "Ljava/io/File;", "getLatestEmojiData", "Lorg/thoughtcrime/securesms/emoji/ParsedEmojiData;", "getMd5", "", "openForReading", "Ljava/io/InputStream;", "name", "", "openForReadingJumbo", "names", "Lorg/thoughtcrime/securesms/emoji/EmojiFiles$JumboCollection;", "openForWriting", "Ljava/io/OutputStream;", "JumboCollection", "Name", "NameCollection", "Version", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EmojiFiles {
    public static final EmojiFiles INSTANCE = new EmojiFiles();

    private EmojiFiles() {
    }

    @JvmStatic
    public static final File getBaseDirectory(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        return EmojiFilesKt.getEmojiDirectory(context);
    }

    @JvmStatic
    public static final void delete(Context context, Version version, UUID uuid) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(version, "version");
        Intrinsics.checkNotNullParameter(uuid, RecipientDatabase.SERVICE_ID);
        try {
            version.getFile(context, uuid).delete();
        } catch (IOException unused) {
            Log.i("EmojiFiles", "Failed to delete file.");
        }
    }

    @JvmStatic
    public static final InputStream openForReading(Context context, String str) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(str, "name");
        Version readVersion$default = Version.Companion.readVersion$default(Version.Companion, context, false, 2, null);
        if (readVersion$default != null) {
            UUID uUIDForName = NameCollection.Companion.read(context, readVersion$default).getUUIDForName(str);
            if (uUIDForName != null) {
                return EmojiFilesKt.getInputStream(context, readVersion$default.getFile(context, uUIDForName));
            }
            throw new IOException("Could not get UUID for name " + str);
        }
        throw new IOException("No emoji version is present on disk");
    }

    public final InputStream openForReadingJumbo(Context context, Version version, JumboCollection jumboCollection, String str) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(version, "version");
        Intrinsics.checkNotNullParameter(jumboCollection, "names");
        Intrinsics.checkNotNullParameter(str, "name");
        UUID uUIDForName = jumboCollection.getUUIDForName(str);
        if (uUIDForName != null) {
            return EmojiFilesKt.getInputStream(context, version.getFile(context, uUIDForName));
        }
        throw new IOException("Could not get UUID for name " + str);
    }

    @JvmStatic
    public static final OutputStream openForWriting(Context context, Version version, UUID uuid) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(version, "version");
        Intrinsics.checkNotNullParameter(uuid, RecipientDatabase.SERVICE_ID);
        return EmojiFilesKt.getOutputStream(context, version.getFile(context, uuid));
    }

    @JvmStatic
    public static final byte[] getMd5(Context context, Version version, UUID uuid) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(version, "version");
        Intrinsics.checkNotNullParameter(uuid, RecipientDatabase.SERVICE_ID);
        File file = version.getFile(context, uuid);
        try {
            HashingSink md5 = HashingSink.Companion.md5(Okio.blackhole());
            BufferedSource buffer = Okio.buffer(Okio.source(EmojiFilesKt.getInputStream(context, file)));
            buffer.readAll(md5);
            byte[] byteArray = md5.hash().toByteArray();
            CloseableKt.closeFinally(buffer, null);
            CloseableKt.closeFinally(md5, null);
            return byteArray;
        } catch (Exception e) {
            Log.i("EmojiFiles", "Could not read emoji data file md5", e);
            return null;
        }
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [java.lang.Throwable, org.thoughtcrime.securesms.emoji.ParsedEmojiData] */
    @JvmStatic
    public static final ParsedEmojiData getLatestEmojiData(Context context, Version version) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(version, "version");
        UUID uUIDForEmojiData = NameCollection.Companion.read(context, version).getUUIDForEmojiData();
        th = 0;
        if (uUIDForEmojiData == null) {
            return th;
        }
        InputStream inputStream = EmojiFilesKt.getInputStream(context, version.getFile(context, uUIDForEmojiData));
        try {
            Object r4 = EmojiJsonParser.INSTANCE.m1780parsegIAlus(inputStream, EmojiFiles$getLatestEmojiData$1$1.INSTANCE);
            Throwable r0 = Result.m155exceptionOrNullimpl(r4);
            if (r0 != null) {
                Log.w("EmojiFiles", "Failed to parse emoji_data", r0);
                r4 = th;
            }
            return (ParsedEmojiData) r4;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    /* compiled from: EmojiFiles.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B#\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0016\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0004\u001a\u00020\u0005R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiFiles$Version;", "", "version", "", RecipientDatabase.SERVICE_ID, "Ljava/util/UUID;", "density", "", "(ILjava/util/UUID;Ljava/lang/String;)V", "getDensity", "()Ljava/lang/String;", "getUuid", "()Ljava/util/UUID;", "getVersion", "()I", "getDirectory", "Ljava/io/File;", "context", "Landroid/content/Context;", "getFile", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Version {
        public static final Companion Companion = new Companion(null);
        private static final ObjectMapper objectMapper = ExtensionsKt.registerKotlinModule(new ObjectMapper());
        private final String density;
        private final UUID uuid;
        private final int version;

        @JvmStatic
        public static final boolean isVersionValid(Context context, Version version) {
            return Companion.isVersionValid(context, version);
        }

        @JvmStatic
        public static final Version readVersion(Context context) {
            return Companion.readVersion(context);
        }

        @JvmStatic
        public static final Version readVersion(Context context, boolean z) {
            return Companion.readVersion(context, z);
        }

        @JvmStatic
        public static final void writeVersion(Context context, Version version) {
            Companion.writeVersion(context, version);
        }

        public Version(@JsonProperty int i, @JsonProperty UUID uuid, @JsonProperty String str) {
            Intrinsics.checkNotNullParameter(uuid, RecipientDatabase.SERVICE_ID);
            Intrinsics.checkNotNullParameter(str, "density");
            this.version = i;
            this.uuid = uuid;
            this.density = str;
        }

        public final String getDensity() {
            return this.density;
        }

        public final UUID getUuid() {
            return this.uuid;
        }

        public final int getVersion() {
            return this.version;
        }

        public final File getFile(Context context, UUID uuid) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(uuid, RecipientDatabase.SERVICE_ID);
            return new File(getDirectory(context), uuid.toString());
        }

        private final File getDirectory(Context context) {
            File file = new File(EmojiFilesKt.getEmojiDirectory(context), this.uuid.toString());
            file.mkdir();
            return file;
        }

        /* compiled from: EmojiFiles.kt */
        @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001a\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0007J\u001c\u0010\u000b\u001a\u0004\u0018\u00010\n2\u0006\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\f\u001a\u00020\u0006H\u0007J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiFiles$Version$Companion;", "", "()V", "objectMapper", "Lcom/fasterxml/jackson/databind/ObjectMapper;", "isVersionValid", "", "context", "Landroid/content/Context;", "version", "Lorg/thoughtcrime/securesms/emoji/EmojiFiles$Version;", "readVersion", "skipValidation", "writeVersion", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            @JvmStatic
            public final Version readVersion(Context context) {
                Intrinsics.checkNotNullParameter(context, "context");
                return readVersion$default(this, context, false, 2, null);
            }

            private Companion() {
            }

            public static /* synthetic */ Version readVersion$default(Companion companion, Context context, boolean z, int i, Object obj) {
                if ((i & 2) != 0) {
                    z = false;
                }
                return companion.readVersion(context, z);
            }

            @JvmStatic
            public final Version readVersion(Context context, boolean z) {
                Version version;
                Intrinsics.checkNotNullParameter(context, "context");
                try {
                    InputStream inputStream = EmojiFilesKt.getInputStream(context, EmojiFilesKt.getVersionFile(context));
                    version = (Version) Version.objectMapper.readValue(inputStream, Version.class);
                    CloseableKt.closeFinally(inputStream, null);
                } catch (Exception e) {
                    Log.w("EmojiFiles", "Could not read current emoji version from disk.", e);
                    version = null;
                }
                if (z || isVersionValid(context, version)) {
                    return version;
                }
                return null;
            }

            @JvmStatic
            public final void writeVersion(Context context, Version version) {
                Intrinsics.checkNotNullParameter(context, "context");
                Intrinsics.checkNotNullParameter(version, "version");
                File file = EmojiFilesKt.getVersionFile(context);
                try {
                    if (file.exists()) {
                        file.delete();
                    }
                    OutputStream outputStream = EmojiFilesKt.getOutputStream(context, file);
                    Version.objectMapper.writeValue(outputStream, version);
                    Unit unit = Unit.INSTANCE;
                    CloseableKt.closeFinally(outputStream, null);
                } catch (Exception unused) {
                    Log.w("EmojiFiles", "Could not write current emoji version from disk.");
                }
            }

            @JvmStatic
            public final boolean isVersionValid(Context context, Version version) {
                Intrinsics.checkNotNullParameter(context, "context");
                boolean z = false;
                if (version == null) {
                    Log.d("EmojiFiles", "Version does not exist.");
                    return false;
                }
                NameCollection read = NameCollection.Companion.read(context, version);
                if (read.getNames().isEmpty()) {
                    Log.d("EmojiFiles", "NameCollection file is empty.");
                } else {
                    Log.d("EmojiFiles", "Verifying all name files exist.");
                    List<Name> names = read.getNames();
                    ArrayList<File> arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(names, 10));
                    for (Name name : names) {
                        arrayList.add(version.getFile(context, name.getUuid()));
                    }
                    if (!arrayList.isEmpty()) {
                        for (File file : arrayList) {
                            if (!file.exists()) {
                                break;
                            }
                        }
                    }
                    z = true;
                    Log.d("EmojiFiles", "All names exist? " + z);
                }
                return z;
            }
        }
    }

    /* compiled from: EmojiFiles.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u000b2\u00020\u0001:\u0001\u000bB\u0019\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiFiles$Name;", "", "name", "", RecipientDatabase.SERVICE_ID, "Ljava/util/UUID;", "(Ljava/lang/String;Ljava/util/UUID;)V", "getName", "()Ljava/lang/String;", "getUuid", "()Ljava/util/UUID;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Name {
        public static final Companion Companion = new Companion(null);
        private final String name;
        private final UUID uuid;

        @JvmStatic
        public static final Name forEmojiDataJson() {
            return Companion.forEmojiDataJson();
        }

        /* compiled from: EmojiFiles.kt */
        @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0007¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiFiles$Name$Companion;", "", "()V", "forEmojiDataJson", "Lorg/thoughtcrime/securesms/emoji/EmojiFiles$Name;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            @JvmStatic
            public final Name forEmojiDataJson() {
                UUID randomUUID = UUID.randomUUID();
                Intrinsics.checkNotNullExpressionValue(randomUUID, "randomUUID()");
                return new Name("emoji_data.json", randomUUID);
            }
        }

        public Name(@JsonProperty String str, @JsonProperty UUID uuid) {
            Intrinsics.checkNotNullParameter(str, "name");
            Intrinsics.checkNotNullParameter(uuid, RecipientDatabase.SERVICE_ID);
            this.name = str;
            this.uuid = uuid;
        }

        public final String getName() {
            return this.name;
        }

        public final UUID getUuid() {
            return this.uuid;
        }
    }

    /* compiled from: EmojiFiles.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B\u001f\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u000e\b\u0001\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\n\u0010\f\u001a\u0004\u0018\u00010\u0003H\u0007J\u0012\u0010\r\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u000e\u001a\u00020\u000fH\u0007R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiFiles$NameCollection;", "", "versionUuid", "Ljava/util/UUID;", "names", "", "Lorg/thoughtcrime/securesms/emoji/EmojiFiles$Name;", "(Ljava/util/UUID;Ljava/util/List;)V", "getNames", "()Ljava/util/List;", "getVersionUuid", "()Ljava/util/UUID;", "getUUIDForEmojiData", "getUUIDForName", "name", "", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class NameCollection {
        public static final Companion Companion = new Companion(null);
        private static final ObjectMapper objectMapper = ExtensionsKt.registerKotlinModule(new ObjectMapper());
        private final List<Name> names;
        private final UUID versionUuid;

        @JvmStatic
        public static final NameCollection append(Context context, NameCollection nameCollection, Name name) {
            return Companion.append(context, nameCollection, name);
        }

        @JvmStatic
        public static final NameCollection read(Context context, Version version) {
            return Companion.read(context, version);
        }

        /* compiled from: EmojiFiles.kt */
        @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000bH\u0007J\u0018\u0010\f\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\r\u001a\u00020\u000eH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiFiles$NameCollection$Companion;", "", "()V", "objectMapper", "Lcom/fasterxml/jackson/databind/ObjectMapper;", "append", "Lorg/thoughtcrime/securesms/emoji/EmojiFiles$NameCollection;", "context", "Landroid/content/Context;", "nameCollection", "name", "Lorg/thoughtcrime/securesms/emoji/EmojiFiles$Name;", "read", "version", "Lorg/thoughtcrime/securesms/emoji/EmojiFiles$Version;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            @JvmStatic
            public final NameCollection read(Context context, Version version) {
                Intrinsics.checkNotNullParameter(context, "context");
                Intrinsics.checkNotNullParameter(version, "version");
                try {
                    InputStream inputStream = EmojiFilesKt.getInputStream(context, EmojiFilesKt.getNameFile(context, version.getUuid()));
                    NameCollection nameCollection = (NameCollection) NameCollection.objectMapper.readValue(inputStream, new EmojiFiles$NameCollection$Companion$read$lambda0$$inlined$readValue$1());
                    CloseableKt.closeFinally(inputStream, null);
                    return nameCollection;
                } catch (Exception unused) {
                    return new NameCollection(version.getUuid(), CollectionsKt__CollectionsKt.emptyList());
                }
            }

            @JvmStatic
            public final NameCollection append(Context context, NameCollection nameCollection, Name name) {
                Intrinsics.checkNotNullParameter(context, "context");
                Intrinsics.checkNotNullParameter(nameCollection, "nameCollection");
                Intrinsics.checkNotNullParameter(name, "name");
                NameCollection nameCollection2 = new NameCollection(nameCollection.getVersionUuid(), CollectionsKt___CollectionsKt.plus((Collection<? extends Name>) ((Collection<? extends Object>) nameCollection.getNames()), name));
                OutputStream outputStream = EmojiFilesKt.getOutputStream(context, EmojiFilesKt.getNameFile(context, nameCollection.getVersionUuid()));
                try {
                    NameCollection.objectMapper.writeValue(outputStream, nameCollection2);
                    Unit unit = Unit.INSTANCE;
                    th = null;
                    return nameCollection2;
                } finally {
                    try {
                        throw th;
                    } finally {
                    }
                }
            }
        }

        public NameCollection(@JsonProperty UUID uuid, @JsonProperty List<Name> list) {
            Intrinsics.checkNotNullParameter(uuid, "versionUuid");
            Intrinsics.checkNotNullParameter(list, "names");
            this.versionUuid = uuid;
            this.names = list;
        }

        public final List<Name> getNames() {
            return this.names;
        }

        public final UUID getVersionUuid() {
            return this.versionUuid;
        }

        @JsonIgnore
        public final UUID getUUIDForEmojiData() {
            return getUUIDForName("emoji_data.json");
        }

        @JsonIgnore
        public final UUID getUUIDForName(String str) {
            Object obj;
            Intrinsics.checkNotNullParameter(str, "name");
            Iterator<T> it = this.names.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (Intrinsics.areEqual(((Name) obj).getName(), str)) {
                    break;
                }
            }
            Name name = (Name) obj;
            if (name != null) {
                return name.getUuid();
            }
            return null;
        }
    }

    /* compiled from: EmojiFiles.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u0000 \u000f2\u00020\u0001:\u0001\u000fB\u001f\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u000e\b\u0001\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\u0012\u0010\f\u001a\u0004\u0018\u00010\u00032\u0006\u0010\r\u001a\u00020\u000eH\u0007R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiFiles$JumboCollection;", "", "versionUuid", "Ljava/util/UUID;", "names", "", "Lorg/thoughtcrime/securesms/emoji/EmojiFiles$Name;", "(Ljava/util/UUID;Ljava/util/List;)V", "getNames", "()Ljava/util/List;", "getVersionUuid", "()Ljava/util/UUID;", "getUUIDForName", "name", "", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class JumboCollection {
        public static final Companion Companion = new Companion(null);
        private static final ObjectMapper objectMapper = ExtensionsKt.registerKotlinModule(new ObjectMapper());
        private final List<Name> names;
        private final UUID versionUuid;

        @JvmStatic
        public static final JumboCollection append(Context context, JumboCollection jumboCollection, Name name) {
            return Companion.append(context, jumboCollection, name);
        }

        @JvmStatic
        public static final JumboCollection read(Context context, Version version) {
            return Companion.read(context, version);
        }

        /* compiled from: EmojiFiles.kt */
        @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000bH\u0007J\u0018\u0010\f\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\r\u001a\u00020\u000eH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiFiles$JumboCollection$Companion;", "", "()V", "objectMapper", "Lcom/fasterxml/jackson/databind/ObjectMapper;", "append", "Lorg/thoughtcrime/securesms/emoji/EmojiFiles$JumboCollection;", "context", "Landroid/content/Context;", "nameCollection", "name", "Lorg/thoughtcrime/securesms/emoji/EmojiFiles$Name;", "read", "version", "Lorg/thoughtcrime/securesms/emoji/EmojiFiles$Version;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            @JvmStatic
            public final JumboCollection read(Context context, Version version) {
                Intrinsics.checkNotNullParameter(context, "context");
                Intrinsics.checkNotNullParameter(version, "version");
                try {
                    InputStream inputStream = EmojiFilesKt.getInputStream(context, EmojiFilesKt.getJumboFile(context, version.getUuid()));
                    JumboCollection jumboCollection = (JumboCollection) JumboCollection.objectMapper.readValue(inputStream, new EmojiFiles$JumboCollection$Companion$read$lambda0$$inlined$readValue$1());
                    CloseableKt.closeFinally(inputStream, null);
                    return jumboCollection;
                } catch (Exception unused) {
                    return new JumboCollection(version.getUuid(), CollectionsKt__CollectionsKt.emptyList());
                }
            }

            @JvmStatic
            public final JumboCollection append(Context context, JumboCollection jumboCollection, Name name) {
                Intrinsics.checkNotNullParameter(context, "context");
                Intrinsics.checkNotNullParameter(jumboCollection, "nameCollection");
                Intrinsics.checkNotNullParameter(name, "name");
                JumboCollection jumboCollection2 = new JumboCollection(jumboCollection.getVersionUuid(), CollectionsKt___CollectionsKt.plus((Collection<? extends Name>) ((Collection<? extends Object>) jumboCollection.getNames()), name));
                OutputStream outputStream = EmojiFilesKt.getOutputStream(context, EmojiFilesKt.getJumboFile(context, jumboCollection.getVersionUuid()));
                try {
                    JumboCollection.objectMapper.writeValue(outputStream, jumboCollection2);
                    Unit unit = Unit.INSTANCE;
                    th = null;
                    return jumboCollection2;
                } finally {
                    try {
                        throw th;
                    } finally {
                    }
                }
            }
        }

        public JumboCollection(@JsonProperty UUID uuid, @JsonProperty List<Name> list) {
            Intrinsics.checkNotNullParameter(uuid, "versionUuid");
            Intrinsics.checkNotNullParameter(list, "names");
            this.versionUuid = uuid;
            this.names = list;
        }

        public final List<Name> getNames() {
            return this.names;
        }

        public final UUID getVersionUuid() {
            return this.versionUuid;
        }

        @JsonIgnore
        public final UUID getUUIDForName(String str) {
            Object obj;
            Intrinsics.checkNotNullParameter(str, "name");
            Iterator<T> it = this.names.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (Intrinsics.areEqual(((Name) obj).getName(), str)) {
                    break;
                }
            }
            Name name = (Name) obj;
            if (name != null) {
                return name.getUuid();
            }
            return null;
        }
    }
}
