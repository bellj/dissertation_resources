package org.thoughtcrime.securesms.emoji;

import android.app.Application;
import android.net.Uri;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.ResultKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.emoji.Emoji;
import org.thoughtcrime.securesms.components.emoji.EmojiPageModel;
import org.thoughtcrime.securesms.components.emoji.parsing.EmojiDrawInfo;
import org.thoughtcrime.securesms.components.emoji.parsing.EmojiTree;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.emoji.EmojiFiles;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.ScreenDensity;

/* compiled from: EmojiSource.kt */
@Metadata(d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0010 \n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 62\u00020\u0001:\u00016B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0001\u0012\u0016\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006j\u0002`\t¢\u0006\u0002\u0010\nR-\u0010\u000b\u001a\u0014\u0012\u0004\u0012\u00020\r\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\u000e0\f8FX\u0002¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u000f\u0010\u0010R\u0018\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00140\u000eX\u0005¢\u0006\u0006\u001a\u0004\b\u0015\u0010\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0018\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\r0\u000eX\u0005¢\u0006\u0006\u001a\u0004\b\u001a\u0010\u0016R\u0018\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00140\u000eX\u0005¢\u0006\u0006\u001a\u0004\b\u001c\u0010\u0016R\u000e\u0010\u0004\u001a\u00020\u0001X\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006j\u0002`\tX\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u001d\u001a\u00020\u001e8FX\u0002¢\u0006\f\n\u0004\b!\u0010\u0012\u001a\u0004\b\u001f\u0010 R\u0012\u0010\"\u001a\u00020\rX\u0005¢\u0006\u0006\u001a\u0004\b#\u0010$R\u001e\u0010%\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\r0\fX\u0005¢\u0006\u0006\u001a\u0004\b&\u0010\u0010R\u001b\u0010'\u001a\u00020(8FX\u0002¢\u0006\f\n\u0004\b+\u0010\u0012\u001a\u0004\b)\u0010*R\u0012\u0010,\u001a\u00020-X\u0005¢\u0006\u0006\u001a\u0004\b.\u0010/R\u0018\u00100\u001a\b\u0012\u0004\u0012\u0002010\u000eX\u0005¢\u0006\u0006\u001a\u0004\b2\u0010\u0016R'\u00103\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\r0\f8FX\u0002¢\u0006\f\n\u0004\b5\u0010\u0012\u001a\u0004\b4\u0010\u0010¨\u00067"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiSource;", "Lorg/thoughtcrime/securesms/emoji/EmojiData;", "decodeScale", "", "emojiData", "emojiPageFactory", "Lkotlin/Function1;", "Landroid/net/Uri;", "Lorg/thoughtcrime/securesms/emoji/EmojiPage;", "Lorg/thoughtcrime/securesms/emoji/EmojiPageFactory;", "(FLorg/thoughtcrime/securesms/emoji/EmojiData;Lkotlin/jvm/functions/Function1;)V", "canonicalToVariations", "", "", "", "getCanonicalToVariations", "()Ljava/util/Map;", "canonicalToVariations$delegate", "Lkotlin/Lazy;", "dataPages", "Lorg/thoughtcrime/securesms/components/emoji/EmojiPageModel;", "getDataPages", "()Ljava/util/List;", "getDecodeScale", "()F", "densities", "getDensities", "displayPages", "getDisplayPages", "emojiTree", "Lorg/thoughtcrime/securesms/components/emoji/parsing/EmojiTree;", "getEmojiTree", "()Lorg/thoughtcrime/securesms/components/emoji/parsing/EmojiTree;", "emojiTree$delegate", "format", "getFormat", "()Ljava/lang/String;", "jumboPages", "getJumboPages", "maxEmojiLength", "", "getMaxEmojiLength", "()I", "maxEmojiLength$delegate", "metrics", "Lorg/thoughtcrime/securesms/emoji/EmojiMetrics;", "getMetrics", "()Lorg/thoughtcrime/securesms/emoji/EmojiMetrics;", "obsolete", "Lorg/thoughtcrime/securesms/emoji/ObsoleteEmoji;", "getObsolete", "variationsToCanonical", "getVariationsToCanonical", "variationsToCanonical$delegate", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EmojiSource implements EmojiData {
    public static final Companion Companion = new Companion(null);
    private static final CountDownLatch emojiLatch = new CountDownLatch(1);
    private static final AtomicReference<EmojiSource> emojiSource = new AtomicReference<>();
    private final Lazy canonicalToVariations$delegate = LazyKt__LazyJVMKt.lazy(new Function0<Map<String, List<? extends String>>>(this) { // from class: org.thoughtcrime.securesms.emoji.EmojiSource$canonicalToVariations$2
        final /* synthetic */ EmojiSource this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        /* Return type fixed from 'java.util.Map<java.lang.String, java.util.List<java.lang.String>>' to match base method */
        @Override // kotlin.jvm.functions.Function0
        public final Map<String, List<? extends String>> invoke() {
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (EmojiPageModel emojiPageModel : this.this$0.getDataPages()) {
                for (Emoji emoji : emojiPageModel.getDisplayEmoji()) {
                    Intrinsics.checkNotNullExpressionValue(emoji, "page.displayEmoji");
                    Emoji emoji2 = emoji;
                    String value = emoji2.getValue();
                    Intrinsics.checkNotNullExpressionValue(value, "emoji.value");
                    List<String> variations = emoji2.getVariations();
                    Intrinsics.checkNotNullExpressionValue(variations, "emoji.variations");
                    linkedHashMap.put(value, variations);
                }
            }
            return linkedHashMap;
        }
    });
    private final float decodeScale;
    private final EmojiData emojiData;
    private final Function1<Uri, EmojiPage> emojiPageFactory;
    private final Lazy emojiTree$delegate = LazyKt__LazyJVMKt.lazy(new Function0<EmojiTree>(this) { // from class: org.thoughtcrime.securesms.emoji.EmojiSource$emojiTree$2
        final /* synthetic */ EmojiSource this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final EmojiTree invoke() {
            EmojiTree emojiTree = new EmojiTree();
            List<EmojiPageModel> dataPages = this.this$0.getDataPages();
            ArrayList<EmojiPageModel> arrayList = new ArrayList();
            for (Object obj : dataPages) {
                if (((EmojiPageModel) obj).getSpriteUri() != null) {
                    arrayList.add(obj);
                }
            }
            EmojiSource emojiSource2 = this.this$0;
            for (EmojiPageModel emojiPageModel : arrayList) {
                Function1 function1 = emojiSource2.emojiPageFactory;
                Uri spriteUri = emojiPageModel.getSpriteUri();
                Intrinsics.checkNotNull(spriteUri);
                EmojiPage emojiPage = (EmojiPage) function1.invoke(spriteUri);
                List<Emoji> displayEmoji = emojiPageModel.getDisplayEmoji();
                Intrinsics.checkNotNullExpressionValue(displayEmoji, "page.displayEmoji");
                int i = 0;
                for (Emoji emoji : displayEmoji) {
                    List<String> variations = emoji.getVariations();
                    Intrinsics.checkNotNullExpressionValue(variations, "emoji.variations");
                    int i2 = i;
                    int i3 = 0;
                    for (Object obj2 : variations) {
                        int i4 = i3 + 1;
                        if (i3 < 0) {
                            CollectionsKt__CollectionsKt.throwIndexOverflow();
                        }
                        String str = (String) obj2;
                        String rawVariation = emoji.getRawVariation(i3);
                        i2++;
                        Intrinsics.checkNotNullExpressionValue(str, "variation");
                        emojiTree.add(str, new EmojiDrawInfo(emojiPage, i2, str, rawVariation, emojiSource2.getJumboPages().get(rawVariation)));
                        i3 = i4;
                        emojiSource2 = emojiSource2;
                    }
                    i = i2;
                }
            }
            for (ObsoleteEmoji obsoleteEmoji : this.this$0.getObsolete()) {
                emojiTree.add(obsoleteEmoji.getObsolete(), emojiTree.getEmoji(obsoleteEmoji.getReplaceWith(), 0, obsoleteEmoji.getReplaceWith().length()));
            }
            return emojiTree;
        }
    });
    private final Lazy maxEmojiLength$delegate = LazyKt__LazyJVMKt.lazy(new Function0<Integer>(this) { // from class: org.thoughtcrime.securesms.emoji.EmojiSource$maxEmojiLength$2
        final /* synthetic */ EmojiSource this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Integer invoke() {
            List<EmojiPageModel> dataPages = this.this$0.getDataPages();
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(dataPages, 10));
            for (EmojiPageModel emojiPageModel : dataPages) {
                List<String> emoji = emojiPageModel.getEmoji();
                Intrinsics.checkNotNullExpressionValue(emoji, "it.emoji");
                ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(emoji, 10));
                for (String str : emoji) {
                    arrayList2.add(Integer.valueOf(str.length()));
                }
                arrayList.add(arrayList2);
            }
            return Integer.valueOf(EmojiSourceKt.access$maxOrZero(CollectionsKt__IterablesKt.flatten(arrayList)));
        }
    });
    private final Lazy variationsToCanonical$delegate = LazyKt__LazyJVMKt.lazy(new Function0<Map<String, String>>(this) { // from class: org.thoughtcrime.securesms.emoji.EmojiSource$variationsToCanonical$2
        final /* synthetic */ EmojiSource this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Map<String, String> invoke() {
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (EmojiPageModel emojiPageModel : this.this$0.getDataPages()) {
                for (Emoji emoji : emojiPageModel.getDisplayEmoji()) {
                    Intrinsics.checkNotNullExpressionValue(emoji, "page.displayEmoji");
                    Emoji emoji2 = emoji;
                    for (String str : emoji2.getVariations()) {
                        Intrinsics.checkNotNullExpressionValue(str, "emoji.variations");
                        String value = emoji2.getValue();
                        Intrinsics.checkNotNullExpressionValue(value, "emoji.value");
                        linkedHashMap.put(str, value);
                    }
                }
            }
            return linkedHashMap;
        }
    });

    public static final EmojiSource getLatest() {
        return Companion.getLatest();
    }

    @JvmStatic
    public static final void refresh() {
        Companion.refresh();
    }

    @Override // org.thoughtcrime.securesms.emoji.EmojiData
    public List<EmojiPageModel> getDataPages() {
        return this.emojiData.getDataPages();
    }

    @Override // org.thoughtcrime.securesms.emoji.EmojiData
    public List<String> getDensities() {
        return this.emojiData.getDensities();
    }

    @Override // org.thoughtcrime.securesms.emoji.EmojiData
    public List<EmojiPageModel> getDisplayPages() {
        return this.emojiData.getDisplayPages();
    }

    @Override // org.thoughtcrime.securesms.emoji.EmojiData
    public String getFormat() {
        return this.emojiData.getFormat();
    }

    @Override // org.thoughtcrime.securesms.emoji.EmojiData
    public Map<String, String> getJumboPages() {
        return this.emojiData.getJumboPages();
    }

    @Override // org.thoughtcrime.securesms.emoji.EmojiData
    public EmojiMetrics getMetrics() {
        return this.emojiData.getMetrics();
    }

    @Override // org.thoughtcrime.securesms.emoji.EmojiData
    public List<ObsoleteEmoji> getObsolete() {
        return this.emojiData.getObsolete();
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: kotlin.jvm.functions.Function1<? super android.net.Uri, ? extends org.thoughtcrime.securesms.emoji.EmojiPage> */
    /* JADX WARN: Multi-variable type inference failed */
    public EmojiSource(float f, EmojiData emojiData, Function1<? super Uri, ? extends EmojiPage> function1) {
        Intrinsics.checkNotNullParameter(emojiData, "emojiData");
        Intrinsics.checkNotNullParameter(function1, "emojiPageFactory");
        this.decodeScale = f;
        this.emojiData = emojiData;
        this.emojiPageFactory = function1;
    }

    public final float getDecodeScale() {
        return this.decodeScale;
    }

    public final Map<String, String> getVariationsToCanonical() {
        return (Map) this.variationsToCanonical$delegate.getValue();
    }

    public final Map<String, List<String>> getCanonicalToVariations() {
        return (Map) this.canonicalToVariations$delegate.getValue();
    }

    public final int getMaxEmojiLength() {
        return ((Number) this.maxEmojiLength$delegate.getValue()).intValue();
    }

    public final EmojiTree getEmojiTree() {
        return (EmojiTree) this.emojiTree$delegate.getValue();
    }

    /* compiled from: EmojiSource.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\f\u001a\u00020\u0007H\u0002J\b\u0010\r\u001a\u00020\u0007H\u0002J\n\u0010\u000e\u001a\u0004\u0018\u00010\u0007H\u0002J\b\u0010\u000f\u001a\u00020\u0010H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\b\u001a\u00020\u00078FX\u0004¢\u0006\f\u0012\u0004\b\t\u0010\u0002\u001a\u0004\b\n\u0010\u000b¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiSource$Companion;", "", "()V", "emojiLatch", "Ljava/util/concurrent/CountDownLatch;", "emojiSource", "Ljava/util/concurrent/atomic/AtomicReference;", "Lorg/thoughtcrime/securesms/emoji/EmojiSource;", "latest", "getLatest$annotations", "getLatest", "()Lorg/thoughtcrime/securesms/emoji/EmojiSource;", "getEmojiSource", "loadAssetBasedEmojis", "loadRemoteBasedEmojis", "refresh", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        @JvmStatic
        public static /* synthetic */ void getLatest$annotations() {
        }

        private Companion() {
        }

        public final EmojiSource getLatest() {
            EmojiSource.emojiLatch.await();
            Object obj = EmojiSource.emojiSource.get();
            Intrinsics.checkNotNullExpressionValue(obj, "emojiSource.get()");
            return (EmojiSource) obj;
        }

        @JvmStatic
        public final void refresh() {
            EmojiSource.emojiSource.set(getEmojiSource());
            EmojiSource.emojiLatch.countDown();
        }

        private final EmojiSource getEmojiSource() {
            EmojiSource loadRemoteBasedEmojis = loadRemoteBasedEmojis();
            return loadRemoteBasedEmojis == null ? loadAssetBasedEmojis() : loadRemoteBasedEmojis;
        }

        private final EmojiSource loadRemoteBasedEmojis() {
            ParsedEmojiData parsedEmojiData;
            if (SignalStore.internalValues().forceBuiltInEmoji()) {
                return null;
            }
            Application application = ApplicationDependencies.getApplication();
            Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
            EmojiFiles.Version readVersion$default = EmojiFiles.Version.Companion.readVersion$default(EmojiFiles.Version.Companion, application, false, 2, null);
            if (readVersion$default == null) {
                return null;
            }
            ParsedEmojiData latestEmojiData = EmojiFiles.getLatestEmojiData(application, readVersion$default);
            if (latestEmojiData != null) {
                parsedEmojiData = ParsedEmojiData.copy$default(latestEmojiData, null, null, null, CollectionsKt___CollectionsKt.plus((Collection<? extends EmojiPageModel>) ((Collection<? extends Object>) latestEmojiData.getDisplayPages()), EmojiSourceKt.PAGE_EMOTICONS), CollectionsKt___CollectionsKt.plus((Collection<? extends EmojiPageModel>) ((Collection<? extends Object>) latestEmojiData.getDataPages()), EmojiSourceKt.PAGE_EMOTICONS), null, null, 103, null);
            } else {
                parsedEmojiData = null;
            }
            float xhdpiRelativeDensityScaleFactor = ScreenDensity.xhdpiRelativeDensityScaleFactor(readVersion$default.getDensity());
            if (parsedEmojiData != null) {
                return new EmojiSource(xhdpiRelativeDensityScaleFactor, parsedEmojiData, EmojiSource$Companion$loadRemoteBasedEmojis$1$1.INSTANCE);
            }
            return null;
        }

        private final EmojiSource loadAssetBasedEmojis() {
            InputStream open = ApplicationDependencies.getApplication().getAssets().open("emoji/emoji_data.json");
            Intrinsics.checkNotNullExpressionValue(open, "getApplication().assets.…(\"emoji/emoji_data.json\")");
            try {
                Object r1 = EmojiJsonParser.INSTANCE.m1780parsegIAlus(open, EmojiSource$Companion$loadAssetBasedEmojis$1$parsedData$1.INSTANCE);
                ResultKt.throwOnFailure(r1);
                ParsedEmojiData parsedEmojiData = (ParsedEmojiData) r1;
                EmojiSource emojiSource = new EmojiSource(ScreenDensity.xhdpiRelativeDensityScaleFactor("xhdpi"), ParsedEmojiData.copy$default(parsedEmojiData, null, null, null, CollectionsKt___CollectionsKt.plus((Collection<? extends EmojiPageModel>) ((Collection<? extends Object>) parsedEmojiData.getDisplayPages()), EmojiSourceKt.PAGE_EMOTICONS), CollectionsKt___CollectionsKt.plus((Collection<? extends EmojiPageModel>) ((Collection<? extends Object>) parsedEmojiData.getDataPages()), EmojiSourceKt.PAGE_EMOTICONS), null, null, 103, null), EmojiSource$Companion$loadAssetBasedEmojis$1$1.INSTANCE);
                th = null;
                return emojiSource;
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        }
    }
}
