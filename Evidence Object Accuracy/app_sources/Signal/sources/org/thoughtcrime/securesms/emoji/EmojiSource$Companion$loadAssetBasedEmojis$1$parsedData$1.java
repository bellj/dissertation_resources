package org.thoughtcrime.securesms.emoji;

import android.net.Uri;
import kotlin.Metadata;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.FunctionReferenceImpl;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: EmojiSource.kt */
@Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public /* synthetic */ class EmojiSource$Companion$loadAssetBasedEmojis$1$parsedData$1 extends FunctionReferenceImpl implements Function2<String, String, Uri> {
    public static final EmojiSource$Companion$loadAssetBasedEmojis$1$parsedData$1 INSTANCE = new EmojiSource$Companion$loadAssetBasedEmojis$1$parsedData$1();

    EmojiSource$Companion$loadAssetBasedEmojis$1$parsedData$1() {
        super(2, EmojiSourceKt.class, "getAssetsUri", "getAssetsUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;", 1);
    }

    public final Uri invoke(String str, String str2) {
        Intrinsics.checkNotNullParameter(str, "p0");
        Intrinsics.checkNotNullParameter(str2, "p1");
        return EmojiSourceKt.getAssetsUri(str, str2);
    }
}
