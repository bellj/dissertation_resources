package org.thoughtcrime.securesms.emoji;

import android.net.Uri;
import kotlin.Metadata;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.FunctionReferenceImpl;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: EmojiFiles.kt */
@Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public /* synthetic */ class EmojiFiles$getLatestEmojiData$1$1 extends FunctionReferenceImpl implements Function2<String, String, Uri> {
    public static final EmojiFiles$getLatestEmojiData$1$1 INSTANCE = new EmojiFiles$getLatestEmojiData$1$1();

    EmojiFiles$getLatestEmojiData$1$1() {
        super(2, EmojiFilesKt.class, "getFilesUri", "getFilesUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;", 1);
    }

    public final Uri invoke(String str, String str2) {
        Intrinsics.checkNotNullParameter(str, "p0");
        Intrinsics.checkNotNullParameter(str2, "p1");
        return EmojiFilesKt.getFilesUri(str, str2);
    }
}
