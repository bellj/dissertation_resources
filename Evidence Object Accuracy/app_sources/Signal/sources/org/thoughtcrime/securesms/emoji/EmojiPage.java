package org.thoughtcrime.securesms.emoji;

import android.net.Uri;
import com.bumptech.glide.load.Key;
import java.security.MessageDigest;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsJVMKt;

/* compiled from: EmojiPage.kt */
@Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u000b\fB\u000f\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u0001\u0002\r\u000e¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiPage;", "Lcom/bumptech/glide/load/Key;", "uri", "Landroid/net/Uri;", "(Landroid/net/Uri;)V", "getUri", "()Landroid/net/Uri;", "updateDiskCacheKey", "", "messageDigest", "Ljava/security/MessageDigest;", "Asset", "Disk", "Lorg/thoughtcrime/securesms/emoji/EmojiPage$Asset;", "Lorg/thoughtcrime/securesms/emoji/EmojiPage$Disk;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class EmojiPage implements Key {
    private final Uri uri;

    public /* synthetic */ EmojiPage(Uri uri, DefaultConstructorMarker defaultConstructorMarker) {
        this(uri);
    }

    private EmojiPage(Uri uri) {
        this.uri = uri;
    }

    public Uri getUri() {
        return this.uri;
    }

    @Override // com.bumptech.glide.load.Key
    public void updateDiskCacheKey(MessageDigest messageDigest) {
        Intrinsics.checkNotNullParameter(messageDigest, "messageDigest");
        messageDigest.update(StringsKt__StringsJVMKt.encodeToByteArray("EmojiPage"));
        String uri = getUri().toString();
        Intrinsics.checkNotNullExpressionValue(uri, "uri.toString()");
        messageDigest.update(StringsKt__StringsJVMKt.encodeToByteArray(uri));
    }

    /* compiled from: EmojiPage.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiPage$Asset;", "Lorg/thoughtcrime/securesms/emoji/EmojiPage;", "uri", "Landroid/net/Uri;", "(Landroid/net/Uri;)V", "getUri", "()Landroid/net/Uri;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Asset extends EmojiPage {
        private final Uri uri;

        public static /* synthetic */ Asset copy$default(Asset asset, Uri uri, int i, Object obj) {
            if ((i & 1) != 0) {
                uri = asset.getUri();
            }
            return asset.copy(uri);
        }

        public final Uri component1() {
            return getUri();
        }

        public final Asset copy(Uri uri) {
            Intrinsics.checkNotNullParameter(uri, "uri");
            return new Asset(uri);
        }

        @Override // com.bumptech.glide.load.Key
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof Asset) && Intrinsics.areEqual(getUri(), ((Asset) obj).getUri());
        }

        @Override // com.bumptech.glide.load.Key
        public int hashCode() {
            return getUri().hashCode();
        }

        public String toString() {
            return "Asset(uri=" + getUri() + ')';
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Asset(Uri uri) {
            super(uri, null);
            Intrinsics.checkNotNullParameter(uri, "uri");
            this.uri = uri;
        }

        @Override // org.thoughtcrime.securesms.emoji.EmojiPage
        public Uri getUri() {
            return this.uri;
        }
    }

    /* compiled from: EmojiPage.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiPage$Disk;", "Lorg/thoughtcrime/securesms/emoji/EmojiPage;", "uri", "Landroid/net/Uri;", "(Landroid/net/Uri;)V", "getUri", "()Landroid/net/Uri;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Disk extends EmojiPage {
        private final Uri uri;

        public static /* synthetic */ Disk copy$default(Disk disk, Uri uri, int i, Object obj) {
            if ((i & 1) != 0) {
                uri = disk.getUri();
            }
            return disk.copy(uri);
        }

        public final Uri component1() {
            return getUri();
        }

        public final Disk copy(Uri uri) {
            Intrinsics.checkNotNullParameter(uri, "uri");
            return new Disk(uri);
        }

        @Override // com.bumptech.glide.load.Key
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof Disk) && Intrinsics.areEqual(getUri(), ((Disk) obj).getUri());
        }

        @Override // com.bumptech.glide.load.Key
        public int hashCode() {
            return getUri().hashCode();
        }

        public String toString() {
            return "Disk(uri=" + getUri() + ')';
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Disk(Uri uri) {
            super(uri, null);
            Intrinsics.checkNotNullParameter(uri, "uri");
            this.uri = uri;
        }

        @Override // org.thoughtcrime.securesms.emoji.EmojiPage
        public Uri getUri() {
            return this.uri;
        }
    }
}
