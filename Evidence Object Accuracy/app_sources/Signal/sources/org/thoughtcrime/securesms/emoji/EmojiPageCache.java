package org.thoughtcrime.securesms.emoji;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsJVMKt;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.emoji.EmojiPage;
import org.thoughtcrime.securesms.emoji.EmojiPageCache;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.util.ListenableFutureTask;
import org.thoughtcrime.securesms.util.SoftHashMap;

/* compiled from: EmojiPageCache.kt */
@Metadata(d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u001a\u001bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0006\u0010\u000e\u001a\u00020\u000fJ \u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0007J\u001a\u0010\u0018\u001a\u0004\u0018\u00010\t2\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0019\u001a\u00020\bH\u0003R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007X\u0004¢\u0006\u0002\n\u0000R6\u0010\n\u001a*\u0012\u0004\u0012\u00020\b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\f0\u000bj\u0014\u0012\u0004\u0012\u00020\b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\f`\rX\u0004¢\u0006\u0002\n\u0000¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiPageCache;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "cache", "Lorg/thoughtcrime/securesms/util/SoftHashMap;", "Lorg/thoughtcrime/securesms/emoji/EmojiPageCache$EmojiPageRequest;", "Landroid/graphics/Bitmap;", "tasks", "Ljava/util/HashMap;", "Lorg/thoughtcrime/securesms/util/ListenableFutureTask;", "Lkotlin/collections/HashMap;", "clear", "", "load", "Lorg/thoughtcrime/securesms/emoji/EmojiPageCache$LoadResult;", "context", "Landroid/content/Context;", "emojiPage", "Lorg/thoughtcrime/securesms/emoji/EmojiPage;", "inSampleSize", "", "loadInternal", "emojiPageRequest", "EmojiPageRequest", "LoadResult", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EmojiPageCache {
    public static final EmojiPageCache INSTANCE = new EmojiPageCache();
    private static final String TAG = Log.tag(EmojiPageCache.class);
    private static final SoftHashMap<EmojiPageRequest, Bitmap> cache = new SoftHashMap<>();
    private static final HashMap<EmojiPageRequest, ListenableFutureTask<Bitmap>> tasks = new HashMap<>();

    private EmojiPageCache() {
    }

    public final LoadResult load(Context context, EmojiPage emojiPage, int i) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(emojiPage, "emojiPage");
        Context applicationContext = context.getApplicationContext();
        EmojiPageRequest emojiPageRequest = new EmojiPageRequest(emojiPage, i);
        Bitmap bitmap = cache.get(emojiPageRequest);
        HashMap<EmojiPageRequest, ListenableFutureTask<Bitmap>> hashMap = tasks;
        ListenableFutureTask<Bitmap> listenableFutureTask = hashMap.get(emojiPageRequest);
        if (bitmap != null) {
            return new LoadResult.Immediate(bitmap);
        }
        if (listenableFutureTask != null) {
            return new LoadResult.Async(listenableFutureTask);
        }
        ListenableFutureTask<Bitmap> listenableFutureTask2 = new ListenableFutureTask<>(new Callable(applicationContext) { // from class: org.thoughtcrime.securesms.emoji.EmojiPageCache$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return EmojiPageCache.m1784load$lambda0(EmojiPageCache.EmojiPageRequest.this, this.f$1);
            }
        });
        hashMap.put(emojiPageRequest, listenableFutureTask2);
        SimpleTask.run(new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.emoji.EmojiPageCache$$ExternalSyntheticLambda1
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return EmojiPageCache.load$run__proxy(ListenableFutureTask.this);
            }
        }, new SimpleTask.ForegroundTask(emojiPageRequest) { // from class: org.thoughtcrime.securesms.emoji.EmojiPageCache$$ExternalSyntheticLambda2
            public final /* synthetic */ EmojiPageCache.EmojiPageRequest f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                EmojiPageCache.m1785load$lambda1(ListenableFutureTask.this, this.f$1, (Unit) obj);
            }
        });
        return new LoadResult.Async(listenableFutureTask2);
    }

    /* renamed from: load$lambda-0 */
    public static final Bitmap m1784load$lambda0(EmojiPageRequest emojiPageRequest, Context context) {
        Intrinsics.checkNotNullParameter(emojiPageRequest, "$emojiPageRequest");
        try {
            String str = TAG;
            Log.i(str, "Loading page " + emojiPageRequest);
            EmojiPageCache emojiPageCache = INSTANCE;
            Intrinsics.checkNotNullExpressionValue(context, "applicationContext");
            return emojiPageCache.loadInternal(context, emojiPageRequest);
        } catch (IOException e) {
            Log.w(TAG, e);
            return null;
        }
    }

    public static final /* synthetic */ Unit load$run__proxy(ListenableFutureTask listenableFutureTask) {
        listenableFutureTask.run();
        return Unit.INSTANCE;
    }

    /* renamed from: load$lambda-1 */
    public static final void m1785load$lambda1(ListenableFutureTask listenableFutureTask, EmojiPageRequest emojiPageRequest, Unit unit) {
        Intrinsics.checkNotNullParameter(listenableFutureTask, "$newTask");
        Intrinsics.checkNotNullParameter(emojiPageRequest, "$emojiPageRequest");
        try {
            Bitmap bitmap = (Bitmap) listenableFutureTask.get();
            if (bitmap == null) {
                String str = TAG;
                Log.w(str, "Failed to load emoji bitmap for request " + emojiPageRequest);
            } else {
                cache.put(emojiPageRequest, bitmap);
            }
        } finally {
            tasks.remove(emojiPageRequest);
        }
    }

    public final void clear() {
        cache.clear();
    }

    /* JADX WARN: Type inference failed for: r9v2, types: [java.lang.Throwable, android.graphics.Rect] */
    private final Bitmap loadInternal(Context context, EmojiPageRequest emojiPageRequest) {
        InputStream inputStream;
        EmojiPage emojiPage = emojiPageRequest.getEmojiPage();
        if (emojiPage instanceof EmojiPage.Asset) {
            AssetManager assets = context.getAssets();
            String uri = emojiPageRequest.getEmojiPage().getUri().toString();
            Intrinsics.checkNotNullExpressionValue(uri, "emojiPageRequest.emojiPage.uri.toString()");
            inputStream = assets.open(StringsKt__StringsJVMKt.replace$default(uri, "file:///android_asset/", "", false, 4, (Object) null));
            Intrinsics.checkNotNullExpressionValue(inputStream, "context.assets.open(emoj…:///android_asset/\", \"\"))");
        } else if (emojiPage instanceof EmojiPage.Disk) {
            String emojiFilename = PartAuthority.getEmojiFilename(emojiPageRequest.getEmojiPage().getUri());
            Intrinsics.checkNotNullExpressionValue(emojiFilename, "getEmojiFilename(emojiPageRequest.emojiPage.uri)");
            inputStream = EmojiFiles.openForReading(context, emojiFilename);
        } else {
            throw new NoWhenBranchMatchedException();
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = emojiPageRequest.getInSampleSize();
        th = 0;
        try {
            return BitmapFactory.decodeStream(inputStream, th, options);
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    /* compiled from: EmojiPageCache.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0005HÖ\u0001J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiPageCache$EmojiPageRequest;", "", "emojiPage", "Lorg/thoughtcrime/securesms/emoji/EmojiPage;", "inSampleSize", "", "(Lorg/thoughtcrime/securesms/emoji/EmojiPage;I)V", "getEmojiPage", "()Lorg/thoughtcrime/securesms/emoji/EmojiPage;", "getInSampleSize", "()I", "component1", "component2", "copy", "equals", "", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class EmojiPageRequest {
        private final EmojiPage emojiPage;
        private final int inSampleSize;

        public static /* synthetic */ EmojiPageRequest copy$default(EmojiPageRequest emojiPageRequest, EmojiPage emojiPage, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                emojiPage = emojiPageRequest.emojiPage;
            }
            if ((i2 & 2) != 0) {
                i = emojiPageRequest.inSampleSize;
            }
            return emojiPageRequest.copy(emojiPage, i);
        }

        public final EmojiPage component1() {
            return this.emojiPage;
        }

        public final int component2() {
            return this.inSampleSize;
        }

        public final EmojiPageRequest copy(EmojiPage emojiPage, int i) {
            Intrinsics.checkNotNullParameter(emojiPage, "emojiPage");
            return new EmojiPageRequest(emojiPage, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof EmojiPageRequest)) {
                return false;
            }
            EmojiPageRequest emojiPageRequest = (EmojiPageRequest) obj;
            return Intrinsics.areEqual(this.emojiPage, emojiPageRequest.emojiPage) && this.inSampleSize == emojiPageRequest.inSampleSize;
        }

        public int hashCode() {
            return (this.emojiPage.hashCode() * 31) + this.inSampleSize;
        }

        public String toString() {
            return "EmojiPageRequest(emojiPage=" + this.emojiPage + ", inSampleSize=" + this.inSampleSize + ')';
        }

        public EmojiPageRequest(EmojiPage emojiPage, int i) {
            Intrinsics.checkNotNullParameter(emojiPage, "emojiPage");
            this.emojiPage = emojiPage;
            this.inSampleSize = i;
        }

        public final EmojiPage getEmojiPage() {
            return this.emojiPage;
        }

        public final int getInSampleSize() {
            return this.inSampleSize;
        }
    }

    /* compiled from: EmojiPageCache.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0003\u0004B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0002\u0005\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiPageCache$LoadResult;", "", "()V", "Async", "Immediate", "Lorg/thoughtcrime/securesms/emoji/EmojiPageCache$LoadResult$Immediate;", "Lorg/thoughtcrime/securesms/emoji/EmojiPageCache$LoadResult$Async;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class LoadResult {
        public /* synthetic */ LoadResult(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* compiled from: EmojiPageCache.kt */
        @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiPageCache$LoadResult$Immediate;", "Lorg/thoughtcrime/securesms/emoji/EmojiPageCache$LoadResult;", "bitmap", "Landroid/graphics/Bitmap;", "(Landroid/graphics/Bitmap;)V", "getBitmap", "()Landroid/graphics/Bitmap;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Immediate extends LoadResult {
            private final Bitmap bitmap;

            public static /* synthetic */ Immediate copy$default(Immediate immediate, Bitmap bitmap, int i, Object obj) {
                if ((i & 1) != 0) {
                    bitmap = immediate.bitmap;
                }
                return immediate.copy(bitmap);
            }

            public final Bitmap component1() {
                return this.bitmap;
            }

            public final Immediate copy(Bitmap bitmap) {
                Intrinsics.checkNotNullParameter(bitmap, "bitmap");
                return new Immediate(bitmap);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof Immediate) && Intrinsics.areEqual(this.bitmap, ((Immediate) obj).bitmap);
            }

            public int hashCode() {
                return this.bitmap.hashCode();
            }

            public String toString() {
                return "Immediate(bitmap=" + this.bitmap + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Immediate(Bitmap bitmap) {
                super(null);
                Intrinsics.checkNotNullParameter(bitmap, "bitmap");
                this.bitmap = bitmap;
            }

            public final Bitmap getBitmap() {
                return this.bitmap;
            }
        }

        private LoadResult() {
        }

        /* compiled from: EmojiPageCache.kt */
        @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rHÖ\u0003J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiPageCache$LoadResult$Async;", "Lorg/thoughtcrime/securesms/emoji/EmojiPageCache$LoadResult;", "task", "Lorg/thoughtcrime/securesms/util/ListenableFutureTask;", "Landroid/graphics/Bitmap;", "(Lorg/thoughtcrime/securesms/util/ListenableFutureTask;)V", "getTask", "()Lorg/thoughtcrime/securesms/util/ListenableFutureTask;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Async extends LoadResult {
            private final ListenableFutureTask<Bitmap> task;

            /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.emoji.EmojiPageCache$LoadResult$Async */
            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Async copy$default(Async async, ListenableFutureTask listenableFutureTask, int i, Object obj) {
                if ((i & 1) != 0) {
                    listenableFutureTask = async.task;
                }
                return async.copy(listenableFutureTask);
            }

            public final ListenableFutureTask<Bitmap> component1() {
                return this.task;
            }

            public final Async copy(ListenableFutureTask<Bitmap> listenableFutureTask) {
                Intrinsics.checkNotNullParameter(listenableFutureTask, "task");
                return new Async(listenableFutureTask);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof Async) && Intrinsics.areEqual(this.task, ((Async) obj).task);
            }

            public int hashCode() {
                return this.task.hashCode();
            }

            public String toString() {
                return "Async(task=" + this.task + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Async(ListenableFutureTask<Bitmap> listenableFutureTask) {
                super(null);
                Intrinsics.checkNotNullParameter(listenableFutureTask, "task");
                this.task = listenableFutureTask;
            }

            public final ListenableFutureTask<Bitmap> getTask() {
                return this.task;
            }
        }
    }
}
