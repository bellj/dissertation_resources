package org.thoughtcrime.securesms.emoji;

import android.net.Uri;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.emoji.EmojiPageModel;
import org.thoughtcrime.securesms.components.emoji.StaticEmojiPageModel;

/* compiled from: EmojiSource.kt */
@Metadata(d1 = {"\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\u0010 \n\u0000\u001a\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0005H\u0002\u001a\u0012\u0010\u0007\u001a\u00020\b*\b\u0012\u0004\u0012\u00020\b0\tH\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0004¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"PAGE_EMOTICONS", "Lorg/thoughtcrime/securesms/components/emoji/EmojiPageModel;", "getAssetsUri", "Landroid/net/Uri;", "name", "", "format", "maxOrZero", "", "", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EmojiSourceKt {
    private static final EmojiPageModel PAGE_EMOTICONS = new StaticEmojiPageModel(EmojiCategory.EMOTICONS, new String[]{":-)", ";-)", "(-:", ":->", ":-D", "\\o/", ":-P", "B-)", ":-$", ":-*", "O:-)", "=-O", "O_O", "O_o", "o_O", ":O", ":-!", ":-x", ":-|", ":-\\", ":-(", ":'(", ":-[", ">:-(", "^.^", "^_^", "\\(ˆ˚ˆ)/", "ヽ(°◇° )ノ", "¯\\(°_o)/¯", "¯\\_(ツ)_/¯", "(¬_¬)", "(>_<)", "(╥﹏╥)", "(☞ﾟヮﾟ)☞", "☜(ﾟヮﾟ☜)", "☜(⌒▽⌒)☞", "(╯°□°)╯︵", "┻━┻", "┬─┬", "ノ(°–°ノ)", "(^._.^)ﾉ", "ฅ^•ﻌ•^ฅ", "ʕ•ᴥ•ʔ", "(•_•)", " ■-■¬ <(•_•) ", "(■_■¬)", "ƪ(ړײ)‎ƪ​​"}, (Uri) null);

    public static final int maxOrZero(List<Integer> list) {
        Integer num = (Integer) CollectionsKt.maxOrNull((Iterable) list);
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }

    public static final Uri getAssetsUri(String str, String str2) {
        Uri parse = Uri.parse("file:///android_asset/emoji/" + str + '.' + str2);
        Intrinsics.checkNotNullExpressionValue(parse, "parse(\"file:///android_asset/emoji/$name.$format\")");
        return parse;
    }
}
