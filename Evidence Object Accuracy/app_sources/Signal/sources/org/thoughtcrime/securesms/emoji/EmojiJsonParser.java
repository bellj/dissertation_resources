package org.thoughtcrime.securesms.emoji;

import android.net.Uri;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.TuplesKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.sequences.Sequence;
import kotlin.sequences.SequencesKt__SequencesKt;
import kotlin.sequences.SequencesKt___SequencesKt;
import org.thoughtcrime.securesms.components.emoji.CompositeEmojiPageModel;
import org.thoughtcrime.securesms.components.emoji.Emoji;
import org.thoughtcrime.securesms.components.emoji.EmojiPageModel;
import org.thoughtcrime.securesms.components.emoji.StaticEmojiPageModel;
import org.thoughtcrime.securesms.emoji.EmojiCategory;

/* compiled from: EmojiJsonParser.kt */
@Metadata(bv = {}, d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b&\u0010'J\u0010\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007J.\u0010\u000e\u001a\u00020\r2\u0006\u0010\u0007\u001a\u00020\u00062\u001c\u0010\f\u001a\u0018\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\bj\u0002`\u000bH\u0002J<\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112\u0006\u0010\u000f\u001a\u00020\t2\u0006\u0010\u0010\u001a\u00020\u00062\u001c\u0010\f\u001a\u0018\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\bj\u0002`\u000bH\u0002J\u001e\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t0\u00152\b\u0010\u0014\u001a\u0004\u0018\u00010\u0006H\u0002J>\u0010\u0019\u001a\u00020\u00122\u0006\u0010\u0017\u001a\u00020\t2\u0006\u0010\u000f\u001a\u00020\t2\u0006\u0010\u0018\u001a\u00020\u00062\u001c\u0010\f\u001a\u0018\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\bj\u0002`\u000bH\u0002J\u001c\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011H\u0002JB\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\r0\u001c2\u0006\u0010\u0003\u001a\u00020\u00022\u001c\u0010\f\u001a\u0018\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\bj\u0002`\u000bø\u0001\u0000ø\u0001\u0001ø\u0001\u0002¢\u0006\u0004\b\u001d\u0010\u001eR\u0014\u0010!\u001a\u00020 8\u0002X\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\u0014\u0010$\u001a\u00020#8\u0002XT¢\u0006\u0006\n\u0004\b$\u0010%\u0002\u000f\n\u0002\b\u0019\n\u0002\b!\n\u0005\b¡\u001e0\u0001¨\u0006("}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiJsonParser;", "", "Ljava/io/InputStream;", "body", "", "verify", "Lcom/fasterxml/jackson/databind/JsonNode;", "node", "Lkotlin/Function2;", "", "Landroid/net/Uri;", "Lorg/thoughtcrime/securesms/emoji/UriFactory;", "uriFactory", "Lorg/thoughtcrime/securesms/emoji/ParsedEmojiData;", "buildEmojiSourceFromNode", "format", "emoji", "", "Lorg/thoughtcrime/securesms/components/emoji/EmojiPageModel;", "getDataPages", "jumbo", "", "getJumboPages", "pageName", "page", "createPage", "dataPages", "mergeToDisplayPages", "Lkotlin/Result;", "parse-gIAlu-s", "(Ljava/io/InputStream;Lkotlin/jvm/functions/Function2;)Ljava/lang/Object;", "parse", "Lcom/fasterxml/jackson/databind/ObjectMapper;", "OBJECT_MAPPER", "Lcom/fasterxml/jackson/databind/ObjectMapper;", "", "ESTIMATED_EMOJI_COUNT", "I", "<init>", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class EmojiJsonParser {
    private static final int ESTIMATED_EMOJI_COUNT;
    public static final EmojiJsonParser INSTANCE = new EmojiJsonParser();
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private EmojiJsonParser() {
    }

    @JvmStatic
    public static final void verify(InputStream inputStream) {
        Intrinsics.checkNotNullParameter(inputStream, "body");
        ResultKt.throwOnFailure(INSTANCE.m1780parsegIAlus(inputStream, EmojiJsonParser$verify$1.INSTANCE));
    }

    /* renamed from: parse-gIAlu-s */
    public final Object m1780parsegIAlus(InputStream inputStream, Function2<? super String, ? super String, ? extends Uri> function2) {
        Intrinsics.checkNotNullParameter(inputStream, "body");
        Intrinsics.checkNotNullParameter(function2, "uriFactory");
        try {
            Result.Companion companion = Result.Companion;
            JsonNode readTree = OBJECT_MAPPER.readTree(inputStream);
            Intrinsics.checkNotNullExpressionValue(readTree, "OBJECT_MAPPER.readTree(body)");
            return Result.m153constructorimpl(buildEmojiSourceFromNode(readTree, function2));
        } catch (Exception e) {
            Result.Companion companion2 = Result.Companion;
            return Result.m153constructorimpl(ResultKt.createFailure(e));
        }
    }

    private final ParsedEmojiData buildEmojiSourceFromNode(JsonNode jsonNode, Function2<? super String, ? super String, ? extends Uri> function2) {
        String textValue = jsonNode.get("format").textValue();
        Intrinsics.checkNotNullExpressionValue(textValue, "node[\"format\"].textValue()");
        List list = EmojiJsonParserKt.toObseleteList(jsonNode.get("obsolete"));
        JsonNode jsonNode2 = jsonNode.get("emoji");
        Intrinsics.checkNotNullExpressionValue(jsonNode2, "node[\"emoji\"]");
        List<EmojiPageModel> dataPages = getDataPages(textValue, jsonNode2, function2);
        Map<String, String> jumboPages = getJumboPages(jsonNode.get("jumbomoji"));
        List<EmojiPageModel> mergeToDisplayPages = mergeToDisplayPages(dataPages);
        JsonNode jsonNode3 = jsonNode.get("metrics");
        Intrinsics.checkNotNullExpressionValue(jsonNode3, "node[\"metrics\"]");
        EmojiMetrics emojiMetrics = EmojiJsonParserKt.toEmojiMetrics(jsonNode3);
        JsonNode jsonNode4 = jsonNode.get("densities");
        Intrinsics.checkNotNullExpressionValue(jsonNode4, "node[\"densities\"]");
        return new ParsedEmojiData(emojiMetrics, EmojiJsonParserKt.toDensityList(jsonNode4), textValue, mergeToDisplayPages, dataPages, jumboPages, list);
    }

    private final List<EmojiPageModel> getDataPages(String str, JsonNode jsonNode, Function2<? super String, ? super String, ? extends Uri> function2) {
        Iterator<Map.Entry<String, JsonNode>> fields = jsonNode.fields();
        Intrinsics.checkNotNullExpressionValue(fields, "emoji.fields()");
        return SequencesKt___SequencesKt.toList(SequencesKt___SequencesKt.map(SequencesKt___SequencesKt.sortedWith(SequencesKt__SequencesKt.asSequence(fields), new Comparator() { // from class: org.thoughtcrime.securesms.emoji.EmojiJsonParser$$ExternalSyntheticLambda0
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return EmojiJsonParser.m1779getDataPages$lambda0((Map.Entry) obj, (Map.Entry) obj2);
            }
        }), new Function1<Map.Entry<String, JsonNode>, EmojiPageModel>(str, function2) { // from class: org.thoughtcrime.securesms.emoji.EmojiJsonParser$getDataPages$2
            final /* synthetic */ String $format;
            final /* synthetic */ Function2<String, String, Uri> $uriFactory;

            /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: kotlin.jvm.functions.Function2<? super java.lang.String, ? super java.lang.String, ? extends android.net.Uri> */
            /* JADX WARN: Multi-variable type inference failed */
            /* access modifiers changed from: package-private */
            {
                this.$format = r1;
                this.$uriFactory = r2;
            }

            public final EmojiPageModel invoke(Map.Entry<String, JsonNode> entry) {
                EmojiJsonParser emojiJsonParser = EmojiJsonParser.INSTANCE;
                String key = entry.getKey();
                Intrinsics.checkNotNullExpressionValue(key, "it.key");
                String str2 = this.$format;
                JsonNode value = entry.getValue();
                Intrinsics.checkNotNullExpressionValue(value, "it.value");
                return emojiJsonParser.createPage(key, str2, value, this.$uriFactory);
            }
        }));
    }

    /* renamed from: getDataPages$lambda-0 */
    public static final int m1779getDataPages$lambda0(Map.Entry entry, Map.Entry entry2) {
        EmojiCategory.Companion companion = EmojiCategory.Companion;
        Object key = entry.getKey();
        Intrinsics.checkNotNullExpressionValue(key, "lhs.key");
        EmojiCategory forKey = companion.forKey(EmojiJsonParserKt.asCategoryKey((String) key));
        Object key2 = entry2.getKey();
        Intrinsics.checkNotNullExpressionValue(key2, "rhs.key");
        int compare = Intrinsics.compare(forKey.getPriority(), companion.forKey(EmojiJsonParserKt.asCategoryKey((String) key2)).getPriority());
        if (compare != 0) {
            return compare;
        }
        Object key3 = entry.getKey();
        Intrinsics.checkNotNullExpressionValue(key3, "lhs.key");
        int i = EmojiJsonParserKt.getPageIndex((String) key3);
        Object key4 = entry2.getKey();
        Intrinsics.checkNotNullExpressionValue(key4, "rhs.key");
        return Intrinsics.compare(i, EmojiJsonParserKt.getPageIndex((String) key4));
    }

    private final Map<String, String> getJumboPages(JsonNode jsonNode) {
        if (jsonNode == null) {
            return MapsKt__MapsKt.emptyMap();
        }
        Iterator<Map.Entry<String, JsonNode>> fields = jsonNode.fields();
        Intrinsics.checkNotNullExpressionValue(fields, "jumbo.fields()");
        Sequence<Map.Entry> sequence = SequencesKt___SequencesKt.flatMapIterable(SequencesKt___SequencesKt.map(SequencesKt__SequencesKt.asSequence(fields), EmojiJsonParser$getJumboPages$1.INSTANCE), EmojiJsonParser$getJumboPages$2.INSTANCE);
        HashMap hashMap = new HashMap((int) ESTIMATED_EMOJI_COUNT);
        for (Map.Entry entry : sequence) {
            Pair pair = TuplesKt.to(entry.getKey(), entry.getValue());
            hashMap.put(pair.getFirst(), pair.getSecond());
        }
        return hashMap;
    }

    public final EmojiPageModel createPage(String str, String str2, JsonNode jsonNode, Function2<? super String, ? super String, ? extends Uri> function2) {
        EmojiCategory forKey = EmojiCategory.Companion.forKey(EmojiJsonParserKt.asCategoryKey(str));
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(jsonNode, 10));
        int i = 0;
        for (JsonNode jsonNode2 : jsonNode) {
            int i2 = i + 1;
            if (i < 0) {
                CollectionsKt__CollectionsKt.throwIndexOverflow();
            }
            JsonNode jsonNode3 = jsonNode2;
            if (jsonNode3.size() != 0) {
                ArrayList arrayList2 = new ArrayList();
                ArrayList arrayList3 = new ArrayList();
                Intrinsics.checkNotNullExpressionValue(jsonNode3, "data");
                for (JsonNode jsonNode4 : jsonNode3) {
                    String textValue = jsonNode4.textValue();
                    Intrinsics.checkNotNullExpressionValue(textValue, "it.textValue()");
                    arrayList2.add(EmojiJsonParserKt.encodeAsUtf16(textValue));
                    arrayList3.add(jsonNode4.textValue());
                }
                arrayList.add(new Emoji(arrayList2, arrayList3));
                i = i2;
            } else {
                throw new IllegalStateException("Page index " + str + '.' + i + " had no data");
            }
        }
        return new StaticEmojiPageModel(forKey, arrayList, (Uri) function2.invoke(str, str2));
    }

    private final List<EmojiPageModel> mergeToDisplayPages(List<? extends EmojiPageModel> list) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object obj : list) {
            Integer valueOf = Integer.valueOf(((EmojiPageModel) obj).getIconAttr());
            Object obj2 = linkedHashMap.get(valueOf);
            if (obj2 == null) {
                obj2 = new ArrayList();
                linkedHashMap.put(valueOf, obj2);
            }
            ((List) obj2).add(obj);
        }
        ArrayList arrayList = new ArrayList(linkedHashMap.size());
        for (Map.Entry entry : linkedHashMap.entrySet()) {
            int intValue = ((Number) entry.getKey()).intValue();
            List list2 = (List) entry.getValue();
            arrayList.add(list2.size() <= 1 ? (EmojiPageModel) CollectionsKt___CollectionsKt.first((List) ((List<? extends Object>) list2)) : new CompositeEmojiPageModel(intValue, list2));
        }
        return arrayList;
    }
}
