package org.thoughtcrime.securesms.emoji;

import com.fasterxml.jackson.databind.JsonNode;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.MatchResult;
import kotlin.text.Regex;
import org.signal.core.util.Hex;

/* compiled from: EmojiJsonParser.kt */
@Metadata(bv = {}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001*\u0004\u0018\u00010\u0000H\u0002\u001a\f\u0010\u0005\u001a\u00020\u0004*\u00020\u0000H\u0002\u001a\u0012\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0001*\u00020\u0000H\u0002\u001a\f\u0010\b\u001a\u00020\u0006*\u00020\u0006H\u0002\u001a\f\u0010\t\u001a\u00020\u0006*\u00020\u0006H\u0002\u001a\f\u0010\u000b\u001a\u00020\n*\u00020\u0006H\u0002*.\u0010\u000e\"\u0014\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\r0\f2\u0014\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\r0\f¨\u0006\u000f"}, d2 = {"Lcom/fasterxml/jackson/databind/JsonNode;", "", "Lorg/thoughtcrime/securesms/emoji/ObsoleteEmoji;", "toObseleteList", "Lorg/thoughtcrime/securesms/emoji/EmojiMetrics;", "toEmojiMetrics", "", "toDensityList", "encodeAsUtf16", "asCategoryKey", "", "getPageIndex", "Lkotlin/Function2;", "Landroid/net/Uri;", "UriFactory", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class EmojiJsonParserKt {
    public static final List<ObsoleteEmoji> toObseleteList(JsonNode jsonNode) {
        if (jsonNode == null) {
            return CollectionsKt__CollectionsKt.emptyList();
        }
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(jsonNode, 10));
        for (JsonNode jsonNode2 : jsonNode) {
            String textValue = jsonNode2.get("obsoleted").textValue();
            Intrinsics.checkNotNullExpressionValue(textValue, "node[\"obsoleted\"].textValue()");
            String encodeAsUtf16 = encodeAsUtf16(textValue);
            String textValue2 = jsonNode2.get("replace_with").textValue();
            Intrinsics.checkNotNullExpressionValue(textValue2, "node[\"replace_with\"].textValue()");
            arrayList.add(new ObsoleteEmoji(encodeAsUtf16, encodeAsUtf16(textValue2)));
        }
        return CollectionsKt___CollectionsKt.toList(arrayList);
    }

    public static final EmojiMetrics toEmojiMetrics(JsonNode jsonNode) {
        return new EmojiMetrics(jsonNode.get("raw_width").asInt(), jsonNode.get("raw_height").asInt(), jsonNode.get("per_row").asInt());
    }

    public static final String encodeAsUtf16(String str) {
        byte[] fromStringCondensed = Hex.fromStringCondensed(str);
        Intrinsics.checkNotNullExpressionValue(fromStringCondensed, "fromStringCondensed(this)");
        Charset forName = Charset.forName("UTF-16");
        Intrinsics.checkNotNullExpressionValue(forName, "forName(\"UTF-16\")");
        return new String(fromStringCondensed, forName);
    }

    public static final String asCategoryKey(String str) {
        return new Regex("(_\\d+)*$").replace(str, "");
    }

    public static final int getPageIndex(String str) {
        String str2;
        MatchResult find$default = Regex.find$default(new Regex("^.*_(\\d+)+$"), str, 0, 2, null);
        if (find$default != null && (str2 = find$default.getGroupValues().get(1)) != null) {
            return Integer.parseInt(str2);
        }
        throw new IllegalStateException("No index.");
    }

    public static final List<String> toDensityList(JsonNode jsonNode) {
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(jsonNode, 10));
        Iterator<JsonNode> it = jsonNode.iterator();
        while (it.hasNext()) {
            arrayList.add(it.next().textValue());
        }
        return arrayList;
    }
}
