package org.thoughtcrime.securesms.emoji;

import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import org.thoughtcrime.securesms.components.emoji.EmojiPageModel;

/* compiled from: EmojiSource.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010$\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001R\u0018\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0018\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\u0006R\u0018\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u000b\u0010\u0006R\u0012\u0010\f\u001a\u00020\bX¦\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u001e\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\b0\u0010X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0012R\u0012\u0010\u0013\u001a\u00020\u0014X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0015\u0010\u0016R\u0018\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00180\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u0006¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiData;", "", "dataPages", "", "Lorg/thoughtcrime/securesms/components/emoji/EmojiPageModel;", "getDataPages", "()Ljava/util/List;", "densities", "", "getDensities", "displayPages", "getDisplayPages", "format", "getFormat", "()Ljava/lang/String;", "jumboPages", "", "getJumboPages", "()Ljava/util/Map;", "metrics", "Lorg/thoughtcrime/securesms/emoji/EmojiMetrics;", "getMetrics", "()Lorg/thoughtcrime/securesms/emoji/EmojiMetrics;", "obsolete", "Lorg/thoughtcrime/securesms/emoji/ObsoleteEmoji;", "getObsolete", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public interface EmojiData {
    List<EmojiPageModel> getDataPages();

    List<String> getDensities();

    List<EmojiPageModel> getDisplayPages();

    String getFormat();

    Map<String, String> getJumboPages();

    EmojiMetrics getMetrics();

    List<ObsoleteEmoji> getObsolete();
}
