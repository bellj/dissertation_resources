package org.thoughtcrime.securesms.emoji;

import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.emoji.EmojiPageModel;

/* compiled from: EmojiJsonParser.kt */
@Metadata(d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001Ba\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u0005\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\u0005\u0012\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\f\u0012\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0005¢\u0006\u0002\u0010\u000fJ\t\u0010\u001b\u001a\u00020\u0003HÆ\u0003J\u000f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J\t\u0010\u001d\u001a\u00020\u0006HÆ\u0003J\u000f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\t0\u0005HÆ\u0003J\u000f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\t0\u0005HÆ\u0003J\u0015\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\fHÆ\u0003J\u000f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0005HÆ\u0003Js\u0010\"\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u00062\u000e\b\u0002\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u00052\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\u00052\u0014\b\u0002\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\f2\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0005HÆ\u0001J\u0013\u0010#\u001a\u00020$2\b\u0010%\u001a\u0004\u0018\u00010&HÖ\u0003J\t\u0010'\u001a\u00020(HÖ\u0001J\t\u0010)\u001a\u00020\u0006HÖ\u0001R\u001a\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u001a\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0011R\u001a\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0011R\u0014\u0010\u0007\u001a\u00020\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R \u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\fX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u001a\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0011¨\u0006*"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/ParsedEmojiData;", "Lorg/thoughtcrime/securesms/emoji/EmojiData;", "metrics", "Lorg/thoughtcrime/securesms/emoji/EmojiMetrics;", "densities", "", "", "format", "displayPages", "Lorg/thoughtcrime/securesms/components/emoji/EmojiPageModel;", "dataPages", "jumboPages", "", "obsolete", "Lorg/thoughtcrime/securesms/emoji/ObsoleteEmoji;", "(Lorg/thoughtcrime/securesms/emoji/EmojiMetrics;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/Map;Ljava/util/List;)V", "getDataPages", "()Ljava/util/List;", "getDensities", "getDisplayPages", "getFormat", "()Ljava/lang/String;", "getJumboPages", "()Ljava/util/Map;", "getMetrics", "()Lorg/thoughtcrime/securesms/emoji/EmojiMetrics;", "getObsolete", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "equals", "", "other", "", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ParsedEmojiData implements EmojiData {
    private final List<EmojiPageModel> dataPages;
    private final List<String> densities;
    private final List<EmojiPageModel> displayPages;
    private final String format;
    private final Map<String, String> jumboPages;
    private final EmojiMetrics metrics;
    private final List<ObsoleteEmoji> obsolete;

    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: org.thoughtcrime.securesms.emoji.ParsedEmojiData */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ ParsedEmojiData copy$default(ParsedEmojiData parsedEmojiData, EmojiMetrics emojiMetrics, List list, String str, List list2, List list3, Map map, List list4, int i, Object obj) {
        if ((i & 1) != 0) {
            emojiMetrics = parsedEmojiData.getMetrics();
        }
        if ((i & 2) != 0) {
            list = parsedEmojiData.getDensities();
        }
        if ((i & 4) != 0) {
            str = parsedEmojiData.getFormat();
        }
        if ((i & 8) != 0) {
            list2 = parsedEmojiData.getDisplayPages();
        }
        if ((i & 16) != 0) {
            list3 = parsedEmojiData.getDataPages();
        }
        if ((i & 32) != 0) {
            map = parsedEmojiData.getJumboPages();
        }
        if ((i & 64) != 0) {
            list4 = parsedEmojiData.getObsolete();
        }
        return parsedEmojiData.copy(emojiMetrics, list, str, list2, list3, map, list4);
    }

    public final EmojiMetrics component1() {
        return getMetrics();
    }

    public final List<String> component2() {
        return getDensities();
    }

    public final String component3() {
        return getFormat();
    }

    public final List<EmojiPageModel> component4() {
        return getDisplayPages();
    }

    public final List<EmojiPageModel> component5() {
        return getDataPages();
    }

    public final Map<String, String> component6() {
        return getJumboPages();
    }

    public final List<ObsoleteEmoji> component7() {
        return getObsolete();
    }

    public final ParsedEmojiData copy(EmojiMetrics emojiMetrics, List<String> list, String str, List<? extends EmojiPageModel> list2, List<? extends EmojiPageModel> list3, Map<String, String> map, List<ObsoleteEmoji> list4) {
        Intrinsics.checkNotNullParameter(emojiMetrics, "metrics");
        Intrinsics.checkNotNullParameter(list, "densities");
        Intrinsics.checkNotNullParameter(str, "format");
        Intrinsics.checkNotNullParameter(list2, "displayPages");
        Intrinsics.checkNotNullParameter(list3, "dataPages");
        Intrinsics.checkNotNullParameter(map, "jumboPages");
        Intrinsics.checkNotNullParameter(list4, "obsolete");
        return new ParsedEmojiData(emojiMetrics, list, str, list2, list3, map, list4);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ParsedEmojiData)) {
            return false;
        }
        ParsedEmojiData parsedEmojiData = (ParsedEmojiData) obj;
        return Intrinsics.areEqual(getMetrics(), parsedEmojiData.getMetrics()) && Intrinsics.areEqual(getDensities(), parsedEmojiData.getDensities()) && Intrinsics.areEqual(getFormat(), parsedEmojiData.getFormat()) && Intrinsics.areEqual(getDisplayPages(), parsedEmojiData.getDisplayPages()) && Intrinsics.areEqual(getDataPages(), parsedEmojiData.getDataPages()) && Intrinsics.areEqual(getJumboPages(), parsedEmojiData.getJumboPages()) && Intrinsics.areEqual(getObsolete(), parsedEmojiData.getObsolete());
    }

    public int hashCode() {
        return (((((((((((getMetrics().hashCode() * 31) + getDensities().hashCode()) * 31) + getFormat().hashCode()) * 31) + getDisplayPages().hashCode()) * 31) + getDataPages().hashCode()) * 31) + getJumboPages().hashCode()) * 31) + getObsolete().hashCode();
    }

    public String toString() {
        return "ParsedEmojiData(metrics=" + getMetrics() + ", densities=" + getDensities() + ", format=" + getFormat() + ", displayPages=" + getDisplayPages() + ", dataPages=" + getDataPages() + ", jumboPages=" + getJumboPages() + ", obsolete=" + getObsolete() + ')';
    }

    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.components.emoji.EmojiPageModel> */
    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.components.emoji.EmojiPageModel> */
    /* JADX WARN: Multi-variable type inference failed */
    public ParsedEmojiData(EmojiMetrics emojiMetrics, List<String> list, String str, List<? extends EmojiPageModel> list2, List<? extends EmojiPageModel> list3, Map<String, String> map, List<ObsoleteEmoji> list4) {
        Intrinsics.checkNotNullParameter(emojiMetrics, "metrics");
        Intrinsics.checkNotNullParameter(list, "densities");
        Intrinsics.checkNotNullParameter(str, "format");
        Intrinsics.checkNotNullParameter(list2, "displayPages");
        Intrinsics.checkNotNullParameter(list3, "dataPages");
        Intrinsics.checkNotNullParameter(map, "jumboPages");
        Intrinsics.checkNotNullParameter(list4, "obsolete");
        this.metrics = emojiMetrics;
        this.densities = list;
        this.format = str;
        this.displayPages = list2;
        this.dataPages = list3;
        this.jumboPages = map;
        this.obsolete = list4;
    }

    @Override // org.thoughtcrime.securesms.emoji.EmojiData
    public EmojiMetrics getMetrics() {
        return this.metrics;
    }

    @Override // org.thoughtcrime.securesms.emoji.EmojiData
    public List<String> getDensities() {
        return this.densities;
    }

    @Override // org.thoughtcrime.securesms.emoji.EmojiData
    public String getFormat() {
        return this.format;
    }

    @Override // org.thoughtcrime.securesms.emoji.EmojiData
    public List<EmojiPageModel> getDisplayPages() {
        return this.displayPages;
    }

    @Override // org.thoughtcrime.securesms.emoji.EmojiData
    public List<EmojiPageModel> getDataPages() {
        return this.dataPages;
    }

    @Override // org.thoughtcrime.securesms.emoji.EmojiData
    public Map<String, String> getJumboPages() {
        return this.jumboPages;
    }

    @Override // org.thoughtcrime.securesms.emoji.EmojiData
    public List<ObsoleteEmoji> getObsolete() {
        return this.obsolete;
    }
}
