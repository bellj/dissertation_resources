package org.thoughtcrime.securesms.emoji;

import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

/* compiled from: EmojiJsonParser.kt */
@Metadata(d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\"\n\u0002\u0010&\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010$\n\u0000\u0010\u0000\u001a$\u0012 \u0012\u001e\u0012\f\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003\u0012\f\u0012\n \u0004*\u0004\u0018\u00010\u00030\u00030\u00020\u00012\"\u0010\u0005\u001a\u001e\u0012\f\u0012\n \u0004*\u0004\u0018\u00010\u00030\u0003\u0012\f\u0012\n \u0004*\u0004\u0018\u00010\u00030\u00030\u0006H\n¢\u0006\u0002\b\u0007"}, d2 = {"<anonymous>", "", "", "", "kotlin.jvm.PlatformType", "it", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EmojiJsonParser$getJumboPages$2 extends Lambda implements Function1<Map<String, ? extends String>, Set<? extends Map.Entry<? extends String, ? extends String>>> {
    public static final EmojiJsonParser$getJumboPages$2 INSTANCE = new EmojiJsonParser$getJumboPages$2();

    EmojiJsonParser$getJumboPages$2() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Set<? extends Map.Entry<? extends String, ? extends String>> invoke(Map<String, ? extends String> map) {
        return invoke((Map<String, String>) map);
    }

    public final Set<Map.Entry<String, String>> invoke(Map<String, String> map) {
        Intrinsics.checkNotNullParameter(map, "it");
        return map.entrySet();
    }
}
