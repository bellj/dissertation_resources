package org.thoughtcrime.securesms.emoji;

import java.io.IOException;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.Response;
import org.thoughtcrime.securesms.s3.S3;

/* compiled from: EmojiRemote.kt */
@Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u0007\u001a\u00020\bH\u0007J\b\u0010\u000b\u001a\u00020\fH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/emoji/EmojiRemote;", "", "()V", "VERSION_URI", "", "getMd5", "", "emojiRequest", "Lorg/thoughtcrime/securesms/emoji/EmojiRequest;", "getObject", "Lokhttp3/Response;", "getVersion", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EmojiRemote {
    public static final EmojiRemote INSTANCE = new EmojiRemote();
    private static final String VERSION_URI;

    private EmojiRemote() {
    }

    @JvmStatic
    public static final int getVersion() throws IOException {
        return (int) S3.INSTANCE.getLong(VERSION_URI);
    }

    @JvmStatic
    public static final byte[] getMd5(EmojiRequest emojiRequest) {
        Intrinsics.checkNotNullParameter(emojiRequest, "emojiRequest");
        return S3.INSTANCE.getObjectMD5(emojiRequest.getUri());
    }

    @JvmStatic
    public static final Response getObject(EmojiRequest emojiRequest) {
        Intrinsics.checkNotNullParameter(emojiRequest, "emojiRequest");
        return S3.getObject(emojiRequest.getUri());
    }
}
