package org.thoughtcrime.securesms.emoji;

import android.content.Context;
import android.net.Uri;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.crypto.AttachmentSecretProvider;
import org.thoughtcrime.securesms.crypto.ModernDecryptingPartInputStream;
import org.thoughtcrime.securesms.crypto.ModernEncryptingPartOutputStream;
import org.thoughtcrime.securesms.mms.PartAuthority;

/* compiled from: EmojiFiles.kt */
@Metadata(d1 = {"\u00004\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u001a\u0018\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u00012\u0006\u0010\n\u001a\u00020\u0001H\u0002\u001a\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002\u001a\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u0010H\u0002\u001a\f\u0010\u0014\u001a\u00020\u0010*\u00020\u000eH\u0002\u001a\u0014\u0010\u0015\u001a\u00020\u0010*\u00020\u000e2\u0006\u0010\u0016\u001a\u00020\u0017H\u0002\u001a\u0014\u0010\u0018\u001a\u00020\u0010*\u00020\u000e2\u0006\u0010\u0016\u001a\u00020\u0017H\u0002\u001a\f\u0010\u0019\u001a\u00020\u0010*\u00020\u000eH\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000¨\u0006\u001a"}, d2 = {"EMOJI_DIRECTORY", "", "EMOJI_JSON", "JUMBO_FILE", "NAME_FILE", "TAG", "VERSION_FILE", "getFilesUri", "Landroid/net/Uri;", "name", "format", "getInputStream", "Ljava/io/InputStream;", "context", "Landroid/content/Context;", "inputFile", "Ljava/io/File;", "getOutputStream", "Ljava/io/OutputStream;", "outputFile", "getEmojiDirectory", "getJumboFile", "versionUuid", "Ljava/util/UUID;", "getNameFile", "getVersionFile", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EmojiFilesKt {
    private static final String EMOJI_DIRECTORY;
    private static final String EMOJI_JSON;
    private static final String JUMBO_FILE;
    private static final String NAME_FILE;
    private static final String TAG;
    private static final String VERSION_FILE;

    public static final File getEmojiDirectory(Context context) {
        File dir = context.getDir("emoji", 0);
        Intrinsics.checkNotNullExpressionValue(dir, "getDir(EMOJI_DIRECTORY, Context.MODE_PRIVATE)");
        return dir;
    }

    public static final File getVersionFile(Context context) {
        return new File(getEmojiDirectory(context), VERSION_FILE);
    }

    public static final File getNameFile(Context context, UUID uuid) {
        File file = new File(getEmojiDirectory(context), uuid.toString());
        file.mkdir();
        return new File(file, NAME_FILE);
    }

    public static final File getJumboFile(Context context, UUID uuid) {
        File file = new File(getEmojiDirectory(context), uuid.toString());
        file.mkdir();
        return new File(file, JUMBO_FILE);
    }

    public static final Uri getFilesUri(String str, String str2) {
        Uri emojiUri = PartAuthority.getEmojiUri(str);
        Intrinsics.checkNotNullExpressionValue(emojiUri, "getEmojiUri(name)");
        return emojiUri;
    }

    public static final OutputStream getOutputStream(Context context, File file) {
        Object obj = ModernEncryptingPartOutputStream.createFor(AttachmentSecretProvider.getInstance(context).getOrCreateAttachmentSecret(), file, true).second;
        Intrinsics.checkNotNullExpressionValue(obj, "createFor(attachmentSecr… outputFile, true).second");
        return (OutputStream) obj;
    }

    public static final InputStream getInputStream(Context context, File file) {
        InputStream createFor = ModernDecryptingPartInputStream.createFor(AttachmentSecretProvider.getInstance(context).getOrCreateAttachmentSecret(), file, 0);
        Intrinsics.checkNotNullExpressionValue(createFor, "createFor(attachmentSecret, inputFile, 0)");
        return createFor;
    }
}
