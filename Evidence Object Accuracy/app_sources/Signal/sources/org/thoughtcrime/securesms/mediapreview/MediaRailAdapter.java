package org.thoughtcrime.securesms.mediapreview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.ThumbnailView;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.util.adapter.StableIdGenerator;

/* loaded from: classes4.dex */
public class MediaRailAdapter extends RecyclerView.Adapter<MediaRailViewHolder> {
    private static final int TYPE_BUTTON;
    private static final int TYPE_MEDIA;
    private int activePosition;
    private RailItemAddListener addListener;
    private boolean editable;
    private final GlideRequests glideRequests;
    private boolean interactive;
    private final RailItemListener listener;
    private final List<Media> media = new ArrayList();
    private final StableIdGenerator<Media> stableIdGenerator;

    /* loaded from: classes4.dex */
    public interface RailItemAddListener {
        void onRailItemAddClicked();
    }

    /* loaded from: classes4.dex */
    public interface RailItemListener {
        void onRailItemClicked(int i);

        void onRailItemDeleteClicked(int i);
    }

    public MediaRailAdapter(GlideRequests glideRequests, RailItemListener railItemListener, boolean z) {
        this.glideRequests = glideRequests;
        this.listener = railItemListener;
        this.editable = z;
        this.stableIdGenerator = new StableIdGenerator<>();
        this.interactive = true;
        setHasStableIds(true);
    }

    public MediaRailViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == 1) {
            return new MediaViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.mediarail_media_item, viewGroup, false));
        }
        if (i == 2) {
            return new ButtonViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.mediarail_button_item, viewGroup, false));
        }
        throw new UnsupportedOperationException("Unsupported view type: " + i);
    }

    public void onBindViewHolder(MediaRailViewHolder mediaRailViewHolder, int i) {
        int itemViewType = getItemViewType(i);
        if (itemViewType == 1) {
            MediaViewHolder mediaViewHolder = (MediaViewHolder) mediaRailViewHolder;
            Media media = this.media.get(i);
            int i2 = this.activePosition;
            mediaViewHolder.bind(media, i == i2, this.glideRequests, this.listener, i - i2, this.editable, this.interactive);
        } else if (itemViewType == 2) {
            ((ButtonViewHolder) mediaRailViewHolder).bind(this.addListener);
        } else {
            throw new UnsupportedOperationException("Unsupported view type: " + getItemViewType(i));
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        return (!this.editable || i != getItemCount() - 1) ? 1 : 2;
    }

    public void onViewRecycled(MediaRailViewHolder mediaRailViewHolder) {
        mediaRailViewHolder.recycle();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.editable ? this.media.size() + 1 : this.media.size();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public long getItemId(int i) {
        int itemViewType = getItemViewType(i);
        if (itemViewType == 1) {
            return this.stableIdGenerator.getId(this.media.get(i));
        }
        if (itemViewType == 2) {
            return Long.MAX_VALUE;
        }
        throw new UnsupportedOperationException("Unsupported view type: " + getItemViewType(i));
    }

    public void setMedia(List<Media> list) {
        setMedia(list, this.activePosition);
    }

    public void setMedia(List<Media> list, int i) {
        this.activePosition = i;
        this.media.clear();
        this.media.addAll(list);
        notifyDataSetChanged();
    }

    public void setActivePosition(int i) {
        this.activePosition = i;
        notifyDataSetChanged();
    }

    public void setAddButtonListener(RailItemAddListener railItemAddListener) {
        this.addListener = railItemAddListener;
        notifyDataSetChanged();
    }

    public void setEditable(boolean z) {
        this.editable = z;
        notifyDataSetChanged();
    }

    public void setInteractive(boolean z) {
        this.interactive = z;
        notifyDataSetChanged();
    }

    /* loaded from: classes4.dex */
    public static abstract class MediaRailViewHolder extends RecyclerView.ViewHolder {
        abstract void recycle();

        public MediaRailViewHolder(View view) {
            super(view);
        }
    }

    /* loaded from: classes4.dex */
    public static class MediaViewHolder extends MediaRailViewHolder {
        private final View captionIndicator;
        private final View deleteButton;
        private final ThumbnailView image;
        private final View outline;

        MediaViewHolder(View view) {
            super(view);
            this.image = (ThumbnailView) view.findViewById(R.id.rail_item_image);
            this.outline = view.findViewById(R.id.rail_item_outline);
            this.deleteButton = view.findViewById(R.id.rail_item_delete);
            this.captionIndicator = view.findViewById(R.id.rail_item_caption);
        }

        void bind(Media media, boolean z, GlideRequests glideRequests, RailItemListener railItemListener, int i, boolean z2, boolean z3) {
            this.image.setImageResource(glideRequests, media.getUri());
            this.image.setOnClickListener(new MediaRailAdapter$MediaViewHolder$$ExternalSyntheticLambda0(railItemListener, i));
            this.outline.setVisibility((!z || !z3) ? 8 : 0);
            this.captionIndicator.setVisibility(media.getCaption().isPresent() ? 0 : 8);
            if (!z2 || !z || !z3) {
                this.deleteButton.setVisibility(8);
                return;
            }
            this.deleteButton.setVisibility(0);
            this.deleteButton.setOnClickListener(new MediaRailAdapter$MediaViewHolder$$ExternalSyntheticLambda1(railItemListener, i));
        }

        void recycle() {
            this.image.setOnClickListener(null);
            this.deleteButton.setOnClickListener(null);
        }
    }

    /* loaded from: classes4.dex */
    public static class ButtonViewHolder extends MediaRailViewHolder {
        public ButtonViewHolder(View view) {
            super(view);
        }

        void bind(RailItemAddListener railItemAddListener) {
            if (railItemAddListener != null) {
                this.itemView.setOnClickListener(new MediaRailAdapter$ButtonViewHolder$$ExternalSyntheticLambda0(railItemAddListener));
            }
        }

        void recycle() {
            this.itemView.setOnClickListener(null);
        }
    }
}
