package org.thoughtcrime.securesms.mediapreview;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.ZoomingImageView;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.util.MediaUtil;

/* loaded from: classes4.dex */
public final class ImageMediaPreviewFragment extends MediaPreviewFragment {
    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ZoomingImageView zoomingImageView = (ZoomingImageView) layoutInflater.inflate(R.layout.media_preview_image_fragment, viewGroup, false);
        GlideRequests with = GlideApp.with(requireActivity());
        Bundle requireArguments = requireArguments();
        Uri uri = (Uri) requireArguments.getParcelable(MediaPreviewFragment.DATA_URI);
        String string = requireArguments.getString("DATA_CONTENT_TYPE");
        if (MediaUtil.isImageType(string)) {
            zoomingImageView.setImageUri(with, uri, string, new Runnable() { // from class: org.thoughtcrime.securesms.mediapreview.ImageMediaPreviewFragment$$ExternalSyntheticLambda0
                @Override // java.lang.Runnable
                public final void run() {
                    ImageMediaPreviewFragment.$r8$lambda$7ibugBEEGPWwkR8vneZ8QzEx0XI(ImageMediaPreviewFragment.this);
                }
            });
            zoomingImageView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediapreview.ImageMediaPreviewFragment$$ExternalSyntheticLambda1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ImageMediaPreviewFragment.$r8$lambda$sK7fjPDttOKQvdrxlmSV2cGiMMg(ImageMediaPreviewFragment.this, view);
                }
            });
            return zoomingImageView;
        }
        throw new AssertionError("This fragment can only display images");
    }

    public /* synthetic */ void lambda$onCreateView$0() {
        this.events.onMediaReady();
    }

    public /* synthetic */ void lambda$onCreateView$1(View view) {
        this.events.singleTapOnMedia();
    }
}
