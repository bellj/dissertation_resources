package org.thoughtcrime.securesms.mediapreview;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import j$.util.Optional;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.thoughtcrime.securesms.database.MediaDatabase;
import org.thoughtcrime.securesms.mediasend.Media;

/* loaded from: classes4.dex */
public class MediaPreviewViewModel extends ViewModel {
    private Cursor cursor;
    private boolean leftIsRecent;
    private final MutableLiveData<PreviewData> previewData = new MutableLiveData<>();

    public void setCursor(Context context, Cursor cursor, boolean z) {
        boolean z2 = this.cursor == null && cursor != null;
        this.cursor = cursor;
        this.leftIsRecent = z;
        if (z2) {
            setActiveAlbumRailItem(context, 0);
        }
    }

    public void setActiveAlbumRailItem(Context context, int i) {
        if (this.cursor == null) {
            this.previewData.postValue(new PreviewData(Collections.emptyList(), null, 0));
            return;
        }
        int cursorPosition = getCursorPosition(i);
        this.cursor.moveToPosition(cursorPosition);
        MediaDatabase.MediaRecord from = MediaDatabase.MediaRecord.from(context, this.cursor);
        LinkedList linkedList = new LinkedList();
        Media media = toMedia(from);
        if (media != null) {
            linkedList.add(media);
        }
        while (this.cursor.moveToPrevious()) {
            MediaDatabase.MediaRecord from2 = MediaDatabase.MediaRecord.from(context, this.cursor);
            if (from2.getAttachment().getMmsId() != from.getAttachment().getMmsId()) {
                break;
            }
            Media media2 = toMedia(from2);
            if (media2 != null) {
                linkedList.addFirst(media2);
            }
        }
        this.cursor.moveToPosition(cursorPosition);
        while (this.cursor.moveToNext()) {
            MediaDatabase.MediaRecord from3 = MediaDatabase.MediaRecord.from(context, this.cursor);
            if (from3.getAttachment().getMmsId() != from.getAttachment().getMmsId()) {
                break;
            }
            Media media3 = toMedia(from3);
            if (media3 != null) {
                linkedList.addLast(media3);
            }
        }
        if (!this.leftIsRecent) {
            Collections.reverse(linkedList);
        }
        this.previewData.postValue(new PreviewData(linkedList.size() > 1 ? linkedList : Collections.emptyList(), from.getAttachment().getCaption(), linkedList.indexOf(media)));
    }

    public void resubmitPreviewData() {
        MutableLiveData<PreviewData> mutableLiveData = this.previewData;
        mutableLiveData.postValue(mutableLiveData.getValue());
    }

    private int getCursorPosition(int i) {
        Cursor cursor = this.cursor;
        if (cursor == null) {
            return 0;
        }
        if (this.leftIsRecent) {
            return i;
        }
        return (cursor.getCount() - 1) - i;
    }

    private Media toMedia(MediaDatabase.MediaRecord mediaRecord) {
        Uri uri = mediaRecord.getAttachment().getUri();
        if (uri == null) {
            return null;
        }
        return new Media(uri, mediaRecord.getContentType(), mediaRecord.getDate(), mediaRecord.getAttachment().getWidth(), mediaRecord.getAttachment().getHeight(), mediaRecord.getAttachment().getSize(), 0, mediaRecord.getAttachment().isBorderless(), mediaRecord.getAttachment().isVideoGif(), Optional.empty(), Optional.ofNullable(mediaRecord.getAttachment().getCaption()), Optional.empty());
    }

    public LiveData<PreviewData> getPreviewData() {
        return this.previewData;
    }

    /* loaded from: classes4.dex */
    public static class PreviewData {
        private final int activePosition;
        private final List<Media> albumThumbnails;
        private final String caption;

        public PreviewData(List<Media> list, String str, int i) {
            this.albumThumbnails = list;
            this.caption = str;
            this.activePosition = i;
        }

        public List<Media> getAlbumThumbnails() {
            return this.albumThumbnails;
        }

        public String getCaption() {
            return this.caption;
        }

        public int getActivePosition() {
            return this.activePosition;
        }
    }
}
