package org.thoughtcrime.securesms.mediapreview;

import android.view.View;
import org.thoughtcrime.securesms.mediapreview.MediaRailAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MediaRailAdapter$ButtonViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ MediaRailAdapter.RailItemAddListener f$0;

    public /* synthetic */ MediaRailAdapter$ButtonViewHolder$$ExternalSyntheticLambda0(MediaRailAdapter.RailItemAddListener railItemAddListener) {
        this.f$0 = railItemAddListener;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.onRailItemAddClicked();
    }
}
