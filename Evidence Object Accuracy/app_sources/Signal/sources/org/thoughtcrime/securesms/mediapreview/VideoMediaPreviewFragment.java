package org.thoughtcrime.securesms.mediapreview;

import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.voice.VoiceNoteMediaControllerOwner;
import org.thoughtcrime.securesms.mms.VideoSlide;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.video.VideoPlayer;

/* loaded from: classes4.dex */
public final class VideoMediaPreviewFragment extends MediaPreviewFragment {
    private static final String TAG = Log.tag(VideoMediaPreviewFragment.class);
    private boolean isVideoGif;
    private VideoPlayer videoView;

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.media_preview_video_fragment, viewGroup, false);
        Bundle requireArguments = requireArguments();
        Uri uri = (Uri) requireArguments.getParcelable(MediaPreviewFragment.DATA_URI);
        String string = requireArguments.getString("DATA_CONTENT_TYPE");
        long j = requireArguments.getLong("DATA_SIZE");
        boolean z = requireArguments.getBoolean("AUTO_PLAY");
        this.isVideoGif = requireArguments.getBoolean("VIDEO_GIF");
        if (MediaUtil.isVideo(string)) {
            VideoPlayer videoPlayer = (VideoPlayer) inflate.findViewById(R.id.video_player);
            this.videoView = videoPlayer;
            videoPlayer.setWindow(requireActivity().getWindow());
            this.videoView.setVideoSource(new VideoSlide(getContext(), uri, j, false), z, TAG);
            this.videoView.setPlayerPositionDiscontinuityCallback(new VideoPlayer.PlayerPositionDiscontinuityCallback() { // from class: org.thoughtcrime.securesms.mediapreview.VideoMediaPreviewFragment$$ExternalSyntheticLambda0
                @Override // org.thoughtcrime.securesms.video.VideoPlayer.PlayerPositionDiscontinuityCallback
                public final void onPositionDiscontinuity(VideoPlayer videoPlayer2, int i) {
                    VideoMediaPreviewFragment.this.lambda$onCreateView$0(videoPlayer2, i);
                }
            });
            this.videoView.setPlayerCallback(new VideoPlayer.PlayerCallback() { // from class: org.thoughtcrime.securesms.mediapreview.VideoMediaPreviewFragment.1
                @Override // org.thoughtcrime.securesms.video.VideoPlayer.PlayerCallback
                public void onStopped() {
                }

                @Override // org.thoughtcrime.securesms.video.VideoPlayer.PlayerCallback
                public void onReady() {
                    VideoMediaPreviewFragment.this.events.onMediaReady();
                }

                @Override // org.thoughtcrime.securesms.video.VideoPlayer.PlayerCallback
                public void onPlaying() {
                    FragmentActivity activity = VideoMediaPreviewFragment.this.getActivity();
                    if (!VideoMediaPreviewFragment.this.isVideoGif && (activity instanceof VoiceNoteMediaControllerOwner)) {
                        ((VoiceNoteMediaControllerOwner) activity).getVoiceNoteMediaController().pausePlayback();
                    }
                }

                @Override // org.thoughtcrime.securesms.video.VideoPlayer.PlayerCallback
                public void onError() {
                    VideoMediaPreviewFragment.this.events.mediaNotAvailable();
                }
            });
            if (this.isVideoGif) {
                this.videoView.hideControls();
                this.videoView.loopForever();
            }
            this.videoView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediapreview.VideoMediaPreviewFragment$$ExternalSyntheticLambda1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    VideoMediaPreviewFragment.this.lambda$onCreateView$1(view);
                }
            });
            return inflate;
        }
        throw new AssertionError("This fragment can only display video");
    }

    public /* synthetic */ void lambda$onCreateView$0(VideoPlayer videoPlayer, int i) {
        if (this.events.getVideoControlsDelegate() != null) {
            this.events.getVideoControlsDelegate().onPlayerPositionDiscontinuity(i);
        }
    }

    public /* synthetic */ void lambda$onCreateView$1(View view) {
        this.events.singleTapOnMedia();
    }

    @Override // org.thoughtcrime.securesms.mediapreview.MediaPreviewFragment
    public void cleanUp() {
        VideoPlayer videoPlayer = this.videoView;
        if (videoPlayer != null) {
            videoPlayer.cleanup();
        }
    }

    @Override // org.thoughtcrime.securesms.mediapreview.MediaPreviewFragment, androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        VideoPlayer videoPlayer = this.videoView;
        if (videoPlayer != null && this.isVideoGif) {
            videoPlayer.play();
        }
        if (this.events.getVideoControlsDelegate() != null) {
            this.events.getVideoControlsDelegate().attachPlayer(getUri(), this.videoView, this.isVideoGif);
        }
    }

    @Override // org.thoughtcrime.securesms.mediapreview.MediaPreviewFragment
    public void pause() {
        VideoPlayer videoPlayer = this.videoView;
        if (videoPlayer != null) {
            videoPlayer.pause();
        }
        if (this.events.getVideoControlsDelegate() != null) {
            this.events.getVideoControlsDelegate().detachPlayer();
        }
    }

    @Override // org.thoughtcrime.securesms.mediapreview.MediaPreviewFragment
    public View getPlaybackControls() {
        VideoPlayer videoPlayer = this.videoView;
        if (videoPlayer == null || this.isVideoGif) {
            return null;
        }
        return videoPlayer.getControlView();
    }

    private Uri getUri() {
        return (Uri) requireArguments().getParcelable(MediaPreviewFragment.DATA_URI);
    }
}
