package org.thoughtcrime.securesms.mediapreview;

import android.net.Uri;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.video.VideoPlayer;

/* compiled from: VideoControlsDelegate.kt */
@Metadata(d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\b\u0018\u00002\u00020\u0001:\u0002\u001d\u001eB\u0005¢\u0006\u0002\u0010\u0002J \u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u00072\b\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u0004J\u0006\u0010\u0010\u001a\u00020\u000bJ\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u00122\u0006\u0010\f\u001a\u00020\u0007J\u0006\u0010\u0013\u001a\u00020\u0004J\u0006\u0010\u0014\u001a\u00020\u000bJ\u000e\u0010\u0015\u001a\u00020\u000b2\u0006\u0010\u0016\u001a\u00020\u0017J\r\u0010\u0018\u001a\u0004\u0018\u00010\u000b¢\u0006\u0002\u0010\u0019J\u0006\u0010\u001a\u001a\u00020\u000bJ\u000e\u0010\u001b\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u0007J\u0006\u0010\u001c\u001a\u00020\u000bR\u000e\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00040\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u000e¢\u0006\u0002\n\u0000¨\u0006\u001f"}, d2 = {"Lorg/thoughtcrime/securesms/mediapreview/VideoControlsDelegate;", "", "()V", "isMuted", "", "playWhenReady", "", "Landroid/net/Uri;", "player", "Lorg/thoughtcrime/securesms/mediapreview/VideoControlsDelegate$Player;", "attachPlayer", "", "uri", "videoPlayer", "Lorg/thoughtcrime/securesms/video/VideoPlayer;", "isGif", "detachPlayer", "getPlayerState", "Lorg/thoughtcrime/securesms/mediapreview/VideoControlsDelegate$PlayerState;", "hasAudioStream", "mute", "onPlayerPositionDiscontinuity", "reason", "", "pause", "()Lkotlin/Unit;", "restart", "resume", "unmute", "Player", "PlayerState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class VideoControlsDelegate {
    private boolean isMuted = true;
    private final Map<Uri, Boolean> playWhenReady = new LinkedHashMap();
    private Player player;

    public final PlayerState getPlayerState(Uri uri) {
        Intrinsics.checkNotNullParameter(uri, "uri");
        Player player = this.player;
        if (!Intrinsics.areEqual(player != null ? player.getUri() : null, uri) || player.getVideoPlayer() == null) {
            return null;
        }
        return new PlayerState(uri, player.getVideoPlayer().getPlaybackPosition(), player.getVideoPlayer().getDuration(), player.isGif(), player.getLoopCount());
    }

    public final Unit pause() {
        VideoPlayer videoPlayer;
        Player player = this.player;
        if (player == null || (videoPlayer = player.getVideoPlayer()) == null) {
            return null;
        }
        videoPlayer.pause();
        return Unit.INSTANCE;
    }

    public final void resume(Uri uri) {
        VideoPlayer videoPlayer;
        Intrinsics.checkNotNullParameter(uri, "uri");
        Player player = this.player;
        if (Intrinsics.areEqual(player != null ? player.getUri() : null, uri)) {
            Player player2 = this.player;
            if (player2 != null && (videoPlayer = player2.getVideoPlayer()) != null) {
                videoPlayer.play();
                return;
            }
            return;
        }
        this.playWhenReady.put(uri, Boolean.TRUE);
    }

    public final void restart() {
        Player player = this.player;
        VideoPlayer videoPlayer = player != null ? player.getVideoPlayer() : null;
        if (videoPlayer != null) {
            videoPlayer.setPlaybackPosition(0);
        }
    }

    public final void onPlayerPositionDiscontinuity(int i) {
        Player player = this.player;
        if (player != null && player.isGif()) {
            this.player = Player.copy$default(player, null, null, false, i == 0 ? player.getLoopCount() + 1 : 0, 7, null);
        }
    }

    public final void mute() {
        VideoPlayer videoPlayer;
        this.isMuted = true;
        Player player = this.player;
        if (player != null && (videoPlayer = player.getVideoPlayer()) != null) {
            videoPlayer.mute();
        }
    }

    public final void unmute() {
        VideoPlayer videoPlayer;
        this.isMuted = false;
        Player player = this.player;
        if (player != null && (videoPlayer = player.getVideoPlayer()) != null) {
            videoPlayer.unmute();
        }
    }

    public final boolean hasAudioStream() {
        VideoPlayer videoPlayer;
        Player player = this.player;
        if (player == null || (videoPlayer = player.getVideoPlayer()) == null) {
            return false;
        }
        return videoPlayer.hasAudioTrack();
    }

    public final void attachPlayer(Uri uri, VideoPlayer videoPlayer, boolean z) {
        Intrinsics.checkNotNullParameter(uri, "uri");
        this.player = new Player(uri, videoPlayer, z, 0, 8, null);
        if (this.isMuted) {
            if (videoPlayer != null) {
                videoPlayer.mute();
            }
        } else if (videoPlayer != null) {
            videoPlayer.unmute();
        }
        if (Intrinsics.areEqual(this.playWhenReady.get(uri), Boolean.TRUE)) {
            this.playWhenReady.put(uri, Boolean.FALSE);
            if (videoPlayer != null) {
                videoPlayer.play();
            }
        }
    }

    public final void detachPlayer() {
        this.player = new Player(null, null, false, 0, 15, null);
    }

    /* compiled from: VideoControlsDelegate.kt */
    /* access modifiers changed from: private */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0011\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B/\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u0013\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0007HÆ\u0003J\t\u0010\u0015\u001a\u00020\tHÆ\u0003J3\u0010\u0016\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\tHÆ\u0001J\u0013\u0010\u0017\u001a\u00020\u00072\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0019\u001a\u00020\tHÖ\u0001J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u000bR\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/mediapreview/VideoControlsDelegate$Player;", "", "uri", "Landroid/net/Uri;", "videoPlayer", "Lorg/thoughtcrime/securesms/video/VideoPlayer;", "isGif", "", "loopCount", "", "(Landroid/net/Uri;Lorg/thoughtcrime/securesms/video/VideoPlayer;ZI)V", "()Z", "getLoopCount", "()I", "getUri", "()Landroid/net/Uri;", "getVideoPlayer", "()Lorg/thoughtcrime/securesms/video/VideoPlayer;", "component1", "component2", "component3", "component4", "copy", "equals", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Player {
        private final boolean isGif;
        private final int loopCount;
        private final Uri uri;
        private final VideoPlayer videoPlayer;

        public Player() {
            this(null, null, false, 0, 15, null);
        }

        public static /* synthetic */ Player copy$default(Player player, Uri uri, VideoPlayer videoPlayer, boolean z, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                uri = player.uri;
            }
            if ((i2 & 2) != 0) {
                videoPlayer = player.videoPlayer;
            }
            if ((i2 & 4) != 0) {
                z = player.isGif;
            }
            if ((i2 & 8) != 0) {
                i = player.loopCount;
            }
            return player.copy(uri, videoPlayer, z, i);
        }

        public final Uri component1() {
            return this.uri;
        }

        public final VideoPlayer component2() {
            return this.videoPlayer;
        }

        public final boolean component3() {
            return this.isGif;
        }

        public final int component4() {
            return this.loopCount;
        }

        public final Player copy(Uri uri, VideoPlayer videoPlayer, boolean z, int i) {
            Intrinsics.checkNotNullParameter(uri, "uri");
            return new Player(uri, videoPlayer, z, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Player)) {
                return false;
            }
            Player player = (Player) obj;
            return Intrinsics.areEqual(this.uri, player.uri) && Intrinsics.areEqual(this.videoPlayer, player.videoPlayer) && this.isGif == player.isGif && this.loopCount == player.loopCount;
        }

        public int hashCode() {
            int hashCode = this.uri.hashCode() * 31;
            VideoPlayer videoPlayer = this.videoPlayer;
            int hashCode2 = (hashCode + (videoPlayer == null ? 0 : videoPlayer.hashCode())) * 31;
            boolean z = this.isGif;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return ((hashCode2 + i) * 31) + this.loopCount;
        }

        public String toString() {
            return "Player(uri=" + this.uri + ", videoPlayer=" + this.videoPlayer + ", isGif=" + this.isGif + ", loopCount=" + this.loopCount + ')';
        }

        public Player(Uri uri, VideoPlayer videoPlayer, boolean z, int i) {
            Intrinsics.checkNotNullParameter(uri, "uri");
            this.uri = uri;
            this.videoPlayer = videoPlayer;
            this.isGif = z;
            this.loopCount = i;
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public /* synthetic */ Player(android.net.Uri r2, org.thoughtcrime.securesms.video.VideoPlayer r3, boolean r4, int r5, int r6, kotlin.jvm.internal.DefaultConstructorMarker r7) {
            /*
                r1 = this;
                r7 = r6 & 1
                if (r7 == 0) goto L_0x000b
                android.net.Uri r2 = android.net.Uri.EMPTY
                java.lang.String r7 = "EMPTY"
                kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r2, r7)
            L_0x000b:
                r7 = r6 & 2
                if (r7 == 0) goto L_0x0010
                r3 = 0
            L_0x0010:
                r7 = r6 & 4
                r0 = 0
                if (r7 == 0) goto L_0x0016
                r4 = 0
            L_0x0016:
                r6 = r6 & 8
                if (r6 == 0) goto L_0x001b
                r5 = 0
            L_0x001b:
                r1.<init>(r2, r3, r4, r5)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.mediapreview.VideoControlsDelegate.Player.<init>(android.net.Uri, org.thoughtcrime.securesms.video.VideoPlayer, boolean, int, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }

        public final Uri getUri() {
            return this.uri;
        }

        public final VideoPlayer getVideoPlayer() {
            return this.videoPlayer;
        }

        public final boolean isGif() {
            return this.isGif;
        }

        public final int getLoopCount() {
            return this.loopCount;
        }
    }

    /* compiled from: VideoControlsDelegate.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0013\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n¢\u0006\u0002\u0010\u000bJ\t\u0010\u0014\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0016\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0017\u001a\u00020\bHÆ\u0003J\t\u0010\u0018\u001a\u00020\nHÆ\u0003J;\u0010\u0019\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\nHÆ\u0001J\u0013\u0010\u001a\u001a\u00020\b2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001c\u001a\u00020\nHÖ\u0001J\t\u0010\u001d\u001a\u00020\u001eHÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\u000eR\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\r¨\u0006\u001f"}, d2 = {"Lorg/thoughtcrime/securesms/mediapreview/VideoControlsDelegate$PlayerState;", "", "mediaUri", "Landroid/net/Uri;", "position", "", "duration", "isGif", "", "loopCount", "", "(Landroid/net/Uri;JJZI)V", "getDuration", "()J", "()Z", "getLoopCount", "()I", "getMediaUri", "()Landroid/net/Uri;", "getPosition", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class PlayerState {
        private final long duration;
        private final boolean isGif;
        private final int loopCount;
        private final Uri mediaUri;
        private final long position;

        public static /* synthetic */ PlayerState copy$default(PlayerState playerState, Uri uri, long j, long j2, boolean z, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                uri = playerState.mediaUri;
            }
            if ((i2 & 2) != 0) {
                j = playerState.position;
            }
            if ((i2 & 4) != 0) {
                j2 = playerState.duration;
            }
            if ((i2 & 8) != 0) {
                z = playerState.isGif;
            }
            if ((i2 & 16) != 0) {
                i = playerState.loopCount;
            }
            return playerState.copy(uri, j, j2, z, i);
        }

        public final Uri component1() {
            return this.mediaUri;
        }

        public final long component2() {
            return this.position;
        }

        public final long component3() {
            return this.duration;
        }

        public final boolean component4() {
            return this.isGif;
        }

        public final int component5() {
            return this.loopCount;
        }

        public final PlayerState copy(Uri uri, long j, long j2, boolean z, int i) {
            Intrinsics.checkNotNullParameter(uri, "mediaUri");
            return new PlayerState(uri, j, j2, z, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof PlayerState)) {
                return false;
            }
            PlayerState playerState = (PlayerState) obj;
            return Intrinsics.areEqual(this.mediaUri, playerState.mediaUri) && this.position == playerState.position && this.duration == playerState.duration && this.isGif == playerState.isGif && this.loopCount == playerState.loopCount;
        }

        public int hashCode() {
            int hashCode = ((((this.mediaUri.hashCode() * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.position)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.duration)) * 31;
            boolean z = this.isGif;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return ((hashCode + i) * 31) + this.loopCount;
        }

        public String toString() {
            return "PlayerState(mediaUri=" + this.mediaUri + ", position=" + this.position + ", duration=" + this.duration + ", isGif=" + this.isGif + ", loopCount=" + this.loopCount + ')';
        }

        public PlayerState(Uri uri, long j, long j2, boolean z, int i) {
            Intrinsics.checkNotNullParameter(uri, "mediaUri");
            this.mediaUri = uri;
            this.position = j;
            this.duration = j2;
            this.isGif = z;
            this.loopCount = i;
        }

        public final Uri getMediaUri() {
            return this.mediaUri;
        }

        public final long getPosition() {
            return this.position;
        }

        public final long getDuration() {
            return this.duration;
        }

        public final boolean isGif() {
            return this.isGif;
        }

        public final int getLoopCount() {
            return this.loopCount;
        }
    }
}
