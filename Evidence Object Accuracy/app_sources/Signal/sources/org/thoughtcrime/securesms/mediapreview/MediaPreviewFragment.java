package org.thoughtcrime.securesms.mediapreview;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import java.util.Objects;
import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.AttachmentId;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.mms.PartUriParser;
import org.thoughtcrime.securesms.util.MediaUtil;

/* loaded from: classes4.dex */
public abstract class MediaPreviewFragment extends Fragment {
    static final String AUTO_PLAY;
    static final String DATA_CONTENT_TYPE;
    static final String DATA_SIZE;
    public static final String DATA_URI;
    static final String VIDEO_GIF;
    private AttachmentId attachmentId;
    protected Events events;

    /* loaded from: classes4.dex */
    public interface Events {

        /* renamed from: org.thoughtcrime.securesms.mediapreview.MediaPreviewFragment$Events$-CC */
        /* loaded from: classes4.dex */
        public final /* synthetic */ class CC {
            public static VideoControlsDelegate $default$getVideoControlsDelegate(Events events) {
                return null;
            }
        }

        VideoControlsDelegate getVideoControlsDelegate();

        void mediaNotAvailable();

        void onMediaReady();

        boolean singleTapOnMedia();
    }

    public void cleanUp() {
    }

    public View getPlaybackControls() {
        return null;
    }

    public void pause() {
    }

    public static MediaPreviewFragment newInstance(Attachment attachment, boolean z) {
        return newInstance(attachment.getUri(), attachment.getContentType(), attachment.getSize(), z, attachment.isVideoGif());
    }

    public static MediaPreviewFragment newInstance(Uri uri, String str, long j, boolean z, boolean z2) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(DATA_URI, uri);
        bundle.putString(DATA_CONTENT_TYPE, str);
        bundle.putLong(DATA_SIZE, j);
        bundle.putBoolean(AUTO_PLAY, z);
        bundle.putBoolean(VIDEO_GIF, z2);
        MediaPreviewFragment createCorrectFragmentType = createCorrectFragmentType(str);
        createCorrectFragmentType.setArguments(bundle);
        return createCorrectFragmentType;
    }

    private static MediaPreviewFragment createCorrectFragmentType(String str) {
        if (MediaUtil.isVideo(str)) {
            return new VideoMediaPreviewFragment();
        }
        if (MediaUtil.isImageType(str)) {
            return new ImageMediaPreviewFragment();
        }
        throw new AssertionError("Unexpected media type: " + str);
    }

    @Override // androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Events) {
            this.events = (Events) context;
        } else if (getParentFragment() instanceof Events) {
            this.events = (Events) getParentFragment();
        } else {
            throw new AssertionError("Parent component must support " + Events.class);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        checkMediaStillAvailable();
    }

    private void checkMediaStillAvailable() {
        if (this.attachmentId == null) {
            Uri uri = (Uri) requireArguments().getParcelable(DATA_URI);
            Objects.requireNonNull(uri);
            this.attachmentId = new PartUriParser(uri).getPartId();
        }
        SimpleTask.run(getViewLifecycleOwner().getLifecycle(), new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.mediapreview.MediaPreviewFragment$$ExternalSyntheticLambda0
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return MediaPreviewFragment.this.lambda$checkMediaStillAvailable$0();
            }
        }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.mediapreview.MediaPreviewFragment$$ExternalSyntheticLambda1
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                MediaPreviewFragment.this.lambda$checkMediaStillAvailable$1((Boolean) obj);
            }
        });
    }

    public /* synthetic */ Boolean lambda$checkMediaStillAvailable$0() {
        return Boolean.valueOf(SignalDatabase.attachments().hasAttachment(this.attachmentId));
    }

    public /* synthetic */ void lambda$checkMediaStillAvailable$1(Boolean bool) {
        if (!bool.booleanValue()) {
            this.events.mediaNotAvailable();
        }
    }
}
