package org.thoughtcrime.securesms.mediapreview;

import android.view.View;
import org.thoughtcrime.securesms.mediapreview.MediaRailAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MediaRailAdapter$MediaViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ MediaRailAdapter.RailItemListener f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ MediaRailAdapter$MediaViewHolder$$ExternalSyntheticLambda0(MediaRailAdapter.RailItemListener railItemListener, int i) {
        this.f$0 = railItemListener;
        this.f$1 = i;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.onRailItemClicked(this.f$1);
    }
}
