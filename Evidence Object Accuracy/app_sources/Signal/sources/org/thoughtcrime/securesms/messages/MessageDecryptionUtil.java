package org.thoughtcrime.securesms.messages;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import j$.util.Optional;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.metadata.ProtocolException;
import org.signal.libsignal.protocol.message.DecryptionErrorMessage;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.BadGroupIdException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobs.SendRetryReceiptJob;
import org.thoughtcrime.securesms.logsubmit.SubmitDebugLogActivity;
import org.thoughtcrime.securesms.messages.MessageContentProcessor;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.notifications.NotificationIds;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.GroupUtil;
import org.whispersystems.signalservice.api.crypto.ContentHint;
import org.whispersystems.signalservice.api.messages.SignalServiceContent;
import org.whispersystems.signalservice.api.messages.SignalServiceEnvelope;
import org.whispersystems.signalservice.internal.push.UnsupportedDataMessageException;

/* loaded from: classes4.dex */
public final class MessageDecryptionUtil {
    private static final String TAG = Log.tag(MessageDecryptionUtil.class);

    private static int envelopeTypeToCiphertextMessageType(int i) {
        if (i == 3) {
            return 3;
        }
        if (i != 6) {
            return i != 8 ? 2 : 8;
        }
        return 7;
    }

    private MessageDecryptionUtil() {
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r5v1, types: [java.lang.Object, org.whispersystems.signalservice.api.push.ServiceId] */
    /* JADX WARN: Type inference failed for: r7v1, types: [java.lang.StringBuilder] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x01c8 A[Catch: NoSenderException -> 0x0215, TryCatch #1 {NoSenderException -> 0x0215, blocks: (B:18:0x00b1, B:21:0x00bb, B:23:0x00f2, B:30:0x0103, B:33:0x0135, B:36:0x016d, B:47:0x01ad, B:49:0x01c8, B:51:0x01d2, B:53:0x01d8, B:54:0x01e3, B:55:0x01f7, B:58:0x01fd), top: B:62:0x00b1, inners: #6, #7, #13, #14, #11, #10, #8, #7, #6 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.thoughtcrime.securesms.messages.MessageDecryptionUtil.DecryptionResult decrypt(android.content.Context r11, org.whispersystems.signalservice.api.messages.SignalServiceEnvelope r12) {
        /*
        // Method dump skipped, instructions count: 545
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.messages.MessageDecryptionUtil.decrypt(android.content.Context, org.whispersystems.signalservice.api.messages.SignalServiceEnvelope):org.thoughtcrime.securesms.messages.MessageDecryptionUtil$DecryptionResult");
    }

    private static Job handleRetry(Context context, Recipient recipient, SignalServiceEnvelope signalServiceEnvelope, ProtocolException protocolException) {
        long j;
        int i;
        byte[] bArr;
        ContentHint fromType = ContentHint.fromType(protocolException.getContentHint());
        int senderDevice = protocolException.getSenderDevice();
        long currentTimeMillis = System.currentTimeMillis();
        Optional empty = Optional.empty();
        if (protocolException.getGroupId().isPresent()) {
            try {
                empty = Optional.of(GroupId.push(protocolException.getGroupId().get()));
            } catch (BadGroupIdException unused) {
                String str = TAG;
                Log.w(str, "[" + signalServiceEnvelope.getTimestamp() + "] Bad groupId!");
            }
        }
        String str2 = TAG;
        Log.w(str2, "[" + signalServiceEnvelope.getTimestamp() + "] Could not decrypt a message with a type of " + fromType);
        if (empty.isPresent()) {
            j = SignalDatabase.threads().getOrCreateThreadIdFor(Recipient.externalPossiblyMigratedGroup((GroupId) empty.get()));
        } else {
            j = SignalDatabase.threads().getOrCreateThreadIdFor(recipient);
        }
        int i2 = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$api$crypto$ContentHint[fromType.ordinal()];
        if (i2 == 1) {
            Log.w(str2, "[" + signalServiceEnvelope.getTimestamp() + "] Inserting an error right away because it's " + fromType);
            SignalDatabase.sms().insertBadDecryptMessage(recipient.getId(), senderDevice, signalServiceEnvelope.getTimestamp(), currentTimeMillis, j);
        } else if (i2 == 2) {
            Log.w(str2, "[" + signalServiceEnvelope.getTimestamp() + "] Inserting into pending retries store because it's " + fromType);
            ApplicationDependencies.getPendingRetryReceiptCache().insert(recipient.getId(), senderDevice, signalServiceEnvelope.getTimestamp(), currentTimeMillis, j);
            ApplicationDependencies.getPendingRetryReceiptManager().scheduleIfNecessary();
        } else if (i2 == 3) {
            Log.w(str2, "[" + signalServiceEnvelope.getTimestamp() + "] Not inserting any error because it's " + fromType);
        }
        if (protocolException.getUnidentifiedSenderMessageContent().isPresent()) {
            bArr = protocolException.getUnidentifiedSenderMessageContent().get().getContent();
            i = protocolException.getUnidentifiedSenderMessageContent().get().getType();
        } else {
            bArr = signalServiceEnvelope.getContent();
            i = envelopeTypeToCiphertextMessageType(signalServiceEnvelope.getType());
        }
        return new SendRetryReceiptJob(recipient.getId(), empty, DecryptionErrorMessage.forOriginalMessage(bArr, i, signalServiceEnvelope.getTimestamp(), senderDevice));
    }

    /* renamed from: org.thoughtcrime.securesms.messages.MessageDecryptionUtil$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$api$crypto$ContentHint;

        static {
            int[] iArr = new int[ContentHint.values().length];
            $SwitchMap$org$whispersystems$signalservice$api$crypto$ContentHint = iArr;
            try {
                iArr[ContentHint.DEFAULT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$crypto$ContentHint[ContentHint.RESENDABLE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$crypto$ContentHint[ContentHint.IMPLICIT.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    private static MessageContentProcessor.ExceptionMetadata toExceptionMetadata(UnsupportedDataMessageException unsupportedDataMessageException) throws NoSenderException {
        String sender = unsupportedDataMessageException.getSender();
        GroupId groupId = null;
        if (sender != null) {
            if (unsupportedDataMessageException.getGroup().isPresent()) {
                try {
                    groupId = GroupUtil.idFromGroupContext(unsupportedDataMessageException.getGroup().get());
                } catch (BadGroupIdException e) {
                    Log.w(TAG, "Bad group id found in unsupported data message", e);
                }
            }
            return new MessageContentProcessor.ExceptionMetadata(sender, unsupportedDataMessageException.getSenderDevice(), groupId);
        }
        throw new NoSenderException(null);
    }

    private static MessageContentProcessor.ExceptionMetadata toExceptionMetadata(ProtocolException protocolException) throws NoSenderException {
        String sender = protocolException.getSender();
        if (sender != null) {
            return new MessageContentProcessor.ExceptionMetadata(sender, protocolException.getSenderDevice());
        }
        throw new NoSenderException(null);
    }

    private static void postInternalErrorNotification(Context context) {
        if (FeatureFlags.internalUser()) {
            NotificationManagerCompat.from(context).notify(NotificationIds.INTERNAL_ERROR, new NotificationCompat.Builder(context, NotificationChannels.FAILURES).setSmallIcon(R.drawable.ic_notification).setContentTitle(context.getString(R.string.MessageDecryptionUtil_failed_to_decrypt_message)).setContentText(context.getString(R.string.MessageDecryptionUtil_tap_to_send_a_debug_log)).setContentIntent(PendingIntent.getActivity(context, 0, new Intent(context, SubmitDebugLogActivity.class), 0)).build());
        }
    }

    /* loaded from: classes4.dex */
    public static class NoSenderException extends Exception {
        private NoSenderException() {
        }

        /* synthetic */ NoSenderException(AnonymousClass1 r1) {
            this();
        }
    }

    /* loaded from: classes4.dex */
    public static class DecryptionResult {
        private final SignalServiceContent content;
        private final MessageContentProcessor.ExceptionMetadata exception;
        private final List<Job> jobs;
        private final MessageContentProcessor.MessageState state;

        static DecryptionResult forSuccess(SignalServiceContent signalServiceContent, List<Job> list) {
            return new DecryptionResult(MessageContentProcessor.MessageState.DECRYPTED_OK, signalServiceContent, null, list);
        }

        static DecryptionResult forError(MessageContentProcessor.MessageState messageState, MessageContentProcessor.ExceptionMetadata exceptionMetadata, List<Job> list) {
            return new DecryptionResult(messageState, null, exceptionMetadata, list);
        }

        static DecryptionResult forNoop(List<Job> list) {
            return new DecryptionResult(MessageContentProcessor.MessageState.NOOP, null, null, list);
        }

        private DecryptionResult(MessageContentProcessor.MessageState messageState, SignalServiceContent signalServiceContent, MessageContentProcessor.ExceptionMetadata exceptionMetadata, List<Job> list) {
            this.state = messageState;
            this.content = signalServiceContent;
            this.exception = exceptionMetadata;
            this.jobs = list;
        }

        public MessageContentProcessor.MessageState getState() {
            return this.state;
        }

        public SignalServiceContent getContent() {
            return this.content;
        }

        public MessageContentProcessor.ExceptionMetadata getException() {
            return this.exception;
        }

        public List<Job> getJobs() {
            return this.jobs;
        }
    }
}
