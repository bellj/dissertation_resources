package org.thoughtcrime.securesms.messages;

import j$.util.function.Function;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MessageContentProcessor$$ExternalSyntheticLambda22 implements Function {
    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return Long.valueOf(((ConversationId) obj).getThreadId());
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
