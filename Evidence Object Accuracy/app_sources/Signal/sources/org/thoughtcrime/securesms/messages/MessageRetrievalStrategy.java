package org.thoughtcrime.securesms.messages;

import java.util.HashSet;
import java.util.Set;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobTracker;
import org.thoughtcrime.securesms.jobs.MarkerJob;

/* loaded from: classes4.dex */
public abstract class MessageRetrievalStrategy {
    private volatile boolean canceled;

    /* access modifiers changed from: package-private */
    public abstract boolean execute(long j);

    void cancel() {
        this.canceled = true;
    }

    public boolean isCanceled() {
        return this.canceled;
    }

    public static void blockUntilQueueDrained(String str, String str2, long j) {
        long currentTimeMillis = System.currentTimeMillis();
        if (!ApplicationDependencies.getJobManager().runSynchronously(new MarkerJob(str2), j).isPresent()) {
            Log.w(str, "Timed out waiting for " + str2 + " job(s) to finish!");
        }
        long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
        Log.d(str, "Waited " + currentTimeMillis2 + " ms for the " + str2 + " job(s) to finish.");
    }

    public static String timeSuffix(long j) {
        return " (" + (System.currentTimeMillis() - j) + " ms elapsed)";
    }

    /* access modifiers changed from: protected */
    /* loaded from: classes4.dex */
    public static class QueueFindingJobListener implements JobTracker.JobListener {
        private final Set<String> queues = new HashSet();

        @Override // org.thoughtcrime.securesms.jobmanager.JobTracker.JobListener
        public void onStateChanged(Job job, JobTracker.JobState jobState) {
            synchronized (this.queues) {
                this.queues.add(job.getParameters().getQueue());
            }
        }

        public Set<String> getQueues() {
            HashSet hashSet;
            synchronized (this.queues) {
                hashSet = new HashSet(this.queues);
            }
            return hashSet;
        }
    }
}
