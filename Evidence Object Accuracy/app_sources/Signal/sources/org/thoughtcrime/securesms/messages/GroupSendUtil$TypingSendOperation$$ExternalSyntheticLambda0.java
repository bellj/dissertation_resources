package org.thoughtcrime.securesms.messages;

import j$.util.function.Function;
import org.thoughtcrime.securesms.messages.GroupSendUtil;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupSendUtil$TypingSendOperation$$ExternalSyntheticLambda0 implements Function {
    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return GroupSendUtil.TypingSendOperation.lambda$sendWithSenderKey$0((SignalServiceAddress) obj);
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
