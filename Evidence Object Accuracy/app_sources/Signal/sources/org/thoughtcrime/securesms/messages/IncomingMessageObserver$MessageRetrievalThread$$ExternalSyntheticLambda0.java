package org.thoughtcrime.securesms.messages;

import org.thoughtcrime.securesms.messages.IncomingMessageObserver;
import org.whispersystems.signalservice.api.SignalWebSocket;
import org.whispersystems.signalservice.api.messages.SignalServiceEnvelope;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class IncomingMessageObserver$MessageRetrievalThread$$ExternalSyntheticLambda0 implements SignalWebSocket.MessageReceivedCallback {
    @Override // org.whispersystems.signalservice.api.SignalWebSocket.MessageReceivedCallback
    public final void onMessage(SignalServiceEnvelope signalServiceEnvelope) {
        IncomingMessageObserver.MessageRetrievalThread.lambda$run$0(signalServiceEnvelope);
    }
}
