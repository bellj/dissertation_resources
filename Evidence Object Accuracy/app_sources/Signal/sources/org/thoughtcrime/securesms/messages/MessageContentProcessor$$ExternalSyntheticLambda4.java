package org.thoughtcrime.securesms.messages;

import j$.util.function.Function;
import org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MessageContentProcessor$$ExternalSyntheticLambda4 implements Function {
    public final /* synthetic */ MessageContentProcessor f$0;

    public /* synthetic */ MessageContentProcessor$$ExternalSyntheticLambda4(MessageContentProcessor messageContentProcessor) {
        this.f$0 = messageContentProcessor;
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return this.f$0.serializeTextAttachment((SignalServiceTextAttachment) obj);
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
