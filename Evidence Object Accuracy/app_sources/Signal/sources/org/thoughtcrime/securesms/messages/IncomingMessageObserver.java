package org.thoughtcrime.securesms.messages;

import android.app.Application;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.impl.BackoffUtil;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.jobs.PushDecryptDrainedJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.messages.IncomingMessageProcessor;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.push.SignalServiceNetworkAccess;
import org.thoughtcrime.securesms.util.AppForegroundObserver;
import org.whispersystems.signalservice.api.SignalWebSocket;
import org.whispersystems.signalservice.api.messages.SignalServiceEnvelope;
import org.whispersystems.signalservice.api.websocket.WebSocketUnavailableException;

/* loaded from: classes.dex */
public class IncomingMessageObserver {
    public static final int FOREGROUND_ID;
    private static final AtomicInteger INSTANCE_COUNT = new AtomicInteger(0);
    private static final long REQUEST_TIMEOUT_MINUTES;
    private static final String TAG = Log.tag(IncomingMessageObserver.class);
    private boolean appVisible;
    private final BroadcastReceiver connectionReceiver;
    private final Application context;
    private volatile boolean decryptionDrained;
    private final List<Runnable> decryptionDrainedListeners;
    private final SignalServiceNetworkAccess networkAccess;
    private volatile boolean networkDrained;
    private volatile boolean terminated;

    public IncomingMessageObserver(Application application) {
        if (INSTANCE_COUNT.incrementAndGet() == 1) {
            this.context = application;
            this.networkAccess = ApplicationDependencies.getSignalServiceNetworkAccess();
            this.decryptionDrainedListeners = new CopyOnWriteArrayList();
            new MessageRetrievalThread().start();
            if (!SignalStore.account().isFcmEnabled()) {
                ContextCompat.startForegroundService(application, new Intent(application, ForegroundService.class));
            }
            ApplicationDependencies.getAppForegroundObserver().addListener(new AppForegroundObserver.Listener() { // from class: org.thoughtcrime.securesms.messages.IncomingMessageObserver.1
                @Override // org.thoughtcrime.securesms.util.AppForegroundObserver.Listener
                public void onForeground() {
                    IncomingMessageObserver.this.onAppForegrounded();
                }

                @Override // org.thoughtcrime.securesms.util.AppForegroundObserver.Listener
                public void onBackground() {
                    IncomingMessageObserver.this.onAppBackgrounded();
                }
            });
            AnonymousClass2 r0 = new BroadcastReceiver() { // from class: org.thoughtcrime.securesms.messages.IncomingMessageObserver.2
                @Override // android.content.BroadcastReceiver
                public void onReceive(Context context, Intent intent) {
                    synchronized (IncomingMessageObserver.this) {
                        if (!NetworkConstraint.isMet(context)) {
                            Log.w(IncomingMessageObserver.TAG, "Lost network connection. Shutting down our websocket connections and resetting the drained state.");
                            IncomingMessageObserver.this.networkDrained = false;
                            IncomingMessageObserver.this.decryptionDrained = false;
                            IncomingMessageObserver.this.disconnect();
                        }
                        IncomingMessageObserver.this.notifyAll();
                    }
                }
            };
            this.connectionReceiver = r0;
            application.registerReceiver(r0, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            return;
        }
        throw new AssertionError("Multiple observers!");
    }

    public synchronized void notifyRegistrationChanged() {
        notifyAll();
    }

    public synchronized void addDecryptionDrainedListener(Runnable runnable) {
        this.decryptionDrainedListeners.add(runnable);
        if (this.decryptionDrained) {
            runnable.run();
        }
    }

    public boolean isDecryptionDrained() {
        return this.decryptionDrained || this.networkAccess.isCensored();
    }

    public void notifyDecryptionsDrained() {
        ArrayList<Runnable> arrayList = new ArrayList(this.decryptionDrainedListeners.size());
        synchronized (this) {
            if (this.networkDrained && !this.decryptionDrained) {
                Log.i(TAG, "Decryptions newly drained.");
                this.decryptionDrained = true;
                arrayList.addAll(this.decryptionDrainedListeners);
            }
        }
        for (Runnable runnable : arrayList) {
            runnable.run();
        }
    }

    public synchronized void onAppForegrounded() {
        this.appVisible = true;
        notifyAll();
    }

    public synchronized void onAppBackgrounded() {
        this.appVisible = false;
        notifyAll();
    }

    public synchronized boolean isConnectionNecessary() {
        boolean z;
        boolean isRegistered = SignalStore.account().isRegistered();
        boolean isFcmEnabled = SignalStore.account().isFcmEnabled();
        boolean isMet = NetworkConstraint.isMet(this.context);
        z = false;
        Log.d(TAG, String.format("Network: %s, Foreground: %s, FCM: %s, Censored: %s, Registered: %s, Proxy: %s", Boolean.valueOf(isMet), Boolean.valueOf(this.appVisible), Boolean.valueOf(isFcmEnabled), Boolean.valueOf(this.networkAccess.isCensored()), Boolean.valueOf(isRegistered), Boolean.valueOf(SignalStore.proxy().isProxyEnabled())));
        if (isRegistered && ((this.appVisible || !isFcmEnabled) && isMet)) {
            if (!this.networkAccess.isCensored()) {
                z = true;
            }
        }
        return z;
    }

    public synchronized void waitForConnectionNecessary() {
        while (!isConnectionNecessary()) {
            try {
                wait();
            } catch (InterruptedException e) {
                throw new AssertionError(e);
            }
        }
    }

    public void terminateAsync() {
        INSTANCE_COUNT.decrementAndGet();
        this.context.unregisterReceiver(this.connectionReceiver);
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.messages.IncomingMessageObserver$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                IncomingMessageObserver.this.lambda$terminateAsync$0();
            }
        });
    }

    public /* synthetic */ void lambda$terminateAsync$0() {
        Log.w(TAG, "Beginning termination.");
        this.terminated = true;
        disconnect();
    }

    public void disconnect() {
        ApplicationDependencies.getSignalWebSocket().disconnect();
    }

    /* loaded from: classes4.dex */
    public class MessageRetrievalThread extends Thread implements Thread.UncaughtExceptionHandler {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        MessageRetrievalThread() {
            super("MessageRetrievalService");
            IncomingMessageObserver.this = r3;
            String str = IncomingMessageObserver.TAG;
            Log.i(str, "Initializing! (" + hashCode() + ")");
            setUncaughtExceptionHandler(this);
        }

        @Override // java.lang.Thread, java.lang.Runnable
        public void run() {
            int i;
            Throwable th;
            int i2 = 0;
            while (!IncomingMessageObserver.this.terminated) {
                Log.i(IncomingMessageObserver.TAG, "Waiting for websocket state change....");
                if (i2 > 1) {
                    long exponentialBackoff = BackoffUtil.exponentialBackoff(i2, TimeUnit.SECONDS.toMillis(30));
                    String str = IncomingMessageObserver.TAG;
                    Log.w(str, "Too many failed connection attempts,  attempts: " + i2 + " backing off: " + exponentialBackoff);
                    ThreadUtil.sleep(exponentialBackoff);
                }
                IncomingMessageObserver.this.waitForConnectionNecessary();
                Log.i(IncomingMessageObserver.TAG, "Making websocket connection....");
                SignalWebSocket signalWebSocket = ApplicationDependencies.getSignalWebSocket();
                signalWebSocket.connect();
                while (IncomingMessageObserver.this.isConnectionNecessary()) {
                    try {
                        try {
                            Log.d(IncomingMessageObserver.TAG, "Reading message...");
                            try {
                                if (!signalWebSocket.readOrEmpty(TimeUnit.MINUTES.toMillis(1), new IncomingMessageObserver$MessageRetrievalThread$$ExternalSyntheticLambda0()).isPresent() && !IncomingMessageObserver.this.networkDrained) {
                                    Log.i(IncomingMessageObserver.TAG, "Network was newly-drained. Enqueuing a job to listen for decryption draining.");
                                    IncomingMessageObserver.this.networkDrained = true;
                                    ApplicationDependencies.getJobManager().add(new PushDecryptDrainedJob());
                                }
                            } catch (TimeoutException unused) {
                                Log.w(IncomingMessageObserver.TAG, "Application level read timeout...");
                                i2 = 0;
                            } catch (WebSocketUnavailableException unused2) {
                                i2 = 0;
                                Log.i(IncomingMessageObserver.TAG, "Pipe unexpectedly unavailable, connecting");
                                signalWebSocket.connect();
                            } catch (Throwable th2) {
                                th = th2;
                                i = 0;
                                i2 = i + 1;
                                try {
                                    Log.w(IncomingMessageObserver.TAG, th);
                                    Log.i(IncomingMessageObserver.TAG, "Looping...");
                                } finally {
                                    Log.w(IncomingMessageObserver.TAG, "Shutting down pipe...");
                                    IncomingMessageObserver.this.disconnect();
                                }
                            }
                        } catch (TimeoutException unused3) {
                        } catch (WebSocketUnavailableException unused4) {
                        }
                        i2 = 0;
                    } catch (Throwable th3) {
                        th = th3;
                        i = i2;
                    }
                }
                Log.w(IncomingMessageObserver.TAG, "Shutting down pipe...");
                IncomingMessageObserver.this.disconnect();
                Log.i(IncomingMessageObserver.TAG, "Looping...");
            }
            String str2 = IncomingMessageObserver.TAG;
            Log.w(str2, "Terminated! (" + hashCode() + ")");
        }

        public static /* synthetic */ void lambda$run$0(SignalServiceEnvelope signalServiceEnvelope) {
            String str = IncomingMessageObserver.TAG;
            Log.i(str, "Retrieved envelope! " + signalServiceEnvelope.getTimestamp());
            IncomingMessageProcessor.Processor acquire = ApplicationDependencies.getIncomingMessageProcessor().acquire();
            try {
                acquire.processEnvelope(signalServiceEnvelope);
                acquire.close();
            } catch (Throwable th) {
                if (acquire != null) {
                    try {
                        acquire.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }

        @Override // java.lang.Thread.UncaughtExceptionHandler
        public void uncaughtException(Thread thread, Throwable th) {
            Log.w(IncomingMessageObserver.TAG, "*** Uncaught exception!");
            Log.w(IncomingMessageObserver.TAG, th);
        }
    }

    /* loaded from: classes4.dex */
    public static class ForegroundService extends Service {
        @Override // android.app.Service
        public IBinder onBind(Intent intent) {
            return null;
        }

        @Override // android.app.Service
        public int onStartCommand(Intent intent, int i, int i2) {
            super.onStartCommand(intent, i, i2);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), NotificationChannels.BACKGROUND);
            builder.setContentTitle(getApplicationContext().getString(R.string.MessageRetrievalService_signal));
            builder.setContentText(getApplicationContext().getString(R.string.MessageRetrievalService_background_connection_enabled));
            builder.setPriority(-2);
            builder.setWhen(0);
            builder.setSmallIcon(R.drawable.ic_signal_background_connection);
            startForeground(IncomingMessageObserver.FOREGROUND_ID, builder.build());
            return 1;
        }
    }
}
