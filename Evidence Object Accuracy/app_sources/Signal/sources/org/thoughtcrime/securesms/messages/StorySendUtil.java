package org.thoughtcrime.securesms.messages;

import com.google.protobuf.InvalidProtocolBufferException;
import j$.util.Optional;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.math.MathKt__MathJVMKt;
import org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPost;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.util.Base64;
import org.whispersystems.signalservice.api.messages.SignalServicePreview;
import org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment;

/* compiled from: StorySendUtil.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J*\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0018\u0010\u0007\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bH\u0007J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0002¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/messages/StorySendUtil;", "", "()V", "deserializeBodyToStoryTextAttachment", "Lorg/whispersystems/signalservice/api/messages/SignalServiceTextAttachment;", "message", "Lorg/thoughtcrime/securesms/mms/OutgoingMediaMessage;", "getPreviewsFor", "Lkotlin/Function1;", "", "Lorg/whispersystems/signalservice/api/messages/SignalServicePreview;", "getStyle", "Lorg/whispersystems/signalservice/api/messages/SignalServiceTextAttachment$Style;", "style", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/StoryTextPost$Style;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class StorySendUtil {
    public static final StorySendUtil INSTANCE = new StorySendUtil();

    /* compiled from: StorySendUtil.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[StoryTextPost.Style.values().length];
            iArr[StoryTextPost.Style.REGULAR.ordinal()] = 1;
            iArr[StoryTextPost.Style.BOLD.ordinal()] = 2;
            iArr[StoryTextPost.Style.SERIF.ordinal()] = 3;
            iArr[StoryTextPost.Style.SCRIPT.ordinal()] = 4;
            iArr[StoryTextPost.Style.CONDENSED.ordinal()] = 5;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    private StorySendUtil() {
    }

    @JvmStatic
    public static final SignalServiceTextAttachment deserializeBodyToStoryTextAttachment(OutgoingMediaMessage outgoingMediaMessage, Function1<? super OutgoingMediaMessage, ? extends List<? extends SignalServicePreview>> function1) throws InvalidProtocolBufferException {
        Optional optional;
        Intrinsics.checkNotNullParameter(outgoingMediaMessage, "message");
        Intrinsics.checkNotNullParameter(function1, "getPreviewsFor");
        StoryTextPost parseFrom = StoryTextPost.parseFrom(Base64.decode(outgoingMediaMessage.getBody()));
        if (outgoingMediaMessage.getLinkPreviews().isEmpty()) {
            optional = Optional.empty();
            Intrinsics.checkNotNullExpressionValue(optional, "{\n      Optional.empty()\n    }");
        } else {
            optional = Optional.of(((List) function1.invoke(outgoingMediaMessage)).get(0));
            Intrinsics.checkNotNullExpressionValue(optional, "{\n      Optional.of(getP…ewsFor(message)[0])\n    }");
        }
        if (parseFrom.getBackground().hasLinearGradient()) {
            Optional ofNullable = Optional.ofNullable(parseFrom.getBody());
            StorySendUtil storySendUtil = INSTANCE;
            StoryTextPost.Style style = parseFrom.getStyle();
            Intrinsics.checkNotNullExpressionValue(style, "storyTextPost.style");
            SignalServiceTextAttachment forGradientBackground = SignalServiceTextAttachment.forGradientBackground(ofNullable, Optional.ofNullable(storySendUtil.getStyle(style)), Optional.of(Integer.valueOf(parseFrom.getTextForegroundColor())), Optional.of(Integer.valueOf(parseFrom.getTextBackgroundColor())), optional, new SignalServiceTextAttachment.Gradient(Optional.of(Integer.valueOf(parseFrom.getBackground().getLinearGradient().getColors(0))), Optional.of(Integer.valueOf(parseFrom.getBackground().getLinearGradient().getColors(1))), Optional.of(Integer.valueOf(MathKt__MathJVMKt.roundToInt(parseFrom.getBackground().getLinearGradient().getRotation())))));
            Intrinsics.checkNotNullExpressionValue(forGradientBackground, "{\n      SignalServiceTex…)\n        )\n      )\n    }");
            return forGradientBackground;
        }
        Optional ofNullable2 = Optional.ofNullable(parseFrom.getBody());
        StorySendUtil storySendUtil2 = INSTANCE;
        StoryTextPost.Style style2 = parseFrom.getStyle();
        Intrinsics.checkNotNullExpressionValue(style2, "storyTextPost.style");
        SignalServiceTextAttachment forSolidBackground = SignalServiceTextAttachment.forSolidBackground(ofNullable2, Optional.ofNullable(storySendUtil2.getStyle(style2)), Optional.of(Integer.valueOf(parseFrom.getTextForegroundColor())), Optional.of(Integer.valueOf(parseFrom.getTextBackgroundColor())), optional, parseFrom.getBackground().getSingleColor().getColor());
        Intrinsics.checkNotNullExpressionValue(forSolidBackground, "{\n      SignalServiceTex…Color.color\n      )\n    }");
        return forSolidBackground;
    }

    private final SignalServiceTextAttachment.Style getStyle(StoryTextPost.Style style) {
        int i = WhenMappings.$EnumSwitchMapping$0[style.ordinal()];
        if (i == 1) {
            return SignalServiceTextAttachment.Style.REGULAR;
        }
        if (i == 2) {
            return SignalServiceTextAttachment.Style.BOLD;
        }
        if (i == 3) {
            return SignalServiceTextAttachment.Style.SERIF;
        }
        if (i == 4) {
            return SignalServiceTextAttachment.Style.SCRIPT;
        }
        if (i != 5) {
            return SignalServiceTextAttachment.Style.DEFAULT;
        }
        return SignalServiceTextAttachment.Style.CONDENSED;
    }
}
