package org.thoughtcrime.securesms.messages;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeoutException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobmanager.JobTracker;
import org.thoughtcrime.securesms.jobs.PushProcessMessageJob;
import org.thoughtcrime.securesms.messages.IncomingMessageProcessor;
import org.thoughtcrime.securesms.messages.MessageRetrievalStrategy;
import org.whispersystems.signalservice.api.SignalWebSocket;
import org.whispersystems.signalservice.api.messages.SignalServiceEnvelope;

/* loaded from: classes4.dex */
class WebsocketStrategy extends MessageRetrievalStrategy {
    private static final String TAG = Log.tag(WebsocketStrategy.class);
    private final JobManager jobManager = ApplicationDependencies.getJobManager();
    private final SignalWebSocket signalWebSocket = ApplicationDependencies.getSignalWebSocket();

    @Override // org.thoughtcrime.securesms.messages.MessageRetrievalStrategy
    public boolean execute(long j) {
        long currentTimeMillis = System.currentTimeMillis();
        try {
            Iterator<String> it = drainWebsocket(j, currentTimeMillis).iterator();
            long max = Math.max(0L, j - (System.currentTimeMillis() - currentTimeMillis));
            while (!isCanceled() && it.hasNext() && max > 0) {
                MessageRetrievalStrategy.blockUntilQueueDrained(TAG, it.next(), max);
                max = Math.max(0L, j - (System.currentTimeMillis() - currentTimeMillis));
            }
            return true;
        } catch (IOException e) {
            Log.w(TAG, "Encountered an exception while draining the websocket.", e);
            return false;
        }
    }

    /* JADX INFO: finally extract failed */
    private Set<String> drainWebsocket(long j, long j2) throws IOException {
        MessageRetrievalStrategy.QueueFindingJobListener queueFindingJobListener = new MessageRetrievalStrategy.QueueFindingJobListener();
        this.jobManager.addListener(new JobTracker.JobFilter() { // from class: org.thoughtcrime.securesms.messages.WebsocketStrategy$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.jobmanager.JobTracker.JobFilter
            public final boolean matches(Job job) {
                return WebsocketStrategy.m2340$r8$lambda$oOGI1xffUsvqH3c7JDKWhRBcs(job);
            }
        }, queueFindingJobListener);
        try {
            this.signalWebSocket.connect();
            while (shouldContinue()) {
                try {
                } catch (TimeoutException unused) {
                    String str = TAG;
                    Log.w(str, "Websocket timeout." + MessageRetrievalStrategy.timeSuffix(j2));
                }
                if (!this.signalWebSocket.readOrEmpty(j, new SignalWebSocket.MessageReceivedCallback(j2) { // from class: org.thoughtcrime.securesms.messages.WebsocketStrategy$$ExternalSyntheticLambda1
                    public final /* synthetic */ long f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // org.whispersystems.signalservice.api.SignalWebSocket.MessageReceivedCallback
                    public final void onMessage(SignalServiceEnvelope signalServiceEnvelope) {
                        WebsocketStrategy.$r8$lambda$G8vN2GGlexIXcHjUQJYRm15YlG8(this.f$0, signalServiceEnvelope);
                    }
                }).isPresent()) {
                    String str2 = TAG;
                    Log.i(str2, "Hit an empty response. Finished." + MessageRetrievalStrategy.timeSuffix(j2));
                    break;
                }
                continue;
            }
            this.signalWebSocket.disconnect();
            this.jobManager.removeListener(queueFindingJobListener);
            return queueFindingJobListener.getQueues();
        } catch (Throwable th) {
            this.signalWebSocket.disconnect();
            this.jobManager.removeListener(queueFindingJobListener);
            throw th;
        }
    }

    public static /* synthetic */ boolean lambda$drainWebsocket$0(Job job) {
        return job.getParameters().getQueue() != null && job.getParameters().getQueue().startsWith(PushProcessMessageJob.QUEUE_PREFIX);
    }

    public static /* synthetic */ void lambda$drainWebsocket$1(long j, SignalServiceEnvelope signalServiceEnvelope) {
        String str = TAG;
        Log.i(str, "Retrieved envelope! " + signalServiceEnvelope.getTimestamp() + MessageRetrievalStrategy.timeSuffix(j));
        IncomingMessageProcessor.Processor acquire = ApplicationDependencies.getIncomingMessageProcessor().acquire();
        try {
            acquire.processEnvelope(signalServiceEnvelope);
            acquire.close();
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    private boolean shouldContinue() {
        return !isCanceled();
    }
}
