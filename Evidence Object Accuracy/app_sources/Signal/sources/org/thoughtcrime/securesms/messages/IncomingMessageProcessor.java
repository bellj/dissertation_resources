package org.thoughtcrime.securesms.messages;

import android.app.Application;
import android.content.Context;
import java.io.Closeable;
import java.util.concurrent.locks.ReentrantLock;
import org.signal.core.util.SetUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.MmsSmsDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.BadGroupIdException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobs.PushDecryptMessageJob;
import org.thoughtcrime.securesms.jobs.PushProcessMessageJob;
import org.thoughtcrime.securesms.messages.MessageDecryptionUtil;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.GroupUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.messages.SignalServiceEnvelope;
import org.whispersystems.signalservice.api.messages.SignalServiceGroupContext;

/* loaded from: classes4.dex */
public class IncomingMessageProcessor {
    private static final String TAG = Log.tag(IncomingMessageProcessor.class);
    private final Application context;
    private final ReentrantLock lock = new ReentrantLock();

    public IncomingMessageProcessor(Application application) {
        this.context = application;
    }

    public Processor acquire() {
        this.lock.lock();
        return new Processor(this.context);
    }

    public void release() {
        this.lock.unlock();
    }

    /* loaded from: classes4.dex */
    public class Processor implements Closeable {
        private final Context context;
        private final JobManager jobManager;
        private final MmsSmsDatabase mmsSmsDatabase;

        private Processor(Context context) {
            IncomingMessageProcessor.this = r1;
            this.context = context;
            this.mmsSmsDatabase = SignalDatabase.mmsSms();
            this.jobManager = ApplicationDependencies.getJobManager();
        }

        public String processEnvelope(SignalServiceEnvelope signalServiceEnvelope) {
            if (FeatureFlags.phoneNumberPrivacy() && signalServiceEnvelope.hasSourceE164()) {
                Log.w(IncomingMessageProcessor.TAG, "PNP enabled -- mimicking PNP by dropping the E164 from the envelope.");
                signalServiceEnvelope = signalServiceEnvelope.withoutE164();
            }
            if (signalServiceEnvelope.hasSourceUuid()) {
                Recipient.externalPush(signalServiceEnvelope.getSourceAddress());
            }
            if (signalServiceEnvelope.isReceipt()) {
                processReceipt(signalServiceEnvelope);
                return null;
            } else if (signalServiceEnvelope.isPreKeySignalMessage() || signalServiceEnvelope.isSignalMessage() || signalServiceEnvelope.isUnidentifiedSender() || signalServiceEnvelope.isPlaintextContent()) {
                return processMessage(signalServiceEnvelope);
            } else {
                String str = IncomingMessageProcessor.TAG;
                Log.w(str, "Received envelope of unknown type: " + signalServiceEnvelope.getType());
                return null;
            }
        }

        private String processMessage(SignalServiceEnvelope signalServiceEnvelope) {
            return processMessageDeferred(signalServiceEnvelope);
        }

        private String processMessageDeferred(SignalServiceEnvelope signalServiceEnvelope) {
            PushDecryptMessageJob pushDecryptMessageJob = new PushDecryptMessageJob(this.context, signalServiceEnvelope);
            this.jobManager.add(pushDecryptMessageJob);
            return pushDecryptMessageJob.getId();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:15:0x00ed, code lost:
            if (r6 != null) goto L_0x00ef;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x00ef, code lost:
            r6.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x00fc, code lost:
            return null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x011d, code lost:
            if (r6 != null) goto L_0x00ef;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private java.lang.String processMessageInline(org.whispersystems.signalservice.api.messages.SignalServiceEnvelope r18) {
            /*
            // Method dump skipped, instructions count: 375
            */
            throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.messages.IncomingMessageProcessor.Processor.processMessageInline(org.whispersystems.signalservice.api.messages.SignalServiceEnvelope):java.lang.String");
        }

        private void processReceipt(SignalServiceEnvelope signalServiceEnvelope) {
            Recipient externalPush = Recipient.externalPush(signalServiceEnvelope.getSourceAddress());
            String str = IncomingMessageProcessor.TAG;
            Log.i(str, "Received server receipt. Sender: " + externalPush.getId() + ", Device: " + signalServiceEnvelope.getSourceDevice() + ", Timestamp: " + signalServiceEnvelope.getTimestamp());
            this.mmsSmsDatabase.incrementDeliveryReceiptCount(new MessageDatabase.SyncMessageId(externalPush.getId(), signalServiceEnvelope.getTimestamp()), System.currentTimeMillis());
            SignalDatabase.messageLog().deleteEntryForRecipient(signalServiceEnvelope.getTimestamp(), externalPush.getId(), signalServiceEnvelope.getSourceDevice());
        }

        private boolean needsToEnqueueDecryption() {
            return !this.jobManager.areQueuesEmpty(SetUtil.newHashSet(Job.Parameters.MIGRATION_QUEUE_KEY, PushDecryptMessageJob.QUEUE)) || TextSecurePreferences.getNeedsSqlCipherMigration(this.context);
        }

        private boolean needsToEnqueueProcessing(MessageDecryptionUtil.DecryptionResult decryptionResult) {
            SignalServiceGroupContext groupContextIfPresent = GroupUtil.getGroupContextIfPresent(decryptionResult.getContent());
            if (groupContextIfPresent != null) {
                try {
                    GroupId idFromGroupContext = GroupUtil.idFromGroupContext(groupContextIfPresent);
                    if (!idFromGroupContext.isV2()) {
                        return false;
                    }
                    String queueName = PushProcessMessageJob.getQueueName(Recipient.externalPossiblyMigratedGroup(idFromGroupContext).getId());
                    GroupDatabase groups = SignalDatabase.groups();
                    if (!this.jobManager.isQueueEmpty(queueName) || groupContextIfPresent.getGroupV2().get().getRevision() > groups.getGroupV2Revision(idFromGroupContext.requireV2())) {
                        return true;
                    }
                    if (groups.getGroupV1ByExpectedV2(idFromGroupContext.requireV2()).isPresent()) {
                        return true;
                    }
                    return false;
                } catch (BadGroupIdException unused) {
                    Log.w(IncomingMessageProcessor.TAG, "Bad group ID!");
                    return false;
                }
            } else if (decryptionResult.getContent() != null) {
                return !this.jobManager.isQueueEmpty(PushProcessMessageJob.getQueueName(RecipientId.from(decryptionResult.getContent().getSender())));
            } else if (decryptionResult.getException() == null) {
                return false;
            } else {
                return !this.jobManager.isQueueEmpty(PushProcessMessageJob.getQueueName(Recipient.external(this.context, decryptionResult.getException().getSender()).getId()));
            }
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            IncomingMessageProcessor.this.release();
        }
    }
}
