package org.thoughtcrime.securesms.messages;

import android.content.Context;
import android.os.PowerManager;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.ApplicationContext;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.service.DelayedNotificationController;
import org.thoughtcrime.securesms.service.GenericForegroundService;
import org.thoughtcrime.securesms.util.PowerManagerCompat;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.WakeLockUtil;

/* loaded from: classes4.dex */
public class BackgroundMessageRetriever {
    private static final Semaphore ACTIVE_LOCK = new Semaphore(2);
    public static final long DO_NOT_SHOW_IN_FOREGROUND;
    private static final long NORMAL_TIMEOUT = TimeUnit.SECONDS.toMillis(10);
    private static final String TAG = Log.tag(BackgroundMessageRetriever.class);
    private static final String WAKE_LOCK_TAG;

    public boolean retrieveMessages(Context context, MessageRetrievalStrategy... messageRetrievalStrategyArr) {
        return retrieveMessages(context, -1, messageRetrievalStrategyArr);
    }

    public boolean retrieveMessages(Context context, long j, MessageRetrievalStrategy... messageRetrievalStrategyArr) {
        boolean executeBackgroundRetrieval;
        if (shouldIgnoreFetch()) {
            Log.i(TAG, "Skipping retrieval -- app is in the foreground.");
            return true;
        }
        Semaphore semaphore = ACTIVE_LOCK;
        if (!semaphore.tryAcquire()) {
            Log.i(TAG, "Skipping retrieval -- there's already one enqueued.");
            return true;
        }
        synchronized (this) {
            DelayedNotificationController startForegroundTaskDelayed = GenericForegroundService.startForegroundTaskDelayed(context, context.getString(R.string.BackgroundMessageRetriever_checking_for_messages), j, R.drawable.ic_signal_refresh);
            PowerManager.WakeLock wakeLock = null;
            try {
                wakeLock = WakeLockUtil.acquire(context, 1, TimeUnit.SECONDS.toMillis(60), WAKE_LOCK_TAG);
                TextSecurePreferences.setNeedsMessagePull(context, true);
                long currentTimeMillis = System.currentTimeMillis();
                boolean isDeviceIdleMode = PowerManagerCompat.isDeviceIdleMode(ServiceUtil.getPowerManager(context));
                boolean isMet = new NetworkConstraint.Factory(ApplicationContext.getInstance(context)).create().isMet();
                if (isDeviceIdleMode || !isMet) {
                    String str = TAG;
                    Log.w(str, "We may be operating in a constrained environment. Doze: " + isDeviceIdleMode + " Network: " + isMet);
                }
                Log.i(TAG, "Performing normal message fetch.");
                executeBackgroundRetrieval = executeBackgroundRetrieval(context, currentTimeMillis, messageRetrievalStrategyArr);
                WakeLockUtil.release(wakeLock, WAKE_LOCK_TAG);
                semaphore.release();
                if (startForegroundTaskDelayed != null) {
                    startForegroundTaskDelayed.close();
                }
            } catch (Throwable th) {
                WakeLockUtil.release(wakeLock, WAKE_LOCK_TAG);
                ACTIVE_LOCK.release();
                throw th;
            }
        }
        return executeBackgroundRetrieval;
    }

    private boolean executeBackgroundRetrieval(Context context, long j, MessageRetrievalStrategy[] messageRetrievalStrategyArr) {
        boolean z;
        int length = messageRetrievalStrategyArr.length;
        int i = 0;
        while (true) {
            z = true;
            if (i >= length) {
                z = false;
                break;
            }
            MessageRetrievalStrategy messageRetrievalStrategy = messageRetrievalStrategyArr[i];
            if (shouldIgnoreFetch()) {
                String str = TAG;
                Log.i(str, "Stopping further strategy attempts -- app is in the foreground." + logSuffix(j));
                break;
            }
            String str2 = TAG;
            Log.i(str2, "Attempting strategy: " + messageRetrievalStrategy.toString() + logSuffix(j));
            if (messageRetrievalStrategy.execute(NORMAL_TIMEOUT)) {
                Log.i(str2, "Strategy succeeded: " + messageRetrievalStrategy.toString() + logSuffix(j));
                break;
            }
            Log.w(str2, "Strategy failed: " + messageRetrievalStrategy.toString() + logSuffix(j));
            i++;
        }
        if (z) {
            TextSecurePreferences.setNeedsMessagePull(context, false);
        } else {
            String str3 = TAG;
            Log.w(str3, "All strategies failed!" + logSuffix(j));
        }
        return z;
    }

    public static boolean shouldIgnoreFetch() {
        return ApplicationDependencies.getAppForegroundObserver().isForegrounded() && !ApplicationDependencies.getSignalServiceNetworkAccess().isCensored();
    }

    private static String logSuffix(long j) {
        return " (" + (System.currentTimeMillis() - j) + " ms elapsed)";
    }
}
