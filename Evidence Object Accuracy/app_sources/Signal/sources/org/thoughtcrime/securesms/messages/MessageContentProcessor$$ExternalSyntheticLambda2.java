package org.thoughtcrime.securesms.messages;

import com.annimon.stream.function.Predicate;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MessageContentProcessor$$ExternalSyntheticLambda2 implements Predicate {
    @Override // com.annimon.stream.function.Predicate
    public final boolean test(Object obj) {
        return ((DatabaseAttachment) obj).isSticker();
    }
}
