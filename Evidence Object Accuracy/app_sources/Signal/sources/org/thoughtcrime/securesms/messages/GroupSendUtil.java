package org.thoughtcrime.securesms.messages;

import android.content.Context;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.stream.Collectors;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.InvalidRegistrationIdException;
import org.signal.libsignal.protocol.NoSessionException;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.database.MessageSendLogDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobs.SendRetryReceiptJob$$ExternalSyntheticLambda0;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.util.RecipientAccessList;
import org.thoughtcrime.securesms.util.SignalLocalMetrics;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.crypto.ContentHint;
import org.whispersystems.signalservice.api.crypto.UnidentifiedAccess;
import org.whispersystems.signalservice.api.crypto.UnidentifiedAccessPair;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.SendMessageResult;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.messages.SignalServiceStoryMessage;
import org.whispersystems.signalservice.api.messages.SignalServiceStoryMessageRecipient;
import org.whispersystems.signalservice.api.messages.SignalServiceTypingMessage;
import org.whispersystems.signalservice.api.messages.calls.SignalServiceCallMessage;
import org.whispersystems.signalservice.api.push.DistributionId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.internal.push.http.CancelationSignal;
import org.whispersystems.signalservice.internal.push.http.PartialSendCompleteListener;

/* loaded from: classes4.dex */
public final class GroupSendUtil {
    private static final String TAG = Log.tag(GroupSendUtil.class);

    /* loaded from: classes4.dex */
    public interface SendOperation {
        ContentHint getContentHint();

        MessageId getRelatedMessageId();

        long getSentTimestamp();

        List<SendMessageResult> sendLegacy(SignalServiceMessageSender signalServiceMessageSender, List<SignalServiceAddress> list, List<Optional<UnidentifiedAccessPair>> list2, boolean z, PartialSendCompleteListener partialSendCompleteListener, CancelationSignal cancelationSignal) throws IOException, UntrustedIdentityException;

        List<SendMessageResult> sendWithSenderKey(SignalServiceMessageSender signalServiceMessageSender, DistributionId distributionId, List<SignalServiceAddress> list, List<UnidentifiedAccess> list2, boolean z) throws NoSessionException, UntrustedIdentityException, InvalidKeyException, IOException, InvalidRegistrationIdException;

        boolean shouldIncludeInMessageLog();
    }

    private GroupSendUtil() {
    }

    public static List<SendMessageResult> sendResendableDataMessage(Context context, GroupId.V2 v2, List<Recipient> list, boolean z, ContentHint contentHint, MessageId messageId, SignalServiceDataMessage signalServiceDataMessage) throws IOException, UntrustedIdentityException {
        return sendMessage(context, v2, getDistributionId(v2), messageId, list, z, DataSendOperation.resendable(signalServiceDataMessage, contentHint, messageId), null);
    }

    public static List<SendMessageResult> sendUnresendableDataMessage(Context context, GroupId.V2 v2, List<Recipient> list, boolean z, ContentHint contentHint, SignalServiceDataMessage signalServiceDataMessage) throws IOException, UntrustedIdentityException {
        return sendMessage(context, v2, getDistributionId(v2), null, list, z, DataSendOperation.unresendable(signalServiceDataMessage, contentHint), null);
    }

    public static List<SendMessageResult> sendTypingMessage(Context context, GroupId.V2 v2, List<Recipient> list, SignalServiceTypingMessage signalServiceTypingMessage, CancelationSignal cancelationSignal) throws IOException, UntrustedIdentityException {
        return sendMessage(context, v2, getDistributionId(v2), null, list, false, new TypingSendOperation(signalServiceTypingMessage), cancelationSignal);
    }

    public static List<SendMessageResult> sendCallMessage(Context context, GroupId.V2 v2, List<Recipient> list, SignalServiceCallMessage signalServiceCallMessage) throws IOException, UntrustedIdentityException {
        return sendMessage(context, v2, getDistributionId(v2), null, list, false, new CallSendOperation(signalServiceCallMessage), null);
    }

    public static List<SendMessageResult> sendStoryMessage(Context context, DistributionListId distributionListId, List<Recipient> list, boolean z, MessageId messageId, long j, SignalServiceStoryMessage signalServiceStoryMessage, Set<SignalServiceStoryMessageRecipient> set) throws IOException, UntrustedIdentityException {
        return sendMessage(context, null, getDistributionId(distributionListId), messageId, list, z, new StorySendOperation(messageId, null, j, signalServiceStoryMessage, set), null);
    }

    public static List<SendMessageResult> sendGroupStoryMessage(Context context, GroupId.V2 v2, List<Recipient> list, boolean z, MessageId messageId, long j, SignalServiceStoryMessage signalServiceStoryMessage) throws IOException, UntrustedIdentityException {
        return sendMessage(context, v2, getDistributionId(v2), messageId, list, z, new StorySendOperation(messageId, v2, j, signalServiceStoryMessage, Collections.emptySet()), null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:100:0x0382  */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x0397 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x03b2  */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x03d2  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x0409  */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x0483  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x036f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.util.List<org.whispersystems.signalservice.api.messages.SendMessageResult> sendMessage(android.content.Context r22, org.thoughtcrime.securesms.groups.GroupId.V2 r23, org.whispersystems.signalservice.api.push.DistributionId r24, org.thoughtcrime.securesms.database.model.MessageId r25, java.util.List<org.thoughtcrime.securesms.recipients.Recipient> r26, boolean r27, org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation r28, org.whispersystems.signalservice.internal.push.http.CancelationSignal r29) throws java.io.IOException, org.whispersystems.signalservice.api.crypto.UntrustedIdentityException {
        /*
        // Method dump skipped, instructions count: 1264
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.messages.GroupSendUtil.sendMessage(android.content.Context, org.thoughtcrime.securesms.groups.GroupId$V2, org.whispersystems.signalservice.api.push.DistributionId, org.thoughtcrime.securesms.database.model.MessageId, java.util.List, boolean, org.thoughtcrime.securesms.messages.GroupSendUtil$SendOperation, org.whispersystems.signalservice.internal.push.http.CancelationSignal):java.util.List");
    }

    public static /* synthetic */ boolean lambda$sendMessage$0(Set set, Recipient recipient) {
        return !set.contains(recipient);
    }

    public static /* synthetic */ SignalServiceAddress lambda$sendMessage$1(RecipientData recipientData, Recipient recipient) {
        return recipientData.getAddress(recipient.getId());
    }

    public static /* synthetic */ UnidentifiedAccess lambda$sendMessage$2(RecipientData recipientData, Recipient recipient) {
        return recipientData.requireAccess(recipient.getId());
    }

    public static /* synthetic */ SignalServiceAddress lambda$sendMessage$3(RecipientData recipientData, Recipient recipient) {
        return recipientData.getAddress(recipient.getId());
    }

    public static /* synthetic */ Optional lambda$sendMessage$4(RecipientData recipientData, Recipient recipient) {
        return recipientData.getAccessPair(recipient.getId());
    }

    public static /* synthetic */ void lambda$sendMessage$5(boolean z, AtomicLong atomicLong, MessageSendLogDatabase messageSendLogDatabase, RecipientData recipientData, SendOperation sendOperation, SendMessageResult sendMessageResult) {
        if (z) {
            synchronized (atomicLong) {
                if (atomicLong.get() == -1) {
                    atomicLong.set(messageSendLogDatabase.insertIfPossible(recipientData.requireRecipientId(sendMessageResult.getAddress()), sendOperation.getSentTimestamp(), sendMessageResult, sendOperation.getContentHint(), sendOperation.getRelatedMessageId()));
                } else {
                    atomicLong.set(messageSendLogDatabase.addRecipientToExistingEntryIfPossible(atomicLong.get(), recipientData.requireRecipientId(sendMessageResult.getAddress()), sendOperation.getSentTimestamp(), sendMessageResult, sendOperation.getContentHint(), sendOperation.getRelatedMessageId()));
                }
            }
        }
    }

    public static /* synthetic */ SendMessageResult lambda$sendMessage$6(Recipient recipient) {
        return SendMessageResult.unregisteredFailure(new SignalServiceAddress(recipient.requireServiceId(), recipient.getE164().orElse(null)));
    }

    private static DistributionId getDistributionId(GroupId.V2 v2) {
        if (v2 != null) {
            return SignalDatabase.groups().getOrCreateDistributionId(v2);
        }
        return null;
    }

    private static DistributionId getDistributionId(DistributionListId distributionListId) {
        if (distributionListId != null) {
            return (DistributionId) Optional.ofNullable(SignalDatabase.distributionLists().getDistributionId(distributionListId)).orElse(null);
        }
        return null;
    }

    /* loaded from: classes4.dex */
    public static class DataSendOperation implements SendOperation {
        private final ContentHint contentHint;
        private final SignalServiceDataMessage message;
        private final MessageId relatedMessageId;
        private final boolean resendable;

        public static DataSendOperation resendable(SignalServiceDataMessage signalServiceDataMessage, ContentHint contentHint, MessageId messageId) {
            return new DataSendOperation(signalServiceDataMessage, contentHint, true, messageId);
        }

        public static DataSendOperation unresendable(SignalServiceDataMessage signalServiceDataMessage, ContentHint contentHint) {
            return new DataSendOperation(signalServiceDataMessage, contentHint, false, null);
        }

        private DataSendOperation(SignalServiceDataMessage signalServiceDataMessage, ContentHint contentHint, boolean z, MessageId messageId) {
            this.message = signalServiceDataMessage;
            this.contentHint = contentHint;
            this.resendable = z;
            this.relatedMessageId = messageId;
            if (z && messageId == null) {
                throw new IllegalArgumentException("If a message is resendable, it must have a related message ID!");
            }
        }

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public List<SendMessageResult> sendWithSenderKey(SignalServiceMessageSender signalServiceMessageSender, DistributionId distributionId, List<SignalServiceAddress> list, List<UnidentifiedAccess> list2, boolean z) throws NoSessionException, UntrustedIdentityException, InvalidKeyException, IOException, InvalidRegistrationIdException {
            MessageId messageId = this.relatedMessageId;
            return signalServiceMessageSender.sendGroupDataMessage(distributionId, list, list2, z, this.contentHint, this.message, messageId != null ? new SenderKeyMetricEventListener(messageId.getId()) : SignalServiceMessageSender.SenderKeyGroupEvents.EMPTY);
        }

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public List<SendMessageResult> sendLegacy(SignalServiceMessageSender signalServiceMessageSender, List<SignalServiceAddress> list, List<Optional<UnidentifiedAccessPair>> list2, boolean z, PartialSendCompleteListener partialSendCompleteListener, CancelationSignal cancelationSignal) throws IOException, UntrustedIdentityException {
            MessageId messageId = this.relatedMessageId;
            return signalServiceMessageSender.sendDataMessage(list, list2, z, this.contentHint, this.message, messageId != null ? new LegacyMetricEventListener(messageId.getId()) : SignalServiceMessageSender.LegacyGroupEvents.EMPTY, partialSendCompleteListener, cancelationSignal);
        }

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public ContentHint getContentHint() {
            return this.contentHint;
        }

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public long getSentTimestamp() {
            return this.message.getTimestamp();
        }

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public boolean shouldIncludeInMessageLog() {
            return this.resendable;
        }

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public MessageId getRelatedMessageId() {
            MessageId messageId = this.relatedMessageId;
            if (messageId != null) {
                return messageId;
            }
            throw new UnsupportedOperationException();
        }
    }

    /* loaded from: classes4.dex */
    public static class TypingSendOperation implements SendOperation {
        private final SignalServiceTypingMessage message;

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public boolean shouldIncludeInMessageLog() {
            return false;
        }

        private TypingSendOperation(SignalServiceTypingMessage signalServiceTypingMessage) {
            this.message = signalServiceTypingMessage;
        }

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public List<SendMessageResult> sendWithSenderKey(SignalServiceMessageSender signalServiceMessageSender, DistributionId distributionId, List<SignalServiceAddress> list, List<UnidentifiedAccess> list2, boolean z) throws NoSessionException, UntrustedIdentityException, InvalidKeyException, IOException, InvalidRegistrationIdException {
            signalServiceMessageSender.sendGroupTyping(distributionId, list, list2, this.message);
            return (List) Collection$EL.stream(list).map(new GroupSendUtil$TypingSendOperation$$ExternalSyntheticLambda0()).collect(Collectors.toList());
        }

        public static /* synthetic */ SendMessageResult lambda$sendWithSenderKey$0(SignalServiceAddress signalServiceAddress) {
            return SendMessageResult.success(signalServiceAddress, Collections.emptyList(), true, false, -1, Optional.empty());
        }

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public List<SendMessageResult> sendLegacy(SignalServiceMessageSender signalServiceMessageSender, List<SignalServiceAddress> list, List<Optional<UnidentifiedAccessPair>> list2, boolean z, PartialSendCompleteListener partialSendCompleteListener, CancelationSignal cancelationSignal) throws IOException {
            signalServiceMessageSender.sendTyping(list, list2, this.message, cancelationSignal);
            return (List) Collection$EL.stream(list).map(new GroupSendUtil$TypingSendOperation$$ExternalSyntheticLambda1()).collect(Collectors.toList());
        }

        public static /* synthetic */ SendMessageResult lambda$sendLegacy$1(SignalServiceAddress signalServiceAddress) {
            return SendMessageResult.success(signalServiceAddress, Collections.emptyList(), true, false, -1, Optional.empty());
        }

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public ContentHint getContentHint() {
            return ContentHint.IMPLICIT;
        }

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public long getSentTimestamp() {
            return this.message.getTimestamp();
        }

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public MessageId getRelatedMessageId() {
            throw new UnsupportedOperationException();
        }
    }

    /* loaded from: classes4.dex */
    public static class CallSendOperation implements SendOperation {
        private final SignalServiceCallMessage message;

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public boolean shouldIncludeInMessageLog() {
            return false;
        }

        private CallSendOperation(SignalServiceCallMessage signalServiceCallMessage) {
            this.message = signalServiceCallMessage;
        }

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public List<SendMessageResult> sendWithSenderKey(SignalServiceMessageSender signalServiceMessageSender, DistributionId distributionId, List<SignalServiceAddress> list, List<UnidentifiedAccess> list2, boolean z) throws NoSessionException, UntrustedIdentityException, InvalidKeyException, IOException, InvalidRegistrationIdException {
            return signalServiceMessageSender.sendCallMessage(distributionId, list, list2, this.message);
        }

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public List<SendMessageResult> sendLegacy(SignalServiceMessageSender signalServiceMessageSender, List<SignalServiceAddress> list, List<Optional<UnidentifiedAccessPair>> list2, boolean z, PartialSendCompleteListener partialSendCompleteListener, CancelationSignal cancelationSignal) throws IOException {
            return signalServiceMessageSender.sendCallMessage(list, list2, this.message);
        }

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public ContentHint getContentHint() {
            return ContentHint.IMPLICIT;
        }

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public long getSentTimestamp() {
            return this.message.getTimestamp().get().longValue();
        }

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public MessageId getRelatedMessageId() {
            throw new UnsupportedOperationException();
        }
    }

    /* loaded from: classes4.dex */
    public static class StorySendOperation implements SendOperation {
        private final GroupId groupId;
        private final Set<SignalServiceStoryMessageRecipient> manifest;
        private final SignalServiceStoryMessage message;
        private final MessageId relatedMessageId;
        private final long sentTimestamp;

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public boolean shouldIncludeInMessageLog() {
            return true;
        }

        public StorySendOperation(MessageId messageId, GroupId groupId, long j, SignalServiceStoryMessage signalServiceStoryMessage, Set<SignalServiceStoryMessageRecipient> set) {
            this.relatedMessageId = messageId;
            this.groupId = groupId;
            this.sentTimestamp = j;
            this.message = signalServiceStoryMessage;
            this.manifest = set;
        }

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public List<SendMessageResult> sendWithSenderKey(SignalServiceMessageSender signalServiceMessageSender, DistributionId distributionId, List<SignalServiceAddress> list, List<UnidentifiedAccess> list2, boolean z) throws NoSessionException, UntrustedIdentityException, InvalidKeyException, IOException, InvalidRegistrationIdException {
            return signalServiceMessageSender.sendGroupStory(distributionId, Optional.ofNullable(this.groupId).map(new SendRetryReceiptJob$$ExternalSyntheticLambda0()), list, list2, this.message, getSentTimestamp(), this.manifest);
        }

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public List<SendMessageResult> sendLegacy(SignalServiceMessageSender signalServiceMessageSender, List<SignalServiceAddress> list, List<Optional<UnidentifiedAccessPair>> list2, boolean z, PartialSendCompleteListener partialSendCompleteListener, CancelationSignal cancelationSignal) throws IOException, UntrustedIdentityException {
            return signalServiceMessageSender.sendStory(list, list2, this.message, getSentTimestamp(), this.manifest);
        }

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public ContentHint getContentHint() {
            return ContentHint.RESENDABLE;
        }

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public long getSentTimestamp() {
            return this.sentTimestamp;
        }

        @Override // org.thoughtcrime.securesms.messages.GroupSendUtil.SendOperation
        public MessageId getRelatedMessageId() {
            return this.relatedMessageId;
        }
    }

    /* loaded from: classes4.dex */
    private static final class SenderKeyMetricEventListener implements SignalServiceMessageSender.SenderKeyGroupEvents {
        private final long messageId;

        private SenderKeyMetricEventListener(long j) {
            this.messageId = j;
        }

        @Override // org.whispersystems.signalservice.api.SignalServiceMessageSender.SenderKeyGroupEvents
        public void onSenderKeyShared() {
            SignalLocalMetrics.GroupMessageSend.onSenderKeyShared(this.messageId);
        }

        @Override // org.whispersystems.signalservice.api.SignalServiceMessageSender.SenderKeyGroupEvents
        public void onMessageEncrypted() {
            SignalLocalMetrics.GroupMessageSend.onSenderKeyEncrypted(this.messageId);
        }

        @Override // org.whispersystems.signalservice.api.SignalServiceMessageSender.SenderKeyGroupEvents
        public void onMessageSent() {
            SignalLocalMetrics.GroupMessageSend.onSenderKeyMessageSent(this.messageId);
        }

        @Override // org.whispersystems.signalservice.api.SignalServiceMessageSender.SenderKeyGroupEvents
        public void onSyncMessageSent() {
            SignalLocalMetrics.GroupMessageSend.onSenderKeySyncSent(this.messageId);
        }
    }

    /* loaded from: classes4.dex */
    private static final class LegacyMetricEventListener implements SignalServiceMessageSender.LegacyGroupEvents {
        private final long messageId;

        private LegacyMetricEventListener(long j) {
            this.messageId = j;
        }

        @Override // org.whispersystems.signalservice.api.SignalServiceMessageSender.LegacyGroupEvents
        public void onMessageSent() {
            SignalLocalMetrics.GroupMessageSend.onLegacyMessageSent(this.messageId);
        }

        @Override // org.whispersystems.signalservice.api.SignalServiceMessageSender.LegacyGroupEvents
        public void onSyncMessageSent() {
            SignalLocalMetrics.GroupMessageSend.onLegacySyncFinished(this.messageId);
        }
    }

    /* loaded from: classes4.dex */
    public static final class RecipientData {
        private final Map<RecipientId, Optional<UnidentifiedAccessPair>> accessById;
        private final RecipientAccessList accessList;
        private final Map<RecipientId, SignalServiceAddress> addressById;

        RecipientData(Context context, List<Recipient> list) throws IOException {
            this.accessById = UnidentifiedAccessUtil.getAccessMapFor(context, list);
            this.addressById = mapAddresses(context, list);
            this.accessList = new RecipientAccessList(list);
        }

        SignalServiceAddress getAddress(RecipientId recipientId) {
            SignalServiceAddress signalServiceAddress = this.addressById.get(recipientId);
            Objects.requireNonNull(signalServiceAddress);
            return signalServiceAddress;
        }

        Optional<UnidentifiedAccessPair> getAccessPair(RecipientId recipientId) {
            Optional<UnidentifiedAccessPair> optional = this.accessById.get(recipientId);
            Objects.requireNonNull(optional);
            return optional;
        }

        UnidentifiedAccess requireAccess(RecipientId recipientId) {
            Optional<UnidentifiedAccessPair> optional = this.accessById.get(recipientId);
            Objects.requireNonNull(optional);
            return optional.get().getTargetUnidentifiedAccess().get();
        }

        RecipientId requireRecipientId(SignalServiceAddress signalServiceAddress) {
            return this.accessList.requireIdByAddress(signalServiceAddress);
        }

        private static Map<RecipientId, SignalServiceAddress> mapAddresses(Context context, List<Recipient> list) throws IOException {
            List<SignalServiceAddress> signalServiceAddressesFromResolved = RecipientUtil.toSignalServiceAddressesFromResolved(context, list);
            Iterator<SignalServiceAddress> it = signalServiceAddressesFromResolved.iterator();
            HashMap hashMap = new HashMap(list.size());
            for (Recipient recipient : list) {
                hashMap.put(recipient.getId(), it.next());
            }
            return hashMap;
        }
    }
}
