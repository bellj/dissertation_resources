package org.thoughtcrime.securesms.messages;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobmanager.JobTracker;
import org.thoughtcrime.securesms.jobs.MarkerJob;
import org.thoughtcrime.securesms.jobs.PushDecryptMessageJob;
import org.thoughtcrime.securesms.jobs.PushProcessMessageJob;
import org.thoughtcrime.securesms.messages.IncomingMessageProcessor;
import org.thoughtcrime.securesms.messages.MessageRetrievalStrategy;
import org.whispersystems.signalservice.api.SignalServiceMessageReceiver;
import org.whispersystems.signalservice.api.messages.SignalServiceEnvelope;

/* loaded from: classes4.dex */
public class RestStrategy extends MessageRetrievalStrategy {
    private static final String TAG = Log.tag(RestStrategy.class);

    @Override // org.thoughtcrime.securesms.messages.MessageRetrievalStrategy
    public boolean execute(long j) {
        JobManager jobManager;
        MessageRetrievalStrategy.QueueFindingJobListener queueFindingJobListener;
        try {
            long currentTimeMillis = System.currentTimeMillis();
            jobManager = ApplicationDependencies.getJobManager();
            queueFindingJobListener = new MessageRetrievalStrategy.QueueFindingJobListener();
            IncomingMessageProcessor.Processor acquire = ApplicationDependencies.getIncomingMessageProcessor().acquire();
            try {
                jobManager.addListener(new JobTracker.JobFilter() { // from class: org.thoughtcrime.securesms.messages.RestStrategy$$ExternalSyntheticLambda1
                    @Override // org.thoughtcrime.securesms.jobmanager.JobTracker.JobFilter
                    public final boolean matches(Job job) {
                        return RestStrategy.lambda$execute$0(job);
                    }
                }, queueFindingJobListener);
                int enqueuePushDecryptJobs = enqueuePushDecryptJobs(acquire, currentTimeMillis, j);
                if (enqueuePushDecryptJobs == 0) {
                    Log.d(TAG, "No PushDecryptMessageJobs were enqueued.");
                    if (acquire != null) {
                        acquire.close();
                    }
                    return true;
                }
                String str = TAG;
                Log.d(str, enqueuePushDecryptJobs + " PushDecryptMessageJob(s) were enqueued.");
                long blockUntilQueueDrained = blockUntilQueueDrained(PushDecryptMessageJob.QUEUE, TimeUnit.SECONDS.toMillis(10));
                Set<String> queues = queueFindingJobListener.getQueues();
                Log.d(str, "Discovered " + queues.size() + " queue(s): " + queues);
                if (blockUntilQueueDrained > 0) {
                    Iterator<String> it = queues.iterator();
                    while (it.hasNext() && blockUntilQueueDrained > 0) {
                        blockUntilQueueDrained = blockUntilQueueDrained(it.next(), blockUntilQueueDrained);
                    }
                    if (blockUntilQueueDrained <= 0) {
                        Log.w(TAG, "Ran out of time while waiting for queues to drain.");
                    }
                } else {
                    Log.w(str, "Ran out of time before we could even wait on individual queues!");
                }
                if (acquire != null) {
                    acquire.close();
                }
                return true;
            } catch (Throwable th) {
                if (acquire != null) {
                    try {
                        acquire.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        } catch (IOException e) {
            Log.w(TAG, "Failed to retrieve messages. Resetting the SignalServiceMessageReceiver.", e);
            ApplicationDependencies.resetSignalServiceMessageReceiver();
            return false;
        } finally {
            jobManager.removeListener(queueFindingJobListener);
        }
    }

    public static /* synthetic */ boolean lambda$execute$0(Job job) {
        return job.getParameters().getQueue() != null && job.getParameters().getQueue().startsWith(PushProcessMessageJob.QUEUE_PREFIX);
    }

    private static int enqueuePushDecryptJobs(IncomingMessageProcessor.Processor processor, long j, long j2) throws IOException {
        SignalServiceMessageReceiver signalServiceMessageReceiver = ApplicationDependencies.getSignalServiceMessageReceiver();
        AtomicInteger atomicInteger = new AtomicInteger(0);
        signalServiceMessageReceiver.setSoTimeoutMillis(j2);
        signalServiceMessageReceiver.retrieveMessages(new SignalServiceMessageReceiver.MessageReceivedCallback(j, processor, atomicInteger) { // from class: org.thoughtcrime.securesms.messages.RestStrategy$$ExternalSyntheticLambda0
            public final /* synthetic */ long f$0;
            public final /* synthetic */ IncomingMessageProcessor.Processor f$1;
            public final /* synthetic */ AtomicInteger f$2;

            {
                this.f$0 = r1;
                this.f$1 = r3;
                this.f$2 = r4;
            }

            @Override // org.whispersystems.signalservice.api.SignalServiceMessageReceiver.MessageReceivedCallback
            public final void onMessage(SignalServiceEnvelope signalServiceEnvelope) {
                RestStrategy.lambda$enqueuePushDecryptJobs$1(this.f$0, this.f$1, this.f$2, signalServiceEnvelope);
            }
        });
        return atomicInteger.get();
    }

    public static /* synthetic */ void lambda$enqueuePushDecryptJobs$1(long j, IncomingMessageProcessor.Processor processor, AtomicInteger atomicInteger, SignalServiceEnvelope signalServiceEnvelope) {
        String str = TAG;
        Log.i(str, "Retrieved an envelope." + MessageRetrievalStrategy.timeSuffix(j));
        if (processor.processEnvelope(signalServiceEnvelope) != null) {
            atomicInteger.incrementAndGet();
        }
        Log.i(str, "Successfully processed an envelope." + MessageRetrievalStrategy.timeSuffix(j));
    }

    private static long blockUntilQueueDrained(String str, long j) {
        long currentTimeMillis = System.currentTimeMillis();
        if (!ApplicationDependencies.getJobManager().runSynchronously(new MarkerJob(str), j).isPresent()) {
            String str2 = TAG;
            Log.w(str2, "Timed out waiting for " + str + " job(s) to finish!");
        }
        long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
        String str3 = TAG;
        Log.d(str3, "Waited " + currentTimeMillis2 + " ms for the " + str + " job(s) to finish.");
        return j - currentTimeMillis2;
    }

    public String toString() {
        return Log.tag(RestStrategy.class);
    }
}
