package org.thoughtcrime.securesms.messages;

import android.content.Context;
import android.text.TextUtils;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import com.google.protobuf.ByteString;
import com.mobilecoin.lib.exceptions.SerializationException;
import j$.util.Optional;
import j$.util.function.Function;
import j$.util.function.Supplier;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.Hex;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.signal.libsignal.protocol.ecc.ECPublicKey;
import org.signal.libsignal.protocol.message.DecryptionErrorMessage;
import org.signal.libsignal.protocol.util.Pair;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.signal.ringrtc.CallId;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.attachments.PointerAttachment;
import org.thoughtcrime.securesms.attachments.TombstoneAttachment;
import org.thoughtcrime.securesms.attachments.UriAttachment;
import org.thoughtcrime.securesms.components.emoji.EmojiUtil;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.contactshare.ContactModelMapper;
import org.thoughtcrime.securesms.conversationlist.ConversationListDataSource$$ExternalSyntheticLambda0;
import org.thoughtcrime.securesms.crypto.ProfileKeyUtil;
import org.thoughtcrime.securesms.crypto.SecurityEvent;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.GroupReceiptDatabase;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.NoSuchMessageException;
import org.thoughtcrime.securesms.database.PaymentDatabase;
import org.thoughtcrime.securesms.database.PaymentMetaDataUtil;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SentStorySyncManifest;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.SmsDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageLogEntry;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.database.model.ParentStoryId;
import org.thoughtcrime.securesms.database.model.PendingRetryReceiptModel;
import org.thoughtcrime.securesms.database.model.ReactionRecord;
import org.thoughtcrime.securesms.database.model.StickerRecord;
import org.thoughtcrime.securesms.database.model.StoryType;
import org.thoughtcrime.securesms.database.model.ThreadRecord;
import org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPost;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.BadGroupIdException;
import org.thoughtcrime.securesms.groups.GroupChangeBusyException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.groups.GroupNotAMemberException;
import org.thoughtcrime.securesms.groups.GroupV1MessageProcessor;
import org.thoughtcrime.securesms.groups.GroupsV1MigrationUtil;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobs.AttachmentDownloadJob;
import org.thoughtcrime.securesms.jobs.AutomaticSessionResetJob;
import org.thoughtcrime.securesms.jobs.GroupCallPeekJob;
import org.thoughtcrime.securesms.jobs.MultiDeviceBlockedUpdateJob;
import org.thoughtcrime.securesms.jobs.MultiDeviceConfigurationUpdateJob;
import org.thoughtcrime.securesms.jobs.MultiDeviceContactSyncJob;
import org.thoughtcrime.securesms.jobs.MultiDeviceContactUpdateJob;
import org.thoughtcrime.securesms.jobs.MultiDeviceGroupUpdateJob;
import org.thoughtcrime.securesms.jobs.MultiDeviceKeysUpdateJob;
import org.thoughtcrime.securesms.jobs.MultiDevicePniIdentityUpdateJob;
import org.thoughtcrime.securesms.jobs.MultiDeviceStickerPackSyncJob;
import org.thoughtcrime.securesms.jobs.NullMessageSendJob;
import org.thoughtcrime.securesms.jobs.PaymentLedgerUpdateJob;
import org.thoughtcrime.securesms.jobs.PaymentTransactionCheckJob;
import org.thoughtcrime.securesms.jobs.PushGroupSendJob$$ExternalSyntheticLambda0;
import org.thoughtcrime.securesms.jobs.PushProcessEarlyMessagesJob;
import org.thoughtcrime.securesms.jobs.PushProcessMessageJob;
import org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob;
import org.thoughtcrime.securesms.jobs.RequestGroupInfoJob;
import org.thoughtcrime.securesms.jobs.ResendMessageJob;
import org.thoughtcrime.securesms.jobs.RetrieveProfileJob;
import org.thoughtcrime.securesms.jobs.SendDeliveryReceiptJob;
import org.thoughtcrime.securesms.jobs.SenderKeyDistributionSendJob;
import org.thoughtcrime.securesms.jobs.StickerPackDownloadJob;
import org.thoughtcrime.securesms.jobs.TrimThreadJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.linkpreview.LinkPreviewUtil;
import org.thoughtcrime.securesms.mms.IncomingMediaMessage;
import org.thoughtcrime.securesms.mms.MmsException;
import org.thoughtcrime.securesms.mms.OutgoingExpirationUpdateMessage;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.mms.OutgoingSecureMediaMessage;
import org.thoughtcrime.securesms.mms.QuoteModel;
import org.thoughtcrime.securesms.mms.SlideDeck;
import org.thoughtcrime.securesms.notifications.MarkReadReceiver;
import org.thoughtcrime.securesms.notifications.MessageNotifier;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.payments.MobileCoinPublicAddress;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.thoughtcrime.securesms.service.webrtc.WebRtcData;
import org.thoughtcrime.securesms.sms.IncomingEncryptedMessage;
import org.thoughtcrime.securesms.sms.IncomingEndSessionMessage;
import org.thoughtcrime.securesms.sms.IncomingTextMessage;
import org.thoughtcrime.securesms.sms.OutgoingEncryptedMessage;
import org.thoughtcrime.securesms.sms.OutgoingEndSessionMessage;
import org.thoughtcrime.securesms.sms.OutgoingTextMessage;
import org.thoughtcrime.securesms.stickers.StickerLocator;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.GroupUtil;
import org.thoughtcrime.securesms.util.IdentityUtil;
import org.thoughtcrime.securesms.util.LinkUtil;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.RemoteDeleteUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentPointer;
import org.whispersystems.signalservice.api.messages.SignalServiceContent;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.messages.SignalServiceGroup;
import org.whispersystems.signalservice.api.messages.SignalServiceGroupContext;
import org.whispersystems.signalservice.api.messages.SignalServiceGroupV2;
import org.whispersystems.signalservice.api.messages.SignalServicePreview;
import org.whispersystems.signalservice.api.messages.SignalServiceReceiptMessage;
import org.whispersystems.signalservice.api.messages.SignalServiceStoryMessage;
import org.whispersystems.signalservice.api.messages.SignalServiceTextAttachment;
import org.whispersystems.signalservice.api.messages.SignalServiceTypingMessage;
import org.whispersystems.signalservice.api.messages.calls.AnswerMessage;
import org.whispersystems.signalservice.api.messages.calls.BusyMessage;
import org.whispersystems.signalservice.api.messages.calls.HangupMessage;
import org.whispersystems.signalservice.api.messages.calls.IceUpdateMessage;
import org.whispersystems.signalservice.api.messages.calls.OfferMessage;
import org.whispersystems.signalservice.api.messages.calls.OpaqueMessage;
import org.whispersystems.signalservice.api.messages.multidevice.BlockedListMessage;
import org.whispersystems.signalservice.api.messages.multidevice.ConfigurationMessage;
import org.whispersystems.signalservice.api.messages.multidevice.ContactsMessage;
import org.whispersystems.signalservice.api.messages.multidevice.KeysMessage;
import org.whispersystems.signalservice.api.messages.multidevice.MessageRequestResponseMessage;
import org.whispersystems.signalservice.api.messages.multidevice.OutgoingPaymentMessage;
import org.whispersystems.signalservice.api.messages.multidevice.ReadMessage;
import org.whispersystems.signalservice.api.messages.multidevice.RequestMessage;
import org.whispersystems.signalservice.api.messages.multidevice.SentTranscriptMessage;
import org.whispersystems.signalservice.api.messages.multidevice.SignalServiceSyncMessage;
import org.whispersystems.signalservice.api.messages.multidevice.StickerPackOperationMessage;
import org.whispersystems.signalservice.api.messages.multidevice.VerifiedMessage;
import org.whispersystems.signalservice.api.messages.multidevice.ViewOnceOpenMessage;
import org.whispersystems.signalservice.api.messages.multidevice.ViewedMessage;
import org.whispersystems.signalservice.api.messages.shared.SharedContact;
import org.whispersystems.signalservice.api.payments.Money;
import org.whispersystems.signalservice.api.push.DistributionId;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;

/* loaded from: classes4.dex */
public final class MessageContentProcessor {
    private static final String TAG = Log.tag(MessageContentProcessor.class);
    private final Context context;
    private final boolean processingEarlyContent;

    /* loaded from: classes4.dex */
    public enum MessageState {
        DECRYPTED_OK,
        INVALID_VERSION,
        CORRUPT_MESSAGE,
        NO_SESSION,
        LEGACY_MESSAGE,
        DUPLICATE_MESSAGE,
        UNSUPPORTED_DATA_MESSAGE,
        NOOP
    }

    public static MessageContentProcessor forNormalContent(Context context) {
        return new MessageContentProcessor(context, false);
    }

    public static MessageContentProcessor forEarlyContent(Context context) {
        return new MessageContentProcessor(context, true);
    }

    private MessageContentProcessor(Context context, boolean z) {
        this.context = context;
        this.processingEarlyContent = z;
    }

    public void process(MessageState messageState, SignalServiceContent signalServiceContent, ExceptionMetadata exceptionMetadata, long j, long j2) throws IOException, GroupChangeBusyException {
        Optional<Long> of = j2 > 0 ? Optional.of(Long.valueOf(j2)) : Optional.empty();
        if (messageState == MessageState.DECRYPTED_OK) {
            if (signalServiceContent != null) {
                Recipient externalPush = Recipient.externalPush(signalServiceContent.getSender());
                handleMessage(signalServiceContent, j, externalPush, of);
                Optional<List<SignalServiceContent>> retrieve = ApplicationDependencies.getEarlyMessageCache().retrieve(externalPush.getId(), signalServiceContent.getTimestamp());
                if (retrieve.isPresent()) {
                    String valueOf = String.valueOf(signalServiceContent.getTimestamp());
                    log(valueOf, "Found " + retrieve.get().size() + " dependent item(s) that were retrieved earlier. Processing.");
                    for (SignalServiceContent signalServiceContent2 : retrieve.get()) {
                        handleMessage(signalServiceContent2, j, externalPush, Optional.empty());
                    }
                    return;
                }
                return;
            }
            warn("null", "Null content. Ignoring message.");
        } else if (exceptionMetadata != null) {
            handleExceptionMessage(messageState, exceptionMetadata, j, of);
        } else if (messageState == MessageState.NOOP) {
            String str = TAG;
            Log.d(str, "Nothing to do: " + messageState.name());
        } else {
            warn("Bad state! messageState: " + messageState);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:105:0x02f7 A[Catch: StorageFailedException -> 0x0695, BadGroupIdException -> 0x0685, TryCatch #2 {BadGroupIdException -> 0x0685, StorageFailedException -> 0x0695, blocks: (B:3:0x0008, B:5:0x0012, B:7:0x001c, B:9:0x005b, B:11:0x0074, B:13:0x007e, B:15:0x0088, B:17:0x0092, B:19:0x009c, B:24:0x00aa, B:26:0x00b8, B:32:0x00ce, B:35:0x00ff, B:37:0x0105, B:38:0x0125, B:40:0x0131, B:43:0x013d, B:45:0x0143, B:46:0x015c, B:48:0x0162, B:49:0x0177, B:51:0x0183, B:53:0x018d, B:54:0x0194, B:56:0x01a0, B:59:0x01a9, B:61:0x01b3, B:62:0x01b8, B:64:0x01c2, B:65:0x01c6, B:67:0x01d0, B:68:0x01dd, B:70:0x01e7, B:73:0x01ff, B:74:0x0210, B:76:0x021a, B:77:0x022c, B:79:0x0233, B:81:0x023d, B:82:0x0242, B:84:0x0248, B:86:0x0256, B:87:0x0263, B:89:0x026d, B:90:0x027a, B:93:0x0282, B:94:0x0287, B:96:0x028d, B:98:0x0295, B:100:0x02ba, B:102:0x02c6, B:103:0x02f0, B:105:0x02f7, B:107:0x030b, B:108:0x0321, B:110:0x032f, B:112:0x0348, B:113:0x0357, B:115:0x0361, B:116:0x0374, B:118:0x037e, B:119:0x0398, B:121:0x03a2, B:122:0x03b5, B:124:0x03bf, B:125:0x03d2, B:127:0x03dc, B:128:0x03eb, B:130:0x03f5, B:131:0x0408, B:133:0x0412, B:134:0x0425, B:136:0x042f, B:137:0x043e, B:139:0x0448, B:140:0x045b, B:142:0x0465, B:143:0x0478, B:145:0x0482, B:146:0x0491, B:148:0x049b, B:149:0x04ae, B:151:0x04b8, B:152:0x04cb, B:153:0x04da, B:155:0x04e4, B:157:0x0505, B:159:0x0519, B:161:0x0544, B:163:0x054e, B:164:0x055d, B:166:0x0567, B:167:0x0576, B:169:0x0580, B:170:0x058f, B:172:0x0599, B:173:0x05a8, B:175:0x05b2, B:176:0x05c1, B:178:0x05cb, B:179:0x05da, B:181:0x05e4, B:183:0x05f4, B:184:0x05f9, B:186:0x05ff, B:187:0x0603, B:189:0x0609, B:190:0x060d, B:192:0x0617, B:193:0x0625, B:195:0x062f, B:196:0x063d, B:198:0x0647, B:199:0x0655, B:202:0x0660, B:203:0x066d, B:205:0x0674), top: B:211:0x0008 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0105 A[Catch: StorageFailedException -> 0x0695, BadGroupIdException -> 0x0685, TryCatch #2 {BadGroupIdException -> 0x0685, StorageFailedException -> 0x0695, blocks: (B:3:0x0008, B:5:0x0012, B:7:0x001c, B:9:0x005b, B:11:0x0074, B:13:0x007e, B:15:0x0088, B:17:0x0092, B:19:0x009c, B:24:0x00aa, B:26:0x00b8, B:32:0x00ce, B:35:0x00ff, B:37:0x0105, B:38:0x0125, B:40:0x0131, B:43:0x013d, B:45:0x0143, B:46:0x015c, B:48:0x0162, B:49:0x0177, B:51:0x0183, B:53:0x018d, B:54:0x0194, B:56:0x01a0, B:59:0x01a9, B:61:0x01b3, B:62:0x01b8, B:64:0x01c2, B:65:0x01c6, B:67:0x01d0, B:68:0x01dd, B:70:0x01e7, B:73:0x01ff, B:74:0x0210, B:76:0x021a, B:77:0x022c, B:79:0x0233, B:81:0x023d, B:82:0x0242, B:84:0x0248, B:86:0x0256, B:87:0x0263, B:89:0x026d, B:90:0x027a, B:93:0x0282, B:94:0x0287, B:96:0x028d, B:98:0x0295, B:100:0x02ba, B:102:0x02c6, B:103:0x02f0, B:105:0x02f7, B:107:0x030b, B:108:0x0321, B:110:0x032f, B:112:0x0348, B:113:0x0357, B:115:0x0361, B:116:0x0374, B:118:0x037e, B:119:0x0398, B:121:0x03a2, B:122:0x03b5, B:124:0x03bf, B:125:0x03d2, B:127:0x03dc, B:128:0x03eb, B:130:0x03f5, B:131:0x0408, B:133:0x0412, B:134:0x0425, B:136:0x042f, B:137:0x043e, B:139:0x0448, B:140:0x045b, B:142:0x0465, B:143:0x0478, B:145:0x0482, B:146:0x0491, B:148:0x049b, B:149:0x04ae, B:151:0x04b8, B:152:0x04cb, B:153:0x04da, B:155:0x04e4, B:157:0x0505, B:159:0x0519, B:161:0x0544, B:163:0x054e, B:164:0x055d, B:166:0x0567, B:167:0x0576, B:169:0x0580, B:170:0x058f, B:172:0x0599, B:173:0x05a8, B:175:0x05b2, B:176:0x05c1, B:178:0x05cb, B:179:0x05da, B:181:0x05e4, B:183:0x05f4, B:184:0x05f9, B:186:0x05ff, B:187:0x0603, B:189:0x0609, B:190:0x060d, B:192:0x0617, B:193:0x0625, B:195:0x062f, B:196:0x063d, B:198:0x0647, B:199:0x0655, B:202:0x0660, B:203:0x066d, B:205:0x0674), top: B:211:0x0008 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0125 A[Catch: StorageFailedException -> 0x0695, BadGroupIdException -> 0x0685, TryCatch #2 {BadGroupIdException -> 0x0685, StorageFailedException -> 0x0695, blocks: (B:3:0x0008, B:5:0x0012, B:7:0x001c, B:9:0x005b, B:11:0x0074, B:13:0x007e, B:15:0x0088, B:17:0x0092, B:19:0x009c, B:24:0x00aa, B:26:0x00b8, B:32:0x00ce, B:35:0x00ff, B:37:0x0105, B:38:0x0125, B:40:0x0131, B:43:0x013d, B:45:0x0143, B:46:0x015c, B:48:0x0162, B:49:0x0177, B:51:0x0183, B:53:0x018d, B:54:0x0194, B:56:0x01a0, B:59:0x01a9, B:61:0x01b3, B:62:0x01b8, B:64:0x01c2, B:65:0x01c6, B:67:0x01d0, B:68:0x01dd, B:70:0x01e7, B:73:0x01ff, B:74:0x0210, B:76:0x021a, B:77:0x022c, B:79:0x0233, B:81:0x023d, B:82:0x0242, B:84:0x0248, B:86:0x0256, B:87:0x0263, B:89:0x026d, B:90:0x027a, B:93:0x0282, B:94:0x0287, B:96:0x028d, B:98:0x0295, B:100:0x02ba, B:102:0x02c6, B:103:0x02f0, B:105:0x02f7, B:107:0x030b, B:108:0x0321, B:110:0x032f, B:112:0x0348, B:113:0x0357, B:115:0x0361, B:116:0x0374, B:118:0x037e, B:119:0x0398, B:121:0x03a2, B:122:0x03b5, B:124:0x03bf, B:125:0x03d2, B:127:0x03dc, B:128:0x03eb, B:130:0x03f5, B:131:0x0408, B:133:0x0412, B:134:0x0425, B:136:0x042f, B:137:0x043e, B:139:0x0448, B:140:0x045b, B:142:0x0465, B:143:0x0478, B:145:0x0482, B:146:0x0491, B:148:0x049b, B:149:0x04ae, B:151:0x04b8, B:152:0x04cb, B:153:0x04da, B:155:0x04e4, B:157:0x0505, B:159:0x0519, B:161:0x0544, B:163:0x054e, B:164:0x055d, B:166:0x0567, B:167:0x0576, B:169:0x0580, B:170:0x058f, B:172:0x0599, B:173:0x05a8, B:175:0x05b2, B:176:0x05c1, B:178:0x05cb, B:179:0x05da, B:181:0x05e4, B:183:0x05f4, B:184:0x05f9, B:186:0x05ff, B:187:0x0603, B:189:0x0609, B:190:0x060d, B:192:0x0617, B:193:0x0625, B:195:0x062f, B:196:0x063d, B:198:0x0647, B:199:0x0655, B:202:0x0660, B:203:0x066d, B:205:0x0674), top: B:211:0x0008 }] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0248 A[Catch: StorageFailedException -> 0x0695, BadGroupIdException -> 0x0685, TryCatch #2 {BadGroupIdException -> 0x0685, StorageFailedException -> 0x0695, blocks: (B:3:0x0008, B:5:0x0012, B:7:0x001c, B:9:0x005b, B:11:0x0074, B:13:0x007e, B:15:0x0088, B:17:0x0092, B:19:0x009c, B:24:0x00aa, B:26:0x00b8, B:32:0x00ce, B:35:0x00ff, B:37:0x0105, B:38:0x0125, B:40:0x0131, B:43:0x013d, B:45:0x0143, B:46:0x015c, B:48:0x0162, B:49:0x0177, B:51:0x0183, B:53:0x018d, B:54:0x0194, B:56:0x01a0, B:59:0x01a9, B:61:0x01b3, B:62:0x01b8, B:64:0x01c2, B:65:0x01c6, B:67:0x01d0, B:68:0x01dd, B:70:0x01e7, B:73:0x01ff, B:74:0x0210, B:76:0x021a, B:77:0x022c, B:79:0x0233, B:81:0x023d, B:82:0x0242, B:84:0x0248, B:86:0x0256, B:87:0x0263, B:89:0x026d, B:90:0x027a, B:93:0x0282, B:94:0x0287, B:96:0x028d, B:98:0x0295, B:100:0x02ba, B:102:0x02c6, B:103:0x02f0, B:105:0x02f7, B:107:0x030b, B:108:0x0321, B:110:0x032f, B:112:0x0348, B:113:0x0357, B:115:0x0361, B:116:0x0374, B:118:0x037e, B:119:0x0398, B:121:0x03a2, B:122:0x03b5, B:124:0x03bf, B:125:0x03d2, B:127:0x03dc, B:128:0x03eb, B:130:0x03f5, B:131:0x0408, B:133:0x0412, B:134:0x0425, B:136:0x042f, B:137:0x043e, B:139:0x0448, B:140:0x045b, B:142:0x0465, B:143:0x0478, B:145:0x0482, B:146:0x0491, B:148:0x049b, B:149:0x04ae, B:151:0x04b8, B:152:0x04cb, B:153:0x04da, B:155:0x04e4, B:157:0x0505, B:159:0x0519, B:161:0x0544, B:163:0x054e, B:164:0x055d, B:166:0x0567, B:167:0x0576, B:169:0x0580, B:170:0x058f, B:172:0x0599, B:173:0x05a8, B:175:0x05b2, B:176:0x05c1, B:178:0x05cb, B:179:0x05da, B:181:0x05e4, B:183:0x05f4, B:184:0x05f9, B:186:0x05ff, B:187:0x0603, B:189:0x0609, B:190:0x060d, B:192:0x0617, B:193:0x0625, B:195:0x062f, B:196:0x063d, B:198:0x0647, B:199:0x0655, B:202:0x0660, B:203:0x066d, B:205:0x0674), top: B:211:0x0008 }] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x026d A[Catch: StorageFailedException -> 0x0695, BadGroupIdException -> 0x0685, TryCatch #2 {BadGroupIdException -> 0x0685, StorageFailedException -> 0x0695, blocks: (B:3:0x0008, B:5:0x0012, B:7:0x001c, B:9:0x005b, B:11:0x0074, B:13:0x007e, B:15:0x0088, B:17:0x0092, B:19:0x009c, B:24:0x00aa, B:26:0x00b8, B:32:0x00ce, B:35:0x00ff, B:37:0x0105, B:38:0x0125, B:40:0x0131, B:43:0x013d, B:45:0x0143, B:46:0x015c, B:48:0x0162, B:49:0x0177, B:51:0x0183, B:53:0x018d, B:54:0x0194, B:56:0x01a0, B:59:0x01a9, B:61:0x01b3, B:62:0x01b8, B:64:0x01c2, B:65:0x01c6, B:67:0x01d0, B:68:0x01dd, B:70:0x01e7, B:73:0x01ff, B:74:0x0210, B:76:0x021a, B:77:0x022c, B:79:0x0233, B:81:0x023d, B:82:0x0242, B:84:0x0248, B:86:0x0256, B:87:0x0263, B:89:0x026d, B:90:0x027a, B:93:0x0282, B:94:0x0287, B:96:0x028d, B:98:0x0295, B:100:0x02ba, B:102:0x02c6, B:103:0x02f0, B:105:0x02f7, B:107:0x030b, B:108:0x0321, B:110:0x032f, B:112:0x0348, B:113:0x0357, B:115:0x0361, B:116:0x0374, B:118:0x037e, B:119:0x0398, B:121:0x03a2, B:122:0x03b5, B:124:0x03bf, B:125:0x03d2, B:127:0x03dc, B:128:0x03eb, B:130:0x03f5, B:131:0x0408, B:133:0x0412, B:134:0x0425, B:136:0x042f, B:137:0x043e, B:139:0x0448, B:140:0x045b, B:142:0x0465, B:143:0x0478, B:145:0x0482, B:146:0x0491, B:148:0x049b, B:149:0x04ae, B:151:0x04b8, B:152:0x04cb, B:153:0x04da, B:155:0x04e4, B:157:0x0505, B:159:0x0519, B:161:0x0544, B:163:0x054e, B:164:0x055d, B:166:0x0567, B:167:0x0576, B:169:0x0580, B:170:0x058f, B:172:0x0599, B:173:0x05a8, B:175:0x05b2, B:176:0x05c1, B:178:0x05cb, B:179:0x05da, B:181:0x05e4, B:183:0x05f4, B:184:0x05f9, B:186:0x05ff, B:187:0x0603, B:189:0x0609, B:190:0x060d, B:192:0x0617, B:193:0x0625, B:195:0x062f, B:196:0x063d, B:198:0x0647, B:199:0x0655, B:202:0x0660, B:203:0x066d, B:205:0x0674), top: B:211:0x0008 }] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0280 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x028d A[Catch: StorageFailedException -> 0x0695, BadGroupIdException -> 0x0685, TryCatch #2 {BadGroupIdException -> 0x0685, StorageFailedException -> 0x0695, blocks: (B:3:0x0008, B:5:0x0012, B:7:0x001c, B:9:0x005b, B:11:0x0074, B:13:0x007e, B:15:0x0088, B:17:0x0092, B:19:0x009c, B:24:0x00aa, B:26:0x00b8, B:32:0x00ce, B:35:0x00ff, B:37:0x0105, B:38:0x0125, B:40:0x0131, B:43:0x013d, B:45:0x0143, B:46:0x015c, B:48:0x0162, B:49:0x0177, B:51:0x0183, B:53:0x018d, B:54:0x0194, B:56:0x01a0, B:59:0x01a9, B:61:0x01b3, B:62:0x01b8, B:64:0x01c2, B:65:0x01c6, B:67:0x01d0, B:68:0x01dd, B:70:0x01e7, B:73:0x01ff, B:74:0x0210, B:76:0x021a, B:77:0x022c, B:79:0x0233, B:81:0x023d, B:82:0x0242, B:84:0x0248, B:86:0x0256, B:87:0x0263, B:89:0x026d, B:90:0x027a, B:93:0x0282, B:94:0x0287, B:96:0x028d, B:98:0x0295, B:100:0x02ba, B:102:0x02c6, B:103:0x02f0, B:105:0x02f7, B:107:0x030b, B:108:0x0321, B:110:0x032f, B:112:0x0348, B:113:0x0357, B:115:0x0361, B:116:0x0374, B:118:0x037e, B:119:0x0398, B:121:0x03a2, B:122:0x03b5, B:124:0x03bf, B:125:0x03d2, B:127:0x03dc, B:128:0x03eb, B:130:0x03f5, B:131:0x0408, B:133:0x0412, B:134:0x0425, B:136:0x042f, B:137:0x043e, B:139:0x0448, B:140:0x045b, B:142:0x0465, B:143:0x0478, B:145:0x0482, B:146:0x0491, B:148:0x049b, B:149:0x04ae, B:151:0x04b8, B:152:0x04cb, B:153:0x04da, B:155:0x04e4, B:157:0x0505, B:159:0x0519, B:161:0x0544, B:163:0x054e, B:164:0x055d, B:166:0x0567, B:167:0x0576, B:169:0x0580, B:170:0x058f, B:172:0x0599, B:173:0x05a8, B:175:0x05b2, B:176:0x05c1, B:178:0x05cb, B:179:0x05da, B:181:0x05e4, B:183:0x05f4, B:184:0x05f9, B:186:0x05ff, B:187:0x0603, B:189:0x0609, B:190:0x060d, B:192:0x0617, B:193:0x0625, B:195:0x062f, B:196:0x063d, B:198:0x0647, B:199:0x0655, B:202:0x0660, B:203:0x066d, B:205:0x0674), top: B:211:0x0008 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void handleMessage(org.whispersystems.signalservice.api.messages.SignalServiceContent r23, long r24, org.thoughtcrime.securesms.recipients.Recipient r26, j$.util.Optional<java.lang.Long> r27) throws java.io.IOException, org.thoughtcrime.securesms.groups.GroupChangeBusyException {
        /*
        // Method dump skipped, instructions count: 1716
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.messages.MessageContentProcessor.handleMessage(org.whispersystems.signalservice.api.messages.SignalServiceContent, long, org.thoughtcrime.securesms.recipients.Recipient, j$.util.Optional):void");
    }

    private long handlePendingRetry(PendingRetryReceiptModel pendingRetryReceiptModel, SignalServiceContent signalServiceContent, Recipient recipient) throws BadGroupIdException {
        long currentTimeMillis = System.currentTimeMillis();
        if (pendingRetryReceiptModel == null) {
            return currentTimeMillis;
        }
        warn(signalServiceContent.getTimestamp(), "Incoming message matches a pending retry we were expecting.");
        Long threadIdFor = SignalDatabase.threads().getThreadIdFor(recipient.getId());
        if (threadIdFor != null) {
            ThreadDatabase.ConversationMetadata conversationMetadata = SignalDatabase.threads().getConversationMetadata(threadIdFor.longValue());
            long longValue = ((Long) ApplicationDependencies.getMessageNotifier().getVisibleThread().map(new MessageContentProcessor$$ExternalSyntheticLambda22()).orElse(-1L)).longValue();
            if (threadIdFor.longValue() == longValue || conversationMetadata.getLastSeen() <= 0 || conversationMetadata.getLastSeen() >= pendingRetryReceiptModel.getReceivedTimestamp()) {
                long timestamp = signalServiceContent.getTimestamp();
                StringBuilder sb = new StringBuilder();
                sb.append("Thread was opened after receiving the original message. Using the current time for received time. (Last seen: ");
                sb.append(conversationMetadata.getLastSeen());
                sb.append(", ThreadVisible: ");
                sb.append(threadIdFor.longValue() == longValue);
                sb.append(")");
                warn(timestamp, sb.toString());
                return currentTimeMillis;
            }
            long receivedTimestamp = pendingRetryReceiptModel.getReceivedTimestamp();
            long timestamp2 = signalServiceContent.getTimestamp();
            warn(timestamp2, "Thread has not been opened yet. Using received timestamp of " + receivedTimestamp);
            return receivedTimestamp;
        }
        warn(signalServiceContent.getTimestamp(), "Could not find a thread for the pending message. Using current time for received time.");
        return currentTimeMillis;
    }

    private void handlePayment(SignalServiceContent signalServiceContent, SignalServiceDataMessage signalServiceDataMessage, Recipient recipient) {
        log(signalServiceContent.getTimestamp(), "Payment message.");
        if (!signalServiceDataMessage.getPayment().isPresent()) {
            throw new AssertionError();
        } else if (!signalServiceDataMessage.getPayment().get().getPaymentNotification().isPresent()) {
            warn(signalServiceContent.getTimestamp(), "Ignoring payment message without notification");
        } else {
            SignalServiceDataMessage.PaymentNotification paymentNotification = signalServiceDataMessage.getPayment().get().getPaymentNotification().get();
            PaymentDatabase payments = SignalDatabase.payments();
            UUID randomUUID = UUID.randomUUID();
            String str = "Payment_" + PushProcessMessageJob.getQueueName(recipient.getId());
            try {
                RecipientId id = recipient.getId();
                long timestamp = signalServiceDataMessage.getTimestamp();
                String note = paymentNotification.getNote();
                Money.MobileCoin mobileCoin = Money.MobileCoin.ZERO;
                payments.createIncomingPayment(randomUUID, id, timestamp, note, mobileCoin, mobileCoin, paymentNotification.getReceipt());
            } catch (SerializationException e) {
                warn(signalServiceContent.getTimestamp(), "Ignoring payment with bad data.", e);
            } catch (PaymentDatabase.PublicKeyConflictException unused) {
                warn(signalServiceContent.getTimestamp(), "Ignoring payment with public key already in database");
                return;
            }
            ApplicationDependencies.getJobManager().startChain(new PaymentTransactionCheckJob(randomUUID, str)).then(PaymentLedgerUpdateJob.updateLedger()).enqueue();
        }
    }

    private boolean handleGv2PreProcessing(GroupId.V2 v2, SignalServiceContent signalServiceContent, SignalServiceGroupV2 signalServiceGroupV2, Recipient recipient) throws IOException, GroupChangeBusyException {
        GroupDatabase groups = SignalDatabase.groups();
        Optional<GroupDatabase.GroupRecord> groupV1ByExpectedV2 = groups.getGroupV1ByExpectedV2(v2);
        if (groupV1ByExpectedV2.isPresent()) {
            GroupsV1MigrationUtil.performLocalMigration(this.context, groupV1ByExpectedV2.get().getId().requireV1());
        }
        if (!updateGv2GroupFromServerOrP2PChange(signalServiceContent, signalServiceGroupV2)) {
            String valueOf = String.valueOf(signalServiceContent.getTimestamp());
            log(valueOf, "Ignoring GV2 message for group we are not currently in " + v2);
            return true;
        }
        Optional<GroupDatabase.GroupRecord> group = groups.getGroup(v2);
        if (group.isPresent() && !group.get().getMembers().contains(recipient.getId())) {
            String valueOf2 = String.valueOf(signalServiceContent.getTimestamp());
            log(valueOf2, "Ignoring GV2 message from member not in group " + v2 + ". Sender: " + recipient.getId() + " | " + recipient.requireServiceId());
            return true;
        } else if (!group.isPresent() || !group.get().isAnnouncementGroup() || group.get().getAdmins().contains(recipient)) {
            return false;
        } else {
            if (signalServiceContent.getDataMessage().isPresent()) {
                SignalServiceDataMessage signalServiceDataMessage = signalServiceContent.getDataMessage().get();
                if (!signalServiceDataMessage.getBody().isPresent() && !signalServiceDataMessage.getAttachments().isPresent() && !signalServiceDataMessage.getQuote().isPresent() && !signalServiceDataMessage.getPreviews().isPresent() && !signalServiceDataMessage.getMentions().isPresent() && !signalServiceDataMessage.getSticker().isPresent()) {
                    return false;
                }
                String str = TAG;
                Log.w(str, "Ignoring message from " + recipient.getId() + " because it has disallowed content, and they're not an admin in an announcement-only group.");
                return true;
            } else if (!signalServiceContent.getTypingMessage().isPresent()) {
                return false;
            } else {
                String str2 = TAG;
                Log.w(str2, "Ignoring typing indicator from " + recipient.getId() + " because they're not an admin in an announcement-only group.");
                return true;
            }
        }
    }

    private static SignalServiceGroupContext getGroupContextIfPresent(SignalServiceContent signalServiceContent) {
        if (signalServiceContent.getDataMessage().isPresent() && signalServiceContent.getDataMessage().get().getGroupContext().isPresent()) {
            return signalServiceContent.getDataMessage().get().getGroupContext().get();
        }
        if (!signalServiceContent.getSyncMessage().isPresent() || !signalServiceContent.getSyncMessage().get().getSent().isPresent() || !signalServiceContent.getSyncMessage().get().getSent().get().getDataMessage().get().getGroupContext().isPresent()) {
            return null;
        }
        return signalServiceContent.getSyncMessage().get().getSent().get().getDataMessage().get().getGroupContext().get();
    }

    private boolean updateGv2GroupFromServerOrP2PChange(SignalServiceContent signalServiceContent, SignalServiceGroupV2 signalServiceGroupV2) throws IOException, GroupChangeBusyException {
        try {
            GroupManager.updateGroupFromServer(this.context, signalServiceGroupV2.getMasterKey(), signalServiceGroupV2.getRevision(), signalServiceGroupV2.getSignedGroupChange() != null ? signalServiceContent.getTimestamp() : signalServiceContent.getTimestamp() - 1, signalServiceGroupV2.getSignedGroupChange());
            return true;
        } catch (GroupNotAMemberException unused) {
            warn(String.valueOf(signalServiceContent.getTimestamp()), "Ignoring message for a group we're not in");
            return false;
        }
    }

    private void handleExceptionMessage(MessageState messageState, ExceptionMetadata exceptionMetadata, long j, Optional<Long> optional) {
        Recipient external = Recipient.external(this.context, exceptionMetadata.sender);
        if (external.isBlocked()) {
            warn("Ignoring exception content from blocked sender, message state:" + messageState);
            return;
        }
        switch (AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$messages$MessageContentProcessor$MessageState[messageState.ordinal()]) {
            case 1:
                warn(String.valueOf(j), "Handling invalid version.");
                handleInvalidVersionMessage(exceptionMetadata.sender, exceptionMetadata.senderDevice, j, optional);
                return;
            case 2:
                warn(String.valueOf(j), "Handling legacy message.");
                handleLegacyMessage(exceptionMetadata.sender, exceptionMetadata.senderDevice, j, optional);
                return;
            case 3:
                warn(String.valueOf(j), "Duplicate message. Dropping.");
                return;
            case 4:
                warn(String.valueOf(j), "Handling unsupported data message.");
                handleUnsupportedDataMessage(exceptionMetadata.sender, exceptionMetadata.senderDevice, Optional.ofNullable(exceptionMetadata.groupId), j, optional);
                return;
            case 5:
            case 6:
                warn(String.valueOf(j), "Discovered old enqueued bad encrypted message. Scheduling reset.");
                ApplicationDependencies.getJobManager().add(new AutomaticSessionResetJob(external.getId(), exceptionMetadata.senderDevice, j));
                return;
            default:
                throw new AssertionError("Not handled " + messageState + ". (" + j + ")");
        }
    }

    private void handleCallOfferMessage(SignalServiceContent signalServiceContent, OfferMessage offerMessage, Optional<Long> optional, Recipient recipient) {
        log(String.valueOf(signalServiceContent.getTimestamp()), "handleCallOfferMessage...");
        if (optional.isPresent()) {
            SignalDatabase.sms().markAsMissedCall(optional.get().longValue(), offerMessage.getType() == OfferMessage.Type.VIDEO_CALL);
        } else {
            ApplicationDependencies.getSignalCallManager().receivedOffer(new WebRtcData.CallMetadata(new RemotePeer(recipient.getId(), new CallId(Long.valueOf(offerMessage.getId()))), signalServiceContent.getSenderDevice()), new WebRtcData.OfferMetadata(offerMessage.getOpaque(), offerMessage.getSdp(), offerMessage.getType()), new WebRtcData.ReceivedOfferMetadata((byte[]) ApplicationDependencies.getProtocolStore().aci().identities().getIdentityRecord(recipient.getId()).map(new Function() { // from class: org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda16
                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return MessageContentProcessor.lambda$handleCallOfferMessage$0((IdentityRecord) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).orElse(null), signalServiceContent.getServerReceivedTimestamp(), signalServiceContent.getServerDeliveredTimestamp(), signalServiceContent.getCallMessage().get().isMultiRing()));
        }
    }

    public static /* synthetic */ byte[] lambda$handleCallOfferMessage$0(IdentityRecord identityRecord) {
        return identityRecord.getIdentityKey().serialize();
    }

    private void handleCallAnswerMessage(SignalServiceContent signalServiceContent, AnswerMessage answerMessage, Recipient recipient) {
        log(String.valueOf(signalServiceContent), "handleCallAnswerMessage...");
        ApplicationDependencies.getSignalCallManager().receivedAnswer(new WebRtcData.CallMetadata(new RemotePeer(recipient.getId(), new CallId(Long.valueOf(answerMessage.getId()))), signalServiceContent.getSenderDevice()), new WebRtcData.AnswerMetadata(answerMessage.getOpaque(), answerMessage.getSdp()), new WebRtcData.ReceivedAnswerMetadata((byte[]) ApplicationDependencies.getProtocolStore().aci().identities().getIdentityRecord(recipient.getId()).map(new Function() { // from class: org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda8
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return MessageContentProcessor.lambda$handleCallAnswerMessage$1((IdentityRecord) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(null), signalServiceContent.getCallMessage().get().isMultiRing()));
    }

    public static /* synthetic */ byte[] lambda$handleCallAnswerMessage$1(IdentityRecord identityRecord) {
        return identityRecord.getIdentityKey().serialize();
    }

    private void handleCallIceUpdateMessage(SignalServiceContent signalServiceContent, List<IceUpdateMessage> list, Recipient recipient) {
        String valueOf = String.valueOf(signalServiceContent);
        log(valueOf, "handleCallIceUpdateMessage... " + list.size());
        ArrayList arrayList = new ArrayList(list.size());
        long j = -1;
        for (IceUpdateMessage iceUpdateMessage : list) {
            arrayList.add(iceUpdateMessage.getOpaque());
            j = iceUpdateMessage.getId();
        }
        ApplicationDependencies.getSignalCallManager().receivedIceCandidates(new WebRtcData.CallMetadata(new RemotePeer(recipient.getId(), new CallId(Long.valueOf(j))), signalServiceContent.getSenderDevice()), arrayList);
    }

    private void handleCallHangupMessage(SignalServiceContent signalServiceContent, HangupMessage hangupMessage, Optional<Long> optional, Recipient recipient) {
        log(String.valueOf(signalServiceContent), "handleCallHangupMessage");
        if (optional.isPresent()) {
            SignalDatabase.sms().markAsMissedCall(optional.get().longValue(), false);
            return;
        }
        ApplicationDependencies.getSignalCallManager().receivedCallHangup(new WebRtcData.CallMetadata(new RemotePeer(recipient.getId(), new CallId(Long.valueOf(hangupMessage.getId()))), signalServiceContent.getSenderDevice()), new WebRtcData.HangupMetadata(hangupMessage.getType(), hangupMessage.isLegacy(), hangupMessage.getDeviceId()));
    }

    private void handleCallBusyMessage(SignalServiceContent signalServiceContent, BusyMessage busyMessage, Recipient recipient) {
        log(String.valueOf(signalServiceContent.getTimestamp()), "handleCallBusyMessage");
        ApplicationDependencies.getSignalCallManager().receivedCallBusy(new WebRtcData.CallMetadata(new RemotePeer(recipient.getId(), new CallId(Long.valueOf(busyMessage.getId()))), signalServiceContent.getSenderDevice()));
    }

    private void handleCallOpaqueMessage(SignalServiceContent signalServiceContent, OpaqueMessage opaqueMessage, Recipient recipient) {
        log(String.valueOf(signalServiceContent.getTimestamp()), "handleCallOpaqueMessage");
        long j = 0;
        if (signalServiceContent.getServerReceivedTimestamp() > 0 && signalServiceContent.getServerDeliveredTimestamp() >= signalServiceContent.getServerReceivedTimestamp()) {
            j = (signalServiceContent.getServerDeliveredTimestamp() - signalServiceContent.getServerReceivedTimestamp()) / 1000;
        }
        ApplicationDependencies.getSignalCallManager().receivedOpaqueMessage(new WebRtcData.OpaqueMessageMetadata(recipient.requireServiceId().uuid(), opaqueMessage.getOpaque(), signalServiceContent.getSenderDevice(), j));
    }

    private void handleGroupCallUpdateMessage(SignalServiceContent signalServiceContent, SignalServiceDataMessage signalServiceDataMessage, Optional<GroupId> optional, Recipient recipient) {
        log(signalServiceContent.getTimestamp(), "Group call update message.");
        if (!optional.isPresent() || !optional.get().isV2()) {
            Log.w(TAG, "Invalid group for group call update message");
            return;
        }
        RecipientId orInsertFromPossiblyMigratedGroupId = SignalDatabase.recipients().getOrInsertFromPossiblyMigratedGroupId(optional.get());
        SignalDatabase.sms().insertOrUpdateGroupCall(orInsertFromPossiblyMigratedGroupId, recipient.getId(), signalServiceContent.getServerReceivedTimestamp(), signalServiceDataMessage.getGroupCallUpdate().get().getEraId());
        GroupCallPeekJob.enqueue(orInsertFromPossiblyMigratedGroupId);
    }

    private MessageId handleEndSessionMessage(SignalServiceContent signalServiceContent, Optional<Long> optional, Recipient recipient) {
        Optional<MessageDatabase.InsertResult> optional2;
        log(signalServiceContent.getTimestamp(), "End session message.");
        SmsDatabase sms = SignalDatabase.sms();
        IncomingTextMessage incomingTextMessage = new IncomingTextMessage(recipient.getId(), signalServiceContent.getSenderDevice(), signalServiceContent.getTimestamp(), signalServiceContent.getServerReceivedTimestamp(), System.currentTimeMillis(), "", Optional.empty(), 0, signalServiceContent.isNeedsReceipt(), signalServiceContent.getServerUuid());
        if (!optional.isPresent()) {
            optional2 = sms.insertMessageInbox(new IncomingEndSessionMessage(incomingTextMessage));
        } else {
            sms.markAsEndSession(optional.get().longValue());
            optional2 = Optional.of(new MessageDatabase.InsertResult(optional.get().longValue(), sms.getThreadIdForMessage(optional.get().longValue())));
        }
        if (!optional2.isPresent()) {
            return null;
        }
        ApplicationDependencies.getProtocolStore().aci().deleteAllSessions(signalServiceContent.getSender().getIdentifier());
        SecurityEvent.broadcastSecurityUpdateEvent(this.context);
        ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.forConversation(optional2.get().getThreadId()));
        return new MessageId(optional2.get().getMessageId(), true);
    }

    private long handleSynchronizeSentEndSessionMessage(SentTranscriptMessage sentTranscriptMessage, long j) throws BadGroupIdException {
        log(j, "Synchronize end session message.");
        SmsDatabase sms = SignalDatabase.sms();
        Recipient syncMessageDestination = getSyncMessageDestination(sentTranscriptMessage);
        OutgoingEndSessionMessage outgoingEndSessionMessage = new OutgoingEndSessionMessage(new OutgoingTextMessage(syncMessageDestination, "", -1));
        long orCreateThreadIdFor = SignalDatabase.threads().getOrCreateThreadIdFor(syncMessageDestination);
        if (!syncMessageDestination.isGroup()) {
            ApplicationDependencies.getProtocolStore().aci().deleteAllSessions(syncMessageDestination.requireServiceId().toString());
            SecurityEvent.broadcastSecurityUpdateEvent(this.context);
            sms.markAsSent(sms.insertMessageOutbox(orCreateThreadIdFor, (OutgoingTextMessage) outgoingEndSessionMessage, false, sentTranscriptMessage.getTimestamp(), (MessageDatabase.InsertListener) null), true);
            SignalDatabase.threads().update(orCreateThreadIdFor, true);
        }
        return orCreateThreadIdFor;
    }

    private void handleGroupV1Message(SignalServiceContent signalServiceContent, SignalServiceDataMessage signalServiceDataMessage, Optional<Long> optional, GroupId.V1 v1, Recipient recipient, Recipient recipient2, long j) throws StorageFailedException, BadGroupIdException {
        log(signalServiceContent.getTimestamp(), "GroupV1 message.");
        GroupV1MessageProcessor.process(this.context, signalServiceContent, signalServiceDataMessage, false);
        handlePossibleExpirationUpdate(signalServiceContent, signalServiceDataMessage, Optional.of(v1), recipient, recipient2, j);
        if (optional.isPresent()) {
            SignalDatabase.sms().deleteMessage(optional.get().longValue());
        }
    }

    private void handleUnknownGroupMessage(SignalServiceContent signalServiceContent, SignalServiceGroupContext signalServiceGroupContext, Recipient recipient) throws BadGroupIdException {
        log(signalServiceContent.getTimestamp(), "Unknown group message.");
        if (signalServiceGroupContext.getGroupV1().isPresent()) {
            SignalServiceGroup signalServiceGroup = signalServiceGroupContext.getGroupV1().get();
            if (signalServiceGroup.getType() != SignalServiceGroup.Type.REQUEST_INFO) {
                ApplicationDependencies.getJobManager().add(new RequestGroupInfoJob(recipient.getId(), GroupId.v1(signalServiceGroup.getGroupId())));
            } else {
                warn(signalServiceContent.getTimestamp(), "Received a REQUEST_INFO message for a group we don't know about. Ignoring.");
            }
        } else if (signalServiceGroupContext.getGroupV2().isPresent()) {
            warn(signalServiceContent.getTimestamp(), "Received a GV2 message for a group we have no knowledge of -- attempting to fix this state.");
            ServiceId parseOrNull = ServiceId.parseOrNull(signalServiceContent.getDestinationUuid());
            if (parseOrNull == null) {
                warn(signalServiceContent.getTimestamp(), "Group message missing destination uuid, defaulting to ACI");
                parseOrNull = SignalStore.account().requireAci();
            }
            SignalDatabase.groups().fixMissingMasterKey(parseOrNull, signalServiceGroupContext.getGroupV2().get().getMasterKey());
        } else {
            warn(signalServiceContent.getTimestamp(), "Received a message for a group we don't know about without a group context. Ignoring.");
        }
    }

    private void handlePossibleExpirationUpdate(SignalServiceContent signalServiceContent, SignalServiceDataMessage signalServiceDataMessage, Optional<GroupId> optional, Recipient recipient, Recipient recipient2, long j) throws StorageFailedException {
        if (signalServiceDataMessage.getExpiresInSeconds() != recipient2.getExpiresInSeconds()) {
            warn(signalServiceContent.getTimestamp(), "Message expire time didn't match thread expire time. Handling timer update.");
            handleExpirationUpdate(signalServiceContent, signalServiceDataMessage, Optional.empty(), optional, recipient, recipient2, j, true);
        }
    }

    private MessageId handleExpirationUpdate(SignalServiceContent signalServiceContent, SignalServiceDataMessage signalServiceDataMessage, Optional<Long> optional, Optional<GroupId> optional2, Recipient recipient, Recipient recipient2, long j, boolean z) throws StorageFailedException {
        log(signalServiceContent.getTimestamp(), "Expiration update.");
        if (!optional2.isPresent() || !optional2.get().isV2()) {
            int expiresInSeconds = signalServiceDataMessage.getExpiresInSeconds();
            Optional<SignalServiceGroupContext> groupContext = signalServiceDataMessage.getGroupContext();
            if (recipient2.getExpiresInSeconds() == expiresInSeconds) {
                log(String.valueOf(signalServiceContent.getTimestamp()), "No change in message expiry for group. Ignoring.");
                return null;
            }
            try {
                Optional<MessageDatabase.InsertResult> insertSecureDecryptedMessageInbox = SignalDatabase.mms().insertSecureDecryptedMessageInbox(new IncomingMediaMessage(recipient.getId(), signalServiceContent.getTimestamp() - ((long) (z ? 1 : 0)), signalServiceContent.getServerReceivedTimestamp(), j, StoryType.NONE, null, false, -1, 1000 * ((long) expiresInSeconds), true, false, signalServiceContent.isNeedsReceipt(), Optional.empty(), groupContext, Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), signalServiceContent.getServerUuid(), null), -1);
                SignalDatabase.recipients().setExpireMessages(recipient2.getId(), expiresInSeconds);
                if (optional.isPresent()) {
                    SignalDatabase.sms().deleteMessage(optional.get().longValue());
                }
                if (insertSecureDecryptedMessageInbox.isPresent()) {
                    return new MessageId(insertSecureDecryptedMessageInbox.get().getMessageId(), true);
                }
                return null;
            } catch (MmsException e) {
                throw new StorageFailedException(e, signalServiceContent.getSender().getIdentifier(), signalServiceContent.getSenderDevice(), null);
            }
        } else {
            warn(String.valueOf(signalServiceContent.getTimestamp()), "Expiration update received for GV2. Ignoring.");
            return null;
        }
    }

    private MessageId handleReaction(SignalServiceContent signalServiceContent, SignalServiceDataMessage signalServiceDataMessage, Recipient recipient) throws StorageFailedException {
        long timestamp = signalServiceContent.getTimestamp();
        log(timestamp, "Handle reaction for message " + signalServiceDataMessage.getReaction().get().getTargetSentTimestamp());
        SignalServiceDataMessage.Reaction reaction = signalServiceDataMessage.getReaction().get();
        if (!EmojiUtil.isEmoji(reaction.getEmoji())) {
            Log.w(TAG, "Reaction text is not a valid emoji! Ignoring the message.");
            return null;
        }
        Recipient externalPush = Recipient.externalPush(reaction.getTargetAuthor());
        MessageRecord messageFor = SignalDatabase.mmsSms().getMessageFor(reaction.getTargetSentTimestamp(), externalPush.getId());
        if (messageFor == null) {
            String valueOf = String.valueOf(signalServiceContent.getTimestamp());
            warn(valueOf, "[handleReaction] Could not find matching message! Putting it in the early message cache. timestamp: " + reaction.getTargetSentTimestamp() + "  author: " + externalPush.getId());
            if (!this.processingEarlyContent) {
                ApplicationDependencies.getEarlyMessageCache().store(externalPush.getId(), reaction.getTargetSentTimestamp(), signalServiceContent);
                PushProcessEarlyMessagesJob.enqueue();
            }
            return null;
        } else if (messageFor.isRemoteDelete()) {
            String valueOf2 = String.valueOf(signalServiceContent.getTimestamp());
            warn(valueOf2, "[handleReaction] Found a matching message, but it's flagged as remotely deleted. timestamp: " + reaction.getTargetSentTimestamp() + "  author: " + externalPush.getId());
            return null;
        } else {
            ThreadRecord threadRecord = SignalDatabase.threads().getThreadRecord(Long.valueOf(messageFor.getThreadId()));
            if (threadRecord == null) {
                String valueOf3 = String.valueOf(signalServiceContent.getTimestamp());
                warn(valueOf3, "[handleReaction] Could not find a thread for the message! timestamp: " + reaction.getTargetSentTimestamp() + "  author: " + externalPush.getId());
                return null;
            }
            Recipient resolve = threadRecord.getRecipient().resolve();
            if (resolve.isGroup() && !resolve.getParticipantIds().contains(recipient.getId())) {
                String valueOf4 = String.valueOf(signalServiceContent.getTimestamp());
                warn(valueOf4, "[handleReaction] Reaction author is not in the group! timestamp: " + reaction.getTargetSentTimestamp() + "  author: " + externalPush.getId());
                return null;
            } else if (resolve.isGroup() || recipient.equals(resolve) || recipient.isSelf()) {
                MessageId messageId = new MessageId(messageFor.getId(), messageFor.isMms());
                if (reaction.isRemove()) {
                    SignalDatabase.reactions().deleteReaction(messageId, recipient.getId());
                    ApplicationDependencies.getMessageNotifier().updateNotification(this.context);
                } else {
                    SignalDatabase.reactions().addReaction(messageId, new ReactionRecord(reaction.getEmoji(), recipient.getId(), signalServiceDataMessage.getTimestamp(), System.currentTimeMillis()));
                    ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.fromMessageRecord(messageFor), false);
                }
                return new MessageId(messageFor.getId(), messageFor.isMms());
            } else {
                String valueOf5 = String.valueOf(signalServiceContent.getTimestamp());
                warn(valueOf5, "[handleReaction] Reaction author is not a part of the 1:1 thread! timestamp: " + reaction.getTargetSentTimestamp() + "  author: " + externalPush.getId());
                return null;
            }
        }
    }

    private MessageId handleRemoteDelete(SignalServiceContent signalServiceContent, SignalServiceDataMessage signalServiceDataMessage, Recipient recipient) {
        long timestamp = signalServiceContent.getTimestamp();
        log(timestamp, "Remote delete for message " + signalServiceDataMessage.getRemoteDelete().get().getTargetSentTimestamp());
        SignalServiceDataMessage.RemoteDelete remoteDelete = signalServiceDataMessage.getRemoteDelete().get();
        MessageRecord messageFor = SignalDatabase.mmsSms().getMessageFor(remoteDelete.getTargetSentTimestamp(), recipient.getId());
        if (messageFor != null && RemoteDeleteUtil.isValidReceive(messageFor, recipient, signalServiceContent.getServerReceivedTimestamp())) {
            (messageFor.isMms() ? SignalDatabase.mms() : SignalDatabase.sms()).markAsRemoteDelete(messageFor.getId());
            ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.fromMessageRecord(messageFor), false);
            return new MessageId(messageFor.getId(), messageFor.isMms());
        } else if (messageFor == null) {
            String valueOf = String.valueOf(signalServiceContent.getTimestamp());
            warn(valueOf, "[handleRemoteDelete] Could not find matching message! timestamp: " + remoteDelete.getTargetSentTimestamp() + "  author: " + recipient.getId());
            if (!this.processingEarlyContent) {
                ApplicationDependencies.getEarlyMessageCache().store(recipient.getId(), remoteDelete.getTargetSentTimestamp(), signalServiceContent);
                PushProcessEarlyMessagesJob.enqueue();
            }
            return null;
        } else {
            warn(String.valueOf(signalServiceContent.getTimestamp()), String.format(Locale.ENGLISH, "[handleRemoteDelete] Invalid remote delete! deleteTime: %d, targetTime: %d, deleteAuthor: %s, targetAuthor: %s", Long.valueOf(signalServiceContent.getServerReceivedTimestamp()), Long.valueOf(messageFor.getServerTimestamp()), recipient.getId(), messageFor.getRecipient().getId()));
            return null;
        }
    }

    private void handleSynchronizeVerifiedMessage(VerifiedMessage verifiedMessage) {
        log(verifiedMessage.getTimestamp(), "Synchronize verified message.");
        IdentityUtil.processVerifiedMessage(this.context, verifiedMessage);
    }

    private void handleSynchronizeStickerPackOperation(List<StickerPackOperationMessage> list, long j) {
        log(j, "Synchronize sticker pack operation.");
        JobManager jobManager = ApplicationDependencies.getJobManager();
        for (StickerPackOperationMessage stickerPackOperationMessage : list) {
            if (!stickerPackOperationMessage.getPackId().isPresent() || !stickerPackOperationMessage.getPackKey().isPresent() || !stickerPackOperationMessage.getType().isPresent()) {
                warn("Received incomplete sticker pack operation sync.");
            } else {
                String stringCondensed = Hex.toStringCondensed(stickerPackOperationMessage.getPackId().get());
                String stringCondensed2 = Hex.toStringCondensed(stickerPackOperationMessage.getPackKey().get());
                int i = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$StickerPackOperationMessage$Type[stickerPackOperationMessage.getType().get().ordinal()];
                if (i == 1) {
                    jobManager.add(StickerPackDownloadJob.forInstall(stringCondensed, stringCondensed2, false));
                } else if (i == 2) {
                    SignalDatabase.stickers().uninstallPack(stringCondensed);
                }
            }
        }
    }

    private void handleSynchronizeConfigurationMessage(ConfigurationMessage configurationMessage, long j) {
        log(j, "Synchronize configuration message.");
        if (configurationMessage.getReadReceipts().isPresent()) {
            TextSecurePreferences.setReadReceiptsEnabled(this.context, configurationMessage.getReadReceipts().get().booleanValue());
        }
        if (configurationMessage.getUnidentifiedDeliveryIndicators().isPresent()) {
            TextSecurePreferences.setShowUnidentifiedDeliveryIndicatorsEnabled(this.context, configurationMessage.getReadReceipts().get().booleanValue());
        }
        if (configurationMessage.getTypingIndicators().isPresent()) {
            TextSecurePreferences.setTypingIndicatorsEnabled(this.context, configurationMessage.getTypingIndicators().get().booleanValue());
        }
        if (configurationMessage.getLinkPreviews().isPresent()) {
            SignalStore.settings().setLinkPreviewsEnabled(configurationMessage.getReadReceipts().get().booleanValue());
        }
    }

    private void handleSynchronizeBlockedListMessage(BlockedListMessage blockedListMessage) {
        SignalDatabase.recipients().applyBlockedUpdate(blockedListMessage.getAddresses(), blockedListMessage.getGroupIds());
    }

    private void handleSynchronizeFetchMessage(SignalServiceSyncMessage.FetchType fetchType, long j) {
        log(j, "Received fetch request with type: " + fetchType);
        int i = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$SignalServiceSyncMessage$FetchType[fetchType.ordinal()];
        if (i == 1) {
            ApplicationDependencies.getJobManager().add(new RefreshOwnProfileJob());
        } else if (i == 2) {
            StorageSyncHelper.scheduleSyncForDataChange();
        } else if (i != 3) {
            warn(TAG, "Received a fetch message for an unknown type.");
        } else {
            warn(TAG, "Dropping subscription status fetch message.");
        }
    }

    private void handleSynchronizeMessageRequestResponse(MessageRequestResponseMessage messageRequestResponseMessage, long j) throws BadGroupIdException {
        Recipient recipient;
        log(j, "Synchronize message request response.");
        RecipientDatabase recipients = SignalDatabase.recipients();
        ThreadDatabase threads = SignalDatabase.threads();
        if (messageRequestResponseMessage.getPerson().isPresent()) {
            recipient = Recipient.externalPush(messageRequestResponseMessage.getPerson().get());
        } else if (messageRequestResponseMessage.getGroupId().isPresent()) {
            recipient = Recipient.externalPossiblyMigratedGroup(GroupId.push(messageRequestResponseMessage.getGroupId().get()));
        } else {
            warn("Message request response was missing a thread recipient! Skipping.");
            return;
        }
        long orCreateThreadIdFor = threads.getOrCreateThreadIdFor(recipient);
        int i = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$MessageRequestResponseMessage$Type[messageRequestResponseMessage.getType().ordinal()];
        if (i == 1) {
            recipients.setProfileSharing(recipient.getId(), true);
            recipients.setBlocked(recipient.getId(), false);
        } else if (i == 2) {
            recipients.setProfileSharing(recipient.getId(), false);
            if (orCreateThreadIdFor > 0) {
                threads.deleteConversation(orCreateThreadIdFor);
            }
        } else if (i == 3) {
            recipients.setBlocked(recipient.getId(), true);
            recipients.setProfileSharing(recipient.getId(), false);
        } else if (i != 4) {
            warn("Got an unknown response type! Skipping");
        } else {
            recipients.setBlocked(recipient.getId(), true);
            recipients.setProfileSharing(recipient.getId(), false);
            if (orCreateThreadIdFor > 0) {
                threads.deleteConversation(orCreateThreadIdFor);
            }
        }
    }

    private void handleSynchronizeOutgoingPayment(SignalServiceContent signalServiceContent, OutgoingPaymentMessage outgoingPaymentMessage) {
        RecipientId recipientId = (RecipientId) outgoingPaymentMessage.getRecipient().map(new ConversationListDataSource$$ExternalSyntheticLambda0()).orElse(null);
        long blockTimestamp = outgoingPaymentMessage.getBlockTimestamp();
        if (blockTimestamp == 0) {
            blockTimestamp = System.currentTimeMillis();
        }
        Optional map = outgoingPaymentMessage.getAddress().map(new Function() { // from class: org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda24
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return MobileCoinPublicAddress.fromBytes((byte[]) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        if (!map.isPresent() && recipientId == null) {
            log(signalServiceContent.getTimestamp(), "Inserting defrag");
            map = Optional.of(ApplicationDependencies.getPayments().getWallet().getMobileCoinPublicAddress());
            recipientId = Recipient.self().getId();
        }
        UUID randomUUID = UUID.randomUUID();
        try {
            SignalDatabase.payments().createSuccessfulPayment(randomUUID, recipientId, (MobileCoinPublicAddress) map.get(), blockTimestamp, outgoingPaymentMessage.getBlockIndex(), outgoingPaymentMessage.getNote().orElse(""), outgoingPaymentMessage.getAmount(), outgoingPaymentMessage.getFee(), outgoingPaymentMessage.getReceipt().toByteArray(), PaymentMetaDataUtil.fromKeysAndImages(outgoingPaymentMessage.getPublicKeys(), outgoingPaymentMessage.getKeyImages()));
        } catch (SerializationException e) {
            warn(signalServiceContent.getTimestamp(), "Ignoring synchronized outgoing payment with bad data.", e);
        }
        log("Inserted synchronized payment " + randomUUID);
    }

    private void handleSynchronizeKeys(KeysMessage keysMessage, long j) {
        if (SignalStore.account().isLinkedDevice()) {
            log(j, "Synchronize keys.");
            SignalStore.storageService().setStorageKeyFromPrimary(keysMessage.getStorageService().get());
            return;
        }
        log(j, "Primary device ignores synchronize keys.");
    }

    private void handleSynchronizeContacts(ContactsMessage contactsMessage, long j) throws IOException {
        if (SignalStore.account().isLinkedDevice()) {
            log(j, "Synchronize contacts.");
            if (!(contactsMessage.getContactsStream() instanceof SignalServiceAttachmentPointer)) {
                warn(j, "No contact stream available.");
            } else {
                ApplicationDependencies.getJobManager().add(new MultiDeviceContactSyncJob((SignalServiceAttachmentPointer) contactsMessage.getContactsStream()));
            }
        } else {
            log(j, "Primary device ignores synchronize contacts.");
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:80:0x01e5 A[Catch: MmsException -> 0x0221, TryCatch #0 {MmsException -> 0x0221, blocks: (B:3:0x0020, B:5:0x002e, B:8:0x003a, B:10:0x004a, B:13:0x0081, B:15:0x008a, B:16:0x0093, B:18:0x0099, B:19:0x00a3, B:21:0x00a9, B:24:0x00b3, B:25:0x00b9, B:27:0x00bf, B:28:0x00d0, B:30:0x00d6, B:32:0x00e0, B:33:0x00ec, B:35:0x00f2, B:36:0x00fc, B:38:0x0102, B:39:0x0108, B:41:0x0112, B:42:0x011c, B:44:0x0126, B:45:0x0136, B:47:0x0140, B:49:0x0145, B:51:0x014f, B:53:0x0159, B:55:0x0163, B:57:0x016d, B:59:0x0173, B:62:0x017e, B:63:0x0187, B:64:0x018f, B:66:0x0199, B:68:0x01ad, B:69:0x01ba, B:71:0x01c4, B:73:0x01ca, B:75:0x01d0, B:77:0x01d6, B:80:0x01e5, B:81:0x01f5, B:83:0x01ff, B:84:0x020d, B:86:0x0219), top: B:91:0x0020 }] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x01ff A[Catch: MmsException -> 0x0221, TryCatch #0 {MmsException -> 0x0221, blocks: (B:3:0x0020, B:5:0x002e, B:8:0x003a, B:10:0x004a, B:13:0x0081, B:15:0x008a, B:16:0x0093, B:18:0x0099, B:19:0x00a3, B:21:0x00a9, B:24:0x00b3, B:25:0x00b9, B:27:0x00bf, B:28:0x00d0, B:30:0x00d6, B:32:0x00e0, B:33:0x00ec, B:35:0x00f2, B:36:0x00fc, B:38:0x0102, B:39:0x0108, B:41:0x0112, B:42:0x011c, B:44:0x0126, B:45:0x0136, B:47:0x0140, B:49:0x0145, B:51:0x014f, B:53:0x0159, B:55:0x0163, B:57:0x016d, B:59:0x0173, B:62:0x017e, B:63:0x0187, B:64:0x018f, B:66:0x0199, B:68:0x01ad, B:69:0x01ba, B:71:0x01c4, B:73:0x01ca, B:75:0x01d0, B:77:0x01d6, B:80:0x01e5, B:81:0x01f5, B:83:0x01ff, B:84:0x020d, B:86:0x0219), top: B:91:0x0020 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void handleSynchronizeSentMessage(org.whispersystems.signalservice.api.messages.SignalServiceContent r9, org.whispersystems.signalservice.api.messages.multidevice.SentTranscriptMessage r10, org.thoughtcrime.securesms.recipients.Recipient r11) throws org.thoughtcrime.securesms.messages.MessageContentProcessor.StorageFailedException, org.thoughtcrime.securesms.groups.BadGroupIdException, java.io.IOException, org.thoughtcrime.securesms.groups.GroupChangeBusyException {
        /*
        // Method dump skipped, instructions count: 565
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.messages.MessageContentProcessor.handleSynchronizeSentMessage(org.whispersystems.signalservice.api.messages.SignalServiceContent, org.whispersystems.signalservice.api.messages.multidevice.SentTranscriptMessage, org.thoughtcrime.securesms.recipients.Recipient):void");
    }

    private void handleSynchronizeSentGv2Update(SignalServiceContent signalServiceContent, SentTranscriptMessage sentTranscriptMessage) throws IOException, GroupChangeBusyException {
        long timestamp = signalServiceContent.getTimestamp();
        log(timestamp, "Synchronize sent GV2 update for message with timestamp " + sentTranscriptMessage.getTimestamp());
        SignalServiceGroupV2 signalServiceGroupV2 = sentTranscriptMessage.getDataMessage().get().getGroupContext().get().getGroupV2().get();
        GroupId.V2 v2 = GroupId.v2(signalServiceGroupV2.getMasterKey());
        if (!updateGv2GroupFromServerOrP2PChange(signalServiceContent, signalServiceGroupV2)) {
            String valueOf = String.valueOf(signalServiceContent.getTimestamp());
            log(valueOf, "Ignoring GV2 message for group we are not currently in " + v2);
        }
    }

    private void handleSynchronizeRequestMessage(RequestMessage requestMessage, long j) {
        if (SignalStore.account().isPrimaryDevice()) {
            log(j, "Synchronize request message.");
            if (requestMessage.isContactsRequest()) {
                ApplicationDependencies.getJobManager().add(new MultiDeviceContactUpdateJob(true));
            }
            if (requestMessage.isGroupsRequest()) {
                ApplicationDependencies.getJobManager().add(new MultiDeviceGroupUpdateJob());
            }
            if (requestMessage.isBlockedListRequest()) {
                ApplicationDependencies.getJobManager().add(new MultiDeviceBlockedUpdateJob());
            }
            if (requestMessage.isConfigurationRequest()) {
                ApplicationDependencies.getJobManager().add(new MultiDeviceConfigurationUpdateJob(TextSecurePreferences.isReadReceiptsEnabled(this.context), TextSecurePreferences.isTypingIndicatorsEnabled(this.context), TextSecurePreferences.isShowUnidentifiedDeliveryIndicatorsEnabled(this.context), SignalStore.settings().isLinkPreviewsEnabled()));
                ApplicationDependencies.getJobManager().add(new MultiDeviceStickerPackSyncJob());
            }
            if (requestMessage.isKeysRequest()) {
                ApplicationDependencies.getJobManager().add(new MultiDeviceKeysUpdateJob());
            }
            if (requestMessage.isPniIdentityRequest()) {
                ApplicationDependencies.getJobManager().add(new MultiDevicePniIdentityUpdateJob());
                return;
            }
            return;
        }
        log(j, "Linked device ignoring synchronize request message.");
    }

    private void handleSynchronizeReadMessage(SignalServiceContent signalServiceContent, List<ReadMessage> list, long j, Recipient recipient) {
        log(j, "Synchronize read message. Count: " + list.size() + ", Timestamps: " + Stream.of(list).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda27
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return Long.valueOf(((ReadMessage) obj).getTimestamp());
            }
        }).toList());
        HashMap hashMap = new HashMap();
        Collection<MessageDatabase.SyncMessageId> timestampRead = SignalDatabase.mmsSms().setTimestampRead(recipient, list, j, hashMap);
        List<MessageDatabase.MarkedMessageInfo> readSince = SignalDatabase.threads().setReadSince(hashMap, false);
        if (Util.hasItems(readSince)) {
            String str = TAG;
            Log.i(str, "Updating past messages: " + readSince.size());
            MarkReadReceiver.process(this.context, readSince);
        }
        for (MessageDatabase.SyncMessageId syncMessageId : timestampRead) {
            String valueOf = String.valueOf(signalServiceContent.getTimestamp());
            warn(valueOf, "[handleSynchronizeReadMessage] Could not find matching message! timestamp: " + syncMessageId.getTimetamp() + "  author: " + syncMessageId.getRecipientId());
            if (!this.processingEarlyContent) {
                ApplicationDependencies.getEarlyMessageCache().store(syncMessageId.getRecipientId(), syncMessageId.getTimetamp(), signalServiceContent);
            }
        }
        if (timestampRead.size() > 0 && !this.processingEarlyContent) {
            PushProcessEarlyMessagesJob.enqueue();
        }
        MessageNotifier messageNotifier = ApplicationDependencies.getMessageNotifier();
        messageNotifier.setLastDesktopActivityTimestamp(j);
        messageNotifier.cancelDelayedNotifications();
        messageNotifier.updateNotification(this.context);
    }

    private void handleSynchronizeViewedMessage(List<ViewedMessage> list, long j) {
        log(j, "Synchronize view message. Count: " + list.size() + ", Timestamps: " + Stream.of(list).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda9
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return Long.valueOf(((ViewedMessage) obj).getTimestamp());
            }
        }).toList());
        List<Long> list2 = Stream.of(list).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda10
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MessageContentProcessor.lambda$handleSynchronizeViewedMessage$2((ViewedMessage) obj);
            }
        }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda11
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return MessageContentProcessor.lambda$handleSynchronizeViewedMessage$3((MessageRecord) obj);
            }
        }).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda12
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return Long.valueOf(((MessageRecord) obj).getId());
            }
        }).toList();
        SignalDatabase.mms().setIncomingMessagesViewed(list2);
        SignalDatabase.mms().setOutgoingGiftsRevealed(list2);
        MessageNotifier messageNotifier = ApplicationDependencies.getMessageNotifier();
        messageNotifier.setLastDesktopActivityTimestamp(j);
        messageNotifier.cancelDelayedNotifications();
        messageNotifier.updateNotification(this.context);
    }

    public static /* synthetic */ MessageRecord lambda$handleSynchronizeViewedMessage$2(ViewedMessage viewedMessage) {
        return SignalDatabase.mmsSms().getMessageFor(viewedMessage.getTimestamp(), Recipient.externalPush(viewedMessage.getSender()).getId());
    }

    public static /* synthetic */ boolean lambda$handleSynchronizeViewedMessage$3(MessageRecord messageRecord) {
        return messageRecord != null && messageRecord.isMms();
    }

    private void handleSynchronizeViewOnceOpenMessage(SignalServiceContent signalServiceContent, ViewOnceOpenMessage viewOnceOpenMessage, long j) {
        log(j, "Handling a view-once open for message: " + viewOnceOpenMessage.getTimestamp());
        RecipientId id = Recipient.externalPush(viewOnceOpenMessage.getSender()).getId();
        long timestamp = viewOnceOpenMessage.getTimestamp();
        MessageRecord messageFor = SignalDatabase.mmsSms().getMessageFor(timestamp, id);
        if (messageFor == null || !messageFor.isMms()) {
            warn(String.valueOf(j), "Got a view-once open message for a message we don't have!");
            if (!this.processingEarlyContent) {
                ApplicationDependencies.getEarlyMessageCache().store(id, timestamp, signalServiceContent);
                PushProcessEarlyMessagesJob.enqueue();
            }
        } else {
            SignalDatabase.attachments().deleteAttachmentFilesForViewOnceMessage(messageFor.getId());
        }
        MessageNotifier messageNotifier = ApplicationDependencies.getMessageNotifier();
        messageNotifier.setLastDesktopActivityTimestamp(j);
        messageNotifier.cancelDelayedNotifications();
        messageNotifier.updateNotification(this.context);
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:32:0x0037 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX DEBUG: Type inference failed for r22v0. Raw type applied. Possible types: j$.util.Optional<U>, j$.util.Optional */
    /* JADX DEBUG: Type inference failed for r24v0. Raw type applied. Possible types: j$.util.Optional<U>, j$.util.Optional */
    /* JADX WARN: Type inference failed for: r2v0, types: [long] */
    /* JADX WARN: Type inference failed for: r2v1, types: [org.thoughtcrime.securesms.database.MessageDatabase] */
    /* JADX WARN: Type inference failed for: r2v2, types: [org.thoughtcrime.securesms.database.MessageDatabase, org.thoughtcrime.securesms.database.MmsDatabase] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void handleStoryMessage(org.whispersystems.signalservice.api.messages.SignalServiceContent r33, org.whispersystems.signalservice.api.messages.SignalServiceStoryMessage r34, org.thoughtcrime.securesms.recipients.Recipient r35, org.thoughtcrime.securesms.recipients.Recipient r36) throws org.thoughtcrime.securesms.messages.MessageContentProcessor.StorageFailedException {
        /*
        // Method dump skipped, instructions count: 305
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.messages.MessageContentProcessor.handleStoryMessage(org.whispersystems.signalservice.api.messages.SignalServiceContent, org.whispersystems.signalservice.api.messages.SignalServiceStoryMessage, org.thoughtcrime.securesms.recipients.Recipient, org.thoughtcrime.securesms.recipients.Recipient):void");
    }

    public static /* synthetic */ Optional lambda$handleStoryMessage$4(SignalServiceTextAttachment signalServiceTextAttachment) {
        return signalServiceTextAttachment.getPreview().map(new MessageContentProcessor$$ExternalSyntheticLambda0());
    }

    public String serializeTextAttachment(SignalServiceTextAttachment signalServiceTextAttachment) {
        StoryTextPost.Builder newBuilder = StoryTextPost.newBuilder();
        if (signalServiceTextAttachment.getText().isPresent()) {
            newBuilder.setBody(signalServiceTextAttachment.getText().get());
        }
        if (signalServiceTextAttachment.getStyle().isPresent()) {
            switch (AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$api$messages$SignalServiceTextAttachment$Style[signalServiceTextAttachment.getStyle().get().ordinal()]) {
                case 1:
                    newBuilder.setStyle(StoryTextPost.Style.DEFAULT);
                    break;
                case 2:
                    newBuilder.setStyle(StoryTextPost.Style.REGULAR);
                    break;
                case 3:
                    newBuilder.setStyle(StoryTextPost.Style.BOLD);
                    break;
                case 4:
                    newBuilder.setStyle(StoryTextPost.Style.SERIF);
                    break;
                case 5:
                    newBuilder.setStyle(StoryTextPost.Style.SCRIPT);
                    break;
                case 6:
                    newBuilder.setStyle(StoryTextPost.Style.CONDENSED);
                    break;
            }
        }
        if (signalServiceTextAttachment.getTextBackgroundColor().isPresent()) {
            newBuilder.setTextBackgroundColor(signalServiceTextAttachment.getTextBackgroundColor().get().intValue());
        }
        if (signalServiceTextAttachment.getTextForegroundColor().isPresent()) {
            newBuilder.setTextForegroundColor(signalServiceTextAttachment.getTextForegroundColor().get().intValue());
        }
        ChatColor.Builder newBuilder2 = ChatColor.newBuilder();
        if (signalServiceTextAttachment.getBackgroundColor().isPresent()) {
            newBuilder2.setSingleColor(ChatColor.SingleColor.newBuilder().setColor(signalServiceTextAttachment.getBackgroundColor().get().intValue()));
        } else if (signalServiceTextAttachment.getBackgroundGradient().isPresent()) {
            SignalServiceTextAttachment.Gradient gradient = signalServiceTextAttachment.getBackgroundGradient().get();
            ChatColor.LinearGradient.Builder newBuilder3 = ChatColor.LinearGradient.newBuilder();
            newBuilder3.setRotation(gradient.getAngle().orElse(0).floatValue());
            newBuilder3.addColors(gradient.getStartColor().get().intValue());
            newBuilder3.addColors(gradient.getEndColor().get().intValue());
            newBuilder3.addAllPositions(Arrays.asList(Float.valueOf(0.0f), Float.valueOf(1.0f)));
            newBuilder2.setLinearGradient(newBuilder3);
        }
        newBuilder.setBackground(newBuilder2);
        return Base64.encodeBytes(newBuilder.build().toByteArray());
    }

    /* renamed from: org.thoughtcrime.securesms.messages.MessageContentProcessor$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$messages$MessageContentProcessor$MessageState;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$api$messages$SignalServiceTextAttachment$Style;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$MessageRequestResponseMessage$Type;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$SignalServiceSyncMessage$FetchType;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$StickerPackOperationMessage$Type;

        static {
            int[] iArr = new int[SignalServiceTextAttachment.Style.values().length];
            $SwitchMap$org$whispersystems$signalservice$api$messages$SignalServiceTextAttachment$Style = iArr;
            try {
                iArr[SignalServiceTextAttachment.Style.DEFAULT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$SignalServiceTextAttachment$Style[SignalServiceTextAttachment.Style.REGULAR.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$SignalServiceTextAttachment$Style[SignalServiceTextAttachment.Style.BOLD.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$SignalServiceTextAttachment$Style[SignalServiceTextAttachment.Style.SERIF.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$SignalServiceTextAttachment$Style[SignalServiceTextAttachment.Style.SCRIPT.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$SignalServiceTextAttachment$Style[SignalServiceTextAttachment.Style.CONDENSED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            int[] iArr2 = new int[MessageRequestResponseMessage.Type.values().length];
            $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$MessageRequestResponseMessage$Type = iArr2;
            try {
                iArr2[MessageRequestResponseMessage.Type.ACCEPT.ordinal()] = 1;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$MessageRequestResponseMessage$Type[MessageRequestResponseMessage.Type.DELETE.ordinal()] = 2;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$MessageRequestResponseMessage$Type[MessageRequestResponseMessage.Type.BLOCK.ordinal()] = 3;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$MessageRequestResponseMessage$Type[MessageRequestResponseMessage.Type.BLOCK_AND_DELETE.ordinal()] = 4;
            } catch (NoSuchFieldError unused10) {
            }
            int[] iArr3 = new int[SignalServiceSyncMessage.FetchType.values().length];
            $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$SignalServiceSyncMessage$FetchType = iArr3;
            try {
                iArr3[SignalServiceSyncMessage.FetchType.LOCAL_PROFILE.ordinal()] = 1;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$SignalServiceSyncMessage$FetchType[SignalServiceSyncMessage.FetchType.STORAGE_MANIFEST.ordinal()] = 2;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$SignalServiceSyncMessage$FetchType[SignalServiceSyncMessage.FetchType.SUBSCRIPTION_STATUS.ordinal()] = 3;
            } catch (NoSuchFieldError unused13) {
            }
            int[] iArr4 = new int[StickerPackOperationMessage.Type.values().length];
            $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$StickerPackOperationMessage$Type = iArr4;
            try {
                iArr4[StickerPackOperationMessage.Type.INSTALL.ordinal()] = 1;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$multidevice$StickerPackOperationMessage$Type[StickerPackOperationMessage.Type.REMOVE.ordinal()] = 2;
            } catch (NoSuchFieldError unused15) {
            }
            int[] iArr5 = new int[MessageState.values().length];
            $SwitchMap$org$thoughtcrime$securesms$messages$MessageContentProcessor$MessageState = iArr5;
            try {
                iArr5[MessageState.INVALID_VERSION.ordinal()] = 1;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messages$MessageContentProcessor$MessageState[MessageState.LEGACY_MESSAGE.ordinal()] = 2;
            } catch (NoSuchFieldError unused17) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messages$MessageContentProcessor$MessageState[MessageState.DUPLICATE_MESSAGE.ordinal()] = 3;
            } catch (NoSuchFieldError unused18) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messages$MessageContentProcessor$MessageState[MessageState.UNSUPPORTED_DATA_MESSAGE.ordinal()] = 4;
            } catch (NoSuchFieldError unused19) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messages$MessageContentProcessor$MessageState[MessageState.CORRUPT_MESSAGE.ordinal()] = 5;
            } catch (NoSuchFieldError unused20) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messages$MessageContentProcessor$MessageState[MessageState.NO_SESSION.ordinal()] = 6;
            } catch (NoSuchFieldError unused21) {
            }
        }
    }

    private MessageId handleStoryReaction(SignalServiceContent signalServiceContent, SignalServiceDataMessage signalServiceDataMessage, Recipient recipient) throws StorageFailedException {
        MmsDatabase mms;
        long j;
        QuoteModel quoteModel;
        ParentStoryId parentStoryId;
        log(signalServiceContent.getTimestamp(), "Story reaction.");
        if (!Stories.isFeatureAvailable()) {
            warn(signalServiceContent.getTimestamp(), "Dropping unsupported story reaction.");
            return null;
        }
        SignalServiceDataMessage.Reaction reaction = signalServiceDataMessage.getReaction().get();
        if (!EmojiUtil.isEmoji(reaction.getEmoji())) {
            warn(signalServiceContent.getTimestamp(), "Story reaction text is not a valid emoji! Ignoring the message.");
            return null;
        }
        try {
            SignalServiceDataMessage.StoryContext storyContext = signalServiceDataMessage.getStoryContext().get();
            mms = SignalDatabase.mms();
            mms.beginTransaction();
            try {
                RecipientId from = RecipientId.from(storyContext.getAuthorServiceId());
                try {
                    MessageId storyId = mms.getStoryId(from, storyContext.getSentTimestamp());
                    if (signalServiceDataMessage.getGroupContext().isPresent()) {
                        parentStoryId = new ParentStoryId.GroupReply(storyId.getId());
                        quoteModel = null;
                        j = 0;
                    } else if (SignalDatabase.storySends().canReply(recipient.getId(), storyContext.getSentTimestamp())) {
                        MmsMessageRecord mmsMessageRecord = (MmsMessageRecord) mms.getMessageRecord(storyId.getId());
                        String str = "";
                        if (mmsMessageRecord.getStoryType().isTextStory()) {
                            str = mmsMessageRecord.getBody();
                        }
                        ParentStoryId.DirectReply directReply = new ParentStoryId.DirectReply(storyId.getId());
                        quoteModel = new QuoteModel(storyContext.getSentTimestamp(), from, str, false, mmsMessageRecord.getSlideDeck().asAttachments(), Collections.emptyList(), QuoteModel.Type.NORMAL);
                        j = TimeUnit.SECONDS.toMillis((long) signalServiceDataMessage.getExpiresInSeconds());
                        parentStoryId = directReply;
                    } else {
                        warn(signalServiceContent.getTimestamp(), "Story has reactions disabled. Dropping reaction.");
                        return null;
                    }
                    Optional<MessageDatabase.InsertResult> insertSecureDecryptedMessageInbox = mms.insertSecureDecryptedMessageInbox(new IncomingMediaMessage(recipient.getId(), signalServiceContent.getTimestamp(), signalServiceContent.getServerReceivedTimestamp(), System.currentTimeMillis(), StoryType.NONE, parentStoryId, true, -1, j, false, false, signalServiceContent.isNeedsReceipt(), Optional.of(reaction.getEmoji()), Optional.ofNullable(GroupUtil.getGroupContextIfPresent(signalServiceContent)), Optional.empty(), Optional.ofNullable(quoteModel), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), signalServiceContent.getServerUuid(), null), -1);
                    if (insertSecureDecryptedMessageInbox.isPresent()) {
                        mms.setTransactionSuccessful();
                        if (parentStoryId.isGroupReply()) {
                            ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.fromThreadAndReply(insertSecureDecryptedMessageInbox.get().getThreadId(), (ParentStoryId.GroupReply) parentStoryId));
                        } else {
                            ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.forConversation(insertSecureDecryptedMessageInbox.get().getThreadId()));
                            TrimThreadJob.enqueueAsync(insertSecureDecryptedMessageInbox.get().getThreadId());
                        }
                        if (parentStoryId.isDirectReply()) {
                            return MessageId.fromNullable(insertSecureDecryptedMessageInbox.get().getMessageId(), true);
                        }
                        return null;
                    }
                    warn(signalServiceContent.getTimestamp(), "Failed to insert story reaction");
                    return null;
                } catch (NoSuchMessageException e) {
                    warn(signalServiceContent.getTimestamp(), "Couldn't find story for reaction.", e);
                    return null;
                }
            } catch (MmsException e2) {
                throw new StorageFailedException(e2, signalServiceContent.getSender().getIdentifier(), signalServiceContent.getSenderDevice(), null);
            }
        } finally {
            mms.endTransaction();
        }
    }

    private MessageId handleStoryReply(SignalServiceContent signalServiceContent, SignalServiceDataMessage signalServiceDataMessage, Recipient recipient, long j) throws StorageFailedException {
        MmsDatabase mms;
        QuoteModel quoteModel;
        ParentStoryId parentStoryId;
        log(signalServiceContent.getTimestamp(), "Story reply.");
        if (!Stories.isFeatureAvailable()) {
            warn(signalServiceContent.getTimestamp(), "Dropping unsupported story reply.");
            return null;
        }
        try {
            SignalServiceDataMessage.StoryContext storyContext = signalServiceDataMessage.getStoryContext().get();
            mms = SignalDatabase.mms();
            mms.beginTransaction();
            try {
                RecipientId from = RecipientId.from(storyContext.getAuthorServiceId());
                long j2 = 0;
                try {
                    MessageId storyId = mms.getStoryId(from, storyContext.getSentTimestamp());
                    MmsMessageRecord mmsMessageRecord = (MmsMessageRecord) mms.getMessageRecord(storyId.getId());
                    Recipient recipientForThreadId = SignalDatabase.threads().getRecipientForThreadId(mmsMessageRecord.getThreadId());
                    Objects.requireNonNull(recipientForThreadId);
                    boolean isActiveGroup = recipientForThreadId.isActiveGroup();
                    handlePossibleExpirationUpdate(signalServiceContent, signalServiceDataMessage, recipientForThreadId.getGroupId(), recipient, recipientForThreadId, j);
                    if (signalServiceDataMessage.getGroupContext().isPresent()) {
                        parentStoryId = new ParentStoryId.GroupReply(storyId.getId());
                        quoteModel = null;
                    } else {
                        if (!isActiveGroup && !SignalDatabase.storySends().canReply(recipient.getId(), storyContext.getSentTimestamp())) {
                            warn(signalServiceContent.getTimestamp(), "Story has replies disabled. Dropping reply.");
                            return null;
                        }
                        ParentStoryId.DirectReply directReply = new ParentStoryId.DirectReply(storyId.getId());
                        String str = "";
                        if (mmsMessageRecord.getStoryType().isTextStory()) {
                            str = mmsMessageRecord.getBody();
                        }
                        quoteModel = new QuoteModel(storyContext.getSentTimestamp(), from, str, false, mmsMessageRecord.getSlideDeck().asAttachments(), Collections.emptyList(), QuoteModel.Type.NORMAL);
                        j2 = TimeUnit.SECONDS.toMillis((long) signalServiceDataMessage.getExpiresInSeconds());
                        parentStoryId = directReply;
                    }
                    Optional<MessageDatabase.InsertResult> insertSecureDecryptedMessageInbox = mms.insertSecureDecryptedMessageInbox(new IncomingMediaMessage(recipient.getId(), signalServiceContent.getTimestamp(), signalServiceContent.getServerReceivedTimestamp(), System.currentTimeMillis(), StoryType.NONE, parentStoryId, false, -1, j2, false, false, signalServiceContent.isNeedsReceipt(), signalServiceDataMessage.getBody(), Optional.ofNullable(GroupUtil.getGroupContextIfPresent(signalServiceContent)), Optional.empty(), Optional.ofNullable(quoteModel), Optional.empty(), Optional.empty(), getMentions(signalServiceDataMessage.getMentions()), Optional.empty(), signalServiceContent.getServerUuid(), null), -1);
                    if (insertSecureDecryptedMessageInbox.isPresent()) {
                        mms.setTransactionSuccessful();
                        if (parentStoryId.isGroupReply()) {
                            ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.fromThreadAndReply(insertSecureDecryptedMessageInbox.get().getThreadId(), (ParentStoryId.GroupReply) parentStoryId));
                        } else {
                            ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.forConversation(insertSecureDecryptedMessageInbox.get().getThreadId()));
                            TrimThreadJob.enqueueAsync(insertSecureDecryptedMessageInbox.get().getThreadId());
                        }
                        if (parentStoryId.isDirectReply()) {
                            return MessageId.fromNullable(insertSecureDecryptedMessageInbox.get().getMessageId(), true);
                        }
                        return null;
                    }
                    warn(signalServiceContent.getTimestamp(), "Failed to insert story reply.");
                    return null;
                } catch (NoSuchMessageException e) {
                    warn(signalServiceContent.getTimestamp(), "Couldn't find story for reply.", e);
                    return null;
                }
            } catch (MmsException e2) {
                throw new StorageFailedException(e2, signalServiceContent.getSender().getIdentifier(), signalServiceContent.getSenderDevice(), null);
            }
        } finally {
            mms.endTransaction();
        }
    }

    private MessageId handleGiftMessage(SignalServiceContent signalServiceContent, SignalServiceDataMessage signalServiceDataMessage, Recipient recipient, Recipient recipient2, long j) throws StorageFailedException {
        log(signalServiceDataMessage.getTimestamp(), "Gift message.");
        if (!FeatureFlags.giftBadgeReceiveSupport()) {
            warn(signalServiceDataMessage.getTimestamp(), "Dropping unsupported gift badge message.");
            return null;
        }
        notifyTypingStoppedFromIncomingMessage(recipient, recipient2, signalServiceContent.getSenderDevice());
        MmsDatabase mms = SignalDatabase.mms();
        GiftBadge build = GiftBadge.newBuilder().setRedemptionToken(ByteString.copyFrom(signalServiceDataMessage.getGiftBadge().get().getReceiptCredentialPresentation().serialize())).setRedemptionState(GiftBadge.RedemptionState.PENDING).build();
        try {
            Optional<MessageDatabase.InsertResult> insertSecureDecryptedMessageInbox = mms.insertSecureDecryptedMessageInbox(new IncomingMediaMessage(recipient.getId(), signalServiceDataMessage.getTimestamp(), signalServiceContent.getServerReceivedTimestamp(), j, StoryType.NONE, null, false, -1, TimeUnit.SECONDS.toMillis((long) signalServiceDataMessage.getExpiresInSeconds()), false, false, signalServiceContent.isNeedsReceipt(), Optional.of(Base64.encodeBytes(build.toByteArray())), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), signalServiceContent.getServerUuid(), build), -1);
            if (!insertSecureDecryptedMessageInbox.isPresent()) {
                return null;
            }
            ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.forConversation(insertSecureDecryptedMessageInbox.get().getThreadId()));
            TrimThreadJob.enqueueAsync(insertSecureDecryptedMessageInbox.get().getThreadId());
            return new MessageId(insertSecureDecryptedMessageInbox.get().getMessageId(), true);
        } catch (MmsException e) {
            throw new StorageFailedException(e, signalServiceContent.getSender().getIdentifier(), signalServiceContent.getSenderDevice(), null);
        }
    }

    /* JADX INFO: finally extract failed */
    private MessageId handleMediaMessage(SignalServiceContent signalServiceContent, SignalServiceDataMessage signalServiceDataMessage, Optional<Long> optional, Recipient recipient, Recipient recipient2, long j) throws StorageFailedException {
        MmsDatabase mms;
        try {
            log(signalServiceDataMessage.getTimestamp(), "Media message.");
            notifyTypingStoppedFromIncomingMessage(recipient, recipient2, signalServiceContent.getSenderDevice());
            mms = SignalDatabase.mms();
            mms.beginTransaction();
            try {
                Optional<QuoteModel> validatedQuote = getValidatedQuote(signalServiceDataMessage.getQuote());
                Optional<List<Contact>> contacts = getContacts(signalServiceDataMessage.getSharedContacts());
                Optional<List<LinkPreview>> linkPreviews = getLinkPreviews(signalServiceDataMessage.getPreviews(), signalServiceDataMessage.getBody().orElse(""), false);
                Optional<List<Mention>> mentions = getMentions(signalServiceDataMessage.getMentions());
                Optional<Attachment> stickerAttachment = getStickerAttachment(signalServiceDataMessage.getSticker());
                handlePossibleExpirationUpdate(signalServiceContent, signalServiceDataMessage, Optional.empty(), recipient, recipient2, j);
                Optional<MessageDatabase.InsertResult> insertSecureDecryptedMessageInbox = mms.insertSecureDecryptedMessageInbox(new IncomingMediaMessage(recipient.getId(), signalServiceDataMessage.getTimestamp(), signalServiceContent.getServerReceivedTimestamp(), j, StoryType.NONE, null, false, -1, TimeUnit.SECONDS.toMillis((long) signalServiceDataMessage.getExpiresInSeconds()), false, signalServiceDataMessage.isViewOnce(), signalServiceContent.isNeedsReceipt(), signalServiceDataMessage.getBody(), signalServiceDataMessage.getGroupContext(), signalServiceDataMessage.getAttachments(), validatedQuote, contacts, linkPreviews, mentions, stickerAttachment, signalServiceContent.getServerUuid(), null), -1);
                if (insertSecureDecryptedMessageInbox.isPresent()) {
                    if (optional.isPresent()) {
                        SignalDatabase.sms().deleteMessage(optional.get().longValue());
                    }
                    mms.setTransactionSuccessful();
                }
                mms.endTransaction();
                if (!insertSecureDecryptedMessageInbox.isPresent()) {
                    return null;
                }
                List<DatabaseAttachment> attachmentsForMessage = SignalDatabase.attachments().getAttachmentsForMessage(insertSecureDecryptedMessageInbox.get().getMessageId());
                List<DatabaseAttachment> list = Stream.of(attachmentsForMessage).filter(new MessageContentProcessor$$ExternalSyntheticLambda2()).toList();
                List<DatabaseAttachment> list2 = Stream.of(attachmentsForMessage).filterNot(new MessageContentProcessor$$ExternalSyntheticLambda2()).toList();
                forceStickerDownloadIfNecessary(insertSecureDecryptedMessageInbox.get().getMessageId(), list);
                for (DatabaseAttachment databaseAttachment : list2) {
                    ApplicationDependencies.getJobManager().add(new AttachmentDownloadJob(insertSecureDecryptedMessageInbox.get().getMessageId(), databaseAttachment.getAttachmentId(), false));
                }
                ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.forConversation(insertSecureDecryptedMessageInbox.get().getThreadId()));
                TrimThreadJob.enqueueAsync(insertSecureDecryptedMessageInbox.get().getThreadId());
                if (signalServiceDataMessage.isViewOnce()) {
                    ApplicationDependencies.getViewOnceMessageManager().scheduleIfNecessary();
                }
                return new MessageId(insertSecureDecryptedMessageInbox.get().getMessageId(), true);
            } catch (MmsException e) {
                throw new StorageFailedException(e, signalServiceContent.getSender().getIdentifier(), signalServiceContent.getSenderDevice(), null);
            }
        } catch (Throwable th) {
            mms.endTransaction();
            throw th;
        }
    }

    private long handleSynchronizeSentExpirationUpdate(SentTranscriptMessage sentTranscriptMessage) throws MmsException, BadGroupIdException {
        log(sentTranscriptMessage.getTimestamp(), "Synchronize sent expiration update.");
        MmsDatabase mms = SignalDatabase.mms();
        Recipient syncMessageDestination = getSyncMessageDestination(sentTranscriptMessage);
        OutgoingExpirationUpdateMessage outgoingExpirationUpdateMessage = new OutgoingExpirationUpdateMessage(syncMessageDestination, sentTranscriptMessage.getTimestamp(), TimeUnit.SECONDS.toMillis((long) sentTranscriptMessage.getDataMessage().get().getExpiresInSeconds()));
        long orCreateThreadIdFor = SignalDatabase.threads().getOrCreateThreadIdFor(syncMessageDestination);
        mms.markAsSent(mms.insertMessageOutbox(outgoingExpirationUpdateMessage, orCreateThreadIdFor, false, null), true);
        SignalDatabase.recipients().setExpireMessages(syncMessageDestination.getId(), sentTranscriptMessage.getDataMessage().get().getExpiresInSeconds());
        return orCreateThreadIdFor;
    }

    private long handleSynchronizeSentStoryReply(SentTranscriptMessage sentTranscriptMessage, long j) throws MmsException, BadGroupIdException {
        String str;
        QuoteModel quoteModel;
        ParentStoryId parentStoryId;
        long j2;
        Recipient recipient;
        if (!Stories.isFeatureAvailable()) {
            warn(j, "Dropping unsupported story reply sync message.");
            return -1;
        }
        log(j, "Synchronize sent story reply for " + sentTranscriptMessage.getTimestamp());
        try {
            Optional<SignalServiceDataMessage.Reaction> reaction = sentTranscriptMessage.getDataMessage().get().getReaction();
            SignalServiceDataMessage.StoryContext storyContext = sentTranscriptMessage.getDataMessage().get().getStoryContext().get();
            MmsDatabase mms = SignalDatabase.mms();
            Recipient syncMessageDestination = getSyncMessageDestination(sentTranscriptMessage);
            RecipientId from = RecipientId.from(storyContext.getAuthorServiceId());
            MessageId storyId = mms.getStoryId(from, storyContext.getSentTimestamp());
            MmsMessageRecord mmsMessageRecord = (MmsMessageRecord) mms.getMessageRecord(storyId.getId());
            Recipient recipientForThreadId = SignalDatabase.threads().getRecipientForThreadId(mmsMessageRecord.getThreadId());
            boolean z = recipientForThreadId != null && recipientForThreadId.isActiveGroup();
            if (!reaction.isPresent() || !EmojiUtil.isEmoji(reaction.get().getEmoji())) {
                str = sentTranscriptMessage.getDataMessage().get().getBody().orElse(null);
            } else {
                str = reaction.get().getEmoji();
            }
            if (sentTranscriptMessage.getDataMessage().get().getGroupContext().isPresent()) {
                parentStoryId = new ParentStoryId.GroupReply(storyId.getId());
                quoteModel = null;
                j2 = 0;
            } else {
                if (!z && !SignalDatabase.storySends().canReply(from, storyContext.getSentTimestamp())) {
                    warn(j, "Story has replies disabled. Dropping reply.");
                    return -1;
                }
                ParentStoryId.DirectReply directReply = new ParentStoryId.DirectReply(storyId.getId());
                String str2 = "";
                if (mmsMessageRecord.getStoryType().isTextStory()) {
                    str2 = mmsMessageRecord.getBody();
                }
                QuoteModel quoteModel2 = new QuoteModel(storyContext.getSentTimestamp(), from, str2, false, mmsMessageRecord.getSlideDeck().asAttachments(), Collections.emptyList(), QuoteModel.Type.NORMAL);
                j2 = TimeUnit.SECONDS.toMillis(sentTranscriptMessage.getExpirationStartTimestamp());
                parentStoryId = directReply;
                quoteModel = quoteModel2;
            }
            OutgoingSecureMediaMessage outgoingSecureMediaMessage = new OutgoingSecureMediaMessage(new OutgoingMediaMessage(syncMessageDestination, str, Collections.emptyList(), sentTranscriptMessage.getTimestamp(), -1, j2, false, 2, StoryType.NONE, parentStoryId, sentTranscriptMessage.getDataMessage().get().getReaction().isPresent(), quoteModel, Collections.emptyList(), Collections.emptyList(), getMentions(sentTranscriptMessage.getDataMessage().get().getMentions()).orElse(Collections.emptyList()), Collections.emptySet(), Collections.emptySet(), null));
            if (syncMessageDestination.getExpiresInSeconds() != sentTranscriptMessage.getDataMessage().get().getExpiresInSeconds()) {
                handleSynchronizeSentExpirationUpdate(sentTranscriptMessage);
            }
            long orCreateThreadIdFor = SignalDatabase.threads().getOrCreateThreadIdFor(syncMessageDestination);
            mms.beginTransaction();
            long insertMessageOutbox = mms.insertMessageOutbox((OutgoingMediaMessage) outgoingSecureMediaMessage, orCreateThreadIdFor, false, -1, (MessageDatabase.InsertListener) null);
            if (syncMessageDestination.isGroup()) {
                updateGroupReceiptStatus(sentTranscriptMessage, insertMessageOutbox, syncMessageDestination.requireGroupId());
                recipient = syncMessageDestination;
            } else {
                recipient = syncMessageDestination;
                mms.markUnidentified(insertMessageOutbox, isUnidentified(sentTranscriptMessage, recipient));
            }
            mms.markAsSent(insertMessageOutbox, true);
            if (sentTranscriptMessage.getDataMessage().get().getExpiresInSeconds() > 0) {
                mms.markExpireStarted(insertMessageOutbox, sentTranscriptMessage.getExpirationStartTimestamp());
                ApplicationDependencies.getExpiringMessageManager().scheduleDeletion(insertMessageOutbox, true, sentTranscriptMessage.getExpirationStartTimestamp(), TimeUnit.SECONDS.toMillis((long) sentTranscriptMessage.getDataMessage().get().getExpiresInSeconds()));
            }
            if (recipient.isSelf()) {
                MessageDatabase.SyncMessageId syncMessageId = new MessageDatabase.SyncMessageId(recipient.getId(), sentTranscriptMessage.getTimestamp());
                SignalDatabase.mmsSms().incrementDeliveryReceiptCount(syncMessageId, System.currentTimeMillis());
                SignalDatabase.mmsSms().incrementReadReceiptCount(syncMessageId, System.currentTimeMillis());
            }
            mms.setTransactionSuccessful();
            mms.endTransaction();
            return orCreateThreadIdFor;
        } catch (NoSuchMessageException e) {
            warn(j, "Couldn't find story for reply.", e);
            return -1;
        }
    }

    private void handleSynchronizeSentStoryMessage(SentTranscriptMessage sentTranscriptMessage, long j) throws MmsException {
        log(j, "Synchronize sent story message for " + sentTranscriptMessage.getTimestamp());
        SentStorySyncManifest fromRecipientsSet = SentStorySyncManifest.fromRecipientsSet(sentTranscriptMessage.getStoryMessageRecipients());
        if (sentTranscriptMessage.isRecipientUpdate()) {
            log(j, "Processing recipient update for story message and exiting...");
            SignalDatabase.storySends().applySentStoryManifest(fromRecipientsSet, sentTranscriptMessage.getTimestamp());
            return;
        }
        SignalServiceStoryMessage signalServiceStoryMessage = sentTranscriptMessage.getStoryMessage().get();
        Set<DistributionId> distributionIdSet = fromRecipientsSet.getDistributionIdSet();
        Optional<U> map = signalServiceStoryMessage.getGroupContext().map(new Function() { // from class: org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda3
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return MessageContentProcessor.lambda$handleSynchronizeSentStoryMessage$5((SignalServiceGroupV2) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        String str = (String) signalServiceStoryMessage.getTextAttachment().map(new MessageContentProcessor$$ExternalSyntheticLambda4(this)).orElse(null);
        StoryType storyType = getStoryType(signalServiceStoryMessage);
        List<LinkPreview> orElse = getLinkPreviews(signalServiceStoryMessage.getTextAttachment().flatMap(new Function() { // from class: org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda5
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return MessageContentProcessor.lambda$handleSynchronizeSentStoryMessage$6((SignalServiceTextAttachment) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }), "", true).orElse(Collections.emptyList());
        List<Attachment> forPointers = PointerAttachment.forPointers(signalServiceStoryMessage.getFileAttachment().map(new Function() { // from class: org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda6
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((SignalServiceAttachment) obj).asPointer();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda7
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return Collections.singletonList((SignalServiceAttachmentPointer) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }));
        for (DistributionId distributionId : distributionIdSet) {
            insertSentStoryMessage(sentTranscriptMessage, Recipient.resolved(SignalDatabase.distributionLists().getOrCreateByDistributionId(distributionId, fromRecipientsSet)), str, forPointers, sentTranscriptMessage.getTimestamp(), storyType, orElse);
        }
        if (map.isPresent()) {
            Optional<RecipientId> byGroupId = SignalDatabase.recipients().getByGroupId((GroupId) map.get());
            if (byGroupId.isPresent()) {
                insertSentStoryMessage(sentTranscriptMessage, Recipient.resolved(byGroupId.get()), str, forPointers, sentTranscriptMessage.getTimestamp(), storyType, orElse);
            }
        }
        SignalDatabase.storySends().applySentStoryManifest(fromRecipientsSet, sentTranscriptMessage.getTimestamp());
    }

    public static /* synthetic */ GroupId lambda$handleSynchronizeSentStoryMessage$5(SignalServiceGroupV2 signalServiceGroupV2) {
        return GroupId.v2(signalServiceGroupV2.getMasterKey());
    }

    public static /* synthetic */ Optional lambda$handleSynchronizeSentStoryMessage$6(SignalServiceTextAttachment signalServiceTextAttachment) {
        return signalServiceTextAttachment.getPreview().map(new MessageContentProcessor$$ExternalSyntheticLambda0());
    }

    private void insertSentStoryMessage(SentTranscriptMessage sentTranscriptMessage, Recipient recipient, String str, List<Attachment> list, long j, StoryType storyType, List<LinkPreview> list2) throws MmsException {
        Throwable th;
        if (SignalDatabase.mms().isOutgoingStoryAlreadyInDatabase(recipient.getId(), j)) {
            warn(j, "Already inserted this story.");
            return;
        }
        OutgoingSecureMediaMessage outgoingSecureMediaMessage = new OutgoingSecureMediaMessage(new OutgoingMediaMessage(recipient, str, list, j, -1, 0, false, 2, storyType, null, false, null, Collections.emptyList(), list2, Collections.emptyList(), Collections.emptySet(), Collections.emptySet(), null));
        MmsDatabase mms = SignalDatabase.mms();
        long orCreateThreadIdFor = SignalDatabase.threads().getOrCreateThreadIdFor(recipient);
        mms.beginTransaction();
        try {
            long insertMessageOutbox = mms.insertMessageOutbox((OutgoingMediaMessage) outgoingSecureMediaMessage, orCreateThreadIdFor, false, -1, (MessageDatabase.InsertListener) null);
            try {
                if (recipient.isGroup()) {
                    updateGroupReceiptStatus(sentTranscriptMessage, insertMessageOutbox, recipient.requireGroupId());
                } else {
                    mms.markUnidentified(insertMessageOutbox, isUnidentified(sentTranscriptMessage, recipient));
                }
                mms.markAsSent(insertMessageOutbox, true);
                List<DatabaseAttachment> list3 = Stream.of(SignalDatabase.attachments().getAttachmentsForMessage(insertMessageOutbox)).filterNot(new MessageContentProcessor$$ExternalSyntheticLambda2()).toList();
                if (recipient.isSelf()) {
                    MessageDatabase.SyncMessageId syncMessageId = new MessageDatabase.SyncMessageId(recipient.getId(), sentTranscriptMessage.getTimestamp());
                    SignalDatabase.mmsSms().incrementDeliveryReceiptCount(syncMessageId, System.currentTimeMillis());
                    SignalDatabase.mmsSms().incrementReadReceiptCount(syncMessageId, System.currentTimeMillis());
                }
                mms.setTransactionSuccessful();
                mms.endTransaction();
                for (DatabaseAttachment databaseAttachment : list3) {
                    ApplicationDependencies.getJobManager().add(new AttachmentDownloadJob(insertMessageOutbox, databaseAttachment.getAttachmentId(), false));
                }
            } catch (Throwable th2) {
                th = th2;
                mms.endTransaction();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
        }
    }

    private StoryType getStoryType(SignalServiceStoryMessage signalServiceStoryMessage) {
        if (signalServiceStoryMessage.getAllowsReplies().orElse(Boolean.FALSE).booleanValue()) {
            if (signalServiceStoryMessage.getTextAttachment().isPresent()) {
                return StoryType.TEXT_STORY_WITH_REPLIES;
            }
            return StoryType.STORY_WITH_REPLIES;
        } else if (signalServiceStoryMessage.getTextAttachment().isPresent()) {
            return StoryType.TEXT_STORY_WITHOUT_REPLIES;
        } else {
            return StoryType.STORY_WITHOUT_REPLIES;
        }
    }

    /* JADX INFO: finally extract failed */
    private long handleSynchronizeSentMediaMessage(SentTranscriptMessage sentTranscriptMessage, long j) throws MmsException, BadGroupIdException {
        List<Attachment> list;
        log(j, "Synchronize sent media message for " + sentTranscriptMessage.getTimestamp());
        MmsDatabase mms = SignalDatabase.mms();
        Recipient syncMessageDestination = getSyncMessageDestination(sentTranscriptMessage);
        Optional<QuoteModel> validatedQuote = getValidatedQuote(sentTranscriptMessage.getDataMessage().get().getQuote());
        Optional<Attachment> stickerAttachment = getStickerAttachment(sentTranscriptMessage.getDataMessage().get().getSticker());
        Optional<List<Contact>> contacts = getContacts(sentTranscriptMessage.getDataMessage().get().getSharedContacts());
        Optional<List<LinkPreview>> linkPreviews = getLinkPreviews(sentTranscriptMessage.getDataMessage().get().getPreviews(), sentTranscriptMessage.getDataMessage().get().getBody().orElse(""), false);
        Optional<List<Mention>> mentions = getMentions(sentTranscriptMessage.getDataMessage().get().getMentions());
        Optional<GiftBadge> giftBadge = getGiftBadge(sentTranscriptMessage.getDataMessage().get().getGiftBadge());
        boolean isViewOnce = sentTranscriptMessage.getDataMessage().get().isViewOnce();
        if (isViewOnce) {
            list = Collections.singletonList(new TombstoneAttachment(MediaUtil.VIEW_ONCE, false));
        } else {
            list = PointerAttachment.forPointers(sentTranscriptMessage.getDataMessage().get().getAttachments());
        }
        if (stickerAttachment.isPresent()) {
            list.add(stickerAttachment.get());
        }
        long timestamp = sentTranscriptMessage.getTimestamp();
        TimeUnit timeUnit = TimeUnit.SECONDS;
        OutgoingSecureMediaMessage outgoingSecureMediaMessage = new OutgoingSecureMediaMessage(new OutgoingMediaMessage(syncMessageDestination, sentTranscriptMessage.getDataMessage().get().getBody().orElse(null), list, timestamp, -1, timeUnit.toMillis((long) sentTranscriptMessage.getDataMessage().get().getExpiresInSeconds()), isViewOnce, 2, StoryType.NONE, null, false, validatedQuote.orElse(null), contacts.orElse(Collections.emptyList()), linkPreviews.orElse(Collections.emptyList()), mentions.orElse(Collections.emptyList()), Collections.emptySet(), Collections.emptySet(), giftBadge.orElse(null)));
        if (syncMessageDestination.getExpiresInSeconds() != sentTranscriptMessage.getDataMessage().get().getExpiresInSeconds()) {
            handleSynchronizeSentExpirationUpdate(sentTranscriptMessage);
        }
        long orCreateThreadIdFor = SignalDatabase.threads().getOrCreateThreadIdFor(syncMessageDestination);
        mms.beginTransaction();
        try {
            long insertMessageOutbox = mms.insertMessageOutbox((OutgoingMediaMessage) outgoingSecureMediaMessage, orCreateThreadIdFor, false, -1, (MessageDatabase.InsertListener) null);
            if (syncMessageDestination.isGroup()) {
                updateGroupReceiptStatus(sentTranscriptMessage, insertMessageOutbox, syncMessageDestination.requireGroupId());
            } else {
                mms.markUnidentified(insertMessageOutbox, isUnidentified(sentTranscriptMessage, syncMessageDestination));
            }
            mms.markAsSent(insertMessageOutbox, true);
            List<DatabaseAttachment> attachmentsForMessage = SignalDatabase.attachments().getAttachmentsForMessage(insertMessageOutbox);
            List<DatabaseAttachment> list2 = Stream.of(attachmentsForMessage).filter(new MessageContentProcessor$$ExternalSyntheticLambda2()).toList();
            List<DatabaseAttachment> list3 = Stream.of(attachmentsForMessage).filterNot(new MessageContentProcessor$$ExternalSyntheticLambda2()).toList();
            if (sentTranscriptMessage.getDataMessage().get().getExpiresInSeconds() > 0) {
                mms.markExpireStarted(insertMessageOutbox, sentTranscriptMessage.getExpirationStartTimestamp());
                ApplicationDependencies.getExpiringMessageManager().scheduleDeletion(insertMessageOutbox, true, sentTranscriptMessage.getExpirationStartTimestamp(), timeUnit.toMillis((long) sentTranscriptMessage.getDataMessage().get().getExpiresInSeconds()));
            }
            if (syncMessageDestination.isSelf()) {
                MessageDatabase.SyncMessageId syncMessageId = new MessageDatabase.SyncMessageId(syncMessageDestination.getId(), sentTranscriptMessage.getTimestamp());
                SignalDatabase.mmsSms().incrementDeliveryReceiptCount(syncMessageId, System.currentTimeMillis());
                SignalDatabase.mmsSms().incrementReadReceiptCount(syncMessageId, System.currentTimeMillis());
            }
            mms.setTransactionSuccessful();
            mms.endTransaction();
            for (DatabaseAttachment databaseAttachment : list3) {
                ApplicationDependencies.getJobManager().add(new AttachmentDownloadJob(insertMessageOutbox, databaseAttachment.getAttachmentId(), false));
            }
            forceStickerDownloadIfNecessary(insertMessageOutbox, list2);
            return orCreateThreadIdFor;
        } catch (Throwable th) {
            mms.endTransaction();
            throw th;
        }
    }

    private void handleGroupRecipientUpdate(SentTranscriptMessage sentTranscriptMessage, long j) throws BadGroupIdException {
        log(j, "Group recipient update.");
        Recipient syncMessageDestination = getSyncMessageDestination(sentTranscriptMessage);
        if (!syncMessageDestination.isGroup()) {
            warn("Got recipient update for a non-group message! Skipping.");
            return;
        }
        MessageRecord messageFor = SignalDatabase.mmsSms().getMessageFor(sentTranscriptMessage.getTimestamp(), Recipient.self().getId());
        if (messageFor == null) {
            warn("Got recipient update for non-existing message! Skipping.");
        } else if (!messageFor.isMms()) {
            warn("Recipient update matched a non-MMS message! Skipping.");
        } else {
            updateGroupReceiptStatus(sentTranscriptMessage, messageFor.getId(), syncMessageDestination.requireGroupId());
        }
    }

    private void updateGroupReceiptStatus(SentTranscriptMessage sentTranscriptMessage, long j, GroupId groupId) {
        GroupReceiptDatabase groupReceipts = SignalDatabase.groupReceipts();
        List<RecipientId> list = Stream.of(sentTranscriptMessage.getRecipients()).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda19
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return RecipientId.from((ServiceId) obj);
            }
        }).toList();
        List<Recipient> groupMembers = SignalDatabase.groups().getGroupMembers(groupId, GroupDatabase.MemberSet.FULL_MEMBERS_EXCLUDING_SELF);
        Map map = (Map) Stream.of(groupReceipts.getGroupReceiptInfo(j)).collect(Collectors.toMap(new PushGroupSendJob$$ExternalSyntheticLambda0(), new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda20
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return Integer.valueOf(((GroupReceiptDatabase.GroupReceiptInfo) obj).getStatus());
            }
        }));
        for (RecipientId recipientId : list) {
            if (map.containsKey(recipientId) && ((Integer) map.get(recipientId)).intValue() < 0) {
                groupReceipts.update(recipientId, j, 0, sentTranscriptMessage.getTimestamp());
            } else if (!map.containsKey(recipientId)) {
                groupReceipts.insert(Collections.singletonList(recipientId), j, 0, sentTranscriptMessage.getTimestamp());
            }
        }
        groupReceipts.setUnidentified(Stream.of(groupMembers).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda21
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MessageContentProcessor.lambda$updateGroupReceiptStatus$7(SentTranscriptMessage.this, (Recipient) obj);
            }
        }).toList(), j);
    }

    public static /* synthetic */ Pair lambda$updateGroupReceiptStatus$7(SentTranscriptMessage sentTranscriptMessage, Recipient recipient) {
        return new Pair(recipient.getId(), Boolean.valueOf(sentTranscriptMessage.isUnidentified(recipient.requireServiceId())));
    }

    private MessageId handleTextMessage(SignalServiceContent signalServiceContent, SignalServiceDataMessage signalServiceDataMessage, Optional<Long> optional, Optional<GroupId> optional2, Recipient recipient, Recipient recipient2, long j) throws StorageFailedException {
        Optional<MessageDatabase.InsertResult> optional3;
        log(signalServiceDataMessage.getTimestamp(), "Text message.");
        SmsDatabase sms = SignalDatabase.sms();
        String str = signalServiceDataMessage.getBody().isPresent() ? signalServiceDataMessage.getBody().get() : "";
        handlePossibleExpirationUpdate(signalServiceContent, signalServiceDataMessage, optional2, recipient, recipient2, j);
        if (!optional.isPresent() || signalServiceDataMessage.getGroupContext().isPresent()) {
            notifyTypingStoppedFromIncomingMessage(recipient, recipient2, signalServiceContent.getSenderDevice());
            optional3 = sms.insertMessageInbox(new IncomingEncryptedMessage(new IncomingTextMessage(recipient.getId(), signalServiceContent.getSenderDevice(), signalServiceDataMessage.getTimestamp(), signalServiceContent.getServerReceivedTimestamp(), j, str, optional2, TimeUnit.SECONDS.toMillis((long) signalServiceDataMessage.getExpiresInSeconds()), signalServiceContent.isNeedsReceipt(), signalServiceContent.getServerUuid()), str));
            if (optional.isPresent()) {
                sms.deleteMessage(optional.get().longValue());
            }
        } else {
            optional3 = Optional.of(sms.updateBundleMessageBody(optional.get().longValue(), str));
        }
        if (!optional3.isPresent()) {
            return null;
        }
        ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.forConversation(optional3.get().getThreadId()));
        return new MessageId(optional3.get().getMessageId(), false);
    }

    private long handleSynchronizeSentTextMessage(SentTranscriptMessage sentTranscriptMessage, long j) throws MmsException, BadGroupIdException {
        long j2;
        long j3;
        MessageDatabase messageDatabase;
        long j4;
        log(j, "Synchronize sent text message for " + sentTranscriptMessage.getTimestamp());
        Recipient syncMessageDestination = getSyncMessageDestination(sentTranscriptMessage);
        String orElse = sentTranscriptMessage.getDataMessage().get().getBody().orElse("");
        long millis = TimeUnit.SECONDS.toMillis((long) sentTranscriptMessage.getDataMessage().get().getExpiresInSeconds());
        if (syncMessageDestination.getExpiresInSeconds() != sentTranscriptMessage.getDataMessage().get().getExpiresInSeconds()) {
            handleSynchronizeSentExpirationUpdate(sentTranscriptMessage);
        }
        long orCreateThreadIdFor = SignalDatabase.threads().getOrCreateThreadIdFor(syncMessageDestination);
        boolean isGroup = syncMessageDestination.isGroup();
        if (isGroup) {
            j2 = orCreateThreadIdFor;
            long insertMessageOutbox = SignalDatabase.mms().insertMessageOutbox((OutgoingMediaMessage) new OutgoingSecureMediaMessage(new OutgoingMediaMessage(syncMessageDestination, new SlideDeck(), orElse, sentTranscriptMessage.getTimestamp(), -1, millis, false, 2, StoryType.NONE, null, false, null, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), null)), j2, false, -1, (MessageDatabase.InsertListener) null);
            messageDatabase = SignalDatabase.mms();
            updateGroupReceiptStatus(sentTranscriptMessage, insertMessageOutbox, syncMessageDestination.requireGroupId());
            j3 = insertMessageOutbox;
            j4 = millis;
        } else {
            j2 = orCreateThreadIdFor;
            j4 = millis;
            long insertMessageOutbox2 = SignalDatabase.sms().insertMessageOutbox(j2, (OutgoingTextMessage) new OutgoingEncryptedMessage(syncMessageDestination, orElse, j4), false, sentTranscriptMessage.getTimestamp(), (MessageDatabase.InsertListener) null);
            SmsDatabase sms = SignalDatabase.sms();
            sms.markUnidentified(insertMessageOutbox2, isUnidentified(sentTranscriptMessage, syncMessageDestination));
            j3 = insertMessageOutbox2;
            messageDatabase = sms;
        }
        SignalDatabase.threads().update(j2, true);
        messageDatabase.markAsSent(j3, true);
        if (j4 > 0) {
            messageDatabase.markExpireStarted(j3, sentTranscriptMessage.getExpirationStartTimestamp());
            ApplicationDependencies.getExpiringMessageManager().scheduleDeletion(j3, isGroup, sentTranscriptMessage.getExpirationStartTimestamp(), j4);
        }
        if (syncMessageDestination.isSelf()) {
            MessageDatabase.SyncMessageId syncMessageId = new MessageDatabase.SyncMessageId(syncMessageDestination.getId(), sentTranscriptMessage.getTimestamp());
            SignalDatabase.mmsSms().incrementDeliveryReceiptCount(syncMessageId, System.currentTimeMillis());
            SignalDatabase.mmsSms().incrementReadReceiptCount(syncMessageId, System.currentTimeMillis());
        }
        return j2;
    }

    private void handleInvalidVersionMessage(String str, int i, long j, Optional<Long> optional) {
        log(j, "Invalid version message.");
        SmsDatabase sms = SignalDatabase.sms();
        if (!optional.isPresent()) {
            Optional<MessageDatabase.InsertResult> insertPlaceholder = insertPlaceholder(str, i, j);
            if (insertPlaceholder.isPresent()) {
                sms.markAsInvalidVersionKeyExchange(insertPlaceholder.get().getMessageId());
                ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.forConversation(insertPlaceholder.get().getThreadId()));
                return;
            }
            return;
        }
        sms.markAsInvalidVersionKeyExchange(optional.get().longValue());
    }

    private void handleCorruptMessage(String str, int i, long j, Optional<Long> optional) {
        log(j, "Corrupt message.");
        SmsDatabase sms = SignalDatabase.sms();
        if (!optional.isPresent()) {
            Optional<MessageDatabase.InsertResult> insertPlaceholder = insertPlaceholder(str, i, j);
            if (insertPlaceholder.isPresent()) {
                sms.markAsDecryptFailed(insertPlaceholder.get().getMessageId());
                ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.forConversation(insertPlaceholder.get().getThreadId()));
                return;
            }
            return;
        }
        sms.markAsDecryptFailed(optional.get().longValue());
    }

    private void handleUnsupportedDataMessage(String str, int i, Optional<GroupId> optional, long j, Optional<Long> optional2) {
        log(j, "Unsupported data message.");
        SmsDatabase sms = SignalDatabase.sms();
        if (!optional2.isPresent()) {
            Optional<MessageDatabase.InsertResult> insertPlaceholder = insertPlaceholder(str, i, j, optional);
            if (insertPlaceholder.isPresent()) {
                sms.markAsUnsupportedProtocolVersion(insertPlaceholder.get().getMessageId());
                ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.forConversation(insertPlaceholder.get().getThreadId()));
                return;
            }
            return;
        }
        sms.markAsNoSession(optional2.get().longValue());
    }

    private void handleInvalidMessage(SignalServiceAddress signalServiceAddress, int i, Optional<GroupId> optional, long j, Optional<Long> optional2) {
        log(j, "Invalid message.");
        SmsDatabase sms = SignalDatabase.sms();
        if (!optional2.isPresent()) {
            Optional<MessageDatabase.InsertResult> insertPlaceholder = insertPlaceholder(signalServiceAddress.getIdentifier(), i, j, optional);
            if (insertPlaceholder.isPresent()) {
                sms.markAsInvalidMessage(insertPlaceholder.get().getMessageId());
                ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.forConversation(insertPlaceholder.get().getThreadId()));
                return;
            }
            return;
        }
        sms.markAsNoSession(optional2.get().longValue());
    }

    private void handleLegacyMessage(String str, int i, long j, Optional<Long> optional) {
        log(j, "Legacy message.");
        SmsDatabase sms = SignalDatabase.sms();
        if (!optional.isPresent()) {
            Optional<MessageDatabase.InsertResult> insertPlaceholder = insertPlaceholder(str, i, j);
            if (insertPlaceholder.isPresent()) {
                sms.markAsLegacyVersion(insertPlaceholder.get().getMessageId());
                ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.forConversation(insertPlaceholder.get().getThreadId()));
                return;
            }
            return;
        }
        sms.markAsLegacyVersion(optional.get().longValue());
    }

    private void handleProfileKey(SignalServiceContent signalServiceContent, byte[] bArr, Recipient recipient) {
        RecipientDatabase recipients = SignalDatabase.recipients();
        ProfileKey profileKeyOrNull = ProfileKeyUtil.profileKeyOrNull(bArr);
        if (recipient.isSelf()) {
            if (!Objects.equals(ProfileKeyUtil.getSelfProfileKey(), profileKeyOrNull)) {
                warn(signalServiceContent.getTimestamp(), "Saw a sync message whose profile key doesn't match our records. Scheduling a storage sync to check.");
                StorageSyncHelper.scheduleSyncForDataChange();
            }
        } else if (profileKeyOrNull == null) {
            warn(String.valueOf(signalServiceContent.getTimestamp()), "Ignored invalid profile key seen in message");
        } else if (recipients.setProfileKey(recipient.getId(), profileKeyOrNull)) {
            long timestamp = signalServiceContent.getTimestamp();
            log(timestamp, "Profile key on message from " + recipient.getId() + " didn't match our local store. It has been updated.");
            ApplicationDependencies.getJobManager().add(RetrieveProfileJob.forRecipient(recipient.getId()));
        }
    }

    private void handleNeedsDeliveryReceipt(SignalServiceContent signalServiceContent, SignalServiceDataMessage signalServiceDataMessage, MessageId messageId) {
        ApplicationDependencies.getJobManager().add(new SendDeliveryReceiptJob(RecipientId.from(signalServiceContent.getSender()), signalServiceDataMessage.getTimestamp(), messageId));
    }

    private void handleViewedReceipt(SignalServiceContent signalServiceContent, SignalServiceReceiptMessage signalServiceReceiptMessage, Recipient recipient) {
        Collection<MessageDatabase.SyncMessageId> collection;
        boolean z = FeatureFlags.stories() && !SignalStore.storyValues().isFeatureDisabled() && !TextSecurePreferences.isReadReceiptsEnabled(this.context);
        if (TextSecurePreferences.isReadReceiptsEnabled(this.context) || z) {
            String str = TAG;
            log(str, "Processing viewed receipts. Sender: " + recipient.getId() + ", Device: " + signalServiceContent.getSenderDevice() + ", Only Stories: " + z + ", Timestamps: " + Util.join(signalServiceReceiptMessage.getTimestamps(), ", "));
            List<MessageDatabase.SyncMessageId> list = Stream.of(signalServiceReceiptMessage.getTimestamps()).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda15
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return MessageContentProcessor.lambda$handleViewedReceipt$8(Recipient.this, (Long) obj);
                }
            }).toList();
            if (z) {
                collection = SignalDatabase.mmsSms().incrementViewedStoryReceiptCounts(list, signalServiceContent.getTimestamp());
            } else {
                collection = SignalDatabase.mmsSms().incrementViewedReceiptCounts(list, signalServiceContent.getTimestamp());
            }
            HashSet hashSet = new HashSet(list);
            hashSet.removeAll(collection);
            SignalDatabase.mmsSms().updateViewedStories(hashSet);
            for (MessageDatabase.SyncMessageId syncMessageId : collection) {
                String valueOf = String.valueOf(signalServiceContent.getTimestamp());
                warn(valueOf, "[handleViewedReceipt] Could not find matching message! timestamp: " + syncMessageId.getTimetamp() + "  author: " + syncMessageId.getRecipientId());
                if (!this.processingEarlyContent) {
                    ApplicationDependencies.getEarlyMessageCache().store(syncMessageId.getRecipientId(), syncMessageId.getTimetamp(), signalServiceContent);
                }
            }
            if (collection.size() > 0 && !this.processingEarlyContent) {
                PushProcessEarlyMessagesJob.enqueue();
                return;
            }
            return;
        }
        log("Ignoring viewed receipts for IDs: " + Util.join(signalServiceReceiptMessage.getTimestamps(), ", "));
    }

    public static /* synthetic */ MessageDatabase.SyncMessageId lambda$handleViewedReceipt$8(Recipient recipient, Long l) {
        return new MessageDatabase.SyncMessageId(recipient.getId(), l.longValue());
    }

    private void handleDeliveryReceipt(SignalServiceContent signalServiceContent, SignalServiceReceiptMessage signalServiceReceiptMessage, Recipient recipient) {
        String str = TAG;
        log(str, "Processing delivery receipts. Sender: " + recipient.getId() + ", Device: " + signalServiceContent.getSenderDevice() + ", Timestamps: " + Util.join(signalServiceReceiptMessage.getTimestamps(), ", "));
        Collection<MessageDatabase.SyncMessageId> incrementDeliveryReceiptCounts = SignalDatabase.mmsSms().incrementDeliveryReceiptCounts(Stream.of(signalServiceReceiptMessage.getTimestamps()).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda23
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MessageContentProcessor.lambda$handleDeliveryReceipt$9(Recipient.this, (Long) obj);
            }
        }).toList(), System.currentTimeMillis());
        for (MessageDatabase.SyncMessageId syncMessageId : incrementDeliveryReceiptCounts) {
            String valueOf = String.valueOf(signalServiceContent.getTimestamp());
            warn(valueOf, "[handleDeliveryReceipt] Could not find matching message! timestamp: " + syncMessageId.getTimetamp() + "  author: " + syncMessageId.getRecipientId());
        }
        if (incrementDeliveryReceiptCounts.size() > 0) {
            PushProcessEarlyMessagesJob.enqueue();
        }
        SignalDatabase.messageLog().deleteEntriesForRecipient(signalServiceReceiptMessage.getTimestamps(), recipient.getId(), signalServiceContent.getSenderDevice());
    }

    public static /* synthetic */ MessageDatabase.SyncMessageId lambda$handleDeliveryReceipt$9(Recipient recipient, Long l) {
        return new MessageDatabase.SyncMessageId(recipient.getId(), l.longValue());
    }

    private void handleReadReceipt(SignalServiceContent signalServiceContent, SignalServiceReceiptMessage signalServiceReceiptMessage, Recipient recipient) {
        if (!TextSecurePreferences.isReadReceiptsEnabled(this.context)) {
            log("Ignoring read receipts for IDs: " + Util.join(signalServiceReceiptMessage.getTimestamps(), ", "));
            return;
        }
        String str = TAG;
        log(str, "Processing read receipts. Sender: " + recipient.getId() + ", Device: " + signalServiceContent.getSenderDevice() + ", Timestamps: " + Util.join(signalServiceReceiptMessage.getTimestamps(), ", "));
        Collection<MessageDatabase.SyncMessageId> incrementReadReceiptCounts = SignalDatabase.mmsSms().incrementReadReceiptCounts(Stream.of(signalServiceReceiptMessage.getTimestamps()).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda13
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MessageContentProcessor.lambda$handleReadReceipt$10(Recipient.this, (Long) obj);
            }
        }).toList(), signalServiceContent.getTimestamp());
        for (MessageDatabase.SyncMessageId syncMessageId : incrementReadReceiptCounts) {
            String valueOf = String.valueOf(signalServiceContent.getTimestamp());
            warn(valueOf, "[handleReadReceipt] Could not find matching message! timestamp: " + syncMessageId.getTimetamp() + "  author: " + syncMessageId.getRecipientId());
            if (!this.processingEarlyContent) {
                ApplicationDependencies.getEarlyMessageCache().store(syncMessageId.getRecipientId(), syncMessageId.getTimetamp(), signalServiceContent);
            }
        }
        if (incrementReadReceiptCounts.size() > 0 && !this.processingEarlyContent) {
            PushProcessEarlyMessagesJob.enqueue();
        }
    }

    public static /* synthetic */ MessageDatabase.SyncMessageId lambda$handleReadReceipt$10(Recipient recipient, Long l) {
        return new MessageDatabase.SyncMessageId(recipient.getId(), l.longValue());
    }

    private void handleTypingMessage(SignalServiceContent signalServiceContent, SignalServiceTypingMessage signalServiceTypingMessage, Recipient recipient) throws BadGroupIdException {
        long j;
        if (TextSecurePreferences.isTypingIndicatorsEnabled(this.context)) {
            if (signalServiceTypingMessage.getGroupId().isPresent()) {
                GroupId.Push push = GroupId.push(signalServiceTypingMessage.getGroupId().get());
                if (!SignalDatabase.groups().isCurrentMember(push, recipient.getId())) {
                    String valueOf = String.valueOf(signalServiceContent.getTimestamp());
                    warn(valueOf, "Seen typing indicator for non-member " + recipient.getId());
                    return;
                }
                j = SignalDatabase.threads().getOrCreateThreadIdFor(Recipient.externalPossiblyMigratedGroup(push));
            } else {
                j = SignalDatabase.threads().getOrCreateThreadIdFor(recipient);
            }
            if (j <= 0) {
                warn(String.valueOf(signalServiceContent.getTimestamp()), "Couldn't find a matching thread for a typing message.");
            } else if (signalServiceTypingMessage.isTypingStarted()) {
                String str = TAG;
                Log.d(str, "Typing started on thread " + j);
                ApplicationDependencies.getTypingStatusRepository().onTypingStarted(this.context, j, recipient, signalServiceContent.getSenderDevice());
            } else {
                String str2 = TAG;
                Log.d(str2, "Typing stopped on thread " + j);
                ApplicationDependencies.getTypingStatusRepository().onTypingStopped(this.context, j, recipient, signalServiceContent.getSenderDevice(), false);
            }
        }
    }

    private void handleRetryReceipt(SignalServiceContent signalServiceContent, DecryptionErrorMessage decryptionErrorMessage, Recipient recipient) {
        if (!FeatureFlags.retryReceipts()) {
            warn(String.valueOf(signalServiceContent.getTimestamp()), "[RetryReceipt] Feature flag disabled, skipping retry receipt.");
        } else if (decryptionErrorMessage.getDeviceId() != SignalStore.account().getDeviceId()) {
            log(String.valueOf(signalServiceContent.getTimestamp()), "[RetryReceipt] Received a DecryptionErrorMessage targeting a linked device. Ignoring.");
        } else {
            long timestamp = decryptionErrorMessage.getTimestamp();
            long timestamp2 = signalServiceContent.getTimestamp();
            warn(timestamp2, "[RetryReceipt] Received a retry receipt from " + formatSender(recipient, signalServiceContent) + " for message with timestamp " + timestamp + ".");
            if (!recipient.hasServiceId()) {
                long timestamp3 = signalServiceContent.getTimestamp();
                warn(timestamp3, "[RetryReceipt] Requester " + recipient.getId() + " somehow has no UUID! timestamp: " + timestamp);
                return;
            }
            MessageLogEntry logEntry = SignalDatabase.messageLog().getLogEntry(recipient.getId(), signalServiceContent.getSenderDevice(), timestamp);
            if (decryptionErrorMessage.getRatchetKey().isPresent()) {
                handleIndividualRetryReceipt(recipient, logEntry, signalServiceContent, decryptionErrorMessage);
            } else {
                handleSenderKeyRetryReceipt(recipient, logEntry, signalServiceContent, decryptionErrorMessage);
            }
        }
    }

    private void handleSenderKeyRetryReceipt(Recipient recipient, MessageLogEntry messageLogEntry, SignalServiceContent signalServiceContent, DecryptionErrorMessage decryptionErrorMessage) {
        long timestamp = decryptionErrorMessage.getTimestamp();
        MessageRecord findRetryReceiptRelatedMessage = findRetryReceiptRelatedMessage(this.context, messageLogEntry, timestamp);
        if (findRetryReceiptRelatedMessage == null) {
            warn(signalServiceContent.getTimestamp(), "[RetryReceipt-SK] The related message could not be found! There shouldn't be any sender key resends where we can't find the related message. Skipping.");
            return;
        }
        Recipient recipientForThreadId = SignalDatabase.threads().getRecipientForThreadId(findRetryReceiptRelatedMessage.getThreadId());
        if (recipientForThreadId == null) {
            warn(signalServiceContent.getTimestamp(), "[RetryReceipt-SK] Could not find a thread recipient! Skipping.");
        } else if (!recipientForThreadId.isPushV2Group()) {
            warn(signalServiceContent.getTimestamp(), "[RetryReceipt-SK] Thread recipient is not a v2 group! Skipping.");
        } else {
            GroupId.V2 requireV2 = recipientForThreadId.requireGroupId().requireV2();
            DistributionId orCreateDistributionId = SignalDatabase.groups().getOrCreateDistributionId(requireV2);
            SignalProtocolAddress signalProtocolAddress = new SignalProtocolAddress(recipient.requireServiceId().toString(), signalServiceContent.getSenderDevice());
            SignalDatabase.senderKeyShared().delete(orCreateDistributionId, Collections.singleton(signalProtocolAddress));
            if (messageLogEntry != null) {
                long timestamp2 = signalServiceContent.getTimestamp();
                warn(timestamp2, "[RetryReceipt-SK] Found MSL entry for " + recipient.getId() + " (" + signalProtocolAddress + ") with timestamp " + timestamp + ". Scheduling a resend.");
                ApplicationDependencies.getJobManager().add(new ResendMessageJob(messageLogEntry.getRecipientId(), messageLogEntry.getDateSent(), messageLogEntry.getContent(), messageLogEntry.getContentHint(), requireV2, orCreateDistributionId));
                return;
            }
            long timestamp3 = signalServiceContent.getTimestamp();
            warn(timestamp3, "[RetryReceipt-SK] Unable to find MSL entry for " + recipient.getId() + " (" + signalProtocolAddress + ") with timestamp " + timestamp + ".");
            Optional<GroupDatabase.GroupRecord> group = SignalDatabase.groups().getGroup(requireV2);
            if (!group.isPresent()) {
                warn(signalServiceContent.getTimestamp(), "[RetryReceipt-SK] Could not find a record for the group!");
            } else if (!group.get().getMembers().contains(recipient.getId())) {
                warn(signalServiceContent.getTimestamp(), "[RetryReceipt-SK] The requester is not in the group, so we cannot send them a SenderKeyDistributionMessage.");
            } else {
                warn(signalServiceContent.getTimestamp(), "[RetryReceipt-SK] The requester is in the group, so we'll send them a SenderKeyDistributionMessage.");
                ApplicationDependencies.getJobManager().add(new SenderKeyDistributionSendJob(recipient.getId(), group.get().getId().requireV2()));
            }
        }
    }

    private void handleIndividualRetryReceipt(Recipient recipient, MessageLogEntry messageLogEntry, SignalServiceContent signalServiceContent, DecryptionErrorMessage decryptionErrorMessage) {
        boolean z;
        if (!decryptionErrorMessage.getRatchetKey().isPresent() || !ratchetKeyMatches(recipient, signalServiceContent.getSenderDevice(), decryptionErrorMessage.getRatchetKey().get())) {
            z = false;
        } else {
            warn(signalServiceContent.getTimestamp(), "[RetryReceipt-I] Ratchet key matches. Archiving the session.");
            ApplicationDependencies.getProtocolStore().aci().sessions().archiveSession(recipient.getId(), signalServiceContent.getSenderDevice());
            z = true;
        }
        if (messageLogEntry != null) {
            warn(signalServiceContent.getTimestamp(), "[RetryReceipt-I] Found an entry in the MSL. Resending.");
            ApplicationDependencies.getJobManager().add(new ResendMessageJob(messageLogEntry.getRecipientId(), messageLogEntry.getDateSent(), messageLogEntry.getContent(), messageLogEntry.getContentHint(), null, null));
        } else if (z) {
            warn(signalServiceContent.getTimestamp(), "[RetryReceipt-I] Could not find an entry in the MSL, but we archived the session, so we're sending a null message to complete the reset.");
            ApplicationDependencies.getJobManager().add(new NullMessageSendJob(recipient.getId()));
        } else {
            warn(signalServiceContent.getTimestamp(), "[RetryReceipt-I] Could not find an entry in the MSL. Skipping.");
        }
    }

    private MessageRecord findRetryReceiptRelatedMessage(Context context, MessageLogEntry messageLogEntry, long j) {
        if (messageLogEntry == null || !messageLogEntry.hasRelatedMessage()) {
            return SignalDatabase.mmsSms().getMessageFor(j, Recipient.self().getId());
        }
        MessageId messageId = messageLogEntry.getRelatedMessages().get(0);
        if (messageId.isMms()) {
            return SignalDatabase.mms().getMessageRecordOrNull(messageId.getId());
        }
        return SignalDatabase.sms().getMessageRecordOrNull(messageId.getId());
    }

    public static boolean ratchetKeyMatches(Recipient recipient, int i, ECPublicKey eCPublicKey) {
        return ApplicationDependencies.getProtocolStore().aci().loadSession(recipient.resolve().requireServiceId().toProtocolAddress(i)).currentRatchetKeyMatches(eCPublicKey);
    }

    private static boolean isInvalidMessage(SignalServiceDataMessage signalServiceDataMessage) {
        if (!signalServiceDataMessage.isViewOnce()) {
            return false;
        }
        List<SignalServiceAttachment> orElse = signalServiceDataMessage.getAttachments().orElse(Collections.emptyList());
        if (orElse.size() != 1 || !isViewOnceSupportedContentType(orElse.get(0).getContentType().toLowerCase())) {
            return true;
        }
        return false;
    }

    private static boolean isViewOnceSupportedContentType(String str) {
        return MediaUtil.isImageType(str) || MediaUtil.isVideoType(str);
    }

    private Optional<QuoteModel> getValidatedQuote(Optional<SignalServiceDataMessage.Quote> optional) {
        if (!optional.isPresent()) {
            return Optional.empty();
        }
        if (optional.get().getId() <= 0) {
            warn("Received quote without an ID! Ignoring...");
            return Optional.empty();
        } else if (optional.get().getAuthor() == null) {
            warn("Received quote without an author! Ignoring...");
            return Optional.empty();
        } else {
            RecipientId id = Recipient.externalPush(optional.get().getAuthor()).getId();
            MessageRecord messageFor = SignalDatabase.mmsSms().getMessageFor(optional.get().getId(), id);
            if (messageFor == null || messageFor.isRemoteDelete()) {
                if (messageFor != null) {
                    warn("Found the target for the quote, but it's flagged as remotely deleted.");
                }
                warn("Didn't find matching message record...");
                return Optional.of(new QuoteModel(optional.get().getId(), id, optional.get().getText(), true, PointerAttachment.forPointers(optional.get().getAttachments()), getMentions(optional.get().getMentions()), QuoteModel.Type.fromDataMessageType(optional.get().getType())));
            }
            log("Found matching message record...");
            List<Attachment> linkedList = new LinkedList<>();
            LinkedList linkedList2 = new LinkedList();
            if (messageFor.isMms()) {
                MmsMessageRecord mmsMessageRecord = (MmsMessageRecord) messageFor;
                linkedList2.addAll(SignalDatabase.mentions().getMentionsForMessage(mmsMessageRecord.getId()));
                if (mmsMessageRecord.isViewOnce()) {
                    linkedList.add(new TombstoneAttachment(MediaUtil.VIEW_ONCE, true));
                } else {
                    linkedList = mmsMessageRecord.getSlideDeck().asAttachments();
                    if (linkedList.isEmpty()) {
                        linkedList.addAll(Stream.of(mmsMessageRecord.getLinkPreviews()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda25
                            @Override // com.annimon.stream.function.Predicate
                            public final boolean test(Object obj) {
                                return MessageContentProcessor.lambda$getValidatedQuote$11((LinkPreview) obj);
                            }
                        }).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda26
                            @Override // com.annimon.stream.function.Function
                            public final Object apply(Object obj) {
                                return MessageContentProcessor.lambda$getValidatedQuote$12((LinkPreview) obj);
                            }
                        }).toList());
                    }
                }
            }
            return Optional.of(new QuoteModel(optional.get().getId(), id, messageFor.getBody(), false, linkedList, linkedList2, QuoteModel.Type.fromDataMessageType(optional.get().getType())));
        }
    }

    public static /* synthetic */ boolean lambda$getValidatedQuote$11(LinkPreview linkPreview) {
        return linkPreview.getThumbnail().isPresent();
    }

    public static /* synthetic */ Attachment lambda$getValidatedQuote$12(LinkPreview linkPreview) {
        return linkPreview.getThumbnail().get();
    }

    private Optional<Attachment> getStickerAttachment(Optional<SignalServiceDataMessage.Sticker> optional) {
        if (!optional.isPresent()) {
            return Optional.empty();
        }
        if (optional.get().getPackId() == null || optional.get().getPackKey() == null || optional.get().getAttachment() == null) {
            warn("Malformed sticker!");
            return Optional.empty();
        }
        StickerLocator stickerLocator = new StickerLocator(Hex.toStringCondensed(optional.get().getPackId()), Hex.toStringCondensed(optional.get().getPackKey()), optional.get().getStickerId(), optional.get().getEmoji());
        StickerRecord sticker = SignalDatabase.stickers().getSticker(stickerLocator.getPackId(), stickerLocator.getStickerId(), false);
        if (sticker != null) {
            return Optional.of(new UriAttachment(sticker.getUri(), sticker.getContentType(), 0, sticker.getSize(), 512, 512, null, String.valueOf(new SecureRandom().nextLong()), false, false, false, false, null, stickerLocator, null, null, null));
        }
        return Optional.of(PointerAttachment.forPointer(Optional.of(optional.get().getAttachment()), stickerLocator).get());
    }

    private static Optional<List<Contact>> getContacts(Optional<List<SharedContact>> optional) {
        if (!optional.isPresent()) {
            return Optional.empty();
        }
        ArrayList arrayList = new ArrayList(optional.get().size());
        for (SharedContact sharedContact : optional.get()) {
            arrayList.add(ContactModelMapper.remoteToLocal(sharedContact));
        }
        return Optional.of(arrayList);
    }

    private Optional<List<LinkPreview>> getLinkPreviews(Optional<List<SignalServicePreview>> optional, String str, boolean z) {
        if (!optional.isPresent() || optional.get().isEmpty()) {
            return Optional.empty();
        }
        ArrayList arrayList = new ArrayList(optional.get().size());
        LinkPreviewUtil.Links findValidPreviewUrls = LinkPreviewUtil.findValidPreviewUrls(str);
        for (SignalServicePreview signalServicePreview : optional.get()) {
            Optional<Attachment> forPointer = PointerAttachment.forPointer(signalServicePreview.getImage());
            Optional ofNullable = Optional.ofNullable(signalServicePreview.getUrl());
            Optional ofNullable2 = Optional.ofNullable(signalServicePreview.getTitle());
            Optional ofNullable3 = Optional.ofNullable(signalServicePreview.getDescription());
            boolean z2 = !TextUtils.isEmpty((CharSequence) ofNullable2.orElse(""));
            boolean z3 = ofNullable.isPresent() && findValidPreviewUrls.containsUrl((String) ofNullable.get());
            boolean z4 = ofNullable.isPresent() && LinkUtil.isValidPreviewUrl((String) ofNullable.get());
            if (!z2 || ((!z3 && !z) || !z4)) {
                warn(String.format("Discarding an invalid link preview. hasTitle: %b presentInBody: %b validDomain: %b", Boolean.valueOf(z2), Boolean.valueOf(z3), Boolean.valueOf(z4)));
            } else {
                arrayList.add(new LinkPreview((String) ofNullable.get(), (String) ofNullable2.orElse(""), (String) ofNullable3.orElse(""), signalServicePreview.getDate(), forPointer));
            }
        }
        return Optional.of(arrayList);
    }

    private Optional<List<Mention>> getMentions(Optional<List<SignalServiceDataMessage.Mention>> optional) {
        if (!optional.isPresent()) {
            return Optional.empty();
        }
        return Optional.of(getMentions(optional.get()));
    }

    private List<Mention> getMentions(List<SignalServiceDataMessage.Mention> list) {
        if (list == null || list.isEmpty()) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (SignalServiceDataMessage.Mention mention : list) {
            arrayList.add(new Mention(Recipient.externalPush(mention.getServiceId()).getId(), mention.getStart(), mention.getLength()));
        }
        return arrayList;
    }

    private Optional<GiftBadge> getGiftBadge(Optional<SignalServiceDataMessage.GiftBadge> optional) {
        if (!optional.isPresent()) {
            return Optional.empty();
        }
        return Optional.of(GiftBadge.newBuilder().setRedemptionToken(ByteString.copyFrom(optional.get().getReceiptCredentialPresentation().serialize())).build());
    }

    private Optional<MessageDatabase.InsertResult> insertPlaceholder(String str, int i, long j) {
        return insertPlaceholder(str, i, j, Optional.empty());
    }

    private Optional<MessageDatabase.InsertResult> insertPlaceholder(String str, int i, long j, Optional<GroupId> optional) {
        return SignalDatabase.sms().insertMessageInbox(new IncomingEncryptedMessage(new IncomingTextMessage(Recipient.external(this.context, str).getId(), i, j, -1, System.currentTimeMillis(), "", optional, 0, false, null), ""));
    }

    private Recipient getSyncMessageDestination(SentTranscriptMessage sentTranscriptMessage) throws BadGroupIdException {
        return getGroupRecipient(sentTranscriptMessage.getDataMessage().get().getGroupContext()).orElseGet(new Supplier() { // from class: org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda1
            @Override // j$.util.function.Supplier
            public final Object get() {
                return MessageContentProcessor.lambda$getSyncMessageDestination$13(SentTranscriptMessage.this);
            }
        });
    }

    public static /* synthetic */ Recipient lambda$getSyncMessageDestination$13(SentTranscriptMessage sentTranscriptMessage) {
        return Recipient.externalPush(sentTranscriptMessage.getDestination().get());
    }

    private Recipient getMessageDestination(SignalServiceContent signalServiceContent) throws BadGroupIdException {
        SignalServiceDataMessage orElse = signalServiceContent.getDataMessage().orElse(null);
        return getGroupRecipient(orElse != null ? orElse.getGroupContext() : Optional.empty()).orElseGet(new Supplier() { // from class: org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda14
            @Override // j$.util.function.Supplier
            public final Object get() {
                return MessageContentProcessor.lambda$getMessageDestination$14(SignalServiceContent.this);
            }
        });
    }

    public static /* synthetic */ Recipient lambda$getMessageDestination$14(SignalServiceContent signalServiceContent) {
        return Recipient.externalPush(signalServiceContent.getSender());
    }

    private Optional<Recipient> getGroupRecipient(Optional<SignalServiceGroupContext> optional) throws BadGroupIdException {
        if (optional.isPresent()) {
            return Optional.of(Recipient.externalPossiblyMigratedGroup(GroupUtil.idFromGroupContext(optional.get())));
        }
        return Optional.empty();
    }

    private void notifyTypingStoppedFromIncomingMessage(Recipient recipient, Recipient recipient2, int i) {
        long orCreateThreadIdFor = SignalDatabase.threads().getOrCreateThreadIdFor(recipient2);
        if (orCreateThreadIdFor > 0 && TextSecurePreferences.isTypingIndicatorsEnabled(this.context)) {
            String str = TAG;
            Log.d(str, "Typing stopped on thread " + orCreateThreadIdFor + " due to an incoming message.");
            ApplicationDependencies.getTypingStatusRepository().onTypingStopped(this.context, orCreateThreadIdFor, recipient, i, true);
        }
    }

    private boolean shouldIgnore(SignalServiceContent signalServiceContent, Recipient recipient, Recipient recipient2) throws BadGroupIdException {
        if (signalServiceContent.getDataMessage().isPresent()) {
            SignalServiceDataMessage signalServiceDataMessage = signalServiceContent.getDataMessage().get();
            if (recipient2.isGroup() && recipient2.isBlocked()) {
                return true;
            }
            if (!recipient2.isGroup()) {
                return recipient.isBlocked();
            }
            GroupDatabase groups = SignalDatabase.groups();
            Optional<GroupId> idFromGroupContext = GroupUtil.idFromGroupContext(signalServiceDataMessage.getGroupContext());
            if (idFromGroupContext.isPresent() && idFromGroupContext.get().isV1() && signalServiceDataMessage.isGroupV1Update() && groups.groupExists(idFromGroupContext.get().requireV1().deriveV2MigrationGroupId())) {
                warn(String.valueOf(signalServiceContent.getTimestamp()), "Ignoring V1 update for a group we've already migrated to V2.");
                return true;
            } else if (idFromGroupContext.isPresent() && groups.isUnknownGroup(idFromGroupContext.get())) {
                return recipient.isBlocked();
            } else {
                boolean isPresent = signalServiceDataMessage.getBody().isPresent();
                boolean z = signalServiceDataMessage.getAttachments().isPresent() || signalServiceDataMessage.getQuote().isPresent() || signalServiceDataMessage.getSharedContacts().isPresent() || signalServiceDataMessage.getSticker().isPresent();
                boolean isExpirationUpdate = signalServiceDataMessage.isExpirationUpdate();
                boolean isGroupV2Update = signalServiceDataMessage.isGroupV2Update();
                boolean z2 = !signalServiceDataMessage.isGroupV1Update() && !isGroupV2Update && !isExpirationUpdate && (isPresent || z);
                boolean z3 = idFromGroupContext.isPresent() && groups.isActive(idFromGroupContext.get());
                boolean z4 = signalServiceDataMessage.getGroupContext().isPresent() && signalServiceDataMessage.getGroupContext().get().getGroupV1Type() == SignalServiceGroup.Type.QUIT;
                if ((!z2 || z3) && (!recipient.isBlocked() || z4 || isGroupV2Update)) {
                    return false;
                }
                return true;
            }
        } else if (signalServiceContent.getCallMessage().isPresent()) {
            return recipient.isBlocked();
        } else {
            if (signalServiceContent.getTypingMessage().isPresent()) {
                if (recipient.isBlocked()) {
                    return true;
                }
                if (signalServiceContent.getTypingMessage().get().getGroupId().isPresent()) {
                    GroupId.Push push = GroupId.push(signalServiceContent.getTypingMessage().get().getGroupId().get());
                    Recipient externalPossiblyMigratedGroup = Recipient.externalPossiblyMigratedGroup(push);
                    if (externalPossiblyMigratedGroup.isBlocked() || !externalPossiblyMigratedGroup.isActiveGroup()) {
                        return true;
                    }
                    Optional<GroupDatabase.GroupRecord> group = SignalDatabase.groups().getGroup(push);
                    if (!group.isPresent() || !group.get().isAnnouncementGroup() || group.get().getAdmins().contains(recipient)) {
                        return false;
                    }
                    return true;
                }
            }
            return false;
        }
    }

    private void resetRecipientToPush(Recipient recipient) {
        if (recipient.isForceSmsSelection()) {
            SignalDatabase.recipients().setForceSmsSelection(recipient.getId(), false);
        }
    }

    private void forceStickerDownloadIfNecessary(long j, List<DatabaseAttachment> list) {
        if (!list.isEmpty()) {
            DatabaseAttachment databaseAttachment = list.get(0);
            if (databaseAttachment.getTransferState() != 0) {
                AttachmentDownloadJob attachmentDownloadJob = new AttachmentDownloadJob(j, databaseAttachment.getAttachmentId(), true);
                try {
                    attachmentDownloadJob.setContext(this.context);
                    attachmentDownloadJob.doWork();
                } catch (Exception unused) {
                    warn("Failed to download sticker inline. Scheduling.");
                    ApplicationDependencies.getJobManager().add(attachmentDownloadJob);
                }
            }
        }
    }

    private static boolean isUnidentified(SentTranscriptMessage sentTranscriptMessage, Recipient recipient) {
        if (recipient.hasServiceId()) {
            return sentTranscriptMessage.isUnidentified(recipient.requireServiceId());
        }
        return false;
    }

    private static void log(String str) {
        Log.i(TAG, str);
    }

    private static void log(long j, String str) {
        log(String.valueOf(j), str);
    }

    private static void log(String str, String str2) {
        String str3;
        if (Util.isEmpty(str)) {
            str3 = "";
        } else {
            str3 = "[" + str + "] ";
        }
        Log.i(TAG, str3 + str2);
    }

    private static void warn(String str) {
        warn("", str, (Throwable) null);
    }

    private static void warn(String str, String str2) {
        warn(str, str2, (Throwable) null);
    }

    private static void warn(long j, String str) {
        warn(String.valueOf(j), str);
    }

    private static void warn(long j, String str, Throwable th) {
        warn(String.valueOf(j), str, th);
    }

    private static void warn(String str, Throwable th) {
        warn("", str, th);
    }

    private static void warn(String str, String str2, Throwable th) {
        String str3;
        if (Util.isEmpty(str)) {
            str3 = "";
        } else {
            str3 = "[" + str + "] ";
        }
        Log.w(TAG, str3 + str2, th);
    }

    private static String formatSender(Recipient recipient, SignalServiceContent signalServiceContent) {
        return formatSender(recipient.getId(), signalServiceContent);
    }

    private static String formatSender(RecipientId recipientId, SignalServiceContent signalServiceContent) {
        if (signalServiceContent == null) {
            return recipientId.toString();
        }
        return recipientId + " (" + signalServiceContent.getSender().getIdentifier() + "." + signalServiceContent.getSenderDevice() + ")";
    }

    /* loaded from: classes4.dex */
    public static class StorageFailedException extends Exception {
        private final String sender;
        private final int senderDevice;

        /* synthetic */ StorageFailedException(Exception exc, String str, int i, AnonymousClass1 r4) {
            this(exc, str, i);
        }

        private StorageFailedException(Exception exc, String str, int i) {
            super(exc);
            this.sender = str;
            this.senderDevice = i;
        }

        public String getSender() {
            return this.sender;
        }

        public int getSenderDevice() {
            return this.senderDevice;
        }
    }

    /* loaded from: classes4.dex */
    public static final class ExceptionMetadata {
        private final GroupId groupId;
        private final String sender;
        private final int senderDevice;

        public ExceptionMetadata(String str, int i, GroupId groupId) {
            this.sender = str;
            this.senderDevice = i;
            this.groupId = groupId;
        }

        public ExceptionMetadata(String str, int i) {
            this(str, i, null);
        }

        public String getSender() {
            return this.sender;
        }

        public int getSenderDevice() {
            return this.senderDevice;
        }

        public GroupId getGroupId() {
            return this.groupId;
        }
    }
}
