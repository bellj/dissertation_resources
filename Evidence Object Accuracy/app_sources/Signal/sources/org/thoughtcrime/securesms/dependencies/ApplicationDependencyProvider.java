package org.thoughtcrime.securesms.dependencies;

import android.app.Application;
import android.os.Handler;
import android.os.HandlerThread;
import j$.util.Optional;
import j$.util.function.Supplier;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.concurrent.DeadlockDetector;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.libsignal.protocol.IdentityKeyPair;
import org.signal.libsignal.zkgroup.profiles.ClientZkProfileOperations;
import org.signal.libsignal.zkgroup.receipts.ClientZkReceiptOperations;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.components.TypingStatusRepository;
import org.thoughtcrime.securesms.components.TypingStatusSender;
import org.thoughtcrime.securesms.crypto.ReentrantSessionLock;
import org.thoughtcrime.securesms.crypto.storage.SignalBaseIdentityKeyStore;
import org.thoughtcrime.securesms.crypto.storage.SignalIdentityKeyStore;
import org.thoughtcrime.securesms.crypto.storage.SignalSenderKeyStore;
import org.thoughtcrime.securesms.crypto.storage.SignalServiceAccountDataStoreImpl;
import org.thoughtcrime.securesms.crypto.storage.SignalServiceDataStoreImpl;
import org.thoughtcrime.securesms.crypto.storage.TextSecurePreKeyStore;
import org.thoughtcrime.securesms.crypto.storage.TextSecureSessionStore;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.JobDatabase;
import org.thoughtcrime.securesms.database.MmsSmsColumns;
import org.thoughtcrime.securesms.database.PendingRetryReceiptCache;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobmanager.JobMigrator;
import org.thoughtcrime.securesms.jobmanager.impl.FactoryJobPredicate;
import org.thoughtcrime.securesms.jobmanager.impl.JsonDataSerializer;
import org.thoughtcrime.securesms.jobs.CreateSignedPreKeyJob;
import org.thoughtcrime.securesms.jobs.FastJobStorage;
import org.thoughtcrime.securesms.jobs.GroupCallUpdateSendJob;
import org.thoughtcrime.securesms.jobs.JobManagerFactories;
import org.thoughtcrime.securesms.jobs.MarkerJob;
import org.thoughtcrime.securesms.jobs.PushDecryptMessageJob;
import org.thoughtcrime.securesms.jobs.PushGroupSendJob;
import org.thoughtcrime.securesms.jobs.PushMediaSendJob;
import org.thoughtcrime.securesms.jobs.PushProcessMessageJob;
import org.thoughtcrime.securesms.jobs.PushTextSendJob;
import org.thoughtcrime.securesms.jobs.ReactionSendJob;
import org.thoughtcrime.securesms.jobs.TypingSendJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.megaphone.MegaphoneRepository;
import org.thoughtcrime.securesms.messages.BackgroundMessageRetriever;
import org.thoughtcrime.securesms.messages.IncomingMessageObserver;
import org.thoughtcrime.securesms.messages.IncomingMessageProcessor;
import org.thoughtcrime.securesms.net.SignalWebSocketHealthMonitor;
import org.thoughtcrime.securesms.notifications.MessageNotifier;
import org.thoughtcrime.securesms.notifications.OptimizedMessageNotifier;
import org.thoughtcrime.securesms.payments.MobileCoinConfig;
import org.thoughtcrime.securesms.payments.Payments;
import org.thoughtcrime.securesms.push.SecurityEventListener;
import org.thoughtcrime.securesms.push.SignalServiceNetworkAccess;
import org.thoughtcrime.securesms.recipients.LiveRecipientCache;
import org.thoughtcrime.securesms.revealable.ViewOnceMessageManager;
import org.thoughtcrime.securesms.service.ExpiringMessageManager;
import org.thoughtcrime.securesms.service.ExpiringStoriesManager;
import org.thoughtcrime.securesms.service.PendingRetryReceiptManager;
import org.thoughtcrime.securesms.service.TrimThreadsByDateManager;
import org.thoughtcrime.securesms.service.webrtc.SignalCallManager;
import org.thoughtcrime.securesms.shakereport.ShakeToReport;
import org.thoughtcrime.securesms.util.AlarmSleepTimer;
import org.thoughtcrime.securesms.util.AppForegroundObserver;
import org.thoughtcrime.securesms.util.ByteUnit;
import org.thoughtcrime.securesms.util.EarlyMessageCache;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.FrameRateTracker;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.video.exo.GiphyMp4Cache;
import org.thoughtcrime.securesms.video.exo.SimpleExoPlayerPool;
import org.thoughtcrime.securesms.webrtc.audio.AudioManagerCompat;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.SignalServiceDataStore;
import org.whispersystems.signalservice.api.SignalServiceMessageReceiver;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.SignalWebSocket;
import org.whispersystems.signalservice.api.groupsv2.ClientZkOperations;
import org.whispersystems.signalservice.api.groupsv2.GroupsV2Operations;
import org.whispersystems.signalservice.api.push.ACI;
import org.whispersystems.signalservice.api.push.PNI;
import org.whispersystems.signalservice.api.services.DonationsService;
import org.whispersystems.signalservice.api.services.ProfileService;
import org.whispersystems.signalservice.api.util.CredentialsProvider;
import org.whispersystems.signalservice.api.util.UptimeSleepTimer;
import org.whispersystems.signalservice.api.websocket.WebSocketFactory;
import org.whispersystems.signalservice.internal.websocket.WebSocketConnection;

/* loaded from: classes.dex */
public class ApplicationDependencyProvider implements ApplicationDependencies.Provider {
    private final Application context;

    public ApplicationDependencyProvider(Application application) {
        this.context = application;
    }

    private ClientZkOperations provideClientZkOperations() {
        return ClientZkOperations.create(provideSignalServiceNetworkAccess().getConfiguration());
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public GroupsV2Operations provideGroupsV2Operations() {
        return new GroupsV2Operations(provideClientZkOperations(), FeatureFlags.groupLimits().getHardLimit());
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public SignalServiceAccountManager provideSignalServiceAccountManager() {
        return new SignalServiceAccountManager(provideSignalServiceNetworkAccess().getConfiguration(), new DynamicCredentialsProvider(), BuildConfig.SIGNAL_AGENT, provideGroupsV2Operations(), FeatureFlags.okHttpAutomaticRetry());
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public SignalServiceMessageSender provideSignalServiceMessageSender(SignalWebSocket signalWebSocket, SignalServiceDataStore signalServiceDataStore) {
        return new SignalServiceMessageSender(provideSignalServiceNetworkAccess().getConfiguration(), new DynamicCredentialsProvider(), signalServiceDataStore, ReentrantSessionLock.INSTANCE, BuildConfig.SIGNAL_AGENT, signalWebSocket, Optional.of(new SecurityEventListener(this.context)), provideClientZkOperations().getProfileOperations(), SignalExecutors.newCachedBoundedExecutor("signal-messages", 1, 16, 30), ByteUnit.KILOBYTES.toBytes(256), FeatureFlags.okHttpAutomaticRetry());
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public SignalServiceMessageReceiver provideSignalServiceMessageReceiver() {
        return new SignalServiceMessageReceiver(provideSignalServiceNetworkAccess().getConfiguration(), new DynamicCredentialsProvider(), BuildConfig.SIGNAL_AGENT, provideClientZkOperations().getProfileOperations(), FeatureFlags.okHttpAutomaticRetry());
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public SignalServiceNetworkAccess provideSignalServiceNetworkAccess() {
        return new SignalServiceNetworkAccess(this.context);
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public IncomingMessageProcessor provideIncomingMessageProcessor() {
        return new IncomingMessageProcessor(this.context);
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public BackgroundMessageRetriever provideBackgroundMessageRetriever() {
        return new BackgroundMessageRetriever();
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public LiveRecipientCache provideRecipientCache() {
        return new LiveRecipientCache(this.context);
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public JobManager provideJobManager() {
        return new JobManager(this.context, new JobManager.Configuration.Builder().setDataSerializer(new JsonDataSerializer()).setJobFactories(JobManagerFactories.getJobFactories(this.context)).setConstraintFactories(JobManagerFactories.getConstraintFactories(this.context)).setConstraintObservers(JobManagerFactories.getConstraintObservers(this.context)).setJobStorage(new FastJobStorage(JobDatabase.getInstance(this.context))).setJobMigrator(new JobMigrator(TextSecurePreferences.getJobManagerVersion(this.context), 8, JobManagerFactories.getJobMigrations(this.context))).addReservedJobRunner(new FactoryJobPredicate(PushDecryptMessageJob.KEY, PushProcessMessageJob.KEY, MarkerJob.KEY)).addReservedJobRunner(new FactoryJobPredicate(PushTextSendJob.KEY, PushMediaSendJob.KEY, PushGroupSendJob.KEY, ReactionSendJob.KEY, TypingSendJob.KEY, GroupCallUpdateSendJob.KEY)).build());
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public FrameRateTracker provideFrameRateTracker() {
        return new FrameRateTracker(this.context);
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public MegaphoneRepository provideMegaphoneRepository() {
        return new MegaphoneRepository(this.context);
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public EarlyMessageCache provideEarlyMessageCache() {
        return new EarlyMessageCache();
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public MessageNotifier provideMessageNotifier() {
        return new OptimizedMessageNotifier(this.context);
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public IncomingMessageObserver provideIncomingMessageObserver() {
        return new IncomingMessageObserver(this.context);
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public TrimThreadsByDateManager provideTrimThreadsByDateManager() {
        return new TrimThreadsByDateManager(this.context);
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public ViewOnceMessageManager provideViewOnceMessageManager() {
        return new ViewOnceMessageManager(this.context);
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public ExpiringStoriesManager provideExpiringStoriesManager() {
        return new ExpiringStoriesManager(this.context);
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public ExpiringMessageManager provideExpiringMessageManager() {
        return new ExpiringMessageManager(this.context);
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public TypingStatusRepository provideTypingStatusRepository() {
        return new TypingStatusRepository();
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public TypingStatusSender provideTypingStatusSender() {
        return new TypingStatusSender();
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public DatabaseObserver provideDatabaseObserver() {
        return new DatabaseObserver(this.context);
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public Payments providePayments(SignalServiceAccountManager signalServiceAccountManager) {
        return new Payments(MobileCoinConfig.getMainNet(signalServiceAccountManager));
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public ShakeToReport provideShakeToReport() {
        return new ShakeToReport(this.context);
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public AppForegroundObserver provideAppForegroundObserver() {
        return new AppForegroundObserver();
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public SignalCallManager provideSignalCallManager() {
        return new SignalCallManager(this.context);
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public PendingRetryReceiptManager providePendingRetryReceiptManager() {
        return new PendingRetryReceiptManager(this.context);
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public PendingRetryReceiptCache providePendingRetryReceiptCache() {
        return new PendingRetryReceiptCache();
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public SignalWebSocket provideSignalWebSocket() {
        SignalWebSocketHealthMonitor signalWebSocketHealthMonitor = new SignalWebSocketHealthMonitor(this.context, SignalStore.account().isFcmEnabled() ? new UptimeSleepTimer() : new AlarmSleepTimer(this.context));
        SignalWebSocket signalWebSocket = new SignalWebSocket(provideWebSocketFactory(signalWebSocketHealthMonitor));
        signalWebSocketHealthMonitor.monitor(signalWebSocket);
        return signalWebSocket;
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public SignalServiceDataStoreImpl provideProtocolStore() {
        ACI aci = SignalStore.account().getAci();
        PNI pni = SignalStore.account().getPni();
        if (aci == null) {
            throw new IllegalStateException("No ACI set!");
        } else if (pni != null) {
            boolean z = false;
            boolean z2 = true;
            if (!SignalStore.account().hasAciIdentityKey()) {
                SignalStore.account().generateAciIdentityKeyIfNecessary();
                z = true;
            }
            if (!SignalStore.account().hasPniIdentityKey()) {
                SignalStore.account().generatePniIdentityKeyIfNecessary();
            } else {
                z2 = z;
            }
            if (z2) {
                CreateSignedPreKeyJob.enqueueIfNeeded();
            }
            SignalBaseIdentityKeyStore signalBaseIdentityKeyStore = new SignalBaseIdentityKeyStore(this.context);
            return new SignalServiceDataStoreImpl(this.context, new SignalServiceAccountDataStoreImpl(this.context, new TextSecurePreKeyStore(aci), new SignalIdentityKeyStore(signalBaseIdentityKeyStore, new Supplier() { // from class: org.thoughtcrime.securesms.dependencies.ApplicationDependencyProvider$$ExternalSyntheticLambda0
                @Override // j$.util.function.Supplier
                public final Object get() {
                    return ApplicationDependencyProvider.lambda$provideProtocolStore$0();
                }
            }), new TextSecureSessionStore(aci), new SignalSenderKeyStore(this.context)), new SignalServiceAccountDataStoreImpl(this.context, new TextSecurePreKeyStore(pni), new SignalIdentityKeyStore(signalBaseIdentityKeyStore, new Supplier() { // from class: org.thoughtcrime.securesms.dependencies.ApplicationDependencyProvider$$ExternalSyntheticLambda1
                @Override // j$.util.function.Supplier
                public final Object get() {
                    return ApplicationDependencyProvider.lambda$provideProtocolStore$1();
                }
            }), new TextSecureSessionStore(pni), new SignalSenderKeyStore(this.context)));
        } else {
            throw new IllegalStateException("No PNI set!");
        }
    }

    public static /* synthetic */ IdentityKeyPair lambda$provideProtocolStore$0() {
        return SignalStore.account().getAciIdentityKey();
    }

    public static /* synthetic */ IdentityKeyPair lambda$provideProtocolStore$1() {
        return SignalStore.account().getPniIdentityKey();
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public GiphyMp4Cache provideGiphyMp4Cache() {
        return new GiphyMp4Cache(ByteUnit.MEGABYTES.toBytes(16));
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public SimpleExoPlayerPool provideExoPlayerPool() {
        return new SimpleExoPlayerPool(this.context);
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public AudioManagerCompat provideAndroidCallAudioManager() {
        return AudioManagerCompat.create(this.context);
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public DonationsService provideDonationsService() {
        return new DonationsService(provideSignalServiceNetworkAccess().getConfiguration(), new DynamicCredentialsProvider(), BuildConfig.SIGNAL_AGENT, provideGroupsV2Operations(), FeatureFlags.okHttpAutomaticRetry());
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public ProfileService provideProfileService(ClientZkProfileOperations clientZkProfileOperations, SignalServiceMessageReceiver signalServiceMessageReceiver, SignalWebSocket signalWebSocket) {
        return new ProfileService(clientZkProfileOperations, signalServiceMessageReceiver, signalWebSocket);
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public DeadlockDetector provideDeadlockDetector() {
        HandlerThread handlerThread = new HandlerThread("signal-DeadlockDetector");
        handlerThread.start();
        return new DeadlockDetector(new Handler(handlerThread.getLooper()), TimeUnit.SECONDS.toMillis(5));
    }

    @Override // org.thoughtcrime.securesms.dependencies.ApplicationDependencies.Provider
    public ClientZkReceiptOperations provideClientZkReceiptOperations() {
        return provideClientZkOperations().getReceiptOperations();
    }

    private WebSocketFactory provideWebSocketFactory(final SignalWebSocketHealthMonitor signalWebSocketHealthMonitor) {
        return new WebSocketFactory() { // from class: org.thoughtcrime.securesms.dependencies.ApplicationDependencyProvider.1
            @Override // org.whispersystems.signalservice.api.websocket.WebSocketFactory
            public WebSocketConnection createWebSocket() {
                return new WebSocketConnection("normal", ApplicationDependencyProvider.this.provideSignalServiceNetworkAccess().getConfiguration(), Optional.of(new DynamicCredentialsProvider()), BuildConfig.SIGNAL_AGENT, signalWebSocketHealthMonitor);
            }

            @Override // org.whispersystems.signalservice.api.websocket.WebSocketFactory
            public WebSocketConnection createUnidentifiedWebSocket() {
                return new WebSocketConnection(MmsSmsColumns.UNIDENTIFIED, ApplicationDependencyProvider.this.provideSignalServiceNetworkAccess().getConfiguration(), Optional.empty(), BuildConfig.SIGNAL_AGENT, signalWebSocketHealthMonitor);
            }
        };
    }

    /* loaded from: classes4.dex */
    private static class DynamicCredentialsProvider implements CredentialsProvider {
        private DynamicCredentialsProvider() {
        }

        @Override // org.whispersystems.signalservice.api.util.CredentialsProvider
        public ACI getAci() {
            return SignalStore.account().getAci();
        }

        @Override // org.whispersystems.signalservice.api.util.CredentialsProvider
        public PNI getPni() {
            return SignalStore.account().getPni();
        }

        @Override // org.whispersystems.signalservice.api.util.CredentialsProvider
        public String getE164() {
            return SignalStore.account().getE164();
        }

        @Override // org.whispersystems.signalservice.api.util.CredentialsProvider
        public String getPassword() {
            return SignalStore.account().getServicePassword();
        }

        @Override // org.whispersystems.signalservice.api.util.CredentialsProvider
        public int getDeviceId() {
            return SignalStore.account().getDeviceId();
        }
    }
}
