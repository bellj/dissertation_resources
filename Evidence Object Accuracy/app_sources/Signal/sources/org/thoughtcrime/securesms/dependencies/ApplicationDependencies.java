package org.thoughtcrime.securesms.dependencies;

import android.app.Application;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import okhttp3.ConnectionSpec;
import okhttp3.OkHttpClient;
import org.signal.core.util.Hex;
import org.signal.core.util.concurrent.DeadlockDetector;
import org.signal.libsignal.zkgroup.profiles.ClientZkProfileOperations;
import org.signal.libsignal.zkgroup.receipts.ClientZkReceiptOperations;
import org.thoughtcrime.securesms.KbsEnclave;
import org.thoughtcrime.securesms.components.TypingStatusRepository;
import org.thoughtcrime.securesms.components.TypingStatusSender;
import org.thoughtcrime.securesms.crypto.storage.SignalServiceDataStoreImpl;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.PendingRetryReceiptCache;
import org.thoughtcrime.securesms.groups.GroupsV2Authorization;
import org.thoughtcrime.securesms.groups.GroupsV2AuthorizationMemoryValueCache;
import org.thoughtcrime.securesms.groups.v2.processing.GroupsV2StateProcessor;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.megaphone.MegaphoneRepository;
import org.thoughtcrime.securesms.messages.BackgroundMessageRetriever;
import org.thoughtcrime.securesms.messages.IncomingMessageObserver;
import org.thoughtcrime.securesms.messages.IncomingMessageProcessor;
import org.thoughtcrime.securesms.net.StandardUserAgentInterceptor;
import org.thoughtcrime.securesms.notifications.MessageNotifier;
import org.thoughtcrime.securesms.payments.Payments;
import org.thoughtcrime.securesms.push.SignalServiceNetworkAccess;
import org.thoughtcrime.securesms.push.SignalServiceTrustStore;
import org.thoughtcrime.securesms.recipients.LiveRecipientCache;
import org.thoughtcrime.securesms.revealable.ViewOnceMessageManager;
import org.thoughtcrime.securesms.service.ExpiringMessageManager;
import org.thoughtcrime.securesms.service.ExpiringStoriesManager;
import org.thoughtcrime.securesms.service.PendingRetryReceiptManager;
import org.thoughtcrime.securesms.service.TrimThreadsByDateManager;
import org.thoughtcrime.securesms.service.webrtc.SignalCallManager;
import org.thoughtcrime.securesms.shakereport.ShakeToReport;
import org.thoughtcrime.securesms.util.AppForegroundObserver;
import org.thoughtcrime.securesms.util.EarlyMessageCache;
import org.thoughtcrime.securesms.util.FrameRateTracker;
import org.thoughtcrime.securesms.util.IasKeyStore;
import org.thoughtcrime.securesms.video.exo.GiphyMp4Cache;
import org.thoughtcrime.securesms.video.exo.SimpleExoPlayerPool;
import org.thoughtcrime.securesms.webrtc.audio.AudioManagerCompat;
import org.whispersystems.signalservice.api.KeyBackupService;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.SignalServiceDataStore;
import org.whispersystems.signalservice.api.SignalServiceMessageReceiver;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.SignalWebSocket;
import org.whispersystems.signalservice.api.groupsv2.GroupsV2Operations;
import org.whispersystems.signalservice.api.services.DonationsService;
import org.whispersystems.signalservice.api.services.ProfileService;
import org.whispersystems.signalservice.api.util.Tls12SocketFactory;
import org.whispersystems.signalservice.internal.util.BlacklistingTrustManager;
import org.whispersystems.signalservice.internal.util.Util;

/* loaded from: classes.dex */
public class ApplicationDependencies {
    private static final Object FRAME_RATE_TRACKER_LOCK = new Object();
    private static final Object JOB_MANAGER_LOCK = new Object();
    private static final Object LOCK = new Object();
    private static volatile SignalServiceAccountManager accountManager;
    private static AppForegroundObserver appForegroundObserver;
    private static Application application;
    private static volatile AudioManagerCompat audioManagerCompat;
    private static volatile BackgroundMessageRetriever backgroundMessageRetriever;
    private static volatile ClientZkReceiptOperations clientZkReceiptOperations;
    private static volatile DatabaseObserver databaseObserver;
    private static volatile DeadlockDetector deadlockDetector;
    private static volatile DonationsService donationsService;
    private static volatile EarlyMessageCache earlyMessageCache;
    private static volatile SimpleExoPlayerPool exoPlayerPool;
    private static volatile ExpiringMessageManager expiringMessageManager;
    private static volatile ExpiringStoriesManager expiringStoriesManager;
    private static volatile FrameRateTracker frameRateTracker;
    private static volatile GiphyMp4Cache giphyMp4Cache;
    private static volatile GroupsV2Authorization groupsV2Authorization;
    private static volatile GroupsV2Operations groupsV2Operations;
    private static volatile GroupsV2StateProcessor groupsV2StateProcessor;
    private static volatile IncomingMessageObserver incomingMessageObserver;
    private static volatile IncomingMessageProcessor incomingMessageProcessor;
    private static volatile JobManager jobManager;
    private static volatile MegaphoneRepository megaphoneRepository;
    private static volatile MessageNotifier messageNotifier;
    private static volatile SignalServiceMessageReceiver messageReceiver;
    private static volatile SignalServiceMessageSender messageSender;
    private static volatile OkHttpClient okHttpClient;
    private static volatile Payments payments;
    private static volatile PendingRetryReceiptCache pendingRetryReceiptCache;
    private static volatile PendingRetryReceiptManager pendingRetryReceiptManager;
    private static volatile ProfileService profileService;
    private static volatile SignalServiceDataStoreImpl protocolStore;
    private static Provider provider;
    private static volatile LiveRecipientCache recipientCache;
    private static volatile ShakeToReport shakeToReport;
    private static volatile SignalCallManager signalCallManager;
    private static volatile OkHttpClient signalOkHttpClient;
    private static volatile SignalWebSocket signalWebSocket;
    private static volatile TrimThreadsByDateManager trimThreadsByDateManager;
    private static volatile TypingStatusRepository typingStatusRepository;
    private static volatile TypingStatusSender typingStatusSender;
    private static volatile ViewOnceMessageManager viewOnceMessageManager;

    /* loaded from: classes.dex */
    public interface Provider {
        AudioManagerCompat provideAndroidCallAudioManager();

        AppForegroundObserver provideAppForegroundObserver();

        BackgroundMessageRetriever provideBackgroundMessageRetriever();

        ClientZkReceiptOperations provideClientZkReceiptOperations();

        DatabaseObserver provideDatabaseObserver();

        DeadlockDetector provideDeadlockDetector();

        DonationsService provideDonationsService();

        EarlyMessageCache provideEarlyMessageCache();

        SimpleExoPlayerPool provideExoPlayerPool();

        ExpiringMessageManager provideExpiringMessageManager();

        ExpiringStoriesManager provideExpiringStoriesManager();

        FrameRateTracker provideFrameRateTracker();

        GiphyMp4Cache provideGiphyMp4Cache();

        GroupsV2Operations provideGroupsV2Operations();

        IncomingMessageObserver provideIncomingMessageObserver();

        IncomingMessageProcessor provideIncomingMessageProcessor();

        JobManager provideJobManager();

        MegaphoneRepository provideMegaphoneRepository();

        MessageNotifier provideMessageNotifier();

        Payments providePayments(SignalServiceAccountManager signalServiceAccountManager);

        PendingRetryReceiptCache providePendingRetryReceiptCache();

        PendingRetryReceiptManager providePendingRetryReceiptManager();

        ProfileService provideProfileService(ClientZkProfileOperations clientZkProfileOperations, SignalServiceMessageReceiver signalServiceMessageReceiver, SignalWebSocket signalWebSocket);

        SignalServiceDataStoreImpl provideProtocolStore();

        LiveRecipientCache provideRecipientCache();

        ShakeToReport provideShakeToReport();

        SignalCallManager provideSignalCallManager();

        SignalServiceAccountManager provideSignalServiceAccountManager();

        SignalServiceMessageReceiver provideSignalServiceMessageReceiver();

        SignalServiceMessageSender provideSignalServiceMessageSender(SignalWebSocket signalWebSocket, SignalServiceDataStore signalServiceDataStore);

        SignalServiceNetworkAccess provideSignalServiceNetworkAccess();

        SignalWebSocket provideSignalWebSocket();

        TrimThreadsByDateManager provideTrimThreadsByDateManager();

        TypingStatusRepository provideTypingStatusRepository();

        TypingStatusSender provideTypingStatusSender();

        ViewOnceMessageManager provideViewOnceMessageManager();
    }

    public static void init(Application application2, Provider provider2) {
        synchronized (LOCK) {
            if (application == null && provider == null) {
                application = application2;
                provider = provider2;
                AppForegroundObserver provideAppForegroundObserver = provider2.provideAppForegroundObserver();
                appForegroundObserver = provideAppForegroundObserver;
                provideAppForegroundObserver.begin();
            } else {
                throw new IllegalStateException("Already initialized!");
            }
        }
    }

    public static boolean isInitialized() {
        return application != null;
    }

    public static Application getApplication() {
        return application;
    }

    public static SignalServiceAccountManager getSignalServiceAccountManager() {
        SignalServiceAccountManager signalServiceAccountManager;
        SignalServiceAccountManager signalServiceAccountManager2 = accountManager;
        if (signalServiceAccountManager2 != null) {
            return signalServiceAccountManager2;
        }
        synchronized (LOCK) {
            if (accountManager == null) {
                accountManager = provider.provideSignalServiceAccountManager();
            }
            signalServiceAccountManager = accountManager;
        }
        return signalServiceAccountManager;
    }

    public static GroupsV2Authorization getGroupsV2Authorization() {
        if (groupsV2Authorization == null) {
            synchronized (LOCK) {
                if (groupsV2Authorization == null) {
                    groupsV2Authorization = new GroupsV2Authorization(getSignalServiceAccountManager().getGroupsV2Api(), new GroupsV2AuthorizationMemoryValueCache(SignalStore.groupsV2AciAuthorizationCache()));
                }
            }
        }
        return groupsV2Authorization;
    }

    public static GroupsV2Operations getGroupsV2Operations() {
        if (groupsV2Operations == null) {
            synchronized (LOCK) {
                if (groupsV2Operations == null) {
                    groupsV2Operations = provider.provideGroupsV2Operations();
                }
            }
        }
        return groupsV2Operations;
    }

    public static KeyBackupService getKeyBackupService(KbsEnclave kbsEnclave) {
        return getSignalServiceAccountManager().getKeyBackupService(IasKeyStore.getIasKeyStore(application), kbsEnclave.getEnclaveName(), Hex.fromStringOrThrow(kbsEnclave.getServiceId()), kbsEnclave.getMrEnclave(), 10);
    }

    public static GroupsV2StateProcessor getGroupsV2StateProcessor() {
        if (groupsV2StateProcessor == null) {
            synchronized (LOCK) {
                if (groupsV2StateProcessor == null) {
                    groupsV2StateProcessor = new GroupsV2StateProcessor(application);
                }
            }
        }
        return groupsV2StateProcessor;
    }

    public static SignalServiceMessageSender getSignalServiceMessageSender() {
        SignalServiceMessageSender signalServiceMessageSender;
        SignalServiceMessageSender signalServiceMessageSender2 = messageSender;
        if (signalServiceMessageSender2 != null) {
            return signalServiceMessageSender2;
        }
        synchronized (LOCK) {
            if (messageSender == null) {
                messageSender = provider.provideSignalServiceMessageSender(getSignalWebSocket(), getProtocolStore());
            }
            signalServiceMessageSender = messageSender;
        }
        return signalServiceMessageSender;
    }

    public static SignalServiceMessageReceiver getSignalServiceMessageReceiver() {
        SignalServiceMessageReceiver signalServiceMessageReceiver;
        synchronized (LOCK) {
            if (messageReceiver == null) {
                messageReceiver = provider.provideSignalServiceMessageReceiver();
            }
            signalServiceMessageReceiver = messageReceiver;
        }
        return signalServiceMessageReceiver;
    }

    public static void resetSignalServiceMessageReceiver() {
        synchronized (LOCK) {
            messageReceiver = null;
        }
    }

    public static void closeConnections() {
        synchronized (LOCK) {
            if (incomingMessageObserver != null) {
                incomingMessageObserver.terminateAsync();
            }
            if (messageSender != null) {
                messageSender.cancelInFlightRequests();
            }
            incomingMessageObserver = null;
            messageReceiver = null;
            accountManager = null;
            messageSender = null;
        }
    }

    public static void resetNetworkConnectionsAfterProxyChange() {
        synchronized (LOCK) {
            closeConnections();
        }
    }

    public static SignalServiceNetworkAccess getSignalServiceNetworkAccess() {
        return provider.provideSignalServiceNetworkAccess();
    }

    public static IncomingMessageProcessor getIncomingMessageProcessor() {
        if (incomingMessageProcessor == null) {
            synchronized (LOCK) {
                if (incomingMessageProcessor == null) {
                    incomingMessageProcessor = provider.provideIncomingMessageProcessor();
                }
            }
        }
        return incomingMessageProcessor;
    }

    public static BackgroundMessageRetriever getBackgroundMessageRetriever() {
        if (backgroundMessageRetriever == null) {
            synchronized (LOCK) {
                if (backgroundMessageRetriever == null) {
                    backgroundMessageRetriever = provider.provideBackgroundMessageRetriever();
                }
            }
        }
        return backgroundMessageRetriever;
    }

    public static LiveRecipientCache getRecipientCache() {
        if (recipientCache == null) {
            synchronized (LOCK) {
                if (recipientCache == null) {
                    recipientCache = provider.provideRecipientCache();
                }
            }
        }
        return recipientCache;
    }

    public static JobManager getJobManager() {
        if (jobManager == null) {
            synchronized (JOB_MANAGER_LOCK) {
                if (jobManager == null) {
                    jobManager = provider.provideJobManager();
                }
            }
        }
        return jobManager;
    }

    public static FrameRateTracker getFrameRateTracker() {
        if (frameRateTracker == null) {
            synchronized (FRAME_RATE_TRACKER_LOCK) {
                if (frameRateTracker == null) {
                    frameRateTracker = provider.provideFrameRateTracker();
                }
            }
        }
        return frameRateTracker;
    }

    public static MegaphoneRepository getMegaphoneRepository() {
        if (megaphoneRepository == null) {
            synchronized (LOCK) {
                if (megaphoneRepository == null) {
                    megaphoneRepository = provider.provideMegaphoneRepository();
                }
            }
        }
        return megaphoneRepository;
    }

    public static EarlyMessageCache getEarlyMessageCache() {
        if (earlyMessageCache == null) {
            synchronized (LOCK) {
                if (earlyMessageCache == null) {
                    earlyMessageCache = provider.provideEarlyMessageCache();
                }
            }
        }
        return earlyMessageCache;
    }

    public static MessageNotifier getMessageNotifier() {
        if (messageNotifier == null) {
            synchronized (LOCK) {
                if (messageNotifier == null) {
                    messageNotifier = provider.provideMessageNotifier();
                }
            }
        }
        return messageNotifier;
    }

    public static IncomingMessageObserver getIncomingMessageObserver() {
        IncomingMessageObserver incomingMessageObserver2;
        IncomingMessageObserver incomingMessageObserver3 = incomingMessageObserver;
        if (incomingMessageObserver3 != null) {
            return incomingMessageObserver3;
        }
        synchronized (LOCK) {
            if (incomingMessageObserver == null) {
                incomingMessageObserver = provider.provideIncomingMessageObserver();
            }
            incomingMessageObserver2 = incomingMessageObserver;
        }
        return incomingMessageObserver2;
    }

    public static TrimThreadsByDateManager getTrimThreadsByDateManager() {
        if (trimThreadsByDateManager == null) {
            synchronized (LOCK) {
                if (trimThreadsByDateManager == null) {
                    trimThreadsByDateManager = provider.provideTrimThreadsByDateManager();
                }
            }
        }
        return trimThreadsByDateManager;
    }

    public static ViewOnceMessageManager getViewOnceMessageManager() {
        if (viewOnceMessageManager == null) {
            synchronized (LOCK) {
                if (viewOnceMessageManager == null) {
                    viewOnceMessageManager = provider.provideViewOnceMessageManager();
                }
            }
        }
        return viewOnceMessageManager;
    }

    public static ExpiringStoriesManager getExpireStoriesManager() {
        if (expiringStoriesManager == null) {
            synchronized (LOCK) {
                if (expiringStoriesManager == null) {
                    expiringStoriesManager = provider.provideExpiringStoriesManager();
                }
            }
        }
        return expiringStoriesManager;
    }

    public static PendingRetryReceiptManager getPendingRetryReceiptManager() {
        if (pendingRetryReceiptManager == null) {
            synchronized (LOCK) {
                if (pendingRetryReceiptManager == null) {
                    pendingRetryReceiptManager = provider.providePendingRetryReceiptManager();
                }
            }
        }
        return pendingRetryReceiptManager;
    }

    public static ExpiringMessageManager getExpiringMessageManager() {
        if (expiringMessageManager == null) {
            synchronized (LOCK) {
                if (expiringMessageManager == null) {
                    expiringMessageManager = provider.provideExpiringMessageManager();
                }
            }
        }
        return expiringMessageManager;
    }

    public static TypingStatusRepository getTypingStatusRepository() {
        if (typingStatusRepository == null) {
            synchronized (LOCK) {
                if (typingStatusRepository == null) {
                    typingStatusRepository = provider.provideTypingStatusRepository();
                }
            }
        }
        return typingStatusRepository;
    }

    public static TypingStatusSender getTypingStatusSender() {
        if (typingStatusSender == null) {
            synchronized (LOCK) {
                if (typingStatusSender == null) {
                    typingStatusSender = provider.provideTypingStatusSender();
                }
            }
        }
        return typingStatusSender;
    }

    public static DatabaseObserver getDatabaseObserver() {
        if (databaseObserver == null) {
            synchronized (LOCK) {
                if (databaseObserver == null) {
                    databaseObserver = provider.provideDatabaseObserver();
                }
            }
        }
        return databaseObserver;
    }

    public static Payments getPayments() {
        if (payments == null) {
            synchronized (LOCK) {
                if (payments == null) {
                    payments = provider.providePayments(getSignalServiceAccountManager());
                }
            }
        }
        return payments;
    }

    public static ShakeToReport getShakeToReport() {
        if (shakeToReport == null) {
            synchronized (LOCK) {
                if (shakeToReport == null) {
                    shakeToReport = provider.provideShakeToReport();
                }
            }
        }
        return shakeToReport;
    }

    public static SignalCallManager getSignalCallManager() {
        if (signalCallManager == null) {
            synchronized (LOCK) {
                if (signalCallManager == null) {
                    signalCallManager = provider.provideSignalCallManager();
                }
            }
        }
        return signalCallManager;
    }

    public static OkHttpClient getOkHttpClient() {
        if (okHttpClient == null) {
            synchronized (LOCK) {
                if (okHttpClient == null) {
                    okHttpClient = new OkHttpClient.Builder().addInterceptor(new StandardUserAgentInterceptor()).dns(SignalServiceNetworkAccess.DNS).build();
                }
            }
        }
        return okHttpClient;
    }

    public static OkHttpClient getSignalOkHttpClient() {
        if (signalOkHttpClient == null) {
            synchronized (LOCK) {
                if (signalOkHttpClient == null) {
                    try {
                        OkHttpClient okHttpClient2 = getOkHttpClient();
                        SSLContext instance = SSLContext.getInstance("TLS");
                        TrustManager[] createFor = BlacklistingTrustManager.createFor(new SignalServiceTrustStore(getApplication()));
                        instance.init(null, createFor, null);
                        signalOkHttpClient = okHttpClient2.newBuilder().sslSocketFactory(new Tls12SocketFactory(instance.getSocketFactory()), (X509TrustManager) createFor[0]).connectionSpecs(Util.immutableList(ConnectionSpec.RESTRICTED_TLS)).build();
                    } catch (KeyManagementException | NoSuchAlgorithmException e) {
                        throw new AssertionError(e);
                    }
                }
            }
        }
        return signalOkHttpClient;
    }

    public static AppForegroundObserver getAppForegroundObserver() {
        return appForegroundObserver;
    }

    public static PendingRetryReceiptCache getPendingRetryReceiptCache() {
        if (pendingRetryReceiptCache == null) {
            synchronized (LOCK) {
                if (pendingRetryReceiptCache == null) {
                    pendingRetryReceiptCache = provider.providePendingRetryReceiptCache();
                }
            }
        }
        return pendingRetryReceiptCache;
    }

    public static SignalWebSocket getSignalWebSocket() {
        if (signalWebSocket == null) {
            synchronized (LOCK) {
                if (signalWebSocket == null) {
                    signalWebSocket = provider.provideSignalWebSocket();
                }
            }
        }
        return signalWebSocket;
    }

    public static SignalServiceDataStoreImpl getProtocolStore() {
        if (protocolStore == null) {
            synchronized (LOCK) {
                if (protocolStore == null) {
                    protocolStore = provider.provideProtocolStore();
                }
            }
        }
        return protocolStore;
    }

    public static GiphyMp4Cache getGiphyMp4Cache() {
        if (giphyMp4Cache == null) {
            synchronized (LOCK) {
                if (giphyMp4Cache == null) {
                    giphyMp4Cache = provider.provideGiphyMp4Cache();
                }
            }
        }
        return giphyMp4Cache;
    }

    public static SimpleExoPlayerPool getExoPlayerPool() {
        if (exoPlayerPool == null) {
            synchronized (LOCK) {
                if (exoPlayerPool == null) {
                    exoPlayerPool = provider.provideExoPlayerPool();
                }
            }
        }
        return exoPlayerPool;
    }

    public static AudioManagerCompat getAndroidCallAudioManager() {
        if (audioManagerCompat == null) {
            synchronized (LOCK) {
                if (audioManagerCompat == null) {
                    audioManagerCompat = provider.provideAndroidCallAudioManager();
                }
            }
        }
        return audioManagerCompat;
    }

    public static DonationsService getDonationsService() {
        if (donationsService == null) {
            synchronized (LOCK) {
                if (donationsService == null) {
                    donationsService = provider.provideDonationsService();
                }
            }
        }
        return donationsService;
    }

    public static ProfileService getProfileService() {
        if (profileService == null) {
            synchronized (LOCK) {
                if (profileService == null) {
                    profileService = provider.provideProfileService(getGroupsV2Operations().getProfileOperations(), getSignalServiceMessageReceiver(), getSignalWebSocket());
                }
            }
        }
        return profileService;
    }

    public static ClientZkReceiptOperations getClientZkReceiptOperations() {
        if (clientZkReceiptOperations == null) {
            synchronized (LOCK) {
                if (clientZkReceiptOperations == null) {
                    clientZkReceiptOperations = provider.provideClientZkReceiptOperations();
                }
            }
        }
        return clientZkReceiptOperations;
    }

    public static DeadlockDetector getDeadlockDetector() {
        if (deadlockDetector == null) {
            synchronized (LOCK) {
                if (deadlockDetector == null) {
                    deadlockDetector = provider.provideDeadlockDetector();
                }
            }
        }
        return deadlockDetector;
    }
}
