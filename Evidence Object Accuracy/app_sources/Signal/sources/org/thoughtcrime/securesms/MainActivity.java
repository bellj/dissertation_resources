package org.thoughtcrime.securesms;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;
import org.thoughtcrime.securesms.components.voice.VoiceNoteMediaController;
import org.thoughtcrime.securesms.components.voice.VoiceNoteMediaControllerOwner;
import org.thoughtcrime.securesms.devicetransfer.olddevice.OldDeviceTransferLockedDialog;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.stories.tabs.ConversationListTabRepository;
import org.thoughtcrime.securesms.stories.tabs.ConversationListTabsViewModel;
import org.thoughtcrime.securesms.util.AppStartup;
import org.thoughtcrime.securesms.util.CachedInflater;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.SplashScreenUtil;
import org.thoughtcrime.securesms.util.WindowUtil;

/* loaded from: classes.dex */
public class MainActivity extends PassphraseRequiredActivity implements VoiceNoteMediaControllerOwner {
    public static final int RESULT_CONFIG_CHANGED;
    private ConversationListTabsViewModel conversationListTabsViewModel;
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();
    private VoiceNoteMediaController mediaController;
    private final MainNavigator navigator = new MainNavigator(this);

    public static Intent clearTop(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(872415232);
        return intent;
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        AppStartup.getInstance().onCriticalRenderEventStart();
        super.onCreate(bundle, z);
        setContentView(R.layout.main_activity);
        this.mediaController = new VoiceNoteMediaController(this);
        ConversationListTabsViewModel.Factory factory = new ConversationListTabsViewModel.Factory(new ConversationListTabRepository());
        handleGroupLinkInIntent(getIntent());
        handleProxyInIntent(getIntent());
        handleSignalMeIntent(getIntent());
        CachedInflater.from(this).clear();
        this.conversationListTabsViewModel = (ConversationListTabsViewModel) new ViewModelProvider(this, factory).get(ConversationListTabsViewModel.class);
        updateTabVisibility();
    }

    @Override // android.app.Activity
    public Intent getIntent() {
        return super.getIntent().setFlags(872415232);
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleGroupLinkInIntent(intent);
        handleProxyInIntent(intent);
        handleSignalMeIntent(intent);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onPreCreate() {
        super.onPreCreate();
        this.dynamicTheme.onCreate(this);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
        if (SignalStore.misc().isOldDeviceTransferLocked()) {
            OldDeviceTransferLockedDialog.show(getSupportFragmentManager());
        }
        updateTabVisibility();
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onStop() {
        super.onStop();
        SplashScreenUtil.setSplashScreenThemeIfNecessary(this, SignalStore.settings().getTheme());
    }

    @Override // androidx.activity.ComponentActivity, android.app.Activity
    public void onBackPressed() {
        if (!this.navigator.onBackPressed()) {
            super.onBackPressed();
        }
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 901 && i2 == 902) {
            recreate();
        }
    }

    private void updateTabVisibility() {
        if (Stories.isFeatureEnabled()) {
            findViewById(R.id.conversation_list_tabs).setVisibility(0);
            WindowUtil.setNavigationBarColor(this, ContextCompat.getColor(this, R.color.signal_colorSurface2));
            return;
        }
        findViewById(R.id.conversation_list_tabs).setVisibility(8);
        WindowUtil.setNavigationBarColor(this, ContextCompat.getColor(this, R.color.signal_colorBackground));
        this.conversationListTabsViewModel.onChatsSelected();
    }

    public MainNavigator getNavigator() {
        return this.navigator;
    }

    private void handleGroupLinkInIntent(Intent intent) {
        Uri data = intent.getData();
        if (data != null) {
            CommunicationActions.handlePotentialGroupLinkUrl(this, data.toString());
        }
    }

    private void handleProxyInIntent(Intent intent) {
        Uri data = intent.getData();
        if (data != null) {
            CommunicationActions.handlePotentialProxyLinkUrl(this, data.toString());
        }
    }

    private void handleSignalMeIntent(Intent intent) {
        Uri data = intent.getData();
        if (data != null) {
            CommunicationActions.handlePotentialSignalMeUrl(this, data.toString());
        }
    }

    @Override // org.thoughtcrime.securesms.components.voice.VoiceNoteMediaControllerOwner
    public VoiceNoteMediaController getVoiceNoteMediaController() {
        return this.mediaController;
    }
}
