package org.thoughtcrime.securesms;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import org.signal.core.util.logging.Log;

/* loaded from: classes.dex */
public abstract class LoggingFragment extends Fragment {
    private static final String TAG = Log.tag(LoggingFragment.class);

    public LoggingFragment() {
    }

    public LoggingFragment(int i) {
        super(i);
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        logEvent("onCreate()");
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public void onStart() {
        logEvent("onStart()");
        super.onStart();
    }

    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        logEvent("onStop()");
        super.onStop();
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        logEvent("onDestroy()");
        super.onDestroy();
    }

    private void logEvent(String str) {
        String str2 = TAG;
        Log.d(str2, "[" + Log.tag(getClass()) + "] " + str);
    }
}
