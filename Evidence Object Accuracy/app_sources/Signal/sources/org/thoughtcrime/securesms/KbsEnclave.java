package org.thoughtcrime.securesms;

import java.util.Objects;

/* loaded from: classes.dex */
public final class KbsEnclave {
    private final String enclaveName;
    private final String mrEnclave;
    private final String serviceId;

    public KbsEnclave(String str, String str2, String str3) {
        this.enclaveName = str;
        this.serviceId = str2;
        this.mrEnclave = str3;
    }

    public String getMrEnclave() {
        return this.mrEnclave;
    }

    public String getEnclaveName() {
        return this.enclaveName;
    }

    public String getServiceId() {
        return this.serviceId;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || KbsEnclave.class != obj.getClass()) {
            return false;
        }
        KbsEnclave kbsEnclave = (KbsEnclave) obj;
        if (!this.enclaveName.equals(kbsEnclave.enclaveName) || !this.serviceId.equals(kbsEnclave.serviceId) || !this.mrEnclave.equals(kbsEnclave.mrEnclave)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(this.enclaveName, this.serviceId, this.mrEnclave);
    }
}
