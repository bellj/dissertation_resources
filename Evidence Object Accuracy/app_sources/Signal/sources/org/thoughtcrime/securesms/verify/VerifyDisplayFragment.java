package org.thoughtcrime.securesms.verify;

import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.ItemTouchHelper;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.nio.charset.Charset;
import java.util.Locale;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.fingerprint.Fingerprint;
import org.signal.libsignal.protocol.fingerprint.FingerprintVersionMismatchException;
import org.signal.libsignal.protocol.fingerprint.NumericFingerprintGenerator;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.webrtc.WebRtcCallView;
import org.thoughtcrime.securesms.crypto.IdentityKeyParcelable;
import org.thoughtcrime.securesms.crypto.ReentrantSessionLock;
import org.thoughtcrime.securesms.database.IdentityDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobs.MultiDeviceVerifiedUpdateJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.qr.QrCode;
import org.thoughtcrime.securesms.recipients.LiveRecipient;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.IdentityUtil;
import org.thoughtcrime.securesms.util.MessageRecordUtil;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.whispersystems.signalservice.api.SignalSessionLock;

/* loaded from: classes.dex */
public class VerifyDisplayFragment extends Fragment implements ViewTreeObserver.OnScrollChangedListener {
    private static final String LOCAL_IDENTITY;
    private static final String LOCAL_NUMBER;
    private static final String RECIPIENT_ID;
    private static final String REMOTE_IDENTITY;
    private static final String TAG = Log.tag(VerifyDisplayFragment.class);
    private static final String VERIFIED_STATE;
    private boolean animateFailureOnDraw = false;
    private boolean animateSuccessOnDraw = false;
    private View bottomShadow;
    private Callback callback;
    private TextView[] codes = new TextView[12];
    private boolean currentVerifiedState = false;
    private TextView description;
    private Fingerprint fingerprint;
    private View loading;
    private IdentityKey localIdentity;
    private View numbersContainer;
    private ImageView qrCode;
    private View qrCodeContainer;
    private ImageView qrVerified;
    private LiveRecipient recipient;
    private IdentityKey remoteIdentity;
    private ScrollView scrollView;
    private TextSwitcher tapLabel;
    private Toolbar toolbar;
    private View toolbarShadow;
    private Button verifyButton;

    /* loaded from: classes.dex */
    public interface Callback {
        void onQrCodeContainerClicked();
    }

    public static VerifyDisplayFragment create(RecipientId recipientId, IdentityKeyParcelable identityKeyParcelable, IdentityKeyParcelable identityKeyParcelable2, String str, boolean z) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("recipient_id", recipientId);
        bundle.putParcelable(REMOTE_IDENTITY, identityKeyParcelable);
        bundle.putParcelable(LOCAL_IDENTITY, identityKeyParcelable2);
        bundle.putString(LOCAL_NUMBER, str);
        bundle.putBoolean(VERIFIED_STATE, z);
        VerifyDisplayFragment verifyDisplayFragment = new VerifyDisplayFragment();
        verifyDisplayFragment.setArguments(bundle);
        return verifyDisplayFragment;
    }

    @Override // androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Callback) {
            this.callback = (Callback) context;
        } else if (getParentFragment() instanceof Callback) {
            this.callback = (Callback) getParentFragment();
        } else {
            throw new ClassCastException("Cannot find ScanListener in parent component");
        }
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return ViewUtil.inflate(layoutInflater, viewGroup, R.layout.verify_display_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        this.scrollView = (ScrollView) view.findViewById(R.id.scroll_view);
        this.numbersContainer = view.findViewById(R.id.number_table);
        this.loading = view.findViewById(R.id.loading);
        this.qrCodeContainer = view.findViewById(R.id.qr_code_container);
        this.qrCode = (ImageView) view.findViewById(R.id.qr_code);
        this.verifyButton = (Button) view.findViewById(R.id.verify_button);
        this.qrVerified = (ImageView) view.findViewById(R.id.qr_verified);
        this.description = (TextView) view.findViewById(R.id.description);
        this.tapLabel = (TextSwitcher) view.findViewById(R.id.tap_label);
        this.toolbarShadow = view.findViewById(R.id.toolbar_shadow);
        this.bottomShadow = view.findViewById(R.id.verify_identity_bottom_shadow);
        this.codes[0] = (TextView) view.findViewById(R.id.code_first);
        this.codes[1] = (TextView) view.findViewById(R.id.code_second);
        this.codes[2] = (TextView) view.findViewById(R.id.code_third);
        this.codes[3] = (TextView) view.findViewById(R.id.code_fourth);
        this.codes[4] = (TextView) view.findViewById(R.id.code_fifth);
        this.codes[5] = (TextView) view.findViewById(R.id.code_sixth);
        this.codes[6] = (TextView) view.findViewById(R.id.code_seventh);
        this.codes[7] = (TextView) view.findViewById(R.id.code_eighth);
        this.codes[8] = (TextView) view.findViewById(R.id.code_ninth);
        this.codes[9] = (TextView) view.findViewById(R.id.code_tenth);
        this.codes[10] = (TextView) view.findViewById(R.id.code_eleventh);
        this.codes[11] = (TextView) view.findViewById(R.id.code_twelth);
        this.qrCodeContainer.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.verify.VerifyDisplayFragment$$ExternalSyntheticLambda6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                VerifyDisplayFragment.m3294$r8$lambda$b2C9p0Zk3IycEkC1rvyq0rHKD0(VerifyDisplayFragment.this, view2);
            }
        });
        registerForContextMenu(this.numbersContainer);
        updateVerifyButton(getArguments().getBoolean(VERIFIED_STATE, false), false);
        this.verifyButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.verify.VerifyDisplayFragment$$ExternalSyntheticLambda7
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                VerifyDisplayFragment.$r8$lambda$vi14uIW_SBHmU9hdsdi_MpO2k0k(VerifyDisplayFragment.this, view2);
            }
        });
        this.scrollView.getViewTreeObserver().addOnScrollChangedListener(this);
        this.toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.verify.VerifyDisplayFragment$$ExternalSyntheticLambda8
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                VerifyDisplayFragment.m3295$r8$lambda$kcwFjPojPHK3vH2na8V8f7Vth0(VerifyDisplayFragment.this, view2);
            }
        });
        this.toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() { // from class: org.thoughtcrime.securesms.verify.VerifyDisplayFragment$$ExternalSyntheticLambda9
            @Override // androidx.appcompat.widget.Toolbar.OnMenuItemClickListener
            public final boolean onMenuItemClick(MenuItem menuItem) {
                return VerifyDisplayFragment.this.onToolbarOptionsItemSelected(menuItem);
            }
        });
        this.toolbar.setTitle(R.string.AndroidManifest__verify_safety_number);
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        this.callback.onQrCodeContainerClicked();
    }

    public /* synthetic */ void lambda$onViewCreated$1(View view) {
        updateVerifyButton(!this.currentVerifiedState, true);
    }

    public /* synthetic */ void lambda$onViewCreated$2(View view) {
        requireActivity().onBackPressed();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewStateRestored(Bundle bundle) {
        super.onViewStateRestored(bundle);
        initializeFingerprint();
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        this.scrollView.getViewTreeObserver().removeOnScrollChangedListener(this);
        super.onDestroyView();
    }

    private void initializeFingerprint() {
        byte[] bArr;
        byte[] bArr2;
        this.localIdentity = ((IdentityKeyParcelable) getArguments().getParcelable(LOCAL_IDENTITY)).get();
        this.recipient = Recipient.live((RecipientId) getArguments().getParcelable("recipient_id"));
        this.remoteIdentity = ((IdentityKeyParcelable) getArguments().getParcelable(REMOTE_IDENTITY)).get();
        Recipient resolve = this.recipient.resolve();
        int i = 2;
        if (FeatureFlags.verifyV2() && resolve.getServiceId().isPresent()) {
            Log.i(TAG, "Using UUID (version 2).");
            bArr = SignalStore.account().requireAci().toByteArray();
            bArr2 = resolve.requireServiceId().toByteArray();
        } else if (FeatureFlags.verifyV2() || !resolve.getE164().isPresent()) {
            Log.w(TAG, String.format(Locale.ENGLISH, "Could not show proper verification! verifyV2: %s, hasUuid: %s, hasE164: %s", Boolean.valueOf(FeatureFlags.verifyV2()), Boolean.valueOf(resolve.getServiceId().isPresent()), Boolean.valueOf(resolve.getE164().isPresent())));
            new MaterialAlertDialogBuilder(requireContext()).setMessage((CharSequence) getString(R.string.VerifyIdentityActivity_you_must_first_exchange_messages_in_order_to_view, resolve.getDisplayName(requireContext()))).setPositiveButton(17039370, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.verify.VerifyDisplayFragment$$ExternalSyntheticLambda1
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i2) {
                    VerifyDisplayFragment.$r8$lambda$lDNsq8KfLrfNEqiYGX5Ro6tG4TE(VerifyDisplayFragment.this, dialogInterface, i2);
                }
            }).setOnDismissListener((DialogInterface.OnDismissListener) new DialogInterface.OnDismissListener() { // from class: org.thoughtcrime.securesms.verify.VerifyDisplayFragment$$ExternalSyntheticLambda2
                @Override // android.content.DialogInterface.OnDismissListener
                public final void onDismiss(DialogInterface dialogInterface) {
                    VerifyDisplayFragment.$r8$lambda$zHnL2IAXj0jGwn8FdUapzADXJ10(VerifyDisplayFragment.this, dialogInterface);
                }
            }).show();
            return;
        } else {
            Log.i(TAG, "Using E164 (version 1).");
            bArr = Recipient.self().requireE164().getBytes();
            bArr2 = resolve.requireE164().getBytes();
            i = 1;
        }
        this.recipient.observe(this, new Observer() { // from class: org.thoughtcrime.securesms.verify.VerifyDisplayFragment$$ExternalSyntheticLambda3
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                VerifyDisplayFragment.$r8$lambda$AcFfJ_4X2IBltV5UhY4yVjyxrwA(VerifyDisplayFragment.this, (Recipient) obj);
            }
        });
        SimpleTask.run(new SimpleTask.BackgroundTask(i, bArr, bArr2) { // from class: org.thoughtcrime.securesms.verify.VerifyDisplayFragment$$ExternalSyntheticLambda4
            public final /* synthetic */ int f$1;
            public final /* synthetic */ byte[] f$2;
            public final /* synthetic */ byte[] f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return VerifyDisplayFragment.$r8$lambda$u8n3y9dSE2sZtWixwJBjX3YqijI(VerifyDisplayFragment.this, this.f$1, this.f$2, this.f$3);
            }
        }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.verify.VerifyDisplayFragment$$ExternalSyntheticLambda5
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                VerifyDisplayFragment.$r8$lambda$mf_AZRiCJwp2EjNHckNBmqKSogg(VerifyDisplayFragment.this, (Fingerprint) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializeFingerprint$3(DialogInterface dialogInterface, int i) {
        requireActivity().finish();
    }

    public /* synthetic */ void lambda$initializeFingerprint$4(DialogInterface dialogInterface) {
        requireActivity().finish();
        dialogInterface.dismiss();
    }

    public /* synthetic */ Fingerprint lambda$initializeFingerprint$5(int i, byte[] bArr, byte[] bArr2) {
        return new NumericFingerprintGenerator(5200).createFor(i, bArr, this.localIdentity, bArr2, this.remoteIdentity);
    }

    public /* synthetic */ void lambda$initializeFingerprint$6(Fingerprint fingerprint) {
        if (getActivity() != null) {
            this.fingerprint = fingerprint;
            setFingerprintViews(fingerprint, true);
            initializeOptionsMenu();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        setRecipientText(this.recipient.get());
        Fingerprint fingerprint = this.fingerprint;
        if (fingerprint != null) {
            setFingerprintViews(fingerprint, false);
        }
        if (this.animateSuccessOnDraw) {
            this.animateSuccessOnDraw = false;
            animateVerifiedSuccess();
        } else if (this.animateFailureOnDraw) {
            this.animateFailureOnDraw = false;
            animateVerifiedFailure();
        }
        ThreadUtil.postToMain(new Runnable() { // from class: org.thoughtcrime.securesms.verify.VerifyDisplayFragment$$ExternalSyntheticLambda11
            @Override // java.lang.Runnable
            public final void run() {
                VerifyDisplayFragment.this.onScrollChanged();
            }
        });
    }

    @Override // androidx.fragment.app.Fragment, android.view.View.OnCreateContextMenuListener
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        super.onCreateContextMenu(contextMenu, view, contextMenuInfo);
        if (this.fingerprint != null) {
            getActivity().getMenuInflater().inflate(R.menu.verify_display_fragment_context_menu, contextMenu);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public boolean onContextItemSelected(MenuItem menuItem) {
        if (this.fingerprint == null) {
            return super.onContextItemSelected(menuItem);
        }
        int itemId = menuItem.getItemId();
        if (itemId == R.id.menu_compare) {
            handleCompareWithClipboard(this.fingerprint);
            return true;
        } else if (itemId != R.id.menu_copy) {
            return super.onContextItemSelected(menuItem);
        } else {
            handleCopyToClipboard(this.fingerprint, this.codes.length);
            return true;
        }
    }

    private void initializeOptionsMenu() {
        if (this.fingerprint != null) {
            requireActivity().getMenuInflater().inflate(R.menu.verify_identity, this.toolbar.getMenu());
        }
    }

    public boolean onToolbarOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.verify_identity__share) {
            return false;
        }
        handleShare(this.fingerprint, this.codes.length);
        return true;
    }

    public void setScannedFingerprint(String str) {
        try {
            if (this.fingerprint.getScannableFingerprint().compareTo(str.getBytes("ISO-8859-1"))) {
                this.animateSuccessOnDraw = true;
            } else {
                this.animateFailureOnDraw = true;
            }
        } catch (FingerprintVersionMismatchException e) {
            while (true) {
                Log.w(TAG, e);
                if (e.getOurVersion() < e.getTheirVersion()) {
                    Toast.makeText(getActivity(), (int) R.string.VerifyIdentityActivity_your_contact_is_running_a_newer_version_of_Signal, 1).show();
                } else {
                    Toast.makeText(getActivity(), (int) R.string.VerifyIdentityActivity_your_contact_is_running_an_old_version_of_signal, 1).show();
                }
                this.animateFailureOnDraw = true;
                return;
            }
        } catch (Exception e2) {
            Log.w(TAG, e2);
            Toast.makeText(getActivity(), (int) R.string.VerifyIdentityActivity_the_scanned_qr_code_is_not_a_correctly_formatted_safety_number, 1).show();
            this.animateFailureOnDraw = true;
        }
    }

    private String getFormattedSafetyNumbers(Fingerprint fingerprint, int i) {
        String[] segments = getSegments(fingerprint, i);
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < segments.length; i2++) {
            sb.append(segments[i2]);
            if (i2 != segments.length - 1) {
                if ((i2 + 1) % 4 == 0) {
                    sb.append('\n');
                } else {
                    sb.append(' ');
                }
            }
        }
        return sb.toString();
    }

    private void handleCopyToClipboard(Fingerprint fingerprint, int i) {
        Util.writeTextToClipboard(requireContext(), "Safety numbers", getFormattedSafetyNumbers(fingerprint, i));
    }

    private void handleCompareWithClipboard(Fingerprint fingerprint) {
        String readTextFromClipboard = Util.readTextFromClipboard(getActivity());
        if (readTextFromClipboard == null) {
            Toast.makeText(getActivity(), (int) R.string.VerifyIdentityActivity_no_safety_number_to_compare_was_found_in_the_clipboard, 1).show();
            return;
        }
        String replaceAll = readTextFromClipboard.replaceAll("\\D", "");
        if (TextUtils.isEmpty(replaceAll) || replaceAll.length() != 60) {
            Toast.makeText(getActivity(), (int) R.string.VerifyIdentityActivity_no_safety_number_to_compare_was_found_in_the_clipboard, 1).show();
        } else if (fingerprint.getDisplayableFingerprint().getDisplayText().equals(replaceAll)) {
            animateVerifiedSuccess();
        } else {
            animateVerifiedFailure();
        }
    }

    private void handleShare(Fingerprint fingerprint, int i) {
        String str = getString(R.string.VerifyIdentityActivity_our_signal_safety_number) + "\n" + getFormattedSafetyNumbers(fingerprint, i) + "\n";
        Intent intent = new Intent();
        intent.setAction("android.intent.action.SEND");
        intent.putExtra("android.intent.extra.TEXT", str);
        intent.setType("text/plain");
        try {
            startActivity(Intent.createChooser(intent, getString(R.string.VerifyIdentityActivity_share_safety_number_via)));
        } catch (ActivityNotFoundException unused) {
            Toast.makeText(getActivity(), (int) R.string.VerifyIdentityActivity_no_app_to_share_to, 1).show();
        }
    }

    public void setRecipientText(Recipient recipient) {
        this.description.setText(Html.fromHtml(String.format(getActivity().getString(R.string.verify_display_fragment__to_verify_the_security_of_your_end_to_end_encryption_with_s), Html.escapeHtml(recipient.getDisplayName(getContext())))));
        this.description.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void setFingerprintViews(Fingerprint fingerprint, boolean z) {
        String[] segments = getSegments(fingerprint, this.codes.length);
        int i = 0;
        while (true) {
            TextView[] textViewArr = this.codes;
            if (i >= textViewArr.length) {
                break;
            }
            if (z) {
                setCodeSegment(textViewArr[i], segments[i]);
            } else {
                textViewArr[i].setText(segments[i]);
            }
            i++;
        }
        this.qrCode.setImageBitmap(QrCode.create(new String(fingerprint.getScannableFingerprint().getSerialized(), Charset.forName("ISO-8859-1"))));
        if (z) {
            ViewUtil.fadeIn(this.qrCode, MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH);
            ViewUtil.fadeIn(this.tapLabel, MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH);
            ViewUtil.fadeOut(this.loading, WebRtcCallView.PIP_RESIZE_DURATION, 8);
            return;
        }
        this.qrCode.setVisibility(0);
        this.tapLabel.setVisibility(0);
        this.loading.setVisibility(8);
    }

    private void setCodeSegment(TextView textView, String str) {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setObjectValues(0, Integer.valueOf(Integer.parseInt(str)));
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(textView) { // from class: org.thoughtcrime.securesms.verify.VerifyDisplayFragment$$ExternalSyntheticLambda10
            public final /* synthetic */ TextView f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator2) {
                VerifyDisplayFragment.m3293$r8$lambda$U8G_W_xKjq3fYupN97KJgVy40(this.f$0, valueAnimator2);
            }
        });
        valueAnimator.setEvaluator(new TypeEvaluator<Integer>() { // from class: org.thoughtcrime.securesms.verify.VerifyDisplayFragment.1
            public Integer evaluate(float f, Integer num, Integer num2) {
                return Integer.valueOf(Math.round(((float) num.intValue()) + (((float) (num2.intValue() - num.intValue())) * f)));
            }
        });
        valueAnimator.setDuration(1000L);
        valueAnimator.start();
    }

    public static /* synthetic */ void lambda$setCodeSegment$7(TextView textView, ValueAnimator valueAnimator) {
        textView.setText(String.format(Locale.getDefault(), "%05d", Integer.valueOf(((Integer) valueAnimator.getAnimatedValue()).intValue())));
    }

    private String[] getSegments(Fingerprint fingerprint, int i) {
        String[] strArr = new String[i];
        String displayText = fingerprint.getDisplayableFingerprint().getDisplayText();
        int length = displayText.length() / i;
        for (int i2 = 0; i2 < i; i2++) {
            int i3 = i2 * length;
            strArr[i2] = displayText.substring(i3, i3 + length);
        }
        return strArr;
    }

    private Bitmap createVerifiedBitmap(int i, int i2, int i3) {
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), i3);
        float width = (float) ((i - decodeResource.getWidth()) / 2);
        canvas.drawBitmap(decodeResource, width, width, (Paint) null);
        return createBitmap;
    }

    private void animateVerifiedSuccess() {
        Bitmap bitmap = ((BitmapDrawable) this.qrCode.getDrawable()).getBitmap();
        this.qrVerified.setImageBitmap(createVerifiedBitmap(bitmap.getWidth(), bitmap.getHeight(), R.drawable.ic_check_white_48dp));
        this.qrVerified.getBackground().setColorFilter(getResources().getColor(R.color.green_500), PorterDuff.Mode.MULTIPLY);
        this.tapLabel.setText(getString(R.string.verify_display_fragment__successful_match));
        animateVerified();
    }

    private void animateVerifiedFailure() {
        Bitmap bitmap = ((BitmapDrawable) this.qrCode.getDrawable()).getBitmap();
        this.qrVerified.setImageBitmap(createVerifiedBitmap(bitmap.getWidth(), bitmap.getHeight(), R.drawable.ic_close_white_48dp));
        this.qrVerified.getBackground().setColorFilter(getResources().getColor(R.color.red_500), PorterDuff.Mode.MULTIPLY);
        this.tapLabel.setText(getString(R.string.verify_display_fragment__failed_to_verify_safety_number));
        animateVerified();
    }

    private void animateVerified() {
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setInterpolator(new FastOutSlowInInterpolator());
        scaleAnimation.setDuration(800);
        scaleAnimation.setAnimationListener(new Animation.AnimationListener() { // from class: org.thoughtcrime.securesms.verify.VerifyDisplayFragment.2
            @Override // android.view.animation.Animation.AnimationListener
            public void onAnimationRepeat(Animation animation) {
            }

            @Override // android.view.animation.Animation.AnimationListener
            public void onAnimationStart(Animation animation) {
            }

            @Override // android.view.animation.Animation.AnimationListener
            public void onAnimationEnd(Animation animation) {
                VerifyDisplayFragment.this.qrVerified.postDelayed(new VerifyDisplayFragment$2$$ExternalSyntheticLambda0(this), 2000);
            }

            public /* synthetic */ void lambda$onAnimationEnd$0() {
                ScaleAnimation scaleAnimation2 = new ScaleAnimation(1.0f, 0.0f, 1.0f, 0.0f, 1, 0.5f, 1, 0.5f);
                scaleAnimation2.setInterpolator(new AnticipateInterpolator());
                scaleAnimation2.setDuration(500);
                ViewUtil.animateOut(VerifyDisplayFragment.this.qrVerified, scaleAnimation2, 8);
                ViewUtil.fadeIn(VerifyDisplayFragment.this.qrCode, 800);
                VerifyDisplayFragment.this.qrCodeContainer.setEnabled(true);
                VerifyDisplayFragment.this.tapLabel.setText(VerifyDisplayFragment.this.getString(R.string.verify_display_fragment__tap_to_scan));
            }
        });
        ViewUtil.fadeOut(this.qrCode, 200, 4);
        ViewUtil.animateIn(this.qrVerified, scaleAnimation);
        this.qrCodeContainer.setEnabled(false);
    }

    private void updateVerifyButton(boolean z, boolean z2) {
        this.currentVerifiedState = z;
        if (z) {
            this.verifyButton.setText(R.string.verify_display_fragment__clear_verification);
        } else {
            this.verifyButton.setText(R.string.verify_display_fragment__mark_as_verified);
        }
        if (z2) {
            SignalExecutors.BOUNDED.execute(new Runnable(z, this.recipient.getId(), requireContext().getApplicationContext()) { // from class: org.thoughtcrime.securesms.verify.VerifyDisplayFragment$$ExternalSyntheticLambda0
                public final /* synthetic */ boolean f$1;
                public final /* synthetic */ RecipientId f$2;
                public final /* synthetic */ Context f$3;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    VerifyDisplayFragment.$r8$lambda$fLEHD60ypLKUfWMr7Z0AqGiqlYw(VerifyDisplayFragment.this, this.f$1, this.f$2, this.f$3);
                }
            });
        }
    }

    public /* synthetic */ void lambda$updateVerifyButton$8(boolean z, RecipientId recipientId, Context context) {
        IdentityDatabase.VerifiedStatus verifiedStatus;
        SignalSessionLock.Lock acquire = ReentrantSessionLock.INSTANCE.acquire();
        try {
            if (z) {
                String str = TAG;
                Log.i(str, "Saving identity: " + recipientId);
                ApplicationDependencies.getProtocolStore().aci().identities().saveIdentityWithoutSideEffects(recipientId, this.remoteIdentity, IdentityDatabase.VerifiedStatus.VERIFIED, false, System.currentTimeMillis(), true);
            } else {
                ApplicationDependencies.getProtocolStore().aci().identities().setVerified(recipientId, this.remoteIdentity, IdentityDatabase.VerifiedStatus.DEFAULT);
            }
            JobManager jobManager = ApplicationDependencies.getJobManager();
            IdentityKey identityKey = this.remoteIdentity;
            if (z) {
                verifiedStatus = IdentityDatabase.VerifiedStatus.VERIFIED;
            } else {
                verifiedStatus = IdentityDatabase.VerifiedStatus.DEFAULT;
            }
            jobManager.add(new MultiDeviceVerifiedUpdateJob(recipientId, identityKey, verifiedStatus));
            StorageSyncHelper.scheduleSyncForDataChange();
            IdentityUtil.markIdentityVerified(context, this.recipient.get(), z, false);
            if (acquire != null) {
                acquire.close();
            }
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    @Override // android.view.ViewTreeObserver.OnScrollChangedListener
    public void onScrollChanged() {
        if (this.scrollView.canScrollVertically(-1)) {
            if (this.toolbarShadow.getVisibility() != 0) {
                ViewUtil.fadeIn(this.toolbarShadow, ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
            }
        } else if (this.toolbarShadow.getVisibility() != 8) {
            ViewUtil.fadeOut(this.toolbarShadow, ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
        }
        if (!this.scrollView.canScrollVertically(1)) {
            ViewUtil.fadeOut(this.bottomShadow, ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
        } else if (this.bottomShadow.getVisibility() != 0) {
            ViewUtil.fadeIn(this.bottomShadow, ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION);
        }
    }
}
