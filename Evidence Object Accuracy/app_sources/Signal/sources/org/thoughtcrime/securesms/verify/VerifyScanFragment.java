package org.thoughtcrime.securesms.verify;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.core.view.OneShotPreDrawListener;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.qr.kitkat.ScanListener;
import org.signal.qr.kitkat.ScanningThread;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.ShapeScrim;
import org.thoughtcrime.securesms.components.camera.CameraView;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException;

/* compiled from: VerifyScanFragment.kt */
@Metadata(d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J&\u0010\u0014\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0016J\b\u0010\u001c\u001a\u00020\u000eH\u0016J\b\u0010\u001d\u001a\u00020\u000eH\u0016J\u001a\u0010\u001e\u001a\u00020\u000e2\u0006\u0010\u001f\u001a\u00020\u00152\b\u0010 \u001a\u0004\u0018\u00010\u001bH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX.¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX.¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX.¢\u0006\u0002\n\u0000¨\u0006!"}, d2 = {"Lorg/thoughtcrime/securesms/verify/VerifyScanFragment;", "Landroidx/fragment/app/Fragment;", "()V", "cameraMarks", "Landroid/widget/ImageView;", "cameraScrim", "Lorg/thoughtcrime/securesms/components/ShapeScrim;", "cameraView", "Lorg/thoughtcrime/securesms/components/camera/CameraView;", "scanListener", "Lorg/signal/qr/kitkat/ScanListener;", "scanningThread", "Lorg/signal/qr/kitkat/ScanningThread;", "onAttach", "", "context", "Landroid/content/Context;", "onConfigurationChanged", "newConfiguration", "Landroid/content/res/Configuration;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "viewGroup", "Landroid/view/ViewGroup;", "bundle", "Landroid/os/Bundle;", "onPause", "onResume", "onViewCreated", "view", "savedInstanceState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class VerifyScanFragment extends Fragment {
    private ImageView cameraMarks;
    private ShapeScrim cameraScrim;
    private CameraView cameraView;
    private ScanListener scanListener;
    private ScanningThread scanningThread;

    @Override // androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        ScanListener scanListener;
        Intrinsics.checkNotNullParameter(context, "context");
        super.onAttach(context);
        ArrayList arrayList = new ArrayList();
        try {
            Fragment fragment = getParentFragment();
            while (true) {
                if (fragment == null) {
                    FragmentActivity requireActivity = requireActivity();
                    if (requireActivity != null) {
                        scanListener = (ScanListener) requireActivity;
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type org.signal.qr.kitkat.ScanListener");
                    }
                } else if (fragment instanceof ScanListener) {
                    scanListener = fragment;
                    break;
                } else {
                    String name = fragment.getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                    arrayList.add(name);
                    fragment = fragment.getParentFragment();
                }
            }
            this.scanListener = scanListener;
        } catch (ClassCastException e) {
            String name2 = requireActivity().getClass().getName();
            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
            arrayList.add(name2);
            throw new ListenerNotFoundException(arrayList, e);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Intrinsics.checkNotNullParameter(layoutInflater, "inflater");
        Intrinsics.checkNotNull(viewGroup);
        return ViewUtil.inflate(layoutInflater, viewGroup, R.layout.verify_scan_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.scanner);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.scanner)");
        this.cameraView = (CameraView) findViewById;
        View findViewById2 = view.findViewById(R.id.camera_scrim);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.camera_scrim)");
        this.cameraScrim = (ShapeScrim) findViewById2;
        View findViewById3 = view.findViewById(R.id.camera_marks);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.camera_marks)");
        this.cameraMarks = (ImageView) findViewById3;
        ShapeScrim shapeScrim = this.cameraScrim;
        if (shapeScrim == null) {
            Intrinsics.throwUninitializedPropertyAccessException("cameraScrim");
            shapeScrim = null;
        }
        OneShotPreDrawListener.add(shapeScrim, new Runnable() { // from class: org.thoughtcrime.securesms.verify.VerifyScanFragment$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                VerifyScanFragment.m3301onViewCreated$lambda0(VerifyScanFragment.this);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m3301onViewCreated$lambda0(VerifyScanFragment verifyScanFragment) {
        Intrinsics.checkNotNullParameter(verifyScanFragment, "this$0");
        ShapeScrim shapeScrim = verifyScanFragment.cameraScrim;
        ImageView imageView = null;
        if (shapeScrim == null) {
            Intrinsics.throwUninitializedPropertyAccessException("cameraScrim");
            shapeScrim = null;
        }
        int scrimWidth = shapeScrim.getScrimWidth();
        ShapeScrim shapeScrim2 = verifyScanFragment.cameraScrim;
        if (shapeScrim2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("cameraScrim");
            shapeScrim2 = null;
        }
        int scrimHeight = shapeScrim2.getScrimHeight();
        ImageView imageView2 = verifyScanFragment.cameraMarks;
        if (imageView2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("cameraMarks");
        } else {
            imageView = imageView2;
        }
        ViewUtil.updateLayoutParams(imageView, scrimWidth, scrimHeight);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ScanningThread scanningThread = new ScanningThread();
        this.scanningThread = scanningThread;
        ScanListener scanListener = this.scanListener;
        ScanningThread scanningThread2 = null;
        if (scanListener == null) {
            Intrinsics.throwUninitializedPropertyAccessException("scanListener");
            scanListener = null;
        }
        scanningThread.setScanListener(scanListener);
        ScanningThread scanningThread3 = this.scanningThread;
        if (scanningThread3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("scanningThread");
            scanningThread3 = null;
        }
        scanningThread3.setCharacterSet("ISO-8859-1");
        CameraView cameraView = this.cameraView;
        if (cameraView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("cameraView");
            cameraView = null;
        }
        cameraView.onResume();
        CameraView cameraView2 = this.cameraView;
        if (cameraView2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("cameraView");
            cameraView2 = null;
        }
        ScanningThread scanningThread4 = this.scanningThread;
        if (scanningThread4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("scanningThread");
            scanningThread4 = null;
        }
        cameraView2.setPreviewCallback(scanningThread4);
        ScanningThread scanningThread5 = this.scanningThread;
        if (scanningThread5 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("scanningThread");
        } else {
            scanningThread2 = scanningThread5;
        }
        scanningThread2.start();
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        CameraView cameraView = this.cameraView;
        ScanningThread scanningThread = null;
        if (cameraView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("cameraView");
            cameraView = null;
        }
        cameraView.onPause();
        ScanningThread scanningThread2 = this.scanningThread;
        if (scanningThread2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("scanningThread");
        } else {
            scanningThread = scanningThread2;
        }
        scanningThread.stopScanning();
    }

    @Override // androidx.fragment.app.Fragment, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        Intrinsics.checkNotNullParameter(configuration, "newConfiguration");
        super.onConfigurationChanged(configuration);
        CameraView cameraView = this.cameraView;
        ScanningThread scanningThread = null;
        if (cameraView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("cameraView");
            cameraView = null;
        }
        cameraView.onPause();
        CameraView cameraView2 = this.cameraView;
        if (cameraView2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("cameraView");
            cameraView2 = null;
        }
        cameraView2.onResume();
        CameraView cameraView3 = this.cameraView;
        if (cameraView3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("cameraView");
            cameraView3 = null;
        }
        ScanningThread scanningThread2 = this.scanningThread;
        if (scanningThread2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("scanningThread");
        } else {
            scanningThread = scanningThread2;
        }
        cameraView3.setPreviewCallback(scanningThread);
    }
}
