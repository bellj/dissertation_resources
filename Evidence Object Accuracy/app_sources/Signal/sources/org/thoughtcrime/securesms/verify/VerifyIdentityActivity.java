package org.thoughtcrime.securesms.verify;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import org.signal.libsignal.protocol.IdentityKey;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.crypto.IdentityKeyParcelable;
import org.thoughtcrime.securesms.database.IdentityDatabase;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;

/* loaded from: classes.dex */
public class VerifyIdentityActivity extends PassphraseRequiredActivity {
    private static final String IDENTITY_EXTRA;
    private static final String RECIPIENT_EXTRA;
    private static final String VERIFIED_EXTRA;
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();

    public static Intent newIntent(Context context, IdentityRecord identityRecord) {
        return newIntent(context, identityRecord.getRecipientId(), identityRecord.getIdentityKey(), identityRecord.getVerifiedStatus() == IdentityDatabase.VerifiedStatus.VERIFIED);
    }

    public static Intent newIntent(Context context, IdentityRecord identityRecord, boolean z) {
        return newIntent(context, identityRecord.getRecipientId(), identityRecord.getIdentityKey(), z);
    }

    public static Intent newIntent(Context context, RecipientId recipientId, IdentityKey identityKey, boolean z) {
        Intent intent = new Intent(context, VerifyIdentityActivity.class);
        intent.putExtra("recipient_id", recipientId);
        intent.putExtra(IDENTITY_EXTRA, new IdentityKeyParcelable(identityKey));
        intent.putExtra(VERIFIED_EXTRA, z);
        return intent;
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        super.onCreate(bundle, z);
        this.dynamicTheme.onCreate(this);
        getSupportFragmentManager().beginTransaction().replace(16908290, VerifyIdentityFragment.create((RecipientId) getIntent().getParcelableExtra("recipient_id"), (IdentityKeyParcelable) getIntent().getParcelableExtra(IDENTITY_EXTRA), getIntent().getBooleanExtra(VERIFIED_EXTRA, false))).commitAllowingStateLoss();
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }
}
