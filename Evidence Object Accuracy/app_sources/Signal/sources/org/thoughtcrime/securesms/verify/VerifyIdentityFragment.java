package org.thoughtcrime.securesms.verify;

import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Toast;
import androidx.core.os.BundleKt;
import androidx.fragment.app.Fragment;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.ThreadUtil;
import org.signal.qr.kitkat.ScanListener;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.WrapperDialogFragment;
import org.thoughtcrime.securesms.crypto.IdentityKeyParcelable;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.verify.VerifyDisplayFragment;

/* compiled from: VerifyIdentityFragment.kt */
@Metadata(d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0011\n\u0000\n\u0002\u0010\u0015\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 +2\u00020\u00012\u00020\u00022\u00020\u0003:\u0002+,B\u0005¢\u0006\u0002\u0010\u0004J\b\u0010\u0019\u001a\u00020\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J-\u0010\u001e\u001a\u00020\u001a2\u0006\u0010\u001f\u001a\u00020 2\u000e\u0010!\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u001d0\"2\u0006\u0010#\u001a\u00020$H\u0016¢\u0006\u0002\u0010%J\u001a\u0010&\u001a\u00020\u001a2\u0006\u0010'\u001a\u00020(2\b\u0010)\u001a\u0004\u0018\u00010*H\u0016R#\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u00068BX\u0002¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\b\u0010\tR\u0014\u0010\f\u001a\u00020\r8BX\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\u000eR\u0014\u0010\u000f\u001a\u00020\u00108BX\u0004¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\u00020\u00148BX\u0004¢\u0006\u0006\u001a\u0004\b\u0015\u0010\u0016R\u000e\u0010\u0017\u001a\u00020\u0018X\u0004¢\u0006\u0002\n\u0000¨\u0006-"}, d2 = {"Lorg/thoughtcrime/securesms/verify/VerifyIdentityFragment;", "Landroidx/fragment/app/Fragment;", "Lorg/signal/qr/kitkat/ScanListener;", "Lorg/thoughtcrime/securesms/verify/VerifyDisplayFragment$Callback;", "()V", "displayFragment", "Lorg/thoughtcrime/securesms/verify/VerifyDisplayFragment;", "kotlin.jvm.PlatformType", "getDisplayFragment", "()Lorg/thoughtcrime/securesms/verify/VerifyDisplayFragment;", "displayFragment$delegate", "Lkotlin/Lazy;", "isVerified", "", "()Z", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "remoteIdentity", "Lorg/thoughtcrime/securesms/crypto/IdentityKeyParcelable;", "getRemoteIdentity", "()Lorg/thoughtcrime/securesms/crypto/IdentityKeyParcelable;", "scanFragment", "Lorg/thoughtcrime/securesms/verify/VerifyScanFragment;", "onQrCodeContainerClicked", "", "onQrDataFound", "data", "", "onRequestPermissionsResult", "requestCode", "", "permissions", "", "grantResults", "", "(I[Ljava/lang/String;[I)V", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "Dialog", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class VerifyIdentityFragment extends Fragment implements ScanListener, VerifyDisplayFragment.Callback {
    public static final Companion Companion = new Companion(null);
    private static final String EXTRA_IDENTITY;
    private static final String EXTRA_RECIPIENT;
    private static final String EXTRA_VERIFIED;
    private final Lazy displayFragment$delegate = LazyKt__LazyJVMKt.lazy(new Function0<VerifyDisplayFragment>(this) { // from class: org.thoughtcrime.securesms.verify.VerifyIdentityFragment$displayFragment$2
        final /* synthetic */ VerifyIdentityFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final VerifyDisplayFragment invoke() {
            return VerifyDisplayFragment.create(this.this$0.getRecipientId(), this.this$0.getRemoteIdentity(), new IdentityKeyParcelable(SignalStore.account().getAciIdentityKey().getPublicKey()), Recipient.self().requireE164(), this.this$0.isVerified());
        }
    });
    private final VerifyScanFragment scanFragment = new VerifyScanFragment();

    @JvmStatic
    public static final VerifyIdentityFragment create(RecipientId recipientId, IdentityKeyParcelable identityKeyParcelable, boolean z) {
        return Companion.create(recipientId, identityKeyParcelable, z);
    }

    public VerifyIdentityFragment() {
        super(R.layout.fragment_container);
    }

    /* compiled from: VerifyIdentityFragment.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/verify/VerifyIdentityFragment$Dialog;", "Lorg/thoughtcrime/securesms/components/WrapperDialogFragment;", "()V", "getWrappedFragment", "Landroidx/fragment/app/Fragment;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Dialog extends WrapperDialogFragment {
        @Override // org.thoughtcrime.securesms.components.WrapperDialogFragment
        public Fragment getWrappedFragment() {
            VerifyIdentityFragment verifyIdentityFragment = new VerifyIdentityFragment();
            verifyIdentityFragment.setArguments(requireArguments());
            return verifyIdentityFragment;
        }
    }

    /* compiled from: VerifyIdentityFragment.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0007J\u001e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/verify/VerifyIdentityFragment$Companion;", "", "()V", "EXTRA_IDENTITY", "", "EXTRA_RECIPIENT", "EXTRA_VERIFIED", "create", "Lorg/thoughtcrime/securesms/verify/VerifyIdentityFragment;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "remoteIdentity", "Lorg/thoughtcrime/securesms/crypto/IdentityKeyParcelable;", "verified", "", "createDialog", "Lorg/thoughtcrime/securesms/verify/VerifyIdentityFragment$Dialog;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final VerifyIdentityFragment create(RecipientId recipientId, IdentityKeyParcelable identityKeyParcelable, boolean z) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(identityKeyParcelable, "remoteIdentity");
            VerifyIdentityFragment verifyIdentityFragment = new VerifyIdentityFragment();
            verifyIdentityFragment.setArguments(BundleKt.bundleOf(TuplesKt.to(VerifyIdentityFragment.EXTRA_RECIPIENT, recipientId), TuplesKt.to(VerifyIdentityFragment.EXTRA_IDENTITY, identityKeyParcelable), TuplesKt.to(VerifyIdentityFragment.EXTRA_VERIFIED, Boolean.valueOf(z))));
            return verifyIdentityFragment;
        }

        public final Dialog createDialog(RecipientId recipientId, IdentityKeyParcelable identityKeyParcelable, boolean z) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(identityKeyParcelable, "remoteIdentity");
            Dialog dialog = new Dialog();
            dialog.setArguments(BundleKt.bundleOf(TuplesKt.to(VerifyIdentityFragment.EXTRA_RECIPIENT, recipientId), TuplesKt.to(VerifyIdentityFragment.EXTRA_IDENTITY, identityKeyParcelable), TuplesKt.to(VerifyIdentityFragment.EXTRA_VERIFIED, Boolean.valueOf(z))));
            return dialog;
        }
    }

    private final VerifyDisplayFragment getDisplayFragment() {
        return (VerifyDisplayFragment) this.displayFragment$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        getChildFragmentManager().beginTransaction().replace(R.id.fragment_container, getDisplayFragment()).commitAllowingStateLoss();
    }

    public final RecipientId getRecipientId() {
        Parcelable parcelable = requireArguments().getParcelable(EXTRA_RECIPIENT);
        Intrinsics.checkNotNull(parcelable);
        return (RecipientId) parcelable;
    }

    public final IdentityKeyParcelable getRemoteIdentity() {
        Parcelable parcelable = requireArguments().getParcelable(EXTRA_IDENTITY);
        Intrinsics.checkNotNull(parcelable);
        return (IdentityKeyParcelable) parcelable;
    }

    public final boolean isVerified() {
        return requireArguments().getBoolean(EXTRA_VERIFIED);
    }

    @Override // org.signal.qr.kitkat.ScanListener
    public void onQrDataFound(String str) {
        Intrinsics.checkNotNullParameter(str, "data");
        ThreadUtil.runOnMain(new Runnable(str) { // from class: org.thoughtcrime.securesms.verify.VerifyIdentityFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                VerifyIdentityFragment.m3300onQrDataFound$lambda0(VerifyIdentityFragment.this, this.f$1);
            }
        });
    }

    /* renamed from: onQrDataFound$lambda-0 */
    public static final void m3300onQrDataFound$lambda0(VerifyIdentityFragment verifyIdentityFragment, String str) {
        Intrinsics.checkNotNullParameter(verifyIdentityFragment, "this$0");
        Intrinsics.checkNotNullParameter(str, "$data");
        ServiceUtil.getVibrator(verifyIdentityFragment.getContext()).vibrate(50);
        verifyIdentityFragment.getChildFragmentManager().popBackStack();
        verifyIdentityFragment.getDisplayFragment().setScannedFingerprint(str);
    }

    @Override // org.thoughtcrime.securesms.verify.VerifyDisplayFragment.Callback
    public void onQrCodeContainerClicked() {
        Permissions.with(this).request("android.permission.CAMERA").ifNecessary().withPermanentDenialDialog(getString(R.string.VerifyIdentityActivity_signal_needs_the_camera_permission_in_order_to_scan_a_qr_code_but_it_has_been_permanently_denied)).onAllGranted(new Runnable() { // from class: org.thoughtcrime.securesms.verify.VerifyIdentityFragment$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                VerifyIdentityFragment.m3298onQrCodeContainerClicked$lambda1(VerifyIdentityFragment.this);
            }
        }).onAnyDenied(new Runnable() { // from class: org.thoughtcrime.securesms.verify.VerifyIdentityFragment$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                VerifyIdentityFragment.m3299onQrCodeContainerClicked$lambda2(VerifyIdentityFragment.this);
            }
        }).execute();
    }

    /* renamed from: onQrCodeContainerClicked$lambda-1 */
    public static final void m3298onQrCodeContainerClicked$lambda1(VerifyIdentityFragment verifyIdentityFragment) {
        Intrinsics.checkNotNullParameter(verifyIdentityFragment, "this$0");
        verifyIdentityFragment.getChildFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_from_top, R.anim.slide_to_bottom, R.anim.slide_from_bottom, R.anim.slide_to_top).replace(R.id.fragment_container, verifyIdentityFragment.scanFragment).addToBackStack(null).commitAllowingStateLoss();
    }

    /* renamed from: onQrCodeContainerClicked$lambda-2 */
    public static final void m3299onQrCodeContainerClicked$lambda2(VerifyIdentityFragment verifyIdentityFragment) {
        Intrinsics.checkNotNullParameter(verifyIdentityFragment, "this$0");
        Toast.makeText(verifyIdentityFragment.requireContext(), (int) R.string.VerifyIdentityActivity_unable_to_scan_qr_code_without_camera_permission, 1).show();
    }

    @Override // androidx.fragment.app.Fragment
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        Intrinsics.checkNotNullParameter(strArr, "permissions");
        Intrinsics.checkNotNullParameter(iArr, "grantResults");
        Permissions.onRequestPermissionsResult(this, i, strArr, iArr);
    }
}
