package org.thoughtcrime.securesms.notifications.v2;

import android.content.Context;
import android.net.Uri;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsKt;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.contactshare.ContactUtil;
import org.thoughtcrime.securesms.database.MentionUtil;
import org.thoughtcrime.securesms.database.MmsSmsDatabase;
import org.thoughtcrime.securesms.database.ReactionDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.database.model.ReactionRecord;
import org.thoughtcrime.securesms.notifications.v2.NotificationItemV2;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.service.KeyCachingService;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.MessageRecordUtil;
import org.thoughtcrime.securesms.util.SpanUtil;

/* compiled from: NotificationItemV2.kt */
@Metadata(d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010\u0015\u001a\u00020\r2\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\n\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0016J\n\u0010\u001a\u001a\u0004\u0018\u00010\u0019H\u0016J\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u0016\u001a\u00020\u0017H\u0014J\u0010\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u0010\u0010\u001e\u001a\u00020\u001f2\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0010\u0010 \u001a\u00020!2\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\b\u0010\"\u001a\u00020#H\u0016R\u0014\u0010\t\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\u00020\rX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014¨\u0006$"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/ReactionNotification;", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationItemV2;", "threadRecipient", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "record", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", ReactionDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/database/model/ReactionRecord;", "(Lorg/thoughtcrime/securesms/recipients/Recipient;Lorg/thoughtcrime/securesms/database/model/MessageRecord;Lorg/thoughtcrime/securesms/database/model/ReactionRecord;)V", "individualRecipient", "getIndividualRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "isNewNotification", "", "()Z", "getReaction", "()Lorg/thoughtcrime/securesms/database/model/ReactionRecord;", "timestamp", "", "getTimestamp", "()J", "canReply", "context", "Landroid/content/Context;", "getBigPictureUri", "Landroid/net/Uri;", "getLargeIconUri", "getPrimaryTextActual", "", "getReactionMessageBody", "getStartingPosition", "", "getThumbnailInfo", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationItemV2$ThumbnailInfo;", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ReactionNotification extends NotificationItemV2 {
    private final Recipient individualRecipient;
    private final boolean isNewNotification;
    private final ReactionRecord reaction;
    private final long timestamp;

    @Override // org.thoughtcrime.securesms.notifications.v2.NotificationItemV2
    public boolean canReply(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        return false;
    }

    @Override // org.thoughtcrime.securesms.notifications.v2.NotificationItemV2
    public Uri getBigPictureUri() {
        return null;
    }

    @Override // org.thoughtcrime.securesms.notifications.v2.NotificationItemV2
    public Uri getLargeIconUri() {
        return null;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ReactionNotification(Recipient recipient, MessageRecord messageRecord, ReactionRecord reactionRecord) {
        super(recipient, messageRecord, null);
        Intrinsics.checkNotNullParameter(recipient, "threadRecipient");
        Intrinsics.checkNotNullParameter(messageRecord, "record");
        Intrinsics.checkNotNullParameter(reactionRecord, ReactionDatabase.TABLE_NAME);
        this.reaction = reactionRecord;
        this.timestamp = reactionRecord.getDateReceived();
        Recipient resolved = Recipient.resolved(reactionRecord.getAuthor());
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(reaction.author)");
        this.individualRecipient = resolved;
        this.isNewNotification = getTimestamp() > getNotifiedTimestamp();
    }

    public final ReactionRecord getReaction() {
        return this.reaction;
    }

    @Override // org.thoughtcrime.securesms.notifications.v2.NotificationItemV2
    public long getTimestamp() {
        return this.timestamp;
    }

    @Override // org.thoughtcrime.securesms.notifications.v2.NotificationItemV2
    public Recipient getIndividualRecipient() {
        return this.individualRecipient;
    }

    @Override // org.thoughtcrime.securesms.notifications.v2.NotificationItemV2
    public boolean isNewNotification() {
        return this.isNewNotification;
    }

    @Override // org.thoughtcrime.securesms.notifications.v2.NotificationItemV2
    protected CharSequence getPrimaryTextActual(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (KeyCachingService.isLocked(context)) {
            CharSequence italic = SpanUtil.italic(context.getString(R.string.MessageNotifier_locked_message));
            Intrinsics.checkNotNullExpressionValue(italic, "{\n      SpanUtil.italic(…er_locked_message))\n    }");
            return italic;
        }
        int i = 0;
        Object[] array = StringsKt__StringsKt.split$default((CharSequence) SpanUtil.italic(getReactionMessageBody(context)).toString(), new String[]{"__EMOJI__"}, false, 0, 6, (Object) null).toArray(new String[0]);
        if (array != null) {
            String[] strArr = (String[]) array;
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
            int length = strArr.length;
            int i2 = 0;
            while (i < length) {
                int i3 = i2 + 1;
                spannableStringBuilder.append(SpanUtil.italic(strArr[i]));
                if (i2 != strArr.length - 1) {
                    spannableStringBuilder.append((CharSequence) this.reaction.getEmoji());
                }
                i++;
                i2 = i3;
            }
            return spannableStringBuilder;
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
    }

    private final CharSequence getReactionMessageBody(Context context) {
        CharSequence updateBodyWithDisplayNames = MentionUtil.updateBodyWithDisplayNames(context, getRecord());
        Intrinsics.checkNotNullExpressionValue(updateBodyWithDisplayNames, "updateBodyWithDisplayNames(context, record)");
        boolean isEmpty = TextUtils.isEmpty(updateBodyWithDisplayNames);
        if (MessageRecordUtil.hasSharedContact(getRecord())) {
            Contact contact = ((MmsMessageRecord) getRecord()).getSharedContacts().get(0);
            Intrinsics.checkNotNullExpressionValue(contact, "(record as MmsMessageRecord).sharedContacts[0]");
            Object stringSummary = ContactUtil.getStringSummary(context, contact);
            Intrinsics.checkNotNullExpressionValue(stringSummary, "getStringSummary(context, contact)");
            String string = context.getString(R.string.MessageNotifier_reacted_s_to_s, "__EMOJI__", stringSummary);
            Intrinsics.checkNotNullExpressionValue(string, "{\n      val contact: Con…NT_STRING, summary)\n    }");
            return string;
        } else if (MessageRecordUtil.hasSticker(getRecord())) {
            String string2 = context.getString(R.string.MessageNotifier_reacted_s_to_your_sticker, "__EMOJI__");
            Intrinsics.checkNotNullExpressionValue(string2, "{\n      context.getStrin…REPLACEMENT_STRING)\n    }");
            return string2;
        } else if (getRecord().isMms() && getRecord().isViewOnce()) {
            String string3 = context.getString(R.string.MessageNotifier_reacted_s_to_your_view_once_media, "__EMOJI__");
            Intrinsics.checkNotNullExpressionValue(string3, "{\n      context.getStrin…REPLACEMENT_STRING)\n    }");
            return string3;
        } else if (!isEmpty) {
            String string4 = context.getString(R.string.MessageNotifier_reacted_s_to_s, "__EMOJI__", updateBodyWithDisplayNames);
            Intrinsics.checkNotNullExpressionValue(string4, "{\n      context.getStrin…EMENT_STRING, body)\n    }");
            return string4;
        } else if (MessageRecordUtil.isMediaMessage(getRecord()) && MediaUtil.isVideoType(getMessageContentType((MmsMessageRecord) getRecord()))) {
            String string5 = context.getString(R.string.MessageNotifier_reacted_s_to_your_video, "__EMOJI__");
            Intrinsics.checkNotNullExpressionValue(string5, "{\n      context.getStrin…REPLACEMENT_STRING)\n    }");
            return string5;
        } else if (MessageRecordUtil.isMediaMessage(getRecord()) && MediaUtil.isGif(getMessageContentType((MmsMessageRecord) getRecord()))) {
            String string6 = context.getString(R.string.MessageNotifier_reacted_s_to_your_gif, "__EMOJI__");
            Intrinsics.checkNotNullExpressionValue(string6, "{\n      context.getStrin…REPLACEMENT_STRING)\n    }");
            return string6;
        } else if (MessageRecordUtil.isMediaMessage(getRecord()) && MediaUtil.isImageType(getMessageContentType((MmsMessageRecord) getRecord()))) {
            String string7 = context.getString(R.string.MessageNotifier_reacted_s_to_your_image, "__EMOJI__");
            Intrinsics.checkNotNullExpressionValue(string7, "{\n      context.getStrin…REPLACEMENT_STRING)\n    }");
            return string7;
        } else if (MessageRecordUtil.isMediaMessage(getRecord()) && MediaUtil.isAudioType(getMessageContentType((MmsMessageRecord) getRecord()))) {
            String string8 = context.getString(R.string.MessageNotifier_reacted_s_to_your_audio, "__EMOJI__");
            Intrinsics.checkNotNullExpressionValue(string8, "{\n      context.getStrin…REPLACEMENT_STRING)\n    }");
            return string8;
        } else if (MessageRecordUtil.isMediaMessage(getRecord())) {
            String string9 = context.getString(R.string.MessageNotifier_reacted_s_to_your_file, "__EMOJI__");
            Intrinsics.checkNotNullExpressionValue(string9, "{\n      context.getStrin…REPLACEMENT_STRING)\n    }");
            return string9;
        } else {
            String string10 = context.getString(R.string.MessageNotifier_reacted_s_to_s, "__EMOJI__", updateBodyWithDisplayNames);
            Intrinsics.checkNotNullExpressionValue(string10, "{\n      context.getStrin…EMENT_STRING, body)\n    }");
            return string10;
        }
    }

    @Override // org.thoughtcrime.securesms.notifications.v2.NotificationItemV2
    public int getStartingPosition(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        MmsSmsDatabase mmsSms = SignalDatabase.Companion.mmsSms();
        long threadId = getThread().getThreadId();
        Long groupStoryId = getThread().getGroupStoryId();
        return mmsSms.getMessagePositionInConversation(threadId, groupStoryId != null ? groupStoryId.longValue() : 0, getRecord().getDateReceived());
    }

    @Override // org.thoughtcrime.securesms.notifications.v2.NotificationItemV2
    public NotificationItemV2.ThumbnailInfo getThumbnailInfo(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        return new NotificationItemV2.ThumbnailInfo(null, null, 3, null);
    }

    @Override // java.lang.Object
    public String toString() {
        return "ReactionNotification(timestamp=" + getTimestamp() + ", isNewNotification=" + isNewNotification() + ')';
    }
}
