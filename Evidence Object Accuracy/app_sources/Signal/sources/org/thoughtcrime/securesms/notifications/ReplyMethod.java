package org.thoughtcrime.securesms.notifications;

import android.content.Context;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public enum ReplyMethod {
    GroupMessage,
    SecureMessage,
    UnsecuredSmsMessage;

    public static ReplyMethod forRecipient(Context context, Recipient recipient) {
        if (recipient.isGroup()) {
            return GroupMessage;
        }
        if (!SignalStore.account().isRegistered() || recipient.getRegistered() != RecipientDatabase.RegisteredState.REGISTERED || recipient.isForceSmsSelection()) {
            return UnsecuredSmsMessage;
        }
        return SecureMessage;
    }
}
