package org.thoughtcrime.securesms.notifications;

import android.app.NotificationManager;
import android.content.Context;
import android.database.Cursor;
import android.os.Build;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.ServiceUtil;

/* loaded from: classes4.dex */
public final class DoNotDisturbUtil {
    private static final String TAG = Log.tag(DoNotDisturbUtil.class);

    private DoNotDisturbUtil() {
    }

    public static boolean shouldDisturbUserWithCall(Context context) {
        int currentInterruptionFilter;
        if (Build.VERSION.SDK_INT <= 23 || (currentInterruptionFilter = ServiceUtil.getNotificationManager(context).getCurrentInterruptionFilter()) == 0 || currentInterruptionFilter == 1) {
            return true;
        }
        return false;
    }

    public static boolean shouldDisturbUserWithCall(Context context, Recipient recipient) {
        if (Build.VERSION.SDK_INT <= 23) {
            return true;
        }
        NotificationManager notificationManager = ServiceUtil.getNotificationManager(context);
        int currentInterruptionFilter = notificationManager.getCurrentInterruptionFilter();
        if (currentInterruptionFilter == 2) {
            return handlePriority(context, notificationManager, recipient);
        }
        if (currentInterruptionFilter == 3 || currentInterruptionFilter == 4) {
            return false;
        }
        return true;
    }

    private static boolean handlePriority(Context context, NotificationManager notificationManager, Recipient recipient) {
        if (Build.VERSION.SDK_INT >= 28 || notificationManager.isNotificationPolicyAccessGranted()) {
            NotificationManager.Policy notificationPolicy = notificationManager.getNotificationPolicy();
            int i = notificationPolicy.priorityCategories;
            boolean z = (i & 8) != 0;
            boolean z2 = (i & 16) != 0;
            if (!z && !z2) {
                return false;
            }
            if (z && !z2) {
                return isContactPriority(context, recipient, notificationPolicy.priorityCallSenders);
            }
            if (!z) {
                return isRepeatCaller(context, recipient);
            }
            if (isContactPriority(context, recipient, notificationPolicy.priorityCallSenders) || isRepeatCaller(context, recipient)) {
                return true;
            }
            return false;
        }
        Log.w(TAG, "Notification Policy is not granted");
        return true;
    }

    private static boolean isContactPriority(Context context, Recipient recipient, int i) {
        if (i == 0) {
            return true;
        }
        if (i == 1) {
            return recipient.resolve().isSystemContact();
        }
        if (i == 2) {
            return isContactStarred(context, recipient);
        }
        String str = TAG;
        Log.w(str, "Unknown priority " + i);
        return true;
    }

    private static boolean isContactStarred(Context context, Recipient recipient) {
        boolean z = false;
        if (!recipient.resolve().isSystemContact() || !Permissions.hasAny(context, "android.permission.READ_CONTACTS", "android.permission.WRITE_CONTACTS")) {
            return false;
        }
        Cursor query = context.getContentResolver().query(recipient.resolve().getContactUri(), new String[]{"starred"}, null, null, null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    if (CursorUtil.requireInt(query, "starred") == 1) {
                        z = true;
                    }
                    query.close();
                    return z;
                }
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return false;
    }

    private static boolean isRepeatCaller(Context context, Recipient recipient) {
        return SignalDatabase.threads().hasCalledSince(recipient, System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(15));
    }
}
