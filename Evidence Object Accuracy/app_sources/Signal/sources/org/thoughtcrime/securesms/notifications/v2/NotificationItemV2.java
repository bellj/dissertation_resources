package org.thoughtcrime.securesms.notifications.v2;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.SpannableStringBuilder;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.database.MentionUtil;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.SlideDeck;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.Util;

/* compiled from: NotificationItemV2.kt */
@Metadata(d1 = {"\u0000z\n\u0002\u0018\u0002\n\u0002\u0010\u000f\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001>B\u0017\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\"\u001a\u00020\u000f2\u0006\u0010#\u001a\u00020$H&J\u0011\u0010%\u001a\u00020&2\u0006\u0010'\u001a\u00020\u0000H\u0002J\n\u0010(\u001a\u0004\u0018\u00010)H&J\u0010\u0010*\u001a\u0004\u0018\u00010+2\u0006\u0010#\u001a\u00020$J\n\u0010,\u001a\u0004\u0018\u00010)H&J\u0010\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u000200H\u0004J\u0010\u00101\u001a\u0004\u0018\u0001022\u0006\u0010#\u001a\u00020$J\u000e\u00103\u001a\u00020+2\u0006\u0010#\u001a\u00020$J\b\u00104\u001a\u0004\u0018\u00010.J\u000e\u00105\u001a\u00020+2\u0006\u0010#\u001a\u00020$J\u0010\u00106\u001a\u00020+2\u0006\u0010#\u001a\u00020$H$J\u0010\u00107\u001a\u00020&2\u0006\u0010#\u001a\u00020$H&J\u0018\u00108\u001a\u00020+2\u0006\u0010#\u001a\u00020$2\b\b\u0002\u00109\u001a\u00020\u000fJ\u0010\u0010:\u001a\u00020;2\u0006\u0010#\u001a\u00020$H&J\u000e\u0010<\u001a\u00020\u000f2\u0006\u0010'\u001a\u00020\u0000J\u000e\u0010=\u001a\u00020+*\u0004\u0018\u00010+H\u0002R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0012\u0010\u000b\u001a\u00020\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\rR\u0011\u0010\u000e\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u0010R\u0011\u0010\u0011\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0010R\u0012\u0010\u0012\u001a\u00020\u000fX¦\u0004¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0010R\u0014\u0010\u0013\u001a\u00020\bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\nR\u0014\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0013\u0010\u0017\u001a\u0004\u0018\u00010\u0018¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\u001b\u001a\u00020\u001c¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\rR\u0012\u0010 \u001a\u00020\bX¦\u0004¢\u0006\u0006\u001a\u0004\b!\u0010\n\u0001\u0002?@¨\u0006A"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/NotificationItemV2;", "", "threadRecipient", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "record", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "(Lorg/thoughtcrime/securesms/recipients/Recipient;Lorg/thoughtcrime/securesms/database/model/MessageRecord;)V", ContactRepository.ID_COLUMN, "", "getId", "()J", "individualRecipient", "getIndividualRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "isJoined", "", "()Z", "isMms", "isNewNotification", "notifiedTimestamp", "getNotifiedTimestamp", "getRecord", "()Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "slideDeck", "Lorg/thoughtcrime/securesms/mms/SlideDeck;", "getSlideDeck", "()Lorg/thoughtcrime/securesms/mms/SlideDeck;", ThreadDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/notifications/v2/ConversationId;", "getThread", "()Lorg/thoughtcrime/securesms/notifications/v2/ConversationId;", "getThreadRecipient", "timestamp", "getTimestamp", "canReply", "context", "Landroid/content/Context;", "compareTo", "", "other", "getBigPictureUri", "Landroid/net/Uri;", "getInboxLine", "", "getLargeIconUri", "getMessageContentType", "", "messageRecord", "Lorg/thoughtcrime/securesms/database/model/MmsMessageRecord;", "getPersonIcon", "Landroid/graphics/Bitmap;", "getPersonName", "getPersonUri", "getPrimaryText", "getPrimaryTextActual", "getStartingPosition", "getStyledPrimaryText", "trimmed", "getThumbnailInfo", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationItemV2$ThumbnailInfo;", "hasSameContent", "trimToDisplayLength", "ThumbnailInfo", "Lorg/thoughtcrime/securesms/notifications/v2/MessageNotification;", "Lorg/thoughtcrime/securesms/notifications/v2/ReactionNotification;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class NotificationItemV2 implements Comparable<NotificationItemV2> {
    private final long id;
    private final boolean isJoined;
    private final boolean isMms;
    private final long notifiedTimestamp;
    private final MessageRecord record;
    private final SlideDeck slideDeck;
    private final ConversationId thread;
    private final Recipient threadRecipient;

    public /* synthetic */ NotificationItemV2(Recipient recipient, MessageRecord messageRecord, DefaultConstructorMarker defaultConstructorMarker) {
        this(recipient, messageRecord);
    }

    public abstract boolean canReply(Context context);

    public abstract Uri getBigPictureUri();

    public abstract Recipient getIndividualRecipient();

    public abstract Uri getLargeIconUri();

    protected abstract CharSequence getPrimaryTextActual(Context context);

    public abstract int getStartingPosition(Context context);

    public abstract ThumbnailInfo getThumbnailInfo(Context context);

    public abstract long getTimestamp();

    public abstract boolean isNewNotification();

    private NotificationItemV2(Recipient recipient, MessageRecord messageRecord) {
        this.threadRecipient = recipient;
        this.record = messageRecord;
        this.id = messageRecord.getId();
        this.thread = ConversationId.Companion.fromMessageRecord(messageRecord);
        this.isMms = messageRecord.isMms();
        SlideDeck slideDeck = null;
        if (!messageRecord.isViewOnce()) {
            MmsMessageRecord mmsMessageRecord = messageRecord instanceof MmsMessageRecord ? (MmsMessageRecord) messageRecord : null;
            if (mmsMessageRecord != null) {
                slideDeck = mmsMessageRecord.getSlideDeck();
            }
        }
        this.slideDeck = slideDeck;
        this.isJoined = messageRecord.isJoined();
        this.notifiedTimestamp = messageRecord.getNotifiedTimestamp();
    }

    public final MessageRecord getRecord() {
        return this.record;
    }

    public final Recipient getThreadRecipient() {
        return this.threadRecipient;
    }

    public final long getId() {
        return this.id;
    }

    public final ConversationId getThread() {
        return this.thread;
    }

    public final boolean isMms() {
        return this.isMms;
    }

    public final SlideDeck getSlideDeck() {
        return this.slideDeck;
    }

    public final boolean isJoined() {
        return this.isJoined;
    }

    public final long getNotifiedTimestamp() {
        return this.notifiedTimestamp;
    }

    public final String getMessageContentType(MmsMessageRecord mmsMessageRecord) {
        Intrinsics.checkNotNullParameter(mmsMessageRecord, "messageRecord");
        Slide thumbnailSlide = mmsMessageRecord.getSlideDeck().getThumbnailSlide();
        String str = MediaUtil.IMAGE_GIF;
        if (thumbnailSlide == null) {
            Slide firstSlide = mmsMessageRecord.getSlideDeck().getFirstSlide();
            if (firstSlide == null || !firstSlide.isVideoGif()) {
                if (firstSlide != null) {
                    str = firstSlide.getContentType();
                } else {
                    Log.w(NotificationItemV2Kt.TAG, "Could not distinguish content type from message record, defaulting to JPEG");
                    str = MediaUtil.IMAGE_JPEG;
                }
            }
            Intrinsics.checkNotNullExpressionValue(str, "{\n      val slide: Slide….IMAGE_JPEG\n      }\n    }");
        } else {
            if (!thumbnailSlide.isVideoGif()) {
                str = thumbnailSlide.getContentType();
            }
            Intrinsics.checkNotNullExpressionValue(str, "{\n      if (thumbnailSli…contentType\n      }\n    }");
        }
        return str;
    }

    public static /* synthetic */ CharSequence getStyledPrimaryText$default(NotificationItemV2 notificationItemV2, Context context, boolean z, int i, Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                z = false;
            }
            return notificationItemV2.getStyledPrimaryText(context, z);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getStyledPrimaryText");
    }

    public final CharSequence getStyledPrimaryText(Context context, boolean z) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (SignalStore.settings().getMessageNotificationsPrivacy().isDisplayNothing()) {
            String string = context.getString(R.string.SingleRecipientNotificationBuilder_new_message);
            Intrinsics.checkNotNullExpressionValue(string, "{\n      context.getStrin…uilder_new_message)\n    }");
            return string;
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        spannableStringBuilder.append(Util.getBoldedString(getIndividualRecipient().getShortDisplayNameIncludingUsername(context)));
        if (!Intrinsics.areEqual(this.threadRecipient, getIndividualRecipient())) {
            spannableStringBuilder.append(Util.getBoldedString(MentionUtil.MENTION_STARTER + this.threadRecipient.getDisplayName(context)));
        }
        spannableStringBuilder.append((CharSequence) ": ");
        CharSequence primaryText = getPrimaryText(context);
        if (z) {
            trimToDisplayLength(primaryText);
        }
        spannableStringBuilder.append(primaryText);
        return spannableStringBuilder;
    }

    public final CharSequence getPersonName(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (SignalStore.settings().getMessageNotificationsPrivacy().isDisplayContact()) {
            String displayName = getIndividualRecipient().getDisplayName(context);
            Intrinsics.checkNotNullExpressionValue(displayName, "{\n      individualRecipi…isplayName(context)\n    }");
            return displayName;
        }
        String string = context.getString(R.string.SingleRecipientNotificationBuilder_signal);
        Intrinsics.checkNotNullExpressionValue(string, "{\n      context.getStrin…tionBuilder_signal)\n    }");
        return string;
    }

    public int compareTo(NotificationItemV2 notificationItemV2) {
        Intrinsics.checkNotNullParameter(notificationItemV2, "other");
        return Intrinsics.compare(getTimestamp(), notificationItemV2.getTimestamp());
    }

    public final String getPersonUri() {
        if (!SignalStore.settings().getMessageNotificationsPrivacy().isDisplayContact() || !getIndividualRecipient().isSystemContact()) {
            return null;
        }
        return String.valueOf(getIndividualRecipient().getContactUri());
    }

    public final Bitmap getPersonIcon(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (SignalStore.settings().getMessageNotificationsPrivacy().isDisplayContact()) {
            return NotificationExtensionsKt.toLargeBitmap(NotificationExtensionsKt.getContactDrawable(getIndividualRecipient(), context), context);
        }
        return null;
    }

    public final CharSequence getPrimaryText(Context context) {
        CharSequence charSequence;
        Intrinsics.checkNotNullParameter(context, "context");
        if (SignalStore.settings().getMessageNotificationsPrivacy().isDisplayMessage()) {
            if (RecipientUtil.isMessageRequestAccepted(context, this.thread.getThreadId())) {
                charSequence = getPrimaryTextActual(context);
            } else {
                charSequence = SpanUtil.italic(context.getString(R.string.SingleRecipientNotificationBuilder_message_request));
            }
            Intrinsics.checkNotNullExpressionValue(charSequence, "{\n      if (RecipientUti…e_request))\n      }\n    }");
            return charSequence;
        }
        String string = context.getString(R.string.SingleRecipientNotificationBuilder_new_message);
        Intrinsics.checkNotNullExpressionValue(string, "{\n      context.getStrin…uilder_new_message)\n    }");
        return string;
    }

    public final CharSequence getInboxLine(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (SignalStore.settings().getMessageNotificationsPrivacy().isDisplayNothing()) {
            return null;
        }
        return getStyledPrimaryText(context, true);
    }

    public final boolean hasSameContent(NotificationItemV2 notificationItemV2) {
        Slide thumbnailSlide;
        Slide thumbnailSlide2;
        Intrinsics.checkNotNullParameter(notificationItemV2, "other");
        if (getTimestamp() == notificationItemV2.getTimestamp() && this.id == notificationItemV2.id && this.isMms == notificationItemV2.isMms && Intrinsics.areEqual(getIndividualRecipient(), notificationItemV2.getIndividualRecipient()) && getIndividualRecipient().hasSameContent(notificationItemV2.getIndividualRecipient())) {
            SlideDeck slideDeck = this.slideDeck;
            Boolean bool = null;
            Boolean valueOf = (slideDeck == null || (thumbnailSlide2 = slideDeck.getThumbnailSlide()) == null) ? null : Boolean.valueOf(thumbnailSlide2.isInProgress());
            SlideDeck slideDeck2 = notificationItemV2.slideDeck;
            if (!(slideDeck2 == null || (thumbnailSlide = slideDeck2.getThumbnailSlide()) == null)) {
                bool = Boolean.valueOf(thumbnailSlide.isInProgress());
            }
            if (Intrinsics.areEqual(valueOf, bool) && this.record.isRemoteDelete() == notificationItemV2.record.isRemoteDelete()) {
                return true;
            }
        }
        return false;
    }

    private final CharSequence trimToDisplayLength(CharSequence charSequence) {
        if (charSequence == null) {
            charSequence = "";
        }
        return charSequence.length() <= 500 ? charSequence : charSequence.subSequence(0, 500);
    }

    /* compiled from: NotificationItemV2.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u000b\u0010\u000b\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\f\u001a\u0004\u0018\u00010\u0005HÆ\u0003J!\u0010\r\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0005HÖ\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/NotificationItemV2$ThumbnailInfo;", "", "uri", "Landroid/net/Uri;", "contentType", "", "(Landroid/net/Uri;Ljava/lang/String;)V", "getContentType", "()Ljava/lang/String;", "getUri", "()Landroid/net/Uri;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ThumbnailInfo {
        private final String contentType;
        private final Uri uri;

        public ThumbnailInfo() {
            this(null, null, 3, null);
        }

        public static /* synthetic */ ThumbnailInfo copy$default(ThumbnailInfo thumbnailInfo, Uri uri, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                uri = thumbnailInfo.uri;
            }
            if ((i & 2) != 0) {
                str = thumbnailInfo.contentType;
            }
            return thumbnailInfo.copy(uri, str);
        }

        public final Uri component1() {
            return this.uri;
        }

        public final String component2() {
            return this.contentType;
        }

        public final ThumbnailInfo copy(Uri uri, String str) {
            return new ThumbnailInfo(uri, str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ThumbnailInfo)) {
                return false;
            }
            ThumbnailInfo thumbnailInfo = (ThumbnailInfo) obj;
            return Intrinsics.areEqual(this.uri, thumbnailInfo.uri) && Intrinsics.areEqual(this.contentType, thumbnailInfo.contentType);
        }

        public int hashCode() {
            Uri uri = this.uri;
            int i = 0;
            int hashCode = (uri == null ? 0 : uri.hashCode()) * 31;
            String str = this.contentType;
            if (str != null) {
                i = str.hashCode();
            }
            return hashCode + i;
        }

        public String toString() {
            return "ThumbnailInfo(uri=" + this.uri + ", contentType=" + this.contentType + ')';
        }

        public ThumbnailInfo(Uri uri, String str) {
            this.uri = uri;
            this.contentType = str;
        }

        public /* synthetic */ ThumbnailInfo(Uri uri, String str, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : uri, (i & 2) != 0 ? null : str);
        }

        public final String getContentType() {
            return this.contentType;
        }

        public final Uri getUri() {
            return this.uri;
        }
    }
}
