package org.thoughtcrime.securesms.notifications;

import android.service.notification.StatusBarNotification;
import com.annimon.stream.function.Function;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class NotificationCancellationHelper$$ExternalSyntheticLambda1 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return ((StatusBarNotification) obj).getNotification();
    }
}
