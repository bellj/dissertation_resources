package org.thoughtcrime.securesms.notifications.v2;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.notifications.DeleteNotificationReceiver;
import org.thoughtcrime.securesms.notifications.MarkReadReceiver;
import org.thoughtcrime.securesms.notifications.NotificationIds;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: NotificationStateV2.kt */
@Metadata(d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\"\n\u0002\b\u0011\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0003\b\b\u0018\u0000 <2\u00020\u0001:\u0002<=B/\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\u0002\u0010\bJ\u000f\u0010(\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u000f\u0010)\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003HÆ\u0003J\u000f\u0010*\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003HÆ\u0003J9\u0010+\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u00032\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003HÆ\u0001J\u0013\u0010,\u001a\u00020\f2\b\u0010-\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\u0010\u0010.\u001a\u0004\u0018\u00010\u00042\u0006\u0010/\u001a\u000200J\u0010\u00101\u001a\u0004\u0018\u0001022\u0006\u00103\u001a\u000204J\u0010\u00105\u001a\u0004\u0018\u0001022\u0006\u00103\u001a\u000204J\u0016\u00106\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\u00107\u001a\u0004\u0018\u000100J\f\u00108\u001a\b\u0012\u0004\u0012\u0002000\u001eJ\t\u00109\u001a\u00020\u000fHÖ\u0001J\t\u0010:\u001a\u00020;HÖ\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\rR\u001b\u0010\u000e\u001a\u00020\u000f8FX\u0002¢\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0010\u0010\u0011R\u0013\u0010\u0014\u001a\u0004\u0018\u00010\u00158F¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u0017R\u0013\u0010\u0018\u001a\u0004\u0018\u00010\u00198F¢\u0006\u0006\u001a\u0004\b\u001a\u0010\u001bR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\nR!\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u000f0\u001e8FX\u0002¢\u0006\f\n\u0004\b!\u0010\u0013\u001a\u0004\b\u001f\u0010 R!\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00150\u00038FX\u0002¢\u0006\f\n\u0004\b$\u0010\u0013\u001a\u0004\b#\u0010\nR\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\b\n\u0000\u001a\u0004\b%\u0010\nR\u0011\u0010&\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b'\u0010\u0011¨\u0006>"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/NotificationStateV2;", "", "conversations", "", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationConversation;", "muteFilteredMessages", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationStateV2$FilteredMessage;", "profileFilteredMessages", "(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V", "getConversations", "()Ljava/util/List;", "isEmpty", "", "()Z", "messageCount", "", "getMessageCount", "()I", "messageCount$delegate", "Lkotlin/Lazy;", "mostRecentNotification", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationItemV2;", "getMostRecentNotification", "()Lorg/thoughtcrime/securesms/notifications/v2/NotificationItemV2;", "mostRecentSender", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "getMostRecentSender", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "getMuteFilteredMessages", "notificationIds", "", "getNotificationIds", "()Ljava/util/Set;", "notificationIds$delegate", "notificationItems", "getNotificationItems", "notificationItems$delegate", "getProfileFilteredMessages", "threadCount", "getThreadCount", "component1", "component2", "component3", "copy", "equals", "other", "getConversation", "conversationId", "Lorg/thoughtcrime/securesms/notifications/v2/ConversationId;", "getDeleteIntent", "Landroid/app/PendingIntent;", "context", "Landroid/content/Context;", "getMarkAsReadIntent", "getNonVisibleConversation", "visibleThread", "getThreadsWithMostRecentNotificationFromSelf", "hashCode", "toString", "", "Companion", "FilteredMessage", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationStateV2 {
    public static final Companion Companion = new Companion(null);
    private static final NotificationStateV2 EMPTY = new NotificationStateV2(CollectionsKt__CollectionsKt.emptyList(), CollectionsKt__CollectionsKt.emptyList(), CollectionsKt__CollectionsKt.emptyList());
    private final List<NotificationConversation> conversations;
    private final boolean isEmpty;
    private final Lazy messageCount$delegate = LazyKt__LazyJVMKt.lazy(new Function0<Integer>(this) { // from class: org.thoughtcrime.securesms.notifications.v2.NotificationStateV2$messageCount$2
        final /* synthetic */ NotificationStateV2 this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Integer invoke() {
            Integer num = 0;
            for (NotificationConversation notificationConversation : this.this$0.getConversations()) {
                num = Integer.valueOf(num.intValue() + notificationConversation.getMessageCount());
            }
            return num;
        }
    });
    private final List<FilteredMessage> muteFilteredMessages;
    private final Lazy notificationIds$delegate = LazyKt__LazyJVMKt.lazy(new Function0<Set<? extends Integer>>(this) { // from class: org.thoughtcrime.securesms.notifications.v2.NotificationStateV2$notificationIds$2
        final /* synthetic */ NotificationStateV2 this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        /* Return type fixed from 'java.util.Set<java.lang.Integer>' to match base method */
        @Override // kotlin.jvm.functions.Function0
        public final Set<? extends Integer> invoke() {
            List<NotificationConversation> conversations = this.this$0.getConversations();
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(conversations, 10));
            for (NotificationConversation notificationConversation : conversations) {
                arrayList.add(Integer.valueOf(notificationConversation.getNotificationId()));
            }
            return CollectionsKt___CollectionsKt.toSet(arrayList);
        }
    });
    private final Lazy notificationItems$delegate = LazyKt__LazyJVMKt.lazy(new Function0<List<? extends NotificationItemV2>>(this) { // from class: org.thoughtcrime.securesms.notifications.v2.NotificationStateV2$notificationItems$2
        final /* synthetic */ NotificationStateV2 this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        /* Return type fixed from 'java.util.List<org.thoughtcrime.securesms.notifications.v2.NotificationItemV2>' to match base method */
        @Override // kotlin.jvm.functions.Function0
        public final List<? extends NotificationItemV2> invoke() {
            List<NotificationConversation> conversations = this.this$0.getConversations();
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(conversations, 10));
            for (NotificationConversation notificationConversation : conversations) {
                arrayList.add(notificationConversation.getNotificationItems());
            }
            return CollectionsKt___CollectionsKt.sorted(CollectionsKt__IterablesKt.flatten(arrayList));
        }
    });
    private final List<FilteredMessage> profileFilteredMessages;
    private final int threadCount;

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.notifications.v2.NotificationStateV2 */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ NotificationStateV2 copy$default(NotificationStateV2 notificationStateV2, List list, List list2, List list3, int i, Object obj) {
        if ((i & 1) != 0) {
            list = notificationStateV2.conversations;
        }
        if ((i & 2) != 0) {
            list2 = notificationStateV2.muteFilteredMessages;
        }
        if ((i & 4) != 0) {
            list3 = notificationStateV2.profileFilteredMessages;
        }
        return notificationStateV2.copy(list, list2, list3);
    }

    public final List<NotificationConversation> component1() {
        return this.conversations;
    }

    public final List<FilteredMessage> component2() {
        return this.muteFilteredMessages;
    }

    public final List<FilteredMessage> component3() {
        return this.profileFilteredMessages;
    }

    public final NotificationStateV2 copy(List<NotificationConversation> list, List<FilteredMessage> list2, List<FilteredMessage> list3) {
        Intrinsics.checkNotNullParameter(list, "conversations");
        Intrinsics.checkNotNullParameter(list2, "muteFilteredMessages");
        Intrinsics.checkNotNullParameter(list3, "profileFilteredMessages");
        return new NotificationStateV2(list, list2, list3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof NotificationStateV2)) {
            return false;
        }
        NotificationStateV2 notificationStateV2 = (NotificationStateV2) obj;
        return Intrinsics.areEqual(this.conversations, notificationStateV2.conversations) && Intrinsics.areEqual(this.muteFilteredMessages, notificationStateV2.muteFilteredMessages) && Intrinsics.areEqual(this.profileFilteredMessages, notificationStateV2.profileFilteredMessages);
    }

    public int hashCode() {
        return (((this.conversations.hashCode() * 31) + this.muteFilteredMessages.hashCode()) * 31) + this.profileFilteredMessages.hashCode();
    }

    public String toString() {
        return "NotificationStateV2(conversations=" + this.conversations + ", muteFilteredMessages=" + this.muteFilteredMessages + ", profileFilteredMessages=" + this.profileFilteredMessages + ')';
    }

    public NotificationStateV2(List<NotificationConversation> list, List<FilteredMessage> list2, List<FilteredMessage> list3) {
        Intrinsics.checkNotNullParameter(list, "conversations");
        Intrinsics.checkNotNullParameter(list2, "muteFilteredMessages");
        Intrinsics.checkNotNullParameter(list3, "profileFilteredMessages");
        this.conversations = list;
        this.muteFilteredMessages = list2;
        this.profileFilteredMessages = list3;
        this.threadCount = list.size();
        this.isEmpty = list.isEmpty();
    }

    public final List<NotificationConversation> getConversations() {
        return this.conversations;
    }

    public final List<FilteredMessage> getMuteFilteredMessages() {
        return this.muteFilteredMessages;
    }

    public final List<FilteredMessage> getProfileFilteredMessages() {
        return this.profileFilteredMessages;
    }

    public final int getThreadCount() {
        return this.threadCount;
    }

    public final boolean isEmpty() {
        return this.isEmpty;
    }

    public final int getMessageCount() {
        return ((Number) this.messageCount$delegate.getValue()).intValue();
    }

    public final List<NotificationItemV2> getNotificationItems() {
        return (List) this.notificationItems$delegate.getValue();
    }

    public final Set<Integer> getNotificationIds() {
        return (Set) this.notificationIds$delegate.getValue();
    }

    public final NotificationItemV2 getMostRecentNotification() {
        return (NotificationItemV2) CollectionsKt___CollectionsKt.lastOrNull((List) ((List<? extends Object>) getNotificationItems()));
    }

    public final Recipient getMostRecentSender() {
        NotificationItemV2 mostRecentNotification = getMostRecentNotification();
        if (mostRecentNotification != null) {
            return mostRecentNotification.getIndividualRecipient();
        }
        return null;
    }

    public final List<NotificationConversation> getNonVisibleConversation(ConversationId conversationId) {
        List<NotificationConversation> list = this.conversations;
        ArrayList arrayList = new ArrayList();
        for (Object obj : list) {
            if (!Intrinsics.areEqual(((NotificationConversation) obj).getThread(), conversationId)) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    public final NotificationConversation getConversation(ConversationId conversationId) {
        Object obj;
        Intrinsics.checkNotNullParameter(conversationId, "conversationId");
        Iterator<T> it = this.conversations.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (Intrinsics.areEqual(((NotificationConversation) obj).getThread(), conversationId)) {
                break;
            }
        }
        return (NotificationConversation) obj;
    }

    public final PendingIntent getDeleteIntent(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        int messageCount = getMessageCount();
        long[] jArr = new long[messageCount];
        boolean[] zArr = new boolean[messageCount];
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = this.conversations.iterator();
        while (true) {
            int i = 0;
            if (it.hasNext()) {
                NotificationConversation notificationConversation = (NotificationConversation) it.next();
                arrayList.add(notificationConversation.getThread());
                for (Object obj : notificationConversation.getNotificationItems()) {
                    int i2 = i + 1;
                    if (i < 0) {
                        CollectionsKt__CollectionsKt.throwIndexOverflow();
                    }
                    NotificationItemV2 notificationItemV2 = (NotificationItemV2) obj;
                    jArr[i] = notificationItemV2.getId();
                    zArr[i] = notificationItemV2.isMms();
                    i = i2;
                }
            } else {
                Intent putParcelableArrayListExtra = new Intent(context, DeleteNotificationReceiver.class).setAction(DeleteNotificationReceiver.DELETE_NOTIFICATION_ACTION).putExtra(DeleteNotificationReceiver.EXTRA_IDS, jArr).putExtra("is_mms", zArr).putParcelableArrayListExtra("threads", new ArrayList<>(arrayList));
                Intrinsics.checkNotNullExpressionValue(putParcelableArrayListExtra, "Intent(context, DeleteNo…EADS, ArrayList(threads))");
                return PendingIntent.getBroadcast(context, 0, NotificationExtensionsKt.makeUniqueToPreventMerging(putParcelableArrayListExtra), 134217728);
            }
        }
    }

    public final PendingIntent getMarkAsReadIntent(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intent action = new Intent(context, MarkReadReceiver.class).setAction(MarkReadReceiver.CLEAR_ACTION);
        List<NotificationConversation> list = this.conversations;
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (NotificationConversation notificationConversation : list) {
            arrayList.add(notificationConversation.getThread());
        }
        Intent putExtra = action.putParcelableArrayListExtra("threads", new ArrayList<>(arrayList)).putExtra(MarkReadReceiver.NOTIFICATION_ID_EXTRA, NotificationIds.MESSAGE_SUMMARY);
        Intrinsics.checkNotNullExpressionValue(putExtra, "Intent(context, MarkRead…ationIds.MESSAGE_SUMMARY)");
        return PendingIntent.getBroadcast(context, 0, NotificationExtensionsKt.makeUniqueToPreventMerging(putExtra), 134217728);
    }

    public final Set<ConversationId> getThreadsWithMostRecentNotificationFromSelf() {
        List<NotificationConversation> list = this.conversations;
        ArrayList<NotificationConversation> arrayList = new ArrayList();
        for (Object obj : list) {
            if (((NotificationConversation) obj).getMostRecentNotification().getIndividualRecipient().isSelf()) {
                arrayList.add(obj);
            }
        }
        ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10));
        for (NotificationConversation notificationConversation : arrayList) {
            arrayList2.add(notificationConversation.getThread());
        }
        return CollectionsKt___CollectionsKt.toSet(arrayList2);
    }

    /* compiled from: NotificationStateV2.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000b\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\r\u001a\u00020\u00052\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\t¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/NotificationStateV2$FilteredMessage;", "", ContactRepository.ID_COLUMN, "", "isMms", "", "(JZ)V", "getId", "()J", "()Z", "component1", "component2", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class FilteredMessage {
        private final long id;
        private final boolean isMms;

        public static /* synthetic */ FilteredMessage copy$default(FilteredMessage filteredMessage, long j, boolean z, int i, Object obj) {
            if ((i & 1) != 0) {
                j = filteredMessage.id;
            }
            if ((i & 2) != 0) {
                z = filteredMessage.isMms;
            }
            return filteredMessage.copy(j, z);
        }

        public final long component1() {
            return this.id;
        }

        public final boolean component2() {
            return this.isMms;
        }

        public final FilteredMessage copy(long j, boolean z) {
            return new FilteredMessage(j, z);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof FilteredMessage)) {
                return false;
            }
            FilteredMessage filteredMessage = (FilteredMessage) obj;
            return this.id == filteredMessage.id && this.isMms == filteredMessage.isMms;
        }

        public int hashCode() {
            int m = SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.id) * 31;
            boolean z = this.isMms;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return m + i;
        }

        public String toString() {
            return "FilteredMessage(id=" + this.id + ", isMms=" + this.isMms + ')';
        }

        public FilteredMessage(long j, boolean z) {
            this.id = j;
            this.isMms = z;
        }

        public final long getId() {
            return this.id;
        }

        public final boolean isMms() {
            return this.isMms;
        }
    }

    /* compiled from: NotificationStateV2.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/NotificationStateV2$Companion;", "", "()V", "EMPTY", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationStateV2;", "getEMPTY", "()Lorg/thoughtcrime/securesms/notifications/v2/NotificationStateV2;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final NotificationStateV2 getEMPTY() {
            return NotificationStateV2.EMPTY;
        }
    }
}
