package org.thoughtcrime.securesms.notifications.v2;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.service.notification.StatusBarNotification;
import java.util.ArrayList;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.notifications.NotificationCancellationHelper;
import org.thoughtcrime.securesms.webrtc.CallNotificationBuilder;

/* compiled from: MessageNotifierV2.kt */
@Metadata(d1 = {"\u00004\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\u001a*\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0002\u001a \u0010\n\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\t0\b0\u000b*\u00020\u0002H\u0002ø\u0001\u0000¢\u0006\u0002\u0010\f\u001a\f\u0010\r\u001a\u00020\u000e*\u00020\u000fH\u0002\u0002\u0004\n\u0002\b\u0019¨\u0006\u0010"}, d2 = {"cancelOrphanedNotifications", "", "Landroid/app/NotificationManager;", "context", "Landroid/content/Context;", "state", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationStateV2;", "stickyNotifications", "", "", "getDisplayedNotificationIds", "Lkotlin/Result;", "(Landroid/app/NotificationManager;)Ljava/lang/Object;", "isMessageNotification", "", "Landroid/service/notification/StatusBarNotification;", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MessageNotifierV2Kt {
    private static final boolean isMessageNotification(StatusBarNotification statusBarNotification) {
        return (statusBarNotification.getId() == 1338 || statusBarNotification.getId() == 4141 || statusBarNotification.getId() == 313399 || statusBarNotification.getId() == 1111 || CallNotificationBuilder.isWebRtcNotification(statusBarNotification.getId())) ? false : true;
    }

    public static final Object getDisplayedNotificationIds(NotificationManager notificationManager) {
        if (Build.VERSION.SDK_INT < 24) {
            Result.Companion companion = Result.Companion;
            return Result.m153constructorimpl(ResultKt.createFailure(new UnsupportedOperationException("SDK level too low")));
        }
        try {
            Result.Companion companion2 = Result.Companion;
            StatusBarNotification[] activeNotifications = notificationManager.getActiveNotifications();
            Intrinsics.checkNotNullExpressionValue(activeNotifications, "activeNotifications");
            ArrayList<StatusBarNotification> arrayList = new ArrayList();
            for (StatusBarNotification statusBarNotification : activeNotifications) {
                Intrinsics.checkNotNullExpressionValue(statusBarNotification, "it");
                if (isMessageNotification(statusBarNotification)) {
                    arrayList.add(statusBarNotification);
                }
            }
            ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10));
            for (StatusBarNotification statusBarNotification2 : arrayList) {
                arrayList2.add(Integer.valueOf(statusBarNotification2.getId()));
            }
            return Result.m153constructorimpl(CollectionsKt___CollectionsKt.toSet(arrayList2));
        } catch (Throwable th) {
            Log.w(MessageNotifierV2.Companion.getTAG(), th);
            Result.Companion companion3 = Result.Companion;
            return Result.m153constructorimpl(ResultKt.createFailure(th));
        }
    }

    public static final void cancelOrphanedNotifications(NotificationManager notificationManager, Context context, NotificationStateV2 notificationStateV2, Set<Integer> set) {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                StatusBarNotification[] activeNotifications = notificationManager.getActiveNotifications();
                Intrinsics.checkNotNullExpressionValue(activeNotifications, "activeNotifications");
                ArrayList<StatusBarNotification> arrayList = new ArrayList();
                for (StatusBarNotification statusBarNotification : activeNotifications) {
                    Intrinsics.checkNotNullExpressionValue(statusBarNotification, "it");
                    if (isMessageNotification(statusBarNotification) && !set.contains(Integer.valueOf(statusBarNotification.getId()))) {
                        arrayList.add(statusBarNotification);
                    }
                }
                ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10));
                for (StatusBarNotification statusBarNotification2 : arrayList) {
                    arrayList2.add(Integer.valueOf(statusBarNotification2.getId()));
                }
                ArrayList<Number> arrayList3 = new ArrayList();
                for (Object obj : arrayList2) {
                    if (!notificationStateV2.getNotificationIds().contains(Integer.valueOf(((Number) obj).intValue()))) {
                        arrayList3.add(obj);
                    }
                }
                for (Number number : arrayList3) {
                    int intValue = number.intValue();
                    Log.d(MessageNotifierV2.Companion.getTAG(), "Cancelling orphaned notification: " + intValue);
                    NotificationCancellationHelper.cancel(context, intValue);
                }
                NotificationCancellationHelper.cancelMessageSummaryIfSoleNotification(context);
            } catch (Throwable th) {
                Log.w(MessageNotifierV2.Companion.getTAG(), th);
            }
        }
    }
}
