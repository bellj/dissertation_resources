package org.thoughtcrime.securesms.notifications.v2;

import android.graphics.Bitmap;
import androidx.core.graphics.drawable.IconCompat;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.notifications.ReplyMethod;

/* compiled from: NotificationBuilder.kt */
@Metadata(d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003*\u0004\u0018\u00010\u0004H\u0002\u001a\f\u0010\u0005\u001a\u00020\u0001*\u00020\u0006H\u0003\"\u000e\u0010\u0000\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"BIG_PICTURE_DIMEN", "", "toIconCompat", "Landroidx/core/graphics/drawable/IconCompat;", "Landroid/graphics/Bitmap;", "toLongDescription", "Lorg/thoughtcrime/securesms/notifications/ReplyMethod;", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationBuilderKt {
    private static final int BIG_PICTURE_DIMEN;

    /* compiled from: NotificationBuilder.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[ReplyMethod.values().length];
            iArr[ReplyMethod.GroupMessage.ordinal()] = 1;
            iArr[ReplyMethod.SecureMessage.ordinal()] = 2;
            iArr[ReplyMethod.UnsecuredSmsMessage.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public static final IconCompat toIconCompat(Bitmap bitmap) {
        if (bitmap != null) {
            return IconCompat.createWithBitmap(bitmap);
        }
        return null;
    }

    public static final int toLongDescription(ReplyMethod replyMethod) {
        int i = WhenMappings.$EnumSwitchMapping$0[replyMethod.ordinal()];
        if (i == 1) {
            return R.string.MessageNotifier_reply;
        }
        if (i == 2) {
            return R.string.MessageNotifier_signal_message;
        }
        if (i == 3) {
            return R.string.MessageNotifier_unsecured_sms;
        }
        throw new NoWhenBranchMatchedException();
    }
}
