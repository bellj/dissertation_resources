package org.thoughtcrime.securesms.notifications.v2;

import android.content.Context;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.notifications.v2.CancelableExecutor;
import org.whispersystems.signalservice.internal.util.Util;

/* compiled from: MessageNotifierV2.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0011B\u0005¢\u0006\u0002\u0010\u0002J\u0006\u0010\b\u001a\u00020\tJ\u0016\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eJ\u0010\u0010\u000f\u001a\u00020\t2\u0006\u0010\u0010\u001a\u00020\u0007H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/CancelableExecutor;", "", "()V", "executor", "Ljava/util/concurrent/Executor;", "tasks", "", "Lorg/thoughtcrime/securesms/notifications/v2/CancelableExecutor$DelayedNotification;", "cancel", "", "enqueue", "context", "Landroid/content/Context;", "conversationId", "Lorg/thoughtcrime/securesms/notifications/v2/ConversationId;", "execute", "runnable", "DelayedNotification", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CancelableExecutor {
    private final Executor executor;
    private final Set<DelayedNotification> tasks = new LinkedHashSet();

    public CancelableExecutor() {
        ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor();
        Intrinsics.checkNotNullExpressionValue(newSingleThreadExecutor, "newSingleThreadExecutor()");
        this.executor = newSingleThreadExecutor;
    }

    public final void enqueue(Context context, ConversationId conversationId) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(conversationId, "conversationId");
        execute(new DelayedNotification(context, conversationId));
    }

    private final void execute(DelayedNotification delayedNotification) {
        synchronized (this.tasks) {
            this.tasks.add(delayedNotification);
        }
        this.executor.execute(new Runnable(this) { // from class: org.thoughtcrime.securesms.notifications.v2.CancelableExecutor$$ExternalSyntheticLambda0
            public final /* synthetic */ CancelableExecutor f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                CancelableExecutor.$r8$lambda$0W846oC7_z74V9JMHkhlabZxMI0(CancelableExecutor.DelayedNotification.this, this.f$1);
            }
        });
    }

    /* renamed from: execute$lambda-2 */
    public static final void m2354execute$lambda2(DelayedNotification delayedNotification, CancelableExecutor cancelableExecutor) {
        Intrinsics.checkNotNullParameter(delayedNotification, "$runnable");
        Intrinsics.checkNotNullParameter(cancelableExecutor, "this$0");
        delayedNotification.run();
        synchronized (cancelableExecutor.tasks) {
            cancelableExecutor.tasks.remove(delayedNotification);
            Unit unit = Unit.INSTANCE;
        }
    }

    public final void cancel() {
        synchronized (this.tasks) {
            for (DelayedNotification delayedNotification : this.tasks) {
                delayedNotification.cancel();
            }
            Unit unit = Unit.INSTANCE;
        }
    }

    /* compiled from: MessageNotifierV2.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0002\u0018\u0000 \u000e2\u00020\u0001:\u0001\u000eB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u000b\u001a\u00020\fJ\b\u0010\r\u001a\u00020\fH\u0016R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/CancelableExecutor$DelayedNotification;", "Ljava/lang/Runnable;", "context", "Landroid/content/Context;", ThreadDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/notifications/v2/ConversationId;", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/notifications/v2/ConversationId;)V", "canceled", "Ljava/util/concurrent/atomic/AtomicBoolean;", "delayUntil", "", "cancel", "", "run", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class DelayedNotification implements Runnable {
        public static final Companion Companion = new Companion(null);
        private static final long DELAY = TimeUnit.SECONDS.toMillis(5);
        private static final String TAG = Log.tag(DelayedNotification.class);
        private final AtomicBoolean canceled = new AtomicBoolean(false);
        private final Context context;
        private final long delayUntil = (System.currentTimeMillis() + DELAY);
        private final ConversationId thread;

        public DelayedNotification(Context context, ConversationId conversationId) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(conversationId, ThreadDatabase.TABLE_NAME);
            this.context = context;
            this.thread = conversationId;
        }

        @Override // java.lang.Runnable
        public void run() {
            long currentTimeMillis = this.delayUntil - System.currentTimeMillis();
            String str = TAG;
            Log.i(str, "Waiting to notify: " + currentTimeMillis);
            if (currentTimeMillis > 0) {
                Util.sleep(currentTimeMillis);
            }
            if (!this.canceled.get()) {
                Log.i(str, "Not canceled, notifying...");
                ApplicationDependencies.getMessageNotifier().updateNotification(this.context, this.thread, true);
                ApplicationDependencies.getMessageNotifier().cancelDelayedNotifications();
                return;
            }
            Log.w(str, "Canceled, not notifying...");
        }

        public final void cancel() {
            this.canceled.set(true);
        }

        /* compiled from: MessageNotifierV2.kt */
        @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/CancelableExecutor$DelayedNotification$Companion;", "", "()V", "DELAY", "", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }
        }
    }
}
