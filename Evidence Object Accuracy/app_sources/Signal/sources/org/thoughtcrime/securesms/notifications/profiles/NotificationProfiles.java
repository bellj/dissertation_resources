package org.thoughtcrime.securesms.notifications.profiles;

import android.content.Context;
import j$.time.LocalDateTime;
import j$.time.ZoneId;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.keyvalue.NotificationProfileValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.JavaTimeExtensionsKt;

/* compiled from: NotificationProfiles.kt */
@Metadata(bv = {}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0011\u0010\u0012J,\u0010\t\u001a\u0004\u0018\u00010\u00032\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\u0007H\u0007J\f\u0010\u000b\u001a\u00020\n*\u00020\u0005H\u0002J \u0010\u0010\u001a\u00020\u000f2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u0005¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfiles;", "", "", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", "profiles", "", "now", "j$/time/ZoneId", "zoneId", "getActiveProfile", "", "isForever", "Landroid/content/Context;", "context", "profile", "", "getActiveProfileDescription", "<init>", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class NotificationProfiles {
    public static final NotificationProfiles INSTANCE = new NotificationProfiles();

    @JvmStatic
    public static final NotificationProfile getActiveProfile(List<NotificationProfile> list) {
        Intrinsics.checkNotNullParameter(list, "profiles");
        return getActiveProfile$default(list, 0, null, 6, null);
    }

    @JvmStatic
    public static final NotificationProfile getActiveProfile(List<NotificationProfile> list, long j) {
        Intrinsics.checkNotNullParameter(list, "profiles");
        return getActiveProfile$default(list, j, null, 4, null);
    }

    private final boolean isForever(long j) {
        return j == Long.MAX_VALUE;
    }

    private NotificationProfiles() {
    }

    public static /* synthetic */ NotificationProfile getActiveProfile$default(List list, long j, ZoneId zoneId, int i, Object obj) {
        if ((i & 2) != 0) {
            j = System.currentTimeMillis();
        }
        if ((i & 4) != 0) {
            zoneId = ZoneId.systemDefault();
            Intrinsics.checkNotNullExpressionValue(zoneId, "systemDefault()");
        }
        return getActiveProfile(list, j, zoneId);
    }

    @JvmStatic
    public static final NotificationProfile getActiveProfile(List<NotificationProfile> list, long j, ZoneId zoneId) {
        NotificationProfile notificationProfile;
        boolean z;
        Object obj;
        boolean z2;
        Intrinsics.checkNotNullParameter(list, "profiles");
        Intrinsics.checkNotNullParameter(zoneId, "zoneId");
        NotificationProfileValues notificationProfileValues = SignalStore.notificationProfileValues();
        Intrinsics.checkNotNullExpressionValue(notificationProfileValues, "notificationProfileValues()");
        LocalDateTime localDateTime = JavaTimeExtensionsKt.toLocalDateTime(j, zoneId);
        Object obj2 = null;
        if (j < notificationProfileValues.getManuallyEnabledUntil()) {
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (((NotificationProfile) obj).getId() == notificationProfileValues.getManuallyEnabledProfile()) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
            notificationProfile = (NotificationProfile) obj;
        } else {
            notificationProfile = null;
        }
        List list2 = CollectionsKt___CollectionsKt.sortedDescending(list);
        ArrayList arrayList = new ArrayList();
        for (Object obj3 : list2) {
            if (((NotificationProfile) obj3).getSchedule().isCurrentlyActive(j, zoneId)) {
                arrayList.add(obj3);
            }
        }
        Iterator it2 = arrayList.iterator();
        while (true) {
            if (!it2.hasNext()) {
                break;
            }
            Object next = it2.next();
            if (JavaTimeExtensionsKt.toMillis(((NotificationProfile) next).getSchedule().startDateTime(localDateTime), JavaTimeExtensionsKt.toOffset(zoneId)) > notificationProfileValues.getManuallyDisabledAt()) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                obj2 = next;
                break;
            }
        }
        NotificationProfile notificationProfile2 = (NotificationProfile) obj2;
        return (notificationProfile == null || notificationProfile2 == null) ? notificationProfile == null ? notificationProfile2 : notificationProfile : Intrinsics.areEqual(notificationProfile, notificationProfile2) ? notificationProfile : notificationProfile2;
    }

    public static /* synthetic */ String getActiveProfileDescription$default(NotificationProfiles notificationProfiles, Context context, NotificationProfile notificationProfile, long j, int i, Object obj) {
        if ((i & 4) != 0) {
            j = System.currentTimeMillis();
        }
        return notificationProfiles.getActiveProfileDescription(context, notificationProfile, j);
    }

    public final String getActiveProfileDescription(Context context, NotificationProfile notificationProfile, long j) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(notificationProfile, "profile");
        NotificationProfileValues notificationProfileValues = SignalStore.notificationProfileValues();
        Intrinsics.checkNotNullExpressionValue(notificationProfileValues, "notificationProfileValues()");
        if (notificationProfile.getId() == notificationProfileValues.getManuallyEnabledProfile()) {
            if (isForever(notificationProfileValues.getManuallyEnabledUntil())) {
                String string = context.getString(R.string.NotificationProfilesFragment__on);
                Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.stri…tionProfilesFragment__on)");
                return string;
            } else if (j < notificationProfileValues.getManuallyEnabledUntil()) {
                String string2 = context.getString(R.string.NotificationProfileSelection__on_until_s, JavaTimeExtensionsKt.formatHours(JavaTimeExtensionsKt.toLocalTime$default(notificationProfileValues.getManuallyEnabledUntil(), null, 1, null), context));
                Intrinsics.checkNotNullExpressionValue(string2, "context.getString(R.stri…e().formatHours(context))");
                return string2;
            }
        }
        String string3 = context.getString(R.string.NotificationProfileSelection__on_until_s, JavaTimeExtensionsKt.formatHours(notificationProfile.getSchedule().endTime(), context));
        Intrinsics.checkNotNullExpressionValue(string3, "context.getString(R.stri…e().formatHours(context))");
        return string3;
    }
}
