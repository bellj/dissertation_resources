package org.thoughtcrime.securesms.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import j$.util.Optional;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.BubbleUtil;

/* loaded from: classes.dex */
public interface MessageNotifier {
    void addStickyThread(ConversationId conversationId, long j);

    void cancelDelayedNotifications();

    void clearReminder(Context context);

    void clearVisibleThread();

    Optional<ConversationId> getVisibleThread();

    void notifyMessageDeliveryFailed(Context context, Recipient recipient, ConversationId conversationId);

    void notifyProofRequired(Context context, Recipient recipient, ConversationId conversationId);

    void removeStickyThread(ConversationId conversationId);

    void setLastDesktopActivityTimestamp(long j);

    void setVisibleThread(ConversationId conversationId);

    void updateNotification(Context context);

    void updateNotification(Context context, ConversationId conversationId);

    void updateNotification(Context context, ConversationId conversationId, BubbleUtil.BubbleState bubbleState);

    void updateNotification(Context context, ConversationId conversationId, boolean z);

    void updateNotification(Context context, ConversationId conversationId, boolean z, int i, BubbleUtil.BubbleState bubbleState);

    /* loaded from: classes4.dex */
    public static class ReminderReceiver extends BroadcastReceiver {
        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            SignalExecutors.BOUNDED.execute(new MessageNotifier$ReminderReceiver$$ExternalSyntheticLambda0(intent, context));
        }

        public static /* synthetic */ void lambda$onReceive$0(Intent intent, Context context) {
            ApplicationDependencies.getMessageNotifier().updateNotification(context, null, true, intent.getIntExtra("reminder_count", 0) + 1, BubbleUtil.BubbleState.HIDDEN);
        }
    }
}
