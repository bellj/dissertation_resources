package org.thoughtcrime.securesms.notifications.profiles;

import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: NotificationProfile.kt */
@Metadata(d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u000f\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0010\u0000\n\u0002\b\u0004\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B[\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\b\b\u0002\u0010\n\u001a\u00020\u000b\u0012\b\b\u0002\u0010\f\u001a\u00020\u000b\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u000e\b\u0002\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010¢\u0006\u0002\u0010\u0012J\u0011\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u0000H\u0002J\t\u0010%\u001a\u00020\u0003HÆ\u0003J\t\u0010&\u001a\u00020\u0005HÆ\u0003J\t\u0010'\u001a\u00020\u0005HÆ\u0003J\t\u0010(\u001a\u00020\bHÆ\u0003J\t\u0010)\u001a\u00020\u0003HÆ\u0003J\t\u0010*\u001a\u00020\u000bHÆ\u0003J\t\u0010+\u001a\u00020\u000bHÆ\u0003J\t\u0010,\u001a\u00020\u000eHÆ\u0003J\u000f\u0010-\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010HÆ\u0003Ji\u0010.\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\u00032\b\b\u0002\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\u000b2\b\b\u0002\u0010\r\u001a\u00020\u000e2\u000e\b\u0002\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010HÆ\u0001J\u0013\u0010/\u001a\u00020\u000b2\b\u0010$\u001a\u0004\u0018\u000100HÖ\u0003J\t\u00101\u001a\u00020#HÖ\u0001J\u000e\u00102\u001a\u00020\u000b2\u0006\u0010\u0002\u001a\u00020\u0011J\t\u00103\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\f\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0014R\u0017\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\t\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u001dR\u0011\u0010\r\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b \u0010!¨\u00064"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", "", ContactRepository.ID_COLUMN, "", "name", "", "emoji", NotificationProfileDatabase.NotificationProfileTable.COLOR, "Lorg/thoughtcrime/securesms/conversation/colors/AvatarColor;", "createdAt", "allowAllCalls", "", "allowAllMentions", "schedule", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfileSchedule;", "allowedMembers", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(JLjava/lang/String;Ljava/lang/String;Lorg/thoughtcrime/securesms/conversation/colors/AvatarColor;JZZLorg/thoughtcrime/securesms/notifications/profiles/NotificationProfileSchedule;Ljava/util/Set;)V", "getAllowAllCalls", "()Z", "getAllowAllMentions", "getAllowedMembers", "()Ljava/util/Set;", "getColor", "()Lorg/thoughtcrime/securesms/conversation/colors/AvatarColor;", "getCreatedAt", "()J", "getEmoji", "()Ljava/lang/String;", "getId", "getName", "getSchedule", "()Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfileSchedule;", "compareTo", "", "other", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "hashCode", "isRecipientAllowed", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationProfile implements Comparable<NotificationProfile> {
    private final boolean allowAllCalls;
    private final boolean allowAllMentions;
    private final Set<RecipientId> allowedMembers;
    private final AvatarColor color;
    private final long createdAt;
    private final String emoji;
    private final long id;
    private final String name;
    private final NotificationProfileSchedule schedule;

    public final long component1() {
        return this.id;
    }

    public final String component2() {
        return this.name;
    }

    public final String component3() {
        return this.emoji;
    }

    public final AvatarColor component4() {
        return this.color;
    }

    public final long component5() {
        return this.createdAt;
    }

    public final boolean component6() {
        return this.allowAllCalls;
    }

    public final boolean component7() {
        return this.allowAllMentions;
    }

    public final NotificationProfileSchedule component8() {
        return this.schedule;
    }

    public final Set<RecipientId> component9() {
        return this.allowedMembers;
    }

    public final NotificationProfile copy(long j, String str, String str2, AvatarColor avatarColor, long j2, boolean z, boolean z2, NotificationProfileSchedule notificationProfileSchedule, Set<? extends RecipientId> set) {
        Intrinsics.checkNotNullParameter(str, "name");
        Intrinsics.checkNotNullParameter(str2, "emoji");
        Intrinsics.checkNotNullParameter(avatarColor, NotificationProfileDatabase.NotificationProfileTable.COLOR);
        Intrinsics.checkNotNullParameter(notificationProfileSchedule, "schedule");
        Intrinsics.checkNotNullParameter(set, "allowedMembers");
        return new NotificationProfile(j, str, str2, avatarColor, j2, z, z2, notificationProfileSchedule, set);
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof NotificationProfile)) {
            return false;
        }
        NotificationProfile notificationProfile = (NotificationProfile) obj;
        return this.id == notificationProfile.id && Intrinsics.areEqual(this.name, notificationProfile.name) && Intrinsics.areEqual(this.emoji, notificationProfile.emoji) && this.color == notificationProfile.color && this.createdAt == notificationProfile.createdAt && this.allowAllCalls == notificationProfile.allowAllCalls && this.allowAllMentions == notificationProfile.allowAllMentions && Intrinsics.areEqual(this.schedule, notificationProfile.schedule) && Intrinsics.areEqual(this.allowedMembers, notificationProfile.allowedMembers);
    }

    @Override // java.lang.Object
    public int hashCode() {
        int m = ((((((((SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.id) * 31) + this.name.hashCode()) * 31) + this.emoji.hashCode()) * 31) + this.color.hashCode()) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.createdAt)) * 31;
        boolean z = this.allowAllCalls;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = (m + i2) * 31;
        boolean z2 = this.allowAllMentions;
        if (!z2) {
            i = z2 ? 1 : 0;
        }
        return ((((i5 + i) * 31) + this.schedule.hashCode()) * 31) + this.allowedMembers.hashCode();
    }

    @Override // java.lang.Object
    public String toString() {
        return "NotificationProfile(id=" + this.id + ", name=" + this.name + ", emoji=" + this.emoji + ", color=" + this.color + ", createdAt=" + this.createdAt + ", allowAllCalls=" + this.allowAllCalls + ", allowAllMentions=" + this.allowAllMentions + ", schedule=" + this.schedule + ", allowedMembers=" + this.allowedMembers + ')';
    }

    /* JADX DEBUG: Multi-variable search result rejected for r12v0, resolved type: java.util.Set<? extends org.thoughtcrime.securesms.recipients.RecipientId> */
    /* JADX WARN: Multi-variable type inference failed */
    public NotificationProfile(long j, String str, String str2, AvatarColor avatarColor, long j2, boolean z, boolean z2, NotificationProfileSchedule notificationProfileSchedule, Set<? extends RecipientId> set) {
        Intrinsics.checkNotNullParameter(str, "name");
        Intrinsics.checkNotNullParameter(str2, "emoji");
        Intrinsics.checkNotNullParameter(avatarColor, NotificationProfileDatabase.NotificationProfileTable.COLOR);
        Intrinsics.checkNotNullParameter(notificationProfileSchedule, "schedule");
        Intrinsics.checkNotNullParameter(set, "allowedMembers");
        this.id = j;
        this.name = str;
        this.emoji = str2;
        this.color = avatarColor;
        this.createdAt = j2;
        this.allowAllCalls = z;
        this.allowAllMentions = z2;
        this.schedule = notificationProfileSchedule;
        this.allowedMembers = set;
    }

    public final long getId() {
        return this.id;
    }

    public final String getName() {
        return this.name;
    }

    public final String getEmoji() {
        return this.emoji;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ NotificationProfile(long r15, java.lang.String r17, java.lang.String r18, org.thoughtcrime.securesms.conversation.colors.AvatarColor r19, long r20, boolean r22, boolean r23, org.thoughtcrime.securesms.notifications.profiles.NotificationProfileSchedule r24, java.util.Set r25, int r26, kotlin.jvm.internal.DefaultConstructorMarker r27) {
        /*
            r14 = this;
            r0 = r26
            r1 = r0 & 8
            if (r1 == 0) goto L_0x000a
            org.thoughtcrime.securesms.conversation.colors.AvatarColor r1 = org.thoughtcrime.securesms.conversation.colors.AvatarColor.A210
            r7 = r1
            goto L_0x000c
        L_0x000a:
            r7 = r19
        L_0x000c:
            r1 = r0 & 32
            r2 = 0
            if (r1 == 0) goto L_0x0013
            r10 = 0
            goto L_0x0015
        L_0x0013:
            r10 = r22
        L_0x0015:
            r1 = r0 & 64
            if (r1 == 0) goto L_0x001b
            r11 = 0
            goto L_0x001d
        L_0x001b:
            r11 = r23
        L_0x001d:
            r0 = r0 & 256(0x100, float:3.59E-43)
            if (r0 == 0) goto L_0x0027
            java.util.Set r0 = kotlin.collections.SetsKt.emptySet()
            r13 = r0
            goto L_0x0029
        L_0x0027:
            r13 = r25
        L_0x0029:
            r2 = r14
            r3 = r15
            r5 = r17
            r6 = r18
            r8 = r20
            r12 = r24
            r2.<init>(r3, r5, r6, r7, r8, r10, r11, r12, r13)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.notifications.profiles.NotificationProfile.<init>(long, java.lang.String, java.lang.String, org.thoughtcrime.securesms.conversation.colors.AvatarColor, long, boolean, boolean, org.thoughtcrime.securesms.notifications.profiles.NotificationProfileSchedule, java.util.Set, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final AvatarColor getColor() {
        return this.color;
    }

    public final long getCreatedAt() {
        return this.createdAt;
    }

    public final boolean getAllowAllCalls() {
        return this.allowAllCalls;
    }

    public final boolean getAllowAllMentions() {
        return this.allowAllMentions;
    }

    public final NotificationProfileSchedule getSchedule() {
        return this.schedule;
    }

    public final Set<RecipientId> getAllowedMembers() {
        return this.allowedMembers;
    }

    public final boolean isRecipientAllowed(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        return this.allowedMembers.contains(recipientId);
    }

    public int compareTo(NotificationProfile notificationProfile) {
        Intrinsics.checkNotNullParameter(notificationProfile, "other");
        return Intrinsics.compare(this.createdAt, notificationProfile.createdAt);
    }
}
