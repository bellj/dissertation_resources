package org.thoughtcrime.securesms.notifications;

import android.app.Notification;
import android.content.Context;
import android.os.Build;
import android.service.notification.StatusBarNotification;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import j$.util.Optional;
import j$.util.function.Function;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.messages.MessageContentProcessor$$ExternalSyntheticLambda22;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.notifications.v2.MessageNotifierV2;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.BubbleUtil;
import org.thoughtcrime.securesms.util.ConversationUtil;
import org.thoughtcrime.securesms.util.ServiceUtil;

/* loaded from: classes4.dex */
public final class NotificationCancellationHelper {
    private static final String TAG = Log.tag(NotificationCancellationHelper.class);

    private NotificationCancellationHelper() {
    }

    public static void cancelAllMessageNotifications(Context context) {
        cancelAllMessageNotifications(context, Collections.emptySet());
    }

    public static void cancelAllMessageNotifications(Context context, Set<Integer> set) {
        if (Build.VERSION.SDK_INT >= 23) {
            try {
                StatusBarNotification[] activeNotifications = ServiceUtil.getNotificationManager(context).getActiveNotifications();
                int i = 0;
                for (StatusBarNotification statusBarNotification : activeNotifications) {
                    if (isSingleThreadNotification(statusBarNotification)) {
                        i++;
                        if (!set.contains(Integer.valueOf(statusBarNotification.getId())) && cancel(context, statusBarNotification.getId())) {
                            i--;
                        }
                    }
                }
                if (i == 0) {
                    cancelLegacy(context, NotificationIds.MESSAGE_SUMMARY);
                }
            } catch (Throwable th) {
                Log.w(TAG, "Canceling all notifications.", th);
                ServiceUtil.getNotificationManager(context).cancelAll();
            }
        } else {
            cancelLegacy(context, NotificationIds.MESSAGE_SUMMARY);
        }
    }

    public static void cancelMessageSummaryIfSoleNotification(Context context) {
        if (Build.VERSION.SDK_INT > 23) {
            try {
                StatusBarNotification[] activeNotifications = ServiceUtil.getNotificationManager(context).getActiveNotifications();
                int length = activeNotifications.length;
                boolean z = false;
                int i = 0;
                boolean z2 = false;
                while (true) {
                    if (i >= length) {
                        z = z2;
                        break;
                    }
                    StatusBarNotification statusBarNotification = activeNotifications[i];
                    if (isSingleThreadNotification(statusBarNotification)) {
                        break;
                    }
                    if (statusBarNotification.getId() == 1338) {
                        z2 = true;
                    }
                    i++;
                }
                if (z) {
                    Log.d(TAG, "Cancelling sole message summary");
                    cancelLegacy(context, NotificationIds.MESSAGE_SUMMARY);
                }
            } catch (Throwable th) {
                Log.w(TAG, th);
            }
        }
    }

    private static boolean isSingleThreadNotification(StatusBarNotification statusBarNotification) {
        return statusBarNotification.getId() != 1338 && Objects.equals(statusBarNotification.getNotification().getGroup(), MessageNotifierV2.NOTIFICATION_GROUP);
    }

    public static boolean cancel(Context context, int i) {
        String str = TAG;
        Log.d(str, "cancel() called with: notificationId = [" + i + "]");
        if (Build.VERSION.SDK_INT >= 30) {
            return cancelWithConversationSupport(context, i);
        }
        cancelLegacy(context, i);
        return true;
    }

    public static void cancelLegacy(Context context, int i) {
        String str = TAG;
        Log.d(str, "cancelLegacy() called with: notificationId = [" + i + "]");
        ServiceUtil.getNotificationManager(context).cancel(i);
    }

    private static boolean cancelWithConversationSupport(Context context, int i) {
        String str = TAG;
        Log.d(str, "cancelWithConversationSupport() called with: notificationId = [" + i + "]");
        if (!isCancellable(context, i)) {
            return false;
        }
        cancelLegacy(context, i);
        return true;
    }

    private static boolean isCancellable(Context context, int i) {
        Notification notification = (Notification) Stream.of(ServiceUtil.getNotificationManager(context).getActiveNotifications()).filter(new Predicate(i) { // from class: org.thoughtcrime.securesms.notifications.NotificationCancellationHelper$$ExternalSyntheticLambda0
            public final /* synthetic */ int f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return NotificationCancellationHelper.lambda$isCancellable$0(this.f$0, (StatusBarNotification) obj);
            }
        }).findFirst().map(new NotificationCancellationHelper$$ExternalSyntheticLambda1()).orElse(null);
        if (notification == null || notification.getShortcutId() == null || notification.getBubbleMetadata() == null) {
            Log.d(TAG, "isCancellable: bubbles not available or notification does not exist");
            return true;
        }
        RecipientId recipientId = ConversationUtil.getRecipientId(notification.getShortcutId());
        if (recipientId == null) {
            Log.d(TAG, "isCancellable: Unable to get recipient from shortcut id");
            return true;
        }
        Long threadIdFor = SignalDatabase.threads().getThreadIdFor(recipientId);
        Optional<ConversationId> visibleThread = ApplicationDependencies.getMessageNotifier().getVisibleThread();
        Long l = (Long) visibleThread.map(new Function() { // from class: org.thoughtcrime.securesms.notifications.NotificationCancellationHelper$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((ConversationId) obj).getGroupStoryId();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(null);
        if (!Objects.equals(threadIdFor, (Long) visibleThread.map(new MessageContentProcessor$$ExternalSyntheticLambda22()).orElse(null)) || l != null) {
            return !BubbleUtil.canBubble(context, recipientId, threadIdFor);
        }
        Log.d(TAG, "isCancellable: user entered full screen thread.");
        return true;
    }

    public static /* synthetic */ boolean lambda$isCancellable$0(int i, StatusBarNotification statusBarNotification) {
        return statusBarNotification.getId() == i;
    }
}
