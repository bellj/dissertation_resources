package org.thoughtcrime.securesms.notifications.v2;

import android.database.Cursor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__MutableCollectionsJVMKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.io.CloseableKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.IntRange;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.MmsSmsColumns;
import org.thoughtcrime.securesms.database.MmsSmsDatabase;
import org.thoughtcrime.securesms.database.ReactionDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.ReactionRecord;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.notifications.v2.MessageNotifierV2;
import org.thoughtcrime.securesms.notifications.v2.NotificationStateV2;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.MessageRecordUtil;

/* compiled from: NotificationStateProvider.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u000e\u000fB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J&\u0010\u0006\u001a\u00020\u00072\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0007R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/NotificationStateProvider;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "constructNotificationState", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationStateV2;", "stickyThreads", "", "Lorg/thoughtcrime/securesms/notifications/v2/ConversationId;", "Lorg/thoughtcrime/securesms/notifications/v2/MessageNotifierV2$StickyThread;", "notificationProfile", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", "MessageInclusion", "NotificationMessage", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationStateProvider {
    public static final NotificationStateProvider INSTANCE = new NotificationStateProvider();
    private static final String TAG = Log.tag(NotificationStateProvider.class);

    /* compiled from: NotificationStateProvider.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/NotificationStateProvider$MessageInclusion;", "", "(Ljava/lang/String;I)V", "INCLUDE", "EXCLUDE", "MUTE_FILTERED", "PROFILE_FILTERED", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum MessageInclusion {
        INCLUDE,
        EXCLUDE,
        MUTE_FILTERED,
        PROFILE_FILTERED
    }

    /* compiled from: NotificationStateProvider.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[MessageInclusion.values().length];
            iArr[MessageInclusion.INCLUDE.ordinal()] = 1;
            iArr[MessageInclusion.EXCLUDE.ordinal()] = 2;
            iArr[MessageInclusion.MUTE_FILTERED.ordinal()] = 3;
            iArr[MessageInclusion.PROFILE_FILTERED.ordinal()] = 4;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    private NotificationStateProvider() {
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX DEBUG: Type inference failed for r11v9. Raw type applied. Possible types: java.util.Iterator<T>, java.util.Iterator */
    /* JADX WARN: Type inference failed for: r12v13, types: [org.thoughtcrime.securesms.database.model.DisplayRecord] */
    /* JADX WARN: Type inference failed for: r12v15 */
    public final NotificationStateV2 constructNotificationState(Map<ConversationId, MessageNotifierV2.StickyThread> map, NotificationProfile notificationProfile) {
        Throwable th;
        Throwable th2;
        int i;
        MmsSmsDatabase.Reader reader;
        Boolean valueOf;
        boolean z;
        Map<ConversationId, MessageNotifierV2.StickyThread> map2 = map;
        Intrinsics.checkNotNullParameter(map2, "stickyThreads");
        ArrayList arrayList = new ArrayList();
        Cursor messagesForNotificationState = SignalDatabase.Companion.mmsSms().getMessagesForNotificationState(map.values());
        try {
            Throwable th3 = null;
            if (messagesForNotificationState.getCount() == 0) {
                return NotificationStateV2.Companion.getEMPTY();
            }
            MmsSmsDatabase.Reader readerFor = MmsSmsDatabase.readerFor(messagesForNotificationState);
            try {
                MessageRecord next = readerFor.getNext();
                while (next != null) {
                    try {
                        SignalDatabase.Companion companion = SignalDatabase.Companion;
                        Recipient recipientForThreadId = companion.threads().getRecipientForThreadId(next.getThreadId());
                        if (recipientForThreadId != null) {
                            boolean z2 = CursorUtil.requireInt(messagesForNotificationState, MmsSmsColumns.REACTIONS_UNREAD) == 1;
                            ConversationId fromMessageRecord = ConversationId.Companion.fromMessageRecord(next);
                            Long groupStoryId = fromMessageRecord.getGroupStoryId();
                            MessageRecord messageRecord = groupStoryId != null ? companion.mms().getMessageRecord(groupStoryId.longValue()) : th3;
                            Long groupStoryId2 = fromMessageRecord.getGroupStoryId();
                            reader = readerFor;
                            if (groupStoryId2 != null) {
                                try {
                                    valueOf = Boolean.valueOf(companion.mms().hasSelfReplyInGroupStory(groupStoryId2.longValue()));
                                } catch (Throwable th4) {
                                    th = th4;
                                    throw th;
                                }
                            } else {
                                valueOf = null;
                            }
                            List<ReactionRecord> reactions = z2 ? companion.reactions().getReactions(new MessageId(next.getId(), next.isMms())) : CollectionsKt__CollectionsKt.emptyList();
                            map2 = map;
                            boolean containsKey = map2.containsKey(fromMessageRecord);
                            boolean z3 = CursorUtil.requireInt(messagesForNotificationState, "read") == 0;
                            long requireLong = CursorUtil.requireLong(messagesForNotificationState, MmsSmsColumns.REACTIONS_LAST_SEEN);
                            boolean isOutgoing = messageRecord != 0 ? messageRecord.isOutgoing() : false;
                            if (valueOf != null) {
                                z = valueOf.booleanValue();
                            } else {
                                z = false;
                            }
                            arrayList.add(new NotificationMessage(next, reactions, recipientForThreadId, fromMessageRecord, containsKey, z3, z2, requireLong, isOutgoing, z));
                        } else {
                            reader = readerFor;
                        }
                        try {
                            next = reader.getNext();
                            readerFor = reader;
                            th3 = null;
                        } catch (IllegalStateException e) {
                            Log.w(TAG, "Failed to read next record!", e);
                            readerFor = reader;
                            th3 = null;
                            next = null;
                        }
                    } catch (Throwable th5) {
                        th = th5;
                    }
                }
                try {
                    Unit unit = Unit.INSTANCE;
                    Throwable th6 = null;
                    CloseableKt.closeFinally(messagesForNotificationState, th6);
                    ArrayList arrayList2 = new ArrayList();
                    ArrayList arrayList3 = new ArrayList();
                    ArrayList arrayList4 = new ArrayList();
                    LinkedHashMap linkedHashMap = new LinkedHashMap();
                    for (Object obj : arrayList) {
                        ConversationId thread = ((NotificationMessage) obj).getThread();
                        Object obj2 = linkedHashMap.get(thread);
                        if (obj2 == null) {
                            obj2 = new ArrayList();
                            linkedHashMap.put(thread, obj2);
                        }
                        ((List) obj2).add(obj);
                    }
                    for (Map.Entry entry : linkedHashMap.entrySet()) {
                        ConversationId conversationId = (ConversationId) entry.getKey();
                        List arrayList5 = new ArrayList();
                        for (NotificationMessage notificationMessage : (List) entry.getValue()) {
                            int i2 = WhenMappings.$EnumSwitchMapping$0[notificationMessage.includeMessage(notificationProfile).ordinal()];
                            int i3 = 3;
                            if (i2 == 1) {
                                arrayList5.add(new MessageNotification(notificationMessage.getThreadRecipient(), notificationMessage.getMessageRecord()));
                            } else if (i2 == 3) {
                                arrayList3.add(new NotificationStateV2.FilteredMessage(notificationMessage.getMessageRecord().getId(), notificationMessage.getMessageRecord().isMms()));
                            } else if (i2 == 4) {
                                arrayList4.add(new NotificationStateV2.FilteredMessage(notificationMessage.getMessageRecord().getId(), notificationMessage.getMessageRecord().isMms()));
                            }
                            if (notificationMessage.getHasUnreadReactions()) {
                                for (ReactionRecord reactionRecord : notificationMessage.getReactions()) {
                                    int i4 = WhenMappings.$EnumSwitchMapping$0[notificationMessage.includeReaction(reactionRecord, notificationProfile).ordinal()];
                                    if (i4 == 1) {
                                        arrayList5.add(new ReactionNotification(notificationMessage.getThreadRecipient(), notificationMessage.getMessageRecord(), reactionRecord));
                                    } else if (i4 == i3) {
                                        arrayList3.add(new NotificationStateV2.FilteredMessage(notificationMessage.getMessageRecord().getId(), notificationMessage.getMessageRecord().isMms()));
                                    } else if (i4 == 4) {
                                        arrayList4.add(new NotificationStateV2.FilteredMessage(notificationMessage.getMessageRecord().getId(), notificationMessage.getMessageRecord().isMms()));
                                    }
                                    i3 = 3;
                                }
                            }
                        }
                        CollectionsKt__MutableCollectionsJVMKt.sort(arrayList5);
                        if ((!arrayList5.isEmpty()) && map2.containsKey(conversationId) && !((NotificationItemV2) CollectionsKt___CollectionsKt.last((List) ((List<? extends Object>) arrayList5))).getIndividualRecipient().isSelf()) {
                            ListIterator listIterator = arrayList5.listIterator(arrayList5.size());
                            while (true) {
                                if (listIterator.hasPrevious()) {
                                    if (((NotificationItemV2) listIterator.previous()).getIndividualRecipient().isSelf()) {
                                        i = listIterator.nextIndex();
                                        break;
                                    }
                                } else {
                                    i = -1;
                                    break;
                                }
                            }
                            arrayList5 = CollectionsKt___CollectionsKt.toMutableList((Collection) CollectionsKt___CollectionsKt.slice(arrayList5, new IntRange(i + 1, CollectionsKt__CollectionsKt.getLastIndex(arrayList5))));
                        }
                        if (!arrayList5.isEmpty()) {
                            arrayList2.add(new NotificationConversation(((NotificationItemV2) arrayList5.get(0)).getThreadRecipient(), conversationId, arrayList5));
                        }
                    }
                    return new NotificationStateV2(arrayList2, arrayList3, arrayList4);
                } catch (Throwable th7) {
                    th2 = th7;
                    th = th2;
                    throw th;
                }
            } catch (Throwable th8) {
                th2 = th8;
            }
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    /* compiled from: NotificationStateProvider.kt */
    @Metadata(d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b$\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B[\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\f\u0012\u0006\u0010\u000e\u001a\u00020\f\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\f\u0012\u0006\u0010\u0012\u001a\u00020\f¢\u0006\u0002\u0010\u0013J\t\u0010'\u001a\u00020\u0003HÆ\u0003J\t\u0010(\u001a\u00020\fHÆ\u0003J\u000f\u0010)\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J\t\u0010*\u001a\u00020\bHÆ\u0003J\t\u0010+\u001a\u00020\nHÆ\u0003J\t\u0010,\u001a\u00020\fHÆ\u0003J\t\u0010-\u001a\u00020\fHÆ\u0003J\t\u0010.\u001a\u00020\fHÆ\u0003J\t\u0010/\u001a\u00020\u0010HÆ\u0003J\t\u00100\u001a\u00020\fHÆ\u0003Js\u00101\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\f2\b\b\u0002\u0010\r\u001a\u00020\f2\b\b\u0002\u0010\u000e\u001a\u00020\f2\b\b\u0002\u0010\u000f\u001a\u00020\u00102\b\b\u0002\u0010\u0011\u001a\u00020\f2\b\b\u0002\u0010\u0012\u001a\u00020\fHÆ\u0001J\u0013\u00102\u001a\u00020\f2\b\u00103\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u00104\u001a\u000205HÖ\u0001J\u0010\u00106\u001a\u0002072\b\u00108\u001a\u0004\u0018\u000109J\u0018\u0010:\u001a\u0002072\u0006\u0010;\u001a\u00020\u00062\b\u00108\u001a\u0004\u0018\u000109J\t\u0010<\u001a\u00020=HÖ\u0001R\u0011\u0010\u0012\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u000e\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0015R\u000e\u0010\u0017\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0011\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0015R\u000e\u0010\u0019\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\r\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u0015R\u0011\u0010\u000f\u001a\u00020\u0010¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b \u0010\u0015R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\"R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b#\u0010$R\u0018\u0010%\u001a\u00020\f*\u00020\b8BX\u0004¢\u0006\u0006\u001a\u0004\b%\u0010&¨\u0006>"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/NotificationStateProvider$NotificationMessage;", "", "messageRecord", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "reactions", "", "Lorg/thoughtcrime/securesms/database/model/ReactionRecord;", "threadRecipient", "Lorg/thoughtcrime/securesms/recipients/Recipient;", ThreadDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/notifications/v2/ConversationId;", "stickyThread", "", "isUnreadMessage", "hasUnreadReactions", "lastReactionRead", "", "isParentStorySentBySelf", "hasSelfRepliedToStory", "(Lorg/thoughtcrime/securesms/database/model/MessageRecord;Ljava/util/List;Lorg/thoughtcrime/securesms/recipients/Recipient;Lorg/thoughtcrime/securesms/notifications/v2/ConversationId;ZZZJZZ)V", "getHasSelfRepliedToStory", "()Z", "getHasUnreadReactions", "isGroupStoryReply", "isNotifiableGroupStoryMessage", "isUnreadIncoming", "getLastReactionRead", "()J", "getMessageRecord", "()Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "getReactions", "()Ljava/util/List;", "getStickyThread", "getThread", "()Lorg/thoughtcrime/securesms/notifications/v2/ConversationId;", "getThreadRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "isDoNotNotifyMentions", "(Lorg/thoughtcrime/securesms/recipients/Recipient;)Z", "component1", "component10", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "", "includeMessage", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationStateProvider$MessageInclusion;", "notificationProfile", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", "includeReaction", ReactionDatabase.TABLE_NAME, "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class NotificationMessage {
        private final boolean hasSelfRepliedToStory;
        private final boolean hasUnreadReactions;
        private final boolean isGroupStoryReply;
        private final boolean isNotifiableGroupStoryMessage;
        private final boolean isParentStorySentBySelf;
        private final boolean isUnreadIncoming;
        private final boolean isUnreadMessage;
        private final long lastReactionRead;
        private final MessageRecord messageRecord;
        private final List<ReactionRecord> reactions;
        private final boolean stickyThread;
        private final ConversationId thread;
        private final Recipient threadRecipient;

        public final MessageRecord component1() {
            return this.messageRecord;
        }

        public final boolean component10() {
            return this.hasSelfRepliedToStory;
        }

        public final List<ReactionRecord> component2() {
            return this.reactions;
        }

        public final Recipient component3() {
            return this.threadRecipient;
        }

        public final ConversationId component4() {
            return this.thread;
        }

        public final boolean component5() {
            return this.stickyThread;
        }

        public final boolean component6() {
            return this.isUnreadMessage;
        }

        public final boolean component7() {
            return this.hasUnreadReactions;
        }

        public final long component8() {
            return this.lastReactionRead;
        }

        public final boolean component9() {
            return this.isParentStorySentBySelf;
        }

        public final NotificationMessage copy(MessageRecord messageRecord, List<ReactionRecord> list, Recipient recipient, ConversationId conversationId, boolean z, boolean z2, boolean z3, long j, boolean z4, boolean z5) {
            Intrinsics.checkNotNullParameter(messageRecord, "messageRecord");
            Intrinsics.checkNotNullParameter(list, "reactions");
            Intrinsics.checkNotNullParameter(recipient, "threadRecipient");
            Intrinsics.checkNotNullParameter(conversationId, ThreadDatabase.TABLE_NAME);
            return new NotificationMessage(messageRecord, list, recipient, conversationId, z, z2, z3, j, z4, z5);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof NotificationMessage)) {
                return false;
            }
            NotificationMessage notificationMessage = (NotificationMessage) obj;
            return Intrinsics.areEqual(this.messageRecord, notificationMessage.messageRecord) && Intrinsics.areEqual(this.reactions, notificationMessage.reactions) && Intrinsics.areEqual(this.threadRecipient, notificationMessage.threadRecipient) && Intrinsics.areEqual(this.thread, notificationMessage.thread) && this.stickyThread == notificationMessage.stickyThread && this.isUnreadMessage == notificationMessage.isUnreadMessage && this.hasUnreadReactions == notificationMessage.hasUnreadReactions && this.lastReactionRead == notificationMessage.lastReactionRead && this.isParentStorySentBySelf == notificationMessage.isParentStorySentBySelf && this.hasSelfRepliedToStory == notificationMessage.hasSelfRepliedToStory;
        }

        public int hashCode() {
            int hashCode = ((((((this.messageRecord.hashCode() * 31) + this.reactions.hashCode()) * 31) + this.threadRecipient.hashCode()) * 31) + this.thread.hashCode()) * 31;
            boolean z = this.stickyThread;
            int i = 1;
            if (z) {
                z = true;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            int i5 = (hashCode + i2) * 31;
            boolean z2 = this.isUnreadMessage;
            if (z2) {
                z2 = true;
            }
            int i6 = z2 ? 1 : 0;
            int i7 = z2 ? 1 : 0;
            int i8 = z2 ? 1 : 0;
            int i9 = (i5 + i6) * 31;
            boolean z3 = this.hasUnreadReactions;
            if (z3) {
                z3 = true;
            }
            int i10 = z3 ? 1 : 0;
            int i11 = z3 ? 1 : 0;
            int i12 = z3 ? 1 : 0;
            int m = (((i9 + i10) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.lastReactionRead)) * 31;
            boolean z4 = this.isParentStorySentBySelf;
            if (z4) {
                z4 = true;
            }
            int i13 = z4 ? 1 : 0;
            int i14 = z4 ? 1 : 0;
            int i15 = z4 ? 1 : 0;
            int i16 = (m + i13) * 31;
            boolean z5 = this.hasSelfRepliedToStory;
            if (!z5) {
                i = z5 ? 1 : 0;
            }
            return i16 + i;
        }

        public String toString() {
            return "NotificationMessage(messageRecord=" + this.messageRecord + ", reactions=" + this.reactions + ", threadRecipient=" + this.threadRecipient + ", thread=" + this.thread + ", stickyThread=" + this.stickyThread + ", isUnreadMessage=" + this.isUnreadMessage + ", hasUnreadReactions=" + this.hasUnreadReactions + ", lastReactionRead=" + this.lastReactionRead + ", isParentStorySentBySelf=" + this.isParentStorySentBySelf + ", hasSelfRepliedToStory=" + this.hasSelfRepliedToStory + ')';
        }

        public NotificationMessage(MessageRecord messageRecord, List<ReactionRecord> list, Recipient recipient, ConversationId conversationId, boolean z, boolean z2, boolean z3, long j, boolean z4, boolean z5) {
            Intrinsics.checkNotNullParameter(messageRecord, "messageRecord");
            Intrinsics.checkNotNullParameter(list, "reactions");
            Intrinsics.checkNotNullParameter(recipient, "threadRecipient");
            Intrinsics.checkNotNullParameter(conversationId, ThreadDatabase.TABLE_NAME);
            this.messageRecord = messageRecord;
            this.reactions = list;
            this.threadRecipient = recipient;
            this.thread = conversationId;
            this.stickyThread = z;
            this.isUnreadMessage = z2;
            this.hasUnreadReactions = z3;
            this.lastReactionRead = j;
            this.isParentStorySentBySelf = z4;
            this.hasSelfRepliedToStory = z5;
            boolean z6 = true;
            boolean z7 = conversationId.getGroupStoryId() != null;
            this.isGroupStoryReply = z7;
            this.isUnreadIncoming = z2 && !messageRecord.isOutgoing() && !z7;
            if (!z2 || messageRecord.isOutgoing() || !z7 || (!z4 && (!z5 || MessageRecordUtil.isStoryReaction(messageRecord)))) {
                z6 = false;
            }
            this.isNotifiableGroupStoryMessage = z6;
        }

        public final MessageRecord getMessageRecord() {
            return this.messageRecord;
        }

        public final List<ReactionRecord> getReactions() {
            return this.reactions;
        }

        public final Recipient getThreadRecipient() {
            return this.threadRecipient;
        }

        public final ConversationId getThread() {
            return this.thread;
        }

        public final boolean getStickyThread() {
            return this.stickyThread;
        }

        public final boolean isUnreadMessage() {
            return this.isUnreadMessage;
        }

        public final boolean getHasUnreadReactions() {
            return this.hasUnreadReactions;
        }

        public final long getLastReactionRead() {
            return this.lastReactionRead;
        }

        public final boolean isParentStorySentBySelf() {
            return this.isParentStorySentBySelf;
        }

        public final boolean getHasSelfRepliedToStory() {
            return this.hasSelfRepliedToStory;
        }

        public final MessageInclusion includeMessage(NotificationProfile notificationProfile) {
            if (!this.isUnreadIncoming && !this.stickyThread && !this.isNotifiableGroupStoryMessage) {
                return MessageInclusion.EXCLUDE;
            }
            if (this.threadRecipient.isMuted() && (isDoNotNotifyMentions(this.threadRecipient) || !this.messageRecord.hasSelfMention())) {
                return MessageInclusion.MUTE_FILTERED;
            }
            if (notificationProfile != null) {
                RecipientId id = this.threadRecipient.getId();
                Intrinsics.checkNotNullExpressionValue(id, "threadRecipient.id");
                if (!notificationProfile.isRecipientAllowed(id) && (!notificationProfile.getAllowAllMentions() || !this.messageRecord.hasSelfMention())) {
                    return MessageInclusion.PROFILE_FILTERED;
                }
            }
            return MessageInclusion.INCLUDE;
        }

        public final MessageInclusion includeReaction(ReactionRecord reactionRecord, NotificationProfile notificationProfile) {
            Intrinsics.checkNotNullParameter(reactionRecord, ReactionDatabase.TABLE_NAME);
            if (this.threadRecipient.isMuted()) {
                return MessageInclusion.MUTE_FILTERED;
            }
            if (notificationProfile != null) {
                RecipientId id = this.threadRecipient.getId();
                Intrinsics.checkNotNullExpressionValue(id, "threadRecipient.id");
                if (!notificationProfile.isRecipientAllowed(id)) {
                    return MessageInclusion.PROFILE_FILTERED;
                }
            }
            if (Intrinsics.areEqual(reactionRecord.getAuthor(), Recipient.self().getId()) || !this.messageRecord.isOutgoing() || reactionRecord.getDateReceived() <= this.lastReactionRead) {
                return MessageInclusion.EXCLUDE;
            }
            return MessageInclusion.INCLUDE;
        }

        private final boolean isDoNotNotifyMentions(Recipient recipient) {
            return recipient.getMentionSetting() == RecipientDatabase.MentionSetting.DO_NOT_NOTIFY;
        }
    }
}
