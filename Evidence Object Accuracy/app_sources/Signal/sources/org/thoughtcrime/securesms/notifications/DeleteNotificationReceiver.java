package org.thoughtcrime.securesms.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import java.util.ArrayList;
import java.util.Iterator;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;

/* loaded from: classes4.dex */
public class DeleteNotificationReceiver extends BroadcastReceiver {
    public static String DELETE_NOTIFICATION_ACTION;
    public static final String EXTRA_IDS;
    public static final String EXTRA_MMS;
    public static final String EXTRA_THREADS;

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (DELETE_NOTIFICATION_ACTION.equals(intent.getAction())) {
            MessageNotifier messageNotifier = ApplicationDependencies.getMessageNotifier();
            messageNotifier.clearReminder(context);
            long[] longArrayExtra = intent.getLongArrayExtra(EXTRA_IDS);
            boolean[] booleanArrayExtra = intent.getBooleanArrayExtra("is_mms");
            ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("threads");
            if (parcelableArrayListExtra != null) {
                Iterator it = parcelableArrayListExtra.iterator();
                while (it.hasNext()) {
                    messageNotifier.removeStickyThread((ConversationId) it.next());
                }
            }
            if (longArrayExtra != null && booleanArrayExtra != null && longArrayExtra.length == booleanArrayExtra.length) {
                SignalExecutors.BOUNDED.execute(new Runnable(longArrayExtra, booleanArrayExtra, goAsync()) { // from class: org.thoughtcrime.securesms.notifications.DeleteNotificationReceiver$$ExternalSyntheticLambda0
                    public final /* synthetic */ long[] f$0;
                    public final /* synthetic */ boolean[] f$1;
                    public final /* synthetic */ BroadcastReceiver.PendingResult f$2;

                    {
                        this.f$0 = r1;
                        this.f$1 = r2;
                        this.f$2 = r3;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        DeleteNotificationReceiver.$r8$lambda$vGXWK9RV6txml_2_CMZZQ4ZihWw(this.f$0, this.f$1, this.f$2);
                    }
                });
            }
        }
    }

    public static /* synthetic */ void lambda$onReceive$0(long[] jArr, boolean[] zArr, BroadcastReceiver.PendingResult pendingResult) {
        for (int i = 0; i < jArr.length; i++) {
            if (!zArr[i]) {
                SignalDatabase.sms().markAsNotified(jArr[i]);
            } else {
                SignalDatabase.mms().markAsNotified(jArr[i]);
            }
        }
        pendingResult.finish();
    }
}
