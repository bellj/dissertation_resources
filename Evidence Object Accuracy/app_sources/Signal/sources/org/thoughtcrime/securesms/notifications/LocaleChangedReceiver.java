package org.thoughtcrime.securesms.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import org.thoughtcrime.securesms.jobs.EmojiSearchIndexDownloadJob;

/* loaded from: classes4.dex */
public class LocaleChangedReceiver extends BroadcastReceiver {
    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        NotificationChannels.create(context);
        EmojiSearchIndexDownloadJob.scheduleImmediately();
    }
}
