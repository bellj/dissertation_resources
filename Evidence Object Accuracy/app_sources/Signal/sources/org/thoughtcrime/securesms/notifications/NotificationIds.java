package org.thoughtcrime.securesms.notifications;

import org.thoughtcrime.securesms.notifications.v2.ConversationId;

/* loaded from: classes4.dex */
public final class NotificationIds {
    public static final int APPLICATION_MIGRATION;
    public static final int DEVICE_TRANSFER;
    public static final int DONOR_BADGE_FAILURE;
    public static final int FCM_FAILURE;
    public static final int FCM_FETCH;
    public static final int INTERNAL_ERROR;
    public static final int LEGACY_SQLCIPHER_MIGRATION;
    public static final int MESSAGE_DELIVERY_FAILURE;
    public static final int MESSAGE_SUMMARY;
    public static final int PENDING_MESSAGES;
    public static final int PRE_REGISTRATION_SMS;
    public static final int SMS_IMPORT_COMPLETE;
    public static final int STORY_MESSAGE_DELIVERY_FAILURE;
    public static final int STORY_THREAD;
    public static final int THREAD;
    public static final int USER_NOTIFICATION_MIGRATION;

    private NotificationIds() {
    }

    public static int getNotificationIdForThread(ConversationId conversationId) {
        int i;
        int threadId;
        if (conversationId.getGroupStoryId() != null) {
            i = STORY_THREAD;
            threadId = conversationId.getGroupStoryId().intValue();
        } else {
            i = 50000;
            threadId = (int) conversationId.getThreadId();
        }
        return threadId + i;
    }

    public static int getNotificationIdForMessageDeliveryFailed(ConversationId conversationId) {
        int i;
        int threadId;
        if (conversationId.getGroupStoryId() != null) {
            i = STORY_MESSAGE_DELIVERY_FAILURE;
            threadId = conversationId.getGroupStoryId().intValue();
        } else {
            i = MESSAGE_DELIVERY_FAILURE;
            threadId = (int) conversationId.getThreadId();
        }
        return threadId + i;
    }
}
