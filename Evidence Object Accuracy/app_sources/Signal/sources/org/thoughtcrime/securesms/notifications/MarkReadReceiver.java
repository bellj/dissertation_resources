package org.thoughtcrime.securesms.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.MmsDatabase$$ExternalSyntheticLambda8;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.MultiDeviceReadUpdateJob;
import org.thoughtcrime.securesms.jobs.SendReadReceiptJob;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.service.ExpiringMessageManager;

/* loaded from: classes4.dex */
public class MarkReadReceiver extends BroadcastReceiver {
    public static final String CLEAR_ACTION;
    public static final String NOTIFICATION_ID_EXTRA;
    private static final String TAG = Log.tag(MarkReadReceiver.class);
    public static final String THREADS_EXTRA;

    public static /* synthetic */ MessageDatabase.MarkedMessageInfo lambda$process$3(MessageDatabase.MarkedMessageInfo markedMessageInfo) {
        return markedMessageInfo;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        ArrayList parcelableArrayListExtra;
        if (CLEAR_ACTION.equals(intent.getAction()) && (parcelableArrayListExtra = intent.getParcelableArrayListExtra("threads")) != null) {
            MessageNotifier messageNotifier = ApplicationDependencies.getMessageNotifier();
            Iterator it = parcelableArrayListExtra.iterator();
            while (it.hasNext()) {
                messageNotifier.removeStickyThread((ConversationId) it.next());
            }
            NotificationCancellationHelper.cancelLegacy(context, intent.getIntExtra(NOTIFICATION_ID_EXTRA, -1));
            SignalExecutors.BOUNDED.execute(new Runnable(parcelableArrayListExtra, context, goAsync()) { // from class: org.thoughtcrime.securesms.notifications.MarkReadReceiver$$ExternalSyntheticLambda11
                public final /* synthetic */ ArrayList f$0;
                public final /* synthetic */ Context f$1;
                public final /* synthetic */ BroadcastReceiver.PendingResult f$2;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    MarkReadReceiver.lambda$onReceive$0(this.f$0, this.f$1, this.f$2);
                }
            });
        }
    }

    public static /* synthetic */ void lambda$onReceive$0(ArrayList arrayList, Context context, BroadcastReceiver.PendingResult pendingResult) {
        LinkedList linkedList = new LinkedList();
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            ConversationId conversationId = (ConversationId) it.next();
            String str = TAG;
            Log.i(str, "Marking as read: " + conversationId);
            linkedList.addAll(SignalDatabase.threads().setRead(conversationId, true));
        }
        process(context, linkedList);
        ApplicationDependencies.getMessageNotifier().updateNotification(context);
        pendingResult.finish();
    }

    public static void process(Context context, List<MessageDatabase.MarkedMessageInfo> list) {
        if (!list.isEmpty()) {
            List list2 = Stream.of(list).map(new Function() { // from class: org.thoughtcrime.securesms.notifications.MarkReadReceiver$$ExternalSyntheticLambda3
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return ((MessageDatabase.MarkedMessageInfo) obj).getSyncMessageId();
                }
            }).toList();
            scheduleDeletion(context, Stream.of(list).map(new Function() { // from class: org.thoughtcrime.securesms.notifications.MarkReadReceiver$$ExternalSyntheticLambda4
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return ((MessageDatabase.MarkedMessageInfo) obj).getExpirationInfo();
                }
            }).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.notifications.MarkReadReceiver$$ExternalSyntheticLambda5
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return ((MessageDatabase.ExpirationInfo) obj).isMms();
                }
            }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.notifications.MarkReadReceiver$$ExternalSyntheticLambda7
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return MarkReadReceiver.lambda$process$2((MessageDatabase.ExpirationInfo) obj);
                }
            }).toList(), Stream.of(list).map(new Function() { // from class: org.thoughtcrime.securesms.notifications.MarkReadReceiver$$ExternalSyntheticLambda4
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return ((MessageDatabase.MarkedMessageInfo) obj).getExpirationInfo();
                }
            }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.notifications.MarkReadReceiver$$ExternalSyntheticLambda5
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return ((MessageDatabase.ExpirationInfo) obj).isMms();
                }
            }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.notifications.MarkReadReceiver$$ExternalSyntheticLambda6
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return MarkReadReceiver.lambda$process$1((MessageDatabase.ExpirationInfo) obj);
                }
            }).toList());
            MultiDeviceReadUpdateJob.enqueue(list2);
            Stream.of((Map) Stream.of(list).collect(Collectors.groupingBy(new MmsDatabase$$ExternalSyntheticLambda8()))).forEach(new Consumer() { // from class: org.thoughtcrime.securesms.notifications.MarkReadReceiver$$ExternalSyntheticLambda8
                @Override // com.annimon.stream.function.Consumer
                public final void accept(Object obj) {
                    MarkReadReceiver.lambda$process$6((Map.Entry) obj);
                }
            });
        }
    }

    public static /* synthetic */ boolean lambda$process$1(MessageDatabase.ExpirationInfo expirationInfo) {
        return expirationInfo.getExpiresIn() > 0 && expirationInfo.getExpireStarted() <= 0;
    }

    public static /* synthetic */ boolean lambda$process$2(MessageDatabase.ExpirationInfo expirationInfo) {
        return expirationInfo.getExpiresIn() > 0 && expirationInfo.getExpireStarted() <= 0;
    }

    public static /* synthetic */ void lambda$process$6(Map.Entry entry) {
        Stream.of((Map) Stream.of((Iterable) entry.getValue()).map(new Function() { // from class: org.thoughtcrime.securesms.notifications.MarkReadReceiver$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MarkReadReceiver.lambda$process$3((MessageDatabase.MarkedMessageInfo) obj);
            }
        }).collect(Collectors.groupingBy(new Function() { // from class: org.thoughtcrime.securesms.notifications.MarkReadReceiver$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MarkReadReceiver.lambda$process$4((MessageDatabase.MarkedMessageInfo) obj);
            }
        }))).forEach(new Consumer(entry) { // from class: org.thoughtcrime.securesms.notifications.MarkReadReceiver$$ExternalSyntheticLambda2
            public final /* synthetic */ Map.Entry f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Consumer
            public final void accept(Object obj) {
                MarkReadReceiver.lambda$process$5(this.f$0, (Map.Entry) obj);
            }
        });
    }

    public static /* synthetic */ RecipientId lambda$process$4(MessageDatabase.MarkedMessageInfo markedMessageInfo) {
        return markedMessageInfo.getSyncMessageId().getRecipientId();
    }

    public static /* synthetic */ void lambda$process$5(Map.Entry entry, Map.Entry entry2) {
        SendReadReceiptJob.enqueue(((Long) entry.getKey()).longValue(), (RecipientId) entry2.getKey(), (List) entry2.getValue());
    }

    private static void scheduleDeletion(Context context, List<MessageDatabase.ExpirationInfo> list, List<MessageDatabase.ExpirationInfo> list2) {
        if (list.size() > 0) {
            SignalDatabase.sms().markExpireStarted(Stream.of(list).map(new Function() { // from class: org.thoughtcrime.securesms.notifications.MarkReadReceiver$$ExternalSyntheticLambda9
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return Long.valueOf(((MessageDatabase.ExpirationInfo) obj).getId());
                }
            }).toList(), System.currentTimeMillis());
        }
        if (list2.size() > 0) {
            SignalDatabase.mms().markExpireStarted(Stream.of(list2).map(new Function() { // from class: org.thoughtcrime.securesms.notifications.MarkReadReceiver$$ExternalSyntheticLambda9
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return Long.valueOf(((MessageDatabase.ExpirationInfo) obj).getId());
                }
            }).toList(), System.currentTimeMillis());
        }
        if (list.size() + list2.size() > 0) {
            Stream.concat(Stream.of(list), Stream.of(list2)).forEach(new Consumer() { // from class: org.thoughtcrime.securesms.notifications.MarkReadReceiver$$ExternalSyntheticLambda10
                @Override // com.annimon.stream.function.Consumer
                public final void accept(Object obj) {
                    MarkReadReceiver.lambda$scheduleDeletion$7(ExpiringMessageManager.this, (MessageDatabase.ExpirationInfo) obj);
                }
            });
        }
    }

    public static /* synthetic */ void lambda$scheduleDeletion$7(ExpiringMessageManager expiringMessageManager, MessageDatabase.ExpirationInfo expirationInfo) {
        expiringMessageManager.scheduleDeletion(expirationInfo.getId(), expirationInfo.isMms(), expirationInfo.getExpiresIn());
    }
}
