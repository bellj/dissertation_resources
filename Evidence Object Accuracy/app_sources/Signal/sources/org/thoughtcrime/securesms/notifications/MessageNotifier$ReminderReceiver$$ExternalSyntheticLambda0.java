package org.thoughtcrime.securesms.notifications;

import android.content.Context;
import android.content.Intent;
import org.thoughtcrime.securesms.notifications.MessageNotifier;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MessageNotifier$ReminderReceiver$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ Intent f$0;
    public final /* synthetic */ Context f$1;

    public /* synthetic */ MessageNotifier$ReminderReceiver$$ExternalSyntheticLambda0(Intent intent, Context context) {
        this.f$0 = intent;
        this.f$1 = context;
    }

    @Override // java.lang.Runnable
    public final void run() {
        MessageNotifier.ReminderReceiver.lambda$onReceive$0(this.f$0, this.f$1);
    }
}
