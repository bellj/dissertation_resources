package org.thoughtcrime.securesms.notifications;

import android.app.NotificationChannel;
import android.app.NotificationChannelGroup;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.widget.Toast;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Predicate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.ConversationUtil;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes.dex */
public class NotificationChannels {
    static final /* synthetic */ boolean $assertionsDisabled;
    public static final String APP_UPDATES;
    public static final String BACKGROUND;
    public static final String BACKUPS;
    public static final String CALLS;
    public static final String CALL_STATUS;
    private static final String CATEGORY_MESSAGES;
    private static final String CONTACT_PREFIX;
    private static final long[] EMPTY_VIBRATION_PATTERN = {0};
    public static final String FAILURES;
    public static final String JOIN_EVENTS;
    public static final String LOCKED_STATUS;
    private static final String MESSAGES_PREFIX;
    public static final String OTHER;
    private static final String TAG = Log.tag(NotificationChannels.class);
    private static final int VERSION;
    public static final String VOICE_NOTES;

    /* loaded from: classes4.dex */
    public interface ChannelUpdater {
        void update(NotificationChannel notificationChannel);
    }

    /* loaded from: classes4.dex */
    private static class Version {
        static final int CALLS_PRIORITY_BUMP;
        static final int MESSAGES_CATEGORY;
        static final int VIBRATE_OFF_OTHER;

        private Version() {
        }
    }

    public static synchronized void create(Context context) {
        synchronized (NotificationChannels.class) {
            if (supported()) {
                NotificationManager notificationManager = ServiceUtil.getNotificationManager(context);
                int notificationChannelVersion = TextSecurePreferences.getNotificationChannelVersion(context);
                if (notificationChannelVersion != 4) {
                    onUpgrade(notificationManager, notificationChannelVersion, 4);
                    TextSecurePreferences.setNotificationChannelVersion(context, 4);
                }
                onCreate(context, notificationManager);
                AsyncTask.SERIAL_EXECUTOR.execute(new Runnable(context) { // from class: org.thoughtcrime.securesms.notifications.NotificationChannels$$ExternalSyntheticLambda1
                    public final /* synthetic */ Context f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        NotificationChannels.ensureCustomChannelConsistency(this.f$0);
                    }
                });
            }
        }
    }

    public static synchronized void restoreContactNotificationChannels(Context context) {
        synchronized (NotificationChannels.class) {
            if (supported()) {
                RecipientDatabase recipients = SignalDatabase.recipients();
                RecipientDatabase.RecipientReader recipientsWithNotificationChannels = recipients.getRecipientsWithNotificationChannels();
                while (true) {
                    Recipient next = recipientsWithNotificationChannels.getNext();
                    if (next == null) {
                        recipientsWithNotificationChannels.close();
                        ensureCustomChannelConsistency(context);
                        return;
                    } else if (!channelExists(ServiceUtil.getNotificationManager(context).getNotificationChannel(next.getNotificationChannel()))) {
                        recipients.setNotificationChannel(next.getId(), createChannelFor(context, next));
                    }
                }
            }
        }
    }

    public static synchronized String getMessagesChannel(Context context) {
        String messagesChannelId;
        synchronized (NotificationChannels.class) {
            messagesChannelId = getMessagesChannelId(TextSecurePreferences.getNotificationMessagesChannelVersion(context));
        }
        return messagesChannelId;
    }

    public static boolean supported() {
        return Build.VERSION.SDK_INT >= 26;
    }

    public static String getChannelDisplayNameFor(Context context, String str, String str2, String str3, String str4) {
        if (!TextUtils.isEmpty(str)) {
            return str;
        }
        if (!TextUtils.isEmpty(str2)) {
            return str2;
        }
        if (!TextUtils.isEmpty(str3)) {
            return str3;
        }
        if (!TextUtils.isEmpty(str4)) {
            return str4;
        }
        return context.getString(R.string.NotificationChannel_missing_display_name);
    }

    public static synchronized String createChannelFor(Context context, Recipient recipient) {
        synchronized (NotificationChannels.class) {
            if (recipient.getId().isUnknown()) {
                return null;
            }
            RecipientDatabase.VibrateState messageVibrate = recipient.getMessageVibrate();
            return createChannelFor(context, generateChannelIdFor(recipient), recipient.getDisplayName(context), recipient.getMessageRingtone() != null ? recipient.getMessageRingtone() : getMessageRingtone(context), messageVibrate == RecipientDatabase.VibrateState.DEFAULT ? SignalStore.settings().isMessageVibrateEnabled() : messageVibrate == RecipientDatabase.VibrateState.ENABLED, ConversationUtil.getShortcutId(recipient));
        }
    }

    public static synchronized String createChannelFor(Context context, String str, String str2, Uri uri, boolean z, String str3) {
        synchronized (NotificationChannels.class) {
            if (!supported()) {
                return null;
            }
            NotificationChannel notificationChannel = new NotificationChannel(str, str2, 4);
            setLedPreference(notificationChannel, SignalStore.settings().getMessageLedColor());
            notificationChannel.setGroup("messages");
            setVibrationEnabled(notificationChannel, z);
            if (uri != null) {
                notificationChannel.setSound(uri, new AudioAttributes.Builder().setContentType(0).setUsage(8).build());
            }
            if (Build.VERSION.SDK_INT >= 30 && str3 != null) {
                notificationChannel.setConversationId(getMessagesChannel(context), str3);
            }
            ServiceUtil.getNotificationManager(context).createNotificationChannel(notificationChannel);
            return str;
        }
    }

    public static synchronized void deleteChannelFor(Context context, Recipient recipient) {
        synchronized (NotificationChannels.class) {
            if (supported()) {
                NotificationManager notificationManager = ServiceUtil.getNotificationManager(context);
                String notificationChannel = recipient.getNotificationChannel();
                if (notificationChannel != null) {
                    Log.i(TAG, "Deleting channel");
                    notificationManager.deleteNotificationChannel(notificationChannel);
                }
            }
        }
    }

    public static void openChannelSettings(Context context, String str, String str2) {
        if (supported()) {
            try {
                Intent intent = new Intent("android.settings.CHANNEL_NOTIFICATION_SETTINGS");
                intent.putExtra("android.provider.extra.CHANNEL_ID", str);
                intent.putExtra("android.provider.extra.APP_PACKAGE", context.getPackageName());
                if (str2 != null && Build.VERSION.SDK_INT >= 30) {
                    intent.putExtra("android.provider.extra.CONVERSATION_ID", str2);
                }
                context.startActivity(intent);
            } catch (ActivityNotFoundException e) {
                Log.w(TAG, "Channel settings activity not found", e);
                Toast.makeText(context, (int) R.string.NotificationChannels__no_activity_available_to_open_notification_channel_settings, 0).show();
            }
        }
    }

    public static synchronized void updateMessagesLedColor(Context context, String str) {
        synchronized (NotificationChannels.class) {
            if (supported()) {
                Log.i(TAG, "Updating LED color.");
                NotificationManager notificationManager = ServiceUtil.getNotificationManager(context);
                updateMessageChannel(context, new ChannelUpdater(str) { // from class: org.thoughtcrime.securesms.notifications.NotificationChannels$$ExternalSyntheticLambda0
                    public final /* synthetic */ String f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // org.thoughtcrime.securesms.notifications.NotificationChannels.ChannelUpdater
                    public final void update(NotificationChannel notificationChannel) {
                        NotificationChannels.setLedPreference(notificationChannel, this.f$0);
                    }
                });
                updateAllRecipientChannelLedColors(context, notificationManager, str);
                ensureCustomChannelConsistency(context);
            }
        }
    }

    public static synchronized Uri getMessageRingtone(Context context) {
        synchronized (NotificationChannels.class) {
            if (!supported()) {
                return Uri.EMPTY;
            }
            Uri sound = ServiceUtil.getNotificationManager(context).getNotificationChannel(getMessagesChannel(context)).getSound();
            if (sound == null) {
                sound = Uri.EMPTY;
            }
            return sound;
        }
    }

    public static synchronized Uri getMessageRingtone(Context context, Recipient recipient) {
        synchronized (NotificationChannels.class) {
            if (supported() && recipient.resolve().getNotificationChannel() != null) {
                NotificationChannel notificationChannel = ServiceUtil.getNotificationManager(context).getNotificationChannel(recipient.getNotificationChannel());
                if (!channelExists(notificationChannel)) {
                    Log.w(TAG, "Recipient had no channel. Returning null.");
                    return null;
                }
                Uri sound = notificationChannel.getSound();
                if (sound == null) {
                    sound = Uri.EMPTY;
                }
                return sound;
            }
            return null;
        }
    }

    public static synchronized void updateMessageRingtone(Context context, Uri uri) {
        synchronized (NotificationChannels.class) {
            if (supported()) {
                String str = TAG;
                Log.i(str, "Updating default message ringtone with URI: " + String.valueOf(uri));
                updateMessageChannel(context, new ChannelUpdater(uri) { // from class: org.thoughtcrime.securesms.notifications.NotificationChannels$$ExternalSyntheticLambda5
                    public final /* synthetic */ Uri f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // org.thoughtcrime.securesms.notifications.NotificationChannels.ChannelUpdater
                    public final void update(NotificationChannel notificationChannel) {
                        NotificationChannels.lambda$updateMessageRingtone$2(this.f$0, notificationChannel);
                    }
                });
            }
        }
    }

    public static /* synthetic */ void lambda$updateMessageRingtone$2(Uri uri, NotificationChannel notificationChannel) {
        if (uri == null) {
            uri = Settings.System.DEFAULT_NOTIFICATION_URI;
        }
        notificationChannel.setSound(uri, getRingtoneAudioAttributes());
    }

    public static synchronized void updateMessageRingtone(Context context, Recipient recipient, Uri uri) {
        synchronized (NotificationChannels.class) {
            if (supported() && recipient.getNotificationChannel() != null) {
                String str = TAG;
                Log.i(str, "Updating recipient message ringtone with URI: " + String.valueOf(uri));
                String generateChannelIdFor = generateChannelIdFor(recipient);
                boolean updateExistingChannel = updateExistingChannel(ServiceUtil.getNotificationManager(context), recipient.getNotificationChannel(), generateChannelIdFor(recipient), new ChannelUpdater(uri) { // from class: org.thoughtcrime.securesms.notifications.NotificationChannels$$ExternalSyntheticLambda8
                    public final /* synthetic */ Uri f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // org.thoughtcrime.securesms.notifications.NotificationChannels.ChannelUpdater
                    public final void update(NotificationChannel notificationChannel) {
                        NotificationChannels.lambda$updateMessageRingtone$3(this.f$0, notificationChannel);
                    }
                });
                RecipientDatabase recipients = SignalDatabase.recipients();
                RecipientId id = recipient.getId();
                if (!updateExistingChannel) {
                    generateChannelIdFor = null;
                }
                recipients.setNotificationChannel(id, generateChannelIdFor);
                ensureCustomChannelConsistency(context);
            }
        }
    }

    public static /* synthetic */ void lambda$updateMessageRingtone$3(Uri uri, NotificationChannel notificationChannel) {
        if (uri == null) {
            uri = Settings.System.DEFAULT_NOTIFICATION_URI;
        }
        notificationChannel.setSound(uri, getRingtoneAudioAttributes());
    }

    public static synchronized boolean getMessageVibrate(Context context) {
        synchronized (NotificationChannels.class) {
            if (!supported()) {
                return false;
            }
            return ServiceUtil.getNotificationManager(context).getNotificationChannel(getMessagesChannel(context)).shouldVibrate();
        }
    }

    public static synchronized boolean getMessageVibrate(Context context, Recipient recipient) {
        synchronized (NotificationChannels.class) {
            if (!supported()) {
                return getMessageVibrate(context);
            }
            NotificationChannel notificationChannel = ServiceUtil.getNotificationManager(context).getNotificationChannel(recipient.getNotificationChannel());
            if (!channelExists(notificationChannel)) {
                Log.w(TAG, "Recipient didn't have a channel. Returning message default.");
                return getMessageVibrate(context);
            }
            return notificationChannel.shouldVibrate() && !Arrays.equals(notificationChannel.getVibrationPattern(), EMPTY_VIBRATION_PATTERN);
        }
    }

    public static synchronized void updateMessageVibrate(Context context, boolean z) {
        synchronized (NotificationChannels.class) {
            if (supported()) {
                String str = TAG;
                Log.i(str, "Updating default vibrate with value: " + z);
                updateMessageChannel(context, new ChannelUpdater(z) { // from class: org.thoughtcrime.securesms.notifications.NotificationChannels$$ExternalSyntheticLambda7
                    public final /* synthetic */ boolean f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // org.thoughtcrime.securesms.notifications.NotificationChannels.ChannelUpdater
                    public final void update(NotificationChannel notificationChannel) {
                        NotificationChannels.setVibrationEnabled(notificationChannel, this.f$0);
                    }
                });
            }
        }
    }

    public static synchronized void updateMessageVibrate(Context context, Recipient recipient, RecipientDatabase.VibrateState vibrateState) {
        synchronized (NotificationChannels.class) {
            if (supported() && recipient.getNotificationChannel() != null) {
                String str = TAG;
                Log.i(str, "Updating recipient vibrate with value: " + vibrateState);
                boolean messageVibrate = vibrateState == RecipientDatabase.VibrateState.DEFAULT ? getMessageVibrate(context) : vibrateState == RecipientDatabase.VibrateState.ENABLED;
                String generateChannelIdFor = generateChannelIdFor(recipient);
                boolean updateExistingChannel = updateExistingChannel(ServiceUtil.getNotificationManager(context), recipient.getNotificationChannel(), generateChannelIdFor, new ChannelUpdater(messageVibrate) { // from class: org.thoughtcrime.securesms.notifications.NotificationChannels$$ExternalSyntheticLambda2
                    public final /* synthetic */ boolean f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // org.thoughtcrime.securesms.notifications.NotificationChannels.ChannelUpdater
                    public final void update(NotificationChannel notificationChannel) {
                        NotificationChannels.setVibrationEnabled(notificationChannel, this.f$0);
                    }
                });
                RecipientDatabase recipients = SignalDatabase.recipients();
                RecipientId id = recipient.getId();
                if (!updateExistingChannel) {
                    generateChannelIdFor = null;
                }
                recipients.setNotificationChannel(id, generateChannelIdFor);
                ensureCustomChannelConsistency(context);
            }
        }
    }

    public static void setVibrationEnabled(NotificationChannel notificationChannel, boolean z) {
        if (z) {
            notificationChannel.setVibrationPattern(null);
            notificationChannel.enableVibration(true);
            return;
        }
        notificationChannel.setVibrationPattern(EMPTY_VIBRATION_PATTERN);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        if (r3.getImportance() != 0) goto L_0x0022;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean isMessageChannelEnabled(android.content.Context r3) {
        /*
            java.lang.Class<org.thoughtcrime.securesms.notifications.NotificationChannels> r0 = org.thoughtcrime.securesms.notifications.NotificationChannels.class
            monitor-enter(r0)
            boolean r1 = supported()     // Catch: all -> 0x0024
            r2 = 1
            if (r1 != 0) goto L_0x000c
            monitor-exit(r0)
            return r2
        L_0x000c:
            android.app.NotificationManager r1 = org.thoughtcrime.securesms.util.ServiceUtil.getNotificationManager(r3)     // Catch: all -> 0x0024
            java.lang.String r3 = getMessagesChannel(r3)     // Catch: all -> 0x0024
            android.app.NotificationChannel r3 = r1.getNotificationChannel(r3)     // Catch: all -> 0x0024
            if (r3 == 0) goto L_0x0021
            int r3 = r3.getImportance()     // Catch: all -> 0x0024
            if (r3 == 0) goto L_0x0021
            goto L_0x0022
        L_0x0021:
            r2 = 0
        L_0x0022:
            monitor-exit(r0)
            return r2
        L_0x0024:
            r3 = move-exception
            monitor-exit(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.notifications.NotificationChannels.isMessageChannelEnabled(android.content.Context):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001d, code lost:
        if (r4.isBlocked() == false) goto L_0x0021;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean isMessagesChannelGroupEnabled(android.content.Context r4) {
        /*
            java.lang.Class<org.thoughtcrime.securesms.notifications.NotificationChannels> r0 = org.thoughtcrime.securesms.notifications.NotificationChannels.class
            monitor-enter(r0)
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch: all -> 0x0023
            r2 = 28
            r3 = 1
            if (r1 >= r2) goto L_0x000c
            monitor-exit(r0)
            return r3
        L_0x000c:
            android.app.NotificationManager r4 = org.thoughtcrime.securesms.util.ServiceUtil.getNotificationManager(r4)     // Catch: all -> 0x0023
            java.lang.String r1 = "messages"
            android.app.NotificationChannelGroup r4 = r4.getNotificationChannelGroup(r1)     // Catch: all -> 0x0023
            if (r4 == 0) goto L_0x0020
            boolean r4 = r4.isBlocked()     // Catch: all -> 0x0023
            if (r4 != 0) goto L_0x0020
            goto L_0x0021
        L_0x0020:
            r3 = 0
        L_0x0021:
            monitor-exit(r0)
            return r3
        L_0x0023:
            r4 = move-exception
            monitor-exit(r0)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.notifications.NotificationChannels.isMessagesChannelGroupEnabled(android.content.Context):boolean");
    }

    public static boolean isCallsChannelValid(Context context) {
        if (!supported()) {
            return true;
        }
        NotificationChannel notificationChannel = ServiceUtil.getNotificationManager(context).getNotificationChannel(CALLS);
        if (notificationChannel == null || notificationChannel.getImportance() != 4) {
            return false;
        }
        return true;
    }

    public static synchronized boolean areNotificationsEnabled(Context context) {
        synchronized (NotificationChannels.class) {
            if (Build.VERSION.SDK_INT < 24) {
                return true;
            }
            return ServiceUtil.getNotificationManager(context).areNotificationsEnabled();
        }
    }

    public static boolean updateWithShortcutBasedChannel(Context context, Recipient recipient) {
        if (Build.VERSION.SDK_INT < 30 || !TextUtils.isEmpty(recipient.getNotificationChannel())) {
            return false;
        }
        Optional findFirst = Collection$EL.stream(ServiceUtil.getNotificationManager(context).getNotificationChannels()).filter(new Predicate(ConversationUtil.getShortcutId(recipient)) { // from class: org.thoughtcrime.securesms.notifications.NotificationChannels$$ExternalSyntheticLambda3
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return NotificationChannels.lambda$updateWithShortcutBasedChannel$6(this.f$0, (NotificationChannel) obj);
            }
        }).findFirst();
        if (!findFirst.isPresent()) {
            return false;
        }
        String str = TAG;
        Log.i(str, "Conversation channel created outside of app, while running. Update " + recipient.getId() + " to use '" + ((NotificationChannel) findFirst.get()).getId() + "'");
        SignalDatabase.recipients().setNotificationChannel(recipient.getId(), ((NotificationChannel) findFirst.get()).getId());
        return true;
    }

    public static /* synthetic */ boolean lambda$updateWithShortcutBasedChannel$6(String str, NotificationChannel notificationChannel) {
        return Objects.equals(str, notificationChannel.getConversationId());
    }

    public static synchronized void updateContactChannelName(Context context, Recipient recipient) {
        synchronized (NotificationChannels.class) {
            if (supported() && recipient.getNotificationChannel() != null) {
                String str = TAG;
                Log.i(str, "Updating contact channel name");
                NotificationManager notificationManager = ServiceUtil.getNotificationManager(context);
                if (notificationManager.getNotificationChannel(recipient.getNotificationChannel()) == null) {
                    Log.w(str, "Tried to update the name of a channel, but that channel doesn't exist.");
                    return;
                }
                NotificationChannel notificationChannel = new NotificationChannel(recipient.getNotificationChannel(), recipient.getDisplayName(context), 4);
                notificationChannel.setGroup("messages");
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }
    }

    public static synchronized void ensureCustomChannelConsistency(Context context) {
        synchronized (NotificationChannels.class) {
            if (supported()) {
                Log.d(TAG, "ensureCustomChannelConsistency()");
                NotificationManager notificationManager = ServiceUtil.getNotificationManager(context);
                RecipientDatabase recipients = SignalDatabase.recipients();
                ArrayList<Recipient> arrayList = new ArrayList();
                HashSet hashSet = new HashSet();
                RecipientDatabase.RecipientReader recipientsWithNotificationChannels = recipients.getRecipientsWithNotificationChannels();
                while (true) {
                    Recipient next = recipientsWithNotificationChannels.getNext();
                    if (next == null) {
                        break;
                    }
                    arrayList.add(next);
                    hashSet.add(next.getNotificationChannel());
                }
                recipientsWithNotificationChannels.close();
                Set set = (Set) Stream.of(notificationManager.getNotificationChannels()).map(new Function() { // from class: org.thoughtcrime.securesms.notifications.NotificationChannels$$ExternalSyntheticLambda4
                    @Override // com.annimon.stream.function.Function
                    public final Object apply(Object obj) {
                        return ((NotificationChannel) obj).getId();
                    }
                }).collect(Collectors.toSet());
                for (NotificationChannel notificationChannel : notificationManager.getNotificationChannels()) {
                    if ((notificationChannel.getId().startsWith(CONTACT_PREFIX) || notificationChannel.getId().startsWith(MESSAGES_PREFIX)) && Build.VERSION.SDK_INT >= 30 && notificationChannel.getConversationId() != null) {
                        if (!hashSet.contains(notificationChannel.getId())) {
                            RecipientId recipientId = ConversationUtil.getRecipientId(notificationChannel.getConversationId());
                            if (recipientId != null) {
                                String str = TAG;
                                Log.i(str, "Consistency: Conversation channel created outside of app, update " + recipientId + " to use '" + notificationChannel.getId() + "'");
                                recipients.setNotificationChannel(recipientId, notificationChannel.getId());
                            } else {
                                String str2 = TAG;
                                Log.i(str2, "Consistency: Conversation channel created outside of app with no matching recipient, deleting channel '" + notificationChannel.getId() + "'");
                                notificationManager.deleteNotificationChannel(notificationChannel.getId());
                            }
                        }
                    } else if (notificationChannel.getId().startsWith(CONTACT_PREFIX) && !hashSet.contains(notificationChannel.getId())) {
                        String str3 = TAG;
                        Log.i(str3, "Consistency: Deleting channel '" + notificationChannel.getId() + "' because the DB has no record of it.");
                        notificationManager.deleteNotificationChannel(notificationChannel.getId());
                    } else if (notificationChannel.getId().startsWith(MESSAGES_PREFIX) && !notificationChannel.getId().equals(getMessagesChannel(context))) {
                        String str4 = TAG;
                        Log.i(str4, "Consistency: Deleting channel '" + notificationChannel.getId() + "' because it's out of date.");
                        notificationManager.deleteNotificationChannel(notificationChannel.getId());
                    }
                }
                for (Recipient recipient : arrayList) {
                    if (!set.contains(recipient.getNotificationChannel())) {
                        String str5 = TAG;
                        Log.i(str5, "Consistency: Removing custom channel '" + recipient.getNotificationChannel() + "' because the system doesn't have it.");
                        recipients.setNotificationChannel(recipient.getId(), null);
                    }
                }
            }
        }
    }

    private static void onCreate(Context context, NotificationManager notificationManager) {
        notificationManager.createNotificationChannelGroup(new NotificationChannelGroup("messages", context.getResources().getString(R.string.NotificationChannel_group_chats)));
        NotificationChannel notificationChannel = new NotificationChannel(getMessagesChannel(context), context.getString(R.string.NotificationChannel_channel_messages), 4);
        NotificationChannel notificationChannel2 = new NotificationChannel(CALLS, context.getString(R.string.NotificationChannel_calls), 4);
        NotificationChannel notificationChannel3 = new NotificationChannel(FAILURES, context.getString(R.string.NotificationChannel_failures), 4);
        NotificationChannel notificationChannel4 = new NotificationChannel(BACKUPS, context.getString(R.string.NotificationChannel_backups), 2);
        NotificationChannel notificationChannel5 = new NotificationChannel(LOCKED_STATUS, context.getString(R.string.NotificationChannel_locked_status), 2);
        NotificationChannel notificationChannel6 = new NotificationChannel(OTHER, context.getString(R.string.NotificationChannel_other), 2);
        NotificationChannel notificationChannel7 = new NotificationChannel(VOICE_NOTES, context.getString(R.string.NotificationChannel_voice_notes), 2);
        NotificationChannel notificationChannel8 = new NotificationChannel(JOIN_EVENTS, context.getString(R.string.NotificationChannel_contact_joined_signal), 3);
        NotificationChannel notificationChannel9 = new NotificationChannel(BACKGROUND, context.getString(R.string.NotificationChannel_background_connection), getDefaultBackgroundChannelImportance(notificationManager));
        NotificationChannel notificationChannel10 = new NotificationChannel(CALL_STATUS, context.getString(R.string.NotificationChannel_call_status), 2);
        notificationChannel.setGroup("messages");
        setVibrationEnabled(notificationChannel, SignalStore.settings().isMessageVibrateEnabled());
        notificationChannel.setSound(SignalStore.settings().getMessageNotificationSound(), getRingtoneAudioAttributes());
        setLedPreference(notificationChannel, SignalStore.settings().getMessageLedColor());
        notificationChannel2.setShowBadge(false);
        notificationChannel4.setShowBadge(false);
        notificationChannel5.setShowBadge(false);
        notificationChannel6.setShowBadge(false);
        setVibrationEnabled(notificationChannel6, false);
        notificationChannel7.setShowBadge(false);
        notificationChannel8.setShowBadge(false);
        notificationChannel9.setShowBadge(false);
        notificationChannel10.setShowBadge(false);
        notificationManager.createNotificationChannels(Arrays.asList(notificationChannel, notificationChannel2, notificationChannel3, notificationChannel4, notificationChannel5, notificationChannel6, notificationChannel7, notificationChannel8, notificationChannel9, notificationChannel10));
        notificationManager.createNotificationChannel(new NotificationChannel(APP_UPDATES, context.getString(R.string.NotificationChannel_app_updates), 4));
    }

    private static int getDefaultBackgroundChannelImportance(NotificationManager notificationManager) {
        NotificationChannel notificationChannel = notificationManager.getNotificationChannel(OTHER);
        if (notificationChannel == null || notificationChannel.getImportance() == 2) {
            return 2;
        }
        return notificationChannel.getImportance();
    }

    private static void onUpgrade(NotificationManager notificationManager, int i, int i2) {
        String str = TAG;
        Log.i(str, "Upgrading channels from " + i + " to " + i2);
        if (i < 2) {
            notificationManager.deleteNotificationChannel("messages");
            notificationManager.deleteNotificationChannel("calls");
            notificationManager.deleteNotificationChannel("locked_status");
            notificationManager.deleteNotificationChannel("backups");
            notificationManager.deleteNotificationChannel("other");
        }
        if (i < 3) {
            notificationManager.deleteNotificationChannel("calls_v2");
        }
        if (i < 4) {
            notificationManager.deleteNotificationChannel("other_v2");
        }
    }

    public static void setLedPreference(NotificationChannel notificationChannel, String str) {
        if ("none".equals(str)) {
            notificationChannel.enableLights(false);
            return;
        }
        notificationChannel.enableLights(true);
        notificationChannel.setLightColor(Color.parseColor(str));
    }

    private static String generateChannelIdFor(Recipient recipient) {
        return CONTACT_PREFIX + recipient.getId().serialize() + "_" + System.currentTimeMillis();
    }

    private static NotificationChannel copyChannel(NotificationChannel notificationChannel, String str) {
        NotificationChannel notificationChannel2 = new NotificationChannel(str, notificationChannel.getName(), notificationChannel.getImportance());
        notificationChannel2.setGroup(notificationChannel.getGroup());
        notificationChannel2.setSound(notificationChannel.getSound(), notificationChannel.getAudioAttributes());
        notificationChannel2.setBypassDnd(notificationChannel.canBypassDnd());
        notificationChannel2.setVibrationPattern(notificationChannel.getVibrationPattern());
        notificationChannel2.enableVibration(notificationChannel.shouldVibrate());
        notificationChannel2.setLockscreenVisibility(notificationChannel.getLockscreenVisibility());
        notificationChannel2.setShowBadge(notificationChannel.canShowBadge());
        notificationChannel2.setLightColor(notificationChannel.getLightColor());
        notificationChannel2.enableLights(notificationChannel.shouldShowLights());
        if (Build.VERSION.SDK_INT >= 30 && notificationChannel.getConversationId() != null) {
            notificationChannel2.setConversationId(notificationChannel.getParentChannelId(), notificationChannel.getConversationId());
        }
        return notificationChannel2;
    }

    private static String getMessagesChannelId(int i) {
        return MESSAGES_PREFIX + i;
    }

    private static void updateAllRecipientChannelLedColors(Context context, NotificationManager notificationManager, String str) {
        RecipientDatabase recipients = SignalDatabase.recipients();
        RecipientDatabase.RecipientReader recipientsWithNotificationChannels = recipients.getRecipientsWithNotificationChannels();
        while (true) {
            try {
                Recipient next = recipientsWithNotificationChannels.getNext();
                if (next != null) {
                    String generateChannelIdFor = generateChannelIdFor(next);
                    boolean updateExistingChannel = updateExistingChannel(notificationManager, next.getNotificationChannel(), generateChannelIdFor, new ChannelUpdater(str) { // from class: org.thoughtcrime.securesms.notifications.NotificationChannels$$ExternalSyntheticLambda6
                        public final /* synthetic */ String f$0;

                        {
                            this.f$0 = r1;
                        }

                        @Override // org.thoughtcrime.securesms.notifications.NotificationChannels.ChannelUpdater
                        public final void update(NotificationChannel notificationChannel) {
                            NotificationChannels.setLedPreference(notificationChannel, this.f$0);
                        }
                    });
                    RecipientId id = next.getId();
                    if (!updateExistingChannel) {
                        generateChannelIdFor = null;
                    }
                    recipients.setNotificationChannel(id, generateChannelIdFor);
                } else {
                    recipientsWithNotificationChannels.close();
                    ensureCustomChannelConsistency(context);
                    return;
                }
            } catch (Throwable th) {
                if (recipientsWithNotificationChannels != null) {
                    try {
                        recipientsWithNotificationChannels.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
    }

    private static void updateMessageChannel(Context context, ChannelUpdater channelUpdater) {
        NotificationManager notificationManager = ServiceUtil.getNotificationManager(context);
        int notificationMessagesChannelVersion = TextSecurePreferences.getNotificationMessagesChannelVersion(context);
        int i = notificationMessagesChannelVersion + 1;
        String str = TAG;
        Log.i(str, "Updating message channel from version " + notificationMessagesChannelVersion + " to " + i);
        if (updateExistingChannel(notificationManager, getMessagesChannelId(notificationMessagesChannelVersion), getMessagesChannelId(i), channelUpdater)) {
            TextSecurePreferences.setNotificationMessagesChannelVersion(context, i);
        } else {
            onCreate(context, notificationManager);
        }
    }

    private static boolean updateExistingChannel(NotificationManager notificationManager, String str, String str2, ChannelUpdater channelUpdater) {
        NotificationChannel notificationChannel = notificationManager.getNotificationChannel(str);
        if (notificationChannel == null) {
            Log.w(TAG, "Tried to update a channel, but it didn't exist.");
            return false;
        }
        notificationManager.deleteNotificationChannel(notificationChannel.getId());
        NotificationChannel copyChannel = copyChannel(notificationChannel, str2);
        channelUpdater.update(copyChannel);
        notificationManager.createNotificationChannel(copyChannel);
        return true;
    }

    private static AudioAttributes getRingtoneAudioAttributes() {
        return new AudioAttributes.Builder().setContentType(0).setUsage(8).build();
    }

    private static boolean channelExists(NotificationChannel notificationChannel) {
        return notificationChannel != null && !"miscellaneous".equals(notificationChannel.getId());
    }
}
