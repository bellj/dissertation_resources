package org.thoughtcrime.securesms.notifications.v2;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import androidx.core.app.NotificationCompat;
import androidx.core.app.Person;
import androidx.core.app.RemoteInput;
import androidx.core.content.LocusIdCompat;
import androidx.core.graphics.drawable.IconCompat;
import j$.util.Optional;
import java.util.List;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsKt;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.ConversationIntents;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.notifications.ReplyMethod;
import org.thoughtcrime.securesms.notifications.v2.NotificationItemV2;
import org.thoughtcrime.securesms.preferences.widgets.NotificationPrivacyPreference;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.service.KeyCachingService;
import org.thoughtcrime.securesms.util.AvatarUtil;
import org.thoughtcrime.securesms.util.BubbleUtil;
import org.thoughtcrime.securesms.util.ConversationUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* compiled from: NotificationBuilder.kt */
@Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\r\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u001a\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 c2\u00020\u0001:\u0002cdB\u000f\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H$J\u000e\u0010\u0011\u001a\u00020\f2\u0006\u0010\u0012\u001a\u00020\u0013J\u0010\u0010\u0014\u001a\u00020\f2\u0006\u0010\u0012\u001a\u00020\u0013H&J\u000e\u0010\u0015\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u0010J\u000e\u0010\u0015\u001a\u00020\f2\u0006\u0010\u0012\u001a\u00020\u0013J\u0018\u0010\u0016\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0017\u001a\u00020\bH$J\u0010\u0010\u0016\u001a\u00020\f2\u0006\u0010\u0012\u001a\u00020\u0013H$J\u000e\u0010\u0018\u001a\u00020\f2\u0006\u0010\u0019\u001a\u00020\u001aJ\u0010\u0010\u001b\u001a\u00020\f2\u0006\u0010\u0019\u001a\u00020\u001aH$J\u000e\u0010\u001c\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u0010J\u0010\u0010\u001d\u001a\u00020\f2\u0006\u0010\u001e\u001a\u00020\u001fH&J\b\u0010 \u001a\u00020!H&J\u0012\u0010\"\u001a\u00020\f2\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH&J\u0010\u0010#\u001a\u00020\f2\u0006\u0010$\u001a\u00020\bH&J\u0016\u0010%\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010&\u001a\u00020'J\u0018\u0010(\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010&\u001a\u00020'H$J\u0010\u0010)\u001a\u00020\f2\u0006\u0010*\u001a\u00020+H&J\u0010\u0010,\u001a\u00020\f2\u0006\u0010-\u001a\u00020+H&J\u0012\u0010.\u001a\u00020\f2\b\b\u0001\u0010/\u001a\u000200H&J\u0010\u00101\u001a\u00020\f2\u0006\u00102\u001a\u00020+H&J\u0012\u00103\u001a\u00020\f2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH&J\u0012\u00104\u001a\u00020\f2\b\u00105\u001a\u0004\u0018\u000106H&J\u0010\u00107\u001a\u00020\f2\u0006\u00108\u001a\u000206H&J\u0012\u00109\u001a\u00020\f2\b\u0010:\u001a\u0004\u0018\u00010\u001fH&J\u0010\u0010;\u001a\u00020\f2\u0006\u0010<\u001a\u00020+H&J\u0010\u0010=\u001a\u00020\f2\u0006\u0010>\u001a\u000200H&J\u0010\u0010?\u001a\u00020\f2\u0006\u0010@\u001a\u00020\bH&J\u0012\u0010A\u001a\u00020\f2\b\u0010B\u001a\u0004\u0018\u00010CH&J\u0006\u0010D\u001a\u00020\fJ\"\u0010D\u001a\u00020\f2\b\b\u0001\u0010/\u001a\u0002002\u0006\u0010E\u001a\u0002002\u0006\u0010F\u001a\u000200H$J\u000e\u0010G\u001a\u00020\f2\u0006\u0010H\u001a\u00020+J\u0010\u0010I\u001a\u00020\f2\u0006\u0010H\u001a\u00020+H&J\u0010\u0010J\u001a\u00020\f2\u0006\u0010K\u001a\u000200H&J\u0010\u0010L\u001a\u00020\f2\u0006\u0010M\u001a\u00020\bH&J\u0010\u0010N\u001a\u00020\f2\u0006\u0010O\u001a\u000200H&J\u000e\u0010P\u001a\u00020\f2\u0006\u0010Q\u001a\u00020+J\u0010\u0010R\u001a\u00020\f2\u0006\u0010Q\u001a\u00020+H$J\u0012\u0010S\u001a\u00020\f2\b\b\u0001\u0010T\u001a\u000200H&J\u0010\u0010U\u001a\u00020\f2\u0006\u0010V\u001a\u00020+H&J\u0010\u0010W\u001a\u00020\f2\u0006\u0010X\u001a\u00020+H&J\u0010\u0010Y\u001a\u00020\f2\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aJ\u0012\u0010Z\u001a\u00020\f2\b\u0010[\u001a\u0004\u0018\u000106H&J\u0010\u0010\\\u001a\u00020\f2\u0006\u0010]\u001a\u00020^H$J\u000e\u0010\\\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u0010J\u0010\u0010\\\u001a\u00020\f2\b\u0010_\u001a\u0004\u0018\u00010`J\u0018\u0010a\u001a\u000e\u0012\u0004\u0012\u000200\u0012\u0004\u0012\u0002000b*\u00020+H\u0002R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000\u0001\u0001e¨\u0006f"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/NotificationBuilder;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "getContext", "()Landroid/content/Context;", "isNotLocked", "", "privacy", "Lorg/thoughtcrime/securesms/preferences/widgets/NotificationPrivacyPreference;", "addActions", "", "replyMethod", "Lorg/thoughtcrime/securesms/notifications/ReplyMethod;", "conversation", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationConversation;", "addMarkAsReadAction", "state", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationStateV2;", "addMarkAsReadActionActual", "addMessages", "addMessagesActual", "includeShortcut", "addPerson", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "addPersonActual", "addReplyActions", "addTurnOffJoinedNotificationsAction", "pendingIntent", "Landroid/app/PendingIntent;", "build", "Landroid/app/Notification;", "setAlarms", "setAutoCancel", "autoCancel", "setBubbleMetadata", "bubbleState", "Lorg/thoughtcrime/securesms/util/BubbleUtil$BubbleState;", "setBubbleMetadataActual", "setCategory", "category", "", "setChannelId", "channelId", "setColor", NotificationProfileDatabase.NotificationProfileTable.COLOR, "", "setContentInfo", "contentInfo", "setContentIntent", "setContentText", "contentText", "", "setContentTitle", "contentTitle", "setDeleteIntent", "deleteIntent", "setGroup", "group", "setGroupAlertBehavior", "behavior", "setGroupSummary", "isGroupSummary", "setLargeIcon", "largeIcon", "Landroid/graphics/Bitmap;", "setLights", "onTime", "offTime", "setLocusId", "locusId", "setLocusIdActual", "setNumber", "number", "setOnlyAlertOnce", "onlyAlertOnce", "setPriority", "priority", "setShortcutId", "shortcutId", "setShortcutIdActual", "setSmallIcon", "drawable", "setSortKey", "sortKey", "setSubText", "subText", "setSummaryContentText", "setTicker", "ticker", "setWhen", "timestamp", "", "notificationItem", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationItemV2;", "parseBlinkPattern", "Lkotlin/Pair;", "Companion", "NotificationBuilderCompat", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationBuilder$NotificationBuilderCompat;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class NotificationBuilder {
    public static final Companion Companion = new Companion(null);
    private final Context context;
    private final boolean isNotLocked;
    private final NotificationPrivacyPreference privacy;

    public /* synthetic */ NotificationBuilder(Context context, DefaultConstructorMarker defaultConstructorMarker) {
        this(context);
    }

    protected abstract void addActions(ReplyMethod replyMethod, NotificationConversation notificationConversation);

    public abstract void addMarkAsReadActionActual(NotificationStateV2 notificationStateV2);

    protected abstract void addMessagesActual(NotificationConversation notificationConversation, boolean z);

    protected abstract void addMessagesActual(NotificationStateV2 notificationStateV2);

    protected abstract void addPersonActual(Recipient recipient);

    public abstract void addTurnOffJoinedNotificationsAction(PendingIntent pendingIntent);

    public abstract Notification build();

    public abstract void setAlarms(Recipient recipient);

    public abstract void setAutoCancel(boolean z);

    protected abstract void setBubbleMetadataActual(NotificationConversation notificationConversation, BubbleUtil.BubbleState bubbleState);

    public abstract void setCategory(String str);

    public abstract void setChannelId(String str);

    public abstract void setColor(int i);

    public abstract void setContentInfo(String str);

    public abstract void setContentIntent(PendingIntent pendingIntent);

    public abstract void setContentText(CharSequence charSequence);

    public abstract void setContentTitle(CharSequence charSequence);

    public abstract void setDeleteIntent(PendingIntent pendingIntent);

    public abstract void setGroup(String str);

    public abstract void setGroupAlertBehavior(int i);

    public abstract void setGroupSummary(boolean z);

    public abstract void setLargeIcon(Bitmap bitmap);

    protected abstract void setLights(int i, int i2, int i3);

    public abstract void setLocusIdActual(String str);

    public abstract void setNumber(int i);

    public abstract void setOnlyAlertOnce(boolean z);

    public abstract void setPriority(int i);

    protected abstract void setShortcutIdActual(String str);

    public abstract void setSmallIcon(int i);

    public abstract void setSortKey(String str);

    public abstract void setSubText(String str);

    public abstract void setTicker(CharSequence charSequence);

    protected abstract void setWhen(long j);

    private NotificationBuilder(Context context) {
        this.context = context;
        NotificationPrivacyPreference messageNotificationsPrivacy = SignalStore.settings().getMessageNotificationsPrivacy();
        Intrinsics.checkNotNullExpressionValue(messageNotificationsPrivacy, "settings().messageNotificationsPrivacy");
        this.privacy = messageNotificationsPrivacy;
        this.isNotLocked = !KeyCachingService.isLocked(context);
    }

    protected final Context getContext() {
        return this.context;
    }

    public final void addPerson(Recipient recipient) {
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        if (this.privacy.isDisplayContact()) {
            addPersonActual(recipient);
        }
    }

    public final void setLocusId(String str) {
        Intrinsics.checkNotNullParameter(str, "locusId");
        if (this.privacy.isDisplayContact() && this.isNotLocked) {
            setLocusIdActual(str);
        }
    }

    public final void setShortcutId(String str) {
        Intrinsics.checkNotNullParameter(str, "shortcutId");
        if (this.privacy.isDisplayContact() && this.isNotLocked) {
            setShortcutIdActual(str);
        }
    }

    public final void setWhen(NotificationConversation notificationConversation) {
        Intrinsics.checkNotNullParameter(notificationConversation, "conversation");
        if (notificationConversation.getWhen() != 0) {
            setWhen(notificationConversation.getWhen());
        }
    }

    public final void setWhen(NotificationItemV2 notificationItemV2) {
        if (notificationItemV2 != null && notificationItemV2.getTimestamp() != 0) {
            setWhen(notificationItemV2.getTimestamp());
        }
    }

    public final void addReplyActions(NotificationConversation notificationConversation) {
        Intrinsics.checkNotNullParameter(notificationConversation, "conversation");
        if (this.privacy.isDisplayMessage() && this.isNotLocked && !notificationConversation.getRecipient().isPushV1Group() && RecipientUtil.isMessageRequestAccepted(this.context, notificationConversation.getRecipient())) {
            if (notificationConversation.getRecipient().isPushV2Group()) {
                Optional<GroupDatabase.GroupRecord> group = SignalDatabase.Companion.groups().getGroup(notificationConversation.getRecipient().requireGroupId());
                Intrinsics.checkNotNullExpressionValue(group, "SignalDatabase.groups.ge…cipient.requireGroupId())");
                if (group.isPresent() && group.get().isAnnouncementGroup() && !group.get().isAdmin(Recipient.self())) {
                    return;
                }
            }
            ReplyMethod forRecipient = ReplyMethod.forRecipient(this.context, notificationConversation.getRecipient());
            Intrinsics.checkNotNullExpressionValue(forRecipient, "forRecipient(context, conversation.recipient)");
            addActions(forRecipient, notificationConversation);
        }
    }

    public final void addMarkAsReadAction(NotificationStateV2 notificationStateV2) {
        Intrinsics.checkNotNullParameter(notificationStateV2, "state");
        if (this.privacy.isDisplayMessage() && this.isNotLocked) {
            addMarkAsReadActionActual(notificationStateV2);
        }
    }

    public final void addMessages(NotificationConversation notificationConversation) {
        Intrinsics.checkNotNullParameter(notificationConversation, "conversation");
        addMessagesActual(notificationConversation, this.privacy.isDisplayContact());
    }

    public final void addMessages(NotificationStateV2 notificationStateV2) {
        Intrinsics.checkNotNullParameter(notificationStateV2, "state");
        if (!this.privacy.isDisplayNothing()) {
            addMessagesActual(notificationStateV2);
        }
    }

    public final void setBubbleMetadata(NotificationConversation notificationConversation, BubbleUtil.BubbleState bubbleState) {
        Intrinsics.checkNotNullParameter(notificationConversation, "conversation");
        Intrinsics.checkNotNullParameter(bubbleState, "bubbleState");
        if (this.privacy.isDisplayContact() && this.isNotLocked) {
            setBubbleMetadataActual(notificationConversation, bubbleState);
        }
    }

    public final void setSummaryContentText(Recipient recipient) {
        String notificationChannel;
        if (this.privacy.isDisplayContact() && recipient != null) {
            Context context = this.context;
            setContentText(context.getString(R.string.MessageNotifier_most_recent_from_s, recipient.getDisplayName(context)));
        }
        if (recipient != null && (notificationChannel = recipient.getNotificationChannel()) != null) {
            setChannelId(notificationChannel);
        }
    }

    public final void setLights() {
        String messageLedColor = SignalStore.settings().getMessageLedColor();
        Intrinsics.checkNotNullExpressionValue(messageLedColor, "settings().messageLedColor");
        if (!Intrinsics.areEqual(messageLedColor, "none")) {
            String messageLedBlinkPattern = SignalStore.settings().getMessageLedBlinkPattern();
            Intrinsics.checkNotNullExpressionValue(messageLedBlinkPattern, "settings().messageLedBlinkPattern");
            if (Intrinsics.areEqual(messageLedBlinkPattern, "custom")) {
                messageLedBlinkPattern = TextSecurePreferences.getNotificationLedPatternCustom(this.context);
                Intrinsics.checkNotNullExpressionValue(messageLedBlinkPattern, "getNotificationLedPatternCustom(context)");
            }
            Pair<Integer, Integer> parseBlinkPattern = parseBlinkPattern(messageLedBlinkPattern);
            setLights(Color.parseColor(messageLedColor), parseBlinkPattern.component1().intValue(), parseBlinkPattern.component2().intValue());
        }
    }

    private final Pair<Integer, Integer> parseBlinkPattern(String str) {
        List list = StringsKt__StringsKt.split$default((CharSequence) str, new String[]{","}, false, 0, 6, (Object) null);
        return TuplesKt.to(Integer.valueOf(Integer.parseInt((String) list.get(0))), Integer.valueOf(Integer.parseInt((String) list.get(1))));
    }

    /* compiled from: NotificationBuilder.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/NotificationBuilder$Companion;", "", "()V", "create", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationBuilder;", "context", "Landroid/content/Context;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final NotificationBuilder create(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return new NotificationBuilderCompat(context);
        }
    }

    /* compiled from: NotificationBuilder.kt */
    @Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\r\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0010\t\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0014J\u0010\u0010\u000f\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\u0018\u0010\u0012\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u0014H\u0014J\u0010\u0010\u0012\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u0011H\u0014J\u0010\u0010\u0015\u001a\u00020\n2\u0006\u0010\u0016\u001a\u00020\u0017H\u0014J\u0010\u0010\u0018\u001a\u00020\n2\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\b\u0010\u001b\u001a\u00020\u001cH\u0016J\u0012\u0010\u001d\u001a\u00020\n2\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0016J\u0010\u0010\u001e\u001a\u00020\n2\u0006\u0010\u001f\u001a\u00020\u0014H\u0016J\u0018\u0010 \u001a\u00020\n2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010!\u001a\u00020\"H\u0014J\u0010\u0010#\u001a\u00020\n2\u0006\u0010$\u001a\u00020%H\u0016J\u0010\u0010&\u001a\u00020\n2\u0006\u0010'\u001a\u00020%H\u0016J\u0012\u0010(\u001a\u00020\n2\b\b\u0001\u0010)\u001a\u00020*H\u0016J\u0010\u0010+\u001a\u00020\n2\u0006\u0010,\u001a\u00020%H\u0016J\u0012\u0010-\u001a\u00020\n2\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J\u0012\u0010.\u001a\u00020\n2\b\u0010/\u001a\u0004\u0018\u000100H\u0016J\u0010\u00101\u001a\u00020\n2\u0006\u00102\u001a\u000200H\u0016J\u0012\u00103\u001a\u00020\n2\b\u00104\u001a\u0004\u0018\u00010\u001aH\u0016J\u0010\u00105\u001a\u00020\n2\u0006\u00106\u001a\u00020%H\u0016J\u0010\u00107\u001a\u00020\n2\u0006\u00108\u001a\u00020*H\u0016J\u0010\u00109\u001a\u00020\n2\u0006\u0010:\u001a\u00020\u0014H\u0016J\u0012\u0010;\u001a\u00020\n2\b\u0010<\u001a\u0004\u0018\u00010=H\u0016J\"\u0010>\u001a\u00020\n2\b\b\u0001\u0010)\u001a\u00020*2\u0006\u0010?\u001a\u00020*2\u0006\u0010@\u001a\u00020*H\u0014J\u0010\u0010A\u001a\u00020\n2\u0006\u0010B\u001a\u00020%H\u0016J\u0010\u0010C\u001a\u00020\n2\u0006\u0010D\u001a\u00020*H\u0016J\u0010\u0010E\u001a\u00020\n2\u0006\u0010F\u001a\u00020\u0014H\u0016J\u0010\u0010G\u001a\u00020\n2\u0006\u0010H\u001a\u00020*H\u0016J\u0010\u0010I\u001a\u00020\n2\u0006\u0010J\u001a\u00020%H\u0014J\u0010\u0010K\u001a\u00020\n2\u0006\u0010L\u001a\u00020*H\u0016J\u0010\u0010M\u001a\u00020\n2\u0006\u0010N\u001a\u00020%H\u0016J\u0010\u0010O\u001a\u00020\n2\u0006\u0010P\u001a\u00020%H\u0016J\u0012\u0010Q\u001a\u00020\n2\b\u0010R\u001a\u0004\u0018\u000100H\u0016J\u0010\u0010S\u001a\u00020\n2\u0006\u0010T\u001a\u00020UH\u0014R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b¨\u0006V"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/NotificationBuilder$NotificationBuilderCompat;", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationBuilder;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "builder", "Landroidx/core/app/NotificationCompat$Builder;", "getBuilder", "()Landroidx/core/app/NotificationCompat$Builder;", "addActions", "", "replyMethod", "Lorg/thoughtcrime/securesms/notifications/ReplyMethod;", "conversation", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationConversation;", "addMarkAsReadActionActual", "state", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationStateV2;", "addMessagesActual", "includeShortcut", "", "addPersonActual", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "addTurnOffJoinedNotificationsAction", "pendingIntent", "Landroid/app/PendingIntent;", "build", "Landroid/app/Notification;", "setAlarms", "setAutoCancel", "autoCancel", "setBubbleMetadataActual", "bubbleState", "Lorg/thoughtcrime/securesms/util/BubbleUtil$BubbleState;", "setCategory", "category", "", "setChannelId", "channelId", "setColor", NotificationProfileDatabase.NotificationProfileTable.COLOR, "", "setContentInfo", "contentInfo", "setContentIntent", "setContentText", "contentText", "", "setContentTitle", "contentTitle", "setDeleteIntent", "deleteIntent", "setGroup", "group", "setGroupAlertBehavior", "behavior", "setGroupSummary", "isGroupSummary", "setLargeIcon", "largeIcon", "Landroid/graphics/Bitmap;", "setLights", "onTime", "offTime", "setLocusIdActual", "locusId", "setNumber", "number", "setOnlyAlertOnce", "onlyAlertOnce", "setPriority", "priority", "setShortcutIdActual", "shortcutId", "setSmallIcon", "drawable", "setSortKey", "sortKey", "setSubText", "subText", "setTicker", "ticker", "setWhen", "timestamp", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class NotificationBuilderCompat extends NotificationBuilder {
        private final NotificationCompat.Builder builder;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public NotificationBuilderCompat(Context context) {
            super(context, null);
            Intrinsics.checkNotNullParameter(context, "context");
            this.builder = new NotificationCompat.Builder(context, NotificationChannels.getMessagesChannel(context));
        }

        public final NotificationCompat.Builder getBuilder() {
            return this.builder;
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        protected void addActions(ReplyMethod replyMethod, NotificationConversation notificationConversation) {
            NotificationCompat.Action action;
            Intrinsics.checkNotNullParameter(replyMethod, "replyMethod");
            Intrinsics.checkNotNullParameter(notificationConversation, "conversation");
            NotificationCompat.Action build = new NotificationCompat.Action.Builder((int) R.drawable.check, getContext().getString(R.string.MessageNotifier_mark_read), notificationConversation.getMarkAsReadIntent(getContext())).setSemanticAction(2).setShowsUserInterface(false).build();
            Intrinsics.checkNotNullExpressionValue(build, "Builder(R.drawable.check…e(false)\n        .build()");
            NotificationCompat.WearableExtender wearableExtender = new NotificationCompat.WearableExtender();
            this.builder.addAction(build);
            wearableExtender.addAction(build);
            if (notificationConversation.getMostRecentNotification().canReply(getContext())) {
                PendingIntent quickReplyIntent = notificationConversation.getQuickReplyIntent(getContext());
                PendingIntent remoteReplyIntent = notificationConversation.getRemoteReplyIntent(getContext(), replyMethod);
                String string = getContext().getString(R.string.MessageNotifier_reply);
                Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.string.MessageNotifier_reply)");
                String string2 = getContext().getString(NotificationBuilderKt.toLongDescription(replyMethod));
                Intrinsics.checkNotNullExpressionValue(string2, "context.getString(replyMethod.toLongDescription())");
                if (Build.VERSION.SDK_INT >= 24) {
                    action = new NotificationCompat.Action.Builder((int) R.drawable.ic_reply_white_36dp, string, remoteReplyIntent).addRemoteInput(new RemoteInput.Builder(MessageNotifierV2.EXTRA_REMOTE_REPLY).setLabel(string2).build()).setSemanticAction(1).setShowsUserInterface(false).build();
                    Intrinsics.checkNotNullExpressionValue(action, "{\n          Notification…       .build()\n        }");
                } else {
                    action = new NotificationCompat.Action((int) R.drawable.ic_reply_white_36dp, string, quickReplyIntent);
                }
                NotificationCompat.Action build2 = new NotificationCompat.Action.Builder((int) R.drawable.ic_reply, string, remoteReplyIntent).addRemoteInput(new RemoteInput.Builder(MessageNotifierV2.EXTRA_REMOTE_REPLY).setLabel(string2).build()).build();
                Intrinsics.checkNotNullExpressionValue(build2, "Builder(R.drawable.ic_re…ild())\n          .build()");
                this.builder.addAction(action);
                wearableExtender.addAction(build2);
            }
            this.builder.extend(wearableExtender);
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void addMarkAsReadActionActual(NotificationStateV2 notificationStateV2) {
            Intrinsics.checkNotNullParameter(notificationStateV2, "state");
            NotificationCompat.Action action = new NotificationCompat.Action((int) R.drawable.check, getContext().getString(R.string.MessageNotifier_mark_all_as_read), notificationStateV2.getMarkAsReadIntent(getContext()));
            this.builder.addAction(action);
            this.builder.extend(new NotificationCompat.WearableExtender().addAction(action));
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void addTurnOffJoinedNotificationsAction(PendingIntent pendingIntent) {
            Intrinsics.checkNotNullParameter(pendingIntent, "pendingIntent");
            this.builder.addAction(new NotificationCompat.Action((int) R.drawable.check, getContext().getString(R.string.MessageNotifier_turn_off_these_notifications), pendingIntent));
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        protected void addMessagesActual(NotificationConversation notificationConversation, boolean z) {
            Uri slideBigPictureUri;
            Intrinsics.checkNotNullParameter(notificationConversation, "conversation");
            IconCompat iconCompat = null;
            if (Build.VERSION.SDK_INT >= 24 || (slideBigPictureUri = notificationConversation.getSlideBigPictureUri(getContext())) == null) {
                Person.Builder name = new Person.Builder().setBot(false).setName(z ? Recipient.self().getDisplayName(getContext()) : getContext().getString(R.string.SingleRecipientNotificationBuilder_you));
                if (z) {
                    Recipient self = Recipient.self();
                    Intrinsics.checkNotNullExpressionValue(self, "self()");
                    iconCompat = NotificationBuilderKt.toIconCompat(NotificationExtensionsKt.toLargeBitmap(NotificationExtensionsKt.getContactDrawable(self, getContext()), getContext()));
                }
                Person build = name.setIcon(iconCompat).setKey(ConversationUtil.getShortcutId(Recipient.self().getId())).build();
                Intrinsics.checkNotNullExpressionValue(build, "Builder()\n        .setBo…f().id))\n        .build()");
                NotificationCompat.MessagingStyle messagingStyle = new NotificationCompat.MessagingStyle(build);
                messagingStyle.setConversationTitle(notificationConversation.getConversationTitle(getContext()));
                messagingStyle.setGroupConversation(notificationConversation.isGroup());
                for (NotificationItemV2 notificationItemV2 : notificationConversation.getNotificationItems()) {
                    Person.Builder icon = new Person.Builder().setBot(false).setName(notificationItemV2.getPersonName(getContext())).setUri(notificationItemV2.getPersonUri()).setIcon(NotificationBuilderKt.toIconCompat(notificationItemV2.getPersonIcon(getContext())));
                    Intrinsics.checkNotNullExpressionValue(icon, "Builder()\n          .set…(context).toIconCompat())");
                    if (z) {
                        icon.setKey(ConversationUtil.getShortcutId(notificationItemV2.getIndividualRecipient()));
                    }
                    NotificationItemV2.ThumbnailInfo thumbnailInfo = notificationItemV2.getThumbnailInfo(getContext());
                    messagingStyle.addMessage(new NotificationCompat.MessagingStyle.Message(notificationItemV2.getPrimaryText(getContext()), notificationItemV2.getTimestamp(), icon.build()).setData(thumbnailInfo.component2(), thumbnailInfo.component1()));
                }
                this.builder.setStyle(messagingStyle);
                return;
            }
            this.builder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(NotificationExtensionsKt.toBitmap(slideBigPictureUri, getContext(), 500)).setSummaryText(notificationConversation.getContentText(getContext())).bigLargeIcon(null));
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        protected void addMessagesActual(NotificationStateV2 notificationStateV2) {
            Intrinsics.checkNotNullParameter(notificationStateV2, "state");
            if (Build.VERSION.SDK_INT < 24) {
                NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
                for (NotificationItemV2 notificationItemV2 : notificationStateV2.getNotificationItems()) {
                    CharSequence inboxLine = notificationItemV2.getInboxLine(getContext());
                    if (inboxLine != null) {
                        inboxStyle.addLine(inboxLine);
                    }
                    addPerson(notificationItemV2.getIndividualRecipient());
                }
                this.builder.setStyle(inboxStyle);
            }
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void setAlarms(Recipient recipient) {
            if (!NotificationChannels.supported()) {
                RecipientDatabase.VibrateState vibrateState = null;
                Uri messageRingtone = recipient != null ? recipient.getMessageRingtone() : null;
                if (recipient != null) {
                    vibrateState = recipient.getMessageVibrate();
                }
                Uri messageNotificationSound = SignalStore.settings().getMessageNotificationSound();
                Intrinsics.checkNotNullExpressionValue(messageNotificationSound, "settings().messageNotificationSound");
                boolean isMessageVibrateEnabled = SignalStore.settings().isMessageVibrateEnabled();
                if (messageRingtone == null && !TextUtils.isEmpty(messageNotificationSound.toString())) {
                    this.builder.setSound(messageNotificationSound);
                } else if (messageRingtone != null) {
                    String uri = messageRingtone.toString();
                    Intrinsics.checkNotNullExpressionValue(uri, "ringtone.toString()");
                    if (uri.length() > 0) {
                        this.builder.setSound(messageRingtone);
                    }
                }
                if (vibrateState == RecipientDatabase.VibrateState.ENABLED || (vibrateState == RecipientDatabase.VibrateState.DEFAULT && isMessageVibrateEnabled)) {
                    this.builder.setDefaults(2);
                }
            }
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        protected void setBubbleMetadataActual(NotificationConversation notificationConversation, BubbleUtil.BubbleState bubbleState) {
            Intrinsics.checkNotNullParameter(notificationConversation, "conversation");
            Intrinsics.checkNotNullParameter(bubbleState, "bubbleState");
            if (Build.VERSION.SDK_INT >= 30) {
                boolean z = false;
                NotificationCompat.BubbleMetadata.Builder builder = new NotificationCompat.BubbleMetadata.Builder(PendingIntent.getActivity(getContext(), 0, ConversationIntents.createBubbleIntent(getContext(), notificationConversation.getRecipient().getId(), notificationConversation.getThread().getThreadId()), 0), AvatarUtil.getIconCompatForShortcut(getContext(), notificationConversation.getRecipient()));
                BubbleUtil.BubbleState bubbleState2 = BubbleUtil.BubbleState.SHOWN;
                NotificationCompat.BubbleMetadata.Builder desiredHeight = builder.setAutoExpandBubble(bubbleState == bubbleState2).setDesiredHeight(600);
                if (bubbleState == bubbleState2) {
                    z = true;
                }
                NotificationCompat.BubbleMetadata build = desiredHeight.setSuppressNotification(z).build();
                Intrinsics.checkNotNullExpressionValue(build, "Builder(intent, AvatarUt…e.SHOWN)\n        .build()");
                this.builder.setBubbleMetadata(build);
            }
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        protected void setLights(int i, int i2, int i3) {
            if (!NotificationChannels.supported()) {
                this.builder.setLights(i, i2, i3);
            }
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void setSmallIcon(int i) {
            this.builder.setSmallIcon(i);
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void setColor(int i) {
            this.builder.setColor(i);
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void setCategory(String str) {
            Intrinsics.checkNotNullParameter(str, "category");
            this.builder.setCategory(str);
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void setGroup(String str) {
            Intrinsics.checkNotNullParameter(str, "group");
            if (Build.VERSION.SDK_INT >= 24) {
                this.builder.setGroup(str);
            }
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void setGroupAlertBehavior(int i) {
            if (Build.VERSION.SDK_INT >= 24) {
                this.builder.setGroupAlertBehavior(i);
            }
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void setChannelId(String str) {
            Intrinsics.checkNotNullParameter(str, "channelId");
            this.builder.setChannelId(str);
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void setContentTitle(CharSequence charSequence) {
            Intrinsics.checkNotNullParameter(charSequence, "contentTitle");
            this.builder.setContentTitle(charSequence);
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void setLargeIcon(Bitmap bitmap) {
            this.builder.setLargeIcon(bitmap);
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        protected void setShortcutIdActual(String str) {
            Intrinsics.checkNotNullParameter(str, "shortcutId");
            this.builder.setShortcutId(str);
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void setContentInfo(String str) {
            Intrinsics.checkNotNullParameter(str, "contentInfo");
            this.builder.setContentInfo(str);
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void setNumber(int i) {
            this.builder.setNumber(i);
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void setContentText(CharSequence charSequence) {
            this.builder.setContentText(charSequence);
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void setTicker(CharSequence charSequence) {
            this.builder.setTicker(charSequence);
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void setContentIntent(PendingIntent pendingIntent) {
            this.builder.setContentIntent(pendingIntent);
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void setDeleteIntent(PendingIntent pendingIntent) {
            this.builder.setDeleteIntent(pendingIntent);
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void setSortKey(String str) {
            Intrinsics.checkNotNullParameter(str, "sortKey");
            this.builder.setSortKey(str);
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void setOnlyAlertOnce(boolean z) {
            this.builder.setOnlyAlertOnce(z);
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void setPriority(int i) {
            if (!NotificationChannels.supported()) {
                this.builder.setPriority(i);
            }
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void setAutoCancel(boolean z) {
            this.builder.setAutoCancel(z);
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public Notification build() {
            Notification build = this.builder.build();
            Intrinsics.checkNotNullExpressionValue(build, "builder.build()");
            return build;
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        protected void addPersonActual(Recipient recipient) {
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            this.builder.addPerson(String.valueOf(recipient.getContactUri()));
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        protected void setWhen(long j) {
            this.builder.setWhen(j);
            this.builder.setShowWhen(true);
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void setGroupSummary(boolean z) {
            this.builder.setGroupSummary(z);
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void setSubText(String str) {
            Intrinsics.checkNotNullParameter(str, "subText");
            this.builder.setSubText(str);
        }

        @Override // org.thoughtcrime.securesms.notifications.v2.NotificationBuilder
        public void setLocusIdActual(String str) {
            Intrinsics.checkNotNullParameter(str, "locusId");
            this.builder.setLocusId(new LocusIdCompat(str));
        }
    }
}
