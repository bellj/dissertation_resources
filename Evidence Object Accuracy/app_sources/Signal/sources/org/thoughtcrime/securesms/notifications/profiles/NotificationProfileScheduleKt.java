package org.thoughtcrime.securesms.notifications.profiles;

import j$.time.LocalDateTime;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: NotificationProfileSchedule.kt */
@Metadata(bv = {}, d1 = {"\u0000\b\n\u0002\u0010\b\n\u0002\b\u0004\u001a\u0012\u0010\u0003\u001a\u00020\u0001*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001¨\u0006\u0004"}, d2 = {"", "j$/time/LocalDateTime", "now", "toLocalDateTime", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class NotificationProfileScheduleKt {
    public static final LocalDateTime toLocalDateTime(int i, LocalDateTime localDateTime) {
        Intrinsics.checkNotNullParameter(localDateTime, "now");
        if (i == 2400) {
            LocalDateTime withSecond = localDateTime.plusDays(1).withHour(0).withMinute(0).withSecond(0);
            Intrinsics.checkNotNullExpressionValue(withSecond, "now.plusDays(1).withHour…thMinute(0).withSecond(0)");
            return withSecond;
        }
        LocalDateTime withSecond2 = localDateTime.withHour(i / 100).withMinute(i % 100).withSecond(0);
        Intrinsics.checkNotNullExpressionValue(withSecond2, "now.withHour(this / 100)…this % 100).withSecond(0)");
        return withSecond2;
    }
}
