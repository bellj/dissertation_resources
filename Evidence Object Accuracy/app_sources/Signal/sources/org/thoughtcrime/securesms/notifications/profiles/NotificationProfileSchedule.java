package org.thoughtcrime.securesms.notifications.profiles;

import j$.time.DayOfWeek;
import j$.time.LocalDateTime;
import j$.time.LocalTime;
import j$.time.ZoneId;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.util.JavaTimeExtensionsKt;

/* compiled from: NotificationProfileSchedule.kt */
@Metadata(bv = {}, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\b\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0014\b\b\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0019\u001a\u00020\u0002\u0012\b\b\u0002\u0010\u001a\u001a\u00020\u0006\u0012\b\b\u0002\u0010\u001b\u001a\u00020\u0013\u0012\b\b\u0002\u0010\u001c\u001a\u00020\u0013\u0012\u000e\b\u0002\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00170\u0016¢\u0006\u0004\b1\u00102J\u001a\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\b\b\u0002\u0010\u0005\u001a\u00020\u0004H\u0007J\u001a\u0010\t\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\u00022\b\b\u0002\u0010\u0005\u001a\u00020\u0004H\u0007J\u0006\u0010\u000b\u001a\u00020\nJ\u000e\u0010\u000e\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\fJ\u0006\u0010\u000f\u001a\u00020\nJ\u000e\u0010\u0010\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\fJ\t\u0010\u0011\u001a\u00020\u0002HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0013HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0013HÆ\u0003J\u000f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00170\u0016HÆ\u0003JA\u0010\u001e\u001a\u00020\u00002\b\b\u0002\u0010\u0019\u001a\u00020\u00022\b\b\u0002\u0010\u001a\u001a\u00020\u00062\b\b\u0002\u0010\u001b\u001a\u00020\u00132\b\b\u0002\u0010\u001c\u001a\u00020\u00132\u000e\b\u0002\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00170\u0016HÆ\u0001J\t\u0010 \u001a\u00020\u001fHÖ\u0001J\t\u0010!\u001a\u00020\u0013HÖ\u0001J\u0013\u0010#\u001a\u00020\u00062\b\u0010\"\u001a\u0004\u0018\u00010\u0001HÖ\u0003R\u0017\u0010\u0019\u001a\u00020\u00028\u0006¢\u0006\f\n\u0004\b\u0019\u0010$\u001a\u0004\b%\u0010&R\u0017\u0010\u001a\u001a\u00020\u00068\u0006¢\u0006\f\n\u0004\b\u001a\u0010'\u001a\u0004\b(\u0010)R\u0017\u0010\u001b\u001a\u00020\u00138\u0006¢\u0006\f\n\u0004\b\u001b\u0010*\u001a\u0004\b+\u0010,R\u0017\u0010\u001c\u001a\u00020\u00138\u0006¢\u0006\f\n\u0004\b\u001c\u0010*\u001a\u0004\b-\u0010,R\u001d\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00170\u00168\u0006¢\u0006\f\n\u0004\b\u001d\u0010.\u001a\u0004\b/\u00100¨\u00063"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfileSchedule;", "", "", "now", "j$/time/ZoneId", "zoneId", "", "isCurrentlyActive", "time", "coversTime", "j$/time/LocalTime", "startTime", "j$/time/LocalDateTime", "localNow", "startDateTime", "endTime", "endDateTime", "component1", "component2", "", "component3", "component4", "", "j$/time/DayOfWeek", "component5", ContactRepository.ID_COLUMN, NotificationProfileDatabase.NotificationProfileScheduleTable.ENABLED, NotificationProfileDatabase.NotificationProfileScheduleTable.START, NotificationProfileDatabase.NotificationProfileScheduleTable.END, "daysEnabled", "copy", "", "toString", "hashCode", "other", "equals", "J", "getId", "()J", "Z", "getEnabled", "()Z", "I", "getStart", "()I", "getEnd", "Ljava/util/Set;", "getDaysEnabled", "()Ljava/util/Set;", "<init>", "(JZIILjava/util/Set;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class NotificationProfileSchedule {
    private final Set<DayOfWeek> daysEnabled;
    private final boolean enabled;
    private final int end;
    private final long id;
    private final int start;

    /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: org.thoughtcrime.securesms.notifications.profiles.NotificationProfileSchedule */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ NotificationProfileSchedule copy$default(NotificationProfileSchedule notificationProfileSchedule, long j, boolean z, int i, int i2, Set set, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            j = notificationProfileSchedule.id;
        }
        if ((i3 & 2) != 0) {
            z = notificationProfileSchedule.enabled;
        }
        if ((i3 & 4) != 0) {
            i = notificationProfileSchedule.start;
        }
        if ((i3 & 8) != 0) {
            i2 = notificationProfileSchedule.end;
        }
        if ((i3 & 16) != 0) {
            set = notificationProfileSchedule.daysEnabled;
        }
        return notificationProfileSchedule.copy(j, z, i, i2, set);
    }

    public final long component1() {
        return this.id;
    }

    public final boolean component2() {
        return this.enabled;
    }

    public final int component3() {
        return this.start;
    }

    public final int component4() {
        return this.end;
    }

    public final Set<DayOfWeek> component5() {
        return this.daysEnabled;
    }

    public final NotificationProfileSchedule copy(long j, boolean z, int i, int i2, Set<? extends DayOfWeek> set) {
        Intrinsics.checkNotNullParameter(set, "daysEnabled");
        return new NotificationProfileSchedule(j, z, i, i2, set);
    }

    public final boolean coversTime(long j) {
        return coversTime$default(this, j, null, 2, null);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof NotificationProfileSchedule)) {
            return false;
        }
        NotificationProfileSchedule notificationProfileSchedule = (NotificationProfileSchedule) obj;
        return this.id == notificationProfileSchedule.id && this.enabled == notificationProfileSchedule.enabled && this.start == notificationProfileSchedule.start && this.end == notificationProfileSchedule.end && Intrinsics.areEqual(this.daysEnabled, notificationProfileSchedule.daysEnabled);
    }

    public int hashCode() {
        int m = SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.id) * 31;
        boolean z = this.enabled;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return ((((((m + i) * 31) + this.start) * 31) + this.end) * 31) + this.daysEnabled.hashCode();
    }

    public final boolean isCurrentlyActive(long j) {
        return isCurrentlyActive$default(this, j, null, 2, null);
    }

    public String toString() {
        return "NotificationProfileSchedule(id=" + this.id + ", enabled=" + this.enabled + ", start=" + this.start + ", end=" + this.end + ", daysEnabled=" + this.daysEnabled + ')';
    }

    /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: java.util.Set<? extends j$.time.DayOfWeek> */
    /* JADX WARN: Multi-variable type inference failed */
    public NotificationProfileSchedule(long j, boolean z, int i, int i2, Set<? extends DayOfWeek> set) {
        Intrinsics.checkNotNullParameter(set, "daysEnabled");
        this.id = j;
        this.enabled = z;
        this.start = i;
        this.end = i2;
        this.daysEnabled = set;
    }

    public final long getId() {
        return this.id;
    }

    public final boolean getEnabled() {
        return this.enabled;
    }

    public final int getStart() {
        return this.start;
    }

    public final int getEnd() {
        return this.end;
    }

    public /* synthetic */ NotificationProfileSchedule(long j, boolean z, int i, int i2, Set set, int i3, DefaultConstructorMarker defaultConstructorMarker) {
        this(j, (i3 & 2) != 0 ? false : z, (i3 & 4) != 0 ? 900 : i, (i3 & 8) != 0 ? 1700 : i2, (i3 & 16) != 0 ? SetsKt__SetsKt.emptySet() : set);
    }

    public final Set<DayOfWeek> getDaysEnabled() {
        return this.daysEnabled;
    }

    public static /* synthetic */ boolean isCurrentlyActive$default(NotificationProfileSchedule notificationProfileSchedule, long j, ZoneId zoneId, int i, Object obj) {
        if ((i & 2) != 0) {
            zoneId = ZoneId.systemDefault();
            Intrinsics.checkNotNullExpressionValue(zoneId, "systemDefault()");
        }
        return notificationProfileSchedule.isCurrentlyActive(j, zoneId);
    }

    public final boolean isCurrentlyActive(long j, ZoneId zoneId) {
        Intrinsics.checkNotNullParameter(zoneId, "zoneId");
        if (!this.enabled) {
            return false;
        }
        return coversTime(j, zoneId);
    }

    public static /* synthetic */ boolean coversTime$default(NotificationProfileSchedule notificationProfileSchedule, long j, ZoneId zoneId, int i, Object obj) {
        if ((i & 2) != 0) {
            zoneId = ZoneId.systemDefault();
            Intrinsics.checkNotNullExpressionValue(zoneId, "systemDefault()");
        }
        return notificationProfileSchedule.coversTime(j, zoneId);
    }

    public final boolean coversTime(long j, ZoneId zoneId) {
        Intrinsics.checkNotNullParameter(zoneId, "zoneId");
        LocalDateTime localDateTime = JavaTimeExtensionsKt.toLocalDateTime(j, zoneId);
        LocalDateTime localDateTime2 = NotificationProfileScheduleKt.toLocalDateTime(this.start, localDateTime);
        LocalDateTime localDateTime3 = NotificationProfileScheduleKt.toLocalDateTime(this.end, localDateTime);
        if (this.end < this.start) {
            if (this.daysEnabled.contains(localDateTime2.getDayOfWeek().minus(1))) {
                LocalDateTime minusDays = localDateTime2.minusDays(1);
                Intrinsics.checkNotNullExpressionValue(minusDays, "localStart.minusDays(1)");
                if (JavaTimeExtensionsKt.isBetween(localDateTime, minusDays, localDateTime3)) {
                    return true;
                }
            }
            if (this.daysEnabled.contains(localDateTime2.getDayOfWeek())) {
                LocalDateTime plusDays = localDateTime3.plusDays(1);
                Intrinsics.checkNotNullExpressionValue(plusDays, "localEnd.plusDays(1)");
                if (JavaTimeExtensionsKt.isBetween(localDateTime, localDateTime2, plusDays)) {
                    return true;
                }
            }
        } else if (this.daysEnabled.contains(localDateTime2.getDayOfWeek()) && JavaTimeExtensionsKt.isBetween(localDateTime, localDateTime2, localDateTime3)) {
            return true;
        }
        return false;
    }

    public final LocalTime startTime() {
        int i = this.start;
        LocalTime of = LocalTime.of(i / 100, i % 100);
        Intrinsics.checkNotNullExpressionValue(of, "of(start / 100, start % 100)");
        return of;
    }

    public final LocalDateTime startDateTime(LocalDateTime localDateTime) {
        Intrinsics.checkNotNullParameter(localDateTime, "localNow");
        LocalDateTime localDateTime2 = NotificationProfileScheduleKt.toLocalDateTime(this.start, localDateTime);
        LocalDateTime localDateTime3 = NotificationProfileScheduleKt.toLocalDateTime(this.end, localDateTime);
        if (this.end >= this.start || !this.daysEnabled.contains(localDateTime2.getDayOfWeek().minus(1))) {
            return localDateTime2;
        }
        LocalDateTime minusDays = localDateTime2.minusDays(1);
        Intrinsics.checkNotNullExpressionValue(minusDays, "localStart.minusDays(1)");
        if (!JavaTimeExtensionsKt.isBetween(localDateTime, minusDays, localDateTime3)) {
            return localDateTime2;
        }
        LocalDateTime minusDays2 = localDateTime2.minusDays(1);
        Intrinsics.checkNotNullExpressionValue(minusDays2, "{\n      localStart.minusDays(1)\n    }");
        return minusDays2;
    }

    public final LocalTime endTime() {
        int i = this.end;
        if (i == 2400) {
            i = 0;
        }
        LocalTime of = LocalTime.of(i / 100, i % 100);
        Intrinsics.checkNotNullExpressionValue(of, "of(adjustedEnd / 100, adjustedEnd % 100)");
        return of;
    }

    public final LocalDateTime endDateTime(LocalDateTime localDateTime) {
        Intrinsics.checkNotNullParameter(localDateTime, "localNow");
        LocalDateTime localDateTime2 = NotificationProfileScheduleKt.toLocalDateTime(this.start, localDateTime);
        LocalDateTime localDateTime3 = NotificationProfileScheduleKt.toLocalDateTime(this.end, localDateTime);
        if (this.end >= this.start || !this.daysEnabled.contains(localDateTime2.getDayOfWeek())) {
            return localDateTime3;
        }
        LocalDateTime plusDays = localDateTime3.plusDays(1);
        Intrinsics.checkNotNullExpressionValue(plusDays, "localEnd.plusDays(1)");
        if (!JavaTimeExtensionsKt.isBetween(localDateTime, localDateTime2, plusDays)) {
            return localDateTime3;
        }
        LocalDateTime plusDays2 = localDateTime3.plusDays(1);
        Intrinsics.checkNotNullExpressionValue(plusDays2, "{\n      localEnd.plusDays(1)\n    }");
        return plusDays2;
    }
}
