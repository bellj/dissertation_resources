package org.thoughtcrime.securesms.notifications.v2;

import android.app.AlarmManager;
import android.app.Application;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.core.content.ContextCompat;
import j$.util.Optional;
import j$.util.concurrent.ConcurrentHashMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt__MutableCollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.TypeIntrinsics;
import me.leolin.shortcutbadger.ShortcutBadger;
import me.leolin.shortcutbadger.impl.NewHtcHomeBadger;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.signal.core.util.PendingIntentFlags;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.MessageNotifier;
import org.thoughtcrime.securesms.notifications.NotificationCancellationHelper;
import org.thoughtcrime.securesms.notifications.NotificationIds;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfiles;
import org.thoughtcrime.securesms.notifications.v2.NotificationStateV2;
import org.thoughtcrime.securesms.preferences.widgets.NotificationPrivacyPreference;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.service.KeyCachingService;
import org.thoughtcrime.securesms.util.BubbleUtil;
import org.thoughtcrime.securesms.util.ServiceUtil;

/* compiled from: MessageNotifierV2.kt */
@Metadata(bv = {}, d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 >2\u00020\u0001:\u0003>?@B\u000f\u0012\u0006\u0010\u0003\u001a\u00020;¢\u0006\u0004\b<\u0010=J,\u0010\t\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004H\u0002J\u0010\u0010\n\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u0002H\u0002J\u0010\u0010\u000b\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u0002H\u0002J\u0012\u0010\r\u001a\u00020\b2\b\u0010\f\u001a\u0004\u0018\u00010\u0005H\u0016J\u000e\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00050\u000eH\u0016J\b\u0010\u0010\u001a\u00020\bH\u0016J\u0010\u0010\u0013\u001a\u00020\b2\u0006\u0010\u0012\u001a\u00020\u0011H\u0016J \u0010\u0016\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u00142\u0006\u0010\f\u001a\u00020\u0005H\u0016J \u0010\u0017\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u00142\u0006\u0010\f\u001a\u00020\u0005H\u0016J\b\u0010\u0018\u001a\u00020\bH\u0016J\u0010\u0010\u0019\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u0002H\u0016J\u0018\u0010\u0019\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\f\u001a\u00020\u0005H\u0016J \u0010\u0019\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\f\u001a\u00020\u00052\u0006\u0010\u001b\u001a\u00020\u001aH\u0016J \u0010\u0019\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\f\u001a\u00020\u00052\u0006\u0010\u001d\u001a\u00020\u001cH\u0016J2\u0010\u0019\u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u00022\b\u0010\f\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u001f\u001a\u00020\u001e2\u0006\u0010\u001b\u001a\u00020\u001aH\u0016J\u0010\u0010 \u001a\u00020\b2\u0006\u0010\u0003\u001a\u00020\u0002H\u0016J\u0018\u0010\"\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u00052\u0006\u0010!\u001a\u00020\u0011H\u0016J\u0010\u0010#\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\u0005H\u0016R\u0018\u0010$\u001a\u0004\u0018\u00010\u00058\u0002@\u0002X\u000e¢\u0006\u0006\n\u0004\b$\u0010%R\u0016\u0010&\u001a\u00020\u00118\u0002@\u0002X\u000e¢\u0006\u0006\n\u0004\b&\u0010'R\u0016\u0010(\u001a\u00020\u00118\u0002@\u0002X\u000e¢\u0006\u0006\n\u0004\b(\u0010'R\u0016\u0010)\u001a\u00020\u00118\u0002@\u0002X\u000e¢\u0006\u0006\n\u0004\b)\u0010'R\u0016\u0010*\u001a\u00020\u001c8\u0002@\u0002X\u000e¢\u0006\u0006\n\u0004\b*\u0010+R\u0016\u0010-\u001a\u00020,8\u0002@\u0002X\u000e¢\u0006\u0006\n\u0004\b-\u0010.R\u0016\u00100\u001a\u00020/8\u0002@\u0002X\u000e¢\u0006\u0006\n\u0004\b0\u00101R \u00104\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u000203028\u0002X\u0004¢\u0006\u0006\n\u0004\b4\u00105R \u00107\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u000206028\u0002X\u0004¢\u0006\u0006\n\u0004\b7\u00105R\u0014\u00109\u001a\u0002088\u0002X\u0004¢\u0006\u0006\n\u0004\b9\u0010:¨\u0006A"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/MessageNotifierV2;", "Lorg/thoughtcrime/securesms/notifications/MessageNotifier;", "Landroid/content/Context;", "context", "", "Lorg/thoughtcrime/securesms/notifications/v2/ConversationId;", "alertOverrides", "threadsThatAlerted", "", "updateReminderTimestamps", "scheduleReminder", "clearReminderInternal", "conversationId", "setVisibleThread", "j$/util/Optional", "getVisibleThread", "clearVisibleThread", "", "timestamp", "setLastDesktopActivityTimestamp", "Lorg/thoughtcrime/securesms/recipients/Recipient;", RecipientDatabase.TABLE_NAME, "notifyMessageDeliveryFailed", "notifyProofRequired", "cancelDelayedNotifications", "updateNotification", "Lorg/thoughtcrime/securesms/util/BubbleUtil$BubbleState;", "defaultBubbleState", "", "signal", "", "reminderCount", "clearReminder", "earliestTimestamp", "addStickyThread", "removeStickyThread", "visibleThread", "Lorg/thoughtcrime/securesms/notifications/v2/ConversationId;", "lastDesktopActivityTimestamp", "J", "lastAudibleNotification", "lastScheduledReminder", "previousLockedStatus", "Z", "Lorg/thoughtcrime/securesms/preferences/widgets/NotificationPrivacyPreference;", "previousPrivacyPreference", "Lorg/thoughtcrime/securesms/preferences/widgets/NotificationPrivacyPreference;", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationStateV2;", "previousState", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationStateV2;", "", "Lorg/thoughtcrime/securesms/notifications/v2/MessageNotifierV2$Reminder;", "threadReminders", "Ljava/util/Map;", "Lorg/thoughtcrime/securesms/notifications/v2/MessageNotifierV2$StickyThread;", "stickyThreads", "Lorg/thoughtcrime/securesms/notifications/v2/CancelableExecutor;", "executor", "Lorg/thoughtcrime/securesms/notifications/v2/CancelableExecutor;", "Landroid/app/Application;", "<init>", "(Landroid/app/Application;)V", "Companion", "Reminder", "StickyThread", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class MessageNotifierV2 implements MessageNotifier {
    public static final Companion Companion = new Companion(null);
    private static final long DESKTOP_ACTIVITY_PERIOD;
    public static final String EXTRA_REMOTE_REPLY;
    private static final long MIN_AUDIBLE_PERIOD_MILLIS = TimeUnit.SECONDS.toMillis(2);
    public static final String NOTIFICATION_GROUP;
    private static final long REMINDER_TIMEOUT;
    private static final String TAG;
    private final CancelableExecutor executor;
    private volatile long lastAudibleNotification = -1;
    private volatile long lastDesktopActivityTimestamp = -1;
    private volatile long lastScheduledReminder;
    private volatile boolean previousLockedStatus;
    private volatile NotificationPrivacyPreference previousPrivacyPreference;
    private volatile NotificationStateV2 previousState;
    private final Map<ConversationId, StickyThread> stickyThreads;
    private final Map<ConversationId, Reminder> threadReminders;
    private volatile ConversationId visibleThread;

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void clearReminder(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public MessageNotifierV2(Application application) {
        Intrinsics.checkNotNullParameter(application, "context");
        this.previousLockedStatus = KeyCachingService.isLocked(application);
        NotificationPrivacyPreference messageNotificationsPrivacy = SignalStore.settings().getMessageNotificationsPrivacy();
        Intrinsics.checkNotNullExpressionValue(messageNotificationsPrivacy, "settings().messageNotificationsPrivacy");
        this.previousPrivacyPreference = messageNotificationsPrivacy;
        this.previousState = NotificationStateV2.Companion.getEMPTY();
        this.threadReminders = new ConcurrentHashMap();
        this.stickyThreads = new LinkedHashMap();
        this.executor = new CancelableExecutor();
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void setVisibleThread(ConversationId conversationId) {
        this.visibleThread = conversationId;
        TypeIntrinsics.asMutableMap(this.stickyThreads).remove(conversationId);
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public Optional<ConversationId> getVisibleThread() {
        Optional<ConversationId> ofNullable = Optional.ofNullable(this.visibleThread);
        Intrinsics.checkNotNullExpressionValue(ofNullable, "ofNullable(visibleThread)");
        return ofNullable;
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void clearVisibleThread() {
        setVisibleThread(null);
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void setLastDesktopActivityTimestamp(long j) {
        this.lastDesktopActivityTimestamp = j;
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void notifyMessageDeliveryFailed(Context context, Recipient recipient, ConversationId conversationId) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        Intrinsics.checkNotNullParameter(conversationId, "conversationId");
        NotificationFactory.INSTANCE.notifyMessageDeliveryFailed(context, recipient, conversationId, this.visibleThread);
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void notifyProofRequired(Context context, Recipient recipient, ConversationId conversationId) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        Intrinsics.checkNotNullParameter(conversationId, "conversationId");
        NotificationFactory.INSTANCE.notifyProofRequired(context, recipient, conversationId, this.visibleThread);
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void cancelDelayedNotifications() {
        this.executor.cancel();
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void updateNotification(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        updateNotification(context, null, false, 0, BubbleUtil.BubbleState.HIDDEN);
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void updateNotification(Context context, ConversationId conversationId) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(conversationId, "conversationId");
        if (System.currentTimeMillis() - this.lastDesktopActivityTimestamp < DESKTOP_ACTIVITY_PERIOD) {
            Log.i(TAG, "Scheduling delayed notification...");
            this.executor.enqueue(context, conversationId);
            return;
        }
        updateNotification(context, conversationId, true);
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void updateNotification(Context context, ConversationId conversationId, BubbleUtil.BubbleState bubbleState) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(conversationId, "conversationId");
        Intrinsics.checkNotNullParameter(bubbleState, "defaultBubbleState");
        updateNotification(context, conversationId, false, 0, bubbleState);
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void updateNotification(Context context, ConversationId conversationId, boolean z) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(conversationId, "conversationId");
        updateNotification(context, conversationId, z, 0, BubbleUtil.BubbleState.HIDDEN);
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void updateNotification(Context context, ConversationId conversationId, boolean z, int i, BubbleUtil.BubbleState bubbleState) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(bubbleState, "defaultBubbleState");
        boolean isLocked = KeyCachingService.isLocked(context);
        NotificationPrivacyPreference messageNotificationsPrivacy = SignalStore.settings().getMessageNotificationsPrivacy();
        Intrinsics.checkNotNullExpressionValue(messageNotificationsPrivacy, "settings().messageNotificationsPrivacy");
        boolean z2 = isLocked != this.previousLockedStatus || !Intrinsics.areEqual(messageNotificationsPrivacy, this.previousPrivacyPreference);
        this.previousLockedStatus = isLocked;
        this.previousPrivacyPreference = messageNotificationsPrivacy;
        if (z2) {
            this.stickyThreads.clear();
        }
        NotificationProfile activeProfile$default = NotificationProfiles.getActiveProfile$default(SignalDatabase.Companion.notificationProfiles().getProfiles(), 0, null, 6, null);
        Log.Logger internal = Log.internal();
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("sticky thread: ");
        sb.append(this.stickyThreads);
        sb.append(" active profile: ");
        sb.append(activeProfile$default != null ? Long.valueOf(activeProfile$default.getId()) : "none");
        internal.i(str, sb.toString());
        NotificationStateV2 constructNotificationState = NotificationStateProvider.INSTANCE.constructNotificationState(this.stickyThreads, activeProfile$default);
        Log.Logger internal2 = Log.internal();
        internal2.i(str, "state: " + constructNotificationState);
        if (!constructNotificationState.getMuteFilteredMessages().isEmpty()) {
            Log.i(str, "Marking " + constructNotificationState.getMuteFilteredMessages().size() + " muted messages as notified to skip notification");
            for (NotificationStateV2.FilteredMessage filteredMessage : constructNotificationState.getMuteFilteredMessages()) {
                (filteredMessage.isMms() ? SignalDatabase.Companion.mms() : SignalDatabase.Companion.sms()).markAsNotified(filteredMessage.getId());
            }
        }
        if (!constructNotificationState.getProfileFilteredMessages().isEmpty()) {
            String str2 = TAG;
            Log.i(str2, "Marking " + constructNotificationState.getProfileFilteredMessages().size() + " profile filtered messages as notified to skip notification");
            for (NotificationStateV2.FilteredMessage filteredMessage2 : constructNotificationState.getProfileFilteredMessages()) {
                (filteredMessage2.isMms() ? SignalDatabase.Companion.mms() : SignalDatabase.Companion.sms()).markAsNotified(filteredMessage2.getId());
            }
        }
        if (!SignalStore.settings().isMessageNotificationsEnabled()) {
            String str3 = TAG;
            Log.i(str3, "Marking " + constructNotificationState.getConversations().size() + " conversations as notified to skip notification");
            for (NotificationConversation notificationConversation : constructNotificationState.getConversations()) {
                for (NotificationItemV2 notificationItemV2 : notificationConversation.getNotificationItems()) {
                    (notificationItemV2.isMms() ? SignalDatabase.Companion.mms() : SignalDatabase.Companion.sms()).markAsNotified(notificationItemV2.getId());
                }
            }
            return;
        }
        NotificationManager notificationManager = ServiceUtil.getNotificationManager(context);
        Intrinsics.checkNotNullExpressionValue(notificationManager, "getNotificationManager(context)");
        Object obj = MessageNotifierV2Kt.getDisplayedNotificationIds(notificationManager);
        if (Result.m157isFailureimpl(obj)) {
            obj = null;
        }
        Set set = (Set) obj;
        if (set != null) {
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            List<NotificationConversation> conversations = constructNotificationState.getConversations();
            ArrayList<NotificationConversation> arrayList = new ArrayList();
            for (Object obj2 : conversations) {
                NotificationConversation notificationConversation2 = (NotificationConversation) obj2;
                if (!(notificationConversation2.hasNewNotifications() || set.contains(Integer.valueOf(notificationConversation2.getNotificationId())))) {
                    arrayList.add(obj2);
                }
            }
            for (NotificationConversation notificationConversation3 : arrayList) {
                linkedHashSet.add(notificationConversation3.getThread());
                for (NotificationItemV2 notificationItemV22 : notificationConversation3.getNotificationItems()) {
                    (notificationItemV22.isMms() ? SignalDatabase.Companion.mms() : SignalDatabase.Companion.sms()).markAsNotified(notificationItemV22.getId());
                }
            }
            if (!linkedHashSet.isEmpty()) {
                String str4 = TAG;
                Log.i(str4, "Cleaned up " + linkedHashSet.size() + " thread(s) with dangling notifications");
                List<NotificationConversation> conversations2 = constructNotificationState.getConversations();
                ArrayList arrayList2 = new ArrayList();
                for (Object obj3 : conversations2) {
                    if (!linkedHashSet.contains(((NotificationConversation) obj3).getThread())) {
                        arrayList2.add(obj3);
                    }
                }
                constructNotificationState = NotificationStateV2.copy$default(constructNotificationState, arrayList2, null, null, 6, null);
            }
        }
        boolean unused = CollectionsKt__MutableCollectionsKt.retainAll(this.stickyThreads.keySet(), new Function1<ConversationId, Boolean>(constructNotificationState.getThreadsWithMostRecentNotificationFromSelf()) { // from class: org.thoughtcrime.securesms.notifications.v2.MessageNotifierV2$updateNotification$7
            final /* synthetic */ Set<ConversationId> $retainStickyThreadIds;

            /* access modifiers changed from: package-private */
            {
                this.$retainStickyThreadIds = r1;
            }

            public final Boolean invoke(ConversationId conversationId2) {
                Intrinsics.checkNotNullParameter(conversationId2, "it");
                return Boolean.valueOf(this.$retainStickyThreadIds.contains(conversationId2));
            }
        });
        if (constructNotificationState.isEmpty()) {
            Log.i(TAG, "State is empty, cancelling all notifications");
            Map<ConversationId, StickyThread> map = this.stickyThreads;
            ArrayList arrayList3 = new ArrayList(map.size());
            for (Map.Entry<ConversationId, StickyThread> entry : map.entrySet()) {
                arrayList3.add(Integer.valueOf(entry.getValue().getNotificationId()));
            }
            NotificationCancellationHelper.cancelAllMessageNotifications(context, CollectionsKt___CollectionsKt.toSet(arrayList3));
            Companion.updateBadge(context, 0);
            clearReminderInternal(context);
            return;
        }
        Map<ConversationId, Reminder> map2 = this.threadReminders;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry<ConversationId, Reminder> entry2 : map2.entrySet()) {
            if (entry2.getValue().getLastNotified() < System.currentTimeMillis() - REMINDER_TIMEOUT) {
                linkedHashMap.put(entry2.getKey(), entry2.getValue());
            }
        }
        Set<ConversationId> keySet = linkedHashMap.keySet();
        Set<ConversationId> notify = NotificationFactory.INSTANCE.notify(new ContextThemeWrapper(context, (int) R.style.TextSecure_LightTheme), constructNotificationState, this.visibleThread, conversationId, bubbleState, this.lastAudibleNotification, z2, keySet, this.previousState);
        this.previousState = constructNotificationState;
        this.lastAudibleNotification = System.currentTimeMillis();
        updateReminderTimestamps(context, keySet, notify);
        NotificationManager notificationManager2 = ServiceUtil.getNotificationManager(context);
        Intrinsics.checkNotNullExpressionValue(notificationManager2, "getNotificationManager(context)");
        Map<ConversationId, StickyThread> map3 = this.stickyThreads;
        ArrayList arrayList4 = new ArrayList(map3.size());
        for (Map.Entry<ConversationId, StickyThread> entry3 : map3.entrySet()) {
            arrayList4.add(Integer.valueOf(entry3.getValue().getNotificationId()));
        }
        MessageNotifierV2Kt.cancelOrphanedNotifications(notificationManager2, context, constructNotificationState, CollectionsKt___CollectionsKt.toSet(arrayList4));
        Companion.updateBadge(context, constructNotificationState.getMessageCount());
        ArrayList arrayList5 = new ArrayList();
        ArrayList arrayList6 = new ArrayList();
        for (NotificationItemV2 notificationItemV23 : constructNotificationState.getNotificationItems()) {
            if (notificationItemV23.isMms()) {
                arrayList6.add(Long.valueOf(notificationItemV23.getId()));
            } else {
                arrayList5.add(Long.valueOf(notificationItemV23.getId()));
            }
        }
        SignalDatabase.Companion.mmsSms().setNotifiedTimestamp(System.currentTimeMillis(), arrayList5, arrayList6);
        String str5 = TAG;
        Log.i(str5, "threads: " + constructNotificationState.getThreadCount() + " messages: " + constructNotificationState.getMessageCount());
        if (Build.VERSION.SDK_INT >= 24) {
            List<NotificationConversation> conversations3 = constructNotificationState.getConversations();
            ArrayList<NotificationConversation> arrayList7 = new ArrayList();
            for (Object obj4 : conversations3) {
                if (!Intrinsics.areEqual(((NotificationConversation) obj4).getThread(), this.visibleThread)) {
                    arrayList7.add(obj4);
                }
            }
            ArrayList arrayList8 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList7, 10));
            for (NotificationConversation notificationConversation4 : arrayList7) {
                arrayList8.add(Integer.valueOf(notificationConversation4.getNotificationId()));
            }
            Map<ConversationId, StickyThread> map4 = this.stickyThreads;
            ArrayList arrayList9 = new ArrayList(map4.size());
            for (Map.Entry<ConversationId, StickyThread> entry4 : map4.entrySet()) {
                arrayList9.add(Integer.valueOf(entry4.getValue().getNotificationId()));
            }
            List list = CollectionsKt___CollectionsKt.plus((Collection) arrayList8, (Iterable) arrayList9);
            NotificationManager notificationManager3 = ServiceUtil.getNotificationManager(context);
            Intrinsics.checkNotNullExpressionValue(notificationManager3, "getNotificationManager(context)");
            Object obj5 = MessageNotifierV2Kt.getDisplayedNotificationIds(notificationManager3);
            obj5 = SetsKt__SetsKt.emptySet();
            if (Result.m157isFailureimpl(obj5)) {
            }
            List list2 = CollectionsKt___CollectionsKt.minus((Iterable) list, (Iterable) obj5);
            if (!list2.isEmpty()) {
                String str6 = TAG;
                Log.e(str6, "Notifications should be showing but are not for " + list2.size() + " threads");
            }
        }
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void addStickyThread(ConversationId conversationId, long j) {
        Intrinsics.checkNotNullParameter(conversationId, "conversationId");
        this.stickyThreads.put(conversationId, new StickyThread(conversationId, NotificationIds.getNotificationIdForThread(conversationId), j));
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void removeStickyThread(ConversationId conversationId) {
        Intrinsics.checkNotNullParameter(conversationId, "conversationId");
        this.stickyThreads.remove(conversationId);
    }

    private final void updateReminderTimestamps(Context context, Set<ConversationId> set, Set<ConversationId> set2) {
        if (SignalStore.settings().getMessageNotificationsRepeatAlerts() != 0) {
            Iterator<Map.Entry<ConversationId, Reminder>> it = this.threadReminders.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<ConversationId, Reminder> next = it.next();
                Reminder value = next.getValue();
                if (set.contains(next.getKey())) {
                    int count = value.getCount() + 1;
                    if (count >= SignalStore.settings().getMessageNotificationsRepeatAlerts()) {
                        it.remove();
                    } else {
                        next.setValue(new Reminder(this.lastAudibleNotification, count));
                    }
                }
            }
            for (ConversationId conversationId : set2) {
                this.threadReminders.put(conversationId, new Reminder(this.lastAudibleNotification, 0, 2, null));
            }
            if (!this.threadReminders.isEmpty()) {
                scheduleReminder(context);
            } else {
                this.lastScheduledReminder = 0;
            }
        }
    }

    private final void scheduleReminder(Context context) {
        long j;
        if (this.lastScheduledReminder != 0) {
            j = Math.max(TimeUnit.SECONDS.toMillis(5), REMINDER_TIMEOUT - (System.currentTimeMillis() - this.lastScheduledReminder));
        } else {
            j = REMINDER_TIMEOUT;
        }
        AlarmManager alarmManager = (AlarmManager) ContextCompat.getSystemService(context, AlarmManager.class);
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, new Intent(context, MessageNotifier.ReminderReceiver.class), PendingIntentFlags.INSTANCE.updateCurrent());
        Intrinsics.checkNotNullExpressionValue(broadcast, "getBroadcast(context, 0,…entFlags.updateCurrent())");
        if (alarmManager != null) {
            alarmManager.set(0, System.currentTimeMillis() + j, broadcast);
        }
        this.lastScheduledReminder = System.currentTimeMillis();
    }

    private final void clearReminderInternal(Context context) {
        AlarmManager alarmManager;
        this.lastScheduledReminder = 0;
        this.threadReminders.clear();
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, new Intent(context, MessageNotifier.ReminderReceiver.class), PendingIntentFlags.INSTANCE.cancelCurrent());
        if (broadcast != null && (alarmManager = (AlarmManager) ContextCompat.getSystemService(context, AlarmManager.class)) != null) {
            alarmManager.cancel(broadcast);
        }
    }

    /* compiled from: MessageNotifierV2.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0002R\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\bXT¢\u0006\u0002\n\u0000R\u0011\u0010\t\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u0006R\u000e\u0010\u000b\u001a\u00020\bXT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\r\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/MessageNotifierV2$Companion;", "", "()V", "DESKTOP_ACTIVITY_PERIOD", "", "getDESKTOP_ACTIVITY_PERIOD", "()J", "EXTRA_REMOTE_REPLY", "", "MIN_AUDIBLE_PERIOD_MILLIS", "getMIN_AUDIBLE_PERIOD_MILLIS", "NOTIFICATION_GROUP", "REMINDER_TIMEOUT", "TAG", "getTAG", "()Ljava/lang/String;", "updateBadge", "", "context", "Landroid/content/Context;", NewHtcHomeBadger.COUNT, "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final String getTAG() {
            return MessageNotifierV2.TAG;
        }

        public final long getMIN_AUDIBLE_PERIOD_MILLIS() {
            return MessageNotifierV2.MIN_AUDIBLE_PERIOD_MILLIS;
        }

        public final long getDESKTOP_ACTIVITY_PERIOD() {
            return MessageNotifierV2.DESKTOP_ACTIVITY_PERIOD;
        }

        public final void updateBadge(Context context, int i) {
            try {
                if (i == 0) {
                    ShortcutBadger.removeCount(context);
                } else {
                    ShortcutBadger.applyCount(context, i);
                }
            } catch (Throwable th) {
                while (true) {
                    Log.w(getTAG(), th);
                    return;
                }
            }
        }
    }

    static {
        Companion = new Companion(null);
        String tag = Log.tag(MessageNotifierV2.class);
        Intrinsics.checkNotNullExpressionValue(tag, "tag(MessageNotifierV2::class.java)");
        TAG = tag;
        TimeUnit timeUnit = TimeUnit.MINUTES;
        REMINDER_TIMEOUT = timeUnit.toMillis(2);
        MIN_AUDIBLE_PERIOD_MILLIS = TimeUnit.SECONDS.toMillis(2);
        DESKTOP_ACTIVITY_PERIOD = timeUnit.toMillis(1);
    }

    /* compiled from: MessageNotifierV2.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0007HÆ\u0003J'\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0016\u001a\u00020\u0005HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/MessageNotifierV2$StickyThread;", "", "conversationId", "Lorg/thoughtcrime/securesms/notifications/v2/ConversationId;", "notificationId", "", "earliestTimestamp", "", "(Lorg/thoughtcrime/securesms/notifications/v2/ConversationId;IJ)V", "getConversationId", "()Lorg/thoughtcrime/securesms/notifications/v2/ConversationId;", "getEarliestTimestamp", "()J", "getNotificationId", "()I", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class StickyThread {
        private final ConversationId conversationId;
        private final long earliestTimestamp;
        private final int notificationId;

        public static /* synthetic */ StickyThread copy$default(StickyThread stickyThread, ConversationId conversationId, int i, long j, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                conversationId = stickyThread.conversationId;
            }
            if ((i2 & 2) != 0) {
                i = stickyThread.notificationId;
            }
            if ((i2 & 4) != 0) {
                j = stickyThread.earliestTimestamp;
            }
            return stickyThread.copy(conversationId, i, j);
        }

        public final ConversationId component1() {
            return this.conversationId;
        }

        public final int component2() {
            return this.notificationId;
        }

        public final long component3() {
            return this.earliestTimestamp;
        }

        public final StickyThread copy(ConversationId conversationId, int i, long j) {
            Intrinsics.checkNotNullParameter(conversationId, "conversationId");
            return new StickyThread(conversationId, i, j);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StickyThread)) {
                return false;
            }
            StickyThread stickyThread = (StickyThread) obj;
            return Intrinsics.areEqual(this.conversationId, stickyThread.conversationId) && this.notificationId == stickyThread.notificationId && this.earliestTimestamp == stickyThread.earliestTimestamp;
        }

        public int hashCode() {
            return (((this.conversationId.hashCode() * 31) + this.notificationId) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.earliestTimestamp);
        }

        public String toString() {
            return "StickyThread(conversationId=" + this.conversationId + ", notificationId=" + this.notificationId + ", earliestTimestamp=" + this.earliestTimestamp + ')';
        }

        public StickyThread(ConversationId conversationId, int i, long j) {
            Intrinsics.checkNotNullParameter(conversationId, "conversationId");
            this.conversationId = conversationId;
            this.notificationId = i;
            this.earliestTimestamp = j;
        }

        public final ConversationId getConversationId() {
            return this.conversationId;
        }

        public final long getEarliestTimestamp() {
            return this.earliestTimestamp;
        }

        public final int getNotificationId() {
            return this.notificationId;
        }
    }

    /* compiled from: MessageNotifierV2.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0005HÖ\u0001J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/MessageNotifierV2$Reminder;", "", "lastNotified", "", NewHtcHomeBadger.COUNT, "", "(JI)V", "getCount", "()I", "getLastNotified", "()J", "component1", "component2", "copy", "equals", "", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Reminder {
        private final int count;
        private final long lastNotified;

        public static /* synthetic */ Reminder copy$default(Reminder reminder, long j, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                j = reminder.lastNotified;
            }
            if ((i2 & 2) != 0) {
                i = reminder.count;
            }
            return reminder.copy(j, i);
        }

        public final long component1() {
            return this.lastNotified;
        }

        public final int component2() {
            return this.count;
        }

        public final Reminder copy(long j, int i) {
            return new Reminder(j, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Reminder)) {
                return false;
            }
            Reminder reminder = (Reminder) obj;
            return this.lastNotified == reminder.lastNotified && this.count == reminder.count;
        }

        public int hashCode() {
            return (SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.lastNotified) * 31) + this.count;
        }

        public String toString() {
            return "Reminder(lastNotified=" + this.lastNotified + ", count=" + this.count + ')';
        }

        public Reminder(long j, int i) {
            this.lastNotified = j;
            this.count = i;
        }

        public /* synthetic */ Reminder(long j, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
            this(j, (i2 & 2) != 0 ? 0 : i);
        }

        public final int getCount() {
            return this.count;
        }

        public final long getLastNotified() {
            return this.lastNotified;
        }
    }
}
