package org.thoughtcrime.securesms.notifications;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.app.RemoteInput;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.ParentStoryId;
import org.thoughtcrime.securesms.database.model.StoryType;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.notifications.v2.MessageNotifierV2;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.sms.OutgoingEncryptedMessage;
import org.thoughtcrime.securesms.sms.OutgoingTextMessage;

/* loaded from: classes4.dex */
public class RemoteReplyReceiver extends BroadcastReceiver {
    public static final String EARLIEST_TIMESTAMP;
    public static final String GROUP_STORY_ID_EXTRA;
    public static final String RECIPIENT_EXTRA;
    public static final String REPLY_ACTION;
    public static final String REPLY_METHOD;

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        Bundle resultsFromIntent;
        if (REPLY_ACTION.equals(intent.getAction()) && (resultsFromIntent = RemoteInput.getResultsFromIntent(intent)) != null) {
            RecipientId recipientId = (RecipientId) intent.getParcelableExtra(RECIPIENT_EXTRA);
            ReplyMethod replyMethod = (ReplyMethod) intent.getSerializableExtra(REPLY_METHOD);
            CharSequence charSequence = resultsFromIntent.getCharSequence(MessageNotifierV2.EXTRA_REMOTE_REPLY);
            long longExtra = intent.getLongExtra(GROUP_STORY_ID_EXTRA, Long.MIN_VALUE);
            if (recipientId == null) {
                throw new AssertionError("No recipientId specified");
            } else if (replyMethod == null) {
                throw new AssertionError("No reply method specified");
            } else if (charSequence != null) {
                SignalExecutors.BOUNDED.execute(new Runnable(longExtra, replyMethod, charSequence, context, intent) { // from class: org.thoughtcrime.securesms.notifications.RemoteReplyReceiver$$ExternalSyntheticLambda0
                    public final /* synthetic */ long f$1;
                    public final /* synthetic */ ReplyMethod f$2;
                    public final /* synthetic */ CharSequence f$3;
                    public final /* synthetic */ Context f$4;
                    public final /* synthetic */ Intent f$5;

                    {
                        this.f$1 = r2;
                        this.f$2 = r4;
                        this.f$3 = r5;
                        this.f$4 = r6;
                        this.f$5 = r7;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        RemoteReplyReceiver.$r8$lambda$SKfLIRRMWSZVUAh5fyMdG9ki4hk(RecipientId.this, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5);
                    }
                });
            }
        }
    }

    public static /* synthetic */ void lambda$onReceive$0(RecipientId recipientId, long j, ReplyMethod replyMethod, CharSequence charSequence, Context context, Intent intent) {
        boolean z;
        long j2;
        Recipient resolved = Recipient.resolved(recipientId);
        int intValue = resolved.getDefaultSubscriptionId().orElse(-1).intValue();
        long millis = TimeUnit.SECONDS.toMillis((long) resolved.getExpiresInSeconds());
        Long l = null;
        ParentStoryId deserialize = j != Long.MIN_VALUE ? ParentStoryId.deserialize(j) : null;
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$notifications$ReplyMethod[replyMethod.ordinal()];
        if (i != 1) {
            if (i == 2) {
                j2 = MessageSender.send(context, (OutgoingTextMessage) new OutgoingEncryptedMessage(resolved, charSequence.toString(), millis), -1L, false, (String) null, (MessageDatabase.InsertListener) null);
            } else if (i == 3) {
                j2 = MessageSender.send(context, new OutgoingTextMessage(resolved, charSequence.toString(), millis, intValue), -1L, true, (String) null, (MessageDatabase.InsertListener) null);
            } else {
                throw new AssertionError("Unknown Reply method");
            }
            z = true;
        } else {
            OutgoingMediaMessage outgoingMediaMessage = new OutgoingMediaMessage(resolved, charSequence.toString(), new LinkedList(), System.currentTimeMillis(), intValue, millis, false, 0, StoryType.NONE, deserialize, false, null, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), Collections.emptySet(), Collections.emptySet(), null);
            z = true;
            j2 = MessageSender.send(context, outgoingMediaMessage, -1L, false, (String) null, (MessageDatabase.InsertListener) null);
        }
        MessageNotifier messageNotifier = ApplicationDependencies.getMessageNotifier();
        if (j != Long.MIN_VALUE) {
            l = Long.valueOf(j);
        }
        messageNotifier.addStickyThread(new ConversationId(j2, l), intent.getLongExtra(EARLIEST_TIMESTAMP, System.currentTimeMillis()));
        List<MessageDatabase.MarkedMessageInfo> read = SignalDatabase.threads().setRead(j2, z);
        ApplicationDependencies.getMessageNotifier().updateNotification(context);
        MarkReadReceiver.process(context, read);
    }

    /* renamed from: org.thoughtcrime.securesms.notifications.RemoteReplyReceiver$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$notifications$ReplyMethod;

        static {
            int[] iArr = new int[ReplyMethod.values().length];
            $SwitchMap$org$thoughtcrime$securesms$notifications$ReplyMethod = iArr;
            try {
                iArr[ReplyMethod.GroupMessage.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$notifications$ReplyMethod[ReplyMethod.SecureMessage.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$notifications$ReplyMethod[ReplyMethod.UnsecuredSmsMessage.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }
}
