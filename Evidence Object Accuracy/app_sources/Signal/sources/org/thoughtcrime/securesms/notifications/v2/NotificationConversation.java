package org.thoughtcrime.securesms.notifications.v2;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.SpannableStringBuilder;
import androidx.core.app.TaskStackBuilder;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.signal.core.util.PendingIntentFlags;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.contacts.TurnOffContactJoinedNotificationsActivity;
import org.thoughtcrime.securesms.contacts.avatars.GeneratedContactPhoto;
import org.thoughtcrime.securesms.conversation.ConversationIntents;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.DeleteNotificationReceiver;
import org.thoughtcrime.securesms.notifications.MarkReadReceiver;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.notifications.NotificationIds;
import org.thoughtcrime.securesms.notifications.RemoteReplyReceiver;
import org.thoughtcrime.securesms.notifications.ReplyMethod;
import org.thoughtcrime.securesms.preferences.widgets.NotificationPrivacyPreference;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.service.KeyCachingService;
import org.thoughtcrime.securesms.stories.StoryViewerArgs;
import org.thoughtcrime.securesms.stories.viewer.StoryViewerActivity;
import org.thoughtcrime.securesms.util.Util;

/* compiled from: NotificationConversation.kt */
@Metadata(d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0010\t\n\u0002\b\u000b\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\b\u0018\u00002\u00020\u0001B#\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\u0002\u0010\tJ\t\u0010!\u001a\u00020\u0003HÆ\u0003J\t\u0010\"\u001a\u00020\u0005HÆ\u0003J\u000f\u0010#\u001a\b\u0012\u0004\u0012\u00020\b0\u0007HÆ\u0003J-\u0010$\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007HÆ\u0001J\u0013\u0010%\u001a\u00020\u000b2\b\u0010&\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\u000e\u0010'\u001a\u00020(2\u0006\u0010)\u001a\u00020*J\u0010\u0010+\u001a\u0004\u0018\u00010,2\u0006\u0010)\u001a\u00020*J\u0010\u0010-\u001a\u0004\u0018\u00010.2\u0006\u0010)\u001a\u00020*J\u000e\u0010/\u001a\u00020.2\u0006\u0010)\u001a\u00020*J\u0010\u00100\u001a\u0004\u0018\u00010.2\u0006\u0010)\u001a\u00020*J\u0010\u00101\u001a\u0004\u0018\u0001022\u0006\u0010)\u001a\u00020*J\u0010\u00103\u001a\u00020(2\u0006\u0010)\u001a\u00020*H\u0002J\u000e\u00104\u001a\u0002022\u0006\u0010)\u001a\u00020*J\u0010\u00105\u001a\u0004\u0018\u0001022\u0006\u0010)\u001a\u00020*J\u000e\u00106\u001a\u0002022\u0006\u0010)\u001a\u00020*J\u0016\u00107\u001a\u0002022\u0006\u0010)\u001a\u00020*2\u0006\u00108\u001a\u000209J\u0010\u0010:\u001a\u0004\u0018\u00010;2\u0006\u0010)\u001a\u00020*J\u000e\u0010<\u001a\u0002022\u0006\u0010)\u001a\u00020*J\u0006\u0010=\u001a\u00020\u001cJ\u0006\u0010>\u001a\u00020\u000bJ\u0010\u0010?\u001a\u00020\u000b2\b\u0010&\u001a\u0004\u0018\u00010\u0000J\t\u0010@\u001a\u00020\u000fHÖ\u0001J\b\u0010A\u001a\u00020(H\u0016R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\fR\u0011\u0010\r\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u0011\u0010\u000e\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0012\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\u0015\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0011R\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\u001b\u001a\u00020\u001c¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010 ¨\u0006B"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/NotificationConversation;", "", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", ThreadDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/notifications/v2/ConversationId;", "notificationItems", "", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationItemV2;", "(Lorg/thoughtcrime/securesms/recipients/Recipient;Lorg/thoughtcrime/securesms/notifications/v2/ConversationId;Ljava/util/List;)V", "isGroup", "", "()Z", "isOnlyContactJoinedEvent", "messageCount", "", "getMessageCount", "()I", "mostRecentNotification", "getMostRecentNotification", "()Lorg/thoughtcrime/securesms/notifications/v2/NotificationItemV2;", "notificationId", "getNotificationId", "getNotificationItems", "()Ljava/util/List;", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "sortKey", "", "getSortKey", "()J", "getThread", "()Lorg/thoughtcrime/securesms/notifications/v2/ConversationId;", "component1", "component2", "component3", "copy", "equals", "other", "getChannelId", "", "context", "Landroid/content/Context;", "getContactLargeIcon", "Landroid/graphics/drawable/Drawable;", "getContentText", "", "getContentTitle", "getConversationTitle", "getDeleteIntent", "Landroid/app/PendingIntent;", "getDisplayName", "getMarkAsReadIntent", "getPendingIntent", "getQuickReplyIntent", "getRemoteReplyIntent", "replyMethod", "Lorg/thoughtcrime/securesms/notifications/ReplyMethod;", "getSlideBigPictureUri", "Landroid/net/Uri;", "getTurnOffJoinedNotificationsIntent", "getWhen", "hasNewNotifications", "hasSameContent", "hashCode", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationConversation {
    private final boolean isGroup;
    private final boolean isOnlyContactJoinedEvent;
    private final int messageCount;
    private final NotificationItemV2 mostRecentNotification;
    private final int notificationId;
    private final List<NotificationItemV2> notificationItems;
    private final Recipient recipient;
    private final long sortKey;
    private final ConversationId thread;

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.notifications.v2.NotificationConversation */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ NotificationConversation copy$default(NotificationConversation notificationConversation, Recipient recipient, ConversationId conversationId, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            recipient = notificationConversation.recipient;
        }
        if ((i & 2) != 0) {
            conversationId = notificationConversation.thread;
        }
        if ((i & 4) != 0) {
            list = notificationConversation.notificationItems;
        }
        return notificationConversation.copy(recipient, conversationId, list);
    }

    public final Recipient component1() {
        return this.recipient;
    }

    public final ConversationId component2() {
        return this.thread;
    }

    public final List<NotificationItemV2> component3() {
        return this.notificationItems;
    }

    public final NotificationConversation copy(Recipient recipient, ConversationId conversationId, List<? extends NotificationItemV2> list) {
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        Intrinsics.checkNotNullParameter(conversationId, ThreadDatabase.TABLE_NAME);
        Intrinsics.checkNotNullParameter(list, "notificationItems");
        return new NotificationConversation(recipient, conversationId, list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof NotificationConversation)) {
            return false;
        }
        NotificationConversation notificationConversation = (NotificationConversation) obj;
        return Intrinsics.areEqual(this.recipient, notificationConversation.recipient) && Intrinsics.areEqual(this.thread, notificationConversation.thread) && Intrinsics.areEqual(this.notificationItems, notificationConversation.notificationItems);
    }

    public int hashCode() {
        return (((this.recipient.hashCode() * 31) + this.thread.hashCode()) * 31) + this.notificationItems.hashCode();
    }

    /* JADX DEBUG: Multi-variable search result rejected for r8v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.notifications.v2.NotificationItemV2> */
    /* JADX WARN: Multi-variable type inference failed */
    public NotificationConversation(Recipient recipient, ConversationId conversationId, List<? extends NotificationItemV2> list) {
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        Intrinsics.checkNotNullParameter(conversationId, ThreadDatabase.TABLE_NAME);
        Intrinsics.checkNotNullParameter(list, "notificationItems");
        this.recipient = recipient;
        this.thread = conversationId;
        this.notificationItems = list;
        NotificationItemV2 notificationItemV2 = (NotificationItemV2) CollectionsKt___CollectionsKt.last((List) ((List<? extends Object>) list));
        this.mostRecentNotification = notificationItemV2;
        this.notificationId = NotificationIds.getNotificationIdForThread(conversationId);
        this.sortKey = Long.MAX_VALUE - notificationItemV2.getTimestamp();
        int size = list.size();
        this.messageCount = size;
        this.isGroup = recipient.isGroup();
        boolean z = true;
        this.isOnlyContactJoinedEvent = (size != 1 || !notificationItemV2.isJoined()) ? false : z;
    }

    public final Recipient getRecipient() {
        return this.recipient;
    }

    public final ConversationId getThread() {
        return this.thread;
    }

    public final List<NotificationItemV2> getNotificationItems() {
        return this.notificationItems;
    }

    public final NotificationItemV2 getMostRecentNotification() {
        return this.mostRecentNotification;
    }

    public final int getNotificationId() {
        return this.notificationId;
    }

    public final long getSortKey() {
        return this.sortKey;
    }

    public final int getMessageCount() {
        return this.messageCount;
    }

    public final boolean isGroup() {
        return this.isGroup;
    }

    public final boolean isOnlyContactJoinedEvent() {
        return this.isOnlyContactJoinedEvent;
    }

    public final CharSequence getContentTitle(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (SignalStore.settings().getMessageNotificationsPrivacy().isDisplayContact()) {
            return getDisplayName(context);
        }
        String string = context.getString(R.string.SingleRecipientNotificationBuilder_signal);
        Intrinsics.checkNotNullExpressionValue(string, "{\n      context.getStrin…tionBuilder_signal)\n    }");
        return string;
    }

    public final Drawable getContactLargeIcon(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (SignalStore.settings().getMessageNotificationsPrivacy().isDisplayContact()) {
            return NotificationExtensionsKt.getContactDrawable(this.recipient, context);
        }
        return new GeneratedContactPhoto("Unknown", R.drawable.ic_profile_outline_40).asDrawable(context, AvatarColor.UNKNOWN);
    }

    public final Uri getSlideBigPictureUri(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (this.notificationItems.size() != 1 || !SignalStore.settings().getMessageNotificationsPrivacy().isDisplayMessage() || KeyCachingService.isLocked(context)) {
            return null;
        }
        return this.mostRecentNotification.getBigPictureUri();
    }

    public final CharSequence getContentText(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        NotificationPrivacyPreference messageNotificationsPrivacy = SignalStore.settings().getMessageNotificationsPrivacy();
        Intrinsics.checkNotNullExpressionValue(messageNotificationsPrivacy, "settings().messageNotificationsPrivacy");
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        if (messageNotificationsPrivacy.isDisplayContact() && this.recipient.isGroup()) {
            spannableStringBuilder.append(Util.getBoldedString(this.mostRecentNotification.getIndividualRecipient().getDisplayName(context) + ": "));
        }
        if (messageNotificationsPrivacy.isDisplayMessage()) {
            return spannableStringBuilder.append(this.mostRecentNotification.getPrimaryText(context));
        }
        return spannableStringBuilder.append((CharSequence) context.getString(R.string.SingleRecipientNotificationBuilder_new_message));
    }

    public final CharSequence getConversationTitle(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (!SignalStore.settings().getMessageNotificationsPrivacy().isDisplayContact()) {
            return context.getString(R.string.SingleRecipientNotificationBuilder_signal);
        }
        if (this.isGroup) {
            return getDisplayName(context);
        }
        return null;
    }

    public final long getWhen() {
        return this.mostRecentNotification.getTimestamp();
    }

    public final boolean hasNewNotifications() {
        List<NotificationItemV2> list = this.notificationItems;
        if ((list instanceof Collection) && list.isEmpty()) {
            return false;
        }
        for (NotificationItemV2 notificationItemV2 : list) {
            if (notificationItemV2.isNewNotification()) {
                return true;
            }
        }
        return false;
    }

    public final String getChannelId(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (this.isOnlyContactJoinedEvent) {
            return NotificationChannels.JOIN_EVENTS;
        }
        String notificationChannel = this.recipient.getNotificationChannel();
        String messagesChannel = notificationChannel == null ? NotificationChannels.getMessagesChannel(context) : notificationChannel;
        Intrinsics.checkNotNullExpressionValue(messagesChannel, "{\n      recipient.notifi…gesChannel(context)\n    }");
        return messagesChannel;
    }

    public final boolean hasSameContent(NotificationConversation notificationConversation) {
        boolean z;
        if (notificationConversation == null || this.messageCount != notificationConversation.messageCount) {
            return false;
        }
        List<Pair> list = CollectionsKt___CollectionsKt.zip(this.notificationItems, notificationConversation.notificationItems);
        if (!(list instanceof Collection) || !list.isEmpty()) {
            for (Pair pair : list) {
                if (!((NotificationItemV2) pair.component1()).hasSameContent((NotificationItemV2) pair.component2())) {
                    z = false;
                    break;
                }
            }
        }
        z = true;
        return z;
    }

    public final PendingIntent getPendingIntent(Context context) {
        Intent intent;
        Intrinsics.checkNotNullParameter(context, "context");
        if (this.thread.getGroupStoryId() != null) {
            StoryViewerActivity.Companion companion = StoryViewerActivity.Companion;
            RecipientId id = this.recipient.getId();
            Long groupStoryId = this.thread.getGroupStoryId();
            boolean shouldHideStory = this.recipient.shouldHideStory();
            int startingPosition = this.mostRecentNotification.getStartingPosition(context);
            Intrinsics.checkNotNullExpressionValue(id, ContactRepository.ID_COLUMN);
            intent = companion.createIntent(context, new StoryViewerArgs(id, shouldHideStory, groupStoryId.longValue(), null, null, null, null, true, startingPosition, false, false, false, false, 7800, null));
        } else {
            intent = ConversationIntents.createBuilder(context, this.recipient.getId(), this.thread.getThreadId()).withStartingPosition(this.mostRecentNotification.getStartingPosition(context)).build();
            Intrinsics.checkNotNullExpressionValue(intent, "{\n      ConversationInte…))\n        .build()\n    }");
        }
        Intent makeUniqueToPreventMerging = NotificationExtensionsKt.makeUniqueToPreventMerging(intent);
        try {
            return TaskStackBuilder.create(context).addNextIntentWithParentStack(makeUniqueToPreventMerging).getPendingIntent(0, 134217728);
        } catch (NullPointerException e) {
            Log.w(NotificationFactory.INSTANCE.getTAG(), "Vivo device quirk sometimes throws NPE", e);
            makeUniqueToPreventMerging.addFlags(SQLiteDatabase.CREATE_IF_NECESSARY);
            return PendingIntent.getActivity(context, 0, makeUniqueToPreventMerging, PendingIntentFlags.INSTANCE.updateCurrent());
        }
    }

    public final PendingIntent getDeleteIntent(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        int size = this.notificationItems.size();
        long[] jArr = new long[size];
        boolean[] zArr = new boolean[size];
        int i = 0;
        for (Object obj : this.notificationItems) {
            int i2 = i + 1;
            if (i < 0) {
                CollectionsKt__CollectionsKt.throwIndexOverflow();
            }
            NotificationItemV2 notificationItemV2 = (NotificationItemV2) obj;
            jArr[i] = notificationItemV2.getId();
            zArr[i] = notificationItemV2.isMms();
            i = i2;
        }
        Intent putParcelableArrayListExtra = new Intent(context, DeleteNotificationReceiver.class).setAction(DeleteNotificationReceiver.DELETE_NOTIFICATION_ACTION).putExtra(DeleteNotificationReceiver.EXTRA_IDS, jArr).putExtra("is_mms", zArr).putParcelableArrayListExtra("threads", CollectionsKt__CollectionsKt.arrayListOf(this.thread));
        Intrinsics.checkNotNullExpressionValue(putParcelableArrayListExtra, "Intent(context, DeleteNo…ADS, arrayListOf(thread))");
        return PendingIntent.getBroadcast(context, 0, NotificationExtensionsKt.makeUniqueToPreventMerging(putParcelableArrayListExtra), PendingIntentFlags.INSTANCE.updateCurrent());
    }

    public final PendingIntent getMarkAsReadIntent(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intent putExtra = new Intent(context, MarkReadReceiver.class).setAction(MarkReadReceiver.CLEAR_ACTION).putParcelableArrayListExtra("threads", CollectionsKt__CollectionsKt.arrayListOf(this.mostRecentNotification.getThread())).putExtra(MarkReadReceiver.NOTIFICATION_ID_EXTRA, this.notificationId);
        Intrinsics.checkNotNullExpressionValue(putExtra, "Intent(context, MarkRead…ID_EXTRA, notificationId)");
        PendingIntent broadcast = PendingIntent.getBroadcast(context, (int) (this.thread.getThreadId() * ((long) 2)), NotificationExtensionsKt.makeUniqueToPreventMerging(putExtra), PendingIntentFlags.INSTANCE.updateCurrent());
        Intrinsics.checkNotNullExpressionValue(broadcast, "getBroadcast(context, (t…entFlags.updateCurrent())");
        return broadcast;
    }

    public final PendingIntent getQuickReplyIntent(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intent build = ConversationIntents.createPopUpBuilder(context, this.recipient.getId(), this.mostRecentNotification.getThread().getThreadId()).build();
        Intrinsics.checkNotNullExpressionValue(build, "createPopUpBuilder(conte….threadId)\n      .build()");
        PendingIntent activity = PendingIntent.getActivity(context, ((int) (this.thread.getThreadId() * ((long) 2))) + 1, NotificationExtensionsKt.makeUniqueToPreventMerging(build), PendingIntentFlags.INSTANCE.updateCurrent());
        Intrinsics.checkNotNullExpressionValue(activity, "getActivity(context, (th…entFlags.updateCurrent())");
        return activity;
    }

    public final PendingIntent getRemoteReplyIntent(Context context, ReplyMethod replyMethod) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(replyMethod, "replyMethod");
        Intent putExtra = new Intent(context, RemoteReplyReceiver.class).setAction(RemoteReplyReceiver.REPLY_ACTION).putExtra(RemoteReplyReceiver.RECIPIENT_EXTRA, this.recipient.getId()).putExtra(RemoteReplyReceiver.REPLY_METHOD, replyMethod).putExtra(RemoteReplyReceiver.EARLIEST_TIMESTAMP, ((NotificationItemV2) CollectionsKt___CollectionsKt.first((List) ((List<? extends Object>) this.notificationItems))).getTimestamp());
        Long groupStoryId = ((NotificationItemV2) CollectionsKt___CollectionsKt.first((List) ((List<? extends Object>) this.notificationItems))).getThread().getGroupStoryId();
        Intent putExtra2 = putExtra.putExtra(RemoteReplyReceiver.GROUP_STORY_ID_EXTRA, groupStoryId != null ? groupStoryId.longValue() : Long.MIN_VALUE);
        Intrinsics.checkNotNullExpressionValue(putExtra2, "Intent(context, RemoteRe…toryId ?: Long.MIN_VALUE)");
        PendingIntent broadcast = PendingIntent.getBroadcast(context, ((int) (this.thread.getThreadId() * ((long) 2))) + 1, NotificationExtensionsKt.makeUniqueToPreventMerging(putExtra2), PendingIntentFlags.INSTANCE.updateCurrent());
        Intrinsics.checkNotNullExpressionValue(broadcast, "getBroadcast(context, (t…entFlags.updateCurrent())");
        return broadcast;
    }

    public final PendingIntent getTurnOffJoinedNotificationsIntent(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        PendingIntent activity = PendingIntent.getActivity(context, 0, TurnOffContactJoinedNotificationsActivity.newIntent(context, this.thread.getThreadId()), PendingIntentFlags.INSTANCE.updateCurrent());
        Intrinsics.checkNotNullExpressionValue(activity, "getActivity(\n      conte…ags.updateCurrent()\n    )");
        return activity;
    }

    private final String getDisplayName(Context context) {
        if (this.thread.getGroupStoryId() != null) {
            String string = context.getString(R.string.SingleRecipientNotificationBuilder__s_dot_story, this.recipient.getDisplayName(context));
            Intrinsics.checkNotNullExpressionValue(string, "{\n      context.getStrin…splayName(context))\n    }");
            return string;
        }
        String displayName = this.recipient.getDisplayName(context);
        Intrinsics.checkNotNullExpressionValue(displayName, "{\n      recipient.getDisplayName(context)\n    }");
        return displayName;
    }

    public String toString() {
        return "NotificationConversation(thread=" + this.thread + ", notificationItems=" + this.notificationItems + ", messageCount=" + this.messageCount + ", hasNewNotifications=" + hasNewNotifications() + ')';
    }
}
