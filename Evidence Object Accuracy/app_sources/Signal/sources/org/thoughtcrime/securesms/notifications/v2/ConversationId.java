package org.thoughtcrime.securesms.notifications.v2;

import android.os.Parcel;
import android.os.Parcelable;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.database.model.ParentStoryId;

/* compiled from: ConversationId.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 \u001d2\u00020\u0001:\u0001\u001dB\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\u0010\u0010\f\u001a\u0004\u0018\u00010\u0003HÆ\u0003¢\u0006\u0002\u0010\u0007J$\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003HÆ\u0001¢\u0006\u0002\u0010\u000eJ\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0010HÖ\u0001J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001J\u0019\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0010HÖ\u0001R\u0015\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\n\n\u0002\u0010\b\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/ConversationId;", "Landroid/os/Parcelable;", "threadId", "", "groupStoryId", "(JLjava/lang/Long;)V", "getGroupStoryId", "()Ljava/lang/Long;", "Ljava/lang/Long;", "getThreadId", "()J", "component1", "component2", "copy", "(JLjava/lang/Long;)Lorg/thoughtcrime/securesms/notifications/v2/ConversationId;", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ConversationId implements Parcelable {
    public static final Parcelable.Creator<ConversationId> CREATOR = new Creator();
    public static final Companion Companion = new Companion(null);
    private final Long groupStoryId;
    private final long threadId;

    /* compiled from: ConversationId.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Creator implements Parcelable.Creator<ConversationId> {
        @Override // android.os.Parcelable.Creator
        public final ConversationId createFromParcel(Parcel parcel) {
            Intrinsics.checkNotNullParameter(parcel, "parcel");
            return new ConversationId(parcel.readLong(), parcel.readInt() == 0 ? null : Long.valueOf(parcel.readLong()));
        }

        @Override // android.os.Parcelable.Creator
        public final ConversationId[] newArray(int i) {
            return new ConversationId[i];
        }
    }

    public static /* synthetic */ ConversationId copy$default(ConversationId conversationId, long j, Long l, int i, Object obj) {
        if ((i & 1) != 0) {
            j = conversationId.threadId;
        }
        if ((i & 2) != 0) {
            l = conversationId.groupStoryId;
        }
        return conversationId.copy(j, l);
    }

    @JvmStatic
    public static final ConversationId forConversation(long j) {
        return Companion.forConversation(j);
    }

    @JvmStatic
    public static final ConversationId fromMessageRecord(MessageRecord messageRecord) {
        return Companion.fromMessageRecord(messageRecord);
    }

    @JvmStatic
    public static final ConversationId fromThreadAndReply(long j, ParentStoryId.GroupReply groupReply) {
        return Companion.fromThreadAndReply(j, groupReply);
    }

    public final long component1() {
        return this.threadId;
    }

    public final Long component2() {
        return this.groupStoryId;
    }

    public final ConversationId copy(long j, Long l) {
        return new ConversationId(j, l);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ConversationId)) {
            return false;
        }
        ConversationId conversationId = (ConversationId) obj;
        return this.threadId == conversationId.threadId && Intrinsics.areEqual(this.groupStoryId, conversationId.groupStoryId);
    }

    @Override // java.lang.Object
    public int hashCode() {
        int m = SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.threadId) * 31;
        Long l = this.groupStoryId;
        return m + (l == null ? 0 : l.hashCode());
    }

    @Override // java.lang.Object
    public String toString() {
        return "ConversationId(threadId=" + this.threadId + ", groupStoryId=" + this.groupStoryId + ')';
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkNotNullParameter(parcel, "out");
        parcel.writeLong(this.threadId);
        Long l = this.groupStoryId;
        if (l == null) {
            parcel.writeInt(0);
            return;
        }
        parcel.writeInt(1);
        parcel.writeLong(l.longValue());
    }

    public ConversationId(long j, Long l) {
        this.threadId = j;
        this.groupStoryId = l;
    }

    public final long getThreadId() {
        return this.threadId;
    }

    public final Long getGroupStoryId() {
        return this.groupStoryId;
    }

    /* compiled from: ConversationId.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\tH\u0007J\u001a\u0010\n\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0007¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/ConversationId$Companion;", "", "()V", "forConversation", "Lorg/thoughtcrime/securesms/notifications/v2/ConversationId;", "threadId", "", "fromMessageRecord", "record", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "fromThreadAndReply", "groupReply", "Lorg/thoughtcrime/securesms/database/model/ParentStoryId$GroupReply;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final ConversationId forConversation(long j) {
            return new ConversationId(j, null);
        }

        @JvmStatic
        public final ConversationId fromMessageRecord(MessageRecord messageRecord) {
            Intrinsics.checkNotNullParameter(messageRecord, "record");
            long threadId = messageRecord.getThreadId();
            Long l = null;
            MmsMessageRecord mmsMessageRecord = messageRecord instanceof MmsMessageRecord ? (MmsMessageRecord) messageRecord : null;
            ParentStoryId parentStoryId = mmsMessageRecord != null ? mmsMessageRecord.getParentStoryId() : null;
            ParentStoryId.GroupReply groupReply = parentStoryId instanceof ParentStoryId.GroupReply ? (ParentStoryId.GroupReply) parentStoryId : null;
            if (groupReply != null) {
                l = Long.valueOf(groupReply.serialize());
            }
            return new ConversationId(threadId, l);
        }

        @JvmStatic
        public final ConversationId fromThreadAndReply(long j, ParentStoryId.GroupReply groupReply) {
            return new ConversationId(j, groupReply != null ? Long.valueOf(groupReply.serialize()) : null);
        }
    }
}
