package org.thoughtcrime.securesms.notifications.v2;

import android.content.Context;
import android.net.Uri;
import android.text.Spannable;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.contactshare.ContactUtil;
import org.thoughtcrime.securesms.database.MentionUtil;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.ThreadBodyUtil;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.SlideDeck;
import org.thoughtcrime.securesms.notifications.v2.NotificationItemV2;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.service.KeyCachingService;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.MessageRecordUtil;
import org.thoughtcrime.securesms.util.SpanUtil;

/* compiled from: NotificationItemV2.kt */
@Metadata(d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u0011\u001a\u00020\u000b2\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\n\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016J\n\u0010\u0016\u001a\u0004\u0018\u00010\u0015H\u0016J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0012\u001a\u00020\u0013H\u0014J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0010\u0010\u001d\u001a\u00020\u001a2\u0006\u0010\u001e\u001a\u00020\u001fH\u0003J\b\u0010 \u001a\u00020!H\u0016R\u0014\u0010\u0007\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\fR\u0014\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010¨\u0006\""}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/MessageNotification;", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationItemV2;", "threadRecipient", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "record", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "(Lorg/thoughtcrime/securesms/recipients/Recipient;Lorg/thoughtcrime/securesms/database/model/MessageRecord;)V", "individualRecipient", "getIndividualRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "isNewNotification", "", "()Z", "timestamp", "", "getTimestamp", "()J", "canReply", "context", "Landroid/content/Context;", "getBigPictureUri", "Landroid/net/Uri;", "getLargeIconUri", "getPrimaryTextActual", "", "getStartingPosition", "", "getThumbnailInfo", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationItemV2$ThumbnailInfo;", "getViewOnceDescription", "messageRecord", "Lorg/thoughtcrime/securesms/database/model/MmsMessageRecord;", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MessageNotification extends NotificationItemV2 {
    private final Recipient individualRecipient;
    private final boolean isNewNotification;
    private final long timestamp;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public MessageNotification(Recipient recipient, MessageRecord messageRecord) {
        super(recipient, messageRecord, null);
        String str;
        Recipient recipient2;
        Intrinsics.checkNotNullParameter(recipient, "threadRecipient");
        Intrinsics.checkNotNullParameter(messageRecord, "record");
        this.timestamp = messageRecord.getTimestamp();
        if (messageRecord.isOutgoing()) {
            recipient2 = Recipient.self();
            str = "self()";
        } else {
            recipient2 = messageRecord.getIndividualRecipient().resolve();
            str = "record.individualRecipient.resolve()";
        }
        Intrinsics.checkNotNullExpressionValue(recipient2, str);
        this.individualRecipient = recipient2;
        this.isNewNotification = getNotifiedTimestamp() == 0;
    }

    @Override // org.thoughtcrime.securesms.notifications.v2.NotificationItemV2
    public long getTimestamp() {
        return this.timestamp;
    }

    @Override // org.thoughtcrime.securesms.notifications.v2.NotificationItemV2
    public Recipient getIndividualRecipient() {
        return this.individualRecipient;
    }

    @Override // org.thoughtcrime.securesms.notifications.v2.NotificationItemV2
    public boolean isNewNotification() {
        return this.isNewNotification;
    }

    @Override // org.thoughtcrime.securesms.notifications.v2.NotificationItemV2
    protected CharSequence getPrimaryTextActual(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (KeyCachingService.isLocked(context)) {
            CharSequence italic = SpanUtil.italic(context.getString(R.string.MessageNotifier_locked_message));
            Intrinsics.checkNotNullExpressionValue(italic, "{\n      SpanUtil.italic(…er_locked_message))\n    }");
            return italic;
        }
        if (getRecord().isMms()) {
            List<Contact> sharedContacts = ((MmsMessageRecord) getRecord()).getSharedContacts();
            Intrinsics.checkNotNullExpressionValue(sharedContacts, "record as MmsMessageRecord).sharedContacts");
            if (!sharedContacts.isEmpty()) {
                CharSequence stringSummary = ContactUtil.getStringSummary(context, ((MmsMessageRecord) getRecord()).getSharedContacts().get(0));
                Intrinsics.checkNotNullExpressionValue(stringSummary, "{\n      val contact = re…y(context, contact)\n    }");
                return stringSummary;
            }
        }
        if (getRecord().isMms() && getRecord().isViewOnce()) {
            CharSequence italic2 = SpanUtil.italic(context.getString(getViewOnceDescription((MmsMessageRecord) getRecord())));
            Intrinsics.checkNotNullExpressionValue(italic2, "{\n      SpanUtil.italic(…MmsMessageRecord)))\n    }");
            return italic2;
        } else if (getRecord().isRemoteDelete()) {
            CharSequence italic3 = SpanUtil.italic(context.getString(R.string.MessageNotifier_this_message_was_deleted));
            Intrinsics.checkNotNullExpressionValue(italic3, "{\n      SpanUtil.italic(…ssage_was_deleted))\n    }");
            return italic3;
        } else {
            if (getRecord().isMms() && !getRecord().isMmsNotification()) {
                List<Slide> slides = ((MmsMessageRecord) getRecord()).getSlideDeck().getSlides();
                Intrinsics.checkNotNullExpressionValue(slides, "record as MmsMessageRecord).slideDeck.slides");
                if (!slides.isEmpty()) {
                    String formattedBodyFor = ThreadBodyUtil.getFormattedBodyFor(context, getRecord());
                    Intrinsics.checkNotNullExpressionValue(formattedBodyFor, "{\n      ThreadBodyUtil.g…or(context, record)\n    }");
                    return formattedBodyFor;
                }
            }
            if (getRecord().isGroupCall()) {
                Spannable spannable = MessageRecord.getGroupCallUpdateDescription(context, getRecord().getBody(), false).getSpannable();
                Intrinsics.checkNotNullExpressionValue(spannable, "{\n      MessageRecord.ge…y, false).spannable\n    }");
                return spannable;
            } else if (MessageRecordUtil.hasGiftBadge(getRecord())) {
                String formattedBodyFor2 = ThreadBodyUtil.getFormattedBodyFor(context, getRecord());
                Intrinsics.checkNotNullExpressionValue(formattedBodyFor2, "{\n      ThreadBodyUtil.g…or(context, record)\n    }");
                return formattedBodyFor2;
            } else {
                CharSequence updateBodyWithDisplayNames = MentionUtil.updateBodyWithDisplayNames(context, getRecord());
                Intrinsics.checkNotNullExpressionValue(updateBodyWithDisplayNames, "{\n      MentionUtil.upda…es(context, record)\n    }");
                return updateBodyWithDisplayNames;
            }
        }
    }

    private final int getViewOnceDescription(MmsMessageRecord mmsMessageRecord) {
        return MediaUtil.isImageType(getMessageContentType(mmsMessageRecord)) ? R.string.MessageNotifier_view_once_photo : R.string.MessageNotifier_view_once_video;
    }

    @Override // org.thoughtcrime.securesms.notifications.v2.NotificationItemV2
    public int getStartingPosition(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (getThread().getGroupStoryId() != null) {
            return SignalDatabase.Companion.mmsSms().getMessagePositionInConversation(getThread().getThreadId(), getThread().getGroupStoryId().longValue(), getRecord().getDateReceived());
        }
        return -1;
    }

    @Override // org.thoughtcrime.securesms.notifications.v2.NotificationItemV2
    public Uri getLargeIconUri() {
        Slide slide;
        SlideDeck slideDeck = getSlideDeck();
        if (slideDeck == null || (slide = slideDeck.getThumbnailSlide()) == null) {
            SlideDeck slideDeck2 = getSlideDeck();
            slide = slideDeck2 != null ? slideDeck2.getStickerSlide() : null;
        }
        boolean z = false;
        if (slide != null && !slide.isInProgress()) {
            z = true;
        }
        if (z) {
            return slide.getUri();
        }
        return null;
    }

    @Override // org.thoughtcrime.securesms.notifications.v2.NotificationItemV2
    public Uri getBigPictureUri() {
        SlideDeck slideDeck = getSlideDeck();
        Slide thumbnailSlide = slideDeck != null ? slideDeck.getThumbnailSlide() : null;
        boolean z = false;
        if (thumbnailSlide != null && !thumbnailSlide.isInProgress()) {
            z = true;
        }
        if (z) {
            return thumbnailSlide.getUri();
        }
        return null;
    }

    @Override // org.thoughtcrime.securesms.notifications.v2.NotificationItemV2
    public NotificationItemV2.ThumbnailInfo getThumbnailInfo(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        String str = null;
        if (!SignalStore.settings().getMessageNotificationsPrivacy().isDisplayMessage() || KeyCachingService.isLocked(context)) {
            return new NotificationItemV2.ThumbnailInfo(null, null, 3, null);
        }
        SlideDeck slideDeck = getSlideDeck();
        Slide thumbnailSlide = slideDeck != null ? slideDeck.getThumbnailSlide() : null;
        Uri publicUri = thumbnailSlide != null ? thumbnailSlide.getPublicUri() : null;
        if (thumbnailSlide != null) {
            str = thumbnailSlide.getContentType();
        }
        return new NotificationItemV2.ThumbnailInfo(publicUri, str);
    }

    @Override // org.thoughtcrime.securesms.notifications.v2.NotificationItemV2
    public boolean canReply(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (KeyCachingService.isLocked(context) || getRecord().isRemoteDelete() || getRecord().isGroupCall() || getRecord().isViewOnce() || getRecord().isJoined()) {
            return false;
        }
        if (!(getRecord() instanceof MmsMessageRecord)) {
            return true;
        }
        if ((((MmsMessageRecord) getRecord()).isMmsNotification() || ((MmsMessageRecord) getRecord()).getSlideDeck().getSlides().isEmpty()) && ((MmsMessageRecord) getRecord()).getSharedContacts().isEmpty()) {
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public String toString() {
        return "MessageNotification(timestamp=" + getTimestamp() + ", isNewNotification=" + isNewNotification() + ')';
    }
}
