package org.thoughtcrime.securesms.notifications;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import j$.util.Optional;
import org.signal.core.util.ExceptionUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.notifications.v2.MessageNotifierV2;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.BubbleUtil;
import org.thoughtcrime.securesms.util.LeakyBucketLimiter;

/* loaded from: classes4.dex */
public class OptimizedMessageNotifier implements MessageNotifier {
    private final LeakyBucketLimiter limiter = new LeakyBucketLimiter(5, 1000, new Handler(SignalExecutors.getAndStartHandlerThread("signal-notifier").getLooper()));
    private final MessageNotifierV2 messageNotifierV2;

    public OptimizedMessageNotifier(Application application) {
        this.messageNotifierV2 = new MessageNotifierV2(application);
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void setVisibleThread(ConversationId conversationId) {
        getNotifier().setVisibleThread(conversationId);
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public Optional<ConversationId> getVisibleThread() {
        return getNotifier().getVisibleThread();
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void clearVisibleThread() {
        getNotifier().clearVisibleThread();
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void setLastDesktopActivityTimestamp(long j) {
        getNotifier().setLastDesktopActivityTimestamp(j);
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void notifyMessageDeliveryFailed(Context context, Recipient recipient, ConversationId conversationId) {
        getNotifier().notifyMessageDeliveryFailed(context, recipient, conversationId);
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void notifyProofRequired(Context context, Recipient recipient, ConversationId conversationId) {
        getNotifier().notifyProofRequired(context, recipient, conversationId);
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void cancelDelayedNotifications() {
        getNotifier().cancelDelayedNotifications();
    }

    public /* synthetic */ void lambda$updateNotification$0(Context context) {
        getNotifier().updateNotification(context);
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void updateNotification(Context context) {
        runOnLimiter(new Runnable(context) { // from class: org.thoughtcrime.securesms.notifications.OptimizedMessageNotifier$$ExternalSyntheticLambda3
            public final /* synthetic */ Context f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                OptimizedMessageNotifier.this.lambda$updateNotification$0(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$updateNotification$1(Context context, ConversationId conversationId) {
        getNotifier().updateNotification(context, conversationId);
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void updateNotification(Context context, ConversationId conversationId) {
        runOnLimiter(new Runnable(context, conversationId) { // from class: org.thoughtcrime.securesms.notifications.OptimizedMessageNotifier$$ExternalSyntheticLambda4
            public final /* synthetic */ Context f$1;
            public final /* synthetic */ ConversationId f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                OptimizedMessageNotifier.this.lambda$updateNotification$1(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$updateNotification$2(Context context, ConversationId conversationId, BubbleUtil.BubbleState bubbleState) {
        getNotifier().updateNotification(context, conversationId, bubbleState);
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void updateNotification(Context context, ConversationId conversationId, BubbleUtil.BubbleState bubbleState) {
        runOnLimiter(new Runnable(context, conversationId, bubbleState) { // from class: org.thoughtcrime.securesms.notifications.OptimizedMessageNotifier$$ExternalSyntheticLambda2
            public final /* synthetic */ Context f$1;
            public final /* synthetic */ ConversationId f$2;
            public final /* synthetic */ BubbleUtil.BubbleState f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                OptimizedMessageNotifier.this.lambda$updateNotification$2(this.f$1, this.f$2, this.f$3);
            }
        });
    }

    public /* synthetic */ void lambda$updateNotification$3(Context context, ConversationId conversationId, boolean z) {
        getNotifier().updateNotification(context, conversationId, z);
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void updateNotification(Context context, ConversationId conversationId, boolean z) {
        runOnLimiter(new Runnable(context, conversationId, z) { // from class: org.thoughtcrime.securesms.notifications.OptimizedMessageNotifier$$ExternalSyntheticLambda1
            public final /* synthetic */ Context f$1;
            public final /* synthetic */ ConversationId f$2;
            public final /* synthetic */ boolean f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                OptimizedMessageNotifier.this.lambda$updateNotification$3(this.f$1, this.f$2, this.f$3);
            }
        });
    }

    public /* synthetic */ void lambda$updateNotification$4(Context context, ConversationId conversationId, boolean z, int i, BubbleUtil.BubbleState bubbleState) {
        getNotifier().updateNotification(context, conversationId, z, i, bubbleState);
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void updateNotification(Context context, ConversationId conversationId, boolean z, int i, BubbleUtil.BubbleState bubbleState) {
        runOnLimiter(new Runnable(context, conversationId, z, i, bubbleState) { // from class: org.thoughtcrime.securesms.notifications.OptimizedMessageNotifier$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$1;
            public final /* synthetic */ ConversationId f$2;
            public final /* synthetic */ boolean f$3;
            public final /* synthetic */ int f$4;
            public final /* synthetic */ BubbleUtil.BubbleState f$5;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
            }

            @Override // java.lang.Runnable
            public final void run() {
                OptimizedMessageNotifier.this.lambda$updateNotification$4(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5);
            }
        });
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void clearReminder(Context context) {
        getNotifier().clearReminder(context);
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void addStickyThread(ConversationId conversationId, long j) {
        getNotifier().addStickyThread(conversationId, j);
    }

    @Override // org.thoughtcrime.securesms.notifications.MessageNotifier
    public void removeStickyThread(ConversationId conversationId) {
        getNotifier().removeStickyThread(conversationId);
    }

    private void runOnLimiter(Runnable runnable) {
        this.limiter.run(new Runnable(runnable, new Throwable()) { // from class: org.thoughtcrime.securesms.notifications.OptimizedMessageNotifier$$ExternalSyntheticLambda5
            public final /* synthetic */ Runnable f$0;
            public final /* synthetic */ Throwable f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                OptimizedMessageNotifier.lambda$runOnLimiter$5(this.f$0, this.f$1);
            }
        });
    }

    public static /* synthetic */ void lambda$runOnLimiter$5(Runnable runnable, Throwable th) {
        try {
            runnable.run();
        } catch (RuntimeException e) {
            throw ((RuntimeException) ExceptionUtil.joinStackTrace(e, th));
        }
    }

    private MessageNotifier getNotifier() {
        return this.messageNotifierV2;
    }
}
