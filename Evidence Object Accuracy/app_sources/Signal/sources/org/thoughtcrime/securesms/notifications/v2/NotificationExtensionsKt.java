package org.thoughtcrime.securesms.notifications.v2;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.service.notification.StatusBarNotification;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contacts.avatars.ContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.GeneratedContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.ProfileContactPhoto;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.BitmapUtil;
import org.thoughtcrime.securesms.util.BlurTransformation;

/* compiled from: NotificationExtensions.kt */
@Metadata(d1 = {"\u0000:\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\u001a\u0014\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\u0012\u0010\u0005\u001a\u00020\u0006*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\n\u0010\u0007\u001a\u00020\b*\u00020\t\u001a\n\u0010\n\u001a\u00020\u000b*\u00020\u000b\u001a\u001a\u0010\f\u001a\u00020\r*\u00020\u000e2\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u0010\u001a\u0016\u0010\u0011\u001a\u0004\u0018\u00010\r*\u0004\u0018\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004¨\u0006\u0012"}, d2 = {"getContactDrawable", "Landroid/graphics/drawable/Drawable;", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "context", "Landroid/content/Context;", "getFallback", "Lorg/thoughtcrime/securesms/contacts/avatars/FallbackContactPhoto;", "isDisplayingSummaryNotification", "", "Landroid/app/NotificationManager;", "makeUniqueToPreventMerging", "Landroid/content/Intent;", "toBitmap", "Landroid/graphics/Bitmap;", "Landroid/net/Uri;", "dimension", "", "toLargeBitmap", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationExtensionsKt {
    public static final Bitmap toLargeBitmap(Drawable drawable, Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (drawable == null) {
            return null;
        }
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(R.dimen.contact_photo_target_size);
        return BitmapUtil.createFromDrawable(drawable, dimensionPixelSize, dimensionPixelSize);
    }

    public static final Drawable getContactDrawable(Recipient recipient, Context context) {
        FallbackContactPhoto fallbackContactPhoto;
        Intrinsics.checkNotNullParameter(recipient, "<this>");
        Intrinsics.checkNotNullParameter(context, "context");
        ContactPhoto profileContactPhoto = recipient.isSelf() ? new ProfileContactPhoto(recipient) : recipient.getContactPhoto();
        if (recipient.isSelf()) {
            fallbackContactPhoto = getFallback(recipient, context);
        } else {
            fallbackContactPhoto = recipient.getFallbackContactPhoto();
            Intrinsics.checkNotNullExpressionValue(fallbackContactPhoto, "fallbackContactPhoto");
        }
        if (profileContactPhoto == null) {
            return fallbackContactPhoto.asDrawable(context, recipient.getAvatarColor());
        }
        try {
            ArrayList arrayList = new ArrayList();
            if (recipient.shouldBlurAvatar()) {
                arrayList.add(new BlurTransformation(ApplicationDependencies.getApplication(), 0.25f, 25.0f));
            }
            arrayList.add(new CircleCrop());
            return GlideApp.with(context.getApplicationContext()).load((Object) profileContactPhoto).diskCacheStrategy(DiskCacheStrategy.ALL).transform((Transformation<Bitmap>) new MultiTransformation(arrayList)).submit(context.getResources().getDimensionPixelSize(17104901), context.getResources().getDimensionPixelSize(17104902)).get();
        } catch (InterruptedException unused) {
            return fallbackContactPhoto.asDrawable(context, recipient.getAvatarColor());
        } catch (ExecutionException unused2) {
            return fallbackContactPhoto.asDrawable(context, recipient.getAvatarColor());
        }
    }

    public static final Bitmap toBitmap(Uri uri, Context context, int i) {
        Intrinsics.checkNotNullParameter(uri, "<this>");
        Intrinsics.checkNotNullParameter(context, "context");
        try {
            Bitmap bitmap = GlideApp.with(context.getApplicationContext()).asBitmap().load((Object) new DecryptableStreamUriLoader.DecryptableUri(uri)).diskCacheStrategy(DiskCacheStrategy.NONE).submit(i, i).get();
            Intrinsics.checkNotNullExpressionValue(bitmap, "{\n    GlideApp.with(cont…mension)\n      .get()\n  }");
            return bitmap;
        } catch (InterruptedException unused) {
            Bitmap createBitmap = Bitmap.createBitmap(i, i, Bitmap.Config.RGB_565);
            Intrinsics.checkNotNullExpressionValue(createBitmap, "{\n    Bitmap.createBitma…itmap.Config.RGB_565)\n  }");
            return createBitmap;
        } catch (ExecutionException unused2) {
            Bitmap createBitmap2 = Bitmap.createBitmap(i, i, Bitmap.Config.RGB_565);
            Intrinsics.checkNotNullExpressionValue(createBitmap2, "{\n    Bitmap.createBitma…itmap.Config.RGB_565)\n  }");
            return createBitmap2;
        }
    }

    public static final Intent makeUniqueToPreventMerging(Intent intent) {
        Intrinsics.checkNotNullParameter(intent, "<this>");
        Intent data = intent.setData(Uri.parse("custom://" + System.currentTimeMillis()));
        Intrinsics.checkNotNullExpressionValue(data, "setData((Uri.parse(\"cust…em.currentTimeMillis())))");
        return data;
    }

    public static final FallbackContactPhoto getFallback(Recipient recipient, Context context) {
        Intrinsics.checkNotNullParameter(recipient, "<this>");
        Intrinsics.checkNotNullParameter(context, "context");
        return new GeneratedContactPhoto(recipient.getDisplayName(context), R.drawable.ic_profile_outline_40);
    }

    public static final boolean isDisplayingSummaryNotification(NotificationManager notificationManager) {
        Intrinsics.checkNotNullParameter(notificationManager, "<this>");
        if (Build.VERSION.SDK_INT <= 23) {
            return false;
        }
        try {
            StatusBarNotification[] activeNotifications = notificationManager.getActiveNotifications();
            Intrinsics.checkNotNullExpressionValue(activeNotifications, "activeNotifications");
            for (StatusBarNotification statusBarNotification : activeNotifications) {
                if (statusBarNotification.getId() == 1338) {
                    return true;
                }
            }
            return false;
        } catch (Throwable unused) {
            return false;
        }
    }
}
