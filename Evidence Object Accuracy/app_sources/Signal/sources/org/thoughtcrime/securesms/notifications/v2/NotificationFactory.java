package org.thoughtcrime.securesms.notifications.v2;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.TransactionTooLargeException;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt__MutableCollectionsKt;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.MainActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.ConversationIntents;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.database.model.InMemoryMessageRecord;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.notifications.NotificationIds;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.my.MyStoriesActivity;
import org.thoughtcrime.securesms.util.BubbleUtil;
import org.thoughtcrime.securesms.util.ConversationUtil;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* compiled from: NotificationFactory.kt */
@Metadata(d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J^\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\n2\b\u0010\u0010\u001a\u0004\u0018\u00010\n2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\u0018\u001a\u00020\u000eJX\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\n2\b\u0010\u0010\u001a\u0004\u0018\u00010\n2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\u001a\u001a\u00020\u001bH\u0002Jh\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\n2\b\u0010\u0010\u001a\u0004\u0018\u00010\n2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u0018\u001a\u00020\u000eH\u0003J2\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u001f\u001a\u00020 2\b\u0010\u0010\u001a\u0004\u0018\u00010\n2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010!\u001a\u00020\u0016H\u0002J \u0010\"\u001a\u00020\u001e2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010#\u001a\u00020$2\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J(\u0010%\u001a\u00020\u001e2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010#\u001a\u00020$2\u0006\u0010&\u001a\u00020\n2\b\u0010\u000f\u001a\u0004\u0018\u00010\nJ(\u0010'\u001a\u00020\u001e2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010#\u001a\u00020$2\u0006\u0010&\u001a\u00020\n2\b\u0010\u000f\u001a\u0004\u0018\u00010\nJ\u0018\u0010(\u001a\u00020\u001e2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0002J \u0010)\u001a\u00020\u001e2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010#\u001a\u00020$2\u0006\u0010*\u001a\u00020\u0014H\u0007J.\u0010+\u001a\u00020\u001e*\u00020,2\u0006\u0010\u000b\u001a\u00020\f2\b\u0010-\u001a\u0004\u0018\u00010$2\u0006\u0010.\u001a\u00020\u001b2\u0006\u0010/\u001a\u000200H\u0002R\u0019\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u00061"}, d2 = {"Lorg/thoughtcrime/securesms/notifications/v2/NotificationFactory;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "getTAG", "()Ljava/lang/String;", "notify", "", "Lorg/thoughtcrime/securesms/notifications/v2/ConversationId;", "context", "Landroid/content/Context;", "state", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationStateV2;", "visibleThread", "targetThread", "defaultBubbleState", "Lorg/thoughtcrime/securesms/util/BubbleUtil$BubbleState;", "lastAudibleNotification", "", "notificationConfigurationChanged", "", "alertOverrides", "previousState", "notify19", "nonVisibleThreadCount", "", "notify24", "notifyForConversation", "", "conversation", "Lorg/thoughtcrime/securesms/notifications/v2/NotificationConversation;", "shouldAlert", "notifyInThread", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "notifyMessageDeliveryFailed", ThreadDatabase.TABLE_NAME, "notifyProofRequired", "notifySummary", "notifyToBubbleConversation", "threadId", "safelyNotify", "Landroidx/core/app/NotificationManagerCompat;", "threadRecipient", "notificationId", "notification", "Landroid/app/Notification;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationFactory {
    public static final NotificationFactory INSTANCE = new NotificationFactory();
    private static final String TAG = Log.tag(NotificationFactory.class);

    private NotificationFactory() {
    }

    public final String getTAG() {
        return TAG;
    }

    public final Set<ConversationId> notify(Context context, NotificationStateV2 notificationStateV2, ConversationId conversationId, ConversationId conversationId2, BubbleUtil.BubbleState bubbleState, long j, boolean z, Set<ConversationId> set, NotificationStateV2 notificationStateV22) {
        int i;
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(notificationStateV2, "state");
        Intrinsics.checkNotNullParameter(bubbleState, "defaultBubbleState");
        Intrinsics.checkNotNullParameter(set, "alertOverrides");
        Intrinsics.checkNotNullParameter(notificationStateV22, "previousState");
        if (notificationStateV2.isEmpty()) {
            Log.d(TAG, "State is empty, bailing");
            return SetsKt__SetsKt.emptySet();
        }
        List<NotificationConversation> conversations = notificationStateV2.getConversations();
        int i2 = 0;
        if (!(conversations instanceof Collection) || !conversations.isEmpty()) {
            for (NotificationConversation notificationConversation : conversations) {
                if ((!Intrinsics.areEqual(notificationConversation.getThread(), conversationId)) && (i2 = i2 + 1) < 0) {
                    CollectionsKt__CollectionsKt.throwCountOverflow();
                }
            }
            i = i2;
        } else {
            i = 0;
        }
        if (Build.VERSION.SDK_INT < 24) {
            return notify19(context, notificationStateV2, conversationId, conversationId2, bubbleState, j, set, i);
        }
        return notify24(context, notificationStateV2, conversationId, conversationId2, bubbleState, j, z, set, i, notificationStateV22);
    }

    private final Set<ConversationId> notify19(Context context, NotificationStateV2 notificationStateV2, ConversationId conversationId, ConversationId conversationId2, BubbleUtil.BubbleState bubbleState, long j, Set<ConversationId> set, int i) {
        Object obj;
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        Iterator<T> it = notificationStateV2.getConversations().iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (Intrinsics.areEqual(((NotificationConversation) obj).getThread(), conversationId)) {
                break;
            }
        }
        NotificationConversation notificationConversation = (NotificationConversation) obj;
        if (notificationConversation != null && notificationConversation.hasNewNotifications()) {
            Log.Logger internal = Log.internal();
            String str = TAG;
            internal.i(str, "Thread is visible, notifying in thread. notificationId: " + notificationConversation.getNotificationId());
            INSTANCE.notifyInThread(context, notificationConversation.getRecipient(), j);
        }
        if (i == 1) {
            for (NotificationConversation notificationConversation2 : notificationStateV2.getConversations()) {
                if (!Intrinsics.areEqual(notificationConversation2.getThread(), conversationId)) {
                    INSTANCE.notifyForConversation(context, notificationConversation2, conversationId2, bubbleState, (notificationConversation2.hasNewNotifications() || set.contains(notificationConversation2.getThread())) && !notificationConversation2.getMostRecentNotification().getIndividualRecipient().isSelf());
                    if (notificationConversation2.hasNewNotifications()) {
                        linkedHashSet.add(notificationConversation2.getThread());
                    }
                }
            }
            throw new NoSuchElementException("Collection contains no element matching the predicate.");
        } else if (i > 1) {
            List<NotificationConversation> nonVisibleConversation = notificationStateV2.getNonVisibleConversation(conversationId);
            ArrayList<NotificationConversation> arrayList = new ArrayList();
            for (Object obj2 : nonVisibleConversation) {
                if (((NotificationConversation) obj2).hasNewNotifications()) {
                    arrayList.add(obj2);
                }
            }
            ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10));
            for (NotificationConversation notificationConversation3 : arrayList) {
                arrayList2.add(notificationConversation3.getThread());
            }
            boolean unused = CollectionsKt__MutableCollectionsKt.addAll(linkedHashSet, arrayList2);
            notifySummary(context, NotificationStateV2.copy$default(notificationStateV2, nonVisibleConversation, null, null, 6, null));
            return linkedHashSet;
        }
        return linkedHashSet;
    }

    private final Set<ConversationId> notify24(Context context, NotificationStateV2 notificationStateV2, ConversationId conversationId, ConversationId conversationId2, BubbleUtil.BubbleState bubbleState, long j, boolean z, Set<ConversationId> set, int i, NotificationStateV2 notificationStateV22) {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (NotificationConversation notificationConversation : notificationStateV2.getConversations()) {
            if (!Intrinsics.areEqual(notificationConversation.getThread(), conversationId) || !notificationConversation.hasNewNotifications()) {
                if (!z && !notificationConversation.hasNewNotifications() && !set.contains(notificationConversation.getThread())) {
                    if (!notificationConversation.hasSameContent(notificationStateV22.getConversation(notificationConversation.getThread()))) {
                    }
                }
                if (notificationConversation.hasNewNotifications()) {
                    linkedHashSet.add(notificationConversation.getThread());
                }
                try {
                    INSTANCE.notifyForConversation(context, notificationConversation, conversationId2, bubbleState, (notificationConversation.hasNewNotifications() || set.contains(notificationConversation.getThread())) && !notificationConversation.getMostRecentNotification().getIndividualRecipient().isSelf());
                } catch (SecurityException e) {
                    Log.w(TAG, "Too many pending intents device quirk", e);
                }
            } else {
                Log.Logger internal = Log.internal();
                String str = TAG;
                internal.i(str, "Thread is visible, notifying in thread. notificationId: " + notificationConversation.getNotificationId());
                INSTANCE.notifyInThread(context, notificationConversation.getRecipient(), j);
            }
        }
        if (i <= 1) {
            NotificationManager notificationManager = ServiceUtil.getNotificationManager(context);
            Intrinsics.checkNotNullExpressionValue(notificationManager, "getNotificationManager(context)");
            if (!NotificationExtensionsKt.isDisplayingSummaryNotification(notificationManager)) {
                return linkedHashSet;
            }
        }
        notifySummary(context, NotificationStateV2.copy$default(notificationStateV2, notificationStateV2.getNonVisibleConversation(conversationId), null, null, 6, null));
        return linkedHashSet;
    }

    private final void notifyForConversation(Context context, NotificationConversation notificationConversation, ConversationId conversationId, BubbleUtil.BubbleState bubbleState, boolean z) {
        if (!notificationConversation.getNotificationItems().isEmpty()) {
            NotificationBuilder create = NotificationBuilder.Companion.create(context);
            create.setSmallIcon(R.drawable.ic_notification);
            create.setColor(ContextCompat.getColor(context, R.color.core_ultramarine));
            create.setCategory("msg");
            create.setGroup(MessageNotifierV2.NOTIFICATION_GROUP);
            create.setGroupAlertBehavior(2);
            create.setChannelId(notificationConversation.getChannelId(context));
            create.setContentTitle(notificationConversation.getContentTitle(context));
            create.setLargeIcon(NotificationExtensionsKt.toLargeBitmap(notificationConversation.getContactLargeIcon(context), context));
            create.addPerson(notificationConversation.getRecipient());
            if (notificationConversation.getThread().getGroupStoryId() == null) {
                String shortcutId = ConversationUtil.getShortcutId(notificationConversation.getRecipient());
                Intrinsics.checkNotNullExpressionValue(shortcutId, "getShortcutId(conversation.recipient)");
                create.setShortcutId(shortcutId);
                String shortcutId2 = ConversationUtil.getShortcutId(notificationConversation.getRecipient());
                Intrinsics.checkNotNullExpressionValue(shortcutId2, "getShortcutId(conversation.recipient)");
                create.setLocusId(shortcutId2);
            }
            create.setContentInfo(String.valueOf(notificationConversation.getMessageCount()));
            create.setNumber(notificationConversation.getMessageCount());
            create.setContentText(notificationConversation.getContentText(context));
            create.setContentIntent(notificationConversation.getPendingIntent(context));
            create.setDeleteIntent(notificationConversation.getDeleteIntent(context));
            create.setSortKey(String.valueOf(notificationConversation.getSortKey()));
            create.setWhen(notificationConversation);
            create.addReplyActions(notificationConversation);
            create.setOnlyAlertOnce(!z);
            create.addMessages(notificationConversation);
            create.setPriority(TextSecurePreferences.getNotificationPriority(context));
            create.setLights();
            create.setAlarms(notificationConversation.getRecipient());
            create.setTicker(notificationConversation.getMostRecentNotification().getStyledPrimaryText(context, true));
            if (!Intrinsics.areEqual(conversationId, notificationConversation.getThread())) {
                bubbleState = BubbleUtil.BubbleState.HIDDEN;
            }
            create.setBubbleMetadata(notificationConversation, bubbleState);
            if (notificationConversation.isOnlyContactJoinedEvent()) {
                create.addTurnOffJoinedNotificationsAction(notificationConversation.getTurnOffJoinedNotificationsIntent(context));
            }
            int notificationId = Build.VERSION.SDK_INT < 24 ? NotificationIds.MESSAGE_SUMMARY : notificationConversation.getNotificationId();
            NotificationManagerCompat from = NotificationManagerCompat.from(context);
            Intrinsics.checkNotNullExpressionValue(from, "from(context)");
            safelyNotify(from, context, notificationConversation.getRecipient(), notificationId, create.build());
        }
    }

    private final void notifySummary(Context context, NotificationStateV2 notificationStateV2) {
        if (notificationStateV2.getMessageCount() != 0) {
            NotificationBuilder create = NotificationBuilder.Companion.create(context);
            create.setSmallIcon(R.drawable.ic_notification);
            create.setColor(ContextCompat.getColor(context, R.color.core_ultramarine));
            create.setCategory("msg");
            create.setGroup(MessageNotifierV2.NOTIFICATION_GROUP);
            create.setGroupAlertBehavior(2);
            String messagesChannel = NotificationChannels.getMessagesChannel(context);
            Intrinsics.checkNotNullExpressionValue(messagesChannel, "getMessagesChannel(context)");
            create.setChannelId(messagesChannel);
            String string = context.getString(R.string.app_name);
            Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.string.app_name)");
            create.setContentTitle(string);
            boolean z = false;
            create.setContentIntent(PendingIntent.getActivity(context, 0, MainActivity.clearTop(context), 0));
            create.setGroupSummary(true);
            String string2 = context.getString(R.string.MessageNotifier_d_new_messages_in_d_conversations, Integer.valueOf(notificationStateV2.getMessageCount()), Integer.valueOf(notificationStateV2.getThreadCount()));
            Intrinsics.checkNotNullExpressionValue(string2, "context.getString(R.stri…Count, state.threadCount)");
            create.setSubText(string2);
            create.setContentInfo(String.valueOf(notificationStateV2.getMessageCount()));
            create.setNumber(notificationStateV2.getMessageCount());
            create.setSummaryContentText(notificationStateV2.getMostRecentSender());
            create.setDeleteIntent(notificationStateV2.getDeleteIntent(context));
            create.setWhen(notificationStateV2.getMostRecentNotification());
            create.addMarkAsReadAction(notificationStateV2);
            create.addMessages(notificationStateV2);
            List<NotificationItemV2> notificationItems = notificationStateV2.getNotificationItems();
            if (!(notificationItems instanceof Collection) || !notificationItems.isEmpty()) {
                Iterator<T> it = notificationItems.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (((NotificationItemV2) it.next()).isNewNotification()) {
                            z = true;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            create.setOnlyAlertOnce(!z);
            create.setPriority(TextSecurePreferences.getNotificationPriority(context));
            create.setLights();
            create.setAlarms(notificationStateV2.getMostRecentSender());
            NotificationItemV2 mostRecentNotification = notificationStateV2.getMostRecentNotification();
            create.setTicker(mostRecentNotification != null ? mostRecentNotification.getStyledPrimaryText(context, true) : null);
            Log.d(TAG, "showing summary notification");
            NotificationManagerCompat from = NotificationManagerCompat.from(context);
            Intrinsics.checkNotNullExpressionValue(from, "from(context)");
            safelyNotify(from, context, null, NotificationIds.MESSAGE_SUMMARY, create.build());
        }
    }

    private final void notifyInThread(Context context, Recipient recipient, long j) {
        Uri uri;
        if (SignalStore.settings().isMessageNotificationsInChatSoundsEnabled() && ServiceUtil.getAudioManager(context).getRingerMode() == 2 && System.currentTimeMillis() - j >= MessageNotifierV2.Companion.getMIN_AUDIBLE_PERIOD_MILLIS()) {
            if (NotificationChannels.supported()) {
                uri = NotificationChannels.getMessageRingtone(context, recipient);
                if (uri == null) {
                    uri = NotificationChannels.getMessageRingtone(context);
                }
                Intrinsics.checkNotNullExpressionValue(uri, "{\n      NotificationChan…geRingtone(context)\n    }");
            } else {
                uri = recipient.getMessageRingtone();
                if (uri == null) {
                    uri = SignalStore.settings().getMessageNotificationSound();
                }
                Intrinsics.checkNotNullExpressionValue(uri, "{\n      recipient.messag…geNotificationSound\n    }");
            }
            if (!Intrinsics.areEqual(uri, Uri.EMPTY)) {
                String uri2 = uri.toString();
                Intrinsics.checkNotNullExpressionValue(uri2, "uri.toString()");
                if (!(uri2.length() == 0)) {
                    Ringtone ringtone = RingtoneManager.getRingtone(context, uri);
                    if (ringtone == null) {
                        Log.w(TAG, "ringtone is null");
                        return;
                    }
                    if (Build.VERSION.SDK_INT >= 21) {
                        ringtone.setAudioAttributes(new AudioAttributes.Builder().setContentType(0).setUsage(8).build());
                    } else {
                        ringtone.setStreamType(5);
                    }
                    ringtone.play();
                    return;
                }
            }
            Log.d(TAG, "ringtone uri is empty");
        }
    }

    public final void notifyMessageDeliveryFailed(Context context, Recipient recipient, ConversationId conversationId, ConversationId conversationId2) {
        Intent intent;
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        Intrinsics.checkNotNullParameter(conversationId, ThreadDatabase.TABLE_NAME);
        if (Intrinsics.areEqual(conversationId, conversationId2)) {
            notifyInThread(context, recipient, 0);
            return;
        }
        if (recipient.isDistributionList() || conversationId.getGroupStoryId() != null) {
            intent = new Intent(context, MyStoriesActivity.class);
        } else {
            intent = ConversationIntents.createBuilder(context, recipient.getId(), conversationId.getThreadId()).build();
            Intrinsics.checkNotNullExpressionValue(intent, "{\n      ConversationInte…d)\n        .build()\n    }");
        }
        Intent makeUniqueToPreventMerging = NotificationExtensionsKt.makeUniqueToPreventMerging(intent);
        NotificationBuilder create = NotificationBuilder.Companion.create(context);
        create.setSmallIcon(R.drawable.ic_notification);
        create.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_action_warning_red));
        String string = context.getString(R.string.MessageNotifier_message_delivery_failed);
        Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.stri…_message_delivery_failed)");
        create.setContentTitle(string);
        create.setContentText(context.getString(R.string.MessageNotifier_failed_to_deliver_message));
        create.setTicker(context.getString(R.string.MessageNotifier_error_delivering_message));
        create.setContentIntent(PendingIntent.getActivity(context, 0, makeUniqueToPreventMerging, 0));
        create.setAutoCancel(true);
        create.setAlarms(recipient);
        create.setChannelId(NotificationChannels.FAILURES);
        NotificationManagerCompat from = NotificationManagerCompat.from(context);
        Intrinsics.checkNotNullExpressionValue(from, "from(context)");
        safelyNotify(from, context, recipient, NotificationIds.getNotificationIdForMessageDeliveryFailed(conversationId), create.build());
    }

    public final void notifyProofRequired(Context context, Recipient recipient, ConversationId conversationId, ConversationId conversationId2) {
        Intent intent;
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        Intrinsics.checkNotNullParameter(conversationId, ThreadDatabase.TABLE_NAME);
        if (Intrinsics.areEqual(conversationId, conversationId2)) {
            notifyInThread(context, recipient, 0);
            return;
        }
        if (recipient.isDistributionList() || conversationId.getGroupStoryId() != null) {
            intent = new Intent(context, MyStoriesActivity.class);
        } else {
            intent = ConversationIntents.createBuilder(context, recipient.getId(), conversationId.getThreadId()).build();
            Intrinsics.checkNotNullExpressionValue(intent, "{\n      ConversationInte…d)\n        .build()\n    }");
        }
        Intent makeUniqueToPreventMerging = NotificationExtensionsKt.makeUniqueToPreventMerging(intent);
        NotificationBuilder create = NotificationBuilder.Companion.create(context);
        create.setSmallIcon(R.drawable.ic_notification);
        create.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_info_outline));
        String string = context.getString(R.string.MessageNotifier_message_delivery_paused);
        Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.stri…_message_delivery_paused)");
        create.setContentTitle(string);
        create.setContentText(context.getString(R.string.MessageNotifier_verify_to_continue_messaging_on_signal));
        create.setContentIntent(PendingIntent.getActivity(context, 0, makeUniqueToPreventMerging, 0));
        create.setOnlyAlertOnce(true);
        create.setAutoCancel(true);
        create.setAlarms(recipient);
        create.setChannelId(NotificationChannels.FAILURES);
        NotificationManagerCompat from = NotificationManagerCompat.from(context);
        Intrinsics.checkNotNullExpressionValue(from, "from(context)");
        safelyNotify(from, context, recipient, NotificationIds.getNotificationIdForMessageDeliveryFailed(conversationId), create.build());
    }

    @JvmStatic
    public static final void notifyToBubbleConversation(Context context, Recipient recipient, long j) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        NotificationBuilder create = NotificationBuilder.Companion.create(context);
        NotificationConversation notificationConversation = new NotificationConversation(recipient, ConversationId.Companion.forConversation(j), CollectionsKt__CollectionsJVMKt.listOf(new MessageNotification(recipient, new InMemoryMessageRecord.ForceConversationBubble(recipient, j))));
        create.setSmallIcon(R.drawable.ic_notification);
        create.setColor(ContextCompat.getColor(context, R.color.core_ultramarine));
        create.setCategory("msg");
        create.setGroup(MessageNotifierV2.NOTIFICATION_GROUP);
        create.setChannelId(notificationConversation.getChannelId(context));
        create.setContentTitle(notificationConversation.getContentTitle(context));
        create.setLargeIcon(NotificationExtensionsKt.toLargeBitmap(notificationConversation.getContactLargeIcon(context), context));
        create.addPerson(notificationConversation.getRecipient());
        String shortcutId = ConversationUtil.getShortcutId(notificationConversation.getRecipient());
        Intrinsics.checkNotNullExpressionValue(shortcutId, "getShortcutId(conversation.recipient)");
        create.setShortcutId(shortcutId);
        String shortcutId2 = ConversationUtil.getShortcutId(notificationConversation.getRecipient());
        Intrinsics.checkNotNullExpressionValue(shortcutId2, "getShortcutId(conversation.recipient)");
        create.setLocusId(shortcutId2);
        create.addMessages(notificationConversation);
        create.setBubbleMetadata(notificationConversation, BubbleUtil.BubbleState.SHOWN);
        Log.d(TAG, "Posting Notification for requested bubble");
        NotificationFactory notificationFactory = INSTANCE;
        NotificationManagerCompat from = NotificationManagerCompat.from(context);
        Intrinsics.checkNotNullExpressionValue(from, "from(context)");
        notificationFactory.safelyNotify(from, context, recipient, notificationConversation.getNotificationId(), create.build());
    }

    private final void safelyNotify(NotificationManagerCompat notificationManagerCompat, Context context, Recipient recipient, int i, Notification notification) {
        try {
            notificationManagerCompat.notify(i, notification);
            Log.Logger internal = Log.internal();
            String str = TAG;
            internal.i(str, "Posted notification: " + notification);
        } catch (SecurityException unused) {
            Log.i(TAG, "Security exception when posting notification, clearing ringtone");
            if (recipient != null) {
                SignalExecutors.BOUNDED.execute(new Runnable(context) { // from class: org.thoughtcrime.securesms.notifications.v2.NotificationFactory$$ExternalSyntheticLambda0
                    public final /* synthetic */ Context f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        NotificationFactory.m2356safelyNotify$lambda14(Recipient.this, this.f$1);
                    }
                });
            }
        } catch (RuntimeException e) {
            if (e.getCause() instanceof TransactionTooLargeException) {
                Log.e(TAG, "Transaction too large", e);
                return;
            }
            throw e;
        }
    }

    /* renamed from: safelyNotify$lambda-14 */
    public static final void m2356safelyNotify$lambda14(Recipient recipient, Context context) {
        Intrinsics.checkNotNullParameter(context, "$context");
        RecipientDatabase recipients = SignalDatabase.Companion.recipients();
        RecipientId id = recipient.getId();
        Intrinsics.checkNotNullExpressionValue(id, "threadRecipient.id");
        recipients.setMessageRingtone(id, null);
        NotificationChannels.updateMessageRingtone(context, recipient, null);
    }
}
