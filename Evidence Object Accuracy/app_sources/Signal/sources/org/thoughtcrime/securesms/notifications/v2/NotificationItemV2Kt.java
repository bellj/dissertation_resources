package org.thoughtcrime.securesms.notifications.v2;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;

/* compiled from: NotificationItemV2.kt */
@Metadata(d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"EMOJI_REPLACEMENT_STRING", "", "MAX_DISPLAY_LENGTH", "", "TAG", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationItemV2Kt {
    private static final String EMOJI_REPLACEMENT_STRING;
    private static final int MAX_DISPLAY_LENGTH;
    private static final String TAG;

    static {
        String tag = Log.tag(NotificationItemV2.class);
        Intrinsics.checkNotNullExpressionValue(tag, "tag(NotificationItemV2::class.java)");
        TAG = tag;
    }
}
