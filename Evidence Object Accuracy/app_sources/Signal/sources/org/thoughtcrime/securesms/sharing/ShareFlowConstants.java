package org.thoughtcrime.securesms.sharing;

import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: ShareFlowConstants.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bÀ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/ShareFlowConstants;", "", "()V", "ADD_DURATION", "", "CHANGE_DURATION", "MOVE_DURATION", "REMOVE_DURATION", "applySelectedContactsRecyclerAnimationSpeeds", "", "itemAnimator", "Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ShareFlowConstants {
    private static final long ADD_DURATION;
    private static final long CHANGE_DURATION;
    public static final ShareFlowConstants INSTANCE = new ShareFlowConstants();
    private static final long MOVE_DURATION;
    private static final long REMOVE_DURATION;

    private ShareFlowConstants() {
    }

    @JvmStatic
    public static final void applySelectedContactsRecyclerAnimationSpeeds(RecyclerView.ItemAnimator itemAnimator) {
        Intrinsics.checkNotNullParameter(itemAnimator, "itemAnimator");
        itemAnimator.setAddDuration(60);
        itemAnimator.setRemoveDuration(60);
        itemAnimator.setMoveDuration(125);
        itemAnimator.setChangeDuration(125);
    }
}
