package org.thoughtcrime.securesms.sharing.v2;

import android.net.Uri;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.sharing.MultiShareArgs;

/* compiled from: ResolvedShareData.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\b\t\n\u000bB\u0007\b\u0004¢\u0006\u0002\u0010\u0002J\b\u0010\u0006\u001a\u00020\u0007H&R\u0012\u0010\u0003\u001a\u00020\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0005\u0001\u0004\f\r\u000e\u000f¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;", "", "()V", "isMmsOrSmsSupported", "", "()Z", "toMultiShareArgs", "Lorg/thoughtcrime/securesms/sharing/MultiShareArgs;", "ExternalUri", "Failure", "Media", "Primitive", "Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData$Primitive;", "Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData$ExternalUri;", "Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData$Media;", "Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData$Failure;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class ResolvedShareData {
    public /* synthetic */ ResolvedShareData(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    public abstract boolean isMmsOrSmsSupported();

    public abstract MultiShareArgs toMultiShareArgs();

    private ResolvedShareData() {
    }

    /* compiled from: ResolvedShareData.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\f\u001a\u00020\u00062\b\u0010\r\u001a\u0004\u0018\u00010\u000eHÖ\u0003J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001J\b\u0010\u0011\u001a\u00020\u0012H\u0016J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0014\u0010\u0005\u001a\u00020\u0006XD¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData$Primitive;", "Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;", DraftDatabase.Draft.TEXT, "", "(Ljava/lang/CharSequence;)V", "isMmsOrSmsSupported", "", "()Z", "getText", "()Ljava/lang/CharSequence;", "component1", "copy", "equals", "other", "", "hashCode", "", "toMultiShareArgs", "Lorg/thoughtcrime/securesms/sharing/MultiShareArgs;", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Primitive extends ResolvedShareData {
        private final boolean isMmsOrSmsSupported = true;
        private final CharSequence text;

        public static /* synthetic */ Primitive copy$default(Primitive primitive, CharSequence charSequence, int i, Object obj) {
            if ((i & 1) != 0) {
                charSequence = primitive.text;
            }
            return primitive.copy(charSequence);
        }

        public final CharSequence component1() {
            return this.text;
        }

        public final Primitive copy(CharSequence charSequence) {
            Intrinsics.checkNotNullParameter(charSequence, DraftDatabase.Draft.TEXT);
            return new Primitive(charSequence);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof Primitive) && Intrinsics.areEqual(this.text, ((Primitive) obj).text);
        }

        public int hashCode() {
            return this.text.hashCode();
        }

        public String toString() {
            return "Primitive(text=" + ((Object) this.text) + ')';
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Primitive(CharSequence charSequence) {
            super(null);
            Intrinsics.checkNotNullParameter(charSequence, DraftDatabase.Draft.TEXT);
            this.text = charSequence;
        }

        public final CharSequence getText() {
            return this.text;
        }

        @Override // org.thoughtcrime.securesms.sharing.v2.ResolvedShareData
        public boolean isMmsOrSmsSupported() {
            return this.isMmsOrSmsSupported;
        }

        @Override // org.thoughtcrime.securesms.sharing.v2.ResolvedShareData
        public MultiShareArgs toMultiShareArgs() {
            MultiShareArgs build = new MultiShareArgs.Builder(SetsKt__SetsKt.emptySet()).withDraftText(this.text.toString()).build();
            Intrinsics.checkNotNullExpressionValue(build, "Builder(setOf()).withDra…(text.toString()).build()");
            return build;
        }
    }

    /* compiled from: ResolvedShareData.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\f\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0007HÆ\u0003J'\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0012\u001a\u00020\u00072\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\b\u0010\u0017\u001a\u00020\u0018H\u0016J\t\u0010\u0019\u001a\u00020\u0005HÖ\u0001R\u0014\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\r¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData$ExternalUri;", "Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;", "uri", "Landroid/net/Uri;", "mimeType", "", "isMmsOrSmsSupported", "", "(Landroid/net/Uri;Ljava/lang/String;Z)V", "()Z", "getMimeType", "()Ljava/lang/String;", "getUri", "()Landroid/net/Uri;", "component1", "component2", "component3", "copy", "equals", "other", "", "hashCode", "", "toMultiShareArgs", "Lorg/thoughtcrime/securesms/sharing/MultiShareArgs;", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ExternalUri extends ResolvedShareData {
        private final boolean isMmsOrSmsSupported;
        private final String mimeType;
        private final Uri uri;

        public static /* synthetic */ ExternalUri copy$default(ExternalUri externalUri, Uri uri, String str, boolean z, int i, Object obj) {
            if ((i & 1) != 0) {
                uri = externalUri.uri;
            }
            if ((i & 2) != 0) {
                str = externalUri.mimeType;
            }
            if ((i & 4) != 0) {
                z = externalUri.isMmsOrSmsSupported();
            }
            return externalUri.copy(uri, str, z);
        }

        public final Uri component1() {
            return this.uri;
        }

        public final String component2() {
            return this.mimeType;
        }

        public final boolean component3() {
            return isMmsOrSmsSupported();
        }

        public final ExternalUri copy(Uri uri, String str, boolean z) {
            Intrinsics.checkNotNullParameter(uri, "uri");
            Intrinsics.checkNotNullParameter(str, "mimeType");
            return new ExternalUri(uri, str, z);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ExternalUri)) {
                return false;
            }
            ExternalUri externalUri = (ExternalUri) obj;
            return Intrinsics.areEqual(this.uri, externalUri.uri) && Intrinsics.areEqual(this.mimeType, externalUri.mimeType) && isMmsOrSmsSupported() == externalUri.isMmsOrSmsSupported();
        }

        public int hashCode() {
            int hashCode = ((this.uri.hashCode() * 31) + this.mimeType.hashCode()) * 31;
            boolean isMmsOrSmsSupported = isMmsOrSmsSupported();
            if (isMmsOrSmsSupported) {
                isMmsOrSmsSupported = true;
            }
            int i = isMmsOrSmsSupported ? 1 : 0;
            int i2 = isMmsOrSmsSupported ? 1 : 0;
            int i3 = isMmsOrSmsSupported ? 1 : 0;
            return hashCode + i;
        }

        public String toString() {
            return "ExternalUri(uri=" + this.uri + ", mimeType=" + this.mimeType + ", isMmsOrSmsSupported=" + isMmsOrSmsSupported() + ')';
        }

        public final Uri getUri() {
            return this.uri;
        }

        public final String getMimeType() {
            return this.mimeType;
        }

        @Override // org.thoughtcrime.securesms.sharing.v2.ResolvedShareData
        public boolean isMmsOrSmsSupported() {
            return this.isMmsOrSmsSupported;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ExternalUri(Uri uri, String str, boolean z) {
            super(null);
            Intrinsics.checkNotNullParameter(uri, "uri");
            Intrinsics.checkNotNullParameter(str, "mimeType");
            this.uri = uri;
            this.mimeType = str;
            this.isMmsOrSmsSupported = z;
        }

        @Override // org.thoughtcrime.securesms.sharing.v2.ResolvedShareData
        public MultiShareArgs toMultiShareArgs() {
            MultiShareArgs build = new MultiShareArgs.Builder(SetsKt__SetsKt.emptySet()).withDataUri(this.uri).withDataType(this.mimeType).build();
            Intrinsics.checkNotNullExpressionValue(build, "Builder(setOf()).withDat…ataType(mimeType).build()");
            return build;
        }
    }

    /* compiled from: ResolvedShareData.kt */
    @Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001b\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u000f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0006HÆ\u0003J#\u0010\r\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0006HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u00062\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\b\u0010\u0013\u001a\u00020\u0014H\u0016J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001R\u0014\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\bR\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData$Media;", "Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;", "media", "", "Lorg/thoughtcrime/securesms/mediasend/Media;", "isMmsOrSmsSupported", "", "(Ljava/util/List;Z)V", "()Z", "getMedia", "()Ljava/util/List;", "component1", "component2", "copy", "equals", "other", "", "hashCode", "", "toMultiShareArgs", "Lorg/thoughtcrime/securesms/sharing/MultiShareArgs;", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Media extends ResolvedShareData {
        private final boolean isMmsOrSmsSupported;
        private final List<org.thoughtcrime.securesms.mediasend.Media> media;

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.sharing.v2.ResolvedShareData$Media */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ Media copy$default(Media media, List list, boolean z, int i, Object obj) {
            if ((i & 1) != 0) {
                list = media.media;
            }
            if ((i & 2) != 0) {
                z = media.isMmsOrSmsSupported();
            }
            return media.copy(list, z);
        }

        public final List<org.thoughtcrime.securesms.mediasend.Media> component1() {
            return this.media;
        }

        public final boolean component2() {
            return isMmsOrSmsSupported();
        }

        public final Media copy(List<? extends org.thoughtcrime.securesms.mediasend.Media> list, boolean z) {
            Intrinsics.checkNotNullParameter(list, "media");
            return new Media(list, z);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Media)) {
                return false;
            }
            Media media = (Media) obj;
            return Intrinsics.areEqual(this.media, media.media) && isMmsOrSmsSupported() == media.isMmsOrSmsSupported();
        }

        public int hashCode() {
            int hashCode = this.media.hashCode() * 31;
            boolean isMmsOrSmsSupported = isMmsOrSmsSupported();
            if (isMmsOrSmsSupported) {
                isMmsOrSmsSupported = true;
            }
            int i = isMmsOrSmsSupported ? 1 : 0;
            int i2 = isMmsOrSmsSupported ? 1 : 0;
            int i3 = isMmsOrSmsSupported ? 1 : 0;
            return hashCode + i;
        }

        public String toString() {
            return "Media(media=" + this.media + ", isMmsOrSmsSupported=" + isMmsOrSmsSupported() + ')';
        }

        public final List<org.thoughtcrime.securesms.mediasend.Media> getMedia() {
            return this.media;
        }

        @Override // org.thoughtcrime.securesms.sharing.v2.ResolvedShareData
        public boolean isMmsOrSmsSupported() {
            return this.isMmsOrSmsSupported;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.mediasend.Media> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Media(List<? extends org.thoughtcrime.securesms.mediasend.Media> list, boolean z) {
            super(null);
            Intrinsics.checkNotNullParameter(list, "media");
            this.media = list;
            this.isMmsOrSmsSupported = z;
        }

        @Override // org.thoughtcrime.securesms.sharing.v2.ResolvedShareData
        public MultiShareArgs toMultiShareArgs() {
            MultiShareArgs build = new MultiShareArgs.Builder(SetsKt__SetsKt.emptySet()).withMedia(this.media).build();
            Intrinsics.checkNotNullExpressionValue(build, "Builder(setOf()).withMedia(media).build()");
            return build;
        }
    }

    /* compiled from: ResolvedShareData.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0006\u001a\u00020\u0007H\u0016R\u0014\u0010\u0003\u001a\u00020\u00048VX\u0004¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0005¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData$Failure;", "Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;", "()V", "isMmsOrSmsSupported", "", "()Z", "toMultiShareArgs", "Lorg/thoughtcrime/securesms/sharing/MultiShareArgs;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Failure extends ResolvedShareData {
        public static final Failure INSTANCE = new Failure();

        private Failure() {
            super(null);
        }

        @Override // org.thoughtcrime.securesms.sharing.v2.ResolvedShareData
        public boolean isMmsOrSmsSupported() {
            throw new UnsupportedOperationException();
        }

        @Override // org.thoughtcrime.securesms.sharing.v2.ResolvedShareData
        public MultiShareArgs toMultiShareArgs() {
            throw new UnsupportedOperationException();
        }
    }
}
