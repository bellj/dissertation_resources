package org.thoughtcrime.securesms.sharing.v2;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.view.ViewGroup;
import androidx.activity.ComponentActivity;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts$StartActivityForResult;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelLazy;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import j$.util.Optional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.conversation.ConversationIntents;
import org.thoughtcrime.securesms.conversation.MessageSendType;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragmentArgs;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFullScreenDialogFragment;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionActivity;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.sharing.MultiShareArgs;
import org.thoughtcrime.securesms.sharing.MultiShareDialogs;
import org.thoughtcrime.securesms.sharing.MultiShareSender;
import org.thoughtcrime.securesms.sharing.interstitial.ShareInterstitialActivity;
import org.thoughtcrime.securesms.sharing.v2.ShareEvent;
import org.thoughtcrime.securesms.sharing.v2.ShareState;
import org.thoughtcrime.securesms.sharing.v2.ShareViewModel;
import org.thoughtcrime.securesms.sharing.v2.UnresolvedShareData;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.util.ConversationUtil;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.LifecycleDisposable;

/* compiled from: ShareActivity.kt */
@Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 42\u00020\u00012\u00020\u0002:\u00014B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\b\u0010\u0019\u001a\u00020\u0016H\u0016J\b\u0010\u001a\u001a\u00020\u001bH\u0016J\b\u0010\u001c\u001a\u00020\u001dH\u0016J\b\u0010\u001e\u001a\u00020\u001fH\u0002J\u001a\u0010 \u001a\u00020\u00162\b\u0010!\u001a\u0004\u0018\u00010\"2\u0006\u0010#\u001a\u00020$H\u0014J\b\u0010%\u001a\u00020\u0016H\u0016J\b\u0010&\u001a\u00020\u0016H\u0014J\b\u0010'\u001a\u00020\u0016H\u0014J\b\u0010(\u001a\u00020\u0016H\u0016J\u0010\u0010)\u001a\u00020\u00162\u0006\u0010*\u001a\u00020+H\u0002J\u0010\u0010,\u001a\u00020\u00162\u0006\u0010*\u001a\u00020-H\u0002J\u0010\u0010.\u001a\u00020\u00162\u0006\u0010*\u001a\u00020/H\u0002J\u0010\u00100\u001a\u00020\u00162\u0006\u0010*\u001a\u000201H\u0002J\u0010\u00102\u001a\u00020\u00162\u0006\u00103\u001a\u00020\"H\u0016R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058BX\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX.¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u000f\u001a\u00020\u00108BX\u0002¢\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0011\u0010\u0012¨\u00065"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/ShareActivity;", "Lorg/thoughtcrime/securesms/PassphraseRequiredActivity;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardFragment$Callback;", "()V", "directShareTarget", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getDirectShareTarget", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "dynamicTheme", "Lorg/thoughtcrime/securesms/util/DynamicNoActionBarTheme;", "finishOnOkResultLauncher", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "viewModel", "Lorg/thoughtcrime/securesms/sharing/v2/ShareViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/sharing/v2/ShareViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "ensureFragment", "", "resolvedShareData", "Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;", "exitFlow", "getContainer", "Landroid/view/ViewGroup;", "getDialogBackgroundColor", "", "getUnresolvedShareData", "Lorg/thoughtcrime/securesms/sharing/v2/UnresolvedShareData;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "ready", "", "onFinishForwardAction", "onPreCreate", "onResume", "onSearchInputFocused", "openConversation", "shareEvent", "Lorg/thoughtcrime/securesms/sharing/v2/ShareEvent$OpenConversation;", "openMediaInterstitial", "Lorg/thoughtcrime/securesms/sharing/v2/ShareEvent$OpenMediaInterstitial;", "openTextInterstitial", "Lorg/thoughtcrime/securesms/sharing/v2/ShareEvent$OpenTextInterstitial;", "sendWithoutInterstitial", "Lorg/thoughtcrime/securesms/sharing/v2/ShareEvent$SendWithoutInterstitial;", "setResult", "bundle", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ShareActivity extends PassphraseRequiredActivity implements MultiselectForwardFragment.Callback {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(ShareActivity.class);
    private final DynamicNoActionBarTheme dynamicTheme = new DynamicNoActionBarTheme();
    private ActivityResultLauncher<Intent> finishOnOkResultLauncher;
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private final Lazy viewModel$delegate = new ViewModelLazy(Reflection.getOrCreateKotlinClass(ShareViewModel.class), new Function0<ViewModelStore>(this) { // from class: org.thoughtcrime.securesms.sharing.v2.ShareActivity$special$$inlined$viewModels$2
        final /* synthetic */ ComponentActivity $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = this.$this_viewModels.getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.sharing.v2.ShareActivity$viewModel$2
        final /* synthetic */ ShareActivity this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            return new ShareViewModel.Factory(ShareActivity.access$getUnresolvedShareData(this.this$0), new ShareRepository(this.this$0));
        }
    });

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public void exitFlow() {
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public void onFinishForwardAction() {
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public void onSearchInputFocused() {
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public Stories.MediaTransform.SendRequirements getStorySendRequirements() {
        return MultiselectForwardFragment.Callback.DefaultImpls.getStorySendRequirements(this);
    }

    /* compiled from: ShareActivity.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/ShareActivity$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    private final ShareViewModel getViewModel() {
        return (ShareViewModel) this.viewModel$delegate.getValue();
    }

    private final RecipientId getDirectShareTarget() {
        return ConversationUtil.getRecipientId(getIntent().getStringExtra("android.intent.extra.shortcut.ID"));
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onPreCreate() {
        super.onPreCreate();
        this.dynamicTheme.onCreate(this);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        setContentView(R.layout.share_activity_v2);
        ActivityResultLauncher<Intent> registerForActivityResult = registerForActivityResult(new ActivityResultContracts$StartActivityForResult(), new ActivityResultCallback() { // from class: org.thoughtcrime.securesms.sharing.v2.ShareActivity$$ExternalSyntheticLambda0
            @Override // androidx.activity.result.ActivityResultCallback
            public final void onActivityResult(Object obj) {
                ShareActivity.m2739$r8$lambda$vbIfzIzi6s0P2Ew7tTkd9VR2ok(ShareActivity.this, (ActivityResult) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(registerForActivityResult, "registerForActivityResul…   finish()\n      }\n    }");
        this.finishOnOkResultLauncher = registerForActivityResult;
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        Disposable subscribe = getViewModel().getEvents().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.sharing.v2.ShareActivity$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ShareActivity.$r8$lambda$hBT6jA2IznFf5cduXBGXSpzrgZY(ShareActivity.this, (ShareEvent) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.events.subscri…shareEvent)\n      }\n    }");
        lifecycleDisposable.plusAssign(subscribe);
        LifecycleDisposable lifecycleDisposable2 = this.lifecycleDisposable;
        Disposable subscribe2 = getViewModel().getState().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.sharing.v2.ShareActivity$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ShareActivity.m2738$r8$lambda$Fs2EoKxNwL6oNONffJT34PsfgQ(ShareActivity.this, (ShareState) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe2, "viewModel.state.observeO…}\n        }\n      }\n    }");
        lifecycleDisposable2.plusAssign(subscribe2);
    }

    /* renamed from: onCreate$lambda-1 */
    public static final void m2740onCreate$lambda1(ShareActivity shareActivity, ActivityResult activityResult) {
        Intrinsics.checkNotNullParameter(shareActivity, "this$0");
        if (activityResult.getResultCode() == -1) {
            shareActivity.finish();
        }
    }

    /* renamed from: onCreate$lambda-2 */
    public static final void m2741onCreate$lambda2(ShareActivity shareActivity, ShareEvent shareEvent) {
        Intrinsics.checkNotNullParameter(shareActivity, "this$0");
        if (shareEvent instanceof ShareEvent.OpenConversation) {
            Intrinsics.checkNotNullExpressionValue(shareEvent, "shareEvent");
            shareActivity.openConversation((ShareEvent.OpenConversation) shareEvent);
        } else if (shareEvent instanceof ShareEvent.OpenMediaInterstitial) {
            Intrinsics.checkNotNullExpressionValue(shareEvent, "shareEvent");
            shareActivity.openMediaInterstitial((ShareEvent.OpenMediaInterstitial) shareEvent);
        } else if (shareEvent instanceof ShareEvent.OpenTextInterstitial) {
            Intrinsics.checkNotNullExpressionValue(shareEvent, "shareEvent");
            shareActivity.openTextInterstitial((ShareEvent.OpenTextInterstitial) shareEvent);
        } else if (shareEvent instanceof ShareEvent.SendWithoutInterstitial) {
            Intrinsics.checkNotNullExpressionValue(shareEvent, "shareEvent");
            shareActivity.sendWithoutInterstitial((ShareEvent.SendWithoutInterstitial) shareEvent);
        }
    }

    /* renamed from: onCreate$lambda-3 */
    public static final void m2742onCreate$lambda3(ShareActivity shareActivity, ShareState shareState) {
        Intrinsics.checkNotNullParameter(shareActivity, "this$0");
        ShareState.ShareDataLoadState loadState = shareState.getLoadState();
        if (Intrinsics.areEqual(loadState, ShareState.ShareDataLoadState.Init.INSTANCE)) {
            return;
        }
        if (Intrinsics.areEqual(loadState, ShareState.ShareDataLoadState.Failed.INSTANCE)) {
            shareActivity.finish();
        } else if (loadState instanceof ShareState.ShareDataLoadState.Loaded) {
            RecipientId directShareTarget = shareActivity.getDirectShareTarget();
            if (directShareTarget != null) {
                Log.d(TAG, "Encountered a direct share target. Opening conversation with resolved share data.");
                shareActivity.openConversation(new ShareEvent.OpenConversation(((ShareState.ShareDataLoadState.Loaded) shareState.getLoadState()).getResolvedShareData(), new ContactSearchKey.RecipientSearchKey.KnownRecipient(directShareTarget)));
                return;
            }
            shareActivity.ensureFragment(((ShareState.ShareDataLoadState.Loaded) shareState.getLoadState()).getResolvedShareData());
        }
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public void setResult(Bundle bundle) {
        Intrinsics.checkNotNullParameter(bundle, "bundle");
        if (bundle.containsKey(MultiselectForwardFragment.RESULT_SENT)) {
            throw new AssertionError("Should never happen.");
        } else if (bundle.containsKey(MultiselectForwardFragment.RESULT_SELECTION)) {
            ArrayList<ContactSearchKey.ParcelableRecipientSearchKey> parcelableArrayList = bundle.getParcelableArrayList(MultiselectForwardFragment.RESULT_SELECTION);
            Intrinsics.checkNotNull(parcelableArrayList);
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(parcelableArrayList, 10));
            for (ContactSearchKey.ParcelableRecipientSearchKey parcelableRecipientSearchKey : parcelableArrayList) {
                arrayList.add(parcelableRecipientSearchKey.asRecipientSearchKey());
            }
            getViewModel().onContactSelectionConfirmed(arrayList);
        } else {
            throw new AssertionError("Expected a recipient selection!");
        }
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public ViewGroup getContainer() {
        View findViewById = findViewById(R.id.container);
        Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.container)");
        return (ViewGroup) findViewById;
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public int getDialogBackgroundColor() {
        return ContextCompat.getColor(this, R.color.signal_background_primary);
    }

    public final UnresolvedShareData getUnresolvedShareData() {
        UnresolvedShareData unresolvedShareData;
        UnresolvedShareData externalPrimitiveShare;
        if (!Intrinsics.areEqual(getIntent().getAction(), "android.intent.action.SEND_MULTIPLE") || !getIntent().hasExtra("android.intent.extra.TEXT")) {
            if (Intrinsics.areEqual(getIntent().getAction(), "android.intent.action.SEND_MULTIPLE") && getIntent().hasExtra("android.intent.extra.STREAM")) {
                ArrayList parcelableArrayListExtra = getIntent().getParcelableArrayListExtra("android.intent.extra.STREAM");
                if (parcelableArrayListExtra != null) {
                    externalPrimitiveShare = new UnresolvedShareData.ExternalMultiShare(parcelableArrayListExtra);
                } else {
                    throw new IllegalStateException("ACTION_SEND_MULTIPLE with EXTRA_STREAM but the EXTRA_STREAM was null".toString());
                }
            } else if (Intrinsics.areEqual(getIntent().getAction(), "android.intent.action.SEND") && getIntent().hasExtra("android.intent.extra.STREAM")) {
                Uri uri = (Uri) getIntent().getParcelableExtra("android.intent.extra.STREAM");
                if (uri != null) {
                    externalPrimitiveShare = new UnresolvedShareData.ExternalSingleShare(uri, getIntent().getType());
                } else {
                    throw new IllegalStateException("ACTION_SEND with EXTRA_STREAM but the EXTRA_STREAM was null".toString());
                }
            } else if (!Intrinsics.areEqual(getIntent().getAction(), "android.intent.action.SEND") || !getIntent().hasExtra("android.intent.extra.TEXT")) {
                unresolvedShareData = null;
            } else {
                CharSequence charSequenceExtra = getIntent().getCharSequenceExtra("android.intent.extra.TEXT");
                if (charSequenceExtra != null) {
                    externalPrimitiveShare = new UnresolvedShareData.ExternalPrimitiveShare(charSequenceExtra);
                } else {
                    throw new IllegalStateException("ACTION_SEND with EXTRA_TEXT but the EXTRA_TEXT was null".toString());
                }
            }
            unresolvedShareData = externalPrimitiveShare;
        } else {
            ArrayList<CharSequence> charSequenceArrayListExtra = getIntent().getCharSequenceArrayListExtra("android.intent.extra.TEXT");
            if (charSequenceArrayListExtra != null) {
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
                int i = 0;
                for (Object obj : charSequenceArrayListExtra) {
                    int i2 = i + 1;
                    if (i < 0) {
                        CollectionsKt__CollectionsKt.throwIndexOverflow();
                    }
                    spannableStringBuilder.append((CharSequence) obj);
                    if (i != CollectionsKt__CollectionsKt.getLastIndex(charSequenceArrayListExtra)) {
                        spannableStringBuilder.append((CharSequence) "\n");
                    }
                    i = i2;
                }
                unresolvedShareData = new UnresolvedShareData.ExternalPrimitiveShare(spannableStringBuilder);
            } else {
                throw new IllegalStateException("ACTION_SEND_MULTIPLE with EXTRA_TEXT but the EXTRA_TEXT was null".toString());
            }
        }
        if (unresolvedShareData != null) {
            return unresolvedShareData;
        }
        throw new IllegalStateException("Intent Action: android.intent.action.SEND_MULTIPLE could not be resolved with the given arguments.".toString());
    }

    private final void ensureFragment(ResolvedShareData resolvedShareData) {
        if (!getSupportFragmentManager().isStateSaved()) {
            List<Fragment> fragments = getSupportFragmentManager().getFragments();
            Intrinsics.checkNotNullExpressionValue(fragments, "supportFragmentManager.fragments");
            boolean z = true;
            if (!(fragments instanceof Collection) || !fragments.isEmpty()) {
                Iterator<T> it = fragments.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (((Fragment) it.next()) instanceof MultiselectForwardFullScreenDialogFragment) {
                            z = false;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            if (z) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, MultiselectForwardFragment.Companion.create(new MultiselectForwardFragmentArgs(resolvedShareData.isMmsOrSmsSupported(), CollectionsKt__CollectionsJVMKt.listOf(resolvedShareData.toMultiShareArgs()), R.string.MultiselectForwardFragment__share_with, true, true, false, 0, null, 224, null))).commitNow();
            }
        }
    }

    private final void openConversation(ShareEvent.OpenConversation openConversation) {
        if (!openConversation.getContact().isStory()) {
            Log.d(TAG, "Opening conversation...");
            MultiShareArgs multiShareArgs = openConversation.getMultiShareArgs();
            ConversationIntents.Builder asBorderless = ConversationIntents.createBuilder(this, openConversation.getContact().getRecipientId(), -1).withDataUri(multiShareArgs.getDataUri()).withDataType(multiShareArgs.getDataType()).withMedia(multiShareArgs.getMedia()).withDraftText(multiShareArgs.getDraftText()).withStickerLocator(multiShareArgs.getStickerLocator()).asBorderless(multiShareArgs.isBorderless());
            Intrinsics.checkNotNullExpressionValue(asBorderless, "createBuilder(this, shar…tiShareArgs.isBorderless)");
            finish();
            startActivity(asBorderless.build());
            return;
        }
        throw new IllegalStateException("Can't open a conversation for a story!".toString());
    }

    private final void openMediaInterstitial(ShareEvent.OpenMediaInterstitial openMediaInterstitial) {
        Log.d(TAG, "Opening media share interstitial...");
        MultiShareArgs multiShareArgs = openMediaInterstitial.getMultiShareArgs();
        ArrayList arrayList = new ArrayList(multiShareArgs.getMedia());
        if (arrayList.isEmpty() && multiShareArgs.getDataUri() != null) {
            arrayList.add(new Media(multiShareArgs.getDataUri(), multiShareArgs.getDataType(), 0, 0, 0, 0, 0, false, false, Optional.empty(), Optional.empty(), Optional.empty()));
        }
        boolean z = multiShareArgs.allRecipientsAreStories() && arrayList.isEmpty();
        MediaSelectionActivity.Companion companion = MediaSelectionActivity.Companion;
        MessageSendType worstTransportOption = MultiShareSender.getWorstTransportOption(this, multiShareArgs.getRecipientSearchKeys());
        Intrinsics.checkNotNullExpressionValue(worstTransportOption, "getWorstTransportOption(…Args.recipientSearchKeys)");
        Set<ContactSearchKey.RecipientSearchKey> recipientSearchKeys = multiShareArgs.getRecipientSearchKeys();
        Intrinsics.checkNotNullExpressionValue(recipientSearchKeys, "multiShareArgs.recipientSearchKeys");
        Intent share = companion.share(this, worstTransportOption, arrayList, CollectionsKt___CollectionsKt.toList(recipientSearchKeys), multiShareArgs.getDraftText(), z);
        ActivityResultLauncher<Intent> activityResultLauncher = this.finishOnOkResultLauncher;
        if (activityResultLauncher == null) {
            Intrinsics.throwUninitializedPropertyAccessException("finishOnOkResultLauncher");
            activityResultLauncher = null;
        }
        activityResultLauncher.launch(share);
    }

    private final void openTextInterstitial(ShareEvent.OpenTextInterstitial openTextInterstitial) {
        Log.d(TAG, "Opening text share interstitial...");
        ActivityResultLauncher<Intent> activityResultLauncher = this.finishOnOkResultLauncher;
        if (activityResultLauncher == null) {
            Intrinsics.throwUninitializedPropertyAccessException("finishOnOkResultLauncher");
            activityResultLauncher = null;
        }
        activityResultLauncher.launch(ShareInterstitialActivity.createIntent(this, openTextInterstitial.getMultiShareArgs()));
    }

    private final void sendWithoutInterstitial(ShareEvent.SendWithoutInterstitial sendWithoutInterstitial) {
        Log.d(TAG, "Sending without an interstitial...");
        MultiShareSender.send(sendWithoutInterstitial.getMultiShareArgs(), new androidx.core.util.Consumer() { // from class: org.thoughtcrime.securesms.sharing.v2.ShareActivity$$ExternalSyntheticLambda4
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                ShareActivity.$r8$lambda$B1qpKLGlETIun92lv2dkZ8FgObg(ShareActivity.this, (MultiShareSender.MultiShareSendResultCollection) obj);
            }
        });
    }

    /* renamed from: sendWithoutInterstitial$lambda-12 */
    public static final void m2743sendWithoutInterstitial$lambda12(ShareActivity shareActivity, MultiShareSender.MultiShareSendResultCollection multiShareSendResultCollection) {
        Intrinsics.checkNotNullParameter(shareActivity, "this$0");
        Intrinsics.checkNotNull(multiShareSendResultCollection);
        MultiShareDialogs.displayResultDialog(shareActivity, multiShareSendResultCollection, new Runnable() { // from class: org.thoughtcrime.securesms.sharing.v2.ShareActivity$$ExternalSyntheticLambda3
            @Override // java.lang.Runnable
            public final void run() {
                ShareActivity.$r8$lambda$UtWulaVOZ5GTNkLs4rHeMd1cZqE(ShareActivity.this);
            }
        });
    }

    /* renamed from: sendWithoutInterstitial$lambda-12$lambda-11 */
    public static final void m2744sendWithoutInterstitial$lambda12$lambda11(ShareActivity shareActivity) {
        Intrinsics.checkNotNullParameter(shareActivity, "this$0");
        shareActivity.finish();
    }
}
