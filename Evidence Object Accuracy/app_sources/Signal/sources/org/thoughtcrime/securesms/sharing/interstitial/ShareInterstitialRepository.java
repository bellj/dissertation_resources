package org.thoughtcrime.securesms.sharing.interstitial;

import androidx.core.util.Consumer;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import java.util.List;
import java.util.Set;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8;
import org.thoughtcrime.securesms.recipients.Recipient;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public class ShareInterstitialRepository {
    public /* synthetic */ void lambda$loadRecipients$0(Consumer consumer, Set set) {
        consumer.accept(resolveRecipients(set));
    }

    public void loadRecipients(Set<ContactSearchKey.RecipientSearchKey> set, Consumer<List<Recipient>> consumer) {
        SignalExecutors.BOUNDED.execute(new Runnable(consumer, set) { // from class: org.thoughtcrime.securesms.sharing.interstitial.ShareInterstitialRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ Consumer f$1;
            public final /* synthetic */ Set f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ShareInterstitialRepository.this.lambda$loadRecipients$0(this.f$1, this.f$2);
            }
        });
    }

    private List<Recipient> resolveRecipients(Set<ContactSearchKey.RecipientSearchKey> set) {
        return Stream.of(set).map(new Function() { // from class: org.thoughtcrime.securesms.sharing.interstitial.ShareInterstitialRepository$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ((ContactSearchKey.RecipientSearchKey) obj).getRecipientId();
            }
        }).map(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8()).toList();
    }
}
