package org.thoughtcrime.securesms.sharing.interstitial;

import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.viewholders.RecipientViewHolder;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public class ShareInterstitialSelectionAdapter extends MappingAdapter {
    public ShareInterstitialSelectionAdapter() {
        registerFactory(ShareInterstitialMappingModel.class, RecipientViewHolder.createFactory(R.layout.share_contact_selection_item, null));
    }
}
