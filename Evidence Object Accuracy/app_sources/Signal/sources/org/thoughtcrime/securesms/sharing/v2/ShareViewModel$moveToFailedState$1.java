package org.thoughtcrime.securesms.sharing.v2;

import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.sharing.v2.ShareState;

/* compiled from: ShareViewModel.kt */
@Metadata(d1 = {"\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0003"}, d2 = {"<anonymous>", "Lorg/thoughtcrime/securesms/sharing/v2/ShareState;", "it", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ShareViewModel$moveToFailedState$1 extends Lambda implements Function1<ShareState, ShareState> {
    public static final ShareViewModel$moveToFailedState$1 INSTANCE = new ShareViewModel$moveToFailedState$1();

    ShareViewModel$moveToFailedState$1() {
        super(1);
    }

    public final ShareState invoke(ShareState shareState) {
        Intrinsics.checkNotNullParameter(shareState, "it");
        return shareState.copy(ShareState.ShareDataLoadState.Failed.INSTANCE);
    }
}
