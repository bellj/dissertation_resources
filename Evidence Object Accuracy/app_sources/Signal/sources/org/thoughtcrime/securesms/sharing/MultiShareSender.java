package org.thoughtcrime.securesms.sharing;

import android.content.Context;
import android.net.Uri;
import androidx.core.util.Consumer;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import j$.util.stream.Stream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.signal.core.util.BreakIteratorCompat;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.conversation.MessageSendType;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.database.model.StoryType;
import org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPost;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.keyvalue.StorySend;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mms.ImageSlide;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.mms.OutgoingSecureMediaMessage;
import org.thoughtcrime.securesms.mms.SentMediaQuality;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.SlideDeck;
import org.thoughtcrime.securesms.mms.SlideFactory;
import org.thoughtcrime.securesms.mms.StickerSlide;
import org.thoughtcrime.securesms.mms.VideoSlide;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.sharing.MultiShareSender;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.sms.OutgoingEncryptedMessage;
import org.thoughtcrime.securesms.sms.OutgoingTextMessage;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.MessageUtil;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public final class MultiShareSender {
    private static final String TAG = Log.tag(MultiShareSender.class);

    private MultiShareSender() {
    }

    public static void send(MultiShareArgs multiShareArgs, Consumer<MultiShareSendResultCollection> consumer) {
        MultiShareSender$$ExternalSyntheticLambda5 multiShareSender$$ExternalSyntheticLambda5 = new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.sharing.MultiShareSender$$ExternalSyntheticLambda5
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return MultiShareSender.sendSync(MultiShareArgs.this);
            }
        };
        Objects.requireNonNull(consumer);
        SimpleTask.run(multiShareSender$$ExternalSyntheticLambda5, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.sharing.MultiShareSender$$ExternalSyntheticLambda6
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                Consumer.this.accept((MultiShareSender.MultiShareSendResultCollection) obj);
            }
        });
    }

    /* JADX WARN: Type inference failed for: r13v0 */
    /* JADX WARN: Type inference failed for: r13v1, types: [java.lang.String, org.thoughtcrime.securesms.database.MessageDatabase$InsertListener, org.thoughtcrime.securesms.sharing.MultiShareSender$1] */
    /* JADX WARN: Type inference failed for: r13v2 */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.thoughtcrime.securesms.sharing.MultiShareSender.MultiShareSendResultCollection sendSync(org.thoughtcrime.securesms.sharing.MultiShareArgs r34) {
        /*
        // Method dump skipped, instructions count: 512
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.sharing.MultiShareSender.sendSync(org.thoughtcrime.securesms.sharing.MultiShareArgs):org.thoughtcrime.securesms.sharing.MultiShareSender$MultiShareSendResultCollection");
    }

    public static MessageSendType getWorstTransportOption(Context context, Set<ContactSearchKey.RecipientSearchKey> set) {
        for (ContactSearchKey.RecipientSearchKey recipientSearchKey : set) {
            MessageSendType resolveTransportOption = resolveTransportOption(context, Recipient.resolved(recipientSearchKey.getRecipientId()).isForceSmsSelection() && !recipientSearchKey.isStory());
            if (resolveTransportOption.usesSmsTransport()) {
                return resolveTransportOption;
            }
        }
        return MessageSendType.SignalMessageSendType.INSTANCE;
    }

    private static MessageSendType resolveTransportOption(Context context, Recipient recipient) {
        return resolveTransportOption(context, !recipient.isDistributionList() && (recipient.isForceSmsSelection() || !recipient.isRegistered()));
    }

    public static MessageSendType resolveTransportOption(Context context, boolean z) {
        if (z) {
            return MessageSendType.getFirstForTransport(context, false, MessageSendType.TransportType.SMS);
        }
        return MessageSendType.SignalMessageSendType.INSTANCE;
    }

    private static void sendMediaMessageOrCollectStoryToBatch(Context context, MultiShareArgs multiShareArgs, Recipient recipient, SlideDeck slideDeck, MessageSendType messageSendType, long j, boolean z, long j2, boolean z2, int i, List<Mention> list, boolean z3, long j3, boolean z4, List<OutgoingMediaMessage> list2, ChatColors chatColors) {
        List list3;
        StoryType storyType;
        List list4;
        String draftText = multiShareArgs.getDraftText();
        if (messageSendType.usesSignalTransport() && !z && draftText != null) {
            MessageUtil.SplitResult splitMessage = MessageUtil.getSplitMessage(context, draftText, messageSendType.calculateCharacters(draftText).maxPrimaryMessageSize);
            String body = splitMessage.getBody();
            if (splitMessage.getTextSlide().isPresent()) {
                slideDeck.addSlide(splitMessage.getTextSlide().get());
            }
            draftText = body;
        }
        ArrayList<OutgoingMediaMessage> arrayList = new ArrayList();
        if (z3) {
            if (recipient.isDistributionList()) {
                storyType = SignalDatabase.distributionLists().getStoryType(recipient.requireDistributionListId());
            } else {
                storyType = StoryType.STORY_WITH_REPLIES;
            }
            if (recipient.isActiveGroup() && recipient.isGroup()) {
                SignalDatabase.groups().markDisplayAsStory(recipient.requireGroupId());
            }
            if (!recipient.isMyStory()) {
                SignalStore.storyValues().setLatestStorySend(StorySend.newSend(recipient));
            }
            if (multiShareArgs.isTextStory()) {
                SlideDeck slideDeck2 = new SlideDeck();
                StoryType textStoryType = storyType.toTextStoryType();
                List emptyList = Collections.emptyList();
                if (multiShareArgs.getLinkPreview() != null) {
                    list4 = Collections.singletonList(multiShareArgs.getLinkPreview());
                } else {
                    list4 = Collections.emptyList();
                }
                arrayList.add(new OutgoingMediaMessage(recipient, slideDeck2, draftText, j3, i, 0, false, 2, textStoryType, null, false, null, emptyList, list4, Collections.emptyList(), null));
            } else if (z4) {
                arrayList.add(generateTextStory(recipient, multiShareArgs, j3, storyType, chatColors));
            } else {
                for (Slide slide : (List) Collection$EL.stream(slideDeck.getSlides()).flatMap(new Function(context) { // from class: org.thoughtcrime.securesms.sharing.MultiShareSender$$ExternalSyntheticLambda0
                    public final /* synthetic */ Context f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // j$.util.function.Function
                    public /* synthetic */ Function andThen(Function function) {
                        return Function.CC.$default$andThen(this, function);
                    }

                    @Override // j$.util.function.Function
                    public final Object apply(Object obj) {
                        return MultiShareSender.lambda$sendMediaMessageOrCollectStoryToBatch$1(this.f$0, (Slide) obj);
                    }

                    @Override // j$.util.function.Function
                    public /* synthetic */ Function compose(Function function) {
                        return Function.CC.$default$compose(this, function);
                    }
                }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.sharing.MultiShareSender$$ExternalSyntheticLambda1
                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate and(Predicate predicate) {
                        return Predicate.CC.$default$and(this, predicate);
                    }

                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate negate() {
                        return Predicate.CC.$default$negate(this);
                    }

                    @Override // j$.util.function.Predicate
                    public /* synthetic */ Predicate or(Predicate predicate) {
                        return Predicate.CC.$default$or(this, predicate);
                    }

                    @Override // j$.util.function.Predicate
                    public final boolean test(Object obj) {
                        return MultiShareSender.lambda$sendMediaMessageOrCollectStoryToBatch$2((Slide) obj);
                    }
                }).collect(Collectors.toList())) {
                    SlideDeck slideDeck3 = new SlideDeck();
                    slideDeck3.addSlide(slide);
                    arrayList.add(new OutgoingMediaMessage(recipient, slideDeck3, draftText, j3, i, 0, false, 2, storyType, null, false, null, Collections.emptyList(), Collections.emptyList(), list, null));
                }
            }
        } else {
            StoryType storyType2 = StoryType.NONE;
            List emptyList2 = Collections.emptyList();
            if (multiShareArgs.getLinkPreview() != null) {
                list3 = Collections.singletonList(multiShareArgs.getLinkPreview());
            } else {
                list3 = Collections.emptyList();
            }
            arrayList.add(new OutgoingMediaMessage(recipient, slideDeck, draftText, j3, i, j2, z2, 2, storyType2, null, false, null, emptyList2, list3, list, null));
        }
        if (z3) {
            list2.addAll(arrayList);
        } else if (shouldSendAsPush(recipient, z)) {
            for (OutgoingMediaMessage outgoingMediaMessage : arrayList) {
                MessageSender.send(context, (OutgoingMediaMessage) new OutgoingSecureMediaMessage(outgoingMediaMessage), j, false, (String) null, (MessageDatabase.InsertListener) null);
            }
        } else {
            for (OutgoingMediaMessage outgoingMediaMessage2 : arrayList) {
                MessageSender.send(context, outgoingMediaMessage2, j, z, (String) null, (MessageDatabase.InsertListener) null);
            }
        }
    }

    public static /* synthetic */ Stream lambda$sendMediaMessageOrCollectStoryToBatch$1(Context context, Slide slide) {
        if (slide instanceof VideoSlide) {
            return Collection$EL.stream(expandToClips(context, (VideoSlide) slide));
        }
        if (slide instanceof ImageSlide) {
            return Stream.CC.of(ensureDefaultQuality(context, (ImageSlide) slide));
        }
        return Stream.CC.of(slide);
    }

    public static /* synthetic */ boolean lambda$sendMediaMessageOrCollectStoryToBatch$2(Slide slide) {
        return MediaUtil.isStorySupportedType(slide.getContentType());
    }

    private static Collection<Slide> expandToClips(Context context, VideoSlide videoSlide) {
        Uri uri = videoSlide.getUri();
        Objects.requireNonNull(uri);
        long videoDuration = Stories.MediaTransform.getVideoDuration(uri);
        if (videoDuration > Stories.MAX_VIDEO_DURATION_MILLIS) {
            return (Collection) Collection$EL.stream(Stories.MediaTransform.clipMediaToStoryDuration(Stories.MediaTransform.videoSlideToMedia(videoSlide, videoDuration))).map(new Function(context) { // from class: org.thoughtcrime.securesms.sharing.MultiShareSender$$ExternalSyntheticLambda2
                public final /* synthetic */ Context f$0;

                {
                    this.f$0 = r1;
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return Stories.MediaTransform.mediaToVideoSlide(this.f$0, (Media) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).collect(Collectors.toList());
        }
        if (videoDuration == 0) {
            return Collections.emptyList();
        }
        return Collections.singletonList(videoSlide);
    }

    private static Slide ensureDefaultQuality(Context context, ImageSlide imageSlide) {
        Attachment asAttachment = imageSlide.asAttachment();
        return asAttachment.getTransformProperties().getSentMediaQuality() == SentMediaQuality.HIGH.getCode() ? new ImageSlide(context, asAttachment.getUri(), asAttachment.getContentType(), asAttachment.getSize(), asAttachment.getWidth(), asAttachment.getHeight(), asAttachment.isBorderless(), asAttachment.getCaption(), asAttachment.getBlurHash(), AttachmentDatabase.TransformProperties.empty()) : imageSlide;
    }

    private static void sendTextMessage(Context context, MultiShareArgs multiShareArgs, Recipient recipient, long j, boolean z, long j2, int i) {
        OutgoingTextMessage outgoingTextMessage;
        if (shouldSendAsPush(recipient, z)) {
            outgoingTextMessage = new OutgoingEncryptedMessage(recipient, multiShareArgs.getDraftText(), j2);
        } else {
            outgoingTextMessage = new OutgoingTextMessage(recipient, multiShareArgs.getDraftText(), j2, i);
        }
        MessageSender.send(context, outgoingTextMessage, j, z, (String) null, (MessageDatabase.InsertListener) null);
    }

    private static OutgoingMediaMessage generateTextStory(Recipient recipient, MultiShareArgs multiShareArgs, long j, StoryType storyType, ChatColors chatColors) {
        List list;
        String encodeBytes = Base64.encodeBytes(StoryTextPost.newBuilder().setBody(getBodyForTextStory(multiShareArgs.getDraftText(), multiShareArgs.getLinkPreview())).setStyle(StoryTextPost.Style.DEFAULT).setBackground(chatColors.serialize()).setTextBackgroundColor(0).setTextForegroundColor(-1).build().toByteArray());
        List emptyList = Collections.emptyList();
        StoryType textStoryType = storyType.toTextStoryType();
        List emptyList2 = Collections.emptyList();
        if (multiShareArgs.getLinkPreview() != null) {
            list = Collections.singletonList(multiShareArgs.getLinkPreview());
        } else {
            list = Collections.emptyList();
        }
        return new OutgoingMediaMessage(recipient, encodeBytes, emptyList, j, -1, 0, false, 2, textStoryType, null, false, null, emptyList2, list, Collections.emptyList(), Collections.emptySet(), Collections.emptySet(), null);
    }

    private static String getBodyForTextStory(String str, LinkPreview linkPreview) {
        if (Util.isEmpty(str)) {
            return "";
        }
        BreakIteratorCompat instance = BreakIteratorCompat.getInstance();
        instance.setText(str);
        String charSequence = instance.take(Stories.MAX_BODY_SIZE).toString();
        if (linkPreview == null) {
            return charSequence;
        }
        if (linkPreview.getUrl().equals(charSequence)) {
            return "";
        }
        return charSequence.replace(linkPreview.getUrl(), "").trim();
    }

    private static boolean shouldSendAsPush(Recipient recipient, boolean z) {
        return recipient.isDistributionList() || recipient.isServiceIdOnly() || (recipient.isRegistered() && !z);
    }

    private static SlideDeck buildSlideDeck(Context context, MultiShareArgs multiShareArgs) throws SlideNotFoundException {
        SlideDeck slideDeck = new SlideDeck();
        if (multiShareArgs.getStickerLocator() != null) {
            slideDeck.addSlide(new StickerSlide(context, multiShareArgs.getDataUri(), 0, multiShareArgs.getStickerLocator(), multiShareArgs.getDataType()));
        } else if (!multiShareArgs.getMedia().isEmpty()) {
            for (Media media : multiShareArgs.getMedia()) {
                Slide slide = SlideFactory.getSlide(context, media.getMimeType(), media.getUri(), media.getWidth(), media.getHeight(), media.getTransformProperties().orElse(null));
                if (slide != null) {
                    slideDeck.addSlide(slide);
                } else {
                    throw new SlideNotFoundException();
                }
            }
        } else if (multiShareArgs.getDataUri() != null) {
            Slide slide2 = SlideFactory.getSlide(context, multiShareArgs.getDataType(), multiShareArgs.getDataUri(), 0, 0, null);
            if (slide2 != null) {
                slideDeck.addSlide(slide2);
            } else {
                throw new SlideNotFoundException();
            }
        }
        return slideDeck;
    }

    private static List<Mention> getValidMentionsForRecipient(Recipient recipient, List<Mention> list) {
        if (list.isEmpty() || !recipient.isPushV2Group() || !recipient.isActiveGroup()) {
            return Collections.emptyList();
        }
        return (List) Collection$EL.stream(list).filter(new Predicate(new HashSet(recipient.getParticipantIds())) { // from class: org.thoughtcrime.securesms.sharing.MultiShareSender$$ExternalSyntheticLambda4
            public final /* synthetic */ Set f$0;

            {
                this.f$0 = r1;
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return MultiShareSender.lambda$getValidMentionsForRecipient$4(this.f$0, (Mention) obj);
            }
        }).collect(Collectors.toList());
    }

    public static /* synthetic */ boolean lambda$getValidMentionsForRecipient$4(Set set, Mention mention) {
        return set.contains(mention.getRecipientId());
    }

    /* loaded from: classes4.dex */
    public static final class MultiShareSendResultCollection {
        private final List<MultiShareSendResult> results;

        private MultiShareSendResultCollection(List<MultiShareSendResult> list) {
            this.results = list;
        }

        public static /* synthetic */ boolean lambda$containsFailures$0(MultiShareSendResult multiShareSendResult) {
            return multiShareSendResult.type != MultiShareSendResult.Type.SUCCESS;
        }

        public boolean containsFailures() {
            return com.annimon.stream.Stream.of(this.results).anyMatch(new MultiShareSender$MultiShareSendResultCollection$$ExternalSyntheticLambda0());
        }

        public static /* synthetic */ boolean lambda$containsOnlyFailures$1(MultiShareSendResult multiShareSendResult) {
            return multiShareSendResult.type != MultiShareSendResult.Type.SUCCESS;
        }

        public boolean containsOnlyFailures() {
            return com.annimon.stream.Stream.of(this.results).allMatch(new MultiShareSender$MultiShareSendResultCollection$$ExternalSyntheticLambda1());
        }
    }

    /* loaded from: classes4.dex */
    public static final class MultiShareSendResult {
        private final ContactSearchKey.RecipientSearchKey recipientSearchKey;
        private final Type type;

        /* loaded from: classes4.dex */
        public enum Type {
            GENERIC_ERROR,
            INVALID_SHARE_TO_STORY,
            MMS_NOT_ENABLED,
            SUCCESS
        }

        private MultiShareSendResult(ContactSearchKey.RecipientSearchKey recipientSearchKey, Type type) {
            this.recipientSearchKey = recipientSearchKey;
            this.type = type;
        }

        public ContactSearchKey.RecipientSearchKey getContactSearchKey() {
            return this.recipientSearchKey;
        }

        public Type getType() {
            return this.type;
        }
    }

    /* loaded from: classes4.dex */
    public static final class SlideNotFoundException extends Exception {
        private SlideNotFoundException() {
        }
    }
}
