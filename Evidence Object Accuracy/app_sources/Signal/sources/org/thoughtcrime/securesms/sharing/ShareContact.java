package org.thoughtcrime.securesms.sharing;

import j$.util.Optional;
import java.util.Objects;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public final class ShareContact {
    private final String number;
    private final Optional<RecipientId> recipientId;

    public ShareContact(RecipientId recipientId) {
        this.recipientId = Optional.of(recipientId);
        this.number = null;
    }

    public ShareContact(Optional<RecipientId> optional, String str) {
        this.recipientId = optional;
        this.number = str;
    }

    public Optional<RecipientId> getRecipientId() {
        return this.recipientId;
    }

    public String getNumber() {
        return this.number;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || ShareContact.class != obj.getClass()) {
            return false;
        }
        ShareContact shareContact = (ShareContact) obj;
        if (!this.recipientId.equals(shareContact.recipientId) || !Objects.equals(this.number, shareContact.number)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(this.recipientId, this.number);
    }
}
