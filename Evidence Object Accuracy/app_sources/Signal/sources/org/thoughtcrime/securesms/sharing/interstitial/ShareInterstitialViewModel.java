package org.thoughtcrime.securesms.sharing.interstitial;

import android.text.TextUtils;
import androidx.arch.core.util.Function;
import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.Stream;
import com.annimon.stream.function.IndexedFunction;
import java.util.List;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.sharing.MultiShareArgs;
import org.thoughtcrime.securesms.sharing.MultiShareSender;
import org.thoughtcrime.securesms.util.DefaultValueLiveData;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public class ShareInterstitialViewModel extends ViewModel {
    private final MultiShareArgs args;
    private final MutableLiveData<String> draftText;
    private LinkPreview linkPreview;
    private final MutableLiveData<MappingModelList> recipients = new MutableLiveData<>();

    ShareInterstitialViewModel(MultiShareArgs multiShareArgs, ShareInterstitialRepository shareInterstitialRepository) {
        this.args = multiShareArgs;
        this.draftText = new DefaultValueLiveData((String) Util.firstNonNull(multiShareArgs.getDraftText(), ""));
        shareInterstitialRepository.loadRecipients(multiShareArgs.getRecipientSearchKeys(), new Consumer() { // from class: org.thoughtcrime.securesms.sharing.interstitial.ShareInterstitialViewModel$$ExternalSyntheticLambda0
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                ShareInterstitialViewModel.this.lambda$new$1((List) obj);
            }
        });
    }

    public /* synthetic */ void lambda$new$1(List list) {
        this.recipients.postValue((MappingModelList) Stream.of(list).mapIndexed(new IndexedFunction() { // from class: org.thoughtcrime.securesms.sharing.interstitial.ShareInterstitialViewModel$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.IndexedFunction
            public final Object apply(int i, Object obj) {
                return ShareInterstitialViewModel.lambda$new$0(i, (Recipient) obj);
            }
        }).collect(MappingModelList.toMappingModelList()));
    }

    public static /* synthetic */ ShareInterstitialMappingModel lambda$new$0(int i, Recipient recipient) {
        return new ShareInterstitialMappingModel(recipient, i == 0);
    }

    public LiveData<MappingModelList> getRecipients() {
        return this.recipients;
    }

    public static /* synthetic */ Boolean lambda$hasDraftText$2(String str) {
        return Boolean.valueOf(!TextUtils.isEmpty(str));
    }

    public LiveData<Boolean> hasDraftText() {
        return Transformations.map(this.draftText, new Function() { // from class: org.thoughtcrime.securesms.sharing.interstitial.ShareInterstitialViewModel$$ExternalSyntheticLambda2
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ShareInterstitialViewModel.lambda$hasDraftText$2((String) obj);
            }
        });
    }

    public void onDraftTextChanged(String str) {
        this.draftText.setValue(str);
    }

    public void onLinkPreviewChanged(LinkPreview linkPreview) {
        this.linkPreview = linkPreview;
    }

    public void send(Consumer<MultiShareSender.MultiShareSendResultCollection> consumer) {
        MultiShareSender.send(this.args.buildUpon().withDraftText(this.draftText.getValue()).withLinkPreview(this.linkPreview).build(), consumer);
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes4.dex */
    public static class Factory implements ViewModelProvider.Factory {
        private final MultiShareArgs args;
        private final ShareInterstitialRepository repository;

        public Factory(MultiShareArgs multiShareArgs, ShareInterstitialRepository shareInterstitialRepository) {
            this.args = multiShareArgs;
            this.repository = shareInterstitialRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new ShareInterstitialViewModel(this.args, this.repository));
        }
    }
}
