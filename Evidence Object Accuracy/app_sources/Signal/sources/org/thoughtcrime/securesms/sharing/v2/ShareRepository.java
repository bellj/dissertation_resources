package org.thoughtcrime.securesms.sharing.v2;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import androidx.core.content.ContextCompat;
import io.reactivex.rxjava3.core.Single;
import j$.util.Optional;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.MapsKt__MapsJVMKt;
import kotlin.collections.MapsKt___MapsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.RangesKt___RangesKt;
import kotlin.text.StringsKt__StringsJVMKt;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.UriAttachment;
import org.thoughtcrime.securesms.conversation.MessageSendType;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mms.MediaConstraints;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.sharing.v2.ResolvedShareData;
import org.thoughtcrime.securesms.sharing.v2.UnresolvedShareData;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.UriUtil;
import org.thoughtcrime.securesms.util.Util;

/* compiled from: ShareRepository.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\n\u001a\u00020\u000bJ\u0010\u0010\u0007\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\rH\u0003J\u0010\u0010\u0007\u001a\u00020\t2\u0006\u0010\u000e\u001a\u00020\u000fH\u0003R\u0016\u0010\u0005\u001a\n \u0006*\u0004\u0018\u00010\u00030\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/ShareRepository;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "appContext", "kotlin.jvm.PlatformType", "resolve", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;", "unresolvedShareData", "Lorg/thoughtcrime/securesms/sharing/v2/UnresolvedShareData;", "externalMultiShare", "Lorg/thoughtcrime/securesms/sharing/v2/UnresolvedShareData$ExternalMultiShare;", "multiShareExternal", "Lorg/thoughtcrime/securesms/sharing/v2/UnresolvedShareData$ExternalSingleShare;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ShareRepository {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(ShareRepository.class);
    private final Context appContext;

    public ShareRepository(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.appContext = context.getApplicationContext();
    }

    /* renamed from: resolve$lambda-0 */
    public static final ResolvedShareData m2745resolve$lambda0(ShareRepository shareRepository, UnresolvedShareData unresolvedShareData) {
        Intrinsics.checkNotNullParameter(shareRepository, "this$0");
        Intrinsics.checkNotNullParameter(unresolvedShareData, "$unresolvedShareData");
        return shareRepository.resolve((UnresolvedShareData.ExternalMultiShare) unresolvedShareData);
    }

    public final Single<ResolvedShareData> resolve(UnresolvedShareData unresolvedShareData) {
        Intrinsics.checkNotNullParameter(unresolvedShareData, "unresolvedShareData");
        if (unresolvedShareData instanceof UnresolvedShareData.ExternalMultiShare) {
            Single<ResolvedShareData> fromCallable = Single.fromCallable(new Callable(unresolvedShareData) { // from class: org.thoughtcrime.securesms.sharing.v2.ShareRepository$$ExternalSyntheticLambda0
                public final /* synthetic */ UnresolvedShareData f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.util.concurrent.Callable
                public final Object call() {
                    return ShareRepository.$r8$lambda$2s601CW5yuaLohRVS28gfU23nxI(ShareRepository.this, this.f$1);
                }
            });
            Intrinsics.checkNotNullExpressionValue(fromCallable, "fromCallable { resolve(unresolvedShareData) }");
            return fromCallable;
        } else if (unresolvedShareData instanceof UnresolvedShareData.ExternalSingleShare) {
            Single<ResolvedShareData> fromCallable2 = Single.fromCallable(new Callable(unresolvedShareData) { // from class: org.thoughtcrime.securesms.sharing.v2.ShareRepository$$ExternalSyntheticLambda1
                public final /* synthetic */ UnresolvedShareData f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.util.concurrent.Callable
                public final Object call() {
                    return ShareRepository.$r8$lambda$4BOMuxKEiNH7Dmhs8BExTCFpXU4(ShareRepository.this, this.f$1);
                }
            });
            Intrinsics.checkNotNullExpressionValue(fromCallable2, "fromCallable { resolve(unresolvedShareData) }");
            return fromCallable2;
        } else if (unresolvedShareData instanceof UnresolvedShareData.ExternalPrimitiveShare) {
            Single<ResolvedShareData> just = Single.just(new ResolvedShareData.Primitive(((UnresolvedShareData.ExternalPrimitiveShare) unresolvedShareData).getText()));
            Intrinsics.checkNotNullExpressionValue(just, "just(ResolvedShareData.P…nresolvedShareData.text))");
            return just;
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    /* renamed from: resolve$lambda-1 */
    public static final ResolvedShareData m2746resolve$lambda1(ShareRepository shareRepository, UnresolvedShareData unresolvedShareData) {
        Intrinsics.checkNotNullParameter(shareRepository, "this$0");
        Intrinsics.checkNotNullParameter(unresolvedShareData, "$unresolvedShareData");
        return shareRepository.resolve((UnresolvedShareData.ExternalSingleShare) unresolvedShareData);
    }

    private final ResolvedShareData resolve(UnresolvedShareData.ExternalSingleShare externalSingleShare) throws IOException {
        InputStream inputStream;
        if (!UriUtil.isValidExternalUri(this.appContext, externalSingleShare.getUri())) {
            return ResolvedShareData.Failure.INSTANCE;
        }
        Uri uri = externalSingleShare.getUri();
        Companion companion = Companion;
        Context context = this.appContext;
        Intrinsics.checkNotNullExpressionValue(context, "appContext");
        String mimeType = companion.getMimeType(context, uri, externalSingleShare.getMimeType());
        try {
            inputStream = this.appContext.getContentResolver().openInputStream(uri);
        } catch (SecurityException e) {
            Log.w(TAG, "Failed to read stream!", e);
            inputStream = null;
        }
        if (inputStream == null) {
            return ResolvedShareData.Failure.INSTANCE;
        }
        Companion companion2 = Companion;
        Context context2 = this.appContext;
        Intrinsics.checkNotNullExpressionValue(context2, "appContext");
        long size = companion2.getSize(context2, uri);
        Context context3 = this.appContext;
        Intrinsics.checkNotNullExpressionValue(context3, "appContext");
        Uri createForSingleSessionOnDisk = BlobProvider.getInstance().forData(inputStream, size).withMimeType(mimeType).withFileName(companion2.getFileName(context3, uri)).createForSingleSessionOnDisk(this.appContext);
        Intrinsics.checkNotNullExpressionValue(createForSingleSessionOnDisk, "blobUri");
        Context context4 = this.appContext;
        Intrinsics.checkNotNullExpressionValue(context4, "appContext");
        return new ResolvedShareData.ExternalUri(createForSingleSessionOnDisk, mimeType, companion2.isMmsSupported(context4, companion2.asUriAttachment(createForSingleSessionOnDisk, mimeType, size)));
    }

    private final ResolvedShareData resolve(UnresolvedShareData.ExternalMultiShare externalMultiShare) {
        boolean z;
        Media media;
        InputStream openInputStream;
        List<Uri> uris = externalMultiShare.getUris();
        LinkedHashMap linkedHashMap = new LinkedHashMap(RangesKt___RangesKt.coerceAtLeast(MapsKt__MapsJVMKt.mapCapacity(CollectionsKt__IterablesKt.collectionSizeOrDefault(uris, 10)), 16));
        for (Object obj : uris) {
            Companion companion = Companion;
            Context context = this.appContext;
            Intrinsics.checkNotNullExpressionValue(context, "appContext");
            linkedHashMap.put(obj, companion.getMimeType(context, (Uri) obj, null));
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        Iterator it = linkedHashMap.entrySet().iterator();
        while (true) {
            z = false;
            if (!it.hasNext()) {
                break;
            }
            Map.Entry entry = (Map.Entry) it.next();
            String str = (String) entry.getValue();
            if (MediaUtil.isImageType(str) || MediaUtil.isVideoType(str)) {
                z = true;
            }
            if (z) {
                linkedHashMap2.put(entry.getKey(), entry.getValue());
            }
        }
        if (linkedHashMap2.isEmpty()) {
            return ResolvedShareData.Failure.INSTANCE;
        }
        List<Pair> list = CollectionsKt___CollectionsKt.take(MapsKt___MapsKt.toList(linkedHashMap2), 32);
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (Pair pair : list) {
            Uri uri = (Uri) pair.component1();
            String str2 = (String) pair.component2();
            try {
                openInputStream = this.appContext.getContentResolver().openInputStream(uri);
            } catch (IOException unused) {
                String str3 = TAG;
                Log.w(str3, "Failed to open: " + uri);
                media = null;
            }
            if (openInputStream == null) {
                return ResolvedShareData.Failure.INSTANCE;
            }
            Companion companion2 = Companion;
            Context context2 = this.appContext;
            Intrinsics.checkNotNullExpressionValue(context2, "appContext");
            long size = companion2.getSize(context2, uri);
            android.util.Pair<Integer, Integer> dimensions = MediaUtil.getDimensions(this.appContext, str2, uri);
            Intrinsics.checkNotNullExpressionValue(dimensions, "getDimensions(appContext, mimeType, uri)");
            Pair pair2 = new Pair(dimensions.first, dimensions.second);
            media = new Media(BlobProvider.getInstance().forData(openInputStream, size).withMimeType(str2).createForSingleSessionOnDisk(this.appContext), str2, System.currentTimeMillis(), ((Number) pair2.getFirst()).intValue(), ((Number) pair2.getSecond()).intValue(), size, 0, false, false, Optional.of(Media.ALL_MEDIA_BUCKET_ID), Optional.empty(), Optional.empty());
            arrayList.add(media);
        }
        List<Media> list2 = CollectionsKt___CollectionsKt.filterNotNull(arrayList);
        if (!(!list2.isEmpty())) {
            return ResolvedShareData.Failure.INSTANCE;
        }
        if (!(list2 instanceof Collection) || !list2.isEmpty()) {
            for (Media media2 : list2) {
                Companion companion3 = Companion;
                Context context3 = this.appContext;
                Intrinsics.checkNotNullExpressionValue(context3, "appContext");
                Uri uri2 = media2.getUri();
                Intrinsics.checkNotNullExpressionValue(uri2, "it.uri");
                String mimeType = media2.getMimeType();
                Intrinsics.checkNotNullExpressionValue(mimeType, "it.mimeType");
                if (!companion3.isMmsSupported(context3, companion3.asUriAttachment(uri2, mimeType, media2.getSize()))) {
                    break;
                }
            }
        }
        z = true;
        return new ResolvedShareData.Media(list2, z);
    }

    /* compiled from: ShareRepository.kt */
    @Metadata(d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\fH\u0002J\u001a\u0010\r\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\b\u001a\u00020\tH\u0002J\"\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\b\u001a\u00020\t2\b\u0010\n\u001a\u0004\u0018\u00010\u0004H\u0002J\u0018\u0010\u0011\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\b\u001a\u00020\tH\u0002J\u0018\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u0015H\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/ShareRepository$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "asUriAttachment", "Lorg/thoughtcrime/securesms/attachments/UriAttachment;", "uri", "Landroid/net/Uri;", "mimeType", MediaPreviewActivity.SIZE_EXTRA, "", "getFileName", "context", "Landroid/content/Context;", "getMimeType", "getSize", "isMmsSupported", "", "attachment", "Lorg/thoughtcrime/securesms/attachments/Attachment;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final String getMimeType(Context context, Uri uri, String str) {
            String mimeType = MediaUtil.getMimeType(context, uri);
            if (mimeType == null) {
                mimeType = MediaUtil.getCorrectedMimeType(str);
            }
            return mimeType == null ? MediaUtil.UNKNOWN : mimeType;
        }

        /* JADX INFO: finally extract failed */
        /* JADX WARNING: Removed duplicated region for block: B:13:0x0033  */
        /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final long getSize(android.content.Context r8, android.net.Uri r9) throws java.io.IOException {
            /*
                r7 = this;
                java.lang.String r0 = "_size"
                android.content.ContentResolver r1 = r8.getContentResolver()
                r3 = 0
                r4 = 0
                r5 = 0
                r6 = 0
                r2 = r9
                android.database.Cursor r1 = r1.query(r2, r3, r4, r5, r6)
                r2 = 0
                if (r1 == 0) goto L_0x0028
                boolean r4 = r1.moveToFirst()     // Catch: all -> 0x0038
                if (r4 == 0) goto L_0x0028
                int r4 = r1.getColumnIndex(r0)     // Catch: all -> 0x0038
                if (r4 < 0) goto L_0x0028
                int r0 = r1.getColumnIndexOrThrow(r0)     // Catch: all -> 0x0038
                long r4 = r1.getLong(r0)     // Catch: all -> 0x0038
                goto L_0x0029
            L_0x0028:
                r4 = r2
            L_0x0029:
                kotlin.Unit r0 = kotlin.Unit.INSTANCE     // Catch: all -> 0x0038
                r0 = 0
                kotlin.io.CloseableKt.closeFinally(r1, r0)
                int r0 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
                if (r0 > 0) goto L_0x0037
                long r4 = org.thoughtcrime.securesms.util.MediaUtil.getMediaSize(r8, r9)
            L_0x0037:
                return r4
            L_0x0038:
                r8 = move-exception
                throw r8     // Catch: all -> 0x003a
            L_0x003a:
                r9 = move-exception
                kotlin.io.CloseableKt.closeFinally(r1, r8)
                throw r9
            */
            throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.sharing.v2.ShareRepository.Companion.getSize(android.content.Context, android.net.Uri):long");
        }

        /* JADX WARN: Type inference failed for: r9v1, types: [java.lang.Throwable, java.lang.String] */
        public final String getFileName(Context context, Uri uri) {
            if (StringsKt__StringsJVMKt.equals(uri.getScheme(), "file", true)) {
                return uri.getLastPathSegment();
            }
            Cursor query = context.getContentResolver().query(uri, null, null, null, null);
            th = 0;
            if (query != null) {
                try {
                    if (query.moveToFirst() && query.getColumnIndex("_display_name") >= 0) {
                        return query.getString(query.getColumnIndexOrThrow("_display_name"));
                    }
                } finally {
                    try {
                        throw th;
                    } finally {
                    }
                }
            }
            Unit unit = Unit.INSTANCE;
            return th;
        }

        public final UriAttachment asUriAttachment(Uri uri, String str, long j) {
            return new UriAttachment(uri, str, -1, j, null, false, false, false, false, null, null, null, null, null);
        }

        public final boolean isMmsSupported(Context context, Attachment attachment) {
            boolean z = ContextCompat.checkSelfPermission(context, "android.permission.READ_PHONE_STATE") == 0;
            if (!Util.isDefaultSmsProvider(context) || !z || !Util.isMmsCapable(context)) {
                return false;
            }
            Integer simSubscriptionId = MessageSendType.Companion.getFirstForTransport(context, true, MessageSendType.TransportType.SMS).getSimSubscriptionId();
            MediaConstraints mmsMediaConstraints = MediaConstraints.getMmsMediaConstraints(simSubscriptionId != null ? simSubscriptionId.intValue() : -1);
            if (mmsMediaConstraints.isSatisfied(context, attachment) || mmsMediaConstraints.canResize(attachment)) {
                return true;
            }
            return false;
        }
    }
}
