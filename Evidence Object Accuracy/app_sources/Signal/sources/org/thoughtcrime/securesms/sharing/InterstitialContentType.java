package org.thoughtcrime.securesms.sharing;

/* loaded from: classes4.dex */
public enum InterstitialContentType {
    MEDIA,
    TEXT,
    NONE
}
