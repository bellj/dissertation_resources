package org.thoughtcrime.securesms.sharing;

import android.content.Context;
import j$.util.Optional;
import j$.util.function.Function;
import j$.util.function.Supplier;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.GroupDatabase$GroupRecord$$ExternalSyntheticLambda0;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* loaded from: classes4.dex */
public class ShareSelectionMappingModel implements MappingModel<ShareSelectionMappingModel> {
    private final boolean isFirst;
    private final ShareContact shareContact;

    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
    public /* synthetic */ Object getChangePayload(ShareSelectionMappingModel shareSelectionMappingModel) {
        return MappingModel.CC.$default$getChangePayload(this, shareSelectionMappingModel);
    }

    public ShareSelectionMappingModel(ShareContact shareContact, boolean z) {
        this.shareContact = shareContact;
        this.isFirst = z;
    }

    public String getName(Context context) {
        Optional map = this.shareContact.getRecipientId().map(new GroupDatabase$GroupRecord$$ExternalSyntheticLambda0()).map(new Function(context) { // from class: org.thoughtcrime.securesms.sharing.ShareSelectionMappingModel$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ShareSelectionMappingModel.lambda$getName$0(this.f$0, (Recipient) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        ShareContact shareContact = this.shareContact;
        Objects.requireNonNull(shareContact);
        String str = (String) map.orElseGet(new Supplier() { // from class: org.thoughtcrime.securesms.sharing.ShareSelectionMappingModel$$ExternalSyntheticLambda1
            @Override // j$.util.function.Supplier
            public final Object get() {
                return ShareContact.this.getNumber();
            }
        });
        return this.isFirst ? str : context.getString(R.string.ShareActivity__comma_s, str);
    }

    public static /* synthetic */ String lambda$getName$0(Context context, Recipient recipient) {
        if (recipient.isSelf()) {
            return context.getString(R.string.note_to_self);
        }
        return recipient.getShortDisplayNameIncludingUsername(context);
    }

    public boolean areItemsTheSame(ShareSelectionMappingModel shareSelectionMappingModel) {
        return shareSelectionMappingModel.shareContact.equals(this.shareContact);
    }

    public boolean areContentsTheSame(ShareSelectionMappingModel shareSelectionMappingModel) {
        return areItemsTheSame(shareSelectionMappingModel) && shareSelectionMappingModel.isFirst == this.isFirst;
    }
}
