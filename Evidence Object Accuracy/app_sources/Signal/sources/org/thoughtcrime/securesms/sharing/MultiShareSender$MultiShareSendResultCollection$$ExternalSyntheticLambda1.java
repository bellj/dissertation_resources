package org.thoughtcrime.securesms.sharing;

import com.annimon.stream.function.Predicate;
import org.thoughtcrime.securesms.sharing.MultiShareSender;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MultiShareSender$MultiShareSendResultCollection$$ExternalSyntheticLambda1 implements Predicate {
    @Override // com.annimon.stream.function.Predicate
    public final boolean test(Object obj) {
        return MultiShareSender.MultiShareSendResultCollection.lambda$containsOnlyFailures$1((MultiShareSender.MultiShareSendResult) obj);
    }
}
