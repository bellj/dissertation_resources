package org.thoughtcrime.securesms.sharing.interstitial;

import android.content.Context;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel;

/* loaded from: classes4.dex */
public class ShareInterstitialMappingModel extends RecipientMappingModel<ShareInterstitialMappingModel> {
    private final boolean isFirst;
    private final Recipient recipient;

    public ShareInterstitialMappingModel(Recipient recipient, boolean z) {
        this.recipient = recipient;
        this.isFirst = z;
    }

    @Override // org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel
    public String getName(Context context) {
        String str;
        if (this.recipient.isSelf()) {
            str = context.getString(R.string.note_to_self);
        } else {
            str = this.recipient.getShortDisplayNameIncludingUsername(context);
        }
        return this.isFirst ? str : context.getString(R.string.ShareActivity__comma_s, str);
    }

    @Override // org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel
    public Recipient getRecipient() {
        return this.recipient;
    }

    public boolean areContentsTheSame(ShareInterstitialMappingModel shareInterstitialMappingModel) {
        return super.areContentsTheSame(shareInterstitialMappingModel) && this.isFirst == shareInterstitialMappingModel.isFirst;
    }
}
