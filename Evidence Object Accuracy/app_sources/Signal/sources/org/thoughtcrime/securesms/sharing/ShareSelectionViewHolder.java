package org.thoughtcrime.securesms.sharing;

import android.view.View;
import android.widget.TextView;
import j$.util.function.Function;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.adapter.mapping.Factory;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* loaded from: classes4.dex */
public class ShareSelectionViewHolder extends MappingViewHolder<ShareSelectionMappingModel> {
    protected final TextView name = ((TextView) findViewById(R.id.recipient_view_name));

    public ShareSelectionViewHolder(View view) {
        super(view);
    }

    public void bind(ShareSelectionMappingModel shareSelectionMappingModel) {
        this.name.setText(shareSelectionMappingModel.getName(this.context));
    }

    public static Factory<ShareSelectionMappingModel> createFactory(int i) {
        return new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.sharing.ShareSelectionViewHolder$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new ShareSelectionViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, i);
    }
}
