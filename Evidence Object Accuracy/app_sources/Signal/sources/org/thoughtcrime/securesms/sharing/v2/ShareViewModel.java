package org.thoughtcrime.securesms.sharing.v2;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import io.reactivex.rxjava3.subjects.PublishSubject;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.collections.CollectionsKt___CollectionsJvmKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.sharing.InterstitialContentType;
import org.thoughtcrime.securesms.sharing.v2.ResolvedShareData;
import org.thoughtcrime.securesms.sharing.v2.ShareEvent;
import org.thoughtcrime.securesms.sharing.v2.ShareState;
import org.thoughtcrime.securesms.util.rx.RxStore;

/* compiled from: ShareViewModel.kt */
@Metadata(bv = {}, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 '2\u00020\u0001:\u0002'(B\u0017\u0012\u0006\u0010\"\u001a\u00020!\u0012\u0006\u0010$\u001a\u00020#¢\u0006\u0004\b%\u0010&J\u0014\u0010\u0005\u001a\u00020\u00042\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0002H\u0002J\u0014\u0010\t\u001a\u00020\u00042\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006J\b\u0010\n\u001a\u00020\u0004H\u0014R\u001a\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000b8\u0002X\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0014\u0010\u0010\u001a\u00020\u000f8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011R8\u0010\u0015\u001a&\u0012\f\u0012\n \u0014*\u0004\u0018\u00010\u00130\u0013 \u0014*\u0012\u0012\f\u0012\n \u0014*\u0004\u0018\u00010\u00130\u0013\u0018\u00010\u00120\u00128\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0015\u0010\u0016R\u001d\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\f0\u00178\u0006¢\u0006\f\n\u0004\b\u0018\u0010\u0019\u001a\u0004\b\u001a\u0010\u001bR\u001d\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00130\u001c8\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 ¨\u0006)"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/ShareViewModel;", "Landroidx/lifecycle/ViewModel;", "", "throwable", "", "moveToFailedState", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "contactSearchKeys", "onContactSelectionConfirmed", "onCleared", "Lorg/thoughtcrime/securesms/util/rx/RxStore;", "Lorg/thoughtcrime/securesms/sharing/v2/ShareState;", "store", "Lorg/thoughtcrime/securesms/util/rx/RxStore;", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "Lio/reactivex/rxjava3/subjects/PublishSubject;", "Lorg/thoughtcrime/securesms/sharing/v2/ShareEvent;", "kotlin.jvm.PlatformType", "eventSubject", "Lio/reactivex/rxjava3/subjects/PublishSubject;", "Lio/reactivex/rxjava3/core/Flowable;", "state", "Lio/reactivex/rxjava3/core/Flowable;", "getState", "()Lio/reactivex/rxjava3/core/Flowable;", "Lio/reactivex/rxjava3/core/Observable;", "events", "Lio/reactivex/rxjava3/core/Observable;", "getEvents", "()Lio/reactivex/rxjava3/core/Observable;", "Lorg/thoughtcrime/securesms/sharing/v2/UnresolvedShareData;", "unresolvedShareData", "Lorg/thoughtcrime/securesms/sharing/v2/ShareRepository;", "shareRepository", "<init>", "(Lorg/thoughtcrime/securesms/sharing/v2/UnresolvedShareData;Lorg/thoughtcrime/securesms/sharing/v2/ShareRepository;)V", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class ShareViewModel extends ViewModel {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(ShareViewModel.class);
    private final CompositeDisposable disposables;
    private final PublishSubject<ShareEvent> eventSubject;
    private final Observable<ShareEvent> events;
    private final Flowable<ShareState> state;
    private final RxStore<ShareState> store;

    /* compiled from: ShareViewModel.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[InterstitialContentType.values().length];
            iArr[InterstitialContentType.MEDIA.ordinal()] = 1;
            iArr[InterstitialContentType.TEXT.ordinal()] = 2;
            iArr[InterstitialContentType.NONE.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public ShareViewModel(UnresolvedShareData unresolvedShareData, ShareRepository shareRepository) {
        Intrinsics.checkNotNullParameter(unresolvedShareData, "unresolvedShareData");
        Intrinsics.checkNotNullParameter(shareRepository, "shareRepository");
        RxStore<ShareState> rxStore = new RxStore<>(new ShareState(null, 1, null), null, 2, null);
        this.store = rxStore;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        PublishSubject<ShareEvent> create = PublishSubject.create();
        this.eventSubject = create;
        this.state = rxStore.getStateFlowable();
        Intrinsics.checkNotNullExpressionValue(create, "eventSubject");
        this.events = create;
        DisposableKt.plusAssign(compositeDisposable, SubscribersKt.subscribeBy(shareRepository.resolve(unresolvedShareData), new Function1<Throwable, Unit>(this) { // from class: org.thoughtcrime.securesms.sharing.v2.ShareViewModel.1
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
                invoke(th);
                return Unit.INSTANCE;
            }

            public final void invoke(Throwable th) {
                ((ShareViewModel) this.receiver).moveToFailedState(th);
            }
        }, new Function1<ResolvedShareData, Unit>(this) { // from class: org.thoughtcrime.securesms.sharing.v2.ShareViewModel.2
            final /* synthetic */ ShareViewModel this$0;

            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(ResolvedShareData resolvedShareData) {
                invoke(resolvedShareData);
                return Unit.INSTANCE;
            }

            public final void invoke(final ResolvedShareData resolvedShareData) {
                Intrinsics.checkNotNullParameter(resolvedShareData, "data");
                if (Intrinsics.areEqual(resolvedShareData, ResolvedShareData.Failure.INSTANCE)) {
                    ShareViewModel.moveToFailedState$default(this.this$0, null, 1, null);
                } else {
                    this.this$0.store.update(new Function1<ShareState, ShareState>() { // from class: org.thoughtcrime.securesms.sharing.v2.ShareViewModel.2.1
                        public final ShareState invoke(ShareState shareState) {
                            Intrinsics.checkNotNullParameter(shareState, "it");
                            return shareState.copy(new ShareState.ShareDataLoadState.Loaded(resolvedShareData));
                        }
                    });
                }
            }
        }));
    }

    /* compiled from: ShareViewModel.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/ShareViewModel$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public final Flowable<ShareState> getState() {
        return this.state;
    }

    public final Observable<ShareEvent> getEvents() {
        return this.events;
    }

    public final void onContactSelectionConfirmed(List<? extends ContactSearchKey> list) {
        boolean z;
        ShareEvent shareEvent;
        Intrinsics.checkNotNullParameter(list, "contactSearchKeys");
        ShareState.ShareDataLoadState loadState = this.store.getState().getLoadState();
        if (loadState instanceof ShareState.ShareDataLoadState.Loaded) {
            List<ContactSearchKey.RecipientSearchKey> list2 = CollectionsKt___CollectionsJvmKt.filterIsInstance(list, ContactSearchKey.RecipientSearchKey.class);
            boolean z2 = false;
            if (!(list2 instanceof Collection) || !list2.isEmpty()) {
                for (ContactSearchKey.RecipientSearchKey recipientSearchKey : list2) {
                    if (recipientSearchKey.isStory()) {
                        z = true;
                        break;
                    }
                }
            }
            z = false;
            if (!z && list2.size() == 1) {
                z2 = true;
            }
            ResolvedShareData resolvedShareData = ((ShareState.ShareDataLoadState.Loaded) loadState).getResolvedShareData();
            if (z2) {
                this.eventSubject.onNext(new ShareEvent.OpenConversation(resolvedShareData, (ContactSearchKey.RecipientSearchKey) CollectionsKt___CollectionsKt.first((List) ((List<? extends Object>) list2))));
                return;
            }
            int i = WhenMappings.$EnumSwitchMapping$0[resolvedShareData.toMultiShareArgs().getInterstitialContentType().ordinal()];
            if (i == 1) {
                shareEvent = new ShareEvent.OpenMediaInterstitial(resolvedShareData, list2);
            } else if (i == 2) {
                shareEvent = new ShareEvent.OpenTextInterstitial(resolvedShareData, list2);
            } else if (i == 3) {
                shareEvent = new ShareEvent.SendWithoutInterstitial(resolvedShareData, list2);
            } else {
                throw new NoWhenBranchMatchedException();
            }
            this.eventSubject.onNext(shareEvent);
        }
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    static /* synthetic */ void moveToFailedState$default(ShareViewModel shareViewModel, Throwable th, int i, Object obj) {
        if ((i & 1) != 0) {
            th = null;
        }
        shareViewModel.moveToFailedState(th);
    }

    public final void moveToFailedState(Throwable th) {
        Log.w(TAG, "Could not load share data.", th);
        this.store.update(ShareViewModel$moveToFailedState$1.INSTANCE);
    }

    /* compiled from: ShareViewModel.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/ShareViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "unresolvedShareData", "Lorg/thoughtcrime/securesms/sharing/v2/UnresolvedShareData;", "shareRepository", "Lorg/thoughtcrime/securesms/sharing/v2/ShareRepository;", "(Lorg/thoughtcrime/securesms/sharing/v2/UnresolvedShareData;Lorg/thoughtcrime/securesms/sharing/v2/ShareRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final ShareRepository shareRepository;
        private final UnresolvedShareData unresolvedShareData;

        public Factory(UnresolvedShareData unresolvedShareData, ShareRepository shareRepository) {
            Intrinsics.checkNotNullParameter(unresolvedShareData, "unresolvedShareData");
            Intrinsics.checkNotNullParameter(shareRepository, "shareRepository");
            this.unresolvedShareData = unresolvedShareData;
            this.shareRepository = shareRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new ShareViewModel(this.unresolvedShareData, this.shareRepository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.sharing.v2.ShareViewModel.Factory.create");
        }
    }
}
