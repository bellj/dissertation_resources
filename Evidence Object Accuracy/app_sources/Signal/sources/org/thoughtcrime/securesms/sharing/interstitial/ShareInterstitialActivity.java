package org.thoughtcrime.securesms.sharing.interstitial;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import androidx.appcompat.widget.Toolbar;
import androidx.core.util.Consumer;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import java.util.Objects;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.LinkPreviewView;
import org.thoughtcrime.securesms.components.SelectionAwareEmojiEditText;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.linkpreview.LinkPreviewRepository;
import org.thoughtcrime.securesms.linkpreview.LinkPreviewViewModel;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.sharing.MultiShareArgs;
import org.thoughtcrime.securesms.sharing.MultiShareDialogs;
import org.thoughtcrime.securesms.sharing.MultiShareSender;
import org.thoughtcrime.securesms.sharing.ShareFlowConstants;
import org.thoughtcrime.securesms.sharing.interstitial.ShareInterstitialViewModel;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;
import org.thoughtcrime.securesms.util.text.AfterTextChanged;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;

/* loaded from: classes4.dex */
public class ShareInterstitialActivity extends PassphraseRequiredActivity {
    private static final String ARGS;
    private final ShareInterstitialSelectionAdapter adapter = new ShareInterstitialSelectionAdapter();
    private CircularProgressMaterialButton confirm;
    private RecyclerView contactsRecycler;
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();
    private LinkPreviewViewModel linkPreviewViewModel;
    private LinkPreviewView preview;
    private Toolbar toolbar;
    private ShareInterstitialViewModel viewModel;

    public static Intent createIntent(Context context, MultiShareArgs multiShareArgs) {
        Intent intent = new Intent(context, ShareInterstitialActivity.class);
        intent.putExtra("args", multiShareArgs);
        return intent;
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        this.dynamicTheme.onCreate(this);
        setContentView(R.layout.share_interstitial_activity);
        MultiShareArgs multiShareArgs = (MultiShareArgs) getIntent().getParcelableExtra("args");
        initializeViewModels(multiShareArgs);
        initializeViews(multiShareArgs);
        initializeObservers();
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }

    private void initializeViewModels(MultiShareArgs multiShareArgs) {
        this.viewModel = (ShareInterstitialViewModel) new ViewModelProvider(this, new ShareInterstitialViewModel.Factory(multiShareArgs, new ShareInterstitialRepository())).get(ShareInterstitialViewModel.class);
        this.linkPreviewViewModel = (LinkPreviewViewModel) new ViewModelProvider(this, new LinkPreviewViewModel.Factory(new LinkPreviewRepository())).get(LinkPreviewViewModel.class);
        if (Stream.of(multiShareArgs.getRecipientSearchKeys()).anyMatch(new Predicate() { // from class: org.thoughtcrime.securesms.sharing.interstitial.ShareInterstitialActivity$$ExternalSyntheticLambda9
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ShareInterstitialActivity.$r8$lambda$Thx29PBFNlsutAiRmqMIdRzedHo((ContactSearchKey.RecipientSearchKey) obj);
            }
        })) {
            this.linkPreviewViewModel.onTransportChanged(true);
        }
    }

    public static /* synthetic */ boolean lambda$initializeViewModels$0(ContactSearchKey.RecipientSearchKey recipientSearchKey) {
        Recipient resolved = Recipient.resolved(recipientSearchKey.getRecipientId());
        if (resolved.isDistributionList()) {
            return false;
        }
        if (!resolved.isRegistered() || resolved.isForceSmsSelection()) {
            return true;
        }
        return false;
    }

    private void initializeViews(MultiShareArgs multiShareArgs) {
        this.confirm = (CircularProgressMaterialButton) findViewById(R.id.share_confirm);
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.preview = (LinkPreviewView) findViewById(R.id.link_preview);
        this.confirm.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.sharing.interstitial.ShareInterstitialActivity$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ShareInterstitialActivity.$r8$lambda$zbeHTgsUGF4yzQOIq6w2aNpIG4g(ShareInterstitialActivity.this, view);
            }
        });
        SelectionAwareEmojiEditText selectionAwareEmojiEditText = (SelectionAwareEmojiEditText) findViewById(R.id.text);
        this.toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.sharing.interstitial.ShareInterstitialActivity$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ShareInterstitialActivity.$r8$lambda$_7_G0YbDIMaDCb85xPHGHPlK09g(ShareInterstitialActivity.this, view);
            }
        });
        selectionAwareEmojiEditText.addTextChangedListener(new AfterTextChanged(new Consumer(selectionAwareEmojiEditText) { // from class: org.thoughtcrime.securesms.sharing.interstitial.ShareInterstitialActivity$$ExternalSyntheticLambda4
            public final /* synthetic */ SelectionAwareEmojiEditText f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                ShareInterstitialActivity.$r8$lambda$jnHMjTf1SNApD4FA1XmVM3sBYz0(ShareInterstitialActivity.this, this.f$1, (Editable) obj);
            }
        }));
        selectionAwareEmojiEditText.setOnSelectionChangedListener(new SelectionAwareEmojiEditText.OnSelectionChangedListener(selectionAwareEmojiEditText) { // from class: org.thoughtcrime.securesms.sharing.interstitial.ShareInterstitialActivity$$ExternalSyntheticLambda5
            public final /* synthetic */ SelectionAwareEmojiEditText f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.components.SelectionAwareEmojiEditText.OnSelectionChangedListener
            public final void onSelectionChanged(int i, int i2) {
                ShareInterstitialActivity.$r8$lambda$T9aCYQ08twzNWFOvYT2uRS1gDsI(ShareInterstitialActivity.this, this.f$1, i, i2);
            }
        });
        LinkPreviewView linkPreviewView = this.preview;
        LinkPreviewViewModel linkPreviewViewModel = this.linkPreviewViewModel;
        Objects.requireNonNull(linkPreviewViewModel);
        linkPreviewView.setCloseClickedListener(new LinkPreviewView.CloseClickedListener() { // from class: org.thoughtcrime.securesms.sharing.interstitial.ShareInterstitialActivity$$ExternalSyntheticLambda6
            @Override // org.thoughtcrime.securesms.components.LinkPreviewView.CloseClickedListener
            public final void onCloseClicked() {
                LinkPreviewViewModel.this.onUserCancel();
            }
        });
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.thumbnail_default_radius);
        this.preview.setCorners(dimensionPixelSize, dimensionPixelSize);
        selectionAwareEmojiEditText.setText(multiShareArgs.getDraftText());
        ViewUtil.focusAndMoveCursorToEndAndOpenKeyboard(selectionAwareEmojiEditText);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.selected_list);
        this.contactsRecycler = recyclerView;
        recyclerView.setAdapter(this.adapter);
        RecyclerView.ItemAnimator itemAnimator = this.contactsRecycler.getItemAnimator();
        Objects.requireNonNull(itemAnimator);
        ShareFlowConstants.applySelectedContactsRecyclerAnimationSpeeds(itemAnimator);
        this.confirm.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: org.thoughtcrime.securesms.sharing.interstitial.ShareInterstitialActivity$$ExternalSyntheticLambda7
            @Override // android.view.View.OnLayoutChangeListener
            public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                ShareInterstitialActivity.m2732$r8$lambda$TNkjAK17jUZA_bnMmvgoe7b_v8(ShareInterstitialActivity.this, view, i, i2, i3, i4, i5, i6, i7, i8);
            }
        });
    }

    public /* synthetic */ void lambda$initializeViews$1(View view) {
        onConfirm();
    }

    public /* synthetic */ void lambda$initializeViews$2(View view) {
        finish();
    }

    public /* synthetic */ void lambda$initializeViews$3(SelectionAwareEmojiEditText selectionAwareEmojiEditText, Editable editable) {
        this.linkPreviewViewModel.onTextChanged(this, editable.toString(), selectionAwareEmojiEditText.getSelectionStart(), selectionAwareEmojiEditText.getSelectionEnd());
        this.viewModel.onDraftTextChanged(editable.toString());
    }

    public /* synthetic */ void lambda$initializeViews$4(SelectionAwareEmojiEditText selectionAwareEmojiEditText, int i, int i2) {
        this.linkPreviewViewModel.onTextChanged(this, selectionAwareEmojiEditText.getText().toString(), selectionAwareEmojiEditText.getSelectionStart(), selectionAwareEmojiEditText.getSelectionEnd());
    }

    public /* synthetic */ void lambda$initializeViews$5(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        ViewUtil.setPaddingEnd(this.contactsRecycler, Math.abs(view.getWidth() + ViewUtil.dpToPx(16)));
    }

    private void initializeObservers() {
        this.viewModel.getRecipients().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.sharing.interstitial.ShareInterstitialActivity$$ExternalSyntheticLambda10
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ShareInterstitialActivity.$r8$lambda$nNPeehhBt5jTxsNBAuJtGd6ZbIY(ShareInterstitialActivity.this, (MappingModelList) obj);
            }
        });
        this.viewModel.hasDraftText().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.sharing.interstitial.ShareInterstitialActivity$$ExternalSyntheticLambda11
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ShareInterstitialActivity.m2734$r8$lambda$i6p1NyeJEHysgBYI02I_jQNpvs(ShareInterstitialActivity.this, ((Boolean) obj).booleanValue());
            }
        });
        this.linkPreviewViewModel.getLinkPreviewState().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.sharing.interstitial.ShareInterstitialActivity$$ExternalSyntheticLambda12
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ShareInterstitialActivity.m2735$r8$lambda$thKMDk_frwgZyML53NghMwLGDc(ShareInterstitialActivity.this, (LinkPreviewViewModel.LinkPreviewState) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializeObservers$7(MappingModelList mappingModelList) {
        this.adapter.submitList(mappingModelList, new Runnable(mappingModelList) { // from class: org.thoughtcrime.securesms.sharing.interstitial.ShareInterstitialActivity$$ExternalSyntheticLambda0
            public final /* synthetic */ MappingModelList f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ShareInterstitialActivity.$r8$lambda$4a5iEAdZQ_auLJkYbiYlfM9e3PI(ShareInterstitialActivity.this, this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$initializeObservers$6(MappingModelList mappingModelList) {
        this.contactsRecycler.scrollToPosition(mappingModelList.size() - 1);
    }

    public /* synthetic */ void lambda$initializeObservers$8(LinkPreviewViewModel.LinkPreviewState linkPreviewState) {
        this.preview.setVisibility(0);
        if (linkPreviewState.getError() != null) {
            this.preview.setNoPreview(linkPreviewState.getError());
            this.viewModel.onLinkPreviewChanged(null);
        } else if (linkPreviewState.isLoading()) {
            this.preview.setLoading();
            this.viewModel.onLinkPreviewChanged(null);
        } else if (linkPreviewState.getLinkPreview().isPresent()) {
            this.preview.setLinkPreview(GlideApp.with((FragmentActivity) this), linkPreviewState.getLinkPreview().get(), true);
            this.viewModel.onLinkPreviewChanged(linkPreviewState.getLinkPreview().get());
        } else if (!linkPreviewState.hasLinks()) {
            this.preview.setVisibility(8);
            this.viewModel.onLinkPreviewChanged(null);
        }
    }

    public void handleHasDraftText(boolean z) {
        this.confirm.setEnabled(z);
        this.confirm.setAlpha(z ? 1.0f : 0.5f);
    }

    private void onConfirm() {
        this.confirm.setSpinning();
        this.viewModel.send(new Consumer() { // from class: org.thoughtcrime.securesms.sharing.interstitial.ShareInterstitialActivity$$ExternalSyntheticLambda8
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                ShareInterstitialActivity.$r8$lambda$facABGqlDKE1AcyA0PH39n30cuk(ShareInterstitialActivity.this, (MultiShareSender.MultiShareSendResultCollection) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onConfirm$10(MultiShareSender.MultiShareSendResultCollection multiShareSendResultCollection) {
        MultiShareDialogs.displayResultDialog(this, multiShareSendResultCollection, new Runnable() { // from class: org.thoughtcrime.securesms.sharing.interstitial.ShareInterstitialActivity$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                ShareInterstitialActivity.m2733$r8$lambda$dNErz2_5CFvmipDpyBFrPwzoA(ShareInterstitialActivity.this);
            }
        });
    }

    public /* synthetic */ void lambda$onConfirm$9() {
        setResult(-1);
        finish();
    }
}
