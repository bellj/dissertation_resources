package org.thoughtcrime.securesms.sharing;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.annimon.stream.Stream;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import org.signal.core.util.BreakIteratorCompat;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.stickers.StickerLocator;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.ParcelUtil;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public final class MultiShareArgs implements Parcelable {
    public static final Parcelable.Creator<MultiShareArgs> CREATOR = new Parcelable.Creator<MultiShareArgs>() { // from class: org.thoughtcrime.securesms.sharing.MultiShareArgs.1
        @Override // android.os.Parcelable.Creator
        public MultiShareArgs createFromParcel(Parcel parcel) {
            return new MultiShareArgs(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public MultiShareArgs[] newArray(int i) {
            return new MultiShareArgs[i];
        }
    };
    private final boolean borderless;
    private final Set<ContactSearchKey> contactSearchKeys;
    private final String dataType;
    private final Uri dataUri;
    private final String draftText;
    private final long expiresAt;
    private final boolean isTextStory;
    private final LinkPreview linkPreview;
    private final List<Media> media;
    private final List<Mention> mentions;
    private final StickerLocator stickerLocator;
    private final long timestamp;
    private final boolean viewOnce;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    private MultiShareArgs(Builder builder) {
        this.contactSearchKeys = builder.contactSearchKeys;
        this.media = builder.media == null ? new ArrayList() : new ArrayList(builder.media);
        this.draftText = builder.draftText;
        this.stickerLocator = builder.stickerLocator;
        this.borderless = builder.borderless;
        this.dataUri = builder.dataUri;
        this.dataType = builder.dataType;
        this.viewOnce = builder.viewOnce;
        this.linkPreview = builder.linkPreview;
        this.mentions = builder.mentions == null ? new ArrayList() : new ArrayList(builder.mentions);
        this.timestamp = builder.timestamp;
        this.expiresAt = builder.expiresAt;
        this.isTextStory = builder.isTextStory;
    }

    protected MultiShareArgs(Parcel parcel) {
        this.contactSearchKeys = (Set) Collection$EL.stream(parcel.createTypedArrayList(ContactSearchKey.ParcelableRecipientSearchKey.CREATOR)).map(new Function() { // from class: org.thoughtcrime.securesms.sharing.MultiShareArgs$$ExternalSyntheticLambda3
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((ContactSearchKey.ParcelableRecipientSearchKey) obj).asRecipientSearchKey();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toSet());
        this.media = parcel.createTypedArrayList(Media.CREATOR);
        this.draftText = parcel.readString();
        this.stickerLocator = (StickerLocator) parcel.readParcelable(StickerLocator.class.getClassLoader());
        boolean z = true;
        this.borderless = parcel.readByte() != 0;
        this.dataUri = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        this.dataType = parcel.readString();
        this.viewOnce = parcel.readByte() == 0 ? false : z;
        this.mentions = parcel.createTypedArrayList(Mention.CREATOR);
        this.timestamp = parcel.readLong();
        this.expiresAt = parcel.readLong();
        this.isTextStory = ParcelUtil.readBoolean(parcel);
        String readString = parcel.readString();
        LinkPreview linkPreview = null;
        if (readString != null) {
            try {
                linkPreview = LinkPreview.deserialize(readString);
            } catch (IOException unused) {
            }
        }
        this.linkPreview = linkPreview;
    }

    public Set<ContactSearchKey> getContactSearchKeys() {
        return this.contactSearchKeys;
    }

    public Set<ContactSearchKey.RecipientSearchKey> getRecipientSearchKeys() {
        return (Set) Collection$EL.stream(this.contactSearchKeys).filter(new Predicate() { // from class: org.thoughtcrime.securesms.sharing.MultiShareArgs$$ExternalSyntheticLambda4
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return MultiShareArgs.lambda$getRecipientSearchKeys$0((ContactSearchKey) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.sharing.MultiShareArgs$$ExternalSyntheticLambda5
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return MultiShareArgs.lambda$getRecipientSearchKeys$1((ContactSearchKey) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toSet());
    }

    public static /* synthetic */ boolean lambda$getRecipientSearchKeys$0(ContactSearchKey contactSearchKey) {
        return contactSearchKey instanceof ContactSearchKey.RecipientSearchKey;
    }

    public static /* synthetic */ ContactSearchKey.RecipientSearchKey lambda$getRecipientSearchKeys$1(ContactSearchKey contactSearchKey) {
        return (ContactSearchKey.RecipientSearchKey) contactSearchKey;
    }

    public List<Media> getMedia() {
        return this.media;
    }

    public StickerLocator getStickerLocator() {
        return this.stickerLocator;
    }

    public String getDataType() {
        return this.dataType;
    }

    public String getDraftText() {
        return this.draftText;
    }

    public Uri getDataUri() {
        return this.dataUri;
    }

    public boolean isBorderless() {
        return this.borderless;
    }

    public boolean isViewOnce() {
        return this.viewOnce;
    }

    public boolean isTextStory() {
        return this.isTextStory;
    }

    public LinkPreview getLinkPreview() {
        return this.linkPreview;
    }

    public List<Mention> getMentions() {
        return this.mentions;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public long getExpiresAt() {
        return this.expiresAt;
    }

    public boolean isValidForStories() {
        return this.isTextStory || (!this.media.isEmpty() && Collection$EL.stream(this.media).allMatch(new Predicate() { // from class: org.thoughtcrime.securesms.sharing.MultiShareArgs$$ExternalSyntheticLambda1
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return MultiShareArgs.lambda$isValidForStories$2((Media) obj);
            }
        })) || MediaUtil.isStorySupportedType(this.dataType) || isValidForTextStoryGeneration();
    }

    public static /* synthetic */ boolean lambda$isValidForStories$2(Media media) {
        return MediaUtil.isStorySupportedType(media.getMimeType());
    }

    public boolean isValidForNonStories() {
        return !this.isTextStory;
    }

    public boolean isValidForTextStoryGeneration() {
        if (this.isTextStory || !this.media.isEmpty()) {
            return false;
        }
        if (!Util.isEmpty(getDraftText())) {
            BreakIteratorCompat instance = BreakIteratorCompat.getInstance();
            instance.setText(getDraftText());
            if (instance.countBreaks() > 700) {
                return false;
            }
        }
        if (this.linkPreview != null || !Util.isEmpty(this.draftText)) {
            return true;
        }
        return false;
    }

    public InterstitialContentType getInterstitialContentType() {
        if (!requiresInterstitial()) {
            return InterstitialContentType.NONE;
        }
        if (!getMedia().isEmpty() || (getDataUri() != null && getDataUri() != Uri.EMPTY && getDataType() != null && MediaUtil.isImageOrVideoType(getDataType()))) {
            return InterstitialContentType.MEDIA;
        }
        if (!TextUtils.isEmpty(getDraftText()) && allRecipientsAreStories()) {
            return InterstitialContentType.MEDIA;
        }
        if (!TextUtils.isEmpty(getDraftText())) {
            return InterstitialContentType.TEXT;
        }
        return InterstitialContentType.NONE;
    }

    public static /* synthetic */ boolean lambda$allRecipientsAreStories$3(ContactSearchKey contactSearchKey) {
        return contactSearchKey instanceof ContactSearchKey.RecipientSearchKey.Story;
    }

    public boolean allRecipientsAreStories() {
        return !this.contactSearchKeys.isEmpty() && Collection$EL.stream(this.contactSearchKeys).allMatch(new Predicate() { // from class: org.thoughtcrime.securesms.sharing.MultiShareArgs$$ExternalSyntheticLambda2
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return MultiShareArgs.lambda$allRecipientsAreStories$3((ContactSearchKey) obj);
            }
        });
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(Stream.of(this.contactSearchKeys).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.sharing.MultiShareArgs$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ((ContactSearchKey) obj).requireParcelable();
            }
        }).toList());
        parcel.writeTypedList(this.media);
        parcel.writeString(this.draftText);
        parcel.writeParcelable(this.stickerLocator, i);
        parcel.writeByte(this.borderless ? (byte) 1 : 0);
        parcel.writeParcelable(this.dataUri, i);
        parcel.writeString(this.dataType);
        parcel.writeByte(this.viewOnce ? (byte) 1 : 0);
        parcel.writeTypedList(this.mentions);
        parcel.writeLong(this.timestamp);
        parcel.writeLong(this.expiresAt);
        ParcelUtil.writeBoolean(parcel, this.isTextStory);
        LinkPreview linkPreview = this.linkPreview;
        if (linkPreview != null) {
            try {
                parcel.writeString(linkPreview.serialize());
            } catch (IOException unused) {
                parcel.writeString("");
            }
        } else {
            parcel.writeString("");
        }
    }

    public Builder buildUpon() {
        return buildUpon(this.contactSearchKeys);
    }

    public Builder buildUpon(Set<ContactSearchKey> set) {
        return new Builder(set).asBorderless(this.borderless).asViewOnce(this.viewOnce).withDataType(this.dataType).withDataUri(this.dataUri).withDraftText(this.draftText).withLinkPreview(this.linkPreview).withMedia(this.media).withStickerLocator(this.stickerLocator).withMentions(this.mentions).withTimestamp(this.timestamp).withExpiration(this.expiresAt).asTextStory(this.isTextStory);
    }

    private boolean requiresInterstitial() {
        return this.stickerLocator == null && (!this.media.isEmpty() || !TextUtils.isEmpty(this.draftText) || MediaUtil.isImageOrVideoType(this.dataType) || (!this.contactSearchKeys.isEmpty() && Collection$EL.stream(this.contactSearchKeys).anyMatch(new Predicate() { // from class: org.thoughtcrime.securesms.sharing.MultiShareArgs$$ExternalSyntheticLambda6
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return MultiShareArgs.lambda$requiresInterstitial$4((ContactSearchKey) obj);
            }
        })));
    }

    public static /* synthetic */ boolean lambda$requiresInterstitial$4(ContactSearchKey contactSearchKey) {
        return contactSearchKey instanceof ContactSearchKey.RecipientSearchKey.Story;
    }

    /* loaded from: classes4.dex */
    public static final class Builder {
        private boolean borderless;
        private final Set<ContactSearchKey> contactSearchKeys;
        private String dataType;
        private Uri dataUri;
        private String draftText;
        private long expiresAt;
        private boolean isTextStory;
        private LinkPreview linkPreview;
        private List<Media> media;
        private List<Mention> mentions;
        private StickerLocator stickerLocator;
        private long timestamp;
        private boolean viewOnce;

        public Builder() {
            this(Collections.emptySet());
        }

        public Builder(Set<ContactSearchKey> set) {
            this.contactSearchKeys = set;
        }

        public Builder withMedia(List<Media> list) {
            this.media = list != null ? new ArrayList(list) : null;
            return this;
        }

        public Builder withDraftText(String str) {
            this.draftText = str;
            return this;
        }

        public Builder withStickerLocator(StickerLocator stickerLocator) {
            this.stickerLocator = stickerLocator;
            return this;
        }

        public Builder asBorderless(boolean z) {
            this.borderless = z;
            return this;
        }

        public Builder withDataUri(Uri uri) {
            this.dataUri = uri;
            return this;
        }

        public Builder withDataType(String str) {
            this.dataType = str;
            return this;
        }

        public Builder withLinkPreview(LinkPreview linkPreview) {
            this.linkPreview = linkPreview;
            return this;
        }

        public Builder asViewOnce(boolean z) {
            this.viewOnce = z;
            return this;
        }

        public Builder withMentions(List<Mention> list) {
            this.mentions = list != null ? new ArrayList(list) : null;
            return this;
        }

        public Builder withTimestamp(long j) {
            this.timestamp = j;
            return this;
        }

        public Builder withExpiration(long j) {
            this.expiresAt = j;
            return this;
        }

        public Builder asTextStory(boolean z) {
            this.isTextStory = z;
            return this;
        }

        public MultiShareArgs build() {
            return new MultiShareArgs(this);
        }
    }
}
