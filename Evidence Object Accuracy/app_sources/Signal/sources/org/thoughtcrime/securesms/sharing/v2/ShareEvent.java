package org.thoughtcrime.securesms.sharing.v2;

import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.contactshare.ContactShareEditActivity;
import org.thoughtcrime.securesms.sharing.MultiShareArgs;

/* compiled from: ShareEvent.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u000e\u000f\u0010\u0011B\u0007\b\u0004¢\u0006\u0002\u0010\u0002J\u0006\u0010\f\u001a\u00020\rR\u0018\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¤\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0012\u0010\b\u001a\u00020\tX¤\u0004¢\u0006\u0006\u001a\u0004\b\n\u0010\u000b\u0001\u0004\u0012\u0013\u0014\u0015¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/ShareEvent;", "", "()V", ContactShareEditActivity.KEY_CONTACTS, "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "getContacts", "()Ljava/util/List;", "shareData", "Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;", "getShareData", "()Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;", "getMultiShareArgs", "Lorg/thoughtcrime/securesms/sharing/MultiShareArgs;", "OpenConversation", "OpenMediaInterstitial", "OpenTextInterstitial", "SendWithoutInterstitial", "Lorg/thoughtcrime/securesms/sharing/v2/ShareEvent$OpenConversation;", "Lorg/thoughtcrime/securesms/sharing/v2/ShareEvent$OpenMediaInterstitial;", "Lorg/thoughtcrime/securesms/sharing/v2/ShareEvent$OpenTextInterstitial;", "Lorg/thoughtcrime/securesms/sharing/v2/ShareEvent$SendWithoutInterstitial;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class ShareEvent {
    public /* synthetic */ ShareEvent(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    protected abstract List<ContactSearchKey.RecipientSearchKey> getContacts();

    protected abstract ResolvedShareData getShareData();

    private ShareEvent() {
    }

    public final MultiShareArgs getMultiShareArgs() {
        MultiShareArgs build = getShareData().toMultiShareArgs().buildUpon(CollectionsKt___CollectionsKt.toSet(getContacts())).build();
        Intrinsics.checkNotNullExpressionValue(build, "shareData.toMultiShareAr…cts.toSet()\n    ).build()");
        return build;
    }

    /* compiled from: ShareEvent.kt */
    @Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\b\b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000f\u001a\u00020\u0003HÄ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015HÖ\u0003J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00050\nX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/ShareEvent$OpenConversation;", "Lorg/thoughtcrime/securesms/sharing/v2/ShareEvent;", "shareData", "Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;", "contact", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "(Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;)V", "getContact", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", ContactShareEditActivity.KEY_CONTACTS, "", "getContacts", "()Ljava/util/List;", "getShareData", "()Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class OpenConversation extends ShareEvent {
        private final ContactSearchKey.RecipientSearchKey contact;
        private final List<ContactSearchKey.RecipientSearchKey> contacts;
        private final ResolvedShareData shareData;

        public static /* synthetic */ OpenConversation copy$default(OpenConversation openConversation, ResolvedShareData resolvedShareData, ContactSearchKey.RecipientSearchKey recipientSearchKey, int i, Object obj) {
            if ((i & 1) != 0) {
                resolvedShareData = openConversation.getShareData();
            }
            if ((i & 2) != 0) {
                recipientSearchKey = openConversation.contact;
            }
            return openConversation.copy(resolvedShareData, recipientSearchKey);
        }

        protected final ResolvedShareData component1() {
            return getShareData();
        }

        public final ContactSearchKey.RecipientSearchKey component2() {
            return this.contact;
        }

        public final OpenConversation copy(ResolvedShareData resolvedShareData, ContactSearchKey.RecipientSearchKey recipientSearchKey) {
            Intrinsics.checkNotNullParameter(resolvedShareData, "shareData");
            Intrinsics.checkNotNullParameter(recipientSearchKey, "contact");
            return new OpenConversation(resolvedShareData, recipientSearchKey);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof OpenConversation)) {
                return false;
            }
            OpenConversation openConversation = (OpenConversation) obj;
            return Intrinsics.areEqual(getShareData(), openConversation.getShareData()) && Intrinsics.areEqual(this.contact, openConversation.contact);
        }

        public int hashCode() {
            return (getShareData().hashCode() * 31) + this.contact.hashCode();
        }

        public String toString() {
            return "OpenConversation(shareData=" + getShareData() + ", contact=" + this.contact + ')';
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public OpenConversation(ResolvedShareData resolvedShareData, ContactSearchKey.RecipientSearchKey recipientSearchKey) {
            super(null);
            Intrinsics.checkNotNullParameter(resolvedShareData, "shareData");
            Intrinsics.checkNotNullParameter(recipientSearchKey, "contact");
            this.shareData = resolvedShareData;
            this.contact = recipientSearchKey;
            this.contacts = CollectionsKt__CollectionsJVMKt.listOf(recipientSearchKey);
        }

        public final ContactSearchKey.RecipientSearchKey getContact() {
            return this.contact;
        }

        @Override // org.thoughtcrime.securesms.sharing.v2.ShareEvent
        protected ResolvedShareData getShareData() {
            return this.shareData;
        }

        @Override // org.thoughtcrime.securesms.sharing.v2.ShareEvent
        protected List<ContactSearchKey.RecipientSearchKey> getContacts() {
            return this.contacts;
        }
    }

    /* compiled from: ShareEvent.kt */
    @Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\f\u001a\u00020\u0003HÄ\u0003J\u000f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÄ\u0003J#\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012HÖ\u0003J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001R\u001a\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/ShareEvent$OpenMediaInterstitial;", "Lorg/thoughtcrime/securesms/sharing/v2/ShareEvent;", "shareData", "Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;", ContactShareEditActivity.KEY_CONTACTS, "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "(Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;Ljava/util/List;)V", "getContacts", "()Ljava/util/List;", "getShareData", "()Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class OpenMediaInterstitial extends ShareEvent {
        private final List<ContactSearchKey.RecipientSearchKey> contacts;
        private final ResolvedShareData shareData;

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.sharing.v2.ShareEvent$OpenMediaInterstitial */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ OpenMediaInterstitial copy$default(OpenMediaInterstitial openMediaInterstitial, ResolvedShareData resolvedShareData, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                resolvedShareData = openMediaInterstitial.getShareData();
            }
            if ((i & 2) != 0) {
                list = openMediaInterstitial.getContacts();
            }
            return openMediaInterstitial.copy(resolvedShareData, list);
        }

        protected final ResolvedShareData component1() {
            return getShareData();
        }

        protected final List<ContactSearchKey.RecipientSearchKey> component2() {
            return getContacts();
        }

        public final OpenMediaInterstitial copy(ResolvedShareData resolvedShareData, List<? extends ContactSearchKey.RecipientSearchKey> list) {
            Intrinsics.checkNotNullParameter(resolvedShareData, "shareData");
            Intrinsics.checkNotNullParameter(list, ContactShareEditActivity.KEY_CONTACTS);
            return new OpenMediaInterstitial(resolvedShareData, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof OpenMediaInterstitial)) {
                return false;
            }
            OpenMediaInterstitial openMediaInterstitial = (OpenMediaInterstitial) obj;
            return Intrinsics.areEqual(getShareData(), openMediaInterstitial.getShareData()) && Intrinsics.areEqual(getContacts(), openMediaInterstitial.getContacts());
        }

        public int hashCode() {
            return (getShareData().hashCode() * 31) + getContacts().hashCode();
        }

        public String toString() {
            return "OpenMediaInterstitial(shareData=" + getShareData() + ", contacts=" + getContacts() + ')';
        }

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.contacts.paged.ContactSearchKey$RecipientSearchKey> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public OpenMediaInterstitial(ResolvedShareData resolvedShareData, List<? extends ContactSearchKey.RecipientSearchKey> list) {
            super(null);
            Intrinsics.checkNotNullParameter(resolvedShareData, "shareData");
            Intrinsics.checkNotNullParameter(list, ContactShareEditActivity.KEY_CONTACTS);
            this.shareData = resolvedShareData;
            this.contacts = list;
        }

        @Override // org.thoughtcrime.securesms.sharing.v2.ShareEvent
        protected List<ContactSearchKey.RecipientSearchKey> getContacts() {
            return this.contacts;
        }

        @Override // org.thoughtcrime.securesms.sharing.v2.ShareEvent
        protected ResolvedShareData getShareData() {
            return this.shareData;
        }
    }

    /* compiled from: ShareEvent.kt */
    @Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\f\u001a\u00020\u0003HÄ\u0003J\u000f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÄ\u0003J#\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012HÖ\u0003J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001R\u001a\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/ShareEvent$OpenTextInterstitial;", "Lorg/thoughtcrime/securesms/sharing/v2/ShareEvent;", "shareData", "Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;", ContactShareEditActivity.KEY_CONTACTS, "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "(Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;Ljava/util/List;)V", "getContacts", "()Ljava/util/List;", "getShareData", "()Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class OpenTextInterstitial extends ShareEvent {
        private final List<ContactSearchKey.RecipientSearchKey> contacts;
        private final ResolvedShareData shareData;

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.sharing.v2.ShareEvent$OpenTextInterstitial */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ OpenTextInterstitial copy$default(OpenTextInterstitial openTextInterstitial, ResolvedShareData resolvedShareData, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                resolvedShareData = openTextInterstitial.getShareData();
            }
            if ((i & 2) != 0) {
                list = openTextInterstitial.getContacts();
            }
            return openTextInterstitial.copy(resolvedShareData, list);
        }

        protected final ResolvedShareData component1() {
            return getShareData();
        }

        protected final List<ContactSearchKey.RecipientSearchKey> component2() {
            return getContacts();
        }

        public final OpenTextInterstitial copy(ResolvedShareData resolvedShareData, List<? extends ContactSearchKey.RecipientSearchKey> list) {
            Intrinsics.checkNotNullParameter(resolvedShareData, "shareData");
            Intrinsics.checkNotNullParameter(list, ContactShareEditActivity.KEY_CONTACTS);
            return new OpenTextInterstitial(resolvedShareData, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof OpenTextInterstitial)) {
                return false;
            }
            OpenTextInterstitial openTextInterstitial = (OpenTextInterstitial) obj;
            return Intrinsics.areEqual(getShareData(), openTextInterstitial.getShareData()) && Intrinsics.areEqual(getContacts(), openTextInterstitial.getContacts());
        }

        public int hashCode() {
            return (getShareData().hashCode() * 31) + getContacts().hashCode();
        }

        public String toString() {
            return "OpenTextInterstitial(shareData=" + getShareData() + ", contacts=" + getContacts() + ')';
        }

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.contacts.paged.ContactSearchKey$RecipientSearchKey> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public OpenTextInterstitial(ResolvedShareData resolvedShareData, List<? extends ContactSearchKey.RecipientSearchKey> list) {
            super(null);
            Intrinsics.checkNotNullParameter(resolvedShareData, "shareData");
            Intrinsics.checkNotNullParameter(list, ContactShareEditActivity.KEY_CONTACTS);
            this.shareData = resolvedShareData;
            this.contacts = list;
        }

        @Override // org.thoughtcrime.securesms.sharing.v2.ShareEvent
        protected List<ContactSearchKey.RecipientSearchKey> getContacts() {
            return this.contacts;
        }

        @Override // org.thoughtcrime.securesms.sharing.v2.ShareEvent
        protected ResolvedShareData getShareData() {
            return this.shareData;
        }
    }

    /* compiled from: ShareEvent.kt */
    @Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\f\u001a\u00020\u0003HÄ\u0003J\u000f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÄ\u0003J#\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012HÖ\u0003J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001R\u001a\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/ShareEvent$SendWithoutInterstitial;", "Lorg/thoughtcrime/securesms/sharing/v2/ShareEvent;", "shareData", "Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;", ContactShareEditActivity.KEY_CONTACTS, "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "(Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;Ljava/util/List;)V", "getContacts", "()Ljava/util/List;", "getShareData", "()Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class SendWithoutInterstitial extends ShareEvent {
        private final List<ContactSearchKey.RecipientSearchKey> contacts;
        private final ResolvedShareData shareData;

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.sharing.v2.ShareEvent$SendWithoutInterstitial */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ SendWithoutInterstitial copy$default(SendWithoutInterstitial sendWithoutInterstitial, ResolvedShareData resolvedShareData, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                resolvedShareData = sendWithoutInterstitial.getShareData();
            }
            if ((i & 2) != 0) {
                list = sendWithoutInterstitial.getContacts();
            }
            return sendWithoutInterstitial.copy(resolvedShareData, list);
        }

        protected final ResolvedShareData component1() {
            return getShareData();
        }

        protected final List<ContactSearchKey.RecipientSearchKey> component2() {
            return getContacts();
        }

        public final SendWithoutInterstitial copy(ResolvedShareData resolvedShareData, List<? extends ContactSearchKey.RecipientSearchKey> list) {
            Intrinsics.checkNotNullParameter(resolvedShareData, "shareData");
            Intrinsics.checkNotNullParameter(list, ContactShareEditActivity.KEY_CONTACTS);
            return new SendWithoutInterstitial(resolvedShareData, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SendWithoutInterstitial)) {
                return false;
            }
            SendWithoutInterstitial sendWithoutInterstitial = (SendWithoutInterstitial) obj;
            return Intrinsics.areEqual(getShareData(), sendWithoutInterstitial.getShareData()) && Intrinsics.areEqual(getContacts(), sendWithoutInterstitial.getContacts());
        }

        public int hashCode() {
            return (getShareData().hashCode() * 31) + getContacts().hashCode();
        }

        public String toString() {
            return "SendWithoutInterstitial(shareData=" + getShareData() + ", contacts=" + getContacts() + ')';
        }

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.contacts.paged.ContactSearchKey$RecipientSearchKey> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public SendWithoutInterstitial(ResolvedShareData resolvedShareData, List<? extends ContactSearchKey.RecipientSearchKey> list) {
            super(null);
            Intrinsics.checkNotNullParameter(resolvedShareData, "shareData");
            Intrinsics.checkNotNullParameter(list, ContactShareEditActivity.KEY_CONTACTS);
            this.shareData = resolvedShareData;
            this.contacts = list;
        }

        @Override // org.thoughtcrime.securesms.sharing.v2.ShareEvent
        protected List<ContactSearchKey.RecipientSearchKey> getContacts() {
            return this.contacts;
        }

        @Override // org.thoughtcrime.securesms.sharing.v2.ShareEvent
        protected ResolvedShareData getShareData() {
            return this.shareData;
        }
    }
}
