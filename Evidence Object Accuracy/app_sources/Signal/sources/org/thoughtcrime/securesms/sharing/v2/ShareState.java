package org.thoughtcrime.securesms.sharing.v2;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: ShareState.kt */
@Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001:\u0001\u0010B\u000f\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\f\u001a\u00020\rHÖ\u0001J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/ShareState;", "", "loadState", "Lorg/thoughtcrime/securesms/sharing/v2/ShareState$ShareDataLoadState;", "(Lorg/thoughtcrime/securesms/sharing/v2/ShareState$ShareDataLoadState;)V", "getLoadState", "()Lorg/thoughtcrime/securesms/sharing/v2/ShareState$ShareDataLoadState;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "", "ShareDataLoadState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ShareState {
    private final ShareDataLoadState loadState;

    public ShareState() {
        this(null, 1, null);
    }

    public static /* synthetic */ ShareState copy$default(ShareState shareState, ShareDataLoadState shareDataLoadState, int i, Object obj) {
        if ((i & 1) != 0) {
            shareDataLoadState = shareState.loadState;
        }
        return shareState.copy(shareDataLoadState);
    }

    public final ShareDataLoadState component1() {
        return this.loadState;
    }

    public final ShareState copy(ShareDataLoadState shareDataLoadState) {
        Intrinsics.checkNotNullParameter(shareDataLoadState, "loadState");
        return new ShareState(shareDataLoadState);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof ShareState) && Intrinsics.areEqual(this.loadState, ((ShareState) obj).loadState);
    }

    public int hashCode() {
        return this.loadState.hashCode();
    }

    public String toString() {
        return "ShareState(loadState=" + this.loadState + ')';
    }

    public ShareState(ShareDataLoadState shareDataLoadState) {
        Intrinsics.checkNotNullParameter(shareDataLoadState, "loadState");
        this.loadState = shareDataLoadState;
    }

    public /* synthetic */ ShareState(ShareDataLoadState shareDataLoadState, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? ShareDataLoadState.Init.INSTANCE : shareDataLoadState);
    }

    public final ShareDataLoadState getLoadState() {
        return this.loadState;
    }

    /* compiled from: ShareState.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0003\u0006\u0007\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/ShareState$ShareDataLoadState;", "", "()V", "Failed", "Init", "Loaded", "Lorg/thoughtcrime/securesms/sharing/v2/ShareState$ShareDataLoadState$Init;", "Lorg/thoughtcrime/securesms/sharing/v2/ShareState$ShareDataLoadState$Loaded;", "Lorg/thoughtcrime/securesms/sharing/v2/ShareState$ShareDataLoadState$Failed;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class ShareDataLoadState {
        public /* synthetic */ ShareDataLoadState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* compiled from: ShareState.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/ShareState$ShareDataLoadState$Init;", "Lorg/thoughtcrime/securesms/sharing/v2/ShareState$ShareDataLoadState;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Init extends ShareDataLoadState {
            public static final Init INSTANCE = new Init();

            private Init() {
                super(null);
            }
        }

        private ShareDataLoadState() {
        }

        /* compiled from: ShareState.kt */
        @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/ShareState$ShareDataLoadState$Loaded;", "Lorg/thoughtcrime/securesms/sharing/v2/ShareState$ShareDataLoadState;", "resolvedShareData", "Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;", "(Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;)V", "getResolvedShareData", "()Lorg/thoughtcrime/securesms/sharing/v2/ResolvedShareData;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Loaded extends ShareDataLoadState {
            private final ResolvedShareData resolvedShareData;

            public static /* synthetic */ Loaded copy$default(Loaded loaded, ResolvedShareData resolvedShareData, int i, Object obj) {
                if ((i & 1) != 0) {
                    resolvedShareData = loaded.resolvedShareData;
                }
                return loaded.copy(resolvedShareData);
            }

            public final ResolvedShareData component1() {
                return this.resolvedShareData;
            }

            public final Loaded copy(ResolvedShareData resolvedShareData) {
                Intrinsics.checkNotNullParameter(resolvedShareData, "resolvedShareData");
                return new Loaded(resolvedShareData);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof Loaded) && Intrinsics.areEqual(this.resolvedShareData, ((Loaded) obj).resolvedShareData);
            }

            public int hashCode() {
                return this.resolvedShareData.hashCode();
            }

            public String toString() {
                return "Loaded(resolvedShareData=" + this.resolvedShareData + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Loaded(ResolvedShareData resolvedShareData) {
                super(null);
                Intrinsics.checkNotNullParameter(resolvedShareData, "resolvedShareData");
                this.resolvedShareData = resolvedShareData;
            }

            public final ResolvedShareData getResolvedShareData() {
                return this.resolvedShareData;
            }
        }

        /* compiled from: ShareState.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/ShareState$ShareDataLoadState$Failed;", "Lorg/thoughtcrime/securesms/sharing/v2/ShareState$ShareDataLoadState;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Failed extends ShareDataLoadState {
            public static final Failed INSTANCE = new Failed();

            private Failed() {
                super(null);
            }
        }
    }
}
