package org.thoughtcrime.securesms.sharing;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.sharing.MultiShareSender;

/* loaded from: classes4.dex */
public final class MultiShareDialogs {
    private MultiShareDialogs() {
    }

    public static void displayResultDialog(Context context, MultiShareSender.MultiShareSendResultCollection multiShareSendResultCollection, Runnable runnable) {
        if (multiShareSendResultCollection.containsFailures()) {
            displayFailuresDialog(context, runnable);
        } else {
            runnable.run();
        }
    }

    public static void displayMaxSelectedDialog(Context context, int i) {
        new AlertDialog.Builder(context).setMessage(context.getString(R.string.MultiShareDialogs__you_can_only_share_with_up_to, Integer.valueOf(i))).setPositiveButton(17039370, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.sharing.MultiShareDialogs$$ExternalSyntheticLambda2
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                MultiShareDialogs.$r8$lambda$huq7pyd4Y_O71Fz_Mol7k5RfWRY(dialogInterface, i2);
            }
        }).setCancelable(true).show();
    }

    private static void displayFailuresDialog(Context context, Runnable runnable) {
        new AlertDialog.Builder(context).setMessage(R.string.MultiShareDialogs__failed_to_send_to_some_users).setPositiveButton(17039370, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.sharing.MultiShareDialogs$$ExternalSyntheticLambda0
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                MultiShareDialogs.m2729$r8$lambda$lUhcVUprdMXe1r40pSguW13P6g(dialogInterface, i);
            }
        }).setOnDismissListener(new DialogInterface.OnDismissListener(runnable) { // from class: org.thoughtcrime.securesms.sharing.MultiShareDialogs$$ExternalSyntheticLambda1
            public final /* synthetic */ Runnable f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.content.DialogInterface.OnDismissListener
            public final void onDismiss(DialogInterface dialogInterface) {
                MultiShareDialogs.$r8$lambda$svOi8ItxzzUY1QQ4h9qWOVWEKu0(this.f$0, dialogInterface);
            }
        }).setCancelable(true).show();
    }
}
