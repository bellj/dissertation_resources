package org.thoughtcrime.securesms.sharing;

import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;

/* loaded from: classes4.dex */
public class ShareSelectionAdapter extends MappingAdapter {
    public ShareSelectionAdapter() {
        registerFactory(ShareSelectionMappingModel.class, ShareSelectionViewHolder.createFactory(R.layout.share_contact_selection_item));
    }
}
