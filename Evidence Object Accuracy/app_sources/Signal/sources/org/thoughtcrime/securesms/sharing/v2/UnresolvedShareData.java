package org.thoughtcrime.securesms.sharing.v2;

import android.net.Uri;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* compiled from: UnresolvedShareData.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0003\u0006\u0007\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/UnresolvedShareData;", "", "()V", "ExternalMultiShare", "ExternalPrimitiveShare", "ExternalSingleShare", "Lorg/thoughtcrime/securesms/sharing/v2/UnresolvedShareData$ExternalMultiShare;", "Lorg/thoughtcrime/securesms/sharing/v2/UnresolvedShareData$ExternalSingleShare;", "Lorg/thoughtcrime/securesms/sharing/v2/UnresolvedShareData$ExternalPrimitiveShare;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class UnresolvedShareData {
    public /* synthetic */ UnresolvedShareData(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    /* compiled from: UnresolvedShareData.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rHÖ\u0003J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/UnresolvedShareData$ExternalMultiShare;", "Lorg/thoughtcrime/securesms/sharing/v2/UnresolvedShareData;", "uris", "", "Landroid/net/Uri;", "(Ljava/util/List;)V", "getUris", "()Ljava/util/List;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ExternalMultiShare extends UnresolvedShareData {
        private final List<Uri> uris;

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.sharing.v2.UnresolvedShareData$ExternalMultiShare */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ ExternalMultiShare copy$default(ExternalMultiShare externalMultiShare, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                list = externalMultiShare.uris;
            }
            return externalMultiShare.copy(list);
        }

        public final List<Uri> component1() {
            return this.uris;
        }

        public final ExternalMultiShare copy(List<? extends Uri> list) {
            Intrinsics.checkNotNullParameter(list, "uris");
            return new ExternalMultiShare(list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof ExternalMultiShare) && Intrinsics.areEqual(this.uris, ((ExternalMultiShare) obj).uris);
        }

        public int hashCode() {
            return this.uris.hashCode();
        }

        public String toString() {
            return "ExternalMultiShare(uris=" + this.uris + ')';
        }

        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends android.net.Uri> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ExternalMultiShare(List<? extends Uri> list) {
            super(null);
            Intrinsics.checkNotNullParameter(list, "uris");
            this.uris = list;
        }

        public final List<Uri> getUris() {
            return this.uris;
        }
    }

    private UnresolvedShareData() {
    }

    /* compiled from: UnresolvedShareData.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\f\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u001f\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0005HÖ\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/UnresolvedShareData$ExternalSingleShare;", "Lorg/thoughtcrime/securesms/sharing/v2/UnresolvedShareData;", "uri", "Landroid/net/Uri;", "mimeType", "", "(Landroid/net/Uri;Ljava/lang/String;)V", "getMimeType", "()Ljava/lang/String;", "getUri", "()Landroid/net/Uri;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ExternalSingleShare extends UnresolvedShareData {
        private final String mimeType;
        private final Uri uri;

        public static /* synthetic */ ExternalSingleShare copy$default(ExternalSingleShare externalSingleShare, Uri uri, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                uri = externalSingleShare.uri;
            }
            if ((i & 2) != 0) {
                str = externalSingleShare.mimeType;
            }
            return externalSingleShare.copy(uri, str);
        }

        public final Uri component1() {
            return this.uri;
        }

        public final String component2() {
            return this.mimeType;
        }

        public final ExternalSingleShare copy(Uri uri, String str) {
            Intrinsics.checkNotNullParameter(uri, "uri");
            return new ExternalSingleShare(uri, str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ExternalSingleShare)) {
                return false;
            }
            ExternalSingleShare externalSingleShare = (ExternalSingleShare) obj;
            return Intrinsics.areEqual(this.uri, externalSingleShare.uri) && Intrinsics.areEqual(this.mimeType, externalSingleShare.mimeType);
        }

        public int hashCode() {
            int hashCode = this.uri.hashCode() * 31;
            String str = this.mimeType;
            return hashCode + (str == null ? 0 : str.hashCode());
        }

        public String toString() {
            return "ExternalSingleShare(uri=" + this.uri + ", mimeType=" + this.mimeType + ')';
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ExternalSingleShare(Uri uri, String str) {
            super(null);
            Intrinsics.checkNotNullParameter(uri, "uri");
            this.uri = uri;
            this.mimeType = str;
        }

        public final String getMimeType() {
            return this.mimeType;
        }

        public final Uri getUri() {
            return this.uri;
        }
    }

    /* compiled from: UnresolvedShareData.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/sharing/v2/UnresolvedShareData$ExternalPrimitiveShare;", "Lorg/thoughtcrime/securesms/sharing/v2/UnresolvedShareData;", DraftDatabase.Draft.TEXT, "", "(Ljava/lang/CharSequence;)V", "getText", "()Ljava/lang/CharSequence;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ExternalPrimitiveShare extends UnresolvedShareData {
        private final CharSequence text;

        public static /* synthetic */ ExternalPrimitiveShare copy$default(ExternalPrimitiveShare externalPrimitiveShare, CharSequence charSequence, int i, Object obj) {
            if ((i & 1) != 0) {
                charSequence = externalPrimitiveShare.text;
            }
            return externalPrimitiveShare.copy(charSequence);
        }

        public final CharSequence component1() {
            return this.text;
        }

        public final ExternalPrimitiveShare copy(CharSequence charSequence) {
            Intrinsics.checkNotNullParameter(charSequence, DraftDatabase.Draft.TEXT);
            return new ExternalPrimitiveShare(charSequence);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof ExternalPrimitiveShare) && Intrinsics.areEqual(this.text, ((ExternalPrimitiveShare) obj).text);
        }

        public int hashCode() {
            return this.text.hashCode();
        }

        public String toString() {
            return "ExternalPrimitiveShare(text=" + ((Object) this.text) + ')';
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ExternalPrimitiveShare(CharSequence charSequence) {
            super(null);
            Intrinsics.checkNotNullParameter(charSequence, DraftDatabase.Draft.TEXT);
            this.text = charSequence;
        }

        public final CharSequence getText() {
            return this.text;
        }
    }
}
