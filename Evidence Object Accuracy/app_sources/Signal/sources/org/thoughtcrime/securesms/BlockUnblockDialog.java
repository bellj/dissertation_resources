package org.thoughtcrime.securesms;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.Lifecycle;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes.dex */
public final class BlockUnblockDialog {
    private BlockUnblockDialog() {
    }

    public static void showBlockFor(Context context, Lifecycle lifecycle, Recipient recipient, Runnable runnable) {
        SimpleTask.run(lifecycle, new SimpleTask.BackgroundTask(context, recipient, runnable) { // from class: org.thoughtcrime.securesms.BlockUnblockDialog$$ExternalSyntheticLambda9
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ Recipient f$1;
            public final /* synthetic */ Runnable f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return BlockUnblockDialog.$r8$lambda$YGmOkAhOqWl4qEEo_CcU0fmI6tQ(this.f$0, this.f$1, this.f$2);
            }
        }, new BlockUnblockDialog$$ExternalSyntheticLambda7());
    }

    public static void showBlockAndReportSpamFor(Context context, Lifecycle lifecycle, Recipient recipient, Runnable runnable, Runnable runnable2) {
        SimpleTask.run(lifecycle, new SimpleTask.BackgroundTask(context, recipient, runnable, runnable2) { // from class: org.thoughtcrime.securesms.BlockUnblockDialog$$ExternalSyntheticLambda8
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ Recipient f$1;
            public final /* synthetic */ Runnable f$2;
            public final /* synthetic */ Runnable f$3;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return BlockUnblockDialog.$r8$lambda$b5cAF0Raq_uCyWjU8ocN_KBUMtk(this.f$0, this.f$1, this.f$2, this.f$3);
            }
        }, new BlockUnblockDialog$$ExternalSyntheticLambda7());
    }

    public static void showUnblockFor(Context context, Lifecycle lifecycle, Recipient recipient, Runnable runnable) {
        SimpleTask.run(lifecycle, new SimpleTask.BackgroundTask(context, recipient, runnable) { // from class: org.thoughtcrime.securesms.BlockUnblockDialog$$ExternalSyntheticLambda6
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ Recipient f$1;
            public final /* synthetic */ Runnable f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return BlockUnblockDialog.$r8$lambda$RBZXaxm3A6kiMwuM9Pi1If9wZdQ(this.f$0, this.f$1, this.f$2);
            }
        }, new BlockUnblockDialog$$ExternalSyntheticLambda7());
    }

    public static AlertDialog.Builder buildBlockFor(Context context, Recipient recipient, Runnable runnable, Runnable runnable2) {
        Recipient resolve = recipient.resolve();
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(context);
        Resources resources = context.getResources();
        if (resolve.isGroup()) {
            if (SignalDatabase.groups().isActive(resolve.requireGroupId())) {
                materialAlertDialogBuilder.setTitle((CharSequence) resources.getString(R.string.BlockUnblockDialog_block_and_leave_s, resolve.getDisplayName(context)));
                materialAlertDialogBuilder.setMessage(R.string.BlockUnblockDialog_you_will_no_longer_receive_messages_or_updates);
                materialAlertDialogBuilder.setPositiveButton(R.string.BlockUnblockDialog_block_and_leave, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(runnable) { // from class: org.thoughtcrime.securesms.BlockUnblockDialog$$ExternalSyntheticLambda0
                    public final /* synthetic */ Runnable f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        BlockUnblockDialog.m235$r8$lambda$xwblhneatjUqoouVzmtnD8wTeg(this.f$0, dialogInterface, i);
                    }
                });
                materialAlertDialogBuilder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
            } else {
                materialAlertDialogBuilder.setTitle((CharSequence) resources.getString(R.string.BlockUnblockDialog_block_s, resolve.getDisplayName(context)));
                materialAlertDialogBuilder.setMessage(R.string.BlockUnblockDialog_group_members_wont_be_able_to_add_you);
                materialAlertDialogBuilder.setPositiveButton(R.string.RecipientPreferenceActivity_block, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(runnable) { // from class: org.thoughtcrime.securesms.BlockUnblockDialog$$ExternalSyntheticLambda1
                    public final /* synthetic */ Runnable f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        BlockUnblockDialog.$r8$lambda$LV4pfofKfOzjsF5581s9HtsN11E(this.f$0, dialogInterface, i);
                    }
                });
                materialAlertDialogBuilder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
            }
        } else if (resolve.isReleaseNotes()) {
            materialAlertDialogBuilder.setTitle((CharSequence) resources.getString(R.string.BlockUnblockDialog_block_s, resolve.getDisplayName(context)));
            materialAlertDialogBuilder.setMessage(R.string.BlockUnblockDialog_block_getting_signal_updates_and_news);
            materialAlertDialogBuilder.setPositiveButton(R.string.BlockUnblockDialog_block, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(runnable) { // from class: org.thoughtcrime.securesms.BlockUnblockDialog$$ExternalSyntheticLambda2
                public final /* synthetic */ Runnable f$0;

                {
                    this.f$0 = r1;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    BlockUnblockDialog.m237$r8$lambda$OsMtxb999gFP2Ayfx7wI3jnLBo(this.f$0, dialogInterface, i);
                }
            });
            materialAlertDialogBuilder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        } else {
            materialAlertDialogBuilder.setTitle((CharSequence) resources.getString(R.string.BlockUnblockDialog_block_s, resolve.getDisplayName(context)));
            materialAlertDialogBuilder.setMessage(resolve.isRegistered() ? R.string.BlockUnblockDialog_blocked_people_wont_be_able_to_call_you_or_send_you_messages : R.string.BlockUnblockDialog_blocked_people_wont_be_able_to_send_you_messages);
            if (runnable2 != null) {
                materialAlertDialogBuilder.setNeutralButton(17039360, (DialogInterface.OnClickListener) null);
                materialAlertDialogBuilder.setNegativeButton(R.string.BlockUnblockDialog_report_spam_and_block, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(runnable2) { // from class: org.thoughtcrime.securesms.BlockUnblockDialog$$ExternalSyntheticLambda3
                    public final /* synthetic */ Runnable f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        BlockUnblockDialog.$r8$lambda$kyoYXvZLngH3aMxoI1DbAYlBztE(this.f$0, dialogInterface, i);
                    }
                });
                materialAlertDialogBuilder.setPositiveButton(R.string.BlockUnblockDialog_block, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(runnable) { // from class: org.thoughtcrime.securesms.BlockUnblockDialog$$ExternalSyntheticLambda4
                    public final /* synthetic */ Runnable f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        BlockUnblockDialog.m236$r8$lambda$JDC_yTZlPqqpPEfvc9gvV3sW2o(this.f$0, dialogInterface, i);
                    }
                });
            } else {
                materialAlertDialogBuilder.setPositiveButton(R.string.BlockUnblockDialog_block, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(runnable) { // from class: org.thoughtcrime.securesms.BlockUnblockDialog$$ExternalSyntheticLambda5
                    public final /* synthetic */ Runnable f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        BlockUnblockDialog.$r8$lambda$2h9ILGouMixIsLRF03ux3LUMgr4(this.f$0, dialogInterface, i);
                    }
                });
                materialAlertDialogBuilder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
            }
        }
        return materialAlertDialogBuilder;
    }

    public static AlertDialog.Builder buildUnblockFor(Context context, Recipient recipient, Runnable runnable) {
        Recipient resolve = recipient.resolve();
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(context);
        Resources resources = context.getResources();
        if (resolve.isGroup()) {
            if (SignalDatabase.groups().isActive(resolve.requireGroupId())) {
                materialAlertDialogBuilder.setTitle((CharSequence) resources.getString(R.string.BlockUnblockDialog_unblock_s, resolve.getDisplayName(context)));
                materialAlertDialogBuilder.setMessage(R.string.BlockUnblockDialog_group_members_will_be_able_to_add_you);
                materialAlertDialogBuilder.setPositiveButton(R.string.RecipientPreferenceActivity_unblock, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(runnable) { // from class: org.thoughtcrime.securesms.BlockUnblockDialog$$ExternalSyntheticLambda10
                    public final /* synthetic */ Runnable f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        BlockUnblockDialog.$r8$lambda$GFs_Ptq2Ssc2CpXe2n_W6FkF8yw(this.f$0, dialogInterface, i);
                    }
                });
                materialAlertDialogBuilder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
            } else {
                materialAlertDialogBuilder.setTitle((CharSequence) resources.getString(R.string.BlockUnblockDialog_unblock_s, resolve.getDisplayName(context)));
                materialAlertDialogBuilder.setMessage(R.string.BlockUnblockDialog_group_members_will_be_able_to_add_you);
                materialAlertDialogBuilder.setPositiveButton(R.string.RecipientPreferenceActivity_unblock, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(runnable) { // from class: org.thoughtcrime.securesms.BlockUnblockDialog$$ExternalSyntheticLambda11
                    public final /* synthetic */ Runnable f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        BlockUnblockDialog.$r8$lambda$2mLVSgGdqmFK6brrszenNURHKQ8(this.f$0, dialogInterface, i);
                    }
                });
                materialAlertDialogBuilder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
            }
        } else if (resolve.isReleaseNotes()) {
            materialAlertDialogBuilder.setTitle((CharSequence) resources.getString(R.string.BlockUnblockDialog_unblock_s, resolve.getDisplayName(context)));
            materialAlertDialogBuilder.setMessage(R.string.BlockUnblockDialog_resume_getting_signal_updates_and_news);
            materialAlertDialogBuilder.setPositiveButton(R.string.RecipientPreferenceActivity_unblock, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(runnable) { // from class: org.thoughtcrime.securesms.BlockUnblockDialog$$ExternalSyntheticLambda12
                public final /* synthetic */ Runnable f$0;

                {
                    this.f$0 = r1;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    BlockUnblockDialog.m238$r8$lambda$gHYti3zSx3a9px42XMFkgpuBg(this.f$0, dialogInterface, i);
                }
            });
            materialAlertDialogBuilder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        } else {
            materialAlertDialogBuilder.setTitle((CharSequence) resources.getString(R.string.BlockUnblockDialog_unblock_s, resolve.getDisplayName(context)));
            materialAlertDialogBuilder.setMessage(resolve.isRegistered() ? R.string.BlockUnblockDialog_you_will_be_able_to_call_and_message_each_other : R.string.BlockUnblockDialog_you_will_be_able_to_message_each_other);
            materialAlertDialogBuilder.setPositiveButton(R.string.RecipientPreferenceActivity_unblock, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(runnable) { // from class: org.thoughtcrime.securesms.BlockUnblockDialog$$ExternalSyntheticLambda13
                public final /* synthetic */ Runnable f$0;

                {
                    this.f$0 = r1;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    BlockUnblockDialog.m239$r8$lambda$qT6IpHqP6SlXNiu58EMhdkihbs(this.f$0, dialogInterface, i);
                }
            });
            materialAlertDialogBuilder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        }
        return materialAlertDialogBuilder;
    }
}
