package org.thoughtcrime.securesms.service;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.PendingRetryReceiptCache;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.PendingRetryReceiptModel;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.util.FeatureFlags;

/* loaded from: classes.dex */
public final class PendingRetryReceiptManager extends TimedEventManager<PendingRetryReceiptModel> {
    private static final String TAG = Log.tag(PendingRetryReceiptManager.class);
    private final MessageDatabase messageDatabase = SignalDatabase.sms();
    private final PendingRetryReceiptCache pendingCache = ApplicationDependencies.getPendingRetryReceiptCache();

    public PendingRetryReceiptManager(Application application) {
        super(application, "PendingRetryReceiptManager");
        scheduleIfNecessary();
    }

    @Override // org.thoughtcrime.securesms.service.TimedEventManager
    public PendingRetryReceiptModel getNextClosestEvent() {
        PendingRetryReceiptModel oldest = this.pendingCache.getOldest();
        if (oldest != null) {
            String str = TAG;
            Log.i(str, "Next closest expiration is in " + getDelayForEvent(oldest) + " ms for timestamp " + oldest.getSentTimestamp() + ".");
        } else {
            Log.d(TAG, "No pending receipts to schedule.");
        }
        return oldest;
    }

    public void executeEvent(PendingRetryReceiptModel pendingRetryReceiptModel) {
        String str = TAG;
        Log.w(str, "It's been " + (System.currentTimeMillis() - pendingRetryReceiptModel.getReceivedTimestamp()) + " ms since this retry receipt was received. Showing an error.");
        this.messageDatabase.insertBadDecryptMessage(pendingRetryReceiptModel.getAuthor(), pendingRetryReceiptModel.getAuthorDevice(), pendingRetryReceiptModel.getSentTimestamp(), pendingRetryReceiptModel.getReceivedTimestamp(), pendingRetryReceiptModel.getThreadId());
        this.pendingCache.delete(pendingRetryReceiptModel);
    }

    public long getDelayForEvent(PendingRetryReceiptModel pendingRetryReceiptModel) {
        return Math.max(0L, (pendingRetryReceiptModel.getReceivedTimestamp() + FeatureFlags.retryReceiptLifespan()) - System.currentTimeMillis());
    }

    @Override // org.thoughtcrime.securesms.service.TimedEventManager
    protected void scheduleAlarm(Application application, long j) {
        TimedEventManager.setAlarm(application, j, PendingRetryReceiptAlarm.class);
    }

    /* loaded from: classes4.dex */
    public static class PendingRetryReceiptAlarm extends BroadcastReceiver {
        private static final String TAG = Log.tag(PendingRetryReceiptAlarm.class);

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive()");
            ApplicationDependencies.getPendingRetryReceiptManager().scheduleIfNecessary();
        }
    }
}
