package org.thoughtcrime.securesms.service.webrtc.state;

import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.sensors.Orientation;
import org.thoughtcrime.securesms.ringrtc.CameraState;
import org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager;
import org.webrtc.PeerConnection;

/* compiled from: LocalDeviceState.kt */
@Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b+\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001Be\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\u0005\u0012\b\b\u0002\u0010\t\u001a\u00020\u0007\u0012\b\b\u0002\u0010\n\u001a\u00020\u000b\u0012\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\r\u0012\b\b\u0002\u0010\u000e\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u0010¢\u0006\u0002\u0010\u0011J\t\u0010.\u001a\u00020\u0003HÆ\u0003J\t\u0010/\u001a\u00020\u0005HÆ\u0003J\t\u00100\u001a\u00020\u0007HÆ\u0003J\t\u00101\u001a\u00020\u0005HÆ\u0003J\t\u00102\u001a\u00020\u0007HÆ\u0003J\t\u00103\u001a\u00020\u000bHÆ\u0003J\u000f\u00104\u001a\b\u0012\u0004\u0012\u00020\u000b0\rHÆ\u0003J\t\u00105\u001a\u00020\u0005HÆ\u0003J\t\u00106\u001a\u00020\u0010HÆ\u0003Ji\u00107\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\u00052\b\b\u0002\u0010\t\u001a\u00020\u00072\b\b\u0002\u0010\n\u001a\u00020\u000b2\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\r2\b\b\u0002\u0010\u000e\u001a\u00020\u00052\b\b\u0002\u0010\u000f\u001a\u00020\u0010HÆ\u0001J\u0006\u00108\u001a\u00020\u0000J\u0013\u00109\u001a\u00020\u00052\b\u0010:\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010;\u001a\u00020<HÖ\u0001J\t\u0010=\u001a\u00020>HÖ\u0001R\u001a\u0010\n\u001a\u00020\u000bX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R \u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\rX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u001a\u0010\u000e\u001a\u00020\u0005X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR\u001a\u0010\u0002\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R\u001a\u0010\t\u001a\u00020\u0007X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%R\u001a\u0010\b\u001a\u00020\u0005X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\u001b\"\u0004\b&\u0010\u001dR\u001a\u0010\u0004\u001a\u00020\u0005X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0004\u0010\u001b\"\u0004\b'\u0010\u001dR\u001a\u0010\u000f\u001a\u00020\u0010X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u001a\u0010\u0006\u001a\u00020\u0007X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b,\u0010#\"\u0004\b-\u0010%¨\u0006?"}, d2 = {"Lorg/thoughtcrime/securesms/service/webrtc/state/LocalDeviceState;", "", "cameraState", "Lorg/thoughtcrime/securesms/ringrtc/CameraState;", "isMicrophoneEnabled", "", "orientation", "Lorg/thoughtcrime/securesms/components/sensors/Orientation;", "isLandscapeEnabled", "deviceOrientation", "activeDevice", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$AudioDevice;", "availableDevices", "", "bluetoothPermissionDenied", "networkConnectionType", "Lorg/webrtc/PeerConnection$AdapterType;", "(Lorg/thoughtcrime/securesms/ringrtc/CameraState;ZLorg/thoughtcrime/securesms/components/sensors/Orientation;ZLorg/thoughtcrime/securesms/components/sensors/Orientation;Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$AudioDevice;Ljava/util/Set;ZLorg/webrtc/PeerConnection$AdapterType;)V", "getActiveDevice", "()Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$AudioDevice;", "setActiveDevice", "(Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$AudioDevice;)V", "getAvailableDevices", "()Ljava/util/Set;", "setAvailableDevices", "(Ljava/util/Set;)V", "getBluetoothPermissionDenied", "()Z", "setBluetoothPermissionDenied", "(Z)V", "getCameraState", "()Lorg/thoughtcrime/securesms/ringrtc/CameraState;", "setCameraState", "(Lorg/thoughtcrime/securesms/ringrtc/CameraState;)V", "getDeviceOrientation", "()Lorg/thoughtcrime/securesms/components/sensors/Orientation;", "setDeviceOrientation", "(Lorg/thoughtcrime/securesms/components/sensors/Orientation;)V", "setLandscapeEnabled", "setMicrophoneEnabled", "getNetworkConnectionType", "()Lorg/webrtc/PeerConnection$AdapterType;", "setNetworkConnectionType", "(Lorg/webrtc/PeerConnection$AdapterType;)V", "getOrientation", "setOrientation", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "duplicate", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class LocalDeviceState {
    private SignalAudioManager.AudioDevice activeDevice;
    private Set<? extends SignalAudioManager.AudioDevice> availableDevices;
    private boolean bluetoothPermissionDenied;
    private CameraState cameraState;
    private Orientation deviceOrientation;
    private boolean isLandscapeEnabled;
    private boolean isMicrophoneEnabled;
    private PeerConnection.AdapterType networkConnectionType;
    private Orientation orientation;

    public LocalDeviceState() {
        this(null, false, null, false, null, null, null, false, null, 511, null);
    }

    public static /* synthetic */ LocalDeviceState copy$default(LocalDeviceState localDeviceState, CameraState cameraState, boolean z, Orientation orientation, boolean z2, Orientation orientation2, SignalAudioManager.AudioDevice audioDevice, Set set, boolean z3, PeerConnection.AdapterType adapterType, int i, Object obj) {
        return localDeviceState.copy((i & 1) != 0 ? localDeviceState.cameraState : cameraState, (i & 2) != 0 ? localDeviceState.isMicrophoneEnabled : z, (i & 4) != 0 ? localDeviceState.orientation : orientation, (i & 8) != 0 ? localDeviceState.isLandscapeEnabled : z2, (i & 16) != 0 ? localDeviceState.deviceOrientation : orientation2, (i & 32) != 0 ? localDeviceState.activeDevice : audioDevice, (i & 64) != 0 ? localDeviceState.availableDevices : set, (i & 128) != 0 ? localDeviceState.bluetoothPermissionDenied : z3, (i & 256) != 0 ? localDeviceState.networkConnectionType : adapterType);
    }

    public final CameraState component1() {
        return this.cameraState;
    }

    public final boolean component2() {
        return this.isMicrophoneEnabled;
    }

    public final Orientation component3() {
        return this.orientation;
    }

    public final boolean component4() {
        return this.isLandscapeEnabled;
    }

    public final Orientation component5() {
        return this.deviceOrientation;
    }

    public final SignalAudioManager.AudioDevice component6() {
        return this.activeDevice;
    }

    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.util.Set<? extends org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager$AudioDevice>, java.util.Set<org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager$AudioDevice> */
    public final Set<SignalAudioManager.AudioDevice> component7() {
        return this.availableDevices;
    }

    public final boolean component8() {
        return this.bluetoothPermissionDenied;
    }

    public final PeerConnection.AdapterType component9() {
        return this.networkConnectionType;
    }

    public final LocalDeviceState copy(CameraState cameraState, boolean z, Orientation orientation, boolean z2, Orientation orientation2, SignalAudioManager.AudioDevice audioDevice, Set<? extends SignalAudioManager.AudioDevice> set, boolean z3, PeerConnection.AdapterType adapterType) {
        Intrinsics.checkNotNullParameter(cameraState, "cameraState");
        Intrinsics.checkNotNullParameter(orientation, "orientation");
        Intrinsics.checkNotNullParameter(orientation2, "deviceOrientation");
        Intrinsics.checkNotNullParameter(audioDevice, "activeDevice");
        Intrinsics.checkNotNullParameter(set, "availableDevices");
        Intrinsics.checkNotNullParameter(adapterType, "networkConnectionType");
        return new LocalDeviceState(cameraState, z, orientation, z2, orientation2, audioDevice, set, z3, adapterType);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LocalDeviceState)) {
            return false;
        }
        LocalDeviceState localDeviceState = (LocalDeviceState) obj;
        return Intrinsics.areEqual(this.cameraState, localDeviceState.cameraState) && this.isMicrophoneEnabled == localDeviceState.isMicrophoneEnabled && this.orientation == localDeviceState.orientation && this.isLandscapeEnabled == localDeviceState.isLandscapeEnabled && this.deviceOrientation == localDeviceState.deviceOrientation && this.activeDevice == localDeviceState.activeDevice && Intrinsics.areEqual(this.availableDevices, localDeviceState.availableDevices) && this.bluetoothPermissionDenied == localDeviceState.bluetoothPermissionDenied && this.networkConnectionType == localDeviceState.networkConnectionType;
    }

    public int hashCode() {
        int hashCode = this.cameraState.hashCode() * 31;
        boolean z = this.isMicrophoneEnabled;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int hashCode2 = (((hashCode + i2) * 31) + this.orientation.hashCode()) * 31;
        boolean z2 = this.isLandscapeEnabled;
        if (z2) {
            z2 = true;
        }
        int i5 = z2 ? 1 : 0;
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int hashCode3 = (((((((hashCode2 + i5) * 31) + this.deviceOrientation.hashCode()) * 31) + this.activeDevice.hashCode()) * 31) + this.availableDevices.hashCode()) * 31;
        boolean z3 = this.bluetoothPermissionDenied;
        if (!z3) {
            i = z3 ? 1 : 0;
        }
        return ((hashCode3 + i) * 31) + this.networkConnectionType.hashCode();
    }

    public String toString() {
        return "LocalDeviceState(cameraState=" + this.cameraState + ", isMicrophoneEnabled=" + this.isMicrophoneEnabled + ", orientation=" + this.orientation + ", isLandscapeEnabled=" + this.isLandscapeEnabled + ", deviceOrientation=" + this.deviceOrientation + ", activeDevice=" + this.activeDevice + ", availableDevices=" + this.availableDevices + ", bluetoothPermissionDenied=" + this.bluetoothPermissionDenied + ", networkConnectionType=" + this.networkConnectionType + ')';
    }

    public LocalDeviceState(CameraState cameraState, boolean z, Orientation orientation, boolean z2, Orientation orientation2, SignalAudioManager.AudioDevice audioDevice, Set<? extends SignalAudioManager.AudioDevice> set, boolean z3, PeerConnection.AdapterType adapterType) {
        Intrinsics.checkNotNullParameter(cameraState, "cameraState");
        Intrinsics.checkNotNullParameter(orientation, "orientation");
        Intrinsics.checkNotNullParameter(orientation2, "deviceOrientation");
        Intrinsics.checkNotNullParameter(audioDevice, "activeDevice");
        Intrinsics.checkNotNullParameter(set, "availableDevices");
        Intrinsics.checkNotNullParameter(adapterType, "networkConnectionType");
        this.cameraState = cameraState;
        this.isMicrophoneEnabled = z;
        this.orientation = orientation;
        this.isLandscapeEnabled = z2;
        this.deviceOrientation = orientation2;
        this.activeDevice = audioDevice;
        this.availableDevices = set;
        this.bluetoothPermissionDenied = z3;
        this.networkConnectionType = adapterType;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ LocalDeviceState(org.thoughtcrime.securesms.ringrtc.CameraState r11, boolean r12, org.thoughtcrime.securesms.components.sensors.Orientation r13, boolean r14, org.thoughtcrime.securesms.components.sensors.Orientation r15, org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager.AudioDevice r16, java.util.Set r17, boolean r18, org.webrtc.PeerConnection.AdapterType r19, int r20, kotlin.jvm.internal.DefaultConstructorMarker r21) {
        /*
            r10 = this;
            r0 = r20
            r1 = r0 & 1
            if (r1 == 0) goto L_0x000e
            org.thoughtcrime.securesms.ringrtc.CameraState r1 = org.thoughtcrime.securesms.ringrtc.CameraState.UNKNOWN
            java.lang.String r2 = "UNKNOWN"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r1, r2)
            goto L_0x000f
        L_0x000e:
            r1 = r11
        L_0x000f:
            r2 = r0 & 2
            if (r2 == 0) goto L_0x0015
            r2 = 1
            goto L_0x0016
        L_0x0015:
            r2 = r12
        L_0x0016:
            r3 = r0 & 4
            if (r3 == 0) goto L_0x001d
            org.thoughtcrime.securesms.components.sensors.Orientation r3 = org.thoughtcrime.securesms.components.sensors.Orientation.PORTRAIT_BOTTOM_EDGE
            goto L_0x001e
        L_0x001d:
            r3 = r13
        L_0x001e:
            r4 = r0 & 8
            r5 = 0
            if (r4 == 0) goto L_0x0025
            r4 = 0
            goto L_0x0026
        L_0x0025:
            r4 = r14
        L_0x0026:
            r6 = r0 & 16
            if (r6 == 0) goto L_0x002d
            org.thoughtcrime.securesms.components.sensors.Orientation r6 = org.thoughtcrime.securesms.components.sensors.Orientation.PORTRAIT_BOTTOM_EDGE
            goto L_0x002e
        L_0x002d:
            r6 = r15
        L_0x002e:
            r7 = r0 & 32
            if (r7 == 0) goto L_0x0035
            org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager$AudioDevice r7 = org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager.AudioDevice.NONE
            goto L_0x0037
        L_0x0035:
            r7 = r16
        L_0x0037:
            r8 = r0 & 64
            if (r8 == 0) goto L_0x0040
            java.util.Set r8 = kotlin.collections.SetsKt.emptySet()
            goto L_0x0042
        L_0x0040:
            r8 = r17
        L_0x0042:
            r9 = r0 & 128(0x80, float:1.794E-43)
            if (r9 == 0) goto L_0x0047
            goto L_0x0049
        L_0x0047:
            r5 = r18
        L_0x0049:
            r0 = r0 & 256(0x100, float:3.59E-43)
            if (r0 == 0) goto L_0x0050
            org.webrtc.PeerConnection$AdapterType r0 = org.webrtc.PeerConnection.AdapterType.UNKNOWN
            goto L_0x0052
        L_0x0050:
            r0 = r19
        L_0x0052:
            r11 = r10
            r12 = r1
            r13 = r2
            r14 = r3
            r15 = r4
            r16 = r6
            r17 = r7
            r18 = r8
            r19 = r5
            r20 = r0
            r11.<init>(r12, r13, r14, r15, r16, r17, r18, r19, r20)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.service.webrtc.state.LocalDeviceState.<init>(org.thoughtcrime.securesms.ringrtc.CameraState, boolean, org.thoughtcrime.securesms.components.sensors.Orientation, boolean, org.thoughtcrime.securesms.components.sensors.Orientation, org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager$AudioDevice, java.util.Set, boolean, org.webrtc.PeerConnection$AdapterType, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final CameraState getCameraState() {
        return this.cameraState;
    }

    public final void setCameraState(CameraState cameraState) {
        Intrinsics.checkNotNullParameter(cameraState, "<set-?>");
        this.cameraState = cameraState;
    }

    public final boolean isMicrophoneEnabled() {
        return this.isMicrophoneEnabled;
    }

    public final void setMicrophoneEnabled(boolean z) {
        this.isMicrophoneEnabled = z;
    }

    public final Orientation getOrientation() {
        return this.orientation;
    }

    public final void setOrientation(Orientation orientation) {
        Intrinsics.checkNotNullParameter(orientation, "<set-?>");
        this.orientation = orientation;
    }

    public final boolean isLandscapeEnabled() {
        return this.isLandscapeEnabled;
    }

    public final void setLandscapeEnabled(boolean z) {
        this.isLandscapeEnabled = z;
    }

    public final Orientation getDeviceOrientation() {
        return this.deviceOrientation;
    }

    public final void setDeviceOrientation(Orientation orientation) {
        Intrinsics.checkNotNullParameter(orientation, "<set-?>");
        this.deviceOrientation = orientation;
    }

    public final SignalAudioManager.AudioDevice getActiveDevice() {
        return this.activeDevice;
    }

    public final void setActiveDevice(SignalAudioManager.AudioDevice audioDevice) {
        Intrinsics.checkNotNullParameter(audioDevice, "<set-?>");
        this.activeDevice = audioDevice;
    }

    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.util.Set<? extends org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager$AudioDevice>, java.util.Set<org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager$AudioDevice> */
    public final Set<SignalAudioManager.AudioDevice> getAvailableDevices() {
        return this.availableDevices;
    }

    public final void setAvailableDevices(Set<? extends SignalAudioManager.AudioDevice> set) {
        Intrinsics.checkNotNullParameter(set, "<set-?>");
        this.availableDevices = set;
    }

    public final boolean getBluetoothPermissionDenied() {
        return this.bluetoothPermissionDenied;
    }

    public final void setBluetoothPermissionDenied(boolean z) {
        this.bluetoothPermissionDenied = z;
    }

    public final PeerConnection.AdapterType getNetworkConnectionType() {
        return this.networkConnectionType;
    }

    public final void setNetworkConnectionType(PeerConnection.AdapterType adapterType) {
        Intrinsics.checkNotNullParameter(adapterType, "<set-?>");
        this.networkConnectionType = adapterType;
    }

    public final LocalDeviceState duplicate() {
        return copy$default(this, null, false, null, false, null, null, null, false, null, 511, null);
    }
}
