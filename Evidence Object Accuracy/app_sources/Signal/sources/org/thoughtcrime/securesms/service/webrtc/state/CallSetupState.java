package org.thoughtcrime.securesms.service.webrtc.state;

import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.webrtc.PeerConnection;

/* compiled from: CallSetupState.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b-\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u0000 A2\u00020\u0001:\u0001ABy\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0003\u0012\b\b\u0002\u0010\b\u001a\u00020\t\u0012\b\b\u0002\u0010\n\u001a\u00020\u000b\u0012\b\b\u0002\u0010\f\u001a\u00020\u0003\u0012\b\b\u0002\u0010\r\u001a\u00020\u0003\u0012\u000e\b\u0002\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f\u0012\b\b\u0002\u0010\u0011\u001a\u00020\u0003¢\u0006\u0002\u0010\u0012J\t\u0010.\u001a\u00020\u0003HÆ\u0003J\u000f\u0010/\u001a\b\u0012\u0004\u0012\u00020\u00100\u000fHÆ\u0003J\t\u00100\u001a\u00020\u0003HÆ\u0003J\t\u00101\u001a\u00020\u0003HÆ\u0003J\t\u00102\u001a\u00020\u0003HÆ\u0003J\t\u00103\u001a\u00020\u0003HÆ\u0003J\t\u00104\u001a\u00020\u0003HÆ\u0003J\t\u00105\u001a\u00020\tHÆ\u0003J\t\u00106\u001a\u00020\u000bHÆ\u0003J\t\u00107\u001a\u00020\u0003HÆ\u0003J\t\u00108\u001a\u00020\u0003HÆ\u0003J}\u00109\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\u00032\b\b\u0002\u0010\r\u001a\u00020\u00032\u000e\b\u0002\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f2\b\b\u0002\u0010\u0011\u001a\u00020\u0003HÆ\u0001J\u0006\u0010:\u001a\u00020\u0000J\u0013\u0010;\u001a\u00020\u00032\b\u0010<\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010=\u001a\u00020>HÖ\u0001J\t\u0010?\u001a\u00020@HÖ\u0001R\u001c\u0010\u0011\u001a\u00020\u00038\u0007X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R \u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000fX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u001a\u0010\u0005\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0014\"\u0004\b\u001b\u0010\u0016R\u001a\u0010\u0002\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0002\u0010\u0014\"\u0004\b\u001c\u0010\u0016R\u001a\u0010\u0004\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0004\u0010\u0014\"\u0004\b\u001d\u0010\u0016R\u001c\u0010\u0007\u001a\u00020\u00038\u0007X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u0014\"\u0004\b\u001f\u0010\u0016R\u001a\u0010\b\u001a\u00020\tX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b \u0010!\"\u0004\b\"\u0010#R\u001a\u0010\n\u001a\u00020\u000bX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b$\u0010%\"\u0004\b&\u0010'R\u001c\u0010\u0006\u001a\u00020\u00038\u0007X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b(\u0010\u0014\"\u0004\b)\u0010\u0016R\u001c\u0010\r\u001a\u00020\u00038\u0007X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b*\u0010\u0014\"\u0004\b+\u0010\u0016R\u001c\u0010\f\u001a\u00020\u00038\u0007X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b,\u0010\u0014\"\u0004\b-\u0010\u0016¨\u0006B"}, d2 = {"Lorg/thoughtcrime/securesms/service/webrtc/state/CallSetupState;", "", "isEnableVideoOnCreate", "", "isRemoteVideoOffer", "isAcceptWithVideo", "sentJoinedMessage", "ringGroup", "ringId", "", "ringerRecipient", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "waitForTelecom", "telecomApproved", "iceServers", "", "Lorg/webrtc/PeerConnection$IceServer;", "alwaysTurnServers", "(ZZZZZJLorg/thoughtcrime/securesms/recipients/Recipient;ZZLjava/util/List;Z)V", "isAlwaysTurnServers", "()Z", "setAlwaysTurnServers", "(Z)V", "getIceServers", "()Ljava/util/List;", "setIceServers", "(Ljava/util/List;)V", "setAcceptWithVideo", "setEnableVideoOnCreate", "setRemoteVideoOffer", "shouldRingGroup", "setRingGroup", "getRingId", "()J", "setRingId", "(J)V", "getRingerRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "setRingerRecipient", "(Lorg/thoughtcrime/securesms/recipients/Recipient;)V", "hasSentJoinedMessage", "setSentJoinedMessage", "isTelecomApproved", "setTelecomApproved", "shouldWaitForTelecomApproval", "setWaitForTelecom", "component1", "component10", "component11", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "duplicate", "equals", "other", "hashCode", "", "toString", "", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CallSetupState {
    public static final Companion Companion = new Companion(null);
    public static final long NO_RING;
    private boolean alwaysTurnServers;
    private List<PeerConnection.IceServer> iceServers;
    private boolean isAcceptWithVideo;
    private boolean isEnableVideoOnCreate;
    private boolean isRemoteVideoOffer;
    private boolean ringGroup;
    private long ringId;
    private Recipient ringerRecipient;
    private boolean sentJoinedMessage;
    private boolean telecomApproved;
    private boolean waitForTelecom;

    public CallSetupState() {
        this(false, false, false, false, false, 0, null, false, false, null, false, 2047, null);
    }

    public static /* synthetic */ CallSetupState copy$default(CallSetupState callSetupState, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, long j, Recipient recipient, boolean z6, boolean z7, List list, boolean z8, int i, Object obj) {
        return callSetupState.copy((i & 1) != 0 ? callSetupState.isEnableVideoOnCreate : z, (i & 2) != 0 ? callSetupState.isRemoteVideoOffer : z2, (i & 4) != 0 ? callSetupState.isAcceptWithVideo : z3, (i & 8) != 0 ? callSetupState.sentJoinedMessage : z4, (i & 16) != 0 ? callSetupState.ringGroup : z5, (i & 32) != 0 ? callSetupState.ringId : j, (i & 64) != 0 ? callSetupState.ringerRecipient : recipient, (i & 128) != 0 ? callSetupState.waitForTelecom : z6, (i & 256) != 0 ? callSetupState.telecomApproved : z7, (i & 512) != 0 ? callSetupState.iceServers : list, (i & 1024) != 0 ? callSetupState.alwaysTurnServers : z8);
    }

    public final boolean component1() {
        return this.isEnableVideoOnCreate;
    }

    public final List<PeerConnection.IceServer> component10() {
        return this.iceServers;
    }

    public final boolean component11() {
        return this.alwaysTurnServers;
    }

    public final boolean component2() {
        return this.isRemoteVideoOffer;
    }

    public final boolean component3() {
        return this.isAcceptWithVideo;
    }

    public final boolean component4() {
        return this.sentJoinedMessage;
    }

    public final boolean component5() {
        return this.ringGroup;
    }

    public final long component6() {
        return this.ringId;
    }

    public final Recipient component7() {
        return this.ringerRecipient;
    }

    public final boolean component8() {
        return this.waitForTelecom;
    }

    public final boolean component9() {
        return this.telecomApproved;
    }

    public final CallSetupState copy(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, long j, Recipient recipient, boolean z6, boolean z7, List<PeerConnection.IceServer> list, boolean z8) {
        Intrinsics.checkNotNullParameter(recipient, "ringerRecipient");
        Intrinsics.checkNotNullParameter(list, "iceServers");
        return new CallSetupState(z, z2, z3, z4, z5, j, recipient, z6, z7, list, z8);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CallSetupState)) {
            return false;
        }
        CallSetupState callSetupState = (CallSetupState) obj;
        return this.isEnableVideoOnCreate == callSetupState.isEnableVideoOnCreate && this.isRemoteVideoOffer == callSetupState.isRemoteVideoOffer && this.isAcceptWithVideo == callSetupState.isAcceptWithVideo && this.sentJoinedMessage == callSetupState.sentJoinedMessage && this.ringGroup == callSetupState.ringGroup && this.ringId == callSetupState.ringId && Intrinsics.areEqual(this.ringerRecipient, callSetupState.ringerRecipient) && this.waitForTelecom == callSetupState.waitForTelecom && this.telecomApproved == callSetupState.telecomApproved && Intrinsics.areEqual(this.iceServers, callSetupState.iceServers) && this.alwaysTurnServers == callSetupState.alwaysTurnServers;
    }

    public int hashCode() {
        boolean z = this.isEnableVideoOnCreate;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = i2 * 31;
        boolean z2 = this.isRemoteVideoOffer;
        if (z2) {
            z2 = true;
        }
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int i9 = (i5 + i6) * 31;
        boolean z3 = this.isAcceptWithVideo;
        if (z3) {
            z3 = true;
        }
        int i10 = z3 ? 1 : 0;
        int i11 = z3 ? 1 : 0;
        int i12 = z3 ? 1 : 0;
        int i13 = (i9 + i10) * 31;
        boolean z4 = this.sentJoinedMessage;
        if (z4) {
            z4 = true;
        }
        int i14 = z4 ? 1 : 0;
        int i15 = z4 ? 1 : 0;
        int i16 = z4 ? 1 : 0;
        int i17 = (i13 + i14) * 31;
        boolean z5 = this.ringGroup;
        if (z5) {
            z5 = true;
        }
        int i18 = z5 ? 1 : 0;
        int i19 = z5 ? 1 : 0;
        int i20 = z5 ? 1 : 0;
        int m = (((((i17 + i18) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.ringId)) * 31) + this.ringerRecipient.hashCode()) * 31;
        boolean z6 = this.waitForTelecom;
        if (z6) {
            z6 = true;
        }
        int i21 = z6 ? 1 : 0;
        int i22 = z6 ? 1 : 0;
        int i23 = z6 ? 1 : 0;
        int i24 = (m + i21) * 31;
        boolean z7 = this.telecomApproved;
        if (z7) {
            z7 = true;
        }
        int i25 = z7 ? 1 : 0;
        int i26 = z7 ? 1 : 0;
        int i27 = z7 ? 1 : 0;
        int hashCode = (((i24 + i25) * 31) + this.iceServers.hashCode()) * 31;
        boolean z8 = this.alwaysTurnServers;
        if (!z8) {
            i = z8 ? 1 : 0;
        }
        return hashCode + i;
    }

    public String toString() {
        return "CallSetupState(isEnableVideoOnCreate=" + this.isEnableVideoOnCreate + ", isRemoteVideoOffer=" + this.isRemoteVideoOffer + ", isAcceptWithVideo=" + this.isAcceptWithVideo + ", sentJoinedMessage=" + this.sentJoinedMessage + ", ringGroup=" + this.ringGroup + ", ringId=" + this.ringId + ", ringerRecipient=" + this.ringerRecipient + ", waitForTelecom=" + this.waitForTelecom + ", telecomApproved=" + this.telecomApproved + ", iceServers=" + this.iceServers + ", alwaysTurnServers=" + this.alwaysTurnServers + ')';
    }

    public CallSetupState(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, long j, Recipient recipient, boolean z6, boolean z7, List<PeerConnection.IceServer> list, boolean z8) {
        Intrinsics.checkNotNullParameter(recipient, "ringerRecipient");
        Intrinsics.checkNotNullParameter(list, "iceServers");
        this.isEnableVideoOnCreate = z;
        this.isRemoteVideoOffer = z2;
        this.isAcceptWithVideo = z3;
        this.sentJoinedMessage = z4;
        this.ringGroup = z5;
        this.ringId = j;
        this.ringerRecipient = recipient;
        this.waitForTelecom = z6;
        this.telecomApproved = z7;
        this.iceServers = list;
        this.alwaysTurnServers = z8;
    }

    public final boolean isEnableVideoOnCreate() {
        return this.isEnableVideoOnCreate;
    }

    public final void setEnableVideoOnCreate(boolean z) {
        this.isEnableVideoOnCreate = z;
    }

    public final boolean isRemoteVideoOffer() {
        return this.isRemoteVideoOffer;
    }

    public final void setRemoteVideoOffer(boolean z) {
        this.isRemoteVideoOffer = z;
    }

    public final boolean isAcceptWithVideo() {
        return this.isAcceptWithVideo;
    }

    public final void setAcceptWithVideo(boolean z) {
        this.isAcceptWithVideo = z;
    }

    public final boolean hasSentJoinedMessage() {
        return this.sentJoinedMessage;
    }

    public final void setSentJoinedMessage(boolean z) {
        this.sentJoinedMessage = z;
    }

    public final void setRingGroup(boolean z) {
        this.ringGroup = z;
    }

    public final boolean shouldRingGroup() {
        return this.ringGroup;
    }

    public final long getRingId() {
        return this.ringId;
    }

    public final void setRingId(long j) {
        this.ringId = j;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ CallSetupState(boolean r14, boolean r15, boolean r16, boolean r17, boolean r18, long r19, org.thoughtcrime.securesms.recipients.Recipient r21, boolean r22, boolean r23, java.util.List r24, boolean r25, int r26, kotlin.jvm.internal.DefaultConstructorMarker r27) {
        /*
            r13 = this;
            r0 = r26
            r1 = r0 & 1
            r2 = 0
            if (r1 == 0) goto L_0x0009
            r1 = 0
            goto L_0x000a
        L_0x0009:
            r1 = r14
        L_0x000a:
            r3 = r0 & 2
            if (r3 == 0) goto L_0x0010
            r3 = 0
            goto L_0x0011
        L_0x0010:
            r3 = r15
        L_0x0011:
            r4 = r0 & 4
            if (r4 == 0) goto L_0x0017
            r4 = 0
            goto L_0x0019
        L_0x0017:
            r4 = r16
        L_0x0019:
            r5 = r0 & 8
            if (r5 == 0) goto L_0x001f
            r5 = 0
            goto L_0x0021
        L_0x001f:
            r5 = r17
        L_0x0021:
            r6 = r0 & 16
            if (r6 == 0) goto L_0x0027
            r6 = 1
            goto L_0x0029
        L_0x0027:
            r6 = r18
        L_0x0029:
            r7 = r0 & 32
            if (r7 == 0) goto L_0x0030
            r7 = 0
            goto L_0x0032
        L_0x0030:
            r7 = r19
        L_0x0032:
            r9 = r0 & 64
            if (r9 == 0) goto L_0x003e
            org.thoughtcrime.securesms.recipients.Recipient r9 = org.thoughtcrime.securesms.recipients.Recipient.UNKNOWN
            java.lang.String r10 = "UNKNOWN"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r9, r10)
            goto L_0x0040
        L_0x003e:
            r9 = r21
        L_0x0040:
            r10 = r0 & 128(0x80, float:1.794E-43)
            if (r10 == 0) goto L_0x0046
            r10 = 0
            goto L_0x0048
        L_0x0046:
            r10 = r22
        L_0x0048:
            r11 = r0 & 256(0x100, float:3.59E-43)
            if (r11 == 0) goto L_0x004e
            r11 = 0
            goto L_0x0050
        L_0x004e:
            r11 = r23
        L_0x0050:
            r12 = r0 & 512(0x200, float:7.175E-43)
            if (r12 == 0) goto L_0x005a
            java.util.ArrayList r12 = new java.util.ArrayList
            r12.<init>()
            goto L_0x005c
        L_0x005a:
            r12 = r24
        L_0x005c:
            r0 = r0 & 1024(0x400, float:1.435E-42)
            if (r0 == 0) goto L_0x0061
            goto L_0x0063
        L_0x0061:
            r2 = r25
        L_0x0063:
            r14 = r13
            r15 = r1
            r16 = r3
            r17 = r4
            r18 = r5
            r19 = r6
            r20 = r7
            r22 = r9
            r23 = r10
            r24 = r11
            r25 = r12
            r26 = r2
            r14.<init>(r15, r16, r17, r18, r19, r20, r22, r23, r24, r25, r26)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.service.webrtc.state.CallSetupState.<init>(boolean, boolean, boolean, boolean, boolean, long, org.thoughtcrime.securesms.recipients.Recipient, boolean, boolean, java.util.List, boolean, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final Recipient getRingerRecipient() {
        return this.ringerRecipient;
    }

    public final void setRingerRecipient(Recipient recipient) {
        Intrinsics.checkNotNullParameter(recipient, "<set-?>");
        this.ringerRecipient = recipient;
    }

    public final void setWaitForTelecom(boolean z) {
        this.waitForTelecom = z;
    }

    public final boolean shouldWaitForTelecomApproval() {
        return this.waitForTelecom;
    }

    public final boolean isTelecomApproved() {
        return this.telecomApproved;
    }

    public final void setTelecomApproved(boolean z) {
        this.telecomApproved = z;
    }

    public final List<PeerConnection.IceServer> getIceServers() {
        return this.iceServers;
    }

    public final void setIceServers(List<PeerConnection.IceServer> list) {
        Intrinsics.checkNotNullParameter(list, "<set-?>");
        this.iceServers = list;
    }

    public final boolean isAlwaysTurnServers() {
        return this.alwaysTurnServers;
    }

    public final void setAlwaysTurnServers(boolean z) {
        this.alwaysTurnServers = z;
    }

    public final CallSetupState duplicate() {
        return copy$default(this, false, false, false, false, false, 0, null, false, false, null, false, 2047, null);
    }

    /* compiled from: CallSetupState.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/service/webrtc/state/CallSetupState$Companion;", "", "()V", "NO_RING", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
