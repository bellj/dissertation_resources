package org.thoughtcrime.securesms.service.webrtc;

import android.app.Application;
import android.content.ComponentName;
import android.os.Build;
import android.os.Bundle;
import android.os.Process;
import android.telecom.DisconnectCause;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import androidx.core.content.ContextCompat;
import androidx.core.os.BundleKt;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager;

/* compiled from: AndroidTelecomUtil.kt */
@Metadata(d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\bÇ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\nH\u0007J \u0010\u0019\u001a\u00020\u00072\u0006\u0010\u0018\u001a\u00020\n2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u0007H\u0007J \u0010\u001d\u001a\u00020\u00072\u0006\u0010\u0018\u001a\u00020\n2\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\u0007H\u0007J\b\u0010\u001f\u001a\u00020 H\u0007J\u000e\u0010!\u001a\u00020\"2\u0006\u0010\u0018\u001a\u00020\nJ\b\u0010#\u001a\u00020\u0007H\u0002J\b\u0010$\u001a\u00020\u0017H\u0007J\u0010\u0010%\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\nH\u0007J\u0016\u0010&\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\n2\u0006\u0010'\u001a\u00020\"J\u0010\u0010(\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\nH\u0007R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u000e¢\u0006\u0002\n\u0000R(\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t8\u0006X\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b\f\u0010\u0002\u001a\u0004\b\r\u0010\u000eR\u000e\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0007X\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\u0012\u001a\u00020\u00078FX\u0004¢\u0006\f\u0012\u0004\b\u0013\u0010\u0002\u001a\u0004\b\u0014\u0010\u0015¨\u0006)"}, d2 = {"Lorg/thoughtcrime/securesms/service/webrtc/AndroidTelecomUtil;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "accountRegistered", "", "connections", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Lorg/thoughtcrime/securesms/service/webrtc/AndroidCallConnection;", "getConnections$annotations", "getConnections", "()Ljava/util/Map;", "context", "Landroid/app/Application;", "systemRejected", "telecomSupported", "getTelecomSupported$annotations", "getTelecomSupported", "()Z", "activateCall", "", "recipientId", "addIncomingCall", "callId", "", "remoteVideoOffer", "addOutgoingCall", "isVideoCall", "getPhoneAccountHandle", "Landroid/telecom/PhoneAccountHandle;", "getSelectedAudioDevice", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$AudioDevice;", "isTelecomAllowedForDevice", "registerPhoneAccount", "reject", "selectAudioDevice", "device", "terminateCall", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class AndroidTelecomUtil {
    public static final AndroidTelecomUtil INSTANCE = new AndroidTelecomUtil();
    private static final String TAG = Log.tag(AndroidTelecomUtil.class);
    private static boolean accountRegistered;
    private static final Map<RecipientId, AndroidCallConnection> connections = new LinkedHashMap();
    private static final Application context;
    private static boolean systemRejected;

    /* compiled from: AndroidTelecomUtil.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[SignalAudioManager.AudioDevice.values().length];
            iArr[SignalAudioManager.AudioDevice.SPEAKER_PHONE.ordinal()] = 1;
            iArr[SignalAudioManager.AudioDevice.BLUETOOTH.ordinal()] = 2;
            iArr[SignalAudioManager.AudioDevice.WIRED_HEADSET.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    @JvmStatic
    public static /* synthetic */ void getConnections$annotations() {
    }

    @JvmStatic
    public static /* synthetic */ void getTelecomSupported$annotations() {
    }

    private AndroidTelecomUtil() {
    }

    static {
        INSTANCE = new AndroidTelecomUtil();
        TAG = Log.tag(AndroidTelecomUtil.class);
        Application application = ApplicationDependencies.getApplication();
        Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
        context = application;
        connections = new LinkedHashMap();
    }

    public static final boolean getTelecomSupported() {
        if (Build.VERSION.SDK_INT < 26 || systemRejected || !INSTANCE.isTelecomAllowedForDevice()) {
            return false;
        }
        if (!accountRegistered) {
            registerPhoneAccount();
        }
        if (!accountRegistered) {
            return false;
        }
        Object systemService = ContextCompat.getSystemService(context, TelecomManager.class);
        Intrinsics.checkNotNull(systemService);
        PhoneAccount phoneAccount = ((TelecomManager) systemService).getPhoneAccount(getPhoneAccountHandle());
        return phoneAccount != null && phoneAccount.isEnabled();
    }

    public static final Map<RecipientId, AndroidCallConnection> getConnections() {
        return connections;
    }

    @JvmStatic
    public static final void registerPhoneAccount() {
        if (Build.VERSION.SDK_INT >= 26 && !systemRejected) {
            String str = TAG;
            Log.i(str, "Registering phone account");
            PhoneAccount build = new PhoneAccount.Builder(getPhoneAccountHandle(), "Signal").setCapabilities(2056).build();
            try {
                Object systemService = ContextCompat.getSystemService(context, TelecomManager.class);
                Intrinsics.checkNotNull(systemService);
                ((TelecomManager) systemService).registerPhoneAccount(build);
                Log.i(str, "Phone account registered successfully");
                accountRegistered = true;
            } catch (Exception e) {
                Log.w(TAG, "Unable to register telecom account", e);
                systemRejected = true;
            }
        }
    }

    @JvmStatic
    public static final PhoneAccountHandle getPhoneAccountHandle() {
        Application application = context;
        return new PhoneAccountHandle(new ComponentName(application, AndroidCallConnectionService.class), application.getPackageName(), Process.myUserHandle());
    }

    @JvmStatic
    public static final boolean addIncomingCall(RecipientId recipientId, long j, boolean z) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        if (getTelecomSupported()) {
            Pair[] pairArr = new Pair[2];
            Pair[] pairArr2 = new Pair[4];
            pairArr2[0] = TuplesKt.to(AndroidCallConnectionService.KEY_RECIPIENT_ID, recipientId.serialize());
            pairArr2[1] = TuplesKt.to(AndroidCallConnectionService.KEY_CALL_ID, Long.valueOf(j));
            pairArr2[2] = TuplesKt.to(AndroidCallConnectionService.KEY_VIDEO_CALL, Boolean.valueOf(z));
            int i = 3;
            pairArr2[3] = TuplesKt.to("android.telecom.extra.INCOMING_VIDEO_STATE", Integer.valueOf(z ? 3 : 0));
            pairArr[0] = TuplesKt.to("android.telecom.extra.INCOMING_CALL_EXTRAS", BundleKt.bundleOf(pairArr2));
            if (!z) {
                i = 0;
            }
            pairArr[1] = TuplesKt.to("android.telecom.extra.INCOMING_VIDEO_STATE", Integer.valueOf(i));
            Bundle bundleOf = BundleKt.bundleOf(pairArr);
            try {
                String str = TAG;
                Log.i(str, "Adding incoming call " + bundleOf);
                Object systemService = ContextCompat.getSystemService(context, TelecomManager.class);
                Intrinsics.checkNotNull(systemService);
                ((TelecomManager) systemService).addNewIncomingCall(getPhoneAccountHandle(), bundleOf);
            } catch (SecurityException e) {
                Log.w(TAG, "Unable to add incoming call", e);
                systemRejected = true;
                return false;
            }
        }
        return true;
    }

    @JvmStatic
    public static final void reject(RecipientId recipientId) {
        AndroidCallConnection androidCallConnection;
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        if (getTelecomSupported() && (androidCallConnection = connections.get(recipientId)) != null) {
            androidCallConnection.setDisconnected(new DisconnectCause(6));
        }
    }

    @JvmStatic
    public static final void activateCall(RecipientId recipientId) {
        AndroidCallConnection androidCallConnection;
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        if (getTelecomSupported() && (androidCallConnection = connections.get(recipientId)) != null) {
            androidCallConnection.setActive();
        }
    }

    @JvmStatic
    public static final void terminateCall(RecipientId recipientId) {
        Map<RecipientId, AndroidCallConnection> map;
        AndroidCallConnection androidCallConnection;
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        if (getTelecomSupported() && (androidCallConnection = (map = connections).get(recipientId)) != null) {
            if (androidCallConnection.getDisconnectCause() == null) {
                androidCallConnection.setDisconnected(new DisconnectCause(0));
            }
            androidCallConnection.destroy();
            map.remove(recipientId);
        }
    }

    @JvmStatic
    public static final boolean addOutgoingCall(RecipientId recipientId, long j, boolean z) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        if (getTelecomSupported()) {
            Pair[] pairArr = new Pair[3];
            pairArr[0] = TuplesKt.to("android.telecom.extra.PHONE_ACCOUNT_HANDLE", getPhoneAccountHandle());
            pairArr[1] = TuplesKt.to("android.telecom.extra.START_CALL_WITH_VIDEO_STATE", Integer.valueOf(z ? 3 : 0));
            pairArr[2] = TuplesKt.to("android.telecom.extra.OUTGOING_CALL_EXTRAS", BundleKt.bundleOf(TuplesKt.to(AndroidCallConnectionService.KEY_RECIPIENT_ID, recipientId.serialize()), TuplesKt.to(AndroidCallConnectionService.KEY_CALL_ID, Long.valueOf(j)), TuplesKt.to(AndroidCallConnectionService.KEY_VIDEO_CALL, Boolean.valueOf(z))));
            Bundle bundleOf = BundleKt.bundleOf(pairArr);
            try {
                String str = TAG;
                Log.i(str, "Adding outgoing call " + bundleOf);
                Object systemService = ContextCompat.getSystemService(context, TelecomManager.class);
                Intrinsics.checkNotNull(systemService);
                ((TelecomManager) systemService).placeCall(AndroidTelecomUtilKt.generateTelecomE164(recipientId), bundleOf);
            } catch (SecurityException e) {
                Log.w(TAG, "Unable to add outgoing call", e);
                systemRejected = true;
                return false;
            }
        }
        return true;
    }

    public final void selectAudioDevice(RecipientId recipientId, SignalAudioManager.AudioDevice audioDevice) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(audioDevice, "device");
        if (getTelecomSupported()) {
            AndroidCallConnection androidCallConnection = connections.get(recipientId);
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Selecting audio route: ");
            sb.append(audioDevice);
            sb.append(" connection: ");
            sb.append(androidCallConnection != null);
            Log.i(str, sb.toString());
            if ((androidCallConnection != null ? androidCallConnection.getCallAudioState() : null) != null) {
                int i = WhenMappings.$EnumSwitchMapping$0[audioDevice.ordinal()];
                if (i == 1) {
                    AndroidTelecomUtilKt.setAudioRouteIfDifferent(androidCallConnection, 8);
                } else if (i == 2) {
                    AndroidTelecomUtilKt.setAudioRouteIfDifferent(androidCallConnection, 2);
                } else if (i != 3) {
                    AndroidTelecomUtilKt.setAudioRouteIfDifferent(androidCallConnection, 1);
                } else {
                    AndroidTelecomUtilKt.setAudioRouteIfDifferent(androidCallConnection, 4);
                }
            }
        }
    }

    public final SignalAudioManager.AudioDevice getSelectedAudioDevice(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        if (getTelecomSupported()) {
            AndroidCallConnection androidCallConnection = connections.get(recipientId);
            if ((androidCallConnection != null ? androidCallConnection.getCallAudioState() : null) != null) {
                int route = androidCallConnection.getCallAudioState().getRoute();
                if (route == 2) {
                    return SignalAudioManager.AudioDevice.BLUETOOTH;
                }
                if (route == 4) {
                    return SignalAudioManager.AudioDevice.WIRED_HEADSET;
                }
                if (route != 8) {
                    return SignalAudioManager.AudioDevice.EARPIECE;
                }
                return SignalAudioManager.AudioDevice.SPEAKER_PHONE;
            }
        }
        return SignalAudioManager.AudioDevice.NONE;
    }

    private final boolean isTelecomAllowedForDevice() {
        if (FeatureFlags.internalUser()) {
            return !SignalStore.internalValues().callingDisableTelecom();
        }
        return RingRtcDynamicConfiguration.INSTANCE.isTelecomAllowedForDevice();
    }
}
