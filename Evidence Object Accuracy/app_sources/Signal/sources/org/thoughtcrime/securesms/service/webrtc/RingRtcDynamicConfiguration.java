package org.thoughtcrime.securesms.service.webrtc;

import android.os.Build;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsJVMKt;
import kotlin.text.StringsKt__StringsKt;
import kotlin.text.StringsKt___StringsKt;
import org.signal.ringrtc.CallManager;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.FeatureFlags;

/* compiled from: RingRtcDynamicConfiguration.kt */
@Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0007J\b\u0010\u0005\u001a\u00020\u0006H\u0002J\b\u0010\u0007\u001a\u00020\u0006H\u0002J\u0006\u0010\b\u001a\u00020\u0006J\u0018\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u000bH\u0007¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/service/webrtc/RingRtcDynamicConfiguration;", "", "()V", "getAudioProcessingMethod", "Lorg/signal/ringrtc/CallManager$AudioProcessingMethod;", "isHardwareBlocklisted", "", "isSoftwareBlocklisted", "isTelecomAllowedForDevice", "modelInList", "model", "", "serializedList", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class RingRtcDynamicConfiguration {
    public static final RingRtcDynamicConfiguration INSTANCE = new RingRtcDynamicConfiguration();

    private RingRtcDynamicConfiguration() {
    }

    @JvmStatic
    public static final CallManager.AudioProcessingMethod getAudioProcessingMethod() {
        if (SignalStore.internalValues().callingAudioProcessingMethod() != CallManager.AudioProcessingMethod.Default) {
            CallManager.AudioProcessingMethod callingAudioProcessingMethod = SignalStore.internalValues().callingAudioProcessingMethod();
            Intrinsics.checkNotNullExpressionValue(callingAudioProcessingMethod, "internalValues().callingAudioProcessingMethod()");
            return callingAudioProcessingMethod;
        }
        boolean useAec3 = FeatureFlags.useAec3();
        RingRtcDynamicConfiguration ringRtcDynamicConfiguration = INSTANCE;
        if (ringRtcDynamicConfiguration.isHardwareBlocklisted() && useAec3) {
            return CallManager.AudioProcessingMethod.ForceSoftwareAec3;
        }
        if (ringRtcDynamicConfiguration.isHardwareBlocklisted()) {
            return CallManager.AudioProcessingMethod.ForceSoftwareAecM;
        }
        if (ringRtcDynamicConfiguration.isSoftwareBlocklisted()) {
            return CallManager.AudioProcessingMethod.ForceHardware;
        }
        int i = Build.VERSION.SDK_INT;
        if (i < 29 && FeatureFlags.useHardwareAecIfOlderThanApi29()) {
            return CallManager.AudioProcessingMethod.ForceHardware;
        }
        if (i < 29 && useAec3) {
            return CallManager.AudioProcessingMethod.ForceSoftwareAec3;
        }
        if (i < 29) {
            return CallManager.AudioProcessingMethod.ForceSoftwareAecM;
        }
        return CallManager.AudioProcessingMethod.ForceHardware;
    }

    public final boolean isTelecomAllowedForDevice() {
        String str = Build.MANUFACTURER;
        Intrinsics.checkNotNullExpressionValue(str, "MANUFACTURER");
        Locale locale = Locale.ROOT;
        String lowerCase = str.toLowerCase(locale);
        Intrinsics.checkNotNullExpressionValue(lowerCase, "this as java.lang.String).toLowerCase(Locale.ROOT)");
        String telecomManufacturerAllowList = FeatureFlags.telecomManufacturerAllowList();
        Intrinsics.checkNotNullExpressionValue(telecomManufacturerAllowList, "telecomManufacturerAllowList()");
        String lowerCase2 = telecomManufacturerAllowList.toLowerCase(locale);
        Intrinsics.checkNotNullExpressionValue(lowerCase2, "this as java.lang.String).toLowerCase(Locale.ROOT)");
        if (modelInList(lowerCase, lowerCase2)) {
            String str2 = Build.MODEL;
            Intrinsics.checkNotNullExpressionValue(str2, "MODEL");
            String lowerCase3 = str2.toLowerCase(locale);
            Intrinsics.checkNotNullExpressionValue(lowerCase3, "this as java.lang.String).toLowerCase(Locale.ROOT)");
            String telecomModelBlockList = FeatureFlags.telecomModelBlockList();
            Intrinsics.checkNotNullExpressionValue(telecomModelBlockList, "telecomModelBlockList()");
            String lowerCase4 = telecomModelBlockList.toLowerCase(locale);
            Intrinsics.checkNotNullExpressionValue(lowerCase4, "this as java.lang.String).toLowerCase(Locale.ROOT)");
            if (!modelInList(lowerCase3, lowerCase4)) {
                return true;
            }
        }
        return false;
    }

    private final boolean isHardwareBlocklisted() {
        String str = Build.MODEL;
        Intrinsics.checkNotNullExpressionValue(str, "MODEL");
        String hardwareAecBlocklistModels = FeatureFlags.hardwareAecBlocklistModels();
        Intrinsics.checkNotNullExpressionValue(hardwareAecBlocklistModels, "hardwareAecBlocklistModels()");
        return modelInList(str, hardwareAecBlocklistModels);
    }

    private final boolean isSoftwareBlocklisted() {
        String str = Build.MODEL;
        Intrinsics.checkNotNullExpressionValue(str, "MODEL");
        String softwareAecBlocklistModels = FeatureFlags.softwareAecBlocklistModels();
        Intrinsics.checkNotNullExpressionValue(softwareAecBlocklistModels, "softwareAecBlocklistModels()");
        return modelInList(str, softwareAecBlocklistModels);
    }

    public final boolean modelInList(String str, String str2) {
        boolean z;
        Intrinsics.checkNotNullParameter(str, "model");
        Intrinsics.checkNotNullParameter(str2, "serializedList");
        List<String> list = StringsKt__StringsKt.split$default((CharSequence) str2, new String[]{","}, false, 0, 6, (Object) null);
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (String str3 : list) {
            arrayList.add(StringsKt__StringsKt.trim(str3).toString());
        }
        ArrayList arrayList2 = new ArrayList();
        Iterator it = arrayList.iterator();
        while (true) {
            boolean z2 = true;
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            if (((String) next).length() <= 0) {
                z2 = false;
            }
            if (z2) {
                arrayList2.add(next);
            }
        }
        List list2 = CollectionsKt___CollectionsKt.toList(arrayList2);
        ArrayList arrayList3 = new ArrayList();
        for (Object obj : list2) {
            if (StringsKt___StringsKt.last((String) obj) != '*') {
                arrayList3.add(obj);
            }
        }
        ArrayList<String> arrayList4 = new ArrayList();
        for (Object obj2 : list2) {
            if (StringsKt___StringsKt.last((String) obj2) == '*') {
                arrayList4.add(obj2);
            }
        }
        if (arrayList3.contains(str)) {
            return true;
        }
        ArrayList<String> arrayList5 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList4, 10));
        for (String str4 : arrayList4) {
            String substring = str4.substring(0, str4.length() - 1);
            Intrinsics.checkNotNullExpressionValue(substring, "this as java.lang.String…ing(startIndex, endIndex)");
            arrayList5.add(substring);
        }
        if (!arrayList5.isEmpty()) {
            for (String str5 : arrayList5) {
                if (StringsKt__StringsJVMKt.startsWith$default(str, str5, false, 2, null)) {
                    z = true;
                    break;
                }
            }
        }
        z = false;
        if (z) {
            return true;
        }
        return false;
    }
}
