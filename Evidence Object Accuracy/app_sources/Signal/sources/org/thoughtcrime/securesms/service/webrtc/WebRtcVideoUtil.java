package org.thoughtcrime.securesms.service.webrtc;

import android.content.Context;
import org.signal.core.util.ThreadUtil;
import org.thoughtcrime.securesms.components.webrtc.BroadcastVideoSink;
import org.thoughtcrime.securesms.components.webrtc.EglBaseWrapper;
import org.thoughtcrime.securesms.ringrtc.Camera;
import org.thoughtcrime.securesms.ringrtc.CameraEventListener;
import org.thoughtcrime.securesms.ringrtc.CameraState;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceState;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceStateBuilder;
import org.webrtc.CapturerObserver;
import org.webrtc.VideoFrame;

/* loaded from: classes4.dex */
public final class WebRtcVideoUtil {
    private WebRtcVideoUtil() {
    }

    public static WebRtcServiceState initializeVideo(Context context, CameraEventListener cameraEventListener, WebRtcServiceState webRtcServiceState, Object obj) {
        WebRtcServiceStateBuilder builder = webRtcServiceState.builder();
        ThreadUtil.runOnMainSync(new Runnable(obj, webRtcServiceState, context, cameraEventListener, builder) { // from class: org.thoughtcrime.securesms.service.webrtc.WebRtcVideoUtil$$ExternalSyntheticLambda1
            public final /* synthetic */ Object f$0;
            public final /* synthetic */ WebRtcServiceState f$1;
            public final /* synthetic */ Context f$2;
            public final /* synthetic */ CameraEventListener f$3;
            public final /* synthetic */ WebRtcServiceStateBuilder f$4;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // java.lang.Runnable
            public final void run() {
                WebRtcVideoUtil.lambda$initializeVideo$0(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4);
            }
        });
        return builder.build();
    }

    public static /* synthetic */ void lambda$initializeVideo$0(Object obj, WebRtcServiceState webRtcServiceState, Context context, CameraEventListener cameraEventListener, WebRtcServiceStateBuilder webRtcServiceStateBuilder) {
        EglBaseWrapper acquireEglBase = EglBaseWrapper.acquireEglBase(obj);
        BroadcastVideoSink broadcastVideoSink = new BroadcastVideoSink(acquireEglBase, true, false, webRtcServiceState.getLocalDeviceState().getOrientation().getDegrees());
        Camera camera = new Camera(context, cameraEventListener, acquireEglBase, CameraState.Direction.FRONT);
        camera.setOrientation(Integer.valueOf(webRtcServiceState.getLocalDeviceState().getOrientation().getDegrees()));
        webRtcServiceStateBuilder.changeVideoState().eglBase(acquireEglBase).localSink(broadcastVideoSink).camera(camera).commit().changeLocalDeviceState().cameraState(camera.getCameraState()).commit();
    }

    public static WebRtcServiceState reinitializeCamera(Context context, CameraEventListener cameraEventListener, WebRtcServiceState webRtcServiceState) {
        WebRtcServiceStateBuilder builder = webRtcServiceState.builder();
        ThreadUtil.runOnMainSync(new Runnable(context, cameraEventListener, builder) { // from class: org.thoughtcrime.securesms.service.webrtc.WebRtcVideoUtil$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$1;
            public final /* synthetic */ CameraEventListener f$2;
            public final /* synthetic */ WebRtcServiceStateBuilder f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                WebRtcVideoUtil.lambda$reinitializeCamera$1(WebRtcServiceState.this, this.f$1, this.f$2, this.f$3);
            }
        });
        return builder.build();
    }

    public static /* synthetic */ void lambda$reinitializeCamera$1(WebRtcServiceState webRtcServiceState, Context context, CameraEventListener cameraEventListener, WebRtcServiceStateBuilder webRtcServiceStateBuilder) {
        Camera requireCamera = webRtcServiceState.getVideoState().requireCamera();
        requireCamera.setEnabled(false);
        requireCamera.dispose();
        Camera camera = new Camera(context, cameraEventListener, webRtcServiceState.getVideoState().getLockableEglBase(), webRtcServiceState.getLocalDeviceState().getCameraState().getActiveDirection());
        camera.setOrientation(Integer.valueOf(webRtcServiceState.getLocalDeviceState().getOrientation().getDegrees()));
        webRtcServiceStateBuilder.changeVideoState().camera(camera).commit().changeLocalDeviceState().cameraState(camera.getCameraState()).commit();
    }

    public static WebRtcServiceState deinitializeVideo(WebRtcServiceState webRtcServiceState) {
        Camera camera = webRtcServiceState.getVideoState().getCamera();
        if (camera != null) {
            camera.dispose();
        }
        return webRtcServiceState.builder().changeVideoState().eglBase(null).camera(null).localSink(null).commit().changeLocalDeviceState().cameraState(CameraState.UNKNOWN).build();
    }

    public static WebRtcServiceState initializeVanityCamera(WebRtcServiceState webRtcServiceState) {
        Camera requireCamera = webRtcServiceState.getVideoState().requireCamera();
        final BroadcastVideoSink requireLocalSink = webRtcServiceState.getVideoState().requireLocalSink();
        if (requireCamera.hasCapturer()) {
            requireCamera.initCapturer(new CapturerObserver() { // from class: org.thoughtcrime.securesms.service.webrtc.WebRtcVideoUtil.1
                @Override // org.webrtc.CapturerObserver
                public void onCapturerStarted(boolean z) {
                }

                @Override // org.webrtc.CapturerObserver
                public void onCapturerStopped() {
                }

                @Override // org.webrtc.CapturerObserver
                public void onFrameCaptured(VideoFrame videoFrame) {
                    requireLocalSink.onFrame(videoFrame);
                }
            });
            requireCamera.setEnabled(true);
        }
        return webRtcServiceState.builder().changeLocalDeviceState().cameraState(requireCamera.getCameraState()).build();
    }
}
