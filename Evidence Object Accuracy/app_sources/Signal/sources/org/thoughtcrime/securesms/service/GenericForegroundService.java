package org.thoughtcrime.securesms.service;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.MainActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.service.DelayedNotificationController;

/* loaded from: classes4.dex */
public final class GenericForegroundService extends Service {
    private static final String ACTION_START;
    private static final String ACTION_STOP;
    private static final Entry DEFAULTS = new Entry("", NotificationChannels.OTHER, R.drawable.ic_notification, -1, 0, 0, false);
    private static final String EXTRA_CHANNEL_ID;
    private static final String EXTRA_ICON_RES;
    private static final String EXTRA_ID;
    private static final String EXTRA_PROGRESS;
    private static final String EXTRA_PROGRESS_INDETERMINATE;
    private static final String EXTRA_PROGRESS_MAX;
    private static final String EXTRA_TITLE;
    private static final AtomicInteger NEXT_ID = new AtomicInteger();
    private static final int NOTIFICATION_ID;
    private static final String TAG = Log.tag(GenericForegroundService.class);
    private final LinkedHashMap<Integer, Entry> allActiveMessages = new LinkedHashMap<>();
    private final IBinder binder = new LocalBinder();
    private Entry lastPosted;

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        if (intent != null) {
            synchronized (GenericForegroundService.class) {
                String action = intent.getAction();
                if (action != null) {
                    if ("start".equals(action)) {
                        handleStart(intent);
                    } else if (ACTION_STOP.equals(action)) {
                        handleStop(intent);
                    } else {
                        throw new IllegalStateException(String.format("Action needs to be %s or %s.", "start", ACTION_STOP));
                    }
                    updateNotification();
                }
            }
            return 2;
        }
        throw new IllegalStateException("Intent needs to be non-null.");
    }

    private synchronized void updateNotification() {
        Iterator<Entry> it = this.allActiveMessages.values().iterator();
        if (it.hasNext()) {
            postObligatoryForegroundNotification(it.next());
        } else {
            Log.i(TAG, "Last request. Ending foreground service.");
            Entry entry = this.lastPosted;
            if (entry == null) {
                entry = DEFAULTS;
            }
            postObligatoryForegroundNotification(entry);
            stopForeground(true);
            stopSelf();
        }
    }

    private synchronized void handleStart(Intent intent) {
        Entry fromIntent = Entry.fromIntent(intent);
        Log.i(TAG, String.format(Locale.US, "handleStart() %s", fromIntent));
        this.allActiveMessages.put(Integer.valueOf(fromIntent.id), fromIntent);
    }

    private synchronized void handleStop(Intent intent) {
        String str = TAG;
        Log.i(str, "handleStop()");
        if (this.allActiveMessages.remove(Integer.valueOf(intent.getIntExtra(EXTRA_ID, -1))) == null) {
            Log.w(str, "Could not find entry to remove");
        }
    }

    private void postObligatoryForegroundNotification(Entry entry) {
        this.lastPosted = entry;
        startForeground(NOTIFICATION_ID, new NotificationCompat.Builder(this, entry.channelId).setSmallIcon(entry.iconRes).setContentTitle(entry.title).setProgress(entry.progressMax, entry.progress, entry.indeterminate).setContentIntent(PendingIntent.getActivity(this, 0, MainActivity.clearTop(this), 0)).setVibrate(new long[]{0}).build());
    }

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return this.binder;
    }

    public static /* synthetic */ NotificationController lambda$startForegroundTaskDelayed$0(Context context, String str, int i) {
        return startForegroundTask(context, str, DEFAULTS.channelId, i);
    }

    public static DelayedNotificationController startForegroundTaskDelayed(Context context, String str, long j, int i) {
        return DelayedNotificationController.create(j, new DelayedNotificationController.Create(context, str, i) { // from class: org.thoughtcrime.securesms.service.GenericForegroundService$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ String f$1;
            public final /* synthetic */ int f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.thoughtcrime.securesms.service.DelayedNotificationController.Create
            public final NotificationController create() {
                return GenericForegroundService.lambda$startForegroundTaskDelayed$0(this.f$0, this.f$1, this.f$2);
            }
        });
    }

    public static NotificationController startForegroundTask(Context context, String str) {
        return startForegroundTask(context, str, DEFAULTS.channelId);
    }

    public static NotificationController startForegroundTask(Context context, String str, String str2) {
        return startForegroundTask(context, str, str2, DEFAULTS.iconRes);
    }

    public static NotificationController startForegroundTask(Context context, String str, String str2, int i) {
        int andIncrement = NEXT_ID.getAndIncrement();
        Intent intent = new Intent(context, GenericForegroundService.class);
        intent.setAction("start");
        intent.putExtra(EXTRA_TITLE, str);
        intent.putExtra(EXTRA_CHANNEL_ID, str2);
        intent.putExtra(EXTRA_ICON_RES, i);
        intent.putExtra(EXTRA_ID, andIncrement);
        Log.i(TAG, String.format(Locale.US, "Starting foreground service (%s) id=%d", str, Integer.valueOf(andIncrement)));
        ContextCompat.startForegroundService(context, intent);
        return new NotificationController(context, andIncrement);
    }

    public static void stopForegroundTask(Context context, int i) {
        Intent intent = new Intent(context, GenericForegroundService.class);
        intent.setAction(ACTION_STOP);
        intent.putExtra(EXTRA_ID, i);
        Log.i(TAG, String.format(Locale.US, "Stopping foreground service id=%d", Integer.valueOf(i)));
        ContextCompat.startForegroundService(context, intent);
    }

    public synchronized void replaceProgress(int i, int i2, int i3, boolean z) {
        Entry entry = this.allActiveMessages.get(Integer.valueOf(i));
        if (entry == null) {
            Log.w(TAG, "Failed to replace notification, it was not found");
            return;
        }
        Entry entry2 = new Entry(entry.title, entry.channelId, entry.iconRes, entry.id, i2, i3, z);
        if (entry.equals(entry2)) {
            Log.d(TAG, String.format("handleReplace() skip, no change %s", entry2));
            return;
        }
        Log.i(TAG, String.format("handleReplace() %s", entry2));
        this.allActiveMessages.put(Integer.valueOf(entry2.id), entry2);
        updateNotification();
    }

    /* loaded from: classes4.dex */
    public static class Entry {
        final String channelId;
        final int iconRes;
        final int id;
        final boolean indeterminate;
        final int progress;
        final int progressMax;
        final String title;

        private Entry(String str, String str2, int i, int i2, int i3, int i4, boolean z) {
            this.title = str;
            this.channelId = str2;
            this.iconRes = i;
            this.id = i2;
            this.progress = i4;
            this.progressMax = i3;
            this.indeterminate = z;
        }

        public static Entry fromIntent(Intent intent) {
            int intExtra = intent.getIntExtra(GenericForegroundService.EXTRA_ID, GenericForegroundService.DEFAULTS.id);
            String stringExtra = intent.getStringExtra(GenericForegroundService.EXTRA_TITLE);
            if (stringExtra == null) {
                stringExtra = GenericForegroundService.DEFAULTS.title;
            }
            String stringExtra2 = intent.getStringExtra(GenericForegroundService.EXTRA_CHANNEL_ID);
            if (stringExtra2 == null) {
                stringExtra2 = GenericForegroundService.DEFAULTS.channelId;
            }
            return new Entry(stringExtra, stringExtra2, intent.getIntExtra(GenericForegroundService.EXTRA_ICON_RES, GenericForegroundService.DEFAULTS.iconRes), intExtra, intent.getIntExtra(GenericForegroundService.EXTRA_PROGRESS_MAX, GenericForegroundService.DEFAULTS.progressMax), intent.getIntExtra(GenericForegroundService.EXTRA_PROGRESS, GenericForegroundService.DEFAULTS.progress), intent.getBooleanExtra(GenericForegroundService.EXTRA_PROGRESS_INDETERMINATE, GenericForegroundService.DEFAULTS.indeterminate));
        }

        public String toString() {
            Locale locale = Locale.US;
            Object[] objArr = new Object[5];
            objArr[0] = this.channelId;
            objArr[1] = Integer.valueOf(this.id);
            objArr[2] = Integer.valueOf(this.progress);
            objArr[3] = Integer.valueOf(this.progressMax);
            objArr[4] = this.indeterminate ? "indeterminate" : "determinate";
            return String.format(locale, "ChannelId: %s  Id: %d Progress: %d/%d %s", objArr);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            Entry entry = (Entry) obj;
            if (this.id == entry.id && this.iconRes == entry.iconRes && this.progress == entry.progress && this.progressMax == entry.progressMax && this.indeterminate == entry.indeterminate && Objects.equals(this.title, entry.title) && Objects.equals(this.channelId, entry.channelId)) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return (((((((((((this.title.hashCode() * 31) + this.channelId.hashCode()) * 31) + this.id) * 31) + this.iconRes) * 31) + this.progress) * 31) + this.progressMax) * 31) + (this.indeterminate ? 1 : 0);
        }
    }

    /* loaded from: classes4.dex */
    class LocalBinder extends Binder {
        LocalBinder() {
            GenericForegroundService.this = r1;
        }

        public GenericForegroundService getService() {
            return GenericForegroundService.this;
        }
    }
}
