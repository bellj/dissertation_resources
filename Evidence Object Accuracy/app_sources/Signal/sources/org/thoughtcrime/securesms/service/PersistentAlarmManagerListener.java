package org.thoughtcrime.securesms.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import org.signal.core.util.logging.Log;

/* loaded from: classes.dex */
public abstract class PersistentAlarmManagerListener extends BroadcastReceiver {
    private static final String TAG = Log.tag(PersistentAlarmManagerListener.class);

    protected abstract long getNextScheduledExecutionTime(Context context);

    protected abstract long onAlarm(Context context, long j);

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        String str = TAG;
        Log.i(str, String.format("%s#onReceive(%s)", getClass().getSimpleName(), intent.getAction()));
        long nextScheduledExecutionTime = getNextScheduledExecutionTime(context);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, new Intent(context, getClass()), 0);
        if (System.currentTimeMillis() >= nextScheduledExecutionTime) {
            nextScheduledExecutionTime = onAlarm(context, nextScheduledExecutionTime);
        }
        Log.i(str, getClass() + " scheduling for: " + nextScheduledExecutionTime + " action: " + intent.getAction());
        if (broadcast != null) {
            alarmManager.cancel(broadcast);
            alarmManager.set(0, nextScheduledExecutionTime, broadcast);
            return;
        }
        Log.i(str, "PendingIntent somehow null, skipping");
    }
}
