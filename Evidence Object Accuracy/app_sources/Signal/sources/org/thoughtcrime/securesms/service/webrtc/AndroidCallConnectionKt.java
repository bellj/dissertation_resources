package org.thoughtcrime.securesms.service.webrtc;

import java.util.LinkedHashSet;
import java.util.Set;
import kotlin.Metadata;
import org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager;

/* compiled from: AndroidCallConnection.kt */
@Metadata(d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\u001a\u0012\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u0003H\u0002¨\u0006\u0004"}, d2 = {"toDevices", "", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$AudioDevice;", "", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AndroidCallConnectionKt {
    public static final Set<SignalAudioManager.AudioDevice> toDevices(int i) {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        if ((i & 2) != 0) {
            linkedHashSet.add(SignalAudioManager.AudioDevice.BLUETOOTH);
        }
        if ((i & 1) != 0) {
            linkedHashSet.add(SignalAudioManager.AudioDevice.EARPIECE);
        }
        if ((i & 4) != 0) {
            linkedHashSet.add(SignalAudioManager.AudioDevice.WIRED_HEADSET);
        }
        if ((i & 8) != 0) {
            linkedHashSet.add(SignalAudioManager.AudioDevice.SPEAKER_PHONE);
        }
        return linkedHashSet;
    }
}
