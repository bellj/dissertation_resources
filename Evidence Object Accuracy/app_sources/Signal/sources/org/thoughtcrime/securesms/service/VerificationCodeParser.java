package org.thoughtcrime.securesms.service;

import j$.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* loaded from: classes4.dex */
public class VerificationCodeParser {
    private static final Pattern CHALLENGE_PATTERN = Pattern.compile("(.*\\D|^)([0-9]{3,4})-?([0-9]{3,4}).*", 32);

    public static Optional<String> parse(String str) {
        if (str == null) {
            return Optional.empty();
        }
        Matcher matcher = CHALLENGE_PATTERN.matcher(str);
        if (!matcher.matches()) {
            return Optional.empty();
        }
        return Optional.of(matcher.group(matcher.groupCount() - 1) + matcher.group(matcher.groupCount()));
    }
}
