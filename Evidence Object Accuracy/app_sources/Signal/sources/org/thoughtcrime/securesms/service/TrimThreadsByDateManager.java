package org.thoughtcrime.securesms.service;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.MmsSmsDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.KeepMessagesDuration;
import org.thoughtcrime.securesms.keyvalue.SignalStore;

/* loaded from: classes4.dex */
public class TrimThreadsByDateManager extends TimedEventManager<TrimEvent> {
    private static final String TAG = Log.tag(TrimThreadsByDateManager.class);
    private final MmsSmsDatabase mmsSmsDatabase = SignalDatabase.mmsSms();
    private final ThreadDatabase threadDatabase = SignalDatabase.threads();

    public TrimThreadsByDateManager(Application application) {
        super(application, "TrimThreadsByDateManager");
        scheduleIfNecessary();
    }

    @Override // org.thoughtcrime.securesms.service.TimedEventManager
    public TrimEvent getNextClosestEvent() {
        KeepMessagesDuration keepMessagesDuration = SignalStore.settings().getKeepMessagesDuration();
        if (keepMessagesDuration == KeepMessagesDuration.FOREVER) {
            return null;
        }
        long currentTimeMillis = System.currentTimeMillis() - keepMessagesDuration.getDuration();
        if (this.mmsSmsDatabase.getMessageCountBeforeDate(currentTimeMillis) > 0) {
            Log.i(TAG, "Messages exist before date, trim immediately");
            return new TrimEvent(0);
        }
        long timestampForFirstMessageAfterDate = this.mmsSmsDatabase.getTimestampForFirstMessageAfterDate(currentTimeMillis);
        if (timestampForFirstMessageAfterDate == 0) {
            return null;
        }
        return new TrimEvent(Math.max(0L, keepMessagesDuration.getDuration() - (System.currentTimeMillis() - timestampForFirstMessageAfterDate)));
    }

    public void executeEvent(TrimEvent trimEvent) {
        KeepMessagesDuration keepMessagesDuration = SignalStore.settings().getKeepMessagesDuration();
        int threadTrimLength = SignalStore.settings().isTrimByLengthEnabled() ? SignalStore.settings().getThreadTrimLength() : Integer.MAX_VALUE;
        long currentTimeMillis = keepMessagesDuration != KeepMessagesDuration.FOREVER ? System.currentTimeMillis() - keepMessagesDuration.getDuration() : 0;
        String str = TAG;
        Log.i(str, "Trimming all threads with length: " + threadTrimLength + " before: " + currentTimeMillis);
        this.threadDatabase.trimAllThreads(threadTrimLength, currentTimeMillis);
    }

    public long getDelayForEvent(TrimEvent trimEvent) {
        return trimEvent.delay;
    }

    @Override // org.thoughtcrime.securesms.service.TimedEventManager
    protected void scheduleAlarm(Application application, long j) {
        TimedEventManager.setAlarm(application, j, TrimThreadsByDateAlarm.class);
    }

    /* loaded from: classes4.dex */
    public static class TrimThreadsByDateAlarm extends BroadcastReceiver {
        private static final String TAG = Log.tag(TrimThreadsByDateAlarm.class);

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive()");
            ApplicationDependencies.getTrimThreadsByDateManager().scheduleIfNecessary();
        }
    }

    /* loaded from: classes4.dex */
    public static class TrimEvent {
        final long delay;

        public TrimEvent(long j) {
            this.delay = j;
        }
    }
}
