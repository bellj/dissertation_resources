package org.thoughtcrime.securesms.service.webrtc;

import java.util.UUID;
import org.signal.core.util.logging.Log;
import org.signal.ringrtc.CallException;
import org.signal.ringrtc.CallManager;
import org.thoughtcrime.securesms.components.webrtc.EglBaseWrapper;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfiles;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceState;
import org.whispersystems.signalservice.api.messages.calls.OfferMessage;

/* loaded from: classes4.dex */
public class IdleActionProcessor extends WebRtcActionProcessor {
    private static final String TAG = Log.tag(IdleActionProcessor.class);
    private final BeginCallActionProcessorDelegate beginCallDelegate;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public IdleActionProcessor(org.thoughtcrime.securesms.service.webrtc.WebRtcInteractor r3) {
        /*
            r2 = this;
            java.lang.String r0 = org.thoughtcrime.securesms.service.webrtc.IdleActionProcessor.TAG
            r2.<init>(r3, r0)
            org.thoughtcrime.securesms.service.webrtc.BeginCallActionProcessorDelegate r1 = new org.thoughtcrime.securesms.service.webrtc.BeginCallActionProcessorDelegate
            r1.<init>(r3, r0)
            r2.beginCallDelegate = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.service.webrtc.IdleActionProcessor.<init>(org.thoughtcrime.securesms.service.webrtc.WebRtcInteractor):void");
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleStartIncomingCall(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer, OfferMessage.Type type) {
        Log.i(TAG, "handleStartIncomingCall():");
        return this.beginCallDelegate.handleStartIncomingCall(WebRtcVideoUtil.initializeVideo(this.context, this.webRtcInteractor.getCameraEventListener(), webRtcServiceState, remotePeer.getCallId().longValue()), remotePeer, type);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleOutgoingCall(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer, OfferMessage.Type type) {
        String str = TAG;
        Log.i(str, "handleOutgoingCall():");
        if (Recipient.resolved(remotePeer.getId()).isGroup()) {
            Log.w(str, "Aborting attempt to start 1:1 call for group recipient: " + remotePeer.getId());
            return webRtcServiceState;
        }
        return this.beginCallDelegate.handleOutgoingCall(WebRtcVideoUtil.initializeVideo(this.context, this.webRtcInteractor.getCameraEventListener(), webRtcServiceState, EglBaseWrapper.OUTGOING_PLACEHOLDER), remotePeer, type);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handlePreJoinCall(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer) {
        WebRtcActionProcessor webRtcActionProcessor;
        Log.i(TAG, "handlePreJoinCall():");
        boolean isPushV2Group = remotePeer.getRecipient().isPushV2Group();
        if (isPushV2Group) {
            webRtcActionProcessor = new GroupPreJoinActionProcessor(this.webRtcInteractor);
        } else {
            webRtcActionProcessor = new PreJoinActionProcessor(this.webRtcInteractor);
        }
        WebRtcServiceState build = WebRtcVideoUtil.initializeVanityCamera(WebRtcVideoUtil.initializeVideo(this.context, this.webRtcInteractor.getCameraEventListener(), webRtcServiceState, isPushV2Group ? RemotePeer.GROUP_CALL_ID.longValue() : EglBaseWrapper.OUTGOING_PLACEHOLDER)).builder().actionProcessor(webRtcActionProcessor).changeCallInfoState().callState(WebRtcViewModel.State.CALL_PRE_JOIN).callRecipient(remotePeer.getRecipient()).build();
        return isPushV2Group ? build.getActionProcessor().handlePreJoinCall(build, remotePeer) : build;
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleGroupCallRingUpdate(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer, GroupId.V2 v2, long j, UUID uuid, CallManager.RingUpdate ringUpdate) {
        String str = TAG;
        Log.i(str, "handleGroupCallRingUpdate(): recipient: " + remotePeer.getId() + " ring: " + j + " update: " + ringUpdate);
        if (ringUpdate != CallManager.RingUpdate.REQUESTED) {
            SignalDatabase.groupCallRings().insertOrUpdateGroupRing(j, System.currentTimeMillis(), ringUpdate);
            return webRtcServiceState;
        } else if (SignalDatabase.groupCallRings().isCancelled(j)) {
            try {
                Log.i(str, "Incoming ring request for already cancelled ring: " + j);
                this.webRtcInteractor.getCallManager().cancelGroupRing(v2.getDecodedId(), j, null);
            } catch (CallException e) {
                String str2 = TAG;
                Log.w(str2, "Error while trying to cancel ring: " + j, e);
            }
            return webRtcServiceState;
        } else {
            NotificationProfile activeProfile = NotificationProfiles.getActiveProfile(SignalDatabase.notificationProfiles().getProfiles());
            if (activeProfile == null || activeProfile.isRecipientAllowed(remotePeer.getId()) || activeProfile.getAllowAllCalls()) {
                this.webRtcInteractor.peekGroupCallForRingingCheck(new GroupCallRingCheckInfo(remotePeer.getId(), v2, j, uuid, ringUpdate));
                return webRtcServiceState;
            }
            try {
                Log.i(str, "Incoming ring request for profile restricted recipient");
                SignalDatabase.groupCallRings().insertOrUpdateGroupRing(j, System.currentTimeMillis(), CallManager.RingUpdate.EXPIRED_REQUEST);
                this.webRtcInteractor.getCallManager().cancelGroupRing(v2.getDecodedId(), j, CallManager.RingCancelReason.DeclinedByUser);
            } catch (CallException e2) {
                String str3 = TAG;
                Log.w(str3, "Error while trying to cancel ring: " + j, e2);
            }
            return webRtcServiceState;
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleReceivedGroupCallPeekForRingingCheck(WebRtcServiceState webRtcServiceState, GroupCallRingCheckInfo groupCallRingCheckInfo, long j) {
        String str = this.tag;
        Log.i(str, "handleReceivedGroupCallPeekForRingingCheck(): recipient: " + groupCallRingCheckInfo.getRecipientId() + " ring: " + groupCallRingCheckInfo.getRingId() + " deviceCount: " + j);
        if (SignalDatabase.groupCallRings().isCancelled(groupCallRingCheckInfo.getRingId())) {
            try {
                String str2 = TAG;
                Log.i(str2, "Ring was cancelled while getting peek info ring: " + groupCallRingCheckInfo.getRingId());
                this.webRtcInteractor.getCallManager().cancelGroupRing(groupCallRingCheckInfo.getGroupId().getDecodedId(), groupCallRingCheckInfo.getRingId(), null);
            } catch (CallException e) {
                String str3 = TAG;
                Log.w(str3, "Error while trying to cancel ring: " + groupCallRingCheckInfo.getRingId(), e);
            }
            return webRtcServiceState;
        } else if (j == 0) {
            Log.i(TAG, "No one in the group call, mark as expired and do not ring");
            SignalDatabase.groupCallRings().insertOrUpdateGroupRing(groupCallRingCheckInfo.getRingId(), System.currentTimeMillis(), CallManager.RingUpdate.EXPIRED_REQUEST);
            return webRtcServiceState;
        } else {
            WebRtcServiceState build = webRtcServiceState.builder().actionProcessor(new IncomingGroupCallActionProcessor(this.webRtcInteractor)).build();
            return build.getActionProcessor().handleGroupCallRingUpdate(build, new RemotePeer(groupCallRingCheckInfo.getRecipientId()), groupCallRingCheckInfo.getGroupId(), groupCallRingCheckInfo.getRingId(), groupCallRingCheckInfo.getRingerUuid(), groupCallRingCheckInfo.getRingUpdate());
        }
    }
}
