package org.thoughtcrime.securesms.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import androidx.core.app.NotificationCompat;
import java.lang.ref.WeakReference;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.MainActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.SmsMigrator;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.notifications.NotificationIds;

/* loaded from: classes4.dex */
public class ApplicationMigrationService extends Service implements SmsMigrator.SmsMigrationProgressListener {
    public static final String COMPLETED_ACTION;
    private static final String DATABASE_MIGRATED;
    public static final String MIGRATE_DATABASE;
    private static final String PREFERENCES_NAME;
    private static final String TAG = Log.tag(ApplicationMigrationService.class);
    private final Binder binder = new ApplicationMigrationBinder();
    private final BroadcastReceiver completedReceiver = new CompletedReceiver();
    private final Executor executor = Executors.newSingleThreadExecutor();
    private WeakReference<Handler> handler = null;
    private NotificationCompat.Builder notification = null;
    private ImportState state = new ImportState(0, null);

    @Override // android.app.Service
    public void onCreate() {
        registerCompletedReceiver();
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        if (!(intent == null || intent.getAction() == null || !intent.getAction().equals(MIGRATE_DATABASE))) {
            this.executor.execute(new ImportRunnable());
        }
        return 2;
    }

    @Override // android.app.Service
    public void onDestroy() {
        unregisterCompletedReceiver();
    }

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return this.binder;
    }

    public void setImportStateHandler(Handler handler) {
        this.handler = new WeakReference<>(handler);
    }

    private void registerCompletedReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(COMPLETED_ACTION);
        registerReceiver(this.completedReceiver, intentFilter);
    }

    private void unregisterCompletedReceiver() {
        unregisterReceiver(this.completedReceiver);
    }

    public void notifyImportComplete() {
        Intent intent = new Intent();
        intent.setAction(COMPLETED_ACTION);
        sendOrderedBroadcast(intent, null);
    }

    @Override // org.thoughtcrime.securesms.database.SmsMigrator.SmsMigrationProgressListener
    public void progressUpdate(SmsMigrator.ProgressDescription progressDescription) {
        setState(new ImportState(2, progressDescription));
    }

    public ImportState getState() {
        return this.state;
    }

    public void setState(ImportState importState) {
        Handler handler;
        this.state = importState;
        WeakReference<Handler> weakReference = this.handler;
        if (!(weakReference == null || (handler = weakReference.get()) == null)) {
            handler.obtainMessage(importState.state, importState.progress).sendToTarget();
        }
        SmsMigrator.ProgressDescription progressDescription = importState.progress;
        if (progressDescription != null && progressDescription.secondaryComplete == 0) {
            updateBackgroundNotification(progressDescription.primaryTotal, progressDescription.primaryComplete);
        }
    }

    private void updateBackgroundNotification(int i, int i2) {
        this.notification.setProgress(i, i2, false);
        ((NotificationManager) getSystemService("notification")).notify(NotificationIds.APPLICATION_MIGRATION, this.notification.build());
    }

    public NotificationCompat.Builder initializeBackgroundNotification() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NotificationChannels.OTHER);
        builder.setSmallIcon(R.drawable.ic_notification);
        builder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_notification));
        builder.setContentTitle(getString(R.string.ApplicationMigrationService_importing_text_messages));
        builder.setContentText(getString(R.string.ApplicationMigrationService_import_in_progress));
        builder.setOngoing(true);
        builder.setProgress(100, 0, false);
        builder.setContentIntent(PendingIntent.getActivity(this, 0, MainActivity.clearTop(this), 0));
        stopForeground(true);
        startForeground(NotificationIds.APPLICATION_MIGRATION, builder.build());
        return builder;
    }

    /* loaded from: classes4.dex */
    private class ImportRunnable implements Runnable {
        ImportRunnable() {
            ApplicationMigrationService.this = r1;
        }

        @Override // java.lang.Runnable
        public void run() {
            ApplicationMigrationService applicationMigrationService = ApplicationMigrationService.this;
            applicationMigrationService.notification = applicationMigrationService.initializeBackgroundNotification();
            PowerManager.WakeLock newWakeLock = ((PowerManager) ApplicationMigrationService.this.getSystemService("power")).newWakeLock(1, "signal:migration");
            try {
                newWakeLock.acquire();
                ApplicationMigrationService.this.setState(new ImportState(1, null));
                ApplicationMigrationService applicationMigrationService2 = ApplicationMigrationService.this;
                SmsMigrator.migrateDatabase(applicationMigrationService2, applicationMigrationService2);
                ApplicationMigrationService.this.setState(new ImportState(3, null));
                ApplicationMigrationService.setDatabaseImported(ApplicationMigrationService.this);
                ApplicationMigrationService.this.stopForeground(true);
                ApplicationMigrationService.this.notifyImportComplete();
                ApplicationMigrationService.this.stopSelf();
            } finally {
                newWakeLock.release();
            }
        }
    }

    /* loaded from: classes4.dex */
    public class ApplicationMigrationBinder extends Binder {
        public ApplicationMigrationBinder() {
            ApplicationMigrationService.this = r1;
        }

        public ApplicationMigrationService getService() {
            return ApplicationMigrationService.this;
        }
    }

    /* loaded from: classes4.dex */
    private static class CompletedReceiver extends BroadcastReceiver {
        private CompletedReceiver() {
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NotificationChannels.OTHER);
            builder.setSmallIcon(R.drawable.ic_notification);
            builder.setContentTitle(context.getString(R.string.ApplicationMigrationService_import_complete));
            builder.setContentText(context.getString(R.string.ApplicationMigrationService_system_database_import_is_complete));
            builder.setContentIntent(PendingIntent.getActivity(context, 0, MainActivity.clearTop(context), 0));
            builder.setWhen(System.currentTimeMillis());
            builder.setDefaults(2);
            builder.setAutoCancel(true);
            ((NotificationManager) context.getSystemService("notification")).notify(NotificationIds.SMS_IMPORT_COMPLETE, builder.build());
        }
    }

    /* loaded from: classes4.dex */
    public static class ImportState {
        public static final int STATE_IDLE;
        public static final int STATE_MIGRATING_BEGIN;
        public static final int STATE_MIGRATING_COMPLETE;
        public static final int STATE_MIGRATING_IN_PROGRESS;
        public SmsMigrator.ProgressDescription progress;
        public int state;

        public ImportState(int i, SmsMigrator.ProgressDescription progressDescription) {
            this.state = i;
            this.progress = progressDescription;
        }
    }

    public static boolean isDatabaseImported(Context context) {
        return context.getSharedPreferences(PREFERENCES_NAME, 0).getBoolean(DATABASE_MIGRATED, false);
    }

    public static void setDatabaseImported(Context context) {
        context.getSharedPreferences(PREFERENCES_NAME, 0).edit().putBoolean(DATABASE_MIGRATED, true).apply();
    }
}
