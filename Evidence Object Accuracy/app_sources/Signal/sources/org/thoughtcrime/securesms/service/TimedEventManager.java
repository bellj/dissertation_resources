package org.thoughtcrime.securesms.service;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import org.thoughtcrime.securesms.util.ServiceUtil;

/* loaded from: classes.dex */
public abstract class TimedEventManager<E> {
    private final Application application;
    private final Handler handler;

    protected abstract void executeEvent(E e);

    protected abstract long getDelayForEvent(E e);

    protected abstract E getNextClosestEvent();

    protected abstract void scheduleAlarm(Application application, long j);

    public TimedEventManager(Application application, String str) {
        HandlerThread handlerThread = new HandlerThread(str);
        handlerThread.start();
        this.application = application;
        this.handler = new Handler(handlerThread.getLooper());
    }

    public void scheduleIfNecessary() {
        this.handler.removeCallbacksAndMessages(null);
        this.handler.post(new Runnable() { // from class: org.thoughtcrime.securesms.service.TimedEventManager$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                TimedEventManager.this.lambda$scheduleIfNecessary$1();
            }
        });
    }

    public /* synthetic */ void lambda$scheduleIfNecessary$1() {
        E nextClosestEvent = getNextClosestEvent();
        if (nextClosestEvent != null) {
            long delayForEvent = getDelayForEvent(nextClosestEvent);
            this.handler.postDelayed(new Runnable(nextClosestEvent) { // from class: org.thoughtcrime.securesms.service.TimedEventManager$$ExternalSyntheticLambda0
                public final /* synthetic */ Object f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    TimedEventManager.this.lambda$scheduleIfNecessary$0(this.f$1);
                }
            }, delayForEvent);
            scheduleAlarm(this.application, delayForEvent);
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public /* synthetic */ void lambda$scheduleIfNecessary$0(Object obj) {
        executeEvent(obj);
        scheduleIfNecessary();
    }

    public static void setAlarm(Context context, long j, Class cls) {
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, new Intent(context, cls), 0);
        AlarmManager alarmManager = ServiceUtil.getAlarmManager(context);
        alarmManager.cancel(broadcast);
        alarmManager.set(0, System.currentTimeMillis() + j, broadcast);
    }
}
