package org.thoughtcrime.securesms.service;

import android.content.Context;
import android.content.Intent;
import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.RotateSignedPreKeyJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes.dex */
public class RotateSignedPreKeyListener extends PersistentAlarmManagerListener {
    private static final long INTERVAL = TimeUnit.DAYS.toMillis(2);

    @Override // org.thoughtcrime.securesms.service.PersistentAlarmManagerListener
    protected long getNextScheduledExecutionTime(Context context) {
        return TextSecurePreferences.getSignedPreKeyRotationTime(context);
    }

    @Override // org.thoughtcrime.securesms.service.PersistentAlarmManagerListener
    protected long onAlarm(Context context, long j) {
        if (j != 0 && SignalStore.account().isRegistered()) {
            ApplicationDependencies.getJobManager().add(new RotateSignedPreKeyJob());
        }
        long currentTimeMillis = System.currentTimeMillis() + INTERVAL;
        TextSecurePreferences.setSignedPreKeyRotationTime(context, currentTimeMillis);
        return currentTimeMillis;
    }

    public static void schedule(Context context) {
        new RotateSignedPreKeyListener().onReceive(context, new Intent());
    }
}
