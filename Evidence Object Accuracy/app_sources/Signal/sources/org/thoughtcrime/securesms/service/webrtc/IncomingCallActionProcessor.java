package org.thoughtcrime.securesms.service.webrtc;

import android.net.Uri;
import android.os.ResultReceiver;
import java.util.List;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.signal.ringrtc.CallException;
import org.signal.ringrtc.CallId;
import org.signal.ringrtc.CallManager;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.DoNotDisturbUtil;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.ringrtc.CallState;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.thoughtcrime.securesms.service.webrtc.state.CallSetupState;
import org.thoughtcrime.securesms.service.webrtc.state.VideoState;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceState;
import org.thoughtcrime.securesms.util.NetworkUtil;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.webrtc.locks.LockManager;
import org.webrtc.PeerConnection;

/* loaded from: classes4.dex */
public class IncomingCallActionProcessor extends DeviceAwareActionProcessor {
    private static final String TAG = Log.tag(IncomingCallActionProcessor.class);
    private final ActiveCallActionProcessorDelegate activeCallDelegate;
    private final CallSetupActionProcessorDelegate callSetupDelegate;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public IncomingCallActionProcessor(org.thoughtcrime.securesms.service.webrtc.WebRtcInteractor r3) {
        /*
            r2 = this;
            java.lang.String r0 = org.thoughtcrime.securesms.service.webrtc.IncomingCallActionProcessor.TAG
            r2.<init>(r3, r0)
            org.thoughtcrime.securesms.service.webrtc.ActiveCallActionProcessorDelegate r1 = new org.thoughtcrime.securesms.service.webrtc.ActiveCallActionProcessorDelegate
            r1.<init>(r3, r0)
            r2.activeCallDelegate = r1
            org.thoughtcrime.securesms.service.webrtc.CallSetupActionProcessorDelegate r1 = new org.thoughtcrime.securesms.service.webrtc.CallSetupActionProcessorDelegate
            r1.<init>(r3, r0)
            r2.callSetupDelegate = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.service.webrtc.IncomingCallActionProcessor.<init>(org.thoughtcrime.securesms.service.webrtc.WebRtcInteractor):void");
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleIsInCallQuery(WebRtcServiceState webRtcServiceState, ResultReceiver resultReceiver) {
        return this.activeCallDelegate.handleIsInCallQuery(webRtcServiceState, resultReceiver);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleTurnServerUpdate(WebRtcServiceState webRtcServiceState, List<PeerConnection.IceServer> list, boolean z) {
        RemotePeer requireActivePeer = webRtcServiceState.getCallInfoState().requireActivePeer();
        String str = TAG;
        Log.i(str, "handleTurnServerUpdate(): call_id: " + requireActivePeer.getCallId());
        return proceed(webRtcServiceState.builder().changeCallSetupState(requireActivePeer.getCallId()).iceServers(list).alwaysTurn(z).build());
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleSetTelecomApproved(WebRtcServiceState webRtcServiceState, long j, RecipientId recipientId) {
        return proceed(super.handleSetTelecomApproved(webRtcServiceState, j, recipientId));
    }

    private WebRtcServiceState proceed(WebRtcServiceState webRtcServiceState) {
        RemotePeer requireActivePeer = webRtcServiceState.getCallInfoState().requireActivePeer();
        CallSetupState callSetupState = webRtcServiceState.getCallSetupState(requireActivePeer.getCallId());
        if (callSetupState.getIceServers().isEmpty() || (callSetupState.shouldWaitForTelecomApproval() && !callSetupState.isTelecomApproved())) {
            String str = TAG;
            Log.i(str, "Unable to proceed without ice server and telecom approval iceServers: " + Util.hasItems(callSetupState.getIceServers()) + " waitForTelecom: " + callSetupState.shouldWaitForTelecomApproval() + " telecomApproved: " + callSetupState.isTelecomApproved());
            return webRtcServiceState;
        }
        boolean z = !requireActivePeer.getRecipient().isSystemContact() || callSetupState.isAlwaysTurnServers();
        VideoState videoState = webRtcServiceState.getVideoState();
        CallParticipant remoteCallParticipant = webRtcServiceState.getCallInfoState().getRemoteCallParticipant(requireActivePeer.getRecipient());
        Objects.requireNonNull(remoteCallParticipant);
        try {
            this.webRtcInteractor.getCallManager().proceed(requireActivePeer.getCallId(), this.context, videoState.getLockableEglBase().require(), RingRtcDynamicConfiguration.getAudioProcessingMethod(), videoState.requireLocalSink(), remoteCallParticipant.getVideoSink(), videoState.requireCamera(), callSetupState.getIceServers(), z, NetworkUtil.getCallingBandwidthMode(this.context), 200, false);
            this.webRtcInteractor.updatePhoneState(LockManager.PhoneState.PROCESSING);
            this.webRtcInteractor.postStateUpdate(webRtcServiceState);
            return webRtcServiceState;
        } catch (CallException e) {
            return callFailure(webRtcServiceState, "Unable to proceed with call: ", e);
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleDropCall(WebRtcServiceState webRtcServiceState, long j) {
        return this.callSetupDelegate.handleDropCall(webRtcServiceState, j);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleAcceptCall(WebRtcServiceState webRtcServiceState, boolean z) {
        RemotePeer requireActivePeer = webRtcServiceState.getCallInfoState().requireActivePeer();
        String str = TAG;
        Log.i(str, "handleAcceptCall(): call_id: " + requireActivePeer.getCallId());
        SignalDatabase.sms().insertReceivedCall(requireActivePeer.getId(), webRtcServiceState.getCallSetupState(requireActivePeer).isRemoteVideoOffer());
        WebRtcServiceState build = webRtcServiceState.builder().changeCallSetupState(requireActivePeer.getCallId()).acceptWithVideo(z).build();
        try {
            this.webRtcInteractor.getCallManager().acceptCall(requireActivePeer.getCallId());
            return build;
        } catch (CallException e) {
            return callFailure(build, "accept() failed: ", e);
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleDenyCall(WebRtcServiceState webRtcServiceState) {
        RemotePeer requireActivePeer = webRtcServiceState.getCallInfoState().requireActivePeer();
        if (requireActivePeer.getState() != CallState.LOCAL_RINGING) {
            Log.w(TAG, "Can only deny from ringing!");
            return webRtcServiceState;
        }
        Log.i(TAG, "handleDenyCall():");
        try {
            this.webRtcInteractor.rejectIncomingCall(requireActivePeer.getId());
            this.webRtcInteractor.getCallManager().hangup();
            SignalDatabase.sms().insertMissedCall(requireActivePeer.getId(), System.currentTimeMillis(), webRtcServiceState.getCallSetupState(requireActivePeer).isRemoteVideoOffer());
            return terminate(webRtcServiceState, requireActivePeer);
        } catch (CallException e) {
            return callFailure(webRtcServiceState, "hangup() failed: ", e);
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleLocalRinging(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer) {
        String str = TAG;
        Log.i(str, "handleLocalRinging(): call_id: " + remotePeer.getCallId());
        RemotePeer requireActivePeer = webRtcServiceState.getCallInfoState().requireActivePeer();
        Recipient recipient = remotePeer.getRecipient();
        requireActivePeer.localRinging();
        this.webRtcInteractor.updatePhoneState(LockManager.PhoneState.INTERACTIVE);
        boolean shouldDisturbUserWithCall = DoNotDisturbUtil.shouldDisturbUserWithCall(this.context.getApplicationContext(), recipient);
        if (shouldDisturbUserWithCall && !this.webRtcInteractor.startWebRtcCallActivityIfPossible()) {
            Log.i(str, "Unable to start call activity due to OS version or not being in the foreground");
            ApplicationDependencies.getAppForegroundObserver().addListener(this.webRtcInteractor.getForegroundListener());
        }
        if (shouldDisturbUserWithCall && SignalStore.settings().isCallNotificationsEnabled()) {
            Uri callRingtone = recipient.resolve().getCallRingtone();
            RecipientDatabase.VibrateState callVibrate = recipient.resolve().getCallVibrate();
            if (callRingtone == null) {
                callRingtone = SignalStore.settings().getCallRingtone();
            }
            this.webRtcInteractor.startIncomingRinger(callRingtone, callVibrate == RecipientDatabase.VibrateState.ENABLED || (callVibrate == RecipientDatabase.VibrateState.DEFAULT && SignalStore.settings().isCallVibrateEnabled()));
        }
        this.webRtcInteractor.setCallInProgressNotification(1, requireActivePeer);
        this.webRtcInteractor.registerPowerButtonReceiver();
        return webRtcServiceState.builder().changeCallInfoState().callState(WebRtcViewModel.State.CALL_INCOMING).build();
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleScreenOffChange(WebRtcServiceState webRtcServiceState) {
        Log.i(TAG, "Silencing incoming ringer...");
        this.webRtcInteractor.silenceIncomingRinger();
        return webRtcServiceState;
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleRemoteVideoEnable(WebRtcServiceState webRtcServiceState, boolean z) {
        return this.activeCallDelegate.handleRemoteVideoEnable(webRtcServiceState, z);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleScreenSharingEnable(WebRtcServiceState webRtcServiceState, boolean z) {
        return this.activeCallDelegate.handleScreenSharingEnable(webRtcServiceState, z);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleReceivedOfferWhileActive(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer) {
        return this.activeCallDelegate.handleReceivedOfferWhileActive(webRtcServiceState, remotePeer);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleEndedRemote(WebRtcServiceState webRtcServiceState, CallManager.CallEvent callEvent, RemotePeer remotePeer) {
        return this.activeCallDelegate.handleEndedRemote(webRtcServiceState, callEvent, remotePeer);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleEnded(WebRtcServiceState webRtcServiceState, CallManager.CallEvent callEvent, RemotePeer remotePeer) {
        return this.activeCallDelegate.handleEnded(webRtcServiceState, callEvent, remotePeer);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleSetupFailure(WebRtcServiceState webRtcServiceState, CallId callId) {
        return this.activeCallDelegate.handleSetupFailure(webRtcServiceState, callId);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleCallConnected(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer) {
        return this.callSetupDelegate.handleCallConnected(webRtcServiceState, remotePeer);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleSetEnableVideo(WebRtcServiceState webRtcServiceState, boolean z) {
        return this.callSetupDelegate.handleSetEnableVideo(webRtcServiceState, z);
    }
}
