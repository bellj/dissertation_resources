package org.thoughtcrime.securesms.service.webrtc;

import java.util.UUID;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.signal.ringrtc.CallManager;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: GroupCallRingCheckInfo.kt */
@Metadata(d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\t\u0010\u0017\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0019\u001a\u00020\u0007HÆ\u0003J\t\u0010\u001a\u001a\u00020\tHÆ\u0003J\t\u0010\u001b\u001a\u00020\u000bHÆ\u0003J;\u0010\u001c\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u000bHÆ\u0001J\u0013\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010 \u001a\u00020!HÖ\u0001J\t\u0010\"\u001a\u00020#HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016¨\u0006$"}, d2 = {"Lorg/thoughtcrime/securesms/service/webrtc/GroupCallRingCheckInfo;", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "groupId", "Lorg/thoughtcrime/securesms/groups/GroupId$V2;", "ringId", "", "ringerUuid", "Ljava/util/UUID;", "ringUpdate", "Lorg/signal/ringrtc/CallManager$RingUpdate;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/groups/GroupId$V2;JLjava/util/UUID;Lorg/signal/ringrtc/CallManager$RingUpdate;)V", "getGroupId", "()Lorg/thoughtcrime/securesms/groups/GroupId$V2;", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getRingId", "()J", "getRingUpdate", "()Lorg/signal/ringrtc/CallManager$RingUpdate;", "getRingerUuid", "()Ljava/util/UUID;", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class GroupCallRingCheckInfo {
    private final GroupId.V2 groupId;
    private final RecipientId recipientId;
    private final long ringId;
    private final CallManager.RingUpdate ringUpdate;
    private final UUID ringerUuid;

    public static /* synthetic */ GroupCallRingCheckInfo copy$default(GroupCallRingCheckInfo groupCallRingCheckInfo, RecipientId recipientId, GroupId.V2 v2, long j, UUID uuid, CallManager.RingUpdate ringUpdate, int i, Object obj) {
        if ((i & 1) != 0) {
            recipientId = groupCallRingCheckInfo.recipientId;
        }
        if ((i & 2) != 0) {
            v2 = groupCallRingCheckInfo.groupId;
        }
        if ((i & 4) != 0) {
            j = groupCallRingCheckInfo.ringId;
        }
        if ((i & 8) != 0) {
            uuid = groupCallRingCheckInfo.ringerUuid;
        }
        if ((i & 16) != 0) {
            ringUpdate = groupCallRingCheckInfo.ringUpdate;
        }
        return groupCallRingCheckInfo.copy(recipientId, v2, j, uuid, ringUpdate);
    }

    public final RecipientId component1() {
        return this.recipientId;
    }

    public final GroupId.V2 component2() {
        return this.groupId;
    }

    public final long component3() {
        return this.ringId;
    }

    public final UUID component4() {
        return this.ringerUuid;
    }

    public final CallManager.RingUpdate component5() {
        return this.ringUpdate;
    }

    public final GroupCallRingCheckInfo copy(RecipientId recipientId, GroupId.V2 v2, long j, UUID uuid, CallManager.RingUpdate ringUpdate) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(v2, "groupId");
        Intrinsics.checkNotNullParameter(uuid, "ringerUuid");
        Intrinsics.checkNotNullParameter(ringUpdate, "ringUpdate");
        return new GroupCallRingCheckInfo(recipientId, v2, j, uuid, ringUpdate);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GroupCallRingCheckInfo)) {
            return false;
        }
        GroupCallRingCheckInfo groupCallRingCheckInfo = (GroupCallRingCheckInfo) obj;
        return Intrinsics.areEqual(this.recipientId, groupCallRingCheckInfo.recipientId) && Intrinsics.areEqual(this.groupId, groupCallRingCheckInfo.groupId) && this.ringId == groupCallRingCheckInfo.ringId && Intrinsics.areEqual(this.ringerUuid, groupCallRingCheckInfo.ringerUuid) && this.ringUpdate == groupCallRingCheckInfo.ringUpdate;
    }

    public int hashCode() {
        return (((((((this.recipientId.hashCode() * 31) + this.groupId.hashCode()) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.ringId)) * 31) + this.ringerUuid.hashCode()) * 31) + this.ringUpdate.hashCode();
    }

    public String toString() {
        return "GroupCallRingCheckInfo(recipientId=" + this.recipientId + ", groupId=" + this.groupId + ", ringId=" + this.ringId + ", ringerUuid=" + this.ringerUuid + ", ringUpdate=" + this.ringUpdate + ')';
    }

    public GroupCallRingCheckInfo(RecipientId recipientId, GroupId.V2 v2, long j, UUID uuid, CallManager.RingUpdate ringUpdate) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(v2, "groupId");
        Intrinsics.checkNotNullParameter(uuid, "ringerUuid");
        Intrinsics.checkNotNullParameter(ringUpdate, "ringUpdate");
        this.recipientId = recipientId;
        this.groupId = v2;
        this.ringId = j;
        this.ringerUuid = uuid;
        this.ringUpdate = ringUpdate;
    }

    public final RecipientId getRecipientId() {
        return this.recipientId;
    }

    public final GroupId.V2 getGroupId() {
        return this.groupId;
    }

    public final long getRingId() {
        return this.ringId;
    }

    public final UUID getRingerUuid() {
        return this.ringerUuid;
    }

    public final CallManager.RingUpdate getRingUpdate() {
        return this.ringUpdate;
    }
}
