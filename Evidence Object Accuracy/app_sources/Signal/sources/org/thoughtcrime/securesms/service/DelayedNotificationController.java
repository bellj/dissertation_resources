package org.thoughtcrime.securesms.service;

import android.os.Handler;
import android.os.Looper;
import java.util.Objects;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public abstract class DelayedNotificationController implements AutoCloseable {
    public static final long DO_NOT_SHOW;
    public static final long SHOW_WITHOUT_DELAY;
    private static final String TAG = Log.tag(DelayedNotificationController.class);

    /* loaded from: classes4.dex */
    public interface Create {
        NotificationController create();
    }

    @Override // java.lang.AutoCloseable
    public void close() {
    }

    public abstract NotificationController showNow();

    private DelayedNotificationController() {
    }

    public static DelayedNotificationController create(long j, Create create) {
        if (j == 0) {
            return new Shown(create.create());
        }
        if (j == -1) {
            return new NoShow();
        }
        if (j > 0) {
            return new DelayedShow(j, create);
        }
        throw new IllegalArgumentException("Illegal delay " + j);
    }

    /* loaded from: classes4.dex */
    public static final class NoShow extends DelayedNotificationController {
        @Override // org.thoughtcrime.securesms.service.DelayedNotificationController
        public NotificationController showNow() {
            return null;
        }

        private NoShow() {
            super();
        }
    }

    /* loaded from: classes4.dex */
    public static final class Shown extends DelayedNotificationController {
        private final NotificationController controller;

        Shown(NotificationController notificationController) {
            super();
            this.controller = notificationController;
        }

        @Override // org.thoughtcrime.securesms.service.DelayedNotificationController, java.lang.AutoCloseable
        public void close() {
            this.controller.close();
        }

        @Override // org.thoughtcrime.securesms.service.DelayedNotificationController
        public NotificationController showNow() {
            return this.controller;
        }
    }

    /* loaded from: classes4.dex */
    public static final class DelayedShow extends DelayedNotificationController {
        private final Create createTask;
        private final Handler handler;
        private boolean isClosed;
        private NotificationController notificationController;
        private final Runnable start;

        private DelayedShow(long j, Create create) {
            super();
            this.createTask = create;
            Handler handler = new Handler(Looper.getMainLooper());
            this.handler = handler;
            DelayedNotificationController$DelayedShow$$ExternalSyntheticLambda0 delayedNotificationController$DelayedShow$$ExternalSyntheticLambda0 = new DelayedNotificationController$DelayedShow$$ExternalSyntheticLambda0(this);
            this.start = delayedNotificationController$DelayedShow$$ExternalSyntheticLambda0;
            handler.postDelayed(delayedNotificationController$DelayedShow$$ExternalSyntheticLambda0, j);
        }

        public void start() {
            SignalExecutors.BOUNDED.execute(new DelayedNotificationController$DelayedShow$$ExternalSyntheticLambda1(this));
        }

        @Override // org.thoughtcrime.securesms.service.DelayedNotificationController
        public synchronized NotificationController showNow() {
            NotificationController showNowInner;
            if (!this.isClosed) {
                showNowInner = showNowInner();
                Objects.requireNonNull(showNowInner);
                NotificationController notificationController = showNowInner;
            } else {
                throw new AssertionError("showNow called after close");
            }
            return showNowInner;
        }

        public synchronized NotificationController showNowInner() {
            NotificationController notificationController = this.notificationController;
            if (notificationController != null) {
                return notificationController;
            }
            if (!this.isClosed) {
                Log.i(DelayedNotificationController.TAG, "Starting foreground service");
                NotificationController create = this.createTask.create();
                this.notificationController = create;
                return create;
            }
            Log.i(DelayedNotificationController.TAG, "Did not start foreground service as close has been called");
            return null;
        }

        @Override // org.thoughtcrime.securesms.service.DelayedNotificationController, java.lang.AutoCloseable
        public synchronized void close() {
            this.handler.removeCallbacks(this.start);
            this.isClosed = true;
            if (this.notificationController != null) {
                Log.d(DelayedNotificationController.TAG, "Closing");
                this.notificationController.close();
            } else {
                Log.d(DelayedNotificationController.TAG, "Never showed");
            }
        }
    }
}
