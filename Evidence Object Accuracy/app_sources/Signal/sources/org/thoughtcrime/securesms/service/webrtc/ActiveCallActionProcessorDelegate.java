package org.thoughtcrime.securesms.service.webrtc;

import android.os.ResultReceiver;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.signal.ringrtc.CallException;
import org.signal.ringrtc.CallId;
import org.signal.ringrtc.CallManager;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.ringrtc.CallState;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceState;
import org.thoughtcrime.securesms.webrtc.audio.OutgoingRinger;

/* loaded from: classes4.dex */
public class ActiveCallActionProcessorDelegate extends WebRtcActionProcessor {
    private static final Map<CallManager.CallEvent, WebRtcViewModel.State> ENDED_REMOTE_EVENT_TO_STATE = new HashMap<CallManager.CallEvent, WebRtcViewModel.State>() { // from class: org.thoughtcrime.securesms.service.webrtc.ActiveCallActionProcessorDelegate.1
        {
            put(CallManager.CallEvent.ENDED_REMOTE_HANGUP_ACCEPTED, WebRtcViewModel.State.CALL_ACCEPTED_ELSEWHERE);
            put(CallManager.CallEvent.ENDED_REMOTE_HANGUP_BUSY, WebRtcViewModel.State.CALL_ONGOING_ELSEWHERE);
            put(CallManager.CallEvent.ENDED_REMOTE_HANGUP_DECLINED, WebRtcViewModel.State.CALL_DECLINED_ELSEWHERE);
            put(CallManager.CallEvent.ENDED_REMOTE_BUSY, WebRtcViewModel.State.CALL_BUSY);
            put(CallManager.CallEvent.ENDED_REMOTE_HANGUP_NEED_PERMISSION, WebRtcViewModel.State.CALL_NEEDS_PERMISSION);
            CallManager.CallEvent callEvent = CallManager.CallEvent.ENDED_REMOTE_GLARE;
            WebRtcViewModel.State state = WebRtcViewModel.State.CALL_DISCONNECTED_GLARE;
            put(callEvent, state);
            put(CallManager.CallEvent.ENDED_REMOTE_RECALL, state);
        }
    };

    public ActiveCallActionProcessorDelegate(WebRtcInteractor webRtcInteractor, String str) {
        super(webRtcInteractor, str);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleIsInCallQuery(WebRtcServiceState webRtcServiceState, ResultReceiver resultReceiver) {
        if (resultReceiver != null) {
            resultReceiver.send(1, null);
        }
        return webRtcServiceState;
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleRemoteVideoEnable(WebRtcServiceState webRtcServiceState, boolean z) {
        RemotePeer requireActivePeer = webRtcServiceState.getCallInfoState().requireActivePeer();
        String str = this.tag;
        Log.i(str, "handleRemoteVideoEnable(): call_id: " + requireActivePeer.getCallId());
        CallParticipant remoteCallParticipant = webRtcServiceState.getCallInfoState().getRemoteCallParticipant(requireActivePeer.getRecipient());
        Objects.requireNonNull(remoteCallParticipant);
        return webRtcServiceState.builder().changeCallInfoState().putParticipant(requireActivePeer.getRecipient(), remoteCallParticipant.withVideoEnabled(z)).build();
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleScreenSharingEnable(WebRtcServiceState webRtcServiceState, boolean z) {
        RemotePeer requireActivePeer = webRtcServiceState.getCallInfoState().requireActivePeer();
        String str = this.tag;
        Log.i(str, "handleScreenSharingEnable(): call_id: " + requireActivePeer.getCallId() + " enable: " + z);
        CallParticipant remoteCallParticipant = webRtcServiceState.getCallInfoState().getRemoteCallParticipant(requireActivePeer.getRecipient());
        Objects.requireNonNull(remoteCallParticipant);
        return webRtcServiceState.builder().changeCallInfoState().putParticipant(requireActivePeer.getRecipient(), remoteCallParticipant.withScreenSharingEnabled(z)).build();
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleLocalHangup(WebRtcServiceState webRtcServiceState) {
        RemotePeer activePeer = webRtcServiceState.getCallInfoState().getActivePeer();
        if (activePeer == null) {
            Log.i(this.tag, "handleLocalHangup(): no active peer");
        } else {
            String str = this.tag;
            Log.i(str, "handleLocalHangup(): call_id: " + activePeer.getCallId());
        }
        ApplicationDependencies.getSignalServiceAccountManager().cancelInFlightRequests();
        ApplicationDependencies.getSignalServiceMessageSender().cancelInFlightRequests();
        try {
            this.webRtcInteractor.getCallManager().hangup();
            WebRtcServiceState build = webRtcServiceState.builder().changeCallInfoState().callState(WebRtcViewModel.State.CALL_DISCONNECTED).build();
            this.webRtcInteractor.postStateUpdate(build);
            return terminate(build, activePeer);
        } catch (CallException e) {
            return callFailure(webRtcServiceState, "hangup() failed: ", e);
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleReceivedOfferWhileActive(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer) {
        RemotePeer requireActivePeer = webRtcServiceState.getCallInfoState().requireActivePeer();
        String str = this.tag;
        Log.i(str, "handleReceivedOfferWhileActive(): call_id: " + remotePeer.getCallId());
        switch (AnonymousClass2.$SwitchMap$org$thoughtcrime$securesms$ringrtc$CallState[requireActivePeer.getState().ordinal()]) {
            case 1:
            case 2:
                this.webRtcInteractor.setCallInProgressNotification(2, requireActivePeer);
                break;
            case 3:
            case 4:
                this.webRtcInteractor.setCallInProgressNotification(4, requireActivePeer);
                break;
            case 5:
                this.webRtcInteractor.setCallInProgressNotification(1, requireActivePeer);
                break;
            case 6:
                this.webRtcInteractor.setCallInProgressNotification(3, requireActivePeer);
                break;
            default:
                throw new IllegalStateException();
        }
        if (requireActivePeer.getState() == CallState.IDLE) {
            this.webRtcInteractor.stopForegroundService();
        }
        this.webRtcInteractor.insertMissedCall(remotePeer, remotePeer.getCallStartTimestamp(), webRtcServiceState.getCallSetupState(remotePeer).isRemoteVideoOffer());
        return terminate(webRtcServiceState, remotePeer);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: org.thoughtcrime.securesms.service.webrtc.ActiveCallActionProcessorDelegate$2 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$ringrtc$CallState;

        static {
            int[] iArr = new int[CallState.values().length];
            $SwitchMap$org$thoughtcrime$securesms$ringrtc$CallState = iArr;
            try {
                iArr[CallState.DIALING.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$ringrtc$CallState[CallState.REMOTE_RINGING.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$ringrtc$CallState[CallState.IDLE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$ringrtc$CallState[CallState.ANSWERING.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$ringrtc$CallState[CallState.LOCAL_RINGING.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$ringrtc$CallState[CallState.CONNECTED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleEndedRemote(WebRtcServiceState webRtcServiceState, CallManager.CallEvent callEvent, RemotePeer remotePeer) {
        String str = this.tag;
        Log.i(str, "handleEndedRemote(): call_id: " + remotePeer.getCallId() + " action: " + callEvent);
        WebRtcViewModel.State callState = webRtcServiceState.getCallInfoState().getCallState();
        RemotePeer activePeer = webRtcServiceState.getCallInfoState().getActivePeer();
        boolean callIdEquals = remotePeer.callIdEquals(activePeer);
        boolean z = false;
        boolean z2 = remotePeer.getState() == CallState.DIALING || remotePeer.getState() == CallState.REMOTE_RINGING;
        if (remotePeer.getState() == CallState.ANSWERING || remotePeer.getState() == CallState.LOCAL_RINGING) {
            z = true;
        }
        if (callIdEquals) {
            Map<CallManager.CallEvent, WebRtcViewModel.State> map = ENDED_REMOTE_EVENT_TO_STATE;
            if (map.containsKey(callEvent)) {
                callState = map.get(callEvent);
                Objects.requireNonNull(callState);
            }
        }
        if (callEvent == CallManager.CallEvent.ENDED_REMOTE_HANGUP) {
            if (callIdEquals) {
                callState = z2 ? WebRtcViewModel.State.RECIPIENT_UNAVAILABLE : WebRtcViewModel.State.CALL_DISCONNECTED;
            }
            if (z) {
                this.webRtcInteractor.insertMissedCall(remotePeer, remotePeer.getCallStartTimestamp(), webRtcServiceState.getCallSetupState(remotePeer).isRemoteVideoOffer());
            }
        } else if (callEvent == CallManager.CallEvent.ENDED_REMOTE_BUSY && callIdEquals) {
            activePeer.receivedBusy();
            OutgoingRinger outgoingRinger = new OutgoingRinger(this.context);
            outgoingRinger.start(OutgoingRinger.Type.BUSY);
            ThreadUtil.runOnMainDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.service.webrtc.ActiveCallActionProcessorDelegate$$ExternalSyntheticLambda0
                @Override // java.lang.Runnable
                public final void run() {
                    OutgoingRinger.this.stop();
                }
            }, 2000);
        } else if (callEvent == CallManager.CallEvent.ENDED_REMOTE_GLARE && z) {
            this.webRtcInteractor.insertMissedCall(remotePeer, remotePeer.getCallStartTimestamp(), webRtcServiceState.getCallSetupState(remotePeer).isRemoteVideoOffer());
        } else if (callEvent == CallManager.CallEvent.ENDED_REMOTE_RECALL && z) {
            this.webRtcInteractor.insertMissedCall(remotePeer, remotePeer.getCallStartTimestamp(), webRtcServiceState.getCallSetupState(remotePeer).isRemoteVideoOffer());
        }
        if (callState == WebRtcViewModel.State.CALL_ACCEPTED_ELSEWHERE) {
            this.webRtcInteractor.insertReceivedCall(remotePeer, webRtcServiceState.getCallSetupState(remotePeer).isRemoteVideoOffer());
        }
        WebRtcServiceState build = webRtcServiceState.builder().changeCallInfoState().callState(callState).build();
        this.webRtcInteractor.postStateUpdate(build);
        return terminate(build, remotePeer);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleEnded(WebRtcServiceState webRtcServiceState, CallManager.CallEvent callEvent, RemotePeer remotePeer) {
        String str = this.tag;
        Log.i(str, "handleEnded(): call_id: " + remotePeer.getCallId() + " action: " + callEvent);
        if (remotePeer.callIdEquals(webRtcServiceState.getCallInfoState().getActivePeer()) && !webRtcServiceState.getCallInfoState().getCallState().isErrorState()) {
            webRtcServiceState = webRtcServiceState.builder().changeCallInfoState().callState(callEvent == CallManager.CallEvent.ENDED_TIMEOUT ? WebRtcViewModel.State.RECIPIENT_UNAVAILABLE : WebRtcViewModel.State.NETWORK_FAILURE).build();
            this.webRtcInteractor.postStateUpdate(webRtcServiceState);
        }
        if (remotePeer.getState() == CallState.ANSWERING || remotePeer.getState() == CallState.LOCAL_RINGING) {
            this.webRtcInteractor.insertMissedCall(remotePeer, remotePeer.getCallStartTimestamp(), webRtcServiceState.getCallSetupState(remotePeer).isRemoteVideoOffer());
        }
        return terminate(webRtcServiceState, remotePeer);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleSetupFailure(WebRtcServiceState webRtcServiceState, CallId callId) {
        String str = this.tag;
        Log.i(str, "handleSetupFailure(): call_id: " + callId);
        RemotePeer activePeer = webRtcServiceState.getCallInfoState().getActivePeer();
        if (activePeer == null || !activePeer.getCallId().equals(callId)) {
            RemotePeer peerByCallId = webRtcServiceState.getCallInfoState().getPeerByCallId(callId);
            if (peerByCallId != null) {
                this.webRtcInteractor.terminateCall(peerByCallId.getId());
            }
            return webRtcServiceState;
        }
        try {
            if (!(activePeer.getState() == CallState.DIALING || activePeer.getState() == CallState.REMOTE_RINGING)) {
                this.webRtcInteractor.getCallManager().drop(callId);
                WebRtcServiceState build = webRtcServiceState.builder().changeCallInfoState().callState(WebRtcViewModel.State.NETWORK_FAILURE).build();
                this.webRtcInteractor.postStateUpdate(build);
                if (activePeer.getState() != CallState.ANSWERING || activePeer.getState() == CallState.LOCAL_RINGING) {
                    this.webRtcInteractor.insertMissedCall(activePeer, activePeer.getCallStartTimestamp(), build.getCallSetupState(activePeer).isRemoteVideoOffer());
                }
                return terminate(build, activePeer);
            }
            this.webRtcInteractor.getCallManager().hangup();
            WebRtcServiceState build = webRtcServiceState.builder().changeCallInfoState().callState(WebRtcViewModel.State.NETWORK_FAILURE).build();
            this.webRtcInteractor.postStateUpdate(build);
            if (activePeer.getState() != CallState.ANSWERING) {
            }
            this.webRtcInteractor.insertMissedCall(activePeer, activePeer.getCallStartTimestamp(), build.getCallSetupState(activePeer).isRemoteVideoOffer());
            return terminate(build, activePeer);
        } catch (CallException e) {
            return callFailure(webRtcServiceState, "Unable to drop call due to setup failure", e);
        }
    }
}
