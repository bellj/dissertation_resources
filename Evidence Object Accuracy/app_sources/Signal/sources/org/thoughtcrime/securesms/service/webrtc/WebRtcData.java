package org.thoughtcrime.securesms.service.webrtc;

import java.util.UUID;
import org.signal.ringrtc.CallId;
import org.signal.ringrtc.CallManager;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.whispersystems.signalservice.api.messages.calls.HangupMessage;
import org.whispersystems.signalservice.api.messages.calls.OfferMessage;

/* loaded from: classes4.dex */
public class WebRtcData {

    /* loaded from: classes4.dex */
    public static class CallMetadata {
        private final int remoteDevice;
        private final RemotePeer remotePeer;

        public CallMetadata(RemotePeer remotePeer, int i) {
            this.remotePeer = remotePeer;
            this.remoteDevice = i;
        }

        public RemotePeer getRemotePeer() {
            return this.remotePeer;
        }

        public CallId getCallId() {
            return this.remotePeer.getCallId();
        }

        public int getRemoteDevice() {
            return this.remoteDevice;
        }
    }

    /* loaded from: classes4.dex */
    public static class OfferMetadata {
        private final OfferMessage.Type offerType;
        private final byte[] opaque;
        private final String sdp;

        public OfferMetadata(byte[] bArr, String str, OfferMessage.Type type) {
            this.opaque = bArr;
            this.sdp = str;
            this.offerType = type;
        }

        public byte[] getOpaque() {
            return this.opaque;
        }

        public String getSdp() {
            return this.sdp;
        }

        public OfferMessage.Type getOfferType() {
            return this.offerType;
        }
    }

    /* loaded from: classes4.dex */
    public static class ReceivedOfferMetadata {
        private final boolean isMultiRing;
        private final byte[] remoteIdentityKey;
        private final long serverDeliveredTimestamp;
        private final long serverReceivedTimestamp;

        public ReceivedOfferMetadata(byte[] bArr, long j, long j2, boolean z) {
            this.remoteIdentityKey = bArr;
            this.serverReceivedTimestamp = j;
            this.serverDeliveredTimestamp = j2;
            this.isMultiRing = z;
        }

        public byte[] getRemoteIdentityKey() {
            return this.remoteIdentityKey;
        }

        public long getServerReceivedTimestamp() {
            return this.serverReceivedTimestamp;
        }

        public long getServerDeliveredTimestamp() {
            return this.serverDeliveredTimestamp;
        }

        boolean isMultiRing() {
            return this.isMultiRing;
        }
    }

    /* loaded from: classes4.dex */
    public static class AnswerMetadata {
        private final byte[] opaque;
        private final String sdp;

        public AnswerMetadata(byte[] bArr, String str) {
            this.opaque = bArr;
            this.sdp = str;
        }

        public byte[] getOpaque() {
            return this.opaque;
        }

        public String getSdp() {
            return this.sdp;
        }
    }

    /* loaded from: classes4.dex */
    public static class ReceivedAnswerMetadata {
        private final boolean isMultiRing;
        private final byte[] remoteIdentityKey;

        public ReceivedAnswerMetadata(byte[] bArr, boolean z) {
            this.remoteIdentityKey = bArr;
            this.isMultiRing = z;
        }

        public byte[] getRemoteIdentityKey() {
            return this.remoteIdentityKey;
        }

        boolean isMultiRing() {
            return this.isMultiRing;
        }
    }

    /* loaded from: classes4.dex */
    public static class HangupMetadata {
        private final int deviceId;
        private final boolean isLegacy;
        private final HangupMessage.Type type;

        public static HangupMetadata fromType(HangupMessage.Type type) {
            return new HangupMetadata(type, true, 0);
        }

        public HangupMetadata(HangupMessage.Type type, boolean z, int i) {
            this.type = type;
            this.isLegacy = z;
            this.deviceId = i;
        }

        public HangupMessage.Type getType() {
            return this.type;
        }

        public CallManager.HangupType getCallHangupType() {
            int i = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$api$messages$calls$HangupMessage$Type[this.type.ordinal()];
            if (i == 1) {
                return CallManager.HangupType.ACCEPTED;
            }
            if (i == 2) {
                return CallManager.HangupType.BUSY;
            }
            if (i == 3) {
                return CallManager.HangupType.NORMAL;
            }
            if (i == 4) {
                return CallManager.HangupType.DECLINED;
            }
            if (i == 5) {
                return CallManager.HangupType.NEED_PERMISSION;
            }
            throw new IllegalArgumentException("Unexpected hangup type: " + this.type);
        }

        public boolean isLegacy() {
            return this.isLegacy;
        }

        public int getDeviceId() {
            return this.deviceId;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: org.thoughtcrime.securesms.service.webrtc.WebRtcData$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$api$messages$calls$HangupMessage$Type;

        static {
            int[] iArr = new int[HangupMessage.Type.values().length];
            $SwitchMap$org$whispersystems$signalservice$api$messages$calls$HangupMessage$Type = iArr;
            try {
                iArr[HangupMessage.Type.ACCEPTED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$calls$HangupMessage$Type[HangupMessage.Type.BUSY.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$calls$HangupMessage$Type[HangupMessage.Type.NORMAL.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$calls$HangupMessage$Type[HangupMessage.Type.DECLINED.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$calls$HangupMessage$Type[HangupMessage.Type.NEED_PERMISSION.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
        }
    }

    /* loaded from: classes4.dex */
    public static class OpaqueMessageMetadata {
        private final long messageAgeSeconds;
        private final byte[] opaque;
        private final int remoteDeviceId;
        private final UUID uuid;

        public OpaqueMessageMetadata(UUID uuid, byte[] bArr, int i, long j) {
            this.uuid = uuid;
            this.opaque = bArr;
            this.remoteDeviceId = i;
            this.messageAgeSeconds = j;
        }

        public UUID getUuid() {
            return this.uuid;
        }

        public byte[] getOpaque() {
            return this.opaque;
        }

        public int getRemoteDeviceId() {
            return this.remoteDeviceId;
        }

        public long getMessageAgeSeconds() {
            return this.messageAgeSeconds;
        }
    }
}
