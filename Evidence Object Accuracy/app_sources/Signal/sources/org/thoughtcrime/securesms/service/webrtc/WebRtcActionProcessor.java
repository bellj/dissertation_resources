package org.thoughtcrime.securesms.service.webrtc;

import android.content.Context;
import android.os.ResultReceiver;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import j$.util.Optional;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.ringrtc.CallException;
import org.signal.ringrtc.CallId;
import org.signal.ringrtc.CallManager;
import org.signal.ringrtc.GroupCall;
import org.signal.ringrtc.NetworkRoute;
import org.thoughtcrime.securesms.components.sensors.Orientation;
import org.thoughtcrime.securesms.components.webrtc.BroadcastVideoSink;
import org.thoughtcrime.securesms.components.webrtc.EglBaseWrapper;
import org.thoughtcrime.securesms.components.webrtc.GroupCallSafetyNumberChangeNotificationUtil;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfiles;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.ringrtc.CallState;
import org.thoughtcrime.securesms.ringrtc.Camera;
import org.thoughtcrime.securesms.ringrtc.CameraState;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.thoughtcrime.securesms.service.webrtc.WebRtcData;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcEphemeralState;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceState;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceStateBuilder;
import org.thoughtcrime.securesms.util.NetworkUtil;
import org.thoughtcrime.securesms.util.TelephonyUtil;
import org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager;
import org.thoughtcrime.securesms.webrtc.locks.LockManager;
import org.webrtc.PeerConnection;
import org.whispersystems.signalservice.api.messages.calls.AnswerMessage;
import org.whispersystems.signalservice.api.messages.calls.BusyMessage;
import org.whispersystems.signalservice.api.messages.calls.HangupMessage;
import org.whispersystems.signalservice.api.messages.calls.IceUpdateMessage;
import org.whispersystems.signalservice.api.messages.calls.OfferMessage;
import org.whispersystems.signalservice.api.messages.calls.SignalServiceCallMessage;

/* loaded from: classes4.dex */
public abstract class WebRtcActionProcessor {
    public static final int AUDIO_LEVELS_INTERVAL;
    protected final Context context;
    protected final String tag;
    protected final WebRtcInteractor webRtcInteractor;

    public WebRtcEphemeralState handleAudioLevelsChanged(WebRtcServiceState webRtcServiceState, WebRtcEphemeralState webRtcEphemeralState, int i, int i2) {
        return webRtcEphemeralState;
    }

    public WebRtcEphemeralState handleGroupAudioLevelsChanged(WebRtcServiceState webRtcServiceState, WebRtcEphemeralState webRtcEphemeralState) {
        return webRtcEphemeralState;
    }

    public WebRtcActionProcessor(WebRtcInteractor webRtcInteractor, String str) {
        this.context = webRtcInteractor.getContext();
        this.webRtcInteractor = webRtcInteractor;
        this.tag = str;
    }

    public String getTag() {
        return this.tag;
    }

    public WebRtcServiceState handleIsInCallQuery(WebRtcServiceState webRtcServiceState, ResultReceiver resultReceiver) {
        if (resultReceiver != null) {
            resultReceiver.send(0, null);
        }
        return webRtcServiceState;
    }

    public WebRtcServiceState handlePreJoinCall(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer) {
        Log.i(this.tag, "handlePreJoinCall not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleCancelPreJoinCall(WebRtcServiceState webRtcServiceState) {
        Log.i(this.tag, "handleCancelPreJoinCall not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleOutgoingCall(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer, OfferMessage.Type type) {
        Log.i(this.tag, "handleOutgoingCall not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleStartOutgoingCall(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer, OfferMessage.Type type) {
        Log.i(this.tag, "handleStartOutgoingCall not processed");
        return webRtcServiceState;
    }

    public final WebRtcServiceState handleSendOffer(WebRtcServiceState webRtcServiceState, WebRtcData.CallMetadata callMetadata, WebRtcData.OfferMetadata offerMetadata, boolean z) {
        Integer num;
        String str = this.tag;
        Log.i(str, "handleSendOffer(): id: " + callMetadata.getCallId().format(Integer.valueOf(callMetadata.getRemoteDevice())));
        OfferMessage offerMessage = new OfferMessage(callMetadata.getCallId().longValue().longValue(), offerMetadata.getSdp(), offerMetadata.getOfferType(), offerMetadata.getOpaque());
        if (z) {
            num = null;
        } else {
            num = Integer.valueOf(callMetadata.getRemoteDevice());
        }
        SignalServiceCallMessage forOffer = SignalServiceCallMessage.forOffer(offerMessage, true, num);
        RecipientUtil.shareProfileIfFirstSecureMessage(this.context, webRtcServiceState.getCallInfoState().getCallRecipient());
        this.webRtcInteractor.sendCallMessage(callMetadata.getRemotePeer(), forOffer);
        return webRtcServiceState;
    }

    public WebRtcServiceState handleRemoteRinging(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer) {
        Log.i(this.tag, "handleRemoteRinging not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleReceivedAnswer(WebRtcServiceState webRtcServiceState, WebRtcData.CallMetadata callMetadata, WebRtcData.AnswerMetadata answerMetadata, WebRtcData.ReceivedAnswerMetadata receivedAnswerMetadata) {
        Log.i(this.tag, "handleReceivedAnswer not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleReceivedBusy(WebRtcServiceState webRtcServiceState, WebRtcData.CallMetadata callMetadata) {
        Log.i(this.tag, "handleReceivedBusy not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleReceivedOffer(WebRtcServiceState webRtcServiceState, WebRtcData.CallMetadata callMetadata, WebRtcData.OfferMetadata offerMetadata, WebRtcData.ReceivedOfferMetadata receivedOfferMetadata) {
        String str = this.tag;
        Log.i(str, "handleReceivedOffer(): id: " + callMetadata.getCallId().format(Integer.valueOf(callMetadata.getRemoteDevice())) + " offer_type:" + offerMetadata.getOfferType());
        boolean z = false;
        if (TelephonyUtil.isAnyPstnLineBusy(this.context)) {
            Log.i(this.tag, "PSTN line is busy.");
            WebRtcServiceState handleSendBusy = webRtcServiceState.getActionProcessor().handleSendBusy(webRtcServiceState, callMetadata, true);
            WebRtcInteractor webRtcInteractor = this.webRtcInteractor;
            RemotePeer remotePeer = callMetadata.getRemotePeer();
            long serverReceivedTimestamp = receivedOfferMetadata.getServerReceivedTimestamp();
            if (offerMetadata.getOfferType() == OfferMessage.Type.VIDEO_CALL) {
                z = true;
            }
            webRtcInteractor.insertMissedCall(remotePeer, serverReceivedTimestamp, z);
            return handleSendBusy;
        } else if (!RecipientUtil.isCallRequestAccepted(this.context.getApplicationContext(), callMetadata.getRemotePeer().getRecipient())) {
            Log.w(this.tag, "Caller is untrusted.");
            WebRtcServiceState handleSendHangup = webRtcServiceState.getActionProcessor().handleSendHangup(webRtcServiceState, callMetadata, WebRtcData.HangupMetadata.fromType(HangupMessage.Type.NEED_PERMISSION), true);
            WebRtcInteractor webRtcInteractor2 = this.webRtcInteractor;
            RemotePeer remotePeer2 = callMetadata.getRemotePeer();
            long serverReceivedTimestamp2 = receivedOfferMetadata.getServerReceivedTimestamp();
            if (offerMetadata.getOfferType() == OfferMessage.Type.VIDEO_CALL) {
                z = true;
            }
            webRtcInteractor2.insertMissedCall(remotePeer2, serverReceivedTimestamp2, z);
            return handleSendHangup;
        } else if (offerMetadata.getOpaque() == null) {
            Log.w(this.tag, "Opaque data is required.");
            WebRtcServiceState handleSendHangup2 = webRtcServiceState.getActionProcessor().handleSendHangup(webRtcServiceState, callMetadata, WebRtcData.HangupMetadata.fromType(HangupMessage.Type.NORMAL), true);
            WebRtcInteractor webRtcInteractor3 = this.webRtcInteractor;
            RemotePeer remotePeer3 = callMetadata.getRemotePeer();
            long serverReceivedTimestamp3 = receivedOfferMetadata.getServerReceivedTimestamp();
            if (offerMetadata.getOfferType() == OfferMessage.Type.VIDEO_CALL) {
                z = true;
            }
            webRtcInteractor3.insertMissedCall(remotePeer3, serverReceivedTimestamp3, z);
            return handleSendHangup2;
        } else {
            NotificationProfile activeProfile = NotificationProfiles.getActiveProfile(SignalDatabase.notificationProfiles().getProfiles());
            if (activeProfile == null || activeProfile.isRecipientAllowed(callMetadata.getRemotePeer().getId()) || activeProfile.getAllowAllCalls()) {
                String str2 = this.tag;
                Log.i(str2, "add remotePeer callId: " + callMetadata.getRemotePeer().getCallId() + " key: " + callMetadata.getRemotePeer().hashCode());
                callMetadata.getRemotePeer().setCallStartTimestamp(receivedOfferMetadata.getServerReceivedTimestamp());
                WebRtcServiceStateBuilder.CallSetupStateBuilder changeCallSetupState = webRtcServiceState.builder().changeCallSetupState(callMetadata.getCallId());
                if (offerMetadata.getOfferType() == OfferMessage.Type.VIDEO_CALL) {
                    z = true;
                }
                WebRtcServiceState build = changeCallSetupState.isRemoteVideoOffer(z).commit().changeCallInfoState().putRemotePeer(callMetadata.getRemotePeer()).build();
                long max = Math.max(receivedOfferMetadata.getServerDeliveredTimestamp() - receivedOfferMetadata.getServerReceivedTimestamp(), 0L) / 1000;
                String str3 = this.tag;
                Log.i(str3, "messageAgeSec: " + max + ", serverReceivedTimestamp: " + receivedOfferMetadata.getServerReceivedTimestamp() + ", serverDeliveredTimestamp: " + receivedOfferMetadata.getServerDeliveredTimestamp());
                try {
                    this.webRtcInteractor.getCallManager().receivedOffer(callMetadata.getCallId(), callMetadata.getRemotePeer(), Integer.valueOf(callMetadata.getRemoteDevice()), offerMetadata.getOpaque(), Long.valueOf(max), WebRtcUtil.getCallMediaTypeFromOfferType(offerMetadata.getOfferType()), Integer.valueOf(SignalStore.account().getDeviceId()), SignalStore.account().isPrimaryDevice(), WebRtcUtil.getPublicKeyBytes(receivedOfferMetadata.getRemoteIdentityKey()), WebRtcUtil.getPublicKeyBytes(SignalStore.account().getAciIdentityKey().getPublicKey().serialize()));
                    return build;
                } catch (InvalidKeyException | CallException e) {
                    return callFailure(build, "Unable to process received offer: ", e);
                }
            } else {
                Log.w(this.tag, "Caller is excluded by notification profile.");
                WebRtcInteractor webRtcInteractor4 = this.webRtcInteractor;
                RemotePeer remotePeer4 = callMetadata.getRemotePeer();
                long serverReceivedTimestamp4 = receivedOfferMetadata.getServerReceivedTimestamp();
                if (offerMetadata.getOfferType() == OfferMessage.Type.VIDEO_CALL) {
                    z = true;
                }
                webRtcInteractor4.insertMissedCall(remotePeer4, serverReceivedTimestamp4, z);
                return webRtcServiceState;
            }
        }
    }

    public WebRtcServiceState handleReceivedOfferExpired(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer) {
        String str = this.tag;
        Log.i(str, "handleReceivedOfferExpired(): call_id: " + remotePeer.getCallId());
        this.webRtcInteractor.insertMissedCall(remotePeer, remotePeer.getCallStartTimestamp(), webRtcServiceState.getCallSetupState(remotePeer).isRemoteVideoOffer());
        return terminate(webRtcServiceState, remotePeer);
    }

    public WebRtcServiceState handleStartIncomingCall(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer, OfferMessage.Type type) {
        Log.i(this.tag, "handleStartIncomingCall not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleAcceptCall(WebRtcServiceState webRtcServiceState, boolean z) {
        Log.i(this.tag, "handleAcceptCall not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleSetTelecomApproved(WebRtcServiceState webRtcServiceState, long j, RecipientId recipientId) {
        String str = this.tag;
        Log.i(str, "handleSetTelecomApproved(): call_id: " + j);
        RemotePeer peerByCallId = webRtcServiceState.getCallInfoState().getPeerByCallId(new CallId(Long.valueOf(j)));
        if (peerByCallId != null && peerByCallId.callIdEquals(webRtcServiceState.getCallInfoState().getActivePeer())) {
            return webRtcServiceState.builder().changeCallSetupState(new CallId(Long.valueOf(j))).telecomApproved(true).build();
        }
        String str2 = this.tag;
        Log.w(str2, "Received telecom approval after call terminated. callId: " + j + " recipient: " + recipientId);
        this.webRtcInteractor.terminateCall(recipientId);
        return webRtcServiceState;
    }

    public WebRtcServiceState handleDropCall(WebRtcServiceState webRtcServiceState, long j) {
        String str = this.tag;
        Log.i(str, "handleDropCall(): call_id: " + j);
        CallId callId = new CallId(Long.valueOf(j));
        RemotePeer peerByCallId = webRtcServiceState.getCallInfoState().getPeerByCallId(callId);
        RemotePeer activePeer = webRtcServiceState.getCallInfoState().getActivePeer();
        boolean z = activePeer != null && activePeer.getCallId().equals(callId);
        if (peerByCallId != null) {
            try {
                if (webRtcServiceState.getCallInfoState().getCallState() == WebRtcViewModel.State.CALL_INCOMING) {
                    this.webRtcInteractor.insertMissedCall(peerByCallId, peerByCallId.getCallStartTimestamp(), webRtcServiceState.getCallSetupState(callId).isRemoteVideoOffer());
                }
            } catch (CallException e) {
                return callFailure(webRtcServiceState, "hangup() failed: ", e);
            }
        }
        this.webRtcInteractor.getCallManager().hangup();
        WebRtcServiceState build = webRtcServiceState.builder().changeCallInfoState().callState(WebRtcViewModel.State.CALL_DISCONNECTED).groupCallState(WebRtcViewModel.GroupCallState.DISCONNECTED).build();
        this.webRtcInteractor.postStateUpdate(build);
        if (z) {
            peerByCallId = activePeer;
        }
        return terminate(build, peerByCallId);
    }

    public WebRtcServiceState handleLocalRinging(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer) {
        Log.i(this.tag, "handleLocalRinging not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleDenyCall(WebRtcServiceState webRtcServiceState) {
        Log.i(this.tag, "handleDenyCall not processed");
        return webRtcServiceState;
    }

    public final WebRtcServiceState handleSendAnswer(WebRtcServiceState webRtcServiceState, WebRtcData.CallMetadata callMetadata, WebRtcData.AnswerMetadata answerMetadata, boolean z) {
        Integer num;
        String str = this.tag;
        Log.i(str, "handleSendAnswer(): id: " + callMetadata.getCallId().format(Integer.valueOf(callMetadata.getRemoteDevice())));
        AnswerMessage answerMessage = new AnswerMessage(callMetadata.getCallId().longValue().longValue(), answerMetadata.getSdp(), answerMetadata.getOpaque());
        if (z) {
            num = null;
        } else {
            num = Integer.valueOf(callMetadata.getRemoteDevice());
        }
        this.webRtcInteractor.sendCallMessage(callMetadata.getRemotePeer(), SignalServiceCallMessage.forAnswer(answerMessage, true, num));
        return webRtcServiceState;
    }

    public WebRtcServiceState handleCallConnected(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer) {
        Log.i(this.tag, "handleCallConnected not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleReceivedOfferWhileActive(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer) {
        Log.i(this.tag, "handleReceivedOfferWhileActive not processed");
        return webRtcServiceState;
    }

    public final WebRtcServiceState handleSendBusy(WebRtcServiceState webRtcServiceState, WebRtcData.CallMetadata callMetadata, boolean z) {
        Integer num;
        String str = this.tag;
        Log.i(str, "handleSendBusy(): id: " + callMetadata.getCallId().format(Integer.valueOf(callMetadata.getRemoteDevice())));
        BusyMessage busyMessage = new BusyMessage(callMetadata.getCallId().longValue().longValue());
        if (z) {
            num = null;
        } else {
            num = Integer.valueOf(callMetadata.getRemoteDevice());
        }
        this.webRtcInteractor.sendCallMessage(callMetadata.getRemotePeer(), SignalServiceCallMessage.forBusy(busyMessage, true, num));
        return webRtcServiceState;
    }

    public final WebRtcServiceState handleCallConcluded(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer) {
        Log.i(this.tag, "handleCallConcluded():");
        if (remotePeer == null) {
            return webRtcServiceState;
        }
        String str = this.tag;
        Log.i(str, "delete remotePeer callId: " + remotePeer.getCallId() + " key: " + remotePeer.hashCode());
        EglBaseWrapper.releaseEglBase(remotePeer.getCallId().longValue());
        return webRtcServiceState.builder().changeCallInfoState().removeRemotePeer(remotePeer).build();
    }

    public WebRtcServiceState handleRemoteVideoEnable(WebRtcServiceState webRtcServiceState, boolean z) {
        Log.i(this.tag, "handleRemoteVideoEnable not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleScreenSharingEnable(WebRtcServiceState webRtcServiceState, boolean z) {
        Log.i(this.tag, "handleScreenSharingEnable not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleReceivedHangup(WebRtcServiceState webRtcServiceState, WebRtcData.CallMetadata callMetadata, WebRtcData.HangupMetadata hangupMetadata) {
        String str = this.tag;
        Log.i(str, "handleReceivedHangup(): id: " + callMetadata.getCallId().format(Integer.valueOf(callMetadata.getRemoteDevice())));
        try {
            this.webRtcInteractor.getCallManager().receivedHangup(callMetadata.getCallId(), Integer.valueOf(callMetadata.getRemoteDevice()), hangupMetadata.getCallHangupType(), Integer.valueOf(hangupMetadata.getDeviceId()));
            return webRtcServiceState;
        } catch (CallException e) {
            return callFailure(webRtcServiceState, "receivedHangup() failed: ", e);
        }
    }

    public WebRtcServiceState handleLocalHangup(WebRtcServiceState webRtcServiceState) {
        Log.i(this.tag, "handleLocalHangup not processed");
        return webRtcServiceState;
    }

    public final WebRtcServiceState handleSendHangup(WebRtcServiceState webRtcServiceState, WebRtcData.CallMetadata callMetadata, WebRtcData.HangupMetadata hangupMetadata, boolean z) {
        Integer num;
        String str = this.tag;
        Log.i(str, "handleSendHangup(): id: " + callMetadata.getCallId().format(Integer.valueOf(callMetadata.getRemoteDevice())));
        HangupMessage hangupMessage = new HangupMessage(callMetadata.getCallId().longValue().longValue(), hangupMetadata.getType(), hangupMetadata.getDeviceId(), hangupMetadata.isLegacy());
        if (z) {
            num = null;
        } else {
            num = Integer.valueOf(callMetadata.getRemoteDevice());
        }
        this.webRtcInteractor.sendCallMessage(callMetadata.getRemotePeer(), SignalServiceCallMessage.forHangup(hangupMessage, true, num));
        return webRtcServiceState;
    }

    public WebRtcServiceState handleMessageSentSuccess(WebRtcServiceState webRtcServiceState, CallId callId) {
        try {
            this.webRtcInteractor.getCallManager().messageSent(callId);
            return webRtcServiceState;
        } catch (CallException e) {
            return callFailure(webRtcServiceState, "callManager.messageSent() failed: ", e);
        }
    }

    public WebRtcServiceState handleMessageSentError(WebRtcServiceState webRtcServiceState, CallId callId, WebRtcViewModel.State state, Optional<IdentityKey> optional) {
        Log.w(this.tag, "handleMessageSentError():");
        try {
            this.webRtcInteractor.getCallManager().messageSendFailure(callId);
        } catch (CallException e) {
            webRtcServiceState = callFailure(webRtcServiceState, "callManager.messageSendFailure() failed: ", e);
        }
        RemotePeer activePeer = webRtcServiceState.getCallInfoState().getActivePeer();
        if (activePeer == null) {
            return webRtcServiceState;
        }
        WebRtcServiceStateBuilder builder = webRtcServiceState.builder();
        WebRtcViewModel.State state2 = WebRtcViewModel.State.UNTRUSTED_IDENTITY;
        if (state == state2) {
            CallParticipant remoteCallParticipant = webRtcServiceState.getCallInfoState().getRemoteCallParticipant(activePeer.getRecipient());
            Objects.requireNonNull(remoteCallParticipant);
            builder.changeCallInfoState().callState(state2).putParticipant(activePeer.getRecipient(), remoteCallParticipant.withIdentityKey(optional.orElse(null))).commit();
        }
        return builder.build();
    }

    public WebRtcServiceState handleAudioDeviceChanged(WebRtcServiceState webRtcServiceState, SignalAudioManager.AudioDevice audioDevice, Set<SignalAudioManager.AudioDevice> set) {
        Log.i(this.tag, "handleAudioDeviceChanged not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleBluetoothPermissionDenied(WebRtcServiceState webRtcServiceState) {
        return webRtcServiceState.builder().changeLocalDeviceState().setBluetoothPermissionDenied(true).build();
    }

    public WebRtcServiceState handleSetUserAudioDevice(WebRtcServiceState webRtcServiceState, SignalAudioManager.AudioDevice audioDevice) {
        Log.i(this.tag, "handleSetUserAudioDevice not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleCallReconnect(WebRtcServiceState webRtcServiceState, CallManager.CallEvent callEvent) {
        Log.i(this.tag, "handleCallReconnect not processed");
        return webRtcServiceState;
    }

    public final WebRtcServiceState handleSendIceCandidates(WebRtcServiceState webRtcServiceState, WebRtcData.CallMetadata callMetadata, boolean z, List<byte[]> list) {
        Integer num;
        String str = this.tag;
        Log.i(str, "handleSendIceCandidates(): id: " + callMetadata.getCallId().format(Integer.valueOf(callMetadata.getRemoteDevice())));
        List list2 = Stream.of(list).map(new Function() { // from class: org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return WebRtcActionProcessor.lambda$handleSendIceCandidates$0(WebRtcData.CallMetadata.this, (byte[]) obj);
            }
        }).toList();
        if (z) {
            num = null;
        } else {
            num = Integer.valueOf(callMetadata.getRemoteDevice());
        }
        this.webRtcInteractor.sendCallMessage(callMetadata.getRemotePeer(), SignalServiceCallMessage.forIceUpdates(list2, true, num));
        return webRtcServiceState;
    }

    public static /* synthetic */ IceUpdateMessage lambda$handleSendIceCandidates$0(WebRtcData.CallMetadata callMetadata, byte[] bArr) {
        return new IceUpdateMessage(callMetadata.getCallId().longValue().longValue(), bArr, null);
    }

    public WebRtcServiceState handleReceivedIceCandidates(WebRtcServiceState webRtcServiceState, WebRtcData.CallMetadata callMetadata, List<byte[]> list) {
        String str = this.tag;
        Log.i(str, "handleReceivedIceCandidates(): id: " + callMetadata.getCallId().format(Integer.valueOf(callMetadata.getRemoteDevice())) + ", count: " + list.size());
        try {
            this.webRtcInteractor.getCallManager().receivedIceCandidates(callMetadata.getCallId(), Integer.valueOf(callMetadata.getRemoteDevice()), list);
            return webRtcServiceState;
        } catch (CallException e) {
            return callFailure(webRtcServiceState, "receivedIceCandidates() failed: ", e);
        }
    }

    public WebRtcServiceState handleTurnServerUpdate(WebRtcServiceState webRtcServiceState, List<PeerConnection.IceServer> list, boolean z) {
        Log.i(this.tag, "handleTurnServerUpdate not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleSetEnableVideo(WebRtcServiceState webRtcServiceState, boolean z) {
        Log.i(this.tag, "handleSetEnableVideo not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleSetMuteAudio(WebRtcServiceState webRtcServiceState, boolean z) {
        Log.i(this.tag, "handleSetMuteAudio not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleSetCameraFlip(WebRtcServiceState webRtcServiceState) {
        Log.i(this.tag, "handleSetCameraFlip not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleScreenOffChange(WebRtcServiceState webRtcServiceState) {
        Log.i(this.tag, "handleScreenOffChange not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleCameraSwitchCompleted(WebRtcServiceState webRtcServiceState, CameraState cameraState) {
        Log.i(this.tag, "handleCameraSwitchCompleted not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleNetworkChanged(WebRtcServiceState webRtcServiceState, boolean z) {
        Log.i(this.tag, "handleNetworkChanged not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleNetworkRouteChanged(WebRtcServiceState webRtcServiceState, NetworkRoute networkRoute) {
        String str = this.tag;
        Log.i(str, "onNetworkRouteChanged: localAdapterType: " + networkRoute.getLocalAdapterType());
        try {
            this.webRtcInteractor.getCallManager().updateBandwidthMode(NetworkUtil.getCallingBandwidthMode(this.context, networkRoute.getLocalAdapterType()));
        } catch (CallException e) {
            Log.w(this.tag, "Unable to update bandwidth mode on CallManager", e);
        }
        return webRtcServiceState.builder().changeLocalDeviceState().setNetworkConnectionType(networkRoute.getLocalAdapterType()).commit().build();
    }

    public WebRtcServiceState handleBandwidthModeUpdate(WebRtcServiceState webRtcServiceState) {
        try {
            this.webRtcInteractor.getCallManager().updateBandwidthMode(NetworkUtil.getCallingBandwidthMode(this.context));
        } catch (CallException unused) {
            Log.i(this.tag, "handleBandwidthModeUpdate: could not update bandwidth mode.");
        }
        return webRtcServiceState;
    }

    public WebRtcServiceState handleOrientationChanged(WebRtcServiceState webRtcServiceState, boolean z, int i) {
        Camera camera = webRtcServiceState.getVideoState().getCamera();
        if (camera != null) {
            camera.setOrientation(Integer.valueOf(i));
        }
        int i2 = z ? -1 : i;
        int i3 = z ? 0 : i;
        BroadcastVideoSink localSink = webRtcServiceState.getVideoState().getLocalSink();
        if (localSink != null) {
            localSink.setDeviceOrientationDegrees(i2);
        }
        for (CallParticipant callParticipant : webRtcServiceState.getCallInfoState().getRemoteCallParticipants()) {
            callParticipant.getVideoSink().setDeviceOrientationDegrees(i2);
        }
        return webRtcServiceState.builder().changeLocalDeviceState().setOrientation(Orientation.fromDegrees(i3)).setLandscapeEnabled(z).setDeviceOrientation(Orientation.fromDegrees(i)).build();
    }

    public WebRtcServiceState handleEndedRemote(WebRtcServiceState webRtcServiceState, CallManager.CallEvent callEvent, RemotePeer remotePeer) {
        Log.i(this.tag, "handleEndedRemote not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleEnded(WebRtcServiceState webRtcServiceState, CallManager.CallEvent callEvent, RemotePeer remotePeer) {
        Log.i(this.tag, "handleEnded not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleSetupFailure(WebRtcServiceState webRtcServiceState, CallId callId) {
        Log.i(this.tag, "handleSetupFailure not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState callFailure(WebRtcServiceState webRtcServiceState, String str, Throwable th) {
        String str2 = this.tag;
        Log.w(str2, "callFailure(): " + str, th);
        WebRtcServiceStateBuilder builder = webRtcServiceState.builder();
        if (webRtcServiceState.getCallInfoState().getActivePeer() != null) {
            builder.changeCallInfoState().callState(WebRtcViewModel.State.CALL_DISCONNECTED);
        }
        try {
            this.webRtcInteractor.getCallManager().reset();
        } catch (CallException e) {
            Log.w(this.tag, "Unable to reset call manager: ", e);
        }
        EglBaseWrapper.forceRelease();
        WebRtcServiceState build = builder.changeCallInfoState().clearPeerMap().build();
        return terminate(build, build.getCallInfoState().getActivePeer());
    }

    public synchronized WebRtcServiceState terminate(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer) {
        boolean z;
        Log.i(this.tag, "terminate():");
        RemotePeer activePeer = webRtcServiceState.getCallInfoState().getActivePeer();
        if (activePeer == null && remotePeer == null) {
            Log.i(this.tag, "skipping with no active peer");
            return webRtcServiceState;
        } else if (activePeer == null || activePeer.callIdEquals(remotePeer)) {
            ApplicationDependencies.getAppForegroundObserver().removeListener(this.webRtcInteractor.getForegroundListener());
            this.webRtcInteractor.updatePhoneState(LockManager.PhoneState.PROCESSING);
            if (!(remotePeer.getState() == CallState.DIALING || remotePeer.getState() == CallState.REMOTE_RINGING || remotePeer.getState() == CallState.RECEIVED_BUSY || remotePeer.getState() == CallState.CONNECTED)) {
                z = false;
                this.webRtcInteractor.stopAudio(z);
                this.webRtcInteractor.terminateCall(remotePeer.getId());
                this.webRtcInteractor.updatePhoneState(LockManager.PhoneState.IDLE);
                this.webRtcInteractor.stopForegroundService();
                return WebRtcVideoUtil.deinitializeVideo(webRtcServiceState).builder().changeCallInfoState().activePeer(null).commit().changeLocalDeviceState().commit().actionProcessor(new IdleActionProcessor(this.webRtcInteractor)).terminate(remotePeer.getCallId()).build();
            }
            z = true;
            this.webRtcInteractor.stopAudio(z);
            this.webRtcInteractor.terminateCall(remotePeer.getId());
            this.webRtcInteractor.updatePhoneState(LockManager.PhoneState.IDLE);
            this.webRtcInteractor.stopForegroundService();
            return WebRtcVideoUtil.deinitializeVideo(webRtcServiceState).builder().changeCallInfoState().activePeer(null).commit().changeLocalDeviceState().commit().actionProcessor(new IdleActionProcessor(this.webRtcInteractor)).terminate(remotePeer.getCallId()).build();
        } else {
            Log.i(this.tag, "skipping remotePeer is not active peer");
            return webRtcServiceState;
        }
    }

    public WebRtcServiceState handleGroupLocalDeviceStateChanged(WebRtcServiceState webRtcServiceState) {
        Log.i(this.tag, "handleGroupLocalDeviceStateChanged not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleGroupRemoteDeviceStateChanged(WebRtcServiceState webRtcServiceState) {
        Log.i(this.tag, "handleGroupRemoteDeviceStateChanged not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleGroupJoinedMembershipChanged(WebRtcServiceState webRtcServiceState) {
        Log.i(this.tag, "handleGroupJoinedMembershipChanged not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleGroupRequestMembershipProof(WebRtcServiceState webRtcServiceState, int i, byte[] bArr) {
        Log.i(this.tag, "handleGroupRequestMembershipProof not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleGroupRequestUpdateMembers(WebRtcServiceState webRtcServiceState) {
        Log.i(this.tag, "handleGroupRequestUpdateMembers not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleUpdateRenderedResolutions(WebRtcServiceState webRtcServiceState) {
        Log.i(this.tag, "handleUpdateRenderedResolutions not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleGroupCallEnded(WebRtcServiceState webRtcServiceState, int i, GroupCall.GroupCallEndReason groupCallEndReason) {
        Log.i(this.tag, "handleGroupCallEnded not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleGroupMessageSentError(WebRtcServiceState webRtcServiceState, Collection<RecipientId> collection, WebRtcViewModel.State state) {
        Log.i(this.tag, "handleGroupMessageSentError not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleGroupApproveSafetyNumberChange(WebRtcServiceState webRtcServiceState, List<RecipientId> list) {
        Log.i(this.tag, "handleGroupApproveSafetyNumberChange not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleReceivedOpaqueMessage(WebRtcServiceState webRtcServiceState, WebRtcData.OpaqueMessageMetadata opaqueMessageMetadata) {
        Log.i(this.tag, "handleReceivedOpaqueMessage():");
        try {
            this.webRtcInteractor.getCallManager().receivedCallMessage(opaqueMessageMetadata.getUuid(), Integer.valueOf(opaqueMessageMetadata.getRemoteDeviceId()), Integer.valueOf(SignalStore.account().getDeviceId()), opaqueMessageMetadata.getOpaque(), Long.valueOf(opaqueMessageMetadata.getMessageAgeSeconds()));
            return webRtcServiceState;
        } catch (CallException e) {
            return groupCallFailure(webRtcServiceState, "Unable to receive opaque message", e);
        }
    }

    public WebRtcServiceState handleGroupCallRingUpdate(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer, GroupId.V2 v2, long j, UUID uuid, CallManager.RingUpdate ringUpdate) {
        String str = this.tag;
        Log.i(str, "handleGroupCallRingUpdate(): recipient: " + remotePeer.getId() + " ring: " + j + " update: " + ringUpdate);
        try {
            CallManager.RingUpdate ringUpdate2 = CallManager.RingUpdate.BUSY_LOCALLY;
            if (!(ringUpdate == ringUpdate2 || ringUpdate == CallManager.RingUpdate.BUSY_ON_ANOTHER_DEVICE)) {
                this.webRtcInteractor.getCallManager().cancelGroupRing(v2.getDecodedId(), j, CallManager.RingCancelReason.Busy);
            }
            SignalDatabase.groupCallRings().insertOrUpdateGroupRing(j, System.currentTimeMillis(), ringUpdate == CallManager.RingUpdate.REQUESTED ? ringUpdate2 : ringUpdate);
        } catch (CallException e) {
            Log.w(this.tag, "Unable to cancel ring", e);
        }
        return webRtcServiceState;
    }

    public WebRtcServiceState handleSetRingGroup(WebRtcServiceState webRtcServiceState, boolean z) {
        Log.i(this.tag, "handleSetRingGroup not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState handleReceivedGroupCallPeekForRingingCheck(WebRtcServiceState webRtcServiceState, GroupCallRingCheckInfo groupCallRingCheckInfo, long j) {
        Log.i(this.tag, "handleReceivedGroupCallPeekForRingingCheck not processed");
        return webRtcServiceState;
    }

    public WebRtcServiceState groupCallFailure(WebRtcServiceState webRtcServiceState, String str, Throwable th) {
        String str2 = this.tag;
        Log.w(str2, "groupCallFailure(): " + str, th);
        GroupCall groupCall = webRtcServiceState.getCallInfoState().getGroupCall();
        Recipient callRecipient = webRtcServiceState.getCallInfoState().getCallRecipient();
        if (callRecipient != null && webRtcServiceState.getCallInfoState().getGroupCallState().isConnected()) {
            this.webRtcInteractor.sendGroupCallMessage(callRecipient, WebRtcUtil.getGroupCallEraId(groupCall));
        }
        WebRtcServiceState build = webRtcServiceState.builder().changeCallInfoState().callState(WebRtcViewModel.State.CALL_DISCONNECTED).groupCallState(WebRtcViewModel.GroupCallState.DISCONNECTED).build();
        this.webRtcInteractor.postStateUpdate(build);
        if (groupCall != null) {
            try {
                groupCall.disconnect();
            } catch (CallException e) {
                Log.w(this.tag, "Unable to reset call manager: ", e);
            }
        }
        this.webRtcInteractor.getCallManager().reset();
        return terminateGroupCall(build);
    }

    public synchronized WebRtcServiceState terminateGroupCall(WebRtcServiceState webRtcServiceState) {
        return terminateGroupCall(webRtcServiceState, true);
    }

    public synchronized WebRtcServiceState terminateGroupCall(WebRtcServiceState webRtcServiceState, boolean z) {
        this.webRtcInteractor.updatePhoneState(LockManager.PhoneState.PROCESSING);
        this.webRtcInteractor.stopAudio(webRtcServiceState.getCallInfoState().getCallState() == WebRtcViewModel.State.CALL_DISCONNECTED);
        this.webRtcInteractor.updatePhoneState(LockManager.PhoneState.IDLE);
        this.webRtcInteractor.stopForegroundService();
        if (z) {
            WebRtcVideoUtil.deinitializeVideo(webRtcServiceState);
            EglBaseWrapper.releaseEglBase(RemotePeer.GROUP_CALL_ID.longValue());
        }
        GroupCallSafetyNumberChangeNotificationUtil.cancelNotification(this.context, webRtcServiceState.getCallInfoState().getCallRecipient());
        return new WebRtcServiceState(new IdleActionProcessor(this.webRtcInteractor));
    }
}
