package org.thoughtcrime.securesms.service.webrtc;

import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.components.webrtc.EglBaseWrapper;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceState;
import org.whispersystems.signalservice.api.messages.calls.OfferMessage;

/* loaded from: classes4.dex */
public class PreJoinActionProcessor extends DeviceAwareActionProcessor {
    private static final String TAG = Log.tag(PreJoinActionProcessor.class);
    private final BeginCallActionProcessorDelegate beginCallDelegate;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public PreJoinActionProcessor(org.thoughtcrime.securesms.service.webrtc.WebRtcInteractor r3) {
        /*
            r2 = this;
            java.lang.String r0 = org.thoughtcrime.securesms.service.webrtc.PreJoinActionProcessor.TAG
            r2.<init>(r3, r0)
            org.thoughtcrime.securesms.service.webrtc.BeginCallActionProcessorDelegate r1 = new org.thoughtcrime.securesms.service.webrtc.BeginCallActionProcessorDelegate
            r1.<init>(r3, r0)
            r2.beginCallDelegate = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.service.webrtc.PreJoinActionProcessor.<init>(org.thoughtcrime.securesms.service.webrtc.WebRtcInteractor):void");
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleCancelPreJoinCall(WebRtcServiceState webRtcServiceState) {
        Log.i(TAG, "handleCancelPreJoinCall():");
        WebRtcVideoUtil.deinitializeVideo(webRtcServiceState);
        EglBaseWrapper.releaseEglBase(EglBaseWrapper.OUTGOING_PLACEHOLDER);
        return new WebRtcServiceState(new IdleActionProcessor(this.webRtcInteractor));
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleStartIncomingCall(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer, OfferMessage.Type type) {
        Log.i(TAG, "handleStartIncomingCall():");
        EglBaseWrapper.replaceHolder(EglBaseWrapper.OUTGOING_PLACEHOLDER, remotePeer.getCallId().longValue());
        WebRtcServiceState build = WebRtcVideoUtil.reinitializeCamera(this.context, this.webRtcInteractor.getCameraEventListener(), webRtcServiceState).builder().changeCallInfoState().callState(WebRtcViewModel.State.CALL_INCOMING).build();
        this.webRtcInteractor.postStateUpdate(build);
        return this.beginCallDelegate.handleStartIncomingCall(build, remotePeer, type);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleOutgoingCall(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer, OfferMessage.Type type) {
        Log.i(TAG, "handleOutgoingCall():");
        return this.beginCallDelegate.handleOutgoingCall(WebRtcVideoUtil.reinitializeCamera(this.context, this.webRtcInteractor.getCameraEventListener(), webRtcServiceState), remotePeer, type);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleSetEnableVideo(WebRtcServiceState webRtcServiceState, boolean z) {
        Log.i(TAG, "handleSetEnableVideo(): Changing for pre-join call.");
        webRtcServiceState.getVideoState().getCamera().setEnabled(z);
        return webRtcServiceState.builder().changeLocalDeviceState().cameraState(webRtcServiceState.getVideoState().getCamera().getCameraState()).build();
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleSetMuteAudio(WebRtcServiceState webRtcServiceState, boolean z) {
        return webRtcServiceState.builder().changeLocalDeviceState().isMicrophoneEnabled(!z).build();
    }
}
