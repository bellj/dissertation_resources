package org.thoughtcrime.securesms.service.webrtc;

import android.content.Context;
import android.content.Intent;
import android.telecom.CallAudioState;
import android.telecom.Connection;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.WebRtcCallActivity;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.webrtc.audio.AudioManagerCommand;
import org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager;

/* compiled from: AndroidCallConnection.kt */
@Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0007\u0018\u0000 \u00182\u00020\u0001:\u0001\u0018B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0002\u0010\tJ\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\b\u0010\u0014\u001a\u00020\u000eH\u0016J\b\u0010\u0015\u001a\u00020\u000eH\u0016J\b\u0010\u0016\u001a\u00020\u000eH\u0016J\b\u0010\u0017\u001a\u00020\u000eH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0007X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/service/webrtc/AndroidCallConnection;", "Landroid/telecom/Connection;", "context", "Landroid/content/Context;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "isOutgoing", "", "isVideoCall", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/recipients/RecipientId;ZZ)V", "initialAudioRoute", "Lorg/thoughtcrime/securesms/webrtc/audio/SignalAudioManager$AudioDevice;", "needToResetAudioRoute", "onAnswer", "", "videoState", "", "onCallAudioStateChanged", "state", "Landroid/telecom/CallAudioState;", "onDisconnect", "onReject", "onShowIncomingCallUi", "onSilence", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AndroidCallConnection extends Connection {
    public static final Companion Companion = new Companion(null);
    private static final String TAG;
    private final Context context;
    private SignalAudioManager.AudioDevice initialAudioRoute;
    private boolean needToResetAudioRoute;
    private final RecipientId recipientId;

    public AndroidCallConnection(Context context, RecipientId recipientId, boolean z, boolean z2) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        this.context = context;
        this.recipientId = recipientId;
        this.needToResetAudioRoute = z && !z2;
        setConnectionProperties(128);
        setConnectionCapabilities(3904);
    }

    @Override // android.telecom.Connection
    public void onShowIncomingCallUi() {
        Log.i(TAG, "onShowIncomingCallUi()");
        WebRtcCallService.update(this.context, 4, this.recipientId);
        setRinging();
    }

    @Override // android.telecom.Connection
    public void onCallAudioStateChanged(CallAudioState callAudioState) {
        Intrinsics.checkNotNullParameter(callAudioState, "state");
        String str = TAG;
        Log.i(str, "onCallAudioStateChanged(" + callAudioState + ')');
        SignalAudioManager.AudioDevice audioDevice = (SignalAudioManager.AudioDevice) CollectionsKt___CollectionsKt.firstOrNull(AndroidCallConnectionKt.toDevices(callAudioState.getRoute()));
        if (audioDevice == null) {
            audioDevice = SignalAudioManager.AudioDevice.EARPIECE;
        }
        ApplicationDependencies.getSignalCallManager().onAudioDeviceChanged(audioDevice, AndroidCallConnectionKt.toDevices(callAudioState.getSupportedRouteMask()));
        if (!this.needToResetAudioRoute) {
            return;
        }
        if (this.initialAudioRoute == null) {
            this.initialAudioRoute = audioDevice;
        } else if (audioDevice == SignalAudioManager.AudioDevice.SPEAKER_PHONE) {
            Log.i(str, "Resetting audio route from SPEAKER_PHONE to " + this.initialAudioRoute);
            AndroidTelecomUtil androidTelecomUtil = AndroidTelecomUtil.INSTANCE;
            RecipientId recipientId = this.recipientId;
            SignalAudioManager.AudioDevice audioDevice2 = this.initialAudioRoute;
            Intrinsics.checkNotNull(audioDevice2);
            androidTelecomUtil.selectAudioDevice(recipientId, audioDevice2);
            this.needToResetAudioRoute = false;
        }
    }

    @Override // android.telecom.Connection
    public void onAnswer(int i) {
        String str = TAG;
        Log.i(str, "onAnswer(" + i + ')');
        if (Permissions.hasAll(this.context, "android.permission.RECORD_AUDIO")) {
            ApplicationDependencies.getSignalCallManager().acceptCall(false);
            return;
        }
        Intent intent = new Intent(this.context, WebRtcCallActivity.class);
        intent.setAction(WebRtcCallActivity.ANSWER_ACTION);
        intent.setFlags(intent.getFlags() | SQLiteDatabase.CREATE_IF_NECESSARY);
        this.context.startActivity(intent);
    }

    @Override // android.telecom.Connection
    public void onSilence() {
        WebRtcCallService.sendAudioManagerCommand(this.context, new AudioManagerCommand.SilenceIncomingRinger());
    }

    @Override // android.telecom.Connection
    public void onReject() {
        Log.i(TAG, "onReject()");
        WebRtcCallService.denyCall(this.context);
    }

    @Override // android.telecom.Connection
    public void onDisconnect() {
        Log.i(TAG, "onDisconnect()");
        WebRtcCallService.hangup(this.context);
    }

    /* compiled from: AndroidCallConnection.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/service/webrtc/AndroidCallConnection$Companion;", "", "()V", "TAG", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    static {
        Companion = new Companion(null);
        String tag = Log.tag(AndroidCallConnection.class);
        Intrinsics.checkNotNullExpressionValue(tag, "tag(AndroidCallConnection::class.java)");
        TAG = tag;
    }
}
