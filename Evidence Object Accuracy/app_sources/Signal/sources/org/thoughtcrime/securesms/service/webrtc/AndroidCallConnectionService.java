package org.thoughtcrime.securesms.service.webrtc;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.telecom.Connection;
import android.telecom.ConnectionRequest;
import android.telecom.ConnectionService;
import android.telecom.PhoneAccountHandle;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: AndroidCallConnectionService.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0007\u0018\u0000 \u000f2\u00020\u0001:\u0002\u000f\u0010B\u0005¢\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J\u001a\u0010\t\u001a\u00020\n2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J\u001a\u0010\u000b\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J\u001a\u0010\f\u001a\u00020\n2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J\f\u0010\r\u001a\u00020\u000e*\u00020\bH\u0002¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/service/webrtc/AndroidCallConnectionService;", "Landroid/telecom/ConnectionService;", "()V", "onCreateIncomingConnection", "Landroid/telecom/Connection;", "connectionManagerPhoneAccount", "Landroid/telecom/PhoneAccountHandle;", "request", "Landroid/telecom/ConnectionRequest;", "onCreateIncomingConnectionFailed", "", "onCreateOutgoingConnection", "onCreateOutgoingConnectionFailed", "getOurExtras", "Lorg/thoughtcrime/securesms/service/webrtc/AndroidCallConnectionService$ServiceExtras;", "Companion", "ServiceExtras", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AndroidCallConnectionService extends ConnectionService {
    public static final Companion Companion = new Companion(null);
    public static final String KEY_CALL_ID;
    public static final String KEY_RECIPIENT_ID;
    public static final String KEY_VIDEO_CALL;
    private static final String TAG;

    @Override // android.telecom.ConnectionService
    public Connection onCreateIncomingConnection(PhoneAccountHandle phoneAccountHandle, ConnectionRequest connectionRequest) {
        Intrinsics.checkNotNullParameter(connectionRequest, "request");
        ServiceExtras ourExtras = getOurExtras(connectionRequest);
        RecipientId component1 = ourExtras.component1();
        long component2 = ourExtras.component2();
        boolean component3 = ourExtras.component3();
        String str = TAG;
        Log.i(str, "onCreateIncomingConnection(" + component1 + ')');
        Recipient resolved = Recipient.resolved(component1);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
        String displayName = resolved.getDisplayName(this);
        Intrinsics.checkNotNullExpressionValue(displayName, "recipient.getDisplayName(this)");
        Context applicationContext = getApplicationContext();
        Intrinsics.checkNotNullExpressionValue(applicationContext, "applicationContext");
        AndroidCallConnection androidCallConnection = new AndroidCallConnection(applicationContext, component1, false, component3);
        androidCallConnection.setInitializing();
        if (SignalStore.settings().getMessageNotificationsPrivacy().isDisplayContact() && resolved.getE164().isPresent()) {
            androidCallConnection.setAddress(Uri.fromParts("tel", resolved.getE164().get(), null), 1);
            androidCallConnection.setCallerDisplayName(displayName, 1);
        }
        androidCallConnection.setVideoState(connectionRequest.getVideoState());
        androidCallConnection.setExtras(connectionRequest.getExtras());
        androidCallConnection.setRinging();
        AndroidTelecomUtil.getConnections().put(component1, androidCallConnection);
        ApplicationDependencies.getSignalCallManager().setTelecomApproved(component2, component1);
        return androidCallConnection;
    }

    @Override // android.telecom.ConnectionService
    public void onCreateIncomingConnectionFailed(PhoneAccountHandle phoneAccountHandle, ConnectionRequest connectionRequest) {
        Intrinsics.checkNotNullParameter(connectionRequest, "request");
        ServiceExtras ourExtras = getOurExtras(connectionRequest);
        RecipientId component1 = ourExtras.component1();
        long component2 = ourExtras.component2();
        String str = TAG;
        Log.i(str, "onCreateIncomingConnectionFailed(" + component1 + ')');
        ApplicationDependencies.getSignalCallManager().dropCall(component2);
    }

    @Override // android.telecom.ConnectionService
    public Connection onCreateOutgoingConnection(PhoneAccountHandle phoneAccountHandle, ConnectionRequest connectionRequest) {
        Intrinsics.checkNotNullParameter(connectionRequest, "request");
        ServiceExtras ourExtras = getOurExtras(connectionRequest);
        RecipientId component1 = ourExtras.component1();
        long component2 = ourExtras.component2();
        boolean component3 = ourExtras.component3();
        String str = TAG;
        Log.i(str, "onCreateOutgoingConnection(" + component1 + ')');
        Context applicationContext = getApplicationContext();
        Intrinsics.checkNotNullExpressionValue(applicationContext, "applicationContext");
        AndroidCallConnection androidCallConnection = new AndroidCallConnection(applicationContext, component1, true, component3);
        androidCallConnection.setVideoState(connectionRequest.getVideoState());
        androidCallConnection.setExtras(connectionRequest.getExtras());
        androidCallConnection.setDialing();
        AndroidTelecomUtil.getConnections().put(component1, androidCallConnection);
        ApplicationDependencies.getSignalCallManager().setTelecomApproved(component2, component1);
        return androidCallConnection;
    }

    @Override // android.telecom.ConnectionService
    public void onCreateOutgoingConnectionFailed(PhoneAccountHandle phoneAccountHandle, ConnectionRequest connectionRequest) {
        Intrinsics.checkNotNullParameter(connectionRequest, "request");
        ServiceExtras ourExtras = getOurExtras(connectionRequest);
        RecipientId component1 = ourExtras.component1();
        long component2 = ourExtras.component2();
        String str = TAG;
        Log.i(str, "onCreateOutgoingConnectionFailed(" + component1 + ')');
        ApplicationDependencies.getSignalCallManager().dropCall(component2);
    }

    /* compiled from: AndroidCallConnectionService.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/service/webrtc/AndroidCallConnectionService$Companion;", "", "()V", "KEY_CALL_ID", "", "KEY_RECIPIENT_ID", "KEY_VIDEO_CALL", "TAG", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    static {
        Companion = new Companion(null);
        String tag = Log.tag(AndroidCallConnectionService.class);
        Intrinsics.checkNotNullExpressionValue(tag, "tag(AndroidCallConnectionService::class.java)");
        TAG = tag;
    }

    private final ServiceExtras getOurExtras(ConnectionRequest connectionRequest) {
        Bundle bundle = connectionRequest.getExtras().getBundle("android.telecom.extra.INCOMING_CALL_EXTRAS");
        if (bundle == null) {
            bundle = connectionRequest.getExtras();
            Intrinsics.checkNotNullExpressionValue(bundle, "extras");
        }
        String string = bundle.getString(KEY_RECIPIENT_ID);
        Intrinsics.checkNotNull(string);
        RecipientId from = RecipientId.from(string);
        Intrinsics.checkNotNullExpressionValue(from, "from(ourExtras.getString(KEY_RECIPIENT_ID)!!)");
        return new ServiceExtras(from, bundle.getLong(KEY_CALL_ID), bundle.getBoolean(KEY_VIDEO_CALL, false));
    }

    /* compiled from: AndroidCallConnectionService.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0002\b\r\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0007HÆ\u0003J'\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0012\u001a\u00020\u00072\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\r¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/service/webrtc/AndroidCallConnectionService$ServiceExtras;", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "callId", "", "isVideoCall", "", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;JZ)V", "getCallId", "()J", "()Z", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "component2", "component3", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ServiceExtras {
        private final long callId;
        private final boolean isVideoCall;
        private final RecipientId recipientId;

        public static /* synthetic */ ServiceExtras copy$default(ServiceExtras serviceExtras, RecipientId recipientId, long j, boolean z, int i, Object obj) {
            if ((i & 1) != 0) {
                recipientId = serviceExtras.recipientId;
            }
            if ((i & 2) != 0) {
                j = serviceExtras.callId;
            }
            if ((i & 4) != 0) {
                z = serviceExtras.isVideoCall;
            }
            return serviceExtras.copy(recipientId, j, z);
        }

        public final RecipientId component1() {
            return this.recipientId;
        }

        public final long component2() {
            return this.callId;
        }

        public final boolean component3() {
            return this.isVideoCall;
        }

        public final ServiceExtras copy(RecipientId recipientId, long j, boolean z) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            return new ServiceExtras(recipientId, j, z);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ServiceExtras)) {
                return false;
            }
            ServiceExtras serviceExtras = (ServiceExtras) obj;
            return Intrinsics.areEqual(this.recipientId, serviceExtras.recipientId) && this.callId == serviceExtras.callId && this.isVideoCall == serviceExtras.isVideoCall;
        }

        public int hashCode() {
            int hashCode = ((this.recipientId.hashCode() * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.callId)) * 31;
            boolean z = this.isVideoCall;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return hashCode + i;
        }

        public String toString() {
            return "ServiceExtras(recipientId=" + this.recipientId + ", callId=" + this.callId + ", isVideoCall=" + this.isVideoCall + ')';
        }

        public ServiceExtras(RecipientId recipientId, long j, boolean z) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            this.recipientId = recipientId;
            this.callId = j;
            this.isVideoCall = z;
        }

        public final long getCallId() {
            return this.callId;
        }

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }

        public final boolean isVideoCall() {
            return this.isVideoCall;
        }
    }
}
