package org.thoughtcrime.securesms.service;

import android.content.Context;
import android.content.Intent;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.UpdateApkJob;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes.dex */
public class UpdateApkRefreshListener extends PersistentAlarmManagerListener {
    private static final long INTERVAL = TimeUnit.HOURS.toMillis(6);
    private static final String TAG = Log.tag(UpdateApkRefreshListener.class);

    @Override // org.thoughtcrime.securesms.service.PersistentAlarmManagerListener
    protected long getNextScheduledExecutionTime(Context context) {
        return TextSecurePreferences.getUpdateApkRefreshTime(context);
    }

    @Override // org.thoughtcrime.securesms.service.PersistentAlarmManagerListener
    protected long onAlarm(Context context, long j) {
        String str = TAG;
        Log.i(str, "onAlarm...");
        if (j != 0) {
            Log.i(str, "Queueing APK update job...");
            ApplicationDependencies.getJobManager().add(new UpdateApkJob());
        }
        long currentTimeMillis = System.currentTimeMillis() + INTERVAL;
        TextSecurePreferences.setUpdateApkRefreshTime(context, currentTimeMillis);
        return currentTimeMillis;
    }

    public static void schedule(Context context) {
        new UpdateApkRefreshListener().onReceive(context, new Intent());
    }
}
