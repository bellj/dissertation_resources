package org.thoughtcrime.securesms.service.webrtc.state;

import com.annimon.stream.OptionalLong;
import java.util.Collection;
import java.util.Set;
import org.signal.ringrtc.CallId;
import org.signal.ringrtc.GroupCall;
import org.thoughtcrime.securesms.components.sensors.Orientation;
import org.thoughtcrime.securesms.components.webrtc.BroadcastVideoSink;
import org.thoughtcrime.securesms.components.webrtc.EglBaseWrapper;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.events.CallParticipantId;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.ringrtc.Camera;
import org.thoughtcrime.securesms.ringrtc.CameraState;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor;
import org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager;
import org.webrtc.PeerConnection;

/* loaded from: classes4.dex */
public class WebRtcServiceStateBuilder {
    private WebRtcServiceState toBuild;

    public WebRtcServiceStateBuilder(WebRtcServiceState webRtcServiceState) {
        this.toBuild = new WebRtcServiceState(webRtcServiceState);
    }

    public WebRtcServiceState build() {
        return this.toBuild;
    }

    public WebRtcServiceStateBuilder actionProcessor(WebRtcActionProcessor webRtcActionProcessor) {
        this.toBuild.actionProcessor = webRtcActionProcessor;
        return this;
    }

    public CallSetupStateBuilder changeCallSetupState(CallId callId) {
        return new CallSetupStateBuilder(callId);
    }

    public CallInfoStateBuilder changeCallInfoState() {
        return new CallInfoStateBuilder();
    }

    public LocalDeviceStateBuilder changeLocalDeviceState() {
        return new LocalDeviceStateBuilder();
    }

    public VideoStateBuilder changeVideoState() {
        return new VideoStateBuilder();
    }

    public WebRtcServiceStateBuilder terminate(CallId callId) {
        this.toBuild.localDeviceState = new LocalDeviceState();
        this.toBuild.videoState = new VideoState();
        CallInfoState callInfoState = new CallInfoState();
        callInfoState.getPeerMap().putAll(this.toBuild.callInfoState.getPeerMap());
        WebRtcServiceState webRtcServiceState = this.toBuild;
        webRtcServiceState.callInfoState = callInfoState;
        webRtcServiceState.callSetupStates.remove(callId);
        return this;
    }

    /* loaded from: classes4.dex */
    public class LocalDeviceStateBuilder {
        private LocalDeviceState toBuild;

        public LocalDeviceStateBuilder() {
            WebRtcServiceStateBuilder.this = r1;
            this.toBuild = r1.toBuild.localDeviceState.duplicate();
        }

        public WebRtcServiceStateBuilder commit() {
            WebRtcServiceStateBuilder.this.toBuild.localDeviceState = this.toBuild;
            return WebRtcServiceStateBuilder.this;
        }

        public WebRtcServiceState build() {
            commit();
            return WebRtcServiceStateBuilder.this.build();
        }

        public LocalDeviceStateBuilder cameraState(CameraState cameraState) {
            this.toBuild.setCameraState(cameraState);
            return this;
        }

        public LocalDeviceStateBuilder isMicrophoneEnabled(boolean z) {
            this.toBuild.setMicrophoneEnabled(z);
            return this;
        }

        public LocalDeviceStateBuilder setOrientation(Orientation orientation) {
            this.toBuild.setOrientation(orientation);
            return this;
        }

        public LocalDeviceStateBuilder setLandscapeEnabled(boolean z) {
            this.toBuild.setLandscapeEnabled(z);
            return this;
        }

        public LocalDeviceStateBuilder setDeviceOrientation(Orientation orientation) {
            this.toBuild.setDeviceOrientation(orientation);
            return this;
        }

        public LocalDeviceStateBuilder setActiveDevice(SignalAudioManager.AudioDevice audioDevice) {
            this.toBuild.setActiveDevice(audioDevice);
            return this;
        }

        public LocalDeviceStateBuilder setAvailableDevices(Set<SignalAudioManager.AudioDevice> set) {
            this.toBuild.setAvailableDevices(set);
            return this;
        }

        public LocalDeviceStateBuilder setBluetoothPermissionDenied(boolean z) {
            this.toBuild.setBluetoothPermissionDenied(z);
            return this;
        }

        public LocalDeviceStateBuilder setNetworkConnectionType(PeerConnection.AdapterType adapterType) {
            this.toBuild.setNetworkConnectionType(adapterType);
            return this;
        }
    }

    /* loaded from: classes4.dex */
    public class CallSetupStateBuilder {
        private CallId callId;
        private CallSetupState toBuild;

        public CallSetupStateBuilder(CallId callId) {
            WebRtcServiceStateBuilder.this = r1;
            this.toBuild = r1.toBuild.getCallSetupState(callId).duplicate();
            this.callId = callId;
        }

        public WebRtcServiceStateBuilder commit() {
            WebRtcServiceStateBuilder.this.toBuild.callSetupStates.put(this.callId, this.toBuild);
            return WebRtcServiceStateBuilder.this;
        }

        public WebRtcServiceState build() {
            commit();
            return WebRtcServiceStateBuilder.this.build();
        }

        public CallSetupStateBuilder enableVideoOnCreate(boolean z) {
            this.toBuild.setEnableVideoOnCreate(z);
            return this;
        }

        public CallSetupStateBuilder isRemoteVideoOffer(boolean z) {
            this.toBuild.setRemoteVideoOffer(z);
            return this;
        }

        public CallSetupStateBuilder acceptWithVideo(boolean z) {
            this.toBuild.setAcceptWithVideo(z);
            return this;
        }

        public CallSetupStateBuilder sentJoinedMessage(boolean z) {
            this.toBuild.setSentJoinedMessage(z);
            return this;
        }

        public CallSetupStateBuilder setRingGroup(boolean z) {
            this.toBuild.setRingGroup(z);
            return this;
        }

        public CallSetupStateBuilder ringId(long j) {
            this.toBuild.setRingId(j);
            return this;
        }

        public CallSetupStateBuilder ringerRecipient(Recipient recipient) {
            this.toBuild.setRingerRecipient(recipient);
            return this;
        }

        public CallSetupStateBuilder waitForTelecom(boolean z) {
            this.toBuild.setWaitForTelecom(z);
            return this;
        }

        public CallSetupStateBuilder telecomApproved(boolean z) {
            this.toBuild.setTelecomApproved(z);
            return this;
        }

        public CallSetupStateBuilder iceServers(Collection<PeerConnection.IceServer> collection) {
            this.toBuild.getIceServers().clear();
            this.toBuild.getIceServers().addAll(collection);
            return this;
        }

        public CallSetupStateBuilder alwaysTurn(boolean z) {
            this.toBuild.setAlwaysTurnServers(z);
            return this;
        }
    }

    /* loaded from: classes4.dex */
    public class VideoStateBuilder {
        private VideoState toBuild;

        public VideoStateBuilder() {
            WebRtcServiceStateBuilder.this = r2;
            this.toBuild = new VideoState(r2.toBuild.videoState);
        }

        public WebRtcServiceStateBuilder commit() {
            WebRtcServiceStateBuilder.this.toBuild.videoState = this.toBuild;
            return WebRtcServiceStateBuilder.this;
        }

        public WebRtcServiceState build() {
            commit();
            return WebRtcServiceStateBuilder.this.build();
        }

        public VideoStateBuilder eglBase(EglBaseWrapper eglBaseWrapper) {
            this.toBuild.eglBase = eglBaseWrapper;
            return this;
        }

        public VideoStateBuilder localSink(BroadcastVideoSink broadcastVideoSink) {
            this.toBuild.localSink = broadcastVideoSink;
            return this;
        }

        public VideoStateBuilder camera(Camera camera) {
            this.toBuild.camera = camera;
            return this;
        }
    }

    /* loaded from: classes4.dex */
    public class CallInfoStateBuilder {
        private CallInfoState toBuild;

        public CallInfoStateBuilder() {
            WebRtcServiceStateBuilder.this = r1;
            this.toBuild = r1.toBuild.callInfoState.duplicate();
        }

        public WebRtcServiceStateBuilder commit() {
            WebRtcServiceStateBuilder.this.toBuild.callInfoState = this.toBuild;
            return WebRtcServiceStateBuilder.this;
        }

        public WebRtcServiceState build() {
            commit();
            return WebRtcServiceStateBuilder.this.build();
        }

        public CallInfoStateBuilder callState(WebRtcViewModel.State state) {
            this.toBuild.setCallState(state);
            return this;
        }

        public CallInfoStateBuilder callRecipient(Recipient recipient) {
            this.toBuild.setCallRecipient(recipient);
            return this;
        }

        public CallInfoStateBuilder callConnectedTime(long j) {
            this.toBuild.setCallConnectedTime(j);
            return this;
        }

        public CallInfoStateBuilder putParticipant(CallParticipantId callParticipantId, CallParticipant callParticipant) {
            this.toBuild.getRemoteCallParticipantsMap().put(callParticipantId, callParticipant);
            return this;
        }

        public CallInfoStateBuilder putParticipant(Recipient recipient, CallParticipant callParticipant) {
            this.toBuild.getRemoteCallParticipantsMap().put(new CallParticipantId(recipient), callParticipant);
            return this;
        }

        public CallInfoStateBuilder clearParticipantMap() {
            this.toBuild.getRemoteCallParticipantsMap().clear();
            return this;
        }

        public CallInfoStateBuilder putRemotePeer(RemotePeer remotePeer) {
            this.toBuild.getPeerMap().put(Integer.valueOf(remotePeer.hashCode()), remotePeer);
            return this;
        }

        public CallInfoStateBuilder clearPeerMap() {
            this.toBuild.getPeerMap().clear();
            return this;
        }

        public CallInfoStateBuilder removeRemotePeer(RemotePeer remotePeer) {
            this.toBuild.getPeerMap().remove(Integer.valueOf(remotePeer.hashCode()));
            return this;
        }

        public CallInfoStateBuilder activePeer(RemotePeer remotePeer) {
            this.toBuild.setActivePeer(remotePeer);
            return this;
        }

        public CallInfoStateBuilder groupCall(GroupCall groupCall) {
            this.toBuild.setGroupCall(groupCall);
            return this;
        }

        public CallInfoStateBuilder groupCallState(WebRtcViewModel.GroupCallState groupCallState) {
            this.toBuild.setGroupState(groupCallState);
            return this;
        }

        public CallInfoStateBuilder addIdentityChangedRecipients(Collection<RecipientId> collection) {
            this.toBuild.getIdentityChangedRecipients().addAll(collection);
            return this;
        }

        public CallInfoStateBuilder removeIdentityChangedRecipients(Collection<RecipientId> collection) {
            this.toBuild.getIdentityChangedRecipients().removeAll(collection);
            return this;
        }

        public CallInfoStateBuilder remoteDevicesCount(long j) {
            this.toBuild.setRemoteDevicesCount(OptionalLong.of(j));
            return this;
        }

        public CallInfoStateBuilder participantLimit(Long l) {
            this.toBuild.setParticipantLimit(l);
            return this;
        }
    }
}
