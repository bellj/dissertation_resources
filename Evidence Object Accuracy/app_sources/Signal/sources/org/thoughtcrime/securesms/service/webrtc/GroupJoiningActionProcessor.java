package org.thoughtcrime.securesms.service.webrtc;

import android.os.ResultReceiver;
import org.signal.core.util.logging.Log;
import org.signal.ringrtc.CallException;
import org.signal.ringrtc.GroupCall;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.ringrtc.Camera;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceState;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceStateBuilder;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.NetworkUtil;
import org.thoughtcrime.securesms.webrtc.locks.LockManager;

/* loaded from: classes4.dex */
public class GroupJoiningActionProcessor extends GroupActionProcessor {
    private static final String TAG = Log.tag(GroupJoiningActionProcessor.class);

    public GroupJoiningActionProcessor(WebRtcInteractor webRtcInteractor) {
        super(webRtcInteractor, TAG);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleIsInCallQuery(WebRtcServiceState webRtcServiceState, ResultReceiver resultReceiver) {
        if (resultReceiver != null) {
            resultReceiver.send(1, null);
        }
        return webRtcServiceState;
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.GroupActionProcessor, org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleGroupLocalDeviceStateChanged(WebRtcServiceState webRtcServiceState) {
        Log.i(this.tag, "handleGroupLocalDeviceStateChanged():");
        WebRtcServiceState handleGroupLocalDeviceStateChanged = super.handleGroupLocalDeviceStateChanged(webRtcServiceState);
        GroupCall requireGroupCall = handleGroupLocalDeviceStateChanged.getCallInfoState().requireGroupCall();
        GroupCall.LocalDeviceState localDeviceState = requireGroupCall.getLocalDeviceState();
        String str = this.tag;
        Log.i(str, "local device changed: " + localDeviceState.getConnectionState() + " " + localDeviceState.getJoinState());
        WebRtcServiceStateBuilder builder = handleGroupLocalDeviceStateChanged.builder();
        int i = AnonymousClass1.$SwitchMap$org$signal$ringrtc$GroupCall$ConnectionState[localDeviceState.getConnectionState().ordinal()];
        boolean z = true;
        if (i == 1 || i == 2) {
            builder.changeCallInfoState().groupCallState(WebRtcUtil.groupCallStateForConnection(localDeviceState.getConnectionState())).commit();
        } else if (i == 3 || i == 4) {
            if (localDeviceState.getJoinState() == GroupCall.JoinState.JOINED) {
                this.webRtcInteractor.setCallInProgressNotification(3, handleGroupLocalDeviceStateChanged.getCallInfoState().getCallRecipient());
                this.webRtcInteractor.startAudioCommunication();
                if (handleGroupLocalDeviceStateChanged.getLocalDeviceState().getCameraState().isEnabled()) {
                    this.webRtcInteractor.updatePhoneState(LockManager.PhoneState.IN_VIDEO);
                } else {
                    this.webRtcInteractor.updatePhoneState(WebRtcUtil.getInCallPhoneState(this.context));
                }
                try {
                    requireGroupCall.setOutgoingVideoMuted(!handleGroupLocalDeviceStateChanged.getLocalDeviceState().getCameraState().isEnabled());
                    if (handleGroupLocalDeviceStateChanged.getLocalDeviceState().isMicrophoneEnabled()) {
                        z = false;
                    }
                    requireGroupCall.setOutgoingAudioMuted(z);
                    requireGroupCall.setBandwidthMode(NetworkUtil.getCallingBandwidthMode(this.context, localDeviceState.getNetworkRoute().getLocalAdapterType()));
                    if (FeatureFlags.groupCallRinging() && handleGroupLocalDeviceStateChanged.getCallSetupState(RemotePeer.GROUP_CALL_ID).shouldRingGroup()) {
                        try {
                            requireGroupCall.ringAll();
                        } catch (CallException e) {
                            return groupCallFailure(handleGroupLocalDeviceStateChanged, "Unable to ring group", e);
                        }
                    }
                    builder.changeCallInfoState().callState(WebRtcViewModel.State.CALL_CONNECTED).groupCallState(WebRtcViewModel.GroupCallState.CONNECTED_AND_JOINED).callConnectedTime(System.currentTimeMillis()).commit().changeLocalDeviceState().commit().actionProcessor(new GroupConnectedActionProcessor(this.webRtcInteractor));
                } catch (CallException e2) {
                    Log.e(this.tag, e2);
                    throw new RuntimeException(e2);
                }
            } else if (localDeviceState.getJoinState() == GroupCall.JoinState.JOINING) {
                builder.changeCallInfoState().groupCallState(WebRtcViewModel.GroupCallState.CONNECTED_AND_JOINING).commit();
            } else {
                builder.changeCallInfoState().groupCallState(WebRtcUtil.groupCallStateForConnection(localDeviceState.getConnectionState())).commit();
            }
        }
        return builder.build();
    }

    /* renamed from: org.thoughtcrime.securesms.service.webrtc.GroupJoiningActionProcessor$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$signal$ringrtc$GroupCall$ConnectionState;

        static {
            int[] iArr = new int[GroupCall.ConnectionState.values().length];
            $SwitchMap$org$signal$ringrtc$GroupCall$ConnectionState = iArr;
            try {
                iArr[GroupCall.ConnectionState.NOT_CONNECTED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$GroupCall$ConnectionState[GroupCall.ConnectionState.RECONNECTING.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$GroupCall$ConnectionState[GroupCall.ConnectionState.CONNECTING.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$GroupCall$ConnectionState[GroupCall.ConnectionState.CONNECTED.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleLocalHangup(WebRtcServiceState webRtcServiceState) {
        Log.i(this.tag, "handleLocalHangup():");
        try {
            webRtcServiceState.getCallInfoState().requireGroupCall().disconnect();
            WebRtcServiceState build = webRtcServiceState.builder().changeCallInfoState().callState(WebRtcViewModel.State.CALL_DISCONNECTED).groupCallState(WebRtcViewModel.GroupCallState.DISCONNECTED).build();
            this.webRtcInteractor.postStateUpdate(build);
            return terminateGroupCall(build);
        } catch (CallException e) {
            return groupCallFailure(webRtcServiceState, "Unable to disconnect from group call", e);
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleSetEnableVideo(WebRtcServiceState webRtcServiceState, boolean z) {
        GroupCall requireGroupCall = webRtcServiceState.getCallInfoState().requireGroupCall();
        Camera requireCamera = webRtcServiceState.getVideoState().requireCamera();
        try {
            requireGroupCall.setOutgoingVideoMuted(!z);
            requireCamera.setEnabled(z);
            WebRtcServiceState build = webRtcServiceState.builder().changeLocalDeviceState().cameraState(requireCamera.getCameraState()).build();
            WebRtcUtil.enableSpeakerPhoneIfNeeded(this.webRtcInteractor, build);
            return build;
        } catch (CallException e) {
            return groupCallFailure(webRtcServiceState, "Unable to set video muted", e);
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleSetMuteAudio(WebRtcServiceState webRtcServiceState, boolean z) {
        try {
            webRtcServiceState.getCallInfoState().requireGroupCall().setOutgoingAudioMuted(z);
            return webRtcServiceState.builder().changeLocalDeviceState().isMicrophoneEnabled(!z).build();
        } catch (CallException e) {
            return groupCallFailure(webRtcServiceState, "Unable to set audio muted", e);
        }
    }
}
