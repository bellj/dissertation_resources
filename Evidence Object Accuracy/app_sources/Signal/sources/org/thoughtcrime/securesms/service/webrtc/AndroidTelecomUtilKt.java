package org.thoughtcrime.securesms.service.webrtc;

import android.net.Uri;
import android.telecom.Connection;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.IntRange;
import kotlin.text.StringsKt__StringsKt;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: AndroidTelecomUtil.kt */
@Metadata(d1 = {"\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u001a\f\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0002\u001a\u0014\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0003¨\u0006\b"}, d2 = {"generateTelecomE164", "Landroid/net/Uri;", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "setAudioRouteIfDifferent", "", "Landroid/telecom/Connection;", "newRoute", "", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AndroidTelecomUtilKt {
    public static final void setAudioRouteIfDifferent(Connection connection, int i) {
        if (connection.getCallAudioState().getRoute() != i) {
            connection.setAudioRoute(i);
        }
    }

    public static final Uri generateTelecomE164(RecipientId recipientId) {
        String obj = StringsKt__StringsKt.replaceRange(StringsKt__StringsKt.padEnd(String.valueOf(recipientId.toLong()), 10, '0'), new IntRange(3, 5), "555").toString();
        Uri fromParts = Uri.fromParts("tel", "+1" + obj, null);
        Intrinsics.checkNotNullExpressionValue(fromParts, "fromParts(\"tel\", \"+1$pseudoNumber\", null)");
        return fromParts;
    }
}
