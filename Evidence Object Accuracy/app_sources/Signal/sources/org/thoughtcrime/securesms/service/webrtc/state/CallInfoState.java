package org.thoughtcrime.securesms.service.webrtc.state;

import com.annimon.stream.OptionalLong;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.signal.ringrtc.CallId;
import org.signal.ringrtc.GroupCall;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.events.CallParticipantId;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;

/* compiled from: CallInfoState.kt */
@Metadata(d1 = {"\u0000r\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b(\n\u0002\u0010 \n\u0002\b\u0017\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0001\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\u0014\b\u0002\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t\u0012\u0014\b\u0002\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\t\u0012\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u000e\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0011\u0012\b\b\u0002\u0010\u0012\u001a\u00020\u0013\u0012\u000e\b\u0002\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015\u0012\b\b\u0002\u0010\u0017\u001a\u00020\u0018\u0012\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u0007¢\u0006\u0002\u0010\u001aJ\t\u0010J\u001a\u00020\u0003HÆ\u0003J\t\u0010K\u001a\u00020\u0018HÆ\u0003J\u0010\u0010L\u001a\u0004\u0018\u00010\u0007HÆ\u0003¢\u0006\u0002\u00108J\t\u0010M\u001a\u00020\u0005HÆ\u0003J\t\u0010N\u001a\u00020\u0007HÆ\u0003J\u0015\u0010O\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tHÆ\u0003J\u0015\u0010P\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\tHÆ\u0003J\u000b\u0010Q\u001a\u0004\u0018\u00010\u000eHÆ\u0003J\u000b\u0010R\u001a\u0004\u0018\u00010\u0011HÆ\u0003J\t\u0010S\u001a\u00020\u0013HÆ\u0003J\u000f\u0010T\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015HÆ\u0003J \u0001\u0010U\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\u0014\b\u0002\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t2\u0014\b\u0002\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\t2\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u000e2\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00112\b\b\u0002\u0010\u0012\u001a\u00020\u00132\u000e\b\u0002\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00160\u00152\b\b\u0002\u0010\u0017\u001a\u00020\u00182\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u0007HÆ\u0001¢\u0006\u0002\u0010VJ\u0006\u0010W\u001a\u00020\u0000J\u0013\u0010X\u001a\u00020Y2\b\u0010Z\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\u0010\u0010[\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\\\u001a\u00020\rJ\u0010\u0010]\u001a\u0004\u0018\u00010\u000e2\u0006\u0010^\u001a\u00020_J\u0010\u0010`\u001a\u0004\u0018\u00010\u000b2\u0006\u0010a\u001a\u00020\nJ\u0010\u0010`\u001a\u0004\u0018\u00010\u000b2\u0006\u0010b\u001a\u00020\u0005J\t\u0010\\\u001a\u00020\rHÖ\u0001J\u0006\u0010c\u001a\u00020\u000eJ\u0006\u0010d\u001a\u00020\u0011J\t\u0010e\u001a\u00020fHÖ\u0001R\u001c\u0010\u000f\u001a\u0004\u0018\u00010\u000eX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001eR\u001a\u0010\u0006\u001a\u00020\u0007X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R\u001a\u0010\u0004\u001a\u00020\u0005X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b#\u0010$\"\u0004\b%\u0010&R\u001a\u0010\u0002\u001a\u00020\u0003X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b'\u0010(\"\u0004\b)\u0010*R\u001c\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b+\u0010,\"\u0004\b-\u0010.R\u001c\u0010\u0012\u001a\u00020\u00138\u0007X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b/\u00100\"\u0004\b1\u00102R \u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b3\u00104\"\u0004\b5\u00106R\u001e\u0010\u0019\u001a\u0004\u0018\u00010\u0007X\u000e¢\u0006\u0010\n\u0002\u0010;\u001a\u0004\b7\u00108\"\u0004\b9\u0010:R&\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\tX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b<\u0010=\"\u0004\b>\u0010?R\u0017\u0010@\u001a\b\u0012\u0004\u0012\u00020\u000b0A8F¢\u0006\u0006\u001a\u0004\bB\u0010CR\u001a\u0010\u0017\u001a\u00020\u0018X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bD\u0010E\"\u0004\bF\u0010GR(\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t8\u0007X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bH\u0010=\"\u0004\bI\u0010?¨\u0006g"}, d2 = {"Lorg/thoughtcrime/securesms/service/webrtc/state/CallInfoState;", "", "callState", "Lorg/thoughtcrime/securesms/events/WebRtcViewModel$State;", "callRecipient", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "callConnectedTime", "", "remoteParticipants", "", "Lorg/thoughtcrime/securesms/events/CallParticipantId;", "Lorg/thoughtcrime/securesms/events/CallParticipant;", "peerMap", "", "Lorg/thoughtcrime/securesms/ringrtc/RemotePeer;", "activePeer", "groupCall", "Lorg/signal/ringrtc/GroupCall;", "groupState", "Lorg/thoughtcrime/securesms/events/WebRtcViewModel$GroupCallState;", "identityChangedRecipients", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "remoteDevicesCount", "Lcom/annimon/stream/OptionalLong;", "participantLimit", "(Lorg/thoughtcrime/securesms/events/WebRtcViewModel$State;Lorg/thoughtcrime/securesms/recipients/Recipient;JLjava/util/Map;Ljava/util/Map;Lorg/thoughtcrime/securesms/ringrtc/RemotePeer;Lorg/signal/ringrtc/GroupCall;Lorg/thoughtcrime/securesms/events/WebRtcViewModel$GroupCallState;Ljava/util/Set;Lcom/annimon/stream/OptionalLong;Ljava/lang/Long;)V", "getActivePeer", "()Lorg/thoughtcrime/securesms/ringrtc/RemotePeer;", "setActivePeer", "(Lorg/thoughtcrime/securesms/ringrtc/RemotePeer;)V", "getCallConnectedTime", "()J", "setCallConnectedTime", "(J)V", "getCallRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "setCallRecipient", "(Lorg/thoughtcrime/securesms/recipients/Recipient;)V", "getCallState", "()Lorg/thoughtcrime/securesms/events/WebRtcViewModel$State;", "setCallState", "(Lorg/thoughtcrime/securesms/events/WebRtcViewModel$State;)V", "getGroupCall", "()Lorg/signal/ringrtc/GroupCall;", "setGroupCall", "(Lorg/signal/ringrtc/GroupCall;)V", "getGroupCallState", "()Lorg/thoughtcrime/securesms/events/WebRtcViewModel$GroupCallState;", "setGroupState", "(Lorg/thoughtcrime/securesms/events/WebRtcViewModel$GroupCallState;)V", "getIdentityChangedRecipients", "()Ljava/util/Set;", "setIdentityChangedRecipients", "(Ljava/util/Set;)V", "getParticipantLimit", "()Ljava/lang/Long;", "setParticipantLimit", "(Ljava/lang/Long;)V", "Ljava/lang/Long;", "getPeerMap", "()Ljava/util/Map;", "setPeerMap", "(Ljava/util/Map;)V", "remoteCallParticipants", "", "getRemoteCallParticipants", "()Ljava/util/List;", "getRemoteDevicesCount", "()Lcom/annimon/stream/OptionalLong;", "setRemoteDevicesCount", "(Lcom/annimon/stream/OptionalLong;)V", "getRemoteCallParticipantsMap", "setRemoteParticipants", "component1", "component10", "component11", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(Lorg/thoughtcrime/securesms/events/WebRtcViewModel$State;Lorg/thoughtcrime/securesms/recipients/Recipient;JLjava/util/Map;Ljava/util/Map;Lorg/thoughtcrime/securesms/ringrtc/RemotePeer;Lorg/signal/ringrtc/GroupCall;Lorg/thoughtcrime/securesms/events/WebRtcViewModel$GroupCallState;Ljava/util/Set;Lcom/annimon/stream/OptionalLong;Ljava/lang/Long;)Lorg/thoughtcrime/securesms/service/webrtc/state/CallInfoState;", "duplicate", "equals", "", "other", "getPeer", "hashCode", "getPeerByCallId", "callId", "Lorg/signal/ringrtc/CallId;", "getRemoteCallParticipant", "callParticipantId", RecipientDatabase.TABLE_NAME, "requireActivePeer", "requireGroupCall", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CallInfoState {
    private RemotePeer activePeer;
    private long callConnectedTime;
    private Recipient callRecipient;
    private WebRtcViewModel.State callState;
    private GroupCall groupCall;
    private WebRtcViewModel.GroupCallState groupState;
    private Set<RecipientId> identityChangedRecipients;
    private Long participantLimit;
    private Map<Integer, RemotePeer> peerMap;
    private OptionalLong remoteDevicesCount;
    private Map<CallParticipantId, CallParticipant> remoteParticipants;

    public CallInfoState() {
        this(null, null, 0, null, null, null, null, null, null, null, null, 2047, null);
    }

    public static /* synthetic */ CallInfoState copy$default(CallInfoState callInfoState, WebRtcViewModel.State state, Recipient recipient, long j, Map map, Map map2, RemotePeer remotePeer, GroupCall groupCall, WebRtcViewModel.GroupCallState groupCallState, Set set, OptionalLong optionalLong, Long l, int i, Object obj) {
        return callInfoState.copy((i & 1) != 0 ? callInfoState.callState : state, (i & 2) != 0 ? callInfoState.callRecipient : recipient, (i & 4) != 0 ? callInfoState.callConnectedTime : j, (i & 8) != 0 ? callInfoState.remoteParticipants : map, (i & 16) != 0 ? callInfoState.peerMap : map2, (i & 32) != 0 ? callInfoState.activePeer : remotePeer, (i & 64) != 0 ? callInfoState.groupCall : groupCall, (i & 128) != 0 ? callInfoState.groupState : groupCallState, (i & 256) != 0 ? callInfoState.identityChangedRecipients : set, (i & 512) != 0 ? callInfoState.remoteDevicesCount : optionalLong, (i & 1024) != 0 ? callInfoState.participantLimit : l);
    }

    public final WebRtcViewModel.State component1() {
        return this.callState;
    }

    public final OptionalLong component10() {
        return this.remoteDevicesCount;
    }

    public final Long component11() {
        return this.participantLimit;
    }

    public final Recipient component2() {
        return this.callRecipient;
    }

    public final long component3() {
        return this.callConnectedTime;
    }

    public final Map<CallParticipantId, CallParticipant> component4() {
        return this.remoteParticipants;
    }

    public final Map<Integer, RemotePeer> component5() {
        return this.peerMap;
    }

    public final RemotePeer component6() {
        return this.activePeer;
    }

    public final GroupCall component7() {
        return this.groupCall;
    }

    public final WebRtcViewModel.GroupCallState component8() {
        return this.groupState;
    }

    public final Set<RecipientId> component9() {
        return this.identityChangedRecipients;
    }

    public final CallInfoState copy(WebRtcViewModel.State state, Recipient recipient, long j, Map<CallParticipantId, CallParticipant> map, Map<Integer, RemotePeer> map2, RemotePeer remotePeer, GroupCall groupCall, WebRtcViewModel.GroupCallState groupCallState, Set<RecipientId> set, OptionalLong optionalLong, Long l) {
        Intrinsics.checkNotNullParameter(state, "callState");
        Intrinsics.checkNotNullParameter(recipient, "callRecipient");
        Intrinsics.checkNotNullParameter(map, "remoteParticipants");
        Intrinsics.checkNotNullParameter(map2, "peerMap");
        Intrinsics.checkNotNullParameter(groupCallState, "groupState");
        Intrinsics.checkNotNullParameter(set, "identityChangedRecipients");
        Intrinsics.checkNotNullParameter(optionalLong, "remoteDevicesCount");
        return new CallInfoState(state, recipient, j, map, map2, remotePeer, groupCall, groupCallState, set, optionalLong, l);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CallInfoState)) {
            return false;
        }
        CallInfoState callInfoState = (CallInfoState) obj;
        return this.callState == callInfoState.callState && Intrinsics.areEqual(this.callRecipient, callInfoState.callRecipient) && this.callConnectedTime == callInfoState.callConnectedTime && Intrinsics.areEqual(this.remoteParticipants, callInfoState.remoteParticipants) && Intrinsics.areEqual(this.peerMap, callInfoState.peerMap) && Intrinsics.areEqual(this.activePeer, callInfoState.activePeer) && Intrinsics.areEqual(this.groupCall, callInfoState.groupCall) && this.groupState == callInfoState.groupState && Intrinsics.areEqual(this.identityChangedRecipients, callInfoState.identityChangedRecipients) && Intrinsics.areEqual(this.remoteDevicesCount, callInfoState.remoteDevicesCount) && Intrinsics.areEqual(this.participantLimit, callInfoState.participantLimit);
    }

    public int hashCode() {
        int hashCode = ((((((((this.callState.hashCode() * 31) + this.callRecipient.hashCode()) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.callConnectedTime)) * 31) + this.remoteParticipants.hashCode()) * 31) + this.peerMap.hashCode()) * 31;
        RemotePeer remotePeer = this.activePeer;
        int i = 0;
        int hashCode2 = (hashCode + (remotePeer == null ? 0 : remotePeer.hashCode())) * 31;
        GroupCall groupCall = this.groupCall;
        int hashCode3 = (((((((hashCode2 + (groupCall == null ? 0 : groupCall.hashCode())) * 31) + this.groupState.hashCode()) * 31) + this.identityChangedRecipients.hashCode()) * 31) + this.remoteDevicesCount.hashCode()) * 31;
        Long l = this.participantLimit;
        if (l != null) {
            i = l.hashCode();
        }
        return hashCode3 + i;
    }

    public String toString() {
        return "CallInfoState(callState=" + this.callState + ", callRecipient=" + this.callRecipient + ", callConnectedTime=" + this.callConnectedTime + ", remoteParticipants=" + this.remoteParticipants + ", peerMap=" + this.peerMap + ", activePeer=" + this.activePeer + ", groupCall=" + this.groupCall + ", groupState=" + this.groupState + ", identityChangedRecipients=" + this.identityChangedRecipients + ", remoteDevicesCount=" + this.remoteDevicesCount + ", participantLimit=" + this.participantLimit + ')';
    }

    public CallInfoState(WebRtcViewModel.State state, Recipient recipient, long j, Map<CallParticipantId, CallParticipant> map, Map<Integer, RemotePeer> map2, RemotePeer remotePeer, GroupCall groupCall, WebRtcViewModel.GroupCallState groupCallState, Set<RecipientId> set, OptionalLong optionalLong, Long l) {
        Intrinsics.checkNotNullParameter(state, "callState");
        Intrinsics.checkNotNullParameter(recipient, "callRecipient");
        Intrinsics.checkNotNullParameter(map, "remoteParticipants");
        Intrinsics.checkNotNullParameter(map2, "peerMap");
        Intrinsics.checkNotNullParameter(groupCallState, "groupState");
        Intrinsics.checkNotNullParameter(set, "identityChangedRecipients");
        Intrinsics.checkNotNullParameter(optionalLong, "remoteDevicesCount");
        this.callState = state;
        this.callRecipient = recipient;
        this.callConnectedTime = j;
        this.remoteParticipants = map;
        this.peerMap = map2;
        this.activePeer = remotePeer;
        this.groupCall = groupCall;
        this.groupState = groupCallState;
        this.identityChangedRecipients = set;
        this.remoteDevicesCount = optionalLong;
        this.participantLimit = l;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ CallInfoState(org.thoughtcrime.securesms.events.WebRtcViewModel.State r15, org.thoughtcrime.securesms.recipients.Recipient r16, long r17, java.util.Map r19, java.util.Map r20, org.thoughtcrime.securesms.ringrtc.RemotePeer r21, org.signal.ringrtc.GroupCall r22, org.thoughtcrime.securesms.events.WebRtcViewModel.GroupCallState r23, java.util.Set r24, com.annimon.stream.OptionalLong r25, java.lang.Long r26, int r27, kotlin.jvm.internal.DefaultConstructorMarker r28) {
        /*
            r14 = this;
            r0 = r27
            r1 = r0 & 1
            if (r1 == 0) goto L_0x0009
            org.thoughtcrime.securesms.events.WebRtcViewModel$State r1 = org.thoughtcrime.securesms.events.WebRtcViewModel.State.IDLE
            goto L_0x000a
        L_0x0009:
            r1 = r15
        L_0x000a:
            r2 = r0 & 2
            if (r2 == 0) goto L_0x0016
            org.thoughtcrime.securesms.recipients.Recipient r2 = org.thoughtcrime.securesms.recipients.Recipient.UNKNOWN
            java.lang.String r3 = "UNKNOWN"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r2, r3)
            goto L_0x0018
        L_0x0016:
            r2 = r16
        L_0x0018:
            r3 = r0 & 4
            if (r3 == 0) goto L_0x001f
            r3 = -1
            goto L_0x0021
        L_0x001f:
            r3 = r17
        L_0x0021:
            r5 = r0 & 8
            if (r5 == 0) goto L_0x002b
            java.util.LinkedHashMap r5 = new java.util.LinkedHashMap
            r5.<init>()
            goto L_0x002d
        L_0x002b:
            r5 = r19
        L_0x002d:
            r6 = r0 & 16
            if (r6 == 0) goto L_0x0037
            java.util.LinkedHashMap r6 = new java.util.LinkedHashMap
            r6.<init>()
            goto L_0x0039
        L_0x0037:
            r6 = r20
        L_0x0039:
            r7 = r0 & 32
            r8 = 0
            if (r7 == 0) goto L_0x0040
            r7 = r8
            goto L_0x0042
        L_0x0040:
            r7 = r21
        L_0x0042:
            r9 = r0 & 64
            if (r9 == 0) goto L_0x0048
            r9 = r8
            goto L_0x004a
        L_0x0048:
            r9 = r22
        L_0x004a:
            r10 = r0 & 128(0x80, float:1.794E-43)
            if (r10 == 0) goto L_0x0051
            org.thoughtcrime.securesms.events.WebRtcViewModel$GroupCallState r10 = org.thoughtcrime.securesms.events.WebRtcViewModel.GroupCallState.IDLE
            goto L_0x0053
        L_0x0051:
            r10 = r23
        L_0x0053:
            r11 = r0 & 256(0x100, float:3.59E-43)
            if (r11 == 0) goto L_0x005d
            java.util.LinkedHashSet r11 = new java.util.LinkedHashSet
            r11.<init>()
            goto L_0x005f
        L_0x005d:
            r11 = r24
        L_0x005f:
            r12 = r0 & 512(0x200, float:7.175E-43)
            if (r12 == 0) goto L_0x006d
            com.annimon.stream.OptionalLong r12 = com.annimon.stream.OptionalLong.empty()
            java.lang.String r13 = "empty()"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r12, r13)
            goto L_0x006f
        L_0x006d:
            r12 = r25
        L_0x006f:
            r0 = r0 & 1024(0x400, float:1.435E-42)
            if (r0 == 0) goto L_0x0074
            goto L_0x0076
        L_0x0074:
            r8 = r26
        L_0x0076:
            r15 = r14
            r16 = r1
            r17 = r2
            r18 = r3
            r20 = r5
            r21 = r6
            r22 = r7
            r23 = r9
            r24 = r10
            r25 = r11
            r26 = r12
            r27 = r8
            r15.<init>(r16, r17, r18, r20, r21, r22, r23, r24, r25, r26, r27)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.service.webrtc.state.CallInfoState.<init>(org.thoughtcrime.securesms.events.WebRtcViewModel$State, org.thoughtcrime.securesms.recipients.Recipient, long, java.util.Map, java.util.Map, org.thoughtcrime.securesms.ringrtc.RemotePeer, org.signal.ringrtc.GroupCall, org.thoughtcrime.securesms.events.WebRtcViewModel$GroupCallState, java.util.Set, com.annimon.stream.OptionalLong, java.lang.Long, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final WebRtcViewModel.State getCallState() {
        return this.callState;
    }

    public final void setCallState(WebRtcViewModel.State state) {
        Intrinsics.checkNotNullParameter(state, "<set-?>");
        this.callState = state;
    }

    public final Recipient getCallRecipient() {
        return this.callRecipient;
    }

    public final void setCallRecipient(Recipient recipient) {
        Intrinsics.checkNotNullParameter(recipient, "<set-?>");
        this.callRecipient = recipient;
    }

    public final long getCallConnectedTime() {
        return this.callConnectedTime;
    }

    public final void setCallConnectedTime(long j) {
        this.callConnectedTime = j;
    }

    public final Map<CallParticipantId, CallParticipant> getRemoteCallParticipantsMap() {
        return this.remoteParticipants;
    }

    public final void setRemoteParticipants(Map<CallParticipantId, CallParticipant> map) {
        Intrinsics.checkNotNullParameter(map, "<set-?>");
        this.remoteParticipants = map;
    }

    public final Map<Integer, RemotePeer> getPeerMap() {
        return this.peerMap;
    }

    public final void setPeerMap(Map<Integer, RemotePeer> map) {
        Intrinsics.checkNotNullParameter(map, "<set-?>");
        this.peerMap = map;
    }

    public final RemotePeer getActivePeer() {
        return this.activePeer;
    }

    public final void setActivePeer(RemotePeer remotePeer) {
        this.activePeer = remotePeer;
    }

    public final GroupCall getGroupCall() {
        return this.groupCall;
    }

    public final void setGroupCall(GroupCall groupCall) {
        this.groupCall = groupCall;
    }

    public final WebRtcViewModel.GroupCallState getGroupCallState() {
        return this.groupState;
    }

    public final void setGroupState(WebRtcViewModel.GroupCallState groupCallState) {
        Intrinsics.checkNotNullParameter(groupCallState, "<set-?>");
        this.groupState = groupCallState;
    }

    public final Set<RecipientId> getIdentityChangedRecipients() {
        return this.identityChangedRecipients;
    }

    public final void setIdentityChangedRecipients(Set<RecipientId> set) {
        Intrinsics.checkNotNullParameter(set, "<set-?>");
        this.identityChangedRecipients = set;
    }

    public final OptionalLong getRemoteDevicesCount() {
        return this.remoteDevicesCount;
    }

    public final void setRemoteDevicesCount(OptionalLong optionalLong) {
        Intrinsics.checkNotNullParameter(optionalLong, "<set-?>");
        this.remoteDevicesCount = optionalLong;
    }

    public final Long getParticipantLimit() {
        return this.participantLimit;
    }

    public final void setParticipantLimit(Long l) {
        this.participantLimit = l;
    }

    public final List<CallParticipant> getRemoteCallParticipants() {
        return new ArrayList(this.remoteParticipants.values());
    }

    public final CallParticipant getRemoteCallParticipant(Recipient recipient) {
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        return getRemoteCallParticipant(new CallParticipantId(recipient));
    }

    public final CallParticipant getRemoteCallParticipant(CallParticipantId callParticipantId) {
        Intrinsics.checkNotNullParameter(callParticipantId, "callParticipantId");
        return this.remoteParticipants.get(callParticipantId);
    }

    public final RemotePeer getPeer(int i) {
        return this.peerMap.get(Integer.valueOf(i));
    }

    public final RemotePeer getPeerByCallId(CallId callId) {
        Object obj;
        Intrinsics.checkNotNullParameter(callId, "callId");
        Iterator<T> it = this.peerMap.values().iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (Intrinsics.areEqual(((RemotePeer) obj).getCallId(), callId)) {
                break;
            }
        }
        return (RemotePeer) obj;
    }

    public final RemotePeer requireActivePeer() {
        RemotePeer remotePeer = this.activePeer;
        Intrinsics.checkNotNull(remotePeer);
        return remotePeer;
    }

    public final GroupCall requireGroupCall() {
        GroupCall groupCall = this.groupCall;
        Intrinsics.checkNotNull(groupCall);
        return groupCall;
    }

    public final CallInfoState duplicate() {
        return copy$default(this, null, null, 0, MapsKt__MapsKt.toMutableMap(this.remoteParticipants), MapsKt__MapsKt.toMutableMap(this.peerMap), null, null, null, CollectionsKt___CollectionsKt.toMutableSet(this.identityChangedRecipients), null, null, 1767, null);
    }
}
