package org.thoughtcrime.securesms.service.webrtc;

import org.signal.core.util.logging.Log;
import org.signal.ringrtc.CallException;
import org.signal.ringrtc.CallManager;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.ringrtc.Camera;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceState;
import org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager;
import org.thoughtcrime.securesms.webrtc.locks.LockManager;

/* loaded from: classes4.dex */
public class CallSetupActionProcessorDelegate extends WebRtcActionProcessor {
    public CallSetupActionProcessorDelegate(WebRtcInteractor webRtcInteractor, String str) {
        super(webRtcInteractor, str);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleCallConnected(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer) {
        if (!remotePeer.callIdEquals(webRtcServiceState.getCallInfoState().getActivePeer())) {
            Log.w(this.tag, "handleCallConnected(): Ignoring for inactive call.");
            return webRtcServiceState;
        }
        String str = this.tag;
        Log.i(str, "handleCallConnected(): call_id: " + remotePeer.getCallId());
        RemotePeer requireActivePeer = webRtcServiceState.getCallInfoState().requireActivePeer();
        ApplicationDependencies.getAppForegroundObserver().removeListener(this.webRtcInteractor.getForegroundListener());
        this.webRtcInteractor.startAudioCommunication();
        this.webRtcInteractor.activateCall(requireActivePeer.getId());
        requireActivePeer.connected();
        if (webRtcServiceState.getLocalDeviceState().getCameraState().isEnabled()) {
            this.webRtcInteractor.updatePhoneState(LockManager.PhoneState.IN_VIDEO);
        } else {
            this.webRtcInteractor.updatePhoneState(WebRtcUtil.getInCallPhoneState(this.context));
        }
        WebRtcServiceState build = webRtcServiceState.builder().actionProcessor(new ConnectedCallActionProcessor(this.webRtcInteractor)).changeCallInfoState().callState(WebRtcViewModel.State.CALL_CONNECTED).callConnectedTime(System.currentTimeMillis()).commit().changeLocalDeviceState().build();
        this.webRtcInteractor.setCallInProgressNotification(3, requireActivePeer);
        this.webRtcInteractor.unregisterPowerButtonReceiver();
        try {
            CallManager callManager = this.webRtcInteractor.getCallManager();
            callManager.setAudioEnable(build.getLocalDeviceState().isMicrophoneEnabled());
            callManager.setVideoEnable(build.getLocalDeviceState().getCameraState().isEnabled());
            if (build.getCallSetupState(requireActivePeer).isAcceptWithVideo()) {
                build = build.getActionProcessor().handleSetEnableVideo(build, true);
            }
            if (build.getCallSetupState(requireActivePeer).isAcceptWithVideo() || build.getLocalDeviceState().getCameraState().isEnabled()) {
                this.webRtcInteractor.setDefaultAudioDevice(requireActivePeer.getId(), SignalAudioManager.AudioDevice.SPEAKER_PHONE, false);
            } else {
                this.webRtcInteractor.setDefaultAudioDevice(requireActivePeer.getId(), SignalAudioManager.AudioDevice.EARPIECE, false);
            }
            return build;
        } catch (CallException e) {
            return callFailure(build, "Enabling audio/video failed: ", e);
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleSetEnableVideo(WebRtcServiceState webRtcServiceState, boolean z) {
        String str = this.tag;
        Log.i(str, "handleSetEnableVideo(): enable: " + z);
        Camera requireCamera = webRtcServiceState.getVideoState().requireCamera();
        if (requireCamera.isInitialized()) {
            requireCamera.setEnabled(z);
        }
        WebRtcServiceState build = webRtcServiceState.builder().changeLocalDeviceState().cameraState(requireCamera.getCameraState()).build();
        if ((z && requireCamera.isInitialized()) || !z) {
            try {
                this.webRtcInteractor.getCallManager().setVideoEnable(z);
            } catch (CallException e) {
                String str2 = this.tag;
                Log.w(str2, "Unable change video enabled state to " + z, e);
            }
        }
        WebRtcUtil.enableSpeakerPhoneIfNeeded(this.webRtcInteractor, build);
        return build;
    }
}
