package org.thoughtcrime.securesms.service.webrtc;

import android.content.Context;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.ecc.Curve;
import org.signal.ringrtc.CallManager;
import org.signal.ringrtc.GroupCall;
import org.signal.ringrtc.PeekInfo;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceState;
import org.thoughtcrime.securesms.webrtc.audio.AudioManagerCompat;
import org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager;
import org.thoughtcrime.securesms.webrtc.locks.LockManager;
import org.whispersystems.signalservice.api.messages.calls.HangupMessage;
import org.whispersystems.signalservice.api.messages.calls.OfferMessage;
import org.whispersystems.signalservice.api.messages.calls.OpaqueMessage;

/* loaded from: classes4.dex */
public final class WebRtcUtil {
    private WebRtcUtil() {
    }

    public static byte[] getPublicKeyBytes(byte[] bArr) throws InvalidKeyException {
        return Curve.decodePoint(bArr, 0).getPublicKeyBytes();
    }

    public static LockManager.PhoneState getInCallPhoneState(Context context) {
        AudioManagerCompat androidCallAudioManager = ApplicationDependencies.getAndroidCallAudioManager();
        if (androidCallAudioManager.isSpeakerphoneOn() || androidCallAudioManager.isBluetoothScoOn() || androidCallAudioManager.isWiredHeadsetOn()) {
            return LockManager.PhoneState.IN_HANDS_FREE_CALL;
        }
        return LockManager.PhoneState.IN_CALL;
    }

    public static CallManager.CallMediaType getCallMediaTypeFromOfferType(OfferMessage.Type type) {
        return type == OfferMessage.Type.VIDEO_CALL ? CallManager.CallMediaType.VIDEO_CALL : CallManager.CallMediaType.AUDIO_CALL;
    }

    public static OfferMessage.Type getOfferTypeFromCallMediaType(CallManager.CallMediaType callMediaType) {
        return callMediaType == CallManager.CallMediaType.VIDEO_CALL ? OfferMessage.Type.VIDEO_CALL : OfferMessage.Type.AUDIO_CALL;
    }

    public static HangupMessage.Type getHangupTypeFromCallHangupType(CallManager.HangupType hangupType) {
        int i = AnonymousClass1.$SwitchMap$org$signal$ringrtc$CallManager$HangupType[hangupType.ordinal()];
        if (i == 1) {
            return HangupMessage.Type.ACCEPTED;
        }
        if (i == 2) {
            return HangupMessage.Type.BUSY;
        }
        if (i == 3) {
            return HangupMessage.Type.NORMAL;
        }
        if (i == 4) {
            return HangupMessage.Type.DECLINED;
        }
        if (i == 5) {
            return HangupMessage.Type.NEED_PERMISSION;
        }
        throw new IllegalArgumentException("Unexpected hangup type: " + hangupType);
    }

    public static OpaqueMessage.Urgency getUrgencyFromCallUrgency(CallManager.CallMessageUrgency callMessageUrgency) {
        if (callMessageUrgency == CallManager.CallMessageUrgency.HANDLE_IMMEDIATELY) {
            return OpaqueMessage.Urgency.HANDLE_IMMEDIATELY;
        }
        return OpaqueMessage.Urgency.DROPPABLE;
    }

    public static void enableSpeakerPhoneIfNeeded(WebRtcInteractor webRtcInteractor, WebRtcServiceState webRtcServiceState) {
        if (webRtcServiceState.getLocalDeviceState().getCameraState().isEnabled()) {
            if (webRtcServiceState.getLocalDeviceState().getActiveDevice() == SignalAudioManager.AudioDevice.EARPIECE || (webRtcServiceState.getLocalDeviceState().getActiveDevice() == SignalAudioManager.AudioDevice.NONE && webRtcServiceState.getCallInfoState().getActivePeer() != null)) {
                webRtcInteractor.setDefaultAudioDevice(webRtcServiceState.getCallInfoState().requireActivePeer().getId(), SignalAudioManager.AudioDevice.SPEAKER_PHONE, true);
            }
        }
    }

    /* renamed from: org.thoughtcrime.securesms.service.webrtc.WebRtcUtil$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$signal$ringrtc$CallManager$HangupType;
        static final /* synthetic */ int[] $SwitchMap$org$signal$ringrtc$GroupCall$ConnectionState;

        static {
            int[] iArr = new int[GroupCall.ConnectionState.values().length];
            $SwitchMap$org$signal$ringrtc$GroupCall$ConnectionState = iArr;
            try {
                iArr[GroupCall.ConnectionState.CONNECTING.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$GroupCall$ConnectionState[GroupCall.ConnectionState.CONNECTED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$GroupCall$ConnectionState[GroupCall.ConnectionState.RECONNECTING.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            int[] iArr2 = new int[CallManager.HangupType.values().length];
            $SwitchMap$org$signal$ringrtc$CallManager$HangupType = iArr2;
            try {
                iArr2[CallManager.HangupType.ACCEPTED.ordinal()] = 1;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$HangupType[CallManager.HangupType.BUSY.ordinal()] = 2;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$HangupType[CallManager.HangupType.NORMAL.ordinal()] = 3;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$HangupType[CallManager.HangupType.DECLINED.ordinal()] = 4;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$HangupType[CallManager.HangupType.NEED_PERMISSION.ordinal()] = 5;
            } catch (NoSuchFieldError unused8) {
            }
        }
    }

    public static WebRtcViewModel.GroupCallState groupCallStateForConnection(GroupCall.ConnectionState connectionState) {
        int i = AnonymousClass1.$SwitchMap$org$signal$ringrtc$GroupCall$ConnectionState[connectionState.ordinal()];
        if (i == 1) {
            return WebRtcViewModel.GroupCallState.CONNECTING;
        }
        if (i == 2) {
            return WebRtcViewModel.GroupCallState.CONNECTED;
        }
        if (i != 3) {
            return WebRtcViewModel.GroupCallState.DISCONNECTED;
        }
        return WebRtcViewModel.GroupCallState.RECONNECTING;
    }

    public static String getGroupCallEraId(GroupCall groupCall) {
        PeekInfo peekInfo;
        if (groupCall == null || (peekInfo = groupCall.getPeekInfo()) == null) {
            return null;
        }
        return peekInfo.getEraId();
    }

    public static boolean isCallFull(PeekInfo peekInfo) {
        return (peekInfo == null || peekInfo.getMaxDevices() == null || peekInfo.getDeviceCount() < peekInfo.getMaxDevices().longValue()) ? false : true;
    }
}
