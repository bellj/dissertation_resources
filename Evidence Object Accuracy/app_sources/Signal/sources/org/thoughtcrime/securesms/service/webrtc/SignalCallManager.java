package org.thoughtcrime.securesms.service.webrtc;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.ResultReceiver;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import kotlin.jvm.functions.Function1;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.greenrobot.eventbus.EventBus;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.util.Pair;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.VerificationFailedException;
import org.signal.libsignal.zkgroup.groups.GroupIdentifier;
import org.signal.libsignal.zkgroup.groups.UuidCiphertext;
import org.signal.ringrtc.CallException;
import org.signal.ringrtc.CallId;
import org.signal.ringrtc.CallManager;
import org.signal.ringrtc.GroupCall;
import org.signal.ringrtc.HttpHeader;
import org.signal.ringrtc.NetworkRoute;
import org.signal.ringrtc.PeekInfo;
import org.signal.ringrtc.Remote;
import org.signal.storageservice.protos.groups.GroupExternalCredential;
import org.thoughtcrime.securesms.WebRtcCallActivity;
import org.thoughtcrime.securesms.crypto.UnidentifiedAccessUtil;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.events.GroupCallPeekEvent;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.jobs.GroupCallUpdateSendJob;
import org.thoughtcrime.securesms.jobs.RetrieveProfileJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.messages.GroupSendUtil;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.recipients.RecipientUtil$$ExternalSyntheticLambda2;
import org.thoughtcrime.securesms.ringrtc.CameraEventListener;
import org.thoughtcrime.securesms.ringrtc.CameraState;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.thoughtcrime.securesms.service.webrtc.SignalCallManager;
import org.thoughtcrime.securesms.service.webrtc.WebRtcData;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcEphemeralState;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceState;
import org.thoughtcrime.securesms.util.AppForegroundObserver;
import org.thoughtcrime.securesms.util.BubbleUtil;
import org.thoughtcrime.securesms.util.RecipientAccessList;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.rx.RxStore;
import org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager;
import org.thoughtcrime.securesms.webrtc.locks.LockManager;
import org.webrtc.PeerConnection;
import org.whispersystems.signalservice.api.crypto.UntrustedIdentityException;
import org.whispersystems.signalservice.api.messages.SendMessageResult;
import org.whispersystems.signalservice.api.messages.calls.CallingResponse;
import org.whispersystems.signalservice.api.messages.calls.OfferMessage;
import org.whispersystems.signalservice.api.messages.calls.OpaqueMessage;
import org.whispersystems.signalservice.api.messages.calls.SignalServiceCallMessage;
import org.whispersystems.signalservice.api.messages.calls.TurnServerInfo;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.exceptions.UnregisteredUserException;

/* loaded from: classes4.dex */
public final class SignalCallManager implements CallManager.Observer, GroupCall.Observer, CameraEventListener, AppForegroundObserver.Listener {
    public static final int BUSY_TONE_LENGTH;
    private static final String TAG = Log.tag(SignalCallManager.class);
    private final CallManager callManager;
    private final Context context;
    private RxStore<WebRtcEphemeralState> ephemeralStateStore;
    private final LockManager lockManager;
    private boolean needsToSetSelfUuid = true;
    private final Executor networkExecutor;
    private final ExecutorService serviceExecutor;
    private WebRtcServiceState serviceState;

    /* loaded from: classes4.dex */
    public interface ProcessAction {
        WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor);
    }

    @Override // org.thoughtcrime.securesms.util.AppForegroundObserver.Listener
    public /* synthetic */ void onBackground() {
        AppForegroundObserver.Listener.CC.$default$onBackground(this);
    }

    public SignalCallManager(Application application) {
        CallManager callManager;
        Context applicationContext = application.getApplicationContext();
        this.context = applicationContext;
        this.lockManager = new LockManager(applicationContext);
        ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor();
        this.serviceExecutor = newSingleThreadExecutor;
        this.networkExecutor = Executors.newSingleThreadExecutor();
        this.ephemeralStateStore = new RxStore<>(new WebRtcEphemeralState(), Schedulers.from(newSingleThreadExecutor));
        try {
            callManager = CallManager.createCallManager(this);
        } catch (CallException e) {
            Log.w(TAG, "Unable to create CallManager", e);
            callManager = null;
        }
        this.callManager = callManager;
        this.serviceState = new WebRtcServiceState(new IdleActionProcessor(new WebRtcInteractor(this.context, this, this.lockManager, this, this, this)));
    }

    public Flowable<WebRtcEphemeralState> ephemeralStates() {
        return this.ephemeralStateStore.getStateFlowable().distinctUntilChanged();
    }

    public CallManager getRingRtcCallManager() {
        return this.callManager;
    }

    public LockManager getLockManager() {
        return this.lockManager;
    }

    private void process(ProcessAction processAction) {
        Throwable th = new Throwable();
        String methodName = th.getStackTrace().length > 1 ? th.getStackTrace()[1].getMethodName() : "unknown";
        if (this.callManager == null) {
            Log.w(TAG, "Unable to process action, call manager is not initialized");
        } else {
            this.serviceExecutor.execute(new Runnable(methodName, processAction) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda73
                public final /* synthetic */ String f$1;
                public final /* synthetic */ SignalCallManager.ProcessAction f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    SignalCallManager.this.lambda$process$0(this.f$1, this.f$2);
                }
            });
        }
    }

    public /* synthetic */ void lambda$process$0(String str, ProcessAction processAction) {
        if (this.needsToSetSelfUuid) {
            try {
                this.callManager.setSelfUuid(SignalStore.account().requireAci().uuid());
                this.needsToSetSelfUuid = false;
            } catch (CallException e) {
                Log.w(TAG, "Unable to set self UUID on CallManager", e);
            }
        }
        String str2 = TAG;
        Log.v(str2, "Processing action: " + str + ", handler: " + this.serviceState.getActionProcessor().getTag());
        WebRtcServiceState webRtcServiceState = this.serviceState;
        WebRtcServiceState process = processAction.process(webRtcServiceState, webRtcServiceState.getActionProcessor());
        this.serviceState = process;
        if (webRtcServiceState != process && process.getCallInfoState().getCallState() != WebRtcViewModel.State.IDLE) {
            postStateUpdate(this.serviceState);
        }
    }

    private void processStateless(Function1<WebRtcEphemeralState, WebRtcEphemeralState> function1) {
        this.ephemeralStateStore.update(function1);
    }

    public static /* synthetic */ WebRtcServiceState lambda$startPreJoinCall$1(Recipient recipient, WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        return webRtcActionProcessor.handlePreJoinCall(webRtcServiceState, new RemotePeer(recipient.getId()));
    }

    public void startPreJoinCall(Recipient recipient) {
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return SignalCallManager.lambda$startPreJoinCall$1(Recipient.this, webRtcServiceState, webRtcActionProcessor);
            }
        });
    }

    public static /* synthetic */ WebRtcServiceState lambda$startOutgoingAudioCall$2(Recipient recipient, WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        return webRtcActionProcessor.handleOutgoingCall(webRtcServiceState, new RemotePeer(recipient.getId()), OfferMessage.Type.AUDIO_CALL);
    }

    public void startOutgoingAudioCall(Recipient recipient) {
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda49
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return SignalCallManager.lambda$startOutgoingAudioCall$2(Recipient.this, webRtcServiceState, webRtcActionProcessor);
            }
        });
    }

    public static /* synthetic */ WebRtcServiceState lambda$startOutgoingVideoCall$3(Recipient recipient, WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        return webRtcActionProcessor.handleOutgoingCall(webRtcServiceState, new RemotePeer(recipient.getId()), OfferMessage.Type.VIDEO_CALL);
    }

    public void startOutgoingVideoCall(Recipient recipient) {
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda69
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return SignalCallManager.lambda$startOutgoingVideoCall$3(Recipient.this, webRtcServiceState, webRtcActionProcessor);
            }
        });
    }

    public void cancelPreJoin() {
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda48
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleCancelPreJoinCall(webRtcServiceState);
            }
        });
    }

    public void updateRenderedResolutions() {
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda42
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleUpdateRenderedResolutions(webRtcServiceState);
            }
        });
    }

    public void orientationChanged(boolean z, int i) {
        process(new ProcessAction(z, i) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda52
            public final /* synthetic */ boolean f$0;
            public final /* synthetic */ int f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleOrientationChanged(webRtcServiceState, this.f$0, this.f$1);
            }
        });
    }

    public void setMuteAudio(boolean z) {
        process(new ProcessAction(z) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda19
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleSetMuteAudio(webRtcServiceState, this.f$0);
            }
        });
    }

    public void setMuteVideo(boolean z) {
        process(new ProcessAction(z) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda6
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleSetEnableVideo(webRtcServiceState, this.f$0);
            }
        });
    }

    public void flipCamera() {
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda78
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleSetCameraFlip(webRtcServiceState);
            }
        });
    }

    public void acceptCall(boolean z) {
        process(new ProcessAction(z) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda54
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleAcceptCall(webRtcServiceState, this.f$0);
            }
        });
    }

    public void denyCall() {
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda72
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleDenyCall(webRtcServiceState);
            }
        });
    }

    public void localHangup() {
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda43
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleLocalHangup(webRtcServiceState);
            }
        });
    }

    public void requestUpdateGroupMembers() {
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda16
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleGroupRequestUpdateMembers(webRtcServiceState);
            }
        });
    }

    public void groupApproveSafetyChange(List<RecipientId> list) {
        process(new ProcessAction(list) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda55
            public final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleGroupApproveSafetyNumberChange(webRtcServiceState, this.f$0);
            }
        });
    }

    public void isCallActive(ResultReceiver resultReceiver) {
        process(new ProcessAction(resultReceiver) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda17
            public final /* synthetic */ ResultReceiver f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleIsInCallQuery(webRtcServiceState, this.f$0);
            }
        });
    }

    public void networkChange(boolean z) {
        process(new ProcessAction(z) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda44
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleNetworkChanged(webRtcServiceState, this.f$0);
            }
        });
    }

    public void bandwidthModeUpdate() {
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda8
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleBandwidthModeUpdate(webRtcServiceState);
            }
        });
    }

    public void screenOff() {
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda27
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleScreenOffChange(webRtcServiceState);
            }
        });
    }

    public void postStateUpdate(WebRtcServiceState webRtcServiceState) {
        EventBus.getDefault().postSticky(new WebRtcViewModel(webRtcServiceState));
    }

    public void receivedOffer(WebRtcData.CallMetadata callMetadata, WebRtcData.OfferMetadata offerMetadata, WebRtcData.ReceivedOfferMetadata receivedOfferMetadata) {
        process(new ProcessAction(offerMetadata, receivedOfferMetadata) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda11
            public final /* synthetic */ WebRtcData.OfferMetadata f$1;
            public final /* synthetic */ WebRtcData.ReceivedOfferMetadata f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleReceivedOffer(webRtcServiceState, WebRtcData.CallMetadata.this, this.f$1, this.f$2);
            }
        });
    }

    public void receivedAnswer(WebRtcData.CallMetadata callMetadata, WebRtcData.AnswerMetadata answerMetadata, WebRtcData.ReceivedAnswerMetadata receivedAnswerMetadata) {
        process(new ProcessAction(answerMetadata, receivedAnswerMetadata) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda9
            public final /* synthetic */ WebRtcData.AnswerMetadata f$1;
            public final /* synthetic */ WebRtcData.ReceivedAnswerMetadata f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleReceivedAnswer(webRtcServiceState, WebRtcData.CallMetadata.this, this.f$1, this.f$2);
            }
        });
    }

    public void receivedIceCandidates(WebRtcData.CallMetadata callMetadata, List<byte[]> list) {
        process(new ProcessAction(list) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda20
            public final /* synthetic */ List f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleReceivedIceCandidates(webRtcServiceState, WebRtcData.CallMetadata.this, this.f$1);
            }
        });
    }

    public void receivedCallHangup(WebRtcData.CallMetadata callMetadata, WebRtcData.HangupMetadata hangupMetadata) {
        process(new ProcessAction(hangupMetadata) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda5
            public final /* synthetic */ WebRtcData.HangupMetadata f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleReceivedHangup(webRtcServiceState, WebRtcData.CallMetadata.this, this.f$1);
            }
        });
    }

    public void receivedCallBusy(WebRtcData.CallMetadata callMetadata) {
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda60
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleReceivedBusy(webRtcServiceState, WebRtcData.CallMetadata.this);
            }
        });
    }

    public void receivedOpaqueMessage(WebRtcData.OpaqueMessageMetadata opaqueMessageMetadata) {
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda51
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleReceivedOpaqueMessage(webRtcServiceState, WebRtcData.OpaqueMessageMetadata.this);
            }
        });
    }

    public void setRingGroup(boolean z) {
        process(new ProcessAction(z) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda74
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleSetRingGroup(webRtcServiceState, this.f$0);
            }
        });
    }

    private void receivedGroupCallPeekForRingingCheck(GroupCallRingCheckInfo groupCallRingCheckInfo, long j) {
        process(new ProcessAction(j) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda4
            public final /* synthetic */ long f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleReceivedGroupCallPeekForRingingCheck(webRtcServiceState, GroupCallRingCheckInfo.this, this.f$1);
            }
        });
    }

    public void onAudioDeviceChanged(SignalAudioManager.AudioDevice audioDevice, Set<SignalAudioManager.AudioDevice> set) {
        process(new ProcessAction(set) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda64
            public final /* synthetic */ Set f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleAudioDeviceChanged(webRtcServiceState, SignalAudioManager.AudioDevice.this, this.f$1);
            }
        });
    }

    public void onBluetoothPermissionDenied() {
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda24
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleBluetoothPermissionDenied(webRtcServiceState);
            }
        });
    }

    public void selectAudioDevice(SignalAudioManager.AudioDevice audioDevice) {
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda53
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleSetUserAudioDevice(webRtcServiceState, SignalAudioManager.AudioDevice.this);
            }
        });
    }

    public void setTelecomApproved(long j, RecipientId recipientId) {
        process(new ProcessAction(j, recipientId) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda59
            public final /* synthetic */ long f$0;
            public final /* synthetic */ RecipientId f$1;

            {
                this.f$0 = r1;
                this.f$1 = r3;
            }

            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleSetTelecomApproved(webRtcServiceState, this.f$0, this.f$1);
            }
        });
    }

    public void dropCall(long j) {
        process(new ProcessAction(j) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda62
            public final /* synthetic */ long f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleDropCall(webRtcServiceState, this.f$0);
            }
        });
    }

    public void peekGroupCall(RecipientId recipientId) {
        if (this.callManager == null) {
            Log.i(TAG, "Unable to peekGroupCall, call manager is null");
        } else {
            this.networkExecutor.execute(new Runnable(recipientId) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda18
                public final /* synthetic */ RecipientId f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    SignalCallManager.this.lambda$peekGroupCall$34(this.f$1);
                }
            });
        }
    }

    public /* synthetic */ void lambda$peekGroupCall$34(RecipientId recipientId) {
        try {
            Recipient resolved = Recipient.resolved(recipientId);
            GroupId.V2 requireV2 = resolved.requireGroupId().requireV2();
            GroupExternalCredential groupExternalCredential = GroupManager.getGroupExternalCredential(this.context, requireV2);
            this.callManager.peekGroupCall(SignalStore.internalValues().groupCallingServer(), groupExternalCredential.getTokenBytes().toByteArray(), Stream.of(GroupManager.getUuidCipherTexts(this.context, requireV2)).map(new Function() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda67
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return SignalCallManager.lambda$peekGroupCall$32((Map.Entry) obj);
                }
            }).toList(), new CallManager.ResponseHandler(resolved, recipientId) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda68
                public final /* synthetic */ Recipient f$1;
                public final /* synthetic */ RecipientId f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // org.signal.ringrtc.CallManager.ResponseHandler
                public final void handleResponse(Object obj) {
                    SignalCallManager.this.lambda$peekGroupCall$33(this.f$1, this.f$2, (PeekInfo) obj);
                }
            });
        } catch (IOException | VerificationFailedException | CallException e) {
            Log.e(TAG, "error peeking from active conversation", e);
        }
    }

    public static /* synthetic */ GroupCall.GroupMemberInfo lambda$peekGroupCall$32(Map.Entry entry) {
        return new GroupCall.GroupMemberInfo((UUID) entry.getKey(), ((UuidCiphertext) entry.getValue()).serialize());
    }

    public /* synthetic */ void lambda$peekGroupCall$33(Recipient recipient, RecipientId recipientId, PeekInfo peekInfo) {
        Long threadIdFor = SignalDatabase.threads().getThreadIdFor(recipient.getId());
        if (threadIdFor != null) {
            SignalDatabase.sms().updatePreviousGroupCall(threadIdFor.longValue(), peekInfo.getEraId(), peekInfo.getJoinedMembers(), WebRtcUtil.isCallFull(peekInfo));
            ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.forConversation(threadIdFor.longValue()), true, 0, BubbleUtil.BubbleState.HIDDEN);
            EventBus.getDefault().postSticky(new GroupCallPeekEvent(recipientId, peekInfo.getEraId(), Long.valueOf(peekInfo.getDeviceCount()), peekInfo.getMaxDevices()));
        }
    }

    public void peekGroupCallForRingingCheck(GroupCallRingCheckInfo groupCallRingCheckInfo) {
        if (this.callManager == null) {
            Log.i(TAG, "Unable to peekGroupCall, call manager is null");
        } else {
            this.networkExecutor.execute(new Runnable(groupCallRingCheckInfo) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda65
                public final /* synthetic */ GroupCallRingCheckInfo f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    SignalCallManager.this.lambda$peekGroupCallForRingingCheck$37(this.f$1);
                }
            });
        }
    }

    public /* synthetic */ void lambda$peekGroupCallForRingingCheck$37(GroupCallRingCheckInfo groupCallRingCheckInfo) {
        try {
            GroupId.V2 requireV2 = Recipient.resolved(groupCallRingCheckInfo.getRecipientId()).requireGroupId().requireV2();
            GroupExternalCredential groupExternalCredential = GroupManager.getGroupExternalCredential(this.context, requireV2);
            this.callManager.peekGroupCall(SignalStore.internalValues().groupCallingServer(), groupExternalCredential.getTokenBytes().toByteArray(), (List) Collection$EL.stream(GroupManager.getUuidCipherTexts(this.context, requireV2).entrySet()).map(new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda37
                @Override // j$.util.function.Function
                public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return SignalCallManager.lambda$peekGroupCallForRingingCheck$35((Map.Entry) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).collect(Collectors.toList()), new CallManager.ResponseHandler(groupCallRingCheckInfo) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda38
                public final /* synthetic */ GroupCallRingCheckInfo f$1;

                {
                    this.f$1 = r2;
                }

                @Override // org.signal.ringrtc.CallManager.ResponseHandler
                public final void handleResponse(Object obj) {
                    SignalCallManager.this.lambda$peekGroupCallForRingingCheck$36(this.f$1, (PeekInfo) obj);
                }
            });
        } catch (IOException | VerificationFailedException | CallException e) {
            Log.e(TAG, "error peeking for ringing check", e);
        }
    }

    public static /* synthetic */ GroupCall.GroupMemberInfo lambda$peekGroupCallForRingingCheck$35(Map.Entry entry) {
        return new GroupCall.GroupMemberInfo((UUID) entry.getKey(), ((UuidCiphertext) entry.getValue()).serialize());
    }

    public /* synthetic */ void lambda$peekGroupCallForRingingCheck$36(GroupCallRingCheckInfo groupCallRingCheckInfo, PeekInfo peekInfo) {
        receivedGroupCallPeekForRingingCheck(groupCallRingCheckInfo, peekInfo.getDeviceCount());
    }

    public boolean startCallCardActivityIfPossible() {
        if (Build.VERSION.SDK_INT >= 29 && !ApplicationDependencies.getAppForegroundObserver().isForegrounded()) {
            return false;
        }
        this.context.startActivity(new Intent(this.context, WebRtcCallActivity.class).setFlags(SQLiteDatabase.CREATE_IF_NECESSARY));
        return true;
    }

    @Override // org.signal.ringrtc.CallManager.Observer
    public void onStartCall(Remote remote, CallId callId, Boolean bool, CallManager.CallMediaType callMediaType) {
        String str = TAG;
        Log.i(str, "onStartCall(): callId: " + callId + ", outgoing: " + bool + ", type: " + callMediaType);
        if (this.callManager == null) {
            Log.w(str, "Unable to start call, call manager is not initialized");
        } else if (remote != null) {
            process(new ProcessAction(remote, callId, bool, callMediaType) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda1
                public final /* synthetic */ Remote f$1;
                public final /* synthetic */ CallId f$2;
                public final /* synthetic */ Boolean f$3;
                public final /* synthetic */ CallManager.CallMediaType f$4;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                    this.f$4 = r5;
                }

                @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
                public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                    return SignalCallManager.this.lambda$onStartCall$38(this.f$1, this.f$2, this.f$3, this.f$4, webRtcServiceState, webRtcActionProcessor);
                }
            });
        }
    }

    public /* synthetic */ WebRtcServiceState lambda$onStartCall$38(Remote remote, CallId callId, Boolean bool, CallManager.CallMediaType callMediaType, WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        RemotePeer remotePeer = (RemotePeer) remote;
        if (webRtcServiceState.getCallInfoState().getPeer(remotePeer.hashCode()) == null) {
            String str = TAG;
            Log.w(str, "remotePeer not found in map with key: " + remotePeer.hashCode() + "! Dropping.");
            try {
                this.callManager.drop(callId);
            } catch (CallException e) {
                webRtcServiceState = webRtcActionProcessor.callFailure(webRtcServiceState, "callManager.drop() failed: ", e);
            }
        }
        remotePeer.setCallId(callId);
        if (bool.booleanValue()) {
            return webRtcActionProcessor.handleStartOutgoingCall(webRtcServiceState, remotePeer, WebRtcUtil.getOfferTypeFromCallMediaType(callMediaType));
        }
        return webRtcActionProcessor.handleStartIncomingCall(webRtcServiceState, remotePeer, WebRtcUtil.getOfferTypeFromCallMediaType(callMediaType));
    }

    @Override // org.signal.ringrtc.CallManager.Observer
    public void onCallEvent(Remote remote, CallManager.CallEvent callEvent) {
        if (this.callManager == null) {
            Log.w(TAG, "Unable to process call event, call manager is not initialized");
        } else if (remote instanceof RemotePeer) {
            process(new ProcessAction(remote, callEvent) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda47
                public final /* synthetic */ Remote f$1;
                public final /* synthetic */ CallManager.CallEvent f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
                public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                    return SignalCallManager.this.lambda$onCallEvent$39(this.f$1, this.f$2, webRtcServiceState, webRtcActionProcessor);
                }
            });
        }
    }

    public /* synthetic */ WebRtcServiceState lambda$onCallEvent$39(Remote remote, CallManager.CallEvent callEvent, WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        RemotePeer remotePeer = (RemotePeer) remote;
        if (webRtcServiceState.getCallInfoState().getPeer(remotePeer.hashCode()) == null) {
            String str = TAG;
            Log.w(str, "remotePeer not found in map with key: " + remotePeer.hashCode() + "! Dropping.");
            try {
                this.callManager.drop(remotePeer.getCallId());
                return webRtcServiceState;
            } catch (CallException e) {
                return webRtcActionProcessor.callFailure(webRtcServiceState, "callManager.drop() failed: ", e);
            }
        } else {
            String str2 = TAG;
            Log.i(str2, "onCallEvent(): call_id: " + remotePeer.getCallId() + ", state: " + remotePeer.getState() + ", event: " + callEvent);
            switch (AnonymousClass1.$SwitchMap$org$signal$ringrtc$CallManager$CallEvent[callEvent.ordinal()]) {
                case 1:
                    return webRtcActionProcessor.handleLocalRinging(webRtcServiceState, remotePeer);
                case 2:
                    return webRtcActionProcessor.handleRemoteRinging(webRtcServiceState, remotePeer);
                case 3:
                case 4:
                    return webRtcActionProcessor.handleCallReconnect(webRtcServiceState, callEvent);
                case 5:
                case 6:
                    return webRtcActionProcessor.handleCallConnected(webRtcServiceState, remotePeer);
                case 7:
                    return webRtcActionProcessor.handleRemoteVideoEnable(webRtcServiceState, true);
                case 8:
                    return webRtcActionProcessor.handleRemoteVideoEnable(webRtcServiceState, false);
                case 9:
                    return webRtcActionProcessor.handleScreenSharingEnable(webRtcServiceState, true);
                case 10:
                    return webRtcActionProcessor.handleScreenSharingEnable(webRtcServiceState, false);
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                case 18:
                    return webRtcActionProcessor.handleEndedRemote(webRtcServiceState, callEvent, remotePeer);
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                    return webRtcActionProcessor.handleEnded(webRtcServiceState, callEvent, remotePeer);
                case 24:
                    return webRtcActionProcessor.handleReceivedOfferExpired(webRtcServiceState, remotePeer);
                case 25:
                case 26:
                    return webRtcActionProcessor.handleReceivedOfferWhileActive(webRtcServiceState, remotePeer);
                case 27:
                case 28:
                    Log.i(str2, "Ignoring event: " + callEvent);
                    return webRtcServiceState;
                default:
                    throw new AssertionError("Unexpected event: " + callEvent.toString());
            }
        }
    }

    /* renamed from: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$signal$ringrtc$CallManager$CallEvent;

        static {
            int[] iArr = new int[CallManager.CallEvent.values().length];
            $SwitchMap$org$signal$ringrtc$CallManager$CallEvent = iArr;
            try {
                iArr[CallManager.CallEvent.LOCAL_RINGING.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.REMOTE_RINGING.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.RECONNECTING.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.RECONNECTED.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.LOCAL_CONNECTED.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.REMOTE_CONNECTED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.REMOTE_VIDEO_ENABLE.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.REMOTE_VIDEO_DISABLE.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.REMOTE_SHARING_SCREEN_ENABLE.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.REMOTE_SHARING_SCREEN_DISABLE.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.ENDED_REMOTE_HANGUP.ordinal()] = 11;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.ENDED_REMOTE_HANGUP_NEED_PERMISSION.ordinal()] = 12;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.ENDED_REMOTE_HANGUP_ACCEPTED.ordinal()] = 13;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.ENDED_REMOTE_HANGUP_BUSY.ordinal()] = 14;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.ENDED_REMOTE_HANGUP_DECLINED.ordinal()] = 15;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.ENDED_REMOTE_BUSY.ordinal()] = 16;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.ENDED_REMOTE_GLARE.ordinal()] = 17;
            } catch (NoSuchFieldError unused17) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.ENDED_REMOTE_RECALL.ordinal()] = 18;
            } catch (NoSuchFieldError unused18) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.ENDED_TIMEOUT.ordinal()] = 19;
            } catch (NoSuchFieldError unused19) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.ENDED_INTERNAL_FAILURE.ordinal()] = 20;
            } catch (NoSuchFieldError unused20) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.ENDED_SIGNALING_FAILURE.ordinal()] = 21;
            } catch (NoSuchFieldError unused21) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.ENDED_GLARE_HANDLING_FAILURE.ordinal()] = 22;
            } catch (NoSuchFieldError unused22) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.ENDED_CONNECTION_FAILURE.ordinal()] = 23;
            } catch (NoSuchFieldError unused23) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.RECEIVED_OFFER_EXPIRED.ordinal()] = 24;
            } catch (NoSuchFieldError unused24) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.RECEIVED_OFFER_WHILE_ACTIVE.ordinal()] = 25;
            } catch (NoSuchFieldError unused25) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.RECEIVED_OFFER_WITH_GLARE.ordinal()] = 26;
            } catch (NoSuchFieldError unused26) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.ENDED_LOCAL_HANGUP.ordinal()] = 27;
            } catch (NoSuchFieldError unused27) {
            }
            try {
                $SwitchMap$org$signal$ringrtc$CallManager$CallEvent[CallManager.CallEvent.ENDED_APP_DROPPED_CALL.ordinal()] = 28;
            } catch (NoSuchFieldError unused28) {
            }
        }
    }

    @Override // org.signal.ringrtc.CallManager.Observer
    public void onNetworkRouteChanged(Remote remote, NetworkRoute networkRoute) {
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda50
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleNetworkRouteChanged(webRtcServiceState, NetworkRoute.this);
            }
        });
    }

    public /* synthetic */ WebRtcEphemeralState lambda$onAudioLevels$41(int i, int i2, WebRtcEphemeralState webRtcEphemeralState) {
        return this.serviceState.getActionProcessor().handleAudioLevelsChanged(this.serviceState, webRtcEphemeralState, i, i2);
    }

    @Override // org.signal.ringrtc.CallManager.Observer
    public void onAudioLevels(Remote remote, int i, int i2) {
        processStateless(new Function1(i, i2) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda0
            public final /* synthetic */ int f$1;
            public final /* synthetic */ int f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // kotlin.jvm.functions.Function1
            public final Object invoke(Object obj) {
                return SignalCallManager.this.lambda$onAudioLevels$41(this.f$1, this.f$2, (WebRtcEphemeralState) obj);
            }
        });
    }

    @Override // org.signal.ringrtc.CallManager.Observer
    public void onCallConcluded(Remote remote) {
        if (remote instanceof RemotePeer) {
            RemotePeer remotePeer = (RemotePeer) remote;
            String str = TAG;
            Log.i(str, "onCallConcluded: call_id: " + remotePeer.getCallId());
            process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda46
                @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
                public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                    return webRtcActionProcessor.handleCallConcluded(webRtcServiceState, RemotePeer.this);
                }
            });
        }
    }

    @Override // org.signal.ringrtc.CallManager.Observer
    public void onSendOffer(CallId callId, Remote remote, Integer num, Boolean bool, byte[] bArr, CallManager.CallMediaType callMediaType) {
        if (remote instanceof RemotePeer) {
            RemotePeer remotePeer = (RemotePeer) remote;
            String str = TAG;
            Log.i(str, "onSendOffer: id: " + remotePeer.getCallId().format(num) + " type: " + callMediaType.name());
            process(new ProcessAction(new WebRtcData.OfferMetadata(bArr, null, WebRtcUtil.getOfferTypeFromCallMediaType(callMediaType)), bool) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda21
                public final /* synthetic */ WebRtcData.OfferMetadata f$1;
                public final /* synthetic */ Boolean f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
                public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                    return SignalCallManager.lambda$onSendOffer$43(WebRtcData.CallMetadata.this, this.f$1, this.f$2, webRtcServiceState, webRtcActionProcessor);
                }
            });
        }
    }

    public static /* synthetic */ WebRtcServiceState lambda$onSendOffer$43(WebRtcData.CallMetadata callMetadata, WebRtcData.OfferMetadata offerMetadata, Boolean bool, WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        return webRtcActionProcessor.handleSendOffer(webRtcServiceState, callMetadata, offerMetadata, bool.booleanValue());
    }

    @Override // org.signal.ringrtc.CallManager.Observer
    public void onSendAnswer(CallId callId, Remote remote, Integer num, Boolean bool, byte[] bArr) {
        if (remote instanceof RemotePeer) {
            RemotePeer remotePeer = (RemotePeer) remote;
            String str = TAG;
            Log.i(str, "onSendAnswer: id: " + remotePeer.getCallId().format(num));
            process(new ProcessAction(new WebRtcData.AnswerMetadata(bArr, null), bool) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda56
                public final /* synthetic */ WebRtcData.AnswerMetadata f$1;
                public final /* synthetic */ Boolean f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
                public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                    return SignalCallManager.lambda$onSendAnswer$44(WebRtcData.CallMetadata.this, this.f$1, this.f$2, webRtcServiceState, webRtcActionProcessor);
                }
            });
        }
    }

    public static /* synthetic */ WebRtcServiceState lambda$onSendAnswer$44(WebRtcData.CallMetadata callMetadata, WebRtcData.AnswerMetadata answerMetadata, Boolean bool, WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        return webRtcActionProcessor.handleSendAnswer(webRtcServiceState, callMetadata, answerMetadata, bool.booleanValue());
    }

    @Override // org.signal.ringrtc.CallManager.Observer
    public void onSendIceCandidates(CallId callId, Remote remote, Integer num, Boolean bool, List<byte[]> list) {
        if (remote instanceof RemotePeer) {
            RemotePeer remotePeer = (RemotePeer) remote;
            String str = TAG;
            Log.i(str, "onSendIceCandidates: id: " + remotePeer.getCallId().format(num));
            process(new ProcessAction(bool, list) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda10
                public final /* synthetic */ Boolean f$1;
                public final /* synthetic */ List f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
                public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                    return SignalCallManager.lambda$onSendIceCandidates$45(WebRtcData.CallMetadata.this, this.f$1, this.f$2, webRtcServiceState, webRtcActionProcessor);
                }
            });
        }
    }

    public static /* synthetic */ WebRtcServiceState lambda$onSendIceCandidates$45(WebRtcData.CallMetadata callMetadata, Boolean bool, List list, WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        return webRtcActionProcessor.handleSendIceCandidates(webRtcServiceState, callMetadata, bool.booleanValue(), list);
    }

    @Override // org.signal.ringrtc.CallManager.Observer
    public void onSendHangup(CallId callId, Remote remote, Integer num, Boolean bool, CallManager.HangupType hangupType, Integer num2) {
        if (remote instanceof RemotePeer) {
            RemotePeer remotePeer = (RemotePeer) remote;
            String str = TAG;
            Log.i(str, "onSendHangup: id: " + remotePeer.getCallId().format(num) + " type: " + hangupType.name());
            process(new ProcessAction(new WebRtcData.HangupMetadata(WebRtcUtil.getHangupTypeFromCallHangupType(hangupType), false, num2.intValue()), bool) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda14
                public final /* synthetic */ WebRtcData.HangupMetadata f$1;
                public final /* synthetic */ Boolean f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
                public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                    return SignalCallManager.lambda$onSendHangup$46(WebRtcData.CallMetadata.this, this.f$1, this.f$2, webRtcServiceState, webRtcActionProcessor);
                }
            });
        }
    }

    public static /* synthetic */ WebRtcServiceState lambda$onSendHangup$46(WebRtcData.CallMetadata callMetadata, WebRtcData.HangupMetadata hangupMetadata, Boolean bool, WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        return webRtcActionProcessor.handleSendHangup(webRtcServiceState, callMetadata, hangupMetadata, bool.booleanValue());
    }

    @Override // org.signal.ringrtc.CallManager.Observer
    public void onSendBusy(CallId callId, Remote remote, Integer num, Boolean bool) {
        if (remote instanceof RemotePeer) {
            RemotePeer remotePeer = (RemotePeer) remote;
            String str = TAG;
            Log.i(str, "onSendBusy: id: " + remotePeer.getCallId().format(num));
            process(new ProcessAction(bool) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda29
                public final /* synthetic */ Boolean f$1;

                {
                    this.f$1 = r2;
                }

                @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
                public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                    return SignalCallManager.lambda$onSendBusy$47(WebRtcData.CallMetadata.this, this.f$1, webRtcServiceState, webRtcActionProcessor);
                }
            });
        }
    }

    public static /* synthetic */ WebRtcServiceState lambda$onSendBusy$47(WebRtcData.CallMetadata callMetadata, Boolean bool, WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        return webRtcActionProcessor.handleSendBusy(webRtcServiceState, callMetadata, bool.booleanValue());
    }

    @Override // org.signal.ringrtc.CallManager.Observer
    public void onSendCallMessage(UUID uuid, byte[] bArr, CallManager.CallMessageUrgency callMessageUrgency) {
        Log.i(TAG, "onSendCallMessage():");
        this.networkExecutor.execute(new Runnable(uuid, SignalServiceCallMessage.forOpaque(new OpaqueMessage(bArr, WebRtcUtil.getUrgencyFromCallUrgency(callMessageUrgency)), true, null)) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda70
            public final /* synthetic */ UUID f$1;
            public final /* synthetic */ SignalServiceCallMessage f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SignalCallManager.this.lambda$onSendCallMessage$50(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$onSendCallMessage$50(UUID uuid, SignalServiceCallMessage signalServiceCallMessage) {
        Recipient resolved = Recipient.resolved(RecipientId.from(ServiceId.from(uuid)));
        if (!resolved.isBlocked()) {
            try {
                ApplicationDependencies.getSignalServiceMessageSender().sendCallMessage(RecipientUtil.toSignalServiceAddress(this.context, resolved), resolved.isSelf() ? Optional.empty() : UnidentifiedAccessUtil.getAccessFor(this.context, resolved), signalServiceCallMessage);
            } catch (IOException e) {
                Log.i(TAG, "sendOpaqueCallMessage onFailure: ", e);
                process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda33
                    @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
                    public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                        return SignalCallManager.lambda$onSendCallMessage$49(Recipient.this, webRtcServiceState, webRtcActionProcessor);
                    }
                });
            } catch (UntrustedIdentityException e2) {
                Log.i(TAG, "sendOpaqueCallMessage onFailure: ", e2);
                RetrieveProfileJob.enqueue(resolved.getId());
                process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda32
                    @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
                    public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                        return SignalCallManager.lambda$onSendCallMessage$48(Recipient.this, webRtcServiceState, webRtcActionProcessor);
                    }
                });
            }
        }
    }

    public static /* synthetic */ WebRtcServiceState lambda$onSendCallMessage$48(Recipient recipient, WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        return webRtcActionProcessor.handleGroupMessageSentError(webRtcServiceState, Collections.singletonList(recipient.getId()), WebRtcViewModel.State.UNTRUSTED_IDENTITY);
    }

    public static /* synthetic */ WebRtcServiceState lambda$onSendCallMessage$49(Recipient recipient, WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        return webRtcActionProcessor.handleGroupMessageSentError(webRtcServiceState, Collections.singletonList(recipient.getId()), WebRtcViewModel.State.NETWORK_FAILURE);
    }

    @Override // org.signal.ringrtc.CallManager.Observer
    public void onSendCallMessageToGroup(byte[] bArr, byte[] bArr2, CallManager.CallMessageUrgency callMessageUrgency) {
        Log.i(TAG, "onSendCallMessageToGroup():");
        this.networkExecutor.execute(new Runnable(bArr, bArr2, callMessageUrgency) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda41
            public final /* synthetic */ byte[] f$1;
            public final /* synthetic */ byte[] f$2;
            public final /* synthetic */ CallManager.CallMessageUrgency f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SignalCallManager.this.lambda$onSendCallMessageToGroup$54(this.f$1, this.f$2, this.f$3);
            }
        });
    }

    public /* synthetic */ void lambda$onSendCallMessageToGroup$54(byte[] bArr, byte[] bArr2, CallManager.CallMessageUrgency callMessageUrgency) {
        try {
            GroupId.V2 v2 = GroupId.v2(new GroupIdentifier(bArr));
            List<Recipient> eligibleForSending = RecipientUtil.getEligibleForSending((List) Collection$EL.stream(SignalDatabase.groups().getGroupMembers(v2, GroupDatabase.MemberSet.FULL_MEMBERS_EXCLUDING_SELF)).map(new RecipientUtil$$ExternalSyntheticLambda2()).collect(Collectors.toList()));
            Set set = (Set) Collection$EL.stream(GroupSendUtil.sendCallMessage(this.context, v2.requireV2(), eligibleForSending, SignalServiceCallMessage.forOutgoingGroupOpaque(v2.getDecodedId(), System.currentTimeMillis(), new OpaqueMessage(bArr2, WebRtcUtil.getUrgencyFromCallUrgency(callMessageUrgency)), true, null))).filter(new Predicate() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda34
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return SignalCallManager.lambda$onSendCallMessageToGroup$51((SendMessageResult) obj);
                }
            }).map(new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda35
                @Override // j$.util.function.Function
                public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return SignalCallManager.lambda$onSendCallMessageToGroup$52(RecipientAccessList.this, (SendMessageResult) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).collect(Collectors.toSet());
            if (Util.hasItems(set)) {
                process(new ProcessAction(set) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda36
                    public final /* synthetic */ Set f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
                    public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                        return SignalCallManager.lambda$onSendCallMessageToGroup$53(this.f$0, webRtcServiceState, webRtcActionProcessor);
                    }
                });
                RetrieveProfileJob.enqueue(set);
            }
        } catch (IOException | InvalidInputException | UntrustedIdentityException e) {
            Log.w(TAG, "onSendCallMessageToGroup failed", e);
        }
    }

    public static /* synthetic */ boolean lambda$onSendCallMessageToGroup$51(SendMessageResult sendMessageResult) {
        return sendMessageResult.getIdentityFailure() != null;
    }

    public static /* synthetic */ RecipientId lambda$onSendCallMessageToGroup$52(RecipientAccessList recipientAccessList, SendMessageResult sendMessageResult) {
        return recipientAccessList.requireIdByAddress(sendMessageResult.getAddress());
    }

    public static /* synthetic */ WebRtcServiceState lambda$onSendCallMessageToGroup$53(Set set, WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        return webRtcActionProcessor.handleGroupMessageSentError(webRtcServiceState, set, WebRtcViewModel.State.UNTRUSTED_IDENTITY);
    }

    @Override // org.signal.ringrtc.CallManager.Observer
    public void onSendHttpRequest(long j, String str, CallManager.HttpMethod httpMethod, List<HttpHeader> list, byte[] bArr) {
        if (this.callManager == null) {
            Log.w(TAG, "Unable to send http request, call manager is not initialized");
            return;
        }
        String str2 = TAG;
        Log.i(str2, "onSendHttpRequest(): request_id: " + j);
        this.networkExecutor.execute(new Runnable(list, j, str, httpMethod, bArr) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda12
            public final /* synthetic */ List f$1;
            public final /* synthetic */ long f$2;
            public final /* synthetic */ String f$3;
            public final /* synthetic */ CallManager.HttpMethod f$4;
            public final /* synthetic */ byte[] f$5;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r5;
                this.f$4 = r6;
                this.f$5 = r7;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SignalCallManager.this.lambda$onSendHttpRequest$56(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5);
            }
        });
    }

    public /* synthetic */ void lambda$onSendHttpRequest$56(List list, long j, String str, CallManager.HttpMethod httpMethod, byte[] bArr) {
        List<Pair<String, String>> list2;
        if (list != null) {
            list2 = Stream.of(list).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda71
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return SignalCallManager.lambda$onSendHttpRequest$55((HttpHeader) obj);
                }
            }).toList();
        } else {
            list2 = Collections.emptyList();
        }
        CallingResponse makeCallingRequest = ApplicationDependencies.getSignalServiceMessageSender().makeCallingRequest(j, str, httpMethod.name(), list2, bArr);
        try {
            if (makeCallingRequest instanceof CallingResponse.Success) {
                CallingResponse.Success success = (CallingResponse.Success) makeCallingRequest;
                this.callManager.receivedHttpResponse(j, success.getResponseStatus(), success.getResponseBody());
                return;
            }
            this.callManager.httpRequestFailed(j);
        } catch (CallException e) {
            while (true) {
                Log.i(TAG, "Failed to process HTTP response/failure", e);
                return;
            }
        }
    }

    public static /* synthetic */ Pair lambda$onSendHttpRequest$55(HttpHeader httpHeader) {
        return new Pair(httpHeader.getName(), httpHeader.getValue());
    }

    @Override // org.signal.ringrtc.CallManager.Observer
    public void onGroupCallRingUpdate(byte[] bArr, long j, UUID uuid, CallManager.RingUpdate ringUpdate) {
        try {
            GroupId.V2 v2 = GroupId.v2(new GroupIdentifier(bArr));
            Optional<GroupDatabase.GroupRecord> group = SignalDatabase.groups().getGroup(v2);
            if (group.isPresent()) {
                process(new ProcessAction(v2, j, uuid, ringUpdate) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda28
                    public final /* synthetic */ GroupId.V2 f$1;
                    public final /* synthetic */ long f$2;
                    public final /* synthetic */ UUID f$3;
                    public final /* synthetic */ CallManager.RingUpdate f$4;

                    {
                        this.f$1 = r2;
                        this.f$2 = r3;
                        this.f$3 = r5;
                        this.f$4 = r6;
                    }

                    @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
                    public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                        return SignalCallManager.lambda$onGroupCallRingUpdate$57(Optional.this, this.f$1, this.f$2, this.f$3, this.f$4, webRtcServiceState, webRtcActionProcessor);
                    }
                });
            } else {
                Log.w(TAG, "Unable to ring unknown group.");
            }
        } catch (InvalidInputException e) {
            while (true) {
                Log.w(TAG, "Unable to ring group due to invalid group id", e);
                return;
            }
        }
    }

    public static /* synthetic */ WebRtcServiceState lambda$onGroupCallRingUpdate$57(Optional optional, GroupId.V2 v2, long j, UUID uuid, CallManager.RingUpdate ringUpdate, WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        return webRtcActionProcessor.handleGroupCallRingUpdate(webRtcServiceState, new RemotePeer(((GroupDatabase.GroupRecord) optional.get()).getRecipientId()), v2, j, uuid, ringUpdate);
    }

    @Override // org.signal.ringrtc.GroupCall.Observer
    public void requestMembershipProof(GroupCall groupCall) {
        String str = TAG;
        Log.i(str, "requestMembershipProof():");
        Recipient callRecipient = this.serviceState.getCallInfoState().getCallRecipient();
        if (!callRecipient.isPushV2Group()) {
            Log.i(str, "Request membership proof for non-group");
            return;
        }
        GroupCall groupCall2 = this.serviceState.getCallInfoState().getGroupCall();
        if (groupCall2 == null || groupCall2.hashCode() != groupCall.hashCode()) {
            Log.i(str, "Skipping group membership proof request, requested group call does not match current group call");
        } else {
            this.networkExecutor.execute(new Runnable(callRecipient, groupCall) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda13
                public final /* synthetic */ Recipient f$1;
                public final /* synthetic */ GroupCall f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    SignalCallManager.this.lambda$requestMembershipProof$59(this.f$1, this.f$2);
                }
            });
        }
    }

    public /* synthetic */ void lambda$requestMembershipProof$59(Recipient recipient, GroupCall groupCall) {
        try {
            process(new ProcessAction(GroupManager.getGroupExternalCredential(this.context, recipient.getGroupId().get().requireV2())) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda3
                public final /* synthetic */ GroupExternalCredential f$1;

                {
                    this.f$1 = r2;
                }

                @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
                public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                    return SignalCallManager.lambda$requestMembershipProof$58(GroupCall.this, this.f$1, webRtcServiceState, webRtcActionProcessor);
                }
            });
        } catch (IOException e) {
            Log.w(TAG, "Unable to get group membership proof from service", e);
            onEnded(groupCall, GroupCall.GroupCallEndReason.SFU_CLIENT_FAILED_TO_JOIN);
        } catch (VerificationFailedException e2) {
            Log.w(TAG, "Unable to verify group membership proof", e2);
            onEnded(groupCall, GroupCall.GroupCallEndReason.DEVICE_EXPLICITLY_DISCONNECTED);
        }
    }

    public static /* synthetic */ WebRtcServiceState lambda$requestMembershipProof$58(GroupCall groupCall, GroupExternalCredential groupExternalCredential, WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        return webRtcActionProcessor.handleGroupRequestMembershipProof(webRtcServiceState, groupCall.hashCode(), groupExternalCredential.getTokenBytes().toByteArray());
    }

    @Override // org.signal.ringrtc.GroupCall.Observer
    public void requestGroupMembers(GroupCall groupCall) {
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda23
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleGroupRequestUpdateMembers(webRtcServiceState);
            }
        });
    }

    @Override // org.signal.ringrtc.GroupCall.Observer
    public void onLocalDeviceStateChanged(GroupCall groupCall) {
        String str = TAG;
        Log.i(str, "onLocalDeviceStateChanged: localAdapterType: " + groupCall.getLocalDeviceState().getNetworkRoute().getLocalAdapterType());
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda31
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleGroupLocalDeviceStateChanged(webRtcServiceState);
            }
        });
    }

    public /* synthetic */ WebRtcEphemeralState lambda$onAudioLevels$62(WebRtcEphemeralState webRtcEphemeralState) {
        return this.serviceState.getActionProcessor().handleGroupAudioLevelsChanged(this.serviceState, webRtcEphemeralState);
    }

    @Override // org.signal.ringrtc.GroupCall.Observer
    public void onAudioLevels(GroupCall groupCall) {
        processStateless(new Function1() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda7
            @Override // kotlin.jvm.functions.Function1
            public final Object invoke(Object obj) {
                return SignalCallManager.this.lambda$onAudioLevels$62((WebRtcEphemeralState) obj);
            }
        });
    }

    @Override // org.signal.ringrtc.GroupCall.Observer
    public void onRemoteDeviceStatesChanged(GroupCall groupCall) {
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda22
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleGroupRemoteDeviceStateChanged(webRtcServiceState);
            }
        });
    }

    @Override // org.signal.ringrtc.GroupCall.Observer
    public void onPeekChanged(GroupCall groupCall) {
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda40
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleGroupJoinedMembershipChanged(webRtcServiceState);
            }
        });
    }

    public static /* synthetic */ WebRtcServiceState lambda$onEnded$65(GroupCall groupCall, GroupCall.GroupCallEndReason groupCallEndReason, WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        return webRtcActionProcessor.handleGroupCallEnded(webRtcServiceState, groupCall.hashCode(), groupCallEndReason);
    }

    @Override // org.signal.ringrtc.GroupCall.Observer
    public void onEnded(GroupCall groupCall, GroupCall.GroupCallEndReason groupCallEndReason) {
        process(new ProcessAction(groupCallEndReason) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda30
            public final /* synthetic */ GroupCall.GroupCallEndReason f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return SignalCallManager.lambda$onEnded$65(GroupCall.this, this.f$1, webRtcServiceState, webRtcActionProcessor);
            }
        });
    }

    public static /* synthetic */ WebRtcServiceState lambda$onFullyInitialized$66(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        return webRtcActionProcessor.handleOrientationChanged(webRtcServiceState, webRtcServiceState.getLocalDeviceState().isLandscapeEnabled(), webRtcServiceState.getLocalDeviceState().getDeviceOrientation().getDegrees());
    }

    @Override // org.thoughtcrime.securesms.ringrtc.CameraEventListener
    public void onFullyInitialized() {
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda61
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return SignalCallManager.lambda$onFullyInitialized$66(webRtcServiceState, webRtcActionProcessor);
            }
        });
    }

    @Override // org.thoughtcrime.securesms.ringrtc.CameraEventListener
    public void onCameraSwitchCompleted(CameraState cameraState) {
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda15
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return webRtcActionProcessor.handleCameraSwitchCompleted(webRtcServiceState, CameraState.this);
            }
        });
    }

    @Override // org.thoughtcrime.securesms.util.AppForegroundObserver.Listener
    public void onForeground() {
        process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda63
            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return SignalCallManager.this.lambda$onForeground$68(webRtcServiceState, webRtcActionProcessor);
            }
        });
    }

    public /* synthetic */ WebRtcServiceState lambda$onForeground$68(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        WebRtcViewModel.State callState = webRtcServiceState.getCallInfoState().getCallState();
        WebRtcViewModel.GroupCallState groupCallState = webRtcServiceState.getCallInfoState().getGroupCallState();
        if (callState == WebRtcViewModel.State.CALL_INCOMING && (groupCallState == WebRtcViewModel.GroupCallState.IDLE || groupCallState.isRinging())) {
            startCallCardActivityIfPossible();
        }
        ApplicationDependencies.getAppForegroundObserver().removeListener(this);
        return webRtcServiceState;
    }

    public void insertMissedCall(RemotePeer remotePeer, boolean z, long j, boolean z2) {
        ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.forConversation(SignalDatabase.sms().insertMissedCall(remotePeer.getId(), j, z2).second().longValue()), z);
    }

    public void insertReceivedCall(RemotePeer remotePeer, boolean z, boolean z2) {
        ApplicationDependencies.getMessageNotifier().updateNotification(this.context, ConversationId.forConversation(SignalDatabase.sms().insertReceivedCall(remotePeer.getId(), z2).second().longValue()), z);
    }

    public void retrieveTurnServers(RemotePeer remotePeer) {
        this.networkExecutor.execute(new Runnable(remotePeer) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda58
            public final /* synthetic */ RemotePeer f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SignalCallManager.this.lambda$retrieveTurnServers$71(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$retrieveTurnServers$71(RemotePeer remotePeer) {
        try {
            TurnServerInfo turnServerInfo = ApplicationDependencies.getSignalServiceAccountManager().getTurnServerInfo();
            LinkedList linkedList = new LinkedList();
            linkedList.add(PeerConnection.IceServer.builder("stun:stun1.l.google.com:19302").createIceServer());
            for (String str : turnServerInfo.getUrls()) {
                String str2 = TAG;
                Log.i(str2, "ice_server: " + str);
                if (str.startsWith("turn")) {
                    linkedList.add(PeerConnection.IceServer.builder(str).setUsername(turnServerInfo.getUsername()).setPassword(turnServerInfo.getPassword()).createIceServer());
                } else {
                    linkedList.add(PeerConnection.IceServer.builder(str).createIceServer());
                }
            }
            process(new ProcessAction(remotePeer, linkedList) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda25
                public final /* synthetic */ RemotePeer f$1;
                public final /* synthetic */ List f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
                public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                    return SignalCallManager.this.lambda$retrieveTurnServers$69(this.f$1, this.f$2, webRtcServiceState, webRtcActionProcessor);
                }
            });
        } catch (IOException e) {
            Log.w(TAG, "Unable to retrieve turn servers: ", e);
            process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda26
                @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
                public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                    return SignalCallManager.lambda$retrieveTurnServers$70(RemotePeer.this, webRtcServiceState, webRtcActionProcessor);
                }
            });
        }
    }

    public /* synthetic */ WebRtcServiceState lambda$retrieveTurnServers$69(RemotePeer remotePeer, List list, WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        RemotePeer activePeer = webRtcServiceState.getCallInfoState().getActivePeer();
        if (activePeer != null && activePeer.getCallId().equals(remotePeer.getCallId())) {
            return webRtcActionProcessor.handleTurnServerUpdate(webRtcServiceState, list, TextSecurePreferences.isTurnOnly(this.context));
        }
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Ignoring received turn servers for incorrect call id. requesting_call_id: ");
        sb.append(remotePeer.getCallId());
        sb.append(" current_call_id: ");
        sb.append(activePeer != null ? activePeer.getCallId() : "null");
        Log.w(str, sb.toString());
        return webRtcServiceState;
    }

    public static /* synthetic */ WebRtcServiceState lambda$retrieveTurnServers$70(RemotePeer remotePeer, WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        return webRtcActionProcessor.handleSetupFailure(webRtcServiceState, remotePeer.getCallId());
    }

    public static /* synthetic */ void lambda$sendGroupCallUpdateMessage$72(Recipient recipient, String str) {
        ApplicationDependencies.getJobManager().add(GroupCallUpdateSendJob.create(recipient.getId(), str));
    }

    public void sendGroupCallUpdateMessage(Recipient recipient, String str) {
        SignalExecutors.BOUNDED.execute(new Runnable(str) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda39
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SignalCallManager.lambda$sendGroupCallUpdateMessage$72(Recipient.this, this.f$1);
            }
        });
    }

    public static /* synthetic */ void lambda$updateGroupCallUpdateMessage$73(RecipientId recipientId, String str, Collection collection, boolean z) {
        SignalDatabase.sms().insertOrUpdateGroupCall(recipientId, Recipient.self().getId(), System.currentTimeMillis(), str, collection, z);
    }

    public void updateGroupCallUpdateMessage(RecipientId recipientId, String str, Collection<UUID> collection, boolean z) {
        SignalExecutors.BOUNDED.execute(new Runnable(str, collection, z) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda45
            public final /* synthetic */ String f$1;
            public final /* synthetic */ Collection f$2;
            public final /* synthetic */ boolean f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SignalCallManager.lambda$updateGroupCallUpdateMessage$73(RecipientId.this, this.f$1, this.f$2, this.f$3);
            }
        });
    }

    public void sendCallMessage(RemotePeer remotePeer, SignalServiceCallMessage signalServiceCallMessage) {
        this.networkExecutor.execute(new Runnable(remotePeer, signalServiceCallMessage) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda66
            public final /* synthetic */ RemotePeer f$1;
            public final /* synthetic */ SignalServiceCallMessage f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SignalCallManager.this.lambda$sendCallMessage$77(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$sendCallMessage$77(RemotePeer remotePeer, SignalServiceCallMessage signalServiceCallMessage) {
        Recipient resolved = Recipient.resolved(remotePeer.getId());
        if (!resolved.isBlocked()) {
            try {
                ApplicationDependencies.getSignalServiceMessageSender().sendCallMessage(RecipientUtil.toSignalServiceAddress(this.context, resolved), UnidentifiedAccessUtil.getAccessFor(this.context, resolved), signalServiceCallMessage);
                process(new ProcessAction() { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda75
                    @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
                    public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                        return SignalCallManager.lambda$sendCallMessage$74(RemotePeer.this, webRtcServiceState, webRtcActionProcessor);
                    }
                });
            } catch (IOException e) {
                processSendMessageFailureWithChangeDetection(remotePeer, new ProcessAction(e) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda77
                    public final /* synthetic */ IOException f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
                    public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                        return SignalCallManager.lambda$sendCallMessage$76(RemotePeer.this, this.f$1, webRtcServiceState, webRtcActionProcessor);
                    }
                });
            } catch (UntrustedIdentityException e2) {
                RetrieveProfileJob.enqueue(remotePeer.getId());
                processSendMessageFailureWithChangeDetection(remotePeer, new ProcessAction(e2) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda76
                    public final /* synthetic */ UntrustedIdentityException f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
                    public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                        return SignalCallManager.lambda$sendCallMessage$75(RemotePeer.this, this.f$1, webRtcServiceState, webRtcActionProcessor);
                    }
                });
            }
        }
    }

    public static /* synthetic */ WebRtcServiceState lambda$sendCallMessage$74(RemotePeer remotePeer, WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        return webRtcActionProcessor.handleMessageSentSuccess(webRtcServiceState, remotePeer.getCallId());
    }

    public static /* synthetic */ WebRtcServiceState lambda$sendCallMessage$75(RemotePeer remotePeer, UntrustedIdentityException untrustedIdentityException, WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        return webRtcActionProcessor.handleMessageSentError(webRtcServiceState, remotePeer.getCallId(), WebRtcViewModel.State.UNTRUSTED_IDENTITY, Optional.ofNullable(untrustedIdentityException.getIdentityKey()));
    }

    public static /* synthetic */ WebRtcServiceState lambda$sendCallMessage$76(RemotePeer remotePeer, IOException iOException, WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        return webRtcActionProcessor.handleMessageSentError(webRtcServiceState, remotePeer.getCallId(), iOException instanceof UnregisteredUserException ? WebRtcViewModel.State.NO_SUCH_USER : WebRtcViewModel.State.NETWORK_FAILURE, Optional.empty());
    }

    private void processSendMessageFailureWithChangeDetection(RemotePeer remotePeer, ProcessAction processAction) {
        process(new ProcessAction(processAction) { // from class: org.thoughtcrime.securesms.service.webrtc.SignalCallManager$$ExternalSyntheticLambda57
            public final /* synthetic */ SignalCallManager.ProcessAction f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.service.webrtc.SignalCallManager.ProcessAction
            public final WebRtcServiceState process(WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
                return SignalCallManager.lambda$processSendMessageFailureWithChangeDetection$78(RemotePeer.this, this.f$1, webRtcServiceState, webRtcActionProcessor);
            }
        });
    }

    public static /* synthetic */ WebRtcServiceState lambda$processSendMessageFailureWithChangeDetection$78(RemotePeer remotePeer, ProcessAction processAction, WebRtcServiceState webRtcServiceState, WebRtcActionProcessor webRtcActionProcessor) {
        RemotePeer activePeer = webRtcServiceState.getCallInfoState().getActivePeer();
        if (activePeer == null || remotePeer.getState() != activePeer.getState() || !remotePeer.getCallId().equals(activePeer.getCallId())) {
            return webRtcActionProcessor.handleMessageSentSuccess(webRtcServiceState, remotePeer.getCallId());
        }
        return processAction.process(webRtcServiceState, webRtcActionProcessor);
    }
}
