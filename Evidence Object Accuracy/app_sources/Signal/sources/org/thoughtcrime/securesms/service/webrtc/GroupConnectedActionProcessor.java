package org.thoughtcrime.securesms.service.webrtc;

import android.os.ResultReceiver;
import android.util.LongSparseArray;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import org.signal.core.util.logging.Log;
import org.signal.ringrtc.CallException;
import org.signal.ringrtc.CallId;
import org.signal.ringrtc.GroupCall;
import org.signal.ringrtc.PeekInfo;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.events.CallParticipantId;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.ringrtc.Camera;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcEphemeralState;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceState;

/* loaded from: classes4.dex */
public class GroupConnectedActionProcessor extends GroupActionProcessor {
    private static final String TAG = Log.tag(GroupConnectedActionProcessor.class);

    public GroupConnectedActionProcessor(WebRtcInteractor webRtcInteractor) {
        super(webRtcInteractor, TAG);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleIsInCallQuery(WebRtcServiceState webRtcServiceState, ResultReceiver resultReceiver) {
        if (resultReceiver != null) {
            resultReceiver.send(1, null);
        }
        return webRtcServiceState;
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.GroupActionProcessor, org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleGroupLocalDeviceStateChanged(WebRtcServiceState webRtcServiceState) {
        Log.i(this.tag, "handleGroupLocalDeviceStateChanged():");
        WebRtcServiceState handleGroupLocalDeviceStateChanged = super.handleGroupLocalDeviceStateChanged(webRtcServiceState);
        GroupCall.LocalDeviceState localDeviceState = handleGroupLocalDeviceStateChanged.getCallInfoState().requireGroupCall().getLocalDeviceState();
        GroupCall.ConnectionState connectionState = localDeviceState.getConnectionState();
        GroupCall.JoinState joinState = localDeviceState.getJoinState();
        String str = this.tag;
        Log.i(str, "local device changed: " + connectionState + " " + joinState);
        WebRtcViewModel.GroupCallState groupCallStateForConnection = WebRtcUtil.groupCallStateForConnection(connectionState);
        if (connectionState == GroupCall.ConnectionState.CONNECTED || connectionState == GroupCall.ConnectionState.CONNECTING) {
            if (joinState == GroupCall.JoinState.JOINED) {
                groupCallStateForConnection = WebRtcViewModel.GroupCallState.CONNECTED_AND_JOINED;
            } else if (joinState == GroupCall.JoinState.JOINING) {
                groupCallStateForConnection = WebRtcViewModel.GroupCallState.CONNECTED_AND_JOINING;
            }
        }
        return handleGroupLocalDeviceStateChanged.builder().changeCallInfoState().groupCallState(groupCallStateForConnection).build();
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleSetEnableVideo(WebRtcServiceState webRtcServiceState, boolean z) {
        Log.i(TAG, "handleSetEnableVideo():");
        GroupCall requireGroupCall = webRtcServiceState.getCallInfoState().requireGroupCall();
        Camera requireCamera = webRtcServiceState.getVideoState().requireCamera();
        try {
            requireGroupCall.setOutgoingVideoMuted(!z);
            requireCamera.setEnabled(z);
            WebRtcServiceState build = webRtcServiceState.builder().changeLocalDeviceState().cameraState(requireCamera.getCameraState()).build();
            WebRtcUtil.enableSpeakerPhoneIfNeeded(this.webRtcInteractor, build);
            return build;
        } catch (CallException e) {
            return groupCallFailure(webRtcServiceState, "Unable set video muted", e);
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleSetMuteAudio(WebRtcServiceState webRtcServiceState, boolean z) {
        try {
            webRtcServiceState.getCallInfoState().requireGroupCall().setOutgoingAudioMuted(z);
            return webRtcServiceState.builder().changeLocalDeviceState().isMicrophoneEnabled(!z).build();
        } catch (CallException e) {
            return groupCallFailure(webRtcServiceState, "Unable to set audio muted", e);
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcEphemeralState handleGroupAudioLevelsChanged(WebRtcServiceState webRtcServiceState, WebRtcEphemeralState webRtcEphemeralState) {
        GroupCall.RemoteDeviceState remoteDeviceState;
        GroupCall requireGroupCall = webRtcServiceState.getCallInfoState().requireGroupCall();
        LongSparseArray<GroupCall.RemoteDeviceState> remoteDeviceStates = requireGroupCall.getRemoteDeviceStates();
        CallParticipant.AudioLevel fromRawAudioLevel = CallParticipant.AudioLevel.fromRawAudioLevel(requireGroupCall.getLocalDeviceState().getAudioLevel());
        HashMap hashMap = new HashMap();
        for (CallParticipant callParticipant : webRtcServiceState.getCallInfoState().getRemoteCallParticipants()) {
            CallParticipantId callParticipantId = callParticipant.getCallParticipantId();
            if (!(remoteDeviceStates == null || (remoteDeviceState = remoteDeviceStates.get(callParticipantId.getDemuxId())) == null)) {
                hashMap.put(callParticipantId, CallParticipant.AudioLevel.fromRawAudioLevel(remoteDeviceState.getAudioLevel()));
            }
        }
        return webRtcEphemeralState.copy(fromRawAudioLevel, hashMap);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleGroupJoinedMembershipChanged(WebRtcServiceState webRtcServiceState) {
        Log.i(this.tag, "handleGroupJoinedMembershipChanged():");
        GroupCall requireGroupCall = webRtcServiceState.getCallInfoState().requireGroupCall();
        PeekInfo peekInfo = requireGroupCall.getPeekInfo();
        if (peekInfo == null) {
            return webRtcServiceState;
        }
        CallId callId = RemotePeer.GROUP_CALL_ID;
        if (webRtcServiceState.getCallSetupState(callId).hasSentJoinedMessage()) {
            return webRtcServiceState;
        }
        String groupCallEraId = WebRtcUtil.getGroupCallEraId(requireGroupCall);
        this.webRtcInteractor.sendGroupCallMessage(webRtcServiceState.getCallInfoState().getCallRecipient(), groupCallEraId);
        ArrayList arrayList = new ArrayList(peekInfo.getJoinedMembers());
        if (!arrayList.contains(SignalStore.account().requireAci().uuid())) {
            arrayList.add(SignalStore.account().requireAci().uuid());
        }
        this.webRtcInteractor.updateGroupCallUpdateMessage(webRtcServiceState.getCallInfoState().getCallRecipient().getId(), groupCallEraId, arrayList, WebRtcUtil.isCallFull(peekInfo));
        return webRtcServiceState.builder().changeCallSetupState(callId).sentJoinedMessage(true).build();
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleLocalHangup(WebRtcServiceState webRtcServiceState) {
        Log.i(TAG, "handleLocalHangup():");
        GroupCall requireGroupCall = webRtcServiceState.getCallInfoState().requireGroupCall();
        try {
            requireGroupCall.disconnect();
            String groupCallEraId = WebRtcUtil.getGroupCallEraId(requireGroupCall);
            this.webRtcInteractor.sendGroupCallMessage(webRtcServiceState.getCallInfoState().getCallRecipient(), groupCallEraId);
            this.webRtcInteractor.updateGroupCallUpdateMessage(webRtcServiceState.getCallInfoState().getCallRecipient().getId(), groupCallEraId, Stream.of(webRtcServiceState.getCallInfoState().getRemoteCallParticipants()).map(new Function() { // from class: org.thoughtcrime.securesms.service.webrtc.GroupConnectedActionProcessor$$ExternalSyntheticLambda0
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return GroupConnectedActionProcessor.m2695$r8$lambda$PRBVKCojDmbHE5JPKLTaYVUfyQ((CallParticipant) obj);
                }
            }).toList(), false);
            WebRtcServiceState build = webRtcServiceState.builder().changeCallInfoState().callState(WebRtcViewModel.State.CALL_DISCONNECTED).groupCallState(WebRtcViewModel.GroupCallState.DISCONNECTED).build();
            this.webRtcInteractor.postStateUpdate(build);
            return terminateGroupCall(build);
        } catch (CallException e) {
            return groupCallFailure(webRtcServiceState, "Unable to disconnect from group call", e);
        }
    }

    public static /* synthetic */ UUID lambda$handleLocalHangup$0(CallParticipant callParticipant) {
        return callParticipant.getRecipient().requireServiceId().uuid();
    }
}
