package org.thoughtcrime.securesms.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.SystemClock;
import androidx.core.app.NotificationCompat;
import java.util.concurrent.TimeUnit;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.DummyActivity;
import org.thoughtcrime.securesms.MainActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.crypto.InvalidPassphraseException;
import org.thoughtcrime.securesms.crypto.MasterSecret;
import org.thoughtcrime.securesms.crypto.MasterSecretUtil;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.migrations.ApplicationMigrations;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.util.DynamicLanguage;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes.dex */
public class KeyCachingService extends Service {
    public static final String CLEAR_KEY_ACTION;
    public static final String CLEAR_KEY_EVENT;
    public static final String DISABLE_ACTION;
    public static final String KEY_PERMISSION;
    public static final String LOCALE_CHANGE_EVENT;
    public static final String LOCK_TOGGLED_EVENT;
    public static final String NEW_KEY_EVENT;
    private static final String PASSPHRASE_EXPIRED_EVENT;
    public static final int SERVICE_RUNNING_ID;
    private static final String TAG = Log.tag(KeyCachingService.class);
    private static MasterSecret masterSecret;
    private final IBinder binder = new KeySetBinder();
    private DynamicLanguage dynamicLanguage = new DynamicLanguage();

    public static synchronized boolean isLocked(Context context) {
        boolean z;
        synchronized (KeyCachingService.class) {
            z = masterSecret == null && (!TextSecurePreferences.isPasswordDisabled(context) || TextSecurePreferences.isScreenLockEnabled(context));
            if (z) {
                String str = TAG;
                Log.d(str, "Locked! PasswordDisabled: " + TextSecurePreferences.isPasswordDisabled(context) + ", ScreenLock: " + TextSecurePreferences.isScreenLockEnabled(context));
            }
        }
        return z;
    }

    public static synchronized MasterSecret getMasterSecret(Context context) {
        synchronized (KeyCachingService.class) {
            if (masterSecret == null && TextSecurePreferences.isPasswordDisabled(context) && !TextSecurePreferences.isScreenLockEnabled(context)) {
                try {
                    return MasterSecretUtil.getMasterSecret(context, MasterSecretUtil.UNENCRYPTED_PASSPHRASE);
                } catch (InvalidPassphraseException e) {
                    Log.w(TAG, e);
                }
            }
            return masterSecret;
        }
    }

    public static void onAppForegrounded(Context context) {
        if (TextSecurePreferences.isScreenLockEnabled(context) || !TextSecurePreferences.isPasswordDisabled(context)) {
            ServiceUtil.getAlarmManager(context).cancel(buildExpirationPendingIntent(context));
        }
    }

    public static void onAppBackgrounded(Context context) {
        startTimeoutIfAppropriate(context);
    }

    public void setMasterSecret(MasterSecret masterSecret2) {
        synchronized (KeyCachingService.class) {
            masterSecret = masterSecret2;
            foregroundService();
            broadcastNewSecret();
            startTimeoutIfAppropriate(this);
            new AsyncTask<Void, Void, Void>() { // from class: org.thoughtcrime.securesms.service.KeyCachingService.1
                public Void doInBackground(Void... voidArr) {
                    if (ApplicationMigrations.isUpdate(KeyCachingService.this)) {
                        return null;
                    }
                    ApplicationDependencies.getMessageNotifier().updateNotification(KeyCachingService.this);
                    return null;
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        }
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        if (intent == null) {
            return 2;
        }
        String str = TAG;
        Log.d(str, "onStartCommand, " + intent.getAction());
        if (intent.getAction() != null) {
            String action = intent.getAction();
            action.hashCode();
            char c = 65535;
            switch (action.hashCode()) {
                case -1347036216:
                    if (action.equals(LOCALE_CHANGE_EVENT)) {
                        c = 0;
                        break;
                    }
                    break;
                case 356132211:
                    if (action.equals(PASSPHRASE_EXPIRED_EVENT)) {
                        c = 1;
                        break;
                    }
                    break;
                case 422286320:
                    if (action.equals(LOCK_TOGGLED_EVENT)) {
                        c = 2;
                        break;
                    }
                    break;
                case 1754997664:
                    if (action.equals(DISABLE_ACTION)) {
                        c = 3;
                        break;
                    }
                    break;
                case 1846726661:
                    if (action.equals(CLEAR_KEY_ACTION)) {
                        c = 4;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    handleLocaleChanged();
                    break;
                case 1:
                    handleClearKey();
                    break;
                case 2:
                    handleLockToggled();
                    break;
                case 3:
                    handleDisableService();
                    break;
                case 4:
                    handleClearKey();
                    break;
            }
        }
        return 2;
    }

    @Override // android.app.Service
    public void onCreate() {
        Log.i(TAG, "onCreate()");
        super.onCreate();
        if (TextSecurePreferences.isPasswordDisabled(this) && !TextSecurePreferences.isScreenLockEnabled(this)) {
            try {
                setMasterSecret(MasterSecretUtil.getMasterSecret(this, MasterSecretUtil.UNENCRYPTED_PASSPHRASE));
            } catch (InvalidPassphraseException e) {
                Log.w(TAG, e);
            }
        }
    }

    @Override // android.app.Service
    public void onDestroy() {
        super.onDestroy();
        Log.w(TAG, "KCS Is Being Destroyed!");
        handleClearKey();
    }

    @Override // android.app.Service
    public void onTaskRemoved(Intent intent) {
        Intent intent2 = new Intent(this, DummyActivity.class);
        intent2.addFlags(SQLiteDatabase.CREATE_IF_NECESSARY);
        startActivity(intent2);
    }

    private void handleClearKey() {
        Log.i(TAG, "handleClearKey()");
        masterSecret = null;
        stopForeground(true);
        Intent intent = new Intent(CLEAR_KEY_EVENT);
        intent.setPackage(getApplicationContext().getPackageName());
        sendBroadcast(intent, KEY_PERMISSION);
        new AsyncTask<Void, Void, Void>() { // from class: org.thoughtcrime.securesms.service.KeyCachingService.2
            public Void doInBackground(Void... voidArr) {
                ApplicationDependencies.getMessageNotifier().updateNotification(KeyCachingService.this);
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
    }

    private void handleLockToggled() {
        stopForeground(true);
        try {
            setMasterSecret(MasterSecretUtil.getMasterSecret(this, MasterSecretUtil.UNENCRYPTED_PASSPHRASE));
        } catch (InvalidPassphraseException e) {
            Log.w(TAG, e);
        }
    }

    private void handleDisableService() {
        if (TextSecurePreferences.isPasswordDisabled(this) && !TextSecurePreferences.isScreenLockEnabled(this)) {
            stopForeground(true);
        }
    }

    private void handleLocaleChanged() {
        this.dynamicLanguage.updateServiceLocale(this);
        foregroundService();
    }

    private static void startTimeoutIfAppropriate(Context context) {
        long j;
        boolean isForegrounded = ApplicationDependencies.getAppForegroundObserver().isForegrounded();
        boolean z = true;
        boolean z2 = masterSecret != null;
        boolean z3 = TextSecurePreferences.isPassphraseTimeoutEnabled(context) && !TextSecurePreferences.isPasswordDisabled(context);
        if (TextSecurePreferences.getScreenLockTimeout(context) < 60 || !TextSecurePreferences.isScreenLockEnabled(context)) {
            z = false;
        }
        if (!isForegrounded && z2) {
            if (z3 || z) {
                long passphraseTimeoutInterval = (long) TextSecurePreferences.getPassphraseTimeoutInterval(context);
                long screenLockTimeout = TextSecurePreferences.getScreenLockTimeout(context);
                if (!TextSecurePreferences.isPasswordDisabled(context)) {
                    j = TimeUnit.MINUTES.toMillis(passphraseTimeoutInterval);
                } else {
                    j = TimeUnit.SECONDS.toMillis(screenLockTimeout);
                }
                String str = TAG;
                Log.i(str, "Starting timeout: " + j);
                AlarmManager alarmManager = ServiceUtil.getAlarmManager(context);
                PendingIntent buildExpirationPendingIntent = buildExpirationPendingIntent(context);
                alarmManager.cancel(buildExpirationPendingIntent);
                alarmManager.set(3, SystemClock.elapsedRealtime() + j, buildExpirationPendingIntent);
            }
        }
    }

    private void foregroundService() {
        if (!TextSecurePreferences.isPasswordDisabled(this) || TextSecurePreferences.isScreenLockEnabled(this)) {
            Log.i(TAG, "foregrounding KCS");
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NotificationChannels.LOCKED_STATUS);
            builder.setContentTitle(getString(R.string.KeyCachingService_passphrase_cached));
            builder.setContentText(getString(R.string.KeyCachingService_signal_passphrase_cached));
            builder.setSmallIcon(R.drawable.icon_cached);
            builder.setWhen(0);
            builder.setPriority(-2);
            builder.addAction(R.drawable.ic_menu_lock_dark, getString(R.string.KeyCachingService_lock), buildLockIntent());
            builder.setContentIntent(buildLaunchIntent());
            stopForeground(true);
            startForeground(SERVICE_RUNNING_ID, builder.build());
            return;
        }
        stopForeground(true);
    }

    private void broadcastNewSecret() {
        Log.i(TAG, "Broadcasting new secret...");
        Intent intent = new Intent(NEW_KEY_EVENT);
        intent.setPackage(getApplicationContext().getPackageName());
        sendBroadcast(intent, KEY_PERMISSION);
    }

    private PendingIntent buildLockIntent() {
        Intent intent = new Intent(this, KeyCachingService.class);
        intent.setAction(PASSPHRASE_EXPIRED_EVENT);
        return PendingIntent.getService(getApplicationContext(), 0, intent, getPendingIntentFlags());
    }

    private PendingIntent buildLaunchIntent() {
        return PendingIntent.getActivity(getApplicationContext(), 0, MainActivity.clearTop(this), getPendingIntentFlags());
    }

    private static PendingIntent buildExpirationPendingIntent(Context context) {
        return PendingIntent.getService(context, 0, new Intent(PASSPHRASE_EXPIRED_EVENT, null, context, KeyCachingService.class), getPendingIntentFlags());
    }

    private static int getPendingIntentFlags() {
        return Build.VERSION.SDK_INT >= 23 ? 201326592 : 134217728;
    }

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return this.binder;
    }

    /* loaded from: classes4.dex */
    public class KeySetBinder extends Binder {
        public KeySetBinder() {
            KeyCachingService.this = r1;
        }

        public KeyCachingService getService() {
            return KeyCachingService.this;
        }
    }
}
