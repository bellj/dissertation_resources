package org.thoughtcrime.securesms.service.webrtc;

import org.signal.core.util.logging.Log;
import org.signal.ringrtc.CallException;
import org.thoughtcrime.securesms.components.webrtc.BroadcastVideoSink;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.events.CallParticipantId;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceState;
import org.whispersystems.signalservice.api.messages.calls.OfferMessage;

/* loaded from: classes4.dex */
public class BeginCallActionProcessorDelegate extends WebRtcActionProcessor {
    public BeginCallActionProcessorDelegate(WebRtcInteractor webRtcInteractor, String str) {
        super(webRtcInteractor, str);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleOutgoingCall(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer, OfferMessage.Type type) {
        remotePeer.setCallStartTimestamp(System.currentTimeMillis());
        WebRtcServiceState build = webRtcServiceState.builder().actionProcessor(new OutgoingCallActionProcessor(this.webRtcInteractor)).changeCallInfoState().callRecipient(remotePeer.getRecipient()).callState(WebRtcViewModel.State.CALL_OUTGOING).putRemotePeer(remotePeer).putParticipant(remotePeer.getRecipient(), CallParticipant.createRemote(new CallParticipantId(remotePeer.getRecipient()), remotePeer.getRecipient(), null, new BroadcastVideoSink(webRtcServiceState.getVideoState().getLockableEglBase(), false, true, webRtcServiceState.getLocalDeviceState().getOrientation().getDegrees()), true, true, false, 0, true, 0, false, CallParticipant.DeviceOrdinal.PRIMARY)).build();
        try {
            this.webRtcInteractor.getCallManager().call(remotePeer, WebRtcUtil.getCallMediaTypeFromOfferType(type), Integer.valueOf(SignalStore.account().getDeviceId()));
            return build;
        } catch (CallException e) {
            return callFailure(build, "Unable to create outgoing call: ", e);
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleStartIncomingCall(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer, OfferMessage.Type type) {
        remotePeer.answering();
        String str = this.tag;
        Log.i(str, "assign activePeer callId: " + remotePeer.getCallId() + " key: " + remotePeer.hashCode());
        this.webRtcInteractor.setCallInProgressNotification(4, remotePeer);
        this.webRtcInteractor.retrieveTurnServers(remotePeer);
        this.webRtcInteractor.initializeAudioForCall();
        if (this.webRtcInteractor.addNewIncomingCall(remotePeer.getId(), remotePeer.getCallId().longValue().longValue(), type == OfferMessage.Type.VIDEO_CALL)) {
            return webRtcServiceState.builder().actionProcessor(new IncomingCallActionProcessor(this.webRtcInteractor)).changeCallSetupState(remotePeer.getCallId()).waitForTelecom(AndroidTelecomUtil.getTelecomSupported()).telecomApproved(false).commit().changeCallInfoState().callRecipient(remotePeer.getRecipient()).activePeer(remotePeer).callState(WebRtcViewModel.State.CALL_INCOMING).putParticipant(remotePeer.getRecipient(), CallParticipant.createRemote(new CallParticipantId(remotePeer.getRecipient()), remotePeer.getRecipient(), null, new BroadcastVideoSink(webRtcServiceState.getVideoState().getLockableEglBase(), false, true, webRtcServiceState.getLocalDeviceState().getOrientation().getDegrees()), true, true, false, 0, true, 0, false, CallParticipant.DeviceOrdinal.PRIMARY)).build();
        }
        Log.i(this.tag, "Unable to add new incoming call");
        return handleDropCall(webRtcServiceState, remotePeer.getCallId().longValue().longValue());
    }
}
