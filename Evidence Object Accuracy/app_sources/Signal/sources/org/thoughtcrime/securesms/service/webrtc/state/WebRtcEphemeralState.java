package org.thoughtcrime.securesms.service.webrtc.state;

import java.util.Map;
import kotlin.Metadata;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.events.CallParticipantId;

/* compiled from: WebRtcEphemeralState.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B%\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0014\b\u0002\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00030\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\f\u001a\u00020\u0003HÆ\u0003J\u0015\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00030\u0005HÆ\u0003J)\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u0014\b\u0002\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00030\u0005HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u001d\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00030\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/service/webrtc/state/WebRtcEphemeralState;", "", "localAudioLevel", "Lorg/thoughtcrime/securesms/events/CallParticipant$AudioLevel;", "remoteAudioLevels", "", "Lorg/thoughtcrime/securesms/events/CallParticipantId;", "(Lorg/thoughtcrime/securesms/events/CallParticipant$AudioLevel;Ljava/util/Map;)V", "getLocalAudioLevel", "()Lorg/thoughtcrime/securesms/events/CallParticipant$AudioLevel;", "getRemoteAudioLevels", "()Ljava/util/Map;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class WebRtcEphemeralState {
    private final CallParticipant.AudioLevel localAudioLevel;
    private final Map<CallParticipantId, CallParticipant.AudioLevel> remoteAudioLevels;

    public WebRtcEphemeralState() {
        this(null, null, 3, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.service.webrtc.state.WebRtcEphemeralState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ WebRtcEphemeralState copy$default(WebRtcEphemeralState webRtcEphemeralState, CallParticipant.AudioLevel audioLevel, Map map, int i, Object obj) {
        if ((i & 1) != 0) {
            audioLevel = webRtcEphemeralState.localAudioLevel;
        }
        if ((i & 2) != 0) {
            map = webRtcEphemeralState.remoteAudioLevels;
        }
        return webRtcEphemeralState.copy(audioLevel, map);
    }

    public final CallParticipant.AudioLevel component1() {
        return this.localAudioLevel;
    }

    public final Map<CallParticipantId, CallParticipant.AudioLevel> component2() {
        return this.remoteAudioLevels;
    }

    public final WebRtcEphemeralState copy(CallParticipant.AudioLevel audioLevel, Map<CallParticipantId, ? extends CallParticipant.AudioLevel> map) {
        Intrinsics.checkNotNullParameter(audioLevel, "localAudioLevel");
        Intrinsics.checkNotNullParameter(map, "remoteAudioLevels");
        return new WebRtcEphemeralState(audioLevel, map);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof WebRtcEphemeralState)) {
            return false;
        }
        WebRtcEphemeralState webRtcEphemeralState = (WebRtcEphemeralState) obj;
        return this.localAudioLevel == webRtcEphemeralState.localAudioLevel && Intrinsics.areEqual(this.remoteAudioLevels, webRtcEphemeralState.remoteAudioLevels);
    }

    public int hashCode() {
        return (this.localAudioLevel.hashCode() * 31) + this.remoteAudioLevels.hashCode();
    }

    public String toString() {
        return "WebRtcEphemeralState(localAudioLevel=" + this.localAudioLevel + ", remoteAudioLevels=" + this.remoteAudioLevels + ')';
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.Map<org.thoughtcrime.securesms.events.CallParticipantId, ? extends org.thoughtcrime.securesms.events.CallParticipant$AudioLevel> */
    /* JADX WARN: Multi-variable type inference failed */
    public WebRtcEphemeralState(CallParticipant.AudioLevel audioLevel, Map<CallParticipantId, ? extends CallParticipant.AudioLevel> map) {
        Intrinsics.checkNotNullParameter(audioLevel, "localAudioLevel");
        Intrinsics.checkNotNullParameter(map, "remoteAudioLevels");
        this.localAudioLevel = audioLevel;
        this.remoteAudioLevels = map;
    }

    public /* synthetic */ WebRtcEphemeralState(CallParticipant.AudioLevel audioLevel, Map map, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? CallParticipant.AudioLevel.LOWEST : audioLevel, (i & 2) != 0 ? MapsKt__MapsKt.emptyMap() : map);
    }

    public final CallParticipant.AudioLevel getLocalAudioLevel() {
        return this.localAudioLevel;
    }

    public final Map<CallParticipantId, CallParticipant.AudioLevel> getRemoteAudioLevels() {
        return this.remoteAudioLevels;
    }
}
