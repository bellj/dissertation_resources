package org.thoughtcrime.securesms.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class PanicResponderListener extends BroadcastReceiver {
    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        if (intent != null && !TextSecurePreferences.isPasswordDisabled(context) && "info.guardianproject.panic.action.TRIGGER".equals(intent.getAction())) {
            Intent intent2 = new Intent(context, KeyCachingService.class);
            intent2.setAction(KeyCachingService.CLEAR_KEY_ACTION);
            context.startService(intent2);
        }
    }
}
