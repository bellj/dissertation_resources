package org.thoughtcrime.securesms.service;

import android.content.Context;
import android.content.Intent;
import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.DirectoryRefreshJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes.dex */
public class DirectoryRefreshListener extends PersistentAlarmManagerListener {
    @Override // org.thoughtcrime.securesms.service.PersistentAlarmManagerListener
    protected long getNextScheduledExecutionTime(Context context) {
        return TextSecurePreferences.getDirectoryRefreshTime(context);
    }

    @Override // org.thoughtcrime.securesms.service.PersistentAlarmManagerListener
    protected long onAlarm(Context context, long j) {
        if (j != 0 && SignalStore.account().isRegistered()) {
            ApplicationDependencies.getJobManager().add(new DirectoryRefreshJob(true));
        }
        long currentTimeMillis = System.currentTimeMillis() + TimeUnit.SECONDS.toMillis((long) FeatureFlags.cdsRefreshIntervalSeconds());
        TextSecurePreferences.setDirectoryRefreshTime(context, currentTimeMillis);
        return currentTimeMillis;
    }

    public static void schedule(Context context) {
        new DirectoryRefreshListener().onReceive(context, new Intent());
    }
}
