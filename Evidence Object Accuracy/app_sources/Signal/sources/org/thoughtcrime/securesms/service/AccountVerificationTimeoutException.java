package org.thoughtcrime.securesms.service;

/* loaded from: classes4.dex */
public class AccountVerificationTimeoutException extends Exception {
    public AccountVerificationTimeoutException() {
    }

    public AccountVerificationTimeoutException(String str) {
        super(str);
    }

    public AccountVerificationTimeoutException(String str, Throwable th) {
        super(str, th);
    }

    public AccountVerificationTimeoutException(Throwable th) {
        super(th);
    }
}
