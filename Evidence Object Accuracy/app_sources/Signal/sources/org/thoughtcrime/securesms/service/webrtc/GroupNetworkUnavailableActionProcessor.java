package org.thoughtcrime.securesms.service.webrtc;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.components.webrtc.EglBaseWrapper;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceState;

/* loaded from: classes4.dex */
class GroupNetworkUnavailableActionProcessor extends WebRtcActionProcessor {
    private static final String TAG = Log.tag(GroupNetworkUnavailableActionProcessor.class);

    public GroupNetworkUnavailableActionProcessor(WebRtcInteractor webRtcInteractor) {
        super(webRtcInteractor, TAG);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handlePreJoinCall(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer) {
        Log.i(TAG, "handlePreJoinCall():");
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return webRtcServiceState.builder().changeCallInfoState().callState(WebRtcViewModel.State.NETWORK_FAILURE).groupCall(this.webRtcInteractor.getCallManager().createGroupCall(webRtcServiceState.getCallInfoState().getCallRecipient().requireGroupId().getDecodedId(), SignalStore.internalValues().groupCallingServer(), new byte[0], null, RingRtcDynamicConfiguration.getAudioProcessingMethod(), this.webRtcInteractor.getGroupCallObserver())).groupCallState(WebRtcViewModel.GroupCallState.DISCONNECTED).build();
        }
        GroupPreJoinActionProcessor groupPreJoinActionProcessor = new GroupPreJoinActionProcessor(this.webRtcInteractor);
        return groupPreJoinActionProcessor.handlePreJoinCall(webRtcServiceState.builder().actionProcessor(groupPreJoinActionProcessor).build(), remotePeer);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleCancelPreJoinCall(WebRtcServiceState webRtcServiceState) {
        Log.i(TAG, "handleCancelPreJoinCall():");
        WebRtcVideoUtil.deinitializeVideo(webRtcServiceState);
        EglBaseWrapper.releaseEglBase(RemotePeer.GROUP_CALL_ID.longValue());
        return new WebRtcServiceState(new IdleActionProcessor(this.webRtcInteractor));
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleNetworkChanged(WebRtcServiceState webRtcServiceState, boolean z) {
        return z ? webRtcServiceState.builder().actionProcessor(new GroupPreJoinActionProcessor(this.webRtcInteractor)).changeCallInfoState().callState(WebRtcViewModel.State.CALL_PRE_JOIN).build() : webRtcServiceState;
    }
}
