package org.thoughtcrime.securesms.service.webrtc;

import android.net.Uri;
import j$.util.Optional;
import java.util.UUID;
import org.signal.core.util.logging.Log;
import org.signal.ringrtc.CallException;
import org.signal.ringrtc.CallId;
import org.signal.ringrtc.CallManager;
import org.signal.ringrtc.GroupCall;
import org.thoughtcrime.securesms.components.webrtc.BroadcastVideoSink;
import org.thoughtcrime.securesms.components.webrtc.EglBaseWrapper;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.events.CallParticipantId;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.DoNotDisturbUtil;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceState;
import org.thoughtcrime.securesms.util.NetworkUtil;
import org.thoughtcrime.securesms.webrtc.locks.LockManager;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes4.dex */
public final class IncomingGroupCallActionProcessor extends DeviceAwareActionProcessor {
    private static final String TAG = Log.tag(IncomingGroupCallActionProcessor.class);

    public IncomingGroupCallActionProcessor(WebRtcInteractor webRtcInteractor) {
        super(webRtcInteractor, TAG);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleGroupCallRingUpdate(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer, GroupId.V2 v2, long j, UUID uuid, CallManager.RingUpdate ringUpdate) {
        String str = TAG;
        Log.i(str, "handleGroupCallRingUpdate(): recipient: " + remotePeer.getId() + " ring: " + j + " update: " + ringUpdate);
        Recipient recipient = remotePeer.getRecipient();
        CallId callId = RemotePeer.GROUP_CALL_ID;
        boolean z = j == webRtcServiceState.getCallSetupState(callId).getRingId();
        boolean isRinging = webRtcServiceState.getCallInfoState().getGroupCallState().isRinging();
        if (SignalDatabase.groupCallRings().isCancelled(j)) {
            try {
                Log.i(str, "Ignoring incoming ring request for already cancelled ring: " + j);
                this.webRtcInteractor.getCallManager().cancelGroupRing(v2.getDecodedId(), j, null);
            } catch (CallException e) {
                String str2 = TAG;
                Log.w(str2, "Error while trying to cancel ring: " + j, e);
            }
            return webRtcServiceState;
        } else if (ringUpdate != CallManager.RingUpdate.REQUESTED) {
            SignalDatabase.groupCallRings().insertOrUpdateGroupRing(j, System.currentTimeMillis(), ringUpdate);
            if (!z || !isRinging) {
                return webRtcServiceState;
            }
            Log.i(str, "Cancelling current ring: " + j);
            WebRtcServiceState build = webRtcServiceState.builder().changeCallInfoState().callState(WebRtcViewModel.State.CALL_DISCONNECTED).build();
            this.webRtcInteractor.postStateUpdate(build);
            return terminateGroupCall(build);
        } else if (!z && isRinging) {
            try {
                Log.i(str, "Already ringing so reply busy for new ring: " + j);
                this.webRtcInteractor.getCallManager().cancelGroupRing(v2.getDecodedId(), j, CallManager.RingCancelReason.Busy);
            } catch (CallException e2) {
                String str3 = TAG;
                Log.w(str3, "Error while trying to cancel ring: " + j, e2);
            }
            return webRtcServiceState;
        } else if (z) {
            Log.i(str, "Already ringing for ring: " + j);
            return webRtcServiceState;
        } else {
            Log.i(str, "Requesting new ring: " + j);
            SignalDatabase.groupCallRings().insertGroupRing(j, System.currentTimeMillis(), ringUpdate);
            WebRtcServiceState initializeVideo = WebRtcVideoUtil.initializeVideo(this.context, this.webRtcInteractor.getCameraEventListener(), webRtcServiceState, callId.longValue());
            this.webRtcInteractor.setCallInProgressNotification(1, remotePeer);
            this.webRtcInteractor.updatePhoneState(LockManager.PhoneState.INTERACTIVE);
            this.webRtcInteractor.initializeAudioForCall();
            boolean shouldDisturbUserWithCall = DoNotDisturbUtil.shouldDisturbUserWithCall(this.context.getApplicationContext());
            if (shouldDisturbUserWithCall && !this.webRtcInteractor.startWebRtcCallActivityIfPossible()) {
                Log.i(str, "Unable to start call activity due to OS version or not being in the foreground");
                ApplicationDependencies.getAppForegroundObserver().addListener(this.webRtcInteractor.getForegroundListener());
            }
            if (shouldDisturbUserWithCall && SignalStore.settings().isCallNotificationsEnabled()) {
                Uri callRingtone = recipient.resolve().getCallRingtone();
                RecipientDatabase.VibrateState callVibrate = recipient.resolve().getCallVibrate();
                if (callRingtone == null) {
                    callRingtone = SignalStore.settings().getCallRingtone();
                }
                this.webRtcInteractor.startIncomingRinger(callRingtone, callVibrate == RecipientDatabase.VibrateState.ENABLED || (callVibrate == RecipientDatabase.VibrateState.DEFAULT && SignalStore.settings().isCallVibrateEnabled()));
            }
            this.webRtcInteractor.registerPowerButtonReceiver();
            return initializeVideo.builder().changeCallSetupState(callId).isRemoteVideoOffer(true).ringId(j).ringerRecipient(Recipient.externalPush(ServiceId.from(uuid))).commit().changeCallInfoState().activePeer(new RemotePeer(initializeVideo.getCallInfoState().getCallRecipient().getId(), callId)).callRecipient(remotePeer.getRecipient()).callState(WebRtcViewModel.State.CALL_INCOMING).groupCallState(WebRtcViewModel.GroupCallState.RINGING).putParticipant(remotePeer.getRecipient(), CallParticipant.createRemote(new CallParticipantId(remotePeer.getRecipient()), remotePeer.getRecipient(), null, new BroadcastVideoSink(initializeVideo.getVideoState().getLockableEglBase(), false, true, initializeVideo.getLocalDeviceState().getOrientation().getDegrees()), true, true, false, 0, true, 0, false, CallParticipant.DeviceOrdinal.PRIMARY)).build();
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleAcceptCall(WebRtcServiceState webRtcServiceState, boolean z) {
        byte[] decodedId = webRtcServiceState.getCallInfoState().getCallRecipient().requireGroupId().getDecodedId();
        boolean z2 = false;
        GroupCall createGroupCall = this.webRtcInteractor.getCallManager().createGroupCall(decodedId, SignalStore.internalValues().groupCallingServer(), new byte[0], 200, RingRtcDynamicConfiguration.getAudioProcessingMethod(), this.webRtcInteractor.getGroupCallObserver());
        try {
            createGroupCall.setOutgoingAudioMuted(true);
            createGroupCall.setOutgoingVideoMuted(true);
            createGroupCall.setBandwidthMode(NetworkUtil.getCallingBandwidthMode(this.context, createGroupCall.getLocalDeviceState().getNetworkRoute().getLocalAdapterType()));
            String str = TAG;
            Log.i(str, "Connecting to group call: " + webRtcServiceState.getCallInfoState().getCallRecipient().getId());
            createGroupCall.connect();
            WebRtcServiceState build = webRtcServiceState.builder().changeCallInfoState().groupCall(createGroupCall).groupCallState(WebRtcViewModel.GroupCallState.DISCONNECTED).commit().changeCallSetupState(RemotePeer.GROUP_CALL_ID).isRemoteVideoOffer(false).enableVideoOnCreate(z).build();
            this.webRtcInteractor.setCallInProgressNotification(4, build.getCallInfoState().getCallRecipient());
            this.webRtcInteractor.updatePhoneState(WebRtcUtil.getInCallPhoneState(this.context));
            this.webRtcInteractor.initializeAudioForCall();
            try {
                createGroupCall.setOutgoingVideoSource(build.getVideoState().requireLocalSink(), build.getVideoState().requireCamera());
                createGroupCall.setOutgoingVideoMuted(z);
                if (!build.getLocalDeviceState().isMicrophoneEnabled()) {
                    z2 = true;
                }
                createGroupCall.setOutgoingAudioMuted(z2);
                createGroupCall.setBandwidthMode(NetworkUtil.getCallingBandwidthMode(this.context, createGroupCall.getLocalDeviceState().getNetworkRoute().getLocalAdapterType()));
                createGroupCall.join();
                return build.builder().actionProcessor(new GroupJoiningActionProcessor(this.webRtcInteractor)).changeCallInfoState().callState(WebRtcViewModel.State.CALL_OUTGOING).groupCallState(WebRtcViewModel.GroupCallState.CONNECTED_AND_JOINING).commit().changeLocalDeviceState().build();
            } catch (CallException e) {
                return groupCallFailure(build, "Unable to join group call", e);
            }
        } catch (CallException e2) {
            return groupCallFailure(webRtcServiceState, "Unable to connect to group call", e2);
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleDenyCall(WebRtcServiceState webRtcServiceState) {
        Optional<GroupId> groupId = webRtcServiceState.getCallInfoState().getCallRecipient().getGroupId();
        long ringId = webRtcServiceState.getCallSetupState(RemotePeer.GROUP_CALL_ID).getRingId();
        SignalDatabase.groupCallRings().insertOrUpdateGroupRing(ringId, System.currentTimeMillis(), CallManager.RingUpdate.DECLINED_ON_ANOTHER_DEVICE);
        try {
            this.webRtcInteractor.getCallManager().cancelGroupRing(groupId.get().getDecodedId(), ringId, CallManager.RingCancelReason.DeclinedByUser);
        } catch (CallException e) {
            String str = TAG;
            Log.w(str, "Error while trying to cancel ring " + ringId, e);
        }
        this.webRtcInteractor.updatePhoneState(LockManager.PhoneState.PROCESSING);
        this.webRtcInteractor.stopAudio(false);
        this.webRtcInteractor.updatePhoneState(LockManager.PhoneState.IDLE);
        this.webRtcInteractor.stopForegroundService();
        WebRtcServiceState deinitializeVideo = WebRtcVideoUtil.deinitializeVideo(webRtcServiceState);
        CallId callId = RemotePeer.GROUP_CALL_ID;
        EglBaseWrapper.releaseEglBase(callId.longValue());
        return deinitializeVideo.builder().actionProcessor(new IdleActionProcessor(this.webRtcInteractor)).terminate(callId).build();
    }
}
