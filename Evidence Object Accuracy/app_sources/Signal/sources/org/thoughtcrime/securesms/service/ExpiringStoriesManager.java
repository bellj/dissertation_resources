package org.thoughtcrime.securesms.service;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.RangesKt___RangesKt;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;

/* compiled from: ExpiringStoriesManager.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0007\u0018\u0000 \u00102\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0003\u0010\u0011\u0012B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0015J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\n\u001a\u00020\u0002H\u0015J\n\u0010\r\u001a\u0004\u0018\u00010\u0002H\u0015J\u0018\u0010\u000e\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\fH\u0015R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/service/ExpiringStoriesManager;", "Lorg/thoughtcrime/securesms/service/TimedEventManager;", "Lorg/thoughtcrime/securesms/service/ExpiringStoriesManager$Event;", "application", "Landroid/app/Application;", "(Landroid/app/Application;)V", "mmsDatabase", "Lorg/thoughtcrime/securesms/database/MmsDatabase;", "executeEvent", "", "event", "getDelayForEvent", "", "getNextClosestEvent", "scheduleAlarm", "delay", "Companion", "Event", "ExpireStoriesAlarm", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class ExpiringStoriesManager extends TimedEventManager<Event> {
    public static final Companion Companion = new Companion(null);
    private static final long STORY_LIFESPAN = TimeUnit.HOURS.toMillis(24);
    private static final String TAG = Log.tag(ExpiringStoriesManager.class);
    private final MmsDatabase mmsDatabase = SignalDatabase.Companion.mms();

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ExpiringStoriesManager(Application application) {
        super(application, "ExpiringStoriesManager");
        Intrinsics.checkNotNullParameter(application, "application");
        scheduleIfNecessary();
    }

    /* compiled from: ExpiringStoriesManager.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/service/ExpiringStoriesManager$Companion;", "", "()V", "STORY_LIFESPAN", "", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    @Override // org.thoughtcrime.securesms.service.TimedEventManager
    public Event getNextClosestEvent() {
        Long oldestStorySendTimestamp = this.mmsDatabase.getOldestStorySendTimestamp(SignalStore.storyValues().getUserHasSeenOnboardingStory());
        if (oldestStorySendTimestamp == null) {
            return null;
        }
        long j = RangesKt___RangesKt.coerceAtLeast(STORY_LIFESPAN - (System.currentTimeMillis() - oldestStorySendTimestamp.longValue()), 0);
        String str = TAG;
        Log.i(str, "The oldest story needs to be deleted in " + j + " ms.");
        return new Event(j);
    }

    public void executeEvent(Event event) {
        Intrinsics.checkNotNullParameter(event, "event");
        long currentTimeMillis = System.currentTimeMillis() - STORY_LIFESPAN;
        int deleteStoriesOlderThan = this.mmsDatabase.deleteStoriesOlderThan(currentTimeMillis, SignalStore.storyValues().getUserHasSeenOnboardingStory());
        String str = TAG;
        Log.i(str, "Deleted " + deleteStoriesOlderThan + " stories before " + currentTimeMillis);
    }

    public long getDelayForEvent(Event event) {
        Intrinsics.checkNotNullParameter(event, "event");
        return event.getDelay();
    }

    @Override // org.thoughtcrime.securesms.service.TimedEventManager
    protected void scheduleAlarm(Application application, long j) {
        Intrinsics.checkNotNullParameter(application, "application");
        TimedEventManager.setAlarm(application, j, ExpireStoriesAlarm.class);
    }

    /* compiled from: ExpiringStoriesManager.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\f\u001a\u00020\rHÖ\u0001J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/service/ExpiringStoriesManager$Event;", "", "delay", "", "(J)V", "getDelay", "()J", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Event {
        private final long delay;

        public static /* synthetic */ Event copy$default(Event event, long j, int i, Object obj) {
            if ((i & 1) != 0) {
                j = event.delay;
            }
            return event.copy(j);
        }

        public final long component1() {
            return this.delay;
        }

        public final Event copy(long j) {
            return new Event(j);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof Event) && this.delay == ((Event) obj).delay;
        }

        public int hashCode() {
            return SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.delay);
        }

        public String toString() {
            return "Event(delay=" + this.delay + ')';
        }

        public Event(long j) {
            this.delay = j;
        }

        public final long getDelay() {
            return this.delay;
        }
    }

    /* compiled from: ExpiringStoriesManager.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \t2\u00020\u0001:\u0001\tB\u0005¢\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0016¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/service/ExpiringStoriesManager$ExpireStoriesAlarm;", "Landroid/content/BroadcastReceiver;", "()V", "onReceive", "", "context", "Landroid/content/Context;", "intent", "Landroid/content/Intent;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ExpireStoriesAlarm extends BroadcastReceiver {
        public static final Companion Companion = new Companion(null);
        private static final String TAG = Log.tag(ExpireStoriesAlarm.class);

        /* compiled from: ExpiringStoriesManager.kt */
        @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/service/ExpiringStoriesManager$ExpireStoriesAlarm$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive()");
            ApplicationDependencies.getExpireStoriesManager().scheduleIfNecessary();
        }
    }
}
