package org.thoughtcrime.securesms.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.widget.Toast;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.sms.OutgoingTextMessage;
import org.thoughtcrime.securesms.util.Rfc5724Uri;

/* loaded from: classes4.dex */
public class QuickResponseService extends IntentService {
    private static final String TAG = Log.tag(QuickResponseService.class);

    public QuickResponseService() {
        super("QuickResponseService");
    }

    @Override // android.app.IntentService
    protected void onHandleIntent(Intent intent) {
        if (!"android.intent.action.RESPOND_VIA_MESSAGE".equals(intent.getAction())) {
            String str = TAG;
            Log.w(str, "Received unknown intent: " + intent.getAction());
        } else if (KeyCachingService.isLocked(this)) {
            Log.w(TAG, "Got quick response request when locked...");
            Toast.makeText(this, (int) R.string.QuickResponseService_quick_response_unavailable_when_Signal_is_locked, 1).show();
        } else {
            try {
                Rfc5724Uri rfc5724Uri = new Rfc5724Uri(intent.getDataString());
                String stringExtra = intent.getStringExtra("android.intent.extra.TEXT");
                String path = rfc5724Uri.getPath();
                if (path.contains("%")) {
                    path = URLDecoder.decode(path);
                }
                Recipient external = Recipient.external(this, path);
                int intValue = external.getDefaultSubscriptionId().orElse(-1).intValue();
                long millis = TimeUnit.SECONDS.toMillis((long) external.getExpiresInSeconds());
                if (!TextUtils.isEmpty(stringExtra)) {
                    MessageSender.send((Context) this, new OutgoingTextMessage(external, stringExtra, millis, intValue), -1L, false, (String) null, (MessageDatabase.InsertListener) null);
                }
            } catch (URISyntaxException e) {
                Toast.makeText(this, (int) R.string.QuickResponseService_problem_sending_message, 1).show();
                Log.w(TAG, e);
            }
        }
    }
}
