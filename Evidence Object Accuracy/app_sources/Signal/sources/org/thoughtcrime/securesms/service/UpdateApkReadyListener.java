package org.thoughtcrime.securesms.service;

import android.app.DownloadManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import androidx.core.app.NotificationCompat;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import org.signal.core.util.Hex;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.util.FileProviderUtil;
import org.thoughtcrime.securesms.util.FileUtils;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class UpdateApkReadyListener extends BroadcastReceiver {
    private static final String TAG = Log.tag(UpdateApkReadyListener.class);

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        String str = TAG;
        Log.i(str, "onReceive()");
        if ("android.intent.action.DOWNLOAD_COMPLETE".equals(intent.getAction())) {
            long longExtra = intent.getLongExtra("extra_download_id", -2);
            if (longExtra == TextSecurePreferences.getUpdateApkDownloadId(context)) {
                Uri localUriForDownloadId = getLocalUriForDownloadId(context, longExtra);
                String updateApkDigest = TextSecurePreferences.getUpdateApkDigest(context);
                if (localUriForDownloadId == null) {
                    Log.w(str, "Downloaded local URI is null?");
                } else if (isMatchingDigest(context, longExtra, updateApkDigest)) {
                    displayInstallNotification(context, localUriForDownloadId);
                } else {
                    Log.w(str, "Downloaded APK doesn't match digest...");
                }
            }
        }
    }

    private void displayInstallNotification(Context context, Uri uri) {
        Intent intent = new Intent("android.intent.action.INSTALL_PACKAGE");
        intent.setFlags(1);
        intent.setData(uri);
        ServiceUtil.getNotificationManager(context).notify(666, new NotificationCompat.Builder(context, NotificationChannels.APP_UPDATES).setOngoing(true).setContentTitle(context.getString(R.string.UpdateApkReadyListener_Signal_update)).setContentText(context.getString(R.string.UpdateApkReadyListener_a_new_version_of_signal_is_available_tap_to_update)).setSmallIcon(R.drawable.ic_notification).setColor(context.getResources().getColor(R.color.core_ultramarine)).setPriority(1).setCategory("reminder").setContentIntent(PendingIntent.getActivity(context, 0, intent, 0)).build());
    }

    private Uri getLocalUriForDownloadId(Context context, long j) {
        String string;
        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterById(j);
        Cursor query2 = ((DownloadManager) context.getSystemService("download")).query(query);
        if (query2 != null) {
            try {
                if (query2.moveToFirst() && (string = query2.getString(query2.getColumnIndexOrThrow("local_uri"))) != null) {
                    return FileProviderUtil.getUriFor(context, new File(Uri.parse(string).getPath()));
                }
            } finally {
                query2.close();
            }
        }
        return query2 != null ? null : null;
    }

    private boolean isMatchingDigest(Context context, long j, String str) {
        if (str == null) {
            return false;
        }
        try {
            byte[] fromStringCondensed = Hex.fromStringCondensed(str);
            FileInputStream fileInputStream = new FileInputStream(((DownloadManager) context.getSystemService("download")).openDownloadedFile(j).getFileDescriptor());
            byte[] fileDigest = FileUtils.getFileDigest(fileInputStream);
            fileInputStream.close();
            return MessageDigest.isEqual(fileDigest, fromStringCondensed);
        } catch (IOException e) {
            Log.w(TAG, e);
            return false;
        }
    }
}
