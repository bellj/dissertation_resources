package org.thoughtcrime.securesms.service;

import android.content.Context;
import android.content.Intent;
import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.jobs.LocalBackupJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes.dex */
public class LocalBackupListener extends PersistentAlarmManagerListener {
    private static final long INTERVAL = TimeUnit.DAYS.toMillis(1);

    @Override // org.thoughtcrime.securesms.service.PersistentAlarmManagerListener
    protected long getNextScheduledExecutionTime(Context context) {
        return TextSecurePreferences.getNextBackupTime(context);
    }

    @Override // org.thoughtcrime.securesms.service.PersistentAlarmManagerListener
    protected long onAlarm(Context context, long j) {
        if (SignalStore.settings().isBackupEnabled()) {
            LocalBackupJob.enqueue(false);
        }
        return setNextBackupTimeToIntervalFromNow(context);
    }

    public static void schedule(Context context) {
        if (SignalStore.settings().isBackupEnabled()) {
            new LocalBackupListener().onReceive(context, new Intent());
        }
    }

    public static long setNextBackupTimeToIntervalFromNow(Context context) {
        long currentTimeMillis = System.currentTimeMillis() + INTERVAL;
        TextSecurePreferences.setNextBackupTime(context, currentTimeMillis);
        return currentTimeMillis;
    }
}
