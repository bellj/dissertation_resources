package org.thoughtcrime.securesms.service;

import android.content.Context;
import android.content.Intent;
import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.RotateCertificateJob;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes.dex */
public class RotateSenderCertificateListener extends PersistentAlarmManagerListener {
    private static final long INTERVAL = TimeUnit.DAYS.toMillis(1);

    @Override // org.thoughtcrime.securesms.service.PersistentAlarmManagerListener
    protected long getNextScheduledExecutionTime(Context context) {
        return TextSecurePreferences.getUnidentifiedAccessCertificateRotationTime(context);
    }

    @Override // org.thoughtcrime.securesms.service.PersistentAlarmManagerListener
    protected long onAlarm(Context context, long j) {
        ApplicationDependencies.getJobManager().add(new RotateCertificateJob());
        long currentTimeMillis = System.currentTimeMillis() + INTERVAL;
        TextSecurePreferences.setUnidentifiedAccessCertificateRotationTime(context, currentTimeMillis);
        return currentTimeMillis;
    }

    public static void schedule(Context context) {
        new RotateSenderCertificateListener().onReceive(context, new Intent());
    }
}
