package org.thoughtcrime.securesms.service.webrtc;

import android.util.LongSparseArray;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.zkgroup.groups.UuidCiphertext;
import org.signal.ringrtc.CallException;
import org.signal.ringrtc.CallId;
import org.signal.ringrtc.GroupCall;
import org.thoughtcrime.securesms.components.webrtc.BroadcastVideoSink;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.events.CallParticipantId;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.thoughtcrime.securesms.service.webrtc.WebRtcData;
import org.thoughtcrime.securesms.service.webrtc.state.VideoState;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceState;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceStateBuilder;
import org.webrtc.VideoTrack;
import org.whispersystems.signalservice.api.messages.calls.OfferMessage;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes4.dex */
public class GroupActionProcessor extends DeviceAwareActionProcessor {
    public GroupActionProcessor(WebRtcInteractor webRtcInteractor, String str) {
        super(webRtcInteractor, str);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleReceivedOffer(WebRtcServiceState webRtcServiceState, WebRtcData.CallMetadata callMetadata, WebRtcData.OfferMetadata offerMetadata, WebRtcData.ReceivedOfferMetadata receivedOfferMetadata) {
        String str = this.tag;
        Log.i(str, "handleReceivedOffer(): id: " + callMetadata.getCallId().format(Integer.valueOf(callMetadata.getRemoteDevice())));
        Log.i(this.tag, "In a group call, send busy back to 1:1 call offer.");
        boolean z = true;
        webRtcServiceState.getActionProcessor().handleSendBusy(webRtcServiceState, callMetadata, true);
        WebRtcInteractor webRtcInteractor = this.webRtcInteractor;
        RemotePeer remotePeer = callMetadata.getRemotePeer();
        long serverReceivedTimestamp = receivedOfferMetadata.getServerReceivedTimestamp();
        if (offerMetadata.getOfferType() != OfferMessage.Type.VIDEO_CALL) {
            z = false;
        }
        webRtcInteractor.insertMissedCall(remotePeer, serverReceivedTimestamp, z);
        return webRtcServiceState;
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleGroupRemoteDeviceStateChanged(WebRtcServiceState webRtcServiceState) {
        BroadcastVideoSink broadcastVideoSink;
        CallParticipant.DeviceOrdinal deviceOrdinal;
        Log.i(this.tag, "handleGroupRemoteDeviceStateChanged():");
        GroupCall requireGroupCall = webRtcServiceState.getCallInfoState().requireGroupCall();
        Map<CallParticipantId, CallParticipant> remoteCallParticipantsMap = webRtcServiceState.getCallInfoState().getRemoteCallParticipantsMap();
        LongSparseArray<GroupCall.RemoteDeviceState> remoteDeviceStates = requireGroupCall.getRemoteDeviceStates();
        if (remoteDeviceStates == null) {
            Log.w(this.tag, "Unable to update remote devices with null list.");
            return webRtcServiceState;
        }
        WebRtcServiceStateBuilder.CallInfoStateBuilder clearParticipantMap = webRtcServiceState.builder().changeCallInfoState().clearParticipantMap();
        ArrayList<GroupCall.RemoteDeviceState> arrayList = new ArrayList(remoteDeviceStates.size());
        for (int i = 0; i < remoteDeviceStates.size(); i++) {
            arrayList.add(remoteDeviceStates.get(remoteDeviceStates.keyAt(i)));
        }
        Collections.sort(arrayList, new Comparator() { // from class: org.thoughtcrime.securesms.service.webrtc.GroupActionProcessor$$ExternalSyntheticLambda0
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return GroupActionProcessor.m2694$r8$lambda$HWl74a9Jbu_hpwUUmRrLbiIy6o((GroupCall.RemoteDeviceState) obj, (GroupCall.RemoteDeviceState) obj2);
            }
        });
        HashSet hashSet = new HashSet();
        hashSet.add(Recipient.self());
        for (GroupCall.RemoteDeviceState remoteDeviceState : arrayList) {
            Recipient externalPush = Recipient.externalPush(ServiceId.from(remoteDeviceState.getUserId()));
            CallParticipantId callParticipantId = new CallParticipantId(remoteDeviceState.getDemuxId(), externalPush.getId());
            CallParticipant callParticipant = remoteCallParticipantsMap.get(callParticipantId);
            VideoTrack videoTrack = remoteDeviceState.getVideoTrack();
            if (videoTrack != null) {
                if (callParticipant == null || callParticipant.getVideoSink().getLockableEglBase().getEglBase() == null) {
                    broadcastVideoSink = new BroadcastVideoSink(webRtcServiceState.getVideoState().getLockableEglBase(), true, true, webRtcServiceState.getLocalDeviceState().getOrientation().getDegrees());
                } else {
                    broadcastVideoSink = callParticipant.getVideoSink();
                }
                videoTrack.addSink(broadcastVideoSink);
            } else {
                broadcastVideoSink = new BroadcastVideoSink();
            }
            boolean z = remoteDeviceState.getForwardingVideo() == null || remoteDeviceState.getForwardingVideo().booleanValue();
            Boolean bool = Boolean.FALSE;
            boolean equals = bool.equals(remoteDeviceState.getAudioMuted());
            boolean equals2 = bool.equals(remoteDeviceState.getVideoMuted());
            long speakerTime = remoteDeviceState.getSpeakerTime();
            boolean mediaKeysReceived = remoteDeviceState.getMediaKeysReceived();
            long addedTime = remoteDeviceState.getAddedTime();
            boolean equals3 = Boolean.TRUE.equals(remoteDeviceState.getPresenting());
            if (hashSet.contains(externalPush)) {
                deviceOrdinal = CallParticipant.DeviceOrdinal.SECONDARY;
            } else {
                deviceOrdinal = CallParticipant.DeviceOrdinal.PRIMARY;
            }
            clearParticipantMap.putParticipant(callParticipantId, CallParticipant.createRemote(callParticipantId, externalPush, null, broadcastVideoSink, z, equals, equals2, speakerTime, mediaKeysReceived, addedTime, equals3, deviceOrdinal));
            hashSet.add(externalPush);
        }
        clearParticipantMap.remoteDevicesCount((long) remoteDeviceStates.size());
        return clearParticipantMap.build();
    }

    public static /* synthetic */ int lambda$handleGroupRemoteDeviceStateChanged$0(GroupCall.RemoteDeviceState remoteDeviceState, GroupCall.RemoteDeviceState remoteDeviceState2) {
        return Long.compare(remoteDeviceState.getAddedTime(), remoteDeviceState2.getAddedTime());
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleGroupRequestMembershipProof(WebRtcServiceState webRtcServiceState, int i, byte[] bArr) {
        Log.i(this.tag, "handleGroupRequestMembershipProof():");
        GroupCall groupCall = webRtcServiceState.getCallInfoState().getGroupCall();
        if (groupCall == null || groupCall.hashCode() != i) {
            return webRtcServiceState;
        }
        try {
            groupCall.setMembershipProof(bArr);
            return webRtcServiceState;
        } catch (CallException e) {
            return groupCallFailure(webRtcServiceState, "Unable to set group membership proof", e);
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleGroupRequestUpdateMembers(WebRtcServiceState webRtcServiceState) {
        Log.i(this.tag, "handleGroupRequestUpdateMembers():");
        Recipient callRecipient = webRtcServiceState.getCallInfoState().getCallRecipient();
        try {
            webRtcServiceState.getCallInfoState().requireGroupCall().setGroupMembers(new ArrayList(Stream.of(GroupManager.getUuidCipherTexts(this.context, callRecipient.requireGroupId().requireV2())).map(new Function() { // from class: org.thoughtcrime.securesms.service.webrtc.GroupActionProcessor$$ExternalSyntheticLambda1
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return GroupActionProcessor.$r8$lambda$RygCYahCqm8qLSfIHV0nWA2j8GM((Map.Entry) obj);
                }
            }).toList()));
            return webRtcServiceState;
        } catch (CallException e) {
            return groupCallFailure(webRtcServiceState, "Unable set group members", e);
        }
    }

    public static /* synthetic */ GroupCall.GroupMemberInfo lambda$handleGroupRequestUpdateMembers$1(Map.Entry entry) {
        return new GroupCall.GroupMemberInfo((UUID) entry.getKey(), ((UuidCiphertext) entry.getValue()).serialize());
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleUpdateRenderedResolutions(WebRtcServiceState webRtcServiceState) {
        Map<CallParticipantId, CallParticipant> remoteCallParticipantsMap = webRtcServiceState.getCallInfoState().getRemoteCallParticipantsMap();
        ArrayList arrayList = new ArrayList(remoteCallParticipantsMap.size());
        for (Map.Entry<CallParticipantId, CallParticipant> entry : remoteCallParticipantsMap.entrySet()) {
            BroadcastVideoSink videoSink = entry.getValue().getVideoSink();
            BroadcastVideoSink.RequestedSize maxRequestingSize = videoSink.getMaxRequestingSize();
            arrayList.add(new GroupCall.VideoRequest(entry.getKey().getDemuxId(), maxRequestingSize.getWidth(), maxRequestingSize.getHeight(), null));
            videoSink.setCurrentlyRequestedMaxSize(maxRequestingSize);
        }
        try {
            webRtcServiceState.getCallInfoState().requireGroupCall().requestVideo(arrayList);
            return webRtcServiceState;
        } catch (CallException e) {
            return groupCallFailure(webRtcServiceState, "Unable to set rendered resolutions", e);
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleSetRingGroup(WebRtcServiceState webRtcServiceState, boolean z) {
        String str = this.tag;
        Log.i(str, "handleSetRingGroup(): ring: " + z);
        CallId callId = RemotePeer.GROUP_CALL_ID;
        if (webRtcServiceState.getCallSetupState(callId).shouldRingGroup() == z) {
            return webRtcServiceState;
        }
        return webRtcServiceState.builder().changeCallSetupState(callId).setRingGroup(z).build();
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleGroupMessageSentError(WebRtcServiceState webRtcServiceState, Collection<RecipientId> collection, WebRtcViewModel.State state) {
        String str = this.tag;
        Log.w(str, "handleGroupMessageSentError(): error: " + state);
        return state == WebRtcViewModel.State.UNTRUSTED_IDENTITY ? webRtcServiceState.builder().changeCallInfoState().addIdentityChangedRecipients(collection).build() : webRtcServiceState;
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleGroupApproveSafetyNumberChange(WebRtcServiceState webRtcServiceState, List<RecipientId> list) {
        Log.i(this.tag, "handleGroupApproveSafetyNumberChange():");
        GroupCall groupCall = webRtcServiceState.getCallInfoState().getGroupCall();
        if (groupCall == null) {
            return webRtcServiceState;
        }
        WebRtcServiceState build = webRtcServiceState.builder().changeCallInfoState().removeIdentityChangedRecipients(list).build();
        try {
            groupCall.resendMediaKeys();
            return build;
        } catch (CallException e) {
            return groupCallFailure(build, "Unable to resend media keys", e);
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleGroupLocalDeviceStateChanged(WebRtcServiceState webRtcServiceState) {
        return webRtcServiceState.builder().changeLocalDeviceState().setNetworkConnectionType(webRtcServiceState.getCallInfoState().requireGroupCall().getLocalDeviceState().getNetworkRoute().getLocalAdapterType()).commit().build();
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleGroupCallEnded(WebRtcServiceState webRtcServiceState, int i, GroupCall.GroupCallEndReason groupCallEndReason) {
        String str = this.tag;
        Log.i(str, "handleGroupCallEnded(): reason: " + groupCallEndReason);
        GroupCall groupCall = webRtcServiceState.getCallInfoState().getGroupCall();
        if (groupCall == null || groupCall.hashCode() != i) {
            return webRtcServiceState;
        }
        try {
            groupCall.disconnect();
            if (groupCallEndReason != GroupCall.GroupCallEndReason.DEVICE_EXPLICITLY_DISCONNECTED) {
                Log.i(this.tag, "Group call ended unexpectedly, reinitializing and dropping back to lobby");
                Recipient callRecipient = webRtcServiceState.getCallInfoState().getCallRecipient();
                VideoState videoState = webRtcServiceState.getVideoState();
                WebRtcServiceState initializeVanityCamera = WebRtcVideoUtil.initializeVanityCamera(WebRtcVideoUtil.reinitializeCamera(this.context, this.webRtcInteractor.getCameraEventListener(), terminateGroupCall(webRtcServiceState, false).builder().actionProcessor(new GroupNetworkUnavailableActionProcessor(this.webRtcInteractor)).changeVideoState().eglBase(videoState.getLockableEglBase()).camera(videoState.getCamera()).localSink(videoState.getLocalSink()).commit().changeCallInfoState().callState(WebRtcViewModel.State.CALL_PRE_JOIN).callRecipient(callRecipient).build()));
                return initializeVanityCamera.getActionProcessor().handlePreJoinCall(initializeVanityCamera, new RemotePeer(callRecipient.getId()));
            }
            WebRtcServiceState build = webRtcServiceState.builder().changeCallInfoState().callState(WebRtcViewModel.State.CALL_DISCONNECTED).groupCallState(WebRtcViewModel.GroupCallState.DISCONNECTED).build();
            this.webRtcInteractor.postStateUpdate(build);
            return terminateGroupCall(build);
        } catch (CallException e) {
            return groupCallFailure(webRtcServiceState, "Unable to disconnect from group call", e);
        }
    }
}
