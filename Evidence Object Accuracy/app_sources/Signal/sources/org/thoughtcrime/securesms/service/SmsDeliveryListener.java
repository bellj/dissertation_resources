package org.thoughtcrime.securesms.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobs.SmsSentJob;

/* loaded from: classes4.dex */
public class SmsDeliveryListener extends BroadcastReceiver {
    public static final String DELIVERED_SMS_ACTION;
    public static final String SENT_SMS_ACTION;
    private static final String TAG = Log.tag(SmsDeliveryListener.class);

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        int i;
        JobManager jobManager = ApplicationDependencies.getJobManager();
        long longExtra = intent.getLongExtra("message_id", -1);
        int intExtra = intent.getIntExtra("run_attempt", 0);
        boolean booleanExtra = intent.getBooleanExtra("is_multipart", true);
        String action = intent.getAction();
        action.hashCode();
        if (action.equals(SENT_SMS_ACTION)) {
            jobManager.add(new SmsSentJob(longExtra, booleanExtra, SENT_SMS_ACTION, getResultCode(), intExtra));
        } else if (!action.equals(DELIVERED_SMS_ACTION)) {
            String str = TAG;
            Log.w(str, "Unknown action: " + intent.getAction());
        } else {
            byte[] byteArrayExtra = intent.getByteArrayExtra("pdu");
            if (byteArrayExtra == null) {
                Log.w(TAG, "No PDU in delivery receipt!");
                return;
            }
            SmsMessage createFromPdu = SmsMessage.createFromPdu(byteArrayExtra);
            if (createFromPdu == null) {
                Log.w(TAG, "Delivery receipt failed to parse!");
                return;
            }
            int status = createFromPdu.getStatus();
            String str2 = TAG;
            Log.i(str2, "Original status: " + status);
            if ("3gpp2".equals(intent.getStringExtra("format"))) {
                Log.w(str2, "Correcting for CDMA delivery receipt...");
                int i2 = status >> 24;
                if (i2 <= 0) {
                    i = 0;
                } else if (i2 == 2) {
                    i = 32;
                } else if (i2 == 3) {
                    i = 64;
                }
                jobManager.add(new SmsSentJob(longExtra, booleanExtra, DELIVERED_SMS_ACTION, i, intExtra));
            }
            i = status;
            jobManager.add(new SmsSentJob(longExtra, booleanExtra, DELIVERED_SMS_ACTION, i, intExtra));
        }
    }
}
