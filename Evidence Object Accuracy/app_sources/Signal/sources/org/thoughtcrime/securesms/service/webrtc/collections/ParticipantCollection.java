package org.thoughtcrime.securesms.service.webrtc.collections;

import com.annimon.stream.ComparatorCompat;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.IndexedPredicate;
import j$.util.Collection$EL;
import j$.util.stream.Collectors;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.events.CallParticipantId;

/* loaded from: classes4.dex */
public class ParticipantCollection {
    private static final Comparator<CallParticipant> LEAST_RECENTLY_ADDED;
    private static final Comparator<CallParticipant> MOST_RECENTLY_SPOKEN;
    private static final Comparator<CallParticipant> MOST_RECENTLY_SPOKEN_THEN_LEAST_RECENTLY_ADDED;
    private final int maxGridCellCount;
    private final List<CallParticipant> participants;

    static {
        ParticipantCollection$$ExternalSyntheticLambda0 participantCollection$$ExternalSyntheticLambda0 = new Comparator() { // from class: org.thoughtcrime.securesms.service.webrtc.collections.ParticipantCollection$$ExternalSyntheticLambda0
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return ParticipantCollection.lambda$static$0((CallParticipant) obj, (CallParticipant) obj2);
            }
        };
        LEAST_RECENTLY_ADDED = participantCollection$$ExternalSyntheticLambda0;
        ParticipantCollection$$ExternalSyntheticLambda1 participantCollection$$ExternalSyntheticLambda1 = new Comparator() { // from class: org.thoughtcrime.securesms.service.webrtc.collections.ParticipantCollection$$ExternalSyntheticLambda1
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return ParticipantCollection.lambda$static$1((CallParticipant) obj, (CallParticipant) obj2);
            }
        };
        MOST_RECENTLY_SPOKEN = participantCollection$$ExternalSyntheticLambda1;
        MOST_RECENTLY_SPOKEN_THEN_LEAST_RECENTLY_ADDED = ComparatorCompat.chain(participantCollection$$ExternalSyntheticLambda1).thenComparing((Comparator) participantCollection$$ExternalSyntheticLambda0);
    }

    public static /* synthetic */ int lambda$static$0(CallParticipant callParticipant, CallParticipant callParticipant2) {
        return Long.compare(callParticipant.getAddedToCallTime(), callParticipant2.getAddedToCallTime());
    }

    public static /* synthetic */ int lambda$static$1(CallParticipant callParticipant, CallParticipant callParticipant2) {
        return Long.compare(callParticipant2.getLastSpoke(), callParticipant.getLastSpoke());
    }

    public ParticipantCollection(int i) {
        this(i, Collections.emptyList());
    }

    private ParticipantCollection(int i, List<CallParticipant> list) {
        this.maxGridCellCount = i;
        this.participants = Collections.unmodifiableList(list);
    }

    public ParticipantCollection getNext(List<CallParticipant> list) {
        if (list.isEmpty()) {
            return new ParticipantCollection(this.maxGridCellCount);
        }
        if (this.participants.isEmpty()) {
            ArrayList arrayList = new ArrayList(list);
            Collections.sort(arrayList, list.size() <= this.maxGridCellCount ? LEAST_RECENTLY_ADDED : MOST_RECENTLY_SPOKEN_THEN_LEAST_RECENTLY_ADDED);
            return new ParticipantCollection(this.maxGridCellCount, arrayList);
        }
        ArrayList arrayList2 = new ArrayList(list);
        Collections.sort(arrayList2, MOST_RECENTLY_SPOKEN_THEN_LEAST_RECENTLY_ADDED);
        List list2 = Stream.of(getGridParticipants()).map(new Function() { // from class: org.thoughtcrime.securesms.service.webrtc.collections.ParticipantCollection$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ((CallParticipant) obj).getCallParticipantId();
            }
        }).toList();
        for (int i = 0; i < list2.size(); i++) {
            int indexOf = Stream.of(arrayList2).takeUntilIndexed(new IndexedPredicate() { // from class: org.thoughtcrime.securesms.service.webrtc.collections.ParticipantCollection$$ExternalSyntheticLambda3
                @Override // com.annimon.stream.function.IndexedPredicate
                public final boolean test(int i2, Object obj) {
                    return ParticipantCollection.this.lambda$getNext$2(i2, (CallParticipant) obj);
                }
            }).map(new Function() { // from class: org.thoughtcrime.securesms.service.webrtc.collections.ParticipantCollection$$ExternalSyntheticLambda2
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return ((CallParticipant) obj).getCallParticipantId();
                }
            }).toList().indexOf((CallParticipantId) list2.get(i));
            if (!(indexOf == -1 || indexOf == i)) {
                Collections.swap(arrayList2, indexOf, Math.min(i, arrayList2.size() - 1));
            }
        }
        return new ParticipantCollection(this.maxGridCellCount, arrayList2);
    }

    public /* synthetic */ boolean lambda$getNext$2(int i, CallParticipant callParticipant) {
        return i >= this.maxGridCellCount;
    }

    public List<CallParticipant> getGridParticipants() {
        int size = this.participants.size();
        int i = this.maxGridCellCount;
        if (size > i) {
            return Collections.unmodifiableList(this.participants.subList(0, i));
        }
        return Collections.unmodifiableList(this.participants);
    }

    public List<CallParticipant> getListParticipants() {
        int size = this.participants.size();
        int i = this.maxGridCellCount;
        if (size <= i) {
            return Collections.emptyList();
        }
        List<CallParticipant> list = this.participants;
        return Collections.unmodifiableList(list.subList(i, list.size()));
    }

    public boolean isEmpty() {
        return this.participants.isEmpty();
    }

    public List<CallParticipant> getAllParticipants() {
        return this.participants;
    }

    public ParticipantCollection map(j$.util.function.Function<CallParticipant, CallParticipant> function) {
        return new ParticipantCollection(this.maxGridCellCount, (List) Collection$EL.stream(this.participants).map(function).collect(Collectors.toList()));
    }

    public int size() {
        return this.participants.size();
    }

    public CallParticipant get(int i) {
        return this.participants.get(i);
    }
}
