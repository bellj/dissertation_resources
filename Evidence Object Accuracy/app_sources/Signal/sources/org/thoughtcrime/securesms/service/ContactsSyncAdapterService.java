package org.thoughtcrime.securesms.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import org.thoughtcrime.securesms.contacts.ContactsSyncAdapter;

/* loaded from: classes4.dex */
public class ContactsSyncAdapterService extends Service {
    private static ContactsSyncAdapter syncAdapter;

    @Override // android.app.Service
    public synchronized void onCreate() {
        if (syncAdapter == null) {
            syncAdapter = new ContactsSyncAdapter(this, true);
        }
    }

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return syncAdapter.getSyncAdapterBinder();
    }
}
