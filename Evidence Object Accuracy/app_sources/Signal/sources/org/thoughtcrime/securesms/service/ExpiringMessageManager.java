package org.thoughtcrime.securesms.service;

import android.content.Context;
import java.util.Comparator;
import java.util.TreeSet;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.SmsDatabase;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.SmsMessageRecord;

/* loaded from: classes.dex */
public class ExpiringMessageManager {
    private static final String TAG = Log.tag(ExpiringMessageManager.class);
    private final Context context;
    private final Executor executor;
    private final TreeSet<ExpiringMessageReference> expiringMessageReferences = new TreeSet<>(new ExpiringMessageComparator());
    private final MessageDatabase mmsDatabase;
    private final MessageDatabase smsDatabase;

    public ExpiringMessageManager(Context context) {
        ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor();
        this.executor = newSingleThreadExecutor;
        this.context = context.getApplicationContext();
        this.smsDatabase = SignalDatabase.sms();
        this.mmsDatabase = SignalDatabase.mms();
        newSingleThreadExecutor.execute(new LoadTask());
        newSingleThreadExecutor.execute(new ProcessTask());
    }

    public void scheduleDeletion(long j, boolean z, long j2) {
        scheduleDeletion(j, z, System.currentTimeMillis(), j2);
    }

    public void scheduleDeletion(long j, boolean z, long j2, long j3) {
        long j4 = j2 + j3;
        synchronized (this.expiringMessageReferences) {
            this.expiringMessageReferences.add(new ExpiringMessageReference(j, z, j4));
            this.expiringMessageReferences.notifyAll();
        }
    }

    public void checkSchedule() {
        synchronized (this.expiringMessageReferences) {
            this.expiringMessageReferences.notifyAll();
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public class LoadTask implements Runnable {
        private LoadTask() {
            ExpiringMessageManager.this = r1;
        }

        @Override // java.lang.Runnable
        public void run() {
            SmsDatabase.Reader readerFor = SmsDatabase.readerFor(ExpiringMessageManager.this.smsDatabase.getExpirationStartedMessages());
            MmsDatabase.Reader readerFor2 = MmsDatabase.readerFor(ExpiringMessageManager.this.mmsDatabase.getExpirationStartedMessages());
            while (true) {
                SmsMessageRecord next = readerFor.getNext();
                if (next != null) {
                    ExpiringMessageManager.this.expiringMessageReferences.add(new ExpiringMessageReference(next.getId(), next.isMms(), next.getExpireStarted() + next.getExpiresIn()));
                }
            }
            while (true) {
                MessageRecord next2 = readerFor2.getNext();
                if (next2 != null) {
                    ExpiringMessageManager.this.expiringMessageReferences.add(new ExpiringMessageReference(next2.getId(), next2.isMms(), next2.getExpireStarted() + next2.getExpiresIn()));
                } else {
                    readerFor.close();
                    readerFor2.close();
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public class ProcessTask implements Runnable {
        private ProcessTask() {
            ExpiringMessageManager.this = r1;
        }

        /* JADX WARNING: Removed duplicated region for block: B:29:0x0069 A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0000 A[SYNTHETIC] */
        @Override // java.lang.Runnable
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r9 = this;
            L_0x0000:
                r0 = 0
                org.thoughtcrime.securesms.service.ExpiringMessageManager r1 = org.thoughtcrime.securesms.service.ExpiringMessageManager.this
                java.util.TreeSet r1 = org.thoughtcrime.securesms.service.ExpiringMessageManager.access$600(r1)
                monitor-enter(r1)
            L_0x0008:
                org.thoughtcrime.securesms.service.ExpiringMessageManager r2 = org.thoughtcrime.securesms.service.ExpiringMessageManager.this     // Catch: InterruptedException -> 0x005e, all -> 0x005c
                java.util.TreeSet r2 = org.thoughtcrime.securesms.service.ExpiringMessageManager.access$600(r2)     // Catch: InterruptedException -> 0x005e, all -> 0x005c
                boolean r2 = r2.isEmpty()     // Catch: InterruptedException -> 0x005e, all -> 0x005c
                if (r2 == 0) goto L_0x001e
                org.thoughtcrime.securesms.service.ExpiringMessageManager r2 = org.thoughtcrime.securesms.service.ExpiringMessageManager.this     // Catch: InterruptedException -> 0x005e, all -> 0x005c
                java.util.TreeSet r2 = org.thoughtcrime.securesms.service.ExpiringMessageManager.access$600(r2)     // Catch: InterruptedException -> 0x005e, all -> 0x005c
                r2.wait()     // Catch: InterruptedException -> 0x005e, all -> 0x005c
                goto L_0x0008
            L_0x001e:
                org.thoughtcrime.securesms.service.ExpiringMessageManager r2 = org.thoughtcrime.securesms.service.ExpiringMessageManager.this     // Catch: InterruptedException -> 0x005e, all -> 0x005c
                java.util.TreeSet r2 = org.thoughtcrime.securesms.service.ExpiringMessageManager.access$600(r2)     // Catch: InterruptedException -> 0x005e, all -> 0x005c
                java.lang.Object r2 = r2.first()     // Catch: InterruptedException -> 0x005e, all -> 0x005c
                org.thoughtcrime.securesms.service.ExpiringMessageManager$ExpiringMessageReference r2 = (org.thoughtcrime.securesms.service.ExpiringMessageManager.ExpiringMessageReference) r2     // Catch: InterruptedException -> 0x005e, all -> 0x005c
                long r3 = org.thoughtcrime.securesms.service.ExpiringMessageManager.ExpiringMessageReference.access$700(r2)     // Catch: InterruptedException -> 0x005e, all -> 0x005c
                long r5 = java.lang.System.currentTimeMillis()     // Catch: InterruptedException -> 0x005e, all -> 0x005c
                long r3 = r3 - r5
                r5 = 0
                int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r7 <= 0) goto L_0x004c
                org.thoughtcrime.securesms.service.ExpiringMessageManager r2 = org.thoughtcrime.securesms.service.ExpiringMessageManager.this     // Catch: InterruptedException -> 0x005e, all -> 0x005c
                android.content.Context r2 = org.thoughtcrime.securesms.service.ExpiringMessageManager.access$800(r2)     // Catch: InterruptedException -> 0x005e, all -> 0x005c
                org.thoughtcrime.securesms.service.ExpirationListener.setAlarm(r2, r3)     // Catch: InterruptedException -> 0x005e, all -> 0x005c
                org.thoughtcrime.securesms.service.ExpiringMessageManager r2 = org.thoughtcrime.securesms.service.ExpiringMessageManager.this     // Catch: InterruptedException -> 0x005e, all -> 0x005c
                java.util.TreeSet r2 = org.thoughtcrime.securesms.service.ExpiringMessageManager.access$600(r2)     // Catch: InterruptedException -> 0x005e, all -> 0x005c
                r2.wait(r3)     // Catch: InterruptedException -> 0x005e, all -> 0x005c
                goto L_0x0066
            L_0x004c:
                org.thoughtcrime.securesms.service.ExpiringMessageManager r0 = org.thoughtcrime.securesms.service.ExpiringMessageManager.this     // Catch: InterruptedException -> 0x0057, all -> 0x005c
                java.util.TreeSet r0 = org.thoughtcrime.securesms.service.ExpiringMessageManager.access$600(r0)     // Catch: InterruptedException -> 0x0057, all -> 0x005c
                r0.remove(r2)     // Catch: InterruptedException -> 0x0057, all -> 0x005c
                r0 = r2
                goto L_0x0066
            L_0x0057:
                r0 = move-exception
                r8 = r2
                r2 = r0
                r0 = r8
                goto L_0x005f
            L_0x005c:
                r0 = move-exception
                goto L_0x008c
            L_0x005e:
                r2 = move-exception
            L_0x005f:
                java.lang.String r3 = org.thoughtcrime.securesms.service.ExpiringMessageManager.access$900()     // Catch: all -> 0x005c
                org.signal.core.util.logging.Log.w(r3, r2)     // Catch: all -> 0x005c
            L_0x0066:
                monitor-exit(r1)     // Catch: all -> 0x005c
                if (r0 == 0) goto L_0x0000
                boolean r1 = org.thoughtcrime.securesms.service.ExpiringMessageManager.ExpiringMessageReference.access$1000(r0)
                if (r1 == 0) goto L_0x007d
                org.thoughtcrime.securesms.service.ExpiringMessageManager r1 = org.thoughtcrime.securesms.service.ExpiringMessageManager.this
                org.thoughtcrime.securesms.database.MessageDatabase r1 = org.thoughtcrime.securesms.service.ExpiringMessageManager.access$500(r1)
                long r2 = org.thoughtcrime.securesms.service.ExpiringMessageManager.ExpiringMessageReference.access$1100(r0)
                r1.deleteMessage(r2)
                goto L_0x0000
            L_0x007d:
                org.thoughtcrime.securesms.service.ExpiringMessageManager r1 = org.thoughtcrime.securesms.service.ExpiringMessageManager.this
                org.thoughtcrime.securesms.database.MessageDatabase r1 = org.thoughtcrime.securesms.service.ExpiringMessageManager.access$400(r1)
                long r2 = org.thoughtcrime.securesms.service.ExpiringMessageManager.ExpiringMessageReference.access$1100(r0)
                r1.deleteMessage(r2)
                goto L_0x0000
            L_0x008c:
                monitor-exit(r1)     // Catch: all -> 0x005c
                goto L_0x008f
            L_0x008e:
                throw r0
            L_0x008f:
                goto L_0x008e
            */
            throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.service.ExpiringMessageManager.ProcessTask.run():void");
        }
    }

    /* loaded from: classes4.dex */
    public static class ExpiringMessageReference {
        private final long expiresAtMillis;
        private final long id;
        private final boolean mms;

        private ExpiringMessageReference(long j, boolean z, long j2) {
            this.id = j;
            this.mms = z;
            this.expiresAtMillis = j2;
        }

        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof ExpiringMessageReference)) {
                return false;
            }
            ExpiringMessageReference expiringMessageReference = (ExpiringMessageReference) obj;
            if (this.id == expiringMessageReference.id && this.mms == expiringMessageReference.mms && this.expiresAtMillis == expiringMessageReference.expiresAtMillis) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return ((this.mms ? 1 : 0) ^ ((int) this.id)) ^ ((int) this.expiresAtMillis);
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public static class ExpiringMessageComparator implements Comparator<ExpiringMessageReference> {
        private ExpiringMessageComparator() {
        }

        public int compare(ExpiringMessageReference expiringMessageReference, ExpiringMessageReference expiringMessageReference2) {
            if (expiringMessageReference.expiresAtMillis < expiringMessageReference2.expiresAtMillis) {
                return -1;
            }
            if (expiringMessageReference.expiresAtMillis > expiringMessageReference2.expiresAtMillis) {
                return 1;
            }
            if (expiringMessageReference.id < expiringMessageReference2.id) {
                return -1;
            }
            if (expiringMessageReference.id > expiringMessageReference2.id) {
                return 1;
            }
            if (!expiringMessageReference.mms && expiringMessageReference2.mms) {
                return -1;
            }
            if (!expiringMessageReference.mms || expiringMessageReference2.mms) {
                return 0;
            }
            return 1;
        }
    }
}
