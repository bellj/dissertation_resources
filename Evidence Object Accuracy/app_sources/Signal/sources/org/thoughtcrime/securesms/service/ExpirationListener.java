package org.thoughtcrime.securesms.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;

/* loaded from: classes4.dex */
public class ExpirationListener extends BroadcastReceiver {
    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        ApplicationDependencies.getExpiringMessageManager().checkSchedule();
    }

    public static void setAlarm(Context context, long j) {
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, new Intent(context, ExpirationListener.class), 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        alarmManager.cancel(broadcast);
        alarmManager.set(1, System.currentTimeMillis() + j, broadcast);
    }
}
