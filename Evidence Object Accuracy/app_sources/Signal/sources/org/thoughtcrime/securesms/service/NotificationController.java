package org.thoughtcrime.securesms.service;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import java.util.concurrent.atomic.AtomicReference;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.service.GenericForegroundService;

/* loaded from: classes4.dex */
public final class NotificationController implements AutoCloseable, ServiceConnection {
    private static final String TAG = Log.tag(NotificationController.class);
    private final Context context;
    private final int id;
    private boolean indeterminate;
    private boolean isBound;
    private long percent = -1;
    private int progress;
    private int progressMax;
    private final AtomicReference<GenericForegroundService> service = new AtomicReference<>();

    public NotificationController(Context context, int i) {
        this.context = context;
        this.id = i;
        this.isBound = bindToService();
    }

    private boolean bindToService() {
        return this.context.bindService(new Intent(this.context, GenericForegroundService.class), this, 1);
    }

    public int getId() {
        return this.id;
    }

    @Override // java.lang.AutoCloseable
    public void close() {
        try {
            if (this.isBound) {
                this.context.unbindService(this);
                this.isBound = false;
            } else {
                Log.w(TAG, "Service was not bound at the time of close()...");
            }
            GenericForegroundService.stopForegroundTask(this.context, this.id);
        } catch (IllegalArgumentException e) {
            Log.w(TAG, "Failed to unbind service...", e);
        }
    }

    public void setIndeterminateProgress() {
        setProgress(0, 0, true);
    }

    public void setProgress(long j, long j2) {
        setProgress((int) j, (int) j2, false);
    }

    private synchronized void setProgress(int i, int i2, boolean z) {
        int i3;
        if (i != 0) {
            try {
                i3 = (i2 * 100) / i;
            } catch (Throwable th) {
                throw th;
            }
        } else {
            i3 = -1;
        }
        long j = (long) i3;
        boolean z2 = j == this.percent && this.indeterminate == z;
        this.percent = j;
        this.progress = i2;
        this.progressMax = i;
        this.indeterminate = z;
        if (!z2) {
            updateProgressOnService();
        }
    }

    private synchronized void updateProgressOnService() {
        GenericForegroundService genericForegroundService = this.service.get();
        if (genericForegroundService != null) {
            genericForegroundService.replaceProgress(this.id, this.progressMax, this.progress, this.indeterminate);
        }
    }

    @Override // android.content.ServiceConnection
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        String str = TAG;
        Log.i(str, "Service connected " + componentName);
        this.service.set(((GenericForegroundService.LocalBinder) iBinder).getService());
        updateProgressOnService();
    }

    @Override // android.content.ServiceConnection
    public void onServiceDisconnected(ComponentName componentName) {
        String str = TAG;
        Log.i(str, "Service disconnected " + componentName);
        this.service.set(null);
    }
}
