package org.thoughtcrime.securesms.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.MmsReceiveJob;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class MmsListener extends BroadcastReceiver {
    private static final String TAG = Log.tag(MmsListener.class);

    private boolean isRelevant(Context context, Intent intent) {
        if (ApplicationMigrationService.isDatabaseImported(context) && "android.provider.Telephony.WAP_PUSH_RECEIVED".equals(intent.getAction())) {
            Util.isDefaultSmsProvider(context);
        }
        return false;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        String str = TAG;
        Log.i(str, "Got MMS broadcast..." + intent.getAction());
        if (("android.provider.Telephony.WAP_PUSH_DELIVER".equals(intent.getAction()) && Util.isDefaultSmsProvider(context)) || ("android.provider.Telephony.WAP_PUSH_RECEIVED".equals(intent.getAction()) && isRelevant(context, intent))) {
            Log.i(str, "Relevant!");
            ApplicationDependencies.getJobManager().add(new MmsReceiveJob(intent.getByteArrayExtra("data"), intent.getExtras().getInt("subscription", -1)));
            abortBroadcast();
        }
    }
}
