package org.thoughtcrime.securesms.service.webrtc;

import android.os.ResultReceiver;
import java.util.List;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.ringrtc.CallException;
import org.signal.ringrtc.CallId;
import org.signal.ringrtc.CallManager;
import org.thoughtcrime.securesms.components.webrtc.EglBaseWrapper;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.thoughtcrime.securesms.service.webrtc.WebRtcData;
import org.thoughtcrime.securesms.service.webrtc.state.CallSetupState;
import org.thoughtcrime.securesms.service.webrtc.state.VideoState;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceState;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceStateBuilder;
import org.thoughtcrime.securesms.util.NetworkUtil;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager;
import org.webrtc.PeerConnection;
import org.whispersystems.signalservice.api.messages.calls.OfferMessage;

/* loaded from: classes4.dex */
public class OutgoingCallActionProcessor extends DeviceAwareActionProcessor {
    private static final String TAG = Log.tag(OutgoingCallActionProcessor.class);
    private final ActiveCallActionProcessorDelegate activeCallDelegate;
    private final CallSetupActionProcessorDelegate callSetupDelegate;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public OutgoingCallActionProcessor(org.thoughtcrime.securesms.service.webrtc.WebRtcInteractor r3) {
        /*
            r2 = this;
            java.lang.String r0 = org.thoughtcrime.securesms.service.webrtc.OutgoingCallActionProcessor.TAG
            r2.<init>(r3, r0)
            org.thoughtcrime.securesms.service.webrtc.ActiveCallActionProcessorDelegate r1 = new org.thoughtcrime.securesms.service.webrtc.ActiveCallActionProcessorDelegate
            r1.<init>(r3, r0)
            r2.activeCallDelegate = r1
            org.thoughtcrime.securesms.service.webrtc.CallSetupActionProcessorDelegate r1 = new org.thoughtcrime.securesms.service.webrtc.CallSetupActionProcessorDelegate
            r1.<init>(r3, r0)
            r2.callSetupDelegate = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.service.webrtc.OutgoingCallActionProcessor.<init>(org.thoughtcrime.securesms.service.webrtc.WebRtcInteractor):void");
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleIsInCallQuery(WebRtcServiceState webRtcServiceState, ResultReceiver resultReceiver) {
        return this.activeCallDelegate.handleIsInCallQuery(webRtcServiceState, resultReceiver);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleStartOutgoingCall(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer, OfferMessage.Type type) {
        String str = TAG;
        Log.i(str, "handleStartOutgoingCall():");
        WebRtcServiceStateBuilder builder = webRtcServiceState.builder();
        remotePeer.dialing();
        Log.i(str, "assign activePeer callId: " + remotePeer.getCallId() + " key: " + remotePeer.hashCode() + " type: " + type);
        boolean z = type == OfferMessage.Type.VIDEO_CALL;
        this.webRtcInteractor.setCallInProgressNotification(2, remotePeer);
        this.webRtcInteractor.setDefaultAudioDevice(remotePeer.getId(), z ? SignalAudioManager.AudioDevice.SPEAKER_PHONE : SignalAudioManager.AudioDevice.EARPIECE, false);
        this.webRtcInteractor.updatePhoneState(WebRtcUtil.getInCallPhoneState(this.context));
        this.webRtcInteractor.initializeAudioForCall();
        this.webRtcInteractor.startOutgoingRinger();
        if (!this.webRtcInteractor.addNewOutgoingCall(remotePeer.getId(), remotePeer.getCallId().longValue().longValue(), z)) {
            Log.i(str, "Unable to add new outgoing call");
            return handleDropCall(webRtcServiceState, remotePeer.getCallId().longValue().longValue());
        }
        RecipientUtil.setAndSendUniversalExpireTimerIfNecessary(this.context, Recipient.resolved(remotePeer.getId()), SignalDatabase.threads().getThreadIdIfExistsFor(remotePeer.getId()));
        SignalDatabase.sms().insertOutgoingCall(remotePeer.getId(), z);
        EglBaseWrapper.replaceHolder(EglBaseWrapper.OUTGOING_PLACEHOLDER, remotePeer.getCallId().longValue());
        this.webRtcInteractor.retrieveTurnServers(remotePeer);
        return builder.changeCallSetupState(remotePeer.getCallId()).enableVideoOnCreate(z).waitForTelecom(AndroidTelecomUtil.getTelecomSupported()).telecomApproved(false).commit().changeCallInfoState().activePeer(remotePeer).callState(WebRtcViewModel.State.CALL_OUTGOING).commit().changeLocalDeviceState().build();
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleTurnServerUpdate(WebRtcServiceState webRtcServiceState, List<PeerConnection.IceServer> list, boolean z) {
        RemotePeer requireActivePeer = webRtcServiceState.getCallInfoState().requireActivePeer();
        String str = TAG;
        Log.i(str, "handleTurnServerUpdate(): call_id: " + requireActivePeer.getCallId());
        return proceed(webRtcServiceState.builder().changeCallSetupState(requireActivePeer.getCallId()).iceServers(list).alwaysTurn(z).build());
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleSetTelecomApproved(WebRtcServiceState webRtcServiceState, long j, RecipientId recipientId) {
        return proceed(super.handleSetTelecomApproved(webRtcServiceState, j, recipientId));
    }

    private WebRtcServiceState proceed(WebRtcServiceState webRtcServiceState) {
        RemotePeer requireActivePeer = webRtcServiceState.getCallInfoState().requireActivePeer();
        CallSetupState callSetupState = webRtcServiceState.getCallSetupState(requireActivePeer);
        if (callSetupState.getIceServers().isEmpty() || (callSetupState.shouldWaitForTelecomApproval() && !callSetupState.isTelecomApproved())) {
            String str = TAG;
            Log.i(str, "Unable to proceed without ice server and telecom approval iceServers: " + Util.hasItems(callSetupState.getIceServers()) + " waitForTelecom: " + callSetupState.shouldWaitForTelecomApproval() + " telecomApproved: " + callSetupState.isTelecomApproved());
            return webRtcServiceState;
        }
        VideoState videoState = webRtcServiceState.getVideoState();
        CallParticipant remoteCallParticipant = webRtcServiceState.getCallInfoState().getRemoteCallParticipant(requireActivePeer.getRecipient());
        Objects.requireNonNull(remoteCallParticipant);
        try {
            this.webRtcInteractor.getCallManager().proceed(requireActivePeer.getCallId(), this.context, videoState.getLockableEglBase().require(), RingRtcDynamicConfiguration.getAudioProcessingMethod(), videoState.requireLocalSink(), remoteCallParticipant.getVideoSink(), videoState.requireCamera(), callSetupState.getIceServers(), callSetupState.isAlwaysTurnServers(), NetworkUtil.getCallingBandwidthMode(this.context), 200, webRtcServiceState.getCallSetupState(requireActivePeer).isEnableVideoOnCreate());
            return webRtcServiceState.builder().changeLocalDeviceState().cameraState(webRtcServiceState.getVideoState().requireCamera().getCameraState()).build();
        } catch (CallException e) {
            return callFailure(webRtcServiceState, "Unable to proceed with call: ", e);
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleDropCall(WebRtcServiceState webRtcServiceState, long j) {
        return this.callSetupDelegate.handleDropCall(webRtcServiceState, j);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleRemoteRinging(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer) {
        String str = TAG;
        Log.i(str, "handleRemoteRinging(): call_id: " + remotePeer.getCallId());
        webRtcServiceState.getCallInfoState().requireActivePeer().remoteRinging();
        return webRtcServiceState.builder().changeCallInfoState().callState(WebRtcViewModel.State.CALL_RINGING).build();
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleReceivedAnswer(WebRtcServiceState webRtcServiceState, WebRtcData.CallMetadata callMetadata, WebRtcData.AnswerMetadata answerMetadata, WebRtcData.ReceivedAnswerMetadata receivedAnswerMetadata) {
        String str = TAG;
        Log.i(str, "handleReceivedAnswer(): id: " + callMetadata.getCallId().format(Integer.valueOf(callMetadata.getRemoteDevice())));
        if (answerMetadata.getOpaque() == null) {
            return callFailure(webRtcServiceState, "receivedAnswer() failed: answerMetadata did not contain opaque", null);
        }
        try {
            this.webRtcInteractor.getCallManager().receivedAnswer(callMetadata.getCallId(), Integer.valueOf(callMetadata.getRemoteDevice()), answerMetadata.getOpaque(), WebRtcUtil.getPublicKeyBytes(receivedAnswerMetadata.getRemoteIdentityKey()), WebRtcUtil.getPublicKeyBytes(SignalStore.account().getAciIdentityKey().getPublicKey().serialize()));
            return webRtcServiceState;
        } catch (InvalidKeyException | CallException e) {
            return callFailure(webRtcServiceState, "receivedAnswer() failed: ", e);
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleReceivedBusy(WebRtcServiceState webRtcServiceState, WebRtcData.CallMetadata callMetadata) {
        String str = TAG;
        Log.i(str, "handleReceivedBusy(): id: " + callMetadata.getCallId().format(Integer.valueOf(callMetadata.getRemoteDevice())));
        try {
            this.webRtcInteractor.getCallManager().receivedBusy(callMetadata.getCallId(), Integer.valueOf(callMetadata.getRemoteDevice()));
            return webRtcServiceState;
        } catch (CallException e) {
            return callFailure(webRtcServiceState, "receivedBusy() failed: ", e);
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleSetMuteAudio(WebRtcServiceState webRtcServiceState, boolean z) {
        return webRtcServiceState.builder().changeLocalDeviceState().isMicrophoneEnabled(!z).build();
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleRemoteVideoEnable(WebRtcServiceState webRtcServiceState, boolean z) {
        return this.activeCallDelegate.handleRemoteVideoEnable(webRtcServiceState, z);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleScreenSharingEnable(WebRtcServiceState webRtcServiceState, boolean z) {
        return this.activeCallDelegate.handleScreenSharingEnable(webRtcServiceState, z);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleLocalHangup(WebRtcServiceState webRtcServiceState) {
        return this.activeCallDelegate.handleLocalHangup(webRtcServiceState);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleReceivedOfferWhileActive(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer) {
        return this.activeCallDelegate.handleReceivedOfferWhileActive(webRtcServiceState, remotePeer);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleEndedRemote(WebRtcServiceState webRtcServiceState, CallManager.CallEvent callEvent, RemotePeer remotePeer) {
        return this.activeCallDelegate.handleEndedRemote(webRtcServiceState, callEvent, remotePeer);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleEnded(WebRtcServiceState webRtcServiceState, CallManager.CallEvent callEvent, RemotePeer remotePeer) {
        return this.activeCallDelegate.handleEnded(webRtcServiceState, callEvent, remotePeer);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleSetupFailure(WebRtcServiceState webRtcServiceState, CallId callId) {
        return this.activeCallDelegate.handleSetupFailure(webRtcServiceState, callId);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleCallConnected(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer) {
        return this.callSetupDelegate.handleCallConnected(webRtcServiceState, remotePeer);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleSetEnableVideo(WebRtcServiceState webRtcServiceState, boolean z) {
        return this.callSetupDelegate.handleSetEnableVideo(webRtcServiceState, z);
    }
}
