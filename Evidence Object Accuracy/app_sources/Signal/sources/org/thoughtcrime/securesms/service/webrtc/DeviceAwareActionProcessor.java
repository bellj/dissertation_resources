package org.thoughtcrime.securesms.service.webrtc;

import java.util.Set;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.components.webrtc.BroadcastVideoSink;
import org.thoughtcrime.securesms.ringrtc.CameraState;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceState;
import org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager;

/* loaded from: classes4.dex */
public abstract class DeviceAwareActionProcessor extends WebRtcActionProcessor {
    public DeviceAwareActionProcessor(WebRtcInteractor webRtcInteractor, String str) {
        super(webRtcInteractor, str);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleAudioDeviceChanged(WebRtcServiceState webRtcServiceState, SignalAudioManager.AudioDevice audioDevice, Set<SignalAudioManager.AudioDevice> set) {
        String str = this.tag;
        Log.i(str, "handleAudioDeviceChanged(): active: " + audioDevice + " available: " + set);
        if (!webRtcServiceState.getLocalDeviceState().getCameraState().isEnabled()) {
            this.webRtcInteractor.updatePhoneState(WebRtcUtil.getInCallPhoneState(this.context));
        }
        return webRtcServiceState.builder().changeLocalDeviceState().setActiveDevice(audioDevice).setAvailableDevices(set).build();
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleSetUserAudioDevice(WebRtcServiceState webRtcServiceState, SignalAudioManager.AudioDevice audioDevice) {
        String str = this.tag;
        Log.i(str, "handleSetUserAudioDevice(): userDevice: " + audioDevice);
        RemotePeer activePeer = webRtcServiceState.getCallInfoState().getActivePeer();
        this.webRtcInteractor.setUserAudioDevice(activePeer != null ? activePeer.getId() : null, audioDevice);
        return webRtcServiceState;
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleSetCameraFlip(WebRtcServiceState webRtcServiceState) {
        Log.i(this.tag, "handleSetCameraFlip():");
        if (!webRtcServiceState.getLocalDeviceState().getCameraState().isEnabled() || webRtcServiceState.getVideoState().getCamera() == null) {
            return webRtcServiceState;
        }
        webRtcServiceState.getVideoState().getCamera().flip();
        return webRtcServiceState.builder().changeLocalDeviceState().cameraState(webRtcServiceState.getVideoState().getCamera().getCameraState()).build();
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleCameraSwitchCompleted(WebRtcServiceState webRtcServiceState, CameraState cameraState) {
        Log.i(this.tag, "handleCameraSwitchCompleted():");
        BroadcastVideoSink localSink = webRtcServiceState.getVideoState().getLocalSink();
        if (localSink != null) {
            localSink.setRotateToRightSide(cameraState.getActiveDirection() == CameraState.Direction.BACK);
        }
        return webRtcServiceState.builder().changeLocalDeviceState().cameraState(cameraState).build();
    }
}
