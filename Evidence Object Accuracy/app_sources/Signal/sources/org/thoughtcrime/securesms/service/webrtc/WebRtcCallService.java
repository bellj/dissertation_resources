package org.thoughtcrime.securesms.service.webrtc;

import android.app.Notification;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import androidx.core.content.ContextCompat;
import java.lang.Thread;
import java.util.Objects;
import java.util.Set;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.TelephonyUtil;
import org.thoughtcrime.securesms.webrtc.CallNotificationBuilder;
import org.thoughtcrime.securesms.webrtc.UncaughtExceptionHandlerManager;
import org.thoughtcrime.securesms.webrtc.audio.AudioManagerCommand;
import org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager;
import org.thoughtcrime.securesms.webrtc.locks.LockManager;

/* loaded from: classes4.dex */
public final class WebRtcCallService extends Service implements SignalAudioManager.EventListener {
    private static final String ACTION_CHANGE_POWER_BUTTON;
    private static final String ACTION_DENY_CALL;
    private static final String ACTION_LOCAL_HANGUP;
    private static final String ACTION_SEND_AUDIO_COMMAND;
    private static final String ACTION_STOP;
    private static final String ACTION_UPDATE;
    private static final String EXTRA_AUDIO_COMMAND;
    private static final String EXTRA_ENABLED;
    private static final String EXTRA_RECIPIENT_ID;
    private static final String EXTRA_UPDATE_TYPE;
    private static final int INVALID_NOTIFICATION_ID;
    private static final String TAG = Log.tag(WebRtcCallService.class);
    private SignalCallManager callManager;
    private PhoneStateListener hangUpRtcOnDeviceCallAnswered;
    private boolean isGroup = true;
    private Notification lastNotification;
    private int lastNotificationId;
    private NetworkReceiver networkReceiver;
    private PowerButtonReceiver powerButtonReceiver;
    private SignalAudioManager signalAudioManager;
    private UncaughtExceptionHandlerManager uncaughtExceptionHandlerManager;

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static void update(Context context, int i, RecipientId recipientId) {
        Intent intent = new Intent(context, WebRtcCallService.class);
        intent.setAction(ACTION_UPDATE).putExtra(EXTRA_UPDATE_TYPE, i).putExtra(EXTRA_RECIPIENT_ID, recipientId);
        ContextCompat.startForegroundService(context, intent);
    }

    public static void denyCall(Context context) {
        ContextCompat.startForegroundService(context, denyCallIntent(context));
    }

    public static void hangup(Context context) {
        ContextCompat.startForegroundService(context, hangupIntent(context));
    }

    public static void stop(Context context) {
        Intent intent = new Intent(context, WebRtcCallService.class);
        intent.setAction(ACTION_STOP);
        ContextCompat.startForegroundService(context, intent);
    }

    public static Intent denyCallIntent(Context context) {
        return new Intent(context, WebRtcCallService.class).setAction(ACTION_DENY_CALL);
    }

    public static Intent hangupIntent(Context context) {
        return new Intent(context, WebRtcCallService.class).setAction(ACTION_LOCAL_HANGUP);
    }

    public static void sendAudioManagerCommand(Context context, AudioManagerCommand audioManagerCommand) {
        Intent intent = new Intent(context, WebRtcCallService.class);
        intent.setAction(ACTION_SEND_AUDIO_COMMAND).putExtra(EXTRA_AUDIO_COMMAND, audioManagerCommand);
        ContextCompat.startForegroundService(context, intent);
    }

    public static void changePowerButtonReceiver(Context context, boolean z) {
        Intent intent = new Intent(context, WebRtcCallService.class);
        intent.setAction(ACTION_CHANGE_POWER_BUTTON).putExtra(EXTRA_ENABLED, z);
        ContextCompat.startForegroundService(context, intent);
    }

    @Override // android.app.Service
    public void onCreate() {
        Log.v(TAG, "onCreate");
        super.onCreate();
        this.callManager = ApplicationDependencies.getSignalCallManager();
        this.hangUpRtcOnDeviceCallAnswered = new HangUpRtcOnPstnCallAnsweredListener();
        this.lastNotificationId = -1;
        registerUncaughtExceptionHandler();
        registerNetworkReceiver();
        if (!AndroidTelecomUtil.getTelecomSupported()) {
            TelephonyUtil.getManager(this).listen(this.hangUpRtcOnDeviceCallAnswered, 32);
        }
    }

    @Override // android.app.Service
    public void onDestroy() {
        Log.v(TAG, "onDestroy");
        super.onDestroy();
        UncaughtExceptionHandlerManager uncaughtExceptionHandlerManager = this.uncaughtExceptionHandlerManager;
        if (uncaughtExceptionHandlerManager != null) {
            uncaughtExceptionHandlerManager.unregister();
        }
        SignalAudioManager signalAudioManager = this.signalAudioManager;
        if (signalAudioManager != null) {
            signalAudioManager.shutdown();
        }
        unregisterNetworkReceiver();
        unregisterPowerButtonReceiver();
        if (!AndroidTelecomUtil.getTelecomSupported()) {
            TelephonyUtil.getManager(this).listen(this.hangUpRtcOnDeviceCallAnswered, 0);
        }
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        if (intent == null || intent.getAction() == null) {
            return 2;
        }
        String str = TAG;
        Log.i(str, "action: " + intent.getAction());
        String action = intent.getAction();
        action.hashCode();
        char c = 65535;
        switch (action.hashCode()) {
            case -1785516855:
                if (action.equals(ACTION_UPDATE)) {
                    c = 0;
                    break;
                }
                break;
            case -959985151:
                if (action.equals(ACTION_LOCAL_HANGUP)) {
                    c = 1;
                    break;
                }
                break;
            case -416453845:
                if (action.equals(ACTION_SEND_AUDIO_COMMAND)) {
                    c = 2;
                    break;
                }
                break;
            case 2555906:
                if (action.equals(ACTION_STOP)) {
                    c = 3;
                    break;
                }
                break;
            case 490609841:
                if (action.equals(ACTION_DENY_CALL)) {
                    c = 4;
                    break;
                }
                break;
            case 1408699387:
                if (action.equals(ACTION_CHANGE_POWER_BUTTON)) {
                    c = 5;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                RecipientId recipientId = (RecipientId) intent.getParcelableExtra(EXTRA_RECIPIENT_ID);
                Objects.requireNonNull(recipientId);
                this.isGroup = Recipient.resolved(recipientId).isGroup();
                int intExtra = intent.getIntExtra(EXTRA_UPDATE_TYPE, 0);
                RecipientId recipientId2 = (RecipientId) intent.getParcelableExtra(EXTRA_RECIPIENT_ID);
                Objects.requireNonNull(recipientId2);
                setCallInProgressNotification(intExtra, recipientId2);
                return 1;
            case 1:
                setCallNotification();
                this.callManager.localHangup();
                return 2;
            case 2:
                setCallNotification();
                if (this.signalAudioManager == null) {
                    this.signalAudioManager = SignalAudioManager.create(this, this, this.isGroup);
                }
                AudioManagerCommand audioManagerCommand = (AudioManagerCommand) intent.getParcelableExtra(EXTRA_AUDIO_COMMAND);
                Objects.requireNonNull(audioManagerCommand);
                Log.i(str, "Sending audio command [" + audioManagerCommand.getClass().getSimpleName() + "] to " + this.signalAudioManager.getClass().getSimpleName());
                this.signalAudioManager.handleCommand(audioManagerCommand);
                return 1;
            case 3:
                setCallNotification();
                stop();
                return 2;
            case 4:
                setCallNotification();
                this.callManager.denyCall();
                return 2;
            case 5:
                setCallNotification();
                if (intent.getBooleanExtra(EXTRA_ENABLED, false)) {
                    registerPowerButtonReceiver();
                } else {
                    unregisterPowerButtonReceiver();
                }
                return 1;
            default:
                throw new AssertionError("Unknown action: " + intent.getAction());
        }
    }

    private void setCallNotification() {
        int i = this.lastNotificationId;
        if (i != -1) {
            startForegroundCompat(i, this.lastNotification);
            return;
        }
        Log.w(TAG, "Service running without having called start first, show temp notification and terminate service.");
        startForegroundCompat(CallNotificationBuilder.getStartingStoppingNotificationId(), CallNotificationBuilder.getStoppingNotification(this));
        stop();
    }

    public void setCallInProgressNotification(int i, RecipientId recipientId) {
        this.lastNotificationId = CallNotificationBuilder.getNotificationId(i);
        Notification callInProgressNotification = CallNotificationBuilder.getCallInProgressNotification(this, i, Recipient.resolved(recipientId));
        this.lastNotification = callInProgressNotification;
        startForegroundCompat(this.lastNotificationId, callInProgressNotification);
    }

    private void startForegroundCompat(int i, Notification notification) {
        if (Build.VERSION.SDK_INT >= 30) {
            startForeground(i, notification, 192);
        } else {
            startForeground(i, notification);
        }
    }

    private void stop() {
        stopForeground(true);
        stopSelf();
    }

    private void registerUncaughtExceptionHandler() {
        UncaughtExceptionHandlerManager uncaughtExceptionHandlerManager = new UncaughtExceptionHandlerManager();
        this.uncaughtExceptionHandlerManager = uncaughtExceptionHandlerManager;
        uncaughtExceptionHandlerManager.registerHandler(new ProximityLockRelease(this.callManager.getLockManager()));
    }

    private void registerNetworkReceiver() {
        if (this.networkReceiver == null) {
            NetworkReceiver networkReceiver = new NetworkReceiver();
            this.networkReceiver = networkReceiver;
            registerReceiver(networkReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
    }

    private void unregisterNetworkReceiver() {
        NetworkReceiver networkReceiver = this.networkReceiver;
        if (networkReceiver != null) {
            unregisterReceiver(networkReceiver);
            this.networkReceiver = null;
        }
    }

    public void registerPowerButtonReceiver() {
        if (!AndroidTelecomUtil.getTelecomSupported() && this.powerButtonReceiver == null) {
            PowerButtonReceiver powerButtonReceiver = new PowerButtonReceiver();
            this.powerButtonReceiver = powerButtonReceiver;
            registerReceiver(powerButtonReceiver, new IntentFilter("android.intent.action.SCREEN_OFF"));
        }
    }

    public void unregisterPowerButtonReceiver() {
        PowerButtonReceiver powerButtonReceiver = this.powerButtonReceiver;
        if (powerButtonReceiver != null) {
            unregisterReceiver(powerButtonReceiver);
            this.powerButtonReceiver = null;
        }
    }

    @Override // org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager.EventListener
    public void onAudioDeviceChanged(SignalAudioManager.AudioDevice audioDevice, Set<SignalAudioManager.AudioDevice> set) {
        this.callManager.onAudioDeviceChanged(audioDevice, set);
    }

    @Override // org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager.EventListener
    public void onBluetoothPermissionDenied() {
        this.callManager.onBluetoothPermissionDenied();
    }

    /* loaded from: classes4.dex */
    private class HangUpRtcOnPstnCallAnsweredListener extends PhoneStateListener {
        private HangUpRtcOnPstnCallAnsweredListener() {
            WebRtcCallService.this = r1;
        }

        @Override // android.telephony.PhoneStateListener
        public void onCallStateChanged(int i, String str) {
            super.onCallStateChanged(i, str);
            if (i == 2) {
                hangup();
                Log.i(WebRtcCallService.TAG, "Device phone call ended Signal call.");
            }
        }

        private void hangup() {
            WebRtcCallService.this.callManager.localHangup();
        }
    }

    /* loaded from: classes4.dex */
    public static class NetworkReceiver extends BroadcastReceiver {
        private NetworkReceiver() {
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            ApplicationDependencies.getSignalCallManager().networkChange(activeNetworkInfo != null && activeNetworkInfo.isConnected());
            ApplicationDependencies.getSignalCallManager().bandwidthModeUpdate();
        }
    }

    /* loaded from: classes4.dex */
    public static class PowerButtonReceiver extends BroadcastReceiver {
        private PowerButtonReceiver() {
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
                ApplicationDependencies.getSignalCallManager().screenOff();
            }
        }
    }

    /* loaded from: classes4.dex */
    public static class ProximityLockRelease implements Thread.UncaughtExceptionHandler {
        private final LockManager lockManager;

        private ProximityLockRelease(LockManager lockManager) {
            this.lockManager = lockManager;
        }

        @Override // java.lang.Thread.UncaughtExceptionHandler
        public void uncaughtException(Thread thread, Throwable th) {
            Log.i(WebRtcCallService.TAG, "Uncaught exception - releasing proximity lock", th);
            this.lockManager.updatePhoneState(LockManager.PhoneState.IDLE);
        }
    }
}
