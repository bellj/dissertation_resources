package org.thoughtcrime.securesms.service.webrtc.state;

import java.util.HashMap;
import java.util.Map;
import org.signal.ringrtc.CallId;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor;

/* loaded from: classes4.dex */
public final class WebRtcServiceState {
    WebRtcActionProcessor actionProcessor;
    CallInfoState callInfoState;
    Map<CallId, CallSetupState> callSetupStates;
    LocalDeviceState localDeviceState;
    VideoState videoState;

    public WebRtcServiceState(WebRtcActionProcessor webRtcActionProcessor) {
        this.actionProcessor = webRtcActionProcessor;
        this.callSetupStates = new HashMap();
        this.callInfoState = new CallInfoState();
        this.localDeviceState = new LocalDeviceState();
        this.videoState = new VideoState();
    }

    public WebRtcServiceState(WebRtcServiceState webRtcServiceState) {
        this.actionProcessor = webRtcServiceState.actionProcessor;
        this.callInfoState = webRtcServiceState.callInfoState.duplicate();
        this.localDeviceState = webRtcServiceState.localDeviceState.duplicate();
        this.videoState = new VideoState(webRtcServiceState.videoState);
        this.callSetupStates = new HashMap();
        for (Map.Entry<CallId, CallSetupState> entry : webRtcServiceState.callSetupStates.entrySet()) {
            this.callSetupStates.put(entry.getKey(), entry.getValue().duplicate());
        }
    }

    public WebRtcActionProcessor getActionProcessor() {
        return this.actionProcessor;
    }

    public CallSetupState getCallSetupState(RemotePeer remotePeer) {
        return getCallSetupState(remotePeer.getCallId());
    }

    public CallSetupState getCallSetupState(CallId callId) {
        if (callId == null) {
            return new CallSetupState();
        }
        if (!this.callSetupStates.containsKey(callId)) {
            this.callSetupStates.put(callId, new CallSetupState());
        }
        return this.callSetupStates.get(callId);
    }

    public CallInfoState getCallInfoState() {
        return this.callInfoState;
    }

    public LocalDeviceState getLocalDeviceState() {
        return this.localDeviceState;
    }

    public VideoState getVideoState() {
        return this.videoState;
    }

    public WebRtcServiceStateBuilder builder() {
        return new WebRtcServiceStateBuilder(this);
    }
}
