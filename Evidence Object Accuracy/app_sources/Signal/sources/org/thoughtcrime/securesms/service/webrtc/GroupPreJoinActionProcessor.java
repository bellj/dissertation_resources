package org.thoughtcrime.securesms.service.webrtc;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import java.util.List;
import java.util.UUID;
import org.signal.core.util.logging.Log;
import org.signal.ringrtc.CallException;
import org.signal.ringrtc.GroupCall;
import org.signal.ringrtc.PeekInfo;
import org.thoughtcrime.securesms.components.webrtc.BroadcastVideoSink;
import org.thoughtcrime.securesms.components.webrtc.EglBaseWrapper;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.events.CallParticipantId;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceState;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceStateBuilder;
import org.thoughtcrime.securesms.util.NetworkUtil;
import org.whispersystems.signalservice.api.messages.calls.OfferMessage;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes4.dex */
public class GroupPreJoinActionProcessor extends GroupActionProcessor {
    private static final String TAG = Log.tag(GroupPreJoinActionProcessor.class);

    public GroupPreJoinActionProcessor(WebRtcInteractor webRtcInteractor) {
        super(webRtcInteractor, TAG);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handlePreJoinCall(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer) {
        String str = TAG;
        Log.i(str, "handlePreJoinCall():");
        GroupCall createGroupCall = this.webRtcInteractor.getCallManager().createGroupCall(webRtcServiceState.getCallInfoState().getCallRecipient().requireGroupId().getDecodedId(), SignalStore.internalValues().groupCallingServer(), new byte[0], 200, RingRtcDynamicConfiguration.getAudioProcessingMethod(), this.webRtcInteractor.getGroupCallObserver());
        try {
            createGroupCall.setOutgoingAudioMuted(true);
            createGroupCall.setOutgoingVideoMuted(true);
            createGroupCall.setBandwidthMode(NetworkUtil.getCallingBandwidthMode(this.context, createGroupCall.getLocalDeviceState().getNetworkRoute().getLocalAdapterType()));
            Log.i(str, "Connecting to group call: " + webRtcServiceState.getCallInfoState().getCallRecipient().getId());
            createGroupCall.connect();
            SignalStore.tooltips().markGroupCallingLobbyEntered();
            return webRtcServiceState.builder().changeCallInfoState().groupCall(createGroupCall).groupCallState(WebRtcViewModel.GroupCallState.DISCONNECTED).activePeer(new RemotePeer(webRtcServiceState.getCallInfoState().getCallRecipient().getId(), RemotePeer.GROUP_CALL_ID)).build();
        } catch (CallException e) {
            return groupCallFailure(webRtcServiceState, "Unable to connect to group call", e);
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleCancelPreJoinCall(WebRtcServiceState webRtcServiceState) {
        Log.i(TAG, "handleCancelPreJoinCall():");
        try {
            webRtcServiceState.getCallInfoState().requireGroupCall().disconnect();
            WebRtcVideoUtil.deinitializeVideo(webRtcServiceState);
            EglBaseWrapper.releaseEglBase(RemotePeer.GROUP_CALL_ID.longValue());
            return new WebRtcServiceState(new IdleActionProcessor(this.webRtcInteractor));
        } catch (CallException e) {
            return groupCallFailure(webRtcServiceState, "Unable to disconnect from group call", e);
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.GroupActionProcessor, org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleGroupLocalDeviceStateChanged(WebRtcServiceState webRtcServiceState) {
        Log.i(this.tag, "handleGroupLocalDeviceStateChanged():");
        WebRtcServiceState handleGroupLocalDeviceStateChanged = super.handleGroupLocalDeviceStateChanged(webRtcServiceState);
        GroupCall.LocalDeviceState localDeviceState = handleGroupLocalDeviceStateChanged.getCallInfoState().requireGroupCall().getLocalDeviceState();
        String str = this.tag;
        Log.i(str, "local device changed: " + localDeviceState.getConnectionState() + " " + localDeviceState.getJoinState());
        return handleGroupLocalDeviceStateChanged.builder().changeCallInfoState().groupCallState(WebRtcUtil.groupCallStateForConnection(localDeviceState.getConnectionState())).build();
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleGroupJoinedMembershipChanged(WebRtcServiceState webRtcServiceState) {
        Log.i(this.tag, "handleGroupJoinedMembershipChanged():");
        PeekInfo peekInfo = webRtcServiceState.getCallInfoState().requireGroupCall().getPeekInfo();
        if (peekInfo == null) {
            Log.i(this.tag, "No peek info available");
            return webRtcServiceState;
        }
        List<Recipient> list = Stream.of(peekInfo.getJoinedMembers()).map(new Function() { // from class: org.thoughtcrime.securesms.service.webrtc.GroupPreJoinActionProcessor$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return GroupPreJoinActionProcessor.lambda$handleGroupJoinedMembershipChanged$0((UUID) obj);
            }
        }).toList();
        WebRtcServiceStateBuilder.CallInfoStateBuilder clearParticipantMap = webRtcServiceState.builder().changeCallInfoState().remoteDevicesCount(peekInfo.getDeviceCount()).participantLimit(peekInfo.getMaxDevices()).clearParticipantMap();
        for (Recipient recipient : list) {
            clearParticipantMap.putParticipant(recipient, CallParticipant.createRemote(new CallParticipantId(recipient), recipient, null, new BroadcastVideoSink(), true, true, true, 0, false, 0, false, CallParticipant.DeviceOrdinal.PRIMARY));
        }
        return clearParticipantMap.build();
    }

    public static /* synthetic */ Recipient lambda$handleGroupJoinedMembershipChanged$0(UUID uuid) {
        return Recipient.externalPush(ServiceId.from(uuid));
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleOutgoingCall(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer, OfferMessage.Type type) {
        Log.i(TAG, "handleOutgoingCall():");
        GroupCall requireGroupCall = webRtcServiceState.getCallInfoState().requireGroupCall();
        WebRtcServiceState reinitializeCamera = WebRtcVideoUtil.reinitializeCamera(this.context, this.webRtcInteractor.getCameraEventListener(), webRtcServiceState);
        this.webRtcInteractor.setCallInProgressNotification(2, reinitializeCamera.getCallInfoState().getCallRecipient());
        this.webRtcInteractor.updatePhoneState(WebRtcUtil.getInCallPhoneState(this.context));
        this.webRtcInteractor.initializeAudioForCall();
        try {
            requireGroupCall.setOutgoingVideoSource(reinitializeCamera.getVideoState().requireLocalSink(), reinitializeCamera.getVideoState().requireCamera());
            boolean z = true;
            requireGroupCall.setOutgoingVideoMuted(!reinitializeCamera.getLocalDeviceState().getCameraState().isEnabled());
            if (reinitializeCamera.getLocalDeviceState().isMicrophoneEnabled()) {
                z = false;
            }
            requireGroupCall.setOutgoingAudioMuted(z);
            requireGroupCall.setBandwidthMode(NetworkUtil.getCallingBandwidthMode(this.context, requireGroupCall.getLocalDeviceState().getNetworkRoute().getLocalAdapterType()));
            requireGroupCall.join();
            return reinitializeCamera.builder().actionProcessor(new GroupJoiningActionProcessor(this.webRtcInteractor)).changeCallInfoState().callState(WebRtcViewModel.State.CALL_OUTGOING).groupCallState(WebRtcViewModel.GroupCallState.CONNECTED_AND_JOINING).commit().changeLocalDeviceState().build();
        } catch (CallException e) {
            return groupCallFailure(reinitializeCamera, "Unable to join group call", e);
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleSetEnableVideo(WebRtcServiceState webRtcServiceState, boolean z) {
        String str = TAG;
        Log.i(str, "handleSetEnableVideo(): Changing for pre-join group call. enable: " + z);
        webRtcServiceState.getVideoState().requireCamera().setEnabled(z);
        return webRtcServiceState.builder().changeCallSetupState(RemotePeer.GROUP_CALL_ID).enableVideoOnCreate(z).commit().changeLocalDeviceState().cameraState(webRtcServiceState.getVideoState().requireCamera().getCameraState()).build();
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleSetMuteAudio(WebRtcServiceState webRtcServiceState, boolean z) {
        String str = TAG;
        Log.i(str, "handleSetMuteAudio(): Changing for pre-join group call. muted: " + z);
        return webRtcServiceState.builder().changeLocalDeviceState().isMicrophoneEnabled(!z).build();
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleNetworkChanged(WebRtcServiceState webRtcServiceState, boolean z) {
        return !z ? webRtcServiceState.builder().actionProcessor(new GroupNetworkUnavailableActionProcessor(this.webRtcInteractor)).changeCallInfoState().callState(WebRtcViewModel.State.NETWORK_FAILURE).build() : webRtcServiceState;
    }
}
