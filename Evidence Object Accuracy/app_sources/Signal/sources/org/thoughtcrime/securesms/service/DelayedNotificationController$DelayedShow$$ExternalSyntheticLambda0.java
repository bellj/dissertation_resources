package org.thoughtcrime.securesms.service;

import org.thoughtcrime.securesms.service.DelayedNotificationController;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class DelayedNotificationController$DelayedShow$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ DelayedNotificationController.DelayedShow f$0;

    public /* synthetic */ DelayedNotificationController$DelayedShow$$ExternalSyntheticLambda0(DelayedNotificationController.DelayedShow delayedShow) {
        this.f$0 = delayedShow;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.start();
    }
}
