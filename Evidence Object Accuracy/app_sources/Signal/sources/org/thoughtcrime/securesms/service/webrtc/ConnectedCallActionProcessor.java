package org.thoughtcrime.securesms.service.webrtc;

import android.os.ResultReceiver;
import j$.util.Collection$EL;
import j$.util.function.Function;
import java.util.Collections;
import java.util.Map;
import org.signal.core.util.logging.Log;
import org.signal.ringrtc.CallException;
import org.signal.ringrtc.CallManager;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.events.CallParticipantId;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcEphemeralState;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceState;
import org.thoughtcrime.securesms.webrtc.locks.LockManager;

/* loaded from: classes4.dex */
public class ConnectedCallActionProcessor extends DeviceAwareActionProcessor {
    private static final String TAG = Log.tag(ConnectedCallActionProcessor.class);
    private final ActiveCallActionProcessorDelegate activeCallDelegate;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ConnectedCallActionProcessor(org.thoughtcrime.securesms.service.webrtc.WebRtcInteractor r3) {
        /*
            r2 = this;
            java.lang.String r0 = org.thoughtcrime.securesms.service.webrtc.ConnectedCallActionProcessor.TAG
            r2.<init>(r3, r0)
            org.thoughtcrime.securesms.service.webrtc.ActiveCallActionProcessorDelegate r1 = new org.thoughtcrime.securesms.service.webrtc.ActiveCallActionProcessorDelegate
            r1.<init>(r3, r0)
            r2.activeCallDelegate = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.service.webrtc.ConnectedCallActionProcessor.<init>(org.thoughtcrime.securesms.service.webrtc.WebRtcInteractor):void");
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleIsInCallQuery(WebRtcServiceState webRtcServiceState, ResultReceiver resultReceiver) {
        return this.activeCallDelegate.handleIsInCallQuery(webRtcServiceState, resultReceiver);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleSetEnableVideo(WebRtcServiceState webRtcServiceState, boolean z) {
        String str = TAG;
        Log.i(str, "handleSetEnableVideo(): call_id: " + webRtcServiceState.getCallInfoState().requireActivePeer().getCallId());
        try {
            this.webRtcInteractor.getCallManager().setVideoEnable(z);
            WebRtcServiceState build = webRtcServiceState.builder().changeLocalDeviceState().cameraState(webRtcServiceState.getVideoState().requireCamera().getCameraState()).build();
            if (build.getLocalDeviceState().getCameraState().isEnabled()) {
                this.webRtcInteractor.updatePhoneState(LockManager.PhoneState.IN_VIDEO);
            } else {
                this.webRtcInteractor.updatePhoneState(WebRtcUtil.getInCallPhoneState(this.context));
            }
            WebRtcUtil.enableSpeakerPhoneIfNeeded(this.webRtcInteractor, build);
            return build;
        } catch (CallException e) {
            return callFailure(webRtcServiceState, "setVideoEnable() failed: ", e);
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleSetMuteAudio(WebRtcServiceState webRtcServiceState, boolean z) {
        WebRtcServiceState build = webRtcServiceState.builder().changeLocalDeviceState().isMicrophoneEnabled(!z).build();
        try {
            this.webRtcInteractor.getCallManager().setAudioEnable(build.getLocalDeviceState().isMicrophoneEnabled());
            return build;
        } catch (CallException e) {
            return callFailure(build, "Enabling audio failed: ", e);
        }
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcEphemeralState handleAudioLevelsChanged(WebRtcServiceState webRtcServiceState, WebRtcEphemeralState webRtcEphemeralState, int i, int i2) {
        return webRtcEphemeralState.copy(CallParticipant.AudioLevel.fromRawAudioLevel(i), (Map) Collection$EL.stream(webRtcServiceState.getCallInfoState().getRemoteCallParticipantsMap().keySet()).findFirst().map(new Function(i2) { // from class: org.thoughtcrime.securesms.service.webrtc.ConnectedCallActionProcessor$$ExternalSyntheticLambda0
            public final /* synthetic */ int f$0;

            {
                this.f$0 = r1;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ConnectedCallActionProcessor.lambda$handleAudioLevelsChanged$0(this.f$0, (CallParticipantId) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(Collections.emptyMap()));
    }

    public static /* synthetic */ Map lambda$handleAudioLevelsChanged$0(int i, CallParticipantId callParticipantId) {
        return Collections.singletonMap(callParticipantId, CallParticipant.AudioLevel.fromRawAudioLevel(i));
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleCallReconnect(WebRtcServiceState webRtcServiceState, CallManager.CallEvent callEvent) {
        String str = TAG;
        Log.i(str, "handleCallReconnect(): event: " + callEvent);
        return webRtcServiceState.builder().changeCallInfoState().callState(callEvent == CallManager.CallEvent.RECONNECTING ? WebRtcViewModel.State.CALL_RECONNECTING : WebRtcViewModel.State.CALL_CONNECTED).build();
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleRemoteVideoEnable(WebRtcServiceState webRtcServiceState, boolean z) {
        return this.activeCallDelegate.handleRemoteVideoEnable(webRtcServiceState, z);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleScreenSharingEnable(WebRtcServiceState webRtcServiceState, boolean z) {
        return this.activeCallDelegate.handleScreenSharingEnable(webRtcServiceState, z);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleLocalHangup(WebRtcServiceState webRtcServiceState) {
        return this.activeCallDelegate.handleLocalHangup(webRtcServiceState);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleEndedRemote(WebRtcServiceState webRtcServiceState, CallManager.CallEvent callEvent, RemotePeer remotePeer) {
        return this.activeCallDelegate.handleEndedRemote(webRtcServiceState, callEvent, remotePeer);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleEnded(WebRtcServiceState webRtcServiceState, CallManager.CallEvent callEvent, RemotePeer remotePeer) {
        return this.activeCallDelegate.handleEnded(webRtcServiceState, callEvent, remotePeer);
    }

    @Override // org.thoughtcrime.securesms.service.webrtc.WebRtcActionProcessor
    public WebRtcServiceState handleReceivedOfferWhileActive(WebRtcServiceState webRtcServiceState, RemotePeer remotePeer) {
        return this.activeCallDelegate.handleReceivedOfferWhileActive(webRtcServiceState, remotePeer);
    }
}
