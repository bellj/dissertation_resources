package org.thoughtcrime.securesms.service.webrtc.state;

import java.util.Objects;
import org.thoughtcrime.securesms.components.webrtc.BroadcastVideoSink;
import org.thoughtcrime.securesms.components.webrtc.EglBaseWrapper;
import org.thoughtcrime.securesms.ringrtc.Camera;

/* loaded from: classes4.dex */
public final class VideoState {
    Camera camera;
    EglBaseWrapper eglBase;
    BroadcastVideoSink localSink;

    public VideoState() {
        this(null, null, null);
    }

    public VideoState(VideoState videoState) {
        this(videoState.eglBase, videoState.localSink, videoState.camera);
    }

    VideoState(EglBaseWrapper eglBaseWrapper, BroadcastVideoSink broadcastVideoSink, Camera camera) {
        this.eglBase = eglBaseWrapper;
        this.localSink = broadcastVideoSink;
        this.camera = camera;
    }

    public EglBaseWrapper getLockableEglBase() {
        return this.eglBase;
    }

    public BroadcastVideoSink getLocalSink() {
        return this.localSink;
    }

    public BroadcastVideoSink requireLocalSink() {
        BroadcastVideoSink broadcastVideoSink = this.localSink;
        Objects.requireNonNull(broadcastVideoSink);
        return broadcastVideoSink;
    }

    public Camera getCamera() {
        return this.camera;
    }

    public Camera requireCamera() {
        Camera camera = this.camera;
        Objects.requireNonNull(camera);
        return camera;
    }
}
