package org.thoughtcrime.securesms.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.SmsReceiveJob;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class SmsListener extends BroadcastReceiver {
    private static final String SMS_DELIVERED_ACTION;
    private static final String SMS_RECEIVED_ACTION;
    private static final String TAG = Log.tag(SmsListener.class);

    private boolean isExemption(SmsMessage smsMessage, String str) {
        if (smsMessage.getMessageClass() == SmsMessage.MessageClass.CLASS_0 || str.startsWith("Sparebank1://otp?")) {
            return true;
        }
        if (smsMessage.getOriginatingAddress().length() >= 7 || (!str.toUpperCase().startsWith("//ANDROID:") && !str.startsWith("//BREW:"))) {
            return false;
        }
        return true;
    }

    private SmsMessage getSmsMessageFromIntent(Intent intent) {
        Object[] objArr = (Object[]) intent.getExtras().get("pdus");
        if (objArr == null || objArr.length == 0) {
            return null;
        }
        return SmsMessage.createFromPdu((byte[]) objArr[0]);
    }

    private String getSmsMessageBodyFromIntent(Intent intent) {
        Object[] objArr = (Object[]) intent.getExtras().get("pdus");
        StringBuilder sb = new StringBuilder();
        if (objArr == null) {
            return null;
        }
        for (Object obj : objArr) {
            sb.append(SmsMessage.createFromPdu((byte[]) obj).getDisplayMessageBody());
        }
        return sb.toString();
    }

    private boolean isRelevant(Context context, Intent intent) {
        SmsMessage smsMessageFromIntent = getSmsMessageFromIntent(intent);
        String smsMessageBodyFromIntent = getSmsMessageBodyFromIntent(intent);
        if (!(smsMessageFromIntent == null && smsMessageBodyFromIntent == null) && !isExemption(smsMessageFromIntent, smsMessageBodyFromIntent) && ApplicationMigrationService.isDatabaseImported(context) && SMS_RECEIVED_ACTION.equals(intent.getAction())) {
            Util.isDefaultSmsProvider(context);
        }
        return false;
    }

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        String str = TAG;
        Log.i(str, "Got SMS broadcast...");
        if (intent.getAction().equals(SMS_DELIVERED_ACTION) || (intent.getAction().equals(SMS_RECEIVED_ACTION) && isRelevant(context, intent))) {
            Log.i(str, "Constructing SmsReceiveJob...");
            ApplicationDependencies.getJobManager().add(new SmsReceiveJob((Object[]) intent.getExtras().get("pdus"), intent.getExtras().getInt("subscription", -1)));
            abortBroadcast();
        }
    }
}
