package org.thoughtcrime.securesms.service.webrtc;

import android.content.Context;
import android.net.Uri;
import java.util.Collection;
import java.util.UUID;
import org.signal.ringrtc.CallManager;
import org.signal.ringrtc.GroupCall;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.ringrtc.CameraEventListener;
import org.thoughtcrime.securesms.ringrtc.RemotePeer;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcServiceState;
import org.thoughtcrime.securesms.util.AppForegroundObserver;
import org.thoughtcrime.securesms.webrtc.audio.AudioManagerCommand;
import org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager;
import org.thoughtcrime.securesms.webrtc.locks.LockManager;
import org.whispersystems.signalservice.api.messages.calls.SignalServiceCallMessage;

/* loaded from: classes4.dex */
public class WebRtcInteractor {
    private final CameraEventListener cameraEventListener;
    private final Context context;
    private final AppForegroundObserver.Listener foregroundListener;
    private final GroupCall.Observer groupCallObserver;
    private final LockManager lockManager;
    private final SignalCallManager signalCallManager;

    public WebRtcInteractor(Context context, SignalCallManager signalCallManager, LockManager lockManager, CameraEventListener cameraEventListener, GroupCall.Observer observer, AppForegroundObserver.Listener listener) {
        this.context = context;
        this.signalCallManager = signalCallManager;
        this.lockManager = lockManager;
        this.cameraEventListener = cameraEventListener;
        this.groupCallObserver = observer;
        this.foregroundListener = listener;
    }

    public Context getContext() {
        return this.context;
    }

    public CameraEventListener getCameraEventListener() {
        return this.cameraEventListener;
    }

    public CallManager getCallManager() {
        return this.signalCallManager.getRingRtcCallManager();
    }

    public GroupCall.Observer getGroupCallObserver() {
        return this.groupCallObserver;
    }

    public AppForegroundObserver.Listener getForegroundListener() {
        return this.foregroundListener;
    }

    public void updatePhoneState(LockManager.PhoneState phoneState) {
        this.lockManager.updatePhoneState(phoneState);
    }

    public void postStateUpdate(WebRtcServiceState webRtcServiceState) {
        this.signalCallManager.postStateUpdate(webRtcServiceState);
    }

    public void sendCallMessage(RemotePeer remotePeer, SignalServiceCallMessage signalServiceCallMessage) {
        this.signalCallManager.sendCallMessage(remotePeer, signalServiceCallMessage);
    }

    public void sendGroupCallMessage(Recipient recipient, String str) {
        this.signalCallManager.sendGroupCallUpdateMessage(recipient, str);
    }

    public void updateGroupCallUpdateMessage(RecipientId recipientId, String str, Collection<UUID> collection, boolean z) {
        this.signalCallManager.updateGroupCallUpdateMessage(recipientId, str, collection, z);
    }

    public void setCallInProgressNotification(int i, RemotePeer remotePeer) {
        WebRtcCallService.update(this.context, i, remotePeer.getRecipient().getId());
    }

    public void setCallInProgressNotification(int i, Recipient recipient) {
        WebRtcCallService.update(this.context, i, recipient.getId());
    }

    public void retrieveTurnServers(RemotePeer remotePeer) {
        this.signalCallManager.retrieveTurnServers(remotePeer);
    }

    public void stopForegroundService() {
        WebRtcCallService.stop(this.context);
    }

    public void insertMissedCall(RemotePeer remotePeer, long j, boolean z) {
        this.signalCallManager.insertMissedCall(remotePeer, true, j, z);
    }

    public void insertReceivedCall(RemotePeer remotePeer, boolean z) {
        this.signalCallManager.insertReceivedCall(remotePeer, true, z);
    }

    public boolean startWebRtcCallActivityIfPossible() {
        return this.signalCallManager.startCallCardActivityIfPossible();
    }

    public void registerPowerButtonReceiver() {
        WebRtcCallService.changePowerButtonReceiver(this.context, true);
    }

    public void unregisterPowerButtonReceiver() {
        WebRtcCallService.changePowerButtonReceiver(this.context, false);
    }

    public void silenceIncomingRinger() {
        WebRtcCallService.sendAudioManagerCommand(this.context, new AudioManagerCommand.SilenceIncomingRinger());
    }

    public void initializeAudioForCall() {
        WebRtcCallService.sendAudioManagerCommand(this.context, new AudioManagerCommand.Initialize());
    }

    public void startIncomingRinger(Uri uri, boolean z) {
        WebRtcCallService.sendAudioManagerCommand(this.context, new AudioManagerCommand.StartIncomingRinger(uri, z));
    }

    public void startOutgoingRinger() {
        WebRtcCallService.sendAudioManagerCommand(this.context, new AudioManagerCommand.StartOutgoingRinger());
    }

    public void stopAudio(boolean z) {
        WebRtcCallService.sendAudioManagerCommand(this.context, new AudioManagerCommand.Stop(z));
    }

    public void startAudioCommunication() {
        WebRtcCallService.sendAudioManagerCommand(this.context, new AudioManagerCommand.Start());
    }

    public void setUserAudioDevice(RecipientId recipientId, SignalAudioManager.AudioDevice audioDevice) {
        WebRtcCallService.sendAudioManagerCommand(this.context, new AudioManagerCommand.SetUserDevice(recipientId, audioDevice));
    }

    public void setDefaultAudioDevice(RecipientId recipientId, SignalAudioManager.AudioDevice audioDevice, boolean z) {
        WebRtcCallService.sendAudioManagerCommand(this.context, new AudioManagerCommand.SetDefaultDevice(recipientId, audioDevice, z));
    }

    public void peekGroupCallForRingingCheck(GroupCallRingCheckInfo groupCallRingCheckInfo) {
        this.signalCallManager.peekGroupCallForRingingCheck(groupCallRingCheckInfo);
    }

    public void activateCall(RecipientId recipientId) {
        AndroidTelecomUtil.activateCall(recipientId);
    }

    public void terminateCall(RecipientId recipientId) {
        AndroidTelecomUtil.terminateCall(recipientId);
    }

    public boolean addNewIncomingCall(RecipientId recipientId, long j, boolean z) {
        return AndroidTelecomUtil.addIncomingCall(recipientId, j, z);
    }

    public void rejectIncomingCall(RecipientId recipientId) {
        AndroidTelecomUtil.reject(recipientId);
    }

    public boolean addNewOutgoingCall(RecipientId recipientId, long j, boolean z) {
        return AndroidTelecomUtil.addOutgoingCall(recipientId, j, z);
    }
}
