package org.thoughtcrime.securesms;

import j$.util.function.Consumer;
import org.thoughtcrime.securesms.ContactSelectionListFragment;
import org.thoughtcrime.securesms.contacts.SelectedContact;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class ContactSelectionListFragment$ListClickListener$$ExternalSyntheticLambda4 implements Consumer {
    public final /* synthetic */ ContactSelectionListFragment.ListClickListener f$0;
    public final /* synthetic */ SelectedContact f$1;

    public /* synthetic */ ContactSelectionListFragment$ListClickListener$$ExternalSyntheticLambda4(ContactSelectionListFragment.ListClickListener listClickListener, SelectedContact selectedContact) {
        this.f$0 = listClickListener;
        this.f$1 = selectedContact;
    }

    @Override // j$.util.function.Consumer
    public final void accept(Object obj) {
        this.f$0.lambda$onItemClick$4(this.f$1, (Boolean) obj);
    }

    @Override // j$.util.function.Consumer
    public /* synthetic */ Consumer andThen(Consumer consumer) {
        return Consumer.CC.$default$andThen(this, consumer);
    }
}
