package org.thoughtcrime.securesms.permissions;

import com.annimon.stream.function.Consumer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* loaded from: classes4.dex */
public class PermissionsRequest {
    private final Map<String, Boolean> PRE_REQUEST_MAPPING = new HashMap();
    private final Runnable allGrantedListener;
    private final Runnable anyDeniedListener;
    private final Runnable anyPermanentlyDeniedListener;
    private final Runnable anyResultListener;
    private final Consumer<List<String>> someDeniedListener;
    private final Consumer<List<String>> someGrantedListener;
    private final Consumer<List<String>> somePermanentlyDeniedListener;

    public PermissionsRequest(Runnable runnable, Runnable runnable2, Runnable runnable3, Runnable runnable4, Consumer<List<String>> consumer, Consumer<List<String>> consumer2, Consumer<List<String>> consumer3) {
        this.allGrantedListener = runnable;
        this.anyDeniedListener = runnable2;
        this.anyPermanentlyDeniedListener = runnable3;
        this.anyResultListener = runnable4;
        this.someGrantedListener = consumer;
        this.someDeniedListener = consumer2;
        this.somePermanentlyDeniedListener = consumer3;
    }

    public void onResult(String[] strArr, int[] iArr, boolean[] zArr) {
        ArrayList arrayList = new ArrayList(strArr.length);
        ArrayList arrayList2 = new ArrayList(strArr.length);
        ArrayList arrayList3 = new ArrayList(strArr.length);
        for (int i = 0; i < strArr.length; i++) {
            if (iArr[i] == 0) {
                arrayList.add(strArr[i]);
            } else {
                boolean booleanValue = this.PRE_REQUEST_MAPPING.get(strArr[i]).booleanValue();
                if (!(this.somePermanentlyDeniedListener == null && this.anyPermanentlyDeniedListener == null) && !booleanValue && !zArr[i]) {
                    arrayList3.add(strArr[i]);
                } else {
                    arrayList2.add(strArr[i]);
                }
            }
        }
        if (this.allGrantedListener != null && arrayList.size() > 0 && arrayList2.size() == 0 && arrayList3.size() == 0) {
            this.allGrantedListener.run();
        } else if (this.someGrantedListener != null && arrayList.size() > 0) {
            this.someGrantedListener.accept(arrayList);
        }
        if (arrayList2.size() > 0) {
            Runnable runnable = this.anyDeniedListener;
            if (runnable != null) {
                runnable.run();
            }
            Consumer<List<String>> consumer = this.someDeniedListener;
            if (consumer != null) {
                consumer.accept(arrayList2);
            }
        }
        if (arrayList3.size() > 0) {
            Runnable runnable2 = this.anyPermanentlyDeniedListener;
            if (runnable2 != null) {
                runnable2.run();
            }
            Consumer<List<String>> consumer2 = this.somePermanentlyDeniedListener;
            if (consumer2 != null) {
                consumer2.accept(arrayList3);
            }
        }
        Runnable runnable3 = this.anyResultListener;
        if (runnable3 != null) {
            runnable3.run();
        }
    }

    public void addMapping(String str, boolean z) {
        this.PRE_REQUEST_MAPPING.put(str, Boolean.valueOf(z));
    }
}
