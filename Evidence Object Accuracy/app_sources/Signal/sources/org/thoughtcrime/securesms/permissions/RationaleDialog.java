package org.thoughtcrime.securesms.permissions;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.ThemeUtil;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public class RationaleDialog {
    public static MaterialAlertDialogBuilder createFor(Context context, String str, int... iArr) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.permissions_rationale_dialog, (ViewGroup) null);
        ViewGroup viewGroup = (ViewGroup) inflate.findViewById(R.id.header_container);
        TextView textView = (TextView) inflate.findViewById(R.id.message);
        for (int i = 0; i < iArr.length; i++) {
            Drawable drawable = ContextCompat.getDrawable(context, iArr[i]);
            DrawableCompat.setTint(drawable, ContextCompat.getColor(context, R.color.white));
            ImageView imageView = new ImageView(context);
            imageView.setImageDrawable(drawable);
            imageView.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
            viewGroup.addView(imageView);
            if (i != iArr.length - 1) {
                TextView textView2 = new TextView(context);
                textView2.setText("+");
                textView2.setTextSize(2, 40.0f);
                textView2.setTextColor(-1);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
                layoutParams.setMargins(ViewUtil.dpToPx(context, 20), 0, ViewUtil.dpToPx(context, 20), 0);
                textView2.setLayoutParams(layoutParams);
                viewGroup.addView(textView2);
            }
        }
        textView.setText(str);
        return new MaterialAlertDialogBuilder(context, ThemeUtil.isDarkTheme(context) ? R.style.Theme_Signal_AlertDialog_Dark_Cornered : R.style.Theme_Signal_AlertDialog_Light_Cornered).setView(inflate);
    }
}
