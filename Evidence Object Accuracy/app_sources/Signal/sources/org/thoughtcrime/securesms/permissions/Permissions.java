package org.thoughtcrime.securesms.permissions;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Window;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.annimon.stream.function.Predicate;
import java.lang.ref.WeakReference;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.LRUCache;
import org.thoughtcrime.securesms.util.ServiceUtil;

/* loaded from: classes4.dex */
public class Permissions {
    private static final Map<Integer, PermissionsRequest> OUTSTANDING = new LRUCache(2);
    private static final String TAG = Log.tag(Permissions.class);

    public static PermissionsBuilder with(Activity activity) {
        return new PermissionsBuilder(new ActivityPermissionObject(activity));
    }

    public static PermissionsBuilder with(Fragment fragment) {
        return new PermissionsBuilder(new FragmentPermissionObject(fragment));
    }

    /* loaded from: classes4.dex */
    public static class PermissionsBuilder {
        private Runnable allGrantedListener;
        private Runnable anyDeniedListener;
        private Runnable anyPermanentlyDeniedListener;
        private Runnable anyResultListener;
        private boolean condition = true;
        private boolean ifNecesary;
        private final PermissionObject permissionObject;
        private int[] rationalDialogHeader;
        private boolean rationaleDialogCancelable;
        private String rationaleDialogMessage;
        private String[] requestedPermissions;
        private Consumer<List<String>> someDeniedListener;
        private Consumer<List<String>> someGrantedListener;
        private Consumer<List<String>> somePermanentlyDeniedListener;

        public static /* synthetic */ int lambda$executeNoPermissionsRequest$2(String str) {
            return -1;
        }

        PermissionsBuilder(PermissionObject permissionObject) {
            this.permissionObject = permissionObject;
        }

        public PermissionsBuilder request(String... strArr) {
            this.requestedPermissions = strArr;
            return this;
        }

        public PermissionsBuilder ifNecessary() {
            this.ifNecesary = true;
            return this;
        }

        public PermissionsBuilder ifNecessary(boolean z) {
            this.ifNecesary = true;
            this.condition = z;
            return this;
        }

        public PermissionsBuilder withRationaleDialog(String str, int... iArr) {
            return withRationaleDialog(str, true, iArr);
        }

        public PermissionsBuilder withRationaleDialog(String str, boolean z, int... iArr) {
            this.rationalDialogHeader = iArr;
            this.rationaleDialogMessage = str;
            this.rationaleDialogCancelable = z;
            return this;
        }

        public PermissionsBuilder withPermanentDenialDialog(String str) {
            return onAnyPermanentlyDenied(new SettingsDialogListener(this.permissionObject.getContext(), str));
        }

        public PermissionsBuilder onAllGranted(Runnable runnable) {
            this.allGrantedListener = runnable;
            return this;
        }

        public PermissionsBuilder onAnyDenied(Runnable runnable) {
            this.anyDeniedListener = runnable;
            return this;
        }

        public PermissionsBuilder onAnyPermanentlyDenied(Runnable runnable) {
            this.anyPermanentlyDeniedListener = runnable;
            return this;
        }

        public PermissionsBuilder onAnyResult(Runnable runnable) {
            this.anyResultListener = runnable;
            return this;
        }

        public PermissionsBuilder onSomeGranted(Consumer<List<String>> consumer) {
            this.someGrantedListener = consumer;
            return this;
        }

        public PermissionsBuilder onSomeDenied(Consumer<List<String>> consumer) {
            this.someDeniedListener = consumer;
            return this;
        }

        public PermissionsBuilder onSomePermanentlyDenied(Consumer<List<String>> consumer) {
            this.somePermanentlyDeniedListener = consumer;
            return this;
        }

        public void execute() {
            PermissionsRequest permissionsRequest = new PermissionsRequest(this.allGrantedListener, this.anyDeniedListener, this.anyPermanentlyDeniedListener, this.anyResultListener, this.someGrantedListener, this.someDeniedListener, this.somePermanentlyDeniedListener);
            if (this.ifNecesary && (this.permissionObject.hasAll(this.requestedPermissions) || !this.condition)) {
                executePreGrantedPermissionsRequest(permissionsRequest);
            } else if (this.rationaleDialogMessage == null || this.rationalDialogHeader == null) {
                executePermissionsRequest(permissionsRequest);
            } else {
                executePermissionsRequestWithRationale(permissionsRequest);
            }
        }

        private void executePreGrantedPermissionsRequest(PermissionsRequest permissionsRequest) {
            int length = this.requestedPermissions.length;
            int[] iArr = new int[length];
            for (int i = 0; i < length; i++) {
                iArr[i] = 0;
            }
            String[] strArr = this.requestedPermissions;
            permissionsRequest.onResult(strArr, iArr, new boolean[strArr.length]);
        }

        private void executePermissionsRequestWithRationale(PermissionsRequest permissionsRequest) {
            Window window = RationaleDialog.createFor(this.permissionObject.getContext(), this.rationaleDialogMessage, this.rationalDialogHeader).setPositiveButton(R.string.Permissions_continue, (DialogInterface.OnClickListener) new Permissions$PermissionsBuilder$$ExternalSyntheticLambda1(this, permissionsRequest)).setNegativeButton(R.string.Permissions_not_now, (DialogInterface.OnClickListener) new Permissions$PermissionsBuilder$$ExternalSyntheticLambda2(this, permissionsRequest)).setCancelable(this.rationaleDialogCancelable).show().getWindow();
            double windowWidth = (double) this.permissionObject.getWindowWidth();
            Double.isNaN(windowWidth);
            window.setLayout((int) (windowWidth * 0.75d), -2);
        }

        public /* synthetic */ void lambda$executePermissionsRequestWithRationale$0(PermissionsRequest permissionsRequest, DialogInterface dialogInterface, int i) {
            executePermissionsRequest(permissionsRequest);
        }

        public /* synthetic */ void lambda$executePermissionsRequestWithRationale$1(PermissionsRequest permissionsRequest, DialogInterface dialogInterface, int i) {
            executeNoPermissionsRequest(permissionsRequest);
        }

        private void executePermissionsRequest(PermissionsRequest permissionsRequest) {
            int nextInt = new SecureRandom().nextInt(65434) + 100;
            synchronized (Permissions.OUTSTANDING) {
                Permissions.OUTSTANDING.put(Integer.valueOf(nextInt), permissionsRequest);
            }
            String[] strArr = this.requestedPermissions;
            for (String str : strArr) {
                permissionsRequest.addMapping(str, this.permissionObject.shouldShouldPermissionRationale(str));
            }
            this.permissionObject.requestPermissions(nextInt, this.requestedPermissions);
        }

        private void executeNoPermissionsRequest(PermissionsRequest permissionsRequest) {
            for (String str : this.requestedPermissions) {
                permissionsRequest.addMapping(str, true);
            }
            String[] filterNotGranted = Permissions.filterNotGranted(this.permissionObject.getContext(), this.requestedPermissions);
            int[] array = Stream.of(filterNotGranted).mapToInt(new Permissions$PermissionsBuilder$$ExternalSyntheticLambda0()).toArray();
            boolean[] zArr = new boolean[filterNotGranted.length];
            Arrays.fill(zArr, true);
            permissionsRequest.onResult(filterNotGranted, array, zArr);
        }
    }

    public static void requestPermissions(Activity activity, int i, String... strArr) {
        String[] filterNotGranted = filterNotGranted(activity, strArr);
        if (filterNotGranted.length == 0) {
            Log.i(TAG, "No permissions needed!");
        } else {
            ActivityCompat.requestPermissions(activity, filterNotGranted, i);
        }
    }

    public static void requestPermissions(Fragment fragment, int i, String... strArr) {
        if (filterNotGranted(fragment.requireContext(), strArr).length == 0) {
            Log.i(TAG, "No permissions needed!");
        } else {
            fragment.requestPermissions(filterNotGranted(fragment.requireContext(), strArr), i);
        }
    }

    public static String[] filterNotGranted(Context context, String... strArr) {
        return (String[]) Stream.of(strArr).filter(new Predicate(context) { // from class: org.thoughtcrime.securesms.permissions.Permissions$$ExternalSyntheticLambda1
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return Permissions.lambda$filterNotGranted$0(this.f$0, (String) obj);
            }
        }).toList().toArray(new String[0]);
    }

    public static /* synthetic */ boolean lambda$filterNotGranted$0(Context context, String str) {
        return ContextCompat.checkSelfPermission(context, str) != 0;
    }

    public static boolean hasAny(Context context, String... strArr) {
        return Build.VERSION.SDK_INT < 23 || Stream.of(strArr).anyMatch(new Predicate(context) { // from class: org.thoughtcrime.securesms.permissions.Permissions$$ExternalSyntheticLambda2
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return Permissions.lambda$hasAny$1(this.f$0, (String) obj);
            }
        });
    }

    public static /* synthetic */ boolean lambda$hasAny$1(Context context, String str) {
        return ContextCompat.checkSelfPermission(context, str) == 0;
    }

    public static boolean hasAll(Context context, String... strArr) {
        return Build.VERSION.SDK_INT < 23 || Stream.of(strArr).allMatch(new Predicate(context) { // from class: org.thoughtcrime.securesms.permissions.Permissions$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return Permissions.lambda$hasAll$2(this.f$0, (String) obj);
            }
        });
    }

    public static /* synthetic */ boolean lambda$hasAll$2(Context context, String str) {
        return ContextCompat.checkSelfPermission(context, str) == 0;
    }

    public static void onRequestPermissionsResult(Fragment fragment, int i, String[] strArr, int[] iArr) {
        onRequestPermissionsResult(new FragmentPermissionObject(fragment), i, strArr, iArr);
    }

    public static void onRequestPermissionsResult(Activity activity, int i, String[] strArr, int[] iArr) {
        onRequestPermissionsResult(new ActivityPermissionObject(activity), i, strArr, iArr);
    }

    private static void onRequestPermissionsResult(PermissionObject permissionObject, int i, String[] strArr, int[] iArr) {
        PermissionsRequest remove;
        Map<Integer, PermissionsRequest> map = OUTSTANDING;
        synchronized (map) {
            remove = map.remove(Integer.valueOf(i));
        }
        if (remove != null) {
            boolean[] zArr = new boolean[strArr.length];
            for (int i2 = 0; i2 < strArr.length; i2++) {
                if (iArr[i2] != 0) {
                    zArr[i2] = permissionObject.shouldShouldPermissionRationale(strArr[i2]);
                }
            }
            remove.onResult(strArr, iArr, zArr);
        }
    }

    public static Intent getApplicationSettingsIntent(Context context) {
        Intent intent = new Intent();
        intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts("package", context.getPackageName(), null));
        return intent;
    }

    /* loaded from: classes4.dex */
    public static abstract class PermissionObject {
        abstract Context getContext();

        abstract boolean hasAll(String... strArr);

        abstract void requestPermissions(int i, String... strArr);

        abstract boolean shouldShouldPermissionRationale(String str);

        private PermissionObject() {
        }

        int getWindowWidth() {
            Display defaultDisplay = ServiceUtil.getWindowManager(getContext()).getDefaultDisplay();
            DisplayMetrics displayMetrics = new DisplayMetrics();
            defaultDisplay.getMetrics(displayMetrics);
            return displayMetrics.widthPixels;
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public static class ActivityPermissionObject extends PermissionObject {
        private Activity activity;

        ActivityPermissionObject(Activity activity) {
            super();
            this.activity = activity;
        }

        @Override // org.thoughtcrime.securesms.permissions.Permissions.PermissionObject
        public Context getContext() {
            return this.activity;
        }

        @Override // org.thoughtcrime.securesms.permissions.Permissions.PermissionObject
        public boolean shouldShouldPermissionRationale(String str) {
            return ActivityCompat.shouldShowRequestPermissionRationale(this.activity, str);
        }

        @Override // org.thoughtcrime.securesms.permissions.Permissions.PermissionObject
        public boolean hasAll(String... strArr) {
            return Permissions.hasAll(this.activity, strArr);
        }

        @Override // org.thoughtcrime.securesms.permissions.Permissions.PermissionObject
        public void requestPermissions(int i, String... strArr) {
            Permissions.requestPermissions(this.activity, i, strArr);
        }
    }

    /* loaded from: classes4.dex */
    public static class FragmentPermissionObject extends PermissionObject {
        private Fragment fragment;

        FragmentPermissionObject(Fragment fragment) {
            super();
            this.fragment = fragment;
        }

        @Override // org.thoughtcrime.securesms.permissions.Permissions.PermissionObject
        public Context getContext() {
            return this.fragment.getContext();
        }

        @Override // org.thoughtcrime.securesms.permissions.Permissions.PermissionObject
        public boolean shouldShouldPermissionRationale(String str) {
            return this.fragment.shouldShowRequestPermissionRationale(str);
        }

        @Override // org.thoughtcrime.securesms.permissions.Permissions.PermissionObject
        public boolean hasAll(String... strArr) {
            return Permissions.hasAll(this.fragment.getContext(), strArr);
        }

        @Override // org.thoughtcrime.securesms.permissions.Permissions.PermissionObject
        public void requestPermissions(int i, String... strArr) {
            Permissions.requestPermissions(this.fragment, i, strArr);
        }
    }

    /* loaded from: classes4.dex */
    public static class SettingsDialogListener implements Runnable {
        private final WeakReference<Context> context;
        private final String message;

        SettingsDialogListener(Context context, String str) {
            this.message = str;
            this.context = new WeakReference<>(context);
        }

        @Override // java.lang.Runnable
        public void run() {
            Context context = this.context.get();
            if (context != null) {
                new AlertDialog.Builder(context).setTitle(R.string.Permissions_permission_required).setMessage(this.message).setPositiveButton(R.string.Permissions_continue, new Permissions$SettingsDialogListener$$ExternalSyntheticLambda0(context)).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).show();
            }
        }

        public static /* synthetic */ void lambda$run$0(Context context, DialogInterface dialogInterface, int i) {
            context.startActivity(Permissions.getApplicationSettingsIntent(context));
        }
    }
}
