package org.thoughtcrime.securesms.permissions;

import android.content.DialogInterface;
import org.thoughtcrime.securesms.permissions.Permissions;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class Permissions$PermissionsBuilder$$ExternalSyntheticLambda1 implements DialogInterface.OnClickListener {
    public final /* synthetic */ Permissions.PermissionsBuilder f$0;
    public final /* synthetic */ PermissionsRequest f$1;

    public /* synthetic */ Permissions$PermissionsBuilder$$ExternalSyntheticLambda1(Permissions.PermissionsBuilder permissionsBuilder, PermissionsRequest permissionsRequest) {
        this.f$0 = permissionsBuilder;
        this.f$1 = permissionsRequest;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$executePermissionsRequestWithRationale$0(this.f$1, dialogInterface, i);
    }
}
