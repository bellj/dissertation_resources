package org.thoughtcrime.securesms.permissions;

import com.annimon.stream.function.ToIntFunction;
import org.thoughtcrime.securesms.permissions.Permissions;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class Permissions$PermissionsBuilder$$ExternalSyntheticLambda0 implements ToIntFunction {
    @Override // com.annimon.stream.function.ToIntFunction
    public final int applyAsInt(Object obj) {
        return Permissions.PermissionsBuilder.lambda$executeNoPermissionsRequest$2((String) obj);
    }
}
