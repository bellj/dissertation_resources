package org.thoughtcrime.securesms.permissions;

import android.content.Context;
import android.content.DialogInterface;
import org.thoughtcrime.securesms.permissions.Permissions;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class Permissions$SettingsDialogListener$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ Context f$0;

    public /* synthetic */ Permissions$SettingsDialogListener$$ExternalSyntheticLambda0(Context context) {
        this.f$0 = context;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        Permissions.SettingsDialogListener.lambda$run$0(this.f$0, dialogInterface, i);
    }
}
