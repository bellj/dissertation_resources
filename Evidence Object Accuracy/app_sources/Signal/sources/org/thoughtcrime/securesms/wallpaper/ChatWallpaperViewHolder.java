package org.thoughtcrime.securesms.wallpaper;

import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout;
import j$.util.function.Function;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.DisplayMetricsUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.Factory;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaperViewHolder;

/* loaded from: classes5.dex */
public class ChatWallpaperViewHolder extends MappingViewHolder<ChatWallpaperSelectionMappingModel> {
    private final EventListener eventListener;
    private final AspectRatioFrameLayout frame;
    private final ImageView preview;

    public ChatWallpaperViewHolder(View view, EventListener eventListener, DisplayMetrics displayMetrics) {
        super(view);
        AspectRatioFrameLayout aspectRatioFrameLayout = (AspectRatioFrameLayout) view.findViewById(R.id.chat_wallpaper_preview_frame);
        this.frame = aspectRatioFrameLayout;
        this.preview = (ImageView) view.findViewById(R.id.chat_wallpaper_preview);
        this.eventListener = eventListener;
        if (displayMetrics != null) {
            DisplayMetricsUtil.forceAspectRatioToScreenByAdjustingHeight(displayMetrics, view);
        } else if (aspectRatioFrameLayout != null) {
            aspectRatioFrameLayout.setAspectRatio(1.0f);
        }
    }

    public void bind(ChatWallpaperSelectionMappingModel chatWallpaperSelectionMappingModel) {
        chatWallpaperSelectionMappingModel.loadInto(this.preview);
        this.preview.setColorFilter(ChatWallpaperDimLevelUtil.getDimColorFilterForNightMode(this.context, chatWallpaperSelectionMappingModel.getWallpaper()));
        if (this.eventListener != null) {
            this.preview.setOnClickListener(new View.OnClickListener(chatWallpaperSelectionMappingModel) { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperViewHolder$$ExternalSyntheticLambda0
                public final /* synthetic */ ChatWallpaperSelectionMappingModel f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ChatWallpaperViewHolder.this.lambda$bind$0(this.f$1, view);
                }
            });
        }
    }

    public /* synthetic */ void lambda$bind$0(ChatWallpaperSelectionMappingModel chatWallpaperSelectionMappingModel, View view) {
        if (getAdapterPosition() != -1) {
            this.eventListener.onModelClick(chatWallpaperSelectionMappingModel);
        }
    }

    public static Factory<ChatWallpaperSelectionMappingModel> createFactory(int i, EventListener eventListener, DisplayMetrics displayMetrics) {
        return new LayoutFactory(new Function(displayMetrics) { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperViewHolder$$ExternalSyntheticLambda1
            public final /* synthetic */ DisplayMetrics f$1;

            {
                this.f$1 = r2;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ChatWallpaperViewHolder.lambda$createFactory$1(ChatWallpaperViewHolder.EventListener.this, this.f$1, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, i);
    }

    public static /* synthetic */ MappingViewHolder lambda$createFactory$1(EventListener eventListener, DisplayMetrics displayMetrics, View view) {
        return new ChatWallpaperViewHolder(view, eventListener, displayMetrics);
    }

    /* loaded from: classes5.dex */
    public interface EventListener {
        void onClick(ChatWallpaper chatWallpaper);

        void onModelClick(ChatWallpaperSelectionMappingModel chatWallpaperSelectionMappingModel);

        /* renamed from: org.thoughtcrime.securesms.wallpaper.ChatWallpaperViewHolder$EventListener$-CC */
        /* loaded from: classes5.dex */
        public final /* synthetic */ class CC {
            public static void $default$onModelClick(EventListener eventListener, ChatWallpaperSelectionMappingModel chatWallpaperSelectionMappingModel) {
                eventListener.onClick(chatWallpaperSelectionMappingModel.getWallpaper());
            }
        }
    }
}
