package org.thoughtcrime.securesms.wallpaper;

import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;

/* loaded from: classes5.dex */
public class ChatWallpaperPreviewAdapter extends MappingAdapter {
    public ChatWallpaperPreviewAdapter() {
        registerFactory(ChatWallpaperSelectionMappingModel.class, ChatWallpaperViewHolder.createFactory(R.layout.chat_wallpaper_preview_fragment_adapter_item, null, null));
    }
}
