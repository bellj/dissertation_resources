package org.thoughtcrime.securesms.wallpaper;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaperViewHolder;
import org.thoughtcrime.securesms.wallpaper.crop.WallpaperImageSelectionActivity;

/* loaded from: classes5.dex */
public class ChatWallpaperSelectionFragment extends Fragment {
    private static final short CHOOSE_WALLPAPER;
    private ChatWallpaperViewModel viewModel;

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.chat_wallpaper_selection_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        view.findViewById(R.id.chat_wallpaper_choose_from_photos).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperSelectionFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChatWallpaperSelectionFragment.$r8$lambda$x48WfxGdYoiylhflR9xMERMNc2g(ChatWallpaperSelectionFragment.this, view2);
            }
        });
        toolbar.setTitle(R.string.preferences__chat_color_and_wallpaper);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperSelectionFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChatWallpaperSelectionFragment.$r8$lambda$dGl9SVLbnbdC5KLH6SGQ0WQ6S1s(view2);
            }
        });
        ChatWallpaperSelectionAdapter chatWallpaperSelectionAdapter = new ChatWallpaperSelectionAdapter(new ChatWallpaperViewHolder.EventListener() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperSelectionFragment$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaperViewHolder.EventListener
            public final void onClick(ChatWallpaper chatWallpaper) {
                ChatWallpaperSelectionFragment.$r8$lambda$ShMT6576sl1m_Gcpy_Zpavd1gj0(ChatWallpaperSelectionFragment.this, chatWallpaper);
            }

            @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaperViewHolder.EventListener
            public /* synthetic */ void onModelClick(ChatWallpaperSelectionMappingModel chatWallpaperSelectionMappingModel) {
                ChatWallpaperViewHolder.EventListener.CC.$default$onModelClick(this, chatWallpaperSelectionMappingModel);
            }
        });
        ((RecyclerView) view.findViewById(R.id.chat_wallpaper_recycler)).setAdapter(chatWallpaperSelectionAdapter);
        ChatWallpaperViewModel chatWallpaperViewModel = (ChatWallpaperViewModel) ViewModelProviders.of(requireActivity()).get(ChatWallpaperViewModel.class);
        this.viewModel = chatWallpaperViewModel;
        chatWallpaperViewModel.getWallpapers().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperSelectionFragment$$ExternalSyntheticLambda3
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ChatWallpaperSelectionAdapter.this.submitList((List) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        askForPermissionIfNeededAndLaunchPhotoSelection();
    }

    public static /* synthetic */ void lambda$onViewCreated$1(View view) {
        Navigation.findNavController(view).popBackStack();
    }

    public /* synthetic */ void lambda$onViewCreated$2(ChatWallpaper chatWallpaper) {
        startActivityForResult(ChatWallpaperPreviewActivity.create(requireActivity(), chatWallpaper, this.viewModel.getRecipientId(), this.viewModel.getDimInDarkTheme().getValue().booleanValue()), 1);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        this.viewModel.refreshWallpaper();
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 1 && i2 == -1 && intent != null) {
            this.viewModel.setWallpaper((ChatWallpaper) intent.getParcelableExtra(ChatWallpaperPreviewActivity.EXTRA_CHAT_WALLPAPER));
            this.viewModel.saveWallpaperSelection();
            Navigation.findNavController(requireView()).popBackStack();
            return;
        }
        super.onActivityResult(i, i2, intent);
    }

    private void askForPermissionIfNeededAndLaunchPhotoSelection() {
        Permissions.with(this).request("android.permission.READ_EXTERNAL_STORAGE").ifNecessary().onAllGranted(new Runnable() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperSelectionFragment$$ExternalSyntheticLambda4
            @Override // java.lang.Runnable
            public final void run() {
                ChatWallpaperSelectionFragment.$r8$lambda$PwdvywaHJzFwKTDqxJUTPg67_Xs(ChatWallpaperSelectionFragment.this);
            }
        }).onAnyDenied(new Runnable() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperSelectionFragment$$ExternalSyntheticLambda5
            @Override // java.lang.Runnable
            public final void run() {
                ChatWallpaperSelectionFragment.$r8$lambda$NFA3FfVfwNcTEKl2C98eoaOFdAI(ChatWallpaperSelectionFragment.this);
            }
        }).execute();
    }

    public /* synthetic */ void lambda$askForPermissionIfNeededAndLaunchPhotoSelection$3() {
        startActivityForResult(WallpaperImageSelectionActivity.getIntent(requireContext(), this.viewModel.getRecipientId()), 1);
    }

    public /* synthetic */ void lambda$askForPermissionIfNeededAndLaunchPhotoSelection$4() {
        Toast.makeText(requireContext(), (int) R.string.ChatWallpaperPreviewActivity__viewing_your_gallery_requires_the_storage_permission, 0).show();
    }
}
