package org.thoughtcrime.securesms.wallpaper;

import android.content.Context;
import android.net.Uri;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.util.storage.FileStorage;

/* loaded from: classes5.dex */
public final class WallpaperStorage {
    private static final String DIRECTORY;
    private static final String FILENAME_BASE;
    private static final String TAG = Log.tag(WallpaperStorage.class);

    public static ChatWallpaper save(Context context, InputStream inputStream, String str) throws IOException {
        return ChatWallpaperFactory.create(PartAuthority.getWallpaperUri(FileStorage.save(context, inputStream, DIRECTORY, FILENAME_BASE, str)));
    }

    public static InputStream read(Context context, String str) throws IOException {
        return FileStorage.read(context, DIRECTORY, str);
    }

    public static List<ChatWallpaper> getAll(Context context) {
        return (List) Collection$EL.stream(FileStorage.getAll(context, DIRECTORY, FILENAME_BASE)).map(new Function() { // from class: org.thoughtcrime.securesms.wallpaper.WallpaperStorage$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return PartAuthority.getWallpaperUri((String) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.wallpaper.WallpaperStorage$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ChatWallpaperFactory.create((Uri) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList());
    }

    public static void onWallpaperDeselected(Context context, Uri uri) {
        if (!Objects.equals(uri, SignalStore.wallpaper().getWallpaperUri()) && SignalDatabase.recipients().getWallpaperUriUsageCount(uri) <= 0) {
            String wallpaperFilename = PartAuthority.getWallpaperFilename(uri);
            if (!new File(context.getDir(DIRECTORY, 0), wallpaperFilename).delete()) {
                String str = TAG;
                Log.w(str, "Failed to delete " + wallpaperFilename + "!");
            }
        }
    }
}
