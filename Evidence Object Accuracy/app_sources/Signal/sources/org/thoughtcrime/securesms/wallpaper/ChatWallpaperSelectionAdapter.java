package org.thoughtcrime.securesms.wallpaper;

import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaperViewHolder;

/* access modifiers changed from: package-private */
/* loaded from: classes5.dex */
public class ChatWallpaperSelectionAdapter extends MappingAdapter {
    public ChatWallpaperSelectionAdapter(ChatWallpaperViewHolder.EventListener eventListener) {
        registerFactory(ChatWallpaperSelectionMappingModel.class, ChatWallpaperViewHolder.createFactory(R.layout.chat_wallpaper_selection_fragment_adapter_item, eventListener, null));
    }
}
