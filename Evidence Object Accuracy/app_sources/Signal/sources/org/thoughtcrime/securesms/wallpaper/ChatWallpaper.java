package org.thoughtcrime.securesms.wallpaper;

import android.content.Context;
import android.os.Parcelable;
import android.widget.ImageView;
import java.util.Arrays;
import java.util.List;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.conversation.colors.ChatColorsMapper;
import org.thoughtcrime.securesms.database.model.databaseprotos.Wallpaper;

/* loaded from: classes5.dex */
public interface ChatWallpaper extends Parcelable {
    public static final float FIXED_DIM_LEVEL_FOR_DARK_THEME;

    ChatColors getAutoChatColors();

    float getDimLevelForDarkTheme();

    boolean isPhoto();

    boolean isSameSource(ChatWallpaper chatWallpaper);

    void loadInto(ImageView imageView);

    boolean prefetch(Context context, long j);

    Wallpaper serialize();

    /* renamed from: org.thoughtcrime.securesms.wallpaper.ChatWallpaper$-CC */
    /* loaded from: classes5.dex */
    public final /* synthetic */ class CC {
        public static boolean $default$isPhoto(ChatWallpaper chatWallpaper) {
            return false;
        }

        public static boolean $default$prefetch(ChatWallpaper chatWallpaper, Context context, long j) {
            return true;
        }

        public static ChatColors $default$getAutoChatColors(ChatWallpaper chatWallpaper) {
            return ChatColorsMapper.getChatColors(chatWallpaper).withId(ChatColors.Id.Auto.INSTANCE);
        }
    }

    /* loaded from: classes5.dex */
    public enum BuiltIns {
        INSTANCE;

        public List<ChatWallpaper> getAllBuiltIns() {
            return Arrays.asList(SingleColorChatWallpaper.BLUSH, SingleColorChatWallpaper.COPPER, SingleColorChatWallpaper.DUST, SingleColorChatWallpaper.CELADON, SingleColorChatWallpaper.RAINFOREST, SingleColorChatWallpaper.PACIFIC, SingleColorChatWallpaper.FROST, SingleColorChatWallpaper.NAVY, SingleColorChatWallpaper.LILAC, SingleColorChatWallpaper.PINK, SingleColorChatWallpaper.EGGPLANT, SingleColorChatWallpaper.SILVER, GradientChatWallpaper.SUNSET, GradientChatWallpaper.NOIR, GradientChatWallpaper.HEATMAP, GradientChatWallpaper.AQUA, GradientChatWallpaper.IRIDESCENT, GradientChatWallpaper.MONSTERA, GradientChatWallpaper.BLISS, GradientChatWallpaper.SKY, GradientChatWallpaper.PEACH);
        }
    }
}
