package org.thoughtcrime.securesms.wallpaper.crop;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import java.util.Collections;
import java.util.Locale;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.signal.imageeditor.core.ImageEditorView;
import org.signal.imageeditor.core.model.EditorElement;
import org.signal.imageeditor.core.model.EditorModel;
import org.signal.imageeditor.core.renderers.FaceBlurRenderer;
import org.thoughtcrime.securesms.BaseActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.colors.ColorizerView;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.scribbles.UriGlideRenderer;
import org.thoughtcrime.securesms.util.AsynchronousCallback;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.Projection;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaperPreviewActivity;
import org.thoughtcrime.securesms.wallpaper.crop.WallpaperCropViewModel;

/* loaded from: classes5.dex */
public final class WallpaperCropActivity extends BaseActivity {
    private static final String EXTRA_IMAGE_URI;
    private static final String EXTRA_RECIPIENT_ID;
    private static final String TAG = Log.tag(WallpaperCropActivity.class);
    private final DynamicTheme dynamicTheme = new DynamicWallpaperTheme();
    private ImageEditorView imageEditor;
    private WallpaperCropViewModel viewModel;

    public static Intent newIntent(Context context, RecipientId recipientId, Uri uri) {
        Intent intent = new Intent(context, WallpaperCropActivity.class);
        intent.putExtra(EXTRA_RECIPIENT_ID, recipientId);
        Objects.requireNonNull(uri);
        intent.putExtra(EXTRA_IMAGE_URI, uri);
        return intent;
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, android.app.Activity, android.view.ContextThemeWrapper, android.content.ContextWrapper
    public void attachBaseContext(Context context) {
        getDelegate().setLocalNightMode(2);
        super.attachBaseContext(context);
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.dynamicTheme.onCreate(this);
        setContentView(R.layout.chat_wallpaper_crop_activity);
        RecipientId recipientId = (RecipientId) getIntent().getParcelableExtra(EXTRA_RECIPIENT_ID);
        Uri uri = (Uri) getIntent().getParcelableExtra(EXTRA_IMAGE_URI);
        Objects.requireNonNull(uri);
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Cropping wallpaper for ");
        sb.append(recipientId == null ? "default wallpaper" : recipientId);
        Log.i(str, sb.toString());
        this.viewModel = (WallpaperCropViewModel) ViewModelProviders.of(this, new WallpaperCropViewModel.Factory(recipientId)).get(WallpaperCropViewModel.class);
        this.imageEditor = (ImageEditorView) findViewById(R.id.image_editor);
        View findViewById = findViewById(R.id.preview_bubble_2);
        View findViewById2 = findViewById(R.id.preview_set_wallpaper);
        SwitchCompat switchCompat = (SwitchCompat) findViewById(R.id.preview_blur);
        ColorizerView colorizerView = (ColorizerView) findViewById(R.id.colorizer);
        setupImageEditor(uri);
        findViewById2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.wallpaper.crop.WallpaperCropActivity$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WallpaperCropActivity.$r8$lambda$MWnAMBcNI5SW_uznLiLGGA2i_p4(WallpaperCropActivity.this, view);
            }
        });
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        ActionBar supportActionBar = getSupportActionBar();
        Objects.requireNonNull(supportActionBar);
        supportActionBar.setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.ic_arrow_left_24));
        supportActionBar.setDisplayHomeAsUpEnabled(true);
        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: org.thoughtcrime.securesms.wallpaper.crop.WallpaperCropActivity$$ExternalSyntheticLambda2
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                WallpaperCropActivity.m3318$r8$lambda$tDyidotaR8w0D6kL6vszAsNLKg(WallpaperCropActivity.this, compoundButton, z);
            }
        });
        this.viewModel.getBlur().observe(this, new Observer(switchCompat) { // from class: org.thoughtcrime.securesms.wallpaper.crop.WallpaperCropActivity$$ExternalSyntheticLambda3
            public final /* synthetic */ SwitchCompat f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                WallpaperCropActivity.$r8$lambda$LsbAVrkUG055e84kOVJ_8YcBYTM(WallpaperCropActivity.this, this.f$1, (Boolean) obj);
            }
        });
        this.viewModel.getRecipient().observe(this, new Observer((TextView) findViewById(R.id.chat_wallpaper_bubble2_text), findViewById, colorizerView) { // from class: org.thoughtcrime.securesms.wallpaper.crop.WallpaperCropActivity$$ExternalSyntheticLambda4
            public final /* synthetic */ TextView f$1;
            public final /* synthetic */ View f$2;
            public final /* synthetic */ ColorizerView f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                WallpaperCropActivity.$r8$lambda$SOeSUFh4LBHBuqehrEtQYMreN_Y(WallpaperCropActivity.this, this.f$1, this.f$2, this.f$3, (Recipient) obj);
            }
        });
        findViewById.addOnLayoutChangeListener(new View.OnLayoutChangeListener(findViewById) { // from class: org.thoughtcrime.securesms.wallpaper.crop.WallpaperCropActivity$$ExternalSyntheticLambda5
            public final /* synthetic */ View f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnLayoutChangeListener
            public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                WallpaperCropActivity.$r8$lambda$vuHxdiYvk1BDNpA4Vgaye8aUFJ4(ColorizerView.this, this.f$1, view, i, i2, i3, i4, i5, i6, i7, i8);
            }
        });
    }

    public /* synthetic */ void lambda$onCreate$0(View view) {
        setWallpaper();
    }

    public /* synthetic */ void lambda$onCreate$1(CompoundButton compoundButton, boolean z) {
        this.viewModel.setBlur(z);
    }

    public /* synthetic */ void lambda$onCreate$2(SwitchCompat switchCompat, Boolean bool) {
        setBlurred(bool.booleanValue());
        if (bool.booleanValue() != switchCompat.isChecked()) {
            switchCompat.setChecked(bool.booleanValue());
        }
    }

    public /* synthetic */ void lambda$onCreate$3(TextView textView, View view, ColorizerView colorizerView, Recipient recipient) {
        if (recipient.getId().isUnknown()) {
            textView.setText(R.string.WallpaperCropActivity__set_wallpaper_for_all_chats);
            return;
        }
        textView.setText(getString(R.string.WallpaperCropActivity__set_wallpaper_for_s, new Object[]{recipient.getDisplayName(this)}));
        view.getBackground().setColorFilter(recipient.getChatColors().getChatBubbleColorFilter());
        colorizerView.setBackground(recipient.getChatColors().getChatBubbleMask());
    }

    public static /* synthetic */ void lambda$onCreate$4(ColorizerView colorizerView, View view, View view2, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        colorizerView.setProjections(Collections.singletonList(Projection.relativeToViewWithCommonRoot(view, colorizerView, new Projection.Corners((float) ViewUtil.dpToPx(18)))));
    }

    @Override // org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }

    @Override // android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (super.onOptionsItemSelected(menuItem)) {
            return true;
        }
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        finish();
        return true;
    }

    private void setWallpaper() {
        EditorModel model = this.imageEditor.getModel();
        Point point = new Point(this.imageEditor.getWidth(), this.imageEditor.getHeight());
        final AlertDialog show = SimpleProgressDialog.show(this);
        this.viewModel.render(this, model, point, new AsynchronousCallback.MainThread<ChatWallpaper, WallpaperCropViewModel.Error>() { // from class: org.thoughtcrime.securesms.wallpaper.crop.WallpaperCropActivity.1
            @Override // org.thoughtcrime.securesms.util.AsynchronousCallback.MainThread
            public /* synthetic */ AsynchronousCallback.WorkerThread<ChatWallpaper, WallpaperCropViewModel.Error> toWorkerCallback() {
                return AsynchronousCallback.MainThread.CC.$default$toWorkerCallback(this);
            }

            public void onComplete(ChatWallpaper chatWallpaper) {
                show.dismiss();
                WallpaperCropActivity.this.setResult(-1, new Intent().putExtra(ChatWallpaperPreviewActivity.EXTRA_CHAT_WALLPAPER, chatWallpaper));
                WallpaperCropActivity.this.finish();
            }

            public void onError(WallpaperCropViewModel.Error error) {
                show.dismiss();
                Toast.makeText(WallpaperCropActivity.this, (int) R.string.WallpaperCropActivity__error_setting_wallpaper, 0).show();
            }
        }.toWorkerCallback());
    }

    private void setupImageEditor(Uri uri) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int i = displayMetrics.heightPixels;
        int i2 = displayMetrics.widthPixels;
        EditorModel createForWallpaperEditing = EditorModel.createForWallpaperEditing(((float) i2) / ((float) i), ContextCompat.getColor(this, R.color.signal_colorBackground));
        EditorElement editorElement = new EditorElement(new UriGlideRenderer(uri, true, i2, i, 3.0f));
        editorElement.getFlags().setSelectable(false).persist();
        createForWallpaperEditing.addElement(editorElement);
        this.imageEditor.setModel(createForWallpaperEditing);
        this.imageEditor.setSizeChangedListener(new ImageEditorView.SizeChangedListener() { // from class: org.thoughtcrime.securesms.wallpaper.crop.WallpaperCropActivity$$ExternalSyntheticLambda0
            @Override // org.signal.imageeditor.core.ImageEditorView.SizeChangedListener
            public final void onSizeChanged(int i3, int i4) {
                WallpaperCropActivity.$r8$lambda$lUxDHiU_0PVi1_9juQ9m7ZAwal0(EditorModel.this, i3, i4);
            }
        });
    }

    public static /* synthetic */ void lambda$setupImageEditor$5(EditorModel editorModel, int i, int i2) {
        float f = ((float) i) / ((float) i2);
        Log.i(TAG, String.format(Locale.US, "Output size (%d, %d) (ratio %.2f)", Integer.valueOf(i), Integer.valueOf(i2), Float.valueOf(f)));
        editorModel.setFixedRatio(f);
    }

    private void setBlurred(boolean z) {
        EditorElement mainImage;
        this.imageEditor.getModel().clearFaceRenderers();
        if (z && (mainImage = this.imageEditor.getModel().getMainImage()) != null) {
            EditorElement editorElement = new EditorElement(new FaceBlurRenderer(), -1);
            editorElement.getFlags().setEditable(false).setSelectable(false).persist();
            mainImage.addElement(editorElement);
            this.imageEditor.invalidate();
        }
    }

    /* loaded from: classes5.dex */
    private static final class DynamicWallpaperTheme extends DynamicTheme {
        @Override // org.thoughtcrime.securesms.util.DynamicTheme
        protected int getTheme() {
            return R.style.Signal_DayNight_WallpaperCropper;
        }

        private DynamicWallpaperTheme() {
        }
    }
}
