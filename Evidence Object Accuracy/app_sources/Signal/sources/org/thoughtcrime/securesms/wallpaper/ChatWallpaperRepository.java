package org.thoughtcrime.securesms.wallpaper;

import androidx.core.util.Consumer;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executor;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.conversation.colors.ChatColorsPalette;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.concurrent.SerialExecutor;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;

/* loaded from: classes5.dex */
public class ChatWallpaperRepository {
    private static final Executor EXECUTOR = new SerialExecutor(SignalExecutors.BOUNDED);

    public ChatWallpaper getCurrentWallpaper(RecipientId recipientId) {
        if (recipientId != null) {
            return Recipient.live(recipientId).get().getWallpaper();
        }
        return SignalStore.wallpaper().getWallpaper();
    }

    public ChatColors getCurrentChatColors(RecipientId recipientId) {
        if (recipientId != null) {
            return Recipient.live(recipientId).get().getChatColors();
        }
        if (SignalStore.chatColorsValues().hasChatColors()) {
            ChatColors chatColors = SignalStore.chatColorsValues().getChatColors();
            Objects.requireNonNull(chatColors);
            return chatColors;
        } else if (!SignalStore.wallpaper().hasWallpaperSet()) {
            return ChatColorsPalette.Bubbles.getDefault().withId(ChatColors.Id.Auto.INSTANCE);
        } else {
            ChatWallpaper wallpaper = SignalStore.wallpaper().getWallpaper();
            Objects.requireNonNull(wallpaper);
            return wallpaper.getAutoChatColors();
        }
    }

    public void getAllWallpaper(Consumer<List<ChatWallpaper>> consumer) {
        EXECUTOR.execute(new Runnable() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperRepository$$ExternalSyntheticLambda2
            @Override // java.lang.Runnable
            public final void run() {
                ChatWallpaperRepository.lambda$getAllWallpaper$0(Consumer.this);
            }
        });
    }

    public static /* synthetic */ void lambda$getAllWallpaper$0(Consumer consumer) {
        ArrayList arrayList = new ArrayList(ChatWallpaper.BuiltIns.INSTANCE.getAllBuiltIns());
        arrayList.addAll(WallpaperStorage.getAll(ApplicationDependencies.getApplication()));
        consumer.accept(arrayList);
    }

    public void saveWallpaper(RecipientId recipientId, ChatWallpaper chatWallpaper, Runnable runnable) {
        if (recipientId != null) {
            EXECUTOR.execute(new Runnable(chatWallpaper, runnable) { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperRepository$$ExternalSyntheticLambda4
                public final /* synthetic */ ChatWallpaper f$1;
                public final /* synthetic */ Runnable f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ChatWallpaperRepository.lambda$saveWallpaper$1(RecipientId.this, this.f$1, this.f$2);
                }
            });
            return;
        }
        SignalStore.wallpaper().setWallpaper(ApplicationDependencies.getApplication(), chatWallpaper);
        runnable.run();
    }

    public static /* synthetic */ void lambda$saveWallpaper$1(RecipientId recipientId, ChatWallpaper chatWallpaper, Runnable runnable) {
        SignalDatabase.recipients().setWallpaper(recipientId, chatWallpaper);
        runnable.run();
    }

    public void resetAllWallpaper(Runnable runnable) {
        SignalStore.wallpaper().setWallpaper(ApplicationDependencies.getApplication(), null);
        EXECUTOR.execute(new Runnable(runnable) { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperRepository$$ExternalSyntheticLambda5
            public final /* synthetic */ Runnable f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatWallpaperRepository.lambda$resetAllWallpaper$2(this.f$0);
            }
        });
    }

    public static /* synthetic */ void lambda$resetAllWallpaper$2(Runnable runnable) {
        SignalDatabase.recipients().resetAllWallpaper();
        runnable.run();
    }

    public void resetAllChatColors(Runnable runnable) {
        SignalStore.chatColorsValues().setChatColors(null);
        EXECUTOR.execute(new Runnable(runnable) { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ Runnable f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatWallpaperRepository.lambda$resetAllChatColors$3(this.f$0);
            }
        });
    }

    public static /* synthetic */ void lambda$resetAllChatColors$3(Runnable runnable) {
        SignalDatabase.recipients().clearAllColors();
        runnable.run();
    }

    public void setDimInDarkTheme(RecipientId recipientId, boolean z) {
        if (recipientId != null) {
            EXECUTOR.execute(new Runnable(z) { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperRepository$$ExternalSyntheticLambda3
                public final /* synthetic */ boolean f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ChatWallpaperRepository.lambda$setDimInDarkTheme$4(RecipientId.this, this.f$1);
                }
            });
        } else {
            SignalStore.wallpaper().setDimInDarkTheme(z);
        }
    }

    public static /* synthetic */ void lambda$setDimInDarkTheme$4(RecipientId recipientId, boolean z) {
        Recipient resolved = Recipient.resolved(recipientId);
        if (resolved.hasOwnWallpaper()) {
            SignalDatabase.recipients().setDimWallpaperInDarkTheme(recipientId, z);
        } else if (resolved.hasWallpaper()) {
            SignalDatabase.recipients().setWallpaper(recipientId, ChatWallpaperFactory.updateWithDimming(resolved.getWallpaper(), z ? 0.2f : 0.0f));
        } else {
            throw new IllegalStateException("Unexpected call to setDimInDarkTheme, no wallpaper has been set on the given recipient or globally.");
        }
    }

    public void clearChatColor(RecipientId recipientId, Runnable runnable) {
        if (recipientId == null) {
            SignalStore.chatColorsValues().setChatColors(null);
            runnable.run();
            return;
        }
        EXECUTOR.execute(new Runnable(runnable) { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ Runnable f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatWallpaperRepository.lambda$clearChatColor$5(RecipientId.this, this.f$1);
            }
        });
    }

    public static /* synthetic */ void lambda$clearChatColor$5(RecipientId recipientId, Runnable runnable) {
        SignalDatabase.recipients().clearColor(recipientId);
        runnable.run();
    }
}
