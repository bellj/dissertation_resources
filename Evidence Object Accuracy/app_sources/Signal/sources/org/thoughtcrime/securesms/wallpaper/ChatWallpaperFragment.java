package org.thoughtcrime.securesms.wallpaper;

import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.widget.ImageViewCompat;
import androidx.core.widget.TextViewCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import j$.util.Optional;
import java.util.Collections;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.conversation.colors.ColorizerView;
import org.thoughtcrime.securesms.util.DisplayMetricsUtil;
import org.thoughtcrime.securesms.util.Projection;
import org.thoughtcrime.securesms.util.ThemeUtil;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* loaded from: classes5.dex */
public class ChatWallpaperFragment extends Fragment {
    private SwitchCompat dimInNightMode;
    private boolean isSettingDimFromViewModel;
    private ChatWallpaperViewModel viewModel;

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.chat_wallpaper_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.viewModel = (ChatWallpaperViewModel) ViewModelProviders.of(requireActivity()).get(ChatWallpaperViewModel.class);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ImageView imageView = (ImageView) view.findViewById(R.id.chat_wallpaper_preview_background);
        View findViewById = view.findViewById(R.id.chat_wallpaper_set_wallpaper);
        View findViewById2 = view.findViewById(R.id.chat_wallpaper_dim);
        TextView textView = (TextView) view.findViewById(R.id.chat_wallpaper_set_chat_color);
        TextView textView2 = (TextView) view.findViewById(R.id.chat_wallpaper_reset_chat_colors);
        ImageView imageView2 = (ImageView) view.findViewById(R.id.chat_wallpaper_preview_bubble_2);
        ColorizerView colorizerView = (ColorizerView) view.findViewById(R.id.colorizer);
        TextView textView3 = (TextView) view.findViewById(R.id.chat_wallpaper_reset_all_wallpapers);
        View findViewById3 = view.findViewById(R.id.chat_wallpaper_preview_bottom_bar_plus);
        this.dimInNightMode = (SwitchCompat) view.findViewById(R.id.chat_wallpaper_dark_theme_dims_wallpaper);
        toolbar.setTitle(R.string.preferences__chat_color_and_wallpaper);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChatWallpaperFragment.$r8$lambda$s0HoF5KSXzzTPtXJntHsngrROBw(ChatWallpaperFragment.this, view2);
            }
        });
        forceAspectRatioToScreenByAdjustingHeight(imageView);
        this.viewModel.getWallpaperPreviewPortrait().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda9
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ChatWallpaperFragment.m3308$r8$lambda$smLOolF3XQCCw0SfxuaKz_xk(AvatarImageView.this, (WallpaperPreviewPortrait) obj);
            }
        });
        this.viewModel.getCurrentWallpaper().observe(getViewLifecycleOwner(), new Observer(imageView, (AppCompatImageView) view.findViewById(R.id.chat_wallpaper_preview_bubble_1)) { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda10
            public final /* synthetic */ ImageView f$1;
            public final /* synthetic */ AppCompatImageView f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ChatWallpaperFragment.m3304$r8$lambda$5WBBe_cq3xETNFLcs9YQHVtQng(ChatWallpaperFragment.this, this.f$1, this.f$2, (Optional) obj);
            }
        });
        this.viewModel.getDimInDarkTheme().observe(getViewLifecycleOwner(), new Observer(findViewById2) { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda11
            public final /* synthetic */ View f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ChatWallpaperFragment.$r8$lambda$y40f_g918Igy3U1bIhssAdEzkl4(ChatWallpaperFragment.this, this.f$1, (Boolean) obj);
            }
        });
        this.viewModel.getEnableWallpaperControls().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda12
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ChatWallpaperFragment.$r8$lambda$ZqpQGU4gYJPuWGp9yFV7uPEcsWg(ChatWallpaperFragment.this, (Boolean) obj);
            }
        });
        imageView.setOnClickListener(new View.OnClickListener(findViewById) { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda13
            public final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChatWallpaperFragment.m3305$r8$lambda$Bkw4phsW0nrZoHEfbVkiDU0GPM(this.f$0, view2);
            }
        });
        findViewById.setOnClickListener(new View.OnClickListener(view) { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda14
            public final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChatWallpaperFragment.m3307$r8$lambda$hKyMrmZ4Hjv_5HxeVVwgLUCzjI(this.f$0, view2);
            }
        });
        textView.setOnClickListener(new View.OnClickListener(view) { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda15
            public final /* synthetic */ View f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChatWallpaperFragment.$r8$lambda$Bic17y7sOmbQ_HvmTaSs7CVfojw(ChatWallpaperFragment.this, this.f$1, view2);
            }
        });
        if (this.viewModel.isGlobal()) {
            textView3.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda16
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    ChatWallpaperFragment.$r8$lambda$WMXWy0WJsP5DfQA3rnI9GSM52wE(ChatWallpaperFragment.this, view2);
                }
            });
            textView2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda17
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    ChatWallpaperFragment.m3309$r8$lambda$v3QeyWfvoPbliESRVEfgKfZYRk(ChatWallpaperFragment.this, view2);
                }
            });
        } else {
            textView3.setText(R.string.ChatWallpaperFragment__reset_wallpaper);
            textView2.setText(R.string.ChatWallpaperFragment__reset_chat_color);
            textView3.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda5
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    ChatWallpaperFragment.$r8$lambda$PwKfxzzZZCwJg5hWYRT3DuZBvBk(ChatWallpaperFragment.this, view2);
                }
            });
            textView2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda6
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    ChatWallpaperFragment.$r8$lambda$7a7dfH1ayIvkhjSqt2qDbElXw10(ChatWallpaperFragment.this, view2);
                }
            });
        }
        this.viewModel.getCurrentChatColors().observe(getViewLifecycleOwner(), new Observer(imageView2, colorizerView, textView, findViewById3) { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda7
            public final /* synthetic */ ImageView f$0;
            public final /* synthetic */ ColorizerView f$1;
            public final /* synthetic */ TextView f$2;
            public final /* synthetic */ View f$3;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ChatWallpaperFragment.$r8$lambda$ushFzkiJIMUCGpXWowOBHOY9IiU(this.f$0, this.f$1, this.f$2, this.f$3, (ChatColors) obj);
            }
        });
        imageView2.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda8
            @Override // android.view.View.OnLayoutChangeListener
            public final void onLayoutChange(View view2, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                ChatWallpaperFragment.$r8$lambda$oAKpDmkGt1U8dqVUtPPdrWCa9E8(ChatWallpaperFragment.this, view2, i, i2, i3, i4, i5, i6, i7, i8);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        if (!Navigation.findNavController(view).popBackStack()) {
            requireActivity().finish();
        }
    }

    public /* synthetic */ void lambda$onViewCreated$2(ImageView imageView, AppCompatImageView appCompatImageView, Optional optional) {
        if (optional.isPresent()) {
            ((ChatWallpaper) optional.get()).loadInto(imageView);
            ImageViewCompat.setImageTintList(appCompatImageView, ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.conversation_item_recv_bubble_color_wallpaper)));
            return;
        }
        imageView.setImageDrawable(null);
        ImageViewCompat.setImageTintList(appCompatImageView, ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.signal_background_secondary)));
    }

    public /* synthetic */ void lambda$onViewCreated$3(View view, Boolean bool) {
        int i = 0;
        if (bool.booleanValue() != this.dimInNightMode.isChecked()) {
            this.isSettingDimFromViewModel = true;
            this.dimInNightMode.setChecked(bool.booleanValue());
            this.isSettingDimFromViewModel = false;
        }
        view.setAlpha(0.2f);
        if (!bool.booleanValue() || !ThemeUtil.isDarkTheme(requireContext())) {
            i = 8;
        }
        view.setVisibility(i);
    }

    public /* synthetic */ void lambda$onViewCreated$4(Boolean bool) {
        this.dimInNightMode.setEnabled(bool.booleanValue());
        this.dimInNightMode.setAlpha(bool.booleanValue() ? 1.0f : 0.5f);
    }

    public static /* synthetic */ void lambda$onViewCreated$6(View view, View view2) {
        SafeNavigation.safeNavigate(Navigation.findNavController(view), (int) R.id.action_chatWallpaperFragment_to_chatWallpaperSelectionFragment);
    }

    public /* synthetic */ void lambda$onViewCreated$7(View view, View view2) {
        SafeNavigation.safeNavigate(Navigation.findNavController(view), ChatWallpaperFragmentDirections.actionChatWallpaperFragmentToChatColorSelectionFragment(this.viewModel.getRecipientId()));
    }

    public /* synthetic */ void lambda$onViewCreated$11(View view) {
        new MaterialAlertDialogBuilder(requireContext()).setTitle(R.string.ChatWallpaperFragment__reset_wallpaper).setMessage(R.string.ChatWallpaperFragment__would_you_like_to_override_all_wallpapers).setPositiveButton(R.string.ChatWallpaperFragment__reset_default_wallpaper, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda18
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChatWallpaperFragment.$r8$lambda$9Wu0x_9x9v1n7LzxFnsgno5xRVc(ChatWallpaperFragment.this, dialogInterface, i);
            }
        }).setNegativeButton(R.string.ChatWallpaperFragment__reset_all_wallpapers, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda19
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChatWallpaperFragment.$r8$lambda$yC27OU6bROwrgRfXZqrlQ_Htx04(ChatWallpaperFragment.this, dialogInterface, i);
            }
        }).setNeutralButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda20
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChatWallpaperFragment.m3306$r8$lambda$UnP8egMQTBywm_eiONHA4DHBVc(dialogInterface, i);
            }
        }).show();
    }

    public /* synthetic */ void lambda$onViewCreated$8(DialogInterface dialogInterface, int i) {
        this.viewModel.setWallpaper(null);
        this.viewModel.setDimInDarkTheme(true);
        this.viewModel.saveWallpaperSelection();
        dialogInterface.dismiss();
    }

    public /* synthetic */ void lambda$onViewCreated$9(DialogInterface dialogInterface, int i) {
        this.viewModel.setWallpaper(null);
        this.viewModel.setDimInDarkTheme(true);
        this.viewModel.resetAllWallpaper();
        dialogInterface.dismiss();
    }

    public /* synthetic */ void lambda$onViewCreated$15(View view) {
        new MaterialAlertDialogBuilder(requireContext()).setTitle(R.string.ChatWallpaperFragment__reset_chat_colors).setMessage(R.string.ChatWallpaperFragment__would_you_like_to_override_all_chat_colors).setPositiveButton(R.string.ChatWallpaperFragment__reset_default_colors, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda1
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChatWallpaperFragment.$r8$lambda$16b5_66xsbiVc_CRFQIVnrLWxXg(ChatWallpaperFragment.this, dialogInterface, i);
            }
        }).setNegativeButton(R.string.ChatWallpaperFragment__reset_all_colors, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda2
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChatWallpaperFragment.$r8$lambda$LrQH0wJnqNPrlcPxrF_yjBnX5B0(ChatWallpaperFragment.this, dialogInterface, i);
            }
        }).setNeutralButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda3
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChatWallpaperFragment.$r8$lambda$LWzpmh15JS8UQLcAbsz_d6WRBSs(dialogInterface, i);
            }
        }).show();
    }

    public /* synthetic */ void lambda$onViewCreated$12(DialogInterface dialogInterface, int i) {
        this.viewModel.clearChatColor();
        dialogInterface.dismiss();
    }

    public /* synthetic */ void lambda$onViewCreated$13(DialogInterface dialogInterface, int i) {
        this.viewModel.resetAllChatColors();
        dialogInterface.dismiss();
    }

    public /* synthetic */ void lambda$onViewCreated$18(View view) {
        new MaterialAlertDialogBuilder(requireContext()).setMessage(R.string.ChatWallpaperFragment__reset_wallpaper_question).setPositiveButton(R.string.ChatWallpaperFragment__reset, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda21
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChatWallpaperFragment.$r8$lambda$qrcrcwfxpNU8dTCV8_5wOLkGlqU(ChatWallpaperFragment.this, dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda22
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChatWallpaperFragment.$r8$lambda$do9e4kPtr2MioIdsAV5GpV1AQB0(dialogInterface, i);
            }
        }).show();
    }

    public /* synthetic */ void lambda$onViewCreated$16(DialogInterface dialogInterface, int i) {
        this.viewModel.setWallpaper(null);
        this.viewModel.setDimInDarkTheme(true);
        this.viewModel.saveWallpaperSelection();
        this.viewModel.refreshChatColors();
    }

    public /* synthetic */ void lambda$onViewCreated$21(View view) {
        new MaterialAlertDialogBuilder(requireContext()).setMessage(R.string.ChatWallpaperFragment__reset_chat_color_question).setPositiveButton(R.string.ChatWallpaperFragment__reset, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda23
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChatWallpaperFragment.$r8$lambda$V1zYWnkxzvQZ130iULbHy3fKJkY(ChatWallpaperFragment.this, dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda24
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChatWallpaperFragment.$r8$lambda$he2PEnOustoTtwlY2j4r2CQhcGA(dialogInterface, i);
            }
        }).show();
    }

    public /* synthetic */ void lambda$onViewCreated$19(DialogInterface dialogInterface, int i) {
        this.viewModel.clearChatColor();
    }

    public static /* synthetic */ void lambda$onViewCreated$22(ImageView imageView, ColorizerView colorizerView, TextView textView, View view, ChatColors chatColors) {
        imageView.getDrawable().setColorFilter(chatColors.getChatBubbleColorFilter());
        colorizerView.setBackground(chatColors.getChatBubbleMask());
        colorizerView.setProjections(Collections.singletonList(Projection.relativeToViewWithCommonRoot(imageView, colorizerView, new Projection.Corners((float) ViewUtil.dpToPx(10)))));
        Drawable asCircle = chatColors.asCircle();
        asCircle.setBounds(0, 0, ViewUtil.dpToPx(16), ViewUtil.dpToPx(16));
        TextViewCompat.setCompoundDrawablesRelative(textView, null, null, asCircle, null);
        view.getBackground().setColorFilter(chatColors.asSingleColor(), PorterDuff.Mode.MULTIPLY);
        view.getBackground().invalidateSelf();
    }

    public /* synthetic */ void lambda$onViewCreated$23(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        this.viewModel.refreshChatColors();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewStateRestored(Bundle bundle) {
        super.onViewStateRestored(bundle);
        this.dimInNightMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperFragment$$ExternalSyntheticLambda0
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                ChatWallpaperFragment.$r8$lambda$pGA5qKWLqu5t0G7SvYwAlE2Aam8(ChatWallpaperFragment.this, compoundButton, z);
            }
        });
    }

    public /* synthetic */ void lambda$onViewStateRestored$24(CompoundButton compoundButton, boolean z) {
        if (!this.isSettingDimFromViewModel) {
            this.viewModel.setDimInDarkTheme(z);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        this.viewModel.refreshChatColors();
    }

    private void forceAspectRatioToScreenByAdjustingHeight(View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        requireActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        DisplayMetricsUtil.forceAspectRatioToScreenByAdjustingHeight(displayMetrics, view);
    }
}
