package org.thoughtcrime.securesms.wallpaper;

import android.net.Uri;
import org.thoughtcrime.securesms.database.model.databaseprotos.Wallpaper;

/* loaded from: classes5.dex */
public final class ChatWallpaperFactory {
    private ChatWallpaperFactory() {
    }

    public static ChatWallpaper create(Wallpaper wallpaper) {
        if (wallpaper.hasSingleColor()) {
            return buildForSingleColor(wallpaper.getSingleColor(), wallpaper.getDimLevelInDarkTheme());
        }
        if (wallpaper.hasLinearGradient()) {
            return buildForLinearGradinent(wallpaper.getLinearGradient(), wallpaper.getDimLevelInDarkTheme());
        }
        if (wallpaper.hasFile()) {
            return buildForFile(wallpaper.getFile(), wallpaper.getDimLevelInDarkTheme());
        }
        throw new IllegalArgumentException();
    }

    public static ChatWallpaper updateWithDimming(ChatWallpaper chatWallpaper, float f) {
        return create(chatWallpaper.serialize().toBuilder().setDimLevelInDarkTheme(f).build());
    }

    public static ChatWallpaper create(Uri uri) {
        return new UriChatWallpaper(uri, 0.0f);
    }

    private static ChatWallpaper buildForSingleColor(Wallpaper.SingleColor singleColor, float f) {
        return new SingleColorChatWallpaper(singleColor.getColor(), f);
    }

    private static ChatWallpaper buildForLinearGradinent(Wallpaper.LinearGradient linearGradient, float f) {
        int colorsCount = linearGradient.getColorsCount();
        int[] iArr = new int[colorsCount];
        for (int i = 0; i < colorsCount; i++) {
            iArr[i] = linearGradient.getColors(i);
        }
        int positionsCount = linearGradient.getPositionsCount();
        float[] fArr = new float[positionsCount];
        for (int i2 = 0; i2 < positionsCount; i2++) {
            fArr[i2] = linearGradient.getPositions(i2);
        }
        return new GradientChatWallpaper(linearGradient.getRotation(), iArr, fArr, f);
    }

    private static ChatWallpaper buildForFile(Wallpaper.File file, float f) {
        return new UriChatWallpaper(Uri.parse(file.getUri()), f);
    }
}
