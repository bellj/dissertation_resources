package org.thoughtcrime.securesms.wallpaper;

import com.annimon.stream.function.Function;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes5.dex */
public final /* synthetic */ class ChatWallpaperPreviewActivity$$ExternalSyntheticLambda1 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return new ChatWallpaperSelectionMappingModel((ChatWallpaper) obj);
    }
}
