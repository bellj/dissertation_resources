package org.thoughtcrime.securesms.wallpaper;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes5.dex */
public final /* synthetic */ class ChatWallpaperViewModel$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ ChatWallpaperViewModel f$0;

    public /* synthetic */ ChatWallpaperViewModel$$ExternalSyntheticLambda0(ChatWallpaperViewModel chatWallpaperViewModel) {
        this.f$0 = chatWallpaperViewModel;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.refreshChatColors();
    }
}
