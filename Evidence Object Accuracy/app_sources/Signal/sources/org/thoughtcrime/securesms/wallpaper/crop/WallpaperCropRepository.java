package org.thoughtcrime.securesms.wallpaper.crop;

import android.content.Context;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;
import org.thoughtcrime.securesms.wallpaper.WallpaperStorage;

/* loaded from: classes5.dex */
public final class WallpaperCropRepository {
    private static final String TAG = Log.tag(WallpaperCropRepository.class);
    private final Context context = ApplicationDependencies.getApplication();
    private final RecipientId recipientId;

    public WallpaperCropRepository(RecipientId recipientId) {
        this.recipientId = recipientId;
    }

    public ChatWallpaper setWallPaper(byte[] bArr) throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        try {
            ChatWallpaper save = WallpaperStorage.save(this.context, byteArrayInputStream, "webp");
            if (this.recipientId != null) {
                String str = TAG;
                Log.i(str, "Setting image wallpaper for " + this.recipientId);
                SignalDatabase.recipients().setWallpaper(this.recipientId, save);
            } else {
                Log.i(TAG, "Setting image wallpaper for default");
                SignalStore.wallpaper().setWallpaper(this.context, save);
            }
            byteArrayInputStream.close();
            return save;
        } catch (Throwable th) {
            try {
                byteArrayInputStream.close();
            } catch (Throwable th2) {
                th.addSuppressed(th2);
            }
            throw th;
        }
    }
}
