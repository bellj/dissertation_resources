package org.thoughtcrime.securesms.wallpaper.crop;

import android.content.Context;
import android.graphics.Point;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import java.io.IOException;
import java.util.Objects;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.signal.imageeditor.core.model.EditorModel;
import org.thoughtcrime.securesms.fonts.FontTypefaceProvider;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.AsynchronousCallback;
import org.thoughtcrime.securesms.util.BitmapUtil;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;

/* access modifiers changed from: package-private */
/* loaded from: classes5.dex */
public final class WallpaperCropViewModel extends ViewModel {
    private static final String TAG = Log.tag(WallpaperCropViewModel.class);
    private final MutableLiveData<Boolean> blur = new MutableLiveData<>(Boolean.FALSE);
    private final LiveData<Recipient> recipient;
    private final WallpaperCropRepository repository;

    /* loaded from: classes5.dex */
    public enum Error {
        SAVING
    }

    public WallpaperCropViewModel(RecipientId recipientId, WallpaperCropRepository wallpaperCropRepository) {
        this.repository = wallpaperCropRepository;
        this.recipient = recipientId != null ? Recipient.live(recipientId).getLiveData() : LiveDataUtil.just(Recipient.UNKNOWN);
    }

    public void render(Context context, EditorModel editorModel, Point point, AsynchronousCallback.WorkerThread<ChatWallpaper, Error> workerThread) {
        SignalExecutors.BOUNDED.execute(new Runnable(editorModel, context, point, workerThread) { // from class: org.thoughtcrime.securesms.wallpaper.crop.WallpaperCropViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ EditorModel f$1;
            public final /* synthetic */ Context f$2;
            public final /* synthetic */ Point f$3;
            public final /* synthetic */ AsynchronousCallback.WorkerThread f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // java.lang.Runnable
            public final void run() {
                WallpaperCropViewModel.this.lambda$render$0(this.f$1, this.f$2, this.f$3, this.f$4);
            }
        });
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:11:0x0000 */
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: org.signal.imageeditor.core.model.EditorModel */
    /* JADX DEBUG: Multi-variable search result rejected for r2v1, resolved type: android.graphics.Bitmap */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v2, types: [android.graphics.Bitmap] */
    public /* synthetic */ void lambda$render$0(EditorModel editorModel, Context context, Point point, AsynchronousCallback.WorkerThread workerThread) {
        try {
            editorModel = editorModel.render(context, point, new FontTypefaceProvider());
            try {
                workerThread.onComplete(this.repository.setWallPaper(BitmapUtil.toWebPByteArray(editorModel)));
            } catch (IOException e) {
                Log.w(TAG, e);
                workerThread.onError(Error.SAVING);
            }
        } finally {
            editorModel.recycle();
        }
    }

    public LiveData<Boolean> getBlur() {
        return Transformations.distinctUntilChanged(this.blur);
    }

    public LiveData<Recipient> getRecipient() {
        return this.recipient;
    }

    public void setBlur(boolean z) {
        this.blur.setValue(Boolean.valueOf(z));
    }

    /* loaded from: classes5.dex */
    public static class Factory implements ViewModelProvider.Factory {
        private final RecipientId recipientId;

        public Factory(RecipientId recipientId) {
            this.recipientId = recipientId;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            T cast = cls.cast(new WallpaperCropViewModel(this.recipientId, new WallpaperCropRepository(this.recipientId)));
            Objects.requireNonNull(cast);
            return cast;
        }
    }
}
