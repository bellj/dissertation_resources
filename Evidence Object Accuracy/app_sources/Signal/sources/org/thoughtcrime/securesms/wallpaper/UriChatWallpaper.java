package org.thoughtcrime.securesms.wallpaper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.LruCache;
import android.widget.ImageView;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.database.model.databaseprotos.Wallpaper;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;

/* loaded from: classes5.dex */
public final class UriChatWallpaper implements ChatWallpaper, Parcelable {
    private static final LruCache<Uri, Bitmap> CACHE = new LruCache<Uri, Bitmap>(((int) Runtime.getRuntime().maxMemory()) / 8) { // from class: org.thoughtcrime.securesms.wallpaper.UriChatWallpaper.1
        public int sizeOf(Uri uri, Bitmap bitmap) {
            return bitmap.getByteCount();
        }
    };
    public static final Parcelable.Creator<UriChatWallpaper> CREATOR = new Parcelable.Creator<UriChatWallpaper>() { // from class: org.thoughtcrime.securesms.wallpaper.UriChatWallpaper.3
        @Override // android.os.Parcelable.Creator
        public UriChatWallpaper createFromParcel(Parcel parcel) {
            return new UriChatWallpaper(Uri.parse(parcel.readString()), parcel.readFloat());
        }

        @Override // android.os.Parcelable.Creator
        public UriChatWallpaper[] newArray(int i) {
            return new UriChatWallpaper[i];
        }
    };
    private static final String TAG = Log.tag(UriChatWallpaper.class);
    private final float dimLevelInDarkTheme;
    private final Uri uri;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaper
    public /* synthetic */ ChatColors getAutoChatColors() {
        return ChatWallpaper.CC.$default$getAutoChatColors(this);
    }

    @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaper
    public boolean isPhoto() {
        return true;
    }

    public UriChatWallpaper(Uri uri, float f) {
        this.uri = uri;
        this.dimLevelInDarkTheme = f;
    }

    @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaper
    public float getDimLevelForDarkTheme() {
        return this.dimLevelInDarkTheme;
    }

    @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaper
    public void loadInto(ImageView imageView) {
        LruCache<Uri, Bitmap> lruCache = CACHE;
        if (lruCache.get(this.uri) != null) {
            Log.d(TAG, "Using cached value.");
            imageView.setImageBitmap(lruCache.get(this.uri));
            return;
        }
        Log.d(TAG, "Not in cache. Fetching using Glide.");
        GlideApp.with(imageView).load((Object) new DecryptableStreamUriLoader.DecryptableUri(this.uri)).addListener((RequestListener<Drawable>) new RequestListener<Drawable>() { // from class: org.thoughtcrime.securesms.wallpaper.UriChatWallpaper.2
            @Override // com.bumptech.glide.request.RequestListener
            public /* bridge */ /* synthetic */ boolean onResourceReady(Object obj, Object obj2, Target target, DataSource dataSource, boolean z) {
                return onResourceReady((Drawable) obj, obj2, (Target<Drawable>) target, dataSource, z);
            }

            @Override // com.bumptech.glide.request.RequestListener
            public boolean onLoadFailed(GlideException glideException, Object obj, Target<Drawable> target, boolean z) {
                String str = UriChatWallpaper.TAG;
                Log.w(str, "Failed to load wallpaper " + UriChatWallpaper.this.uri);
                return false;
            }

            public boolean onResourceReady(Drawable drawable, Object obj, Target<Drawable> target, DataSource dataSource, boolean z) {
                String str = UriChatWallpaper.TAG;
                Log.i(str, "Loaded wallpaper " + UriChatWallpaper.this.uri);
                return false;
            }
        }).into(imageView);
    }

    @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaper
    public boolean prefetch(Context context, long j) {
        Throwable e;
        LruCache<Uri, Bitmap> lruCache = CACHE;
        if (lruCache.get(this.uri) != null) {
            Log.d(TAG, "Already cached, skipping prefetch.");
            return true;
        }
        long currentTimeMillis = System.currentTimeMillis();
        try {
            lruCache.put(this.uri, GlideApp.with(context).asBitmap().load((Object) new DecryptableStreamUriLoader.DecryptableUri(this.uri)).submit().get(j, TimeUnit.MILLISECONDS));
            String str = TAG;
            Log.d(str, "Prefetched wallpaper in " + (System.currentTimeMillis() - currentTimeMillis) + " ms.");
            return true;
        } catch (InterruptedException e2) {
            e = e2;
            Log.w(TAG, "Failed to prefetch wallpaper.", e);
            return false;
        } catch (ExecutionException e3) {
            e = e3;
            Log.w(TAG, "Failed to prefetch wallpaper.", e);
            return false;
        } catch (TimeoutException unused) {
            Log.w(TAG, "Timed out waiting for prefetch.");
            return false;
        }
    }

    @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaper
    public Wallpaper serialize() {
        return Wallpaper.newBuilder().setFile(Wallpaper.File.newBuilder().setUri(this.uri.toString())).setDimLevelInDarkTheme(this.dimLevelInDarkTheme).build();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.uri.toString());
        parcel.writeFloat(this.dimLevelInDarkTheme);
    }

    @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaper
    public boolean isSameSource(ChatWallpaper chatWallpaper) {
        if (this == chatWallpaper) {
            return true;
        }
        if (UriChatWallpaper.class != chatWallpaper.getClass()) {
            return false;
        }
        return this.uri.equals(((UriChatWallpaper) chatWallpaper).uri);
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || UriChatWallpaper.class != obj.getClass()) {
            return false;
        }
        UriChatWallpaper uriChatWallpaper = (UriChatWallpaper) obj;
        if (Float.compare(uriChatWallpaper.dimLevelInDarkTheme, this.dimLevelInDarkTheme) != 0 || !this.uri.equals(uriChatWallpaper.uri)) {
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Objects.hash(this.uri, Float.valueOf(this.dimLevelInDarkTheme));
    }
}
