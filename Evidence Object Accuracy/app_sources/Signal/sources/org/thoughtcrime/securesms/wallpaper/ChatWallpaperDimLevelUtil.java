package org.thoughtcrime.securesms.wallpaper;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.view.View;
import com.airbnb.lottie.SimpleColorFilter;
import org.thoughtcrime.securesms.util.ThemeUtil;

/* loaded from: classes5.dex */
public final class ChatWallpaperDimLevelUtil {
    private ChatWallpaperDimLevelUtil() {
    }

    public static void applyDimLevelForNightMode(View view, ChatWallpaper chatWallpaper) {
        if (ThemeUtil.isDarkTheme(view.getContext())) {
            view.setAlpha(chatWallpaper.getDimLevelForDarkTheme());
            view.setVisibility(0);
            return;
        }
        view.setVisibility(8);
    }

    public static ColorFilter getDimColorFilterForNightMode(Context context, ChatWallpaper chatWallpaper) {
        if (ThemeUtil.isDarkTheme(context)) {
            return new SimpleColorFilter(Color.argb(Math.round(chatWallpaper.getDimLevelForDarkTheme() * 255.0f), 0, 0, 0));
        }
        return null;
    }
}
