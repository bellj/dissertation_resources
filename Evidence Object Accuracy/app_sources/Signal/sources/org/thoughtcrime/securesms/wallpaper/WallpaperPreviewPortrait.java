package org.thoughtcrime.securesms.wallpaper;

import android.graphics.ColorFilter;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: WallpaperPreviewPortrait.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u0007\b\u0004¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&\u0001\u0002\t\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/wallpaper/WallpaperPreviewPortrait;", "", "()V", "applyToAvatarImageView", "", "avatarImageView", "Lorg/thoughtcrime/securesms/components/AvatarImageView;", "ContactPhoto", "SolidColor", "Lorg/thoughtcrime/securesms/wallpaper/WallpaperPreviewPortrait$ContactPhoto;", "Lorg/thoughtcrime/securesms/wallpaper/WallpaperPreviewPortrait$SolidColor;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes5.dex */
public abstract class WallpaperPreviewPortrait {
    public /* synthetic */ WallpaperPreviewPortrait(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    public abstract void applyToAvatarImageView(AvatarImageView avatarImageView);

    /* compiled from: WallpaperPreviewPortrait.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/wallpaper/WallpaperPreviewPortrait$ContactPhoto;", "Lorg/thoughtcrime/securesms/wallpaper/WallpaperPreviewPortrait;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "(Lorg/thoughtcrime/securesms/recipients/Recipient;)V", "applyToAvatarImageView", "", "avatarImageView", "Lorg/thoughtcrime/securesms/components/AvatarImageView;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public static final class ContactPhoto extends WallpaperPreviewPortrait {
        private final Recipient recipient;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ContactPhoto(Recipient recipient) {
            super(null);
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            this.recipient = recipient;
        }

        @Override // org.thoughtcrime.securesms.wallpaper.WallpaperPreviewPortrait
        public void applyToAvatarImageView(AvatarImageView avatarImageView) {
            Intrinsics.checkNotNullParameter(avatarImageView, "avatarImageView");
            avatarImageView.setAvatar(this.recipient);
            avatarImageView.setColorFilter((ColorFilter) null);
        }
    }

    private WallpaperPreviewPortrait() {
    }

    /* compiled from: WallpaperPreviewPortrait.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/wallpaper/WallpaperPreviewPortrait$SolidColor;", "Lorg/thoughtcrime/securesms/wallpaper/WallpaperPreviewPortrait;", "avatarColor", "Lorg/thoughtcrime/securesms/conversation/colors/AvatarColor;", "(Lorg/thoughtcrime/securesms/conversation/colors/AvatarColor;)V", "applyToAvatarImageView", "", "avatarImageView", "Lorg/thoughtcrime/securesms/components/AvatarImageView;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public static final class SolidColor extends WallpaperPreviewPortrait {
        private final AvatarColor avatarColor;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public SolidColor(AvatarColor avatarColor) {
            super(null);
            Intrinsics.checkNotNullParameter(avatarColor, "avatarColor");
            this.avatarColor = avatarColor;
        }

        @Override // org.thoughtcrime.securesms.wallpaper.WallpaperPreviewPortrait
        public void applyToAvatarImageView(AvatarImageView avatarImageView) {
            Intrinsics.checkNotNullParameter(avatarImageView, "avatarImageView");
            avatarImageView.setImageResource(R.drawable.circle_tintable);
            avatarImageView.setColorFilter(this.avatarColor.colorInt());
        }
    }
}
