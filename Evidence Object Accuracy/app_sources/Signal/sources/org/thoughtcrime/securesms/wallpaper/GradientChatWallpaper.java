package org.thoughtcrime.securesms.wallpaper;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;
import java.util.Arrays;
import java.util.Objects;
import org.thoughtcrime.securesms.components.RotatableGradientDrawable;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.database.model.databaseprotos.Wallpaper;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;

/* loaded from: classes5.dex */
public final class GradientChatWallpaper implements ChatWallpaper, Parcelable {
    public static final ChatWallpaper AQUA = new GradientChatWallpaper(180.0f, new int[]{-16739351, -16608023, -16345369, -15885851, -15294749, -14572576, -13784867, -12931366, -12143146, -11289645, -10501936, -9779763, -9188661, -8729143, -8466489, -8335161}, new float[]{0.0f, 0.0807f, 0.1554f, 0.225f, 0.2904f, 0.3526f, 0.4125f, 0.471f, 0.529f, 0.5875f, 0.6474f, 0.7096f, 0.775f, 0.8446f, 0.9193f, 1.0f}, 0.0f);
    public static final ChatWallpaper BLISS = new GradientChatWallpaper(180.0f, new int[]{-2563590, -2563847, -2564361, -2565133, -2566418, -2633240, -2634526, -2636069, -2637868, -2639411, -2706233, -2707519, -2708804, -2709576, -2710090, -2710347}, new float[]{0.0f, 0.0807f, 0.1554f, 0.225f, 0.2904f, 0.3526f, 0.4125f, 0.471f, 0.529f, 0.5875f, 0.6474f, 0.7096f, 0.775f, 0.8446f, 0.9193f, 1.0f}, 0.0f);
    public static final Parcelable.Creator<GradientChatWallpaper> CREATOR = new Parcelable.Creator<GradientChatWallpaper>() { // from class: org.thoughtcrime.securesms.wallpaper.GradientChatWallpaper.1
        @Override // android.os.Parcelable.Creator
        public GradientChatWallpaper createFromParcel(Parcel parcel) {
            return new GradientChatWallpaper(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public GradientChatWallpaper[] newArray(int i) {
            return new GradientChatWallpaper[i];
        }
    };
    public static final ChatWallpaper HEATMAP = new GradientChatWallpaper(192.0f, new int[]{-706492, -837563, -1296312, -1951668, -2738095, -3721129, -4835234, -6014874, -7129235, -8308875, -9422980, -10406014, -11192441, -11847797, -12306546, -12437617}, new float[]{0.0f, 0.0075f, 0.0292f, 0.0637f, 0.1097f, 0.1659f, 0.231f, 0.3037f, 0.3827f, 0.4666f, 0.5541f, 0.6439f, 0.7347f, 0.8252f, 0.9141f, 1.0f}, 0.0f);
    public static final ChatWallpaper IRIDESCENT = new GradientChatWallpaper(192.0f, new int[]{-1028890, -1160218, -1750299, -2536987, -3586332, -4832029, -6209053, -7651358, -9159455, -10602016, -11979041, -13224738, -14273827, -15126307, -15650851, -15847459}, new float[]{0.0f, 0.0807f, 0.1554f, 0.225f, 0.2904f, 0.3526f, 0.4125f, 0.471f, 0.529f, 0.5875f, 0.6474f, 0.7096f, 0.775f, 0.8446f, 0.9193f, 1.0f}, 0.0f);
    public static final ChatWallpaper MONSTERA = new GradientChatWallpaper(180.0f, new int[]{-10105428, -10170965, -10433624, -10762077, -11156323, -11681642, -12207217, -12798329, -13389441, -13980554, -14571665, -15031704, -15491230, -15819682, -16016805, -16082598}, new float[]{0.0f, 0.0807f, 0.1554f, 0.225f, 0.2904f, 0.3526f, 0.4125f, 0.471f, 0.529f, 0.5875f, 0.6474f, 0.7096f, 0.775f, 0.8446f, 0.9193f, 1.0f}, 0.0f);
    public static final ChatWallpaper NOIR = new GradientChatWallpaper(180.0f, new int[]{-15329763, -15263970, -15066590, -14737624, -14277073, -13816520, -13290174, -12698036, -12105897, -11579551, -11053205, -10526860, -10132100, -9803134, -9605755, -9539961}, new float[]{0.0f, 0.0807f, 0.1554f, 0.225f, 0.2904f, 0.3526f, 0.4125f, 0.471f, 0.529f, 0.5875f, 0.6474f, 0.7096f, 0.775f, 0.8446f, 0.9193f, 1.0f}, 0.0f);
    public static final ChatWallpaper PEACH = new GradientChatWallpaper(192.0f, new int[]{-6718, -6975, -7489, -8259, -74823, -76107, -77391, -144212, -145752, -147293, -214113, -215397, -216425, -217195, -217709, -217966}, new float[]{0.0f, 0.0807f, 0.1554f, 0.225f, 0.2904f, 0.3526f, 0.4125f, 0.471f, 0.529f, 0.5875f, 0.6474f, 0.7096f, 0.775f, 0.8446f, 0.9193f, 1.0f}, 0.0f);
    public static final ChatWallpaper SKY = new GradientChatWallpaper(180.0f, new int[]{-2561027, -2626819, -2758147, -2955267, -3283459, -3611651, -3940099, -4334084, -4728068, -5056516, -5450500, -5778948, -6041605, -6238725, -6370053, -6435589}, new float[]{0.0f, 0.0807f, 0.1554f, 0.225f, 0.2904f, 0.3526f, 0.4125f, 0.471f, 0.529f, 0.5875f, 0.6474f, 0.7096f, 0.775f, 0.8446f, 0.9193f, 1.0f}, 0.0f);
    public static final ChatWallpaper SUNSET = new GradientChatWallpaper(168.0f, new int[]{-795577, -796089, -862906, -865210, -933562, -1067963, -1137340, -1272252, -1341629, -1476797, -1545918, -1680319, -1748927, -1750975, -1818048, -1818560}, new float[]{0.0f, 0.0807f, 0.1554f, 0.225f, 0.2904f, 0.3526f, 0.4125f, 0.471f, 0.529f, 0.5875f, 0.6474f, 0.7096f, 0.775f, 0.8446f, 0.9193f, 1.0f}, 0.0f);
    private final int[] colors;
    private final float degrees;
    private final float dimLevelInDarkTheme;
    private final float[] positions;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaper
    public /* synthetic */ ChatColors getAutoChatColors() {
        return ChatWallpaper.CC.$default$getAutoChatColors(this);
    }

    @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaper
    public /* synthetic */ boolean isPhoto() {
        return ChatWallpaper.CC.$default$isPhoto(this);
    }

    @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaper
    public /* synthetic */ boolean prefetch(Context context, long j) {
        return ChatWallpaper.CC.$default$prefetch(this, context, j);
    }

    public GradientChatWallpaper(float f, int[] iArr, float[] fArr, float f2) {
        this.degrees = f;
        this.colors = iArr;
        this.positions = fArr;
        this.dimLevelInDarkTheme = f2;
    }

    private GradientChatWallpaper(Parcel parcel) {
        this.degrees = parcel.readFloat();
        this.colors = parcel.createIntArray();
        this.positions = parcel.createFloatArray();
        this.dimLevelInDarkTheme = parcel.readFloat();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(this.degrees);
        parcel.writeIntArray(this.colors);
        parcel.writeFloatArray(this.positions);
        parcel.writeFloat(this.dimLevelInDarkTheme);
    }

    private Drawable buildDrawable() {
        return new RotatableGradientDrawable(this.degrees, this.colors, this.positions);
    }

    @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaper
    public float getDimLevelForDarkTheme() {
        return this.dimLevelInDarkTheme;
    }

    @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaper
    public void loadInto(ImageView imageView) {
        imageView.setImageDrawable(buildDrawable());
    }

    @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaper
    public boolean isSameSource(ChatWallpaper chatWallpaper) {
        if (this == chatWallpaper) {
            return true;
        }
        if (GradientChatWallpaper.class != chatWallpaper.getClass()) {
            return false;
        }
        GradientChatWallpaper gradientChatWallpaper = (GradientChatWallpaper) chatWallpaper;
        if (Float.compare(gradientChatWallpaper.degrees, this.degrees) != 0 || !Arrays.equals(this.colors, gradientChatWallpaper.colors) || !Arrays.equals(this.positions, gradientChatWallpaper.positions)) {
            return false;
        }
        return true;
    }

    @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaper
    public Wallpaper serialize() {
        Wallpaper.LinearGradient.Builder newBuilder = Wallpaper.LinearGradient.newBuilder();
        newBuilder.setRotation(this.degrees);
        for (int i : this.colors) {
            newBuilder.addColors(i);
        }
        for (float f : this.positions) {
            newBuilder.addPositions(f);
        }
        return Wallpaper.newBuilder().setLinearGradient(newBuilder).setDimLevelInDarkTheme(this.dimLevelInDarkTheme).build();
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || GradientChatWallpaper.class != obj.getClass()) {
            return false;
        }
        GradientChatWallpaper gradientChatWallpaper = (GradientChatWallpaper) obj;
        if (Float.compare(gradientChatWallpaper.degrees, this.degrees) != 0 || !Arrays.equals(this.colors, gradientChatWallpaper.colors) || !Arrays.equals(this.positions, gradientChatWallpaper.positions) || Float.compare(gradientChatWallpaper.dimLevelInDarkTheme, this.dimLevelInDarkTheme) != 0) {
            return false;
        }
        return true;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return (((Objects.hash(Float.valueOf(this.degrees), Float.valueOf(this.dimLevelInDarkTheme)) * 31) + Arrays.hashCode(this.colors)) * 31) + Arrays.hashCode(this.positions);
    }
}
