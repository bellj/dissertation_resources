package org.thoughtcrime.securesms.wallpaper;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.Stream;
import j$.util.Optional;
import j$.util.function.Function;
import java.util.List;
import java.util.Objects;
import org.thoughtcrime.securesms.blocked.BlockedUsersViewModel$$ExternalSyntheticLambda2;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.LiveRecipient;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientForeverObserver;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.DefaultValueLiveData;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.thoughtcrime.securesms.wallpaper.WallpaperPreviewPortrait;

/* loaded from: classes5.dex */
public class ChatWallpaperViewModel extends ViewModel {
    private final MutableLiveData<List<ChatWallpaper>> builtins;
    private final MutableLiveData<ChatColors> chatColors;
    private final MutableLiveData<Boolean> dimInDarkTheme;
    private final MutableLiveData<Boolean> enableWallpaperControls;
    private final LiveRecipient liveRecipient;
    private final RecipientId recipientId;
    private final RecipientForeverObserver recipientObserver;
    private final ChatWallpaperRepository repository;
    private final MutableLiveData<Optional<ChatWallpaper>> wallpaper;
    private final LiveData<WallpaperPreviewPortrait> wallpaperPreviewPortrait;

    public /* synthetic */ void lambda$new$0(Recipient recipient) {
        refreshChatColors();
    }

    private ChatWallpaperViewModel(RecipientId recipientId) {
        ChatWallpaperRepository chatWallpaperRepository = new ChatWallpaperRepository();
        this.repository = chatWallpaperRepository;
        MutableLiveData<Optional<ChatWallpaper>> mutableLiveData = new MutableLiveData<>();
        this.wallpaper = mutableLiveData;
        this.builtins = new MutableLiveData<>();
        MutableLiveData<Boolean> mutableLiveData2 = new MutableLiveData<>();
        this.dimInDarkTheme = mutableLiveData2;
        MutableLiveData<Boolean> mutableLiveData3 = new MutableLiveData<>();
        this.enableWallpaperControls = mutableLiveData3;
        this.chatColors = new MutableLiveData<>();
        ChatWallpaperViewModel$$ExternalSyntheticLambda2 chatWallpaperViewModel$$ExternalSyntheticLambda2 = new RecipientForeverObserver() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperViewModel$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.recipients.RecipientForeverObserver
            public final void onRecipientChanged(Recipient recipient) {
                ChatWallpaperViewModel.this.lambda$new$0(recipient);
            }
        };
        this.recipientObserver = chatWallpaperViewModel$$ExternalSyntheticLambda2;
        this.recipientId = recipientId;
        ChatWallpaper currentWallpaper = chatWallpaperRepository.getCurrentWallpaper(recipientId);
        mutableLiveData2.setValue(Boolean.valueOf(currentWallpaper == null || currentWallpaper.getDimLevelForDarkTheme() > 0.0f));
        mutableLiveData3.setValue(Boolean.valueOf(hasClearableWallpaper()));
        mutableLiveData.setValue(Optional.ofNullable(currentWallpaper));
        if (recipientId != null) {
            LiveRecipient live = Recipient.live(recipientId);
            this.liveRecipient = live;
            live.observeForever(chatWallpaperViewModel$$ExternalSyntheticLambda2);
            this.wallpaperPreviewPortrait = Transformations.map(live.getLiveData(), new Function() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperViewModel$$ExternalSyntheticLambda3
                @Override // androidx.arch.core.util.Function
                public final Object apply(Object obj) {
                    return ChatWallpaperViewModel.lambda$new$1((Recipient) obj);
                }
            });
            return;
        }
        this.liveRecipient = null;
        this.wallpaperPreviewPortrait = new DefaultValueLiveData(new WallpaperPreviewPortrait.SolidColor(AvatarColor.A100));
    }

    public static /* synthetic */ WallpaperPreviewPortrait lambda$new$1(Recipient recipient) {
        if (recipient.getContactPhoto() != null) {
            return new WallpaperPreviewPortrait.ContactPhoto(recipient);
        }
        return new WallpaperPreviewPortrait.SolidColor(recipient.getAvatarColor());
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        LiveRecipient liveRecipient = this.liveRecipient;
        if (liveRecipient != null) {
            liveRecipient.lambda$asObservable$6(this.recipientObserver);
        }
    }

    public void refreshWallpaper() {
        ChatWallpaperRepository chatWallpaperRepository = this.repository;
        MutableLiveData<List<ChatWallpaper>> mutableLiveData = this.builtins;
        Objects.requireNonNull(mutableLiveData);
        chatWallpaperRepository.getAllWallpaper(new BlockedUsersViewModel$$ExternalSyntheticLambda2(mutableLiveData));
    }

    public void refreshChatColors() {
        this.chatColors.postValue(this.repository.getCurrentChatColors(this.recipientId));
    }

    public void setDimInDarkTheme(boolean z) {
        this.dimInDarkTheme.setValue(Boolean.valueOf(z));
        if (this.wallpaper.getValue().isPresent()) {
            this.repository.setDimInDarkTheme(this.recipientId, z);
        }
    }

    public void setWallpaper(ChatWallpaper chatWallpaper) {
        this.wallpaper.setValue(Optional.ofNullable(chatWallpaper));
    }

    public void saveWallpaperSelection() {
        Optional<ChatWallpaper> value = this.wallpaper.getValue();
        boolean booleanValue = this.dimInDarkTheme.getValue().booleanValue();
        if (!value.isPresent()) {
            this.repository.saveWallpaper(this.recipientId, null, new ChatWallpaperViewModel$$ExternalSyntheticLambda0(this));
            if (this.recipientId != null) {
                ChatWallpaper wallpaper = SignalStore.wallpaper().getWallpaper();
                this.wallpaper.setValue(Optional.ofNullable(wallpaper));
                this.dimInDarkTheme.setValue(Boolean.valueOf(wallpaper == null || wallpaper.getDimLevelForDarkTheme() > 0.0f));
            }
            this.enableWallpaperControls.setValue(Boolean.FALSE);
            return;
        }
        this.enableWallpaperControls.setValue(Boolean.TRUE);
        Optional<U> map = value.map(new j$.util.function.Function(booleanValue) { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperViewModel$$ExternalSyntheticLambda1
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ChatWallpaperViewModel.lambda$saveWallpaperSelection$2(this.f$0, (ChatWallpaper) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        if (map.isPresent()) {
            this.repository.saveWallpaper(this.recipientId, (ChatWallpaper) map.get(), new ChatWallpaperViewModel$$ExternalSyntheticLambda0(this));
        }
    }

    public static /* synthetic */ ChatWallpaper lambda$saveWallpaperSelection$2(boolean z, ChatWallpaper chatWallpaper) {
        return ChatWallpaperFactory.updateWithDimming(chatWallpaper, z ? 0.2f : 0.0f);
    }

    public void resetAllWallpaper() {
        this.repository.resetAllWallpaper(new ChatWallpaperViewModel$$ExternalSyntheticLambda0(this));
    }

    public RecipientId getRecipientId() {
        return this.recipientId;
    }

    public LiveData<Optional<ChatWallpaper>> getCurrentWallpaper() {
        return this.wallpaper;
    }

    public LiveData<ChatColors> getCurrentChatColors() {
        return this.chatColors;
    }

    public LiveData<WallpaperPreviewPortrait> getWallpaperPreviewPortrait() {
        return this.wallpaperPreviewPortrait;
    }

    public LiveData<List<MappingModel<?>>> getWallpapers() {
        return LiveDataUtil.combineLatest(this.builtins, this.dimInDarkTheme, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperViewModel$$ExternalSyntheticLambda4
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return ChatWallpaperViewModel.lambda$getWallpapers$4((List) obj, (Boolean) obj2);
            }
        });
    }

    public static /* synthetic */ List lambda$getWallpapers$4(List list, Boolean bool) {
        return Stream.of(list).map(new com.annimon.stream.function.Function(bool) { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperViewModel$$ExternalSyntheticLambda5
            public final /* synthetic */ Boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ChatWallpaperViewModel.lambda$getWallpapers$3(this.f$0, (ChatWallpaper) obj);
            }
        }).map(new ChatWallpaperPreviewActivity$$ExternalSyntheticLambda1()).toList();
    }

    public static /* synthetic */ ChatWallpaper lambda$getWallpapers$3(Boolean bool, ChatWallpaper chatWallpaper) {
        return ChatWallpaperFactory.updateWithDimming(chatWallpaper, bool.booleanValue() ? 0.2f : 0.0f);
    }

    public LiveData<Boolean> getDimInDarkTheme() {
        return this.dimInDarkTheme;
    }

    public LiveData<Boolean> getEnableWallpaperControls() {
        return this.enableWallpaperControls;
    }

    public boolean isGlobal() {
        return this.recipientId == null;
    }

    public void clearChatColor() {
        this.repository.clearChatColor(this.recipientId, new ChatWallpaperViewModel$$ExternalSyntheticLambda0(this));
    }

    private boolean hasClearableWallpaper() {
        RecipientId recipientId;
        return (isGlobal() && SignalStore.wallpaper().hasWallpaperSet()) || ((recipientId = this.recipientId) != null && Recipient.live(recipientId).get().hasOwnWallpaper());
    }

    public void resetAllChatColors() {
        this.repository.resetAllChatColors(new ChatWallpaperViewModel$$ExternalSyntheticLambda0(this));
    }

    /* loaded from: classes5.dex */
    public static class Factory implements ViewModelProvider.Factory {
        private final RecipientId recipientId;

        public Factory(RecipientId recipientId) {
            this.recipientId = recipientId;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            T cast = cls.cast(new ChatWallpaperViewModel(this.recipientId));
            Objects.requireNonNull(cast);
            return cast;
        }
    }
}
