package org.thoughtcrime.securesms.wallpaper;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.NavGraph;
import androidx.navigation.Navigation;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaperViewModel;

/* loaded from: classes5.dex */
public final class ChatWallpaperActivity extends PassphraseRequiredActivity {
    private static final String EXTRA_RECIPIENT_ID;
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();

    public static Intent createIntent(Context context) {
        return createIntent(context, null);
    }

    public static Intent createIntent(Context context, RecipientId recipientId) {
        Intent intent = new Intent(context, ChatWallpaperActivity.class);
        intent.putExtra(EXTRA_RECIPIENT_ID, recipientId);
        return intent;
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        ViewModelProviders.of(this, new ChatWallpaperViewModel.Factory((RecipientId) getIntent().getParcelableExtra(EXTRA_RECIPIENT_ID))).get(ChatWallpaperViewModel.class);
        this.dynamicTheme.onCreate(this);
        setContentView(R.layout.chat_wallpaper_activity);
        if (bundle == null) {
            Bundle extras = getIntent().getExtras();
            NavGraph graph = Navigation.findNavController(this, R.id.nav_host_fragment).getGraph();
            NavController findNavController = Navigation.findNavController(this, R.id.nav_host_fragment);
            if (extras == null) {
                extras = new Bundle();
            }
            findNavController.setGraph(graph, extras);
        }
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }
}
