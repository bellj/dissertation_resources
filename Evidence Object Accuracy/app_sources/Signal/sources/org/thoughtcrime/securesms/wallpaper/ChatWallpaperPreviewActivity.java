package org.thoughtcrime.securesms.wallpaper;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.util.Consumer;
import androidx.viewpager2.widget.ViewPager2;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.conversation.colors.ColorizerView;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.FullscreenHelper;
import org.thoughtcrime.securesms.util.Projection;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.WindowUtil;

/* loaded from: classes5.dex */
public class ChatWallpaperPreviewActivity extends PassphraseRequiredActivity {
    public static final String EXTRA_CHAT_WALLPAPER;
    private static final String EXTRA_DIM_IN_DARK_MODE;
    private static final String EXTRA_RECIPIENT_ID;
    private ChatWallpaperPreviewAdapter adapter;
    private View bubble2;
    private ColorizerView colorizerView;
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();
    private OnPageChanged onPageChanged;
    private ViewPager2 viewPager;

    public static Intent create(Context context, ChatWallpaper chatWallpaper, RecipientId recipientId, boolean z) {
        Intent intent = new Intent(context, ChatWallpaperPreviewActivity.class);
        intent.putExtra(EXTRA_CHAT_WALLPAPER, chatWallpaper);
        intent.putExtra(EXTRA_DIM_IN_DARK_MODE, z);
        intent.putExtra(EXTRA_RECIPIENT_ID, recipientId);
        return intent;
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        String str;
        this.dynamicTheme.onCreate(this);
        setContentView(R.layout.chat_wallpaper_preview_activity);
        this.adapter = new ChatWallpaperPreviewAdapter();
        this.colorizerView = (ColorizerView) findViewById(R.id.colorizer);
        this.bubble2 = findViewById(R.id.preview_bubble_2);
        this.viewPager = (ViewPager2) findViewById(R.id.preview_pager);
        View findViewById = findViewById(R.id.preview_set_wallpaper);
        ChatWallpaperRepository chatWallpaperRepository = new ChatWallpaperRepository();
        ChatWallpaper chatWallpaper = (ChatWallpaper) getIntent().getParcelableExtra(EXTRA_CHAT_WALLPAPER);
        boolean booleanExtra = getIntent().getBooleanExtra(EXTRA_DIM_IN_DARK_MODE, false);
        TextView textView = (TextView) findViewById(R.id.preview_bubble_2_text);
        ((Toolbar) findViewById(R.id.toolbar)).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperPreviewActivity$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ChatWallpaperPreviewActivity.m3311$r8$lambda$ZLyrrSivVcJRG2YpyjMmQfxsho(ChatWallpaperPreviewActivity.this, view);
            }
        });
        this.viewPager.setAdapter(this.adapter);
        this.adapter.submitList(Collections.singletonList(new ChatWallpaperSelectionMappingModel(chatWallpaper)));
        chatWallpaperRepository.getAllWallpaper(new Consumer(booleanExtra) { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperPreviewActivity$$ExternalSyntheticLambda3
            public final /* synthetic */ boolean f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                ChatWallpaperPreviewActivity.m3312$r8$lambda$oLT0l8455NElgpdABijbNmhB0(ChatWallpaperPreviewActivity.this, this.f$1, (List) obj);
            }
        });
        findViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperPreviewActivity$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ChatWallpaperPreviewActivity.$r8$lambda$fCqq5TAemf4TkrZ1Pk08Bc3VKHc(ChatWallpaperPreviewActivity.this, view);
            }
        });
        RecipientId recipientId = (RecipientId) getIntent().getParcelableExtra(EXTRA_RECIPIENT_ID);
        if (recipientId != null && Recipient.live(recipientId).get().hasOwnChatColors()) {
            Recipient recipient = Recipient.live(recipientId).get();
            Object[] objArr = new Object[1];
            if (recipient.isSelf()) {
                str = getString(R.string.note_to_self);
            } else {
                str = recipient.getDisplayName(this);
            }
            objArr[0] = str;
            textView.setText(getString(R.string.ChatWallpaperPreviewActivity__set_wallpaper_for_s, objArr));
            this.bubble2.addOnLayoutChangeListener(new View.OnLayoutChangeListener(recipient.getChatColors()) { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperPreviewActivity$$ExternalSyntheticLambda5
                public final /* synthetic */ ChatColors f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnLayoutChangeListener
                public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                    ChatWallpaperPreviewActivity.$r8$lambda$o5KGy3z6E1WK9EdSmDwSod5apPY(ChatWallpaperPreviewActivity.this, this.f$1, view, i, i2, i3, i4, i5, i6, i7, i8);
                }
            });
        } else if (SignalStore.chatColorsValues().hasChatColors()) {
            ChatColors chatColors = SignalStore.chatColorsValues().getChatColors();
            Objects.requireNonNull(chatColors);
            this.bubble2.addOnLayoutChangeListener(new View.OnLayoutChangeListener(chatColors) { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperPreviewActivity$$ExternalSyntheticLambda6
                public final /* synthetic */ ChatColors f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnLayoutChangeListener
                public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                    ChatWallpaperPreviewActivity.m3310$r8$lambda$HyajwZWLrSzAhTBXbQ4GDbRNmk(ChatWallpaperPreviewActivity.this, this.f$1, view, i, i2, i3, i4, i5, i6, i7, i8);
                }
            });
        } else {
            OnPageChanged onPageChanged = new OnPageChanged();
            this.onPageChanged = onPageChanged;
            this.viewPager.registerOnPageChangeCallback(onPageChanged);
            this.bubble2.addOnLayoutChangeListener(new UpdateChatColorsOnNextLayoutChange(chatWallpaper.getAutoChatColors()));
        }
        new FullscreenHelper(this).showSystemUI();
        WindowUtil.setLightStatusBarFromTheme(this);
        WindowUtil.setLightNavigationBarFromTheme(this);
    }

    public /* synthetic */ void lambda$onCreate$0(View view) {
        finish();
    }

    public /* synthetic */ void lambda$onCreate$2(boolean z, List list) {
        this.adapter.submitList(Stream.of(list).map(new Function(z) { // from class: org.thoughtcrime.securesms.wallpaper.ChatWallpaperPreviewActivity$$ExternalSyntheticLambda0
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ChatWallpaperPreviewActivity.$r8$lambda$EiphXjo6jxvBVumtmVLaproLEC8(this.f$0, (ChatWallpaper) obj);
            }
        }).map(new ChatWallpaperPreviewActivity$$ExternalSyntheticLambda1()).toList());
    }

    public static /* synthetic */ ChatWallpaper lambda$onCreate$1(boolean z, ChatWallpaper chatWallpaper) {
        return ChatWallpaperFactory.updateWithDimming(chatWallpaper, z ? 0.2f : 0.0f);
    }

    public /* synthetic */ void lambda$onCreate$3(View view) {
        setResult(-1, new Intent().putExtra(EXTRA_CHAT_WALLPAPER, ((ChatWallpaperSelectionMappingModel) this.adapter.getCurrentList().get(this.viewPager.getCurrentItem())).getWallpaper()));
        finish();
    }

    public /* synthetic */ void lambda$onCreate$4(ChatColors chatColors, View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        updateChatColors(chatColors);
    }

    public /* synthetic */ void lambda$onCreate$5(ChatColors chatColors, View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        updateChatColors(chatColors);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onDestroy() {
        OnPageChanged onPageChanged = this.onPageChanged;
        if (onPageChanged != null) {
            this.viewPager.unregisterOnPageChangeCallback(onPageChanged);
        }
        super.onDestroy();
    }

    /* loaded from: classes5.dex */
    private class OnPageChanged extends ViewPager2.OnPageChangeCallback {
        private OnPageChanged() {
            ChatWallpaperPreviewActivity.this = r1;
        }

        @Override // androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
        public void onPageSelected(int i) {
            ChatWallpaperPreviewActivity.this.updateChatColors(((ChatWallpaperSelectionMappingModel) ChatWallpaperPreviewActivity.this.adapter.getCurrentList().get(i)).getWallpaper().getAutoChatColors());
        }
    }

    public void updateChatColors(ChatColors chatColors) {
        this.colorizerView.setBackground(chatColors.getChatBubbleMask());
        ColorizerView colorizerView = this.colorizerView;
        colorizerView.setProjections(Collections.singletonList(Projection.relativeToViewWithCommonRoot(this.bubble2, colorizerView, new Projection.Corners((float) ViewUtil.dpToPx(18)))));
        this.bubble2.getBackground().setColorFilter(chatColors.getChatBubbleColorFilter());
    }

    /* loaded from: classes5.dex */
    private class UpdateChatColorsOnNextLayoutChange implements View.OnLayoutChangeListener {
        private final ChatColors chatColors;

        private UpdateChatColorsOnNextLayoutChange(ChatColors chatColors) {
            ChatWallpaperPreviewActivity.this = r1;
            this.chatColors = chatColors;
        }

        @Override // android.view.View.OnLayoutChangeListener
        public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            view.removeOnLayoutChangeListener(this);
            ChatWallpaperPreviewActivity.this.updateChatColors(this.chatColors);
        }
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }
}
