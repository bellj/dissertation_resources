package org.thoughtcrime.securesms.wallpaper;

import android.widget.ImageView;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* loaded from: classes5.dex */
public class ChatWallpaperSelectionMappingModel implements MappingModel<ChatWallpaperSelectionMappingModel> {
    private final ChatWallpaper chatWallpaper;

    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
    public /* synthetic */ Object getChangePayload(ChatWallpaperSelectionMappingModel chatWallpaperSelectionMappingModel) {
        return MappingModel.CC.$default$getChangePayload(this, chatWallpaperSelectionMappingModel);
    }

    public ChatWallpaperSelectionMappingModel(ChatWallpaper chatWallpaper) {
        this.chatWallpaper = chatWallpaper;
    }

    public ChatWallpaper getWallpaper() {
        return this.chatWallpaper;
    }

    public void loadInto(ImageView imageView) {
        this.chatWallpaper.loadInto(imageView);
    }

    public boolean areItemsTheSame(ChatWallpaperSelectionMappingModel chatWallpaperSelectionMappingModel) {
        return areContentsTheSame(chatWallpaperSelectionMappingModel);
    }

    public boolean areContentsTheSame(ChatWallpaperSelectionMappingModel chatWallpaperSelectionMappingModel) {
        return this.chatWallpaper.equals(chatWallpaperSelectionMappingModel.chatWallpaper);
    }
}
