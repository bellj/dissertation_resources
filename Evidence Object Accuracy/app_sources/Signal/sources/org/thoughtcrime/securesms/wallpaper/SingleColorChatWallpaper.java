package org.thoughtcrime.securesms.wallpaper;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.widget.ImageView;
import java.util.Objects;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.database.model.databaseprotos.Wallpaper;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;

/* loaded from: classes5.dex */
public final class SingleColorChatWallpaper implements ChatWallpaper, Parcelable {
    public static final ChatWallpaper BLUSH = new SingleColorChatWallpaper(-1939069, 0.0f);
    public static final ChatWallpaper CELADON = new SingleColorChatWallpaper(-7754097, 0.0f);
    public static final ChatWallpaper COPPER = new SingleColorChatWallpaper(-2125455, 0.0f);
    public static final Parcelable.Creator<SingleColorChatWallpaper> CREATOR = new Parcelable.Creator<SingleColorChatWallpaper>() { // from class: org.thoughtcrime.securesms.wallpaper.SingleColorChatWallpaper.1
        @Override // android.os.Parcelable.Creator
        public SingleColorChatWallpaper createFromParcel(Parcel parcel) {
            return new SingleColorChatWallpaper(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public SingleColorChatWallpaper[] newArray(int i) {
            return new SingleColorChatWallpaper[i];
        }
    };
    public static final ChatWallpaper DUST = new SingleColorChatWallpaper(-6383481, 0.0f);
    public static final ChatWallpaper EGGPLANT = new SingleColorChatWallpaper(-10337719, 0.0f);
    public static final ChatWallpaper FROST = new SingleColorChatWallpaper(-8611402, 0.0f);
    public static final ChatWallpaper LILAC = new SingleColorChatWallpaper(-3569433, 0.0f);
    public static final ChatWallpaper NAVY = new SingleColorChatWallpaper(-12567663, 0.0f);
    public static final ChatWallpaper PACIFIC = new SingleColorChatWallpaper(-13449246, 0.0f);
    public static final ChatWallpaper PINK = new SingleColorChatWallpaper(-1927229, 0.0f);
    public static final ChatWallpaper RAINFOREST = new SingleColorChatWallpaper(-15441592, 0.0f);
    public static final ChatWallpaper SILVER = new SingleColorChatWallpaper(-6118742, 0.0f);
    private final int color;
    private final float dimLevelInDarkTheme;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaper
    public /* synthetic */ ChatColors getAutoChatColors() {
        return ChatWallpaper.CC.$default$getAutoChatColors(this);
    }

    @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaper
    public /* synthetic */ boolean isPhoto() {
        return ChatWallpaper.CC.$default$isPhoto(this);
    }

    @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaper
    public /* synthetic */ boolean prefetch(Context context, long j) {
        return ChatWallpaper.CC.$default$prefetch(this, context, j);
    }

    public SingleColorChatWallpaper(int i, float f) {
        this.color = i;
        this.dimLevelInDarkTheme = f;
    }

    private SingleColorChatWallpaper(Parcel parcel) {
        this.color = parcel.readInt();
        this.dimLevelInDarkTheme = parcel.readFloat();
    }

    @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaper
    public float getDimLevelForDarkTheme() {
        return this.dimLevelInDarkTheme;
    }

    @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaper
    public void loadInto(ImageView imageView) {
        imageView.setImageDrawable(new ColorDrawable(this.color));
    }

    @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaper
    public boolean isSameSource(ChatWallpaper chatWallpaper) {
        if (this == chatWallpaper) {
            return true;
        }
        if (SingleColorChatWallpaper.class == chatWallpaper.getClass() && this.color == ((SingleColorChatWallpaper) chatWallpaper).color) {
            return true;
        }
        return false;
    }

    @Override // org.thoughtcrime.securesms.wallpaper.ChatWallpaper
    public Wallpaper serialize() {
        Wallpaper.SingleColor.Builder newBuilder = Wallpaper.SingleColor.newBuilder();
        newBuilder.setColor(this.color);
        return Wallpaper.newBuilder().setSingleColor(newBuilder).setDimLevelInDarkTheme(this.dimLevelInDarkTheme).build();
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.color);
        parcel.writeFloat(this.dimLevelInDarkTheme);
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || SingleColorChatWallpaper.class != obj.getClass()) {
            return false;
        }
        SingleColorChatWallpaper singleColorChatWallpaper = (SingleColorChatWallpaper) obj;
        if (this.color == singleColorChatWallpaper.color && Float.compare(this.dimLevelInDarkTheme, singleColorChatWallpaper.dimLevelInDarkTheme) == 0) {
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Objects.hash(Integer.valueOf(this.color), Float.valueOf(this.dimLevelInDarkTheme));
    }
}
