package org.thoughtcrime.securesms.wallpaper;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes5.dex */
public class ChatWallpaperFragmentDirections {
    private ChatWallpaperFragmentDirections() {
    }

    public static NavDirections actionChatWallpaperFragmentToChatWallpaperSelectionFragment() {
        return new ActionOnlyNavDirections(R.id.action_chatWallpaperFragment_to_chatWallpaperSelectionFragment);
    }

    public static ActionChatWallpaperFragmentToChatColorSelectionFragment actionChatWallpaperFragmentToChatColorSelectionFragment(RecipientId recipientId) {
        return new ActionChatWallpaperFragmentToChatColorSelectionFragment(recipientId);
    }

    /* loaded from: classes5.dex */
    public static class ActionChatWallpaperFragmentToChatColorSelectionFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_chatWallpaperFragment_to_chatColorSelectionFragment;
        }

        private ActionChatWallpaperFragmentToChatColorSelectionFragment(RecipientId recipientId) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("recipient_id", recipientId);
        }

        public ActionChatWallpaperFragmentToChatColorSelectionFragment setRecipientId(RecipientId recipientId) {
            this.arguments.put("recipient_id", recipientId);
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("recipient_id")) {
                RecipientId recipientId = (RecipientId) this.arguments.get("recipient_id");
                if (Parcelable.class.isAssignableFrom(RecipientId.class) || recipientId == null) {
                    bundle.putParcelable("recipient_id", (Parcelable) Parcelable.class.cast(recipientId));
                } else if (Serializable.class.isAssignableFrom(RecipientId.class)) {
                    bundle.putSerializable("recipient_id", (Serializable) Serializable.class.cast(recipientId));
                } else {
                    throw new UnsupportedOperationException(RecipientId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            return bundle;
        }

        public RecipientId getRecipientId() {
            return (RecipientId) this.arguments.get("recipient_id");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionChatWallpaperFragmentToChatColorSelectionFragment actionChatWallpaperFragmentToChatColorSelectionFragment = (ActionChatWallpaperFragmentToChatColorSelectionFragment) obj;
            if (this.arguments.containsKey("recipient_id") != actionChatWallpaperFragmentToChatColorSelectionFragment.arguments.containsKey("recipient_id")) {
                return false;
            }
            if (getRecipientId() == null ? actionChatWallpaperFragmentToChatColorSelectionFragment.getRecipientId() == null : getRecipientId().equals(actionChatWallpaperFragmentToChatColorSelectionFragment.getRecipientId())) {
                return getActionId() == actionChatWallpaperFragmentToChatColorSelectionFragment.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((getRecipientId() != null ? getRecipientId().hashCode() : 0) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionChatWallpaperFragmentToChatColorSelectionFragment(actionId=" + getActionId() + "){recipientId=" + getRecipientId() + "}";
        }
    }
}
