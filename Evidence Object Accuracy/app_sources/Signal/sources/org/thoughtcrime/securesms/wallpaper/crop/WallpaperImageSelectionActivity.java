package org.thoughtcrime.securesms.wallpaper.crop;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes5.dex */
public final class WallpaperImageSelectionActivity extends AppCompatActivity implements MediaGalleryFragment.Callbacks {
    private static final int CROP;
    private static final String EXTRA_RECIPIENT_ID;

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public boolean isCameraEnabled() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public boolean isMultiselectEnabled() {
        return false;
    }

    public static Intent getIntent(Context context, RecipientId recipientId) {
        Intent intent = new Intent(context, WallpaperImageSelectionActivity.class);
        intent.putExtra(EXTRA_RECIPIENT_ID, recipientId);
        return intent;
    }

    @Override // androidx.appcompat.app.AppCompatActivity, android.app.Activity, android.view.ContextThemeWrapper, android.content.ContextWrapper
    public void attachBaseContext(Context context) {
        getDelegate().setLocalNightMode(2);
        super.attachBaseContext(context);
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.wallpaper_image_selection_activity);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MediaGalleryFragment()).commit();
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public void onMediaSelected(Media media) {
        startActivityForResult(WallpaperCropActivity.newIntent(this, getRecipientId(), media.getUri()), 901);
    }

    private RecipientId getRecipientId() {
        return (RecipientId) getIntent().getParcelableExtra(EXTRA_RECIPIENT_ID);
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 901 && i2 == -1) {
            setResult(-1, intent);
            finish();
        }
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public void onMediaUnselected(Media media) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public void onSelectedMediaClicked(Media media) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public void onNavigateToCamera() {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public void onSubmit() {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.gallery.MediaGalleryFragment.Callbacks
    public void onToolbarNavigationClicked() {
        setResult(0);
        finish();
    }
}
