package org.thoughtcrime.securesms.video;

import android.media.MediaDataSource;
import java.io.File;
import org.thoughtcrime.securesms.crypto.AttachmentSecret;

/* loaded from: classes5.dex */
public final class EncryptedMediaDataSource {
    public static MediaDataSource createFor(AttachmentSecret attachmentSecret, File file, byte[] bArr, long j) {
        if (bArr == null) {
            return new ClassicEncryptedMediaDataSource(attachmentSecret, file, j);
        }
        return new ModernEncryptedMediaDataSource(attachmentSecret, file, bArr, j);
    }

    public static MediaDataSource createForDiskBlob(AttachmentSecret attachmentSecret, File file) {
        return new ModernEncryptedMediaDataSource(attachmentSecret, file, null, file.length() - 32);
    }
}
