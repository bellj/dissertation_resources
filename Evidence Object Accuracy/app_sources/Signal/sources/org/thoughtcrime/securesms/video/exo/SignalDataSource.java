package org.thoughtcrime.securesms.video.exo;

import android.content.Context;
import android.net.Uri;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.DefaultDataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.TransferListener;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import okhttp3.OkHttpClient;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.providers.BlobProvider;

/* loaded from: classes5.dex */
public class SignalDataSource implements DataSource {
    private final BlobDataSource blobDataSource;
    private final ChunkedDataSource chunkedDataSource;
    private DataSource dataSource;
    private final DefaultDataSource defaultDataSource;
    private final PartDataSource partDataSource;

    @Override // com.google.android.exoplayer2.upstream.DataSource
    public void addTransferListener(TransferListener transferListener) {
    }

    public SignalDataSource(DefaultDataSource defaultDataSource, PartDataSource partDataSource, BlobDataSource blobDataSource, ChunkedDataSource chunkedDataSource) {
        this.defaultDataSource = defaultDataSource;
        this.partDataSource = partDataSource;
        this.blobDataSource = blobDataSource;
        this.chunkedDataSource = chunkedDataSource;
    }

    @Override // com.google.android.exoplayer2.upstream.DataSource
    public long open(DataSpec dataSpec) throws IOException {
        if (BlobProvider.isAuthority(dataSpec.uri)) {
            this.dataSource = this.blobDataSource;
        } else if (PartAuthority.isLocalUri(dataSpec.uri)) {
            this.dataSource = this.partDataSource;
        } else if (this.chunkedDataSource == null || !isRemoteUri(dataSpec.uri)) {
            this.dataSource = this.defaultDataSource;
        } else {
            this.dataSource = this.chunkedDataSource;
        }
        return this.dataSource.open(dataSpec);
    }

    @Override // com.google.android.exoplayer2.upstream.DataReader
    public int read(byte[] bArr, int i, int i2) throws IOException {
        return this.dataSource.read(bArr, i, i2);
    }

    @Override // com.google.android.exoplayer2.upstream.DataSource
    public Uri getUri() {
        return this.dataSource.getUri();
    }

    @Override // com.google.android.exoplayer2.upstream.DataSource
    public Map<String, List<String>> getResponseHeaders() {
        return Collections.emptyMap();
    }

    @Override // com.google.android.exoplayer2.upstream.DataSource
    public void close() throws IOException {
        this.dataSource.close();
    }

    private static boolean isRemoteUri(Uri uri) {
        if (uri == null) {
            return false;
        }
        String scheme = uri.getScheme();
        if ("http".equalsIgnoreCase(scheme) || "https".equalsIgnoreCase(scheme)) {
            return true;
        }
        return false;
    }

    /* loaded from: classes5.dex */
    public static final class Factory implements DataSource.Factory {
        private final Context context;
        private final TransferListener listener;
        private final OkHttpClient okHttpClient;

        public Factory(Context context, OkHttpClient okHttpClient, TransferListener transferListener) {
            this.context = context;
            this.okHttpClient = okHttpClient;
            this.listener = transferListener;
        }

        @Override // com.google.android.exoplayer2.upstream.DataSource.Factory
        public SignalDataSource createDataSource() {
            ChunkedDataSource chunkedDataSource = null;
            DefaultDataSource createDataSource = new DefaultDataSourceFactory(this.context, "GenericUserAgent", (TransferListener) null).createDataSource();
            PartDataSource partDataSource = new PartDataSource(this.context, this.listener);
            BlobDataSource blobDataSource = new BlobDataSource(this.context, this.listener);
            OkHttpClient okHttpClient = this.okHttpClient;
            if (okHttpClient != null) {
                chunkedDataSource = new ChunkedDataSource(okHttpClient, this.listener);
            }
            return new SignalDataSource(createDataSource, partDataSource, blobDataSource, chunkedDataSource);
        }
    }
}
