package org.thoughtcrime.securesms.video;

import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.MediaMetadata;
import com.google.android.exoplayer2.PlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.device.DeviceInfo;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.source.ClippingMediaSource;
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.video.VideoListener;
import com.google.android.exoplayer2.video.VideoSize;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.mms.VideoSlide;
import org.thoughtcrime.securesms.util.MediaUtil;

/* loaded from: classes5.dex */
public class VideoPlayer extends FrameLayout {
    private static final String TAG = Log.tag(VideoPlayer.class);
    private boolean clipped;
    private long clippedStartUs;
    private final PlayerControlView exoControls;
    private SimpleExoPlayer exoPlayer;
    private ExoPlayerListener exoPlayerListener;
    private final PlayerView exoView;
    private MediaItem mediaItem;
    private final DefaultMediaSourceFactory mediaSourceFactory;
    private PlayerCallback playerCallback;
    private Player.Listener playerListener;
    private PlayerPositionDiscontinuityCallback playerPositionDiscontinuityCallback;
    private PlayerStateCallback playerStateCallback;
    private Window window;

    /* loaded from: classes5.dex */
    public interface PlayerCallback {

        /* renamed from: org.thoughtcrime.securesms.video.VideoPlayer$PlayerCallback$-CC */
        /* loaded from: classes5.dex */
        public final /* synthetic */ class CC {
            public static void $default$onReady(PlayerCallback playerCallback) {
            }
        }

        void onError();

        void onPlaying();

        void onReady();

        void onStopped();
    }

    /* loaded from: classes5.dex */
    public interface PlayerPositionDiscontinuityCallback {
        void onPositionDiscontinuity(VideoPlayer videoPlayer, int i);
    }

    /* loaded from: classes5.dex */
    public interface PlayerStateCallback {
        void onPlayerReady();
    }

    public VideoPlayer(Context context) {
        this(context, null);
    }

    public VideoPlayer(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public VideoPlayer(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        FrameLayout.inflate(context, R.layout.video_player, this);
        this.mediaSourceFactory = new DefaultMediaSourceFactory(context);
        this.exoView = (PlayerView) findViewById(R.id.video_view);
        PlayerControlView playerControlView = new PlayerControlView(getContext());
        this.exoControls = playerControlView;
        playerControlView.setShowTimeoutMs(-1);
        this.exoPlayerListener = new ExoPlayerListener();
        this.playerListener = new Player.Listener() { // from class: org.thoughtcrime.securesms.video.VideoPlayer.1
            @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.audio.AudioListener
            public /* bridge */ /* synthetic */ void onAudioAttributesChanged(AudioAttributes audioAttributes) {
                Player.Listener.CC.$default$onAudioAttributesChanged(this, audioAttributes);
            }

            public /* bridge */ /* synthetic */ void onAudioSessionIdChanged(int i2) {
                Player.Listener.CC.$default$onAudioSessionIdChanged(this, i2);
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            public /* bridge */ /* synthetic */ void onAvailableCommandsChanged(Player.Commands commands) {
                Player.Listener.CC.$default$onAvailableCommandsChanged(this, commands);
            }

            @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.text.TextOutput
            public /* bridge */ /* synthetic */ void onCues(List list) {
                Player.Listener.CC.$default$onCues(this, list);
            }

            @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.device.DeviceListener
            public /* bridge */ /* synthetic */ void onDeviceInfoChanged(DeviceInfo deviceInfo) {
                Player.Listener.CC.$default$onDeviceInfoChanged(this, deviceInfo);
            }

            @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.device.DeviceListener
            public /* bridge */ /* synthetic */ void onDeviceVolumeChanged(int i2, boolean z) {
                Player.Listener.CC.$default$onDeviceVolumeChanged(this, i2, z);
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            public /* bridge */ /* synthetic */ void onEvents(Player player, Player.Events events) {
                Player.Listener.CC.$default$onEvents(this, player, events);
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            public /* bridge */ /* synthetic */ void onIsLoadingChanged(boolean z) {
                Player.Listener.CC.$default$onIsLoadingChanged(this, z);
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            public /* bridge */ /* synthetic */ void onIsPlayingChanged(boolean z) {
                Player.Listener.CC.$default$onIsPlayingChanged(this, z);
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            @Deprecated
            public /* bridge */ /* synthetic */ void onLoadingChanged(boolean z) {
                Player.EventListener.CC.$default$onLoadingChanged(this, z);
            }

            public /* bridge */ /* synthetic */ void onMaxSeekToPreviousPositionChanged(int i2) {
                Player.EventListener.CC.$default$onMaxSeekToPreviousPositionChanged(this, i2);
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            public /* bridge */ /* synthetic */ void onMediaItemTransition(MediaItem mediaItem, int i2) {
                Player.Listener.CC.$default$onMediaItemTransition(this, mediaItem, i2);
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            public /* bridge */ /* synthetic */ void onMediaMetadataChanged(MediaMetadata mediaMetadata) {
                Player.Listener.CC.$default$onMediaMetadataChanged(this, mediaMetadata);
            }

            @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.metadata.MetadataOutput
            public /* bridge */ /* synthetic */ void onMetadata(Metadata metadata) {
                Player.Listener.CC.$default$onMetadata(this, metadata);
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            public /* bridge */ /* synthetic */ void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
                Player.Listener.CC.$default$onPlaybackParametersChanged(this, playbackParameters);
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            public /* bridge */ /* synthetic */ void onPlaybackSuppressionReasonChanged(int i2) {
                Player.Listener.CC.$default$onPlaybackSuppressionReasonChanged(this, i2);
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            public /* bridge */ /* synthetic */ void onPlayerErrorChanged(PlaybackException playbackException) {
                Player.Listener.CC.$default$onPlayerErrorChanged(this, playbackException);
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            @Deprecated
            public /* bridge */ /* synthetic */ void onPlayerStateChanged(boolean z, int i2) {
                Player.EventListener.CC.$default$onPlayerStateChanged(this, z, i2);
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            public /* bridge */ /* synthetic */ void onPlaylistMetadataChanged(MediaMetadata mediaMetadata) {
                Player.Listener.CC.$default$onPlaylistMetadataChanged(this, mediaMetadata);
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            @Deprecated
            public /* bridge */ /* synthetic */ void onPositionDiscontinuity(int i2) {
                Player.EventListener.CC.$default$onPositionDiscontinuity(this, i2);
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            public /* bridge */ /* synthetic */ void onPositionDiscontinuity(Player.PositionInfo positionInfo, Player.PositionInfo positionInfo2, int i2) {
                Player.Listener.CC.$default$onPositionDiscontinuity(this, positionInfo, positionInfo2, i2);
            }

            @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.video.VideoListener
            public /* bridge */ /* synthetic */ void onRenderedFirstFrame() {
                Player.Listener.CC.$default$onRenderedFirstFrame(this);
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            public /* bridge */ /* synthetic */ void onRepeatModeChanged(int i2) {
                Player.Listener.CC.$default$onRepeatModeChanged(this, i2);
            }

            public /* bridge */ /* synthetic */ void onSeekBackIncrementChanged(long j) {
                Player.Listener.CC.$default$onSeekBackIncrementChanged(this, j);
            }

            public /* bridge */ /* synthetic */ void onSeekForwardIncrementChanged(long j) {
                Player.Listener.CC.$default$onSeekForwardIncrementChanged(this, j);
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            @Deprecated
            public /* bridge */ /* synthetic */ void onSeekProcessed() {
                Player.EventListener.CC.$default$onSeekProcessed(this);
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            public /* bridge */ /* synthetic */ void onShuffleModeEnabledChanged(boolean z) {
                Player.Listener.CC.$default$onShuffleModeEnabledChanged(this, z);
            }

            @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.audio.AudioListener
            public /* bridge */ /* synthetic */ void onSkipSilenceEnabledChanged(boolean z) {
                Player.Listener.CC.$default$onSkipSilenceEnabledChanged(this, z);
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            @Deprecated
            public /* bridge */ /* synthetic */ void onStaticMetadataChanged(List list) {
                Player.EventListener.CC.$default$onStaticMetadataChanged(this, list);
            }

            @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.video.VideoListener
            public /* bridge */ /* synthetic */ void onSurfaceSizeChanged(int i2, int i3) {
                Player.Listener.CC.$default$onSurfaceSizeChanged(this, i2, i3);
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            public /* bridge */ /* synthetic */ void onTimelineChanged(Timeline timeline, int i2) {
                Player.Listener.CC.$default$onTimelineChanged(this, timeline, i2);
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            public /* bridge */ /* synthetic */ void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
                Player.Listener.CC.$default$onTracksChanged(this, trackGroupArray, trackSelectionArray);
            }

            @Override // com.google.android.exoplayer2.video.VideoListener
            @Deprecated
            public /* bridge */ /* synthetic */ void onVideoSizeChanged(int i2, int i3, int i4, float f) {
                VideoListener.CC.$default$onVideoSizeChanged(this, i2, i3, i4, f);
            }

            @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.video.VideoListener
            public /* bridge */ /* synthetic */ void onVideoSizeChanged(VideoSize videoSize) {
                Player.Listener.CC.$default$onVideoSizeChanged(this, videoSize);
            }

            @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.audio.AudioListener
            public /* bridge */ /* synthetic */ void onVolumeChanged(float f) {
                Player.Listener.CC.$default$onVolumeChanged(this, f);
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            public void onPlayWhenReadyChanged(boolean z, int i2) {
                onPlaybackStateChanged(z, VideoPlayer.this.exoPlayer.getPlaybackState());
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            public void onPlaybackStateChanged(int i2) {
                onPlaybackStateChanged(VideoPlayer.this.exoPlayer.getPlayWhenReady(), i2);
            }

            private void onPlaybackStateChanged(boolean z, int i2) {
                if (VideoPlayer.this.playerCallback == null) {
                    return;
                }
                if (i2 == 3) {
                    VideoPlayer.this.playerCallback.onReady();
                    if (z) {
                        VideoPlayer.this.playerCallback.onPlaying();
                    }
                } else if (i2 == 4) {
                    VideoPlayer.this.playerCallback.onStopped();
                }
            }

            @Override // com.google.android.exoplayer2.Player.EventListener
            public void onPlayerError(PlaybackException playbackException) {
                Log.w(VideoPlayer.TAG, "A player error occurred", playbackException);
                if (VideoPlayer.this.playerCallback != null) {
                    VideoPlayer.this.playerCallback.onError();
                }
            }
        };
    }

    public void setVideoSource(VideoSlide videoSlide, boolean z, String str) {
        if (this.exoPlayer == null) {
            SimpleExoPlayer require = ApplicationDependencies.getExoPlayerPool().require(str);
            this.exoPlayer = require;
            require.addListener((Player.Listener) this.exoPlayerListener);
            this.exoPlayer.addListener(this.playerListener);
            this.exoView.setPlayer(this.exoPlayer);
            this.exoControls.setPlayer(this.exoPlayer);
        }
        Uri uri = videoSlide.getUri();
        Objects.requireNonNull(uri);
        MediaItem fromUri = MediaItem.fromUri(uri);
        this.mediaItem = fromUri;
        this.exoPlayer.setMediaItem(fromUri);
        this.exoPlayer.prepare();
        this.exoPlayer.setPlayWhenReady(z);
    }

    public void mute() {
        SimpleExoPlayer simpleExoPlayer = this.exoPlayer;
        if (simpleExoPlayer != null && simpleExoPlayer.getAudioComponent() != null) {
            this.exoPlayer.getAudioComponent().setVolume(0.0f);
        }
    }

    public void unmute() {
        SimpleExoPlayer simpleExoPlayer = this.exoPlayer;
        if (simpleExoPlayer != null && simpleExoPlayer.getAudioComponent() != null) {
            this.exoPlayer.getAudioComponent().setVolume(1.0f);
        }
    }

    public boolean hasAudioTrack() {
        TrackGroupArray currentTrackGroups;
        SimpleExoPlayer simpleExoPlayer = this.exoPlayer;
        if (!(simpleExoPlayer == null || (currentTrackGroups = simpleExoPlayer.getCurrentTrackGroups()) == null)) {
            for (int i = 0; i < currentTrackGroups.length; i++) {
                for (int i2 = 0; i2 < currentTrackGroups.get(i).length; i2++) {
                    if (MediaUtil.isAudioType(currentTrackGroups.get(i).getFormat(i2).sampleMimeType)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean isInitialized() {
        return this.exoPlayer != null;
    }

    public void setResizeMode(int i) {
        this.exoView.setResizeMode(i);
    }

    public boolean isPlaying() {
        SimpleExoPlayer simpleExoPlayer = this.exoPlayer;
        if (simpleExoPlayer != null) {
            return simpleExoPlayer.isPlaying();
        }
        return false;
    }

    public void pause() {
        SimpleExoPlayer simpleExoPlayer = this.exoPlayer;
        if (simpleExoPlayer != null) {
            simpleExoPlayer.setPlayWhenReady(false);
        }
    }

    public void hideControls() {
        PlayerView playerView = this.exoView;
        if (playerView != null) {
            playerView.hideController();
        }
    }

    public View getControlView() {
        return this.exoControls;
    }

    public void cleanup() {
        SimpleExoPlayer simpleExoPlayer = this.exoPlayer;
        if (simpleExoPlayer != null) {
            simpleExoPlayer.stop();
            this.exoPlayer.clearMediaItems();
            this.exoView.setPlayer(null);
            this.exoControls.setPlayer(null);
            this.exoPlayer.removeListener(this.playerListener);
            this.exoPlayer.removeListener((Player.Listener) this.exoPlayerListener);
            ApplicationDependencies.getExoPlayerPool().pool(this.exoPlayer);
            this.exoPlayer = null;
        }
    }

    public void loopForever() {
        SimpleExoPlayer simpleExoPlayer = this.exoPlayer;
        if (simpleExoPlayer != null) {
            simpleExoPlayer.setRepeatMode(1);
        }
    }

    public long getDuration() {
        SimpleExoPlayer simpleExoPlayer = this.exoPlayer;
        if (simpleExoPlayer != null) {
            return simpleExoPlayer.getDuration();
        }
        return 0;
    }

    public long getPlaybackPosition() {
        SimpleExoPlayer simpleExoPlayer = this.exoPlayer;
        if (simpleExoPlayer != null) {
            return simpleExoPlayer.getCurrentPosition();
        }
        return 0;
    }

    public long getPlaybackPositionUs() {
        SimpleExoPlayer simpleExoPlayer = this.exoPlayer;
        if (simpleExoPlayer != null) {
            return TimeUnit.MILLISECONDS.toMicros(simpleExoPlayer.getCurrentPosition()) + this.clippedStartUs;
        }
        return 0;
    }

    public void setPlaybackPosition(long j) {
        SimpleExoPlayer simpleExoPlayer = this.exoPlayer;
        if (simpleExoPlayer != null) {
            simpleExoPlayer.seekTo(j);
        }
    }

    public void clip(long j, long j2, boolean z) {
        MediaItem mediaItem;
        if (this.exoPlayer != null && (mediaItem = this.mediaItem) != null) {
            this.exoPlayer.setMediaSource(new ClippingMediaSource(this.mediaSourceFactory.createMediaSource(mediaItem), j, j2));
            this.exoPlayer.prepare();
            this.exoPlayer.setPlayWhenReady(z);
            this.clipped = true;
            this.clippedStartUs = j;
        }
    }

    public void removeClip(boolean z) {
        MediaItem mediaItem;
        SimpleExoPlayer simpleExoPlayer = this.exoPlayer;
        if (simpleExoPlayer != null && (mediaItem = this.mediaItem) != null) {
            if (this.clipped) {
                simpleExoPlayer.setMediaItem(mediaItem);
                this.exoPlayer.prepare();
                this.clipped = false;
                this.clippedStartUs = 0;
            }
            this.exoPlayer.setPlayWhenReady(z);
        }
    }

    public void setWindow(Window window) {
        this.window = window;
    }

    public void setPlayerStateCallbacks(PlayerStateCallback playerStateCallback) {
        this.playerStateCallback = playerStateCallback;
    }

    public void setPlayerCallback(PlayerCallback playerCallback) {
        this.playerCallback = playerCallback;
    }

    public void setPlayerPositionDiscontinuityCallback(PlayerPositionDiscontinuityCallback playerPositionDiscontinuityCallback) {
        this.playerPositionDiscontinuityCallback = playerPositionDiscontinuityCallback;
    }

    public void play() {
        SimpleExoPlayer simpleExoPlayer = this.exoPlayer;
        if (simpleExoPlayer != null) {
            simpleExoPlayer.setPlayWhenReady(true);
            if (this.exoPlayer.getCurrentPosition() >= this.exoPlayer.getDuration()) {
                this.exoPlayer.seekTo(0);
            }
        }
    }

    /* loaded from: classes5.dex */
    public class ExoPlayerListener implements Player.Listener {
        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.audio.AudioListener
        public /* bridge */ /* synthetic */ void onAudioAttributesChanged(AudioAttributes audioAttributes) {
            Player.Listener.CC.$default$onAudioAttributesChanged(this, audioAttributes);
        }

        public /* bridge */ /* synthetic */ void onAudioSessionIdChanged(int i) {
            Player.Listener.CC.$default$onAudioSessionIdChanged(this, i);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onAvailableCommandsChanged(Player.Commands commands) {
            Player.Listener.CC.$default$onAvailableCommandsChanged(this, commands);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.text.TextOutput
        public /* bridge */ /* synthetic */ void onCues(List list) {
            Player.Listener.CC.$default$onCues(this, list);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.device.DeviceListener
        public /* bridge */ /* synthetic */ void onDeviceInfoChanged(DeviceInfo deviceInfo) {
            Player.Listener.CC.$default$onDeviceInfoChanged(this, deviceInfo);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.device.DeviceListener
        public /* bridge */ /* synthetic */ void onDeviceVolumeChanged(int i, boolean z) {
            Player.Listener.CC.$default$onDeviceVolumeChanged(this, i, z);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onEvents(Player player, Player.Events events) {
            Player.Listener.CC.$default$onEvents(this, player, events);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onIsLoadingChanged(boolean z) {
            Player.Listener.CC.$default$onIsLoadingChanged(this, z);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onIsPlayingChanged(boolean z) {
            Player.Listener.CC.$default$onIsPlayingChanged(this, z);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        @Deprecated
        public /* bridge */ /* synthetic */ void onLoadingChanged(boolean z) {
            Player.EventListener.CC.$default$onLoadingChanged(this, z);
        }

        public /* bridge */ /* synthetic */ void onMaxSeekToPreviousPositionChanged(int i) {
            Player.EventListener.CC.$default$onMaxSeekToPreviousPositionChanged(this, i);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onMediaItemTransition(MediaItem mediaItem, int i) {
            Player.Listener.CC.$default$onMediaItemTransition(this, mediaItem, i);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onMediaMetadataChanged(MediaMetadata mediaMetadata) {
            Player.Listener.CC.$default$onMediaMetadataChanged(this, mediaMetadata);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.metadata.MetadataOutput
        public /* bridge */ /* synthetic */ void onMetadata(Metadata metadata) {
            Player.Listener.CC.$default$onMetadata(this, metadata);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
            Player.Listener.CC.$default$onPlaybackParametersChanged(this, playbackParameters);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onPlaybackSuppressionReasonChanged(int i) {
            Player.Listener.CC.$default$onPlaybackSuppressionReasonChanged(this, i);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onPlayerError(PlaybackException playbackException) {
            Player.Listener.CC.$default$onPlayerError(this, playbackException);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onPlayerErrorChanged(PlaybackException playbackException) {
            Player.Listener.CC.$default$onPlayerErrorChanged(this, playbackException);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        @Deprecated
        public /* bridge */ /* synthetic */ void onPlayerStateChanged(boolean z, int i) {
            Player.EventListener.CC.$default$onPlayerStateChanged(this, z, i);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onPlaylistMetadataChanged(MediaMetadata mediaMetadata) {
            Player.Listener.CC.$default$onPlaylistMetadataChanged(this, mediaMetadata);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        @Deprecated
        public /* bridge */ /* synthetic */ void onPositionDiscontinuity(int i) {
            Player.EventListener.CC.$default$onPositionDiscontinuity(this, i);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.video.VideoListener
        public /* bridge */ /* synthetic */ void onRenderedFirstFrame() {
            Player.Listener.CC.$default$onRenderedFirstFrame(this);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onRepeatModeChanged(int i) {
            Player.Listener.CC.$default$onRepeatModeChanged(this, i);
        }

        public /* bridge */ /* synthetic */ void onSeekBackIncrementChanged(long j) {
            Player.Listener.CC.$default$onSeekBackIncrementChanged(this, j);
        }

        public /* bridge */ /* synthetic */ void onSeekForwardIncrementChanged(long j) {
            Player.Listener.CC.$default$onSeekForwardIncrementChanged(this, j);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        @Deprecated
        public /* bridge */ /* synthetic */ void onSeekProcessed() {
            Player.EventListener.CC.$default$onSeekProcessed(this);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onShuffleModeEnabledChanged(boolean z) {
            Player.Listener.CC.$default$onShuffleModeEnabledChanged(this, z);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.audio.AudioListener
        public /* bridge */ /* synthetic */ void onSkipSilenceEnabledChanged(boolean z) {
            Player.Listener.CC.$default$onSkipSilenceEnabledChanged(this, z);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        @Deprecated
        public /* bridge */ /* synthetic */ void onStaticMetadataChanged(List list) {
            Player.EventListener.CC.$default$onStaticMetadataChanged(this, list);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.video.VideoListener
        public /* bridge */ /* synthetic */ void onSurfaceSizeChanged(int i, int i2) {
            Player.Listener.CC.$default$onSurfaceSizeChanged(this, i, i2);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onTimelineChanged(Timeline timeline, int i) {
            Player.Listener.CC.$default$onTimelineChanged(this, timeline, i);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
            Player.Listener.CC.$default$onTracksChanged(this, trackGroupArray, trackSelectionArray);
        }

        @Override // com.google.android.exoplayer2.video.VideoListener
        @Deprecated
        public /* bridge */ /* synthetic */ void onVideoSizeChanged(int i, int i2, int i3, float f) {
            VideoListener.CC.$default$onVideoSizeChanged(this, i, i2, i3, f);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.video.VideoListener
        public /* bridge */ /* synthetic */ void onVideoSizeChanged(VideoSize videoSize) {
            Player.Listener.CC.$default$onVideoSizeChanged(this, videoSize);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.audio.AudioListener
        public /* bridge */ /* synthetic */ void onVolumeChanged(float f) {
            Player.Listener.CC.$default$onVolumeChanged(this, f);
        }

        private ExoPlayerListener() {
            VideoPlayer.this = r1;
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public void onPlayWhenReadyChanged(boolean z, int i) {
            onPlaybackStateChanged(z, VideoPlayer.this.exoPlayer.getPlaybackState());
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public void onPlaybackStateChanged(int i) {
            onPlaybackStateChanged(VideoPlayer.this.exoPlayer.getPlayWhenReady(), i);
        }

        private void onPlaybackStateChanged(boolean z, int i) {
            if (!(i == 1 || i == 2)) {
                if (i == 3) {
                    if (VideoPlayer.this.window != null) {
                        if (z) {
                            VideoPlayer.this.window.addFlags(128);
                        } else {
                            VideoPlayer.this.window.clearFlags(128);
                        }
                    }
                    notifyPlayerReady();
                    return;
                } else if (i != 4) {
                    return;
                }
            }
            if (VideoPlayer.this.window != null) {
                VideoPlayer.this.window.clearFlags(128);
            }
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public void onPositionDiscontinuity(Player.PositionInfo positionInfo, Player.PositionInfo positionInfo2, int i) {
            if (VideoPlayer.this.playerPositionDiscontinuityCallback != null) {
                VideoPlayer.this.playerPositionDiscontinuityCallback.onPositionDiscontinuity(VideoPlayer.this, i);
            }
        }

        private void notifyPlayerReady() {
            if (VideoPlayer.this.playerStateCallback != null) {
                VideoPlayer.this.playerStateCallback.onPlayerReady();
            }
        }
    }
}
