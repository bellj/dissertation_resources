package org.thoughtcrime.securesms.video.videoconverter.muxer;

import android.util.SparseIntArray;
import java.io.IOException;
import java.nio.ByteBuffer;
import org.mp4parser.boxes.iso14496.part1.objectdescriptors.AudioSpecificConfig;
import org.mp4parser.boxes.iso14496.part1.objectdescriptors.DecoderConfigDescriptor;
import org.mp4parser.boxes.iso14496.part1.objectdescriptors.ESDescriptor;
import org.mp4parser.boxes.iso14496.part1.objectdescriptors.SLConfigDescriptor;
import org.mp4parser.boxes.iso14496.part12.SampleDescriptionBox;
import org.mp4parser.boxes.iso14496.part14.ESDescriptorBox;
import org.mp4parser.boxes.sampleentry.AudioSampleEntry;
import org.mp4parser.streaming.extensions.DefaultSampleFlagsTrackExtension;
import org.mp4parser.streaming.input.AbstractStreamingTrack;
import org.mp4parser.streaming.input.StreamingSampleImpl;

/* loaded from: classes5.dex */
public abstract class AacTrack extends AbstractStreamingTrack {
    private static final SparseIntArray SAMPLING_FREQUENCY_INDEX_MAP;
    private int sampleRate;
    private final SampleDescriptionBox stsd;

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
    }

    @Override // org.mp4parser.streaming.StreamingTrack
    public String getHandler() {
        return "soun";
    }

    @Override // org.mp4parser.streaming.StreamingTrack
    public String getLanguage() {
        return "```";
    }

    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        SAMPLING_FREQUENCY_INDEX_MAP = sparseIntArray;
        sparseIntArray.put(96000, 0);
        sparseIntArray.put(88200, 1);
        sparseIntArray.put(64000, 2);
        sparseIntArray.put(48000, 3);
        sparseIntArray.put(44100, 4);
        sparseIntArray.put(32000, 5);
        sparseIntArray.put(24000, 6);
        sparseIntArray.put(22050, 7);
        sparseIntArray.put(16000, 8);
        sparseIntArray.put(12000, 9);
        sparseIntArray.put(11025, 10);
        sparseIntArray.put(8000, 11);
    }

    public AacTrack(long j, long j2, int i, int i2, int i3) {
        this.sampleRate = i;
        DefaultSampleFlagsTrackExtension defaultSampleFlagsTrackExtension = new DefaultSampleFlagsTrackExtension();
        defaultSampleFlagsTrackExtension.setIsLeading(2);
        defaultSampleFlagsTrackExtension.setSampleDependsOn(2);
        defaultSampleFlagsTrackExtension.setSampleIsDependedOn(2);
        defaultSampleFlagsTrackExtension.setSampleHasRedundancy(2);
        defaultSampleFlagsTrackExtension.setSampleIsNonSyncSample(false);
        addTrackExtension(defaultSampleFlagsTrackExtension);
        SampleDescriptionBox sampleDescriptionBox = new SampleDescriptionBox();
        this.stsd = sampleDescriptionBox;
        AudioSampleEntry audioSampleEntry = new AudioSampleEntry("mp4a");
        if (i2 == 7) {
            audioSampleEntry.setChannelCount(8);
        } else {
            audioSampleEntry.setChannelCount(i2);
        }
        audioSampleEntry.setSampleRate((long) i);
        audioSampleEntry.setDataReferenceIndex(1);
        audioSampleEntry.setSampleSize(16);
        ESDescriptorBox eSDescriptorBox = new ESDescriptorBox();
        ESDescriptor eSDescriptor = new ESDescriptor();
        eSDescriptor.setEsId(0);
        SLConfigDescriptor sLConfigDescriptor = new SLConfigDescriptor();
        sLConfigDescriptor.setPredefined(2);
        eSDescriptor.setSlConfigDescriptor(sLConfigDescriptor);
        DecoderConfigDescriptor decoderConfigDescriptor = new DecoderConfigDescriptor();
        decoderConfigDescriptor.setObjectTypeIndication(64);
        decoderConfigDescriptor.setStreamType(5);
        decoderConfigDescriptor.setBufferSizeDB(1536);
        decoderConfigDescriptor.setMaxBitRate(j2);
        decoderConfigDescriptor.setAvgBitRate(j);
        AudioSpecificConfig audioSpecificConfig = new AudioSpecificConfig();
        audioSpecificConfig.setOriginalAudioObjectType(i3);
        audioSpecificConfig.setSamplingFrequencyIndex(SAMPLING_FREQUENCY_INDEX_MAP.get(i));
        audioSpecificConfig.setChannelConfiguration(i2);
        decoderConfigDescriptor.setAudioSpecificInfo(audioSpecificConfig);
        eSDescriptor.setDecoderConfigDescriptor(decoderConfigDescriptor);
        eSDescriptorBox.setEsDescriptor(eSDescriptor);
        audioSampleEntry.addBox(eSDescriptorBox);
        sampleDescriptionBox.addBox(audioSampleEntry);
    }

    @Override // org.mp4parser.streaming.StreamingTrack
    public long getTimescale() {
        return (long) this.sampleRate;
    }

    @Override // org.mp4parser.streaming.StreamingTrack
    public synchronized SampleDescriptionBox getSampleDescriptionBox() {
        return this.stsd;
    }

    public void processSample(ByteBuffer byteBuffer) throws IOException {
        this.sampleSink.acceptSample(new StreamingSampleImpl(byteBuffer, 1024), this);
    }
}
