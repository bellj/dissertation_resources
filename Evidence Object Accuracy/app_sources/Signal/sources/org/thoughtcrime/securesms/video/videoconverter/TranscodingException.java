package org.thoughtcrime.securesms.video.videoconverter;

/* access modifiers changed from: package-private */
/* loaded from: classes5.dex */
public final class TranscodingException extends Exception {
    public TranscodingException(String str) {
        super(str);
    }

    public TranscodingException(Throwable th) {
        super(th);
    }

    public TranscodingException(String str, Throwable th) {
        super(str, th);
    }
}
