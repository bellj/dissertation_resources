package org.thoughtcrime.securesms.video.videoconverter;

import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import org.thoughtcrime.securesms.media.MediaInput;
import org.thoughtcrime.securesms.video.VideoUtil;
import org.thoughtcrime.securesms.video.videoconverter.muxer.StreamingMuxer;

/* loaded from: classes5.dex */
public final class MediaConverter {
    private static final String TAG;
    private static final boolean VERBOSE;
    public static final String VIDEO_CODEC_H264;
    public static final String VIDEO_CODEC_H265;
    private int mAudioBitrate = 128000;
    private boolean mCancelled;
    private MediaInput mInput;
    private Listener mListener;
    private Output mOutput;
    private long mTimeFrom;
    private long mTimeTo;
    private int mVideoBitrate = VideoUtil.VIDEO_BIT_RATE;
    private String mVideoCodec = "video/avc";
    private int mVideoResolution;

    /* loaded from: classes5.dex */
    public interface Listener {
        boolean onProgress(int i);
    }

    /* loaded from: classes5.dex */
    public interface Output {
        Muxer createMuxer() throws IOException;
    }

    @Retention(RetentionPolicy.SOURCE)
    /* loaded from: classes.dex */
    public @interface VideoCodec {
    }

    public void setInput(MediaInput mediaInput) {
        this.mInput = mediaInput;
    }

    public void setOutput(File file) {
        this.mOutput = new FileOutput(file);
    }

    public void setOutput(FileDescriptor fileDescriptor) {
        this.mOutput = new FileDescriptorOutput(fileDescriptor);
    }

    public void setOutput(OutputStream outputStream) {
        this.mOutput = new StreamOutput(outputStream);
    }

    public void setTimeRange(long j, long j2) {
        this.mTimeFrom = j;
        this.mTimeTo = j2;
        if (j2 > 0 && j >= j2) {
            throw new IllegalArgumentException("timeFrom:" + j + " timeTo:" + j2);
        }
    }

    public void setVideoResolution(int i) {
        this.mVideoResolution = i;
    }

    public void setVideoCodec(String str) throws FileNotFoundException {
        if (selectCodec(str) != null) {
            this.mVideoCodec = str;
            return;
        }
        throw new FileNotFoundException();
    }

    public void setVideoBitrate(int i) {
        this.mVideoBitrate = i;
    }

    public void setAudioBitrate(int i) {
        this.mAudioBitrate = i;
    }

    public void setListener(Listener listener) {
        this.mListener = listener;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:30:0x0068 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:37:0x0076 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:45:0x0083 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:67:0x00b7 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:72:0x00bf */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r6v2 */
    /* JADX WARN: Type inference failed for: r6v3 */
    /* JADX WARN: Type inference failed for: r6v4, types: [java.lang.Throwable] */
    /* JADX WARN: Type inference failed for: r6v17 */
    /* JADX WARN: Type inference failed for: r6v18 */
    /* JADX WARN: Type inference failed for: r6v24 */
    /* JADX WARN: Type inference failed for: r6v27 */
    /* JADX WARN: Type inference failed for: r6v32 */
    /* JADX WARN: Type inference failed for: r6v33 */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x00c6 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0094 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x00d0 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00ac A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00ad  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x00d5 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x0099 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x009e A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void convert() throws org.thoughtcrime.securesms.video.videoconverter.EncodingException, java.io.IOException {
        /*
        // Method dump skipped, instructions count: 225
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.video.videoconverter.MediaConverter.convert():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:73:0x00cc, code lost:
        r18.verifyEndState();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void doExtractDecodeEditEncodeMux(org.thoughtcrime.securesms.video.videoconverter.VideoTrackConverter r18, org.thoughtcrime.securesms.video.videoconverter.AudioTrackConverter r19, org.thoughtcrime.securesms.video.videoconverter.Muxer r20) throws java.io.IOException, org.thoughtcrime.securesms.video.videoconverter.TranscodingException {
        /*
        // Method dump skipped, instructions count: 213
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.video.videoconverter.MediaConverter.doExtractDecodeEditEncodeMux(org.thoughtcrime.securesms.video.videoconverter.VideoTrackConverter, org.thoughtcrime.securesms.video.videoconverter.AudioTrackConverter, org.thoughtcrime.securesms.video.videoconverter.Muxer):void");
    }

    public static String getMimeTypeFor(MediaFormat mediaFormat) {
        return mediaFormat.getString("mime");
    }

    public static MediaCodecInfo selectCodec(String str) {
        int codecCount = MediaCodecList.getCodecCount();
        for (int i = 0; i < codecCount; i++) {
            MediaCodecInfo codecInfoAt = MediaCodecList.getCodecInfoAt(i);
            if (codecInfoAt.isEncoder()) {
                for (String str2 : codecInfoAt.getSupportedTypes()) {
                    if (str2.equalsIgnoreCase(str)) {
                        return codecInfoAt;
                    }
                }
                continue;
            }
        }
        return null;
    }

    /* loaded from: classes5.dex */
    private static class FileOutput implements Output {
        final File file;

        FileOutput(File file) {
            this.file = file;
        }

        @Override // org.thoughtcrime.securesms.video.videoconverter.MediaConverter.Output
        public Muxer createMuxer() throws IOException {
            return new AndroidMuxer(this.file);
        }
    }

    /* loaded from: classes5.dex */
    public static class FileDescriptorOutput implements Output {
        final FileDescriptor fileDescriptor;

        FileDescriptorOutput(FileDescriptor fileDescriptor) {
            this.fileDescriptor = fileDescriptor;
        }

        @Override // org.thoughtcrime.securesms.video.videoconverter.MediaConverter.Output
        public Muxer createMuxer() throws IOException {
            return new AndroidMuxer(this.fileDescriptor);
        }
    }

    /* loaded from: classes5.dex */
    public static class StreamOutput implements Output {
        final OutputStream outputStream;

        StreamOutput(OutputStream outputStream) {
            this.outputStream = outputStream;
        }

        @Override // org.thoughtcrime.securesms.video.videoconverter.MediaConverter.Output
        public Muxer createMuxer() {
            return new StreamingMuxer(this.outputStream);
        }
    }
}
