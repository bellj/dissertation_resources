package org.thoughtcrime.securesms.video.videoconverter.muxer;

/* access modifiers changed from: package-private */
/* loaded from: classes5.dex */
public final class MuxingException extends RuntimeException {
    public MuxingException(String str) {
        super(str);
    }

    public MuxingException(String str, Throwable th) {
        super(str, th);
    }
}
