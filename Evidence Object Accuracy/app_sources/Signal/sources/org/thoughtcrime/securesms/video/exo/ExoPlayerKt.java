package org.thoughtcrime.securesms.video.exo;

import com.google.android.exoplayer2.ExoPlayer;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: ExoPlayer.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0003\u001a\u00020\u0001*\u00020\u0002¨\u0006\u0004"}, d2 = {"configureForGifPlayback", "", "Lcom/google/android/exoplayer2/ExoPlayer;", "configureForVideoPlayback", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes5.dex */
public final class ExoPlayerKt {
    public static final void configureForGifPlayback(ExoPlayer exoPlayer) {
        Intrinsics.checkNotNullParameter(exoPlayer, "<this>");
        exoPlayer.setRepeatMode(2);
        exoPlayer.setVolume(0.0f);
    }

    public static final void configureForVideoPlayback(ExoPlayer exoPlayer) {
        Intrinsics.checkNotNullParameter(exoPlayer, "<this>");
        exoPlayer.setRepeatMode(0);
        exoPlayer.setVolume(1.0f);
    }
}
