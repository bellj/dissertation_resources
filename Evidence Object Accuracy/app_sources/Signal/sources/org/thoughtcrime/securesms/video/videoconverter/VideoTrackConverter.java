package org.thoughtcrime.securesms.video.videoconverter;

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCrypto;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.view.Surface;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicReference;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.media.MediaInput;

/* loaded from: classes5.dex */
public final class VideoTrackConverter {
    private static final String MEDIA_FORMAT_KEY_DISPLAY_HEIGHT;
    private static final String MEDIA_FORMAT_KEY_DISPLAY_WIDTH;
    private static final int OUTPUT_VIDEO_FRAME_RATE;
    private static final int OUTPUT_VIDEO_IFRAME_INTERVAL;
    private static final String TAG;
    private static final int TIMEOUT_USEC;
    private static final boolean VERBOSE;
    MediaFormat mEncoderOutputVideoFormat;
    final long mInputDuration;
    private final InputSurface mInputSurface;
    private Muxer mMuxer;
    long mMuxingVideoPresentationTime;
    private final OutputSurface mOutputSurface;
    private int mOutputVideoTrack = -1;
    private final long mTimeFrom;
    private final long mTimeTo;
    private int mVideoDecodedFrameCount;
    private final MediaCodec mVideoDecoder;
    private boolean mVideoDecoderDone;
    private final ByteBuffer[] mVideoDecoderInputBuffers;
    private final MediaCodec.BufferInfo mVideoDecoderOutputBufferInfo;
    private int mVideoEncodedFrameCount;
    private final MediaCodec mVideoEncoder;
    boolean mVideoEncoderDone;
    private final MediaCodec.BufferInfo mVideoEncoderOutputBufferInfo;
    private ByteBuffer[] mVideoEncoderOutputBuffers;
    private int mVideoExtractedFrameCount;
    private final MediaExtractor mVideoExtractor;
    boolean mVideoExtractorDone;

    public static VideoTrackConverter create(MediaInput mediaInput, long j, long j2, int i, int i2, String str) throws IOException, TranscodingException {
        MediaExtractor createExtractor = mediaInput.createExtractor();
        int andSelectVideoTrackIndex = getAndSelectVideoTrackIndex(createExtractor);
        if (andSelectVideoTrackIndex != -1) {
            return new VideoTrackConverter(createExtractor, andSelectVideoTrackIndex, j, j2, i, i2, str);
        }
        createExtractor.release();
        return null;
    }

    private VideoTrackConverter(MediaExtractor mediaExtractor, int i, long j, long j2, int i2, int i3, String str) throws IOException, TranscodingException {
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        this.mTimeFrom = j;
        this.mTimeTo = j2;
        this.mVideoExtractor = mediaExtractor;
        MediaCodecInfo selectCodec = MediaConverter.selectCodec(str);
        if (selectCodec != null) {
            MediaFormat trackFormat = mediaExtractor.getTrackFormat(i);
            this.mInputDuration = trackFormat.containsKey("durationUs") ? trackFormat.getLong("durationUs") : 0;
            int integer = trackFormat.containsKey("rotation-degrees") ? trackFormat.getInteger("rotation-degrees") : 0;
            if (trackFormat.containsKey(MEDIA_FORMAT_KEY_DISPLAY_WIDTH)) {
                i4 = trackFormat.getInteger(MEDIA_FORMAT_KEY_DISPLAY_WIDTH);
            } else {
                i4 = trackFormat.getInteger("width");
            }
            if (trackFormat.containsKey(MEDIA_FORMAT_KEY_DISPLAY_HEIGHT)) {
                i5 = trackFormat.getInteger(MEDIA_FORMAT_KEY_DISPLAY_HEIGHT);
            } else {
                i5 = trackFormat.getInteger("height");
            }
            if (i4 < i5) {
                i6 = (i5 * i2) / i4;
                i7 = i2;
            } else {
                i7 = (i4 * i2) / i5;
                i6 = i2;
            }
            int i10 = (i6 + 7) & -16;
            int i11 = (i7 + 7) & -16;
            if (integer % SubsamplingScaleImageView.ORIENTATION_180 == 90) {
                i8 = i11;
                i9 = i10;
            } else {
                i9 = i11;
                i8 = i10;
            }
            MediaFormat createVideoFormat = MediaFormat.createVideoFormat(str, i9, i8);
            createVideoFormat.setInteger("color-format", 2130708361);
            createVideoFormat.setInteger("bitrate", i3);
            createVideoFormat.setInteger("frame-rate", 30);
            createVideoFormat.setInteger("i-frame-interval", 1);
            AtomicReference<Surface> atomicReference = new AtomicReference<>();
            MediaCodec createVideoEncoder = createVideoEncoder(selectCodec, createVideoFormat, atomicReference);
            this.mVideoEncoder = createVideoEncoder;
            InputSurface inputSurface = new InputSurface(atomicReference.get());
            this.mInputSurface = inputSurface;
            inputSurface.makeCurrent();
            OutputSurface outputSurface = new OutputSurface();
            this.mOutputSurface = outputSurface;
            outputSurface.changeFragmentShader(createFragmentShader(trackFormat.getInteger("width"), trackFormat.getInteger("height"), i11, i10));
            MediaCodec createVideoDecoder = createVideoDecoder(trackFormat, outputSurface.getSurface());
            this.mVideoDecoder = createVideoDecoder;
            this.mVideoDecoderInputBuffers = createVideoDecoder.getInputBuffers();
            this.mVideoEncoderOutputBuffers = createVideoEncoder.getOutputBuffers();
            this.mVideoDecoderOutputBufferInfo = new MediaCodec.BufferInfo();
            this.mVideoEncoderOutputBufferInfo = new MediaCodec.BufferInfo();
            if (j > 0) {
                mediaExtractor.seekTo(1000 * j, 0);
                Log.i(TAG, "Seek video:" + j + " " + mediaExtractor.getSampleTime());
                return;
            }
            return;
        }
        Log.e(TAG, "Unable to find an appropriate codec for " + str);
        throw new FileNotFoundException();
    }

    public void setMuxer(Muxer muxer) throws IOException {
        this.mMuxer = muxer;
        if (this.mEncoderOutputVideoFormat != null) {
            Log.d(TAG, "muxer: adding video track.");
            this.mOutputVideoTrack = muxer.addTrack(this.mEncoderOutputVideoFormat);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0051  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void step() throws java.io.IOException, org.thoughtcrime.securesms.video.videoconverter.TranscodingException {
        /*
        // Method dump skipped, instructions count: 380
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.video.videoconverter.VideoTrackConverter.step():void");
    }

    public void release() throws Exception {
        Exception e;
        try {
            MediaExtractor mediaExtractor = this.mVideoExtractor;
            if (mediaExtractor != null) {
                mediaExtractor.release();
            }
            e = null;
        } catch (Exception e2) {
            e = e2;
            Log.e(TAG, "error while releasing mVideoExtractor", e);
        }
        try {
            MediaCodec mediaCodec = this.mVideoDecoder;
            if (mediaCodec != null) {
                mediaCodec.stop();
                this.mVideoDecoder.release();
            }
        } catch (Exception e3) {
            Log.e(TAG, "error while releasing mVideoDecoder", e3);
            if (e == null) {
                e = e3;
            }
        }
        try {
            OutputSurface outputSurface = this.mOutputSurface;
            if (outputSurface != null) {
                outputSurface.release();
            }
        } catch (Exception e4) {
            Log.e(TAG, "error while releasing mOutputSurface", e4);
            if (e == null) {
                e = e4;
            }
        }
        try {
            InputSurface inputSurface = this.mInputSurface;
            if (inputSurface != null) {
                inputSurface.release();
            }
        } catch (Exception e5) {
            Log.e(TAG, "error while releasing mInputSurface", e5);
            if (e == null) {
                e = e5;
            }
        }
        try {
            MediaCodec mediaCodec2 = this.mVideoEncoder;
            if (mediaCodec2 != null) {
                mediaCodec2.stop();
                this.mVideoEncoder.release();
            }
        } catch (Exception e6) {
            Log.e(TAG, "error while releasing mVideoEncoder", e6);
            if (e == null) {
                e = e6;
            }
        }
        if (e != null) {
            throw e;
        }
    }

    String dumpState() {
        Locale locale = Locale.US;
        Object[] objArr = new Object[8];
        boolean z = false;
        objArr[0] = Integer.valueOf(this.mVideoExtractedFrameCount);
        objArr[1] = Boolean.valueOf(this.mVideoExtractorDone);
        objArr[2] = Integer.valueOf(this.mVideoDecodedFrameCount);
        objArr[3] = Boolean.valueOf(this.mVideoDecoderDone);
        objArr[4] = Integer.valueOf(this.mVideoEncodedFrameCount);
        objArr[5] = Boolean.valueOf(this.mVideoEncoderDone);
        if (this.mMuxer != null) {
            z = true;
        }
        objArr[6] = Boolean.valueOf(z);
        objArr[7] = Integer.valueOf(this.mOutputVideoTrack);
        return String.format(locale, "V{extracted:%d(done:%b) decoded:%d(done:%b) encoded:%d(done:%b) muxing:%b(track:%d)} ", objArr);
    }

    public void verifyEndState() {
        boolean z = true;
        Preconditions.checkState("encoded (" + this.mVideoEncodedFrameCount + ") and decoded (" + this.mVideoDecodedFrameCount + ") video frame counts should match", this.mVideoDecodedFrameCount == this.mVideoEncodedFrameCount);
        if (this.mVideoDecodedFrameCount > this.mVideoExtractedFrameCount) {
            z = false;
        }
        Preconditions.checkState("decoded frame count should be less than extracted frame count", z);
    }

    private static String createFragmentShader(int i, int i2, int i3, int i4) {
        String str;
        float f = (float) i;
        float f2 = f / ((float) i3);
        float f3 = (float) i2;
        float f4 = f3 / ((float) i4);
        Log.i(TAG, "kernel " + f2 + "x" + f4);
        if (f2 > 2.0f || f4 > 2.0f) {
            int ceil = ((int) Math.ceil((double) (f2 - 0.1f))) / 2;
            int ceil2 = ((int) Math.ceil((double) (f4 - 0.1f))) / 2;
            int i5 = (ceil * 2) + 1;
            float f5 = (f2 / ((float) i5)) * (1.0f / f);
            int i6 = (ceil2 * 2) + 1;
            float f6 = (f4 / ((float) i6)) * (1.0f / f3);
            float f7 = (float) (i5 * i6);
            StringBuilder sb = new StringBuilder();
            for (int i7 = -ceil; i7 <= ceil; i7++) {
                for (int i8 = -ceil2; i8 <= ceil2; i8++) {
                    if (i7 != 0 || i8 != 0) {
                        sb.append("      + texture2D(sTexture, vTextureCoord.xy + vec2(");
                        sb.append(((float) i7) * f5);
                        sb.append(", ");
                        sb.append(((float) i8) * f6);
                        sb.append("))\n");
                    }
                }
            }
            str = "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 vTextureCoord;\nuniform samplerExternalOES sTexture;\nvoid main() {\n    gl_FragColor = (texture2D(sTexture, vTextureCoord)\n" + sb.toString() + "    ) / " + f7 + ";\n}\n";
        } else {
            str = "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nvarying vec2 vTextureCoord;\nuniform samplerExternalOES sTexture;\nvoid main() {\n    gl_FragColor = texture2D(sTexture, vTextureCoord);\n}\n";
        }
        Log.i(TAG, str);
        return str;
    }

    private MediaCodec createVideoDecoder(MediaFormat mediaFormat, Surface surface) throws IOException {
        MediaCodec createDecoderByType = MediaCodec.createDecoderByType(MediaConverter.getMimeTypeFor(mediaFormat));
        createDecoderByType.configure(mediaFormat, surface, (MediaCrypto) null, 0);
        createDecoderByType.start();
        return createDecoderByType;
    }

    private MediaCodec createVideoEncoder(MediaCodecInfo mediaCodecInfo, MediaFormat mediaFormat, AtomicReference<Surface> atomicReference) throws IOException {
        MediaCodec createByCodecName = MediaCodec.createByCodecName(mediaCodecInfo.getName());
        createByCodecName.configure(mediaFormat, (Surface) null, (MediaCrypto) null, 1);
        atomicReference.set(createByCodecName.createInputSurface());
        createByCodecName.start();
        return createByCodecName;
    }

    private static int getAndSelectVideoTrackIndex(MediaExtractor mediaExtractor) {
        for (int i = 0; i < mediaExtractor.getTrackCount(); i++) {
            if (isVideoFormat(mediaExtractor.getTrackFormat(i))) {
                mediaExtractor.selectTrack(i);
                return i;
            }
        }
        return -1;
    }

    private static boolean isVideoFormat(MediaFormat mediaFormat) {
        return MediaConverter.getMimeTypeFor(mediaFormat).startsWith("video/");
    }
}
