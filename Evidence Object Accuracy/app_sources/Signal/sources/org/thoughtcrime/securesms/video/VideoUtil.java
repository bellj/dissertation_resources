package org.thoughtcrime.securesms.video;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Size;
import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.mms.MediaConstraints;

/* loaded from: classes5.dex */
public final class VideoUtil {
    public static final int AUDIO_BIT_RATE;
    public static final String AUDIO_MIME_TYPE;
    public static final String RECORDED_VIDEO_CONTENT_TYPE;
    private static final int TOTAL_BYTES_PER_SECOND;
    public static final int VIDEO_BIT_RATE;
    public static final int VIDEO_FRAME_RATE;
    private static final int VIDEO_LONG_WIDTH;
    private static final int VIDEO_MAX_RECORD_LENGTH_S;
    private static final int VIDEO_MAX_UPLOAD_LENGTH_S = ((int) TimeUnit.MINUTES.toSeconds(10));
    public static final String VIDEO_MIME_TYPE;
    static final int VIDEO_SHORT_WIDTH;

    private VideoUtil() {
    }

    public static Size getVideoRecordingSize() {
        if (isPortrait(screenSize())) {
            return new Size(VIDEO_SHORT_WIDTH, VIDEO_LONG_WIDTH);
        }
        return new Size(VIDEO_LONG_WIDTH, VIDEO_SHORT_WIDTH);
    }

    public static int getMaxVideoRecordDurationInSeconds(Context context, MediaConstraints mediaConstraints) {
        return Math.min((int) Math.floor((double) (((float) mediaConstraints.getCompressedVideoMaxSize(context)) / 274000.0f)), 60);
    }

    public static int getMaxVideoUploadDurationInSeconds() {
        return VIDEO_MAX_UPLOAD_LENGTH_S;
    }

    private static Size screenSize() {
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        return new Size(displayMetrics.widthPixels, displayMetrics.heightPixels);
    }

    private static boolean isPortrait(Size size) {
        return size.getWidth() < size.getHeight();
    }
}
